using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Linq;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a Visual Tree combo box control.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ListControlElement" />
    public class ComboBoxElement : ListControlElement, IBindingViewModel
    {
        private readonly AutoCompleteStringCollection _autoCompleteStrings = null;
        private bool _selectTextOnFocus = true;
        private ComboBoxStyle _dropDownStyle = ComboBoxStyle.DropDown;
        private bool _matchComboboxWidth = false;
        private bool _droppedDown = false;
        private ComboboxColumnCollection _columns;
        private bool _locked = false;
        private int _dropDownWidth = -1;
        private int _dropDownHeight = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBoxElement"/> class.
        /// </summary>
        public ComboBoxElement()
        {
            _autoCompleteStrings = new AutoCompleteStringCollection();
            _columns = new ComboboxColumnCollection();
            ClientBeforeSelect = "return false;";
        }

        /// <summary>
        /// Get, set MatchComboboxWidth property . 
        /// MatchComboboxWidth Gives indication if the drop down list of the combo box fit the width of the combo box width .
        /// </summary>
        /// <value>
        /// true  Whether the  dropdown's width should be explicitly set to match the 
        /// width of the Combo box with , otherwise false .
        /// </value>
        [DefaultValue(false)]
        public bool MatchComboboxWidth
        {
            get
            {
                return this._matchComboboxWidth;
            }
            set
            {
                this._matchComboboxWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ComboBoxElement"/> is locked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if locked; otherwise, <c>false</c>.
        /// </value>
        public bool Locked
        {
            get
            {
                return _locked;
            }
            set
            {
                if (value != _locked)
                {
                    _locked = value;
                    OnPropertyChanged("Locked");
                }
            }
        }

      

        /// <summary>
        /// Get Column collection .
        /// </summary>
        public ComboboxColumnCollection Columns
        {
            get { return _columns; }
        }

        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void IBindingViewModel.Bind(IDataReader dataReader)
        {
            // If there is a valid data reader
            if (dataReader != null)
            {
                // Loop all fields
                for (int index = 0; index < dataReader.FieldCount; index++)
                {
                    _columns.Add(new ComboboxColumn(dataReader.GetName(index)));
                }
                if (_columns.Count > 0)
                {
                    using (this.BatchBlock)
                    {
                        int listIndex = 0;

                        // Loop all records
                        while (dataReader.Read())
                        {
                            // Create list item id.
                            string listItem_Id = string.Format("_{0}_listitem{1}", this.ClientID, listIndex++);

                            // Create List item .
                            ListItem listItem = new ListItem() { ID = listItem_Id };
                            int columnIndex = -1;

                            columnIndex = TryGetColumnIndex(this.DisplayMember);
                            if (columnIndex != -1)
                            {
                                listItem.Text = Convert.ToString(dataReader.GetValue(columnIndex));
                            }
                            columnIndex = TryGetColumnIndex(this.ValueMember);
                            if (columnIndex != -1)
                            {
                                listItem.Value = dataReader.GetValue(columnIndex);
                            }
                            Items.Add(listItem);
                        }
                    }
                }
            }

            OnPropertyChanged("DataSource");
        }

        /// <summary>
        /// Try to get the column index from .
        /// </summary>
        /// <param name="columnName">the column name</param>
        /// <returns>the column index based on column collection, -1 if the column name does not match any combobox column .</returns>
        private int TryGetColumnIndex(string columnName)
        {
            int index = 0;
            try
            {
                foreach (ComboboxColumn column in this.Columns)
                {
                    if (column.DataMember.Equals(columnName))
                    {
                        return index;
                    }
                    index++;
                }
            }
            catch (Exception ex)
            {
                index = -1;
            }
            return -1;
        }



        private bool _showFilter = false;
        /// <summary>
        /// Gets or sets a value indicating if the filter toolbar is shown.
        /// </summary>
        /// <value>
        /// True to show filter toolbar. If not, false.
        /// </value>
        [DefaultValue(false)]
        public bool ShowFilter
        {
            get { return _showFilter; }
            set
            {
                if (_showFilter != value)
                {
                    _showFilter = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        public override int SelectedIndex
        {
            get
            {
                if (base.SelectedIndex < 0 || base.SelectedIndex >= this.Items.Count)
                {
                    return -1;
                }
                return base.SelectedIndex;
            }
            set
            {

                // Raise the selected index changed event
                PerformSelectedIndexChanged(new ValueChangedArgs<int>(value));

                // Notify property SelectedCells changed
                OnPropertyChanged("Text");

            }
        }

        /// <summary>
        /// Sets the index of the selected.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        protected bool SetSelectedIndex(int value)
        {

            if (base.SelectedIndex != value)
            {
                base.SelectedIndex = value;

                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public object SelectedItem
        {
            get
            {
                if (SelectedIndex > -1 && Items.Count > SelectedIndex)
                {
                    return Items[SelectedIndex];
                }

                return null;
            }
            set
            {
                ListItem c = ListItem.GetItem(value);
                SelectedIndex = Items.IndexOf(c);
            }
        }

        /// <summary>
        /// Gets or sets the width of the of the drop-down portion of a combo box element .
        /// </summary>
        /// <value>
        /// The width, in pixels, of the drop-down box.
        /// </value>
        public int DropDownWidth
        {
            get
            {
                return _dropDownWidth;
            }
            set
            {
                if (_dropDownWidth != value && this.MatchComboboxWidth != true)
                {
                    _dropDownWidth = value;
                    OnPropertyChanged("DropDownWidth");
                }
            }
        }


        /// <summary>
        /// Get ,set DroppedDown property .
        /// </summary>
        /// <value>
        /// true if the drop down list is dropped , otherwise false .
        /// </value>
        public bool DroppedDown
        {
            get
            {
                return _droppedDown;
            }
            set
            {
                _droppedDown = value;
                // If the _droppedDown is true .
                if (_droppedDown)
                {
                    // expand the drop down list 
                    ExpandDropDownList();
                }
            }
        }


        /// <summary>
        /// Collapse the drop down list .
        /// </summary>
        [RendererMethodDescription]
        private void CollapseDropDownList()
        {
            this.InvokeMethodOnce("CollapseDropDownList");
        }

        /// <summary>
        /// Expand the drop down list .
        /// </summary>
        [RendererMethodDescription]
        private void ExpandDropDownList()
        {
            this.InvokeMethodOnce("ExpandDropDownList");
        }

        /// <summary>
        /// Gets or sets the height in pixels of the drop-down portion of the ComboBox .
        /// </summary>
        /// </value>
        /// The height, in pixels, of the drop-down box.
        /// </value>
        [RendererPropertyDescription]
        public int DropDownHeight
        {
            get
            {
                return _dropDownHeight;
            }

            set
            {
                _dropDownHeight = value;
                OnPropertyChanged("DropDownHeight");
            }

        }

        /// <summary>
        /// Gets or sets a value specifying the style of the combo box.
        /// </summary>
        /// <value>
        /// The drop down style.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(ComboBoxStyle.DropDown)]
        public ComboBoxStyle DropDownStyle
        {
            get { return _dropDownStyle; }
            set
            {
                if (_dropDownStyle != value)
                {
                    _dropDownStyle = value;
                    OnPropertyChanged("DropDownStyle");
                }
            }
        }

        /// <summary>
        /// Gets the selection start.
        /// </summary>
        /// <value>
        /// The selection start.
        /// </value>
        public int SelectionStart
        {
            get;
            set;

        }

        /// <summary>
        /// Gets the length of the selection.
        /// </summary>
        /// <value>
        /// The length of the selection.
        /// </value>
        public int SelectionLength
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the automatic complete custom source.
        /// </summary>
        /// <value>
        /// The automatic complete custom source.
        /// </value>
        public AutoCompleteStringCollection AutoCompleteCustomSource
        {
            get { return _autoCompleteStrings; }
        }

        /// <summary>
        /// DropDownClosed event handler.
        /// </summary>
        private event EventHandler _DropDownClosed;

        /// <summary>
        /// Add or remove DropDownClosed action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler DropDownClosed
        {
            add
            {
                bool needNotification = (_DropDownClosed == null);

                _DropDownClosed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DropDownClosed");
                }

            }
            remove
            {
                _DropDownClosed -= value;

                if (_DropDownClosed == null)
                {
                    OnEventHandlerDeattached("DropDownClosed");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DropDownClosed listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDropDownClosedListeners
        {
            get { return _DropDownClosed != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DropDownClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DropDownClosed"/> instance containing the event data.</param>
        protected virtual void OnDropDownClosed(EventArgs args)
        {

            // Check if there are listeners.
            if (_DropDownClosed != null)
            {
                _DropDownClosed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DropDownClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDropDownClosed(EventArgs args)
        {

            // Raise the DropDownClosed event.
            OnDropDownClosed(args);
        }

        /// <summary>
        /// SelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler _SelectedIndexChanged;

        /// <summary>
        /// Add or remove SelectedIndexChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler SelectedIndexChanged
        {
            add
            {
                bool needNotification = (_SelectedIndexChanged == null);

                _SelectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChanged");
                }

            }
            remove
            {
                _SelectedIndexChanged -= value;

                if (_SelectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedIndexChanged != null && _locked == false)
            {
                _SelectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformSelectedIndexChanged(EventArgs args)
        {
            // Try to get string value changed arguments
            ValueChangedArgs<int> valueChangedArgs = args as ValueChangedArgs<int>;

            // If there is a valid string value changed arguments

            if (valueChangedArgs != null)
            {
                string oldTextValue = this.Text;
                int value = valueChangedArgs.Value;
                if (SetSelectedIndex(value))
                {
                    if (this.Text != oldTextValue)
                    {   
                        // Try to get string value changed arguments
                        ValueChangedArgs<string> stringValueChangedArgs = new ValueChangedArgs<string>(this.Text);
                        // Raise the text changed event
                        PerformTextChanged(stringValueChangedArgs);

                    }
                    // Raise the selected index changed event
                    OnSelectedIndexChanged(valueChangedArgs);
                }
            }
        }

        /// <summary>
        /// SelectionChangeCommitted event handler.
        /// </summary>
        private event EventHandler _SelectionChangeCommitted;

        /// <summary>
        /// Add or remove SelectionChangeCommitted action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler SelectionChangeCommitted
        {
            add
            {
                bool needNotification = (_SelectionChangeCommitted == null);

                _SelectionChangeCommitted += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectionChangeCommitted");
                }

            }
            remove
            {
                _SelectionChangeCommitted -= value;

                if (_SelectionChangeCommitted == null)
                {
                    OnEventHandlerDeattached("SelectionChangeCommitted");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectionChangeCommitted listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectionChangeCommittedListeners
        {
            get { return _SelectionChangeCommitted != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectionChangeCommitted" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectionChangeCommitted"/> instance containing the event data.</param>
        protected virtual void OnSelectionChangeCommitted(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectionChangeCommitted != null)
            {
                _SelectionChangeCommitted(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectionChangeCommitted" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectionChangeCommitted(EventArgs args)
        {

            // Raise the SelectionChangeCommitted event.
            OnSelectionChangeCommitted(args);
        }

        /// <summary>
        /// DropDown event handler.
        /// </summary>
        private event EventHandler<EventArgs> _DropDown;

        /// <summary>
        /// Add or remove DropDown action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> DropDown
        {
            add
            {
                bool needNotification = (_DropDown == null);

                _DropDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DropDown");
                }

            }
            remove
            {
                _DropDown -= value;

                if (_DropDown == null)
                {
                    OnEventHandlerDeattached("DropDown");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DropDown listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDropDownListeners
        {
            get
            {
                if (this.MultiColumn)
                {
                    return true;
                }

                return _DropDown != null;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:DropDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DropDown"/> instance containing the event data.</param>
        protected virtual void OnDropDown(EventArgs args)
        {

            // Check if there are listeners.
            if (_DropDown != null)
            {
                _DropDown(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DropDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDropDown(EventArgs args)
        {
            // Raise the DropDown event.
            OnDropDown(args);
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Add(string item)
        {
            Add(new ListItem(item), false);
        }

        /// <summary>
        /// Returns the index of the first item in the ComboBox that starts with the specified string.
        /// </summary>
        /// <param name="subStr">The String to search for.</param>
        /// <returns>he zero-based index of the first item found; returns -1 if no match is found.</returns>
        public int FindString(string subStr)
        {
            if (this.Items != null)
            {
                foreach (ListItem item in this.Items)
                {
                    if (!String.IsNullOrEmpty(item.Text))
                    {
                        if (item.Text.IndexOf(subStr, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            return this.Items.IndexOf(item);
                        }
                    }
                }
            }
            return -1;
        }
        /// <summary>
        /// Returns the index of the first item in the ComboBox that starts with the specified string.
        /// </summary>
        /// <param name="exStr">The String to search for.</param>
        /// <returns>he zero-based index of the first item found; returns -1 if no match is found.;returns 0 if the item is empty</returns>
        /// 
        public int FindStringExact(string exStr)
        {
            if (this.Items == null)
                return 0;
            foreach (ListItem item in this.Items)
            {
                if (item.ToString() == exStr)
                {
                    return this.Items.IndexOf(item);
                }
            }
            return -1;
        }

        /// <summary>
        /// Get,Set SelectTextOnFocus property . 
        /// </summary>
        /// <value>
        /// true to select the text of focus , otherwise false.
        /// </value>
        [RendererPropertyDescription]
        public bool SelectTextOnFocus
        {
            get { return _selectTextOnFocus; }
            set
            {
                if (this._selectTextOnFocus != value)
                {
                    this._selectTextOnFocus = value;
                }
            }
        }


        /// <summary>
        /// Tells the renderer whether to expand on text changed or not. 
        /// </summary>
        private bool _AllowExpand = true;


        /// <summary>
        /// Tells the renderer whether to expand on or not.
        /// </summary>
        public bool AllowExpand
        {
            get
            {
                return _AllowExpand;
            }
            set
            {
                if (_AllowExpand != value)
                {
                    _AllowExpand = value;
                }
            }
        }



        /// <summary>
        /// Gets or sets the client before select.
        /// </summary>
        /// <value>
        /// The client before select.
        /// </value>
        [DesignerIgnore]
        public string ClientBeforeSelect { get; set; }

        /// <summary>
        /// BeforeSelect event handler.
        /// </summary>
        private event EventHandler<EventArgs> _beforeSelect;

        /// <summary>
        /// Add or remove BeforeSelect action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> BeforeSelect
        {
            add
            {
                bool needNotification = (_beforeSelect == null);

                _beforeSelect += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeSelect");
                }

            }
            remove
            {
                _beforeSelect -= value;

                if (_beforeSelect == null)
                {
                    OnEventHandlerDeattached("BeforeSelect");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeSelect listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeSelectListeners
        {
            get { return this.Locked; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeSelect"/> instance containing the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs args)
        {

            // Check if there are listeners.
            if (_beforeSelect != null)
            {
                _beforeSelect(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeSelect(EventArgs args)
        {

            // Raise the BeforeSelect event.
            OnBeforeSelect(args);
        }

        /// <summary>
        /// Gets or sets the selected text.
        /// </summary>
        /// <value>
        /// The selected text.
        /// </value>
        public override object SelectedText
        {

            get
            {

                if (SelectedIndex > -1 && Items.Count > SelectedIndex)
                {
                    return Items[SelectedIndex].Text;
                }
                return null;
            }
            set
            {
                ListItem listItem = this.Items.FirstOrDefault(item => (item.Text.Equals(value)));
                if (listItem != null)
                {
                    SelectedIndex = this.Items.IndexOf(listItem);

                }
            }
        }

        /// <summary>
        /// Gets or sets the selected value.
        /// </summary>
        /// <value>
        /// The selected value.
        /// </value>
        public override object SelectedValue
        {

            get
            {
                if (SelectedIndex > -1 && Items.Count > SelectedIndex)
                {
                    return Items[SelectedIndex].Value;
                }

                return null;
            }
            set
            {
                ListItem listItem = this.Items.FirstOrDefault(item => (item.Value != null && item.Value.Equals(value)));
                if (listItem != null)
                {
                    SelectedIndex = this.Items.IndexOf(listItem);

                    // Raise the SelectedValueChanged event.
                    OnSelectedValueChanged(EventArgs.Empty);
                }
            }
        }
    }
}
