﻿namespace System.Web.VisualTree.Elements
{
    public class ToolBarDropDownMenu : ToolBarDropDown
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownMenu"/> class.
        /// </summary>
        public ToolBarDropDownMenu()
        {
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Get toolbar item
            ToolBarItem objToolBarItem = visualElement as ToolBarItem;

            // If there is a valid toolbar item
            if (objToolBarItem != null)
            {
                // Add toolbar item
                Items.Add(objToolBarItem);
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }
    }
}
