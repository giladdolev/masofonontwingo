using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class NotifyIcon : VisualElement
    {
        public NotifyIcon() { }


        public NotifyIcon(IContainer container)
        {
            this.ParentElement = (VisualElement)container;

        }


        public bool Visible
        {
            get;
            set;
        }


        public string Text
        {
            get;
            set;
        }


        public Icon Icon
        {
            get;
            set;
        }


        /// <summary>
        /// DoubleClick event handler.
        /// </summary>
        private event EventHandler _DoubleClick;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove DoubleClick action.
        /// </summary>
        public event EventHandler DoubleClick
        {
            add
            {
                bool needNotification = (_DoubleClick == null);

                _DoubleClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DoubleClick");
                }

            }
            remove
            {
                _DoubleClick -= value;

                if (_DoubleClick == null)
                {
                    OnEventHandlerDeattached("DoubleClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DoubleClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDoubleClickListeners
        {
            get { return _DoubleClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DoubleClick"/> instance containing the event data.</param>
        protected virtual void OnDoubleClick(EventArgs args)
        {

            // Check if there are listeners.
            if (_DoubleClick != null)
            {
                _DoubleClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDoubleClick(EventArgs args)
        {

            // Raise the DoubleClick event.
            OnDoubleClick(args);
        }


        /// <summary>
        /// Click event handler.
        /// </summary>
        private event EventHandler _Click;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Click action.
        /// </summary>
        public event EventHandler Click
        {
            add
            {
                bool needNotification = (_Click == null);

                _Click += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Click");
                }

            }
            remove
            {
                _Click -= value;

                if (_Click == null)
                {
                    OnEventHandlerDeattached("Click");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Click listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasClickListeners
        {
            get { return _Click != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Click"/> instance containing the event data.</param>
        protected virtual void OnClick(EventArgs args)
        {

            // Check if there are listeners.
            if (_Click != null)
            {
                _Click(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformClick(EventArgs args)
        {

            // Raise the Click event.
            OnClick(args);
        }
    }
}
