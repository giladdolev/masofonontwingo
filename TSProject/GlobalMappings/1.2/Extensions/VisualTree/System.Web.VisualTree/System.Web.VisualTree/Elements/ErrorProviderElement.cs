﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ErrorProviderElement : VisualElement
    {
        private int _blinkRate = 250;
        private ControlContainerElement _controlContainer;
        private IconReference _icon;
        private bool _rightToLeft = false;
        private Dictionary<ControlElement, string> errorList = new Dictionary<ControlElement, string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorProviderElement"/> class.
        /// </summary>
        public ErrorProviderElement()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorProviderElement"/> class.
        /// </summary>
        public ErrorProviderElement(ControlContainerElement controlContainer)
        {
            this.ControlContainer = controlContainer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorProviderElement"/> class.
        /// </summary>
        public ErrorProviderElement(IContainer container)
        {

        }



        /// <summary>
        /// Gets or sets a value indicating whether this textbox allows blank
        /// </summary>

        [DefaultValue(250)]
        public int BlinkRate
        {
            get { return _blinkRate; }
            set { _blinkRate = value; }
        }



        /// <summary>
        /// Gets or sets the control container.
        /// </summary>
        /// <value>
        /// the control container.
        /// </value>

        public ControlContainerElement ControlContainer
        {
            get
            {
                return _controlContainer;
            }
            set
            {
                _controlContainer = value;
            }
        }



        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        ///   the icon.
        /// </value>

        public IconReference Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
            }
        }



        /// <summary>
        /// Gets or sets rightToLeft.
        /// </summary>
        /// <value>
        /// 	<c>true</c> the error icon appears to the left of the associated control and the ToolTip text is displayed in right-to-left order;; otherwise, <c>false</c>.
        /// </value>

        public bool RightToLeft
        {
            get
            {
                return _rightToLeft;
            }
            set
            {
                _rightToLeft = value;
            }
        }

        /// <summary>
        /// Sets the error description string for the specified control.
        /// </summary>
        /// <param name="control">The control to set the error description string for.</param>
        /// <param name="value">The error description string, or null or Empty to remove the error.</param>
        public void SetError(ControlElement control, string value)
        {

            if (!errorList.ContainsKey(control))
            {
                if (string.IsNullOrEmpty(value))
                {
                    errorList.Remove(control);
                }
                else
                {
                    errorList.Add(control, value);
                    control.SetError(value);
                }
            }
            else
            {
                errorList[control] = value;
                control.SetError(value);
            }
        }

        /// <summary>
        /// Returns the current error description string for the specified control.
        /// </summary>
        /// <param name="control">The item to get the error description string for.</param>
        /// <returns>The error description string for the specified control.</returns>
        public string GetError(ControlElement control)
        {
            if (control != null)
            {
                if (errorList.ContainsKey(control))
                {
                    return errorList[control];
                }
            }
            return "";
        }

        /// <summary>
        /// Clears the error list of the ErrorProviderElemenet.
        /// </summary>
        public void Clear()
        {
            if (errorList != null)
            {
                errorList.Clear();
            }
        }
    }
}
