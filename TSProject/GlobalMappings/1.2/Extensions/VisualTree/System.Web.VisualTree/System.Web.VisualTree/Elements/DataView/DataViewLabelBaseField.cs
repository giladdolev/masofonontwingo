﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DataViewLabelBaseField : DataViewField
    {

        /// <summary>
        /// The template text
        /// </summary>
        private string _templateText = null;

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get
            {
                return NormalizeTemplate(_templateText);
            }
            set
            {
                _templateText = NormalizeTemplate(value);
            }
        }

        /// <summary>
        /// Gets or sets the template text.
        /// </summary>
        /// <value>
        /// The template text.
        /// </value>
        [RendererPropertyDescription]
        public string TemplateText
        {
            get
            {
                // If there is no valid template text
                if (string.IsNullOrWhiteSpace(_templateText))
                {
                    // Get the data member
                    string dataMember = this.DataMember;

                    // If there is a valid data member
                    if(!string.IsNullOrEmpty(dataMember))
                    {
                        // Set default template to data member
                        return string.Concat("{", dataMember, "}");
                    }
                    else
                    {
                        // Return empty template
                        return string.Empty;
                    }
                }
                else
                {
                    return _templateText;
                }
            }
            set
            {
                // If template text has changed
                if (_templateText != value)
                {
                    // Set template text
                    _templateText = value;

                    OnPropertyChanged("TemplateText");
                }
            }
        }
    }
}
