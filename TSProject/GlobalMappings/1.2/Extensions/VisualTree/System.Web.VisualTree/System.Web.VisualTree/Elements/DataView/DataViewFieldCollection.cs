﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class DataViewFieldCollection : VisualElementCollection<DataViewField>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewFieldCollection" /> class.
        /// </summary>
        /// <param name="dataBand">The data band.</param>
        /// <param name="propertyName">The property name.</param>
        public DataViewFieldCollection(DataViewBand dataBand, string propertyName)
            : base(dataBand, propertyName)
        {

        }
    }
}
