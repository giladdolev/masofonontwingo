﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Contains information that enables a <see cref="T:Binding"/> to resolve a data binding to either the property of an object or the property of the current object in a list of objects.
    /// </summary>
    /// <filterpriority>2</filterpriority>
    public struct BindingMemberInfo
    {
        private string dataList;
        private string dataField;

        /// <summary>
        /// Gets the property name, or the period-delimited hierarchy of property names, that comes before the property name of the data-bound object.
        /// </summary>
        /// 
        /// <returns>
        /// The property name, or the period-delimited hierarchy of property names, that comes before the data-bound object property name.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public string BindingPath
        {
            get
            {
                if (this.dataList == null)
                    return "";
                else
                    return this.dataList;
            }
        }

        /// <summary>
        /// Gets the property name of the data-bound object.
        /// </summary>
        /// 
        /// <returns>
        /// The property name of the data-bound object. This can be an empty string ("").
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public string BindingField
        {
            get
            {
                if (this.dataField == null)
                    return "";
                else
                    return this.dataField;
            }
        }

        /// <summary>
        /// Gets the information that is used to specify the property name of the data-bound object.
        /// </summary>
        /// 
        /// <returns>
        /// An empty string (""), a single property name, or a hierarchy of period-delimited property names that resolves to the property name of the final data-bound object.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public string BindingMember
        {
            get
            {
                if (this.BindingPath.Length <= 0)
                    return this.BindingField;
                else
                    return this.BindingPath + "." + this.BindingField;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BindingMemberInfo"/> class.
        /// </summary>
        /// <param name="dataMember">A navigation path that resolves to either the property of an object or the property of the current object in a list of objects. </param>
        public BindingMemberInfo(string dataMember)
        {
            if (dataMember == null)
                dataMember = "";
            int length = dataMember.LastIndexOf(".");
            if (length != -1)
            {
                this.dataList = dataMember.Substring(0, length);
                this.dataField = dataMember.Substring(length + 1);
            }
            else
            {
                this.dataList = "";
                this.dataField = dataMember;
            }
        }

        /// <summary>
        /// Determines whether two <see cref="T:BindingMemberInfo"/> objects are equal.
        /// </summary>
        /// 
        /// <returns>
        /// true if the <see cref="P:BindingMemberInfo.BindingMember"/> strings for <paramref name="a"/> and <paramref name="b"/> are equal; otherwise false.
        /// </returns>
        /// <param name="a">The first <see cref="T:BindingMemberInfo"/> to compare for equality.</param><param name="b">The second <see cref="T:BindingMemberInfo"/> to compare for equality.</param>
        public static bool operator ==(BindingMemberInfo a, BindingMemberInfo b)
        {
            return a.Equals((object)b);
        }

        /// <summary>
        /// Determines whether two <see cref="T:BindingMemberInfo"/> objects are not equal.
        /// </summary>
        /// 
        /// <returns>
        /// true if the <see cref="P:BindingMemberInfo.BindingMember"/> strings for <paramref name="a"/> and <paramref name="b"/> are not equal; otherwise false.
        /// </returns>
        /// <param name="a">The first <see cref="T:BindingMemberInfo"/> to compare for inequality.</param><param name="b">The second <see cref="T:BindingMemberInfo"/> to compare for inequality.</param>
        public static bool operator !=(BindingMemberInfo a, BindingMemberInfo b)
        {
            return !a.Equals((object)b);
        }

        /// <summary>
        /// Determines whether the specified object is equal to this <see cref="T:BindingMemberInfo"/>.
        /// </summary>
        /// 
        /// <returns>
        /// true if <paramref name="otherObject"/> is a <see cref="T:BindingMemberInfo"/> and both <see cref="P:BindingMemberInfo.BindingMember"/> strings are equal; otherwise false.
        /// </returns>
        /// <param name="otherObject">The object to compare for equality.</param><filterpriority>1</filterpriority>
        public override bool Equals(object otherObject)
        {
            if (otherObject is BindingMemberInfo)
                return string.Equals(this.BindingMember, ((BindingMemberInfo)otherObject).BindingMember, StringComparison.OrdinalIgnoreCase);
            else
                return false;
        }

        /// <summary>
        /// Returns the hash code for this <see cref="T:BindingMemberInfo"/>.
        /// </summary>
        /// 
        /// <returns>
        /// The hash code for this <see cref="T:BindingMemberInfo"/>.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
