﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// ComboBox style
    /// </summary>
    public enum ComboBoxStyle
    {
        /// <summary>
        /// The simple
        /// </summary>
        Simple,
        /// <summary>
        /// The drop down
        /// </summary>
        DropDown,
        /// <summary>
        /// The drop down list
        /// </summary>
        DropDownList
    }
}
