﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class ControlElementCollection : VisualElementCollection<ControlElement>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlElementCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ControlElementCollection(ControlElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }


        /// <summary>
        /// Finds the specified predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public ControlElement[] Find(Predicate<ControlElement> predicate)
        {
            return this.Where(element => predicate(element)).ToArray();
        }

    }
}
