﻿namespace System.Web.VisualTree.Elements
{
    public enum TabAlignment
    {
        Top,
        Bottom,
        Left,
        Right
    }
}
