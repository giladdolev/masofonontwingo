﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Store ListViewItem data.
    /// </summary>
    public class ListViewItemData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItemData"/> class.
        /// </summary>
        /// <param name="row">The row index.</param>
        /// <param name="col">The column index.</param>
        /// <param name="foreColor">The fore color.</param>
        public ListViewItemData(int row,int col,Color foreColor)
        {
            RowIndex = row;
            ColumnIndex = col;
            ForeColor = foreColor;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItemData"/> class.
        /// </summary>
        public ListViewItemData()
        {
        }

        /// <summary>
        /// Get or Set the Column Index.
        /// </summary>
        public int ColumnIndex
        {
            get;
            set;
        }  
        /// <summary>
        /// Get or Set the Row Index.
        /// </summary>
        public int RowIndex
        {
            get;
            set;
        }  
        /// <summary>
        /// Get or Set the Fore Color.
        /// </summary>
        public Color ForeColor
        {
            get;
            set;
        } 
    }
}
