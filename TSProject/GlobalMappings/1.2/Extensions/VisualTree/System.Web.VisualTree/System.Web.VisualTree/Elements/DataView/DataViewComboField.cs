﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.ComponentModel;

namespace System.Web.VisualTree.Elements
{
    public class DataViewComboField : DataViewField
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewComboField"/> class.
        /// </summary>
        public DataViewComboField()
        {
            
        }

        
        private bool _Sorted;
        /// <summary>
        /// Gets or sets the Sorted.
        /// </summary>
        /// <value>
        /// The Sorted.
        /// </value>
        [RendererPropertyDescription]
        [Category("Behavior")]
        public bool Sorted
        {
            get { return _Sorted; }
            set
            {
                if (_Sorted != value)
                {
                    _Sorted = value;

                    OnPropertyChanged("Sorted");
                }
            }
        }

        /// <summary>
        /// Gets or sets the value member.
        /// </summary>
        /// <value>
        /// The value member.
        /// </value>
        [RendererPropertyDescription]
        public string ValueMember
        {
            get
            {
                // Get binding 
                Binding binding = Binding.GetDataBinding(this, "Value");

                // If there is a valid binding
                if (binding != null)
                {
                    // Return the source
                    return binding.Source;
                }

                return null;
            }
            set
            {
                // Set the data binding
                Binding.SetDataBinding(this, value, "Value");
            }
        }

        /// <summary>
        /// Gets or sets the value member.
        /// </summary>
        /// <value>
        /// The value member.
        /// </value>
        [RendererPropertyDescription]
        public string DisplayMember
        {
            get
            {
                // Get binding 
                Binding binding = Binding.GetDataBinding(this, "Text");

                // If there is a valid binding
                if (binding != null)
                {
                    // Return the source
                    return binding.Source;
                }

                return null;
            }
            set
            {
                // Set the data binding
                Binding.SetDataBinding(this, value, "Text");
            }
        }

         /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                // Get data source as items list
                List<ListItem> listItems = this.DataSource as List<ListItem>;

                // If there is a valid list items
                if (listItems != null)
                {
                    // Return list items as children
                    return listItems;
                }

                // Return empty array
                return VisualElement.EmptyArray;
            }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Get list item
            ListItem listItem = visualElement as ListItem;

            // If there is a valid list item
            if (listItem != null)
            {
                // Get data source as list items
                List<ListItem> listItems = this.DataSource as List<ListItem>;

                // If there is no valid list items
                if(listItems == null)
                {
                    // Create list items settings
                    this.DataSource = listItems = new List<ListItem>();
                    this.ValueMember = "Value";
                    this.DisplayMember = "Text";
                }

                // Add item to list items
                listItems.Add(listItem);
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }
    }
}
