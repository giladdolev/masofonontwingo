﻿using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.TableStyle" />
    [DesignerComponent]
    public class ColumnStyle : TableStyle
    {

        /// <summary>
        /// The empty array
        /// </summary>
        private static ColumnStyle[] _EmptyColumnStyleArray = new ColumnStyle[] { };


        /// <summary>
        /// The size type
        /// </summary>
        private readonly SizeType _sizeType;

        /// <summary>
        /// The size
        /// </summary>
        private readonly float _size;


        /// <summary>
        /// Gets the empty array.
        /// </summary>
        /// <value>
        /// The empty array.
        /// </value>
        public static new ColumnStyle[] EmptyArray
        {
            get
            {
                return _EmptyColumnStyleArray;
            }
        }



        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public float Size
        {
            get { return _size; }
        }



        /// <summary>
        /// Gets the type of the size.
        /// </summary>
        /// <value>
        /// The type of the size.
        /// </value>
        public SizeType SizeType
        {
            get { return _sizeType; }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnStyle"/> class.
        /// </summary>
        /// <param name="sizeType">The size type.</param>
        /// <param name="size">The size.</param>
        public ColumnStyle(SizeType sizeType, float size)
        {
            _size = size;
            _sizeType = sizeType;
        }



        public ColumnStyle()
        {

        }
    }
}