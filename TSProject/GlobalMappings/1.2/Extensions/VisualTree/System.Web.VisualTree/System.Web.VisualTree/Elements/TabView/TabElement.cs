using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;
using System.Web.VisualTree.Utilities;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents the method that will handle the Selected or Deselected event of a TabControl control.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="TabControlEventArgs"/> instance containing the event data.</param>
    public delegate void TabControlEventHandler(object sender, TabControlEventArgs e);


    /// <summary>
    /// Represents the method that will handle the Selecting or Deselecting event of a TabControl control.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="TabViewCancelEventArgs"/> instance containing the event data.</param>
    public delegate void TabViewCancelEventHandler(object sender, TabViewCancelEventArgs e);



    public class TabElement : ControlContainerElement, IWindowTargetContainer, IWindowTarget, ITVisualContainerElement, IClinetHistoryContainerElement
    {
        private int _selectedIndex;
        private int _previousIndex = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="TabElement"/> class.
        /// </summary>
        public TabElement()
        {

        }


        /// <summary>
        /// Gets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        TVisualLayout ITVisualContainerElement.Layout
        {
            get
            {
                return TVisualLayout.Default;
            }
        }


        /// <summary>
        /// Gets or sets the tabs per row.
        /// </summary>
        /// <value>
        /// The tabs per row.
        /// </value>
        public int TabsPerRow
        {
            get;
            set;
        }



        /// <summary>
        ///Determines whether text in the caption of each tab will wrap to the next line if it is too long.
        /// </summary>
        public bool WordWrap
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        [RendererPropertyDescription]
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;//otherwise return the selected item index
            }
            set { SetSelectedIndex(value); }
        }


        /// <summary>
        /// Gets or sets the index of the Previous.
        /// </summary>
        /// <value>
        /// The index of the Previous..
        /// </value>
        [RendererPropertyDescription]
        public int PreviousIndex
        {
            get
            {
                //if the tab control does not have items return - 1
                if (this.TabItems.Count == 0)
                {
                    _previousIndex = -1;
                }
                return _previousIndex;//otherwise return the selected item index
            }
        }




        /// <summary>
        /// Gets or sets the alignment.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        [RendererPropertyDescription]
        public TabAlignment Alignment
        {
            get;
            set;
        }



        /// <summary>
        /// Gets the tab items.
        /// </summary>
        /// <value>
        /// The tab items.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<TabItem>))]
        public TabItemCollection TabItems
        {
            get { return Controls as TabItemCollection; }
        }



        /// <summary>
        /// Creates the control element collection.
        /// </summary>
        /// <returns></returns>
        protected override ControlElementCollection CreateControlElementCollection()
        {
            return new TabItemCollection(this, "Controls");
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // If is a control
            if (visualElement is ControlElement)
            {
                // Validate the control is only a tab page
                if (!(visualElement is TabItem))
                {
                    // Don't add element
                    return;
                }
            }
            base.Add(visualElement, false);
        }



        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>
        /// The selected tab.
        /// </value>
        [System.Web.UI.IDReferenceProperty]
        public TabItem SelectedTab
        {
            get
            {
                int selectedIndex = SelectedIndex;

                if (selectedIndex == -1 || TabItems.Count == 0 || SelectedIndex >= TabItems.Count)
                {
                    return null;
                }

                // update tabIndex property
                if (TabItems[selectedIndex] != null)
                {
                    TabItems[selectedIndex].TabIndex = selectedIndex;
                }

                return TabItems[selectedIndex];
            }
            set { SetSelectedTab(value); }
        }



        /// <summary>
        /// Sets the selected tab.
        /// </summary>
        /// <param name="selectedTabItem">The selected tab.</param>
        /// <param name="raisePropertyChanged">if set to <c>true</c> raise property changed.</param>
        internal void SetSelectedTab(TabItem selectedTabItem, bool raisePropertyChanged = true)
        {
            int tabItemIndex = -1;

            // If there is a valid tab
            if (selectedTabItem != null)
            {
                tabItemIndex = TabItems.IndexOf(selectedTabItem);
            }

            SetSelectedIndex(tabItemIndex, raisePropertyChanged);
        }



        /// <summary>
        /// Sets the index of the selected.
        /// </summary>
        /// <param name="selectedTabIndex">The value.</param>
        /// <param name="raisePropertyChanged">if set to <c>true</c> raise property changed.</param>
        private void SetSelectedIndex(int selectedTabIndex, bool raisePropertyChanged = true)
        {
            // If there is a valid different tab index
            if (_selectedIndex != selectedTabIndex && selectedTabIndex > -1)
            {
                // Set previous index
                _previousIndex = _selectedIndex;
                // Set the selected index
                _selectedIndex = selectedTabIndex;

                // Get the selected tab item
                TabItem tabItem = this.SelectedTab;

                // If there is a valid tab item
                if (tabItem != null)
                {
                    // If we should report history
                    if (this.ShouldReportHistoryChange())
                    {
                        // Ensure this response is marked as an history
                        ApplicationElement.EnsureHistoryBookmark();
                    }
                }

                // If we should raise property changed
                if (raisePropertyChanged)
                {
                    // Raise property changed
                    OnPropertyChanged("SelectedIndex");
                }

                // Raise the selected index changed
                OnSelectedIndexChanged(EventArgs.Empty);
            }
        }


        /// <summary>
        /// Makes the specified TabItem the current tab.
        /// </summary>
        /// <param name="tabPage">The TabItem to select.</param>
        public void SelectTab(TabItem tabPage)
        {
            TabItem tabItem = TabItems[tabPage.TabIndex] as TabItem;
            PerformSelectTab(tabItem);
        }

        /// <summary>
        /// Makes the tab with the specified name the current tab.
        /// </summary>
        /// <param name="tabPageName">The TabItem Name of the tab to select.</param>
        public void SelectTab(string tabPageName)
        {
            TabItem tabItem = TabItems[tabPageName] as TabItem;
            PerformSelectTab(tabItem);
        }

        /// <summary>
        /// Selects the tab.
        /// </summary>
        /// <param name="index">The index.</param>
        public void SelectTab(int index)
        {
            TabItem tabItem = TabItems[index] as TabItem;
            PerformSelectTab(tabItem);
        }

        /// <summary>
        /// perform select tab
        /// </summary>
        /// <param name="tabItem">tab item to select</param>
        private void PerformSelectTab(TabItem tabItem)
        {
            if (tabItem != null)
            {
                tabItem.PerformTabActivated(EventArgs.Empty);
            }
        }

        /// <summary>
        /// SelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler _SelectedIndexChanged;

        /// <summary>
        /// Add or remove SelectedIndexChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler SelectedIndexChanged
        {
            add
            {
                bool needNotification = (_SelectedIndexChanged == null);

                _SelectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChanged");
                }

            }
            remove
            {
                _SelectedIndexChanged -= value;

                if (_SelectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedIndexChanged != null)
            {
                _SelectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectedIndexChanged(EventArgs args)
        {
            // Get the selected tab data
            ValueChangedArgs<string> valueChangedArgs = args as ValueChangedArgs<string>;

            // If there is valid data for selected tab
            if (valueChangedArgs != null)
            {
                // Get selected tab from tab id
                TabItem selectedTab = GetChildVisualElement<TabItem>(valueChangedArgs.Value);

                // If there is a valid selected tab
                if (selectedTab != null)
                {
                    // Select the tab
                    SetSelectedTab(selectedTab, false);
                }
            }

            // To emulate desktop like event we do the click event
            PerformClick(args);
        }


        /// <summary>
        /// Selecting event handler.
        /// </summary>
        private event TabViewCancelEventHandler _Selecting;

        /// <summary>
        /// Add or remove Selecting action.
        /// </summary>
        [RendererEventDescription]
        public event TabViewCancelEventHandler Selecting
        {
            add
            {
                bool needNotification = (_Selecting == null);

                _Selecting += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Selecting");
                }

            }
            remove
            {
                _Selecting -= value;

                if (_Selecting == null)
                {
                    OnEventHandlerDeattached("Selecting");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Selecting listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectingListeners
        {
            get { return _Selecting != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Selecting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Selecting"/> instance containing the event data.</param>
        protected virtual void OnSelecting(TabViewCancelEventArgs args)
        {

            // Check if there are listeners.
            if (_Selecting != null)
            {
                _Selecting(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Selecting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="TabViewCancelEventArgs"/> instance containing the event data.</param>
        public void PerformSelecting(TabViewCancelEventArgs args)
        {
            // Raise the Selecting event.
            OnSelecting(args);
        }


        /// <summary>
        /// Deselecting event handler.
        /// </summary>
        private event TabViewCancelEventHandler _Deselecting;

        /// <summary>
        /// Add or remove Deselecting action.
        /// </summary>
        [RendererEventDescription]
        public event TabViewCancelEventHandler Deselecting
        {
            add
            {
                bool needNotification = (_Deselecting == null);

                _Deselecting += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Deselecting");
                }

            }
            remove
            {
                _Deselecting -= value;

                if (_Deselecting == null)
                {
                    OnEventHandlerDeattached("Deselecting");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Deselecting listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDeselectingListeners
        {
            get { return _Deselecting != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Deselecting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Deselecting"/> instance containing the event data.</param>
        protected virtual void OnDeselecting(TabViewCancelEventArgs args)
        {

            // Check if there are listeners.
            if (_Deselecting != null)
            {
                _Deselecting(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Deselecting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="TabViewCancelEventArgs"/> instance containing the event data.</param>
        public void PerformDeselecting(TabViewCancelEventArgs args)
        {

            // Raise the Deselecting event.
            OnDeselecting(args);
        }


        /// <summary>
        /// Gets or sets the window target identifier.
        /// </summary>
        /// <value>
        /// The window target identifier.
        /// </value>
        public string WindowTargetID
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void IWindowTarget.AddWindow(WindowElement windowElement)
        {
            this.AddWindow(windowElement);
        }

        /// <summary>
        /// Ensures the visible.
        /// </summary>
        /// <param name="element">The element to ensure visible.</param>
        public override void EnsureVisibleChild(VisualElement element)
        {
            // Get the tab item
            TabItem tabItem = element as TabItem;

            // If there is a valid tab item
            if (tabItem != null)
            {
                // Select tab item
                this.SelectedTab = tabItem;
            }

        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected virtual void AddWindow(WindowElement windowElement)
        {
            // If there is a valid window item
            if (windowElement != null)
            {
                // Create the tab item
                TabItem tabItem = new TabItem(windowElement.Text);

                // Set the user data
                tabItem.Tag = windowElement;

                // Fill the window
                windowElement.Dock = Dock.Fill;

                // Add window to tab
                tabItem.Controls.Add(windowElement);

                // Add tab items
                this.TabItems.Add(tabItem);

                // Ensure this control is visible
                tabItem.EnsureVisible(true);
            }
        }

        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void IWindowTarget.RemoveWindow(WindowElement windowElement)
        {
            this.RemoveWindow(windowElement);
        }

        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected virtual void RemoveWindow(WindowElement windowElement)
        {
            // If there is a valid window item
            if (windowElement != null)
            {
                // The tab to remove
                TabItem toRemoveTabItem = null;

                // Loop all tab items
                foreach (TabItem tabItem in this.TabItems)
                {
                    // If there is a valid tab which holds the window
                    if (tabItem != null && object.ReferenceEquals(tabItem.Tag, windowElement))
                    {
                        // Set tab to remove
                        toRemoveTabItem = tabItem;
                        break;
                    }
                }


                // If we found tab to remove
                if (toRemoveTabItem != null)
                {
                    // Remove the tab items
                    this.TabItems.Remove(toRemoveTabItem);
                }
            }
        }

       

        /// <summary>
        /// Makes the tab following the tab with the specified index the current tab.
        /// </summary>
        /// <param name="index">the current selected tab index</param>
        public void DeselectTab(int index)
        {
            int nextTabIndex = GetNextTabIndex(this.TabItems[index]);
            PerformSelectTab(this.TabItems[nextTabIndex]);
        }

        /// <summary>
        /// Makes the tab following the tab with the specified name the current tab.
        /// </summary>
        /// <param name="tabPageName">the current selected tab name</param>
        public void DeselectTab(string tabPageName)
        {
            int nextTabIndex = GetNextTabIndex(this.TabItems[tabPageName] as TabItem);
            PerformSelectTab(this.TabItems[nextTabIndex]);
        }


        /// <summary>
        /// Makes the tab following the specified TabItem the current tab.
        /// </summary>
        /// <param name="tabPage">the current selected tab item</param>
        public void DeselectTab(TabItem tabPage)
        {
            int nextTabIndex = GetNextTabIndex(tabPage);
            PerformSelectTab(this.TabItems[nextTabIndex]);
        }

        /// <summary>
        /// get tab index for DeselectTab method
        /// </summary>
        /// <param name="tabPage">tab item </param>
        /// <returns>tab item index</returns>
        private int GetNextTabIndex(TabItem tabPage)
        {
            if (tabPage == null)
            {
                throw new ArgumentNullException("tabPage");
            }

            int index = tabPage.TabIndex;
            if (index + 1 == this.TabItems.Count)
            {
                return 0;
            }
            return ++index;
        }

        /// <summary>
        /// Gets the navigated element.
        /// </summary>
        /// <value>
        /// The navigated element.
        /// </value>
        IClientHistoryElement IClinetHistoryContainerElement.NavigatedElement
        {
            get
            {
                return this.SelectedTab;
            }
        }



        /// <summary>
        /// Selected event handler.
        /// </summary>
        private event EventHandler<TabControlEventArgs> _selected;

        /// <summary>
        /// Add or remove Selected action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<TabControlEventArgs> Selected
        {
            add
            {
                bool needNotification = (_selected == null);

                _selected += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Selected");
                }

            }
            remove
            {
                _selected -= value;

                if (_selected == null)
                {
                    OnEventHandlerDeattached("Selected");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Selected listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectedListeners
        {
            get { return true; }
        }

        /// <summary>
        /// Raises the <see cref="E:Selected" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Selected"/> instance containing the event data.</param>
        protected virtual void OnSelected(TabControlEventArgs args)
        {

            // Check if there are listeners.
            if (_selected != null)
            {
                _selected(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Selected" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelected(TabControlEventArgs args)
        {
            SetSelectedIndex(args.TabPageIndex, true);
            // Raise the Selected event.
            OnSelected(args);
        }

    }
}
