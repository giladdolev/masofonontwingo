﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree
{
    public class GridComboBoxColumnEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the RowIndex
        /// </summary>
        /// <value>
        /// The RowIndex.
        /// </value>
        public int RowIndex { get; set; }

        /// <summary>
        /// Gets or sets the ColumnIndex
        /// </summary>
        /// <value>
        /// The ColumnIndex.
        /// </value>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        /// <value>
        /// The Value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the SelectedIndex
        /// </summary>
        /// <value>
        /// The SelectedIndex.
        /// </value>
        public int SelectedIndex { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridComboBoxColumnEventArgs" /> class.
        /// </summary>
        /// <param name="cellIndex">The Column index of ComboCell.</param>
        /// <param name="rowIndex">The Row index of ComboCell.</param>
        /// <param name="value">The selected string value.</param>
        /// <param name="index">The index.</param>
        public GridComboBoxColumnEventArgs(int cellIndex, int rowIndex, string  value,int index) :
            base()
        {
            ColumnIndex = cellIndex;
            RowIndex = rowIndex;
            Value = value;
            SelectedIndex = index;
        }
    }
}
