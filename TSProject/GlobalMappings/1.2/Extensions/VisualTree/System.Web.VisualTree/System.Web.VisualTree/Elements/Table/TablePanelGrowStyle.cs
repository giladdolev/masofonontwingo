﻿namespace System.Web.VisualTree.Elements
{
    public enum TablePanelGrowStyle
    {
        FixedSize,
        AddRows,
        AddColumns
    }
}
