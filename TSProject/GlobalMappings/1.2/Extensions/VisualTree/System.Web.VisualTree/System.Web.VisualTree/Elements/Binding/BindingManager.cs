﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BindingManager : VisualElement
    {
        /// <summary>
        /// AllowFilter allow store filtered
        /// </summary>
        private bool _allowFilter = false;

        /// <summary>
        /// Mark manager for deferred loading
        /// </summary>
        private bool _isDeferred = true;

        /// <summary>
        /// Occurs when item updated.
        /// </summary>
        public event EventHandler ItemUpdated;

        /// <summary>
        /// Occurs when item removed.
        /// </summary>
        public event EventHandler ItemDeleted;

        /// <summary>
        /// Occurs when item created.
        /// </summary>
        public event EventHandler ItemCreated;

        /// <summary>
        /// Occurs when current item has changed.
        /// </summary>
        public event EventHandler CurrentItemChanged;

        /// <summary>
        /// The action type name
        /// </summary>
        private OperationType _actionType = OperationType.Create ;

        /// <summary>
        /// The item operation type
        /// </summary>
        internal enum OperationType
        {
            /// <summary>
            /// The create
            /// </summary>
            Create,

            /// <summary>
            /// The update
            /// </summary>
            Update,

            /// <summary>
            /// The delete
            /// </summary>
            Delete

        }



        /// <summary>
        /// The current item
        /// </summary>
        private object _currentItem = null;

        /// <summary>
        /// The group filed string.
        /// </summary>
        private string _groupFiled;

        /// <summary>
        /// The operation queue
        /// </summary>
        private Queue<KeyValuePair<OperationType, object>> _operationQueue = null;

        /// <summary>
        /// The suspend server side tracking
        /// </summary>
        private SafeFlag _suspendServerSideTracking = new SafeFlag();



        /// <summary>
        /// Initializes a new instance of the <see cref="BindingManager"/> class.
        /// </summary>
        public BindingManager()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingManager"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        public BindingManager(VisualElement parentElement)
        {
            this.ParentElement = parentElement;
            parentElement.InitializeBindingManager(this);
        }

        /// <summary>
        /// Gets a value indicating whether [suspend server side tracking].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [suspend server side tracking]; otherwise, <c>false</c>.
        /// </value>
        public bool SuspendServerSideTracking
        {
            get
            {
                return this._suspendServerSideTracking.Flag;
            }
        }



        /// <summary>
        /// Gets a value indicating the actionType type.
        /// </summary>
        internal OperationType ActionType
        {
            get
            {
                return this._actionType;
            }
           private set
            {
                this._actionType = value;
            }
        }

        /// <summary>
        /// Gets the suspend server side tracking block.
        /// </summary>
        /// <value>
        /// The suspend server side tracking block.
        /// </value>
        public IDisposable SuspendServerSideTrackingBlock
        {
            get
            {
                return this._suspendServerSideTracking.Start();
            }
        }

        /// <summary>
        /// Clears the changes.
        /// </summary>
        public virtual void ClearChanges()
        {

        }

        /// <summary>
        /// Attaches the columns events.
        /// </summary>
        /// <param name="columns">The columns.</param>
        protected void AttachColumnsEvents(object columns)
        {
            INotifyCollectionChanged notifyCollectionChanged = columns as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged += OnServerSideColumnsCollectionChanged;
            }
        }

        /// <summary>
        /// Detaches the columns events.
        /// </summary>
        /// <param name="columns">The columns.</param>
        protected void DettachColumnsEvents(object columns)
        {
            INotifyCollectionChanged notifyCollectionChanged = columns as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged -= OnServerSideColumnsCollectionChanged;
            }
        }

        /// <summary>
        /// Attaches the items events.
        /// </summary>
        /// <param name="items">The items.</param>
        protected void AttachItemsEvents(object items)
        {
            INotifyCollectionChanged notifyCollectionChanged = items as INotifyCollectionChanged;
            if(notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged += OnServerSideItemsCollectionChanged;
            }
        }

        /// <summary>
        /// Detaches the items events.
        /// </summary>
        /// <param name="items">The items.</param>
        protected void DettachItemsEvents(object items)
        {
            INotifyCollectionChanged notifyCollectionChanged = items as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged -= OnServerSideItemsCollectionChanged;
            }
        }


        /// <summary>
        /// Called when columns collection changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnServerSideColumnsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // If server side tracking is not suspended
            if (!this.SuspendServerSideTracking)
            {

            }
        }


        /// <summary>
        /// Called when items collection changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnServerSideItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // If server side tracking is not suspended
            if (!this.SuspendServerSideTracking)
            {

            }
        }



        /// <summary>
        /// Register the used column.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <returns></returns>
        internal abstract IBindingDataColumn RegisterUsedColumn(string columnName);

        /// <summary>
        /// Resets the has index columns.
        /// </summary>
        internal abstract void ResetHasIndexColumns();

        /// <summary>
        /// Gets a value indicating whether this instance has index columns.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has index columns; otherwise, <c>false</c>.
        /// </value>
        public abstract bool HasIndexColumns
        {
            get;
        }


        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        [RendererPropertyDescription]
        public abstract IEnumerable<IBindingDataColumn> Columns
        {
            get;
        }

        /// <summary>
        /// The empty items
        /// </summary>
        protected static IEnumerable EmptyItems = new object[] { };

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        [RendererPropertyDescription]
        public abstract IEnumerable Items
        {
            get;
        }

        /// <summary>
        /// Gets the current item.
        /// </summary>
        /// <value>
        /// The current item.
        /// </value>
        public object CurrentItem
        {
            get
            {
                return _currentItem;
            }
        }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        /// <value>
        /// The item type.
        /// </value>
        public abstract Type ItemType
        {
            get;
        }

        /// <summary>
        /// Sets the parent data source.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        internal void SetParentDataSource(BindingManager dataSource)
        {
        }

        /// <summary>
        /// Binds the data binding to the source.
        /// </summary>
        /// <param name="dataBinding">The data binding.</param>
        internal void Bind(Binding dataBinding)
        {
            // If there is a valid data binding
            if (dataBinding != null)
            {
                // Register used column
                this.RegisterUsedColumn(dataBinding.Source);
            }
        }

        /// <summary>
        /// Gets the binding manager.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        public static BindingManager GetBindingManager(VisualElement visualElement)
        {
            // If visual element is a binding view mode
            if (visualElement is IBindingViewModel)
            {
                // Get or create view model
                return VisualElementExtender.GetOrCreateExtender<BindingManager>(visualElement, () => new BindingViewModelManager(visualElement));
            }
            else
            {
                // Get binding manager
                return VisualElementExtender.GetExtender<BindingManager>(visualElement);
            }
        }

        /// <summary>
        /// Creates the binding manager.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        internal static BindingManager CreateBindingManager(VisualElement visualElement)
        {
            // The binding manager
            BindingManager bindingManager = null;

            // If there is a valid visual element
            if (visualElement != null)
            {
                // If there is a binding view model
                if (visualElement is IBindingViewModel)
                {
                    // Gets the default binding manager
                    return GetBindingManager(visualElement);
                }
                else
                {
                    // Remove previous data source element
                    VisualElementExtender.RemoveExtender<BindingManager>(visualElement);

                    // Create the binding manager
                    bindingManager = new BindingModelManager(visualElement);

                    // Add the data binding manager as an extender
                    VisualElementExtender.AddExtender(visualElement, bindingManager);
                }
            }

            // Return the binding manager
            return bindingManager;
        }


        /// <summary>
        /// Sets the binding manager.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="bindingManager">The binding manager.</param>
        internal static void SetBindingManager(VisualElement visualElement, BindingManager bindingManager)
        {
            // If there is a valid visual element and it is not a view model element
            if (visualElement != null && !(visualElement is IBindingViewModel))
            {
                // Remove extender
                VisualElementExtender.RemoveExtender<BindingManager>(visualElement);

                // If there is a valid manager
                if (bindingManager != null)
                {
                    // Set the parent element
                    bindingManager.ParentElement = visualElement;

                    // Add new manager
                    VisualElementExtender.AddExtender(visualElement, bindingManager);
                }

            }
        }

         /// <summary>
        /// Gets a value indicating whether this instance is deferred.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is deferred; otherwise, <c>false</c>.
        /// </value>
       public bool IsDeferred
        {
            get { return _isDeferred; }
            set { _isDeferred = value; }
        }
        
 
        /// <summary>
        /// Gets a value indicating whether this instance is editable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is editable; otherwise, <c>false</c>.
        /// </value>
        public bool IsEditable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is automatic synchronize.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is automatic synchronize; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool IsAutoSynchronize
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// Get,set a GroupField value .
        /// </summary>
        [RendererPropertyDescription]
        public string GroupField
        {
            get { return _groupFiled; }
            set { _groupFiled = value; }
        }

        /// <summary>
        /// Updates the client on data changes
        /// </summary>
        /// <returns></returns>
        [RendererMethodDescription]
        public void UpdateClient()
        {
            this.InvokeMethodOnce("UpdateClient");
        }

        /// <summary>
        /// Gets the type of the item identifier.
        /// </summary>
        /// <value>
        /// The type of the item identifier.
        /// </value>
        public Type ItemIdType
        {
            get
            {
                return typeof(string);
            }
        }

        /// <summary>
        /// Gets the item by item identifier.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns></returns>
        public abstract object GetItemByItemId(object itemId);


        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Update(object item)
        {
            // En-queue operation
            this.EnqueueOperation(OperationType.Update, item);
        }

        /// <summary>
        /// En-queues the operation.
        /// </summary>
        /// <param name="operationType">The operation type.</param>
        /// <param name="item">The item.</param>
        private void EnqueueOperation(OperationType operationType, object item)
        {
            // If there is a valid item
            if (item != null)
            {
                // If we need to create queue
                if (_operationQueue == null)
                {
                    // Create operation queue
                    _operationQueue = new Queue<KeyValuePair<OperationType, object>>();
                }

                // Add key value pair
                _operationQueue.Enqueue(new KeyValuePair<OperationType, object>(operationType, item));
            }
        }

        /// <summary>
        /// Sets the current.
        /// </summary>
        /// <param name="item">The item.</param>
        public void SetCurrent(object item)
        {
            _currentItem = item;

            if (this.CurrentItemChanged != null)
            {
                this.CurrentItemChanged(item, EventArgs.Empty);
            }

            // Get parent
            VisualElement _parent = this.ParentElement;

            // If there is a valid parent
            if (_parent != null)
            {
                // Raise event
                _parent.OnDataSourceCurrentItemChanged(item);
            }
        }

        /// <summary>
        /// Commit changes.
        /// </summary>
        public void Commit()
        {
            // If there is a valid operation queue
            if (_operationQueue != null)
            {
                // Get the operation queue
                Queue<KeyValuePair<OperationType, object>> operationQueue = _operationQueue;

                // Clear current queue
                _operationQueue = null;

                // If there are items to queue
                while (operationQueue.Count > 0)
                {
                    // Get operation entry
                    KeyValuePair<OperationType, object> operationEntry = operationQueue.Dequeue();
                    switch (operationEntry.Key)
                    {
                        case OperationType.Update:
                            this.ActionType = OperationType.Update;
                            OnItemUpdated(operationEntry.Value);
                            break;
                        case OperationType.Delete:
                            this.ActionType = OperationType.Delete;
                            OnItemDeleted(operationEntry.Value);
                            break;
                        case OperationType.Create:
                            this.ActionType = OperationType.Create;
                            OnItemCreated(operationEntry.Value);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Called when item updated.
        /// </summary>
        /// <param name="item">The item.</param>
        private void OnItemUpdated(object item)
        {
            if (this.ItemUpdated != null)
            {
                this.ItemUpdated(item, EventArgs.Empty);
            }

            // Get parent
            VisualElement _parent = this.ParentElement;

            // If there is a valid parent
            if (_parent != null)
            {
                // Raise event
                _parent.OnDataSourceItemUpdated(item);
            }
        }

        /// <summary>
        /// Called when item deleted.
        /// </summary>
        /// <param name="item">The item.</param>
        private void OnItemDeleted(object item)
        {
            if (this.ItemDeleted != null)
            {
                this.ItemDeleted(item, EventArgs.Empty);
            }

            // Get parent
            VisualElement _parent = this.ParentElement;

            // If there is a valid parent
            if (_parent != null)
            {
                // Raise event
                _parent.OnDataSourceItemDeleted(item);
            }
        }

        /// <summary>
        /// Called when item created.
        /// </summary>
        /// <param name="item">The item.</param>
        private void OnItemCreated(object item)
        {
            if (this.ItemCreated != null)
            {
                this.ItemCreated(item, EventArgs.Empty);
            }

            // Get parent
            VisualElement _parent = this.ParentElement;

            // If there is a valid parent
            if (_parent != null)
            {
                // Raise event
                _parent.OnDataSourceItemCreated(item);
            }
        }


        /// <summary>
        /// Gets the property.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <returns></returns>
        protected PropertyInfo GetProperty(string columnName)
        {
            // Get the item type
            Type itemType = this.ItemType;

            // If there is a valid item type
            if (itemType != null)
            {
                // Get property from item type
                return itemType.GetProperty(columnName, BindingDataReader.BindingFlags);
            }

            return null;
        }


        /// <summary>
        /// Creates the reader.
        /// </summary>
        /// <returns></returns>
        public virtual IDataReader CreateReader(IEnumerable items = null)
        {
            // If we have external items list
            if (items != null)
            {
                // Create reader
                return BindingDataReader.Create(items, this.Columns.ToArray());
            }
            else
            {
                // Create reader
                return BindingDataReader.Create(this.Items, this.Columns.ToArray());
            }
        }

        /// <summary>
        /// Get,set AllowFilter to allow store filtered
        /// </summary>
        [RendererPropertyDescription]
        public bool AllowFilter
        {
            get { return _allowFilter; }
            set
            {
                if (_allowFilter != value)
                {
                    _allowFilter = value;
                    OnPropertyChanged("AllowFilter");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is view model.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is view model; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsViewModel
        {
            get
            {
                return false;   
            }
        }
    }

    /// <summary>
    /// Filter info class
    /// </summary>
    public class FilterInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterInfo"/> class.
        /// </summary>
        public FilterInfo()
        {

        }

        /// <summary>
        /// Set the key and the value .
        /// </summary>
        /// <param name="filterInfoKey">The filter information key.</param>
        /// <param name="filterInfoVAlue">The filter information v alue.</param>
        public FilterInfo(string filterInfoKey, string filterInfoVAlue)
        {
            this.FilterInfoKey = filterInfoKey;
            this.FilterInfoValue = filterInfoVAlue;
        }
        /// <summary>
        /// Get,set Filter key .
        /// </summary>
        public string FilterInfoKey
        {
            get;
            set;
        }

        /// <summary>
        /// Get,set Filter value .
        /// </summary>
        public string FilterInfoValue
        {
            get;
            set;
        }
    }

}
