﻿namespace System.Web.VisualTree.Elements
{
    public enum GridTristate
    {
        /// <summary>The property is not set and will behave in a default manner.</summary>
		NotSet,
		/// <summary>The property's state is true.</summary>
		True,
		/// <summary>The property's state is false.</summary>
		False
    }
}
