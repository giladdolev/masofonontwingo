﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum ToolBarStyleConstants
    {

        //Standard
        tbrStandard = 0,//&H0

        //Transparent
        tbrFlat = 1//&H1
    }
}
