namespace System.Web.VisualTree.Elements
{
	public class GridCellFormattingEventArgs : EventArgs
	{
        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
		public int ColumnIndex
		{
			get { return default(int); }
		}




        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
		public int RowIndex
		{
			get { return default(int); }
		}


		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public object Value
		{
			get { return null; }
			set
			{

			}
		}
	}
}
