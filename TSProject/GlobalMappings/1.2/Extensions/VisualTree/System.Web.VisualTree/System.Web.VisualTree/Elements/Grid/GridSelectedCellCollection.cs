﻿namespace System.Web.VisualTree.Elements
{
    public class GridSelectedCellCollection : GridCellCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridSelectedCellCollection"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        public GridSelectedCellCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {

        }

    }
}
