﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class GridComboBoxColumn : GridColumn
    {
        private string _displayMember = "text";
        private string _valueMember = "value";
        public bool _forceSelection = false;
        private Dictionary<int, int> _itemData = new Dictionary<int, int>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GridComboBoxColumn"/> class.
        /// </summary>
        public GridComboBoxColumn()
            : base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridComboBoxColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(GridComboBoxColumn))
        {
            _itemData = new Dictionary<int, int>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridComboBoxColumn"/> class.
        /// </summary>
        /// <param name="columnName">columnName</param>
        /// <param name="headerText">headerText</param>
        /// <param name="dataType">dataType</param>
        /// <returns></returns>
        public GridComboBoxColumn(string columnName, string headerText, System.Type dataType)
            : base(columnName, headerText, dataType)
        {
            _itemData = new Dictionary<int, int>();
        }




        [RendererPropertyDescription]
        /// <summary>
        /// Gets or sets the property to display for this elemtnt.
        /// </summary>
        public string DisplayMember
        {
            get
            {
                return _displayMember;
            }
            set
            {
                _displayMember = value;
                OnPropertyChanged("DisplayMember");

                this.Bind();
            }
        }

        /// <summary>
        /// Binds to data
        /// </summary>
        private void Bind()
        {
            // Re-bind the data source
            this.DataSource = this.DataSource;
        }

        [RendererPropertyDescription]
        public string ValueMember
        {
            get
            {
                return _valueMember;
            }
            set
            {
                _valueMember = value;
                OnPropertyChanged("ValueMember");

                this.Bind();
            }
        }

        /// <summary>
        /// Gets or sets the automatic complete mode.
        /// </summary>
        /// <value>
        /// The automatic complete mode.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(AutoCompleteMode.None)]
        public AutoCompleteMode AutoCompleteMode
        {
            get;
            set;
        }

        [DefaultValue(false)]
        /// <summary>
        /// Force selection 
        /// </summary>
        /// <value>
        /// true to restrict the selected value to one of the values in the list, false to allow the user to set arbitrary text into the 
        /// field.
        /// </value>
        public bool ForceSelection
        {
            get
            {
                return _forceSelection;
            }
            set
            {
                _forceSelection = value;
            }
        }


        /// <summary>
        /// Gets the item data for a given item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The item data .</returns>
        public int GetItemData(int index)
        {

            if (_itemData.ContainsKey(index))
            {
                return _itemData[index];
            }

            return -1;
        }

        /// <summary>
        /// Sets the item data for a given item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="value">The value.</param>
        public void SetItemData(int index, int value)
        {
            if (!_itemData.ContainsKey(index))
            {
                _itemData.Add(index, value);
            }
            else
            {
                _itemData[index] = value;
            }
        }

        private bool _expandOnFocus = false;
        /// <summary>
        /// Gets or Sets a value indicating whether this component will expand on focus.
        /// </summary>
        /// <value>
        /// <c>true</c> if this component will exapnd on focus; otherwise, <c>false</c>.
        /// </value>
        public bool ExpandOnFocus
        {
            get
            {
                return _expandOnFocus;
            }
            set
            {
                if (_expandOnFocus != value)
                {
                    _expandOnFocus = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>
        /// The type of the data.
        /// </value>
        public override System.Type DataType
        {
            get
            {
                return typeof(string);
            }
        }

    }
}
