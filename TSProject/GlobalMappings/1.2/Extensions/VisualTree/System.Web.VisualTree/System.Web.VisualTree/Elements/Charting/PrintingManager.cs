using System.Drawing;
using System.Drawing.Printing;

namespace System.Web.VisualTree.Elements
{
	public class PrintingManager : VisualElement
	{
		/// <summary>
		///Prints the chart.
		/// </summary>
		/// <param name="showPrintDialog">Indicates whether a print dialog should be shown.</param>
        public static void Print(bool showPrintDialog)
		{
		}
		/// <summary>
		///Draws the chart on the printer graphics.
		/// </summary>
		/// <param name="graphics">The printer T:System.Drawing.Graphics object.</param>
		/// <param name="position">The position to draw on the graphics.</param>
        public static void PrintPaint(Graphics graphics, Rectangle position)
		{
		}
		/// <summary>
		///Provides a preview of the chart.
		/// </summary>
        public static void PrintPreview()
		{
		}
		/// <summary>
		///Gets or sets a print document for the chart.
		/// </summary>
		public PrintDocument PrintDocument
		{
			get;
			set;
		}
	}
}
