using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class Chart : ControlElement
    {
        private Axis _axisX = null;

        private Axis _axisY = null;

        [DesignerIgnore]
        public ControlElement RootElement { get; set; }

        /// <summary>
        /// The chart areas
        /// </summary>
        private readonly ChartAreaCollection _chartAreas = null;

        /// <summary>
        /// The annotations
        /// </summary>
        private readonly AnnotationCollection _annotations = null;

        /// <summary>
        /// The legends
        /// </summary>
        private readonly Legend _legend = null;

        /// <summary>
        /// The titles
        /// </summary>
        private readonly TitleCollection _titles = null;

        /// <summary>
        /// The series
        /// </summary>
        private readonly SeriesCollection _series = null;

        /// <summary>
        /// The printing manager
        /// </summary>
        private readonly PrintingManager _printingManager = null;

        /// <summary>
        /// The chart serializer
        /// </summary>
        private readonly ChartSerializer _chartSerializer = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Chart"/> class.
        /// </summary>
        public Chart()
        {
            _chartAreas = new ChartAreaCollection(this, "ChartAreas");
            _annotations = new AnnotationCollection(this, "Annotations");
            _titles = new TitleCollection(this, "Titles");
            _series = new SeriesCollection(this, "Series");
            _legend = new Legend() { ParentElement = this };
            _printingManager = new PrintingManager() { ParentElement = this };
            _chartSerializer = new ChartSerializer() { ParentElement = this };
        }

        /// <summary>
        /// Redraws the graph.
        /// </summary>
        public void RedrawGraph()
        {
            // If is valid 
            if (IsLoadedToClient)
            {
                ApplicationElement.RegisterChange(this, "$refresh");
            }
        }

        [DesignerIgnore]
        public System.Web.VisualTree.Elements.ChartArea GetArea<ChartArea>()
        {
            return _chartAreas.FirstOrDefault();
        }

        /// <summary>
        ///Stores the chart annotations.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ChartAnnotation>))]
        public AnnotationCollection Annotations
        {
            get { return _annotations; }
        }


        /// <summary>
        ///Gets a read-only T:System.Windows.Forms.DataVisualization.Charting.ChartAreaCollection object that is used to store T:System.Windows.Forms.DataVisualization.Charting.ChartArea objects.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ChartArea>))]
        public ChartAreaCollection ChartAreas
        {
            get { return _chartAreas; }
        }

        /// <summary>
        ///Stores all T:System.Windows.Forms.DataVisualization.Charting.Legend objects used by the T:System.Windows.Forms.DataVisualization.Charting.Chart control.
        /// </summary>
        public Legend Legend
        {
            get { return _legend; }
        }


        /// <summary>
        ///Gets a read-only T:System.Windows.Forms.DataVisualization.Charting.PrintingManager object used for printing a chart.
        /// </summary>
        public PrintingManager Printing
        {
            get { return _printingManager; }
        }

        /// <summary>
        ///Gets a T:System.Windows.Forms.DataVisualization.Charting.ChartSerializer object that is used for chart serialization.
        /// </summary>
        public ChartSerializer Serializer
        {
            get { return _chartSerializer; }
        }

        /// <summary>
        ///Gets a T:System.Windows.Forms.DataVisualization.Charting.SeriesCollection object.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<Series>))]
        public SeriesCollection Series
        {
            get { return _series; }
        }

        /// <summary>
        ///Stores all T:System.Windows.Forms.DataVisualization.Charting.Title objects used by the T:System.Windows.Forms.DataVisualization.Charting.Chart control.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ChartTitle>))]
        public TitleCollection Titles
        {
            get { return _titles; }
        }

        /// <summary>
        ///Called when the axis scale view position or size is changed.
        /// </summary>
        public event EventHandler AxisViewChanged;

        /// <summary>
        /// Raises the <see cref="E:AxisViewChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AxisViewChanged"/> instance containing the event data.</param>
        protected virtual void OnAxisViewChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (AxisViewChanged != null)
            {
                AxisViewChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AxisViewChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void PerformAxisViewChanged(EventArgs args)
        {

            // Raise the AxisViewChanged event.
            this.OnAxisViewChanged(args);
        }


        /// <summary>
        ///Occurs when the cursor position is about to change.
        /// </summary>
        public event EventHandler CursorPositionChanging;

        /// <summary>
        /// Raises the <see cref="E:CursorPositionChanging" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CursorPositionChanging"/> instance containing the event data.</param>
        protected virtual void OnCursorPositionChanging(EventArgs args)
        {

            // Check if there are listeners.
            if (CursorPositionChanging != null)
            {
                CursorPositionChanging(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CursorPositionChanging" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformCursorPositionChanging(EventArgs args)
        {

            // Raise the CursorPositionChanging event.
            this.OnCursorPositionChanging(args);
        }


        public event EventHandler LabelFormatting;



        /// <summary>
        ///Gets or sets an array that represents all axes for a chart area.
        /// </summary>
        [DesignerIgnore]
        public Axis[] Axes
        {
            get
            {
                return new Axis[] { _axisX, _axisY };
            }
            set
            {
                if (value != null && value.Length == 2)
                {
                    AxisX = value[0];
                    AxisY = value[1];
                }
            }
        }

        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.Axis object that represents the primary X-axis.
        /// </summary>
        public Axis AxisX
        {
            get
            {
                return _axisX;
            }
            set
            {
                _axisX = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }

        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.Axis object that represents the primary Y-axis.
        /// </summary>
        public Axis AxisY
        {
            get
            {
                return _axisY;
            }
            set
            {
                _axisY = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }


    }
}
