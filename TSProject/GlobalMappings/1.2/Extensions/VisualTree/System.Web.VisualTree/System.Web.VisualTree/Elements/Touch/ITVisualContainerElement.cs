﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements.Touch
{
    public interface ITVisualContainerElement : ITVisualElement
    {
        TVisualLayout Layout
        {
            get;
        }
    }
}
