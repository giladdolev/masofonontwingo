﻿namespace System.Web.VisualTree.Elements
{
    public abstract class ChartElementCollection<TElement> : VisualElementCollection<TElement>
        where TElement : VisualElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartElementCollection{TElement}"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ChartElementCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {
        }
    }
}
