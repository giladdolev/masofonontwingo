namespace System.Web.VisualTree.Elements
{
	public class NumericUpDownEventArgs : EventArgs
	{
        private string _direction;
        
        /// <summary>
        /// The direction of the arrow that pressed (up\down).
        /// </summary>
        public string Direction { get { return _direction; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericUpDownEventArgs"/> class with the specified settings
        /// </summary>
        /// <param name="_direction">The direction of the arrow that pressed (up\down).</param>
        public NumericUpDownEventArgs(string _direction)
        {
            this._direction = _direction;
        }
	}
}
