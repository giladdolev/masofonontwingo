﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class DataViewField : VisualElement, IControlElement, IDisposable
    {
        /// <summary>
        /// Determines whether to wrap text
        /// </summary>
        private bool _wrapText;

        /// <summary>
        /// The height
        /// </summary>
        private Unit _height;

        /// <summary>
        /// The width
        /// </summary>
        private Unit _width;

        /// <summary>
        /// The top
        /// </summary>
        private Unit _top;

        /// <summary>
        /// The left
        /// </summary>
        private Unit _left;

        /// <summary>
        /// The enabled
        /// </summary>
        private bool _enabled = true;

        /// <summary>
        /// The font
        /// </summary>
        private Font _font = SystemFonts.DefaultFont;

        /// <summary>
        /// The fore color
        /// </summary>
        private Color _foreColor = Color.Empty;

        /// <summary>
        /// The border style
        /// </summary>
        private BorderStyle _borderStyle;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            _font.Dispose();
        }

        /// <summary>
        /// indicate if the grid is editable
        /// by default the element is not editable
        /// </summary>
        private bool _readOnly = true;

        /// <summary>
        /// Normalizes the template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns></returns>
        protected string NormalizeTemplate(string template)
        {
            if (template != null)
            {
                // Escape template
                return template.Replace("{{", "{").Replace("}}", "}");
            }
            return null;
        }


        /// <summary>
        /// Gets or sets the data member.
        /// </summary>
        /// <value>
        /// The data member.
        /// </value>
        [RendererPropertyDescription]
        public string DataMember
        {
            get
            {
                // Get binding 
                Binding binding = Binding.GetDataBinding(this, "Member");

                // If there is a valid binding
                if(binding != null)
                {
                    // Return the source
                    return binding.Source;
                }

                return null;
            }
            set
            {
                // Set the data binding
                Binding.SetDataBinding(this, value, "Member", true);
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether [read only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [read only]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool ReadOnly 
        {
            get { return _readOnly; }
            set
            {
                this._readOnly = value;
            }
        }

        /// <summary>
        /// Gets or sets the skin to apply to the control.
        /// </summary>
        /// <value>
        /// The name of the skin to apply to the control. The default is System.String.Empty.
        /// </value>
        [DesignerIgnore]
        [DefaultValue("")]
        public string SkinID { get; set; }

        /// <summary>
        /// Gets or sets the color of the back.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public virtual Font Font
        {
            get {
                if (_font == null)
                {
                    _font = new Font(FontFamily.GenericSansSerif, 10);
                }
                return _font;
            }
            set
            {
                _font = value;

                // Notify property BackColor changed
                OnPropertyChanged("Font");
            }
        }

        /// <summary>
        /// Gets or sets the color of the fore.
        /// </summary>
        /// <value>
        /// The color of the fore.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Color ForeColor
        {
            get { return _foreColor; }
            set
            {
                if(_foreColor != value)
                {
                    _foreColor = value;

                    OnPropertyChanged("ForeColor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the back color.
        /// </summary>
        /// <value>
        /// The back color.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Color BackColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                // If is a different value
                if (_enabled != value)
                {
                    // Set enabled
                    _enabled = value;

                    // Indicate property changed
                    OnPropertyChanged("Enabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IControlElement" /> is visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if visible; otherwise, <c>false</c>.
        /// </value>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index of the tab.
        /// </summary>
        /// <value>
        /// The index of the tab.
        /// </value>
        public int TabIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Unit Width
        {
            get { return _width; }
            set
            {
                // If there is a new width value
                if (_width != value)
                {
                    // Set the width
                    _width = value;

                    OnPropertyChanged("Width");
                }
            }
        }

        /// <summary>
        /// Gets or sets the width of the pixel.
        /// </summary>
        /// <value>
        /// The width of the pixel.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelWidth
        {
            get { return UnitTypeConverter.GetPixels(Width); }
            set
            {

                Width = Unit.Pixel(value);
            }
        }

        /// <summary>
        /// Gets or sets the pixel left.
        /// </summary>
        /// <value>
        /// The pixel left.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelLeft
        {
            get { return UnitTypeConverter.GetPixels(Left); }
            set
            {

                Left = Unit.Pixel(value);
            }
        }




        /// <summary>
        /// Gets or sets the pixel top.
        /// </summary>
        /// <value>
        /// The pixel top.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelTop
        {
            get { return UnitTypeConverter.GetPixels(Top); }
            set
            {

                Top = Unit.Pixel(value);
            }
        }

        /// <summary>
        /// Gets or sets the left.
        /// </summary>
        /// <value>
        /// The left.
        /// </value>
        [RendererPropertyDescription]
        public Unit Left
        {
            get { return _left; }
            set
            {
                if (_left != value)
                {
                    _left = value;

                    OnPropertyChanged("Left");
                }
            }
        }

        /// <summary>
        /// Gets or sets the top.
        /// </summary>
        /// <value>
        /// The top.
        /// </value>
        [RendererPropertyDescription]
        public Unit Top
        {
            get { return _top; }
            set
            {
                if (_top != value)
                {
                    _top = value;

                    OnPropertyChanged("Top");
                }
            }
        }

        
        private HorizontalAlignment _Alignment;
        /// <summary>
        /// Gets or sets the Alignment.
        /// </summary>
        /// <value>
        /// The Alignment.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public HorizontalAlignment Alignment
        {
            get { return _Alignment; }
            set
            {
                if (_Alignment != value)
                {
                    _Alignment = value;

                    OnPropertyChanged("Alignment");
                }
            }
        }



        /// <summary>
        /// Gets or sets the border style.
        /// </summary>
        /// <value>
        /// The border style.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public BorderStyle BorderStyle
        {
            get { return _borderStyle; }
            set
            {
                _borderStyle = value;

                // Notify property BorderStyle changed
                OnPropertyChanged("BorderStyle");
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Unit Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;

                    OnPropertyChanged("Height");
                }
            }
        }

        /// <summary>
        /// Gets or sets the height of the pixel.
        /// </summary>
        /// <value>
        /// The height of the pixel.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelHeight
        {
            get { return UnitTypeConverter.GetPixels(Height); }
            set
            {

                Height = Unit.Pixel(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to wrap text.
        /// </summary>
        /// <value>
        ///   <c>true</c> if wrap text; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool WrapText
        {
            get { return _wrapText; }
            set
            {
                if (_wrapText != value)
                {
                    _wrapText = value;

                    OnPropertyChanged("WrapText");
                }
            }
        }


        /// <summary>
        /// Gets or sets the color of the back.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Color BorderColor
        {
            get;
            set;
        }
    }
}
