namespace System.Web.VisualTree.Elements
{
	public class ViewEventArgs : EventArgs
	{

		private Axis _axis;
		private double _p1;
		private double _p2;
		private DateTimeIntervalType _dateTimeInterval;

		public ViewEventArgs(Axis axis, double p1, double p2, DateTimeIntervalType dateTimeIntervalType)
		{
			_axis = axis;
			_p1 = p1;
			_p2 = p2;
			DateTimeInterval = dateTimeIntervalType;
		}

		/// <summary>
		///Gets the new position of a view along an axis.
		/// </summary>
		public double NewPosition
		{
			get { return _p1; }
			set { _p1 = value; }
		}

		/// <summary>
		///Gets or sets the size of a view.
		/// </summary>
		public double NewSize
		{
			get { return _p2; }
			set { _p2 = value; }
		}
		/// <summary>
		///Gets the T:System.Windows.Forms.DataVisualization.Charting.ChartArea object that a view belongs to.
		/// </summary>
		public ChartArea ChartArea
		{
			get
			{
				if (_axis != null)
				{
					return _axis.ParentElement as ChartArea;
				}
				return null;
			}
		}

	    public DateTimeIntervalType DateTimeInterval
	    {
	        get { return _dateTimeInterval; }
	        set { _dateTimeInterval = value; }
	    }
	}
}
