﻿using System.ComponentModel;
namespace System.Web.VisualTree.Elements
{
    public class GridContextMenuCellEventArgs : GridCellMouseEventArgs
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GridContextMenuCellEventArgs"/> class.
        /// </summary>
        public GridContextMenuCellEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridContextMenuCellEventArgs"/> class.
        /// </summary>
        public GridContextMenuCellEventArgs(string xYposition)
        {
            // If there is a valid string value changed arguments
            if (!string.IsNullOrEmpty(xYposition))
            {
                string[] xYpositionArray = xYposition.Split(',');
                if (xYpositionArray != null && xYpositionArray.Length == 2)
                {
                    this.X = int.Parse(xYpositionArray[0]);
                    this.Y = int.Parse(xYpositionArray[1]);
                }
            }
        }
    }
}