using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.VisualTree.Common.Attributes;
using System.Linq;
using System.Globalization;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.GridBand" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingDataColumn" />
    public class GridColumn : GridBand, IBindingDataColumn
    {
        
        private string _dataMember;
        private bool _IsAutoID;
        private string _headerText;
        private bool _readOnly = false;
        private int _maxInputLength = -1;
        private ResourceReference _icon;
        private int _displayIndex = -1;
        private int _maxWidth = 500;
        private string _format = String.Empty;
        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        public GridColumn()
            : this(null, null, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        public GridColumn(string columnName)
            : this(columnName, columnName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridColumn(string columnName, string headerText)
            : this(columnName, headerText, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// <param name="dataType">The column data type.</param>
        public GridColumn(string columnName, string headerText, System.Type dataType)
        {
            if (!String.IsNullOrWhiteSpace(columnName))
            {
                ID = NormalizePropertyName(columnName, this);
                _IsAutoID = false;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(headerText))
                {
                    ID = NormalizePropertyName(headerText, this);
                    _IsAutoID = false;
                }
                else
                {
                    ID = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
                    _IsAutoID = true;
                }

            }

            HeaderText = headerText;
            DataType = dataType;


        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="cellTemplate">The cell template.</param>
        public GridColumn(string columnName, GridCellElement cellTemplate)
            : this(columnName, null, typeof(string))
        {
            this._cellTemplate = cellTemplate;
        }


        private GridCellElement _cellTemplate;
        /// <summary>
        /// Gets or sets the cell template.
        /// </summary>
        /// <value>
        /// The cell template.
        /// </value>
        public virtual GridCellElement CellTemplate
        {
            get
            {
                return this._cellTemplate;
            }
            set
            {
                this._cellTemplate = value;
            }
        }

        private bool _columnFilter = true;

        /// <summary>
        ///  Gets or Sets whether the grid has a column filter-bar or not
        /// </summary>
        public bool ColumnFilter
        {
            get { return _columnFilter; }
            set
            {
                if (value != _columnFilter)
                {
                    _columnFilter = value;
                    OnPropertyChanged("ColumnFilter");
                }
            }
        }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        public string Format
        {
            get
            {
                return _format;
            }
            set
            {
                if (_format != value)
                {
                    _format = value;
                    OnPropertyChanged("Format");
                }
            }
        }

       

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public object Content
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the header text.
        /// </summary>
        /// <value>
        /// The header text.
        /// </value>
        [RendererPropertyDescription]
        public string HeaderText
        {
            get
            {
                return this._headerText;
            }
            set
            {
                if (this._headerText != value)
                {
                    this._headerText = value;
                    OnPropertyChanged("HeaderText");
                }
            }
        }

        /// <summary>
        /// Normalize property name.
        /// </summary>
        /// <param name="name">The id name</param>
        /// <param name="grdColumn">The GRD column.</param>
        /// <returns>
        /// correct id
        /// </returns>
        private static string NormalizePropertyName(string name, GridColumn grdColumn)
        {

            string newName = new string((from c in name where char.GetUnicodeCategory(c) != UnicodeCategory.OtherLetter && (char.IsLetterOrDigit(c) || c == '_') select c).ToArray());

            if (!newName.Equals(name))
            {
                newName = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
                grdColumn.DirtyDataMemberName = name;
                grdColumn.IsDirtyDataMember = true;
            }

            return newName;
        }


        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>
        /// The type of the data.
        /// </value>
        public virtual System.Type DataType
        {
            get;
            set;
        }

        ///<summary>
        /// Get,set DirtyDataMemberName.
        ///</summary>
        [DesignerIgnore]
        [DefaultValue("")]
        public string DirtyDataMemberName
        {
            get;
            set;
        }

        ///<summary>
        /// Get,set IsDirtyDataMember.
        ///</summary>
        [DesignerIgnore]
        [DefaultValue(false)]
        public bool IsDirtyDataMember
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the data property.
        /// </summary>
        /// <value>
        /// The name of the data property.
        /// </value>
        public string DataMember
        {
            get
            {
                return this._dataMember;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    this._dataMember = NormalizePropertyName(value, this);
                }

            }
        }




        /// <summary>
        /// Gets or sets the sort mode.
        /// </summary>
        /// <value>
        /// The sort mode.
        /// </value>
        GridColumnSortMode _sortMode = GridColumnSortMode.Automatic;

        /// <summary>
        /// Gets or sets the sort mode.
        /// </summary>
        /// <value>
        /// The sort mode.
        /// </value>
        [DefaultValue(GridColumnSortMode.Automatic)]
        [RendererPropertyDescription]
        public GridColumnSortMode SortMode
        {
            get { return this._sortMode; }
            set
            {
                this._sortMode = value;
                OnPropertyChanged("SortMode");
            }
        }


        /// <summary>
        /// Gets or sets the minimum width.
        /// </summary>
        /// <value>
        /// The minimum width.
        /// </value>
        public int MinimumWidth
        {
            get;
            set;
        }




        /// <summary>
        /// Gets or sets the fill weight.
        /// </summary>
        /// <value>
        /// The fill weight.
        /// </value>
        public float FillWeight
        {
            get;
            set;
        }




        /// <summary>
        /// Gets or sets the automatic size mode.
        /// </summary>
        /// <value>
        /// The automatic size mode.
        /// </value>
        public GridAutoSizeColumnMode AutoSizeMode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string IBindingDataColumn.Name
        {
            get
            {
                return this.DataMember;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type IBindingDataColumn.Type
        {
            get
            {
                return this.DataType;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsValid
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        object IBindingDataColumn.GetValue(object item, int index)
        {
            GridRow row = item as GridRow;
            if (row != null && index >= 0)
            {
                GridCellCollection cells = row.Cells;
                if (cells != null && index < cells.Count)
                {
                    return cells[index].Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsIndex
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Binds the specified property information.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        void IBindingDataColumn.Bind(PropertyInfo propertyInfo)
        {

        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        [DesignerIgnore]
        public virtual string DataTypeName
        {
            get
            {
                return this.DataType.Name;
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public GridCellElement Clone()
        {
            return (GridCellElement)this.MemberwiseClone();
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get
            {
                GridElement grd = this.Parent as GridElement;
                if (grd != null)
                {
                    return grd.Columns.IndexOf(this);
                }
                return -1;
            }
        }


        /// <summary>
        /// Gets or sets the readOnly property.
        /// </summary>
        /// <value>
        /// false/ true.
        /// </value>

        [DefaultValue(false)]
        [RendererPropertyDescription]
        public override bool ReadOnly
        {
            get { return this._readOnly; }
            set
            {
                this._readOnly = value;
                OnPropertyChanged("ReadOnly");
            }
        }

        /// <summary>
        /// Gets or set the max input length 
        /// </summary>
        [DefaultValue(-1)]
        [RendererPropertyDescription]
        public int MaxInputLength
        {
            get { return this._maxInputLength; }
            set
            {
                this._maxInputLength = value;
                OnPropertyChanged("MaxInputLength");
            }
        }





        private bool _MenuDisabled = true;

        /// <summary>
        /// MenuDisabled = true Description.
        /// </summary>
        [DefaultValue(true)]
        [RendererPropertyDescription]
        public bool MenuDisabled
        {
            get
            {
                return _MenuDisabled;
            }
            set
            {
                if (_MenuDisabled != value)
                {
                    _MenuDisabled = value;
                }
            }
        }


        /// <summary>
        /// Raises the <see cref="E:Resize" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ResizeEventArgs"/> instance containing the event data.</param>
        protected override void OnResize(ResizeEventArgs args)
        {
            if (args != null)
            {
                this.Width = args.Width;

                // Get the parent element .
                GridElement grd = this.Parent as GridElement;
                if (grd != null)
                {
                    ResizeEventArgs _newArgs = new ResizeEventArgs(int.Parse(Convert.ToString(grd.Width.Value)), int.Parse(Convert.ToString(grd.Height.Value)));
                    // Raise parent resize event
                    grd.PerformResize(_newArgs);
                }
            }

            base.OnResize(args);
        }


        /// <summary>
        /// Gets or sets the Display index property.
        /// </summary>
        /// <value>
        /// int , grid column display index , index column in client side .
        /// </value>
        [RendererPropertyDescription]
        public int DisplayIndex
        {
            get { return this._displayIndex; }
            set
            {
                this._displayIndex = value;
            }
        }

        /// <summary>
        /// Get,Set sort order of column . 
        /// </summary>
        public SortOrder SortOrder { get; set; }



        /// <summary>
        /// Get, set icon header image .
        /// </summary>
        [RendererPropertyDescription]
        public ResourceReference Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                if (_icon != value)
                {
                    this._icon = value;
                    OnPropertyChanged("Icon");
                }

            }
        }

        /// <summary>
        /// Gets or sets the column's maximum width.
        /// </summary>
        public int MaxWidth
        {
            get
            {
                return _maxWidth;
            }
            set
            {
                if (_maxWidth != value)
                {
                    _maxWidth = value;
                    OnPropertyChanged("MaxWidth");
                }
            }
        }

        /// <summary>
        /// Format cell text .
        /// </summary>
        /// <returns></returns>
        public virtual string FormatValue(object value)
        {
            return Convert.ToString(value);
        }


        private ColumnSortMode _sortType = ColumnSortMode.StringAscending;

        /// <summary>
        /// Gets the name of the Sort type.
        /// </summary>
        /// <value>
        /// The name of the Sort type.
        /// </value>
        public ColumnSortMode SortType
        {
            get
            {
                return _sortType;
            }
            set
            {
                this._sortType = value;
            }
        }

        [DefaultValue(false)]
        public bool NumberField
        {
            get;
            set;
        }
    }
}
