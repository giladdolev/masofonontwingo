﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class KeyItem : VisualElement
    {
        private string _key;
        private int _imageItem;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyItem"/> class.
        /// </summary>
        public KeyItem()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyItem"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="imageIndex">Index of the image.</param>
        public KeyItem(string key, int imageIndex)
        {
            _key = key;
            _imageItem = imageIndex;
        }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public string Key
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
            }
        }

        /// <summary>
        /// Gets or sets the index of the image.
        /// </summary>
        /// <value>
        /// The index of the image.
        /// </value>
        public int ImageIndex
        {
            get
            {
                return _imageItem;
            }

            set
            {
                _imageItem = value;
            }
        }
    }
}
