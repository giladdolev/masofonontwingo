﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Enumeration for button style
    /// </summary>
    public enum ButtonStyleConstans
    {
        /// <summary>
        /// Default
        /// </summary>
        Default,
        /// <summary>
        /// Check
        /// </summary>
        Check,
        /// <summary>
        /// Button group
        /// </summary>
        ButtonGroup,
        /// <summary>
        /// Separator
        /// </summary>
        Separator,
        /// <summary>
        /// Placeholder
        /// </summary>
        Placeholder,
        /// <summary>
        /// DropDown
        /// </summary>
        DropDown
    }
}
