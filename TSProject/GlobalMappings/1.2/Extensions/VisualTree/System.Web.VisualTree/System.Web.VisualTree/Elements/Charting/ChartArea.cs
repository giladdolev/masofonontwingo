using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ChartNamedElement" />
    [DesignerComponent]
    public class ChartArea : ChartNamedElement
    {
        private ChartCursor _cursorX = null;
        private ChartCursor _cursorY = null;
        private Axis _axisX = null;
        private Axis _axisY = null;
        private ElementPosition _innerPlotPosition = null;
        private ElementPosition _position = null;
        private ChartingGrid _grid = null;

        public ChartingGrid Grid
        {
            get
            {
                return _grid;
            }
        }



        public ChartArea()
        {
            CursorX = new ChartCursor();
            CursorY = new ChartCursor();
            AxisX = new Axis();
            AxisY = new Axis();
            InnerPlotPosition = new ElementPosition();
            Position = new ElementPosition();
            _visible = true;
        }

        public ChartArea(object p)
            : this()
        {
            string id = p as string;

            if (!string.IsNullOrEmpty(id))
            {
                ID = id;
            }
        }


        /// <value>
        /// The orientation.
        /// </value>
        private Orientation _orientation = Orientation.Vertical;
        /// <summary>
        /// Gets or sets the orientation.
        /// </summary>
        /// <value>
        /// The orientation.
        /// </value>
        [DefaultValue(Orientation.Vertical)]
        public Orientation Orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                if (_orientation != value)
                {
                    // Set the splitter orientation
                    _orientation = value;

                    OnPropertyChanged("Orientation");
                }
            }
        }

        private bool _visible;
        /// <summary>
        ///Gets or sets a flag that determines if a chart area is visible.
        /// </summary>
        public virtual bool Visible
        {
            get { return _visible; }
            set
            {
                _visible = value;

                // Notify property BackColor changed
                OnPropertyChanged("Visible");
            }
        }

        private bool _showGrid;
        /// <summary>
        ///Gets or sets a flag that determines if a chart area is visible.
        /// </summary>
        public virtual bool ShowGrid
        {
            get { return _showGrid; }
            set
            {
                _showGrid = value;

                // Notify property BackColor changed
                OnPropertyChanged("ShowGrid");
            }
        }

        /// <summary>
        ///Gets or sets the name of the T:System.Windows.Forms.DataVisualization.Charting.ChartArea object to which this chart area should be aligned.
        /// </summary>
        public string AlignWithChartArea
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets a T:System.Windows.Forms.DataVisualization.Charting.Cursor object that is used for cursors and selected ranges along the Y-axis.
        /// </summary>
        public ChartCursor CursorY
        {
            get
            {
                return _cursorY;
            }
            set
            {
                _cursorY = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }
        /// <summary>
        ///Gets or sets a T:System.Windows.Forms.DataVisualization.Charting.Cursor object that is used for cursors and selected ranges along the X-axis.
        /// </summary>
        public ChartCursor CursorX
        {
            get
            {
                return _cursorX;
            }
            set
            {
                _cursorX = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }


        /// <summary>
        ///Gets or sets the alignment orientation of a chart area.
        /// </summary>
        public AreaAlignmentOrientations AlignmentOrientation
        {
            get;
            set;
        }


        public System.Web.VisualTree.Elements.ChartingGrid GetGrid<ChartingGrid>()
        {
            return _grid;
        }

        /// <summary>
        ///Gets or sets the alignment style of the T:System.Windows.Forms.DataVisualization.Charting.ChartArea.
        /// </summary>
        public AreaAlignmentStyles AlignmentStyle
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the border width of a T:System.Windows.Forms.DataVisualization.Charting.ChartArea object.
        /// </summary>
        public int BorderWidth
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the border style of a T:System.Windows.Forms.DataVisualization.Charting.ChartArea object.
        /// </summary>
        public ChartDashStyle BorderDashStyle
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets an array that represents all axes for a chart area.
        /// </summary>
        public Axis[] Axes
        {
            get
            {
                return new Axis[] { _axisX, _axisY };
            }
            set
            {
                if (value != null && value.Length == 2)
                {
                    AxisX = value[0];
                    AxisY = value[1];
                }
            }
        }

        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.Axis object that represents the primary X-axis.
        /// </summary>
        public Axis AxisX
        {
            get
            {
                return _axisX;
            }
            set
            {
                _axisX = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }

        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.Axis object that represents the primary Y-axis.
        /// </summary>
        public Axis AxisY
        {
            get
            {
                return _axisY;
            }
            set
            {
                _axisY = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }

        /// <summary>
        ///Gets or sets the drawing mode for the background image of a T:System.Windows.Forms.DataVisualization.Charting.ChartArea object.
        /// </summary>
        public ChartImageWrapMode BackImageWrapMode
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.ElementPosition object, which defines the inner plot position of a chart area object.
        /// </summary>
        public ElementPosition InnerPlotPosition
        {
            get
            {
                return _innerPlotPosition;
            }
            set
            {
                _innerPlotPosition = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }
        /// <summary>
        ///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.ElementPosition object that defines the position of a T:System.Windows.Forms.DataVisualization.Charting.ChartArea object within the T:System.Windows.Forms.DataVisualization.Charting.Chart.
        /// </summary>
        public ElementPosition Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;

                if (value != null)
                {
                    value.ParentElement = this;
                }
            }
        }
    }
}
