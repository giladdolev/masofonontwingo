using System.ComponentModel;

namespace System.Web.VisualTree.Elements
{
	public class BindingSourceElement : VisualElement, ISupportInitialize
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="BindingSourceElement"/> class.
		/// </summary>
		public BindingSourceElement()
		{
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="BindingSourceElement"/> class.
		/// </summary>
		/// <param name="container">The container.</param>
		public BindingSourceElement(IContainer container)
		{

		}


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingSourceElement" /> class.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        /// <param name="dataMember">The data member.</param>
        public BindingSourceElement(object dataSource , string dataMember)
        {
            this.DataSource = dataSource;
            this.DataMember = DataMember;

        }

		public string DataMember
		{
			get;
			set;
		}
		public object DataSource
		{
			get;
			set;
		}

		/// <summary>
		/// Signals the object that initialization is starting.
		/// </summary>
		void ISupportInitialize.BeginInit()
		{
			BeginInit();
		}

		protected void BeginInit()
		{

		}

		/// <summary>
		/// Signals the object that initialization is complete.
		/// </summary>
		void ISupportInitialize.EndInit()
		{
			EndInit();
		}

		protected void EndInit()
		{

		}
		/// <summary>
		///Moves to the previous item in the list.
		/// </summary>
		public void MovePrevious()
		{
		}
		/// <summary>
		///Moves to the next item in the list.
		/// </summary>
		public void MoveNext()
		{
		}
		/// <summary>
		///Moves to the last item in the list.
		/// </summary>
		public void MoveLast()
		{
		}
		/// <summary>
		///Moves to the first item in the list.
		/// </summary>
		public void MoveFirst()
		{
		}
		/// <summary>
		///Gets or sets the index of the current item in the underlying list.
		/// </summary>
		public int Position
		{
			get;
			set;
		}
		/// <summary>
		///Gets the total number of items in the underlying list, taking the current P:System.Windows.Forms.BindingSource.Filter value into consideration.
		/// </summary>
		public virtual int Count
		{
			get { return default(int); }
		}
		/// <summary>
		///Removes the specified item from the list.
		/// </summary>
		/// <param name="value">The item to remove from the underlying list represented by the P:System.Windows.Forms.BindingSource.List property.</param>
		public virtual void Remove(object value)
		{
		}
	}
}
