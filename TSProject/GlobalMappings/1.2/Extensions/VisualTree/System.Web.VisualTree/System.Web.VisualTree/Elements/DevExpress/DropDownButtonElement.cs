﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    public class DropDownButtonElement : ToolBarDropDownItem
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DropDownButtonElement"/> class.
        /// </summary>
        public DropDownButtonElement()
        {
        }




        /// <summary>
        /// DropDownOpening event handler.
        /// </summary>
        private event EventHandler<EventArgs> _dropDownOpening;

        /// <summary>
        /// Add or remove DropDownOpening action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> DropDownOpening
        {
            add
            {
                bool needNotification = (_dropDownOpening == null);

                _dropDownOpening += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DropDownOpening");
                }

            }
            remove
            {
                _dropDownOpening -= value;

                if (_dropDownOpening == null)
                {
                    OnEventHandlerDeattached("DropDownOpening");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DropDownOpening listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDropDownOpeningListeners
        {
            get { return _dropDownOpening != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DropDownOpening" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DropDownOpening"/> instance containing the event data.</param>
        protected virtual void OnDropDownOpening(EventArgs args)
        {

            // Check if there are listeners.
            if (_dropDownOpening != null)
            {
                _dropDownOpening(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DropDownOpening" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDropDownOpening(EventArgs args)
        {

            // Raise the DropDownOpening event.
            OnDropDownOpening(args);
        }

        /// <summary>
        /// DropDownOpened event handler.
        /// </summary>
        private event EventHandler<EventArgs> _dropDownOpened;

        /// <summary>
        /// Add or remove open action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> DropDownOpened
        {
            add
            {
                bool needNotification = (_dropDownOpened == null);

                _dropDownOpened += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DropDownOpened");
                }

            }
            remove
            {
                _dropDownOpened -= value;

                if (_dropDownOpened == null)
                {
                    OnEventHandlerDeattached("DropDownOpened");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DropDownOpened listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has DropDownOpened listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDropDownOpenedListeners
        {
            get { return _dropDownOpened != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DropDownOpened" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DropDownOpened"/> instance containing the event data.</param>
        protected virtual void OnDropDownOpened(EventArgs args)
        {

            // Check if there are listeners.
            if (_dropDownOpened != null)
            {
                _dropDownOpened(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:open" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDropDownOpened(EventArgs args)
        {
            // Raise the DropDownOpened event.
            OnDropDownOpened(args);
        }



        /// <summary>
        /// DropDownClosed event handler.
        /// </summary>
        private event EventHandler<EventArgs> _dropDownClosed;

        /// <summary>
        /// Add or remove open action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> DropDownClosed
        {
            add
            {
                bool needNotification = (_dropDownClosed == null);

                _dropDownClosed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DropDownClosed");
                }

            }
            remove
            {
                _dropDownClosed -= value;

                if (_dropDownClosed == null)
                {
                    OnEventHandlerDeattached("DropDownClosed");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DropDownClosed listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has DropDownClosed listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDropDownClosedListeners
        {
            get { return _dropDownClosed != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DropDownClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DropDownClosed"/> instance containing the event data.</param>
        protected virtual void OnDropDownClosed(EventArgs args)
        {

            // Check if there are listeners.
            if (_dropDownClosed != null)
            {
                _dropDownClosed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DropDownClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDropDownClosed(EventArgs args)
        {
            // Raise the DropDownOpened event.
            OnDropDownClosed(args);
        }
    }

}
