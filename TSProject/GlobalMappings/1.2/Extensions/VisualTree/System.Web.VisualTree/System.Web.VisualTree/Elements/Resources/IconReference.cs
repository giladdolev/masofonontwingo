﻿using System.Drawing;
using System.IO;
using System.Web.Mvc;

namespace System.Web.VisualTree.Elements
{
    public class IconReference : ResourceReference
    {
        private readonly string _source;

        private readonly Icon _icon;


        /// <summary>
        /// Initializes a new instance of the <see cref="IconReference" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="image">The image.</param>
        public IconReference(string source, Icon image)
        {
            _source = source;
            _icon = image;
        }

        public IconReference()
        {

        }

        public override string Source
        {
            get
            {
                return _source;
            }
        }



        public Size IconSize
        {
            get { return _icon.Size; }
        }



        /// <summary>
        /// Performs an implicit conversion from <see cref="IconReference"/> to <see cref="Icon"/>.
        /// </summary>
        /// <param name="iconReference">The icon reference.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Icon(IconReference iconReference)
        {
            // If there is a valid image reference
            if (iconReference != null)
            {
                // Return the image reference image
                return iconReference._icon;
            }

            return null;
        }
    }
}
