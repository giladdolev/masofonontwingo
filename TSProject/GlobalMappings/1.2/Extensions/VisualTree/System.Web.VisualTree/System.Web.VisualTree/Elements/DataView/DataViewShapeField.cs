﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{
    public class DataViewShapeField : DataViewField
    {
        
        private ShapeType _Type;
        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        /// <value>
        /// The Type.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public ShapeType Type
        {
            get { return _Type; }
            set
            {
                if (_Type != value)
                {
                    _Type = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the pixel left.
        /// </summary>
        /// <value>
        /// The pixel left.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        new public int PixelLeft
        {
            get { return base.PixelLeft; }
            set
            {
                base.PixelLeft = value;
                int width = this.PixelRight - value;
                if (width > 0)
                {
                    this.PixelWidth = width;
                }
            }
        }

        /// <summary>
        /// Gets or sets the pixel right.
        /// </summary>
        /// <value>
        /// The pixel right.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelRight
        {
            get { return this.PixelLeft + this.PixelWidth; }
            set
            {
                int width = value - this.PixelLeft;
                if (width > 0)
                {
                    this.PixelWidth = width;
                }
            }
        }

        /// <summary>
        /// Gets or sets the pixel top.
        /// </summary>
        /// <value>
        /// The pixel top.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelTop
        {
            get { return base.PixelTop; }
            set
            {
                base.PixelTop = value;
                int height = this.PixelBottom - value;
                if (height > 0)
                {
                    this.PixelHeight = height;
                }
            }
        }

        /// <summary>
        /// Gets or sets the pixel bottom.
        /// </summary>
        /// <value>
        /// The pixel bottom.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelBottom
        {
            get { return this.PixelTop + this.PixelHeight; }
            set
            {
                int height = value - this.PixelTop;
                if (height > 0)
                {
                    this.PixelHeight = height;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewShapeField"/> class.
        /// </summary>
        public DataViewShapeField()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewShapeField"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public DataViewShapeField(ShapeType type)
        {
            _Type = type;
        }
    }
}
