﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;


namespace System.Web.VisualTree.Elements
{
    public class WidgetColumnData
    {
        public WidgetColumnData()
        {

        }
        public string DataMember { get; set; }

        public int SelectedRowIndex { get; set; }

        public int SelectedColumnIndex { get; set; }

        public int WidgetControlIndex { get; set; }

        public GridElement Grid { get; set; }

        public bool Enabled { get; set; }

        public Color BackColor
        {
            get;
            set;
        }

        public string Store { get; set; }
    }
}
