﻿namespace System.Web.VisualTree.Elements
{
    public class GridImageColumn : GridColumn
    {
        public GridImageColumn()
        {
        }

        public GridImageColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(byte[]))
        {
        }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        public string Image { get; set; }
    }
}
