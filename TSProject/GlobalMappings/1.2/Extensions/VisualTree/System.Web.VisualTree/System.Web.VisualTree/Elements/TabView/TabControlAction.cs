﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Defines values representing TabControl events.
    /// </summary>
    public enum TabControlAction
    {
        /// <summary>
        /// Represents the TabControl.Deselected event.
        /// </summary>
        Deselected,
        /// <summary>
        /// Represents the TabControl.Deselecting event.
        /// </summary>
        Deselecting,
        /// <summary>
        /// Represents the TabControl.Selected event.
        /// </summary>
        Selected,
        /// <summary>
        /// Represents the TabControl.Selecting event.
        /// </summary>
        Selecting
    }
}
