using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;


namespace System.Web.VisualTree.Elements
{
    public class WindowElement : ControlContainerElement, IWindowElement, IWindowTargetContainer, IComponentManagerContainer
    {
        private int _paletteMode;
        private bool _autoShowChildren;
        private bool _activated = false;
        private bool _topMost;
        private bool _showIcon = true;
        private double _opacity = 1.0;
        private bool _minimizeBox = true;
        private bool _maximizeBox = true;
        private bool _controlBox = true;
        private bool _isClosing = false;
        private ButtonElement _acceptButton;
        private Size _autoScaleBaseSize;
        private AutoSizeMode _autoSizeMode = AutoSizeMode.GrowOnly;
        private ButtonBaseElement _cancelButton;
        private Rectangle _desktopBounds;
        private WindowBorderStyle _windowBorderStyle = WindowBorderStyle.Sizable;
        private ResourceReference _icon;
        private WindowMdiContainer _mdiContainer = null;
        private ToolBarElement _toolbar;
        private WindowElement[] _MdiChildren;
        private WindowElement mobjMdiParent;
        private SizeGripStyle _sizeGripStyle = SizeGripStyle.Auto;
        private WindowStartPosition _windowStartPosition = WindowStartPosition.CenterParent;
        private Color _transparencyKey;
        private WindowState _windowState = WindowState.Normal;
        private EventHandler _oneTimeWindowClosed = null;
        private MenuElement _menu;
        ScaleModeConstants _scaleMode = ScaleModeConstants.Pixels;

        /// <summary>
        /// The empty array
        /// </summary>
        private static WindowElement[] _EmptyWindowElementArray = new WindowElement[] { };

        /// <summary>
        /// The component manager elements (also knows as ghost elements)
        /// </summary>
        private List<VisualElement> componentManagerElements = new List<VisualElement>();

        /// <summary>
        /// Provides support for tracking opened windows
        /// </summary>
        private class WindowElementOpenerContext : IDisposable
        {
            /// <summary>
            /// The parent opener context
            /// </summary>
            private WindowElementOpenerContext _parentOpenerContext;

            /// <summary>
            /// The action
            /// </summary>
            private Action<WindowElement> _action;

            /// <summary>
            /// A flag indicating if action was activated
            /// </summary>
            private bool _actionActivated = false;

            /// <summary>
            /// Initializes a new instance of the <see cref="WindowElementOpenerContext"/> class.
            /// </summary>
            /// <param name="parentOpenerContext">The parent opener context.</param>
            /// <param name="action">The action.</param>
            public WindowElementOpenerContext(WindowElementOpenerContext parentOpenerContext, Action<WindowElement> action)
            {
                _parentOpenerContext = parentOpenerContext;
                _action = action;
            }

            /// <summary>
            /// Notifies window creation.
            /// </summary>
            /// <param name="windowElement">The window element.</param>
            internal void NotifyWindowCreation(WindowElement windowElement)
            {
                // If action was not activated
                if (!_actionActivated)
                {
                    // Indicate action activated
                    _actionActivated = true;

                    // If there is a valid action
                    if (_action != null)
                    {
                        // Notify window open
                        _action(windowElement);
                    }
                }
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                _openerContext = _parentOpenerContext;
            }
        }

        /// <summary>
        /// The current opener context
        /// </summary>
        [ThreadStatic]
        private static WindowElementOpenerContext _openerContext = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowElement"/> class.
        /// </summary>
        public WindowElement()
        {
            // If we have a valid opener context 
            if (_openerContext != null)
            {
                _openerContext.NotifyWindowCreation(this);
            }
            Resizable = true;
        }

        /// <summary>
        /// Creates the opener context.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IDisposable CreateOpenerContext(Action<WindowElement> action = null)
        {
            return _openerContext = new WindowElementOpenerContext(_openerContext, action);
        }



        /// <summary>
        /// Gets the empty array.
        /// </summary>
        /// <value>
        /// The empty array.
        /// </value>
        public static new WindowElement[] EmptyArray
        {
            get
            {
                return _EmptyWindowElementArray;
            }
        }



        [DesignerIgnore]
        public bool AutoRedraw
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the window target.
        /// </summary>
        /// <value>
        /// The window target.
        /// </value>
        public string WindowTarget
        {
            get;
            set;
        }


        /// <summary>
        /// Gets a value indicating whether this instance has window target.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has window target; otherwise, <c>false</c>.
        /// </value>
        public bool HasWindowTarget
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.WindowTarget);
            }
        }


        /// <summary>
        /// Returns or sets a value that determines whether MDI child forms are displayed when loaded.
        /// </summary>
        /// <value>
        /// <c>true</c> if automatic show children forms; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool AutoShowChildren
        {
            get { return _autoShowChildren; }
            set
            {
                _autoShowChildren = value;

                // Notify property AutoShowChildren changed
                OnPropertyChanged("AutoShowChildren");
            }
        }




        /// <summary>
        /// Gets or sets the palette mode.
        /// </summary>
        /// <value>
        /// The palette mode.
        /// </value>
        public int PaletteMode
        {
            get { return _paletteMode; }
            set
            {
                _paletteMode = value;

                // Notify property AutoShowChildren changed
                OnPropertyChanged("PaletteMode");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is touch window.
        /// </summary>
        /// <value>
        ///   <c>true</c> if is touch window; otherwise, <c>false</c>.
        /// </value>
        public bool IsTouchWindow { get; set; }

        /// <summary>
        /// Gets or sets the scale mode.
        /// </summary>
        /// <value>
        /// The scale mode.
        /// </value>
        [DefaultValue(ScaleModeConstants.Pixels)]
        public ScaleModeConstants ScaleMode
        {
            get { return _scaleMode; }
            set
            {
                _scaleMode = value;

                // Notify property ScaleMode changed
                OnPropertyChanged("ScaleMode");
            }
        }




        /// <summary>
        ///Gets an array of T:System.Windows.Forms.Form objects that represent all forms that are owned by this form.
        /// </summary>
        public WindowElement[] OwnedForms { get; private set; }




        /// <summary>
        ///Gets or sets the form that owns this form.
        /// </summary>
        [DesignerIgnore]
        public WindowElement Owner
        {
            get
            {
                return ApplicationElement.GetOwnerWindow(this) as WindowElement;
            }
        }


        /// <summary>
        ///Gets the currently active multiple-document interface (MDI) child window.
        /// </summary>
        [DesignerIgnore]
        public WindowElement ActiveMdiChild
        {
            get;
            private set;
        }




        /// <summary>
        ///Gets or sets the primary menu container for the form.
        /// </summary>
        [System.Web.UI.IDReferenceProperty]
        public override MenuElement Menu
        {
            get { return _menu; }
            set
            {
                _menu = value;

                if (_menu != null)
                {
                    _menu.ParentElement = this;
                }
            }
        }




        /// <summary>
        /// Gets the parent element.
        /// </summary>
        /// <value>
        /// The parent element.
        /// </value>
        IVisualElement IRootElement.ParentElement
        {
            get { return this.ParentElement; }
            set { this.ParentElement = value as VisualElement; }
        }


        /// <summary>
        /// The client identifier
        /// </summary>
        private string _clientID = null;

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        public override string ClientID
        {
            get
            {
                // Get the application element
                ApplicationElement applicationElement = ParentElement as ApplicationElement;

                // If there is a valid application element
                if (applicationElement != null)
                {
                    // Get client id from application element
                    string clientID = applicationElement.GetClientId(this);

                    // If there is a valid client
                    if (!string.IsNullOrEmpty(clientID))
                    {
                        // Store the client ID
                        _clientID = clientID;

                        // Return the client ID
                        return clientID;
                    }
                    else
                    {
                        // If there is a valid client ID
                        if (!string.IsNullOrEmpty(_clientID))
                        {
                            return _clientID;
                        }
                    }
                }

                // Return the id as is
                return ID;
            }
        }





        /// <summary>
        /// Gets or sets the dialog result.
        /// </summary>
        /// <value>
        /// The dialog result.
        /// </value>
        public DialogResult DialogResult
        {
            get;
            set;
        }




        /// <summary>
        /// Gets or sets the accept button.
        /// </summary>
        /// <value>
        /// The accept button.
        /// </value>
        [System.Web.UI.IDReferenceProperty]
        public ButtonElement AcceptButton
        {
            get { return _acceptButton; }
            set
            {
                _acceptButton = value;

                // Notify property AcceptButton changed
                OnPropertyChanged("AcceptButton");
            }
        }

        /// <summary>
        /// Gets or sets the size of the automatic scale base.
        /// </summary>
        /// <value>
        /// The size of the automatic scale base.
        /// </value>
        public Size AutoScaleBaseSize
        {
            get { return _autoScaleBaseSize; }
            set
            {
                _autoScaleBaseSize = value;

                // Notify property AutoScaleBaseSize changed
                OnPropertyChanged("AutoScaleBaseSize");
            }
        }




        /// <summary>
        /// Gets or sets the automatic size mode.
        /// </summary>
        /// <value>
        /// The automatic size mode.
        /// </value>
        [DefaultValue(AutoSizeMode.GrowOnly)]
        public AutoSizeMode AutoSizeMode
        {
            get { return _autoSizeMode; }
            set
            {
                _autoSizeMode = value;

                // Notify property AutoSizeMode changed
                OnPropertyChanged("AutoSizeMode");
            }
        }




        /// <summary>
        /// Gets or sets the cancel button.
        /// </summary>
        /// <value>
        /// The cancel button.
        /// </value>
        [System.Web.UI.IDReferenceProperty]
        public ButtonBaseElement CancelButton
        {
            get { return _cancelButton; }
            set
            {
                _cancelButton = value;

                // Notify property CancelButton changed
                OnPropertyChanged("CancelButton");
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether control box.
        /// </summary>
        /// <value>
        ///   <c>true</c> if control box; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool ControlBox
        {
            get { return _controlBox; }
            set
            {
                _controlBox = value;
                if (value == false)
                {
                    MinimizeBox = value;
                    MaximizeBox = value;
                }

                OnPropertyChanged("ControlBox");
            }
        }




        /// <summary>
        /// Gets or sets the desktop bounds.
        /// </summary>
        /// <value>
        /// The desktop bounds.
        /// </value>
        public Rectangle DesktopBounds
        {
            get { return _desktopBounds; }
            set
            {
                _desktopBounds = value;

                // Notify property DesktopBounds changed
                OnPropertyChanged("DesktopBounds");
            }
        }





        /// <summary>
        /// Gets or sets the window border style.
        /// </summary>
        /// <value>
        /// The window border style.
        /// </value>
        [DefaultValue(WindowBorderStyle.Sizable)]
        public WindowBorderStyle WindowBorderStyle
        {
            get { return _windowBorderStyle; }
            set
            {
                _windowBorderStyle = value;

                // Notify property WindowBorderStyle changed
                OnPropertyChanged("WindowBorderStyle");
            }
        }




        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ResourceReference Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;

                // Notify property Icon changed
                OnPropertyChanged("Icon");
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether this instance is MDI container.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is MDI container; otherwise, <c>false</c>.
        /// </value>
        public bool IsMdiContainer
        {
            get { return _mdiContainer != null; }
            set
            {
                // If is an mdi container
                if (value)
                {
                    // If we need to create container
                    if (_mdiContainer == null)
                    {
                        // Create mdi container
                        _mdiContainer = new WindowMdiContainer();

                        // Add the mdi container
                        Add(_mdiContainer, false);
                    }
                }
            }
        }


        /// <summary>
        /// Gets or sets the opacity.
        /// </summary>
        /// <value>
        /// The opacity.
        /// </value>
        [DefaultValue(1.0)]
        public double Opacity
        {
            get { return _opacity; }
            set
            {
                _opacity = value;

                // Notify property Opacity changed
                OnPropertyChanged("Opacity");
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether show icon.
        /// </summary>
        /// <value>
        ///   <c>true</c> if show icon; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool ShowIcon
        {
            get { return _showIcon; }
            set
            {
                _showIcon = value;

                // Notify property ShowIcon changed
                OnPropertyChanged("ShowIcon");
            }
        }

        /// <summary>
        /// Gets or sets the size grip style.
        /// </summary>
        /// <value>
        /// The size grip style.
        /// </value>
        [DefaultValue(SizeGripStyle.Auto)]
        public SizeGripStyle SizeGripStyle
        {
            get { return _sizeGripStyle; }
            set
            {
                _sizeGripStyle = value;

                // Notify property SizeGripStyle changed
                OnPropertyChanged("SizeGripStyle");
            }
        }




        /// <summary>
        /// Gets or sets the start position.
        /// </summary>
        /// <value>
        /// The start position.
        /// </value>
        [DefaultValue(WindowStartPosition.CenterParent)]
        [RendererPropertyDescription]
        public WindowStartPosition StartPosition
        {
            get { return _windowStartPosition; }
            set
            {
                _windowStartPosition = value;

                // Notify property StartPosition changed
                OnPropertyChanged("StartPosition");
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether top most.
        /// </summary>
        /// <value>
        ///   <c>true</c> if top most; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool TopMost
        {
            get { return _topMost; }
            set
            {
                _topMost = value;

                // Notify property TopMost changed
                OnPropertyChanged("TopMost");
            }
        }




        /// <summary>
        /// Gets or sets the transparency key.
        /// </summary>
        /// <value>
        /// The transparency key.
        /// </value>
        public Color TransparencyKey
        {
            get { return _transparencyKey; }
            set
            {
                _transparencyKey = value;

                // Notify property TransparencyKey changed
                OnPropertyChanged("TransparencyKey");
            }
        }




        /// <summary>
        /// Gets or sets the state of the window.
        /// </summary>
        /// <value>
        /// The state of the window.
        /// </value>
        [DefaultValue(WindowState.Normal)]
        [RendererPropertyDescription]
        public WindowState WindowState
        {
            get { return _windowState; }
            set
            {
                _windowState = value;

                // Notify property WindowState changed
                OnPropertyChanged("WindowState");
            }
        }




        /// <summary>
        /// Gets the MDI container.
        /// </summary>
        /// <value>
        /// The MDI container.
        /// </value>
        internal WindowMdiContainer MdiContainer
        {
            get { return _mdiContainer; }
        }




        /// <summary>
        /// Gets or sets the main menu bar.
        /// </summary>
        /// <value>
        /// The main menu bar.
        /// </value>
        [System.Web.UI.IDReferenceProperty]
        public ToolBarElement ToolBar
        {
            get { return _toolbar; }
            set
            {
                _toolbar = value;

                // Notify property Toolbar changed
                OnPropertyChanged("Toolbar");
            }
        }





        /// <summary>
        /// Gets or sets a value indicating whether maximize box will be shown. 
        /// </summary>
        /// <value>
        ///   <c>true</c> if maximize box; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(true)]
        public bool MaximizeBox
        {
            get
            {
                return this.ControlBox && this._maximizeBox;
            }
            set
            {
                this._maximizeBox = value;

                // Notify property MaximizeBox changed
                OnPropertyChanged("MaximizeBox");
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether minimize box will be shown.
        /// </summary>
        /// <value>
        ///   <c>true</c> if minimize box; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(true)]
        public bool MinimizeBox
        {
            get
            {
                return this.ControlBox && this._minimizeBox;
            }
            set
            {
                this._minimizeBox = value;

                // Notify property MinimizeBox changed
                OnPropertyChanged("MinimizeBox");
            }
        }





        /// <summary>
        /// Gets or sets the MDI children.
        /// </summary>
        /// <value>
        /// The MDI children.
        /// </value>
        [DesignerIgnore]
        public WindowElement[] MdiChildren
        {
            get { return _MdiChildren; }
            set
            {
                _MdiChildren = value;

                // Notify property MdiChildren changed
                OnPropertyChanged("MdiChildren");
            }
        }




        /// <summary>
        ///Gets or sets a value indicating whether the form will receive key events before the event is passed to the control that has focus.
        /// </summary>
        public bool KeyPreview
        {
            get;
            set;
        }




        /// <summary>
        /// Gets a value indicating whether this window is windowless.
        /// </summary>
        /// <value>
        /// <c>true</c> if this window is windowless; otherwise, <c>false</c>.
        /// </value>
        public bool IsWindowless
        {
            get
            {
                // Get parent 
                VisualElement parentElement = ParentElement;

                // If there is a valid parent
                if (parentElement != null)
                {
                    // If is an mdi container window
                    if (parentElement is WindowMdiContainer)
                    {
                        return true;
                    }

                    // If is not the main window
                    return ApplicationElement.MainWindow == this;
                }
                else
                {
                    return false;
                }
            }
        }




        /// <summary>
        /// Gets or sets the MDI parent.
        /// </summary>
        /// <value>
        /// The MDI parent.
        /// </value>
        [DesignerIgnore]
        public WindowElement MdiParent
        {
            get { return mobjMdiParent; }
            set
            {
                mobjMdiParent = value;

                // Notify property MdiParent changed
                OnPropertyChanged("MdiParent");
            }
        }









        /// <summary>
        /// Closes this instance.
        /// </summary>
        public void Close()
        {
            if (!_isClosing)
            {
                _isClosing = true;

                try
                {
                    // Fire on window closing event
                    if (OnWindowClosing(CloseReason.UserClosing))
                    {
                        OnWindowClosed(new WindowClosedEventArgs(CloseReason.Code));
                    }
                }
                finally
                {
                    _isClosing = false;
                }
            }
        }




        /// <summary>
        /// Invokes the window action.
        /// </summary>
        /// <param name="windowActionType">The window action data.</param>
        [RendererMethodDescription]
        internal void InvokeWindowAction(WindowActionType windowActionType)
        {
            this.InvokeMethodOnce("InvokeWindowAction", windowActionType);
        }

        /// <summary>
        /// Activate the current window by bringing it to the front
        /// </summary>
        [RendererMethodDescription]
        public void Activate()
        {
            this.InvokeMethodOnce("Activate", null);
            //Raise activated event .
            PerformActivated(new EventArgs());
        }



        /// <summary>
        /// Raises the <see cref="E:TextChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void OnTextChanged(EventArgs args)
        {
            base.OnTextChanged(args);

            // If is the main window
            if (ApplicationElement.IsMainWindow(this))
            {
                this.ApplyWindowTitle();
            }
        }



        /// <summary>
        /// Applies the window title.
        /// </summary>
        internal void ApplyWindowTitle()
        {
            // Set the document title based on the top window text
            BrowserUtils.SetDocumentTitle(this.Text);
        }




        /// <summary>
        /// Removes the window.
        /// </summary>
        private void RemoveWindow(bool closeDialog)
        {
            // Get the current control container
            ApplicationElement applicationElement = ApplicationElement.Current;

            // If there is a valid control container
            if (applicationElement != null)
            {
                // Removes the current visual window
                applicationElement.Remove(this, closeDialog);
            }
        }




        /// <summary>
        /// Layouts the MDI.
        /// </summary>
        /// <param name="enmMdiLayout">The MDI layout.</param>
        public void LayoutMdi(MdiLayout enmMdiLayout)
        {
            Arange(enmMdiLayout);
        }

        /// <summary>
        /// Shows the non modal dialog.
        /// </summary>
        /// <exception cref="System.NullReferenceException"></exception>
        private void ShowNonModalDialog()
        {
            // Get the current control container
            ApplicationElement applicationElement = ApplicationElement.Current;

            // If there is a valid control container
            if (applicationElement != null)
            {
                // If there is a valid window target
                if (this.HasWindowTarget)
                {
                    // Get the window target
                    IWindowTarget windowTarget = applicationElement.GetWindowTarget(this.WindowTarget);

                    // If there is a valid window target
                    if (windowTarget != null)
                    {
                        // Add this window
                        windowTarget.AddWindow(this);

                        // Make the window visible
                        Visible = true;
                    }
                    else
                    {
                        // Throw error indicating that window target was not found
                        throw new NullReferenceException(string.Format("Window target was not found [id={0}].", this.WindowTarget));
                    }
                }
                else
                {
                    // Add the current visual element
                    applicationElement.Add(this, true, false);

                    // Make the window visible
                    Visible = true;
                }
            }

        }


        /// <summary>
        /// Shows the form as a modal dialog box.
        /// </summary>
        /// <returns>One of the T:System.Windows.Forms.DialogResult values.</returns>
        public Task<DialogResult> ShowDialog()
        {
            // Create task source
            TaskCompletionSource<DialogResult> taskSource = new TaskCompletionSource<DialogResult>();

            // Get the current control container
            ApplicationElement applicationElement = ApplicationElement.Current;

            // If there is a valid control container
            if (applicationElement != null)
            {
                // Set one time handler
                _oneTimeWindowClosed = (sender, args) =>
                {
                    // Set the callback result
                    RenderingContext.SetSynchronizationCallbackResult(taskSource, this.DialogResult);
                };

                // If there is a valid window target
                if (this.HasWindowTarget)
                {
                    // Get the window target
                    IWindowTarget windowTarget = applicationElement.GetWindowTarget(this.WindowTarget);

                    // If there is a valid window target
                    if (windowTarget != null)
                    {
                        // Add this window
                        windowTarget.AddWindow(this);

                        // Make the window visible
                        Visible = true;
                    }
                    else
                    {
                        // Throw error indicating that window target was not found
                        throw new NullReferenceException(string.Format("Window target was not found [id={0}].", this.WindowTarget));
                    }
                }
                else
                {
                    // Add the current visual element
                    applicationElement.Add(this, true, true);

                    // Make the window visible
                    Visible = true;
                }
            }

            return taskSource.Task;
        }





        /// <summary>
        /// Shows this control.
        /// </summary>
        public override void Show()
        {
            // If there is a valid window target
            if (this.HasWindowTarget)
            {
                // Show the dialog window (using window target).
                this.ShowNonModalDialog();
            }
            else
            {
                // Get the mdi parent
                WindowElement mdiParent = MdiParent;

                // If there is a valid mdi parent
                if (mdiParent != null)
                {
                    // Get the mdi container
                    WindowMdiContainer mdiContainer = mdiParent.MdiContainer;

                    // If there is a valid mdi container
                    if (mdiContainer != null)
                    {
                        // Add this window to the mdi container
                        mdiContainer.Add(this, false);

                        // Make the window visible
                        Visible = true;
                    }
                }
                else
                {
                    this.ShowNonModalDialog();
                }
            }
        }



        /// <summary>
        /// Hides this control
        /// </summary>
        public override void Hide()
        {
            base.Hide();
            this.InvokeWindowAction(WindowActionType.Close);
        }


        /// <summary>
        /// WindowClosing event handler.
        /// </summary>
        private event EventHandler<WindowClosingEventArgs> _windowClosing;

        /// <summary>
        /// Add or remove WindowClosing action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<WindowClosingEventArgs> WindowClosing
        {
            add
            {
                bool needNotification = (_windowClosing == null);

                _windowClosing += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WindowClosing");
                }

            }
            remove
            {
                _windowClosing -= value;

                if (_windowClosing == null)
                {
                    OnEventHandlerDeattached("WindowClosing");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has WindowClosing listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasWindowClosingListeners
        {
            get { return true; }
        }

        /// <summary>
        /// Raises the <see cref="E:WindowClosing" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WindowClosing"/> instance containing the event data.</param>
        protected virtual void OnWindowClosing(WindowClosingEventArgs args)
        {

            // Check if there are listeners.
            if (_windowClosing != null)
            {
                _windowClosing(this, args);
            }

            if (!args.Cancel)
            {
                WindowClosedEventArgs wndowClosedArgs = new WindowClosedEventArgs();
                PerformWindowClosed(wndowClosedArgs);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WindowClosing" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWindowClosing(WindowClosingEventArgs args)
        {

            // Raise the WindowClosing event.
            OnWindowClosing(args);
        }


        /// <summary>
        /// Shown event handler.
        /// </summary>
        private event EventHandler _Shown;

        /// <summary>
        /// Add or remove Shown action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler Shown
        {
            add
            {
                bool needNotification = (_Shown == null);

                _Shown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Shown");
                }

            }
            remove
            {
                _Shown -= value;

                if (_Shown == null)
                {
                    OnEventHandlerDeattached("Shown");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Shown listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasShownListeners
        {
            get { return _Shown != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Shown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Shown"/> instance containing the event data.</param>
        protected virtual void OnShown(EventArgs args)
        {

            // Check if there are listeners.
            if (_Shown != null)
            {
                _Shown(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Shown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformShown(EventArgs args)
        {

            // Raise the Shown event.
            OnShown(args);
        }


        /// <summary>
        /// WindowClosed event handler.
        /// </summary>
        private event EventHandler<WindowClosedEventArgs> _windowClosed;

        /// <summary>
        /// Add or remove WindowClosed action.
        /// </summary>
        public event EventHandler<WindowClosedEventArgs> WindowClosed
        {
            add
            {
                bool needNotification = (_windowClosed == null);

                _windowClosed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WindowClosed");
                }

            }
            remove
            {
                _windowClosed -= value;

                if (_windowClosed == null)
                {
                    OnEventHandlerDeattached("WindowClosed");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:WindowClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WindowClosed"/> instance containing the event data.</param>
        protected virtual void OnWindowClosed(WindowClosedEventArgs args)
        {
            // Remove window
            RemoveWindow(args.Reason == CloseReason.Code);

            OnOneTimeWindowClosed(args);

            // Check if there are listeners.
            if (_windowClosed != null)
            {
                _windowClosed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WindowClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWindowClosed(WindowClosedEventArgs args)
        {
            RaiseClosedEvent();
            // Raise the WindowClosed event.
            OnWindowClosed(args);
        }

        /// <summary>
        /// 
        /// </summary>
        [RendererMethodDescription]
        private void RaiseClosedEvent()
        {
            this.InvokeMethodOnce("RaiseClosedEvent");
        }


        /// <summary>
        /// Raises the <see cref="E:VisibleChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void OnVisibleChanged(EventArgs args)
        {
            base.OnVisibleChanged(args);

            if (!Visible)
            {
                OnOneTimeWindowClosed(WindowClosedEventArgs.Empty);

            }
        }




        /// <summary>
        /// Raises the <see cref="E:OneTimeWindowClosed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WindowClosedEventArgs"/> instance containing the event data.</param>
        private void OnOneTimeWindowClosed(WindowClosedEventArgs args)
        {
            // If there is a one time closed event
            if (_oneTimeWindowClosed != null)
            {

                // Save one time window closed
                EventHandler oneTimeWindowClosed = _oneTimeWindowClosed;

                // Ensure one time window closed is cleared
                _oneTimeWindowClosed = null;

                // Invoke one time close event
                oneTimeWindowClosed(this, args);
            }
        }



        /// <summary>
        /// Called when window closing.
        /// </summary>
        /// <param name="closeReason">The close reason.</param>
        /// <returns></returns>
        private bool OnWindowClosing(CloseReason closeReason)
        {
            WindowClosingEventArgs args = new WindowClosingEventArgs(closeReason, false);
            OnWindowClosing(args);
            return !args.Cancel;
        }





        /// <summary>
        ///Raises the E:System.Windows.Forms.Form.Closed event.
        /// </summary>
        /// <param name="e">The T:System.EventArgs that contains the event data. </param>
        protected virtual void OnClosed(EventArgs e)
        {
        }


        /// <summary>
        /// Deactivate event handler.
        /// </summary>
        private event EventHandler _Deactivate;

        /// <summary>
        /// Add or remove Deactivate action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler Deactivate
        {
            add
            {
                bool needNotification = (_Deactivate == null);

                _Deactivate += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Deactivate");
                }

            }
            remove
            {
                _Deactivate -= value;

                if (_Deactivate == null)
                {
                    OnEventHandlerDeattached("Deactivate");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Deactivate listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDeactivateListeners
        {
            get { return _Deactivate != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Deactivate" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Deactivate"/> instance containing the event data.</param>
        protected virtual void OnDeactivate(EventArgs args)
        {

            // Check if there are listeners.
            if (_Deactivate != null)
            {
                _Deactivate(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Deactivate" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDeactivate(EventArgs args)
        {

            // Raise the Deactivate event.
            OnDeactivate(args);
        }


        /// <summary>
        /// Activated event handler.
        /// </summary>
        private event EventHandler _Activated;

        /// <summary>
        /// Add or remove Activated action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler Activated
        {
            add
            {
                bool needNotification = (_Activated == null);

                _Activated += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Activated");
                }

            }
            remove
            {
                _Activated -= value;

                if (_Activated == null)
                {
                    OnEventHandlerDeattached("Activated");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Activated listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasActivatedListeners
        {
            get { return _Activated != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Activated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Activated"/> instance containing the event data.</param>
        protected virtual void OnActivated(EventArgs args)
        {

            // Check if there are listeners.
            if (_Activated != null)
            {
                _Activated(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Activated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformActivated(EventArgs args)
        {

            // Raise the Activated event.
            OnActivated(args);
        }


        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);

            // If is not activated yet
            if (!_activated)
            {
                // Indicate activated
                _activated = true;

                // Call the activated event
                OnActivated(args);
            }
        }


        internal override TVisualElement GetVisualElement<TVisualElement>(string visualElementID)
        {
            if (_menu != null)
            {
                if (string.Equals(_menu.ID, visualElementID))
                {
                    return _menu as TVisualElement;
                }
            }
            return base.GetVisualElement<TVisualElement>(visualElementID);
        }




        /// <summary>
        /// Shows the specified modal.
        /// </summary>
        /// <param name="modal">The modal mode.</param>
        /// <param name="ownerWindow">The owner window.</param>
        public void Show(DialogMode modal, object ownerWindow)
        {
            // If is a modal dialog mode
            if (modal == DialogMode.Modal)
            {
                ShowDialog();
            }
            else
            {
                Show();
            }
        }





        /// <summary>
        /// Shows the specified modal.
        /// </summary>
        /// <param name="modal">The modal mode.</param>
        public void Show(DialogMode modal)
        {
            Show(modal, null);
        }



        public void PopupMenu(object menu, PopupMenuFlags flags, int x, int y, string boldCommand)
        {
            // If menu parameter is omitted, the form with the focus is assumed to be menu.
            if (menu == null)
            {
                menu = this;
            }
        }





        /// <summary>
        /// Arrange children of MDI form
        /// </summary>
        /// <param name="layout"></param>
        public void Arange(MdiLayout layout)
        {
            if (!IsMdiContainer)
            {
                return;
            }
            switch (layout)
            {
                case MdiLayout.Cascade:
                    break;
                case MdiLayout.TileHorizontal:
                    break;
                case MdiLayout.TileVertical:
                    break;
                case MdiLayout.ArrangeIcons:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("layout");
            }
        }


        /// <summary>
        /// Registers the component.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        void IComponentManagerContainer.RegisterComponent(IVisualElement visualElement)
        {
            // Get actual visual element
            VisualElement actualVisualElement = visualElement as VisualElement;


            // If there is a valid actual visual element
            if (actualVisualElement != null)
            {
                // Add to component manager elements
                componentManagerElements.Add(actualVisualElement);
                actualVisualElement.ParentElement = this;
            }
        }

        /// <summary>
        /// Gets the component elements.
        /// </summary>
        /// <value>
        /// The component elements.
        /// </value>
        IEnumerable<IVisualElement> IComponentManagerContainer.Elements
        {
            get
            {
                return componentManagerElements;
            }
        }

        /// <summary>
        /// Prints the contents of an image file on a page.
        /// </summary>
        /// <param name="imageReference">Image value representing the image to be printed..</param>
        /// <param name="lvlngX">Single value indicating the horizontal destination coordinates where the image will be printed. The ScaleMode property determines the units of measure used..</param>
        /// <param name="lvlngY">TSingle value indicating the vertical destination coordinates where the image will be printed. The ScaleMode property determines the units of measure used.</param>
        /// <param name="width">Optional. Single value indicating the destination width of the picture. The ScaleMode property of object determines the units of measurement used. If the destination width is larger or smaller than the source width, picture is stretched or compressed to fit. If omitted, the source width is used.</param>
        /// <param name="height">Optional. Single value indicating the destination height of the picture. The ScaleMode property of object determines the units of measurement used. If the destination height is larger or smaller than the source height, picture is stretched or compressed to fit. If omitted, the source height is used.</param>
        /// <param name="o">Optional. Single values indicating the coordinates (x-axis) of a clipping region within picture. The ScaleMode property of object determines the units of measurement used. If omitted, 0 is assumed.</param>
        /// <param name="o1">Optional. Single values indicating the coordinates (y-axis) of a clipping region within picture. The ScaleMode property of object determines the units of measurement used. If omitted, 0 is assumed.</param>
        /// <param name="o2">Optional. Single value indicating the source width of a clipping region within picture. The ScaleMode property of object determines the units of measurement used. If omitted, the entire source width is used.</param>
        /// <param name="o3">Optional. Single value indicating the source height of a clipping region within picture. The ScaleMode property of object determines the units of measurement used. If omitted, the entire source height is used.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void PaintPicture(ImageReference imageReference, int lvlngX, int lvlngY, int width, int height, object o, object o1, object o2, object o3)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control is created.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this control is created; otherwise, <c>false</c>.
        /// </value>
        public bool IsCreated
        {
            get;
            set;
        }



        private bool _EnableEsc = false;

        /// <summary>
        /// gets or sets the ability of closing the window with the Esc key.
        /// </summary>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool EnableEsc
        {
            get
            {
                return _EnableEsc;
            }
            set
            {
                if (_EnableEsc != value)
                {
                    _EnableEsc = value;

                    OnPropertyChanged("EnableEsc");
                }
            }
        }

        /// <summary>
        /// Called when view restored.
        /// </summary>
        protected internal override void OnViewRestored()
        {
            ApplicationElement.RestoreView(this);
        }


    }


}
