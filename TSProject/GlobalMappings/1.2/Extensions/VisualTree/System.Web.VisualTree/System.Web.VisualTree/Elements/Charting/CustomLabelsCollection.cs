namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="ChartElementCollection{CustomLabel}" />
    [Serializable]
	public class CustomLabelsCollection : ChartElementCollection<CustomLabel>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomLabelsCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal CustomLabelsCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
		{
		}

	}
}
