﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    ///  Provides data for System.Web.VisualTree.Elements.TabElement.Selected event.
    /// </summary>
    public class TabControlEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the TabControlEventArgs class.
        /// </summary>
        /// <param name="tabPage">The tab page.</param>
        /// <param name="prevTabPage">The previous tab page.</param>
        /// <param name="tabPageIndex">Index of the tab page.</param>
        /// <param name="action">The action.</param>
        public TabControlEventArgs(TabItem tabPage, TabItem prevTabPage, Int32 tabPageIndex, TabControlAction action)
        {
            Page = tabPage;
            PrevPage = prevTabPage;
            TabPageIndex = tabPageIndex;
            Action = action;
        }


        /// <summary>
        /// The TabPage the event is occurring for.
        /// </summary>
        public TabItem Page
        {
            get;
            set;
        }


        /// <summary>
        /// The PrevTabPage the event is occurring for.
        /// </summary>
        public TabItem PrevPage
        {
            get;
            set;
        }


        /// <summary>
        /// The zero-based index of tabPage in the TabControl.TabPages collection
        /// </summary>
        public Int32 TabPageIndex
        {
            get;
            set;
        }


        /// <summary>
        /// TabControl event.
        /// </summary>
        public TabControlAction Action
        {
            get;
            set;
        }
    }
}
