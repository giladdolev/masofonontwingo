﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for window targets
    /// </summary>
    internal interface IWindowTarget
    {
        /// <summary>
        /// Gets or sets the window target identifier.
        /// </summary>
        /// <value>
        /// The window target identifier.
        /// </value>
        string WindowTargetID
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void AddWindow(WindowElement windowElement);

        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void RemoveWindow(WindowElement windowElement);
    }

    /// <summary>
    /// Provides support for window targets containers
    /// </summary>
    internal interface IWindowTargetContainer
    {

    }
}
