﻿namespace System.Web.VisualTree.Elements
{
    public class ScrollEventArgs
    {
        /// <summary>
        /// Keys the press event arguments.
        /// </summary>
        public ScrollEventArgs()
        {
        }



        /// <summary>
        /// Gets or sets a value indicating whether [handled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [handled]; otherwise, <c>false</c>.
        /// </value>
        public bool Handled
        {
            get;
            set;
        }
    }

}
