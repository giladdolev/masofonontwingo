﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class GroupBoxElement : ScrollableControl
    {
        private bool _floodShowPct = false;
        private bool _foundedCorners = false;
        GroupBoxAlignment _alignment = GroupBoxAlignment.LeftTop;


        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBoxElement"/> class.
        /// </summary>
        public GroupBoxElement()
        {

        }



        [DefaultValue(GroupBoxAlignment.LeftTop)]
        public GroupBoxAlignment Alignment
        {
            get { return _alignment; }
            set
            {
                _alignment = value;

                // Notify property Alignment changed
                OnPropertyChanged("Alignment");
            }
        }



        public bool RoundedCorners
        {
            get { return _foundedCorners; }
            set
            {
                _foundedCorners = value;

                // Notify property RoundedCorners changed
                OnPropertyChanged("RoundedCorners");
            }
        }



        public bool FloodShowPct
        {
            get { return _floodShowPct; }
            set
            {
                _floodShowPct = value;

                // Notify property FloodShowPct changed
                OnPropertyChanged("FloodShowPct");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }
    }
}
