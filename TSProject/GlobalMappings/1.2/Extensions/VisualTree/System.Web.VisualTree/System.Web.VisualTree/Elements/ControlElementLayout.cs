﻿namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for adding layout information
    /// </summary>
    public class ControlElementLayout
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlElementLayout"/> class.
        /// </summary>
        /// <param name="layout">The layout.</param>
        public ControlElementLayout(ControlElementLayout layout)
        {

        }
    }
}
