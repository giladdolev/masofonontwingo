﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.FileSystem;

namespace System.Web.VisualTree.Elements.Compatibility
{

    public class DirectoryListBoxElement : FileSystemBase
    {
        /// <summary>
        /// The tree
        /// </summary>
        private readonly TreeElement _tree;


        /// <summary>
        /// Changed event handler.
        /// </summary>
        private event EventHandler _Changed;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Changed action.
        /// </summary>
        public event EventHandler Changed
        {
            add
            {
                bool needNotification = (_Changed == null);

                _Changed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Changed");
                }

            }
            remove
            {
                _Changed -= value;

                if (_Changed == null)
                {
                    OnEventHandlerDeattached("Changed");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Changed listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasChangedListeners
        {
            get { return _Changed != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Changed"/> instance containing the event data.</param>
        protected virtual void OnChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_Changed != null)
            {
                _Changed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformChanged(EventArgs args)
        {

            // Raise the Changed event.
            OnChanged(args);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryListBoxElement"/> class.
        /// </summary>
        public DirectoryListBoxElement()
        {
            _tree = new TreeElement();
            _tree.Dock = Dock.Fill;
            _tree.AfterSelect += OnTreeAfterSelect;
            base.Add(_tree, false);
        }



        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public string Path
        {
            get
            {
                // Get tree item
                TreeItem selectedItem = _tree.SelectedItem;

                // If there is a valid tree item
                if (selectedItem != null)
                {
                    // Get selected directory
                    VirtualDirectory selectedDirectory = selectedItem.Tag as VirtualDirectory;

                    // If there is a valid selected directory
                    if (selectedDirectory != null)
                    {
                        // Return path
                        return selectedDirectory.Path;
                    }
                }

                return null;
            }
            set
            {
                // Clear the tree items
                _tree.Items.Clear();

                // Get virtual directory
                VirtualDirectory directory = FileSystem.GetVirtualDirectory(value);

                // If there is a valid virtual directory
                if (directory != null)
                {
                    // Add directories and set the selected items
                    _tree.SelectedItem = AddDirectory(_tree.Items, directory);


                }

                // Refresh the tree view
                _tree.Refresh();
            }
        }




        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override sealed void Add(VisualElement visualElement, bool insert)
        {
            base.Add(visualElement, insert);
        }


        /// <summary>
        /// Called when tree after select.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="TreeEventArgs"/> instance containing the event data.</param>
        private void OnTreeAfterSelect(object sender, TreeEventArgs args)
        {
            PerformChanged(args);
        }

        
        /// <summary>
        /// Adds the directory.
        /// </summary>
        /// <param name="treeItemCollection">The tree item collection.</param>
        /// <param name="directory">The directory.</param>
        private TreeItem AddDirectory(TreeItemCollection treeItemCollection, VirtualDirectory directory)
        {
            // The tree item
            TreeItem treeItem = null;

            // If there is a valid collection and directory
            if (treeItemCollection != null && directory != null)
            {
                // Create tree item
                treeItem = new TreeItem(directory.Name);
                treeItem.Tag = directory;

                // Add item
                treeItemCollection.Add(treeItem);

                // Loop all sub directories
                foreach (VirtualDirectory subDirectory in directory.GetDirectories())
                {
                    // Add sub directories
                    AddDirectory(treeItem.Items, subDirectory);
                }
            }

            return treeItem;
        }
    }
}
