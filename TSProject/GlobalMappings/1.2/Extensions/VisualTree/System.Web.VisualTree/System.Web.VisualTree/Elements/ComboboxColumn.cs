﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ComboboxColumn : ControlElement, IBindingDataColumn
    {
        private string _headerText;
        private string _dataMember;
        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn"/> class.
        /// </summary>
        public ComboboxColumn()
            : this(null, null, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        public ComboboxColumn(string columnName)
            : this(columnName, columnName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public ComboboxColumn(string columnName, string headerText)
            : this(columnName, headerText, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ComboboxColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// <param name="dataType">The column data type.</param>
        public ComboboxColumn(string columnName, string headerText, System.Type dataType)
        {
            if (!String.IsNullOrWhiteSpace(columnName))
            {
                ID = NormalizePropertyName(columnName, this);
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(headerText))
                {
                    ID = NormalizePropertyName(headerText, this);
                }
                else
                {
                    ID = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
                }

            }         
            DataMember = columnName;
            HeaderText = headerText;


        }

        /// <summary>
        /// Normalize property name.
        /// </summary>
        /// <param name="name">The id name</param>
        /// <param name="cmbColumn">The CMB column.</param>
        /// <returns>
        /// correct id
        /// </returns>
        private static string NormalizePropertyName(string name, ComboboxColumn cmbColumn)
        {

            string newName = new string((from c in name where char.GetUnicodeCategory(c) != UnicodeCategory.OtherLetter && (char.IsLetterOrDigit(c) || c == '_') select c).ToArray());

            if (!newName.Equals(name))
            {
                newName = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
            }

            return newName;
        }

        /// <summary>
        /// Gets or sets the header text.
        /// </summary>
        /// <value>
        /// The header text.
        /// </value>
        public string HeaderText
        {
            get
            {
                return this._headerText;
            }
            set
            {
                this._headerText = value;
            }
        }



        /// <summary>
        /// Gets or sets the name of the data property.
        /// </summary>
        /// <value>
        /// The name of the data property.
        /// </value>
        public string DataMember
        {
            get
            {
                return this._dataMember;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    this._dataMember = NormalizePropertyName(value, this);
                }

            }
        }




        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string IBindingDataColumn.Name
        {
            get
            {
                return this.DataMember;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type IBindingDataColumn.Type
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        /// <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsValid
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        object IBindingDataColumn.GetValue(object item, int index)
        {
            ListItem listItem = item as ListItem;
            if (item != null && index >= 0)
            {
                ListItemCollection cells = listItem.DataRow;
                if (cells != null && index < cells.Count)
                {
                    return cells[index].Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsIndex
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Binds the specified property information.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        void IBindingDataColumn.Bind(PropertyInfo propertyInfo)
        {

        }


        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public GridCellElement Clone()
        {
            return (GridCellElement)this.MemberwiseClone();
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get
            {
                ComboBoxElement combo = this.Parent as ComboBoxElement;
                if (combo != null)
                {
                    return combo.Columns.IndexOf(this);
                }
                return -1;
            }
        }


        /// <summary>
        /// Format cell text .
        /// </summary>
        /// <returns></returns>
        public virtual string FormatValue(object value)
        {
            return Convert.ToString(value);
        }


        public string DataTypeName
        {
            get { return typeof(string).Name; }
        }

        private ColumnSortMode _sortType = ColumnSortMode.StringAscending;

        /// <summary>
        /// Gets the name of the Sort type.
        /// </summary>
        /// <value>
        /// The name of the Sort type.
        /// </value>
        public ColumnSortMode SortType
        {
            get
            {
                return _sortType;
            }
            set
            {
                this._sortType = value;
            }
        }
    }
}


