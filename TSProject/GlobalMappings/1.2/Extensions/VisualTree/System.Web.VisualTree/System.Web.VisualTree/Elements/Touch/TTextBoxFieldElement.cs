﻿namespace System.Web.VisualTree.Elements.Touch
{
    public class TTextBoxFieldElement : TTextBoxElement, ITFormFieldElement
    {
        public string Label
        {
            get;
            set;
        }
    }
}
