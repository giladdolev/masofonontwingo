﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public interface IButtonElement
    {
        event EventHandler Click;


        string Text
        {
            get;
            set;
        }


        bool Enabled { get; set; }
    }
}
