using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
	public abstract class ToolBarDropDown : ToolBarElement
	{
        /// <summary>
        /// Opening event handler.
        /// </summary>
        private event EventHandler _Opening;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Opening action.
        /// </summary>
        public event EventHandler Opening
        {
            add
            {
                bool needNotification = (_Opening == null);

                _Opening += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Opening");
                }

            }
            remove
            {
                _Opening -= value;

                if (_Opening == null)
                {
                    OnEventHandlerDeattached("Opening");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Opening listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasOpeningListeners
        {
            get { return _Opening != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Opening" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Opening"/> instance containing the event data.</param>
        protected virtual void OnOpening(EventArgs args)
        {

            // Check if there are listeners.
            if (_Opening != null)
            {
                _Opening(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Opening" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformOpening(EventArgs args)
        {

            // Raise the Opening event.
            OnOpening(args);
        }
	}
}
