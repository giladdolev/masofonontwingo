﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ComboGridElement : ComboBoxElement
    {
        public ComboGridElement()
        {
            this.WaitMaskDisabled = true;
        }



        [RendererMethodDescription]
        private void CreatePicker()
        {
            ComboGridData cmData = new ComboGridData();
            for (int i = 0; i < this.Columns.Count; i++)
            {
                cmData.Columns += this.Columns[i].ID + ",";
            }

            int columnIndex = 0;
            cmData.Data = "[";
            for (int i = 0; i < this.Items.Count; i++)
            {
                cmData.Data += "{";
                columnIndex = 0;
                for (int j = 0; j < this.Items[i].DataRow.Count; j++)
                {
                    cmData.Data += "\"" + this.Columns[columnIndex].ID + "\"" + ":";
                    cmData.Data += "\"" + this.Items[i].DataRow[j].Text + "\"";
                    if (j < this.Items[i].DataRow.Count - 1)
                    {
                        cmData.Data += ",";
                    }
                    columnIndex++;
                }

                cmData.Data += "}";
                if (i < this.Items.Count - 1)
                {
                    cmData.Data += ",";
                }
            }
            cmData.Data += "]";
            cmData.ShowFilter = this.ShowFilter;
            cmData.DisplayMember = this.DisplayMember;
            cmData.ValueMember = this.ValueMember;
            // Notify the change to the client .
            this.InvokeMethod("CreatePicker", cmData);
        }


        /// <summary>
        /// Raises the <see cref="E:DataSourceChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BindingManagerChangedEventArgs"/> instance containing the event data.</param>
        protected override void OnDataSourceChanged(BindingManagerChangedEventArgs args)
        {
            // Get the binding manager
            BindingManager bindingManager = BindingManager.GetBindingManager(this);
            // clear changes to prevent rendering update to view . 
            bindingManager.ClearChanges();

            CreatePicker();
        }


        /// <summary>
        /// SelectGridItem event handler.
        /// </summary>
        private event EventHandler<GridCellMouseEventArgs> _select;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove SelectGridItem action.
        /// </summary>
        public event EventHandler<GridCellMouseEventArgs> SelectGridItem
        {
            add
            {
                bool needNotification = (_select == null);

                _select += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectGridItem");
                }

            }
            remove
            {
                _select -= value;

                if (_select == null)
                {
                    OnEventHandlerDeattached("SelectGridItem");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectGridItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectGridItem"/> instance containing the event data.</param>
        protected virtual void OnSelectGridItem(GridCellMouseEventArgs args)
        {

            // Check if there are listeners.
            if (_select != null)
            {
                _select(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectGridItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectGridItem(GridCellMouseEventArgs args)
        {
            if (args != null && args.RowIndex >= 0 && args.RowIndex < this.Items.Count)
            {
                this.SelectedItem = this.Items[args.RowIndex];
                this.SelectedText = this.Items[args.RowIndex].Text;
                this.SelectedValue = this.Items[args.RowIndex].Value;
                this.SelectedIndex = args.RowIndex;
            }
            // Raise the SelectGridItem event.
            OnSelectGridItem(args);
        }

    }
}
