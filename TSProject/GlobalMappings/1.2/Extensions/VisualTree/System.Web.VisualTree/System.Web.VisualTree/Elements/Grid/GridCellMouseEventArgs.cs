namespace System.Web.VisualTree.Elements
{
    public class GridCellMouseEventArgs : MouseEventArgs
    {
        private int _columnIndex;
        private int _rowIndex;
        private bool _ctrlKey = false;
        private bool _shiftKey = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GridContextMenuCellEventArgs"/> class.
        /// </summary>
        public GridCellMouseEventArgs()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridContextMenuCellEventArgs"/> class.
        /// </summary>
        public GridCellMouseEventArgs(int columnIndex, int rowIndex)
        {
            _columnIndex = columnIndex;
            _rowIndex = rowIndex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridContextMenuCellEventArgs"/> class.
        /// </summary>
        public GridCellMouseEventArgs(int columnIndex, int rowIndex, int localX, int localY, MouseEventArgs args, bool ctrlKey, bool shiftKey)
            : base(args.Button, args.Clicks, localX, localY, args.Delta)
        {
            _columnIndex = columnIndex;
            _rowIndex = rowIndex;
            _ctrlKey = ctrlKey;
            _shiftKey = shiftKey;
        }


        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int ColumnIndex
        {
            get { return _columnIndex; }
            private set { _columnIndex = value; }
        }



        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex
        {
            get { return _rowIndex; }
            private set { _rowIndex = value; }
        }

        /// <summary>
        /// indicate if the ctrl key pressed .
        /// </summary>
        /// <vakue>true if the ctrl is pressed ,otherwise false.</vakue>
        public bool CtrlKey
        {
            get { return _ctrlKey; }
            set { _ctrlKey = value; }
        }

        /// <summary>
        /// indicate if the shift key pressed .
        /// </summary>
        /// <vakue>true if the   is pressed ,otherwise false.</vakue>
        public bool ShiftKey
        {
            get { return _shiftKey; }
            set { _shiftKey = value; }
        }

    }
}
