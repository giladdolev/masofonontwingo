namespace System.Web.VisualTree.Elements
{
	public struct Message
	{
	    public IntPtr WParam
		{
			get;
			set;
		}
		public IntPtr LParam
		{
			get;
			set;
		}
		public int Msg
		{
			get;
			set;
		}

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = WParam.GetHashCode();
                hashCode = (hashCode * 397) ^ LParam.GetHashCode();
                hashCode = (hashCode * 397) ^ Msg;
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Message && Equals((Message) obj);
        }

        public bool Equals(Message other)
        {
            return WParam.Equals(other.WParam) && LParam.Equals(other.LParam) && Msg == other.Msg;
        }

        public static bool operator ==(Message message1, Message message2)
        {
            return message1.Equals(message2);
        }

        public static bool operator !=(Message message1, Message message2)
        {
            return !message1.Equals(message2);
        }  

	}
}
