﻿using System.ComponentModel;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    public class MenuElement : ToolBarElement, ITVisualElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuElement"/> class.
        /// </summary>
        public MenuElement()
        {

        }


        /// <summary>
        /// The items
        /// </summary>
        public MenuElement(IContainer container)
        {
            if (container is VisualElement)
            {
                this.ParentElement = (VisualElement)container;
            }
        }
    }
}
