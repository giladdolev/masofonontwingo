using System.ComponentModel;
namespace System.Web.VisualTree.Elements
{
    public class ItemCheckEventArgs : EventArgs
    {

        private CheckState _state;
        private int _index = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCheckEventArgs"/> class.
        /// </summary>
        public ItemCheckEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCheckEventArgs"/> class.
        /// </summary>
        public ItemCheckEventArgs(int newIndex)
        {
            _index = newIndex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCheckEventArgs"/> class.
        /// </summary>
        /// <param name="indexs">The indexs.</param>
        /// <param name="state">The state.</param>
        /// <param name="index">The index.</param>
        public ItemCheckEventArgs(Int32[] indexs, CheckState state, int index = -1)
        {
            Indexs = indexs;
            _index = index;
            _state = state;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCheckEventArgs"/> class.
        /// </summary>
        /// <param name="indexs">he array of selected indexs.</param>
        /// <param name="newCheckValue">The new check value.</param>
        /// <param name="currentValue">The current value.</param>
        public ItemCheckEventArgs(Int32[] indexs, CheckState newCheckValue, CheckState currentValue)
        {
            Indexs = indexs;
            NewValue = newCheckValue;
            CurrentValue = currentValue;
        }


        /// <summary>
        /// Gets or sets he array of selected indexs.
        /// </summary>
        /// <value>
        /// The indexs.
        /// </value>
        public Int32[] Indexs
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the new value.
        /// </summary>
        /// <value>
        /// The new value.
        /// </value>
        public CheckState NewValue
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        /// <value>
        /// The current value.
        /// </value>
        public CheckState CurrentValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the index of the item whose state was changed.
        /// </summary>
        [DefaultValue(-1)]
        public int Index
        {
            get { return _index; } 
        }

        /// <summary>
        /// Gets the state of the item.
        /// </summary>
        public CheckState State 
        {
            get { return _state; }
        }
    }
}
