﻿using System.Collections.Generic;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ImageListStreamer : VisualElement
    {

        private readonly IEnumerable<ResourceReference> _images = null;
        private static ImageListStreamer _imageListStreamer;



        /// <summary>
        /// Returns a blank image list streamer.
        /// </summary>
        public static ImageListStreamer Blank
        {
            get { return _imageListStreamer ?? (_imageListStreamer = new ImageListStreamer()); }
        }



        public IEnumerable<ResourceReference> Images
        {
            get
            {
                return _images;
            }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="ImageListStreamer"/> class.
        /// </summary>
        public ImageListStreamer()
        {
            // Create blank images list
            _images = InitializeBlankStreamer();
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="ImageListStreamer"/> class.
        /// </summary>
        /// <param name="images">The images.</param>
        public ImageListStreamer(IEnumerable<ResourceReference> images)
        {
            // If there is a valid images list
            if (images != null)
            {
                // Set the images list
                _images = images;
            }
            else
            {
                // Initialize blank streamer
                _images = InitializeBlankStreamer();
            }
        }



        /// <summary>
        /// Initializes the blank streamer.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<ResourceReference> InitializeBlankStreamer()
        {
            // Create images list
            List<ImageReference> images = new List<ImageReference>();

            // Return 20 images
            for (int i = 0; i < 20; i++)
            {
                // Add black image
                images.Add(ImageReference.Blank);
            }

            // Set the images list
            return images;
        }
    }
}
