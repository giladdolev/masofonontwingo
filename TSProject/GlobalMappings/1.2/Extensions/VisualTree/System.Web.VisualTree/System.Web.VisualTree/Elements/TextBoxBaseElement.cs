using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignTimeVisible(false)]
    public class TextBoxBaseElement : InputElement
    {
     //   private bool _selectOnFocus = false;
        private int _maxLength = 32767;
        private int _selectionLength = 0;

        public TextBoxBaseElement()
        {
        }
        /// <summary>
        /// Gets or sets the maximum length.
        /// </summary>
        /// <value>
        /// The maximum length.
        /// </value>
        [DefaultValue(-1)]
        [RendererPropertyDescription]
        public int MaxLength
        {
            get { return _maxLength; }
            set
            {
                if (_maxLength != value)
                {
                    _maxLength = value;
                    OnPropertyChanged("MaxLength");
                }
            }
        }

        private bool _readOnly = false;
        [Category("Behavior")]
        [RendererPropertyDescription]
        /// <summary>
        /// Gets or sets the read only state.
        /// </summary>
        /// <value>
        /// The read only state.
        /// </value>
        public bool ReadOnly
        {
            get { return _readOnly; }
            set
            {
                _readOnly = value;
                OnPropertyChanged("ReadOnly");
            }
        }



        public virtual bool Multiline
        {
            get;
            set;
        }



        public int SelectionStart
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of characters selected in the text box element.
        /// </summary>
        [RendererPropertyDescription]
        public int SelectionLength
        {
            get { return _selectionLength; }
            set
            {
                if (_selectionLength != value)
                {
                    _selectionLength = value > this.Text.Length ? this.Text.Length - this.SelectionStart : value - this.SelectionStart;
                    OnPropertyChanged("SelectionLength");
                }
            }
        }
        public int TextLength
        {
            get;
            set;
        }


        public string SelectedText
        {
            get;
            set;
        }

        /// <summary>
        /// Clears all the content from the text box.
        /// </summary>
        public void Clear()
        {
            this.Text = string.Empty;
        }

        /// <summary>
        /// Selects all the text in the control.
        /// </summary>
        /// <returns></returns>
        public void SelectAll()
        {
            this.SelectedText = this.Text;
            this.SelectionStart = 0;
            this.SelectionLength = this.Text.Length;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="length"></param>
        public void Select(int start, int length)
        {
            this.SelectedText = this.Text;
            this.SelectionStart = start;
            this.SelectionLength = length;
        }

        /// <summary>
        /// Gets or sets the SelectOnFocus.
        /// true on focus select the text , otherwise false .
        /// </summary>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool SelectOnFocus
        {
            get;
            set;
        }


    }
}
