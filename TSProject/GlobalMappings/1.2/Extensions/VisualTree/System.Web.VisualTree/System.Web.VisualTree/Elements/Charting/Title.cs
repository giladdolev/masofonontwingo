using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ChartNamedElement" />
    [DesignerComponent]
    public class ChartTitle : ChartNamedElement
	{
        public ChartTitle()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartTitle"/> class.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <param name="dock">The dock.</param>
        /// <param name="font">The font.</param>
        /// <param name="color">The color.</param>
        public ChartTitle(string p, Dock dock, Font font, Color color)
        {
            Position = new ElementPosition();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartTitle"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public ChartTitle(string name)
        {
            ID = name;
            Position = new ElementPosition();
        }

		/// <summary>
		///Gets or sets the visibility flag of the title.
		/// </summary>
		public virtual bool Visible
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets an T:System.Windows.Forms.DataVisualization.Charting.ElementPosition object, which can be used to get or set the position of the title.
		/// </summary>
		public ElementPosition Position
		{
			get;
			set;
		}
	}
}
