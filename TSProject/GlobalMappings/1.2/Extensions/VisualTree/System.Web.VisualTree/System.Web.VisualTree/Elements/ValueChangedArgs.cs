﻿namespace System.Web.VisualTree.Elements
{
    public class ValueChangedArgs<TValue> : EventArgs
    {
        private readonly TValue _value;


        /// <summary>
        /// Initializes a new instance of the <see cref="ValueChangedArgs{TValue}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public ValueChangedArgs(TValue value)
        {
            _value = value;
        }



        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public TValue Value
        {
            get { return _value; }
        }




        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public TValue GetValue(EventArgs args, TValue defaultValue )
        {
            ValueChangedArgs<TValue> valueArgs = args as ValueChangedArgs<TValue>;

            if(valueArgs != null)
            {
                return valueArgs.Value;
            }

            return defaultValue;
        }




        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        /// <returns></returns>
        public TValue GetValue(EventArgs args)
        {
            return GetValue(args,default(TValue));
        }
    }
}
