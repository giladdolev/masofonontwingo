﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.Mvc.VisualTree.Elements
{
    public class TabItemSettings : ControlSettings
    {
        /// <summary>
        /// The tab title
        /// </summary>
        private string mstrTitle = String.Empty;


        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get
            {
                return mstrTitle;
            }
            set
            {
                mstrTitle = value;
            }
        }



        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public object Header
        {
            get;
            set;
        }
    }
}
