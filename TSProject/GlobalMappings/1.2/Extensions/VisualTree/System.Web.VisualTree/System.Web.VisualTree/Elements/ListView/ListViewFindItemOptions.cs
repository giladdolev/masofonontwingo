﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// . An integer or constant specifying whether the string will be matched to the ListItem object's Text, Subitems, or Tag property, as described in Settings.
    /// </summary>
    public enum ListViewFindItemOptions
    {
        /// <summary>
        /// (Default) Matches the string with a ListItem object's Text property.
        /// </summary>
        Subitem,

        /// <summary>
        /// Matches the string with any string in a ListItem object's Subitems property.
        /// </summary>
        Tag,

        /// <summary>
        /// Matches the string with any ListItem object's Tag property.
        /// </summary>
        Text
    }
}
