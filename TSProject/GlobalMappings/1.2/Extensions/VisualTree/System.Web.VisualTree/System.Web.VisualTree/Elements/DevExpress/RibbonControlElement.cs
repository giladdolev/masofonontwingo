﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    public class RibbonControlElement : ContentControlElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonControlElement"/> class.
        /// </summary>
        public RibbonControlElement()
        {
        }


        /// <summary>
        /// Gets the Pages.
        /// </summary>
        /// <value>
        /// The Pages.
        /// </value>
        public RibbonPageCollection Pages
        {
            get { return Controls as RibbonPageCollection; }
        }

        /// <summary>
        /// Creates the RibbonPage element collection.
        /// </summary>
        /// <returns></returns>
        protected override ControlElementCollection CreateControlElementCollection()
        {
            return new RibbonPageCollection(this, "Pages");
        }
    }
}
