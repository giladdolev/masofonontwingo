using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Displays a hierarchical collection of labeled items, each represented by a TreeItem
    /// </summary>
    public class TreeElement : ControlElement, IBindingViewModel, IClinetHistoryContainerElement
    {
        private bool _checkBoxes;
        private bool _hideSelection = true;
        private Color _lineColor;
        private ImageList _stateImageList;
        private TreeElementStyles _style = TreeElementStyles.TreelinesPlusMinusText;
        private ImageList _imageList;
        private TreeDrawMode _drawMode = TreeDrawMode.Normal;
        private TreeItem _selectedItem;
        private readonly TreeItemCollection _treeItems;
        private bool _searchPanel = false;
        private TreeColumnCollection _columns;

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeElement"/> class.
        /// </summary>
        public TreeElement()
        {
            // Initialize tree view nodes
            _treeItems = new TreeItemCollection(this, "Items");
            _columns = new TreeColumnCollection(this, "Columns");
        }


        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void IBindingViewModel.Bind(IDataReader dataReader)
        {
            List<string> treeItemValues = new List<string>();
            // create data table .
            DataTable treeDataTable = new DataTable();
            string itemId = "";

            // If there is a valid data reader
            if (dataReader != null)
            {
                string fieldName = "";
                fieldName = dataReader.GetName(dataReader.GetOrdinal(KeyFieldName));
                // Add column
                _columns.Add(new TreeColumn(fieldName));
                treeDataTable.Columns.Add(fieldName);
                // Loop all fields
                for (int index = 1; index < dataReader.FieldCount; index++)
                {
                    fieldName = dataReader.GetName(index);
                    _columns.Add(new TreeColumn(fieldName));
                    treeDataTable.Columns.Add(fieldName);

                }


                using (this.BatchBlock)
                {
                    // Loop all records and create data table source .
                    while (dataReader.Read())
                    {
                        DataRow row = treeDataTable.NewRow();
                        // Loop all columns indexes
                        for (int index = 0; index < treeDataTable.Columns.Count; index++)
                        {
                            row[dataReader.GetName(index)] = dataReader.GetValue(index);

                        }
                        // add data row.
                        treeDataTable.Rows.Add(row);
                    }


                    //Create DataTable of item's children from DataTable
                    DataRow[] rows = treeDataTable.Select(String.Format("{0}='{1}'", ParentFieldName, itemId));
                    if (rows.Count() == 0)
                    {
                        return;
                    }
            
                    IDataReader newDataReader = BindingDataReader.Create(rows.CopyToDataTable());



                    BuildTree(treeDataTable, newDataReader, itemId);
        }
            }

            // Notify property DataSource changed
            OnPropertyChanged("DataSource");
        }


        /// <summary>
        /// Builds the treeElement
        /// </summary>
        /// <param name="treeDataTable"></param>
        /// <param name="dataReader"></param>
        /// <param name="parent"></param>
        private void BuildTree(DataTable treeDataTable, IDataReader dataReader, string parent)
        {
            TreeItem treeItem = null;
            string itemId = "";
            string value = "";
            int ID_ColIndex = treeDataTable.Columns.IndexOf(KeyFieldName);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    treeItem = new TreeItem();
                    // Loop all columns indexes
                    for (int index = 0; index < dataReader.FieldCount; index++)
                    {
                        value = string.IsNullOrEmpty(dataReader.GetValue(index).ToString()) ? "" : dataReader.GetValue(index).ToString();
                        treeItem.Values.Add(value);
                    }
                    itemId = treeItem.Values[ID_ColIndex];

                    treeItem.ID = itemId;
                    if (treeItem != null)
                    {
                        // Add item
                        Items.Add(treeItem);
                    }

                    //Create DataTable of item's children from DataTable
                    DataRow[] rows = treeDataTable.Select(String.Format("{0}='{1}'", ParentFieldName, itemId));
                    if (rows.Count() == 0)
                    {
                        continue;
                    }

                    //Build Items of Tree roots
                    IDataReader newDataReader = BindingDataReader.Create(rows.CopyToDataTable());
                    Items[Items.Count - 1].BuildTreeItems(treeDataTable, newDataReader, itemId);

                }
            }
            return;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                return _treeItems;
            }
        }



        /// <summary>
        /// Gets the tree items.
        /// </summary>
        /// <value>
        /// The tree items.
        /// </value>        
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<TreeItem>))]
        public TreeItemCollection Items
        {
            get { return _treeItems; }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }



        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        [RendererPropertyDescription]
        [DesignerIgnore]
        public TreeItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                this.SetSelectedItem(value);

                // If there is a valid item
                if (_selectedItem != null)
                {
                    // Raise after select
                    OnAfterSelect(new TreeEventArgs(_selectedItem));
                }
            }
        }

        /// <summary>
        /// Sets the selected item.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="blnUpdateClient">if set to <c>true</c> update client.</param>
        private void SetSelectedItem(TreeItem value, bool blnUpdateClient = true)
        {
            // If the selected item had changed
            if (_selectedItem != value)
            {
                // Set the selected item
                _selectedItem = value;

                // If there is a valid selected item
                if (_selectedItem != null)
                {
                    // If we should report history
                    if (this.ShouldReportHistoryChange())
                    {
                        // Ensure this response is marked as an history
                        ApplicationElement.EnsureHistoryBookmark();
                    }
                }

                // If we should update the client
                if (blnUpdateClient)
                {
                    // If selected item had changed
                    OnPropertyChanged("SelectedItem");
                }
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether check boxes should be rendered.
        /// </summary>
        /// <value>
        ///   <c>true</c> if should render check boxes; otherwise, <c>false</c>.
        /// </value>
        public bool CheckBoxes
        {
            get { return _checkBoxes; }
            set
            {
                _checkBoxes = value;

                // Notify property CheckBoxes changed
                OnPropertyChanged("CheckBoxes");
            }
        }


        /// <summary>
        /// Gets a value indicating whether the search field is shown
        /// </summary>
        /// <value>
        ///   <c>true</c> if the search field is shown; otherwise, <c>false</c>.
        /// </value>
        public bool SearchPanel
        {
            get { return _searchPanel; }
            set
            {
                _searchPanel = value;

                // Notify property SearchPanel changed
                OnPropertyChanged("SearchPanel");
            }
        }




        /// <summary>
        /// Gets or sets the draw mode.
        /// </summary>
        /// <value>
        /// The draw mode.
        /// </value>
        [DefaultValue(TreeDrawMode.Normal)]
        public TreeDrawMode DrawMode
        {
            get { return _drawMode; }
            set
            {
                _drawMode = value;

                // Notify property DrawMode changed
                OnPropertyChanged("DrawMode");
            }
        }



        /// <summary>
        /// Gets or sets a value indicating whether the selected tree node remains highlighted even when the tree view has lost the focus..
        /// </summary>
        /// <value>
        ///   true if the selected tree node is not highlighted when the tree view has lost the focus; otherwise, false. The default is true.
        /// </value>
        [DefaultValue(true)]
        public bool HideSelection
        {
            get { return _hideSelection; }
            set
            {
                _hideSelection = value;

                // Notify property HideSelection changed
                OnPropertyChanged("HideSelection");
            }
        }



        /// <summary>
        /// image list
        /// </summary>
        [IDReferenceProperty]
        public ImageList ImageList
        {
            get { return _imageList; }
            set
            {
                _imageList = value;

                // Notify property ImageList changed
                OnPropertyChanged("ImageList");
            }
        }



        public Color LineColor
        {
            get { return _lineColor; }
            set
            {
                _lineColor = value;

                // Notify property LineColor changed
                OnPropertyChanged("LineColor");
            }
        }



        /// <summary>
        /// Gets or sets the state image list.
        /// </summary>
        /// <value>
        /// The state image list.
        /// </value>
        [IDReferenceProperty]
        public ImageList StateImageList
        {
            get { return _stateImageList; }
            set
            {
                _stateImageList = value;

                // Notify property StateImageList changed
                OnPropertyChanged("StateImageList");
            }
        }



        [DefaultValue(TreeElementStyles.TreelinesPlusMinusText)]
        public TreeElementStyles Style
        {
            get { return _style; }
            set
            {
                if (_style != value)
                {
                    _style = value;

                    OnPropertyChanged("Style");
                }
            }
        }



        


        
        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            // Loop all child items
            foreach (VisualElement visualElement in Items)
            {
                // If there is a valid child control
                if (IsMatchingVisualElement<TVisualElement>(visualElement, strVisualElementID))
                {
                    // Return element
                    return visualElement as TVisualElement;
                }

                // Search in TreeItem children recursively
                TreeItem treeItem = GetChildTreeNode(strVisualElementID, visualElement as TreeItem);

                // If the item is valid
                if (treeItem != null)
                {
                    return treeItem as TVisualElement;
                }
            }

            return null;
        }



        /// <summary>
        /// Gets the child tree node.
        /// </summary>
        /// <param name="treeItemId">The tree item identifier.</param>
        /// <param name="treeItem">The tree item.</param>
        /// <returns></returns>
        internal TreeItem GetChildTreeNode(string treeItemId, TreeItem treeItem)
        {
            // Loop all child items
            foreach (TreeItem item in treeItem.Items)
            {
                // If there is a valid child control
                if (IsMatchingVisualElement<TreeItem>(item, treeItemId))
                {
                    return item;
                }

                // Get child tree node
                TreeItem childTreeItem = GetChildTreeNode(treeItemId, item);

                // If the item is valid
                if (childTreeItem != null)
                {
                    return treeItem;
                }

            }
            return null;
        }

        

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Try to get treeview node element
            TreeItem objTreeNodeElement = visualElement as TreeItem;

            // If there is a valid treeview node element
            if (objTreeNodeElement != null)
            {
                _treeItems.Add(objTreeNodeElement);
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }

        /// <summary>
        /// Apply visability filters.
        /// </summary>
        public void ApplyFilter()
        {
            // If is valid 
            if (IsLoadedToClient)
            {
                // Ensure tree filters are updated
                this.InvokeMethodOnce("ApplyFilter");
            }
        }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();

            // If is valid 
            if (IsLoadedToClient)
            {
                // Ensure tree is updated
                this.InvokeMethodOnce("Refresh");
            }
        }

        /// <summary>
        /// Collapse all the nodes inside the tree
        /// </summary>
        [RendererMethodDescription]
        public void CollapseAll()
        {
            this.InvokeMethodOnce("CollapseAll");
        }



        /// <summary>
        /// Called when item chekced changed.
        /// </summary>
        /// <param name="treeItem">The tree item.</param>
        /// <param name="value">if set to <c>true</c> value.</param>
        internal void OnAfterCheckInternal(TreeItem treeItem, bool value)
        {
            // If is checked
            if (value)
            {
                // Call after check
                OnAfterCheck(new TreeEventArgs(treeItem));
            }
            this.SelectedItem = this.Items[treeItem.Index];
        }



        /// <summary>
        /// Called before checking the element
        /// </summary>
        /// <param name="objTreeCancelActionArgs">The tree cancel action arguments.</param>
        protected virtual void OnBeforeCheck(TreeCancelEventArgs objTreeCancelActionArgs)
        {

        }



        /// <summary>
        /// Called before drawing node
        /// </summary>
        /// <param name="objDrawTreeItemActionArgs">The draw tree item action arguments.</param>
        protected virtual void OnDrawNode(DrawTreeItemEventArgs objDrawTreeItemActionArgs)
        {

        }


        /// <summary>
        /// AfterSelect event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _AfterSelect;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove AfterSelect action.
        /// </summary>
        public event EventHandler<TreeEventArgs> AfterSelect
        {
            add
            {
                bool needNotification = (_AfterSelect == null);

                _AfterSelect += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AfterSelect");
                }

            }
            remove
            {
                _AfterSelect -= value;

                if (_AfterSelect == null)
                {
                    OnEventHandlerDeattached("AfterSelect");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AfterSelect listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAfterSelectListeners
        {
            get { return _AfterSelect != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AfterSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AfterSelect"/> instance containing the event data.</param>
        protected virtual void OnAfterSelect(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_AfterSelect != null)
            {
                _AfterSelect(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AfterSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterSelect(TreeEventArgs args)
        {

            // Raise the AfterSelect event.
            OnAfterSelect(args);
        }

        
        /// <summary>
        /// AfterCheck event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _afterCheck;

        /// <summary>
        /// Add or remove AfterCheck action.
        /// </summary>
        public event EventHandler<TreeEventArgs> AfterCheck
        {
            add
            {
                bool needNotification = (_afterCheck == null);

                _afterCheck += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AfterCheck");
                }

            }
            remove
            {
                _afterCheck -= value;

                if (_afterCheck == null)
                {
                    OnEventHandlerDeattached("AfterCheck");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AfterCheck listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAfterCheckListeners
        {
            get { return _afterCheck != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AfterCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AfterCheck"/> instance containing the event data.</param>
        protected virtual void OnAfterCheck(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_afterCheck != null)
            {
                _afterCheck(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AfterCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterCheck(TreeEventArgs args)
        {
            if (args != null)
            {
                this.Items[args.Item.Index].UpdateState(args.Checked);
            }
            // Raise the AfterCheck event.
            OnAfterCheck(args);
        }


        /// <summary>
        /// BeforeCheck event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _BeforeCheck;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeCheck action.
        /// </summary>
        public event EventHandler<TreeEventArgs> BeforeCheck
        {
            add
            {
                bool needNotification = (_BeforeCheck == null);

                _BeforeCheck += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeCheck");
                }

            }
            remove
            {
                _BeforeCheck -= value;

                if (_BeforeCheck == null)
                {
                    OnEventHandlerDeattached("BeforeCheck");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:BeforeCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeCheck"/> instance containing the event data.</param>
        protected virtual void OnBeforeCheck(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeCheck != null)
            {
                _BeforeCheck(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeCheck(TreeEventArgs args)
        {
            bool state = args.Checked;
            if (args != null)
            {
                this.Items[args.Item.Index].UpdateState(state);
            }

            // Raise the BeforeCheck event.
            OnBeforeCheck(args);

            //Raise the afterCheck event .
            args.Checked = !state;
            PerformAfterCheck(args);
        }

        
        /// <summary>
        /// BeforeSelect event handler.
        /// </summary>
        private event EventHandler _BeforeSelect;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeSelect action.
        /// </summary>
        public event EventHandler BeforeSelect
        {
            add
            {
                bool needNotification = (_BeforeSelect == null);

                _BeforeSelect += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeSelect");
                }

            }
            remove
            {
                _BeforeSelect -= value;

                if (_BeforeSelect == null)
                {
                    OnEventHandlerDeattached("BeforeSelect");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeSelect listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeSelectListeners
        {
            get { return _BeforeSelect != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeSelect"/> instance containing the event data.</param>
        protected virtual void OnBeforeSelect(EventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeSelect != null)
            {
                _BeforeSelect(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeSelect" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeSelect(EventArgs args)
        {

            // Raise the BeforeSelect event.
            OnBeforeSelect(args);
        }

        
        /// <summary>
        /// NodeMouseClick event handler.
        /// </summary>
        private event EventHandler<TreeMouseClickEventArgs> _NodeMouseClick;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove NodeMouseClick action.
        /// </summary>
        public event EventHandler<TreeMouseClickEventArgs> NodeMouseClick
        {
            add
            {
                bool needNotification = (_NodeMouseClick == null);

                _NodeMouseClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("NodeMouseClick");
                }

            }
            remove
            {
                _NodeMouseClick -= value;

                if (_NodeMouseClick == null)
                {
                    OnEventHandlerDeattached("NodeMouseClick");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:NodeMouseClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="NodeMouseClick"/> instance containing the event data.</param>
        protected virtual void OnNodeMouseClick(TreeMouseClickEventArgs args)
        {
            SetSelectedItem(args.Node, false);

            // Check if there are listeners.
            if (_NodeMouseClick != null)
            {
                _NodeMouseClick(this, args);
            }

            // Raise after select
            OnAfterSelect(new TreeEventArgs(_selectedItem));
        }

        /// <summary>
        /// Performs the <see cref="E:NodeMouseClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformNodeMouseClick(TreeMouseClickEventArgs args)
        {

            // Raise the NodeMouseClick event.
            OnNodeMouseClick(args);
        }

        
        /// <summary>
        /// NodeMouseDoubleClick event handler.
        /// </summary>
        private event EventHandler<TreeMouseClickEventArgs> _NodeMouseDoubleClick;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove NodeMouseDoubleClick action.
        /// </summary>
        public event EventHandler<TreeMouseClickEventArgs> NodeMouseDoubleClick
        {
            add
            {
                bool needNotification = (_NodeMouseDoubleClick == null);

                _NodeMouseDoubleClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("NodeMouseDoubleClick");
                }

            }
            remove
            {
                _NodeMouseDoubleClick -= value;

                if (_NodeMouseDoubleClick == null)
                {
                    OnEventHandlerDeattached("NodeMouseDoubleClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has NodeMouseDoubleClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasNodeMouseDoubleClickListeners
        {
            get { return _NodeMouseDoubleClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:NodeMouseDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="NodeMouseDoubleClick"/> instance containing the event data.</param>
        protected virtual void OnNodeMouseDoubleClick(TreeMouseClickEventArgs args)
        {

            // Check if there are listeners.
            if (_NodeMouseDoubleClick != null)
            {
                _NodeMouseDoubleClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:NodeMouseDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformNodeMouseDoubleClick(TreeMouseClickEventArgs args)
        {

            // Raise the NodeMouseDoubleClick event.
            OnNodeMouseDoubleClick(args);
        }

        
        /// <summary>
        /// BeforeCollapse event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _BeforeCollapse;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeCollapse action.
        /// </summary>
        public event EventHandler<TreeEventArgs> BeforeCollapse
        {
            add
            {
                bool needNotification = (_BeforeCollapse == null);

                _BeforeCollapse += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeCollapse");
                }

            }
            remove
            {
                _BeforeCollapse -= value;

                if (_BeforeCollapse == null)
                {
                    OnEventHandlerDeattached("BeforeCollapse");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeCollapse listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeCollapseListeners
        {
            get { return _BeforeCollapse != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeCollapse"/> instance containing the event data.</param>
        protected virtual void OnBeforeCollapse(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeCollapse != null)
            {
                _BeforeCollapse(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeCollapse(TreeEventArgs args)
        {

            // Raise the BeforeCollapse event.
            OnBeforeCollapse(args);
        }


        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        IEnumerable<IBindingDataColumn> IBindingViewModel.Columns
        {
            get
            {
                return this.Columns;
            }
        }


        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public TreeColumnCollection Columns
        {
            get
            {
                return _columns;
            }
        }

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        IEnumerable<IBindingViewModelRecord> IBindingViewModel.Records
        {
            get
            {
                return this.Items;
            }
        }

        /// <summary>
        /// Expands all.
        /// </summary>
        public void ExpandAll()
        {
            // Expand TreeItem children recursively
            foreach (TreeItem treeItem in Items.Where(treeItem => treeItem != null))
            {
                treeItem.ExpandAll();
            }
        }

        /// <summary>
        /// Gets the node count.
        /// </summary>
        /// <param name="includeSubtrees">if set to <c>true</c> include sub trees.</param>
        /// <returns></returns>
        public int GetNodeCount(bool includeSubtrees)
        {
            int childCount = Items.Count;
            if (includeSubtrees)
            {
                for (int i = 0; i < childCount; i++)
                {
                    childCount += Items[i].GetNodeCount(true);
                }
            }
            return childCount;
        }

        
        /// <summary>
        /// BeforeExpand event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _BeforeExpand;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeExpand action.
        /// </summary>
        public event EventHandler<TreeEventArgs> BeforeExpand
        {
            add
            {
                bool needNotification = (_BeforeExpand == null);

                _BeforeExpand += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeExpand");
                }

            }
            remove
            {
                _BeforeExpand -= value;

                if (_BeforeExpand == null)
                {
                    OnEventHandlerDeattached("BeforeExpand");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeExpand listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeExpandListeners
        {
            get { return _BeforeExpand != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeExpand"/> instance containing the event data.</param>
        protected virtual void OnBeforeExpand(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeExpand != null)
            {
                _BeforeExpand(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeExpand(TreeEventArgs args)
        {

            // Raise the BeforeExpand event.
            OnBeforeExpand(args);
        }


        /// <summary>
        /// AfterExpand event handler.
        /// </summary>
        private event EventHandler<TreeEventArgs> _AfterExpand;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove AfterExpand action.
        /// </summary>
        public event EventHandler<TreeEventArgs> AfterExpand
        {
            add
            {
                bool needNotification = (_AfterExpand == null);

                _AfterExpand += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AfterExpand");
                }

            }
            remove
            {
                _AfterExpand -= value;

                if (_AfterExpand == null)
                {
                    OnEventHandlerDeattached("AfterExpand");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AfterExpand listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAfterExpandListeners
        {
            get { return _AfterExpand != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AfterExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AfterExpand"/> instance containing the event data.</param>
        protected virtual void OnAfterExpand(TreeEventArgs args)
        {

            // Check if there are listeners.
            if (_AfterExpand != null)
            {
                _AfterExpand(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AfterExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterExpand(TreeEventArgs args)
        {

            // Raise the AfterExpand event.
            OnAfterExpand(args);
        }


        private bool _showLines;
        /// <summary>
        /// Gets or sets a value indicating whether lines are drawn between tree nodes in the tree view control.
        /// </summary>
        /// 
        /// <returns>
        /// true if lines are drawn between tree nodes in the tree view control; otherwise, false. The default is true.
        /// </returns>
        [Category("Behavior")]
        [DefaultValue(true)]
        public bool ShowLines
        {
            get
            {
                return this._showLines;
            }
            set
            {
                if (_showLines != value)
                {
                    this._showLines = value;

                    OnPropertyChanged("ShowLines");
                }
                
            }
        }

        
        private bool _sorted = false;
        /// <summary>
        /// Gets or sets a value indicating whether the tree nodes in the tree view are sorted.
        /// </summary>
        /// <value>
        /// true if the tree nodes in the tree view are sorted; otherwise, false. The default is false.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Appearance")]
        public bool Sorted
        {
            get { return _sorted; }
            set
            {
                if (_sorted != value)
                {
                    _sorted = value;

                    OnPropertyChanged("sorted");
                }
            }
        }

        
        private bool _labelEdit = false;
        /// <summary>
        /// Gets or sets a value indicating whether the label text of the tree nodes can be edited.
        /// </summary>
        /// <value>
        /// true if the label text of the tree nodes can be edited; otherwise, false. The default is false.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Appearance")]
        public bool LabelEdit
        {
            get { return _labelEdit; }
            set
            {
                if (_labelEdit != value)
                {
                    _labelEdit = value;

                    OnPropertyChanged("labelEdit");
                }
            }
        }


        
        private int _indent = 19;
        /// <summary>
        /// Gets or sets the distance to indent each child tree node level.
        /// </summary>
        /// <value>
        /// The distance, in pixels, to indent each child tree node level. The default value is 19.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(19)]
        [Category("Appearance")]
        public int Indent 
        {
            get { return _indent; }
            set
            {
                if (_indent != value)
                {
                    _indent = value;

                    OnPropertyChanged("Indent");
                }
            }
        }


        /// <summary>
        /// Gets the navigated element.
        /// </summary>
        /// <value>
        /// The navigated element.
        /// </value>
        IClientHistoryElement IClinetHistoryContainerElement.NavigatedElement
        {
            get
            {
                return this.SelectedItem;
            }
        }


        private string _parentFieldName = "";
        /// <summary>
        /// Gets or sets the field used as an identifier of a parent record within a data source. 
        /// </summary>
        /// <value>
        /// The field name
        /// </value>
        public string ParentFieldName
        {
            get { return _parentFieldName; }
            set
            {
                if (_parentFieldName != value)
                {
                    _parentFieldName = value;
                }
            }
        }


        private string _keyFieldName = "";
        /// <summary>
        /// Gets or sets the name of the field used as the unique record identifier. 
        /// </summary>
        /// <value>
        /// The field name
        /// </value>
        public string KeyFieldName
        {
            get { return _keyFieldName; }
            set
            {
                if (_keyFieldName != value)
                {
                    _keyFieldName = value;
                }
            }
        }

    }
}
