﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// GridColumnData data to store previousDisplayIndex , newDisplayIndex .
    /// </summary>
    public class GridRowData
    {

      //  private int _rowIndex = -1;


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData" /> class.
        /// </summary>
        public GridRowData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData"/> class.
        /// </summary>
        /// <param name="previousDisplayIndex"> previous DisplayIndex. </param>
        /// <param name="?">new DisplayIndex.</param>
        public GridRowData(int rowIndex)
        {
            RowIndex = rowIndex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData"/> class.
        /// </summary>
        /// <param name="previousDisplayIndex"> previous DisplayIndex. </param>
        /// <param name="?">new DisplayIndex.</param>
        public GridRowData(int rowIndex, int columnIndex)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
        }
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="visibility">if set to <c>true</c> [visibility].</param>
        public GridRowData(int rowIndex, bool visibility)
        {
            RowIndex = rowIndex;
            RowVisibility = visibility;
        }

      

        /// <summary>
        /// Get,Set the Row Index.
        /// </summary>
        public int RowIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the row visibility.
        /// </summary>
        public bool RowVisibility
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the Row Index.
        /// </summary>
        public int ColumnIndex
        {
            get;
            private set;
        }
    }
}
