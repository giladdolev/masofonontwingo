﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    public class TimeEditElement : TextBoxElement
    {
        public const string MAX_TIME_FOEMAT = "h:mm tt";
        /// <summary>
        /// The maximum allowed time.
        /// </summary>
        private DateTime _maxTime = DateTime.MaxValue;


        /// <summary>
        /// The minimum allowed time.
        /// </summary>
        private DateTime _minTime = DateTime.MinValue;


        /// <summary>
        /// The number of minutes between each time value in the list.
        /// </summary>
        private int _increment;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeEditElement"/> class.
        /// </summary>
        public TimeEditElement()
        {

        }


        /// <summary>
        /// increment Description.
        /// </summary>
        /// <value>
        /// The increment.
        /// </value>
        [RendererPropertyDescription]
        public int Increment
        {
            get
            {
                return _increment;
            }
            set
            {
                if (_increment != value)
                {
                    _increment = value;

                    OnPropertyChanged("Increment");
                }
            }
        }



        /// <summary>
        /// minValue Description.
        /// </summary>
        /// <value>
        /// The minimum time.
        /// </value>
        [RendererPropertyDescription]
        public DateTime MinTime
        {
            get
            {
                return _minTime;
            }
            set
            {
                if (_minTime != value)
                {
                    _minTime = value;

                    OnPropertyChanged("MinTime");
                }
            }
        }



        /// <summary>
        /// maxValue Description.
        /// </summary>
        /// <value>
        /// The maximum time.
        /// </value>
        [RendererPropertyDescription]
        public DateTime MaxTime
        {
            get
            {
                return _maxTime;
            }
            set
            {
                if (_maxTime != value)
                {
                    _maxTime = value;

                    OnPropertyChanged("MaxTime");
                }
            }
        }


      
    }
}
