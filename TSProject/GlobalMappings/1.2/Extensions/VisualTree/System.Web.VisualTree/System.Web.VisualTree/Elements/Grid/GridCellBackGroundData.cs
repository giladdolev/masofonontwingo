﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridCellBackGroundData : GridIndexesData
    {

        private Color color;
        private ResourceReference _image;

        /// <summary>
        /// initializes an instance of Indexes
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="col">The col.</param>
        /// <param name="color">The color.</param>
        public GridCellBackGroundData(int row, int col, Color color)
        {
            RowIndex = row;
            ColumnIndex = col;
            this.color = color;
        }

        /// <summary>
        /// initializes an instance of Indexes
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="image">The image.</param>
        public GridCellBackGroundData(int row, ResourceReference image)
        {
            RowIndex = row;
            ColumnIndex = 0;
            BackGroundImage = image;
        }

        /// <summary>
        /// initializes an instance of Indexes
        /// </summary>
        /// <param name="image"></param>
        public GridCellBackGroundData(ResourceReference image)
        {
            RowIndex = 0;
            ColumnIndex = 0;
            BackGroundImage = image;
        }
        /// <summary>
        /// Gets or sets the background color.
        /// </summary>
        /// <value>
        /// The background color.
        /// </value>
        public Color BackgroundColor
        {
            get { return color; }
            set
            {
                color = value;
            }
        }

        /// <summary>
        /// Gets or sets the background color.
        /// </summary>
        /// <value>
        /// The background color.
        /// </value>
        public ResourceReference BackGroundImage
        {
            get { return _image; }
            set
            {
                _image = value;
            }
        }


    }
}