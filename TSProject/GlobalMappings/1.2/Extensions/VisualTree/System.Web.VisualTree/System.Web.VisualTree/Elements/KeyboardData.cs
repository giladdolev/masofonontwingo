﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class KeyboardData
    {
        private string _inputField;
        private string _keyboardId;

        public KeyboardData(string inputField, string KeyboardId)
        {
            this._inputField = inputField;
            this._keyboardId = KeyboardId;
        }

        public string KeyboardId
        {
            get
            {
                return _keyboardId;
            }
            set
            {
                _keyboardId = value;
            }
        }

        public string InputField
        {
            get
            {
                return _inputField;
            }
            set
            {
                _inputField = value; 
            }
        }
    }
}
