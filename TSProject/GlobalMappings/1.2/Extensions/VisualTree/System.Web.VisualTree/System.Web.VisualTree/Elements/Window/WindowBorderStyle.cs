﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Window border style
    /// </summary>
    public enum WindowBorderStyle
    {
        None,
        FixedSingle,
        FixedDouble,
        FixedDialog,
        Sizable,
        FixedToolWindow,
        SizableToolWindow
    }
}
