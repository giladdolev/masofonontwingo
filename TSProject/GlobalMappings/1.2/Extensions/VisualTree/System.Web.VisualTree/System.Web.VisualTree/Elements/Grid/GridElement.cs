using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Web.VisualTree.Common.Attributes;

using System.Web.UI;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for grid element cell related events
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
    public delegate void GridElementCellEventHandler(object sender, GridElementCellEventArgs e);


    /// <summary>
    /// Provides support for grid element selection changed events
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="GridElementSelectionChangeEventArgs"/> instance containing the event data.</param>
    public delegate void GridElementSelectionChangeEventHandler(object sender, GridElementSelectionChangeEventArgs e);

    /// <summary>
    /// Displays data in a customizable grid
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ScrollableControl" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModel" />
    public class GridElement : ScrollableControl, IBindingViewModel
    {
        private bool _readOnly = false;
        internal int _selectedRowIndex = -1;
        internal int _selectedColumnIndex = -1;
        private bool _allowUserToAddRows = true;
        private bool _allowUserToDeleteRows = true;
        private bool _allowUserToResizeColumns = true;
        private bool allowUserToOrderColumns;
        private bool _allowUserToResizeRows = true;
        private bool _showAutoFilterColumnItem = false;
        private int mintColumnCount;
        private int _firstDisplayedScrollingRowIndex;
        private bool _columnHeadersVisible = true;
        private int _rowHeadersWidth = 43;
        private GridRowCollection _rows;
        private GridHeaderBorderStyle _rowHeadersBorderStyle;
        private GridColumnHeadersHeightSizeMode _columnHeadersHeightSizeMode = GridColumnHeadersHeightSizeMode.EnableResizing;
        private GridCellStyle _defaultCellStyle = new GridCellStyle();
        private GridRow _rowTemplate;
        private readonly GridColumnCollection _columns;
        private readonly GridColumnCollection _headerColumns;
        private MenuElement _contextMenu;
        private bool _autoGenerateColumns = true;
        private bool _canSelect = true;
        private bool _allowTrackMouseOver = true;
        internal bool _cancelEndEditEvent = false;
        private GridCellStyle _rowsDefaultCellStyle;
        private ScrollBars _scrollBars = ScrollBars.Both;
        private IList<GridCellElement> _selectedCells;
        private IList<GridRow> _selectedRows;
        private IList<GridColumn> _selectedColumns;
      //  private SortOrder _sortOrder;
        private ResourceReference _rowHeaderCornerIcon;
        private Dictionary<int, string> _columnHtmlContent = new Dictionary<int, string>();
        internal bool _itemChangedRaised = false;
        internal bool _selectionEventsRaised = false;


        // show column lines yes/no
        private int _maxRows = Int32.MaxValue;
        private FilterInfo _columnFilterInfo;

        private CellSelectionMode _selectionMode = CellSelectionMode.Row;


        /// <summary>
        /// Initializes a new instance of the <see cref="GridElement"/> class.
        /// </summary>
        public GridElement()
        {
            _columns = new GridColumnCollection(this, "$refresh");
            _headerColumns = new GridColumnCollection(this, "HeaderColumns");
            _rows = new GridRowCollection(this);
            ColData = new Dictionary<int, object>();
            RowData = new Dictionary<int, object>();
            SelectedRows = new List<GridRow>();
            SelectedCells = new List<GridCellElement>();
            SelectedColumns = new List<GridColumn>();
            ComboBoxList = new Dictionary<string, string>();
            TextButtonList = new List<string>();
            SetRowHeader();
            ClientCellEndEdit = "editor.record.commit();";
            ClientSelectionChanged = "";
        }


        /// <summary>
        /// CellDoubleClick event handler.
        /// </summary>
        private event GridElementCellEventHandler _CellDoubleClick;

        /// <summary>
        /// Add or remove CellDoubleClick action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler CellDoubleClick
        {
            add
            {
                bool needNotification = (_CellDoubleClick == null);

                _CellDoubleClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellDoubleClick");
                }

            }
            remove
            {
                _CellDoubleClick -= value;

                if (_CellDoubleClick == null)
                {
                    OnEventHandlerDeattached("CellDoubleClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellDoubleClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellDoubleClickListeners
        {
            get { return _CellDoubleClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CellDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        protected virtual void OnCellDoubleClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CellDoubleClick != null)
            {
                _CellDoubleClick(this, args);
            }
        }

        /// <summary>
        /// Updates the cell value
        /// </summary>
        /// <param name="rowIndex">Row index</param>
        /// <param name="columnIndex">Cell Index</param>
        /// <param name="value">Cell value</param>
        [RendererMethodDescription]
        public void UpdateCellValue(int rowIndex, int columnIndex, string value)
        {
            this.InvokeMethod("UpdateCellValue", new ArrayList() { rowIndex, columnIndex, value });
        }


        /// <summary>
        /// Performs the <see cref="E:CellDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellDoubleClick(GridElementCellEventArgs args)
        {

            // Raise the CellDoubleClick event.
            OnCellDoubleClick(args);
        }


        /// <summary>
        /// CellButtonClick event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _CellButtonClick;

        /// <summary>
        /// Add or remove CellButtonClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridElementCellEventArgs> CellButtonClick
        {
            add
            {
                bool needNotification = (_CellButtonClick == null);

                _CellButtonClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellButtonClick");
                }

            }
            remove
            {
                _CellButtonClick -= value;

                if (_CellButtonClick == null)
                {
                    OnEventHandlerDeattached("CellButtonClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellButtonClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has CellButtonClick listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellButtonClickListeners
        {
            // Must renderer this event cause we need to save some data of the current row 
            get { return _CellButtonClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CellButtonClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellButtonClick"/> instance containing the event data.</param>
        protected virtual void OnCellButtonClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CellButtonClick != null)
            {
                _CellButtonClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellButtonClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCellButtonClick(EventArgs args)
        {
            GridElementCellEventArgs newArgs = new GridElementCellEventArgs(this.SelectedRowIndex, this.SelectedColumnIndex, this.SelectedRowIndex, this.SelectedColumnIndex);

            // Raise the CellButtonClick event.
            OnCellButtonClick(newArgs);



        }

        /// <summary>
        /// Occurs when [cell leave].
        /// </summary>
        public event GridElementCellEventHandler CellLeave;

        /// <summary>
        /// SelectionChanged event handler.
        /// </summary>
        private event GridElementSelectionChangeEventHandler _SelectionChanged;


        /// <summary>
        /// Add or remove SelectionChanged action.
        /// </summary>
        [RendererEventDescription]
        public virtual event GridElementSelectionChangeEventHandler SelectionChanged
        {
            add
            {
                bool needNotification = (_SelectionChanged == null);

                _SelectionChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectionChanged");
                }

            }
            remove
            {
                _SelectionChanged -= value;

                if (_SelectionChanged == null)
                {
                    OnEventHandlerDeattached("SelectionChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectionChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasSelectionChangedListeners
        {
            get { return (this._SelectionChanged != null || this.RowHeaderVisible == true); }
        }


        /// <summary>
        /// Raises the <see cref="E:SelectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectionChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectionChanged(GridElementSelectionChangeEventArgs args)
        {

            // Check if there are listeners.
            if (_SelectionChanged != null)
            {
                _SelectionChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformSelectionChanged(GridElementSelectionChangeEventArgs args)
        {
            if (this.PreviousRow != null && this.PreviousRow.Index == args.RowIndex)
            {
                return;
            }
            if (_itemChangedRaised == true && this.PreviousRow != null && this.PreviousRow.Index == args.RowIndex && _selectionEventsRaised == true)
            {
                return;
            }
            _selectionEventsRaised = true;
            // Start before selection sequence. 
            PerformBeforeSelectionChanged(args);


            // Check for possible user cancellation
            if (!args.Cancel)
            {
                if (PreviousColumn != null && PreviousColumn is GridCheckBoxColumn)
                {
                    // update selected row index and column index for cell end edit event in case of checkbox
                    this._selectedRowIndex = this.PreviousRow.Index;
                    this._selectedColumnIndex = this.PreviousColumn.Index;
                    if (this.PreviousRow != null && (PreviousRow.Index >= 0 && PreviousRow.Index < Rows.Count))
                    {
                        string value = Convert.ToString(Rows[PreviousRow.Index].Cells[PreviousColumn.Index].Value);
                        ValueChangedArgs<string> newArgs = new ValueChangedArgs<string>(value);
                        if (newArgs != null)
                        {
                            this.PerformCellEndEdit(newArgs);
                        }
                    }
                }
                
                if (args.SelectedRows != null && args.SelectedRows.Count == 1)
                {
                    if (IsValidIndexes(_selectedColumnIndex, _selectedRowIndex))
                    {
                        //Update previous row,col
                        this.PreviousRow = Rows[_selectedRowIndex];
                        this.PreviousColumn = Columns[_selectedColumnIndex];
                    }
                    else
                    {
                        this.PreviousRow = null;
                        this.PreviousColumn = null;
                    }

                    // Update indexes for single row selection .
                    this._selectedColumnIndex = args.ColumnIndex;
                    this._selectedRowIndex = args.RowIndex;
                }

                // Update CurrentCell .

                if (IsValidIndexes(_selectedColumnIndex, _selectedRowIndex))
                {
                    this._currentCell = this.Rows[_selectedRowIndex].Cells[_selectedColumnIndex];
                }

                SetSelectedRecords(args.SelectedRows, args.SelectedCells);

                // Raise the SelectionChanged event.
                OnSelectionChanged(args);
            }
            else
            {
                // id the cancel set to true do not change the selectedRowIndex and selectedColumnIndex .
                this._selectedRowIndex = (this.PreviousRow == null) ? -1 : this.PreviousRow.Index;
                this._selectedColumnIndex = (this.PreviousColumn == null) ? -1 : this.PreviousColumn.Index;
            }
            // Check for possible user cancellation
            if (!args.Cancel)
            {
                GridElementCellEventArgs gridElementCellEventArgs =
                    new GridElementCellEventArgs(this._selectedColumnIndex, this._selectedRowIndex);

                // Process currentCellChanged and CellBeginEdit
                PerformCurrentCellChanged(gridElementCellEventArgs);
                PerformCellBeginEdit(gridElementCellEventArgs);
                _cancelEndEditEvent = gridElementCellEventArgs.Cancel;
            }
        }
        internal virtual void PerformSelectionChangedInternal(GridElementSelectionChangeEventArgs args)
        {
            if (this.PreviousRow != null && this.PreviousRow.Index == args.RowIndex)
            {
                return;
            }
            if (_itemChangedRaised == true && this.PreviousRow != null && 
                this.PreviousRow.Index == args.RowIndex && _selectionEventsRaised == true)
            {
                return;
            }
            _selectionEventsRaised = true;

            if (PreviousColumn != null && PreviousColumn is GridCheckBoxColumn)
            {
                // update selected row index and column index for cell end edit event in case of checkbox
                this._selectedRowIndex = this.PreviousRow.Index;
                this._selectedColumnIndex = this.PreviousColumn.Index;
            }

            if (args.SelectedRows != null && args.SelectedRows.Count == 1)
            {
                if (IsValidIndexes(_selectedColumnIndex, _selectedRowIndex))
                {
                    //Update previous row,col
                    this.PreviousRow = Rows[_selectedRowIndex];
                    this.PreviousColumn = Columns[_selectedColumnIndex];
                }
                else
                {
                    this.PreviousRow = null;
                    this.PreviousColumn = null;
                }

                // Update indexes for single row selection .
                this._selectedColumnIndex = args.ColumnIndex;
                this._selectedRowIndex = args.RowIndex;
            }

            // Update CurrentCell .

            if (IsValidIndexes(_selectedColumnIndex, _selectedRowIndex))
            {
                this._currentCell = this.Rows[_selectedRowIndex].Cells[_selectedColumnIndex];
            }

        }
        /// <summary>
        /// Fill selectedCells, selectedRows , SelectedClumns
        /// </summary>
        /// <param name="selectedRows">The selected rows.</param>
        /// <param name="selectedCells">The selected cells.</param>
        internal void SetSelectedRecords(IEnumerable<Int32> selectedRows, IEnumerable<GridIndexesData> selectedCells)
        {
            // Clear selected rows/columns/cells collections
            ClearSelection();
            if (selectedRows != null)
            {
                // Fill selected rows
                foreach (var item in selectedRows)
                {
                    if (item > 0 && item < this.RowCount)
                    {
                        SelectedRows.Add(this.Rows[item]);
                    }
                }

            }
            IList<Int32> columns = new List<Int32>();

            if (selectedCells != null)
            {
                // Fill selected cells
                foreach (var item in selectedCells)
                {
                    if (IsValidIndexes(item.ColumnIndex, item.RowIndex))
                    {
                        // Fill selected cells 
                        SelectedCells.Add(this.Rows[item.RowIndex].Cells[item.ColumnIndex]);

                        if (!columns.Contains(item.ColumnIndex))
                        {
                            SelectedColumns.Add(this.Columns[item.ColumnIndex]);
                            columns.Add(item.ColumnIndex);
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Before SelectionChanged event handler.
        /// </summary>
        private event GridElementSelectionChangeEventHandler _BeforeSelectionChanged;


        /// <summary>
        /// Add or remove BeforeSelectionChanged action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementSelectionChangeEventHandler BeforeSelectionChanged
        {
            add
            {
                bool needNotification = (_BeforeSelectionChanged == null);

                _BeforeSelectionChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeSelectionChanged");
                }

            }
            remove
            {
                _BeforeSelectionChanged -= value;

                if (_BeforeSelectionChanged == null)
                {
                    OnEventHandlerDeattached("BeforeSelectionChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeSelectionChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeSelectionChangedListeners
        {
            get { return _BeforeSelectionChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeSelectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeSelectionChanged"/> instance containing the event data.</param>
        protected virtual void OnBeforeSelectionChanged(GridElementSelectionChangeEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeSelectionChanged != null)
            {
                _BeforeSelectionChanged(this, args);
            }
        }



        /// <summary>
        /// Performs the <see cref="E:BeforeSelectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeSelectionChanged(GridElementSelectionChangeEventArgs args)
        {

            // Raise the BeforeSelectionChanged event.
            OnBeforeSelectionChanged(args);
        }

        /// <summary>
        /// ColumnHeaderMouseClick event handler.
        /// </summary>
        private event GridElementCellEventHandler _ColumnHeaderMouseClick;

        /// <summary>
        /// Add or remove ColumnHeaderMouseClick action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler ColumnHeaderMouseClick
        {
            add
            {
                bool needNotification = (_ColumnHeaderMouseClick == null);

                _ColumnHeaderMouseClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnHeaderMouseClick");
                }

            }
            remove
            {
                _ColumnHeaderMouseClick -= value;

                if (_ColumnHeaderMouseClick == null)
                {
                    OnEventHandlerDeattached("ColumnHeaderMouseClick");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:ColumnHeaderMouseClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        protected virtual void OnColumnHeaderMouseClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_ColumnHeaderMouseClick != null)
            {
                _ColumnHeaderMouseClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:GridElementCellEventArgs" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformColumnHeaderMouseClick(GridElementCellEventArgs args)
        {
            if (args != null)
            {
                if (args.ColumnIndex > -1)
                {
                    // Update the selected column index .
                    _selectedColumnIndex = args.ColumnIndex;
                }
            }
            // Raise the ColumnHeaderMouseClick event.
            OnColumnHeaderMouseClick(args);
        }


        /// <summary>
        /// CellContentClick event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _CellContentClick;

        /// <summary>
        /// Add or remove CellContentClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridElementCellEventArgs> CellContentClick
        {
            add
            {
                bool needNotification = (_CellContentClick == null);

                _CellContentClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellContentClick");
                }

            }
            remove
            {
                _CellContentClick -= value;

                if (_CellContentClick == null)
                {
                    OnEventHandlerDeattached("CellContentClick");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CellContentClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellContentClick"/> instance containing the event data.</param>
        protected virtual void OnCellContentClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CellContentClick != null)
            {
                _CellContentClick(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellContentClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellContentClickListeners
        {
            get { return _CellContentClick != null; }
        }

        /// <summary>
        /// Performs the <see cref="E:CellContentClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCellContentClick(GridElementCellEventArgs args)
        {

            // Raise the CellContentClick event.
            OnCellContentClick(args);
        }

        /// <summary>
        /// CellFormatting event handler.
        /// </summary>
        private event EventHandler _CellFormatting;

        /// <summary>
        /// Add or remove CellFormatting action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler CellFormatting
        {
            add
            {
                bool needNotification = (_CellFormatting == null);

                _CellFormatting += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellFormatting");
                }

            }
            remove
            {
                _CellFormatting -= value;

                if (_CellFormatting == null)
                {
                    OnEventHandlerDeattached("CellFormatting");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellFormatting listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellFormattingListeners
        {
            get { return _CellFormatting != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CellFormatting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellFormatting"/> instance containing the event data.</param>
        protected virtual void OnCellFormatting(EventArgs args)
        {

            // Check if there are listeners.
            if (_CellFormatting != null)
            {
                _CellFormatting(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellFormatting" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCellFormatting(EventArgs args)
        {

            // Raise the CellFormatting event.
            OnCellFormatting(args);
        }


        /// <summary>
        /// ColumnStateChanged event handler.
        /// </summary>
        private event EventHandler _ColumnStateChanged;

        /// <summary>
        /// Add or remove ColumnStateChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler ColumnStateChanged
        {
            add
            {
                bool needNotification = (_ColumnStateChanged == null);

                _ColumnStateChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnStateChanged");
                }

            }
            remove
            {
                _ColumnStateChanged -= value;

                if (_ColumnStateChanged == null)
                {
                    OnEventHandlerDeattached("ColumnStateChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ColumnStateChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasColumnStateChangedListeners
        {
            get { return _ColumnStateChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ColumnStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ColumnStateChanged"/> instance containing the event data.</param>
        protected virtual void OnColumnStateChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_ColumnStateChanged != null)
            {
                _ColumnStateChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ColumnStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformColumnStateChanged(EventArgs args)
        {

            // Raise the ColumnStateChanged event.
            OnColumnStateChanged(args);
        }


        /// <summary>
        /// DataBindingComplete event handler.
        /// </summary>
        private event EventHandler _DataBindingComplete;

        /// <summary>
        /// Add or remove DataBindingComplete action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler DataBindingComplete
        {
            add
            {
                bool needNotification = (_DataBindingComplete == null);

                _DataBindingComplete += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DataBindingComplete");
                }

            }
            remove
            {
                _DataBindingComplete -= value;

                if (_DataBindingComplete == null)
                {
                    OnEventHandlerDeattached("DataBindingComplete");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DataBindingComplete listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDataBindingCompleteListeners
        {
            get { return _DataBindingComplete != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DataBindingComplete" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DataBindingComplete"/> instance containing the event data.</param>
        protected virtual void OnDataBindingComplete(EventArgs args)
        {

            // Check if there are listeners.
            if (_DataBindingComplete != null)
            {
                _DataBindingComplete(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:DataBindingComplete" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDataBindingComplete(EventArgs args)
        {

            // Raise the DataBindingComplete event.
            OnDataBindingComplete(args);
        }


        /// <summary>
        /// AddedColumn event handler.
        /// </summary>
        private event EventHandler<ValueChangedArgs<int>> _AddedColumn;

        /// <summary>
        /// Add or remove AddedColumn action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ValueChangedArgs<int>> AddedColumn
        {
            add
            {
                bool needNotification = (_AddedColumn == null);

                _AddedColumn += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AddedColumn");
                }

            }
            remove
            {
                _AddedColumn -= value;

                if (_AddedColumn == null)
                {
                    OnEventHandlerDeattached("AddedColumn");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AddedColumn listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAddedColumnListeners
        {
            get { return _AddedColumn != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AddedColumn" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AddedColumn" /> instance containing the event data.</param>
        protected virtual void OnAddedColumn(ValueChangedArgs<int> args)
        {

            // Check if there are listeners.
            if (_AddedColumn != null)
            {
                _AddedColumn(this, args);
            }
            foreach (GridRow item in this.Rows)
            {
                if (item.Cells.Count < this.ColumnCount)
                {
                    // Create header cell
                    GridContentCell cellElement = new GridContentCell();

                    item.Cells.Add(cellElement);
                }
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AddedColumn" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void PerformAddedColumn(ValueChangedArgs<int> args)
        {

            // Update display index of the columns
            if (this.Columns.Count > 0)
            {
                if (this.Columns[this.ColumnCount - 1].Visible)
                {
                    this.Columns[this.ColumnCount - 1].DisplayIndex = this.Columns.IndexOf(this.Columns[this.ColumnCount - 1]);
                }
            }
            // Raise the AddedColumn event.
            OnAddedColumn(args);
        }


        /// <summary>
        /// CurrentCellChanged event handler.
        /// </summary>
        private event GridElementCellEventHandler _CurrentCellChanged;

        /// <summary>
        /// Add or remove CurrentCellChanged action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler CurrentCellChanged
        {
            add
            {
                bool needNotification = (_CurrentCellChanged == null);

                _CurrentCellChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CurrentCellChanged");
                }

            }
            remove
            {
                _CurrentCellChanged -= value;

                if (_CurrentCellChanged == null)
                {
                    OnEventHandlerDeattached("CurrentCellChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CurrentCellChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCurrentCellChangedListeners
        {
            get { return false; }
        }

        /// <summary>
        /// Raises the <see cref="E:CurrentCellChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CurrentCellChanged"/> instance containing the event data.</param>
        protected virtual void OnCurrentCellChanged(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CurrentCellChanged != null)
            {
                _CurrentCellChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CurrentCellChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridCellEventArgs"/> instance containing the event data.</param>
        public void PerformCurrentCellChanged(GridElementCellEventArgs args)
        {

            // Raise the CurrentCellChanged event.
            OnCurrentCellChanged(args);
        }






        /// <summary>
        /// Set row header in spreadsheet mode.
        /// </summary>
        private void SetRowHeader()
        {
            if (this.Columns.All(clm => (clm.GetType() != typeof(GridRowHeader))))
            {
                if (this.RowHeaderVisible)
                {
                    this.EnableLocking = true;
                    // Add RowHeaderColumns .
                    GridRowHeader _rowHeader = new GridRowHeader();
                    this.HeaderColumns.Add(_rowHeader);
                }
            }
        }



        private string _ClientRowTemplate = "";
        /// <summary>
        /// Gets or sets the ClientRowTemplate.
        /// </summary>
        /// <value>
        /// The ClientRowTemplate.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue("")]
        [Category("Appearance")]
        public string ClientRowTemplate
        {
            get { return _ClientRowTemplate; }
            set
            {
                if (_ClientRowTemplate != value)
                {
                    _ClientRowTemplate = value;
                }
            }
        }



        /// <summary>
        /// Gets or sets the context menu x position.
        /// </summary>
        /// <value>
        /// The context menu x position.
        /// </value>
        public int contextMenuXPosition { get; set; }


        /// <summary>
        /// Gets or sets the context menu y position.
        /// </summary>
        /// <value>
        /// The context menu y position.
        /// </value>
        public int contextMenuYPosition { get; set; }

        /// <summary>
        /// Gets or sets the context menu.
        /// </summary>
        /// <value>
        /// The context menu.
        /// </value>
        [IDReferenceProperty]
        public MenuElement ContextMenu
        {
            get { return this._contextMenu; }
            set
            {
                this._contextMenu = value;
            }
        }


        /// <summary>
        /// Refreshes the data.
        /// </summary>
        public void RefreshData()
        {
            // If is valid 
            if (IsLoadedToClient)
            {
                this.OnPropertyChanged("DataSource");
            }
        }



        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<GridColumn>))]
        public GridColumnCollection Columns
        {
            get { return _columns; }
        }


        /// <summary>
        /// Gets the header columns.
        /// </summary>
        /// <value>
        /// The header columns.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<GridColumn>))]
        public GridColumnCollection HeaderColumns
        {
            get { return _headerColumns; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the user can edit the cells of the GridElement control.
        /// </summary>
        public bool ReadOnly
        {
            get { return _readOnly; }
            set
            {
                if (_readOnly != value)
                {
                    _readOnly = value;
                    ChangeColumnsReadOnly(_readOnly);
                }
            }
        }

        /// <summary>
        /// Change the columns readOnly property state 
        /// </summary>
        /// <param name="readOnly">new readOnly property state</param>
        private void ChangeColumnsReadOnly(bool readOnly)
        {
            if (this.Columns != null)
            {
                foreach (GridColumn column in this.Columns)
                {
                    column.ReadOnly = readOnly;
                }
            }
        }



        /// <summary>
        /// Gets or sets the row template.
        /// </summary>
        /// <value>
        /// The row template.
        /// </value>
        [DesignerIgnore]
        public GridRow RowTemplate
        {
            get
            {
                _rowTemplate = _rowTemplate ?? new GridRow();
                return _rowTemplate;
            }
            set
            {
                if (value != null)
                {
                    _rowTemplate = value;
                    if (_rowTemplate.ParentElement == null)
                    {
                        _rowTemplate.ParentElement = this;
                    }
                    // Notify property RowTemplate changed
                    OnPropertyChanged("RowTemplate");
                }
            }
        }



        /// <summary>
        /// Gets or sets the default cell style.
        /// </summary>
        /// <value>
        /// The default cell style.
        /// </value>
        [DesignerIgnore]
        public GridCellStyle DefaultCellStyle
        {
            get { return _defaultCellStyle; }
            set
            {
                if (value != null)
                {
                    _defaultCellStyle = value;

                    // Notify property DefaultCellStyle changed
                    OnPropertyChanged("DefaultCellStyle");
                }
            }
        }



        /// <summary>
        /// Gets or sets a value indicating whether [allow user to add rows].
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow user to add rows]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AllowUserToAddRows
        {
            get { return _allowUserToAddRows; }
            set
            {
                _allowUserToAddRows = value;

                // Notify property AllowUserToAddRows changed
                OnPropertyChanged("AllowUserToAddRows");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this grid is grouping grid.
        /// </summary>
        /// <value>
        /// <c>true</c> if this grid is grouping grid; otherwise, <c>false</c>.
        /// </value>
        public bool IsGroupingGrid { get; set; }



        /// <summary>
        /// Gets or sets a value indicating whether [allow user to delete rows].
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow user to delete rows]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AllowUserToDeleteRows
        {
            get { return _allowUserToDeleteRows; }
            set
            {
                _allowUserToDeleteRows = value;

                // Notify property AllowUserToDeleteRows changed
                OnPropertyChanged("AllowUserToDeleteRows");
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether to allow user to resize columns.
        /// </summary>
        /// <value>
        ///   <c>true</c> if allow user to resize columns; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AllowUserToResizeColumns
        {
            get { return _allowUserToResizeColumns; }
            set
            {
                _allowUserToResizeColumns = value;
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether to allow user to order columns.
        /// </summary>
        /// <value>
        /// <c>true</c> if allow user to order columns; otherwise, <c>false</c>.
        /// </value>
        public bool AllowUserToOrderColumns
        {
            get { return allowUserToOrderColumns; }
            set
            {
                allowUserToOrderColumns = value;
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether [allow user to resize rows].
        /// </summary>
        /// <value>
        /// <c>true</c> if [allow user to resize rows]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AllowUserToResizeRows
        {
            get { return _allowUserToResizeRows; }
            set
            {
                _allowUserToResizeRows = value;

                // Notify property AllowUserToResizeRows changed
                OnPropertyChanged("AllowUserToResizeRows");
            }
        }



        /// <summary>
        /// Gets or sets the column headers height size mode.
        /// </summary>
        /// <value>
        /// The column headers height size mode.
        /// </value>
        [DefaultValue(GridColumnHeadersHeightSizeMode.EnableResizing)]
        public GridColumnHeadersHeightSizeMode ColumnHeadersHeightSizeMode
        {
            get { return _columnHeadersHeightSizeMode; }
            set
            {
                _columnHeadersHeightSizeMode = value;

                // Notify property ColumnHeadersHeightSizeMode changed
                OnPropertyChanged("ColumnHeadersHeightSizeMode");
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether [automatic generate columns].
        /// </summary>
        /// <value>
        /// <c>true</c> if [automatic generate columns]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AutoGenerateColumns
        {
            get { return _autoGenerateColumns; }
            set
            {
                _autoGenerateColumns = value;
            }
        }




        /// <summary>
        /// Gets or sets the column count.
        /// </summary>
        /// <value>
        /// The column count.
        /// </value>
        public int ColumnCount
        {
            get
            {
                return this.Columns.Count;
            }
            set
            {
                mintColumnCount = value;

                // Notify property ColumnCount changed
                // OnPropertyChanged("ColumnCount");
                ColumnCountChange(value);
            }
        }


        /// <summary>
        /// Changes column count.
        /// </summary>
        /// <param name="columnCount">number of columns</param>
        public void ColumnCountChange(int columnCount)
        {
            int iter = Math.Abs(columnCount - this.Columns.Count);
            if (columnCount < this.Columns.Count)
            {
                for (int i = 0; i < iter; i++)
                {
                    this.Columns.RemoveAt(this.Columns.Count - 1);
                }
            }
            else if (columnCount > this.Columns.Count)
            {
                for (int i = 1; i <= iter; i++)
                {
                    this.Columns.Add(new GridColumn());
                }
            }
        }

        /// <summary>
        /// Gets or sets the current row.
        /// </summary>
        /// <value>
        /// The current row.
        /// </value>
        [DesignerIgnore]
        public GridRow CurrentRow
        {
            get
            {
                return (_selectedRowIndex >= 0 && _selectedRowIndex < _rows.Count) ? _rows[_selectedRowIndex] : null;
            }
            set
            {
                // get indexes of selected rows.
                List<int> lstRows = new List<int>() { Rows.IndexOf(value) };
                // get selected cells .
                List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(Rows.IndexOf(value), this.SelectedColumnIndex) };
                // build the new args 
                GridElementSelectionChangeEventArgs selectionChangeEventArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);
                //Raise selection changed event .
                this.PerformSelectionChanged(selectionChangeEventArgs);

                // Notify property CurrentRow changed.
                SetSelection();

            }
        }

        /// <summary>
        /// Gets or sets the current column.
        /// </summary>
        /// <value>
        /// The current column.
        /// </value>
        [DesignerIgnore]
        public GridColumn CurrentColumn
        {
            get
            {
                return (_selectedColumnIndex >= 0 && _selectedColumnIndex < _columns.Count) ? _columns[_selectedColumnIndex] : null;
            }
        }

        /// <summary>
        /// Gets or sets the previous column.
        /// </summary>
        /// <value>
        /// The previous column.
        /// </value>
        [DesignerIgnore]
        public GridColumn PreviousColumn { get; internal set; }

        /// <summary>
        /// Gets or sets the previous column.
        /// </summary>
        /// <value>
        /// The previous column.
        /// </value>
        [DesignerIgnore]
        public GridRow PreviousRow { get; internal set; }

        /// <summary>
        /// Select row from grid element
        /// </summary>
        [RendererMethodDescription]
        private void SetSelection()
        {
            if (this.CurrentRow != null)
            {
                this.InvokeMethodOnce("SetSelection");
            }
        }

        internal GridCellElement _currentCell = new GridContentCell { ColumnIndex = 0, RowIndex = 0 };
        /// <summary>
        /// Gets or sets the current row.
        /// </summary>
        /// <value>
        /// The current row.
        /// </value>
        [DesignerIgnore]
        public GridCellElement CurrentCell
        {
            get { return _currentCell; }
            set
            {
                _currentCell = value;

            }
        }

        private bool _useFixedHeaders = false;

        /// <summary>
        /// Gets or sets Fixed Headers functionality.
        /// </summary>
        [RendererPropertyDescription]
        public bool UseFixedHeaders
        {
            get
            {
                return _useFixedHeaders;
            }
            set
            {
                _useFixedHeaders = value;
                OnPropertyChanged("UseFixedHeaders");
            }
        }

        /// <summary>
        /// Gets or sets the first index of the displayed scrolling row.
        /// </summary>
        /// <value>
        /// The first index of the displayed scrolling row.
        /// </value>
        public int FirstDisplayedScrollingRowIndex
        {
            get { return _firstDisplayedScrollingRowIndex; }
            set
            {
                _firstDisplayedScrollingRowIndex = value;

                // Notify property FirstDisplayedScrollingRowIndex changed
                OnPropertyChanged("FirstDisplayedScrollingRowIndex");
            }
        }




        /// <summary>
        /// Gets or sets the row count.
        /// </summary>
        /// <value>
        /// The row count.
        /// </value>
        public int RowCount
        {
            get { return this.Rows.Count; }
            set
            {
                RowCountChange(value);
            }
        }


        /// <summary>
        /// Changes rows count.
        /// </summary>
        /// <param name="rowCount">The row count.</param>
        public void RowCountChange(int rowCount)
        {
            int iter = Math.Abs(rowCount - this.Rows.Count);
            if (rowCount < this.Rows.Count)
            {
                for (int i = 0; i < iter; i++)
                {
                    this.Rows.RemoveAt(this.Rows.Count - 1);
                }
            }
            else if (rowCount > this.Rows.Count)
            {
                for (int i = 0; i < iter; i++)
                {
                    this.Rows.Insert(this.Rows.Count, new GridRow());
                }
            }
        }

        /// <summary>
        /// Gets or sets the row headers border style.
        /// </summary>
        /// <value>
        /// The row headers border style.
        /// </value>
        public GridHeaderBorderStyle RowHeadersBorderStyle
        {
            get { return _rowHeadersBorderStyle; }
            set
            {
                _rowHeadersBorderStyle = value;

                // Notify property RowHeadersBorderStyle changed
                OnPropertyChanged("RowHeadersBorderStyle");
            }
        }


        /// <summary>
        /// Row Header Visible . 
        /// </summary>
        private bool _rowHeaderVisible = true;

        /// <summary>
        ///Gets or sets a value indicating whether the column that contains row headers is displayed.
        /// </summary>
        /// <value>
        /// true if the column that contains row headers is displayed; otherwise, false, The default is true
        /// </value>
        [RendererPropertyDescription]
        public bool RowHeaderVisible
        {
            get
            {
                return _rowHeaderVisible;
            }
            set
            {
                if (_rowHeaderVisible != value)
                {
                    _rowHeaderVisible = value;
                    // if the grid mode is a spreadsheet mode.
                    SetRowHeader();
                    OnPropertyChanged("selModel");
                }

            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether [column headers visible].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [column headers visible]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool ColumnHeadersVisible
        {
            get { return _columnHeadersVisible; }
            set
            {
                _columnHeadersVisible = value;

                // Notify property ColumnHeadersVisible changed
                OnPropertyChanged("ColumnHeadersVisible");
            }
        }



        /// <summary>
        /// Gets or sets the width of the row headers.
        /// </summary>
        /// <value>
        /// The width of the row headers.
        /// </value>
        [DefaultValue(43)]
        public int RowHeadersWidth
        {
            get { return _rowHeadersWidth; }
            set
            {
                _rowHeadersWidth = value;

                // Notify property RowHeadersWidth changed
                OnPropertyChanged("RowHeadersWidth");
            }
        }



        /// <summary>
        /// Gets the rows.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        [DesignerIgnore]
        public GridRowCollection Rows
        {
            get { return _rows; }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                foreach (VisualElement column in this.Columns)
                {
                    yield return column;
                }
                foreach (VisualElement row in this.Rows)
                {
                    yield return row;
                }
            }
        }
        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        IEnumerable<IBindingDataColumn> IBindingViewModel.Columns
        {
            get
            {
                return this.Columns;
            }
        }

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        IEnumerable<IBindingViewModelRecord> IBindingViewModel.Records
        {
            get
            {
                return this.Rows;
            }
        }

        /// <summary>
        /// Initializes the binding manager.
        /// </summary>
        /// <param name="bindingManager">The binding manager.</param>
        public override void InitializeBindingManager(BindingManager bindingManager)
        {
            // Get the Group Field
            if (this.IsGroupingGrid && !string.IsNullOrEmpty(this.GroupField))
            {
                if (this.Columns.Any(column => column.ID == this.GroupField))
                {
                    bindingManager.GroupField = this.GroupField;
                }
            }

        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // If is a control
            if (visualElement is GridColumn)
            {
                this.Columns.Add((GridColumn)visualElement);
                return;
            }
            base.Add(visualElement, insert);
        }

        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void IBindingViewModel.Bind(IDataReader dataReader)
        {
            if (this.DataSource == null)
            {
                return;
            }
            // Clear rows and columns
            if (_autoGenerateColumns)
            {
                _columns.Clear();
            }
            _rows.Clear();
            // If there is a valid data reader
            if (dataReader != null)
            {
                if (_autoGenerateColumns)
                {
                    // Loop all fields
                    for (int index = 0; index < dataReader.FieldCount; index++)
                    {
                        string comboBoxDatamember = "";
                        if (this.ComboBoxList.Count > 0)
                        {
                            if (this.ComboBoxList.ContainsKey(dataReader.GetName(index)))
                            {
                                comboBoxDatamember = dataReader.GetName(index);
                            }
                        }
                        // Add column
                        switch (Convert.ToString(dataReader.GetFieldType(index)))
                        {
                            case "System.Boolean":
                                _columns.Add(new GridCheckBoxColumn(dataReader.GetName(index), dataReader.GetName(index)));
                                break;
                            case "System.DateTime":
                                _columns.Add(new GridDateTimePickerColumn(dataReader.GetName(index), dataReader.GetName(index)));
                                break;
                            default:
                                if (this.ColumnHtmlContent != null)
                                {
                                    if (this.ColumnHtmlContent.Count > 0)
                                    {
                                        if (ColumnHtmlContent.ContainsKey(index))
                                        {
                                            _columns.Add(new GridHtmlContentColumn(dataReader.GetName(index), dataReader.GetName(index), ColumnHtmlContent[index]));
                                            break;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(comboBoxDatamember))
                                {
                                    _columns.Add(new GridComboBoxColumn(dataReader.GetName(index), dataReader.GetName(index)));
                                }
                                else
                                {
                                    _columns.Add(new GridTextBoxColumn(dataReader.GetName(index), dataReader.GetName(index)));
                                }
                                break;
                        }


                    }

                }
                // Get the columns count
                int columnsCount = this.Columns.Count;
                int[] ordinals = new int[columnsCount];
                for (int index = 0; index < columnsCount; index++)
                {
                    string _dataMember = _columns[index].DataMember;

                    if (!String.IsNullOrWhiteSpace(_dataMember))
                    {
                        // check if the DataMember is dirty
                        if (_columns[index].IsDirtyDataMember)
                        {
                            _dataMember = _columns[index].DirtyDataMemberName;
                        }
                        ordinals[index] = dataReader.GetOrdinal(_dataMember);
                    }
                    else
                    {
                        ordinals[index] = -1;
                    }
                }
                using (this.BatchBlock)
                {
                    int rowIndex = 0;
                    // Loop all records
                    while (dataReader.Read())
                    {
                        // Create grid row
                        string rowId = string.Format("_{0}_row{1}", this.ClientID, rowIndex++);
                        // Create grid row
                        GridRow gridRow = new GridRow() { ID = rowId };

                        // Add row
                        _rows.Add(gridRow);
                        // Loop all columns indexes
                        for (int index = 0; index < columnsCount; index++)
                        {
                            // Create header cell
                            GridContentCell cellElement = new GridContentCell();

                            int _ordinal = ordinals[index];

                            // Check if column is data bound
                            if (_ordinal >= 0)
                            {
                                // if we have an image to show
                                if (_columns[index].DataType.FullName == "System.Byte[]")
                                {
                                    gridRow.Cells[index].Value = dataReader.GetValue(_ordinal);
                                }
                                else
                                {
                                    // save the text/value
                                    gridRow.Cells[index].Text = _columns[index].FormatValue(dataReader.GetValue(_ordinal));
                                }
                            }


                        }
                    }
                    _rows.PerformUpdated(0, "Bind", null);
                }
            }

            //Select the first row .
            if (IsValidIndexes(0, 0))
            {
                this.Rows[0]._selected = true;
                this._selectedColumnIndex = 0;
                this._selectedRowIndex = 0;
            }
            // Notify property DataSource changed
            OnPropertyChanged("DataSource");
        }





        /// <summary>
        /// Gets or sets the rows default cell style.
        /// </summary>
        /// <value>
        /// The rows default cell style.
        /// </value>
        public GridCellStyle RowsDefaultCellStyle
        {
            get { return _rowsDefaultCellStyle; }
            set
            {
                _rowsDefaultCellStyle = value;

                // Notify property RowsDefaultCellStyle changed
                OnPropertyChanged("RowsDefaultCellStyle");
            }
        }



        /// <summary>
        /// Gets or sets the scroll bars.
        /// </summary>
        /// <value>
        /// The scroll bars.
        /// </value>
        [DefaultValue(ScrollBars.Both)]
        public ScrollBars ScrollBars
        {
            get { return _scrollBars; }
            set
            {
                _scrollBars = value;

                // Notify property ScrollBars changed
                OnPropertyChanged("ScrollBars");
            }
        }



        /// <summary>
        /// Gets the selected cells.
        /// </summary>
        /// <value>
        /// The selected cells.
        /// </value>
        [DesignerIgnore]
        public IList<GridCellElement> SelectedCells
        {
            get { return _selectedCells; }
            private set { _selectedCells = value; }
        }




        /// <summary>
        /// Gets the selected rows.
        /// </summary>
        /// <value>
        /// The selected rows.
        /// </value>
        [DesignerIgnore]
        public IList<GridRow> SelectedRows
        {
            get { return _selectedRows; }
            private set { _selectedRows = value; }
        }

        /// <summary>
        /// Gets the selected columns.
        /// </summary>
        /// <value>
        /// The selected columns.
        /// </value>
        [DesignerIgnore]
        public IList<GridColumn> SelectedColumns
        {
            get { return _selectedColumns; }
            private set { _selectedColumns = value; }
        }


        /// <summary>
        ///Returns/sets the alignment of data in the fixed cells of a column.
        /// </summary>
        public short FixedAlignment
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the selection mode.
        /// </summary>
        /// <value>
        /// The selection mode.
        /// </value>
        [DefaultValue(CellSelectionMode.Cell)]
        [RendererPropertyDescription()]
        public CellSelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                _selectionMode = value;

                // Notify property SelectionMode changed
                OnPropertyChanged("selModel");
            }
        }



        /// <summary>
        /// Gets or sets the current row index.
        /// </summary>
        /// <value>
        /// The row index.
        /// </value>
        public int SelectedRowIndex
        {

            get { return this._selectedRowIndex; }
            private set
            {
                this._selectedRowIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets the mouse column index.
        /// </summary>
        /// <value>
        /// The column index.
        /// </value>
        public int SelectedColumnIndex
        {

            get { return this._selectedColumnIndex; }
            set
            {
                this._selectedColumnIndex = value;
            }
        }


        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();

            // If is valid 
            if (IsLoadedToClient)
            {
                ApplicationElement.RegisterChange(this, "$refresh");
            }
        }

        /// <summary>
        /// CellBeginEdit event handler.
        /// </summary>
        private event GridElementCellEventHandler _cellBeginEdit;

        /// <summary>
        /// Add or remove EventName action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler CellBeginEdit
        {
            add
            {
                bool needNotification = (_cellBeginEdit == null);

                _cellBeginEdit += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellBeginEdit");
                }

            }
            remove
            {
                _cellBeginEdit -= value;

                if (_cellBeginEdit == null)
                {
                    OnEventHandlerDeattached("CellBeginEdit");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CellBeginEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellBeginEdit"/> instance containing the event data.</param>
        public virtual void OnCellBeginEdit(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_cellBeginEdit != null)
            {
                _cellBeginEdit(this, args);
            }
        }



        /// <summary>
        /// Performs the <see cref="E:CellBeginEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellBeginEdit(GridElementCellEventArgs args)
        {
            if (!this.ReadOnly && this.CurrentColumn != null && !this.CurrentColumn.ReadOnly)
            {
                // Raise the CellBeginEdit event.
                OnCellBeginEdit(args);
            }

        }


        /// <summary>
        /// Raises the <see cref="E:ellLeave" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        protected virtual void OnCellLeave(GridElementCellEventArgs args)
        {
            // Check if there are listeners.
            if (CellLeave != null)
            {
                CellLeave(this, args);
            }
        }



        /// <summary>
        /// Performs the <see cref="E:ellLeave" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellLeave(GridElementCellEventArgs args)
        {
            // Raise the ellLeave event.
            OnCellLeave(args);
        }


        /// <summary>
        ///Clears the contents of the FlexGrid. This includes all text, pictures, and cell formatting.
        /// </summary>
        public void Clear()
        {
        }

        /// <summary>
        /// Cancels the selection of currently selected cells.
        /// </summary>
        public void ClearSelection()
        {
            if (_selectedCells != null)
            {
                _selectedCells.Clear();
            }
            if (_selectedColumns != null)
            {
                _selectedColumns.Clear();
            }
            if (_selectedRows != null)
            {
                _selectedRows.Clear();
            }
        }


        /// <summary>
        /// CellClick event handler.
        /// </summary>
        private event GridElementCellEventHandler _CellClick;


        /// <summary>
        /// Add or remove CellClick action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler CellClick
        {
            add
            {
                bool needNotification = (_CellClick == null);

                _CellClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellClick");
                }

            }
            remove
            {
                _CellClick -= value;

                if (_CellClick == null)
                {
                    OnEventHandlerDeattached("CellClick");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CellClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellClick"/> instance containing the event data.</param>
        protected virtual void OnCellClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CellClick != null)
            {
                _CellClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellClick(GridElementCellEventArgs args)
        {
            OnCellClick(args);
        }



        /// <summary>
        /// Sets the width of the specified column in the grid
        /// </summary>
        /// <param name="colNumber">The column number</param>
        /// <param name="colWidth">The width to set</param>
        public void SetColWidth(int colNumber, int colWidth)
        {
            if (this.Columns[colNumber] != null)
            {
                this.Columns[colNumber].Width = colWidth;
            }
        }



        /// <summary>
        /// Gets the width of the specified column in the grid
        /// </summary>
        /// <param name="colNumber"></param>
        /// <returns></returns>
        public int GetColWidth(int colNumber)
        {
            if (this.Columns[colNumber] != null)
            {
                return this.Columns[colNumber].PixelWidth;
            }

            //column doesn't exit
            throw new NullReferenceException();
        }

        bool _multiSelect = false;
        /// <summary>
        /// get or set the multi select mode 
        /// </summary>
        [RendererPropertyDescription()]
        public bool MultiSelect
        {
            get { return _multiSelect; }
            set
            {
                _multiSelect = value;
                // Notify property MultiSelect changed
                OnPropertyChanged("MultiSelect");
            }
        }

        /// <summary>
        /// Contexts the menu show.
        /// </summary>
        [RendererMethodDescription]
        public void ContextMenuShow()
        {

            // If is valid 
            if (IsLoadedToClient)
            {
                // Ensure control is updated
                this.InvokeMethodOnce("ContextMenuShow");
            }

        }



        /// <summary>
        /// Shows the context menu.
        /// </summary>
        /// <param name="menuElement">The menu element.</param>
        private void ShowContextMenu(MenuElement menuElement)
        {
            InvokeMethodOnce("ContextMenuShow", (menuElement as MenuElement).ClientID);
        }

        private object _content = null;


        /// <summary>
        /// The Content of the Grid 
        /// </summary>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        public override object Content
        {
            get { return _content; }
            set
            {
                _content = value;
                OnPropertyChanged("Content");
            }
        }

        /// <summary>
        /// Set or get maximum rows allowed
        /// </summary>
        public int MaxRows
        {
            get
            {
                return _maxRows;
            }
            set
            {
                if (value < Rows.Count)
                {
                    throw new ArgumentException("The current number of rows is bigger than the new value");
                }
                _maxRows = value;
                Rows.MaxRows = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether [allow user to select cells].
        /// </summary>
        /// <value>
        /// <c>true</c> if allow user select cells; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        [RendererPropertyDescription]
        public bool CanSelect
        {
            get
            {
                return _canSelect;
            }
            set
            {
                _canSelect = value;

                // Notify property CanSelect changed
                OnPropertyChanged("CanSelect");
            }
        }


        /// <summary>
        /// CellEndEdit event handler.
        /// </summary>
        private event EventHandler _CellEndEdit;

        /// <summary>
        /// Add or remove CellEndEdit action. 
        /// This Action invoked on cell edit end, even if no changes to cell value were made.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler CellEndEdit
        {
            add
            {
                bool needNotification = (_CellEndEdit == null);

                _CellEndEdit += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellEndEdit");
                }

            }
            remove
            {
                _CellEndEdit -= value;

                if (_CellEndEdit == null)
                {
                    OnEventHandlerDeattached("CellEndEdit");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:CellEndEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellEndEdit"/> instance containing the event data.</param>
        protected virtual void OnCellEndEdit(EventArgs args)
        {

            // Check if there are listeners.
            if (_CellEndEdit != null)
            {
                _CellEndEdit(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has cell end edit listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has cell end edit listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellEndEditListeners
        {
            get { return _CellEndEdit != null; }
        }


        /// <summary>
        /// Performs the <see cref="E:CellEndEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellEndEdit(EventArgs args)
        {
            if (args != null)
            {
                // If we cancel the cell begin edit event do not raise cell end edit event .
                if (_cancelEndEditEvent)
                {
                    _cancelEndEditEvent = false;
                    return;
                }

                // If current column is read only, do not raise the cell end edit event .
                if (CurrentColumn != null && CurrentColumn.ReadOnly)
                {
                    return;
                }

                // Raise the CellEndEdit event.
                if (_selectedRowIndex >= 0 && _selectedColumnIndex >= 0)
                {
                    OnCellEndEdit(args);
                }
            }
        }


        /// <summary>
        /// ColumnHeaderRightClick event handler.
        /// </summary>
        private event GridElementCellEventHandler _columnHeaderMouseRightClick;

        /// <summary>
        /// Add or remove ColumnHeaderRightClick action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler ColumnHeaderMouseRightClick
        {
            add
            {
                bool needNotification = (_columnHeaderMouseRightClick == null);

                _columnHeaderMouseRightClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnHeaderMouseRightClick");
                }

            }
            remove
            {
                _columnHeaderMouseRightClick -= value;

                if (_columnHeaderMouseRightClick == null)
                {
                    OnEventHandlerDeattached("ColumnHeaderMouseRightClick");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:ColumnHeaderMouseRightClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ColumnHeaderMouseRightClick"/> instance containing the event data.</param>
        protected virtual void OnColumnHeaderMouseRightClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_columnHeaderMouseRightClick != null)
            {
                _columnHeaderMouseRightClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ColumnHeaderMouseRightClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformColumnHeaderMouseRightClick(GridElementCellEventArgs args)
        {
            if (args != null)
            {
                if (args.ColumnIndex > -1)
                {
                    // Update the selected column index .
                    _selectedColumnIndex = args.ColumnIndex;
                }
            }
            // Raise the ColumnHeaderMouseRightClick event.
            OnColumnHeaderMouseRightClick(args);
        }

        /// <summary>
        /// number of frozen columns
        /// </summary>
        private int _frozenCols = 0;

        /// <summary>
        /// FrozenCols Description.
        /// </summary>
        [DefaultValue(0)]
        [RendererPropertyDescription]
        public int FrozenCols
        {
            get
            {
                return _frozenCols;
            }
            set
            {
                if (_frozenCols != value)
                {
                    _frozenCols = value;

                    OnPropertyChanged("FrozenCols");
                }
            }
        }




        /// <summary>
        /// RowClick event handler.
        /// </summary>
        private event EventHandler<GridRowClickEventArgs> _RowClick;

        /// <summary>
        /// Add or remove RowClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridRowClickEventArgs> RowClick
        {
            add
            {
                bool needNotification = (_RowClick == null);

                _RowClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("RowClick");
                }

            }
            remove
            {
                _RowClick -= value;

                if (_RowClick == null)
                {
                    OnEventHandlerDeattached("RowClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has RowClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasRowClickListeners
        {
            // Must renderer this event cause we need to save some data of the current row 
            get { return _RowClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:RowClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="RowClick"/> instance containing the event data.</param>
        protected virtual void OnRowClick(GridRowClickEventArgs args)
        {

            // Check if there are listeners.
            if (_RowClick != null)
            {
                _RowClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:RowClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformRowClick(GridRowClickEventArgs args)
        {
            // Raise the RowClick event.
            OnRowClick(args);
        }


        /// <summary>
        /// Returns or sets an arbitrary long value associated with each column.
        /// </summary>
        public Dictionary<int, object> ColData
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns or sets an arbitrary long value associated with each row.
        /// </summary>
        public Dictionary<int, object> RowData
        {
            get;
            private set;
        }


        /// <summary>
        /// CellMouseDown event handler.
        /// </summary>
        private event EventHandler<GridCellMouseEventArgs> _CellMouseDown;

        /// <summary>
        /// Add or remove CellMouseDown action.
        /// </summary>
        [RendererEventDescription]
        public virtual event EventHandler<GridCellMouseEventArgs> CellMouseDown
        {
            add
            {
                bool needNotification = (_CellMouseDown == null);

                _CellMouseDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellMouseDown");
                }

            }
            remove
            {
                _CellMouseDown -= value;

                if (_CellMouseDown == null)
                {
                    OnEventHandlerDeattached("CellMouseDown");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:CellMouseDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellMouseDown"/> instance containing the event data.</param>
        protected virtual void OnCellMouseDown(GridCellMouseEventArgs args)
        {

            // Check if there are listeners.
            if (_CellMouseDown != null)
            {
                _CellMouseDown(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellMouseDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformCellMouseDown(GridCellMouseEventArgs args)
        {


            // Update ColumnIndex , CurrentRow
            this.PreviousColumn = CurrentColumn;
            this.PreviousRow = CurrentRow;
            int prevIndex = _selectedRowIndex;
            // Update SelectedRowIndex , ColumnIndex , CurrentRow
            _selectedColumnIndex = args.ColumnIndex;
            _selectedRowIndex = args.RowIndex;

            // Update current cell . 
            if (IsValidIndexes(_selectedColumnIndex, _selectedRowIndex))
            {
                _currentCell = this.Rows[_selectedRowIndex].Cells[_selectedColumnIndex];
            }

            // Raise the CellMouseDown event.
            OnCellMouseDown(args);

            // Create GridElementCellEventArgs 
            GridElementCellEventArgs _gridElementCellEventArgs = new GridElementCellEventArgs(this._selectedColumnIndex, this._selectedRowIndex);

            // Raise related actions . 
            PerformCellClick(_gridElementCellEventArgs);

            // Raise Selectionchanged 



            GridRowClickEventArgs _gridRowClickEventArgs = new GridRowClickEventArgs(this._selectedRowIndex);
            PerformRowClick(_gridRowClickEventArgs);

            // Process mouse up sequence
            _gridElementCellEventArgs.Cancel = false;
            PerformCellMouseUp(_gridElementCellEventArgs);


            if (this.ColumnCount > 0 && (this.Columns[0] as WidgetColumn) != null)
            {
                if (prevIndex != -1 && prevIndex != _selectedRowIndex && _itemChangedRaised == true && _selectionEventsRaised == false)
                {
                    //Raise SelectionChanged
                    List<int> lstRows = new List<int>() { _selectedRowIndex };
                    List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(_selectedRowIndex, _selectedColumnIndex) };
                    GridElementSelectionChangeEventArgs newArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);
                    PerformSelectionChanged(newArgs);
                }
                _itemChangedRaised = false;
                _selectionEventsRaised = false;
            }
        }

        /// <summary>
        /// Validate the row and column index .
        /// </summary>
        /// <param name="columnIndex">The grid column index .</param>
        /// <param name="rowIndex">The grid row index .</param>
        /// <returns></returns>
        internal bool IsValidIndexes(int columnIndex, int rowIndex)
        {
            if (IsValidColumn(columnIndex))
            {
                if (IsValidRow(rowIndex))
                {
                    int cellsCount = this.Rows[rowIndex].Cells.Count;
                    if (cellsCount > 0 && cellsCount > columnIndex)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public override void PerformWidgetControlItemChanged(EventArgs args)
        {
            _itemChangedRaised = true;
            base.PerformWidgetControlItemChanged(args);

        }
        /// <summary>
        /// Get, set Column FilterInfo .
        /// </summary>
        [RendererPropertyDescription]
        public FilterInfo ColumnFilterInfo
        {
            get { return _columnFilterInfo; }
            set
            {
                if (_columnFilterInfo != value)
                {
                    if (this.DataSource != null)
                    {
                        // Get the binding manager
                        BindingManager bindingManager = BindingManager.CreateBindingManager(this);
                        if (bindingManager != null)
                        {   // if the sorted column exist in the column collection
                            if (this.Columns.Any(column => column.ID.Equals(value.FilterInfoKey)))
                            {
                                // Allow filtering
                                bindingManager.AllowFilter = true;
                                _columnFilterInfo = value;
                                OnPropertyChanged("ColumnFilterInfo");
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Clear data source filter 
        /// </summary>
        [RendererMethodDescription]
        public void ClearFilter()
        {
            this.InvokeMethodOnce("ClearFilter");
        }
        /// <summary>
        /// Get,set group field 
        /// </summary>
        public string GroupField
        {
            get
            {
                return this.BindingManager.GroupField;
            }
            set
            {
                this.BindingManager.GroupField = value;
            }
        }

        /// <summary>
        /// The ComboBox list
        /// </summary>
        private Dictionary<string, string> _ComboBoxList;

        /// <summary>
        /// ComboBoxList Description.
        /// </summary>
        public Dictionary<string, string> ComboBoxList
        {
            get
            {
                return _ComboBoxList;
            }
            set
            {
                if (_ComboBoxList != value)
                {
                    _ComboBoxList = value;
                }
            }
        }


        private List<string> _textButtonList;

        /// <summary>
        /// ComboBoxList List.
        /// </summary>
        public List<string> TextButtonList
        {
            get
            {
                return _textButtonList;
            }
            set
            {
                if (_textButtonList != value)
                {
                    _textButtonList = value;
                }
            }
        }


        private bool _SortableColumns = true;

        /// <summary>
        /// SortableColumns = true Description.
        /// </summary>
        [DefaultValue(true)]
        [RendererPropertyDescription]
        public bool SortableColumns
        {
            get
            {
                return _SortableColumns;
            }
            set
            {
                if (_SortableColumns != value)
                {
                    _SortableColumns = value;

                    OnPropertyChanged("SortableColumns");
                }
            }
        }

        /// <summary>
        /// MouseRightClick event handler.
        /// </summary>
        private event EventHandler<EventArgs> _mouseRightClick;

        /// <summary>
        /// Add or remove MouseRightClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> MouseRightClick
        {
            add
            {
                bool needNotification = (_mouseRightClick == null);

                _mouseRightClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseRightClick");
                }

            }
            remove
            {
                _mouseRightClick -= value;

                if (_mouseRightClick == null)
                {
                    OnEventHandlerDeattached("MouseRightClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has MouseRightClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasMouseRightClickListeners
        {
            get { return _mouseRightClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:MouseRightClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseRightClick"/> instance containing the event data.</param>
        protected virtual void OnMouseRightClick(EventArgs args)
        {

            // Check if there are listeners.
            if (_mouseRightClick != null)
            {
                _mouseRightClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:mMouseRightClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformMouseRightClick(EventArgs args)
        {

            // Raise the MouseRightClick event.
            OnMouseRightClick(args);
        }


        /// <summary>
        /// BeforeContextMenuShow event handler.
        /// </summary>
        private event CancelEventHandler _BeforeContextMenuShow;

        /// <summary>
        /// Add or remove BeforeContextMenuShow action.
        /// </summary>
        [RendererEventDescription]
        public event CancelEventHandler BeforeContextMenuShow
        {
            add
            {
                bool needNotification = (_BeforeContextMenuShow == null);

                _BeforeContextMenuShow += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeContextMenuShow");
                }

            }
            remove
            {
                _BeforeContextMenuShow -= value;

                if (_BeforeContextMenuShow == null)
                {
                    OnEventHandlerDeattached("BeforeContextMenuShow");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeContextMenuShow listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeContextMenuShowListeners
        {
            get { return _BeforeContextMenuShow != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeContextMenuShow" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeContextMenuShow"/> instance containing the event data.</param>
        protected virtual void OnBeforeContextMenuShow(CancelEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeContextMenuShow != null)
            {
                _BeforeContextMenuShow(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeContextMenuShow" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeContextMenuShow(CancelEventArgs args)
        {
            // Raise the BeforeContextMenuShow event.
            OnBeforeContextMenuShow(args);
        }

        private bool _AllowColumnSelection;

        /// <summary>
        /// Gets or sets a value indicating whether an entire column can be selected 
        /// </summary>
        /// <value>
        /// <c>true</c> if column can be selected; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool AllowColumnSelection
        {
            get
            {
                return _AllowColumnSelection;
            }
            set
            {
                if (_AllowColumnSelection != value)
                {
                    _AllowColumnSelection = value;

                    OnPropertyChanged("AllowColumnSelection");
                }
            }
        }



        /// <summary>
        /// Called when data source changed.
        /// </summary>
        /// <param name="args">The <see cref="BindingManagerChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnDataSourceChanged(BindingManagerChangedEventArgs args)
        {
            // If the Data Source is not empty .
            if (this.DataSource != null)
            {
                // Reset the grid state .
                ResetState();
            }
            base.OnDataSourceChanged(args);

            // Revise ComboBox List Builder And TextButton List Builder .
            ReviseColumns();

            var dt = this.DataSource as DataTable;
            var ds = this.DataSource as DataSet;
            if (dt == null && ds != null)
            {
                dt = ds.Tables[0];
            }
            if (dt != null)
            {

                //set header text 
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    this.Columns[i].HeaderText = dt.Columns[i].Caption;
                }
            }

            // Always register refresh method .
            ApplicationElement.RegisterChange(this, "$refresh");
        }

        /// <summary>
        /// Rest the grid state to the default values.
        /// </summary>
        private void ResetState()
        {
            // Clear selected records 
            ClearSelection();
            _selectedColumnIndex = -1;
            _selectedRowIndex = -1;
        }

        /// <summary>
        /// Revise ComboBox Column.
        /// </summary>
        private void ReviseColumns()
        {
            //ComboBoxList
            if (this.ComboBoxList.Count > 0)
            {
                foreach (string columnDataMember in this.ComboBoxList.Keys)
                {
                    GridColumn oldColumn = this.Columns.Where(clm => clm.DataMember == columnDataMember).FirstOrDefault();
                    if (oldColumn != null)
                    {
                        GridColumn newColumn = null;
                        if (ComboBoxList[columnDataMember] == null)
                        {
                            newColumn = new GridComboBoxColumn(oldColumn.ID, oldColumn.HeaderText, oldColumn.DataType);
                        }
                        else
                        {
                            newColumn = new GridComboBoxColumn(oldColumn.ID, oldColumn.HeaderText, typeof(GridComboBoxColumn));
                            if (oldColumn is GridComboBoxColumn)
                            {
                                (newColumn as GridComboBoxColumn).ExpandOnFocus = (oldColumn as GridComboBoxColumn).ExpandOnFocus;
                            }
                        }
                        newColumn.DataMember = oldColumn.DataMember;
                        newColumn.DisplayIndex = oldColumn.DisplayIndex;
                        newColumn.Width = oldColumn.Width;
                        newColumn.ReadOnly = oldColumn.ReadOnly;

                        int columnIndex = this.Columns.IndexOf(oldColumn);
                        this.Columns[columnIndex] = newColumn;
                    }
                }
            }
            // TextButtonList
            if (this.TextButtonList.Count > 0)
            {
                foreach (string columnDataMember in this.TextButtonList)
                {
                    GridColumn oldColumn = this.Columns.Where(clm => clm.DataMember == columnDataMember).FirstOrDefault();
                    if (oldColumn != null)
                    {
                        GridTextButtonColumn newColumn = new GridTextButtonColumn(oldColumn.ID, oldColumn.HeaderText, oldColumn.DataType);
                        newColumn.DataMember = oldColumn.DataMember;
                        newColumn.DisplayIndex = oldColumn.DisplayIndex;
                        newColumn.Width = oldColumn.Width;

                        int columnIndex = this.Columns.IndexOf(oldColumn);
                        this.Columns[columnIndex] = newColumn;
                    }
                }
            }
        }

        /// <summary>
        /// Add values to the ComboBox list by given  columnName. 
        /// </summary>
        /// <param name="columnDataMember">The Column DataMemebr.</param>
        /// <param name="strValues">The ComboBox values.</param>
        public void AddToComboBoxList(string columnDataMember, string strValues)
        {
            // Check if the ComboBoxList already contain this key.
            if (this.ComboBoxList.ContainsKey(columnDataMember))
            {
                string _comboboxListValue = "";
                if (this.ComboBoxList.TryGetValue(columnDataMember, out _comboboxListValue))
                {
                    if ((_comboboxListValue == null) || !_comboboxListValue.Equals(strValues))
                    {
                        // update dictionary
                        this.ComboBoxList.Remove(columnDataMember);
                        this.ComboBoxList.Add(columnDataMember, strValues);
                    }
                }
            }
            else
            {
                this.ComboBoxList.Add(columnDataMember, strValues);
            }
            ReviseColumns();
        }

        /// <summary>
        /// ColumnDisplayIndexChanged event handler.
        /// </summary>
        private event EventHandler<GridColumnMoveEventArgs> _columnDisplayIndexChanged;

        /// <summary>
        /// Add or remove ColumnDisplayIndexChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridColumnMoveEventArgs> ColumnDisplayIndexChanged
        {
            add
            {
                bool needNotification = (_columnDisplayIndexChanged == null);

                _columnDisplayIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnDisplayIndexChanged");
                }

            }
            remove
            {
                _columnDisplayIndexChanged -= value;

                if (_columnDisplayIndexChanged == null)
                {
                    OnEventHandlerDeattached("ColumnDisplayIndexChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:ColumnDisplayIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridColumnMoveEventArgs"/> instance containing the event data.</param>
        protected virtual void OnColumnDisplayIndexChanged(GridColumnMoveEventArgs args)
        {

            // Check if there are listeners.
            if (_columnDisplayIndexChanged != null)
            {
                _columnDisplayIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the column display index changed.
        /// </summary>
        /// <param name="args">The <see cref="GridColumnMoveEventArgs"/> instance containing the event data.</param>
        public void PerformColumnDisplayIndexChanged(GridColumnMoveEventArgs args)
        {
            // Update Display index 
            UpdateColumnDisplyIndex(args.FromIndex, args.ToIndex);
            // Raise the ColumnDisplayIndexChanged event.
            OnColumnDisplayIndexChanged(args);
        }



        /// <summary>
        /// CellMouseUp event handler.
        /// </summary>
        private event GridElementCellEventHandler _CellMouseUp;

        /// <summary>
        /// Add or remove CellMouseUp action.
        /// </summary>
        [RendererEventDescription]
        public event GridElementCellEventHandler CellMouseUp
        {
            add
            {
                bool needNotification = (_CellMouseUp == null);

                _CellMouseUp += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellMouseUp");
                }

            }
            remove
            {
                _CellMouseUp -= value;

                if (_CellMouseUp == null)
                {
                    OnEventHandlerDeattached("CellMouseUp");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellMouseUp listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCellMouseUpListeners
        {
            get { return _CellMouseUp != null || this.RowHeaderVisible; }
        }

        /// <summary>
        /// Raises the <see cref="E:CellMouseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellMouseUp"/> instance containing the event data.</param>
        protected virtual void OnCellMouseUp(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_CellMouseUp != null)
            {
                _CellMouseUp(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellMouseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformCellMouseUp(GridElementCellEventArgs args)
        {
            // Raise the CellMouseUp event.
            OnCellMouseUp(args);
        }

        private bool _EnableLocking;

        /// <summary>
        /// EnableLocking Description .
        /// </summary>
        /// <value>
        /// True lock the grid columns , otherwise false to unlock the grid columns.
        /// </value>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool EnableLocking
        {
            get
            {
                return _EnableLocking;
            }
            set
            {
                if (_EnableLocking != value)
                {
                    _EnableLocking = value;

                    OnPropertyChanged("EnableLocking");
                }
            }
        }

        /// <summary>
        /// Swap between columns .
        /// </summary>
        /// <param name="prevIndex">previous index of the column </param>
        /// <param name="displayIndex"></param>
        [RendererMethodDescription]
        private void SwapColumns(int prevIndex, int displayIndex)
        {
            if (prevIndex >= 0 && displayIndex >= 0)
            {
                if (this.RowHeaderVisible)
                {
                    prevIndex++;
                    displayIndex++;
                }
                // Notify the change to the client .
                this.InvokeMethod("SwapColumns", new GridColumnData(prevIndex, displayIndex));
            }
        }

        /// <summary>
        /// Update the new values of the column display index .
        /// </summary>
        /// <param name="prevIndex">previous display index value.</param>
        /// <param name="displayIndex">new display index value.</param>
        internal void UpdateColumnDisplyIndex(int prevIndex, int displayIndex)
        {
            int _prevIndex = prevIndex;
            int _displayIndex = displayIndex;

            if (_prevIndex >= 0 && _displayIndex >= 0)
            {
                // Order Display Index.
                OrderDisplayIndex(_prevIndex, _displayIndex, _prevIndex < _displayIndex);
            }
        }

        /// <summary>
        /// Order Display Index 
        /// </summary>
        /// <param name="_prevIndex">previous display index .</param>
        /// <param name="_displayIndex">new display index .</param>
        /// <param name="direction">direction move .</param>
        private void OrderDisplayIndex(int _prevIndex, int _displayIndex, bool direction)
        {
            // If we moved linked columns . 
            if (Math.Abs(_prevIndex - _displayIndex) == 1)
            {
                // Mark the moved column with -1.
                this.Columns.Where(clm => clm.DisplayIndex == _prevIndex).First().DisplayIndex = -1;
                // Mark the swapped column with -2.
                this.Columns.Where(clm => clm.DisplayIndex == _displayIndex).First().DisplayIndex = -2;

                // Update display indexes . 
                this.Columns.Where(clm => clm.DisplayIndex == -1).First().DisplayIndex = _displayIndex;
                // Mark the swapped column with -2.
                this.Columns.Where(clm => clm.DisplayIndex == -2).First().DisplayIndex = _prevIndex;
            }
            // if the difference between the previous display index and the new display index more than one .
            else if (Math.Abs(_prevIndex - _displayIndex) > 1)
            {
                // If we moved columns from right to left 
                if (direction)
                {
                    // Mark the moved column with -1.
                    this.Columns.Where(clm => clm.DisplayIndex == _prevIndex).First().DisplayIndex = -1;
                    //loop over all the middle columns and update the display index .
                    for (int index = _prevIndex + 1; index <= _displayIndex; index++)
                    {
                        this.Columns.Where(clm => clm.DisplayIndex == index).First().DisplayIndex -= 1;
                    }
                    // update the column with the old display index value . 
                    this.Columns.Where(clm => clm.DisplayIndex == -1).First().DisplayIndex = _displayIndex;
                }
                else
                {
                    // Mark the moved column with -1.
                    this.Columns.Where(clm => clm.DisplayIndex == _prevIndex).First().DisplayIndex = -1;
                    //loop over all the middle columns and update the display index .
                    for (int index = _prevIndex - 1; index >= _displayIndex; index--)
                    {
                        this.Columns.Where(clm => clm.DisplayIndex == index).First().DisplayIndex += 1;
                    }
                    // update the column with the old display index value . 
                    this.Columns.Where(clm => clm.DisplayIndex == -1).First().DisplayIndex = _displayIndex;
                }
            }
        }





        /// <summary>
        /// ShowAutoFilterColumnItem Description.
        /// </summary>
        [RendererPropertyDescription]
        public bool ShowAutoFilterColumnItem
        {
            get
            {
                return _showAutoFilterColumnItem;
            }
            set
            {
                if (_showAutoFilterColumnItem != value)
                {
                    _showAutoFilterColumnItem = value;

                    OnPropertyChanged("ShowAutoFilterColumnItem");
                }
            }
        }



        /// <summary>
        /// Change Column Display index Zero based.
        /// </summary>
        /// <param name="fromIndex">From index.</param>
        /// <param name="toIndex">To index.</param>
        public void ChangeColumnPosition(int fromIndex, int toIndex)
        {
            if (fromIndex != -1 && toIndex != -1)
            {
                if (fromIndex != toIndex)
                {
                    if (this.RowHeaderVisible)
                    {
                        this.UpdateColumnDisplyIndex(fromIndex, toIndex);
                        this.SwapColumns(fromIndex, toIndex);
                    }
                }
            }

        }


        /// <summary>
        /// Sorts the contents of the GridElement control in ascending or descending order based on the contents of the specified column.
        /// </summary>
        /// <param name="grdColumn">The column by which to sort the contents of the GridElement</param>
        /// <param name="sortOrder">One of the SortOrder values</param>
        [RendererMethodDescription]
        public void Sort(GridColumn grdColumn, SortOrder sortOrder)
        {
            if (grdColumn != null)
            {
                if (sortOrder != SortOrder.None)
                {
                    grdColumn.SortOrder = sortOrder;
                    this.InvokeMethod("Sort", new GridColumnData { ColumnDataIndex = grdColumn.DataMember, ColumnSortOrder = grdColumn.SortOrder });

                }
            }
        }

        /// <summary>
        /// Scroll to row
        /// </summary>
        [RendererMethodDescription]
        public void ScrollToRow(int rowIndex)
        {
            //Check if rowIndex is valid
            if (rowIndex > -1 && rowIndex < this.Rows.Count)
            {
                this.InvokeMethod("ScrollToRow", new GridRowData(rowIndex));
            }
        }
        /// <summary>
        /// Cell Style . 
        /// </summary>
        [RendererMethodDescription]
        public void SetWidgetStyle(int rowIndex, int columnIndex, string dataMember, string cssClass, bool appned = false)
        {
            //Check if rowIndex is valid
            if (rowIndex > -1 && rowIndex < this.Rows.Count)
            {
                WidgetColumn wdColumn = this.Columns[columnIndex] as WidgetColumn;
                if (wdColumn != null)
                {
                    int index = this.Rows[rowIndex].GetWidgetControlByDataMember(dataMember);
                    this.InvokeMethod("SetWidgetStyle", new GridCellInfo(rowIndex, columnIndex, cssClass, appned, index));
                }
            }
        }


        /// <summary>
        /// Set protect cell
        /// </summary>
        [RendererMethodDescription]
        public void SetProtectCell(int rowIndex, int colIndex, bool protect)
        {
            if (rowIndex >= 0 && rowIndex < this.Rows.Count)
            {
                if (colIndex >= 0 && colIndex < this.Columns.Count)
                {
                    if (this.Rows[rowIndex].Cells[colIndex].Protect != protect)
                    {
                        this.InvokeMethod("SetProtectCell", new GridIndexesData(rowIndex, colIndex, protect));
                        this.Rows[rowIndex].Cells[colIndex].Protect = protect;
                    }
                }
            }
        }

        /// <summary>
        /// RowHeaderMouseRightClick event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _RowHeaderMouseRightClick;

        /// <summary>
        /// Add or remove RowHeaderMouseRightClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridElementCellEventArgs> RowHeaderMouseRightClick
        {
            add
            {
                bool needNotification = (_RowHeaderMouseRightClick == null);

                _RowHeaderMouseRightClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("RowHeaderMouseRightClick");
                }

            }
            remove
            {
                _RowHeaderMouseRightClick -= value;

                if (_RowHeaderMouseRightClick == null)
                {
                    OnEventHandlerDeattached("RowHeaderMouseRightClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has RowHeaderMouseRightClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasRowHeaderMouseRightClickListeners
        {
            get { return _RowHeaderMouseRightClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:RowHeaderMouseRightClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="System.Web.VisualTree.Elements.GridElementCellEventArgs" /> instance containing the event data.</param>
        protected virtual void OnRowHeaderMouseRightClick(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_RowHeaderMouseRightClick != null)
            {
                _RowHeaderMouseRightClick(this, args);
            }
        }

        /// <summary>
        /// Performs the row header mouse right click.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformRowHeaderMouseRightClick(GridElementCellEventArgs args)
        {
            if (args.ColumnIndex == 0)
            {
                // Raise the RowHeaderMouseRightClick event.
                OnRowHeaderMouseRightClick(args);
            }
        }

        /// <summary>
        /// Selects the cell in the given indexes
        /// </summary>
        /// <param name="row">The <see cref="int"/> row index.</param>
        /// <param name="col">The <see cref="int"/> column index.</param>
        [RendererMethodDescription]
        public void SelectCell(int row, int col)
        {
            int columnIndex = col;
            if (this.RowHeaderVisible)
            {
                if (columnIndex + 1 < this.ColumnCount)
                {
                    columnIndex += 1;
                }
            }
            if (this.Rows[row].Cells[col].Selected == false)
            {
                this.Rows[row].Cells[col].Selected = true;
                SetCellBackgroundColor(row, col, SelectionColor);
                this.InvokeMethod("SelectCell", new GridIndexesData(row, columnIndex));

            }
        }



        /// <summary>
        /// CancelEdit event handler.
        /// </summary>
        private event EventHandler<EventArgs> _CancelEdit;

        /// <summary>
        /// Add or remove CancelEdit action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> CancelEdit
        {
            add
            {
                bool needNotification = (_CancelEdit == null);

                _CancelEdit += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CancelEdit");
                }

            }
            remove
            {
                _CancelEdit -= value;

                if (_CancelEdit == null)
                {
                    OnEventHandlerDeattached("CancelEdit");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CancelEdit listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCancelEditListeners
        {
            get { return _CancelEdit != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CancelEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CancelEdit"/> instance containing the event data.</param>
        protected virtual void OnCancelEdit(EventArgs args)
        {

            // Check if there are listeners.
            if (_CancelEdit != null)
            {
                _CancelEdit(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CancelEdit" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCancelEdit(EventArgs args)
        {

            // Raise the CancelEdit event.
            OnCancelEdit(args);
        }

        /// <summary>
        /// Set Row visibility.
        /// </summary>
        /// <param name="rowIndex">
        /// The row index.
        /// </param>
        /// <param name="visible">
        /// Row visible value.
        /// </param>
        [RendererMethodDescription]
        public void SetRowVisibility(int rowIndex, bool visible)
        {
            if (rowIndex >= 0 && rowIndex < this.Rows.Count)
            {
                if (this.Rows[rowIndex].Visible != visible)
                {
                    this.Rows[rowIndex].Visible = visible;
                    this.InvokeMethod("SetRowVisibility", new GridRowData(rowIndex, visible));
                }
            }
        }

        /// <summary>
        /// Edit grid cell with the given indexes
        /// </summary>
        /// <param name="rowIndex">index of the row </param>
        /// <param name="colIndex">index of the column </param>
        [RendererMethodDescription]
        public void EditCellByPosition(int rowIndex, int colIndex)
        {
            if (this != null)
            {
                if (rowIndex < this.Rows.Count && colIndex < this.Columns.Count && colIndex < this.Rows[rowIndex].Cells.Count)
                {
                    // Notify the change to the client .
                    this.InvokeMethod("EditCellByPosition", new GridIndexesData(rowIndex, colIndex + 1));
                }
            }
        }

        private Color _SelectionColor = Color.LightBlue;

        /// <summary>
        /// gets and sets the background color of a selected cell
        /// </summary>
        /// <value>
        /// the background color.
        /// </value>
        public Color SelectionColor
        {
            get
            {
                return _SelectionColor;
            }
            set
            {
                if (_SelectionColor != value)
                {
                    _SelectionColor = value;

                }
            }
        }

        /// <summary>
        /// sets the cell background color
        /// </summary>
        /// <param name="rowIndex">index of the row</param>
        /// <param name="colIndex">index of the column</param>
        /// <param name="color">The color.</param>
        [RendererMethodDescription]
        public void SetCellBackgroundColor(int rowIndex, int colIndex, Color color)
        {
            if (this != null)
            {
                if (rowIndex < this.Rows.Count && colIndex < this.Columns.Count && colIndex < this.Rows[rowIndex].Cells.Count)
                {
                    // Notify the change to the client .
                    this.InvokeMethod("SetCellBackgroundColor", new GridCellBackGroundData(rowIndex, colIndex + 1, color));
                }
            }
        }

        /// <summary>
        /// sets the row header cell background color . 
        /// </summary>
        /// <param name="rowIndex">index of the row </param>
        /// <param name="color">back color of the row header</param>
        [RendererMethodDescription]
        public void SetRowHeaderBackgroundColor(int rowIndex, Color color)
        {
            if (this != null)
            {
                if (rowIndex < this.Rows.Count && rowIndex >= 0)
                {
                    // Notify the change to the client .
                    this.InvokeMethod("SetRowHeaderBackgroundColor", new GridCellBackGroundData(rowIndex, 0, color));
                }
            }
        }

        /// <summary>
        /// sets the row header cell background image . 
        /// </summary>
        /// <param name="rowIndex">index of the row </param>
        /// <param name="image">image of the row header</param>
        [RendererMethodDescription]
        public void SetRowHeaderBackgroundImage(int rowIndex, ResourceReference image)
        {
            if (this != null)
            {
                if (rowIndex < this.Rows.Count && rowIndex >= 0)
                {
                    // Notify the change to the client .
                    this.InvokeMethod("SetRowHeaderBackgroundImage", new GridCellBackGroundData(rowIndex, image));
                }
            }
        }

        /// <summary>
        /// Get, set RowHeaderCornerIcon .
        /// </summary>
        public ResourceReference RowHeaderCornerIcon
        {
            get
            {
                return _rowHeaderCornerIcon;
            }
            set
            {
                _rowHeaderCornerIcon = value;
                SetRowHeaderCornerIcon(_rowHeaderCornerIcon);
            }
        }

        /// <summary>
        /// sets the cell background color
        /// </summary>
        /// <param name="icon">index of the column </param>
        [RendererMethodDescription]
        private void SetRowHeaderCornerIcon(ResourceReference icon)
        {
            if (this != null)
            {
                this.HeaderColumns[0].Icon = icon;
                this.InvokeMethod("SetRowHeaderCornerIcon", new GridCellBackGroundData(icon));
            }
        }



        /// <summary>
        /// Get ,Set AllowTrackMouseOver property . 
        /// </summary>
        /// <value>
        /// true to hightlight the row when moiuse over , otherwise false .
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(true)]
        public bool AllowTrackMouseOver
        {
            get
            {
                return _allowTrackMouseOver;
            }
            set
            {
                if (_allowTrackMouseOver != value)
                {
                    _allowTrackMouseOver = value;
                    OnPropertyChanged("AllowTrackMouseOver");
                }

            }
        }



        /// <summary>
        /// CellkeyDown  event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _cellkeyDown;

        /// <summary>
        /// Add or remove CellkeyDown  action.
        /// </summary>
        [RendererEventDescription]
        public virtual event EventHandler<GridElementCellEventArgs> CellkeyDown
        {
            add
            {
                bool needNotification = (_cellkeyDown == null);

                _cellkeyDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellkeyDown ");
                }

            }
            remove
            {
                _cellkeyDown -= value;

                if (_cellkeyDown == null)
                {
                    OnEventHandlerDeattached("CellkeyDown ");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellkeyDown listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has CellkeyDown listeners; otherwise, <c>false</c>.
        /// </value>
        public bool HasCellkeyDownListeners
        {
            get { return _cellkeyDown != null; }
        }


        /// <summary>
        /// Raises the <see cref="E:CellkeyDown " /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellkeyDown "/> instance containing the event data.</param>
        protected virtual void OnCellkeyDown(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_cellkeyDown != null)
            {
                _cellkeyDown(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellkeyDown " /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformCellkeyDown(GridElementCellEventArgs args)
        {
            if (args != null)
            {
                if (IsValidIndexes(args.ColumnIndex, args.RowIndex))
                {
                    _selectedColumnIndex = args.ColumnIndex;
                    _selectedRowIndex = args.RowIndex;
                }
            }
            // Raise the CellkeyDown  event.
            OnCellkeyDown(args);
        }

        /// <summary>
        /// ComboCellSelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler<GridComboBoxColumnEventArgs> _ComboCellSelectedIndexChanged;

        /// <summary>
        /// Add or remove ComboCellSelectedIndexChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridComboBoxColumnEventArgs> ComboCellSelectedIndexChanged
        {
            add
            {
                bool needNotification = (_ComboCellSelectedIndexChanged == null);

                _ComboCellSelectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ComboCellSelectedIndexChanged");
                }

            }
            remove
            {
                _ComboCellSelectedIndexChanged -= value;

                if (_ComboCellSelectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("ComboCellSelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ComboCellSelectedIndexChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has ComboCellSelectedIndexChanged listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasComboCellSelectedIndexChangedListeners
        {
            // Must renderer this event cause we need to save some data of the current row 
            get { return _ComboCellSelectedIndexChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ComboCellSelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ComboCellSelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnComboCellSelectedIndexChanged(GridComboBoxColumnEventArgs args)
        {

            // Check if there are listeners.
            if (_ComboCellSelectedIndexChanged != null)
            {
                _ComboCellSelectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ComboCellSelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridComboBoxColumnEventArgs"/> instance containing the event data.</param>
        public void PerformComboCellSelectedIndexChanged(GridComboBoxColumnEventArgs args)
        {
            // Raise the ComboCellSelectedIndexChanged event.
            OnComboCellSelectedIndexChanged(args);
        }

        /// <summary>
        /// ClientEvent event handler.
        /// </summary>
        private event EventHandler _ClientEvent;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ClientEvent action.
        /// </summary>
        public event EventHandler ClientEvent
        {
            add
            {
                bool needNotification = (_ClientEvent == null);

                _ClientEvent += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ClientEvent");
                }

            }
            remove
            {
                _ClientEvent -= value;

                if (_ClientEvent == null)
                {
                    OnEventHandlerDeattached("ClientEvent");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:ClientEvent" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ClientEvent"/> instance containing the event data.</param>
        protected virtual void OnClientEvent(EventArgs args)
        {

            // Check if there are listeners.
            if (_ClientEvent != null)
            {
                _ClientEvent(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ClientEvent" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformClientEvent(EventArgs args)
        {

            // Raise the ClientEvent event.
            OnClientEvent(args);
        }



        /// <summary>
        /// KeyDown event handler.
        /// </summary>
        private event EventHandler<KeyDownEventArgs> _keyDown;

        /// <summary>
        /// Add or remove KeyDown action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<KeyDownEventArgs> KeyDown
        {
            add
            {
                bool needNotification = (_keyDown == null);

                _keyDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("KeyDown");
                }

            }
            remove
            {
                _keyDown -= value;

                if (_keyDown == null)
                {
                    OnEventHandlerDeattached("KeyDown");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:KeyDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="KeyDown"/> instance containing the event data.</param>
        protected override void OnKeyDown(KeyDownEventArgs args)
        {

            // Check if there are listeners.
            if (_keyDown != null)
            {
                _keyDown(this, args);
            }
            if (args.KeyCode == 32) //'32' is SPACEBAR code.
            {
                PerformCellContentClick(new GridElementCellEventArgs(args.ColumnIndex, args.RowIndex));
            }
        }

        /// <summary>
        /// Performs the <see cref="E:KeyDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        public override void PerformKeyDown(KeyDownEventArgs args)
        {

            // Raise the KeyDown event.
            OnKeyDown(args);
        }


        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }



        /// <summary>
        /// ButtonsColumnClick event handler.
        /// </summary>
        private event EventHandler<ButtonsColumnClickEventsArgs> _buttonsColumnClick;

        /// <summary>
        /// Add or remove ButtonsColumnClick action.
        /// </summary>
        public event EventHandler<ButtonsColumnClickEventsArgs> ButtonsColumnClick
        {
            add
            {
                bool needNotification = (_buttonsColumnClick == null);

                _buttonsColumnClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ButtonsColumnClick");
                }

            }
            remove
            {
                _buttonsColumnClick -= value;

                if (_buttonsColumnClick == null)
                {
                    OnEventHandlerDeattached("ButtonsColumnClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ButtonsColumnClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasButtonsColumnClickListeners
        {
            get { return _buttonsColumnClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ButtonsColumnClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ButtonsColumnClick"/> instance containing the event data.</param>
        protected virtual void OnButtonsColumnClick(ButtonsColumnClickEventsArgs args)
        {

            // Check if there are listeners.
            if (_buttonsColumnClick != null)
            {
                _buttonsColumnClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ButtonsColumnClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformButtonsColumnClick(ButtonsColumnClickEventsArgs args)
        {

            // Raise the ButtonsColumnClick event.
            OnButtonsColumnClick(args);
        }


        [RendererMethodDescription]
        internal void SetButtonVisibility(int rowsIndex, int columnIndex, int button, bool state)
        {
            this.InvokeMethodOnce("SetButtonVisibility", new GridIndexesData() { RowIndex = rowsIndex, ColumnIndex = columnIndex, Button = button, State = state });
        }


        public Dictionary<int, string> ColumnHtmlContent { get; set; }








        [DesignerIgnore]
        public override bool IsContainer
        {
            get { return true; }
        }



        private bool _enableExport = false;
        /// <summary>
        /// Get ,Set EnableExport property . 
        /// </summary>
        /// <value>
        /// true to be able to export , otherwise false .
        /// </value>

        [DefaultValue(false)]
        public bool EnableExport
        {
            get
            {
                return _enableExport;
            }
            set
            {
                if (_enableExport != value)
                {
                    _enableExport = value;
                }
            }
        }

        private int GetWidgetControlIndex(int rowIndex, string dataMemeber)
        {
            if (rowIndex > -1 && !string.IsNullOrEmpty(dataMemeber))
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    WidgetColumn wdColumn = this.Columns[i] as WidgetColumn;
                    if (wdColumn != null)
                    {
                        int index = this.Rows[rowIndex].GetWidgetControlByDataMember(dataMemeber);
                        if (index > -1)
                        {
                            return index;
                        }
                    }
                }
            }
            return -1;
        }

        public virtual void WidgetControlFocus(int rowIndex, string dataMemeber)
        {
            int index = GetWidgetControlIndex(rowIndex, dataMemeber);
            if (index > -1)
            {
                this.Rows[rowIndex].WidgetControls[index].SetRowIndex(rowIndex);
                this.Rows[rowIndex].WidgetControls[index].SetWidgetControlIndex(index);
                this.Rows[rowIndex].WidgetControls[index].Focus();
            }
        }

        public void WidgetControlEnabled(int rowIndex, string dataMemeber, bool state)
        {
            int index = GetWidgetControlIndex(rowIndex, dataMemeber);
            if (index > -1)
            {
                this.Rows[rowIndex].WidgetControls[index].SetRowIndex(rowIndex);
                this.Rows[rowIndex].WidgetControls[index].SetWidgetControlIndex(index);
                this.Rows[rowIndex].WidgetControls[index].Enabled = state;
            }
        }

        public void WidgetControlStore(int rowIndex, string dataMemeber, string jsonData)
        {
            int index = GetWidgetControlIndex(rowIndex, dataMemeber);
            if (index > -1)
            {
                this.Rows[rowIndex].WidgetControls[index].SetRowIndex(rowIndex);
                this.Rows[rowIndex].WidgetControls[index].SetWidgetControlIndex(index);
                this.Rows[rowIndex].WidgetControls[index].Store = jsonData;
            }
        }

        public void WidgetControlBackColor(int rowIndex, string dataMemeber, Color color)
        {
            int index = GetWidgetControlIndex(rowIndex, dataMemeber);
            if (index > -1)
            {
                this.Rows[rowIndex].WidgetControls[index].SetRowIndex(rowIndex);
                this.Rows[rowIndex].WidgetControls[index].SetWidgetControlIndex(index);
                this.Rows[rowIndex].WidgetControls[index].BackColor = color;
            }
        }

        [RendererMethodDescription]
        public override void OnDataSourceItemUpdated(object item)
        {

            int rowIndex = -1;
            int columnIndex = -1;
            GridRow updatedRow = item as GridRow;
            // get row index and column index 
            if (updatedRow != null && updatedRow.Cells[0] != null)
            {
                rowIndex = updatedRow.Cells[0].RowIndex;
                columnIndex = updatedRow.Cells[0].ColumnIndex;
            }
            // get the grid data source as data table
            DataTable originalSource = this.DataSource as DataTable;
            if (originalSource != null)
            {
                // check if the row index and the column index is valid 
                if (originalSource.Rows.Count > rowIndex && originalSource.Columns.Count > columnIndex)
                {
                    for (int i = 0; i < updatedRow.Cells.Count; i++)
                    {

                        switch (((DataTable)originalSource).Columns[i].DataType.Name)
                        {
                            case "Boolean":
                                bool BooleanResult = false;
                                if (bool.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out BooleanResult))
                                {
                                    originalSource.Rows[rowIndex][i] = BooleanResult;
                                }
                                break;
                            case "Byte":
                                byte ByteResult = default(byte);
                                if (byte.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out ByteResult))
                                {
                                    originalSource.Rows[rowIndex][i] = ByteResult;
                                }
                                break;
                            case "Char":
                                char CharResult = default(char);
                                if (char.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out CharResult))
                                {
                                    originalSource.Rows[rowIndex][i] = CharResult;
                                }
                                break;
                            case "DateTime":
                                DateTime DateTimeResult = default(DateTime);
                                if (DateTime.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out DateTimeResult))
                                {
                                    originalSource.Rows[rowIndex][i] = DateTimeResult;
                                }
                                break;
                            case "Decimal":
                                decimal DecimalResult = default(decimal);
                                if (decimal.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out DecimalResult))
                                {
                                    originalSource.Rows[rowIndex][i] = DecimalResult;
                                }
                                break;
                            case "Double":
                                double DoubleResult = default(double);
                                if (double.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out DoubleResult))
                                {
                                    originalSource.Rows[rowIndex][i] = DoubleResult;
                                }
                                break;
                            case "Guid":
                                Guid GuidResult = default(Guid);
                                if (Guid.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out GuidResult))
                                {
                                    originalSource.Rows[rowIndex][i] = GuidResult;
                                }
                                break;
                            case "Int16":
                                Int16 Int16Result = default(Int16);
                                if (Int16.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out Int16Result))
                                {
                                    originalSource.Rows[rowIndex][i] = Int16Result;
                                }
                                break;
                            case "Int32":
                                Int32 Int32Result = default(Int32);
                                if (Int32.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out Int32Result))
                                {
                                    originalSource.Rows[rowIndex][i] = Int32Result;
                                }
                                break;
                            case "Int64":
                                Int64 Int64Result = default(Int64);
                                if (Int64.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out Int64Result))
                                {
                                    originalSource.Rows[rowIndex][i] = Int64Result;
                                }
                                break;
                            case "SByte":
                                SByte SByteResult = default(SByte);
                                if (SByte.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out SByteResult))
                                {
                                    originalSource.Rows[rowIndex][i] = SByteResult;
                                }
                                break;
                            case "Single":
                                Single SingleResult = default(Single);
                                if (Single.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out SingleResult))
                                {
                                    originalSource.Rows[rowIndex][i] = SingleResult;
                                }
                                break;
                            case "String":
                                originalSource.Rows[rowIndex][i] = Convert.ToString(updatedRow.Cells[i].Value);
                                break;
                            case "TimeSpan":
                                TimeSpan TimeSpanResult = default(TimeSpan);
                                if (TimeSpan.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out TimeSpanResult))
                                {
                                    originalSource.Rows[rowIndex][i] = TimeSpanResult;
                                }
                                break;
                            case "UInt16":
                                UInt16 UInt16Result = default(UInt16);
                                if (UInt16.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out UInt16Result))
                                {
                                    originalSource.Rows[rowIndex][i] = UInt16Result;
                                }
                                break;
                            case "UInt32":
                                UInt32 UInt32Result = default(UInt32);
                                if (UInt32.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out UInt32Result))
                                {
                                    originalSource.Rows[rowIndex][i] = UInt32Result;
                                }
                                break;
                            case "UInt64":
                                UInt64 UInt64Result = default(UInt64);
                                if (UInt64.TryParse(Convert.ToString(updatedRow.Cells[i].Value), out UInt64Result))
                                {
                                    originalSource.Rows[rowIndex][i] = UInt64Result;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            // update bindingManager datasource .
            BindingManager bind = BindingManager.GetBindingManager(this);
            bind.DataSource = originalSource;
        }


        private bool _searchPanel = false;

        /// <summary>
        /// Gets and Sets a value indicating if the filter field is shown .
        /// </summary>
        /// <value>
        /// true to show the filter field , otherwise false .
        /// </value>
        [RendererPropertyDescription]
        public bool SearchPanel
        {
            get
            {
                return _searchPanel;
            }
            set
            {
                if (_searchPanel != value)
                {
                    _searchPanel = value;

                    OnPropertyChanged("SearchPanel");
                }
            }
        }


        /// <summary>
        /// Gets or sets the client cell end edit.
        /// </summary>
        /// <value>
        /// The client cell end edit.
        /// </value>
        [DesignerIgnore]
        public string ClientCellEndEdit { get; set; }




        /// <summary>
        /// dataChanged event handler.
        /// </summary>
        private event EventHandler<EventArgs> _dataChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove dataChanged action.
        /// </summary>
        public event EventHandler<EventArgs> dataChanged
        {
            add
            {
                bool needNotification = (_dataChanged == null);

                _dataChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("dataChanged");
                }

            }
            remove
            {
                _dataChanged -= value;

                if (_dataChanged == null)
                {
                    OnEventHandlerDeattached("dataChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has dataChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasdataChangedListeners
        {
            get { return _dataChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:dataChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="dataChanged"/> instance containing the event data.</param>
        protected virtual void OndataChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_dataChanged != null)
            {
                _dataChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:dataChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformdataChanged(EventArgs args)
        {

            // Raise the dataChanged event.
            OndataChanged(args);
        }



       


        [DefaultValue(false)]
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets the client cell mouse down.
        /// </summary>
        /// <value>
        /// The client cell mouse down.
        /// </value>
        [DesignerIgnore]
        public string ClientCellMouseDown { get; set; }



        /// <summary>
        /// Cell Style . 
        /// </summary>
        [RendererMethodDescription]
        public void SetCellStyle(int rowIndex, int columnIndex, string cssClass, bool appned = false)
        {
            //Check if rowIndex is valid
            if (IsValidRow(rowIndex))
            {
                this.InvokeMethod("SetCellStyle", new GridCellInfo(rowIndex, columnIndex, cssClass, appned));
            }
        }

        /// <summary>
        /// Chaek if the rowindex is valid .
        /// </summary>
        /// <param name="rowIndex">The row index .</param>
        /// <returns>true nif the rowindex is valid , otherwise false</returns>
        private bool IsValidRow(int rowIndex)
        {
            //Check if rowIndex is valid
            if (rowIndex > -1 && rowIndex < this.Rows.Count)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Chaek if the column index is valid .
        /// </summary>
        /// <param name="columnIndx">The column indx.</param>
        /// <returns>
        /// true nif the rowindex is valid , otherwise false
        /// </returns>
        private bool IsValidColumn(int columnIndx)
        {
            //Check if rowIndex is valid
            if (columnIndx > -1 && columnIndx < this.Columns.Count)
            {
                return true;
            }
            return false;
        }



        /// <summary>
        /// Sets the selected index of combobox column
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="selectedIndex"></param>
        [RendererMethodDescription]
        internal void SetComboboxSelectedIndex(int rowIndex, int columnIndex, int selectedIndex)
        {

            this.InvokeMethodOnce("SetComboboxSelectedIndex", new GridComboBoxCellSelectionData(rowIndex, columnIndex, selectedIndex));
        }


        private GridCellBorderStyle _cellBorderStyle;

        /// <summary>
        /// Gets or sets the cell border style.
        /// </summary>
        /// <value>
        /// The cell border style.
        /// </value>
        [RendererPropertyDescription]
        public GridCellBorderStyle CellBorderStyle
        {
            get
            {
                return _cellBorderStyle;
            }
            set
            {
                if (_cellBorderStyle != value)
                {
                    _cellBorderStyle = value;
                }
            }
        }


        /// <summary>
        /// ContextMenuClick event handler.
        /// </summary>
        private event EventHandler<GridContextMenuCellEventArgs> _contextMenuClick;

        /// <summary>
        /// Add or remove ContextMenuClick action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridContextMenuCellEventArgs> ContextMenuClick
        {
            add
            {
                bool needNotification = (_contextMenuClick == null);

                _contextMenuClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ContextMenuClick");
                }

            }
            remove
            {
                _contextMenuClick -= value;

                if (_contextMenuClick == null)
                {
                    OnEventHandlerDeattached("ContextMenuClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ContextMenuClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasContextMenuClickListeners
        {
            get { return _contextMenuClick != null; }
        }

        public string ClientSelectionChanged { get; set; }

        [RendererMethodDescription]
        public void CompleteEdit()
        {
            this.InvokeMethod("CompleteEdit", null);
        }

        [RendererMethodDescription]
        public void CompleteEdit(Action a)
        {
            CompleteEdit();
            ApplicationElement.ExecuteOnRespone((o, e) => { a(); });
        }
    }
}
