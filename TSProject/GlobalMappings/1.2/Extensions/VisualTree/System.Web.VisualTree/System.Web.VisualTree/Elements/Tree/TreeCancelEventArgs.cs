namespace System.Web.VisualTree.Elements
{
	public class TreeCancelEventArgs : EventArgs
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeCancelEventArgs"/> class.
        /// </summary>
        /// <param name="objItem">The item.</param>
        /// <param name="IsCancel">if set to <c>true</c> [is cancel].</param>
        /// <param name="objAction">The action.</param>
        public TreeCancelEventArgs(TreeItem objItem, bool IsCancel, EventHandler<TreeEventArgs> objAction)
		{
		}



		/// <summary>
		/// Gets the item.
		/// </summary>
		/// <value>
		/// The item.
		/// </value>
		public TreeItem Item
		{
			get;
			set;
		}



		/// <summary>
		/// Gets the action.
		/// </summary>
		/// <value>
		/// The action.
		/// </value>
		public EventHandler<TreeEventArgs> Action
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TreeCancelEventArgs"/> is cancel.
        /// </summary>
        /// <value>
        ///   <c>true</c> if cancel; otherwise, <c>false</c>.
        /// </value>
        public bool Cancel
		{
			get;
			set;
		}
	}
}
