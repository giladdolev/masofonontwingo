namespace System.Web.VisualTree.Elements
{
	public class HelpProvider : VisualElement
	{
		public virtual void SetHelpKeyword(ControlElement ctl, string keyword)
		{
		}



		public virtual void SetHelpNavigator(ControlElement ctl, HelpNavigator navigator)
		{
		}



		public virtual void SetShowHelp(ControlElement ctl, bool value)
		{
		}



		public virtual string HelpNamespace
		{
			get;
			set;
		}
	}
}
