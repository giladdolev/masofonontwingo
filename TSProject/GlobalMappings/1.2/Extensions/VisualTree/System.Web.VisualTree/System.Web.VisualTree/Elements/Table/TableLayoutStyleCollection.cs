﻿namespace System.Web.VisualTree.Elements
{
    public class TableLayoutStyleCollection<TStyle> : VisualElementCollection<TStyle>
        where TStyle : VisualElement, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutStyleCollection{TStyle}"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TableLayoutStyleCollection(TablePanel objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }
    }
}
