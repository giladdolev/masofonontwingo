﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.FileSystem;

namespace System.Web.VisualTree.Elements.Compatibility
{

    public class DriveListBoxElement : FileSystemBase
    {
        /// <summary>
        /// The combo
        /// </summary>
        private readonly ComboBoxElement _combo;
        
        /// <summary>
        /// Changed event handler.
        /// </summary>
        private event EventHandler _Changed;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Changed action.
        /// </summary>
        public event EventHandler Changed
        {
            add
            {
                bool needNotification = (_Changed == null);

                _Changed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Changed");
                }

            }
            remove
            {
                _Changed -= value;

                if (_Changed == null)
                {
                    OnEventHandlerDeattached("Changed");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Changed"/> instance containing the event data.</param>
        protected virtual void OnChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_Changed != null)
            {
                _Changed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformChanged(EventArgs args)
        {

            // Raise the Changed event.
            OnChanged(args);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="DriveListBoxElement"/> class.
        /// </summary>
        public DriveListBoxElement()
        {
            // Set the ComboBox element 
            _combo = new ComboBoxElement();
            _combo.Dock = Dock.Fill;
            _combo.DropDownStyle = ComboBoxStyle.DropDownList;
            InitializeDrives();
            _combo.BorderStyle = BorderStyle.None;
            _combo.SelectedIndexChanged += OnComboSelectedIndexChanged;
            Add(_combo, false);
        }

        private void InitializeDrives()
        {
            foreach (var item in FileSystem.GetVirtualDrives())
            {
                _combo.Items.Add(new ListItem(item.Drive, item.Name));

            }
        }



        /// <summary>
        /// Gets or sets the drive.
        /// </summary>
        /// <value>
        /// The drive.
        /// </value>
        [DesignerIgnore]
        public VirtualDrive Drive
        {
            get
            {
                // Get the selected drive
                ListItem selectedItem = _combo.SelectedItem as ListItem;

                // If there is a valid selected drive
                if (selectedItem != null)
                {
                    VirtualDrive selectedDrive = FileSystem.GetVirtualDrive(selectedItem.Value as string);
                    return selectedDrive;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the drive identifier.
        /// </summary>
        /// <value>
        /// The drive identifier.
        /// </value>
        public string DriveID
        {
            get
            {
                return this.Drive.Drive;
            }
            set
            {
                if (value != null)
                {
                    // Loop all items
                    foreach (object item in _combo.Items)
                    {
                        // Get current drive
                        ListItem currentItem = item as ListItem;

                        // If there is a valid drive and it is a match to the value
                        if (currentItem != null && value.Equals(currentItem.Value))
                        {
                            // Setting the index will raise the changed event
                            _combo.SelectedItem = currentItem;

                            // We don't want to raise the event twice
                            return;
                        }
                    }

                    // Force value change
                    PerformChanged(EventArgs.Empty);
                }
            }
        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override sealed void Add(VisualElement visualElement, bool insert)
        {
            base.Add(visualElement, false);
        }




        /// <summary>
        /// Called when combo selected index changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnComboSelectedIndexChanged(object sender, EventArgs e)
        {
            PerformChanged(e);
        }
    }
}
