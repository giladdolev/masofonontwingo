﻿namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ControlElement" />
    public class GridItemElement : ControlElement
    {
        /// <summary>
        /// Gets the grid element.
        /// </summary>
        /// <value>
        /// The grid element.
        /// </value>
        public GridElement GridElement
        {
            get
            {
                GridElement parent = this.ParentElement as GridElement;
                if (parent != null)
                {
                    return parent;
                }

                GridRow row = this.ParentElement as GridRow;
                if (row != null)
                {
                    GridElement grd = row.ParentElement as GridElement;
                    if (grd != null)
                    {
                        return grd;
                    }
                }
                return null;
            }
        }
    }
}
