﻿namespace System.Web.VisualTree.Elements.Touch
{

    public class TFormElement : PanelElement, ITVisualElement, ITVisualContainerElement
    {
        /// <summary>
        /// Gets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        TVisualLayout ITVisualContainerElement.Layout
        {
            get
            {
                return TVisualLayout.Default;
            }
        }
    }
}
