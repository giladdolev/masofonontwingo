﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    ///  Provides data for System.Web.VisualTree.Elements.TabElement.Selecting event.
    /// </summary>
    public class TabPageChangedEventArgs : EventArgs
    {
        private TabItem _page;
        private TabItem _prevPage;
        /// <summary>
        /// Initializes a new instance of the System.Web.VisualTree.Elements.TabPageChangedEventArgs class.
        /// </summary>
        /// <param name="prevPage">
        ///  An System.Web.VisualTree.Elements.TabItem object which represents the tab page previously
        ///  processed. This value is assigned to the System.Web.VisualTree.Elements.TabPageChangedEventArgs.PrevPage
        ///  property.
        ///  </param>
        /// <param name="page">
        ///  An System.Web.VisualTree.Elements.TabItem object which represents the tab page currently
        ///  being processed. This value is assigned to the System.Web.VisualTree.Elements.TabPageChangedEventArgs.Page
        ///  property.
        /// </param>
        public TabPageChangedEventArgs(TabItem prevPage, TabItem page)
        {
            _prevPage = prevPage;
            _page = page;
        }

        /// <summary>
        /// Gets the tab page currently being processed.
        /// </summary>
        public TabItem Page
        {
            get { return _page; }
        }

        /// <summary>
        /// Gets the previously processed tab page.
        /// </summary>
        public TabItem PrevPage
        {
            get { return _prevPage; }
        }
    }
}
