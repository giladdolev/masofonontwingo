﻿using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.PanelElement" />
    [DesignerComponent]
    public class SplitterPanelElement : PanelElement
    {
        private Dock originalDock = Dock.None;
        /// <summary>
        /// Sets the dock.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="defaultDock">The default dock.</param>
        internal void SetDock(Dock value, Dock defaultDock)
        {
            StoreOriginalDock(defaultDock);
            this.Dock = value;
        }

        /// <summary>
        /// Stores the original dock.
        /// </summary>
        /// <param name="defaultDock">The default dock.</param>
        private void StoreOriginalDock(Dock defaultDock)
        {
            if (originalDock == Dock.None)
            {
                originalDock = (this.Dock == Dock.None) ? defaultDock : this.Dock;
            }
        }

        /// <summary>
        /// Resets the dock.
        /// </summary>
        /// <param name="defaultDock">The default dock.</param>
        internal void ResetDock(Dock defaultDock)
        {
            StoreOriginalDock(defaultDock);
            this.Dock = originalDock;
        }
    }
}
