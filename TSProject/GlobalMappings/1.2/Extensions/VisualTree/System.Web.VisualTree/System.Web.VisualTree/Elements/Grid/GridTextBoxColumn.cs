﻿using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class GridTextBoxColumn : GridColumn
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextBoxColumn"/> class.
        /// </summary>
        public GridTextBoxColumn()
        {

        }

         /// <summary>
         /// Initializes a new instance of the <see cref="GridTextBoxColumn" /> class.
         /// We used a default constructor how use 2 string
         /// </summary>
         /// <param name="columnName">The column name.</param>
         /// <param name="headerText">The header text.</param>
         public GridTextBoxColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(string))
        {
        }

         /// <summary>
         /// Initializes a new instance of the <see cref="GridTextBoxColumn" /> class.
         /// We used a default constructor how use 2 string
         /// </summary>
         /// <param name="columnName">The column name.</param>
         /// <param name="headerText">The header text.</param>
         /// <param name="dataType">The type .</param>
         public GridTextBoxColumn(string columnName, string headerText, System.Type dataType)
             : base(columnName, headerText, dataType)
        {
            
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
         [DesignerIgnore]
         public override string DataTypeName
         {
             get { return typeof(string).Name; }
         }
    }
}
