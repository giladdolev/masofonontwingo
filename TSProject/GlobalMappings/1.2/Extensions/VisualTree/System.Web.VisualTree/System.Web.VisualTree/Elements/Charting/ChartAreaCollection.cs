﻿namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{ChartArea}" />
    public class ChartAreaCollection : VisualElementCollection<ChartArea>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartAreaCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ChartAreaCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {
        }
        
        public object NextUniqueName()
        {
            string name = "ChartArea";

            int nameIndex = 1;

            string uniqueName = name + nameIndex;

            while (this[uniqueName] != null)
            {
                nameIndex++;

                uniqueName = name + nameIndex;
            }

            return uniqueName;      
        }        
    }
}
