namespace System.Web.VisualTree.Elements
{    
	public class ChartingGrid : VisualElement
	{

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartingGrid"/> class.
        /// </summary>
        public ChartingGrid()
        {
            Enabled = true;
        }

		/// <summary>
		///Gets or sets a flag that determines whether major or minor grid lines are enabled.
		/// </summary>
		public bool Enabled
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the interval between major or minor grid lines.
		/// </summary>
		public double Interval
		{
			get;
			set;
		}
	}
}
