﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents an item in a Visual Tree List control.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.VisualElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModelRecord" />
    [DesignerComponent]
    public class ListItem : VisualElement, IBindingViewModelRecord
    {
        private ListItemCollection _dataRow = new ListItemCollection();
        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.
        /// </summary>
        public ListItem()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public ListItem(string text)
            : this(text, text)
        {

        }

       


        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DesignerType(typeof(string))]
        public object Value
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display.
        /// </summary>
        /// <value>
        /// The display.
        /// </value>
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// Get the image index
        /// </summary>
        public int ImageIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ItemData.
        /// </summary>
        public int ItemData
        {
            get;
            set;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="text">The text.</param>
        public ListItem(object value, string text)
            : this(value, text, 0)
        {
           
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem" /> class
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageIndex">Index of the image.</param>
        public ListItem(object value, string text, int imageIndex)
        {
            this.Value = value;
            this.Text = text;
            this.ImageIndex = imageIndex;
            this.ID = String.Format("ListItem_{0}", this.Value.ToString());
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return (this.Text != null) ? this.Text.ToString() : String.Empty;
        }

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        internal static ListItem GetItem(object item)
        {
            ListItem listItem = item as ListItem;
            if (listItem == null)
            {
                listItem = new ListItem(Convert.ToString(item));
            }
            return listItem;
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public IBindingViewModelField GetField(string key)
        {
            // List is not updateable at this point
            return null;
        }

        /// <summary>
        /// Get,set DataRow 
        /// </summary>
        public ListItemCollection DataRow
        {
            get { return _dataRow; }
            set { _dataRow = value; }
        }



    }
}