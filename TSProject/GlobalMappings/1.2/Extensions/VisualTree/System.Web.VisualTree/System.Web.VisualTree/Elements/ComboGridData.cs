﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class ComboGridData
    {
        [DefaultValue("")]
        public string Columns
        {
            get;
            set;
        }

        [DefaultValue("")]
        public string Data
        {
            get;
            set;
        }

        [DefaultValue(false)]
        public bool ShowFilter
        {
            get;
            set;
        }

        public string DisplayMember
        {
            get;
            set;
        }

        public string ValueMember
        {
            get;
            set;
        }
    }

}
