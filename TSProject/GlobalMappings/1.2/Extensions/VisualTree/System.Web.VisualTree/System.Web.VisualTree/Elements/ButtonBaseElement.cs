using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ContentControlElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IButtonElement" />
    public abstract class ButtonBaseElement : ContentControlElement, IButtonElement
    {
        private FlatStyle _flatStyle = FlatStyle.System;
        private ContentAlignment _imageAlign = ContentAlignment.MiddleCenter;
        private int _imageIndex = -1;
        private TextImageRelation _textImageRelation = TextImageRelation.Overlay;
        private ResourceReference _image = null;

        /// <summary>
        /// Gets or sets the flat style.
        /// </summary>
        /// <value>
        /// The flat style.
        /// </value>
        [DefaultValue(FlatStyle.System)]
        public FlatStyle FlatStyle
        {
            get { return _flatStyle; }
            set { _flatStyle = value; }
        }



        /// <summary>
        /// Gets or sets  the image displayed on the button element control.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        [RendererPropertyDescription]
        public ResourceReference Image
        {
            get { return _image; }
            set
            {
                if (this._image != value)
                {
                    this._image = value;
                    OnPropertyChanged("Image");
                }
            }
        }


        /// <summary>
        /// Gets or sets the index of the image.
        /// </summary>
        /// <value>
        /// The index of the image.
        /// </value>
        [DefaultValue(-1)]
        public int ImageIndex
        {
            get { return this._imageIndex; }
            set
            {
                if (this._imageIndex != value)
                {
                    this._imageIndex = value;
                    SetBackGroundImage();
                }
            }
        }

        /// <summary>
        /// set the background image according to imagelist images  
        /// </summary>
        private void SetBackGroundImage()
        {
            if (this.ImageList != null && (this.ImageIndex >= 0 || this.ImageIndex < this.ImageList.Images.Count))
            {
                this.BackgroundImage = this.ImageList.Images[this.ImageIndex];
            }
        }

        /// <summary>
        /// Gets or sets the image align.
        /// </summary>
        /// <value>
        /// The image align.
        /// </value>
        [DefaultValue(ContentAlignment.MiddleCenter)]
        public ContentAlignment ImageAlign
        {
            get { return _imageAlign; }
            set { _imageAlign = value; }
        }




        /// <summary>
        /// Gets or sets a value indicating whether [use visual style back color].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use visual style back color]; otherwise, <c>false</c>.
        /// </value>
        public bool UseVisualStyleBackColor
        {
            get;
            set;
        }




        /// <summary>
        /// Gets or sets the width of the image.
        /// </summary>
        /// <value>
        /// The width of the image.
        /// </value>
        [DesignerIgnore]
        public int ImageWidth { get; set; }




        /// <summary>
        /// Gets or sets the height of the image.
        /// </summary>
        /// <value>
        /// The height of the image.
        /// </value>
        [DesignerIgnore]
        public int ImageHeight { get; set; }

        /// <summary>
        /// The text alignment
        /// </summary>
        [DefaultValue(ContentAlignment.MiddleCenter)]
        private ContentAlignment _TextAlign = ContentAlignment.MiddleCenter;

        /// <summary>
        /// Gets or sets the text alignment.
        /// </summary>
        /// <value>
        /// The text align.
        /// </value>
        [DefaultValue(ContentAlignment.MiddleCenter)]
        public ContentAlignment TextAlign
        {
            get { return _TextAlign; }
            set { _TextAlign = value; }
        }



        /// <summary>
        /// Gets or sets the TextImageRelation value of the control.
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(TextImageRelation.Overlay)]
        public TextImageRelation TextImageRelation
        {
            get { return _textImageRelation; }
            set
            {
                _textImageRelation = value;
                OnPropertyChanged("TextImageRelation");
            }
        }
    }
}
