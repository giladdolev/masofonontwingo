﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{

    public class HTMLElement : PanelElement
    {
        private string _url = "";
        [RendererPropertyDescription]
        public string Url
        {
            get { return _url; }
            set
            {
                if (this._url != value)
                {
                    this._url = value;
                    OnPropertyChanged("Url");
                }
            }
        }
    }
}
