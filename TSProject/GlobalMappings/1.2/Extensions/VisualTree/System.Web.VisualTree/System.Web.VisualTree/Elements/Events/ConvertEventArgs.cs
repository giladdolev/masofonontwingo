namespace System.Web.VisualTree.Elements
{
	public class ConvertEventArgs : EventArgs
	{
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
		public object Value
		{
			get;
			set;
		}
	}
}
