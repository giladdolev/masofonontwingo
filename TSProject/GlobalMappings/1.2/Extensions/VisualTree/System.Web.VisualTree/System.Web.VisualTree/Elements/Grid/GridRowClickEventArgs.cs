﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridRowClickEventArgs : EventArgs
    {
        private int _rowHandle;

        /// <summary>
        /// Initializes a new instance of the RowClickEventArgs class with the specified settings.
        /// </summary>
        /// <param name="rowHandle"> 
        /// An integer value that specifies the handle of the clicked row. This value is
        /// assigned to the System.Web.VisualTree.Elements.GridRowClickEventArgs.RowHandle property.
        /// </param>
        public GridRowClickEventArgs(int rowHandle)
        {
            _rowHandle = rowHandle;
        }

        /// <summary>
        /// Gets the handle of the clicked row.
        /// </summary>
        public int RowHandle 
        { 
            get 
            {
                return _rowHandle;
            } 
        }
    }
}
