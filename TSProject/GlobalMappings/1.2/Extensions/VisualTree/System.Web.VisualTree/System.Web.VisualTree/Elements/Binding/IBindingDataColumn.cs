﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// Provides support for binding columns
    /// </summary>
    public interface IBindingDataColumn
    {

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name
        {
            get;
        }


        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type Type
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid
        {
            get;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        object GetValue(object item, int index);

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        bool IsIndex
        {
            get;
        }


        /// <summary>
        /// Binds the specified property information.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        void Bind(PropertyInfo propertyInfo);

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        string DataTypeName { get; }

        /// <summary>
        /// sort type mode 
        /// </summary>
        ColumnSortMode SortType
        {
            get;
            set;
        }
    }


}
