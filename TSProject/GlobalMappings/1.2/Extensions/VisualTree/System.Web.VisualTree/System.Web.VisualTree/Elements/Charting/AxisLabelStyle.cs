namespace System.Web.VisualTree.Elements
{    
	public class AxisLabelStyle : ChartItemElement
	{
		/// <summary>
		///Gets or sets a flag that indicates whether the label is enabled.
		/// </summary>
		public bool Enabled
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the formatting string for the label text.
		/// </summary>
		public string Format
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the size of the label interval.
		/// </summary>
		public double Interval
		{
			get;
			set;
		}

        public AxisLabelStyle()
        {
            Enabled = true;
        }
	}
}
