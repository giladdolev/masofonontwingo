﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace System.Web.VisualTree.Elements
{
    public class SelectedListViewIndexCollection : Collection<Int32>
    {

        public SelectedListViewIndexCollection(IList<Int32> list)
            : base(list)
        {

        }

        public SelectedListViewIndexCollection():base()
        {
        }

    }
}
