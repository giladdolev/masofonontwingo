namespace System.Web.VisualTree.Elements
{
    public class SaveFileDialog : FileDialog
	{
        /// <summary>
        /// Gets or sets a value indicating whether [overwrite prompt].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [overwrite prompt]; otherwise, <c>false</c>.
        /// </value>
		public bool OverwritePrompt
		{
			get;
			set;
		}
	}
}
