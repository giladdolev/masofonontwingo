namespace System.Web.VisualTree.Elements
{
	public class LineElement : ControlElement
	{
		public LineElement()
		{
            this.BorderStyle = VisualTree.BorderStyle.Solid;
            this.PixelHeight = 1;
		}


        public int X1
        {
            get;
            set;
        }


		public int Y1
		{
			get;
			set;
		}


		public int X2
		{
			get;
			set;
		}


		public int Y2
		{
			get;
			set;
		}

	}
}
