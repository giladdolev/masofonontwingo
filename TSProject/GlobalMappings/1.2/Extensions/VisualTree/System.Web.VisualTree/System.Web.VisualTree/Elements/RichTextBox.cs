﻿using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class RichTextBox : TextBoxBaseElement
    {
        private bool _hideSelection = true;
        private string _textRtf = string.Empty;
        private ScrollBars _scrollBars = ScrollBars.Both;

        private Color _selectionColor;
        /// <summary>
        /// get or set the selection color of selected text in htmlEditor
        /// </summary> 
        [RendererPropertyDescription]
        public Color SelectionColor
        {
            get { return this._selectionColor; }
            set
            {
                this._selectionColor = value;
                OnPropertyChanged("SelectionColor");
            }
        }



        private Font _selectionFont;
        /// <summary>
        /// get or set the Selection Font of selected text in htmlEditor
        /// </summary>
        [RendererPropertyDescription]
        public Font SelectionFont
        {
            get { return this._selectionFont; }
            set
            {
                this._selectionFont = value;
                FontName = this._selectionFont.Name;
                FontSize = this._selectionFont.Size;
                //TODO FontStyle
                OnPropertyChanged("SelectionFont");
            }
        }

        private string _selectedText;
        /// <summary>
        /// get or set the Selected Text of selected text in htmlEditor
        /// </summary>
        [RendererPropertyDescription]
        public string SelectedText
        {
            get { return this._selectedText; }
            set
            {
                this._selectedText = value;
                OnPropertyChanged("SelectedText");
            }
        }



        private string _value;
        /// <summary>
        /// Get or Set the value of htmlEditor
        /// </summary>
        [RendererPropertyDescription]
        public string Value
        {
            get { return this._value; }
            set
            {
                value = value.Replace("\n", "</br>");
                string tagged = "<span style=\"white-space: pre; color:" + this.SelectionColor.Name + "; font-family: " + this.FontName + "; font-size: " + this.FontSize + "px;  \" >" + value + "</span>";
                this._value = tagged;
                OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Gets or sets the scroll bars.
        /// </summary>
        /// <value>
        /// The scroll bars.
        /// </value>
        [DefaultValue(ScrollBars.Both)]
        public ScrollBars ScrollBars
        {
            get { return _scrollBars; }
            set
            {
                _scrollBars = value;

                // Notify property ScaleMode changed
                OnPropertyChanged("ScrollBars");
            }
        }


        
        /// <summary>
        /// Setting the TextRTF property replaces the entire contents of a RichTextBox control with the new string.
        /// You can use the TextRTF property along with the Print function to write .rtf files. 
        /// The resulting file can be read by any other word processor capable of reading RTF-encoded text.
        /// </summary>
        /// <value>
        /// The RTF text.
        /// </value>
        [DefaultValue("")]
        public string TextRtf
        {
            get { return _textRtf; }
            set
            {
                _textRtf = value;

                // Notify property ScaleMode changed
                OnPropertyChanged("TextRtf");
            }
        }

        


        /// <summary>
        /// Gets or sets a value indicating whether the selected text in the text box control remains highlighted when the control loses focus.
        /// </summary>
        /// <value>
        ///   <c>true</c> if hide selection; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool HideSelection
        {
            get { return _hideSelection; }
            set
            {
                _hideSelection = value;

                // Notify property ScaleMode changed
                OnPropertyChanged("HideSelection");
            }
        }
        public string FontName
        {
            get;
            set;
        }

        public float FontSize
        {
            get;
            set;
        }

        public int TextLength
        {
            get;
            set;
        }
        /// <summary>
        /// Clear Method 
        /// </summary>
        public void Clear()
        {
            this.Text = "";
            this._value = "";
        }
    }
}
