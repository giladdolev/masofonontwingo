﻿namespace System.Web.VisualTree.Elements
{
    public enum GridCellBorderStyle
    {
        None, //No borders.
        Single, //A single line border.
        SingleVertical, //A vertical single-line border.
        SingleHorizontal, //A horizontal single-line border.
    }
}
