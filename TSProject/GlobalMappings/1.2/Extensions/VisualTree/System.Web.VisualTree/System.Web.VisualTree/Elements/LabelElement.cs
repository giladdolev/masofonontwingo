using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class LabelElement : ContentControlElement
    {
        private FlatStyle _flatStyle = FlatStyle.Standard;
        private int _maxWidth = -1;
        private int _maxHeight = -1;
        private ContentAlignment _imageAlign = ContentAlignment.MiddleCenter;
        private ResourceReference _image = new ResourceReference();

        public LabelElement()
        {

        }



        /// <summary>
        /// Gets or sets the alignment of text in the label element.
        /// </summary>
        private ContentAlignment _textAlign = ContentAlignment.TopLeft;

        public virtual ContentAlignment TextAlign
        {
            get { return _textAlign; }
            set
            {
                if (_textAlign != value)
                {
                    _textAlign = value;
                    OnPropertyChanged("TextAlign");
                }
            }

        }


        [RendererPropertyDescription]
        [DefaultValue(-1)]
        /// <summary>
        /// The maximum value in pixels which this Component will set its width to.
        /// </summary>
        public int MaxWidth
        {
            get { return _maxWidth; }
            set
            {
                _maxWidth = value;
                OnPropertyChanged("Width");
            }
        }

        [RendererPropertyDescription]
        [DefaultValue(-1)]
        /// <summary>
        /// The maximum value in pixels which this Component will set its height to .
        /// </summary>
        public int MaxHeight
        {
            get { return _maxHeight; }
            set
            {
                _maxHeight = value;
                OnPropertyChanged("MaxHeight");
            }
        }

        private bool _AutoEllipsis = false;
        /// <summary>
        /// Gets or sets a value indicating whether the ellipsis character (...) appears
        //     at the right edge of the System.Web.VisualTree Label, denoting that the System.Web.VisualTree Label
        //     text extends beyond the specified length of the System.Web.VisualTree Label.
        /// </summary>
        /// <value>
        /// <c>true</c> if the additional label text is to be indicated by an ellipsis; otherwise,
        //     <c>false</c>. The default is <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Behavior")]
        public bool AutoEllipsis
        {
            get { return _AutoEllipsis; }
            set
            {
                if (_AutoEllipsis != value)
                {
                    _AutoEllipsis = value;

                    OnPropertyChanged("AutoEllipsis");
                }
            }
        }



        public virtual int PreferredWidth
        {
            get { return default(int); }
        }



        [DefaultValue(FlatStyle.Standard)]
        public FlatStyle FlatStyle
        {
            get { return _flatStyle; }
            set { _flatStyle = value; }
        }



        public bool WordWrap
        {
            get;
            set;
        }

        private object _content = null;
        /// <summary>
        /// The Content of the Label
        /// </summary>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        public override object Content
        {
            get { return _content; }
            set
            {
                _content = value;
                OnPropertyChanged("Content");
            }
        }

        /// <summary>
        /// the image of the label.
        /// </summary>
        [RendererPropertyDescription]
        public ResourceReference Image
        {
            get { return _image; }
            set
            {
                _image = value;
                // Notify property Image changed
                OnPropertyChanged("Image");
            }
        }

        /// <summary>
        /// Gets or sets the alignment of an image that is displayed in the control.
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(ContentAlignment.MiddleCenter)]
        public ContentAlignment ImageAlign
        {
            get { return _imageAlign; }
            set
            {
                if (_imageAlign != value)
                {
                    _imageAlign = value;
                    OnPropertyChanged("ImageAlign");
                }
            }

        }
    }
}
