namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{Title}" />
	public class TitleCollection : VisualElementCollection<ChartTitle>
	{

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleCollection" /> class.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="propertyName">The property name.</param>
        public TitleCollection(Chart chart, string propertyName)
            : base(chart, propertyName)
        {
            
        }

		/// <summary>
		///Adds a T:System.Windows.Forms.DataVisualization.Charting.Title object with the specified name to the end of the collection.
		/// </summary>
		/// <param name="name">The name of the title to add to the collection.</param>
		///  <returns>Index of the added object.</returns>
		public ChartTitle Add(string name)
		{
            // Create, add and return title
            ChartTitle title = new ChartTitle(name);
            Add(title);
            return title;
		}
	}
}
