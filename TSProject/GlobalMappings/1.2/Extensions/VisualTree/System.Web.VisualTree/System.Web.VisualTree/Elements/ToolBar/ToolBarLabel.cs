namespace System.Web.VisualTree.Elements
{
	public class ToolBarLabel : ToolBarItem
	{
		/// <summary>
		///Gets or sets a value indicating whether the T:System.Windows.Forms.ToolStripStatusLabel automatically fills the available space on the T:System.Windows.Forms.StatusStrip as the form is resized. 
		/// </summary>
		public bool Spring
		{
			get;
			set;
		}
	}
}
