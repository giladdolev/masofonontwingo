﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class GridTextButtonColumn : GridColumn
    {
        private EventHandler _ellipsesButtonClick;
        private string _buttonText = "";
        private bool _textReadOnly = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn"/> class.
        /// </summary>
        public GridTextButtonColumn()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridTextButtonColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(string))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridTextButtonColumn" /> class.
        /// We used a default constructor how use 3 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// /// <param name="dataType">The dataType .</param>
        public GridTextButtonColumn(string columnName, string headerText, System.Type dataType)
            : base(columnName, headerText, dataType)
        {

        }


        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [DefaultValue("")]
        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                // If the new value is not same as the old value
                if (_buttonText != value)
                {
                    // Set the text value
                    _buttonText = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the read only of text.
        /// </summary>
        /// <value>
        /// The read only state.
        /// </value>
        [DefaultValue("")]
        public bool TextReadOnly
        {
            get { return _textReadOnly; }
            set
            {
                // If the new value is not same as the old value
                if (_textReadOnly != value)
                {
                    // Set the read only  value.
                    _textReadOnly = value;
                }
            }
        }

    }
}
