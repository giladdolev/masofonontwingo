﻿namespace System.Web.VisualTree.Elements
{
    public struct TableLayoutPanelCellPosition
    {
        /// <summary>
        /// The column
        /// </summary>
        private int _column;

        /// <summary>
        /// The row
        /// </summary>
        private int _row;

        /// <summary>
        /// The row span
        /// </summary>
        private int _rowSpan;

        /// <summary>
        /// The column span
        /// </summary>
        private int _columnSpan;



        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutPanelCellPosition"/> struct.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="row">The row.</param>
        public TableLayoutPanelCellPosition(int column, int row)
        {
            _column = column;
            _row = row;
            _rowSpan = -1;
            _columnSpan = -1;
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutPanelCellPosition"/> struct.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <param name="row">The row.</param>
        /// <param name="columnSpan">The column span.</param>
        /// <param name="rowSpan">The row span.</param>
        public TableLayoutPanelCellPosition(int column, int row, int columnSpan, int rowSpan)
        {
            _column = column;
            _row = row;
            _rowSpan = columnSpan;
            _columnSpan = rowSpan;
        }



        /// <summary>
        /// Gets the row span.
        /// </summary>
        /// <value>
        /// The row span.
        /// </value>
        public int RowSpan
        {
            get
            {
                return _rowSpan;
            }
        }



        /// <summary>
        /// Gets the column span.
        /// </summary>
        /// <value>
        /// The column span.
        /// </value>
        public int ColumnSpan
        {
            get
            {
                return _columnSpan;
            }
        }



        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        public int Row
        {
            get { return _row; }
            set { _row = value; }
        }



        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public int Column
        {
            get { return _column; }
            set { _column = value; }
        }



        /// <summary>
        /// Gets the required columns.
        /// </summary>
        /// <value>
        /// The required columns.
        /// </value>
        public int RequiredColumns
        {
            get
            {
                return _column + _columnSpan;
            }
        }



        /// <summary>
        /// Gets the required rows.
        /// </summary>
        /// <value>
        /// The required rows.
        /// </value>
        public int RequiredRows
        {
            get
            {
                return _row + _rowSpan;
            }
        }



        public override int GetHashCode()
        {
            return _row ^ _column;
        }  



        public override bool Equals(object obj)
        {
            if (!(obj is TableLayoutPanelCellPosition))
                return false;

            return Equals((TableLayoutPanelCellPosition)obj);
        }



        public bool Equals(TableLayoutPanelCellPosition other)
        {
            if (_row != other._row)
                return false;


            return _column == other._column;
        }



        public static bool operator ==(TableLayoutPanelCellPosition tableLayout1, TableLayoutPanelCellPosition tableLayout2)
        {
            return tableLayout1.Equals(tableLayout2);
        }



        public static bool operator !=(TableLayoutPanelCellPosition tableLayout1, TableLayoutPanelCellPosition tableLayout2)
        {
            return !tableLayout1.Equals(tableLayout2);
        }  
    }
}
