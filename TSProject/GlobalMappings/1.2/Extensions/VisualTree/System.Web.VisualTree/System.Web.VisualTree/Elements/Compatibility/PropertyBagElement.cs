﻿namespace System.Web.VisualTree.Elements.Compatibility
{
    public class PropertyBagElement : VisualElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyBagElement"/> class.
        /// </summary>
        public PropertyBagElement()
        {

        }




        /// <summary>
        /// Gets or sets the contents.
        /// </summary>
        /// <value>
        /// The contents.
        /// </value>
        public object Contents
        {
            get;
            set;
        }




        /// <summary>
        /// Reads the property.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public object ReadProperty(string name)
        {
            return ReadProperty(name, null);
        }




        /// <summary>
        /// Reads the property.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public object ReadProperty(string name, object defaultValue)
        {
            return null;
        }




        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void WriteProperty(string name, object value)
        {
            WriteProperty(name, value, null);
        }




        /// <summary>
        /// Writes the property.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        public void WriteProperty(string name, object value, object defaultValue)
        {

        }
    }
}
