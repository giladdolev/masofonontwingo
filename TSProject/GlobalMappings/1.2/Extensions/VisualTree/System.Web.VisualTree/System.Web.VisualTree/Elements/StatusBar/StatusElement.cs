using System.ComponentModel;

namespace System.Web.VisualTree.Elements
{
	public class StatusElement : ToolBarElement
	{
	    private bool _sizingGrip = true;


	    /// <summary>
        /// Initializes a new instance of the <see cref="StatusElement"/> class.
        /// </summary>
        public StatusElement()
        {

        }

        /// <summary>
        /// Gets the default dock.
        /// </summary>
        /// <value>
        /// The default dock.
        /// </value>
        protected override Dock DefaultDock
        {
            get
            {
                return Dock.Bottom;
            }
        }



	    [DefaultValue(true)]
	    public bool SizingGrip
	    {
	        get { return _sizingGrip; }
	        set { _sizingGrip = value; }
	    }
	}
}
