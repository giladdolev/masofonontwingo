﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.Mvc.VisualTree.Elements
{
    public class TabElement : ControlContainerElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TabElement"/> class.
        /// </summary>
        public TabElement()
        {

        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="objVisualElement">The visual element.</param>
        public override void Add(VisualElement objVisualElement)
        {
            // If is a control
            if(objVisualElement is ControlElement)
            {
                // Validate the control is only a tab page
                if(!(objVisualElement is TabItem))
                {
                    // Don't add element
                    return;
                }
            }
            base.Add(objVisualElement);
        }

    }
}
