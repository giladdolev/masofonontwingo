using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public class TablePanel : PanelElement
	{
        private TablePanelGrowStyle _growStyle = TablePanelGrowStyle.AddRows;

        /// <summary>
        /// The column styles
        /// </summary>
        private TableLayoutColumnStyleCollection _columnStyles = null;

        /// <summary>
        /// The row styles
        /// </summary>
        private TableLayoutRowStyleCollection _rowStyles = null;



	    /// <summary>
        /// Initializes a new instance of the <see cref="TablePanel"/> class.
        /// </summary>
        public TablePanel()
        {
            _columnStyles = new TableLayoutColumnStyleCollection(this, "ColumnStyle");
            _rowStyles = new TableLayoutRowStyleCollection(this, "RowStyles");
        }        



        /// <summary>
        /// Gets the column styles.
        /// </summary>
        /// <value>
        /// The column styles.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ColumnStyle>))]
        public TableLayoutColumnStyleCollection ColumnStyles
        {
            get
            {
                return _columnStyles;
            }
        }



        /// <summary>
        /// Gets the row styles.
        /// </summary>
        /// <value>
        /// The row styles.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<RowStyle>))]
        public TableLayoutRowStyleCollection RowStyles
        {
            get
            {
                return _rowStyles;
            }
        }



        /// <summary>
        /// Gets or sets the layout settings.
        /// </summary>
        /// <value>
        /// The layout settings.
        /// </value>
        public TablePanelLayoutSettings LayoutSettings
        {
            get
            {
                return new TablePanelLayoutSettings(this);
            }
            set
            {
                if(value != null)
                {
                    value.Apply(this);
                }
            }
        }



		/// <summary>
		/// Gets the controls.
		/// </summary>
		/// <value>
		/// The controls.
		/// </value>
		public new TableControlContainerElement Controls
		{
			get { return (TableControlContainerElement)base.Controls; }
		}


        /// <summary>
        /// Gets or sets the column count.
        /// </summary>
        /// <value>
        /// The column count.
        /// </value>
        public int ColumnCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the row count.
        /// </summary>
        /// <value>
        /// The row count.
        /// </value>
        public int RowCount
        {
            get;
            set;
        }



        /// <summary>
        /// Creates the control element collection.
        /// </summary>
        /// <returns></returns>
        protected override ControlElementCollection CreateControlElementCollection()
        {
            return new TableControlContainerElement(this, "Controls");
        }



        /// <summary>
        /// Sets the row span.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The value.</param>
		public void SetRowSpan(ControlElement control, int value)
		{
            if(control != null)
            {
                control.ControlLayout = new ControlElementTableLayout(control.ControlLayout, -1, -1, -1, value);
            }
		}

        /// <summary>
        /// Sets the column span.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="value">The value.</param>
		public void SetColumnSpan(ControlElement control, int value)
		{
            if (control != null)
            {
                control.ControlLayout = new ControlElementTableLayout(control.ControlLayout, -1, -1, value, -1);
            }
		}

        /// <summary>
        /// Gets the row span.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
		public int GetRowSpan(ControlElement control)
		{
            // validate parameter
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            // Get the table layout
            ControlElementTableLayout tableLayout = control.ControlLayout as ControlElementTableLayout;

            // If there is a valid table layou
            if(tableLayout != null)
            {
                // Return raw span
                return tableLayout.RowSpan;
            }

			return 0;
		}



        /// <summary>
        /// Gets the column span.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        public int GetColumnSpan(ControlElement control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control"); 
            }

            // Get the table layout
            ControlElementTableLayout tableLayout = control.ControlLayout as ControlElementTableLayout;

            // If there is a valid table layou
            if (tableLayout != null)
            {
                // Return column span
                return tableLayout.ColumnSpan;
            }

            return 0;
        }



        /// <summary>
        /// Gets the column.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        public int GetColumn(ControlElement control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }
            // Get the table layout
            ControlElementTableLayout tableLayout = control.ControlLayout as ControlElementTableLayout;

            // If there is a valid table layou
            if (tableLayout != null)
            {
                // Return column 
                return tableLayout.Column;
            }

            return 0;
        }



        /// <summary>
        /// Gets the row.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        public int GetRow(ControlElement control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            // Get the table layout
            ControlElementTableLayout tableLayout = control.ControlLayout as ControlElementTableLayout;

            // If there is a valid table layou
            if (tableLayout != null)
            {
                // Return row 
                return tableLayout.Row;
            }

            return 0;
        }



	    /// <summary>
	    /// Gets or sets the grow style.
	    /// </summary>
	    /// <value>
	    /// The grow style.
	    /// </value>
	    [DefaultValue(TablePanelGrowStyle.AddRows)]
	    public TablePanelGrowStyle GrowStyle
	    {
	        get { return _growStyle; }
	        set { _growStyle = value; }
	    }



	    /// <summary>
	    /// Sets the cell position.
	    /// </summary>
	    /// <param name="control">The control.</param>
	    /// <param name="position">The position.</param>
	    public void SetCellPosition(ControlElement control, TableLayoutPanelCellPosition position)
		{
            if (control != null)
            {
                // Create the element table layout
                ControlElementTableLayout elementTableLayout = new ControlElementTableLayout(control.ControlLayout, position.Column, position.Row, position.ColumnSpan, position.RowSpan); 

                // If we need to enlarge the columns
                if (ColumnCount < elementTableLayout.RequiredColumns)
                {
                    ColumnCount = elementTableLayout.RequiredColumns;
                }

                // If we need to enlarge the rows
                if (RowCount < elementTableLayout.RequiredRows)
                {
                    RowCount = elementTableLayout.RequiredRows;
                }

                // Set table layout
                control.ControlLayout = elementTableLayout;
            }
		}
	}
}
