using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignTimeVisible(false)]
    public class ScrollBarElement : ControlElement
	{
        private int _largeChange = 10;
        private int _smallChange = 1;
        private int _maximum = 100;


        /// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public int Value
		{
			get;
			set;
		}


        
        /// <summary>
        /// Gets or sets the large change.
        /// </summary>
        /// <value>
        /// The large change.
        /// </value>
        [DefaultValue(10)]
        public int LargeChange
        {
            get { return _largeChange; }
            set { _largeChange = value; }
        }

        

        /// <summary>
        /// Gets or sets the small change.
        /// </summary>
        /// <value>
        /// The small change.
        /// </value>
        [DefaultValue(1)]
        public int SmallChange
        {
            get { return _smallChange; }
            set { _smallChange = value; }
        }
        


        /// <summary>
        /// Gets or sets the maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        [DefaultValue(100)]
        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; }
        }




        /// <summary>
        /// Gets or sets the minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public int Minimum
		{
			get;
			set;
		}


        /// <summary>
        /// ValueChanged event handler.
        /// </summary>
        private event EventHandler _ValueChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ValueChanged action.
        /// </summary>
        public event EventHandler ValueChanged
        {
            add
            {
                bool needNotification = (_ValueChanged == null);

                _ValueChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ValueChanged");
                }

            }
            remove
            {
                _ValueChanged -= value;

                if (_ValueChanged == null)
                {
                    OnEventHandlerDeattached("ValueChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ValueChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasValueChangedListeners
        {
            get { return _ValueChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ValueChanged"/> instance containing the event data.</param>
        protected virtual void OnValueChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_ValueChanged != null)
            {
                _ValueChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformValueChanged(EventArgs args)
        {

            // Raise the ValueChanged event.
            OnValueChanged(args);
        }


        /// <summary>
        /// Scroll event handler.
        /// </summary>
        private event EventHandler _Scroll;

        [RendererEventDescription]
        /// <summary>
        /// Occurs when the scroll box has been moved by either a mouse or keyboard action.
        /// </summary>
        public event EventHandler Scroll
        {
            add
            {
                bool needNotification = (_Scroll == null);

                _Scroll += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Scroll");
                }

            }
            remove
            {
                _Scroll -= value;

                if (_Scroll == null)
                {
                    OnEventHandlerDeattached("Scroll");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Scroll listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasScrollListeners
        {
            get { return _Scroll != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Scroll" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Scroll"/> instance containing the event data.</param>
        protected virtual void OnScroll(EventArgs args)
        {

            // Check if there are listeners.
            if (_Scroll != null)
            {
                _Scroll(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Scroll" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformScroll(EventArgs args)
        {

            // Raise the Scroll event.
            OnScroll(args);
        }

	}
}
