using System.ComponentModel;
using System.Diagnostics;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;


namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for posting back to the server after a given interval
    /// </summary>
    [DesignerComponent]
    public class TimerElement : VisualElement, ITimer
    {

        /// <summary>
        /// 
        /// </summary>
		private bool _enabled = false;

        /// <summary>
        /// 
        /// </summary>
		private int _interval = 10000;

        /// <summary>
        /// 
        /// </summary>
		private long _lastTicks = 0;

		/// <summary>
		/// Occurs when the specified timer interval has elapsed and the timer is enabled.
		/// </summary>
		public event EventHandler Tick;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimerElement"/> class.
        /// </summary>
        /// <param name="objContainer">An <see cref="T:System.ComponentModel.IContainer"></see> that represents the container for the timer.</param>
		public TimerElement(IContainer objContainer) : this()
		{
  
		}


        /// <summary>
        /// Initializes a new instance of the <see cref="TimerElement"/> class.
        /// </summary>
        public TimerElement()
		{
			
		}
		

		/// <summary>
		/// Gets or sets whether the timer is running.
		/// </summary>
		/// <returns>true if the timer is currently enabled; otherwise, false. The default is false.</returns>
		public virtual bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				if (this._enabled != value)
				{
                    // Get timer handler
                    ITimerHandler timerHandler = ApplicationElement.Current as ITimerHandler;
                    if(timerHandler != null)
                    {
                        if(value)
                        {
                            this._lastTicks = DateTime.Now.Ticks;
                            timerHandler.AddTimer(this);
                        }
                        else
                        {
                            timerHandler.RemoveTimer(this);
                        }
                    }



					this._enabled = value;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the time, in milliseconds, between timer ticks.
		/// </summary>
		/// <returns>The number of milliseconds between each timer tick. The value is not less than one.</returns>
		public int Interval
		{
			get
			{
				return this._interval;
			}
			set
			{
				if (this._interval != value)
				{
					this._interval = value;
				}
			}
		}
		
		/// <summary>
		/// Starts the timer.
		/// </summary>
		/// <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" /><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /></PermissionSet>
		public void Start()
		{
			this.Enabled = true;
		}
		
		/// <summary>
		/// Stops the timer.
		/// </summary>
		/// <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" /><IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /></PermissionSet>
		public void Stop()
		{
			this.Enabled = false;
		}
		
		/// <summary>
        /// Returns a string that represents the <see cref="T:Gizmox.WebGUI.Forms.Timer"></see>.
		/// </summary>
        /// <returns>A string that represents the current <see cref="T:Gizmox.WebGUI.Forms.Timer"></see>. </returns>
		public override string ToString()
		{
            string strText1 = base.ToString();
            return (strText1 + ", Interval: " + this.Interval.ToString());
		}
		

		/// <summary>
		/// Disposes of the resources (other than memory) used by the timer.
		/// </summary>
		protected override void Dispose(bool blnDisposing)
		{
            if (blnDisposing)
			{
				this.Enabled = false;
			}
            base.Dispose(blnDisposing);
		}
		
		/// <summary>
        /// Raises the <see cref="E:Gizmox.WebGUI.Forms.Timer.Tick"></see> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data. This is always <see cref="F:System.EventArgs.Empty"></see>. </param>
		protected virtual void OnTick(EventArgs e)
		{
            // Raise event if needed
            EventHandler objEventHandler = this.Tick;
            if (objEventHandler != null)
            {
                objEventHandler(this, e);
            }
		}
		

        /// <summary>
        /// Gets the timer interval
        /// </summary>
        int ITimer.Interval
		{
			get
			{
				return this.Interval;
			}
		}

		/// <summary>
		/// Gets timer enabled state
		/// </summary>
		bool ITimer.Enabled
		{
			get
			{
				return this.Enabled;
			}
		}

		/// <summary>
		/// Invoke timer.
		/// </summary>
		int ITimer.InvokeTimer()
		{
			this._lastTicks = DateTime.Now.Ticks;

            try
            {
                this.OnTick(EventArgs.Empty);
            }
            catch (Exception objException)
            {
                throw objException;
            }

			return ((ITimer)this).GetNextInvokation(_lastTicks);
		}

		/// <summary>
		/// Gets the next planed invocation.
		/// </summary>
		/// <param name="currentTicks"></param>
		int ITimer.GetNextInvokation(long currentTicks)
		{


			// Get passed ticks from last execution
			TimeSpan timeSpan = TimeSpan.FromTicks(currentTicks-this._lastTicks);

			// Get milliseconds to next execution
			int milliseconds = _interval - (int)timeSpan.TotalMilliseconds;
			
			// Validate value
			if(milliseconds < 0)
			{
				milliseconds = 0;
			}
			
			// Return milliseconds to next execution
			return milliseconds;
		}
	
	

    }
}
