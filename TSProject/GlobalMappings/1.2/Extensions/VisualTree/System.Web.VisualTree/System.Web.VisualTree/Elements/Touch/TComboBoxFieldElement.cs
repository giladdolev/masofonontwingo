﻿namespace System.Web.VisualTree.Elements.Touch
{
    public class TComboBoxFieldElement : TComboBoxElement, ITFormFieldElement
    {
        public string Label
        {
            get;
            set;
        }
    }
}