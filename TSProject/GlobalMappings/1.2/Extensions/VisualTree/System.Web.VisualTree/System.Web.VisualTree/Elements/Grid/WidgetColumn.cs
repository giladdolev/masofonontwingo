﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class WidgetColumn : GridColumn
    {

        private string _dataMembers = "";
        private string _comboStore = "";
        private string _comboFields = "";

        public WidgetColumn()
        {
            Items = new ControlElementCollection(this, "Items");
        }

        /// <summary>
        /// 
        /// </summary>
        [RendererPropertyDescription]
        public string DataMembers
        {
            get { return this._dataMembers; }
            set
            {
                this._dataMembers = value;
                OnPropertyChanged("DataMembers");
            }
        }


        public ControlElementCollection Items
        {
            get;
            set;
        }

        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }

        [RendererMethodDescription]
        internal void SetFocus(WidgetColumnData widgetColumnData)
        {
            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                this.InvokeMethod("SetFocus", widgetColumnData);
            }
        }

        [RendererMethodDescription]
        internal void SetEnabled(WidgetColumnData widgetColumnData)
        {
            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                this.InvokeMethod("SetEnabled", widgetColumnData);
            }
        }

        [RendererMethodDescription]
        internal void SetBackColor(WidgetColumnData widgetColumnData)
        {
            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                this.InvokeMethod("SetBackColor", widgetColumnData);
            }
        }

        [RendererMethodDescription]
        internal void SetStore(WidgetColumnData widgetColumnData)
        {
            GridElement grd = this.Parent as GridElement;
            if (grd != null)
            {
                this.InvokeMethod("SetStore", widgetColumnData);
            }
        }
    }
}
