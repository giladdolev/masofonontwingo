﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    public class RadioGroupElement : PanelElement
    {
       
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupElement"/> class.
        /// </summary>
        public RadioGroupElement()
        {
        }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get;
            set;
        }


        /// <summary>
        /// Gets the Items.
        /// </summary>
        /// <value>
        /// The Items.
        /// </value>
        public RadioGroupItemCollection Items
        {
            get { return Controls as RadioGroupItemCollection; } 
        }

        /// <summary>
        /// Creates the RadioGroupItem collection.
        /// </summary>
        /// <returns></returns>
        protected override ControlElementCollection CreateControlElementCollection()
        {
            return new RadioGroupItemCollection(this, "Controls"); 
        }


        /// <summary>
        /// Change event handler.
        /// </summary>
        private event EventHandler _Change;

        /// <summary>
        /// Add or remove Change action.
        /// </summary>
        public event EventHandler Change
        {
            add
            {
                bool needNotification = (_Change == null);

                _Change += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Change");
                }

            }
            remove
            {
                _Change -= value;

                if (_Change == null)
                {
                    OnEventHandlerDeattached("Change");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Change listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasChangeListeners
        {
            get { return _Change != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Change" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Change"/> instance containing the event data.</param>
        protected virtual void OnChange(EventArgs args)
        {

            // Check if there are listeners.
            if (_Change != null)
            {
                _Change(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Change" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformChange(EventArgs args)
        {

            // Raise the Change event.
            OnChange(args);
        }
    }
}
