﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for data entry
    /// </summary>
    public class DataEntryElement : ControlElement, ITVisualElement
    {
        /// <summary>
        /// The data source
        /// </summary>
        private object _dataSource;

        /// <summary>
        /// The item source
        /// </summary>
        private object _itemSource;



        public FlowDirection FlowDirection
        {
            get;
            set;
        }



        public System.Drawing.Size ItemDefaultSize
        {
            get;
            set;
        }


        [DesignerIgnore]
        public ControlElement PanelContainer
        {
            get;
            set;
        }



        public int ColumnCount
        {
            get;
            set;
        }



        public bool FitToParentWidth
        {
            get;
            set;
        }



        public int ItemSpace
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(null)]
        public object DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                // Set the data source
                _dataSource = value;

                // Set the item source
                _itemSource = GetItemSource(value);

                // Handle data source changed
                OnPropertyChanged("DataSource");
            }
        }



        /// <summary>
        /// Gets the item source.
        /// </summary>
        /// <value>
        /// The item source.
        /// </value>
        public object ItemSource
        {
            get
            {
                return _itemSource;
            }
        }



        /// <summary>
        /// Gets the item properties.
        /// </summary>
        /// <value>
        /// The item properties.
        /// </value>
        public PropertyDescriptorCollection ItemProperties
        {
            get
            {
                // If there is a valid item source
                if (_itemSource != null)
                {
                    // Get item source properties
                    return TypeDescriptor.GetProperties(_itemSource);

                }

                // Return empty array
                return PropertyDescriptorCollection.Empty;
            }
        }


        /// <summary>
        /// Updated event handler.
        /// </summary>
        private event EventHandler _Updated;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Updated action.
        /// </summary>
        public event EventHandler Updated
        {
            add
            {
                bool needNotification = (_Updated == null);

                _Updated += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Updated");
                }

            }
            remove
            {
                _Updated -= value;

                if (_Updated == null)
                {
                    OnEventHandlerDeattached("Updated");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Updated listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasUpdatedListeners
        {
            get { return _Updated != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Updated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Updated"/> instance containing the event data.</param>
        protected virtual void OnUpdated(EventArgs args)
        {

            // Check if there are listeners.
            if (_Updated != null)
            {
                _Updated(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Updated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformUpdated(EventArgs args)
        {

            // Raise the Updated event.
            OnUpdated(args);
        }


        /// <summary>
        /// Gets the item source.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static object GetItemSource(object value)
        {
            // Get list
            IList list = value as IList;

            // If we didn't find a list
            if (list == null)
            {
                // Get list source
                IListSource listSource = value as IListSource;

                // If there is a valid list source
                if (listSource != null)
                {
                    // Get list
                    list = listSource.GetList();
                }
            }

            // If there is a valid list
            if (list != null)
            {
                // If there are list items
                if (list.Count > 0)
                {
                    // Return the first item
                    return list[0];
                }
            }
            return value;
        }
    }
}
