﻿using System.Drawing;

namespace System.Web.VisualTree.Elements
{
    public class GridCellStyle : VisualElement
    {
        /// <value>
        /// The back color.
        /// </value>
        private Color _backColor;


        /// <summary>
        /// The fore color
        /// </summary>
        private Color _foreColor = Color.Empty;


        /// <summary>
        /// The font
        /// </summary>
        private Font _font;


        /// <summary>
        /// The null value
        /// </summary>
        private object _nullValue = "";


        /// <summary>
        /// The format
        /// </summary>
        private string _format = "";


        /// <summary>
        /// The selection back color
        /// </summary>
        private Color mobjSelectionBackColor;


        /// <summary>
        /// The selection fore color
        /// </summary>
        private Color mobjSelectionForeColor;


        /// <summary>
        /// The grid tri state
        /// </summary>
        private GridTristate menmWrapMode;


        /// <summary>
        /// The alignmernt
        /// </summary>
        private GridContentAlignment menmAlignmernt;




        /// <summary>
        /// Gets or sets the back color.
        /// </summary>
        /// <value>
        /// The back color.
        /// </value>
        public Color BackColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                _backColor = value;

                // Notify property BackColor changed
                OnPropertyChanged("BackColor");
            }
        }



        /// <summary>
        /// Gets or sets the color of the fore.
        /// </summary>
        /// <value>
        /// The color of the fore.
        /// </value>
        public Color ForeColor
        {
            get
            {
                return _foreColor;
            }
            set
            {
                _foreColor = value;

                // Notify property ForeColor changed
                OnPropertyChanged("ForeColor");
            }
        }




        /*
         TODO: consider using System.Web.UI.WebControls.Font
         */
        public Font Font
        {
            get { return _font; }
            set
            {
                _font = value;

                // Notify property Font changed
                OnPropertyChanged("Font");
            }
        }



       
        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        public string Format
        {
            get { return _format; }
            set
            {
                _format = value;

                // Notify property Format changed
                OnPropertyChanged("Format");
            }
        }




        /// <summary>
        /// Gets or sets the null value.
        /// </summary>
        /// <value>
        /// The null value.
        /// </value>
        public object NullValue
        {
            get { return _nullValue; }
            set
            {
                _nullValue = value;

                // Notify property NullValue changed
                OnPropertyChanged("NullValue");
            }
        }



        
        /// <summary>
        /// Gets or sets the color of the selection back.
        /// </summary>
        /// <value>
        /// The color of the selection back.
        /// </value>
        public Color SelectionBackColor
        {
            get { return mobjSelectionBackColor; }
            set
            {
                mobjSelectionBackColor = value;

                // Notify property SelectionBackColor changed
                OnPropertyChanged("SelectionBackColor");
            }
        }




        
        /// <summary>
        /// Gets or sets the color of the selection fore.
        /// </summary>
        /// <value>
        /// The color of the selection fore.
        /// </value>
        public Color SelectionForeColor
        {
            get { return mobjSelectionForeColor; }
            set
            {
                mobjSelectionForeColor = value;

                // Notify property SelectionForeColor changed
                OnPropertyChanged("SelectionForeColor");
            }
        }



        
        /// <summary>
        /// Gets or sets the state of the grid tri.
        /// </summary>
        /// <value>
        /// The state of the grid tri.
        /// </value>
        public GridTristate WrapMode
        {
            get { return menmWrapMode; }
            set
            {
                menmWrapMode = value;

                // Notify property GridTriState changed
                OnPropertyChanged("WrapMode");
            }
        }




        

        /// <summary>
        /// Gets or sets the alignment.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        public GridContentAlignment Alignment
        {
            get { return menmAlignmernt; }
            set
            {
                menmAlignmernt = value;

                // Notify property Alignment changed
                OnPropertyChanged("Alignment");
            }
        }

        private CurrencyCountry _countryCurrencyFormat = CurrencyCountry.US;
        /// <summary>
        ///  Gets or sets the country for the currency format  <see cref="Format"/> .
        /// </summary>
        /// <param name="CountryCurrencyFormat">the country for the currency format.</param>
        /// </summary>
        public CurrencyCountry CountryCurrencyFormat
        {
            get
            {
                return _countryCurrencyFormat;
            }
            set
            {
                if (_countryCurrencyFormat != value)
                {
                    _countryCurrencyFormat = value;
                }
            }
        }
    }
}
