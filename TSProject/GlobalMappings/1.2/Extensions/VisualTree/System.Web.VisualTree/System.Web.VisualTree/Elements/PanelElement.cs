using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class PanelElement : ScrollableControl, IWindowTarget, IWindowTargetContainer
    {
        private bool _roundedCorners;
        private bool _floodShowPct;
        private AutoSizeMode _autoSizeMode = AutoSizeMode.GrowOnly;
        private int _minWidth = 25;
        private int _minHeight = 25;

        /// <summary>
        /// Gets or sets the panel1 MinSize .
        /// </summary>
        /// <value>
        /// The panel1 MinSize .
        /// </value>
        [RendererPropertyDescription]
        public int MinWidth
        {
            get { return _minWidth; }
            set
            {
                if (_minWidth != value)
                {
                    _minWidth = value;
                    OnPropertyChanged("MinWidth");
                }
            }
        }

        /// <summary>
        /// Gets or sets the panel1 MinSize .
        /// </summary>
        /// <value>
        /// The panel1 MinSize .
        /// </value>
        [RendererPropertyDescription]
        public int MinHeight
        {
            get { return _minHeight; }
            set
            {
                if (_minHeight != value)
                {
                    _minHeight = value;
                    OnPropertyChanged("MinHeight");
                }
            }
        }

        /// <summary>
        /// BeforeCollapse event handler.
        /// </summary>
        private event EventHandler<EventArgs> _BeforeCollapse;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeCollapse action.
        /// </summary>
        public event EventHandler<EventArgs> BeforeCollapse
        {
            add
            {
                bool needNotification = (_BeforeCollapse == null);

                _BeforeCollapse += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeCollapse");
                }

            }
            remove
            {
                _BeforeCollapse -= value;

                if (_BeforeCollapse == null)
                {
                    OnEventHandlerDeattached("BeforeCollapse");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeCollapse listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeCollapseListeners
        {
            get { return _BeforeCollapse != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeCollapse"/> instance containing the event data.</param>
        protected virtual void OnBeforeCollapse(EventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeCollapse != null)
            {
                _BeforeCollapse(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeCollapse(EventArgs args)
        {

            // Raise the BeforeCollapse event.
            OnBeforeCollapse(args);
        }
        
        /// <summary>
        /// BeforeExpand event handler.
        /// </summary>
        private event EventHandler<EventArgs> _BeforeExpand;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove BeforeExpand action.
        /// </summary>
        public event EventHandler<EventArgs> BeforeExpand
        {
            add
            {
                bool needNotification = (_BeforeExpand == null);

                _BeforeExpand += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeExpand");
                }

            }
            remove
            {
                _BeforeExpand -= value;

                if (_BeforeExpand == null)
                {
                    OnEventHandlerDeattached("BeforeExpand");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has BeforeExpand listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasBeforeExpandListeners
        {
            get { return _BeforeExpand != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeExpand"/> instance containing the event data.</param>
        protected virtual void OnBeforeExpand(EventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeExpand != null)
            {
                _BeforeExpand(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformBeforeExpand(EventArgs args)
        {

            // Raise the BeforeExpand event.
            OnBeforeExpand(args);
        }

        
        /// <summary>
        /// AfterCollapse event handler.
        /// </summary>
        private event EventHandler<EventArgs> _AfterCollapse;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove AfterCollapse action.
        /// </summary>
        public event EventHandler<EventArgs> AfterCollapse
        {
            add
            {
                bool needNotification = (_AfterCollapse == null);

                _AfterCollapse += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AfterCollapse");
                }

            }
            remove
            {
                _AfterCollapse -= value;

                if (_AfterCollapse == null)
                {
                    OnEventHandlerDeattached("AfterCollapse");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AfterCollapse listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAfterCollapseListeners
        {
            get { return _AfterCollapse != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AfterCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AfterCollapse"/> instance containing the event data.</param>
        protected virtual void OnAfterCollapse(EventArgs args)
        {

            // Check if there are listeners.
            if (_AfterCollapse != null)
            {
                _AfterCollapse(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AfterCollapse" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterCollapse(EventArgs args)
        {
            this.Collapsed = true;

            // Raise the AfterCollapse event.
            OnAfterCollapse(args);
        }
        
        /// <summary>
        /// AfterExpand event handler.
        /// </summary>
        private event EventHandler<EventArgs> _AfterExpand;

        /// <summary>
        /// Add or remove AfterExpand action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<EventArgs> AfterExpand
        {
            add
            {
                bool needNotification = (_AfterExpand == null);

                _AfterExpand += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("AfterExpand");
                }

            }
            remove
            {
                _AfterExpand -= value;

                if (_AfterExpand == null)
                {
                    OnEventHandlerDeattached("AfterExpand");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has AfterExpand listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasAfterExpandListeners
        {
            get { return _AfterExpand != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:AfterExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="AfterExpand"/> instance containing the event data.</param>
        protected virtual void OnAfterExpand(EventArgs args)
        {

            // Check if there are listeners.
            if (_AfterExpand != null)
            {
                _AfterExpand(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:AfterExpand" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterExpand(EventArgs args)
        {
            this.Collapsed = false;

            // Raise the AfterExpand event.
            OnAfterExpand(args);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PanelElement"/> class.
        /// </summary>
        public PanelElement()
        {

        }


        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }


        
        [DefaultValue(AutoSizeMode.GrowOnly)]
        public virtual AutoSizeMode AutoSizeMode
        {
            get { return _autoSizeMode; }
            set { _autoSizeMode = value; }
        }



        public bool RoundedCorners
        {
            get { return _roundedCorners; }
            set
            {
                if (_roundedCorners != value)
                {
                    _roundedCorners = value;

                    OnPropertyChanged("RoundedCorners");
                }
            }
        }



        public bool FloodShowPct
        {
            get { return _floodShowPct; }
            set
            {
                if (_floodShowPct != value)
                {
                    _floodShowPct = value;

                    OnPropertyChanged("FloodShowPct");
                }
            }
        }

        private bool _ShowHeader = false;
        /// <summary>
        /// Gets or sets the ShowHeader.
        /// </summary>
        /// <value>
        ///   <c>true</c> if show header; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Appearance")]
        public bool ShowHeader
        {
            get { return _ShowHeader; }
            set
            {
                if (_ShowHeader != value)
                {
                    _ShowHeader = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the window target identifier.
        /// </summary>
        /// <value>
        /// The window target identifier.
        /// </value>
        public string WindowTargetID
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void IWindowTarget.AddWindow(WindowElement windowElement)
        {
            this.AddWindow(windowElement);
        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected virtual void AddWindow(WindowElement windowElement)
        {
            // If there is a valid window
            if (windowElement != null)
            {
                // Clear all controls
                this.Controls.Clear();

                // Ensure it's docked to top
                windowElement.Dock = Dock.Fill;

                // Add window element to panel
                this.Controls.Add(windowElement);

                // Ensure redraw
                this.Refresh();
            }
        }

        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        void IWindowTarget.RemoveWindow(WindowElement windowElement)
        {
            this.RemoveWindow(windowElement);
        }

        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected virtual void RemoveWindow(WindowElement windowElement)
        {
            // If there is a valid window
            if (windowElement != null)
            {
                // Add window element to panel
                this.Controls.Remove(windowElement);

                // Ensure redraw
                this.Refresh();
            }
        }

        private object _content = null;
        /// <summary>
        /// The Content of the Panel
        /// </summary>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        public override object Content
        {
            get { return _content; }
            set
            {
                _content = value;
                OnPropertyChanged("Content");
            }
        }

        private bool _collapsed = false;
        /// <summary>
        /// true to render the panel collapsed, false to render it expanded.
        /// </summary>
        [RendererPropertyDescription]
        public bool Collapsed
        {
            get { return _collapsed; }
            set
            {
                if (_collapsed != value)
                {
                    _collapsed = value;

                    OnExpandCollapse(value);
                    OnPropertyChanged("Collapsed");
                }
            }
        }

        /// <summary>
        /// Called when collapsed property changed to raise events.
        /// </summary>
        /// <param name="collapsed">if set to <c>true</c> collapsed.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void OnExpandCollapse(bool collapsed)
        {
            if (collapsed)
            {
                OnAfterCollapse(EventArgs.Empty);
            }
            else
            {
                OnAfterExpand(EventArgs.Empty);
            }
        }

        private bool _collapsible = false;
        /// <summary>
        /// True to make the panel collapsible and have an expand/collapse toggle Tool added into the header tool button area. 
        /// </summary>
        [RendererPropertyDescription]
        public bool Collapsible
        {
            get { return _collapsible; }
            set
            {
                if (_collapsible != value)
                {
                    _collapsible = value;

                    OnPropertyChanged("Collapsible");
                }
            }
        }
    }
}
