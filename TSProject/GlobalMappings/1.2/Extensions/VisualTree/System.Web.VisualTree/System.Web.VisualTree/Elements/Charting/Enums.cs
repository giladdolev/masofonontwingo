namespace System.Web.VisualTree.Elements
{
	public enum SeriesChartType
	{
		/// <summary>Point chart type.</summary>
		Point,
		/// <summary>FastPoint chart type.</summary>
		FastPoint,
		/// <summary>Bubble chart type.</summary>
		Bubble,
		/// <summary>Line chart type.</summary>
		Line,
		/// <summary>Spline chart type.</summary>
		Spline,
		/// <summary>StepLine chart type.</summary>
		StepLine,
		/// <summary>FastLine chart type.</summary>
		FastLine,
		/// <summary>Bar chart type.</summary>
		Bar,
		/// <summary>Stacked bar chart type.</summary>
		StackedBar,
		/// <summary>Hundred-percent stacked bar chart type.</summary>
		StackedBar100,
		/// <summary>Column chart type.</summary>
		Column,
		/// <summary>Stacked column chart type.</summary>
		StackedColumn,
		/// <summary>Hundred-percent stacked column chart type.</summary>
		StackedColumn100,
		/// <summary>Area chart type.</summary>
		Area,
		/// <summary>Spline area chart type.</summary>
		SplineArea,
		/// <summary>Stacked area chart type.</summary>
		StackedArea,
		/// <summary>Hundred-percent stacked area chart type.</summary>
		StackedArea100,
		/// <summary>Pie chart type.</summary>
		Pie,
		/// <summary>Doughnut chart type.</summary>
		Doughnut,
		/// <summary>Stock chart type.</summary>
		Stock,
		/// <summary>Candlestick chart type.</summary>
		Candlestick,
		/// <summary>Range chart type.</summary>
		Range,
		/// <summary>Spline range chart type.</summary>
		SplineRange,
		/// <summary>RangeBar chart type.</summary>
		RangeBar,
		/// <summary>Range column chart type.</summary>
		RangeColumn,
		/// <summary>Radar chart type.</summary>
		Radar,
		/// <summary>Polar chart type.</summary>
		Polar,
		/// <summary>Error bar chart type.</summary>
		ErrorBar,
		/// <summary>Box plot chart type.</summary>
		Boxplot,
		/// <summary>Renko chart type.</summary>
		Renko,
		/// <summary>ThreeLineBreak chart type.</summary>
		ThreeLineBreak,
		/// <summary>Kagi chart type.</summary>
		Kagi,
		/// <summary>PointAndFigure chart type.</summary>
		PointAndFigure,
		/// <summary>Funnel chart type.</summary>
		Funnel,
		/// <summary>Pyramid chart type.</summary>
		Pyramid
	}



	public enum ScrollType
	{
		/// <summary>The data view is decreased by one small scrolling interval.</summary>
		SmallDecrement,
		/// <summary>The data view is increased by one small scrolling interval.</summary>
		SmallIncrement,
		/// <summary>The data view is decreased by one large scrolling interval.</summary>
		LargeDecrement,
		/// <summary>The data view is increased by one large scrolling interval.</summary>
		LargeIncrement,
		/// <summary>Data is scrolled to the first displayed view.</summary>
		First,
		/// <summary>Data is scrolled to the last displayed view.</summary>
		Last
	}



	public enum MarkerStyle
	{
		/// <summary>No marker is displayed for the series or data point.</summary>
		None,
		/// <summary>A square marker is displayed.</summary>
		Square,
		/// <summary>A circular marker is displayed.</summary>
		Circle,
		/// <summary>A diamond-shaped marker is displayed.</summary>
		Diamond,
		/// <summary>A triangular marker is displayed.</summary>
		Triangle,
		/// <summary>A cross-shaped marker is displayed.</summary>
		Cross,
		/// <summary>A 4-point star-shaped marker is displayed.</summary>
		Star4,
		/// <summary>A 5-point star-shaped marker is displayed.</summary>
		Star5,
		/// <summary>A 6-point star-shaped marker is displayed.</summary>
		Star6,
		/// <summary>A 10-point star-shaped marker is displayed.</summary>
		Star10
	}



	public enum IntervalAutoMode
	{
        /// <summary>
        /// A fixed number of intervals are always created on the axis.
        /// </summary>
        FixedCount,
        /// <summary>
        /// The number of axis intervals depends on the axis length.
        /// </summary>
        VariableCount
    }



	[Flags()]
	public enum GridTickTypes
	{
		/// <summary>No tick mark or grid line is shown.</summary>
		None = 0,
		/// <summary>A tick mark is shown.</summary>
		TickMark = 1,
		/// <summary>A grid line is shown.</summary>
		GridLine = 2,
		/// <summary>A tick mark and a grid line are shown.</summary>
		All = 3
	}



    /// <summary>
    /// 
    /// </summary>
    public enum DateTimeIntervalType
	{
        /// <summary>
        /// Automatically determined by the <see cref="T:System.Web.UI.DataVisualization.Charting.Chart" /> control.
        /// </summary>
        Auto,
        /// <summary>
        /// Interval type is in numerical.
        /// </summary>
        Number,
        /// <summary>
        /// Interval type is in years.
        /// </summary>
        Years,
        /// <summary>
        /// Interval type is in months.
        /// </summary>
        Months,
        /// <summary>
        /// Interval type is in weeks.
        /// </summary>
        Weeks,
        /// <summary>
        /// Interval type is in days.
        /// </summary>
        Days,
        /// <summary>
        /// Interval type is in hours.
        /// </summary>
        Hours,
        /// <summary>
        /// Interval type is in minutes.
        /// </summary>
        Minutes,
        /// <summary>
        /// Interval type is in seconds.
        /// </summary>
        Seconds,
        /// <summary>
        /// Interval type is in milliseconds.
        /// </summary>
        Milliseconds,
        /// <summary>
        /// The IntervalType or IntervalOffsetType property is not set. This value is used for grid lines, tick marks, strip lines and axis labels, and indicates that the interval type is being obtained from the <see cref="T:System.Web.UI.DataVisualization.Charting.Axis" /> object to which the element belongs. Setting this value for an <see cref="T:System.Web.UI.DataVisualization.Charting.Axis" /> object will have no effect.
        /// </summary>
        NotSet
    }



	public enum ChartHatchStyle
	{
		/// <summary>No hatching style.</summary>
		None,
		/// <summary>Backward diagonal style.</summary>
		BackwardDiagonal,
		/// <summary>Cross style.</summary>
		Cross,
		/// <summary>Dark downward diagonal style.</summary>
		DarkDownwardDiagonal,
		/// <summary>Dark horizontal style.</summary>
		DarkHorizontal,
		/// <summary>Dark upward diagonal style.</summary>
		DarkUpwardDiagonal,
		/// <summary>Dark vertical style.</summary>
		DarkVertical,
		/// <summary>Dashed downward diagonal style.</summary>
		DashedDownwardDiagonal,
		/// <summary>Dashed horizontal style.</summary>
		DashedHorizontal,
		/// <summary>Dashed upward diagonal style.</summary>
		DashedUpwardDiagonal,
		/// <summary>Dashed vertical style.</summary>
		DashedVertical,
		/// <summary>Diagonal brick style.</summary>
		DiagonalBrick,
		/// <summary>Diagonal cross style.</summary>
		DiagonalCross,
		/// <summary>Divot style.</summary>
		Divot,
		/// <summary>Dotted diamond style.</summary>
		DottedDiamond,
		/// <summary>Dotted grid style.</summary>
		DottedGrid,
		/// <summary>Forward diagonal style.</summary>
		ForwardDiagonal,
		/// <summary>Horizontal style.</summary>
		Horizontal,
		/// <summary>Horizontal brick style.</summary>
		HorizontalBrick,
		/// <summary>Large checker board style.</summary>
		LargeCheckerboard,
		/// <summary>Large confetti style.</summary>
		LargeConfetti,
		/// <summary>Large grid style.</summary>
		LargeGrid,
		/// <summary>Light downward diagonal style.</summary>
		LightDownwardDiagonal,
		/// <summary>Light horizontal style.</summary>
		LightHorizontal,
		/// <summary>Light upward diagonal style.</summary>
		LightUpwardDiagonal,
		/// <summary>Light vertical style.</summary>
		LightVertical,
		/// <summary>Narrow horizontal style.</summary>
		NarrowHorizontal,
		/// <summary>Narrow vertical style.</summary>
		NarrowVertical,
		/// <summary>Outlined diamond style.</summary>
		OutlinedDiamond,
		/// <summary>Percent05 style.</summary>
		Percent05,
		/// <summary>Percent10 style.</summary>
		Percent10,
		/// <summary>Percent20 style.</summary>
		Percent20,
		/// <summary>Percent25 style.</summary>
		Percent25,
		/// <summary>Percent30 style.</summary>
		Percent30,
		/// <summary>Percent40 style.</summary>
		Percent40,
		/// <summary>Percent50 style.</summary>
		Percent50,
		/// <summary>Percent60 style.</summary>
		Percent60,
		/// <summary>Percent70 style.</summary>
		Percent70,
		/// <summary>Percent75 style.</summary>
		Percent75,
		/// <summary>Percent80 style.</summary>
		Percent80,
		/// <summary>Percent90 style.</summary>
		Percent90,
		/// <summary>Plaid style.</summary>
		Plaid,
		/// <summary>Shingle style.</summary>
		Shingle,
		/// <summary>Small checker board style.</summary>
		SmallCheckerboard,
		/// <summary>Small confetti style.</summary>
		SmallConfetti,
		/// <summary>Small grid style.</summary>
		SmallGrid,
		/// <summary>Solid diamond style.</summary>
		SolidDiamond,
		/// <summary>Sphere style.</summary>
		Sphere,
		/// <summary>Trellis style.</summary>
		Trellis,
		/// <summary>Vertical style.</summary>
		Vertical,
		/// <summary>Wave style.</summary>
		Wave,
		/// <summary>Weave style.</summary>
		Weave,
		/// <summary>Wide downward diagonal style.</summary>
		WideDownwardDiagonal,
		/// <summary>Wide upward diagonal style.</summary>
		WideUpwardDiagonal,
		/// <summary>Zigzag style.</summary>
		Zigzag
	}



	public enum ChartDashStyle
	{
        /// <summary>
        /// The line style is not set.
        /// </summary>
        NotSet,
        /// <summary>
        /// A dashed line.
        /// </summary>
        Dash,
        /// <summary>
        /// A line with a repeating dash-dot pattern.
        /// </summary>
        DashDot,
        /// <summary>
        /// A line a repeating dash-dot-dot pattern.
        /// </summary>
        DashDotDot,
        /// <summary>
        /// A line with a repeating dot pattern.
        /// </summary>
        Dot,
        /// <summary>
        /// A solid line.
        /// </summary>
        Solid
    }



	public enum CalloutStyle
	{
        /// <summary>
        /// Callout text is underlined and a line points to the anchor point.
        /// </summary>
        SimpleLine,
        /// <summary>
        /// A border is drawn around the callout text and a line points to the anchor point.
        /// </summary>
        Borderline,
        /// <summary>
        /// Callout text is inside the cloud and smaller clouds point to the anchor point.
        /// </summary>
        Cloud,
        /// <summary>
        /// A rectangle is drawn around the callout text, which is connected to the anchor point.
        /// </summary>
        Rectangle,
        /// <summary>
        /// A rounded rectangle is drawn around the callout text, which is connected to the anchor point.
        /// </summary>
        RoundedRectangle,
        /// <summary>
        /// An ellipse is drawn around the callout text, which is connected to the anchor point.
        /// </summary>
        Ellipse,
        /// <summary>
        /// A perspective rectangle is drawn around the callout text, which is connected to the anchor point.
        /// </summary>
        Perspective
    }



	public enum AxisName
	{
        /// <summary>
        /// Primary X-axis.
        /// </summary>
        X,
        /// <summary>
        /// Primary Y-axis.
        /// </summary>
        Y,
        /// <summary>
        /// Secondary X-axis.
        /// </summary>
        X2,
        /// <summary>
        /// Secondary Y-axis.
        /// </summary>
        Y2
    }



	public enum AxisEnabled
	{
        /// <summary>
        /// The axis is only enabled if it used to plot a <see cref="T:System.Windows.Forms.DataVisualization.Charting.Series" />.
        /// </summary>
        Auto,
        /// <summary>
        /// The axis is always enabled.
        /// </summary>
        True,
        /// <summary>
        /// The axis is never enabled.
        /// </summary>
        False
    }



	[Flags()]
	public enum AreaAlignmentStyles
	{
        /// <summary>
        /// Chart areas are not automatically aligned.
        /// </summary>
        None = 0,
        /// <summary>
        /// Chart areas are aligned by the chart area position.
        /// </summary>
        Position = 1,
        /// <summary>
        /// Chart areas are aligned by their inner plot position.
        /// </summary>
        PlotPosition = 2,
        /// <summary>
        /// Chart areas are aligned by their data view.
        /// </summary>
        AxesView = 4,
        /// <summary>
        /// Chart areas are aligned using their cursors.
        /// </summary>
        Cursor = 8,
        /// <summary>
        /// Chart areas are aligned using all values.
        /// </summary>
        All = 15
	}



	[Flags()]
	public enum AreaAlignmentOrientations
	{
        /// <summary>
        /// Chart areas are not automatically aligned.
        /// </summary>
        None = 0,
        /// <summary>
        /// Chart areas are aligned vertically.
        /// </summary>
        Vertical = 1,
        /// <summary>
        /// Chart areas are aligned horizontally.
        /// </summary>
        Horizontal = 2,
        /// <summary>
        /// Chart areas are aligned horizontally and vertically.
        /// </summary>
        All = 3
	}

	public enum TextOrientation
	{
        /// <summary>
        /// The automatic
        /// </summary>
        Auto,
        /// <summary>
        /// The horizontal
        /// </summary>
        Horizontal,
        /// <summary>
        /// The rotated90
        /// </summary>
        Rotated90,
        /// <summary>
        /// The rotated270
        /// </summary>
        Rotated270,
        /// <summary>
        /// The stacked
        /// </summary>
        Stacked
    }

    public enum ControlStyles
    {
        /// <summary>
        /// All painting in wm paint
        /// </summary>
        AllPaintingInWmPaint,
        /// <summary>
        /// The cache text
        /// </summary>
        CacheText,
        /// <summary>
        /// The container control
        /// </summary>
        ContainerControl,
        /// <summary>
        /// The double buffer
        /// </summary>
        DoubleBuffer,
        /// <summary>
        /// The enable notify message
        /// </summary>
        EnableNotifyMessage,
        /// <summary>
        /// The fixed height
        /// </summary>
        FixedHeight,
        /// <summary>
        /// The fixed width
        /// </summary>
        FixedWidth,
        /// <summary>
        /// The opaque
        /// </summary>
        Opaque,
        /// <summary>
        /// The optimized double buffer
        /// </summary>
        OptimizedDoubleBuffer,
        /// <summary>
        /// The resize redraw
        /// </summary>
        ResizeRedraw,
        /// <summary>
        /// The selectable
        /// </summary>
        Selectable,
        /// <summary>
        /// The standard click
        /// </summary>
        StandardClick,
        /// <summary>
        /// The standard double click
        /// </summary>
        StandardDoubleClick,
        /// <summary>
        /// The supports transparent back color
        /// </summary>
        SupportsTransparentBackColor,
        /// <summary>
        /// The user mouse
        /// </summary>
        UserMouse,
        /// <summary>
        /// The user paint
        /// </summary>
        UserPaint,
        /// <summary>
        /// The use text for accessibility
        /// </summary>
        UseTextForAccessibility

    }
}
