using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web.VisualTree.Utilities;
using System.Linq;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{TreeItem}" />
    public class TreeItemCollection : VisualElementCollection<TreeItem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItemCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TreeItemCollection(TreeItem parentElement, string propertyName)
            : base(parentElement, propertyName)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItemCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TreeItemCollection(TreeElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {
        }




        /// <summary>
        /// Finds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="searchAllChildren">if set to <c>true</c> search all children.</param>
        /// <returns></returns>
        public TreeItem[] Find(string key, bool searchAllChildren)
        {
            List<TreeItem> list = FindInternal(key, searchAllChildren, this, new List<TreeItem>());
            return list.ToArray();
        }

        /// <summary>
        /// Adds the item.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public TreeItem Add(string key, string text)
        {
            TreeItem treeItem = new TreeItem(text);
            treeItem.ID = key;
            Add(treeItem);
            treeItem.ParentElement = this.Owner;
            return treeItem;
        }



        /// <summary>
        /// Adds the item
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="text">The text.</param>
        /// <param name="imageIndex">The image index.</param>
        /// <param name="selectedImageIndex">The selected image index.</param>
        /// <returns></returns>
        public virtual TreeItem Add(string key, string text, int imageIndex, int selectedImageIndex)
        {
            return Add(key, text);
        }



        /// <summary>
        /// Finds the internal.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="searchAllChildren">if set to <c>true</c> search all children.</param>
        /// <param name="treeItemCollectionToLookIn">The tree item collection to look in.</param>
        /// <param name="foundTreeItems">The found tree items.</param>
        /// <returns></returns>
        private List<TreeItem> FindInternal(string key, bool searchAllChildren, TreeItemCollection treeItemCollectionToLookIn, List<TreeItem> foundTreeItems)
        {
            if ((treeItemCollectionToLookIn == null) || (foundTreeItems == null))
            {
                return null;
            }
            for (int i = 0; i < treeItemCollectionToLookIn.Count; i++)
            {
                if ((treeItemCollectionToLookIn[i] != null) && Strings.SafeCompareStrings(treeItemCollectionToLookIn[i].ID, key, true))
                {
                    foundTreeItems.Add(treeItemCollectionToLookIn[i]);
                }
            }
            if (searchAllChildren)
            {
                for (int j = 0; j < treeItemCollectionToLookIn.Count; j++)
                {
                    if (((treeItemCollectionToLookIn[j] != null) && (treeItemCollectionToLookIn[j].Items != null)) && (treeItemCollectionToLookIn[j].Items.Count > 0))
                    {
                        foundTreeItems = FindInternal(key, searchAllChildren, treeItemCollectionToLookIn[j].Items, foundTreeItems);
                    }
                }
            }
            return foundTreeItems;
        }




        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, TreeItem item)
        {
            // validate parameter
            if (item == null)
            {
                return;
            }

            base.InsertItem(index, item);

            item.Index = index;
        }



        /// <summary>
        /// Sets the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void SetItem(int index, TreeItem item)
        {
            // validate parameter
            if (item != null)
            {
                base.SetItem(index, item);

                item.Index = index;
            }
        }


        /// <summary>
        /// remove item 
        /// </summary>
        /// <param name="treeItem">TreeItem to remove</param>
        /// <returns>true if remove success, otherwise false</returns>
        public bool Remove(TreeItem treeItem)
        {
            if (treeItem != null)
            {
                TreeElement tempParentTree = treeItem.ParentTree;
                if (tempParentTree != null)
                {
                    List<TreeItem> treeList = new List<TreeItem>();
                    this.FindInternal(treeItem.ID, true, tempParentTree.Items, treeList);
                    if (treeList.Count == 1)
                    {
                        tempParentTree.Items.RemoveByKey(Convert.ToString(treeList[0].ID));
                    }
                    else if (treeList.Count > 1)
                    {
                        TreeItemCollection newTreeItems = (treeItem.ParentElement as TreeItem).Items;
                        if (newTreeItems != null && newTreeItems.Count > 0)
                            tempParentTree.Items.RemoveByKey(Convert.ToString(treeList[0].ID), newTreeItems);
                    }
                    else { return false; }
                }
            }
            return true;
        }

        /// <summary>
        /// Removes the by key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void RemoveByKey(string key)
        {
            // If key is valid
            if (key != null)
            {
                // Find items
                TreeItem[] items = this.Find(key, false);

                // If items are valid
                if (items != null)
                {
                    // Take first or default
                    TreeItem item = items.FirstOrDefault();

                    // If item is valid
                    if (item != null)
                    {
                        this.RemoveItem(item.Index);
                    }
                }
            }
        }

        /// <summary>
        /// Removes the by key.
        /// 
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="subItems">sub items </param>
        private void RemoveByKey(string key, TreeItemCollection subItems)
        {
            // If key is valid
            if (key != null)
            {
                // Find items
                TreeItem[] items = subItems.Find(key, false);

                // If items are valid
                if (items != null)
                {
                    // Take first or default
                    TreeItem item = items.FirstOrDefault();

                    // If item is valid
                    if (item != null)
                    {
                        subItems.RemoveItem(item.Index);
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the collection contains a tree node with the specified key.
        /// </summary>
        /// <param name="key">  The name of the System.Web.VisualTree.Elements.TreeItem to search for. </param>
        /// <returns> 
        /// true to indicate the collection contains a System.Web.VisualTree.Elements.TreeItem
        /// with the specified key; otherwise, false.
        /// </returns>
        public bool ContainsKey(string key)
        {
            if (key != null)
            {
                // Find items
                TreeItem[] items = this.Find(key, false);
                if (items != null && items.Length > 0)
                    return true;
            }
            return false;
        }
    }
}
