using System.Drawing.Printing;

namespace System.Web.VisualTree.Elements
{
	public class PrintPreviewDialog : CommonDialog
	{
        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
		public PrintDocument Document
		{
			get;
			set;
		}
	}
}
