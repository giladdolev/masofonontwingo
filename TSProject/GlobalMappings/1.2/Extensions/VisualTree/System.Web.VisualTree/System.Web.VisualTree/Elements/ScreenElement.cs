using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
	public class ScreenElement : VisualElement
	{
        private static ScreenElement mobjPrimaryScreen = new ScreenElement();


        /// <summary>
        /// Prevents a default instance of the <see cref="ScreenElement"/> class from being created.
        /// </summary>
        private ScreenElement()
        {
            PixelHeight = 600;
            PixelWidth = 800;
        }



        public CursorElement MousePointer { get; set; }



		/// <summary>
		///Gets the primary display.
		/// </summary>
		public static ScreenElement PrimaryScreen
		{
			get
            {
                return mobjPrimaryScreen;
            }
		}



        /// <summary>
        /// Gets or sets the height in pixels.
        /// </summary>
        /// <value>
        /// The height in pixels.
        /// </value>
        public int PixelHeight
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the width in pixels.
        /// </summary>
        /// <value>
        /// The width in pixels.
        /// </value>
        public int PixelWidth
        {
            get;
            set;
        }



        /// <summary>
        /// Gets the twips pew pixel y.
        /// </summary>
        /// <value>
        /// The twips pew pixel y.
        /// </value>
        public static int TwipsPewPixelY
        {
            get
            {
                return 15;
            }
        }



        /// <summary>
        /// Gets the twips pew pixel x.
        /// </summary>
        /// <value>
        /// The twips pew pixel x.
        /// </value>
        public static int TwipsPewPixelX
        {
            get
            {
                return 15;
            }
        }
	}
}
