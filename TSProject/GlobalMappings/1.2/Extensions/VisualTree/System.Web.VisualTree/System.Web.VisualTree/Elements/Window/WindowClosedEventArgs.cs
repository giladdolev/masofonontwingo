namespace System.Web.VisualTree.Elements
{
	public class WindowClosedEventArgs : EventArgs
	{
        public CloseReason Reason
        {
            get;
            set;
        }
        public WindowClosedEventArgs(CloseReason Reason = CloseReason.UserClosing)
        {
            this.Reason = Reason;
        }



	    public new static WindowClosedEventArgs Empty
	    {
            get {  return new WindowClosedEventArgs();}  
	    } 
	}
}
