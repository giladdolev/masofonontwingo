using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ButtonBaseElement" />
    public class RadioButtonElement : ButtonBaseElement
    {
        private bool _isChecked = false;
        private ContentAlignment menmCheckAlign = ContentAlignment.MiddleLeft;

        /// <summary>
        /// Gets or sets a value indicating whether radio button is checked
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                // do property changed only if the values changed 
                if (this._isChecked != value)
                {
                    _isChecked = value;
                    // Notify property IsChecked changed
                    OnPropertyChanged("IsChecked");
                    //raise the click event changed with the new value 
                    OnCheckedChanged(new ValueChangedArgs<bool>(value));

                    if (value)
                    {
                        ChangeIsCheckedProperty(value);
                    }
                }
            }
        }


        /// <summary>
        /// Gets or sets a alignment value of radio button 
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(ContentAlignment.MiddleLeft)]
        public ContentAlignment CheckAlign
        {
            get { return menmCheckAlign; }
            set
            {
                menmCheckAlign = value;
                // Notify property CheckAlign changed
                OnPropertyChanged("CheckAlign");
            }
        }


        /// <summary>
        /// CheckedChanged event handler.
        /// </summary>
        private event EventHandler _CheckedChanged;

        /// <summary>
        /// Add or remove CheckedChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler CheckedChanged
        {
            add
            {
                bool needNotification = (_CheckedChanged == null);

                _CheckedChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CheckedChanged");
                }

            }
            remove
            {
                _CheckedChanged -= value;

                if (_CheckedChanged == null)
                {
                    OnEventHandlerDeattached("CheckedChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CheckedChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CheckedChanged"/> instance containing the event data.</param>
        protected virtual void OnCheckedChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_CheckedChanged != null)
            {
                _CheckedChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CheckedChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCheckedChanged(EventArgs args)
        {
            // If there is a valid changed arguments
            ValueChangedArgs<bool> changedArgs = args as ValueChangedArgs<bool>;

            // If is a checked argument
            if (changedArgs != null)
            {
                bool isChecked = changedArgs.Value;

                if (isChecked)
                {
                    // changed IsChecked property and invoke previous checked radio element
                    ChangeIsCheckedProperty(changedArgs.Value);

                    // Raise the CheckedChanged event for the currently clicked   RadioButton (the one the user clicked)
                    OnCheckedChanged(args);

                    RadioGroupElement parentElement = this.Parent as RadioGroupElement;
                    if (parentElement != null)
                    {
                        // Raise change event . 
                        parentElement.PerformChange(args);
                    }
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The instance containing the event data.</param>
        protected override void OnClick(EventArgs args)
        {
            ChangeIsCheckedProperty(true);
            base.OnClick(args);
        }

        /// <summary>
        /// Changes the is checked property.
        /// </summary>
        /// <param name="isChecked">if set to <c>true</c> [is checked].</param>
        private void ChangeIsCheckedProperty(bool isChecked)
        {
            // Indicate this radio is checked
            _isChecked = isChecked;

            // Get parent control
            ControlElement parentElement = ParentElement as ControlElement;

            // If there is a valid parent control
            if (parentElement != null)
            {
                // Loop all controls
                foreach (VisualElement siblingElement in parentElement.Controls)
                {
                    // Get radio button element
                    RadioButtonElement sibilingRadioElement = siblingElement as RadioButtonElement;

                    // If is a radio button and is not this
                    if (sibilingRadioElement != null && this != sibilingRadioElement)
                    {
                        // Indicate not checked
                        sibilingRadioElement.IsChecked = false;
                    }
                }
            }


        }
       
    }
}
