using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Common;
using System.Data;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class TreeItem : VisualElement, IBindingViewModelRecord, IBindingViewModelField, IClientHistoryElement
    {
        private bool _ensureVisible;
        private bool isSelected;
        private bool _checked;
        private Font _font;
        private readonly TreeItemCollection _treeItems;
        private List<string> _values;
        private bool _visible=true;

        public TreeItem()
        {
            _values = new List<string>();
            // Initialize TreeView nodes
            _treeItems = new TreeItemCollection(this, "Items");

        }



        public TreeItem(string text)
            : this()
        {
            Text = text;
        }



        public TreeItem(string text, TreeItem[] children)
            : this(text)
        {
            _treeItems.AddRange(children);
        }

        public TreeItem(string text, string key)
            : this(text)
        {
            Key = key;
        }

        public TreeItem(SerializationInfo serializationInfo, StreamingContext context)
            : this()
        {

        }

       

        public TreeItem(string text, int imageIndex, int selectedImageIndex)
            : this(text)
        {
        }



        public TreeItem(string text, int imageIndex, int selectedImageIndex, TreeItem[] children)
            : this(text, children)
        {

        }

        /// <summary>
        /// Gets a value indicating whether this instance is leaf.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is leaf; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool IsLeaf
        {
            get
            {
                return (this.Items != null && this.Items.Count < 1);
            }
        }




        /// <summary>
        /// Gets tree items.
        /// </summary>
        /// <value>
        /// The tree items.
        /// </value>  
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<TreeItem>))]
        public TreeItemCollection Items
        {
            get { return _treeItems; }
        }



        /// <summary>
        /// Gets or sets the user data.
        /// </summary>
        /// <value>
        /// The user data.
        /// </value>
        [DesignerType(typeof(string))]
        public object Tag
        {
            get;
            set;
        }



        private ResourceReference _Icon;
        /// <summary>
        /// Gets or sets the MyProperty.
        /// </summary>
        /// <value>
        /// The MyProperty.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public ResourceReference Icon
        {
            get { return _Icon; }
            set
            {
                if (_Icon != value)
                {
                    _Icon = value;

                    OnPropertyChanged("Icon");
                }
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TreeItem"/> is checked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                // If checked has a different value
                if (_checked != value)
                {
                    // Set checked value
                    _checked = value;

                    // Get the tree 
                    TreeElement treeElement = ParentTree;

                    // If there is a valid tree
                    if (treeElement != null)
                    {                   
                        // Raise tree item checked
                        treeElement.OnAfterCheckInternal(this, value);
                        Check(value);
                    }
                }
            }
        }

        /// <summary>
        /// Raise check event .
        /// </summary>
        /// <param name="state">state</param>
        private void Check(bool state)
        {
            this.InvokeMethodOnce("Check", state);
        }
       

        /// <summary>
        /// Gets the parent tree.
        /// </summary>
        /// <value>
        /// The parent tree.
        /// </value>
        [DesignerIgnore]
        public TreeElement ParentTree
        {
            get
            {
                // Get parent element
                VisualElement parent = ParentElement;

                // If is a tree element
                TreeElement treeElement = parent as TreeElement;
                if (treeElement != null)
                {
                    // Return parent as tree
                    return treeElement;
                }

                // Get tree item 
                TreeItem treeItem = parent as TreeItem;

                // If there is a valid tree item
                if (treeItem != null)
                {
                    // Return tree
                    return treeItem.ParentTree;
                }

                return null;

            }
        }


        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }


        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;

                OnPropertyChanged("IsSelected");
            }
        }



        public TreeItem PrevNode
        {
            get;
            private set;
        }



        public TreeItem NextNode { get; private set; }



        public int SelectedImageIndex { get; set; }



        public string ImageKey { get; set; }



        public int ImageIndex { get; set; }



        [RendererPropertyDescription]
        public string Text
        {
            get;
            set;
        }



        public string ToolTipText
        {
            get;
            set;
        }



        [RendererPropertyDescription]
        /// <summary>
        /// Visible Description.
        /// </summary>
        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                if (_visible != value)
                {
                    _visible = value;

                    OnPropertyChanged("Visible");
                    this.ParentTree.ApplyFilter();
                }
              
            }
        }



        private Color _foreColor = Color.Empty;

        public Color ForeColor
        {
            get { return _foreColor = Color.Empty; }
            set { _foreColor = value; }
        }


        public Color BackColor
        {
            get;
            set;
        }



        public int Index
        {
            get;
            internal set;
        }



        public int StateImageIndex
        {
            get;
            set;
        }



        public string SelectedImageKey
        {
            get;
            set;
        }



        public int Level
        {
            get;
            private set;
        }



        /// <summary>
        /// Expands specific note an its children.
        /// </summary>
        public void ExpandAll()
        {
            // Expands specific note
            Expand();

            // Expand TreeItem children recursively
            foreach (TreeItem treeItem in Items.Where(treeItem => treeItem != null))
            {
                treeItem.ExpandAll();
            }
        }


        /// <summary>
        /// Gets the item identifier.
        /// </summary>
        /// <param name="treeItem">The tree item.</param>
        /// <returns></returns>
        public static string GetItemId(TreeItem treeItem)
        {
            // If there is a valid tree item
            if (treeItem != null)
            {
                return treeItem.ClientID;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the item path.
        /// </summary>
        /// <param name="treeItem">The tree item.</param>
        /// <returns></returns>
        public static string GetItemPath(TreeItem treeItem)
        {
            StringBuilder itemIdBuffer = new StringBuilder();
            FillItemPath(itemIdBuffer, treeItem);
            return itemIdBuffer.ToString();
        }



        /// <summary>
        /// Fills the item path.
        /// </summary>
        /// <param name="itemIdBuffer">The item identifier buffer.</param>
        /// <param name="treeItem">The tree item.</param>
        private static void FillItemPath(StringBuilder itemIdBuffer, TreeItem treeItem)
        {
            // If there is a valid tree item
            if (treeItem != null)
            {
                // Render parent path
                FillItemPath(itemIdBuffer, treeItem.ParentElement as TreeItem);

                // If we need to add separator
                if (itemIdBuffer.Length > 0)
                {
                    // Add path separator
                    itemIdBuffer.Append("/");
                }

                // Add client id
                itemIdBuffer.Append(treeItem.ClientID);
            }
        }



        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            // Loop all child items
            foreach (VisualElement visualElement in Items)
            {
                // If there is a valid child control
                if (IsMatchingVisualElement<TVisualElement>(visualElement, strVisualElementID))
                {
                    // Return element
                    return visualElement as TVisualElement;
                }
            }

            return null;
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Try to get TreeView node element
            TreeItem objTreeNodeElement = visualElement as TreeItem;

            // If there is a valid TreeView node element
            if (objTreeNodeElement != null)
            {
                _treeItems.Add(objTreeNodeElement);
                if (ParentTree != null)
                {
                    ParentTree.Refresh();
                }
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }



        public void Remove()
        {
        }




        public void Expand()
        {
            if (!IsExpanded)
            {
                IsExpanded = true;
            }
        }



        /// <summary>
        /// Ensures the visible.
        /// </summary>
        /// <returns></returns>
        public bool EnsureVisible()
        {
            _ensureVisible = true;
            return _ensureVisible;
        }




        public void Collapse()
        {
            if (IsExpanded)
            {
                IsExpanded = false;
            }
        }



        /// <summary>
        /// Gets the node count.
        /// </summary>
        /// <param name="includeSubtrees">if set to <c>true</c> include sub trees.</param>
        /// <returns></returns>
        public int GetNodeCount(bool includeSubtrees)
        {
            int childCount = Items.Count;
            if (includeSubtrees)
            {
                for (int i = 0; i < childCount; i++)
                {
                    childCount += Items[i].GetNodeCount(true);
                }
            }
            return childCount;
        }

        public Font Font
        {
            get { return _font; }
            set
            {
                _font = value;

                // Notify property Font changed
                OnPropertyChanged("Font");
            }
        }



        /// <summary>
        /// Gets a value indicating whether the tree node is in the expanded state.
        /// </summary>
        /// <value>
        /// <c>true</c> if this node is expanded; otherwise, <c>false</c>.
        /// </value>
        private bool _isExpanded = false;
        /// <summary>
        /// Gets or sets the isExpanded.
        /// </summary>
        /// <value>
        /// The isExpanded.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Appearance")]
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (_isExpanded != value)
                {
                    _isExpanded = value;

                    OnPropertyChanged("IsExpanded");
                }
            }
        }



        private string _key = null;
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(null)]
        [Category("Appearance")]
        public string Key
        {
            get { return _key; }
            set
            {
                if (_key != value)
                {
                    _key = value;

                    OnPropertyChanged("key");
                }
            }
        }


        /// <summary>
        /// Navigates to this element.
        /// </summary>
        void IClientHistoryElement.Navigate()
        {
            // Get parent tree
            TreeElement tree = this.ParentTree;

            // If there is a valid tree
            if (tree != null)
            {
                // Suspend history
                using (tree.CreateSuspendHistorySession())
                {
                    // Set the selected tree item
                    tree.SelectedItem = this;
                }
            }
        }



        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool SetValue(object value)
        {
            string text = value as string;
            if (!String.IsNullOrEmpty(text))
            {
                this.Text = text;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the type of the field.
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public Type FieldType
        {
            get { return typeof(string); }
        }


        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public IBindingViewModelField GetField(string key)
        {
            return this;
        }

        /// <summary>
        /// Gets or sets the MyProperty.
        /// </summary>
        /// <value>
        /// The MyProperty.
        /// </value>
        public List<string> Values
        {
            get { return _values; }
            set
            {
                _values = value;
            }
        }

        /// <summary>
        /// Build the Items of the tree in recursion
        /// </summary>
        /// <param name="treeDataTable"></param>
        /// <param name="dataReader"></param>
        /// <param name="parent"></param>
        internal void BuildTreeItems(DataTable treeDataTable, IDataReader dataReader, string parent)
        {
            TreeItem treeItem = null;
            string itemId = "";
            string value = "";
            int ID_ColIndex = treeDataTable.Columns.IndexOf(this.ParentTree.KeyFieldName);

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    treeItem = new TreeItem();
                    // Loop all columns indexes
                    for (int index = 0; index < dataReader.FieldCount; index++)
                    {
                        value = string.IsNullOrEmpty(dataReader.GetValue(index).ToString()) ? "" : dataReader.GetValue(index).ToString();
                        treeItem.Values.Add(value);
                    }
                    itemId = treeItem.Values[ID_ColIndex];
                    treeItem.ID = itemId; 
                    if (treeItem != null)
                    {
                        // Add item
                        Items.Add(treeItem);
                    }

                    //Create DataTable of item's children from DataTable
                    DataRow[] rows = treeDataTable.Select("" + this.ParentTree.ParentFieldName + "= '" + itemId + "'");
                    if (rows.Count() == 0)
                    {
                        return;
                    }

                    IDataReader newDataReader = BindingDataReader.Create(rows.CopyToDataTable());
                    Items[Items.Count - 1].BuildTreeItems(treeDataTable, newDataReader, itemId);
                }
            }
            return;
        }

        /// <summary>
        /// update check state .
        /// </summary>
        /// <param name="state"></param>
        internal void UpdateState(bool state)
        {
            this._checked = state;
        }
    }
}
