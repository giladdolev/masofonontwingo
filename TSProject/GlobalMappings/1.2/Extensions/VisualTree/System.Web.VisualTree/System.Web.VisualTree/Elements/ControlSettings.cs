﻿using System.ComponentModel;
using System.Web.UI.WebControls;

namespace System.Web.VisualTree.Elements
{
    public class ControlSettings : VisualSettings
    {
        private Unit _width;


        /// <summary>
        /// Gets or sets the dock style for this control.
        /// </summary>
        /// <value>
        /// The dock style for this control.
        /// </value>
        public Dock Dock
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        [DefaultValue(typeof (Unit), "")]
        public Unit Width
        {
            get { return _width; }
            set { _width = value; }
        }



        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        [DefaultValue(typeof(Unit), "")]
        public Unit Height
        {
            get;
            set;
        }
    }
}
