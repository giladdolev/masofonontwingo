﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    [Flags()]
    public  enum ControlElementState
    {
        None = 0,
        Loaded = 1,
        Hidden = 2,
        Created = 4,
        Selected =8
    }
}
