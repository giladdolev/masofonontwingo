﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class RibbonPageCollection : ControlElementCollection
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="RibbonPageCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal RibbonPageCollection(RibbonControlElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
		{
		}




        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public RibbonPageElement this[int index]
        {
            get
            {
                return base[index] as RibbonPageElement;
            }
            set
            {
                base[index] = value;
            }
        }

        /// <summary>
        /// Adds range of RibbonPageGroupElement.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(RibbonPageGroupElement[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (RibbonPageGroupElement item in items)
            {
                // Add the item
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }

        /// <summary>
        /// Adds range of RibbonPageElement.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(RibbonPageElement[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (RibbonPageElement item in items)
            {
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }
	}
}
