using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ToolBarControlHost" />
    /// <seealso cref="System.Web.VisualTree.Elements.IListItems" />
    [DesignerIgnore]
    public class ToolBarComboBox : ToolBarControlHost, IListItems
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarComboBox"/> class.
        /// </summary>
		public ToolBarComboBox() 
            : base(new ComboBoxElement())
		{

		}


		/// <summary>
		/// Gets the hosted ComboBox.
		/// </summary>
		/// <value>
		/// The hosted ComboBox.
		/// </value>
		private ComboBoxElement HostedComboBox
		{
			get { return (ComboBoxElement)Control; }
		}



		/// <summary>
		/// Gets or sets the index of the selected.
		/// </summary>
		/// <value>
		/// The index of the selected.
		/// </value>
		public int SelectedIndex
		{
			get { return HostedComboBox.SelectedIndex; }
			set { HostedComboBox.SelectedIndex = value; }
		}



		/// <summary>
		/// Gets the items.
		/// </summary>
		/// <value>
		/// The items.
		/// </value>
		public ListItemCollection Items
		{
			get { return HostedComboBox.Items; }
		}


        /// <summary>
        /// SelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler _SelectedIndexChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove SelectedIndexChanged action.
        /// </summary>
        public event EventHandler SelectedIndexChanged
        {
            add
            {
                bool needNotification = (_SelectedIndexChanged == null);

                _SelectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChanged");
                }

            }
            remove
            {
                _SelectedIndexChanged -= value;

                if (_SelectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectedIndexChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectedIndexChangedListeners
        {
            get { return _SelectedIndexChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedIndexChanged != null)
            {
                _SelectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectedIndexChanged(EventArgs args)
        {

            // Raise the SelectedIndexChanged event.
            OnSelectedIndexChanged(args);
        }


		/// <summary>
		/// Gets the item text.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public string GetItemText(object item)
		{
			if (item != null)
			{
				return item.ToString();
			}
			else
			{
				return string.Empty;
			}
		}

        /// <summary>
        /// Gets the item value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public object GetItemValue(object item)
        {
            return null;
        }
	}
}
