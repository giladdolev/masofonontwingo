namespace System.Web.VisualTree.Elements
{
	public class FileDialog : CommonDialog
	{

        /// <summary>
        /// Gets or sets a value indicating whether [restore directory].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [restore directory]; otherwise, <c>false</c>.
        /// </value>
		public bool RestoreDirectory
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
		public string Title
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
		public string Filter
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the initial directory.
        /// </summary>
        /// <value>
        /// The initial directory.
        /// </value>
		public string InitialDirectory
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
		public string FileName
		{
			get;
			set;
		}

	    /// <summary>
	    /// Gets or sets the path of the file.
	    /// </summary>
	    /// <value>
	    /// The path of the file.
	    /// </value>
	    public string FilePath
	    {
	        get;
	        set;
	    }


        /// <summary>
        /// Gets or sets the index of the filter.
        /// </summary>
        /// <value>
        /// The index of the filter.
        /// </value>
		public int FilterIndex
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether [check path exists].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check path exists]; otherwise, <c>false</c>.
        /// </value>
		public bool CheckPathExists
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether [check file exists].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check file exists]; otherwise, <c>false</c>.
        /// </value>
		public virtual bool CheckFileExists
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the default ext.
        /// </summary>
        /// <value>
        /// The default ext.
        /// </value>
		public string DefaultExt
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether [add extension].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [add extension]; otherwise, <c>false</c>.
        /// </value>
		public bool AddExtension
		{
			get;
			set;
		}



        /// <summary>
        /// Gets the file names.
        /// </summary>
        /// <value>
        /// The file names.
        /// </value>
        public string[] FileNames
        {
            get { return default(string[]); }
        }
	}
}
