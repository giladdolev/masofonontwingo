﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    [DesignerComponent]
    public class DataViewBand : VisualElement, IEnumerable<DataViewField>
    {
        /// <summary>
        /// Determines whether to wrap text
        /// </summary>
      //  private bool _wrapText;

        /// <summary>
        /// The fields
        /// </summary>
        private DataViewFieldCollection _fields = null;

        /// <summary>
        /// The type
        /// </summary>
        private DataViewBandType _type = DataViewBandType.Details;

        /// <summary>
        /// The height
        /// </summary>
        private Unit _height;

        /// <summary>
        /// The css class
        /// </summary>
        private string _cssClass;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewBand"/> class.
        /// </summary>
        public DataViewBand()
        {
            _fields = new DataViewFieldCollection(this, "Fields");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewBand"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public DataViewBand(DataViewBandType type)
            : this()
        {
            _type = type;
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [DesignerIgnore]
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                return _fields;
            }
        }

        /// <summary>
        /// Gets or sets the height of the pixel.
        /// </summary>
        /// <value>
        /// The height of the pixel.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelHeight
        {
            get { return UnitTypeConverter.GetPixels(Height); }
            set
            {

                Height = Unit.Pixel(value);
            }
        }

        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        /// <value>
        /// The CSS class.
        /// </value>
        [RendererPropertyDescription]
        public string CssClass
        {
            get { return _cssClass; }
            set
            {
                if(_cssClass != value)
                {
                    _cssClass = value;

                    OnPropertyChanged("CssClass");
                }
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        [RendererPropertyDescription]
        public Unit Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;

                    OnPropertyChanged("Height");
                }
            }
        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            
            DataViewField field = visualElement as DataViewField;

            // If there is a valid field
            if (field != null)
            {
                // Add field
                _fields.Add(field);

                // Don't override the WrapText of the field with the WrapText default value of Band
                if (WrapText)
                {
                    field.WrapText = WrapText;
                }
                // Set parent element
                field.ParentElement = this;
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the fields.
        /// </summary>
        /// <value>
        /// The fields.
        /// </value>
        [RendererPropertyDescription]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<DataViewField>))]
        public DataViewFieldCollection Fields
        {
            get
            {
                return _fields;
            }
        }


        private Color _BackColor;
        /// <summary>
        /// Gets or sets the BackgroundColor.
        /// </summary>
        /// <value>
        /// The BackgoundColor.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public Color BackColor
        {
            get { return _BackColor; }
            set
            {
                if (_BackColor != value)
                {
                    _BackColor = value;

                    OnPropertyChanged("BackColor");
                }
            }
        }

        
        private Color _ForeColor = Color.Empty;
        /// <summary>
        /// Gets or sets the ForeColor.
        /// </summary>
        /// <value>
        /// The ForeColor.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "")]
        public Color ForeColor
        {
            get { return _ForeColor; }
            set
            {
                if (_ForeColor != value)
                {
                    _ForeColor = value;

                    OnPropertyChanged("ForeColor");
                }
            }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [DefaultValue(DataViewBandType.None)]
        public DataViewBandType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        IEnumerator<DataViewField> IEnumerable<DataViewField>.GetEnumerator()
        {
            return _fields.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return _fields.GetEnumerator();
        }

        /// <summary>
        /// Gets or sets a value indicating whether to wrap the text.
        /// </summary>
        /// <value>
        ///   <c>true</c> if wrap th etxt; otherwise, <c>false</c>.
        /// </value>
        public bool WrapText { get; set; }

    }

    
}
