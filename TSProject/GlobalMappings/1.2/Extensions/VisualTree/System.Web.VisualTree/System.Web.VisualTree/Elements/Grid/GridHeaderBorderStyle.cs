﻿namespace System.Web.VisualTree.Elements
{
    public enum GridHeaderBorderStyle
    {
        /// <summary>A border that has been customized.</summary>
        Custom,
        /// <summary>A single-line border.</summary>
        Single,
        /// <summary>A three-dimensional raised border.</summary>
        Raised,
        /// <summary>A three-dimensional sunken border.</summary>
        Sunken,
        /// <summary>No borders.</summary>
        None
    }
}
