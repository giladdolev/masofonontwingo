﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    public class DataViewElement : ControlElement, IEnumerable<DataViewBand>, ITVisualElement
    {
        /// <summary>
        /// The bands
        /// </summary>
        private DataViewBandCollection _bands = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewElement"/> class.
        /// </summary>
        public DataViewElement()
        {
            _bands = new DataViewBandCollection(this, "Bands");
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                return _bands;
            }
        }

    

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // If there is a valid bind
            if (visualElement is DataViewBand)
            {
                // Add band
                _bands.Add((DataViewBand)visualElement);

                // Set parent element
                visualElement.ParentElement = this;
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the bands.
        /// </summary>
        /// <value>
        /// The bands.
        /// </value>
        [RendererPropertyDescription]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<DataViewBand>))]
        public DataViewBandCollection Bands
        {
            get
            {
                return _bands;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        IEnumerator<DataViewBand> IEnumerable<DataViewBand>.GetEnumerator()
        {
            return _bands.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return _bands.GetEnumerator();
        }

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing colors, text settings, and other appearance settings of controls
        /// Controlling the Print Preview display
        /// Deleting and adding controls (such as lines or bitmaps) in the DataViewElement
        /// </summary>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// Returns the empty string (“”) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument’s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        public string Modify(string controlName, string propertyName, string value)
        {
            string result = null;
            try
            {
                VisualElement control = this.GetVisualElementById<VisualElement>(controlName);
                if (control != null)
                {
                    PropertyInfo propertyInfo = control.GetType().GetProperty(propertyName);
                    if (propertyInfo != null)
                    {
                        Type t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
                        object safeValue = (value == null) ? null : Convert.ChangeType(value, t);
                        propertyInfo.SetValue(control, safeValue, null);
                    }
                }
                result = "";
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }
        /// <summary>
        /// Reports the values of properties of a DataViewElement object and controls within the DataViewElement object.
        /// </summary>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        public string Describe(string controlName, string propertyName)
        {
            string result = null;
            try
            {
                VisualElement control = this.GetVisualElementById<VisualElement>(controlName);
                if (control != null)
                {
                    PropertyInfo propertyInfo = control.GetType().GetProperty(propertyName);
                    if (propertyInfo != null)
                    {
                        Type t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
                        return propertyInfo.GetValue(control).ToString();
                    }
                }
                result = @"""?""";
            }
            catch (Exception e)
            {
                result = @"""!""";
            }

            return result;
        }

 
    }
}
