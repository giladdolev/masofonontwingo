﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum ListViewMatchOptions
    {
        /// <summary>
        /// (Default) An integer or constant specifying that a match will occur if the item's Text property begins with the whole word being searched. 
        /// Ignored if the criteria is not text.
        /// </summary>
        WholeWord,

        /// <summary>
        /// An integer or constant specifying that a match will occur if the item's Text property begins with the string being searched. 
        /// Ignored if the criteria is not text.
        /// </summary>
        Partial
    }
}
