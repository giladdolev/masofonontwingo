﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    internal abstract class BindingDataReader : BindingDataRecord, IDataReader
    {
        /// <summary>
        /// The ID index
        /// </summary>
        public const int ID_INDEX = int.MinValue;

        /// <summary>
        /// Provides support for enumerator data reader
        /// </summary>
        private class BindingEnumeratorDataReader : BindingDataReader
        {
            /// <summary>
            /// The enumerator
            /// </summary>
            private readonly IEnumerator _enumerator;

            /// <summary>
            /// The index
            /// </summary>
            private int index = -1;

            /// <summary>
            /// Initializes a new instance of the <see cref="BindingEnumeratorDataReader" /> class.
            /// </summary>
            /// <param name="enumerator">The enumerator.</param>
            /// <param name="columns">The columns.</param>
            public BindingEnumeratorDataReader(IEnumerator enumerator, IBindingDataColumn[] columns)
                : base(columns)
            {
                _enumerator = enumerator;
            }

            /// <summary>
            /// Gets the current item identifier.
            /// </summary>
            /// <value>
            /// The current item identifier.
            /// </value>
            public override object CurrentItemId
            {
                get
                {
                    return index;
                }
            }

            /// <summary>
            /// Gets the current item.
            /// </summary>
            /// <value>
            /// The current item.
            /// </value>
            protected override object CurrentItem
            {
                get
                {
                    return _enumerator.Current;
                }
            }

            /// <summary>
            /// Advances the <see cref="T:System.Data.IDataReader" /> to the next record.
            /// </summary>
            /// <returns>
            /// true if there are more rows; otherwise, false.
            /// </returns>
            public override bool Read()
            {
                index++;
                return _enumerator.MoveNext();
            }
        }
        

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDataReader"/> class.
        /// </summary>
        /// <param name="columns">The columns.</param>
        public BindingDataReader(IBindingDataColumn[] columns)
            : base(columns)
        {

        }


        /// <summary>
        /// Creates the data reader.
        /// </summary>
        /// <param name="data">The data reader.</param>
        /// <returns></returns>
        public static IDataReader Create(object data)
        {
            // Get data table
            DataTable dataTable = data as DataTable;

            // If there is a valid data table
            if(dataTable != null)
            {
                // Create data reader from table
                return dataTable.CreateDataReader();
            }                
            else
            {
                // Get data set
                DataSet dataSet = data as DataSet;

                // If there is a valid data set
                if (dataSet != null)
                {
                    // Return data set reader
                    return dataSet.CreateDataReader();
                }
                else
                {
                    // Create binding reader
                    return Create(data, GetItemColumns(data));
                }
            }
        }


        /// <summary>
        /// Creates the specified enumerator.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="columns">The columns.</param>
        /// <returns></returns>
        public static IDataReader Create(object data, IBindingDataColumn[] columns)
        {
            // Get enumerable from data
            IEnumerable enumerable = data as IEnumerable;

            // If there is no valid enumerable
            if(enumerable == null)
            {
                // Create default enumerable
                enumerable = new object[] { };
            }

            // If there is no valid columns
            if(columns == null)
            {
                // Create empty columns
                columns = new IBindingDataColumn[] { };
            }

            // Return enumerator data reader
            return new BindingEnumeratorDataReader(enumerable.GetEnumerator(), columns);
        }

        /// <summary>
        /// Closes the <see cref="T:System.Data.IDataReader" /> Object.
        /// </summary>
        public virtual void Close()
        {
            
        }


        /// <summary>
        /// Gets the binding flags.
        /// </summary>
        /// <value>
        /// The binding flags.
        /// </value>
        internal static BindingFlags BindingFlags
        {
            get
            {
                return BindingFlags.GetProperty |
                    BindingFlags.Instance |
                    BindingFlags.Public;
            }        
        }

        /// <summary>
        /// Gets a value indicating the depth of nesting for the current row.
        /// </summary>
        public virtual int Depth
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.Data.DataTable" /> that describes the column metadata of the <see cref="T:System.Data.IDataReader" />.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Data.DataTable" /> that describes the column metadata.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a value indicating whether the data reader is closed.
        /// </summary>
        public virtual bool IsClosed
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Advances the data reader to the next result, when reading the results of batch SQL statements.
        /// </summary>
        /// <returns>
        /// true if there are more rows; otherwise, false.
        /// </returns>
        public virtual bool NextResult()
        {
            return false;   
        }

        /// <summary>
        /// Advances the <see cref="T:System.Data.IDataReader" /> to the next record.
        /// </summary>
        /// <returns>
        /// true if there are more rows; otherwise, false.
        /// </returns>
        public abstract bool Read();

        /// <summary>
        /// Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.
        /// </summary>
        public virtual int RecordsAffected
        {
            get
            {
                return -1;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            
        }

        /// <summary>
        /// Gets the item columns.
        /// </summary>
        /// <param name="dataContext">The data source.</param>
        /// <returns></returns>
        internal static IBindingDataColumn[] GetItemColumns(object dataContext)
        {
            // The list of columns
            List<IBindingDataColumn> columns = new List<IBindingDataColumn>();

            // Get item type
            Type itemType = GetItemType(dataContext);

            // If there is a valid item type
            if(itemType != null)
            {
                // Loop all properties
                foreach(PropertyInfo property in itemType.GetProperties(BindingDataReader.BindingFlags))
                {
                    // Add data column for property
                    columns.Add(new BindingDataColumn(property));
                }
            }

            // Return all columns
            return columns.ToArray();
        }

        /// <summary>
        /// Gets the item item.
        /// </summary>
        /// <param name="dataContext">The data source.</param>
        /// <returns></returns>
        internal static Type GetItemType(object dataContext)
        {
            // The default object type
            Type objectType = typeof(object);

            // Get items
            IEnumerable items = dataContext as IEnumerable;

            // If there is a valid items
            if (items != null)
            {
                // Get items type
                Type itemsType = items.GetType();

                // Get typed enumerable type
                Type typedEnumerableType = itemsType.FindInterfaces((type, data) => type.Name == "IEnumerable`1", null).FirstOrDefault();

                // If there is a valid enumerable type
                if (typedEnumerableType != null)
                {
                    // Get the type type
                    Type itemType = typedEnumerableType.GetGenericArguments().FirstOrDefault();

                    // If there is a valid type
                    if(itemType != null && itemType != objectType)
                    {
                        // Return item type
                        return itemType;
                    }
                }

                // Loop all values
                foreach (object valueItem in items)
                {
                    // If there is a valid value
                    if (valueItem != null)
                    {
                        // Set the item type
                        return valueItem.GetType();                        
                    }
                }


            }
            // If there is a valid data source
            else if (dataContext != null)
            {
                // Set the item type
                return dataContext.GetType();
            }

            return null;
        }
    }
}
