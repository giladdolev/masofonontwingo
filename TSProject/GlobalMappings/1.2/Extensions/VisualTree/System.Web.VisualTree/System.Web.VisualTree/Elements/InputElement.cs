﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Base class for all field element (TextBox, CheckBox, ComboBox etc.)
    /// </summary>
    [DesignTimeVisible(false)]
    public class InputElement : ControlElement
    {

        private AutoCompleteMode _autoCompleteMode = AutoCompleteMode.None;

        /// <summary>
        /// Gets or sets the automatic complete mode.
        /// </summary>
        /// <value>
        /// The automatic complete mode.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(AutoCompleteMode.None)]
        public AutoCompleteMode AutoCompleteMode
        {
            get { return _autoCompleteMode; }

            set
            {

                if (_autoCompleteMode != value)
                {
                    _autoCompleteMode = value;
                    OnPropertyChanged("AutoCompleteMode");
                }
            }
        }
    }
}
