using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ImageList : VisualElement
    {
        private readonly ImageCollection _images;
        private readonly KeyCollection _keys;

        private ImageListStreamer _imageStream = null;


        public ImageList()
        {
            _images = new ImageCollection(this, "Items");
            _keys = new KeyCollection(this, "Items");
        }



        public ImageList(IContainer container)
            : this()
        {

        }



        public Color TransparentColor
        {
            get;
            set;
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // If is a ResourceReference
            if (visualElement is ResourceReference)
            {
                this.Images.Add((ResourceReference)visualElement);
                return;
            }
            // If is a KeyItem
            if (visualElement is KeyItem)
            {
                this.Keys.Add((KeyItem)visualElement);
                return;
            }
            base.Add(visualElement, insert);
        }

        [IDReferenceProperty]
        public ImageListStreamer ImageStream
        {
            get
            {
                return _imageStream;
            }
            set
            {
                _imageStream = value;

                if (value != null)
                {
                    _images.Clear();
                    _images.AddRange(value.Images);
                }
            }
        }



        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ResourceReference>))]
        public ImageCollection Images
        {
            get
            {
                return _images;
            }
        }


        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<KeyItem>))]
        public KeyCollection Keys
        {
            get
            {
                return _keys;
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                // If there are images
                if (_images != null)
                {
                    // Loop all images
                    foreach (VisualElement image in Images)
                    {
                        // Return column
                        yield return image;
                    }

                    // Loop all image key
                    foreach (VisualElement key in Keys)
                    {
                        yield return key;
                    }
                }
            }
        }


        public Size ImageSize
        {
            get;
            set;
        }
    }
}
