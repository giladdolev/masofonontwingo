namespace System.Web.VisualTree.Elements
{
	public class CommonDialog : VisualElement
	{
        /// <summary>
        /// Gets or sets a value indicating whether [cancel error].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cancel error]; otherwise, <c>false</c>.
        /// </value>
        public bool CancelError
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        public string Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index of the filter.
        /// </summary>
        /// <value>
        /// The index of the filter.
        /// </value>
        public int FilterIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the maximum size of the file.
        /// </summary>
        /// <value>
        /// The maximum size of the file.
        /// </value>
        public long MaxFileSize
        {
            get;
            set;
        }


        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <returns></returns>
        public static DialogResult ShowDialog()
        {
            return ShowDialog(null);
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <returns></returns>
        public static DialogResult ShowDialog(EventHandler<WindowClosedEventArgs> closedAction)
        {
            return DialogResult.None;
        }
	}
}
