namespace System.Web.VisualTree.Elements
{
	public class CalloutAnnotation : TextAnnotation
	{
		/// <summary>
		///Gets or sets the annotation callout style.
		/// </summary>
		public virtual CalloutStyle CalloutStyle
		{
			get;
			set;
		}
	}
}
