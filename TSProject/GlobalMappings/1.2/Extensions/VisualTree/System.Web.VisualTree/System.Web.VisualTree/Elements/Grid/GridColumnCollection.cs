using System.Collections;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    public class GridColumnCollection : VisualElementCollection<GridColumn>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name</param>
        public GridColumnCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnCollection"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        internal GridColumnCollection(VisualElement parentElement)
            : base(parentElement, new CollectionBindingColumnsChangeTarget())
        {

        }

        /// <summary>
        /// Adds a <see cref="T:System.Web.VisualTree.Elements.GridColumn" /> with the given column name and column header text to the collection.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// <returns></returns>
        public virtual int Add(string columnName, string headerText)
        {
            GridColumn column = new GridColumn(columnName, headerText, typeof(string));
            this.Add(column);

            return this.IndexOf(column);
        }

        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, GridColumn item)
        {
            base.InsertItem(index, item);

            GridElement grid = this.Owner as GridElement;
            if (grid != null)
            {
                // Initialize DataMember for auto-generated columns
                if (grid.AutoGenerateColumns && String.IsNullOrEmpty(item.DataMember))
                {
                    item.DataMember = item.ID;
                }
                grid.PerformAddedColumn(new ValueChangedArgs<int>(index));
            }
        }

        /// <summary>
        ///  Gets the index of the given string in the collection.    
        /// </summary>
        /// <param name="colName">The string to return the index of</param>
        /// <returns>The index of the given column name.</returns>
        public int IndexOf(string colName)
        {
            foreach (GridColumn col in this)        
            {
                if (col.ID == colName)
                    return this.Items.IndexOf(col);
            }
            return -1;
        }

        /// <summary>
        /// Adds range of GridColumn.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(GridColumn[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (GridColumn item in items)
            {
                // Add the item
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }
        /// <summary>
        /// Determines whether the collection contains the column referred to by the given name.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        /// true if the column is contained in the collection; otherwise, false.
        /// </returns>
        /// <exception cref="ArgumentNullException">columnName is empty</exception>
        public bool Contains(string columnName)
        {
            if (columnName == null)
                throw new ArgumentNullException("columnName is empty");
            foreach (GridColumn col in this)
            {
                if (col.ID == columnName)
                    return true;
            }
            return false;
        }
    }
}
