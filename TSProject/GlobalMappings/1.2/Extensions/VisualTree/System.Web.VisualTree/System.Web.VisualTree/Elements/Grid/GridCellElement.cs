using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents an individual cell in a System.Web.VisualTree.Elements.GridElement control
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.GridItemElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModelField" />
    public abstract class GridCellElement : GridItemElement, IBindingViewModelField
    {
        private object _value;
        private bool _protect;

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value
        {
            get { return _value; }
            set 
            { 
                _value = value; 
                GridRow row = this.ParentElement as GridRow;
                if (row != null)
                {
                    GridElement grid = row.ParentElement as GridElement;
                    if (grid != null)
                    {
                        grid.Rows.UpdateItem(row);
                        //grid.updat(row.Index, this.ColumnIndex, value.ToString());
                        grid.IsDirty = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        public int SelectedIndex
        {
            get 
            {
                // If the column is not ComboBoxColumn return -1
                if (this.OwningColumn.GetType() == typeof(GridComboBoxColumn))
                {
                    GridElement grid = this.ParentElement.ParentElement as GridElement;

                    if (grid != null)
                    {
                        string[] values = grid.ComboBoxList[this.OwningColumn.DataMember].Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (values[i].Contains(Convert.ToString(this.Value)))
                            {
                                return i;
                            }
                        }
                    }
                }
                return -1;            
            }

            set
            {
                var column = this.OwningColumn;
                GridElement gridElement = column.ParentElement as GridElement;

                if (column is GridComboBoxColumn)
                {
                    if (gridElement != null)
                    {
                        string[] values = gridElement.ComboBoxList[column.DataMember].Split(',');
                        if (values.Length <= value || value < 0)
                        {
                           
                            return;
                        }
                        
                        this.Value = Convert.ToString(values[value]);
                        gridElement.SetComboboxSelectedIndex(RowIndex, ColumnIndex, value);                    
                    }
                }  
            }
        }


        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                this.Value = value;
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public GridCellElement Clone()
        {
            return (GridCellElement)this.MemberwiseClone();
        }

        private bool _selected = false;
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="GridCellElement"/> is selected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if selected; otherwise, <c>false</c>.
        /// </value>
        public bool Selected
        {
            get
            {
                GridRow row = this.ParentElement as GridRow;
                GridElement grid = row.ParentElement as GridElement;
                if (row != null && grid.RowHeaderVisible == false)
                {
                    return row.Selected;
                }
                return _selected;
            }
            set
            {
                GridRow row = this.ParentElement as GridRow;
                GridElement grid = row.ParentElement as GridElement;
                if (row != null && grid.RowHeaderVisible == false)
                {
                    if (row.Selected != value)
                    {
                        row.Selected = value;
                    }
                }
                else
                {
                    if (_selected != value)
                    {
                        _selected = value;
                        if (_selected == true)
                        {
                            grid.SelectCell(this.RowIndex, this.ColumnIndex);
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public GridCellStyle Style
        {
            get;
            set;
        }




        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex
        {
            get
            {
                GridRow row = this.ParentElement as GridRow;
                if (row != null)
                {
                    GridElement grd = row.ParentElement as GridElement;
                    if (grd != null)
                    {
                        return grd.Rows.IndexOf(row);
                    }
                }
                return 0;
            }
            set
            {

            }
        }


        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int ColumnIndex
        {
            get
            {
                GridRow row = ParentElement as GridRow;
                if (row != null)
                {
                    return row.Cells.IndexOf(this);
                }
                return 0;
            }
            set
            {

            }
        }


        private GridColumn _owningColumn;
        /// <summary>
        /// Gets the owning column.
        /// </summary>
        /// <value>
        /// The owning column.
        /// </value>
        /// 
        [DesignerIgnore]
        public GridColumn OwningColumn
        {
            get { return _owningColumn; }
            set { _owningColumn = value; }
        }




        /// <summary>
        /// Gets the formatted value.
        /// </summary>
        /// <value>
        /// The formatted value.
        /// </value>
        public object FormattedValue
        {
            get { return default(object); }
        }

        /// <summary>
        /// Gets the type of the field.
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public Type FieldType
        {
            get 
            { 
                GridColumn column = this.OwningColumn;
                if (column != null)
                {
                    return column.DataType;
                }
                return typeof(object);
            }
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool SetValue(object value)
        {
            this.Value = value;
            return true;
        }

        private bool _button1Visible = true;
        public bool Button1Visible
        {
            get
            {
                return this._button1Visible;
            }
            set
            {
                this._button1Visible = value;
                (this.Parent.ParentElement as GridElement).SetButtonVisibility(this.RowIndex, this.ColumnIndex, 1, this._button1Visible);
            }
        }

        private bool _button2Visible = true;
        public bool Button2Visible
        {
            get
            {
                return this._button2Visible;
            }
            set
            {
                this._button2Visible = value;
                (this.Parent.ParentElement as GridElement).SetButtonVisibility(this.RowIndex, this.ColumnIndex, 2, this._button2Visible);
            }
        }


        /// <summary>
        /// Gets or sets the Protect.
        /// </summary>
        /// <value>
        /// The Protect.
        /// </value>
        public bool Protect
        {
            get { return _protect; }
            set
            {
                GridRow row = this.ParentElement as GridRow;
                if (row != null)
                {
                    GridElement grd = row.ParentElement as GridElement;
                    if (grd != null)
                    {
                        if (_protect != value)
                        {
                            grd.InvokeMethod("SetProtectCell", new GridIndexesData(this.RowIndex, this.ColumnIndex, value));
                            _protect = value;
                        }
                    }
                }


            }
        }
    }
}
