﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for storing layout information
    /// </summary>
    public class ControlElementTableLayout : ControlElementLayout
    {
        /// <summary>
        /// The column
        /// </summary>
        private int _column;

        /// <summary>
        /// The row
        /// </summary>
        private int _row;

        /// <summary>
        /// The row span
        /// </summary>
        private int _rowSpan;

        /// <summary>
        /// The column span
        /// </summary>
        private int _columnSpan;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutPanelCellPosition" /> struct.
        /// </summary>
        /// <param name="layout">The layout.</param>
        /// <param name="column">The column.</param>
        /// <param name="row">The row.</param>
        /// <param name="columnSpan">The column span.</param>
        /// <param name="rowSpan">The row span.</param>
        public ControlElementTableLayout(ControlElementLayout layout, int column, int row, int columnSpan, int rowSpan)
            : base(layout)
        {
            // Set local variables
            _column = column;
            _row = row;
            _rowSpan = rowSpan;
            _columnSpan = columnSpan;

            // Get old layout
            ControlElementTableLayout oldLayout = layout as ControlElementTableLayout;

            // If there is an old layout
            if (oldLayout != null)
            {
                if (_rowSpan < 1)
                {
                    _rowSpan = oldLayout._rowSpan;
                }

                if (_columnSpan < 1)
                {
                    _columnSpan = oldLayout._columnSpan;
                }

                if (_column < 0)
                {
                    _column = oldLayout._column;
                }

                if (_row < 0)
                {
                    _row = oldLayout._row;
                }
            }


            _rowSpan = Math.Max(_rowSpan, 1);
            _columnSpan = Math.Max(_columnSpan, 1);
            _column = Math.Max(_column, 0);
            _row = Math.Max(_row, 0);
        }




        /// <summary>
        /// Gets the row span.
        /// </summary>
        /// <value>
        /// The row span.
        /// </value>
        public int RowSpan
        {
            get
            {
                return _rowSpan;
            }
        }


        /// <summary>
        /// Gets the column span.
        /// </summary>
        /// <value>
        /// The column span.
        /// </value>
        public int ColumnSpan
        {
            get
            {
                return _columnSpan;
            }
        }

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        public int Row
        {
            get { return _row; }
            set { _row = value; }
        }


        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public int Column
        {
            get { return _column; }
            set { _column = value; }
        }

        /// <summary>
        /// Gets or sets the required columns.
        /// </summary>
        /// <value>
        /// The required columns.
        /// </value>
        public int RequiredColumns
        {
            get
            {
                return _column + _columnSpan;
            }
        }

        /// <summary>
        /// Gets the required rows.
        /// </summary>
        /// <value>
        /// The required rows.
        /// </value>
        public int RequiredRows
        {
            get
            {
                return _row + _rowSpan;
            }
        }
    }
}
