﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ResourceReference : VisualElement, IResourceReference
    {
        private static Dictionary<string, ResourceReference> _resources = new Dictionary<string, ResourceReference>(StringComparer.OrdinalIgnoreCase);


        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceReference"/> class.
        /// </summary>
        public ResourceReference()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceReference"/> class.
        /// </summary>
        public ResourceReference(string source)
        {
            this.Source = source;
        }


        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public virtual string Source
        {
            get;
            set;
        }



        /// <summary>
        /// Registers the specified resource identifier.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="resourceReference">The resource reference.</param>
        public static void Register(string resourceId, ResourceReference resourceReference)
        {
            if(!string.IsNullOrEmpty(resourceId) && resourceReference != null)
            {
                _resources[resourceId] = resourceReference;
            }
        }




        /// <summary>
        /// Resolves the specified resource identifier.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static ResourceReference Resolve(string resourceId)
        {
            ResourceReference resourceReference = null;

            // If there is a valid resource id
            if (!string.IsNullOrEmpty(resourceId))
            {
                // Try to get resource id
                _resources.TryGetValue(resourceId, out resourceReference);
            }

            return resourceReference;
        }




        /// <summary>
        /// Resolves the specified resource identifier.
        /// </summary>
        /// <typeparam name="TReferenceType">The type of the reference type.</typeparam>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="creator">The creator.</param>
        /// <returns></returns>
        public static TReferenceType Resolve<TReferenceType>(string resourceId, Func<TReferenceType> creator)
            where TReferenceType : ResourceReference
        {
            // The resource reference
            ResourceReference resourceReference = null;

            // If there is a valid resource id
            if(!string.IsNullOrEmpty(resourceId))
            {
                // Try to get resource id
                if(!_resources.TryGetValue(resourceId, out resourceReference))
                {
                    // If there is a valid creator
                    if(creator != null)
                    {
                        // Call creator
                        resourceReference = creator();

                        // If there is a valid resource reference
                        if(resourceReference != null)
                        {
                            // Cache resource
                            _resources[resourceId] = resourceReference;
                        }
                    }
                }
            }

            // Return resource reference 
            return resourceReference as TReferenceType;
        }




        /// <summary>
        /// Resolves the specified resource identifier.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static TReferenceType Resolve<TReferenceType>(string resourceId)
            where TReferenceType : ResourceReference
        {
            return  Resolve<TReferenceType>(resourceId,null);

        }



        /// <summary>
        /// Loads the specified absolute path.
        /// </summary>
        /// <param name="absolutePath">The absolute path.</param>
        /// <returns></returns>
        public static ResourceReference Load(string absolutePath)
        {
            // Check file exists
            if (!String.IsNullOrEmpty(absolutePath) && File.Exists(absolutePath))
            {
                // Get file type
                string extension = Path.GetExtension(absolutePath);
                switch (extension.ToUpperInvariant())
                {
                        // Load image
                    case ".JPEG":
                    case ".JPG":
                    case ".BMP":
                    case ".PNG":
                        Image image = Image.FromFile(absolutePath);
                        return image;
                }

            }
            return null;
        }




        /// <summary>
        /// Items the check action.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ResourceReference(Image image)
        {
            if (image != null)
            {
				using (MemoryStream stream = new MemoryStream())
				{
					image.Save(stream, ImageFormat.Png);
					stream.Seek(0, SeekOrigin.Begin);
					string base64 = Convert.ToBase64String(stream.ToArray());

					return new ImageReference(string.Concat("data:image/gif;base64,", base64), image);
				}
            }

            return null;
        }




        /// <summary>
        /// Companion to the operator overload resource reference.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static ResourceReference ToResourceReference(Image image)
        {
            if (image != null)
            {
				using (MemoryStream stream = new MemoryStream())
				{
					image.Save(stream, ImageFormat.Png);
                stream.Seek(0, SeekOrigin.Begin);
                string base64 = Convert.ToBase64String(stream.ToArray());

                return new ImageReference(string.Concat("data:image/gif;base64,", base64), image);
            }
            }

            return null;
        }




        /// <summary>
        /// Items the check action.
        /// </summary>
        /// <param name="icon">The icon.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ResourceReference(Icon icon)
        {
            if (icon != null)
            {
				using (MemoryStream stream = new MemoryStream())
				{
					icon.Save(stream);
					stream.Seek(0, SeekOrigin.Begin);
					string base64 = Convert.ToBase64String(stream.ToArray());

					return new IconReference(string.Concat("data:image/icon;base64,", base64), icon);
				}
            }

            return null;
        }




        /// <summary>
        /// Companion to the operator overload ResourceReference.
        /// </summary>
        /// <param name="icon">The icon.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static ResourceReference ToResourceReference(Icon icon)
        {
            if (icon != null)
            {
				using (MemoryStream stream = new MemoryStream())
				{
                icon.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                string base64 = Convert.ToBase64String(stream.ToArray());

                return new IconReference(string.Concat("data:image/icon;base64,", base64), icon);
            }
            }

            return null;
        }




        /// <summary>
        /// Gets the size of the image.
        /// </summary>
        /// <param name="resourceReference">The resource reference.</param>
        /// <returns></returns>
        public static Size GetImageSize(ResourceReference resourceReference)
        {
            return GetImageSize(resourceReference, Size.Empty);
        }




        /// <summary>
        /// Gets the size of the image.
        /// </summary>
        /// <param name="resourceReference">The resource reference.</param>
        /// <param name="defaultSize">The default size.</param>
        /// <returns></returns>
        public static Size GetImageSize(ResourceReference resourceReference, Size defaultSize)
        {
            ImageReference imageReference = resourceReference as ImageReference;
            if (imageReference != null)
            {
                return imageReference.ImageSize;
            }
            else
            {
                IconReference iconReference = resourceReference as IconReference;
                if (iconReference != null)
                    return iconReference.IconSize;
            }

            return defaultSize;
        }
    }
}
