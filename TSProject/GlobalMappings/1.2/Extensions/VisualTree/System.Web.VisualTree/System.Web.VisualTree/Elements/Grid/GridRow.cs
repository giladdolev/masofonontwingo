using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a row in a System.Web.VisualTree.Elements.GridElement control
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.GridBand" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModelRecord" />
    public class GridRow : GridBand, IBindingViewModelRecord
    {
        private bool _readOnly;
        internal bool _selected = false;
        private GridCellCollection _cells = null;



        /// <summary>
        /// Initializes a new instance of the <see cref="GridRow"/> class.
        /// </summary>
        public GridRow()
        {
            _cells = new GridCellCollection(this, "Cells");
            _widgetControls = new List<ControlElement>();
        }




        /// <summary>
        /// Gets or sets a value indicating whether [read only].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [read only]; otherwise, <c>false</c>.
        /// </value>
        public override bool ReadOnly
        {
            get { return _readOnly; }
            set
            {
                _readOnly = value;

                // Notify property ReadOnly changed
                OnPropertyChanged("ReadOnly");
            }
        }




        /// <summary>
        /// Gets the cells.
        /// </summary>
        /// <value>
        /// The cells.
        /// </value>
        [DesignerIgnore]
        public GridCellCollection Cells
        {
            get
            {
                return _cells;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the row is the row for new records.
        /// <value>
        /// true if the row is the last row in the GridElement, which is used for the entry of a new row of data; otherwise, false.
        /// </value>
        /// </summary>
        [DesignerIgnore]
        public bool IsNewRow
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets a value indicating whether the row is selected.
        /// </summary>
        /// <value> 
        /// true if the row is selected; otherwise, false.
        /// </value>
        public override bool Selected
        {
            get { return _selected; }
            set
            {
                if (value != false)
                {
                    GridElement grideleElement = this.ParentElement as GridElement;
                    if (grideleElement != null)
                    {
                        // Select the row 
                        grideleElement.CurrentRow = this;
                        _selected = value;
                    }
                }
            }
        }
        public bool SelectedInternal
        {
            get { return _selected; }
            set
            {
                // get indexes of selected rows.
                List<int> lstRows = new List<int>() { this.Index };
                // get selected cells 
                List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(this.Index, this.GridElement.SelectedColumnIndex) };
                // build the new args 
                GridElementSelectionChangeEventArgs selectionChangeEventArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);
                //Raise selection changed event .
                this.GridElement.PerformSelectionChangedInternal(selectionChangeEventArgs);

            }
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool SetValue(string key, object value)
        {
            GridCellElement cell = this.Cells[key];
            if (cell != null)
            {
                cell.Value = value;
                return true;
            }

            return false;
        }


        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public IBindingViewModelField GetField(string key)
        {
            return this.Cells[key];
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get
            {
                GridElement grd = this.Parent as GridElement;
                if (grd != null)
                {
                    return grd.Rows.IndexOf(this);
                }
                return -1;
            }
        }
        private List<ControlElement> _widgetControls = null;
        public List<ControlElement> WidgetControls
        {
            get { return _widgetControls; }
        }

        internal int GetWidgetControlByDataMember(string dataMemeber)
        {
            for (int i = 0; i < this.WidgetControls.Count; i++)
            {
                if (this.WidgetControls[i].DataMember == dataMemeber)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
