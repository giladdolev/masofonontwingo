﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum PageViewMode
    {
        Strip,
        Stack,
        Outlook,
        ExplorerBar,
        Backstage
    }
}
