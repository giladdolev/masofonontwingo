﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements.Touch
{
    public class TCarouselElement : PanelElement, ITVisualElement, ITVisualContainerElement
    {
        private int _selectedIndex = 0;


        /// <summary>
        /// Sets the active control.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        public void SetActiveControl(ControlElement controlElement)
        {
            // Get the control index
            int controlIndex = this.Controls.IndexOf(controlElement);

            // If we found the control
            if(controlIndex > -1)
            {
                // Set selected control index
                this.SelectedIndex = controlIndex;
            }
        }



        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        [RendererPropertyDescription]
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                _selectedIndex = value;

                // Indicate selected index changed
                OnPropertyChanged("SelectedIndex");
            }
        }

        /// <summary>
        /// Ensures the visible child.
        /// </summary>
        /// <param name="element">The element.</param>
        public override void EnsureVisibleChild(VisualElement element)
        {
            this.SetActiveControl(element as ControlElement);
        }

        /// <summary>
        /// Adds the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected override void AddWindow(WindowElement windowElement)
        {
            // If there is a valid window
            if (windowElement != null)
            {
                // Add window element to panel
                this.Controls.Add(windowElement);

                // Make sure child is visible
                this.EnsureVisibleChild(windowElement);

                // Ensure redraw
                this.Refresh();
            }
        }


        /// <summary>
        /// Removes the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        protected override void RemoveWindow(WindowElement windowElement)
        {
            // If there is a valid window
            if (windowElement != null)
            {
                // Add window element to panel
                this.Controls.Remove(windowElement);

                // Ensure redraw
                this.Refresh();
            }
        }

        /// <summary>
        /// Gets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        TVisualLayout ITVisualContainerElement.Layout
        {
            get
            {
                return TVisualLayout.Default;
            }
        }
    }
}
