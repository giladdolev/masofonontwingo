﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DataViewTextField : DataViewField
    {

        private string _format = "[general]";
        /// <summary>
        /// Gets or sets the Format.
        /// </summary>
        /// <value>
        /// The Format.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue("[general]")]
        [Category("Behavior")]
        public string Format
        {
            get { return _format; }
            set
            {
                if (_format != value)
                {
                    _format = value;

                    OnPropertyChanged("Format");
                }
            }
        }

        
        private string _Mask;
        /// <summary>
        /// Gets or sets the Mask.
        /// </summary>
        /// <value>
        /// The Mask.
        /// </value>
        [RendererPropertyDescription]
        [Category("Behavior")]
        public string Mask
        {
            get { return _Mask; }
            set
            {
                if (_Mask != value)
                {
                    _Mask = value;

                    OnPropertyChanged("Mask");
                }
            }
        }


    }
}
