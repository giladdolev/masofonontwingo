﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements.Touch
{
    public class TTabItem : TabItem, ITVisualContainerElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TTabItem"/> class.
        /// </summary>
        public TTabItem()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="TTabItem"/> class.
        /// </summary>
        /// <param name="txt">The text.</param>
        public TTabItem(string txt)
            : base(txt)
        {
        }



        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        public TVisualLayout Layout
        {
            get;
            set;
        }
    }
}
