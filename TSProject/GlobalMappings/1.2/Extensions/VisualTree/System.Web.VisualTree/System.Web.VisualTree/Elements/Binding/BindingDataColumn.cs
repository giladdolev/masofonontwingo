﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for binding columns
    /// </summary>
    internal class BindingDataColumn : IBindingDataColumn
    {

        /// <summary>
        /// The empty array
        /// </summary>
        public static readonly IBindingDataColumn[] EmptyArray = new IBindingDataColumn[] { };

        /// <summary>
        /// The property
        /// </summary>
        private PropertyInfo _property = null;

        /// <summary>
        /// The property name
        /// </summary>
        private readonly string _name = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDataColumn"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        public BindingDataColumn(PropertyInfo property)
        {
            // If property is null
            if(property == null)
            {
                throw new ArgumentNullException("property");
            }

            _property = property;
            _name = property.Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDataColumn" /> class.
        /// </summary>
        /// <param name="name">The column name.</param>
        /// <exception cref="System.ArgumentNullException">name</exception>
        public BindingDataColumn(string name)
        {
            // If name is an empty name
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            this._name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public Type Type
        {
            get
            {
                if (_property != null)
                {
                    return _property.PropertyType;
                }

                return typeof(object);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        {
            get
            {
                return _property != null;
            }
        }



        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public object GetValue(object item, int index)
        {
            if (_property != null && item != null)
            {
                return _property.GetValue(item);
            }

            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsIndex
        {
            get
            {
                return false;
            }
        }


        /// <summary>
        /// Binds the property.
        /// </summary>
        /// <param name="property">The property.</param>
        public void Bind(PropertyInfo property)
        {
            _property = property;
        }


        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        public string DataTypeName
        {
            get
            {
                if(_property!= null)
                {
                    string tempName = _property.PropertyType.Name;
                    return tempName;
                }

                return null;
            }
        }

        private ColumnSortMode _sortType;

        /// <summary>
        /// Gets the name of the Sort type.
        /// </summary>
        /// <value>
        /// The name of the Sort type.
        /// </value>
        public ColumnSortMode SortType
        {
            get
            {
                return _sortType;
            }
            set
            {
                this._sortType = value;
            }
        }
    }

}
