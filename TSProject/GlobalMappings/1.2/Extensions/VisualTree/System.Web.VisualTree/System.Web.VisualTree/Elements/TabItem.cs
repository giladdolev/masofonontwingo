﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.Mvc.VisualTree.Elements
{
    public class TabItem : ControlContainerElement
    {
        /// <summary>
        /// The tab title
        /// </summary>
        private string mstrTitle = String.Empty;


        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
        public TabItem()
        {

        }



        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get
            {
                return mstrTitle;
            }
            set
            {
                mstrTitle = value;
            }
        }



        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public object Header
        {
            get;
            set;
        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="objVisualElement">The visual element.</param>
        public override void Add(VisualElement objVisualElement)
        {
            // Get the tag page settings
            TabItemSettings objTabPageSettings = objVisualElement as TabItemSettings;

            // If there is a valid tab page settings
            if(objTabPageSettings != null)
            {
                // Set properties
                this.Header = objTabPageSettings.Header;
                this.Title = objTabPageSettings.Title;
            }

            base.Add(objVisualElement);
        }

    }
}
