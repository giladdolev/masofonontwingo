﻿using System.Globalization;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{Series}" />
    public class SeriesCollection : VisualElementCollection<Series>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesCollection" /> class.
        /// </summary>
        /// <param name="chart">The chart.</param>
        /// <param name="propertyName">The property name.</param>
        internal SeriesCollection(Chart chart, string propertyName)
            : base(chart, propertyName)
        {

        }


        /// <summary>
        /// Inserts an element into the <see cref="T:System.Collections.ObjectModel.Collection`1" /> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert. The value can be null for reference types.</param>
        protected override void InsertItem(int index, Series item)
        {
            base.InsertItem(index, item);

            
        }


        protected override void SetItem(int index, Series objItem)
        {
            base.SetItem(index, objItem);
        }



        /// <summary>
        /// Nexts the name of the unique.
        /// </summary>
        /// <returns></returns>
        public string NextUniqueName()
        {
            string name = "Series";
           
            int nameIndex = 1;

            string uniqueName = name + nameIndex;

            while(this[uniqueName] != null)
            {
                nameIndex++;

                uniqueName = name + nameIndex.ToString(CultureInfo.InvariantCulture);
            }

            return uniqueName;
        }
    }
}
