namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{ColumnHeader}" />
	public class ColumnHeaderCollection : VisualElementCollection<ColumnHeader>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnHeaderCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        public ColumnHeaderCollection(VisualElement objParentElement)
            : base(objParentElement, new CollectionBindingColumnsChangeTarget())
        {

        }


        /// <summary>
        /// Default ColumnHeader Constructor with values :
        /// index : 0, key = 0, text = "", width = 60, alignment = null, icon = null
        /// </summary>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add()
        {
            return Add( 0, "", "", 60, null, null);
        }


        /// <summary>
        /// ColumnHeader constructor that takes coulmn index
        /// </summary>
        /// <param name="index">Index of the new ColumnHeader</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index )
        {
            return Add(index , "", "", 60, null, null);
        }


        /// <summary>
        /// ColumnHeader constructor that takes column index and key
        /// </summary>
        /// <param name="index">Index of the ColumnHeader</param>
        /// <param name="key">Key of the ColumnHeader</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index, string key )
        {
            return Add(index , key, "", 60, null, null);
        }


        /// <summary>
        /// ColumnHeader constructor that take index, key, text
        /// </summary>
        /// <param name="index">Index of the ColumnHeader</param>
        /// <param name="key">Key of the ColumnHeader</param>
        /// <param name="text">Text of the ColumnHeader</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index , string key , string text )
        {
            return Add(index , key , text , 60, null, null);
        }


        /// <summary>
        /// ColumnHeader constructor that take index, key, text, width
        /// </summary>
        /// <param name="index">Index of the ColumnHeader</param>
        /// <param name="key">Key of the ColumnHeader</param>
        /// <param name="text">Text of the ColumnHeader</param>
        /// <param name="width">Width of the ColumnHeader</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index , string key, string text , float width )
        {
            return Add(index, key , text , width , null, null);
        }


        /// <summary>
        /// ColumnHeader constructor that take index, key, text, width
        /// </summary>
        /// <param name="index">Index of the ColumnHeader</param>
        /// <param name="key">Key of the ColumnHeader</param>
        /// <param name="text">Text of the ColumnHeader</param>
        /// <param name="width">Width of the ColumnHeader</param>
        /// <param name="alignment">Alighment of the text</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index, string key , string text , float width , object alignment )
        {
            return Add(index , key , text , width , alignment , null);
        }


        /// <summary>
        /// ColumnHeader constructor that take index, key, text, width
        /// </summary>
        /// <param name="index">Index of the ColumnHeader</param>
        /// <param name="key">Key of the ColumnHeader</param>
        /// <param name="text">Text of the ColumnHeader</param>
        /// <param name="width">Width of the ColumnHeader</param>
        /// <param name="alignment">Alighment of the text</param>
        /// <param name="icon">Icon of the ColumnHeader</param>
        /// <returns>ColumnHeader Object</returns>
        public ColumnHeader Add(int index, string key, string text, float width, object alignment, object icon)
        {
            if (index == 0)
            {
                index = Count;
            }

            ColumnHeader columnHeader;

            if (alignment != null)
                columnHeader = new ColumnHeader() { Text = text, Width = (int)width, Index = index, TextAlign = (HorizontalAlignment)alignment };
            else
                columnHeader = new ColumnHeader() { Text = text, Width = (int)width, Index = index};

            Add(columnHeader);

            return columnHeader;
        }

        /// <summary>
        /// Adds range of ColumnHeader.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <exception cref="System.ArgumentNullException">items</exception>
        public void AddRange(ColumnHeader[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach (ColumnHeader item in items)
            {
                // Add the item
                if (!this.Contains(item))
                {
                    Add(item);
                }
            }
        }
	}    
}
