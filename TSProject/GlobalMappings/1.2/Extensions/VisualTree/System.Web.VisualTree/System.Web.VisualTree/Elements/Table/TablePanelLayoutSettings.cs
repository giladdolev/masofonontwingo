﻿using System.ComponentModel;
using System.Globalization;
using System.Xml.Linq;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// The table panel settings
    /// </summary>
    [TypeConverter(typeof(TablePanelLayoutSettingsConverter))]
    [Serializable]
    public class TablePanelLayoutSettings
    {
        /// <summary>
        /// The columns
        /// </summary>
        private readonly string _columns;


        /// <summary>
        /// The rows
        /// </summary>
        private readonly string _rows;


        /// <summary>
        /// Initializes a new instance of the <see cref="TablePanelLayoutSettings"/> class.
        /// </summary>
        public TablePanelLayoutSettings()
        {

        }



        /// <summary>
        /// Initializes a new instance of the <see cref="TablePanelLayoutSettings"/> class.
        /// </summary>
        /// <param name="tablePanel">The table panel.</param>
        internal TablePanelLayoutSettings(TablePanel tablePanel)
        {

        }



        /// <summary>
        /// Initializes a new instance of the <see cref="TablePanelLayoutSettings"/> class.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <param name="rows">The rows.</param>
        public TablePanelLayoutSettings(string columns, string rows)
        {
            _columns = columns;
            _rows = rows;
        }


        /// <summary>
        /// Gets the rows.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public string Rows
        {
            get { return _rows; }
        }


        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public string Columns
        {
            get { return _columns; }
        }



        /// <summary>
        /// Froms the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal static TablePanelLayoutSettings FromString(string value)
        {
            // The table columns
            string columns = null;

            // The table rows
            string rows = null;

            // Get element from value
            XElement element = XElement.Parse(value);

            // If there is a valid element
            if (element != null)
            {
                // Get columns element
                XElement columnsElement = element.Element("Columns");

                // If there is a valid columns element
                if (columnsElement != null)
                {
                    // Get styles attribute
                    XAttribute stylesAttribute = columnsElement.Attribute("Styles");

                    // If there is a valid styles attribute
                    if (stylesAttribute != null)
                    {
                        // Get columns value
                        columns = stylesAttribute.Value;
                    }
                }

                // Get the rows element
                XElement rowsElement = element.Element("Rows");

                // If there is a valid rows element
                if (rowsElement != null)
                {
                    // Get the styles attribute
                    XAttribute stylesAttribute = rowsElement.Attribute("Styles");

                    // If there is a valid style attirbute
                    if (stylesAttribute != null)
                    {
                        // Get the rows value
                        rows = stylesAttribute.Value;
                    }
                }
            }

            // Return the table settings
            return new TablePanelLayoutSettings(columns, rows);
        }



        /// <summary>
        /// Applies settings to table panel.
        /// </summary>
        /// <param name="tablePanel">The table panel.</param>
        internal void Apply(TablePanel tablePanel)
        {
            // If there are valid rows
            if (_rows != null)
            {

                // Clear the row styles
                tablePanel.RowStyles.Clear();

                // Get rows
                string[] rows = _rows.Split(',');

                // Loop rows
                for (int index = 0; index < rows.Length; index += 2)
                {
                    // If there is a valid index
                    if (index + 1 < rows.Length)
                    {
                        // The height
                        int height = 0;

                        // Get rows height
                        if (int.TryParse(rows[index + 1], out height))
                        {
                            // Check rows index
                            switch (rows[index])
                            {
                                case "Percent":
                                    tablePanel.RowStyles.Add(new RowStyle(SizeType.Percent, height));
                                    break;
                                case "Absolute":
                                    tablePanel.RowStyles.Add(new RowStyle(SizeType.Absolute, height));
                                    break;
                                case "AutoSize":
                                default:
                                    tablePanel.RowStyles.Add(new RowStyle(SizeType.AutoSize, height));
                                    break;
                            }
                        }

                    }
                }
            }

            // If there are valid columns
            if (_columns != null)
            {

                // Clear the column styles
                tablePanel.ColumnStyles.Clear();

                // Get columns
                string[] columns = _columns.Split(',');

                // Loop columns
                for (int index = 0; index < columns.Length; index += 2)
                {
                    // If there is a valid index
                    if (index + 1 < columns.Length)
                    {
                        // The width
                        int width = 0;

                        // Get columns width
                        if (int.TryParse(columns[index + 1], out width))
                        {
                            // Check columns index
                            switch (columns[index])
                            {
                                case "Percent":
                                    tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, width));
                                    break;
                                case "Absolute":
                                    tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, width));
                                    break;
                                case "AutoSize":
                                default:
                                    tablePanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize, width));
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}
