using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a System.Web.VisualTree text box control.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.TextBoxBaseElement" />
    public class WidgetColumnTextBoxElement : TextBoxElement
    {
        private bool _hasEditChange = false;
        public bool HasEditChange
        {
            get
            {
                return this._hasEditChange;
            }
            set
            {
                this._hasEditChange = value;
            }
        }

        private bool _hasFocusEnter = false;
        public bool HasFocusEnter
        {
            get
            {
                return this._hasFocusEnter;
            }
            set
            {
                this._hasFocusEnter = value;
            }
        }


        private bool _hasItemChanged = false;
        public bool HasItemChanged
        {
            get
            {
                return this._hasItemChanged;
            }
            set
            {
                this._hasItemChanged = value;
            }
        }

        private bool _hasDoubleClick = false;
        public bool HasDoubleClick
        {
            get
            {
                return this._hasDoubleClick;
            }
            set
            {
                this._hasDoubleClick = value;
            }
        }

        private bool _hasClick = false;
        public bool HasClick
        {
            get
            {
                return this._hasClick;
            }
            set
            {
                this._hasClick = value;
            }
        }


        public override bool HasWidgetControlEditChangedListeners
        {
            get
            {
                return HasEditChange;
            }
        }

        public override bool HasWidgetControlItemChangedListeners
        {
            get
            {
                return HasItemChanged;
            }
        }

        public override bool HasWidgetControlFocusEnterListeners
        {
            get
            {
                return HasFocusEnter;
            }
        }


        public override bool HasWidgetControlDoubleClickListeners
        {
            get
            {
                return HasDoubleClick;
            }
        }

        public override bool HasWidgetControlClickListeners
        {
            get
            {
                return HasClick;
            }
        }

        public override void Focus()
        {
            WidgetColumn wdColunm = this.Parent as WidgetColumn;
            GridElement grd = null;
            if (wdColunm != null)
            {
                grd = wdColunm.Parent as GridElement;
                if (grd != null)
                {
                    wdColunm.SetFocus(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex() });

                }
            }

        }

        private bool _enabled = true;
        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public override bool Enabled
        {
            get { return _enabled; }
            set
            {
                // If is a different value
                if (_enabled != value)
                {
                    // Set enabled
                    _enabled = value;

                    WidgetColumn wdColunm = this.Parent as WidgetColumn;
                    GridElement grd = null;
                    if (wdColunm != null)
                    {
                        grd = wdColunm.Parent as GridElement;
                        if (grd != null)
                        {
                            wdColunm.SetEnabled(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex(), Enabled = value });
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Gets a value indicating whether [has key press listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has key press listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasKeyPressListeners
        {
            get { return true; }
        }

        private Color _backColor;
        /// <summary>
        /// Gets or sets the background color for the control.
        /// </summary>
        /// <value>
        /// A System.Drawing.Color that represents the background color of the control.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public override Color BackColor
        {
            get { return _backColor; }
            set
            {
                _backColor = value;


                WidgetColumn wdColunm = this.Parent as WidgetColumn;
                GridElement grd = null;
                if (wdColunm != null)
                {
                    grd = wdColunm.Parent as GridElement;
                    if (grd != null)
                    {
                        wdColunm.SetBackColor(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex(), BackColor = value });
                    }
                }
            }
        }


        
    }
}
