﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements.Touch
{
    public class TListViewElement : ListViewElement, ITVisualElement
    {
        [RendererPropertyDescription]
        public string ItemTemplate
        {
            get;
            set;
        }
    }
}
