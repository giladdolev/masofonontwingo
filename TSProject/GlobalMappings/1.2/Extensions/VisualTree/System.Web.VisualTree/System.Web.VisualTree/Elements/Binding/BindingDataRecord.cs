﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    internal class BindingDataRecord : IDataRecord
    {
        /// <summary>
        /// The columns
        /// </summary>
        private readonly IBindingDataColumn[] _columns;

        /// <summary>
        /// The names
        /// </summary>
        private readonly List<string> _names;

        /// <summary>
        /// The converters
        /// </summary>
        private static readonly TypeConverter _booleanConverter = TypeDescriptor.GetConverter(typeof(bool));
        private static readonly TypeConverter _byteConverter = TypeDescriptor.GetConverter(typeof(byte));
        private static readonly TypeConverter _charConverter = TypeDescriptor.GetConverter(typeof(char));
        private static readonly TypeConverter _dateTimeConverter = TypeDescriptor.GetConverter(typeof(DateTime));
        private static readonly TypeConverter _decimalConverter = TypeDescriptor.GetConverter(typeof(decimal));
        private static readonly TypeConverter _doubleConverter = TypeDescriptor.GetConverter(typeof(double));
        private static readonly TypeConverter _floatConverter = TypeDescriptor.GetConverter(typeof(float));
        private static readonly TypeConverter _guidConverter = TypeDescriptor.GetConverter(typeof(Guid));
        private static readonly TypeConverter _shortConverter = TypeDescriptor.GetConverter(typeof(short));
        private static readonly TypeConverter _intConverter = TypeDescriptor.GetConverter(typeof(int));
        private static readonly TypeConverter _longConverter = TypeDescriptor.GetConverter(typeof(long));
        private static readonly TypeConverter _stringConverter = TypeDescriptor.GetConverter(typeof(string));
        
        
        /// <summary>
        /// Initializes a new instance of the <see cref="BindingDataRecord"/> class.
        /// </summary>
        /// <param name="columns">The columns.</param>
        public BindingDataRecord(IBindingDataColumn[] columns)
        {
            // Set list of columns
            _columns = columns ?? new IBindingDataColumn[] { };

            // Create list of names
            _names = new List<string>();

            // Loop all columns
            foreach(IBindingDataColumn column in _columns)
            {
                // If there is a valid column
                if (column != null)
                {
                    // Add column name
                    _names.Add(column.Name);
                }
                else
                {
                    // Add empty name
                    _names.Add(null);
                }
            }
        }

        /// <summary>
        /// Gets the current item.
        /// </summary>
        /// <value>
        /// The current item.
        /// </value>
        protected virtual object CurrentItem
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the current item identifier.
        /// </summary>
        /// <value>
        /// The current item identifier.
        /// </value>
        public virtual object CurrentItemId
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the number of columns in the current row.
        /// </summary>
        public int FieldCount
        {
            get
            {
                return _columns.Length;
            }
        }

        /// <summary>
        /// Gets the value of the specified column as a Boolean.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>
        /// The value of the column.
        /// </returns>
        public bool GetBoolean(int i)
        {
            return (bool)_booleanConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the 8-bit unsigned integer value of the specified column.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>
        /// The 8-bit unsigned integer value of the specified column.
        /// </returns>
        public byte GetByte(int i)
        {
            return (byte)_byteConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the given buffer offset.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <param name="fieldOffset">The index within the field from which to start the read operation.</param>
        /// <param name="buffer">The buffer into which to read the stream of bytes.</param>
        /// <param name="bufferoffset">The index for <paramref name="buffer" /> to start the read operation.</param>
        /// <param name="length">The number of bytes to read.</param>
        /// <returns>
        /// The actual number of bytes read.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the character value of the specified column.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <returns>
        /// The character value of the specified column.
        /// </returns>
        public char GetChar(int i)
        {
            return (char)_charConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Reads a stream of characters from the specified column offset into the buffer as an array, starting at the given buffer offset.
        /// </summary>
        /// <param name="i">The zero-based column ordinal.</param>
        /// <param name="fieldoffset">The index within the row from which to start the read operation.</param>
        /// <param name="buffer">The buffer into which to read the stream of bytes.</param>
        /// <param name="bufferoffset">The index for <paramref name="buffer" /> to start the read operation.</param>
        /// <param name="length">The number of bytes to read.</param>
        /// <returns>
        /// The actual number of characters read.
        /// </returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Returns an <see cref="T:System.Data.IDataReader" /> for the specified column ordinal.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The <see cref="T:System.Data.IDataReader" /> for the specified column ordinal.
        /// </returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public virtual IDataReader GetData(int i)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Gets the column.
        /// </summary>
        /// <param name="i">The index of the column to find.</param>
        /// <returns></returns>
        private IBindingDataColumn GetColumn(int i)
        {
            // If there is a valid index
            if (i > -1)
            {
                // If columns are valid
                if (_columns != null && _columns.Length > i)
                {
                    // Return column
                    return _columns[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the data type information for the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The data type information for the specified field.
        /// </returns>
        public string GetDataTypeName(int i)
        {
            // Get the column
            IBindingDataColumn column = this.GetColumn(i);

            // If there is a valid column
            if(column != null)
            {
                // Return column
                return column.DataTypeName;
            }

            return null;
        }

        /// <summary>
        /// Gets the date and time data value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The date and time data value of the specified field.
        /// </returns>
        public DateTime GetDateTime(int i)
        {
            return (DateTime)_dateTimeConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the fixed-position numeric value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The fixed-position numeric value of the specified field.
        /// </returns>
        public decimal GetDecimal(int i)
        {
            return (decimal)_decimalConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the double-precision floating point number of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The double-precision floating point number of the specified field.
        /// </returns>
        public double GetDouble(int i)
        {
            return (double)_doubleConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the <see cref="T:System.Type" /> information corresponding to the type of <see cref="T:System.Object" /> that would be returned from <see cref="M:System.Data.IDataRecord.GetValue(System.Int32)" />.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The <see cref="T:System.Type" /> information corresponding to the type of <see cref="T:System.Object" /> that would be returned from <see cref="M:System.Data.IDataRecord.GetValue(System.Int32)" />.
        /// </returns>
        public Type GetFieldType(int i)
        {
            // Get the column
            IBindingDataColumn column = this.GetColumn(i);

            // If there is a valid column
            if (column != null)
            {
                // Return column
                return column.Type;
            }

            return null;
        }

        /// <summary>
        /// Gets the single-precision floating point number of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The single-precision floating point number of the specified field.
        /// </returns>
        public float GetFloat(int i)
        {
            return (float)_floatConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Returns the GUID value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The GUID value of the specified field.
        /// </returns>
        public Guid GetGuid(int i)
        {
            return (Guid)_guidConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the 16-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 16-bit signed integer value of the specified field.
        /// </returns>
        public short GetInt16(int i)
        {
            return (short)_shortConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the 32-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 32-bit signed integer value of the specified field.
        /// </returns>
        public int GetInt32(int i)
        {
            return (int)_intConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the 64-bit signed integer value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The 64-bit signed integer value of the specified field.
        /// </returns>
        public long GetInt64(int i)
        {
            return (long)_longConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Gets the name for the field to find.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The name of the field or the empty string (""), if there is no value to return.
        /// </returns>
        public string GetName(int i)
        {
            // Get the column
            IBindingDataColumn column = this.GetColumn(i);

            // If there is a valid column
            if (column != null)
            {
                // Return column
                return column.Name;
            }

            return null;
        }

        /// <summary>
        /// Return the index of the named field.
        /// </summary>
        /// <param name="name">The name of the field to find.</param>
        /// <returns>
        /// The index of the named field.
        /// </returns>
        public int GetOrdinal(string name)
        {
            return _names.IndexOf(name);
        }

        /// <summary>
        /// Gets the string value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The string value of the specified field.
        /// </returns>
        public string GetString(int i)
        {
            return (string)_stringConverter.ConvertFrom(this.GetValue(i));
        }

        /// <summary>
        /// Return the value of the specified field.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// The <see cref="T:System.Object" /> which will contain the field value upon return.
        /// </returns>
        public object GetValue(int i)
        {
            // If is the min value which is the id
            if (i == BindingDataReader.ID_INDEX)
            {
                // Return current id
                return this.CurrentItemId;
            }
            else
            {
                // Get the column
                IBindingDataColumn column = this.GetColumn(i);

                // If there is a valid column
                if (column != null)
                {
                    // Get value from current item
                    return column.GetValue(this.CurrentItem, i);
                }

                return null;
            }
        }

        

        /// <summary>
        /// Populates an array of objects with the column values of the current record.
        /// </summary>
        /// <param name="values">An array of <see cref="T:System.Object" /> to copy the attribute fields into.</param>
        /// <returns>
        /// The number of instances of <see cref="T:System.Object" /> in the array.
        /// </returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public int GetValues(object[] values)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Return whether the specified field is set to null.
        /// </summary>
        /// <param name="i">The index of the field to find.</param>
        /// <returns>
        /// true if the specified field is set to null; otherwise, false.
        /// </returns>
        public bool IsDBNull(int i)
        {
            return this.GetValue(i) == null;
        }

        /// <summary>
        /// Gets the column with the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                return this.GetValue(this.GetOrdinal(name));
            }
        }

        /// <summary>
        /// Gets the column located at the specified index.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public object this[int i]
        {
            get
            {
                return this.GetValue(i);
            }
        }

        
    }
}
