﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class RibbonPageGroupElement : PanelElement
    {
      //  private ControlElementCollection _items;
        /// <summary>
        /// Default Ctor 
        /// </summary>
        public RibbonPageGroupElement()
        {
            ShowHeader = true;
        }


        /// <summary>
        /// Gets or sets the Items.
        /// </summary>
        /// <value>
        /// The Items.
        /// </value>
        public ControlElementCollection Items
        {
            get { return Controls as ControlElementCollection; }
        }

        /// <summary>
        /// Creates the control element collection.
        /// </summary>
        /// <returns></returns>
        protected override ControlElementCollection CreateControlElementCollection()
        {
            return new ControlElementCollection(this, "Items");
        }

    }
}
