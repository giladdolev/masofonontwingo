﻿namespace System.Web.VisualTree.Elements
{
    public enum ToolBarItemAlignment
    {
        /// <summary>Specifies that the <see cref="T:System.Windows.Forms.ToolStripItem" /> is to be anchored toward the left or top end of the <see cref="T:ToolBarElement" />, depending on the <see cref="T:ToolBarElement" /> orientation. If the value of <see cref="T:System.Windows.Forms.RightToLeft" /> is Yes, items marked as <see cref="F:System.Windows.Forms.ToolStripItemAlignment.Left" /> are aligned to the right side of the <see cref="T:ToolBarElement" />.</summary>
        Left,
        /// <summary>Specifies that the <see cref="T:System.Windows.Forms.ToolStripItem" /> is to be anchored toward the right or bottom end of the <see cref="T:ToolBarElement" />, depending on the <see cref="T:ToolBarElement" /> orientation. If the value of <see cref="T:System.Windows.Forms.RightToLeft" /> is Yes, items marked as <see cref="F:System.Windows.Forms.ToolStripItemAlignment.Right" /> are aligned to the left side of the <see cref="T:ToolBarElement" />.</summary>
        Right
    }
}
