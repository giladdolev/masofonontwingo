namespace System.Web.VisualTree.Elements
{
	public class ListViewSubitemCollection : VisualElementCollection<ListViewSubitem>
	{

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewSubitemCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ListViewSubitemCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {
            
        }


        public ListViewSubitem Add(string text)
		{
            ListViewSubitem subitem = CreateNewSubitem(text);
            Add(subitem);

            return subitem;
		}



        public ListViewSubitem Insert(int index, string text)
        {
            ListViewSubitem subitem = CreateNewSubitem(text);
            Insert(index, subitem);

            return subitem;
        }



        private static ListViewSubitem CreateNewSubitem(string text)
        {
            return new ListViewSubitem() { Text = text };
        }



		public void AddRange(string[] items)
		{
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            foreach(string item in items)
            {
                Add(item);
            }
		}
	}
}
