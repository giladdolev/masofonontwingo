﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for accordion tab element
    /// </summary>
    public class AccordionElement : TabElement
    {
        public PageViewItemSizeMode ItemSizeMode
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the view mode.
        /// </summary>
        /// <value>
        /// The view mode.
        /// </value>
        public PageViewMode ViewMode
        {
            get;
            set;
        }
    }
}
