﻿namespace System.Web.VisualTree.Elements
{
    /// MousePointer Changes in .NET: http://msdn.microsoft.com/es-mx/library/e6ct1438(v=vs.71).aspx
    /// <include file="doc\Cursors.uex" path="docs/doc[@for="Cursors"]/*">
    /// <devdoc>
    ///     Standard cursors
    /// </devdoc>
    public sealed class CursorsElement {

        public static CursorElement AppStarting
	    {
		    get { return new CursorElement("AppStarting");}

	    }


        public static CursorElement Arrow
        {
            get { return new CursorElement("Arrow"); }

        }

        public static CursorElement Cross
        {
            get { return new CursorElement("Cross"); }

        }


        public static CursorElement Default
        {
            get { return new CursorElement("default"); }

        }

        public static CursorElement IBeam
        {
            get { return new CursorElement("IBeam"); }

        }

        public static CursorElement No
        {
            get { return new CursorElement("No"); }

        }

        public static CursorElement SizeAll
        {
            get { return new CursorElement("SizeAll"); }

        }

        public static CursorElement SizeNESW
        {
            get { return new CursorElement("SizeNESW"); }

        }

        public static CursorElement SizeNS
        {
            get { return new CursorElement("SizeNS"); }

        }

        public static CursorElement SizeNWSE
        {
            get { return new CursorElement("SizeNWSE"); }

        }

        public static CursorElement SizeWE
        {
            get { return new CursorElement("SizeWE"); }

        }

        public static CursorElement UpArrow
        {
            get { return new CursorElement("UpArrow"); }

        }


        public static CursorElement WaitCursor
        {
            get { return new CursorElement("wait"); }

        }

        public static CursorElement Help
        {
            get { return new CursorElement("help"); }

        }

        public static CursorElement HSplit
        {
            get { return new CursorElement("HSplit"); }

        }

        public static CursorElement VSplit
        {
            get { return new CursorElement("VSplit"); }

        }

        public static CursorElement NoMove2D
        {
            get { return new CursorElement("NoMove2D"); }

        }

        public static CursorElement NoMoveHoriz
        {
            get { return new CursorElement("NoMoveHoriz"); }

        }

        public static CursorElement NoMoveVert
        {
            get { return new CursorElement("NoMoveVert"); }

        }

        public static CursorElement PanEast
        {
            get { return new CursorElement("PanEast"); }

        }

        public static CursorElement PanNE
        {
            get { return new CursorElement("PanNE"); }

        }

        public static CursorElement PanNorth
        {
            get { return new CursorElement("PanNorth"); }

        }

        public static CursorElement PanNW
        {
            get { return new CursorElement("PanNW"); }

        }

        public static CursorElement PanSE
        {
            get { return new CursorElement("PanSE"); }

        }

        public static CursorElement PanSouth
        {
            get { return new CursorElement("PanSouth"); }

        }

        public static CursorElement PanSW
        {
            get { return new CursorElement("PanSW"); }

        }

        public static CursorElement PanWest
        {
            get { return new CursorElement("PanWest"); }

        }

        public static CursorElement Hand
        {
            get { return new CursorElement("Hand"); }

        }
    }
}