namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TableLayoutStyleCollection{ColumnStyle}" />
    public class TableLayoutColumnStyleCollection : TableLayoutStyleCollection<ColumnStyle>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutColumnStyleCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TableLayoutColumnStyleCollection(TablePanel objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }
	}
}
