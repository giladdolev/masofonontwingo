namespace System.Web.VisualTree.Elements
{
    public class OpenFileDialog : FileDialog
	{
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="OpenFileDialog"/> is multiselect.
        /// </summary>
        /// <value>
        ///   <c>true</c> if multiselect; otherwise, <c>false</c>.
        /// </value>
		public bool Multiselect
		{
			get;
			set;
		}
	}
}
