

using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public static class MessageBox
    {

        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text, string caption, MessageBoxButtons buttons, MessageBoxDirection direction = MessageBoxDirection.Ltr)
        {
            return Show(text, caption, buttons, MessageBoxIcon.None, direction);
        }

        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDirection direction = MessageBoxDirection.Ltr)
        {
            return ShowMessageBox(text, caption, buttons, icon, direction);
        }

        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text)
        {
            return Show(text, "", MessageBoxIcon.Information);
        }


        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text, string caption)
        {
            return Show(text, caption, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="icon">The icon.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text, string caption, MessageBoxIcon icon)
        {
            return Show(text, caption, MessageBoxButtons.OK, icon);
        }



        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public static Task<DialogResult> Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxDirection direction = MessageBoxDirection.Ltr)
        {
            return ShowMessageBox(text, caption, buttons, icon,direction, defaultButton);            
        }


        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <returns></returns>
        [RendererMethodDescription]
        private static Task<DialogResult> ShowMessageBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDirection direction, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            // Create task source
            TaskCompletionSource<DialogResult> taskSource = new TaskCompletionSource<DialogResult>();

            // The method callback
            Action<object> methodCallback = (result) =>
            {
                // The dialog result
                DialogResult dialogResult = DialogResult.None;

                // If result is dialog result
                if (result is DialogResult)
                {
                    // Get the dialog result
                    dialogResult = (DialogResult)result;
                }

                // Set the callback result
                RenderingContext.SetSynchronizationCallbackResult(taskSource, dialogResult);
            };

            ApplicationElement.InvokeMethod("ShowMessageBox", typeof(MessageBox), new MessageBoxData(text, caption, buttons, icon, defaultButton, direction), methodCallback);

            // Return task
            return taskSource.Task;
        }

        
    }


    [EditorBrowsable(EditorBrowsableState.Never)]
    public class MessageBoxData
    {
        /// <summary>
        /// The text
        /// </summary>
        private readonly string mstrText;


        /// <summary>
        /// The caption
        /// </summary>
        private readonly string mstrCaption;

        /// <summary>
        /// The callback
        /// </summary>
        private EventHandler _callback;


        /// <summary>
        /// The buttons
        /// </summary>
        private readonly MessageBoxButtons _buttons;

        /// <summary>
        /// The icon
        /// </summary>
        private readonly MessageBoxIcon _icon;

        /// <summary>
        /// The default button
        /// </summary>
        private readonly MessageBoxDefaultButton _defaultButton;

        /// <summary>
        /// The direction
        /// </summary>
        private readonly MessageBoxDirection _direction;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="callback">The callback.</param>
        public MessageBoxData(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon,MessageBoxDirection direction, EventHandler callback)
        {
            mstrText = text;
            mstrCaption = caption;
            _callback = callback;
            _buttons = buttons;
            _icon = icon;
            _direction = direction;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="callback">The callback.</param>
        public MessageBoxData(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton,
        MessageBoxDirection direction, EventHandler callback)
        {
            mstrText = text;
            mstrCaption = caption;
            _callback = callback;
            _buttons = buttons;
            _icon = icon;
            _defaultButton = defaultButton;
            _direction = direction;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="direction">The direction.</param>
        public MessageBoxData(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDirection direction)
            : this(text, caption, buttons, icon, direction, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="direction">The direction.</param>
        public MessageBoxData(string text, string caption, MessageBoxButtons buttons, MessageBoxDirection direction)
            : this(text, caption, buttons, MessageBoxIcon.None, direction, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="direction">The direction.</param>
        public MessageBoxData(string text, string caption, MessageBoxDirection direction)
            : this(text, caption, MessageBoxButtons.OK, MessageBoxIcon.None,direction, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <param name="direction">The direction.</param>
        public MessageBoxData(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxDirection direction)
            : this(text, caption, buttons, icon, defaultButton, direction, null)
        {

        }


        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public MessageBoxIcon Icon
        {
            get { return _icon; }
        }

        /// <summary>
        /// Gets the buttons.
        /// </summary>
        /// <value>
        /// The buttons.
        /// </value>
        public MessageBoxButtons Buttons
        {
            get { return _buttons; }
        }


        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption
        {
            get { return mstrCaption; }
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get { return mstrText; }
        }

        /// <summary>
        /// Gets the callback.
        /// </summary>
        /// <value>
        /// The callback.
        /// </value>
        public EventHandler Callback
        {
            get
            {
                return _callback;
            }
        }

        /// <summary>
        /// Gets the default button.
        /// </summary>
        /// <value>
        /// The default button.
        /// </value>
        public MessageBoxDefaultButton DefaultButton
        {
            get
            {
                return _defaultButton;
            }
        }

        /// <summary>
        /// Gets the direction.
        /// </summary>
        /// <value>
        /// The direction.
        /// </value>
        public MessageBoxDirection Direction
        {
            get
            {
                return _direction;
            }
        }
    }
}
