﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class InputBoxData
    {
        private readonly string _text;
        private readonly string _caption;



        /// <summary>
        /// Initializes a new instance of the <see cref="InputBoxData"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        public InputBoxData(string text, string caption)
        {
            _text = text;
            _caption = caption;
        }



        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption
        {
            get { return _caption; }
        }



        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get { return _text; }
        }
    }
}
