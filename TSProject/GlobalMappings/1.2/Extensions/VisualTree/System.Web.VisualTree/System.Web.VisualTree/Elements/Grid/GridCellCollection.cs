﻿namespace System.Web.VisualTree.Elements
{
    public class GridCellCollection : VisualElementCollection<GridCellElement>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCellCollection" /> class.
        /// </summary>
        public GridCellCollection()
        {


        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GridCellCollection" /> class.
        /// </summary>
        /// <param name="gridRow">The grid row.</param>
        /// <param name="propertyName">The property name.</param>
        public GridCellCollection(VisualElement gridRow, string propertyName)
            : base(gridRow, propertyName)
        {

        }



        protected override void InsertItem(int index, GridCellElement item)
        {
            base.InsertItem(index, item);
            GridRow row = item.ParentElement as GridRow;
            if (row != null)
            {
                GridElement grid = item.ParentElement.ParentElement as GridElement;

                if (grid != null && grid.Columns.Count > 0)
                {
                    item.OwningColumn = grid.Columns[index];
                }
            }
        }


        public override GridCellElement this[string name]
        {
            get
            {
                // Loop all visual elements
                foreach (GridCellElement visualElement in this)
                {
                    // If there is a valid element and it's name is a match
                    if (visualElement != null && visualElement.OwningColumn != null && string.Equals(visualElement.OwningColumn.ID, name, StringComparison.Ordinal))
                    {
                        return visualElement;
                    }
                }
                return null;
            }

        }


    }
}
