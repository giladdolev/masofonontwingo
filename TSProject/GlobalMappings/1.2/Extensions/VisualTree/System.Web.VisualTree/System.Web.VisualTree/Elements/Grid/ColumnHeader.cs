using System.Reflection;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.VisualElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingDataColumn" />
    [DesignerComponent]
    public class ColumnHeader : VisualElement, IBindingDataColumn
	{
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
		public string Text
		{
			get;
			set;
		}




        /// <summary>
        /// Gets or sets the text align.
        /// </summary>
        /// <value>
        /// The text align.
        /// </value>
		public HorizontalAlignment TextAlign
		{
			get;
			set;
		}




        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
		public int Width
		{
			get;
			set;
		}




        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string IBindingDataColumn.Name
        {
            get
            {
                return this.ClientID;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type IBindingDataColumn.Type
        {
            get
            {
                return typeof(string);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsValid
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        object IBindingDataColumn.GetValue(object item, int index)
        {
            ListViewItem listItem = item as ListViewItem;

            if(listItem != null)
            {
                if (index == 0)
                {
                    return listItem.Text;
                }
                if (listItem.Subitems.Count > 0 && index < listItem.Subitems.Count)
                {
                    ListViewSubitem subItem = listItem.Subitems[index - 1];

                    if (subItem != null)
                    {
                        return subItem.Text;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsIndex
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Binds the specified property information.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        void IBindingDataColumn.Bind(PropertyInfo propertyInfo)
        {
            
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        string IBindingDataColumn.DataTypeName
        {
            get
            {
                return typeof(string).Name;
            }
        }

        private ColumnSortMode _sortType = ColumnSortMode.StringAscending;
        /// <summary>
        /// Gets the name of the Sort type.
        /// </summary>
        /// <value>
        /// The name of the Sort type.
        /// </value>
        public ColumnSortMode SortType
        {
            get
            {
                return _sortType;
            }
            set
            {
                this._sortType = value;
            }
        }
    }
}
