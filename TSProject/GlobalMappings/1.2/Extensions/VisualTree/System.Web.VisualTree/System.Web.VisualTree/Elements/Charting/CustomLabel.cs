using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
	[DesignerComponent]
    public class CustomLabel : ChartNamedElement
	{
		/// <summary>
		///Gets or sets a property that specifies whether custom tick marks and grid lines will be drawn in the center of the label.
		/// </summary>
		public GridTickTypes GridTicks
		{
			get;
			set;
		}
	}
}
