﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum SeriesLabelPosition
    {
        Top,
        Center
    }

    public class SeriesLabelStyle : ChartItemElement
    {
        private bool _enabled;
        private SeriesLabelPosition _position;

        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesLabelStyle"/> class.
        /// </summary>
        public SeriesLabelStyle()            
        {            
            _enabled = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SeriesLabelStyle"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    OnPropertyChanged("Enabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public SeriesLabelPosition Position
        {
            get { return _position;  }
            set
            {
                if(_position != value)
                {
                    _position = value;
                    OnPropertyChanged("Position");
                }
            }
        }
    }
}
