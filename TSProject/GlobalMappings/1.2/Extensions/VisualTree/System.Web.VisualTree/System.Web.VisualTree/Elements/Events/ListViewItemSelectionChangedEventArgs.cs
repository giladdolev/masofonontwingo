﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class ListViewItemSelectionChangedEventArgs : EventArgs
    {
        /// <summary>
		/// Initializes a new instance of the class.
		/// </summary>
        /// <param name="selectedIndexes">The delta.</param>
        public ListViewItemSelectionChangedEventArgs(Int32[] selectedIndexes)
        {
            this.SelectedIndexes = selectedIndexes;
        }

        /// <summary>
        /// Initializes a new instance of the ListViewItemSelectionChangedEventArgs class.
        /// </summary>
        /// <param name="item">The ListViewItem whose selection state has changed.</param>
        /// <param name="itemIndex">The index of the ListViewItem whose selection state has changed.</param>
        /// <param name="isSelected">true to indicate the item's state has changed to selected; false to indicate the item's state has changed to deselected.</param>
        public ListViewItemSelectionChangedEventArgs(ListViewItem item, int itemIndex, bool isSelected)
        {
            this.Item = item;
            this.ItemIndex = itemIndex;
            this.IsSelected = IsSelected;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ListViewItemSelectionChangedEventArgs()
        {
           
        }

        /// <summary>
		/// Gets or sets the button.
		/// </summary>
		/// <value>
		/// The button.
		/// </value>
        public Int32[] SelectedIndexes
		{
			get;
			set;
		}

        /// <summary>
        /// Gets,Set the item whose selection state has changed.
        /// </summary>
        public ListViewItem Item { get; set; }

        /// <summary>
        /// Gets,Set a value indicating whether the item's state has changed to selected.
        /// </summary>
        public bool IsSelected { get; set; }

        /// <summary>
        /// Gets,Set the index of the item whose selection state has changed.
        /// </summary>
        public int ItemIndex { get; set; }
    }
}
