using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{

    public class ApplicationElement : VisualElement, IVisualElementMethodInvokerContainer, IWindowTargetContainer, IClientHistoryManager, ITimerHandler
    {

        /// <summary>
        /// The setting alternative main window flag
        /// </summary>
        private bool _settingAlternativeMainWindow = false;

        /// <summary>
        /// The main window was closed flag
        /// </summary>
        private bool _mainWindowWasClosed = false;

        /// <summary>
        /// The company name
        /// </summary>
        private static string companyName;

        /// <summary>
        /// The internal synchronize object
        /// </summary>
        private static object internalSyncObject = new object();

        /// <summary>
        /// The product version
        /// </summary>
        private static string productVersion;

        /// <summary>
        /// The product name
        /// </summary>
        private static string productName;


        /// <summary>
        /// The common application data path
        /// </summary>
        private static string commonAppDataPath;


        /// <summary>
        /// The global revision
        /// </summary>
        [ThreadStatic()]
        private static int _globalRevision = 0;

        /// <summary>
        /// The active control .
        /// </summary>
        public static string ActiveControlId = string.Empty;

        /// <summary>
        /// The rendering environment
        /// </summary>
        private RenderingEnvironment _renderingEnvironment = null;

        /// <summary>
        /// Queued method invokers
        /// </summary>
        private Queue<VisualElementMethodInvoker> _queuedMethodInvokers = new Queue<VisualElementMethodInvoker>();


        /// <summary>
        /// The unique invokers
        /// </summary>
        private Dictionary<Tuple<string, object>, VisualElementMethodInvoker> _uniqueInvokers = null;


        /// <summary>
        /// The root element to client id hash
        /// </summary>
        private Dictionary<IRootElement, string> _clientIdByRootElement = new Dictionary<IRootElement, string>();

        /// <summary>
        /// The client id to root element
        /// </summary>
        private Dictionary<string, IRootElement> _rootElementByClientId = new Dictionary<string, IRootElement>();

        /// <summary>
        /// The main window
        /// </summary>
        private WindowElement _mainWindow = null;

        /// <summary>
        /// The application storage
        /// </summary>
        private readonly Dictionary<string, object> _applicationStorage = new Dictionary<string, object>();

        /// <summary>
        /// The mouse position
        /// </summary>
        private Point _mousePosition = Point.Empty;

        /// <summary>
        /// The application identifier
        /// </summary>
        private readonly string _appID;

        /// <summary>
        /// The last usage
        /// </summary>
        private DateTime _lastUsage;

        /// <summary>
        /// The keep alive timer
        /// </summary>
        private ApplicationKeepAliveTimer _keepAliveTimer = null;

        /// <summary>
        /// The window stack
        /// </summary>
        private List<IWindowElement> _windowStack = new List<IWindowElement>();

        /// <summary>
        /// The keep alive timer identifier
        /// </summary>
        private const string KEEP_ALIVE_TIMER_ID = "_keepAlive";

        /// <summary>
        /// Provides support for keeping application alive
        /// </summary>
        private class ApplicationKeepAliveTimer : TimerElement
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="ApplicationKeepAliveTimer"/> class.
            /// </summary>
            /// <param name="keepAliveInterval">The keep alive interval.</param>
            public ApplicationKeepAliveTimer(int keepAliveInterval)
            {
                this.ID = KEEP_ALIVE_TIMER_ID;
                this.Interval = keepAliveInterval;
                this.Enabled = true;
            }

            /// <summary>
            /// Gets or sets the client identifier.
            /// </summary>
            /// <value>
            /// The client identifier.
            /// </value>
            public override string ClientID
            {
                get
                {
                    return this.ID;
                }
            }

            /// <summary>
            /// Raises the <see cref="E:Tick" /> event.
            /// </summary>
            /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
            protected override void OnTick(EventArgs args)
            {
                base.OnTick(args);

                // Get the application element
                ApplicationElement applicationElement = this.ParentElement as ApplicationElement;

                // If there is a valid application element
                if (applicationElement != null)
                {
                    //OnKeepAlive is calling during RestoreApplicationElement, so invoking it here will raise it a second time
                    //This whole timer mechanism will be revised, so temporarily commenting this out
                    // Keep application alive
                    //applicationElement.OnKeepAlive();
                }
            }

        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ApplicationElement" /> class from being created.
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        private ApplicationElement(string appId)
        {
            _appID = appId;
            _lastUsage = DateTime.Now;
        }

        /// <summary>
        /// Keeps the alive.
        /// </summary>
        internal void OnKeepAlive()
        {
            _lastUsage = DateTime.Now;

            if (KeepAlive != null)
            {
                KeepAlive(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Occurs when keep alive occurs.
        /// </summary>
        public event EventHandler KeepAlive;

        /// <summary>
        /// Determines whether the specified life span is alive.
        /// </summary>
        /// <returns></returns>
        internal bool IsAlive()
        {
            return (DateTime.Now - _lastUsage) <= _applicationTimeout;
        }

        /// <summary>
        /// Gets the last usage.
        /// </summary>
        /// <value>
        /// The last usage.
        /// </value>
        public DateTime LastUsage
        {
            get { return _lastUsage; }
        }

        /// <summary>
        /// Gets a value indicating whether [setting alternative main window].
        /// </summary>
        /// <value>
        /// <c>true</c> if [setting alternative main window]; otherwise, <c>false</c>.
        /// </value>
        public bool SettingAlternativeMainWindow
        {
            get
            {
                return _settingAlternativeMainWindow;
            }
        }



        /// <summary>
        /// Gets a value indicating whether [should render blank page].
        /// </summary>
        /// <value>
        /// <c>true</c> if [should render blank page]; otherwise, <c>false</c>.
        /// </value>
        public bool ShouldRenderBlankPage
        {
            get
            {
                return (_mainWindow == null);
            }
        }

        /// <summary>
        /// Gets the or create instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func">The function.</param>
        /// <param name="strName">Name of the string.</param>
        /// <returns></returns>
        public static T GetOrCreateInstance<T>(Func<T> func, string strName)
           where T : class
        {
            T objInstance = null;

            // If T is window element type - create new window and store it in ApllicationElement
            if (typeof(T).IsSubclassOf(typeof(WindowElement)))
            {
                // Try to get window
                WindowElement objWindow = ApplicationElement.Current[strName] as WindowElement;

                // If window did not created yet
                if (objWindow == null)
                {
                    using (WindowElement.CreateOpenerContext((_objWindow) => { _objWindow.IsCreated = true; ApplicationElement.Current[strName] = objWindow = _objWindow; }))
                    {
                        // Create window
                        objWindow = func() as WindowElement;

                        // Register to window close event
                        objWindow.WindowClosed += (sender, e) => OnWindowClosed(sender, e, strName);
                    }
                }

                objInstance = objWindow as T;
            }

            else
            {
                // Create instance
                objInstance = func();

                // If there is a valid value
                if (objInstance != null)
                {
                    // If Current is valid
                    if (ApplicationElement.Current != null)
                    {
                        // Store new instance in ApplicationElement
                        ApplicationElement.Current[strName] = objInstance;
                    }
                }
            }

            return objInstance;
        }

        /// <summary>
        /// Called when [window closed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="WindowClosedEventArgs"/> instance containing the event data.</param>
        /// <param name="strName">Name of the string.</param>
        private static void OnWindowClosed(object sender, WindowClosedEventArgs e, string strName)
        {
            ApplicationElement.Current[strName] = null;
        }

        /// <summary>
        /// Creates or update a unique method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        /// <returns></returns>
        private VisualElementMethodInvoker CreateOrUpdateUniqueMethodInvoker(string methodName, object methodOwner, object methodData, Action<object> methodCallback)
        {
            return this.CreateOrUpdateUniqueMethodInvoker(methodName, methodOwner, methodCallback, (existingData) => methodData);
        }


        /// <summary>
        /// Creates or update a unique method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodCallback">The method callback.</param>
        /// <param name="methodData">The method data.</param>
        /// <returns></returns>
        private VisualElementMethodInvoker CreateOrUpdateUniqueMethodInvoker(string methodName, object methodOwner,
            Action<object> methodCallback, Func<object, object> methodData)
        {
            // Create methodInvoker key
            Tuple<string, object> methodInvokerKey = Tuple.Create(methodName, methodOwner);

            return CreateOrUpdateUniqueMethodInvoker(methodName, methodOwner, methodCallback, methodData, methodInvokerKey);
        }

        /// <summary>
        /// Creates the or update unique method invoker.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodCallback">The method callback.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodInvokerKey">The method invoker key.</param>
        /// <returns></returns>
        private VisualElementMethodInvoker CreateOrUpdateUniqueMethodInvoker(string methodName, object methodOwner,
            Action<object> methodCallback, Func<object, object> methodData, Tuple<string, object> methodInvokerKey)
        {
            // Holds method that we are working on
            VisualElementMethodInvoker methodInvoker = null;

            // Holds methodInvoker that we need to en-queue
            VisualElementMethodInvoker methodInvokerToEnqueue = null;

            // If there are valid parameters
            if (_uniqueInvokers != null)
            {
                // Will hold the existing method
                VisualElementMethodInvoker existingMethodInvoker = null;

                // Set unique method
                if (!_uniqueInvokers.TryGetValue(methodInvokerKey, out existingMethodInvoker))
                {
                    // Create new method
                    _uniqueInvokers[methodInvokerKey] = methodInvokerToEnqueue = new VisualElementMethodInvoker(methodName, methodOwner, GetMethodInvokerData(methodData), methodCallback);

                    // Set method invoker
                    methodInvoker = methodInvokerToEnqueue;
                }
                else
                {
                    // Set method invoker
                    methodInvoker = existingMethodInvoker;

                    // Update existing method
                    existingMethodInvoker.Data = GetMethodInvokerData(methodData, existingMethodInvoker.Data);
                }
            }
            else
            {
                // Create the unique method dictionary
                _uniqueInvokers = new Dictionary<Tuple<string, object>, VisualElementMethodInvoker>();

                // Create new method
                _uniqueInvokers[methodInvokerKey] = methodInvokerToEnqueue = new VisualElementMethodInvoker(methodName, methodOwner, GetMethodInvokerData(methodData), methodCallback);

                // Set method invoker
                methodInvoker = methodInvokerToEnqueue;
            }

            // If there is an method to en-queue
            if (methodInvokerToEnqueue != null)
            {
                // En-queue method
                this.RegisterMethodInvoker(methodInvokerToEnqueue);
            }

            // Return the method invoker
            return methodInvoker;
        }

        /// <summary>
        /// Gets the method invoker data.
        /// </summary>
        /// <param name="methodData">The method data.</param>
        /// <param name="existingData">The existing data.</param>
        /// <returns></returns>
        private object GetMethodInvokerData(Func<object, object> methodData, object existingData = null)
        {
            // If there is a valid method data function
            if (methodData != null)
            {
                // Get method data
                return methodData(existingData);
            }

            return existingData;
        }


        /// <summary>
        /// Invokes the method but if a method with this name and owner was already queued for invocation it updates it's data.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public void InvokeMethodOnce(string methodName, VisualElement methodOwner, object methodData = null, Action<object> methodCallback = null)
        {
            CreateOrUpdateUniqueMethodInvoker(methodName, methodOwner, methodData, methodCallback);
        }

        /// <summary>
        /// Invokes the global method once.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public void InvokeGlobalMethodOnce(string methodName, VisualElement methodOwner, object methodData = null, Action<object> methodCallback = null)
        {
            // Create methodInvoker key
            Tuple<string, object> methodInvokerKey = Tuple.Create(methodName, (object)null);

            CreateOrUpdateUniqueMethodInvoker(methodName, methodOwner, methodCallback, (existingData) => methodData, methodInvokerKey);
        }

        /// <summary>
        /// Invokes the method but if a method with this name and owner was already queued for invocation it updates it's data.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public static void InvokeMethodOnce(string methodName, Type methodOwner, object methodData = null, Action<object> methodCallback = null)
        {
            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication != null)
            {
                currentApplication.CreateOrUpdateUniqueMethodInvoker(methodName, methodOwner, methodData, methodCallback);
            }
        }

        /// <summary>
        /// Invokes the method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public void InvokeMethod(string methodName, VisualElement methodOwner, object methodData = null, Action<object> methodCallback = null)
        {
            this.RegisterMethodInvoker(new VisualElementMethodInvoker(methodName, methodOwner, methodData, methodCallback));
        }

        /// <summary>
        /// Invokes the method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodOwner">The method owner.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="methodCallback">The method callback.</param>
        public static void InvokeMethod(string methodName, Type methodOwner, object methodData = null, Action<object> methodCallback = null)
        {
            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication != null)
            {
                // En-queue method invoker
                currentApplication.RegisterMethodInvoker(new VisualElementMethodInvoker(methodName, methodOwner, methodData, methodCallback));
            }
        }


        /// <summary>
        /// Registers the method invoker.
        /// </summary>
        /// <param name="methodInvoker">The method invoker.</param>
        private void RegisterMethodInvoker(VisualElementMethodInvoker methodInvoker)
        {
            // If there is a valid method invoker
            if (methodInvoker != null)
            {
                // If there is a valid callback
                if (methodInvoker.Callback != null)
                {
                    // Set the parent element as this application element
                    methodInvoker.ParentElement = this;

                    // Register method invoker as a root element
                    this.RegisterRootElement(methodInvoker);
                }

                // En-queue method invoker
                this._queuedMethodInvokers.Enqueue(methodInvoker);
            }
        }

        /// <summary>
        /// Gets the mouse position.
        /// </summary>
        /// <value>
        /// The mouse position.
        /// </value>
        public static Point MousePosition
        {
            get
            {
                // Get current application element
                ApplicationElement currentApplication = Current;

                // If there is a valid application
                if (currentApplication != null)
                {
                    // Return the mouse position
                    return currentApplication._mousePosition;
                }

                // Return default point
                return Point.Empty;
            }
        }




        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> with the specified name.
        /// </summary>
        /// <value>
        /// The <see cref="System.Object"/>.
        /// </value>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                object value = null;

                if (!string.IsNullOrEmpty(name))
                {
                    _applicationStorage.TryGetValue(name, out value);
                }

                return value;
            }
            set
            {
                if (!string.IsNullOrEmpty(name))
                {
                    //this.UnregisterWindow(oldValue as WindowElement);

                    _applicationStorage[name] = value;

                    // Get window element
                    WindowElement windowElement = value as WindowElement;

                    // If there is a valid window element and it does not have a target
                    if (windowElement != null && !windowElement.HasWindowTarget)
                    {
                        // Register root
                        RegisterRootElement(windowElement);
                    }
                }
            }
        }

        /// <summary>
        /// Un-registers the root element.
        /// </summary>
        /// <param name="rootElement">The root element.</param>
        private bool UnregisterRootElement(IRootElement rootElement)
        {
            // If there is a valid root element
            if (rootElement != null)
            {
                // The window client id
                string clientId = null;

                // If window is contained in hash 
                if (_clientIdByRootElement.TryGetValue(rootElement, out clientId))
                {
                    // if there is a valid client id
                    if (clientId != null)
                    {
                        //Remove window by client mapping
                        _rootElementByClientId.Remove(clientId);
                    }

                    // Remove client id by window
                    _clientIdByRootElement.Remove(rootElement);

                    // If the root is also the current main window
                    if (rootElement == _mainWindow)
                    {
                        // Indicate main window closed
                        _mainWindowWasClosed = true;

                        // Clear the main window
                        _mainWindow = null;
                    }

                    // Indicate root element was unregistered
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Registers a root element.
        /// </summary>
        /// <param name="rootElement">The root element.</param>
        private void RegisterRootElement(IRootElement rootElement)
        {
            // If there is a valid window element
            if (rootElement != null)
            {
                // If window is not contained in hash 
                if (!_clientIdByRootElement.ContainsKey(rootElement))
                {
                    // Get the client id
                    string clientId = CreateClientId(rootElement);

                    // If there is a valid client id
                    if (!string.IsNullOrEmpty(clientId))
                    {
                        // Map client id to window
                        _rootElementByClientId[clientId] = rootElement;

                        // Map window to client id
                        _clientIdByRootElement[rootElement] = clientId;

                        // Set this as parent element
                        rootElement.ParentElement = this;

                        // Get the window element
                        IWindowElement windowElement = rootElement as IWindowElement;

                        // If there is a valid window element
                        if (windowElement != null)
                        {
                            // Add to window element stack
                            _windowStack.Add(windowElement);

                            if (!windowElement.IsCreated)
                            {
                                // Ensure control is created
                                windowElement.CreateControl();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unregisters the window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        private void UnregisterWindow(IWindowElement windowElement)
        {
            // If there is a valid window element
            if (windowElement != null)
            {
                // Un-register root element
                if (this.UnregisterRootElement(windowElement))
                {
                    // Remove from window element stack
                    _windowStack.Remove(windowElement);
                }
            }
        }

        /// <summary>
        /// Gets the owner window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        /// <returns></returns>
        internal static IWindowElement GetOwnerWindow(IWindowElement windowElement)
        {
            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication != null)
            {
                IWindowElement ownerElement = null;

                // Loop all windows
                foreach (IWindowElement currentWindow in currentApplication._windowStack)
                {
                    // If we found current window
                    if (windowElement == currentWindow)
                    {
                        // Return the owner window
                        return ownerElement;
                    }
                    else
                    {
                        ownerElement = currentWindow;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the window model by its name.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public static TWindowElement GetWindowModelByName<TWindowElement>(string viewName) where TWindowElement : class, IWindowElement
        {
            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication == null)
            {
                return default(TWindowElement);
            }

            // Get the window model
            IRootElement windowElement;
            if (currentApplication._rootElementByClientId.TryGetValue(viewName, out windowElement))
            {
                // Return root element as element
                return windowElement as TWindowElement;
            }

            return null;

        }



        /// <summary>
        /// Gets the global revision.
        /// </summary>
        /// <value>
        /// The global revision.
        /// </value>
        public static int GlobalRevision
        {
            get { return _globalRevision; }
        }

        /// <summary>
        /// Removes the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="closeDialog">if set to <c>true</c> close dialog.</param>
        internal void Remove(VisualElement visualElement, bool closeDialog)
        {
            // Get the window element
            IWindowElement windowElement = visualElement as IWindowElement;

            // If there is a valid window element
            if (windowElement != null)
            {
                // Register the window
                UnregisterWindow(windowElement);

                // If should close dialog
                if (closeDialog)
                {
                    // Get the actual window element
                    WindowElement actualWindowElement = windowElement as WindowElement;

                    // If there is a valid actual window element
                    if (actualWindowElement != null)
                    {
                        // Invoke the window methodInvoker
                        actualWindowElement.InvokeWindowAction(WindowActionType.Close);
                    }
                }
            }
            else
            {
                base.Remove(visualElement);
            }
        }

        /// <summary>
        /// Removes the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public override void Remove(VisualElement visualElement)
        {
            Remove(visualElement, false);
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="showDialog">if set to <c>true</c> show dialog.</param>
        /// <param name="isModal">if set to <c>true</c> is modal.</param>
        internal void Add(VisualElement visualElement, bool showDialog, bool isModal = true)
        {
            // Get window element
            WindowElement windowElement = visualElement as WindowElement;

            // If there is a valid window element
            if (windowElement != null)
            {
                // If we don't have a main window yet
                if (_mainWindow == null)
                {
                    // Set the main window
                    _mainWindow = windowElement;

                    // Apply the window title
                    _mainWindow.ApplyWindowTitle();

                    // If we are replacing the main window
                    if (_mainWindowWasClosed)
                    {
                        // Indicate process of closing main window is done
                        _mainWindowWasClosed = false;

                        // Indicate setting on alternative window
                        _settingAlternativeMainWindow = true;

                        // Open the new window as main window
                        windowElement.InvokeWindowAction(WindowActionType.OpenAsMain);
                    }
                }
                else if (showDialog)
                {
                    // Extend window with a modality marker
                    ModalityExtender.SetIsModal(windowElement, isModal);

                    // Invoke the window action
                    windowElement.InvokeWindowAction(isModal ? WindowActionType.OpenModal : WindowActionType.Open);
                }

                // Register the window
                RegisterRootElement(windowElement);
            }
            else
            {
                base.Add(visualElement, false);
            }

        }

        /// <summary>
        /// Provides support for marking a window as modal
        /// </summary>
        private class ModalityExtender : VisualElementExtender
        {

            /// <summary>
            /// Sets the is modal.
            /// </summary>
            /// <param name="windowElement">The window element.</param>
            /// <param name="value">if set to <c>true</c> value.</param>
            internal static void SetIsModal(WindowElement windowElement, bool value)
            {
                if(windowElement != null)
                {
                    if(value)
                    {
                        VisualElementExtender.GetOrCreateExtender<ModalityExtender>(windowElement, () => new ModalityExtender());
                    }
                    else
                    {
                        VisualElementExtender.RemoveExtender<ModalityExtender>(windowElement);
                    }
                }
            }

            /// <summary>
            /// Gets the is modal.
            /// </summary>
            /// <param name="windowElement">The window element.</param>
            /// <returns></returns>
            internal static bool GetIsModal(WindowElement windowElement)
            {
                return VisualElementExtender.GetExtender<ModalityExtender>(windowElement) != null;
            }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // This does not add a show dialog operation
            Add(visualElement, insert);
        }

        /// <summary>
        /// Gets the visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The visual element type.</typeparam>
        /// <param name="visualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetVisualElement<TVisualElement>(string visualElementID)
        {
            // If there is a valid visual element id
            if (!string.IsNullOrEmpty(visualElementID))
            {
                // If is the keep alive timer identifier
                if (string.Equals(visualElementID, KEEP_ALIVE_TIMER_ID, StringComparison.Ordinal))
                {
                    // Return the keep alive timer
                    return _keepAliveTimer as TVisualElement;
                }

                // The root element
                IRootElement rootElement = null;

                // If there is a valid root element
                if (_rootElementByClientId.TryGetValue(visualElementID, out rootElement))
                {
                    // Return root element as element
                    return rootElement as TVisualElement;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The string visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            return this.GetVisualElement<TVisualElement>(strVisualElementID);
        }

        /// <summary>
        /// Creates the client identifier.
        /// </summary>
        /// <param name="rootElement">The root element.</param>
        /// <returns></returns>
        private string CreateClientId(IRootElement rootElement)
        {
            // If there is a valid root element
            if (rootElement != null)
            {
                // Get the root element id
                string id = rootElement.ID;

                // If there is no valid id
                if (string.IsNullOrWhiteSpace(id))
                {
                    // Set default id
                    id = "win";
                }

                // Set initial unique id
                string uniqueId = id;

                // The id index
                int idIndex = 1;

                // While id is not unique
                while (_rootElementByClientId.ContainsKey(uniqueId))
                {
                    // Increment id index 
                    idIndex++;

                    // Create new unique index
                    uniqueId = string.Concat(id, idIndex);
                }

                // Return unique index
                return uniqueId;
            }

            return null;
        }

        /// <summary>
        /// Gets a value indicating whether has methodInvokers.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has methodInvokers; otherwise, <c>false</c>.
        /// </value>
        internal bool HasQueuedMethodInvokers
        {
            get
            {
                return _queuedMethodInvokers.Count > 0;
            }
        }

        /// <summary>
        /// De-queues a method invoker.
        /// </summary>
        /// <returns></returns>
        IVisualElementMethodInvoker IVisualElementMethodInvokerContainer.Dequeue()
        {
            // If there are method invokers to de-queue
            if (_queuedMethodInvokers.Count > 0)
            {
                // De-queue invoker
                return _queuedMethodInvokers.Dequeue();
            }

            return null;
        }


        /// <summary>
        /// Clears the method invoker state.
        /// </summary>
        void IVisualElementMethodInvokerContainer.Clear()
        {
            _uniqueInvokers = null;
        }



        /// <summary>
        /// Gets the application.
        /// </summary>
        /// <value>
        /// The application.
        /// </value>
        public override ApplicationElement Application
        {
            get { return this; }
        }

        /// <summary>
        /// Gets a value indicating whether this element is an application.
        /// </summary>
        /// <value>
        /// <c>true</c> if this element is an application; otherwise, <c>false</c>.
        /// </value>
        internal override bool IsApplication
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the main window.
        /// </summary>
        /// <value>
        /// The main window.
        /// </value>
        public static WindowElement MainWindow
        {
            get
            {
                // Get the current application element
                ApplicationElement applicationElement = Current;

                // If there is a valid current application element
                if (applicationElement != null)
                {
                    // Return main window
                    return applicationElement._mainWindow;
                }

                return null;
            }
        }


        /// <summary>
        /// Gets the windows.
        /// </summary>
        /// <value>
        /// The windows.
        /// </value>
        public static IEnumerable<WindowElement> Windows
        {
            get
            {
                // Get the current application element
                ApplicationElement applicationElement = Current;

                // If there is a valid current application element
                if (applicationElement != null)
                {
                    // Return main window
                    return applicationElement.GetOpenWindows();
                }

                // Return an empty window array
                return WindowElement.EmptyArray;
            }
        }

        /// <summary>
        /// Gets the open windows
        /// </summary>
        internal IEnumerable<WindowElement> GetOpenWindows()
        {
            // Loop all window items
            foreach (var item in _rootElementByClientId)
            {
                // Get the window element
                WindowElement windowElement = item.Value as WindowElement;

                // If there is a valid window element
                if (windowElement != null)
                {
                    // Return the window element
                    yield return windowElement;
                }
            }
        }

        /// <summary>
        /// Gets or sets the current rendering environment.
        /// </summary>
        /// <value>
        /// The current rendering environment.
        /// </value>
        public static RenderingEnvironment CurrentRenderingEnvironment
        {
            get
            {
                // Get the current application element
                ApplicationElement applicationElement = Current;

                // If there is a valid current application element
                if (applicationElement != null)
                {
                    return applicationElement.RenderingEnvironment;
                }

                // Return default
                return RenderingEnvironment.Default;
            }
            set
            {
                // Get the current application element
                ApplicationElement applicationElement = Current;

                // If there is a valid current application element
                if (applicationElement != null)
                {
                    // Set the rendering environment
                    applicationElement.RenderingEnvironment = value;
                }
            }
        }

        /// <summary>
        /// Currents the rendering context.
        /// </summary>
        /// <value>
        /// The current rendering context.
        /// </value>
        public static RenderingContext CurrentRenderingContext
        {
            get
            {
                return new RenderingContext(ApplicationElement.CurrentRenderingEnvironment, false);
            }
        }

        /// <summary>
        /// Gets or sets the rendering environment.
        /// </summary>
        /// <value>
        /// The rendering environment.
        /// </value>
        public RenderingEnvironment RenderingEnvironment
        {
            get
            {
                if (_renderingEnvironment != null)
                {
                    return _renderingEnvironment;
                }

                return RenderingEnvironment.Default;
            }
            set
            {
                _renderingEnvironment = value;
            }
        }

        /// <summary>
        /// Restores the view.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        internal static void RestoreView(WindowElement windowElement)
        {
            ApplicationElement applicationElement = ApplicationElement.Current;
            if(applicationElement != null)
            {
                // Loop all windows
                foreach(IWindowElement windowInterface in applicationElement._windowStack)
                {
                    // Get the actual window element
                    WindowElement actualWindowElement = windowInterface as WindowElement;

                    // If there is a valid window and is not the restored view
                    if (actualWindowElement != null && actualWindowElement != windowElement)
                    {
                        // Invoke the window action
                        actualWindowElement.InvokeWindowAction(ModalityExtender.GetIsModal(actualWindowElement) ? WindowActionType.OpenModal : WindowActionType.Open);
                    }
                }
            }
        }

        /// <summary>
        /// Restore an existing application element.
        /// </summary>
        /// <param name="appId">The application identifier.</param>
        /// <param name="blnRecreateIfTimedOut">if set to <c>true</c> [BLN recreate if timed out].</param>
        /// <returns></returns>
        /// <exception cref="TimeoutException">The current application has timed out.</exception>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ApplicationElement RestoreApplicationElement(string appId, bool blnRecreateIfTimedOut = false)
        {
            ApplicationElement applicationElement = null;

            // If there is a valid application ID
            if (!string.IsNullOrEmpty(appId))
            {
                // Get the current context
                HttpContext currentContext = RenderingContext.CurrentHttpContext;

                // If there is a valid context
                if (currentContext != null)
                {
                    // Get the session state
                    HttpSessionState sessionState = currentContext.Session;

                    // If there is a valid session state
                    if (sessionState != null)
                    {
                        // Try to get existing application element
                        applicationElement = sessionState[appId] as ApplicationElement;

                        // If there is a valid application element
                        if (applicationElement != null  || blnRecreateIfTimedOut)
                        {
                            // If we need to create element
                            if (applicationElement == null)
                            {
                                // Recreate app id
                                applicationElement = CreateApplicationElement(appId);
                            }

                            // Set global revision
                            _globalRevision = applicationElement.Revision;

                            // Set the current application element
                            ApplicationElement.Current = applicationElement;

                            // Ensure element 
                            applicationElement.OnKeepAlive();

                            // Destroy dead apps that are not this app
                            DisposeDeadApps(applicationElement);
                        }
                        else
                        {

                            // Destroy dead apps if we are at it
                            DisposeDeadApps(null);

                            // Indicate that application has timed out
                            throw new TimeoutException("The current application has timed out.");
                        }
                    }
                }
            }

            return applicationElement;
        }


        /// <summary>
        /// The application timeout
        /// </summary>
        private static readonly TimeSpan _applicationTimeout = TimeSpan.FromMinutes(500);

        /// <summary>
        /// Destroys the dead apps.
        /// </summary>
        /// <param name="applicationElement">The application element.</param>
        private static void DisposeDeadApps(ApplicationElement applicationElement)
        {
            // Get the current context
            HttpContext currentContext = RenderingContext.CurrentHttpContext;

            // If there is a valid context
            if (currentContext != null)
            {
                // Get the session state
                HttpSessionState sessionState = currentContext.Session;

                // If there is a valid session state
                if (sessionState != null)
                {
                    // Get the live apps list
                    List<ApplicationElement> liveApps = sessionState[VT_LIVE_APPS_CACHE_KEY] as List<ApplicationElement>;

                    // If there are live apps
                    if (liveApps != null && liveApps.Count > 0)
                    {

                        // List of dead apps
                        List<ApplicationElement> deadApps = new List<ApplicationElement>();

                        // Loop all live apps
                        foreach (ApplicationElement liveApp in liveApps)
                        {
                            // If is not the current application element
                            if (liveApp != applicationElement)
                            {
                                // If is not alive
                                if (!liveApp.IsAlive())
                                {
                                    // Add to dead apps list
                                    deadApps.Add(liveApp);
                                }
                            }
                        }

                        // If there are dead apps
                        if (deadApps.Count > 0)
                        {
                            // Loop and destroy all dead apps
                            foreach (ApplicationElement deadApp in deadApps)
                            {
                                // Dispose the application
                                deadApp.Dispose();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Occurs when disposed.
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Dispose the application element
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            // Get the current context
            HttpContext objCurrentContext = RenderingContext.CurrentHttpContext;

            // If there is a valid context
            if (objCurrentContext != null)
            {
                // Get the session state
                HttpSessionState sessionState = objCurrentContext.Session;

                // If there is a valid session state
                if (sessionState != null)
                {
                    // Create new application
                    sessionState.Remove(this.ClientID);

                    // Get the live apps list
                    List<ApplicationElement> liveApps = sessionState[VT_LIVE_APPS_CACHE_KEY] as List<ApplicationElement>;

                    // If there is a live apps list
                    if (liveApps != null)
                    {
                        // Remove live app
                        liveApps.Remove(this);
                    }

                    // If there are disposed listeners
                    if (Disposed != null)
                    {
                        // Raise the dispose event
                        Disposed(this, EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// The visual tree live application cache key
        /// </summary>
        private const string VT_LIVE_APPS_CACHE_KEY = "vt_live_apps";


        /// <summary>
        /// Get an existing application element.
        /// </summary>        
        /// <returns></returns>
        public static ApplicationElement CreateApplicationElement()
        {
            return CreateApplicationElement(null);
        }

        /// <summary>
        /// Get an existing application element.
        /// </summary>        
        /// <returns></returns>
        private static ApplicationElement CreateApplicationElement(string forcedAppId)
        {
            ApplicationElement applicationElement = null;

            // Get the current context
            HttpContext currentContext = RenderingContext.CurrentHttpContext;

            // If there is a valid context
            if (currentContext != null)
            {
                // Get the session state
                HttpSessionState sessionState = currentContext.Session;

                // If there is a valid session state
                if (sessionState != null)
                {
                    // Get unique application ID
                    string appId = !string.IsNullOrEmpty(forcedAppId) ? forcedAppId : GetUniqueApplicationID(sessionState);

                    // If there is a valid appId
                    if (!string.IsNullOrEmpty(appId))
                    {
                        // Try to get existing application element
                        applicationElement = sessionState[appId] as ApplicationElement;

                        // If there is no valid application element
                        if (applicationElement == null)
                        {
                            // Create new application
                            sessionState[appId] = applicationElement = new ApplicationElement(appId);

                            // Get the live apps list
                            List<ApplicationElement> liveApps = sessionState[VT_LIVE_APPS_CACHE_KEY] as List<ApplicationElement>;

                            // If there is no live apps list
                            if (liveApps == null)
                            {
                                // Create live apps list
                                sessionState[VT_LIVE_APPS_CACHE_KEY] = liveApps = new List<ApplicationElement>();
                            }

                            // Add to live apps list
                            liveApps.Add(applicationElement);
                        }

                        // Set global revision
                        _globalRevision = applicationElement.Revision;

                        // Set the current application element
                        ApplicationElement.Current = applicationElement;

                        applicationElement.Initialize();
                    }
                }
            }

            return applicationElement;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Initialize()
        {
            // Create keep alive timer
            _keepAliveTimer = new ApplicationKeepAliveTimer((int)_applicationTimeout.TotalMilliseconds / 2);
            _keepAliveTimer.ParentElement = this;
            _keepAliveTimer.Start();
        }



        /// <summary>
        /// The application element cache key
        /// </summary>
        private const string APPLICATION_ELEMENT_CACHE_KEY = "vtapplication";

        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        public static ApplicationElement Current
        {
            get
            {
                ApplicationElement applicationElement = null;

                // Get the current context
                HttpContext currentContext = RenderingContext.CurrentHttpContext;

                // If there is a valid context
                if (currentContext != null)
                {
                    // Get the application element from the current context
                    applicationElement = currentContext.Items[APPLICATION_ELEMENT_CACHE_KEY] as ApplicationElement;

                    // If there is no valid application 
                    if (applicationElement == null)
                    {
                        // Create a new application element
                        applicationElement = CreateApplicationElement();
                    }
                }

                return applicationElement;
            }
            private set
            {
                // Get the current context
                HttpContext currentContext = RenderingContext.CurrentHttpContext;

                // If there is a valid context
                if (currentContext != null)
                {
                    // Set the application element to the current context
                    currentContext.Items[APPLICATION_ELEMENT_CACHE_KEY] = value;
                }

            }
        }




        /// <summary>
        /// Gets the current container.
        /// </summary>
        /// <value>
        /// The current container.
        /// </value>
        public static VisualElement CurrentContainer
        {
            get
            {
                return Current;
            }
        }


        /// <summary>
        /// Gets the startup path.
        /// </summary>
        /// <value>
        /// The startup path.
        /// </value>
        public static string StartupPath
        {
            get { return RenderingContext.CurrentHttpContext.Request.PhysicalApplicationPath; }
        }

        /// <summary>
        /// Beeps this instance.
        /// </summary>
        [RendererMethodDescription]
        public static void Beep()
        {
            ApplicationElement.InvokeMethod("Beep", typeof(ApplicationElement));
        }

        /// <summary>
        /// Exits this instance.
        /// </summary>
        public static void Exit()
        {
            BrowserUtils.NavigateUrl("about:blank");
        }

        /// <summary>
        /// Does the events.
        /// </summary>
        public static void DoEvents()
        {
            // Get the current application element
            ApplicationElement applicationElement = Current;

            // If there is a valid current application element
            if (applicationElement != null)
            {
                // TODO: DoEvents implementation and actions/operations/methods
                // Loop all methodInvoker
                //foreach(VisualElementMethodInvokerArgs methodInvoker in applicationElement._queuedMethodInvokers.ToArray())
                //{
                //    // If there is a valid methodInvoker
                //    if(methodInvoker != null)
                //    {
                //        // Do the specific behavior of the operator in case of a do events call
                //        //methodInvoker.DoEvents();
                //    }
                //}
            }
        }

        /// <summary>
        /// Gets the common application data path.
        /// </summary>
        /// <value>
        /// The common application data path.
        /// </value>
        public static string CommonAppDataPath
        {
            get
            {
                lock (internalSyncObject)
                {
                    if (commonAppDataPath == null)
                    {
                        commonAppDataPath = GetDataPath(StartupPath);
                    }
                }

                return commonAppDataPath;
            }
        }

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        /// <value>
        /// The client identifier.
        /// </value>
        public override string ClientID
        {
            get
            {
                return _appID;
            }
        }

        /// <summary>
        /// Gets the data path.
        /// </summary>
        /// <param name="basePath">The base path.</param>
        /// <returns></returns>
        private static string GetDataPath(string basePath)
        {
            string format = "{0}\\{1}\\{2}\\{3}";
            string companyName = CompanyName;
            string productName = ProductName;
            string productVersion = ProductVersion;
            string path = string.Format(CultureInfo.CurrentCulture, format, new object[] {
				basePath,
				companyName,
				productName,
				productVersion,
			});
            lock (internalSyncObject)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            return path;
        }

        /// <summary>
        /// Gets the executable path.
        /// </summary>
        /// <value>
        /// The executable path.
        /// </value>
        public static string ExecutablePath
        {
            get { return StartupPath; }
        }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public static string ProductName
        {
            get
            {
                lock (internalSyncObject)
                {
                    if (productName == null)
                    {
                        Assembly entryAssembly = Assembly.GetEntryAssembly();
                        if (entryAssembly != null)
                        {
                            object[] customAttributes = entryAssembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                            if ((customAttributes != null) && (customAttributes.Length > 0))
                            {
                                productName = ((AssemblyProductAttribute)customAttributes[0]).Product;
                            }
                        }
                        if ((productName == null) || (productName.Length == 0))
                        {
                            productName = "ProductName";
                        }

                    }
                }
                return productName;
            }
        }



        /// <summary>
        /// Gets the product version.
        /// </summary>
        /// <value>
        /// The product version.
        /// </value>
        public static string ProductVersion
        {
            get
            {
                lock (internalSyncObject)
                {
                    if (productVersion == null)
                    {
                        Assembly entryAssembly = Assembly.GetEntryAssembly();
                        if (entryAssembly != null)
                        {
                            object[] customAttributes = entryAssembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false);
                            if ((customAttributes != null) && (customAttributes.Length > 0))
                            {
                                productVersion = ((AssemblyInformationalVersionAttribute)customAttributes[0]).InformationalVersion;
                            }
                        }
                        if ((productVersion == null) || (productVersion.Length == 0))
                        {
                            productVersion = "1.0";
                        }
                    }
                }
                return productVersion;
            }
        }

        /// <summary>
        /// Gets the company name.
        /// </summary>
        /// <value>
        /// The company name.
        /// </value>
        public static string CompanyName
        {
            get
            {
                lock (internalSyncObject)
                {
                    if (companyName == null)
                    {
                        Assembly entryAssembly = Assembly.GetEntryAssembly();
                        if (entryAssembly != null)
                        {
                            object[] customAttributes = entryAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                            if ((customAttributes != null) && (customAttributes.Length > 0))
                            {
                                companyName = ((AssemblyCompanyAttribute)customAttributes[0]).Company;
                            }
                        }
                        if ((companyName == null) || (companyName.Length == 0))
                        {
                            companyName = "CompanyName";
                        }

                    }
                }
                return companyName;
            }
        }

        /// <summary>
        /// Gets the client identifier.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        /// <returns></returns>
        internal string GetClientId(IWindowElement windowElement)
        {
            // The client id
            string clientId = null;

            // If there is a valid window element
            if (windowElement != null)
            {
                // Try to get client id
                if (!_clientIdByRootElement.TryGetValue(windowElement, out clientId))
                {
                    // Set default client id
                    clientId = windowElement.ID;
                }
            }

            // Return client id
            return clientId;
        }

        /// <summary>
        /// Increments the revision.
        /// </summary>
        public static void IncrementRevision()
        {
            // Get current application element
            ApplicationElement applicationElement = Current;

            // If there is a valid application element
            if (applicationElement != null)
            {
                // Increment revision
                applicationElement.Revision++;

                // Set global revision
                _globalRevision = applicationElement.Revision;

                // Clear change operation cache
                applicationElement._uniqueInvokers = null;
            }
        }




        /// <summary>
        /// Registers the change.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="propertyName">The property name.</param>
        static internal void RegisterChange(VisualElement visualElement, string propertyName)
        {
            // Get current application element
            ApplicationElement applicationElement = Current;

            // If there is a valid application element
            if (applicationElement != null)
            {
                // Get the change operation
                HashSet<string> propertyChangeMethodData = applicationElement.GetOrCreatePropertyChangedMethodData(visualElement);

                // If there is a valid change operation
                if (propertyChangeMethodData != null)
                {
                    // Add property change operation
                    propertyChangeMethodData.Add(propertyName);
                }
            }
        }

        /// <summary>
        /// Gets the or create property changed method data.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <returns></returns>
        private HashSet<string> GetOrCreatePropertyChangedMethodData(VisualElement visualElement)
        {
            // Get property changed method invoker
            VisualElementMethodInvoker methodInvoker = CreateOrUpdateUniqueMethodInvoker(PropertyChangeMethodName, visualElement, null, (existingData) => existingData is HashSet<string> ? existingData : new HashSet<string>());

            // If there is a valid method invoker
            if (methodInvoker != null)
            {
                // Get method data
                return methodInvoker.Data as HashSet<string>;
            }

            return null;
        }


        /// <summary>
        ///Begins running a standard application message loop on the current thread, without a form.
        /// </summary>
        public static void Run()
        {
        }

        /// <summary>
        /// Gets the product major version.
        /// </summary>
        /// <value>
        /// The product major version.
        /// </value>
        public static int ProductMajorVersion
        {
            get { return default(short); }
        }

        /// <summary>
        /// Gets the product minor version.
        /// </summary>
        /// <value>
        /// The product minor version.
        /// </value>
        public static int ProductMinorVersion
        {
            get { return default(short); }
        }

        /// <summary>
        /// Gets the product revision.
        /// </summary>
        /// <value>
        /// The product revision.
        /// </value>
        public static int ProductRevision
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the product build.
        /// </summary>
        /// <value>
        /// The product build.
        /// </value>
        public static int ProductBuild
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Occurs when mouse position changed.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler MousePositionChanged;

        /// <summary>
        /// Performs the mouse position changed.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        internal void PerformMousePositionChanged(MouseEventArgs args)
        {
            // If there is a valid args
            if (args != null)
            {
                // Set the mouse position
                _mousePosition = new Point(args.X, args.Y);

                // If there are listeners
                if (this.MousePositionChanged != null)
                {
                    // Raise the mouse position changed event
                    this.MousePositionChanged(this, args);
                }
            }
        }

        /// <summary>
        /// Occurs when mouse active control changed.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler ActiveControlChanged;

        /// <summary>
        /// Performs the active control changed.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        internal void PerformActiveControlChanged(ValueChangedArgs<string> args)
        {
            // If there is a valid args
            if (args != null && !string.IsNullOrEmpty(Convert.ToString(args.Value)))
            {
                ActiveControlId = Convert.ToString(args.Value);
            }
        }

        /// <summary>
        /// Determines whether is main window.
        /// </summary>
        /// <param name="windowElement">The window element.</param>
        /// <returns></returns>
        internal static bool IsMainWindow(WindowElement windowElement)
        {
            // If there is a valid window element
            if (windowElement != null)
            {
                // Get current application element
                ApplicationElement applicationElement = Current;

                // If there is a valid application element
                if (applicationElement != null)
                {
                    // Return flag indicating is the main window
                    return applicationElement._mainWindow == windowElement;
                }
            }

            // Could not application
            return false;
        }





        /// <summary>
        /// Gets the client element by path.
        /// </summary>
        /// <param name="clientElementID">The client element identifier.</param>
        /// <returns></returns>
        public IClientElement GetClientElementByPath(string clientElementID)
        {

            // If there is a valid client element id
            if (!string.IsNullOrEmpty(clientElementID))
            {
                // The root element
                IRootElement rootElement = null;

                // Try to get root element 
                if (!_rootElementByClientId.TryGetValue(clientElementID, out rootElement))
                {
                    // Try to get visual element
                    return this.GetVisualElementByPath(clientElementID);
                }
                else
                {
                    return rootElement as IClientElement;
                }
            }

            return null;
        }



        /// <summary>
        /// Releases the method invoker.
        /// </summary>
        /// <param name="methodInvoker">The method invoker.</param>
        internal void ReleaseMethodInvoker(IVisualElementMethodInvoker methodInvoker)
        {
            this.UnregisterRootElement(methodInvoker);
        }




        /// <summary>
        /// Gets the unique application identifier.
        /// </summary>
        /// <returns></returns>
        private static string GetUniqueApplicationID(HttpSessionState sessionState)
        {
            const string SESSION_APP_ID_KEY_PREFIX = "vtsid_";

            // The unique index
            int index = 1;

            // Get unique appId
            string uniqueAppID = string.Concat(SESSION_APP_ID_KEY_PREFIX, index);

            // If there is a valid session state
            if (sessionState != null)
            {

                // Loop until there is an empty slot
                while (sessionState[uniqueAppID] != null)
                {
                    // Next slot
                    index++;

                    // Get unique appId
                    uniqueAppID = string.Concat(SESSION_APP_ID_KEY_PREFIX, index);
                }
            }

            return uniqueAppID;
        }

        /// <summary>
        /// The bookmarks
        /// </summary>
        private Dictionary<string, HistoryBookmark> _bookmarks = new Dictionary<string, HistoryBookmark>();


        /// <summary>
        /// Provides support for storing data and navigating to it
        /// </summary>
        public abstract class HistoryBookmark
        {
            /// <summary>
            /// Navigates to this bookmark
            /// </summary>
            public abstract void Navigate(ApplicationElement application);
        }

        /// <summary>
        /// Provides support for automated bookmarks
        /// </summary>
        private class AutomatedBookmark : HistoryBookmark
        {
            /// <summary>
            /// The element ids
            /// </summary>
            private string[] _elementIds;

            /// <summary>
            /// Initializes a new instance of the <see cref="AutomatedBookmark"/> class.
            /// </summary>
            /// <param name="elementIds">The element ids.</param>
            public AutomatedBookmark(string[] elementIds)
            {
                _elementIds = elementIds;
            }


            /// <summary>
            /// Navigates to this bookmark
            /// </summary>
            public override void Navigate(ApplicationElement application)
            {
                // If there is a valid application element
                if (application != null)
                {
                    // Loop all element ids
                    foreach (string elementId in _elementIds)
                    {
                        // Get the element by id
                        IClientHistoryElement historyElement = application.GetClientElementByPath(elementId) as IClientHistoryElement;

                        // If there is a valid event element
                        if (historyElement != null)
                        {
                            // Navigate
                            historyElement.Navigate();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ensures the history bookmark.
        /// </summary>
        internal static void EnsureHistoryBookmark()
        {
            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication != null)
            {
                // Ensure history bookmark added
                currentApplication.DoEnsureHistoryBookmark();
            }
        }

        /// <summary>
        /// Does the ensure history bookmark.
        /// </summary>
        private void DoEnsureHistoryBookmark()
        {
            // Get history elements
            IClinetHistoryContainerElement[] historyContainerElements = GetHistoryElements();

            // If there are valid elements
            if (historyContainerElements != null && historyContainerElements.Length > 0)
            {
                // The history token
                string historyToken;

                // If there are no bookmarks
                if (_bookmarks.Count == 0)
                {
                    // The default token
                    historyToken = string.Empty;
                }
                else
                {
                    // Create history token
                    historyToken = string.Concat("!bk", _bookmarks.Count);
                }

                // The ids list
                List<string> elementIds = new List<string>();

                // Loop history elements
                foreach (IClinetHistoryContainerElement historyContainerElement in historyContainerElements)
                {
                    // If there is a valid history container
                    if (historyContainerElement != null && historyContainerElement.HistoryEnabled)
                    {
                        // Get navigation element
                        IClientHistoryElement historyElement = historyContainerElement.NavigatedElement;

                        // If there is a valid navigation element
                        if (historyElement != null)
                        {
                            // Add id
                            elementIds.Add(VisualElement.GetElementId(historyElement));
                        }
                    }
                }

                // Store the bookmark
                _bookmarks[historyToken] = new AutomatedBookmark(elementIds.ToArray());

                // Add the history token
                BrowserUtils.AddHistoryToken(historyToken);
            }

        }

        /// <summary>
        /// Gets the history elements.
        /// </summary>
        /// <returns></returns>
        private IClinetHistoryContainerElement[] GetHistoryElements()
        {
            List<Tuple<IClinetHistoryContainerElement, int>> clientHistoryElements = new List<Tuple<IClinetHistoryContainerElement, int>>();

            // Get current application element
            ApplicationElement currentApplication = Current;

            // If there is a valid application
            if (currentApplication != null)
            {
                // Loop all windows
                foreach (IWindowElement currentWindow in currentApplication._windowStack)
                {
                    // Get element
                    VisualElement element = currentWindow as VisualElement;

                    // If there is a valid element
                    if (element != null)
                    {
                        FillClientHistoryElements(clientHistoryElements, element);
                    }
                }
            }

            // Return items ordered by depth
            return clientHistoryElements.OrderBy(element => element.Item2).Select(element => element.Item1).ToArray();
        }

        /// <summary>
        /// Fills the client history elements.
        /// </summary>
        /// <param name="clientHistoryElements">The client history elements.</param>
        /// <param name="element">The element.</param>
        /// <param name="depth">The depth.</param>
        private void FillClientHistoryElements(List<Tuple<IClinetHistoryContainerElement, int>> clientHistoryElements, VisualElement element, int depth = 0)
        {
            // Loop all children
            foreach (VisualElement child in element.Children)
            {
                // Get history element
                IClinetHistoryContainerElement historyElement = child as IClinetHistoryContainerElement;

                // If there is a valid history element
                if (historyElement != null)
                {
                    // Add item
                    clientHistoryElements.Add(new Tuple<IClinetHistoryContainerElement, int>(historyElement, depth));
                }

                // Fill the children of the child
                FillClientHistoryElements(clientHistoryElements, child, depth + 1);
            }
        }

        /// <summary>
        /// Navigates the history.
        /// </summary>
        /// <param name="historyToken">The history token.</param>
        void IClientHistoryManager.Navigate(string historyToken)
        {
            HistoryBookmark bookmark = null;

            // If there is a valid token
            if (historyToken != null)
            {
                // Get bookmark
                if (_bookmarks.TryGetValue(historyToken, out bookmark))
                {
                    // If there is a valid bookmark
                    if (bookmark != null)
                    {
                        // Navigate to bookmark
                        bookmark.Navigate(this);

                        // Exit navigate handling
                        return;
                    }
                }

                // Loop all windows
                foreach (IWindowElement currentWindow in this._windowStack)
                {
                    // Get window element
                    WindowElement windowElement = currentWindow as WindowElement;

                    // If there is a valid window element
                    if (windowElement != null)
                    {
                        windowElement.NavigateBookmark(historyToken);
                    }
                }
            }
        }

        private object _timersSync = new object();
        
        /// <summary>
        /// The timers
        /// </summary>
        private List<ITimer> _timers = new List<ITimer>();

        /// <summary>
        /// Adds the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        void ITimerHandler.AddTimer(ITimer timer)
        {
            if (timer != null)
            {
                lock (_timersSync)
                {
                    _timers.Add(timer);
                }
            }
        }

        /// <summary>
        /// Removes the timer.
        /// </summary>
        /// <param name="timer">The timer.</param>
        void ITimerHandler.RemoveTimer(ITimer timer)
        {
            if (timer != null)
            {
                lock (_timersSync)
                {
                    _timers.Remove(timer);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has timers.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has timers; otherwise, <c>false</c>.
        /// </value>
        bool ITimerHandler.HasTimers
        {
            get
            {
                return this.HasTimersInternal;
            }
        }

        private bool HasTimersInternal
        {
            get
            {
                lock (_timersSync)
                {
                    return _timers.Count > 0;
                }
            }
        }

        /// <summary>
        /// Invokes the timers.
        /// </summary>
        /// <param name="currentTicks">The current ticks.</param>
        /// <returns></returns>
        int ITimerHandler.InvokeTimers(long currentTicks)
        {
            // If there are timers
            if (this.HasTimersInternal)
            {
                // Next invocation time
                int nextInvokation = 0;

                List<ITimer> timersToInvoke = new List<ITimer>();
                lock (_timersSync)
                {
                    // Loop all timers
                    foreach (ITimer timer in _timers)
                    {
                        // Get next scheduled invocation time
                        int timerNextInvokation = timer.GetNextInvokation(currentTicks);

                        // If less then 1000ms to next invocation
                        if (timerNextInvokation < 30)
                        {
                            timersToInvoke.Add(timer);
                        }
                        else
                        {
                            nextInvokation = UpdateNextInvokation(nextInvokation, timerNextInvokation);
                        }
                    }
                }
                foreach (ITimer timer in timersToInvoke)
                {
                    // Invoke timer and return next invocation time
                    int timerNextInvokation = timer.InvokeTimer();

                    nextInvokation = UpdateNextInvokation(nextInvokation, timerNextInvokation);
                }
                // If invocation is to far
                if (nextInvokation > 120000)
                {
                    nextInvokation = 120000;
                }

                // Return next invocation time
                return nextInvokation;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Updates the next invokation.
        /// </summary>
        /// <param name="nextInvokation">The next invokation.</param>
        /// <param name="timerNextInvokation">The timer next invokation.</param>
        /// <returns></returns>
        private static int UpdateNextInvokation(int nextInvokation, int timerNextInvokation)
        {
            // If close invocation
            if (timerNextInvokation < 100)
            {
                nextInvokation = 100;
            }
            // If no invocation time was set yet
            else if (nextInvokation == 0)
            {
                nextInvokation = timerNextInvokation;
            }
            // If this is a closer invocation time
            else if (nextInvokation > timerNextInvokation)
            {
                nextInvokation = timerNextInvokation;
            }
            return nextInvokation;
        }
        /// <summary>
        /// Executes the callback on response
        /// </summary>
        /// <param name="callback">the callback</param>
        /// <param name="sender">(optional) the sender</param>
        /// <param name="args">(optional) the args</param>
        /// <param name="delay">(optional) the delay</param>
        [RendererMethodDescription]
        public static void ExecuteOnRespone(EventHandler callback, object sender = null, EventArgs args = null,
            int delay = 100)
        {
            if (args == null)
            {
                args = EventArgs.Empty;
            }
            Action<object> methodCallback = (x) => { callback(sender, args); };
            ApplicationElement.InvokeMethod("ExecuteOnRespone", typeof (ApplicationElement), delay, methodCallback);

        }

        /// <summary>
        /// Invokes the timers.
        /// </summary>
        public void InvokeTimers()
        {
            // Invoke timers
            int nextCallback = ((ITimerHandler)this).InvokeTimers(DateTime.Now.Ticks);

            // If timer needs to be invoked
            if (nextCallback > 0)
            {
                // Raise next delayed event
                BrowserUtils.RaiseDelayedEvents(nextCallback);

            }
        }
    }


}
