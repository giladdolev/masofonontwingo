﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// The window action type
    /// </summary>
    public enum WindowActionType
    {
        /// <summary>
        /// The open
        /// </summary>
        Open,

        /// <summary>
        /// Open a modal dialog
        /// </summary>
        OpenModal,

        /// <summary>
        /// The open main
        /// </summary>
        OpenAsMain,

        /// <summary>
        /// The close
        /// </summary>
        Close
    }
}
