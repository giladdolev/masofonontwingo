using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;


namespace System.Web.VisualTree.Elements
{
    public class ListViewElement : ControlContainerElement, IBindingViewModel
    {
        private bool _useCompatibleStateImageBehavior = true;

        private bool _showItemToolTips = true;

        private bool _labelWrap = true;

        private bool _fullRowSelect = false;

        private ItemsView _view = ItemsView.LargeIcon;

        private bool _autoGenerateColumns = true;

        private bool _menuDisabled = true;

        private List<int> _checkedIndeces = null;


        /// <summary>
        /// The columns
        /// </summary>
        private readonly ColumnHeaderCollection _columns = null;

        /// <summary>
        /// The items
        /// </summary>
        private readonly ListViewItemCollection _items = null;

        /// <summary>
        /// The selected item
        /// </summary>
        private ListViewItem[] _selectedItems = new ListViewItem[0];

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewElement"/> class.
        /// </summary>
        public ListViewElement()
        {
            // Create columns collection
            _columns = new ColumnHeaderCollection(this);

            // Create the items collection
            _items = new ListViewItemCollection(this, true);
            
        }

        /// <summary>
        /// Gets or sets a value indicating whether [automatic generate columns].
        /// </summary>
        /// <value>
        /// <c>true</c> if [automatic generate columns]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool AutoGenerateColumns
        {
            get { return _autoGenerateColumns; }
            set
            {
                _autoGenerateColumns = value;


                // Notify property AutoGenerateColumns changed
                OnPropertyChanged("AutoGenerateColumns");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether clicking an item selects all its subitems. 
        /// </summary>
        [RendererPropertyDescription]
        public bool FullRowSelect
        {
            get
            {
                return _fullRowSelect;
            }
            set
            {
                if (value != _fullRowSelect)
                {
                    _fullRowSelect = value;
                    OnPropertyChanged("FullRowSelect");
                }
            }
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        IEnumerable<IBindingDataColumn> IBindingViewModel.Columns
        {
            get
            {
                return this.Columns;
            }
        }

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        IEnumerable<IBindingViewModelRecord> IBindingViewModel.Records
        {
            get
            {
                return this.Items;
            }
        }

        /// <summary>
        /// Binds the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        void IBindingViewModel.Bind(IDataReader dataReader)
        {
            // Clear list
            if (_autoGenerateColumns)
            {
                _columns.Clear();
            }
            _items.Clear();

            // If there is a valid data reader
            if (dataReader == null)
            {
                return;
            }

            if (_autoGenerateColumns)
            {
                // Loop all fields
                for (int index = 0; index < dataReader.FieldCount; index++)
                {
                    // Add column
                    _columns.Add(new ColumnHeader()
                    {
                        // Set the text
                        Text = dataReader.GetName(index),

                        // Set the width
                        Width = 100
                    });
                }
            }


            // Get the columns count
            int columnsCount = dataReader.FieldCount;


            // Loop all records
            while (dataReader.Read())
            {
                // Create item
                ListViewItem item = new ListViewItem();

                // Loop all column indices
                for (int index = 0; index < columnsCount; index++)
                {
                    // Create header cell
                    ListViewSubitem subItem = new ListViewSubitem();

                    // Get string from current index
                    subItem.Text = Convert.ToString(dataReader.GetValue(index));

                    // Set sub item
                    item.Subitems.Add(subItem);
                }

                // Add item
                _items.Add(item);
            }

        }

        /// <summary>
        /// Clear all items in the list view (including headers).
        /// </summary>
        public void Clear()
        {
            this.Items.Clear();
        }


        /// <summary>
        /// Gets a list of the current checked items
        /// </summary>
        [DesignerIgnore]
        public List<int> CheckedIndices
        {
            get
            {
                return _checkedIndeces;
            }
        }
        /// <summary>
        /// Gets a value indicating whether headers are visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if headers are visible; otherwise, <c>false</c>.
        /// </value>
        public bool HeadersVisible
        {
            get
            {
                return this.View == ItemsView.Details;
            }
        }



        /// <summary>
        /// Gets a value indicating whether column lines visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if column lines visible; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool ColumnLinesVisible
        {
            get
            {
                return this.HeadersVisible;
            }
        }



        /// <summary>
        /// Gets a value indicating whether automatic adjust column width.
        /// </summary>
        /// <value>
        /// <c>true</c> if automatic adjust column width; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool AutoAdjustColumnWidth
        {
            get
            {
                return !this.HeadersVisible;
            }
        }



        /// <summary>
        /// Gets a value indicating whether row lines visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if row lines visible; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool RowLinesVisible
        {
            get
            {
                return this.HeadersVisible;
            }
        }


        private event EventHandler<ValueChangedArgs<int>> _itemClick;
        [RendererEventDescription]
        public event EventHandler<ValueChangedArgs<int>> ItemClick
        {
            add
            {
                bool needNotification = (_itemClick == null);

                _itemClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ItemClick");
                }

            }
            remove
            {
                _itemClick -= value;

                if (_itemClick == null)
                {
                    OnEventHandlerDeattached("ItemClick");
                }
            }
        }

        /// <summary>
        /// Called when item clicked.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected virtual void OnItemClick(ValueChangedArgs<int> args)
        {

            // Check if there are listeners.
            if (_itemClick != null)
            {
                _itemClick(this, args);
            }
        }


        /// <summary>
        /// Performs the item click.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformItemClick(EventArgs args)
        {
            // Get value as value changed
            ValueChangedArgs<int> valueChangedArgs = args as ValueChangedArgs<int>;

            PerformMouseDown(MouseEventArgs.Empty);
            PerformClick(EventArgs.Empty);
            OnItemClick(valueChangedArgs);
        }


        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ColumnHeader>))]
        public ColumnHeaderCollection Columns
        {
            get
            {
                return _columns;
            }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            ColumnHeader columnHeader = visualElement as ColumnHeader;
            if (columnHeader != null)
            {
                _columns.Add(columnHeader);
                return;
            }
            ListViewItem listViewItem = visualElement as ListViewItem;
            if (listViewItem != null)
            {
                listViewItem.ParentElement = this;
                _items.Add(listViewItem);

                // update listView index . 
                listViewItem.Index = this._items.IndexOf(listViewItem);
                if (listViewItem.Selected)
                {
                    this.SetItemState(listViewItem, listViewItem.Selected);
                }
            }
        }



        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ListViewItem>))]
        public ListViewItemCollection Items
        {
            get
            {
                return _items;
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                // If there are columns
                if (_columns != null)
                {
                    // Loop all columns
                    foreach (VisualElement column in _columns)
                    {
                        // Return column
                        yield return column;
                    }
                }

                // If there are items
                if (_items != null)
                {
                    // Loop all items
                    foreach (VisualElement item in _items)
                    {
                        // Return item
                        yield return item;
                    }
                }
            }
        }



        private bool _checkBoxes = false;
        /// <summary>
        /// Gets or sets a value indicating whether check boxes are visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if check boxes are visible; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        [Category("Appearance")]
        public bool CheckBoxes
        {
            get { return _checkBoxes; }
            set
            {
                if (_checkBoxes != value)
                {
                    _checkBoxes = value;

                    OnPropertyChanged("CheckBoxes");
                }
            }
        }






        /// <summary>
        /// Gets or sets a value indicating whether use compatible state image behavior
        /// </summary>
        /// <value>
        ///   <c>true</c> if use compatible state image behavior; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool UseCompatibleStateImageBehavior
        {
            get { return _useCompatibleStateImageBehavior; }
            set { _useCompatibleStateImageBehavior = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the text of an item or sub-item has the appearance of a hyperlink when the mouse pointer passes over it.
        /// </summary>
        /// <value>
        /// true if the item text has the appearance of a hyperlink when the mouse passes over it , otherwise false. The default is false.
        /// </value>
        private bool _hotTracking = false;
        public bool HotTracking
        {
            get { return this._hotTracking; }
            set
            {
                this._hotTracking = value;
            }
        }

        [DefaultValue(true)]
        public bool ShowItemToolTips
        {
            get { return _showItemToolTips; }
            set { _showItemToolTips = value; }
        }



        [IDReferenceProperty]
        public ImageList SmallImageList
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the view.
        /// </summary>
        /// <value>
        /// The view.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(ItemsView.LargeIcon)]
        public ItemsView View
        {
            get { return _view; }
            set
            {
                if (_view != value)
                {
                    _view = value;
                    OnPropertyChanged("View");
                }
            }
        }


        /// <summary>
        /// Gets the selected items.
        /// </summary>
        /// <value>
        /// The selected items.
        /// </value>
        [DesignerIgnore]
        public SelectedListViewItemCollection SelectedItems
        {
            get
            {
                _selectedItems = _selectedItems.Where(item => _items.Contains(item)).ToArray();
                return new SelectedListViewItemCollection(_selectedItems);
            }
        }




        /// <summary>
        /// Gets or sets the label edit.
        /// </summary>
        /// <value>
        /// The label edit.
        /// </value>
        public ListLabelEditConstants LabelEdit
        {
            get;
            set;
        }




        [DefaultValue(true)]
        public bool LabelWrap
        {
            get { return _labelWrap; }
            set { _labelWrap = value; }
        }




        private bool _hideSelection = true;
        [DefaultValue(true)]
        public bool HideSelection
        {
            get { return _hideSelection; }
            set { _hideSelection = value; }
        }




        public bool GridLines
        {
            get;
            set;
        }




        /// <summary>
        ///Gets or sets the sorting comparer for the control.
        /// </summary>
        public IComparer ListViewItemSorter
        {
            get;
            set;
        }



        /// <summary>
        /// ColumnClick event handler.
        /// </summary>
        private event EventHandler<ColumnClickEventArgs> _columnClick;

        /// <summary>
        /// Add or remove ColumnClick event handler action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ColumnClickEventArgs> ColumnClick
        {
            add
            {
                bool needNotification = (_columnClick == null);

                _columnClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ColumnClick event handler");
                }

            }
            remove
            {
                _columnClick -= value;

                if (_columnClick == null)
                {
                    OnEventHandlerDeattached("ColumnClick event handler");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ColumnClick event handler listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasColumnClick
        {
            get { return _columnClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ColumnClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ColumnClickEventArgs"/> instance containing the event data.</param>
        protected virtual void OnColumnClick(ColumnClickEventArgs args)
        {

            // Check if there are listeners.
            if (_columnClick != null)
            {
                _columnClick(this, args);
            }
        }



        /// <summary>
        /// ItemChecked event handler.
        /// </summary>
        private event EventHandler<ItemCheckEventArgs> _itemChecked;

        /// <summary>
        /// Add or remove ItemChecked action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ItemCheckEventArgs> ItemChecked
        {
            add
            {
                bool needNotification = (_itemChecked == null);

                _itemChecked += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ItemChecked");
                }

            }
            remove
            {
                _itemChecked -= value;

                if (_itemChecked == null)
                {
                    OnEventHandlerDeattached("ItemChecked");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ItemChecked listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasItemCheckedListeners
        {
            get { return _itemChecked != null && _checkBoxes; }
        }

        /// <summary>
        /// Raises the <see cref="E:ItemChecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ItemChecked"/> instance containing the event data.</param>
        protected virtual void OnItemChecked(ItemCheckEventArgs args)
        {

            // Check if there are listeners.
            if (_itemChecked != null)
            {
                _itemChecked(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ItemChecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformItemChecked(ItemCheckEventArgs args)
        {
            int itemCheckRowIndex = args.Index;

            // Update the item's check property
            if (itemCheckRowIndex > -1 && itemCheckRowIndex < _items.Count)
            {
                _items[itemCheckRowIndex].Checked = true;
            }

            // Update the current selected indices
            UpdateCheckedIndices();

            // Raise the ItemChecked event.
            OnItemChecked(args);
        }

        /// <summary>
        /// ItemUnchecked event handler.
        /// </summary>
        private event EventHandler<ItemCheckEventArgs> _itemUnchecked;

        /// <summary>
        /// Add or remove ItemUnchecked action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ItemCheckEventArgs> ItemUnchecked
        {
            add
            {
                bool needNotification = (_itemUnchecked == null);

                _itemUnchecked += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ItemUnchecked");
                }

            }
            remove
            {
                _itemUnchecked -= value;

                if (_itemUnchecked == null)
                {
                    OnEventHandlerDeattached("ItemUnchecked");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ItemUnchecked listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has item uncheck listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasItemUncheckedListeners
        {
            get { return _itemUnchecked != null && _checkBoxes; }
        }

        /// <summary>
        /// Raises the <see cref="E:ItemUnchecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ItemUnchecked"/> instance containing the event data.</param>
        protected virtual void OnItemUnchecked(ItemCheckEventArgs args)
        {

            // Check if there are listeners.
            if (_itemUnchecked != null)
            {
                _itemUnchecked(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ItemUnchecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformItemUnchecked(ItemCheckEventArgs args)
        {
            int itemCheckRowIndex = args.Index;

            // Update the item's check property
            if (itemCheckRowIndex > -1 && itemCheckRowIndex < _items.Count)
            {
                _items[itemCheckRowIndex].Checked = false;
            }

            // Update the current selected indices
            UpdateCheckedIndices();

            // Raise the ItemUnchecked event.
            OnItemUnchecked(args);
        }

        /// <summary>
        /// Returns a reference to the first object visible in the internal area of a ListView.
        /// </summary>
        /// <value></value>
        [DesignerIgnore]
        public ListViewItem FirstVisible
        {
            get { return _items.FirstOrDefault(); }
        }




        /// <summary>
        /// Focuses this instance.
        /// </summary>
        /// <returns></returns>
        public new bool Focus()
        {
            if (Items != null && Items.Count > 0)
            {
                Items[0].Selected = true;
            }
            return true;
        }




        /// <summary>
        /// Finds the item.
        /// </summary>
        /// <param name="text">A string expression indicating the ListItem object to be found.</param>
        /// <returns></returns>
        public ListViewItem FindItem(string text)
        {
            return FindItem(text, ListViewFindItemOptions.Text, 0, ListViewMatchOptions.WholeWord);
        }




        /// <summary>
        /// Finds the item.
        /// </summary>
        /// <param name="text">A string expression indicating the ListItem object to be found.</param>
        /// <param name="option">specifying whether the string will be matched to the ListItem object's Text, Subitems, or Tag property, as described in Settings..</param>
        /// <returns></returns>
        public ListViewItem FindItem(string text, ListViewFindItemOptions option)
        {
            return FindItem(text, option, 0, ListViewMatchOptions.WholeWord);
        }




        /// <summary>
        /// Finds the item.
        /// </summary>
        /// <param name="text">A string expression indicating the ListItem object to be found.</param>
        /// <param name="option">specifying whether the string will be matched to the ListItem object's Text, Subitems, or Tag property, as described in Settings..</param>
        /// <param name="index"> An integer or string that uniquely identifies a member of an object collection and specifies the location from which to begin the search. The integer is the value of the Index property; the string is the value of the Key property. If no index is specified, the default is 1.</param>
        /// <returns></returns>
        public ListViewItem FindItem(string text, ListViewFindItemOptions option, int index)
        {
            return FindItem(text, option = ListViewFindItemOptions.Text, index = 0, ListViewMatchOptions.WholeWord);
        }




        /// <summary>
        /// Finds the item.
        /// </summary>
        /// <param name="text">A string expression indicating the ListItem object to be found.</param>
        /// <param name="option">specifying whether the string will be matched to the ListItem object's Text, Subitems, or Tag property, as described in Settings..</param>
        /// <param name="index"> An integer or string that uniquely identifies a member of an object collection and specifies the location from which to begin the search. The integer is the value of the Index property; the string is the value of the Key property. If no index is specified, the default is 1.</param>
        /// <param name="match">Specifying that a match will occur if the item's Text property is the same as the string, as described in Settings.</param>
        /// <returns></returns>
        public ListViewItem FindItem(string text, ListViewFindItemOptions option, int index, ListViewMatchOptions match)
        {
            if (_items != null && _items.Count > 0)
            {
                switch (option)
                {
                    case ListViewFindItemOptions.Text:
                        if (!String.IsNullOrEmpty(text))
                        {
                            return _items.FirstOrDefault(item => String.Equals(item.Text, text));
                        }
                        else if (index >= 0 && index < _items.Count)
                        {
                            return _items[index];
                        }
                        return null;

                    case ListViewFindItemOptions.Tag:
                        return _items.FirstOrDefault(item => Equals(item.Tag, text));

                    case ListViewFindItemOptions.Subitem:
                        return _items.FirstOrDefault(item =>
                        {
                            ListViewSubitemCollection subitems = item.Subitems;
                            if (subitems != null && subitems.Count > 0)
                            {
                                return subitems.Any(subitem => String.Equals(subitem.Text, text));
                            }

                            return false;
                        });
                }
            }
            return null;
        }

        /// <summary>
        /// Gets or sets the selectedIndex.
        /// </summary>
        /// <value>
        /// The selectedIndex.
        /// </value>
        [DefaultValue(-1)]
        public SelectedListViewIndexCollection SelectedIndexes
        {
            get
            {
                return new SelectedListViewIndexCollection(_selectedItems.Select(item => item.Index).ToList());
            }
        }


        /// <summary>
        /// SelectionChanged event handler.
        /// </summary>
        private event EventHandler<ListViewItemSelectionChangedEventArgs> _SelectionChanged;

        /// <summary>
        /// Add or remove SelectionChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ListViewItemSelectionChangedEventArgs> SelectionChanged
        {
            add
            {
                bool needNotification = (_SelectionChanged == null);

                _SelectionChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectionChanged");
                }

            }
            remove
            {
                _SelectionChanged -= value;

                if (_SelectionChanged == null)
                {
                    OnEventHandlerDeattached("SelectionChanged");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:SelectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectionChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectionChanged(ListViewItemSelectionChangedEventArgs args)
        {

            // Check if there are listeners.
            if (_SelectionChanged != null)
            {
                _SelectionChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the item click.
        /// </summary>
        /// <param name="selectionChangedArgs">The <see cref="ListViewItemSelectionChangedEventArgs"/> instance containing the event data.</param>
        public void PerformSelectionChanged(ListViewItemSelectionChangedEventArgs selectionChangedArgs)
        {

            Int32[] selectedIndexes = null;
            // If there is a valid changed args
            if (selectionChangedArgs != null)
            {
                if (selectionChangedArgs.SelectedIndexes != null)
                {
                    selectedIndexes = selectionChangedArgs.SelectedIndexes;
                    _selectedItems = new ListViewItem[selectedIndexes.Length];
                    for (int i = 0; i < selectedIndexes.Length; i++)
                    {
                        if (selectedIndexes[i] != -1)
                        {
                            _selectedItems[i] = _items[selectedIndexes[i]];
                            _selectedItems[i].Selected = true;
                            _selectedItems[i].Checked = true;
                        }
                    }
                }
                else
                {
                    _selectedItems = null;
                }
            }

            OnSelectionChanged(selectionChangedArgs);

        }


        private bool _multiSelect = true;
        /// <summary>
        /// Gets or sets a value indicating whether multiple items can be selected.
        /// </summary>
        /// <value>
        /// true if multiple items in the control can be selected at one time; otherwise, false. The default is true.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(true)]
        [Category("Appearance")]
        public bool MultiSelect
        {
            get { return _multiSelect; }
            set
            {
                if (_multiSelect != value)
                {
                    _multiSelect = value;

                    OnPropertyChanged("MultiSelect");
                }
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItemData"/> class.
        /// </summary>
        /// <param name="RowIndex">The row index.</param>
        /// <param name="ColumnIndex">The column index.</param>
        /// <param name="color">The fore color.</param>
        [RendererMethodDescription]
        public void SetItemForeColor(Color color, int RowIndex, int ColumnIndex)
        {
            this.InvokeMethodOnce("SetItemForeColor", new ListViewItemData(RowIndex, ColumnIndex, color));
        }


        private SortOrder _sorting;

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(default(System.Web.VisualTree.SortOrder))]
        public SortOrder Sorting
        {
            get { return _sorting; }
            set { _sorting = value; }
        }

        /// <summary>
        /// Sort the ListView according to SortOrder
        /// </summary>
        [RendererMethodDescription]
        public void Sort()
        {
            List<ListViewItem> item = null;

            if (Sorting == SortOrder.None)
                return;

            if (Sorting == SortOrder.Ascending)
            {
                item = _items.OrderBy(x => x.Text).ToList();
            }
            else if (Sorting == SortOrder.Descending)
            {
                item = _items.OrderByDescending(x => x.Text).ToList();
            }

            // clear all items of the list view
            _items.Clear();

            // add the ordered items to the list view
            foreach (ListViewItem it in item)
                _items.Add(it);

        }
        internal void SetItemState(ListViewItem item, bool selected)
        {
            if (!_items.Contains(item))
            {
                return;
            }

            if (MultiSelect)
            {
                if (_selectedItems != null)
                {
                    _selectedItems = _selectedItems.Concat(new ListViewItem[] { item }).ToArray();
                }
                else
                {
                    _selectedItems = new ListViewItem[] { item };
                }
            }
            else
            {
                foreach (var selectedItem in _selectedItems)
                {
                    selectedItem.SelectedInternal = false;
                }

                _selectedItems = new ListViewItem[] { item };
            }

            Int32[] selectedIndexes = _selectedItems.OrderBy(itm => itm.Index).Select(itm => itm.Index).ToArray();

            PerformSelectionChanged(new ListViewItemSelectionChangedEventArgs(selectedIndexes));
        }

        /// <summary>
        /// Updates the CheckedIndices list
        /// </summary>
        internal void UpdateCheckedIndices()
        {
            _checkedIndeces = new List<int>();
            for (int i = 0; i < _items.Count; i++)
            {
                if (Items[i].Checked)
                {
                    _checkedIndeces.Add(i);
                }
            }
        }

        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();

            int columnCount = this.Columns.Count;

            foreach (var item in this.Items)
            {
                if ((item.Subitems.Count + 1) > columnCount)
                {
                    columnCount = item.Subitems.Count + 1;
                }
            }

            while (this.Columns.Count < columnCount)
            {
                this.Columns.Add(this.Columns.Count, "", "", 100, null, null);
            }

            // If is valid 
            if (IsLoadedToClient)
            {
                ApplicationElement.RegisterChange(this, "$refresh");
            }
        }

        /// <summary>
        /// Notifies the update.
        /// </summary>
        /// <param name="listViewItem">The list view item.</param>
        /// <param name="subItem">The sub item.</param>
        /// <param name="propertyName">The property name.</param>
        internal void NotifyUpdate(ListViewItem listViewItem, ListViewSubitem subItem, string propertyName)
        {
            // Update items list
            _items.NotifyUpdate(listViewItem, subItem, propertyName);
        }


        /// <summary>
        /// Gets a value indicating whether has MouseLeave listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has MouseLeave listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool HasMouseLeaveListeners
        {
            get
            {
                if (this.HotTracking)
                {
                    return true;
                }
                return base.HasMouseLeaveListeners;
            }
        }

        /// <summary>
        /// Gets a value indicating whether has MouseEnter listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has MouseEnter listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool HasMouseEnterListeners
        {
            get
            {
                if (this.HotTracking)
                {
                    return true;
                }
                return base.HasMouseEnterListeners;
            }
        }

        /// <summary>
        /// MenuDisabled property . 
        /// True to disable the column header menu containing sort/hide options, otherwise false .
        /// </summary>
        [RendererPropertyDescription]
        public bool MenuDisabled
        {
            get { return _menuDisabled; }
            set
            {
                if (this._menuDisabled != value)
                {
                    this._menuDisabled = value;
                    OnPropertyChanged("MenuDisabled");
                }
            }
        }


        /// <summary>
        /// Finds the first ListViewItem, if indicated, that begins with the specified text value.
        /// </summary>
        /// <param name="text">The text to search for.</param>
        public ListViewItem FindItemWithText(string text)
        {
            if (this.Items != null)
            {
                if (this.Items.Count == 0)
                {
                    return null;
                }
                text = text.ToLowerInvariant();
                return this.Items.FirstOrDefault(item => item.Text != null && (item.Text.ToLowerInvariant().StartsWith(text) || item.Subitems.Any(subItem => subItem.Text!=null && subItem.Text.ToLowerInvariant().StartsWith(text))));
            }
            return null;
        }

        /// <summary>
        /// Finds the first ListViewItem or ListViewItem.ListViewSubItem, 
        /// if indicated, that begins with the specified text value. The search starts at the specified index.
        /// </summary>
        /// <param name="text">The text to search for.</param>
        /// <param name="includeSubItemsInSearch">true to include subitems in the search; otherwise, false.</param>
        /// <param name="startIndex">The index of the item at which to start the search.</param>
        public ListViewItem FindItemWithText(string text, bool includeSubItemsInSearch, int startIndex)
        {
            if (this.Items != null)
            {
                if (this.Items.Count == 0)
                {
                    return null;
                }
                if (this.Items.Count >= startIndex)
                {
                    text = text.ToLowerInvariant();
                    List<ListViewItem> items = this.Items.ToList().GetRange(startIndex, this.Items.Count - startIndex);
                    return items.FirstOrDefault(item => item.Text != null && (item.Text.StartsWith(text, StringComparison.OrdinalIgnoreCase) || (item.Subitems.Any(subItem => subItem.Text != null && subItem.Text.StartsWith(text, StringComparison.OrdinalIgnoreCase)) && includeSubItemsInSearch)));
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the first ListViewItem or ListViewItem.ListViewSubItem, 
        /// if indicated, that begins with the specified text value. The search starts at the specified index.
        /// </summary>
        /// <param name="text">The text to search for.</param>
        /// <param name="includeSubItemsInSearch">true to include subitems in the search; otherwise, false.</param>
        /// <param name="startIndex">The index of the item at which to start the search.</param>
        /// <param name="isPrefixSearch">true to allow partial matches; otherwise, false.</param>
        public ListViewItem FindItemWithText(string text, bool includeSubItemsInSearch, int startIndex, bool isPrefixSearch)
        {
            if (!isPrefixSearch)
            {
                if (this.Items != null)
                {
                    if (this.Items.Count == 0)
                    {
                        return null;
                    }
                    text = text.ToLowerInvariant();
                    if (this.Items.Count >= startIndex)
                    {
                        List<ListViewItem> items = this.Items.ToList().GetRange(startIndex, this.Items.Count - startIndex);
                        return items.FirstOrDefault(item => item.Text != null && (item.Text.ToLowerInvariant() == text || (item.Subitems.Any(subItem => subItem.Text != null && subItem.Text.ToLowerInvariant() == text) && includeSubItemsInSearch)));
                    }
                }
                return null;
            }
            return FindItemWithText(text, includeSubItemsInSearch, startIndex);
        }
    }
}
