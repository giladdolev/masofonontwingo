﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for dock panel items
    /// </summary>
    public abstract class DockPanelItemElement : PanelElement, ITVisualContainerElement
    {
        private ResourceReference mobjIcon;


        TVisualLayout ITVisualContainerElement.Layout
        {
            get { return TVisualLayout.Fit; }
        }



        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ResourceReference Icon
        {
            get { return mobjIcon; }
            set
            {
                mobjIcon = value;

                // Notify property Icon changed
                OnPropertyChanged("Icon");
            }
        }


        /// <summary>
        /// Closed event handler.
        /// </summary>
        private event EventHandler _Closed;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Closed action.
        /// </summary>
        public event EventHandler Closed
        {
            add
            {
                bool needNotification = (_Closed == null);

                _Closed += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Closed");
                }

            }
            remove
            {
                _Closed -= value;

                if (_Closed == null)
                {
                    OnEventHandlerDeattached("Closed");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Closed listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasClosedListeners
        {
            get { return _Closed != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Closed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Closed"/> instance containing the event data.</param>
        protected virtual void OnClosed(EventArgs args)
        {

            // Check if there are listeners.
            if (_Closed != null)
            {
                _Closed(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Closed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformClosed(EventArgs args)
        {
            // Get the parent element
            ControlElement parent = this.Parent;

            // If there is a valid parent element
            if (parent != null)
            {
                // Remove this control from parent
                parent.Controls.Remove(this);
            }

            // Raise the Closed event.
            OnClosed(args);
        }




        /// <summary>
        /// Selects this instance.
        /// </summary>
        public void Select()
        {
            DockPanelElement _DockPanelElement = this.Parent as DockPanelElement;
            if (_DockPanelElement != null)
            {
                _DockPanelElement.SelectedPanel = this;
            }
        }
    }
}
