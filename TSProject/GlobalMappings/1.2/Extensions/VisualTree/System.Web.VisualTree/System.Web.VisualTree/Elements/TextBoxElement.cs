using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a System.Web.VisualTree text box control.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.TextBoxBaseElement" />
	public class TextBoxElement : TextBoxBaseElement
	{
		private bool _allowBlank = true;
        private string _passwordChar = "";
        private string _inputType = "number";

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxElement"/> class.
        /// </summary>
        public TextBoxElement()
		{
            ClientDirtychange = "if(this.isDirtyValue == false ){return false;}else{this.isDirtyValue = false;}";
		}



		/// <summary>
		/// Gets or sets a value indicating whether this textbox allows blank
		/// </summary>
		[DefaultValue(true)]
        [RendererPropertyDescription]
        [Category("Behavior")]
        public bool AllowBlank
		{
			get { return _allowBlank; }
			set { _allowBlank = value; }
		}



        /// <summary>
        /// Gets or sets the field label.
        /// </summary>
        /// <value>
        /// The field label.
        /// </value>
        [DesignerIgnore]
        public string FieldLabel
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets a value indicating whether pressing ENTER in a multiline System.Windows.Forms.TextBox
        ///     control creates a new line of text in the control or activates the default
        ///     button for the form.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the ENTER key creates a new line of text in a multiline version of
        ///     the control; <c>false</c> if the ENTER key activates the default button for the
        ///     form. The default is <c>false</c>.
        /// </value>
		public bool AcceptsReturn
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether the text in the T:System.Web.VisualTree.Elements.TextBoxElement control should appear as the default password character.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the text in the T:System.Web.VisualTree.Elements.TextBoxElement control should appear as the default password character;; otherwise, <c>false</c>.
        /// </value>
		public bool UseSystemPasswordChar
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the scroll bars.
        /// </summary>
        /// <value>
        /// The scroll bars.
        /// </value>
		public ScrollBars ScrollBars
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the character used to mask characters of a password in a single-line T:System.Web.VisualTree.Elements.TextBoxElement control.
        /// </summary>
        /// <value>
        /// The password character.
        /// </value>
	    [DefaultValue("")]
	    public string PasswordChar
	    {
	        get { return _passwordChar; }
	        set { _passwordChar = value; }
	    }

        /// <summary>
        /// Gets or sets the InputType.
        /// </summary>
        /// <value>
        /// The Input Type.
        /// </value>
        public string InputType
        {
            get { return _inputType; }
            set { _inputType = value; }
        }


        private ImeMode _ImeMode = ImeMode.On;
        /// <summary>
        /// Gets or sets the ImeMode.
        /// </summary>
        /// <value>
        /// The ImeMode.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(ImeMode.On)]
        [Category("Behavior")]
        public ImeMode ImeMode
        {
            get { return _ImeMode; }
            set
            {
                if (_ImeMode != value)
                {
                    _ImeMode = value;
                }
            }
        }


        private HorizontalAlignment _TextAlign = HorizontalAlignment.Left;
        /// <summary>
        /// Gets or sets how text is aligned in a T:System.Web.VisualTree TextBox control.
        /// </summary>
        /// <value>
        /// The TextAlign.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(HorizontalAlignment.Left)]
        [Category("Appearance")]
        public HorizontalAlignment TextAlign
        {
            get { return _TextAlign; }
            set
            {
                if (_TextAlign != value)
                {
                    _TextAlign = value;

                    OnPropertyChanged("TextAlign");
                }
            }
        }



        /// <summary>
        /// Gets or sets the length of the sel.
        /// </summary>
        /// <value>
        /// The length of the sel.
        /// </value>
        public int SelLength
		{
			get;
			set;
		}



        public int SelStart
		{
			get;
			set;
		}



        public string SelText
		{
			get;
			set;
		}

        private CharacterCasing _characterCasing;
        /// <summary>
        /// Gets or sets the MyProperty.
        /// </summary>
        /// <value>
        /// The MyProperty.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public CharacterCasing CharacterCasing
        {
            get { return _characterCasing; }
            set
            {
                if (_characterCasing != value)
                {
                    _characterCasing = value;

                    OnPropertyChanged("CharacterCasing");
                }
            }
        }


        /// <summary>
        /// Dirtychange event handler.
        /// </summary>
        private event EventHandler<EventArgs> _dirtychange;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Dirtychange action.
        /// </summary>
        public event EventHandler<EventArgs> Dirtychange
        {
            add
            {
                bool needNotification = (_dirtychange == null);

                _dirtychange += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Dirtychange");
                }

            }
            remove
            {
                _dirtychange -= value;

                if (_dirtychange == null)
                {
                    OnEventHandlerDeattached("Dirtychange");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Dirtychange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Dirtychange"/> instance containing the event data.</param>
        protected virtual void OnDirtychange(EventArgs args)
        {

            // Check if there are listeners.
            if (_dirtychange != null)
            {
                _dirtychange(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Dirtychange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDirtychange(EventArgs args)
        {

            // Raise the Dirtychange event.
            OnDirtychange(args);
        }


        public string ClientDirtychange { get; set; }
	}
}
