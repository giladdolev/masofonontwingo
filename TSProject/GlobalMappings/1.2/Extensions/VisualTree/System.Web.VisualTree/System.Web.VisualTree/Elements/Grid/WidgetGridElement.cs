using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Web.VisualTree.Common.Attributes;

using System.Web.UI;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// Displays data in a customizable grid
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ScrollableControl" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModel" />
    public class WidgetGridElement : GridElement
    {
        private GridColumnCollection _widgetColumnCollection;


        public WidgetGridElement()
            : base()
        {
            _widgetColumnCollection = new GridColumnCollection(this);
        }





        [RendererMethodDescription]
        public void SetFocus(int rowindex, string datamember)
        {
            WidgetColumnData data = new WidgetColumnData() { SelectedRowIndex = rowindex, SelectedColumnIndex = 1, DataMember = datamember };
            this.InvokeMethod("SetFocus", data);
        }

        [RendererMethodDescription]
        public void SetEnabled(int rowindex, string datamember, bool state)
        {
            WidgetColumnData data = new WidgetColumnData() { SelectedRowIndex = rowindex, SelectedColumnIndex = 1, DataMember = datamember, Enabled = state };
            this.InvokeMethod("SetEnabled", data);
        }


        [RendererMethodDescription]
        public void SetBackColor(int rowindex, string datamember, Color color)
        {
            WidgetColumnData data = new WidgetColumnData() { SelectedRowIndex = rowindex, SelectedColumnIndex = 1, DataMember = datamember, BackColor = color };
            this.InvokeMethod("SetBackColor", data);
        }

        [RendererPropertyDescription]
        public string WidgetTemplate { get; set; }

        [RendererPropertyDescription]
        public string WidgetTemplateModel { get; set; }

        [RendererPropertyDescription]
        public int WidgetTemplateWidth { get; set; }

        [RendererPropertyDescription]
        public int WidgetTemplateItemsCount { get; set; }


        [RendererPropertyDescription]
        public string WidgetTemplateDataMembers { get; set; }

        [RendererPropertyDescription]
        public string WidgetTemplateDataAttachMethod { get; set; }


         /// <summary>
        /// CellkeyDown  event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _cellkeyDown;

        /// <summary>
        /// Add or remove CellkeyDown  action.
        /// </summary>
        [RendererEventDescription]
        public new event EventHandler<GridElementCellEventArgs> CellkeyDown
        {
            add
            {
                bool needNotification = (_cellkeyDown == null);

                _cellkeyDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CellkeyDown ");
                }

            }
            remove
            {
                _cellkeyDown -= value;

                if (_cellkeyDown == null)
                {
                    OnEventHandlerDeattached("CellkeyDown ");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CellkeyDown listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has CellkeyDown listeners; otherwise, <c>false</c>.
        /// </value>
        public bool HasCellkeyDownListeners
        {
            get { return _cellkeyDown != null; }
        }


        /// <summary>
        /// Raises the <see cref="E:CellkeyDown " /> event.
        /// </summary>
        /// <param name="args">The <see cref="CellkeyDown "/> instance containing the event data.</param>
        protected override void OnCellkeyDown(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_cellkeyDown != null)
            {
                _cellkeyDown(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CellkeyDown " /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public override void PerformCellkeyDown(GridElementCellEventArgs args)
        {
            if (args != null)
            {
                if (IsValidIndexes(args.ColumnIndex, args.RowIndex))
                {
                    _selectedColumnIndex = args.ColumnIndex;
                    _selectedRowIndex = args.RowIndex;
                }
            }
            // Raise the CellkeyDown  event.
            OnCellkeyDown(args);
        }

        /// <summary>
        /// Called when data source changed.
        /// </summary>
        /// <param name="args">The <see cref="BindingManagerChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnDataSourceChanged(BindingManagerChangedEventArgs args)
        {
            base.OnDataSourceChanged(args);

            // Always register WidgetBind method .
            ApplicationElement.RegisterChange(this, "WidgetBind");
        }

        [RendererMethodDescription]

        internal void RemoveFromViewModelContrlos(int index)
        {
            WidgetColumnData data = new WidgetColumnData() { SelectedRowIndex = index, SelectedColumnIndex = 1 };
            this.InvokeMethod("RemoveFromViewModelContrlos", data);
        }

        /// <summary>
        /// WidgetControlSpecialKey event handler.
        /// </summary>
        private event EventHandler<GridElementCellEventArgs> _widgetControlSpecialKey;

        /// <summary>
        /// Add or remove WidgetControlSpecialKey action. 
        /// This Action invoked on cell edit end, even if no changes to cell value were made.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridElementCellEventArgs> WidgetControlSpecialKey
        {
            add
            {
                bool needNotification = (_widgetControlSpecialKey == null);

                _widgetControlSpecialKey += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlSpecialKey");
                }

            }
            remove
            {
                _widgetControlSpecialKey -= value;

                if (_widgetControlSpecialKey == null)
                {
                    OnEventHandlerDeattached("WidgetControlSpecialKey");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:WidgetControlSpecialKey" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlSpecialKey"/> instance containing the event data.</param>
        protected virtual void OnWidgetControlSpecialKey(GridElementCellEventArgs args)
        {

            // Check if there are listeners.
            if (_widgetControlSpecialKey != null)
            {
                _widgetControlSpecialKey(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has cell end edit listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has cell end edit listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        internal bool HasWidgetControlSpecialKeyListeners
        {
            get { return _widgetControlSpecialKey != null; }
        }


        /// <summary>
        /// Performs the <see cref="E:WidgetControlSpecialKey" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GridElementCellEventArgs"/> instance containing the event data.</param>
        public void PerformWidgetControlSpecialKey(GridElementCellEventArgs args)
        {
            OnWidgetControlSpecialKey(args);
        }

    }
}