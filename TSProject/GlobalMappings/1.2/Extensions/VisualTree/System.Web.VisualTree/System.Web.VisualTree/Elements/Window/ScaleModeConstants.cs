﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    ///  Represents various units of measurement
    /// </summary>
    public enum ScaleModeConstants
    {
        /// <summary>
        /// User
        /// </summary>
        User,

        /// <summary>
        /// Twips
        /// </summary>
        Twips,

        /// <summary>
        /// Points
        /// </summary>
        Points,

        /// <summary>
        /// Pixels
        /// </summary>
        Pixels,

        /// <summary>
        /// Characters
        /// </summary>
        Characters,

        /// <summary>
        /// Inches
        /// </summary>
        Inches,

        /// <summary>
        /// Millimeters
        /// </summary>
        Millimeters,

        /// <summary>
        /// Centimeters
        /// </summary>
        Centimeters,

        /// <summary>
        /// PHimetric
        /// </summary>
        Himetric
    }
}
