using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ListBoxElement : ListControlElement
    {
        private DrawMode _drawMode = DrawMode.Normal;
        MultiSelectMode _selectionMode = MultiSelectMode.ListBoxStandard;


        [DefaultValue(DrawMode.Normal)]
        public virtual DrawMode DrawMode
        {
            get { return _drawMode; }
            set { _drawMode = value; }
        }



        public bool ScrollAlwaysVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the selected items.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        [DesignerIgnore]
        public ListItemCollection SelectedItems
        {
            get;
            set;
        }


        public bool HorizontalScrollBar
        {
            get;
            set;
        }



        public static Rectangle GetItemRectangle(int index)
        {
            return default(Rectangle);
        }


        

        /// <summary>
        /// Gets or sets the selection mode.
        /// </summary>
        /// <value>
        /// The selection mode.
        /// </value>
        [DefaultValue(System.Web.VisualTree.Elements.MultiSelectMode.ListBoxStandard)]
        public virtual MultiSelectMode SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                _selectionMode = value;

                // Notify property ScaleMode changed
                OnPropertyChanged("SelectionMode");
            }
        }

    }
}
