﻿namespace System.Web.VisualTree.Elements
{
    public enum ClipboardFormat
    {
        Bitmap,
        DIB,
        EMeafile,
        Files,
        Link,
        Metafile,
        Palette,
        RTF,
        Text


    }
}
