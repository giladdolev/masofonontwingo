using System.Drawing;

namespace System.Web.VisualTree.Elements
{
	public class ElementPosition : ChartItemElement
	{
        public ElementPosition()
        {
            Auto = true;
        }


		/// <summary>
		///Gets or sets a value that determines whether an applicable chart element will be positioned automatically by the T:System.Windows.Forms.DataVisualization.Charting.Chart control.
		/// </summary>
		public bool Auto
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the height of a chart element.
		/// </summary>
		public float Height
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the width of a chart element.
		/// </summary>
		public float Width
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the relative X-coordinate of the top-left corner of an applicable chart element.
		/// </summary>
		public float X
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the relative Y-coordinate of the top-left corner of an applicable chart element.
		/// </summary>
		public float Y
		{
			get;
			set;
		}        
	}
}
