﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DataViewDropDownField : DataViewField
    {

        /// <summary>
        /// The drop down control
        /// </summary>
        private ControlElement _dropDownControl = null;

        
        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewDropDownField"/> class.
        /// </summary>
        public DataViewDropDownField()
        {
            
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                // If there is a valid drop down control
                if (_dropDownControl != null)
                {
                    // Return the drop down control
                    yield return _dropDownControl;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        public override bool IsContainer
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The string visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            // If there is a valid drop down control
            if(_dropDownControl != null)
            {
                // Get typed drop down control
                TVisualElement visualElement = _dropDownControl as TVisualElement;

                // If there is a valid typed drop down control
                if (visualElement != null)
                {
                    // If there is a matching id
                    if (string.Equals(visualElement.ID, strVisualElementID, StringComparison.Ordinal))
                    {
                        // Return the visual element
                        return visualElement;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets or sets the drop down control.
        /// </summary>
        /// <value>
        /// The drop down control.
        /// </value>
        [RendererPropertyDescription]
        public ControlElement DropDownControl
        {
            get 
            { 
                return _dropDownControl; 
            }
            set 
            { 
                _dropDownControl = value; 
            }
        }

        
        private string _ValueMember;
        /// <summary>
        /// Gets or sets the ValueMember.
        /// </summary>
        /// <value>
        /// The ValueMember.
        /// </value>
        [RendererPropertyDescription]
        [Category("Data")]
        public string ValueMember
        {
            get { return _ValueMember; }
            set
            {
                if (_ValueMember != value)
                {
                    _ValueMember = value;

                    OnPropertyChanged("ValueMember");
                }
            }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Get list item
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid list item
            if (controlElement != null)
            {
                // Set control
                _dropDownControl = controlElement;

                // Set the parent element
                _dropDownControl.ParentElement = this;
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }
    }
}
