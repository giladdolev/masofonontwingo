﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridElementSelectionChangeEventArgs : CancelEventArgs
    {
        private IList<Int32> _selectedRows;

        private IList<GridIndexesData> _selectedCells;
        private GridRow selectedRows;
        private GridCellElement selectedCells;


        public IList<Int32> SelectedRows
        {
            get
            {
                return _selectedRows;
            }
        }

        public int RowIndex
        {
            get { return (this.SelectedRows != null && this.SelectedRows.Count > 0) ? this.SelectedRows[0] : -1; }
        }

        public int ColumnIndex
        {
            get { return (this.SelectedCells != null && this.SelectedCells.Count > 0) ? this.SelectedCells[0].ColumnIndex : -1; }
        }

        public IList<GridIndexesData> SelectedCells
        {
            get
            {
                return _selectedCells;
            }
        }

        public GridElementSelectionChangeEventArgs(IList<Int32> selectedRows, IList<GridIndexesData> selectedCells)
        {
            _selectedRows = selectedRows;
            _selectedCells = selectedCells;

        }

        public GridElementSelectionChangeEventArgs(GridRow selectedRows, GridCellElement selectedCells)
        {
            // TODO: Complete member initialization
            this.selectedRows = selectedRows;
            this.selectedCells = selectedCells;
        }

    }
}
