using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DateTimePickerElement : ControlElement
    {
        private bool _checked = true;
        private DateTimePickerFormat _format = DateTimePickerFormat.Default;

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        [DefaultValue(DateTimePickerFormat.Default)]
        [RendererPropertyDescription]
        public DateTimePickerFormat Format
       { 
            get { return _format; }
            set
            {
                _format = value;
                OnPropertyChanged("Format");
            }
        }

        private DateTime _value = DateTime.Now.Date;
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [RendererPropertyDescription]
        public DateTime Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
                OnPropertyChanged("Value");
            }
        }



        private string _CustomFormat;
        /// <summary>
        /// Gets or sets the custom format.
        /// </summary>
        /// <value>
        /// The custom format. 
        /// y = year, M = month, d = day, h = hour, m = minute, s = second. (case sensative) 
        /// </value>
        public string CustomFormat
        {
            get{
                return _CustomFormat;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _CustomFormat = "dd/MM/yyyy";
                }
                else
                {
                    _CustomFormat = value;
                }
                OnPropertyChanged("Format");
            }
        }
        

        private bool _ShowUpDown=false;

        /// <summary>
        /// When true - the picker turns to Up-Down spinner.
        /// The interval sets by <see cref="SpinInterval" />
        /// </summary>
        /// <value>
        ///   <c>true</c> if show up down; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool ShowUpDown
        {
            get
            {
                return _ShowUpDown;
            }
            set
            {
                if (_ShowUpDown != value)
                {
                    _ShowUpDown = value;

                    OnPropertyChanged("ShowUpDown");
                }
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DateTimePickerElement"/> is checked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool Checked
        {
            get { return _checked; }
            set { _checked = value; }
        }



        /// <summary>
        /// Gets or sets the maximum date.
        /// </summary>
        /// <value>
        /// The maximum date.
        /// </value>
        public DateTime MaxDate
        {
            get;
            set;
        }

        

        private DateInterval _SpinInterval = DateInterval.Day;

        /// <summary>
        /// Determine the interval when using Up-Down spinner<see cref="ShowUpDown"/>.
        /// </summary>
        [RendererPropertyDescription]
        public DateInterval SpinInterval
        {
            get
            {
                return _SpinInterval;
            }
            set
            {
                if (_SpinInterval != value)
                {
                    _SpinInterval = value;

                    OnPropertyChanged("SpinInterval");
                }
            }
        }




        /// <summary>
        /// Gets or sets the minimum date.
        /// </summary>
        /// <value>
        /// The minimum date.
        /// </value>
        public DateTime MinDate
        {
            get;
            set;
        }

        

        private bool _Editable=true;

        /// <summary>
        /// Decides whether to allow the user edit date from the input field - or only by the picker.
        /// </summary>
        /// <value>
        ///   <c>true</c> if editable; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool Editable
        {
            get
            {
                return _Editable;
            }
            set
            {
                if (_Editable != value)
                {
                    _Editable = value;

                    OnPropertyChanged("Editable");
                }
            }
        }



        /// <summary>
        /// Gets or sets a value indicating whether [show CheckBox].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show CheckBox]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowCheckBox
        {
            get;
            set;
        }


        /// <summary>
        /// ValueChanged event handler.
        /// </summary>
        private event EventHandler _ValueChanged;

        /// <summary>
        /// Add or remove ValueChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler ValueChanged
        {
            add
            {
                bool needNotification = (_ValueChanged == null);

                _ValueChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ValueChanged");
                }

            }
            remove
            {
                _ValueChanged -= value;

                if (_ValueChanged == null)
                {
                    OnEventHandlerDeattached("ValueChanged");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ValueChanged"/> instance containing the event data.</param>
        protected virtual void OnValueChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_ValueChanged != null)
            {
                _ValueChanged(this, args);
            }
        }

        
        /// <summary>
        /// Performs the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformValueChanged(EventArgs args)
        {
            // Try to get string value changed arguments
            ValueChangedArgs<string> valueChangedValueChangedArgs = args as ValueChangedArgs<string>;

            // If there is a valid string value changed arguments
            if (valueChangedValueChangedArgs != null)
            {
                try
                {
                    if(!string.IsNullOrWhiteSpace(this.CustomFormat))
                    {
                        CultureInfo en_us = new CultureInfo("en-US"); //try to get the date with 'en_US' CultureInfo. 
                        // Set the new date value
                        this.Value = DateTime.ParseExact(valueChangedValueChangedArgs.Value, this.CustomFormat, en_us);
                    }
                    this.Value = DateTime.Parse(valueChangedValueChangedArgs.Value);
                }
                catch(Exception ex)
                {
                    DateTime.TryParseExact(valueChangedValueChangedArgs.Value, this.CustomFormat,CultureInfo.CurrentUICulture, DateTimeStyles.AdjustToUniversal, out this._value); //if it's the wrong culture, it's trying with the system CultureInfo.   
                }
            }
            // Raise the SelectionChangeCommitted event.
            OnValueChanged(args);
        }



        /// <summary>
        /// Gets or sets the close up action.
        /// </summary>
        /// <value>
        /// The close up action.
        /// </value>
        public event EventHandler CloseUp;



        /// <summary>
        /// Raises the <see cref="E:CloseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CloseUp"/> instance containing the event data.</param>
        protected virtual void OnCloseUp(EventArgs args)
        {
            // Check if there are listeners.
            if (CloseUp != null)
            {
                CloseUp(this, args);
            }
        }



        /// <summary>
        /// Performs the <see cref="E:CloseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCloseUp(EventArgs args)
        {
            // Raise the CloseUp event.
            OnCloseUp(args);
        }


        private Color _monthBackground;
        /// <summary>
        /// Gets or sets the month background.
        /// </summary>
        /// <value>
        /// The month background.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public Color MonthBackground
        {
            get { return _monthBackground; }
            set
            {
                if (_monthBackground != value)
                {
                    _monthBackground = value;

                    OnPropertyChanged("MonthBackground");
                }
            }
        }

    }
}


