﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.GridColumn" />
    public class GridRowHeader : GridColumn
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GridRowHeader"/> class.
        /// </summary>
        public GridRowHeader()
        {
            ReadOnly = true;
        }
        private bool _showRowNumber = false;

        /// <summary>
        /// Get , set ShowRowNumber property .
        /// </summary>
        /// <value>
        /// true to show row number , otherwise false .
        /// </value>
        [RendererPropertyDescription]
        public bool ShowRowNumber
        {
            get
            {
                return _showRowNumber;
            }
            set
            {
                if (_showRowNumber != value)
                {
                    _showRowNumber = value;
                    OnPropertyChanged("ShowRowNumber");
                }
            }
        }
    }
}
