using System.ComponentModel;
using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public class ImageElement : ControlElement
	{
        private PictureBoxSizeMode _sizeMode = PictureBoxSizeMode.Normal;
        private ResourceReference _image;


        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        [DesignerName("ImageReference")]
        [RendererPropertyDescription]
        public ResourceReference Image
        {
            get { return _image; }
            set
            {
                _image = value;
                // Notify property Image changed
                OnPropertyChanged("Image");
            }
        }



        /// <summary>
        /// Gets or sets the initial image.
        /// </summary>
        /// <value>
        /// The initial image.
        /// </value>
        public Image InitialImage
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the size mode.
        /// </summary>
        /// <value>
        /// The size mode.
        /// </value>
        [DefaultValue(PictureBoxSizeMode.Normal)]
	    public PictureBoxSizeMode SizeMode
	    {
	        get { return _sizeMode; }
	        set { _sizeMode = value; }
	    }



	    public void Cls()
	    {
	        Image = null;
	    }
	}
}
