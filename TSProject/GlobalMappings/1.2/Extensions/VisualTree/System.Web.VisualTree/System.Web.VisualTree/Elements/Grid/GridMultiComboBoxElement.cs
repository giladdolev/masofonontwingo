﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridMultiComboBoxElement : ComboBoxElement
    {

        public static string GetComboBoxData(string selectedTxt, DataTable dataTable)
        {
          //  var strData = "Ext.create('Ext.data.Store', {storeId: 'vtgridstore',fields:[ 'value', 'text'],data:";


           var strData = "[";
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                strData += "{";
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    if (j > 0)
                    {
                        strData += ",";
                    }
                    strData += '\"' + dataTable.Columns[j].ColumnName + '\"' + ":";
                    strData += '\"' + Convert.ToString(dataTable.Rows[i][j]) + '\"';
                }
                strData += "}";
                if (i + 1 < dataTable.Rows.Count)
                {
                    strData += ",";
                }
            }
            strData += "]";
           // strData += "});";
            return strData;
        }


    }
}
