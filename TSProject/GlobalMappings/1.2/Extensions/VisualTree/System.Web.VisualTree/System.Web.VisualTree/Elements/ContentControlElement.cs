﻿namespace System.Web.VisualTree.Elements
{
    public abstract class ContentControlElement : ControlElement
    {
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public virtual object Content
        {
            get;
            set;
        }
    }
}
