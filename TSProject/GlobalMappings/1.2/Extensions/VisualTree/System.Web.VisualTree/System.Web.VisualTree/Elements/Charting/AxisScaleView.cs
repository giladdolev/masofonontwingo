namespace System.Web.VisualTree.Elements
{
	public class AxisScaleView : VisualElement
	{


	
		/// <summary>
		///Gets or sets the minimum size of the T:System.Windows.Forms.DataVisualization.Charting.AxisScaleView object.
		/// </summary>
		public double MinSize
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the position of the scale view.
		/// </summary>
		public double Position
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the size of the scale view.
		/// </summary>
		public double Size
		{
			get;
			set;
		}
	

		/// <summary>
		///Gets or sets a flag that indicates whether the zooming user interface is enabled.
		/// </summary>
		public bool Zoomable
		{
			get;
			set;
		}
	}
}
