﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;


namespace System.Web.VisualTree.Elements
{
    public class LinkLabelElement : LabelElement
    {

        public LinkLabelElement()
        {
        }


        public string LinkData
        {
            get;
            set;
        }
    }
}
