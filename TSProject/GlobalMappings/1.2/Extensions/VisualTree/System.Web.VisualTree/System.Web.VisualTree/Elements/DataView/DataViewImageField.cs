﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree.Elements
{
    public class DataViewImageField : DataViewField
    {
        private ResourceReference _image;

        [DesignerName("ImageReference")]
        [RendererPropertyDescription]
        [Category("Behavior")]
        public ResourceReference Image
        {
            get { return _image; }
            set
            {
                _image = value;
                // Notify property Image changed
                OnPropertyChanged("Image");
            }
        }
    }
}
