﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// GridColumnData data to store previousDisplayIndex , newDisplayIndex .
    /// </summary>
    public class GridColumnData
    {

      //  private int _displayIndex = -1;
        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData" /> class.
        /// </summary>
        /// <param name="previousDisplayIndex">previous DisplayIndex.</param>
        /// <param name="newDisplayIndex">New index of the display.</param>
        public GridColumnData(int previousDisplayIndex, int newDisplayIndex)
        {
            PreviousDisplayIndex = previousDisplayIndex;
            NewDisplayIndex = newDisplayIndex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData"/> class.
        /// </summary>
        public GridColumnData()
        {
        }

        /// <summary>
        /// Get,Set the PreviousDisplayIndex.
        /// </summary>
        public int PreviousDisplayIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the NewDisplayIndex.
        /// </summary>
        public int NewDisplayIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the Column DataIndex.
        /// </summary>
        public string ColumnDataIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Get,Set the Column SortOrder.
        /// </summary>
        public SortOrder ColumnSortOrder
        {
            get;
            set;
        }

        /// <summary>
        /// Get , set HeaderCellIcon .
        /// </summary>
        public string HeaderCellIcon
        {
            get;
            set;
        }
    }
}
