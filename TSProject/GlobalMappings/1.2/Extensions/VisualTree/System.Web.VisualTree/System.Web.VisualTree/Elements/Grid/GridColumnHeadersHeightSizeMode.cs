﻿namespace System.Web.VisualTree.Elements
{
    public enum GridColumnHeadersHeightSizeMode
    {
        /// <summary>Users can adjust the column header height with the mouse.</summary>
        EnableResizing,
        /// <summary>Users cannot adjust the column header height with the mouse.</summary>
        DisableResizing,
        /// <summary>The column header height adjusts to fit the contents of all the column header cells. </summary>
        AutoSize
    }
}
