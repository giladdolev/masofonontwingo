﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// Provides support for view model binding
    /// </summary>
    internal class BindingViewModelManager : BindingManager
    {

        /// <summary>
        /// Provides support for enumerator data reader
        /// </summary>
        private class BindingViewModelDataReader : BindingDataReader
        {
            /// <summary>
            /// The enumerator
            /// </summary>
            private readonly IEnumerator _enumerator;


            /// <summary>
            /// Initializes a new instance of the <see cref="BindingViewModelDataReader" /> class.
            /// </summary>
            /// <param name="enumerator">The enumerator.</param>
            /// <param name="columns">The columns.</param>
            public BindingViewModelDataReader(IEnumerator enumerator, IBindingDataColumn[] columns)
                : base(columns)
            {
                _enumerator = enumerator;
            }

            /// <summary>
            /// Gets the current item identifier.
            /// </summary>
            /// <value>
            /// The current item identifier.
            /// </value>
            public override object CurrentItemId
            {
                get
                {
                    // Get the visual element
                    VisualElement visualElement = this.CurrentItem as VisualElement;

                    // If there is a valid visual element
                    if (visualElement != null)
                    {
                        // Return the client id
                        return visualElement.ClientID;
                    }

                    return null;
                }
            }

            /// <summary>
            /// Gets the current item.
            /// </summary>
            /// <value>
            /// The current item.
            /// </value>
            protected override object CurrentItem
            {
                get
                {
                    return _enumerator.Current;
                }
            }

            /// <summary>
            /// Advances the <see cref="T:System.Data.IDataReader" /> to the next record.
            /// </summary>
            /// <returns>
            /// true if there are more rows; otherwise, false.
            /// </returns>
            public override bool Read()
            {
                return _enumerator.MoveNext();
            }
        }

        /// <summary>
        /// The data source
        /// </summary>
        private object _dataSource = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingViewModelManager"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public BindingViewModelManager(VisualElement visualElement)
            : base(visualElement)
        {

        }

        /// <summary>
        /// Gets a value indicating whether this instance is view model.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is view model; otherwise, <c>false</c>.
        /// </value>
        public override bool IsViewModel
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Clears the changes.
        /// </summary>
        public override void ClearChanges()
        {
            ClearChanges(this.Columns);
            ClearChanges(this.Items);
        }

        /// <summary>
        /// Clears the changes.
        /// </summary>
        /// <param name="items">The items.</param>
        private void ClearChanges(IEnumerable items)
        {
            IVisualElementCollectionChangeActionsContainer itemsChangeActionsContainer = items as IVisualElementCollectionChangeActionsContainer;
            if (itemsChangeActionsContainer != null)
            {
                itemsChangeActionsContainer.ClearChangeActions();
            }
        }

        /// <summary>
        /// Creates the reader.
        /// </summary>
        /// <returns></returns>
        public override IDataReader CreateReader(IEnumerable items = null)
        {
            // If we have external items list
            if (items != null)
            {
                // Return enumerator data reader
                return new BindingViewModelDataReader(items.GetEnumerator(), this.Columns.ToArray());
            }
            else
            {
                // Return enumerator data reader
                return new BindingViewModelDataReader(this.Items.GetEnumerator(), this.Columns.ToArray());
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>
        public override object DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                _dataSource = value;

                // Get the binding view model
                IBindingViewModel bindingViewModel = this.ParentElement as IBindingViewModel;

                // If there is a valid view model 
                // and no need to update the client .
                if (bindingViewModel != null && this.ActionType != OperationType.Update)
                {
                    // Bind view model
                    bindingViewModel.Bind(BindingDataReader.Create(_dataSource));
                }
            }
        }



        /// <summary>
        /// Register the used column.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <returns></returns>
        internal override IBindingDataColumn RegisterUsedColumn(string columnName)
        {
            return null;
        }

        /// <summary>
        /// Gets the item by item identifier.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns></returns>
        public override object GetItemByItemId(object itemId)
        {
            IEnumerable<IBindingViewModelRecord> itemsChangeActionsContainer = this.Items as IEnumerable<IBindingViewModelRecord>;
            string id = itemId.ToString();
            return itemsChangeActionsContainer.FirstOrDefault(item => item.ClientID == id);
        }

        /// <summary>
        /// Resets the has index columns.
        /// </summary>
        internal override void ResetHasIndexColumns()
        {

        }

        /// <summary>
        /// Gets a value indicating whether this instance has index columns.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has index columns; otherwise, <c>false</c>.
        /// </value>
        public override bool HasIndexColumns
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the view model.
        /// </summary>
        /// <returns></returns>
        private IBindingViewModel GetViewModel()
        {
            return this.ParentElement as IBindingViewModel;
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public override IEnumerable<IBindingDataColumn> Columns
        {
            get
            {
                // Get the view model
                IBindingViewModel viewModel = this.GetViewModel();

                // If there is a valid view model
                if (viewModel != null)
                {
                    // Get columns
                    IEnumerable<IBindingDataColumn> columns = viewModel.Columns;

                    // If there are valid columns
                    if (columns != null)
                    {
                        // Return columns
                        return columns;
                    }
                }

                return BindingDataColumn.EmptyArray;
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public override IEnumerable Items
        {
            get
            {
                // Get the view model
                IBindingViewModel viewModel = this.GetViewModel();

                // If there is a valid view model
                if (viewModel != null)
                {
                    // Get the items
                    IEnumerable<IBindingViewModelRecord> items = viewModel.Records;

                    // If there is a valid items list
                    if (items != null)
                    {
                        // Return items
                        return items;
                    }
                }

                return new IBindingViewModelRecord[] { };
            }
        }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        /// <value>
        /// The item type.
        /// </value>
        public override Type ItemType
        {
            get
            {
                return typeof(object);
            }
        }
    }


}
