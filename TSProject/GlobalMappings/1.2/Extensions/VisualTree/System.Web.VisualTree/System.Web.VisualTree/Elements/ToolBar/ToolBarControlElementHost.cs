﻿using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    [DesignerIgnore]
    public class ToolBarControlElementHost : ToolBarItem
    {
        /// <summary>
        /// The hosted control
        /// </summary>
        private readonly ControlElement _control = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarControlElementHost"/> class.
        /// </summary>
        /// <param name="control">The control.</param>
        public ToolBarControlElementHost(ControlElement control)
        {
            _control = control;
        }



        /// <summary>
        /// Gets the control.
        /// </summary>
        /// <value>
        /// The control.
        /// </value>
        public ControlElement Control
        {
            get { return _control; }
        } 
    }
}
