namespace System.Web.VisualTree.Elements
{
	public enum GridContentAlignment
	{
		/// <summary>The alignment is not set.</summary>
		NotSet,
		/// <summary>The content is aligned vertically at the top and horizontally at the left of a cell.</summary>
		TopLeft,
		/// <summary>The content is aligned vertically at the top and horizontally at the center of a cell.</summary>
		TopCenter,
		/// <summary>The content is aligned vertically at the top and horizontally at the right of a cell.</summary>
		TopRight ,
		/// <summary>The content is aligned vertically at the middle and horizontally at the left of a cell.</summary>
		MiddleLeft ,
		/// <summary>The content is aligned at the vertical and horizontal center of a cell.</summary>
		MiddleCenter,
		/// <summary>The content is aligned vertically at the middle and horizontally at the right of a cell.</summary>
		MiddleRight ,
		/// <summary>The content is aligned vertically at the bottom and horizontally at the left of a cell.</summary>
		BottomLeft,
		/// <summary>The content is aligned vertically at the bottom and horizontally at the center of a cell.</summary>
		BottomCenter ,
		/// <summary>The content is aligned vertically at the bottom and horizontally at the right of a cell.</summary>
		BottomRight
	}
}
