namespace System.Web.VisualTree.Elements
{
	public class TextAnnotation : ChartAnnotation
	{
		/// <summary>
		///Gets or sets the text of the annotation.
		/// </summary>
		public virtual string Text
		{
			get;
			set;
		}
	}
}
