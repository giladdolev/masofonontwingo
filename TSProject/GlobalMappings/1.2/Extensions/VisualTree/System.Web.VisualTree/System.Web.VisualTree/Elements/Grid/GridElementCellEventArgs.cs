﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for grid element cell related events
    /// </summary>
    public class GridElementCellEventArgs : KeyEventArgs
    {

        /// <summary>
        /// The column index.
        /// </summary>
        private int _columnIndex = -1;

        /// <summary>
        /// The row index.
        /// </summary>
        private int _rowIndex = -1;

        /// <summary>
        /// 
        /// </summary>
        private int _lastColumnIndex = -1;

        /// <summary>
        /// The first column index. 
        /// </summary>
        private int _firstColumnIndex = -1;

        /// <summary>
        /// The first row index.
        /// </summary>
        private int _firstRowIndex = -1;

        /// <summary>
        /// The last row index.
        /// </summary>
        private int _lastRowIndex = -1;
    //    private Keys keys;

        /// <summary>
        /// Gets the first index of the row.
        /// </summary>
        /// <value>
        /// The first index of the row.
        /// </value>
        public int FirstRowIndex
        {
            get { return _firstRowIndex; }
            private set
            {
                if (_firstRowIndex != value)
                {
                    _firstRowIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets the first index of the column.
        /// </summary>
        /// <value>
        /// The first index of the column.
        /// </value>
        public int FirstColumnIndex
        {
            get { return _firstColumnIndex; }
            private set
            {
                if (_firstColumnIndex != value)
                {
                    _firstColumnIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets the last index of the row.
        /// </summary>
        /// <value>
        /// The last index of the row.
        /// </value>
        public int LastRowIndex
        {
            get { return _lastRowIndex; }
            private set
            {
                if (_lastRowIndex != value)
                {
                    _lastRowIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets the last index of the column.
        /// </summary>
        /// <value>
        /// The last index of the column.
        /// </value>
        public int LastColumnIndex
        {
            get { return _lastColumnIndex; }
            private set
            {
                if (_lastColumnIndex != value)
                {
                    _lastColumnIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int ColumnIndex
        {
            get
            {
                return _columnIndex;
            }
            private set
            {
                _columnIndex = value;
            }
        }

        public string WidgetDataMember { get; set; }

        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex
        {
            get
            {
                return _rowIndex;
            }
            private set
            {
                _rowIndex = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs"/> class.
        /// </summary>
        public GridElementCellEventArgs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs" /> class.
        /// </summary>
        /// <param name="rowindex">The rowindex.</param>
        public GridElementCellEventArgs(int rowindex)
        {
            _rowIndex = rowindex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs" /> class.
        /// </summary>
        /// <param name="keyData">The key data.</param>
        /// <param name="cellIndex">The cell index.</param>
        /// <param name="rowIndex">The last row index.</param>
        public GridElementCellEventArgs(Keys keyData, int cellIndex, int rowIndex)
        {
            _keyData = keyData;
            _columnIndex = cellIndex;
            _rowIndex = rowIndex;

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs"/> class.
        /// </summary>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="rowIndex">Index of the row.</param>
        public GridElementCellEventArgs(int columnIndex, int rowIndex)
        {
            ColumnIndex = columnIndex;
            RowIndex = rowIndex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs" /> class.
        /// </summary>
        /// <param name="firstRowIndex">First index of the row.</param>
        /// <param name="firstColumnIndex">First index of the column.</param>
        /// <param name="lastRowIndex">Last index of the row.</param>
        /// <param name="lastColumnIndex">Last index of the column.</param>
        public GridElementCellEventArgs(int firstRowIndex, int firstColumnIndex, int lastRowIndex, int lastColumnIndex)
            : this(firstColumnIndex, firstRowIndex)
        {
            RowIndex = FirstRowIndex = firstRowIndex;
            ColumnIndex = FirstColumnIndex = firstColumnIndex;
            LastRowIndex = lastRowIndex;
            LastColumnIndex = lastColumnIndex;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridElementCellEventArgs"/> class.
        /// </summary>
        /// <param name="cellIndex">The cellIndex.</param>
        /// <param name="firstRowIndex">The first row index.</param>
        /// <param name="lastRowIndex">The last row index.</param>
        public GridElementCellEventArgs(int cellIndex, int firstRowIndex, int lastRowIndex)
        {
            if (cellIndex == 0)
            {
                FirstRowIndex = firstRowIndex;
                LastRowIndex = lastRowIndex;
            }
        }

        public GridElementCellEventArgs(Keys keyData, int cellIndex, int rowIndex, string datamember):this(keyData,cellIndex,rowIndex)
        {
            this.WidgetDataMember = datamember;
        }

    }
}
