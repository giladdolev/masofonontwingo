﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class GridCheckBoxColumn : GridColumn
    {

        private Object _falseValue = false;
        private Object _trueValue = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn"/> class.
        /// </summary>
        public GridCheckBoxColumn()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridCheckBoxColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridCheckBoxColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(bool))
        {
        }

        /// <summary>
        /// Sets the false value.
        /// </summary>
        /// <value>
        /// The false value.
        /// </value>
        public object FalseValue
        {
            set
            {
                _falseValue = value;
            }
        }

        /// <summary>
        /// Sets the true value.
        /// </summary>
        /// <value>
        /// The true value.
        /// </value>
        public object TrueValue
        {
            set
            {
                _trueValue = value;
            }
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        [DesignerIgnore]
        public override string DataTypeName
        {
            get { return typeof(bool).Name; ; }
        }

        /// <summary>
        /// 
        /// </summary>
        public CheckState CheckState
        {
            get;
            set;
        }

        /// <summary>
        /// CheckedChange event handler.
        /// </summary>
        private event EventHandler<EventArgs> _CheckedChange;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove CheckedChange action.
        /// </summary>
        public event EventHandler<EventArgs> CheckedChange
        {
            add
            {
                bool needNotification = (_CheckedChange == null);

                _CheckedChange += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CheckedChange");
                }

            }
            remove
            {
                _CheckedChange -= value;

                if (_CheckedChange == null)
                {
                    OnEventHandlerDeattached("CheckedChange");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:CheckedChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CheckedChange"/> instance containing the event data.</param>
        protected virtual void OnCheckedChange(EventArgs args)
        {

            // Check if there are listeners.
            if (_CheckedChange != null)
            {
                _CheckedChange(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CheckedChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCheckedChange(EventArgs args)
        {
            GridElement grid = this.Parent as GridElement;
            if (grid != null)
            {
                // Raise PerformBeforeChecked event .
                if (_BeforeChecked != null)
                {
                    CancelEventArgs _cancelEventArgs = new CancelEventArgs();
                    PerformBeforeChecked(_cancelEventArgs);
                }
            }
            // Raise the SelectionChanged event .
            List<int> lstRows = new List<int>() { grid.SelectedRowIndex };
            List<GridIndexesData> lstCells = new List<GridIndexesData>() { new GridIndexesData(grid.SelectedRowIndex, grid.SelectedColumnIndex) };
            GridElementSelectionChangeEventArgs newArgs = new GridElementSelectionChangeEventArgs(lstRows, lstCells);
            if (newArgs.RowIndex >= 0 && newArgs.RowIndex < grid.Rows.Count && newArgs.ColumnIndex >= 0 && newArgs.ColumnIndex < grid.Columns.Count)
            {
                string value = Convert.ToString((args as ValueChangedArgs<string>).Value);
                if (string.IsNullOrEmpty(value))
                {
                    grid.Rows[newArgs.RowIndex].Cells[newArgs.ColumnIndex].Value = "null";
                }
                else
                {
                    grid.Rows[newArgs.RowIndex].Cells[newArgs.ColumnIndex].Value = value.ToLowerInvariant();
                }
                grid.PerformSelectionChanged(newArgs);
                //Raise CheckedChange event .
                OnCheckedChange(args);

            }
        }

        /// <summary>
        /// BeforeChecked event handler.
        /// </summary>
        private event CancelEventHandler _BeforeChecked;

        /// <summary>
        /// Add or remove BeforeChecked action.
        /// </summary>
        public event CancelEventHandler BeforeChecked
        {
            add
            {
                bool needNotification = (_BeforeChecked == null);

                _BeforeChecked += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("BeforeChecked");
                }

            }
            remove
            {
                _BeforeChecked -= value;

                if (_BeforeChecked == null)
                {
                    OnEventHandlerDeattached("BeforeChecked");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:BeforeChecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="BeforeChecked"/> instance containing the event data.</param>
        protected virtual void OnBeforeChecked(CancelEventArgs args)
        {

            // Check if there are listeners.
            if (_BeforeChecked != null)
            {
                _BeforeChecked(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:BeforeChecked" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
        public void PerformBeforeChecked(CancelEventArgs args)
        {

            // Raise the BeforeChecked event.
            OnBeforeChecked(args);
        }


        private bool _threeState = false;
        /// <summary>
        /// Gets or sets a value indicating whether the CheckBox will allow three check states rather than two.
        /// </summary>
        [RendererPropertyDescription]
        public bool ThreeState
        {
            get { return _threeState; }
            set
            {
                _threeState = value;

                // Notify property CheckAlign changed
                OnPropertyChanged("ThreeState");
            }
        }
    }
}
