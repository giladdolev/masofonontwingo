﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides generic interface for implementing a window element.
    /// </summary>
    public interface IWindowElement : IRootElement
    {


        /// <summary>
        /// Creates the control.
        /// </summary>
        void CreateControl();

        /// <summary>
        /// Gets the dialog result.
        /// </summary>
        /// <value>
        /// The dialog result.
        /// </value>
        DialogResult DialogResult
        {
            get;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is created.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is created; otherwise, <c>false</c>.
        /// </value>
        bool IsCreated { get; set; }
    }
}
