namespace System.Web.VisualTree.Elements
{
	public class KeyPressEventArgs : KeyEventArgs
	{
        /// <summary>
        /// Keys the press event arguments.
        /// </summary>
        /// <param name="keys">The keys.</param>
		public KeyPressEventArgs(Keys keys)
            : base(keys)
		{
            
		}


        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPressEventArgs"/> class.
        /// </summary>
        /// <param name="intKeyCode">The int key code.</param>
        public KeyPressEventArgs(int intKeyCode)
            : this((Keys)intKeyCode)
        { }
	}
}
