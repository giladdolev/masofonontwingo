﻿namespace System.Web.VisualTree.Elements
{
    public enum GridAutoSizeColumnMode
    {
        /// <summary>The sizing behavior of the column is inherited from the <see cref="P:System.Windows.Forms.DataGridView.AutoSizeColumnsMode" /> property.</summary>
        NotSet,
        /// <summary>The column width does not automatically adjust.</summary>
        None,
        /// <summary>The column width adjusts to fit the contents of all cells in the column, including the header cell. </summary>
        AllCells,
        /// <summary>The column width adjusts to fit the contents of all cells in the column, excluding the header cell. </summary>
        AllCellsExceptHeader ,
        /// <summary>The column width adjusts to fit the contents of all cells in the column that are in rows currently displayed onscreen, including the header cell. </summary>
        DisplayedCells ,
        /// <summary>The column width adjusts to fit the contents of all cells in the column that are in rows currently displayed onscreen, excluding the header cell. </summary>
        DisplayedCellsExceptHeader ,
        /// <summary>The column width adjusts to fit the contents of the column header cell. </summary>
        ColumnHeader ,
        /// <summary>The column width adjusts so that the widths of all columns exactly fills the display area of the control, requiring horizontal scrolling only to keep column widths above the <see cref="P:System.Windows.Forms.DataGridViewColumn.MinimumWidth" /> property values. Relative column widths are determined by the relative <see cref="P:System.Windows.Forms.DataGridViewColumn.FillWeight" /> property values.</summary>
        Fill 
    }
}
