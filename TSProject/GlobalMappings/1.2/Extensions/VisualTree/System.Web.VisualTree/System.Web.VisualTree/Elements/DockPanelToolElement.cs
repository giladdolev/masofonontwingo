﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for docking panel tool
    /// </summary>
    public class DockPanelToolElement : DockPanelItemElement
    {
        public DockPanelToolElement()
        {

        }


        public DockPanelToolElement(string text)
        {
            Text = text;
        }


        [DesignerIgnore]
        public ControlElement RootElement { get; set; }
    }
}
