﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{

    /// <summary>
    /// Arguments for handling binding manager change event
    /// </summary>
    public class BindingManagerChangedEventArgs : EventArgs
    {
        /// <summary>
        /// The binding manager
        /// </summary>
        private readonly BindingManager _bindingManager;



        /// <summary>
        /// Initializes a new instance of the <see cref="BindingManagerChangedEventArgs"/> class.
        /// </summary>
        /// <param name="dataSourceElement">The binding manager.</param>
        public BindingManagerChangedEventArgs(BindingManager dataSourceElement)
        {
            _bindingManager = dataSourceElement;
        }

        /// <summary>
        /// Gets the binding manager.
        /// </summary>
        /// <value>
        /// The binding manager.
        /// </value>
        public BindingManager BidingManager
        {
            get { return _bindingManager; }
        }


    }
}
