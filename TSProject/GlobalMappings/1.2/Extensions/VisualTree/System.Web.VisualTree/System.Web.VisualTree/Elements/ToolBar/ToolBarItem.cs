using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ToolBarItem : ControlElement
    {
        private bool _autoToolTip = true;
        private ToolBarItemDisplayStyle _displayStyle = ToolBarItemDisplayStyle.ImageAndText;
        private string _text = string.Empty;
        private ToolBarItemAlignment _alignment = ToolBarItemAlignment.Left;
        private ContentAlignment _textAlign = ContentAlignment.MiddleRight;
        private TextImageRelation _textImageRelation = TextImageRelation.ImageBeforeText;
        private bool _dropDownArrows = false;
        private bool _rightToLeftAutoMirrorImage;
        private RightToLeft _rightToLeft = RightToLeft.Inherit;
        private ContentAlignment _imageAlign = ContentAlignment.MiddleRight;
        private ToolStripItemImageScaling _imageScaling = ToolStripItemImageScaling.SizeToFit;
		private readonly ToolBarItemCollection _items = null;


		/// <summary>
		/// Initializes a new instance of the <see cref="ListBoxElement"/> class.
		/// </summary>
        public ToolBarItem()
		{
            _items = new ToolBarItemCollection(this, "$refresh");
		}



        [DefaultValue(ToolBarItemDisplayStyle.ImageAndText)]
        public virtual ToolBarItemDisplayStyle DisplayStyle
        {
            get { return _displayStyle; }
            set { _displayStyle = value; }
        }



        [DefaultValue(true)]
        public bool AutoToolTip
        {
            get { return _autoToolTip; }
            set { _autoToolTip = value; }
        }


        [DefaultValue(ToolBarItemAlignment.Left)]
        public ToolBarItemAlignment Alignment
        {
            get { return _alignment; }
            set { _alignment = value; }
        }


        ResourceReference _image = new ResourceReference();
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        [RendererPropertyDescription]
        public virtual ResourceReference Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (_image != value)
                {
                    _image = value;
                    OnPropertyChanged("Image");
                }
            }
        }



        public Color ImageTransparentColor
        {
            get;
            set;
        }



        [DefaultValue(ContentAlignment.MiddleRight)]
        public virtual ContentAlignment TextAlign
        {
            get { return _textAlign; }
            set { _textAlign = value; }
        }



        public string AccessibleName
        {
            get;
            set;
        }


        [DefaultValue(ToolBarTextDirection.Horizontal)]
        public virtual ToolBarTextDirection TextDirection
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the TextImageRelation.
        /// </summary>
        /// <value>
        /// The TextImageRelation.
        /// </value>
        [DefaultValue(TextImageRelation.Overlay)]
        public TextImageRelation TextImageRelation
        {
            get { return _textImageRelation; }
            set 
            {
                if (_textImageRelation != value)
                {
                    _textImageRelation = value;
                    OnPropertyChanged("TextImageRelation");
                }                
            }
        }



        public PanelBevelConstants Bevel
        {
            get;
            set;
        }



        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get { return true; }
        }



        /// <summary>
        ///Gets or sets the index value of the image that is displayed on the item.
        /// </summary>
        public int ImageIndex
        {
            get;
            set;
        }



        /// <summary>
        ///Gets or sets the key accessor for the image in the P:System.Windows.Forms.ToolStrip.ImageList that is displayed on a T:System.Windows.Forms.ToolStripItem.
        /// </summary>
        public string ImageKey
        {
            get;
            set;
        }



        ///  <summary>
        /// Retrieves the T:System.Windows.Forms.ToolStrip that is the container of the current T:System.Windows.Forms.ToolStripItem.
        ///  </summary>
        /// <value>A T:System.Windows.Forms.ToolStrip that is the container of the current T:System.Windows.Forms.ToolStripItem.</value>
        [DesignerIgnore]
        public ToolBarElement CurrentParent
        {
            get { return default(ToolBarElement); }
        }



        /// <summary>
        ///Gets or sets the owner of this item.
        /// </summary>
        [DesignerIgnore]
        public ToolBarElement Owner
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets a value indicating whether drop-down buttons on a toolbar display down arrows.
        /// </summary>
        /// <value>
        ///   true if drop-down toolbar buttons display down arrows; otherwise, false. The default is false.
        /// </value>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool DropDownArrows
        {
            get { return this._dropDownArrows; }
            set
            {
                this._dropDownArrows = value;
                OnPropertyChanged("DropDownArrows");
            }
        }



        /// <summary>
        /// Mirrors automatically the <see cref="T:System.Web.VisualTree.Elements.ToolBarItem"/> image when the <see cref="P:System.Web.VisualTree.Elements.RightToLeft"/> property is set to <see cref="F:System.Web.VisualTree.Elements.Yes"/>.
        /// </summary>
        /// 
        /// <returns>
        /// true to automatically mirror the image; otherwise, false. The default is false.
        /// </returns>
        public bool RightToLeftAutoMirrorImage
        {
            get { return this._rightToLeftAutoMirrorImage; }
            set
            {
                if (_rightToLeft == RightToLeft.Yes || (CurrentParent != null && CurrentParent.RightToLeft == RightToLeft.Yes))
                {
                    this._rightToLeftAutoMirrorImage = value;
                    OnPropertyChanged("RightToLeftAutoMirrorImage");
                }
            }
        }

        /// <summary>
        /// Gets tree items.
        /// </summary>
        /// <value>
        /// The tree items.
        /// </value>  
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [DesignerType(typeof(IList<ToolBarItem>))]
        public ToolBarItemCollection Items
        {
            get { return _items; }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                if (_items != null)
                {
                    // Loop all columns
                    foreach (VisualElement items in _items)
                    {
                        // Return column
                        yield return items;
                    }
                }
            }
        }
        private ToolBarStatusLabelBorderSides _borderSides;
        /// <summary>
        /// Gets or sets the borderSides.
        /// </summary>
        /// <value>
        /// The borderSides.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public ToolBarStatusLabelBorderSides BorderSides
        {
            get { return _borderSides; }
            set
            {
                if (_borderSides != value)
                {
                    _borderSides = value;

                    OnPropertyChanged("BorderSides");
                }
            }
        }

        [DefaultValue(ContentAlignment.MiddleCenter)]
        public ContentAlignment ImageAlign
        {
            get { return _imageAlign; }
            set { _imageAlign = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether an image on a ToolStripItem is automatically resized to fit in a container.
        /// </summary>
        [RendererPropertyDescription]
        public ToolStripItemImageScaling ImageScaling
        {
            get { return _imageScaling; }
            set
            {
                if (_imageScaling != value)
                {
                    _imageScaling = value;
                    OnPropertyChanged("ImageScaling");
                }
            }
        }
    }
}
