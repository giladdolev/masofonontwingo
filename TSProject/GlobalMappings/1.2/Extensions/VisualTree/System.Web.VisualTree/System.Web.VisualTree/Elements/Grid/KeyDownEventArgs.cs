﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    public class KeyDownEventArgs : KeyEventArgs
    { 
        
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyDownEventArgs"/> class.
        /// </summary>
        public KeyDownEventArgs(Keys key):base(key)
        {
          
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyDownEventArgs"/> class.
        /// </summary>
        public KeyDownEventArgs()
        {
            KeyCode = -1;
            ColumnIndex = -1;
            RowIndex = -1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyDownEventArgs"/> class.
        /// </summary>
        public KeyDownEventArgs(int keyCode)
    {
            KeyCode = keyCode;
            ColumnIndex = -1;
            RowIndex = -1;
        }

        /// <summary>
        /// Gets or sets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex { get; set; }
        /// <summary>
        /// Gets or Sets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int ColumnIndex { get; set; }
        /// <summary>
        /// Gets or Sets the pressed key code.
        /// </summary>
        /// <value>
        /// The pressed key code.
        /// </value>
        public int KeyCode { get; set; }


    }
}
