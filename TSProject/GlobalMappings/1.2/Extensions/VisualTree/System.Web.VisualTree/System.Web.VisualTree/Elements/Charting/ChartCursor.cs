﻿using System.Drawing;

namespace System.Web.VisualTree.Elements
{
    public class ChartCursor : VisualElement
    {
        public Color SelectionColor { get; set; }

        public int LineWidth { get; set; }

        public ChartDashStyle LineDashStyle { get; set; }

        public Color LineColor { get; set; }

        public double SelectionStart { get; set; }

        public double SelectionEnd { get; set; }

 
        /// <summary>
        ///Gets or sets a flag that enables or disables the cursor user interface.
        /// </summary>
        public bool IsUserEnabled
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets a flag that enables or disables the range selection user interface.
        /// </summary>
        public bool IsUserSelectionEnabled
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets the position of a cursor.
        /// </summary>
        public double Position
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets the cursor interval.
        /// </summary>
        public double Interval
        {
            get;
            set;
        }
    }
}
