﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DataViewButtonField : DataViewLabelBaseField
    {
        
        private string _Text;
        /// <summary>
        /// Gets or sets the Text.
        /// </summary>
        /// <value>
        /// The Text.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        new public string Text
        {
            get { return _Text; }
            set
            {
                if (_Text != value)
                {
                    _Text = value;

                    OnPropertyChanged("Text");
                }
            }
        }

    }
}
