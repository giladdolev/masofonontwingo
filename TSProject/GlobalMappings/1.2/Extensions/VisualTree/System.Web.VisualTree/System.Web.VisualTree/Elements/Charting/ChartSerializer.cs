using System.Drawing;
using System.Globalization;
using System.Xml;

namespace System.Web.VisualTree.Elements
{
    public class ChartSerializer : VisualElement
    {
        public ChartSerializer()
        {
        }

        /// <summary>
        ///Loads serialized data saved to disk into the T:System.Windows.Forms.DataVisualization.Charting.Chart control.
        /// </summary>
        /// <param name="fileName">The relative or absolute path of the file used to store the serialized data. If a relative path is specified, the path will be relative to the current directory.</param>
        public void Load(string fileName)
        {
            Chart chart = ParentElement as Chart;
            if (chart != null)
            {
                chart.Series.Clear();
                chart.ChartAreas.Clear();

                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.Load(fileName);

                XmlNode chartNode = xmlDocument.SelectNodes("//Chart").Item(0);

                if (chartNode != null)
                {
                    foreach (XmlAttribute attribute in chartNode.Attributes)
                    {
                        if (attribute != null)
                        {
                            switch (attribute.Name)
                            {
                                case "Size":
                                    int[] size = DeserializeIntArray(attribute.Value);

                                    chart.Size = new Size(size[0], size[1]);
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode xmlNode in chartNode.ChildNodes)
                    {
                        switch (xmlNode.Name)
                        {
                            case "Series":
                                LoadSeriesList(xmlNode);
                                break;

                            case "ChartAreas":
                                LoadChartAreas(xmlNode);
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the series list.
        /// </summary>
        /// <param name="seriesListNode">The series list node.</param>
        private void LoadSeriesList(XmlNode seriesListNode)
        {
            Chart chart = ParentElement as Chart;
            if (chart != null)
            {
                foreach (XmlNode seriesNode in seriesListNode.ChildNodes)
                {
                    Series series = new Series();

                    foreach (XmlAttribute attribute in seriesNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "Name":
                                series.ID = attribute.Value;
                                break;

                            case "ChartType":
                                SeriesChartType type;

                                if (Enum.TryParse<SeriesChartType>(attribute.Value, out type))
                                {
                                    series.ChartType = type;
                                }
                                break;

                            case "ChartArea":
                                series.ChartAreaID = attribute.Value;
                                break;

                            case "Color":
                                series.Color = DeserializeColor(attribute.Value);
                                break;
                        }
                    }

                    foreach (XmlNode dataPointNode in seriesNode.SelectNodes("//Points//DataPoint"))
                    {
                        DataPoint dataPoint = new DataPoint();

                        foreach (XmlAttribute attribute in dataPointNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "XValue":
                                    dataPoint.XValue = double.Parse(attribute.Value, CultureInfo.InvariantCulture);
                                    break;

                                case "YValues":
                                    dataPoint.YValues = DeserializeDoubleArray(attribute.Value);
                                    break;

                                case "Color":
                                    dataPoint.Color = DeserializeColor(attribute.Value);
                                    break;
                            }
                        }

                        series.Points.Add(dataPoint);
                    }
                    chart.Series.Add(series);
                }
            }
        }

        /// <summary>
        /// Loads the chart areas.
        /// </summary>
        /// <param name="chartAreasNode">The chart areas node.</param>
        private void LoadChartAreas(XmlNode chartAreasNode)
        {
            Chart chart = ParentElement as Chart;

            if (chart != null)
            {
                foreach (XmlNode chartAreaNode in chartAreasNode.ChildNodes)
                {
                    ChartArea chartArea = new ChartArea();

                    foreach (XmlAttribute attribute in chartAreaNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "Name":
                                chartArea.ID = attribute.Value;
                                break;
                        }
                    }

                    foreach (XmlNode childNode in chartAreaNode.ChildNodes)
                    {
                        switch (childNode.Name)
                        {
                            case "AxisY":
                                Axis axisY = GetChartAreaAxis(childNode);

                                if (axisY != null)
                                {
                                    chartArea.AxisY = axisY;
                                }
                                break;

                            case "AxisX":
                                Axis axisX = GetChartAreaAxis(childNode);

                                if (axisX != null)
                                {
                                    chartArea.AxisX = axisX;
                                }
                                break;
                        }
                    }

                    chart.ChartAreas.Add(chartArea);
                }
            }
        }

        /// <summary>
        /// Gets the chart area axis.
        /// </summary>
        /// <param name="axisNode">The axis node.</param>
        /// <returns></returns>
        private static Axis GetChartAreaAxis(XmlNode axisNode)
        {
            Axis axis = new Axis();

            return axis;
        }

        /// <summary>
        /// De-serialize the int array.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        private static int[] DeserializeIntArray(string text, string separator = ",")
        {
            int[] values = new int[] { };

            if (!string.IsNullOrEmpty(text))
            {
                string[] textValues = text.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);

                values = new int[textValues.Length];

                for (int index = 0; index < values.Length; index++)
                {
                    int value;

                    if (int.TryParse(textValues[index], out value))
                    {
                        values[index] = value;
                    }
                }
            }

            return values;
        }

        /// <summary>
        /// De-serializes the double array.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="separator">The separator.</param>
        /// <returns></returns>
        private static double[] DeserializeDoubleArray(string text, string separator = ",")
        {
            double[] values = new double[] { };

            if (!string.IsNullOrEmpty(text))
            {
                string[] textValues = text.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);

                values = new double[textValues.Length];

                for (int index = 0; index < values.Length; index++)
                {
                    double value;

                    if (double.TryParse(textValues[index], out value))
                    {
                        values[index] = value;
                    }
                }
            }

            return values;
        }

        /// <summary>
        /// Deserializes the color.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static Color DeserializeColor(string text)
        {
            int[] colorRGB = DeserializeIntArray(text);

            if (colorRGB != null && colorRGB.Length > 0)
            {
                return Color.FromArgb(colorRGB[0], colorRGB[1], colorRGB[2]);
            }
            else
            {
                return Color.FromName(text);
            }

            return Color.Empty;
        }
    }
}
