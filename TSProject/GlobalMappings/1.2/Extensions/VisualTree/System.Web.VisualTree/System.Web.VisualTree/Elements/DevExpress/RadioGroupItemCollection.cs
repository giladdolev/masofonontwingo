﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class RadioGroupItemCollection : ControlElementCollection
    {
       
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlElementCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal RadioGroupItemCollection(RadioGroupElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public RadioGroupItemElement this[int index]
        {
            get
            {
                return base[index] as RadioGroupItemElement;
            }
            set
            {
                base[index] = value;
            }
        }

    }
}
