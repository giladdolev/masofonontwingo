﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum TreeElementStyles
    {
        PictureText,
        PlusMinusText,
        PlusPictureText,
        TextOnly,
        TreelinesPictureText,
        TreelinesPlusMinusPictureText,
        TreelinesPlusMinusText,
        TreelinesText
    }
}
