﻿using System.Collections.ObjectModel;

namespace System.Web.VisualTree.Elements
{
    public class FormCollection : Collection<WindowElement>
    {

        private readonly ApplicationElement _parentElement;


        public FormCollection(ApplicationElement objParentElement)
        {
            // Set the parent element
            _parentElement = objParentElement;
        }



        /// <summary>
        /// The parent element
        /// </summary>
        public ApplicationElement ParentElement
        {
            get { return _parentElement; }
        }



        public int Count()
        {
            return Count();
        }
    }
}
