﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{DataViewBand}" />
    public class DataViewBandCollection : VisualElementCollection<DataViewBand>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DataViewBandCollection" /> class.
        /// </summary>
        /// <param name="dataView">The data view.</param>
        /// <param name="propertyName">The property name.</param>
        public DataViewBandCollection(DataViewElement dataView, string propertyName)
            : base(dataView, propertyName)
        {

        }
    }
}
