namespace System.Web.VisualTree.Elements
{
	public class ArrangedItemCollection : VisualElement
	{
		public virtual int Count
		{
			get { return default(int); }
		}
	}
}
