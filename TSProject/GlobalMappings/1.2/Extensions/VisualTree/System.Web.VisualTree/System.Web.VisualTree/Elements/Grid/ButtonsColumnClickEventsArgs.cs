﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.VisualTree.Elements
{
    public class ButtonsColumnClickEventsArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public ButtonsColumnClickEventsArgs()
        {

        }

        public string ClickedButtonText
        {
            get;
            set;
        }

        public int RowIndex
        {
            get;
            set;
        }

        public int ColumnIndex
        {
            get;
            set;
        }
    }
}
