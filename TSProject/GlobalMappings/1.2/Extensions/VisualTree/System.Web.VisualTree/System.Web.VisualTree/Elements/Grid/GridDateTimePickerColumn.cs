﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;


namespace System.Web.VisualTree.Elements
{
    public class GridDateTimePickerColumn : GridColumn
    {
        private DateTimePickerFormat _format = DateTimePickerFormat.Default;
        /// <summary>
        /// Initializes a new instance of the <see cref="GridDateTimePickerColumn"/> class.
        /// </summary>
        public GridDateTimePickerColumn()
        {
            this.DataType = typeof(DateTime);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridDateTimePickerColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public GridDateTimePickerColumn(string columnName, string headerText)
            : base(columnName, headerText, typeof(DateTime))
        {
        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        [DesignerIgnore]
        public override string DataTypeName
        {
            get { return typeof(DateTime).Name; ; }
        }

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        [DefaultValue(DateTimePickerFormat.Default)]
        [RendererPropertyDescription]
        public DateTimePickerFormat Format
        {
            get { return _format; }
            set
            {
                _format = value;
                OnPropertyChanged("Format");
            }
        }

        /// <summary>
        /// Gets or sets the custom format.
        /// </summary>
        /// <value>
        /// The custom format.
        /// </value>
        public string CustomFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Format cell text .
        /// </summary>
        /// <returns></returns>
        public override string FormatValue(object value)
        {
            return Utilities.Utilities.ConvertDateToString(Convert.ToString(value), this.Format,this.CustomFormat);
        }
    }
}
