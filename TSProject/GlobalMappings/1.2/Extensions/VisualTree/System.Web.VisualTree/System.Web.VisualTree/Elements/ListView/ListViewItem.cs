using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.VisualElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.IBindingViewModelRecord" />
    [DesignerComponent]
    public class ListViewItem : VisualElement, IBindingViewModelRecord
    {
        private bool _selected;

        private bool _checked;

        private string _text = null;

        private object _tag;

        private readonly ListViewSubitemCollection _subitems = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItem"/> class.
        /// </summary>
        public ListViewItem()
        {
            _subitems = new ListViewSubitemCollection(this, "SubItems");
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewItem"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public ListViewItem(string text)
            : this()
        {
            Text = text;
        }



        /// <summary>
        /// Gets the default.
        /// </summary>
        /// <value>
        /// The default.
        /// </value>
        public string Default
        {
            get { return Text; }
        }


        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is checked.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is checked; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (_selected == value)
                {
                    return;
                }

                _selected = value;

                //Get list parent .
                ListViewElement parent = ListView;
                if (parent != null)
                {
                    if (parent.SelectedItems != null)
                    {
                        if (!parent.SelectedItems.Contains(this))
                        {
                            parent.SetItemState(this, _selected);
                        }
                    }
                }
            }
        }

        internal bool SelectedInternal
        {
            set
            {
                _selected = value;
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;

                    OnPropertyChanged("Text");

                    if (ListView != null)
                    {
                        ListView.Items.UpdateItem(this);
                    }
                }
            }
        }



        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        public object Tag
        {
            get { return _tag; }
            set
            {
                if (_tag != value)
                {
                    _tag = value;

                    OnPropertyChanged("Tag");
                }
            }
        }



        public Color BackColor
        {
            get;
            set;
        }



        public int ImageIndex
        {
            get;
            set;
        }



        public string Key
        {
            get;
            set;
        }



        /// <summary>
        /// Gets the subitems.
        /// </summary>
        /// <value>
        /// The subitems.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<ListViewSubitem>))]
        public ListViewSubitemCollection Subitems
        {
            get { return _subitems; }
        }

        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            ListViewSubitem listViewSubitem = visualElement as ListViewSubitem;
            if (listViewSubitem != null)
            {
                _subitems.Add(listViewSubitem);
            }
        }

        public bool UseItemStyleForSubitems
        {
            get;
            set;
        }




        /// <summary>
        /// Gets the ListView.
        /// </summary>
        /// <value>
        /// The ListView.
        /// </value>
        [DesignerIgnore]
        public ListViewElement ListView
        {
            get
            {
                return ParentElement as ListViewElement;
            }
        }



        public void Remove(int index)
        {
            ListViewSubitem ListViewSubItem = Subitems[index];
            Subitems.Remove(ListViewSubItem);
        }



        public string GetListViewSubitem(int index)
        {
            return Subitems[index].Text;
        }



        public void SetListViewSubitem(int index, string value)
        {
            while (index > Subitems.Count)
            {
                Subitems.Add(String.Empty);
            }
            Subitems[index - 1].Text = value;
        }



        public void EnsureVisible()
        {
            Index = 0;
        }

        /// <summary>
        /// Get list view item
        /// </summary>
        /// <param name="item">the item</param>
        /// <returns>list view item</returns>
        internal static ListViewItem GetItem(object item)
        {
            ListViewItem listItem = item as ListViewItem;
            if (listItem == null)
            {
                listItem = new ListViewItem(Convert.ToString(item));
            }
            return listItem;
        }

        /// <summary>
        /// Notifies the update.
        /// </summary>
        /// <param name="subItem">The sub item.</param>
        /// <param name="propertyName">The property name.</param>
        internal void NotifyUpdate(ListViewSubitem subItem, string propertyName)
        {
            // Get the list view
            ListViewElement listView = this.ListView;
            if (listView != null)
            {
                // Update item
                listView.NotifyUpdate(this, subItem, propertyName);
            }
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public IBindingViewModelField GetField(string key)
        {
            // List is not updateable at this point
            return null;
        }
    }
}
