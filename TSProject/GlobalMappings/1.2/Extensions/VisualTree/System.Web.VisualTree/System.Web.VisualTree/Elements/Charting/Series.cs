using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.DataPointCustomProperties" />
    [DesignerComponent]
    public class Series : DataPointCustomProperties
    {
        private readonly SeriesLabelStyle _labelStyle = null;
        private readonly DataPointCollection _points = null;
        private readonly SeriesLabelStyle _drawLinesToLabels;
        private string _title = null;

        SeriesChartType _chartType = SeriesChartType.Bar;

        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
        /// <param name="id">The series id.</param>
        public Series(string id)
            : this()
        {
            ID = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
        public Series()
        {
            _points = new DataPointCollection(this, "Points");
            _labelStyle = new SeriesLabelStyle();
            _labelStyle.ParentElement = this;

            _drawLinesToLabels = new SeriesLabelStyle();
            _drawLinesToLabels.ParentElement = this;

        }

        /// <summary>
        ///Gets or sets a flag that indicates whether the series will be visible on the rendered chart.
        /// </summary>
        public bool Enabled
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the chart type of a series. 
        /// </summary>
        public SeriesChartType ChartType
        {
            get { return _chartType; }
            set
            {
                if (_chartType != value)
                {
                    _chartType = value;

                    OnPropertyChanged("ChartType");
                }
            }
        }

        /// <summary>
        ///Gets or sets the name of the T:System.Windows.Forms.DataVisualization.Charting.ChartArea object used to plot the data series, if any.
        /// </summary>
        public string ChartAreaID
        {
            get;
            set;
        }

        public ChartSeriesCombineMode CombineMode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the chart area.
        /// </summary>
        /// <value>
        /// The chart area.
        /// </value>
        public ChartArea ChartArea
        {
            get
            {
                Chart chart = ParentElement as Chart;

                if (chart != null)
                {
                    foreach (ChartArea chartArea in chart.ChartAreas)
                    {
                        if (string.Equals(chartArea.ID, ChartAreaID))
                        {
                            return chartArea;
                        }
                    }

                    ChartArea temp = new ChartArea(ChartAreaID);
                    chart.ChartAreas.Add(temp);
                    return temp;
                }

                return null;
            }
        }

        /// <summary>
        ///Gets or sets the name of the series associated with the T:System.Windows.Forms.DataVisualization.Charting.Legend object.
        /// </summary>
        public string Legend
        {
            get;
            set;
        }

        /// <summary>
        ///Gets a T:System.Windows.Forms.DataVisualization.Charting.DataPointCollection object.
        /// </summary>
        public DataPointCollection Points
        {
            get
            {
                return _points;
            }
        }

        /// <summary>
        /// Gets the label style.
        /// </summary>
        /// <value>
        /// The label style.
        /// </value>
        public SeriesLabelStyle LabelStyle
        {
            get { return _labelStyle; }
        }

        public SeriesLabelStyle DrawLinesToLabels
        {
            get { return _drawLinesToLabels; }
        }

        public ChartValueType XValueType { get; set; }
        public ChartValueType YValueType { get; set; }
        public int YValuesPerPoint { get; set; }

        /// <summary>
        /// Creates the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Series Create(SeriesChartType type)
        {
            return new Series() { ChartType = type };
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get { return _title ?? ID; }
            set
            {
                _title = value;
            }
        }

        /// <summary>
        /// Creates the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static Series Create(SeriesChartType type, string id)
        {
            return new Series(id) { ChartType = type };
        }
    }



    public enum ChartSeriesCombineMode
    {
        None = 0,
        Cluster = 1,
        Stack = 2,
        Stack100 = 3,
    }

    public enum ChartValueType
    {
        /// <summary>Property type is set automatically by the Chart control.</summary>
        Auto,
        /// <summary>A <see cref="T:System.Double" /> value.</summary>
        Double,
        /// <summary>A <see cref="T:System.Single" /> value.</summary>
        Single,
        /// <summary>A <see cref="T:System.Int32" /> value.</summary>
        Int32,
        /// <summary>A <see cref="T:System.Int64" /> value.</summary>
        Int64,
        /// <summary>A <see cref="T:System.UInt32" /> value.</summary>
        UInt32,
        /// <summary>A <see cref="T:System.UInt64" /> value.</summary>
        UInt64,
        /// <summary>A <see cref="T:System.String" /> value.</summary>
        String,
        /// <summary>A <see cref="T:System.DateTime" /> value.</summary>
        DateTime,
        /// <summary>The Date portion of a <see cref="T:System.DateTime" /> value.</summary>
        Date,
        /// <summary>The Time portion of the <see cref="DateTime" /> value.</summary>
        Time,
        /// <summary>A <see cref="T:System.DateTime" /> value with offset.</summary>
        DateTimeOffset
    }
}
