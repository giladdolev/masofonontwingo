﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for docking tools and documents
    /// </summary>
    public class DockPanelElement : PanelElement
    {
        private ControlElement _selectedPanel = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanelElement"/> class.
        /// </summary>
        public DockPanelElement()
        {

        }



        [DefaultValue(null)]
        [RendererPropertyDescription]
        [DesignerIgnore]
        public ControlElement SelectedPanel
        {
            get
            {
                return _selectedPanel;
            }
            set
            {
                _selectedPanel = value;
                OnPropertyChanged("SelectedPanel");
            }
        }



        [DesignerIgnore]
        public string SelectedPanelID
        {
            get
            {
                return (_selectedPanel != null) ? _selectedPanel.ClientID : "-1";
            }
        }



        /// <summary>
        ///Gets or sets the Y-value(s) of a data point.
        /// </summary>
        public bool IsCleanUpTarget
        {
            get;
            set;
        }



        /// <summary>
        /// Adds the document.
        /// </summary>
        /// <param name="document">The document.</param>
        public void AddDocument(DockPanelDocumentElement document)
        {
            if (document != null)
            {
                this.Controls.Add(document);
            }
        }



        /// <summary>
        /// Adds the tool.
        /// </summary>
        /// <param name="tool">The tool.</param>
        public void AddTool(DockPanelToolElement tool)
        {
            if (tool != null)
            {
                this.Controls.Add(tool);
            }
        }
    }
}
