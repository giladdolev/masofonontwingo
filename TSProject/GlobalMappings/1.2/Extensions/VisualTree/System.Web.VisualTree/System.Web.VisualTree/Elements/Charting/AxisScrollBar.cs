namespace System.Web.VisualTree.Elements
{
	public class AxisScrollBar : VisualElement
	{
		/// <summary>
		///Gets or sets a flag that determines whether a scrollbar is enabled.
		/// </summary>
		public bool Enabled
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets a flag that indicates whether scroll bar is positioned inside or outside the chart area.
		/// </summary>
		public bool IsPositionedInside
		{
			get;
			set;
		}
	}
}
