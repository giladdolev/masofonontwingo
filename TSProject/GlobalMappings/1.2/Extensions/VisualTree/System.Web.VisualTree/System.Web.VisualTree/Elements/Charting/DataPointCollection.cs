using System.Linq;

namespace System.Web.VisualTree.Elements
{
	public class DataPointCollection : VisualElementCollection<DataPoint>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPointCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal DataPointCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
		{
		}
		/// <summary>
		///Adds a T:System.Windows.Forms.DataVisualization.Charting.DataPoint object to the end of the collection, with the specified X-value and Y-value(s).
		/// </summary>
		/// <param name="xValue">The X value of the data point.</param>
		/// <param name="yValue">One or more comma-separated values that represent the Y-value(s) of the data point.</param>
		///  <returns>An integer value that represents the zero-based index where the item was inserted into the collection.</returns>
        public int AddXY(double xValue, params double[] yValue)
		{
            return AddWithIndex(new DataPoint(xValue, yValue));
		}
		/// <summary>
		///Adds a T:System.Windows.Forms.DataVisualization.Charting.DataPoint object to the end of the collection, with the specified Y-value.
		/// </summary>
		/// <param name="yValue">The Y-value of the data point.</param>
		///  <returns>An integer that represents the zero-based index where the item was inserted into the data point collection.</returns>
		public int AddY(double yValue)
		{
            return AddWithIndex(new DataPoint(0, yValue));
		}
		/// <summary>
		///Adds a T:System.Windows.Forms.DataVisualization.Charting.DataPoint object to the end of the collection, with the specified Y-value(s).
		/// </summary>
		/// <param name="yValue">A comma-separated list of Y-value(s) of the T:System.Windows.Forms.DataVisualization.Charting.DataPoint object added to the collection.</param>
		///  <returns>An integer that represents the location in zero-based index where the item was inserted into the collection.</returns>
        public int AddY(params double[] yValue)
		{
            return AddWithIndex(new DataPoint(0, yValue));
		}
		/// <summary>
		///Finds  the data point with the maximum value.
		/// </summary>
		///  <returns>The T:System.Windows.Forms.DataVisualization.Charting.DataPoint object with the maximum value.</returns>
		public DataPoint FindMaxByValue()
		{
            return this.FirstOrDefault();
		}
	}
}
