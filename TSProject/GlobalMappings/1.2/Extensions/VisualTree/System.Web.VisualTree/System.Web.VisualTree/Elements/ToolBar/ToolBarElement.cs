using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ToolBarElement : ScrollableControl
    {
        private bool _allowMerge;
        private ToolBarStyleConstants _style;
        private ToolBarTextDirection _textDirection = ToolBarTextDirection.Horizontal;
        private ToolBarRenderMode _renderMode = ToolBarRenderMode.ManagerRenderMode;
        private ToolBarLayoutStyle _layoutStyle = ToolBarLayoutStyle.StackWithOverflow;
        private ToolBarGripStyle _gripStyle = ToolBarGripStyle.Visible;
        private ToolBarItemCollection _items = null;
        private Orientation _orientation = Orientation.Horizontal;


        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarElement"/> class.
        /// </summary>
        public ToolBarElement()
        {
            _items = new ToolBarItemCollection(this, "Items");

            // Set default docking
            this.Dock = this.DefaultDock;
        }



        /// <summary>
        /// Gets a value indicating whether the element is a container
        /// </summary>
        /// <value>
        ///   <c>true</c> if is container; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool IsContainer
        {
            get { return true; }
        }

        private bool _vertical = false;

        [RendererPropertyDescription]
        /// <summary>
        /// Vertical Description.
        /// </summary>
        internal bool Vertical
        {
            get
            {
                return _vertical;
            }
            set
            {
                if (_vertical != value)
                {
                    _vertical = value;

                    OnPropertyChanged("Vertical");
                }
            }
        }

        public bool AllowMerge
        {
            get { return _allowMerge; }
            set
            {
                _allowMerge = value;

                // Notify property AllowMerge changed
                OnPropertyChanged("AllowMerge");
            }
        }



        [DefaultValue(ToolBarGripStyle.Visible)]
        public ToolBarGripStyle GripStyle
        {
            get { return _gripStyle; }
            set
            {
                _gripStyle = value;

                // Notify property GripStyle changed
                OnPropertyChanged("GripStyle");
            }
        }



        [DefaultValue(ToolBarLayoutStyle.StackWithOverflow)]
        public ToolBarLayoutStyle LayoutStyle
        {
            get { return _layoutStyle; }
            set
            {
                _layoutStyle = value;
                if (value == ToolBarLayoutStyle.VerticalStackWithOverflow)
                {
                    this.Vertical = true;
                }
                else
                {
                    this.Vertical = false;
                }
                _layoutStyle = value;

                // Notify property LayoutStyle changed
                OnPropertyChanged("LayoutStyle");
            }
        }



        [DefaultValue(ToolBarRenderMode.ManagerRenderMode)]
        public ToolBarRenderMode RenderMode
        {
            get { return _renderMode; }
            set
            {
                _renderMode = value;

                // Notify property RenderMode changed
                OnPropertyChanged("RenderMode");
            }
        }



        [DefaultValue(ToolBarTextDirection.Horizontal)]
        public ToolBarTextDirection TextDirection
        {
            get { return _textDirection; }
            set
            {
                _textDirection = value;

                // Notify property TextDirection changed
                OnPropertyChanged("TextDirection");
            }
        }



        /// <summary>
        /// Gets or sets the layout orientation.
        /// </summary>
        /// <value>
        /// The layout orientation.
        /// </value>
        public Orientation Orientation
        {
            get
            {
                return _orientation;
            }

            set
            {
                _orientation = value;
            }
        }




        /// <summary>
        /// Gets the default dock.
        /// </summary>
        /// <value>
        /// The default dock.
        /// </value>
        protected virtual Dock DefaultDock
        {
            get
            {
                return Dock.Top;
            }
        }



        /// <summary>
        /// Gets a value indicating whether children order reverted.
        /// </summary>
        /// <value>
        ///   <c>true</c> if children order reverted; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool ChildrenOrderReverted
        {
            get
            {
                return false;
            }
        }



        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public ToolBarStyleConstants Style
        {
            get { return _style; }
            set
            {
                _style = value;

                // Notify property BackColor changed
                OnPropertyChanged("Style");
            }
        }


        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [DesignerType(typeof(IList<ToolBarItem>))]
        public ToolBarItemCollection Items
        {
            get { return _items; }
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert = false)
        {
            ToolBarItem toolBarItem = visualElement as ToolBarItem;
            if (toolBarItem != null)
            {
                this.Items.Add(toolBarItem);
            }
            else
            {
                base.Add(visualElement, insert);
            }
        }

        /// <summary>
        /// Removes the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public override void Remove(VisualElement visualElement)
        {
            ToolBarItem toolBarItem = visualElement as ToolBarItem;

            if (toolBarItem != null)
            {
                this.Items.Remove(toolBarItem);
            }
            else
            {
                base.Remove(visualElement);
            }
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                foreach (VisualElement item in this.Items)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            // Loop all child controls
            foreach (VisualElement visualElement in _items)
            {
                // If there is a valid child control
                if (IsMatchingVisualElement<TVisualElement>(visualElement, strVisualElementID))
                {
                    // Return element
                    return visualElement as TVisualElement;
                }
            }

            return null;
        }


        /// <summary>
        /// ItemAdded event handler.
        /// </summary>
        private event EventHandler _ItemAdded;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ItemAdded action.
        /// </summary>
        public event EventHandler ItemAdded
        {
            add
            {
                bool needNotification = (_ItemAdded == null);

                _ItemAdded += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ItemAdded");
                }

            }
            remove
            {
                _ItemAdded -= value;

                if (_ItemAdded == null)
                {
                    OnEventHandlerDeattached("ItemAdded");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ItemAdded listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasItemAddedListeners
        {
            get { return _ItemAdded != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ItemAdded" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ItemAdded"/> instance containing the event data.</param>
        protected virtual void OnItemAdded(EventArgs args)
        {

            // Check if there are listeners.
            if (_ItemAdded != null)
            {
                _ItemAdded(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ItemAdded" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformItemAdded(EventArgs args)
        {

            // Raise the ItemAdded event.
            OnItemAdded(args);
        }
    }
}
