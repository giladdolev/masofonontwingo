﻿namespace System.Web.VisualTree.Elements
{
    public enum ToolBarGripStyle
    {
        /// <summary>Specifies that a <see cref="T:ToolBarElement" /> move handle (grip) is not visible.</summary>
        Hidden,
        /// <summary>Specifies that a <see cref="T:ToolBarElement" /> move handle (grip) is visible.</summary>
        Visible
    }
}
