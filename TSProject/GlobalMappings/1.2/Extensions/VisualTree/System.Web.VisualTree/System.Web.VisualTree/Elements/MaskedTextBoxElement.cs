﻿using System.ComponentModel;
using System.Text;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Defines how to format the text inside of a <see cref="T:MaskedTextBoxElement"/>.
    /// </summary>
    public enum MaskFormat
    {
        /// <summary>
        /// Return only text input by the user
        /// </summary>
        ExcludePromptAndLiterals,
        /// <summary>
        /// Return text input by the user as well as any literal characters defined in the mask
        /// </summary>
        IncludePrompt,
        /// <summary>
        /// Return text input by the user as well as any instances of the prompt character
        /// </summary>
        IncludeLiterals,
        /// <summary>
        /// Return text input by the user as well as any literal characters defined in the mask and any instances of the prompt character
        /// </summary>
        IncludePromptAndLiterals,
    }

    /// <summary>
    /// Uses a mask to distinguish between proper and improper user input.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.TextBoxBaseElement" />
    public class MaskedTextBoxElement : TextBoxBaseElement
    {
        private bool _allowBlank = true;
        private string _mask = string.Empty;
        private string _maskedText = string.Empty;
        private string _text = "";
        private string _regex = "";

        /// <summary>
        /// Determines the input mask for the control
        /// </summary>
        [DefaultValue("")]
        [RendererPropertyDescription]
        [Category("Text")]
        public string Mask
        {
            get
            {
                return _mask;
            }
            set
            {
                _mask = value;
                OnPropertyChanged("Mask");
            }
        }

        /// <summary>
        /// Get,set regex property. 
        /// </summary>
        [RendererPropertyDescription]
        public string Regex
        {
            get
            {
                return _regex;
            }
            set
            {
                if (value != _regex)
                {
                    _regex = value;
                }
            }
        }


        private char _promptChar = '_';
        /// <summary>
        /// Sets or gets the characters used to prompt a user for input
        /// </summary>
        [DefaultValue('_')]
        [RendererPropertyDescription]
        [Category("Text")]
        public char PromptChar
        {
            get { return _promptChar; }
            set
            {
                _promptChar = value;
                OnPropertyChanged("PromptChar");
            }
        }

        private MaskFormat _textMaskFormat = MaskFormat.ExcludePromptAndLiterals;
        /// <summary>
        /// Defines how to format the text 
        /// </summary>
        [DefaultValue(MaskFormat.ExcludePromptAndLiterals)]
        [RendererPropertyDescription]
        [Category("Text")]
        public MaskFormat TextMaskFormat
        {
            get { return _textMaskFormat; }
            set
            {
                _textMaskFormat = value;
                OnPropertyChanged("TextMaskFormat");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this masked textbox allows blank
        /// </summary>
        [DefaultValue(true)]
        [RendererPropertyDescription]
        [Category("Behavior")]
        public bool AllowBlank
        {
            get { return _allowBlank; }
            set { _allowBlank = value; }
        }

        public override bool Multiline
        {
            get
            {
                return false;
            }

            set { }
        }



        /// <summary>
        /// Gets or sets the MaskedText.
        /// </summary>
        /// <value>
        /// The origin text without the mask.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue("")]
        public string MaskedText
        {
            get
            {
                return _maskedText;
            }
            set
            {

                // set the value with Mask
                _maskedText = GetMaskedTextValue(value);
                // the text property ExludePropmptAndLiterals
                if (this.TextMaskFormat != MaskFormat.ExcludePromptAndLiterals)
                {
                    _text = _maskedText;
                }
                else
                {
                    _text = value;
                }
                ValueChangedArgs<string> newArgs = new ValueChangedArgs<string>(_text);
                // Raise the text changed event
                OnTextChanged(newArgs);

                OnPropertyChanged("MaskedText");
            }
        }


        /// <summary>
        /// GetMaskedTextValue get the text with the mask format .
        /// </summary>
        /// <param name="txt">The text with out mask format .</param>
        /// <returns>Formatted text .</returns>
        private string GetMaskedTextValue(string txt)
        {
            if (String.IsNullOrWhiteSpace(Mask))
            {
                return txt;
            }
            if (String.IsNullOrWhiteSpace(txt))
            {
                return txt;
            }
            StringBuilder strBuilder = new StringBuilder(Mask.Length);
            for (int i = 0, j = 0; j < Mask.Length; ++i, ++j)
            {

                if (j < Mask.Length)
                {
                    switch (Mask[j])
                    {
                        case '/':
                        case '-':
                        case '(':
                        case ')':
                        case ':':
                            strBuilder.Append(Mask[j]);
                            i--;
                            break;
                        case '0':
                            if (i < txt.Length)
                            {
                                if (txt[i] >= '0' && txt[i] <= '9')
                                {
                                    strBuilder.Append(txt[i]);
                                }
                                else
                                {
                                    strBuilder.Append(' ');
                                }
                            }
                            else
                            {
                                strBuilder.Append(PromptChar);
                            }
                            break;
                        case '9':
                            if (i < txt.Length)
                            {
                                if (txt[i] >= '0' && txt[i] <= '9' || txt[i] == ' ')
                                {
                                    strBuilder.Append(txt[i]);
                                }
                                else
                                {
                                    strBuilder.Append(' ');
                                }
                            }
                            else
                            {
                                strBuilder.Append(PromptChar);
                            }
                            break;
                        case '#':
                            if (i < txt.Length)
                            {
                                if (txt[i] >= '0' && txt[i] <= '9' || txt[i] == '+' || txt[i] == '-')
                                {
                                    strBuilder.Append(txt[i]);
                                }
                                else
                                {
                                    strBuilder.Append(' ');
                                }
                            }
                            else
                            {
                                strBuilder.Append(PromptChar);
                            }
                            break;
                        case 'L':
                            if (i < txt.Length)
                            {
                                if (txt[i] >= 'A' && txt[i] <= 'Z' || txt[i] >= 'a' && txt[i] <= 'z')
                                {
                                    strBuilder.Append(txt[i]);
                                }
                                else
                                {
                                    strBuilder.Append(' ');
                                }
                            }
                            else
                            {
                                strBuilder.Append(PromptChar);
                            }
                            break;
                        case ' ':
                            strBuilder.Append(' ');
                            i--;
                            break;
                        default:
                            if (TextMaskFormat == MaskFormat.IncludeLiterals || TextMaskFormat == MaskFormat.IncludePromptAndLiterals)
                            {
                                strBuilder.Append(txt[i]);
                            }
                            break;
                    }
                }
                else
                {
                    strBuilder.Append(txt[i]);
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// GetTextValue get the text without the mask format .
        /// </summary>
        /// <param name="txt">The text with the mask format .</param>
        /// <returns>Text without mask  .</returns>
        private string GetTextValue(string txt)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return string.Empty;
            }

            // In case no mask was entered and we are working with regex
            if (Mask.Length == 0 && Regex.Length > 0)
            {
                return txt;
            }

            StringBuilder strBuilder = new StringBuilder(Mask.Length);
            for (int i = 0; i < Mask.Length; i++)
            {
                if (i < txt.Length)
                {
                    if (Mask[i] == txt[i])
                    {
                        continue;
                    }
                    else
                    {

                        if (txt[i] == PromptChar && (TextMaskFormat == MaskFormat.ExcludePromptAndLiterals))
                        {
                            continue;
                        }
                        else
                        {
                            strBuilder.Append(txt[i]);
                        }
                    }
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public override string Text
        {
            get
            {

                return GetTextValue(base.Text);
            }
            set
            {
                this.MaskedText = value;
                base.Text = value;
            }
        }
    }

}