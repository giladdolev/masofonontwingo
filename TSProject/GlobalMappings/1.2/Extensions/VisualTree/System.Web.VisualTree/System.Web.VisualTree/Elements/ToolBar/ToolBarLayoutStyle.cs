﻿namespace System.Web.VisualTree.Elements
{
    public enum ToolBarLayoutStyle
    {
        /// <summary>Specifies that items are laid out automatically.</summary>
        StackWithOverflow,
        /// <summary>Specifies that items are laid out horizontally and overflow as necessary.</summary>
        HorizontalStackWithOverflow,
        /// <summary>Specifies that items are laid out vertically, are centered within the control, and overflow as necessary.</summary>
        VerticalStackWithOverflow,
        /// <summary>Specifies that items flow horizontally or vertically as necessary.</summary>
        Flow,
        /// <summary>Specifies that items are laid out flush left.</summary>
        Table
    }
}
