﻿

using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public static class InputBox
    {
        /// <summary>
        /// Shows the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="callback"></param>
        /// <returns></returns>
        [RendererMethodDescription]
        public static string Show(string text, string caption, EventHandler callback)
        {
            // The method callback
            Action<object> methodCallback = null;

            // If there is a valid callback
            if (callback != null)
            {
                // Set the method callback
                methodCallback = (result) =>
                {
                    // The input box result
                    string inputBoxResult = null;

                    // If result is a string
                    if (result is string)
                    {
                        // Get the dialog result
                        inputBoxResult = (string)result;
                    }

                    // Call the callback
                    callback(null, new ValueChangedArgs<string>(inputBoxResult));
                };
            }

            // Call client method
            ApplicationElement.InvokeMethod("Show", typeof(InputBox), new InputBoxData(text, caption), methodCallback);
            
            return string.Empty;
        }
    }
}
