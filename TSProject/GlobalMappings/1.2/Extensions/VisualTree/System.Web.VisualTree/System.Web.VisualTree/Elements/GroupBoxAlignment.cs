﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public enum GroupBoxAlignment
    {
        /// <summary>
        /// The bottom of caption
        /// </summary>
        BottomOfCaption = 12,
        /// <summary>
        /// The center bottom
        /// </summary>
        CenterBottom = 8,
        /// <summary>
        /// The center middle
        /// </summary>
        CenterMiddle = 7,
        /// <summary>
        /// The center top
        /// </summary>
        CenterTop = 6,
        /// <summary>
        /// The left bottom
        /// </summary>
        LeftBottom = 2,
        /// <summary>
        /// The left middle
        /// </summary>
        LeftMiddle = 1,
        /// <summary>
        /// The left of caption
        /// </summary>
        LeftOfCaption = 9,
        /// <summary>
        /// The left top
        /// </summary>
        LeftTop = 0,
        /// <summary>
        /// The right bottom
        /// </summary>
        RightBottom = 5,
        /// <summary>
        /// The right middle
        /// </summary>
        RightMiddle = 4,
        /// <summary>
        /// The right of caption
        /// </summary>
        RightOfCaption = 10,
        /// <summary>
        /// The right top
        /// </summary>
        RightTop = 3,
        /// <summary>
        /// The top of caption
        /// </summary>
        TopOfCaption = 11,
    }
}
