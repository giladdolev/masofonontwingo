﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements.Touch
{
    public class TToolBarMenuItem : ToolBarMenuItem, IVisualElementExtender
    {
        /// <summary>
        /// The button appearance
        /// </summary>
        private TButtonAppearance _appearance = TButtonAppearance.Normal;


        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        [RendererPropertyDescription]
        public TButtonAppearance Appearance
        {
            get { return _appearance; }
            set { _appearance = value; }
        }

    }
}
