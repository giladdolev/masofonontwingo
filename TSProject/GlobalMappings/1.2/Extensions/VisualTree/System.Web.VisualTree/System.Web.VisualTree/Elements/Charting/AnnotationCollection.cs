﻿namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="VisualElementCollection{Annotation}" />
    public class AnnotationCollection : VisualElementCollection<ChartAnnotation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationCollection" /> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal AnnotationCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {

        }
    }
}
