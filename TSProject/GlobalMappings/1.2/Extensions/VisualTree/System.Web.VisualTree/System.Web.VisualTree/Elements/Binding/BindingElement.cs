﻿

namespace System.Web.VisualTree.Elements
{
    public class BindingElement : VisualElement
    {
        public BindingElement(string propertyName, object dataSource, string dataMember)
        {

        }

        public BindingElement(string propertyName, object dataSource, string dataMember, bool formattingEnabled)
        {

        }
        public BindingElement(string propertyName, object dataSource, string dataMember, bool formattingEnabled, DataSourceUpdateMode dataSourceUpdateMode)
        {

        }
        public BindingElement(string propertyName, object dataSource, string dataMember, bool formattingEnabled, DataSourceUpdateMode dataSourceUpdateMode, object nullValue)
        {

        }
        public BindingElement(string propertyName, object dataSource, string dataMember, bool formattingEnabled, DataSourceUpdateMode dataSourceUpdateMode, object nullValue, string formatString)
        {

        }
        public BindingElement(string propertyName, object dataSource, string dataMember, bool formattingEnabled, DataSourceUpdateMode dataSourceUpdateMode, object nullValue, string formatString, IFormatProvider formatInfo)
        {

        }

        public BindingMemberInfo BindingMemberInfo { get; set; }
    }
}
