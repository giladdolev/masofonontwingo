﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridIndexesData
    {

        private int rowIdx;
        private int colIdx;
        private bool _protect;

        /// <summary>
        /// initializes an instance of GridIndexesData
        /// </summary>
        public  GridIndexesData()
        {

        }

        /// <summary>
        /// initializes an instance of GridIndexesData
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public GridIndexesData(int row, int col)
        {
            rowIdx = row;
            colIdx = col;
        }

        /// <summary>
        /// initializes an instance of GridIndexesData
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public GridIndexesData(int row, int col, bool protect)
        {
            rowIdx = row;
            colIdx = col;
            _protect = protect;
        }


        /// <summary>
        /// Gets or sets the row index.
        /// </summary>
        /// <value>
        /// The row index.
        /// </value>
        public int RowIndex
        {
            get { return rowIdx; }
            set
            {
                rowIdx = value;
            }
        }

        /// <summary>
        /// Gets or sets the column index.
        /// </summary>
        /// <value>
        /// The column index.
        /// </value>
        public int ColumnIndex
        {
            get { return colIdx; }
            set
            {
                colIdx = value;
            }
        }

        public int Button
        {
            get;
            set;
        }

        public bool State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Protect.
        /// </summary>
        /// <value>
        /// The Protect.
        /// </value>
        public bool Protect
        {
            get { return _protect; }
            set
            {
                _protect = value;
            }
        }
    }
}
