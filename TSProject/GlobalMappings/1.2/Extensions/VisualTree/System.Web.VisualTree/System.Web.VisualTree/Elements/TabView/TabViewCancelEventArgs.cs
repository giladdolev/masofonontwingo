namespace System.Web.VisualTree.Elements
{
	public class TabViewCancelEventArgs : EventArgs
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TabViewCancelEventArgs"/> class.
        /// </summary>
        /// <param name="objTabPage">The tab page.</param>
        /// <param name="objPrevTabPage">The previous tab page.</param>
        /// <param name="intTabPageIndex">Index of the tab page.</param>
        /// <param name="IsCancel">if set to <c>true</c> [is cancel].</param>
        public TabViewCancelEventArgs(TabItem objTabPage, TabItem objPrevTabPage, int intTabPageIndex, bool IsCancel)
		{
            TabPage = objTabPage;
            PrevPage = objPrevTabPage;
            TabItemIndex = intTabPageIndex;
            Cancel = IsCancel;
		}


		/// <summary>
		/// Gets or sets the tab page.
		/// </summary>
		/// <value>
		/// The tab page.
		/// </value>
        public TabItem TabPage
        {
            get;
            set;
        }


		/// <summary>
		/// Gets or sets the index of the tab item.
		/// </summary>
		/// <value>
		/// The index of the tab item.
		/// </value>
        public int TabItemIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TabViewCancelEventArgs"/> is cancel.
        /// </summary>
        /// <value>
        ///   <c>true</c> if cancel; otherwise, <c>false</c>.
        /// </value>
        public bool Cancel
        {
            get;
            set;
        }


        /// <summary>
        /// Gets the previously processed tab page.
        /// </summary>
        public TabItem PrevPage
        {
            get;
            set;
        }
	}
}
