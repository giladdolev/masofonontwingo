﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class ProgressBarElement : ControlElement
    {
        private string _text = string.Empty;
        private double _value = 0;
        private int _max = 100;
        private int _min = 0;


        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;

                // Notify property Enabled changed
                OnPropertyChanged("Text");
            }
        }

        

        [RendererPropertyDescription]
        public double Value
        {
            get { return _value; }
            set
            {
                _value = value;

                // Notify property Enabled changed
                OnPropertyChanged("Value");
            }
        }



        [DefaultValue(100)]
        public int Max
        {
            get { return _max; }
            set
            {
                _max = value;

                // Notify property Enabled changed
                OnPropertyChanged("Max");
            }
        }

        

        public int Min
        {
            get { return _min; }
            set
            {
                _min = value;

                // Notify property Enabled changed
                OnPropertyChanged("Min");
            }
        }
    }
}
