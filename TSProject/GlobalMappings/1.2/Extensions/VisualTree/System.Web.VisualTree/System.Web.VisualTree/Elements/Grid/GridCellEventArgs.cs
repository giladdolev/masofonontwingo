namespace System.Web.VisualTree.Elements
{
    public class GridCellEventArgs : EventArgs
    {

        public GridCellEventArgs()
        {
        }

        public GridCellEventArgs(string value)
        {

        }

        public GridCellEventArgs(int columnIndex, int rowIndex)
        {
            _columnIndex = columnIndex;
            _rowIndex = rowIndex;
        }

        public GridCellEventArgs(string value, string dataindex)
        {
            // TODO: Complete member initialization
            this.Value = value;
            this.DataIndex = dataindex;
        }

        public GridCellEventArgs(string value, string dataindex,object rowIndex)
        {
            this.Value = value;
            this.DataIndex = dataindex;
            this.RowIndex = Convert.ToInt32(rowIndex);
        }


        private int _columnIndex;
        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int ColumnIndex
        {
            get { return _columnIndex; }
            private set { _columnIndex = value; }
        }



        private int _rowIndex;
     //   private string p1;
        //private string p2;
        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int RowIndex
        {
            get { return _rowIndex; }
            private set { _rowIndex = value; }
        }

        public string Value
        {
            get;
            set;
        }

        public string DataIndex { get; set; }
    }
}
