﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class ControlEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlEventArgs"/> class.
        /// </summary>
        /// <param name="element">The object element.</param>
        /// <exception cref="System.ArgumentNullException">objElement</exception>
        public ControlEventArgs(ControlElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            Control = element;
        }



        /// <summary>
        /// Gets or sets the control.
        /// </summary>
        /// <value>
        /// The control.
        /// </value>
        public ControlElement Control { get; set; }
    }
}
