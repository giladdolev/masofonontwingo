using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public class ToolBarProgressBar : ToolBarItem
	{
        private int _value = 0;
        private int _max = 100;
        private int _min = 0;



        /// <summary>
        /// Current value of the progressbar
        /// </summary>
        [RendererPropertyDescription]
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;

                // Notify property Value changed
                OnPropertyChanged("Value");
            }
        }


        /// <summary>
        /// Maximum value of the progressbar
        /// </summary>
        [DefaultValue(100)]
        public int Max
        {
            get { return _max; }
            set
            {
                _max = value;

            }
        }

        /// <summary>
        /// Minumum value of progressbar
        /// </summary>
        public int Min
        {
            get { return _min; }
            set
            {
                _min = value;
            }
        }
	}
}
