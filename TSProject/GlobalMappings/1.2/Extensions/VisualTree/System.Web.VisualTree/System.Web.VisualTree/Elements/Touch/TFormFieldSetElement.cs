﻿namespace System.Web.VisualTree.Elements.Touch
{
    public class TFormFieldSetElement : GroupBoxElement, ITVisualElement, ITVisualContainerElement
    {
        /// <summary>
        /// Gets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        TVisualLayout ITVisualContainerElement.Layout
        {
            get
            {
                return TVisualLayout.VBox;
            }
        }
    }



    public interface ITFormFieldElement
    {
        string Label
        {
            get;
            set;
        }
    }
}
