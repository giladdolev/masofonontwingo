﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    // Summary:
    // Defines constants that specify the direction in which consecutive user interface
    // (UI) elements are placed in a linear layout container
    public enum FlowDirection
    {
        // Summary:
        //     Elements flow from the left edge of the design surface to the right.
        LeftToRight = 0,
        //
        // Summary:
        //     Elements flow from the top of the design surface to the bottom.
        TopDown = 1,
        //
        // Summary:
        //     Elements flow from the right edge of the design surface to the left.
        RightToLeft = 2,
        //
        // Summary:
        //     Elements flow from the bottom of the design surface to the top.
        BottomUp = 3,
    }
}
