namespace System.Web.VisualTree.Elements
{
	public class DragEventArgs : EventArgs
	{

        public DragEventArgs()
        {

        }

        public DragEventArgs(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the effect.
        /// </summary>
        /// <value>
        /// The effect.
        /// </value>
        public DragDropEffects Effect
        {
            get;
            set;
        }

        public int X
        {
            get;
            set;
        }

        public int Y
        {
            get;
            set;
        }
	}
}
