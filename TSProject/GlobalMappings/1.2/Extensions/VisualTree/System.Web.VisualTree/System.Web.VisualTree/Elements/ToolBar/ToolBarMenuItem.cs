using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ToolBarMenuItem : ToolBarDropDownItem
    {
        public static string DefaultCustomUI = "";

        private Keys _shortcutKeys = Keys.None;


        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuItem"/> class.
        /// </summary>
        public ToolBarMenuItem()
        {
            if (!string.IsNullOrEmpty(ToolBarMenuItem.DefaultCustomUI))
            {
                this.CustomUI = DefaultCustomUI;
            }
        }

        /// <summary>
        /// Initializes a new instance of the ToolBarMenuItem class with the specified name that displays the specified text and image that does the
        /// specified action when the ToolStripMenuItem is clicked, and displays the specified shortcut keys.
        /// </summary>
        /// <param name="txt">the name of the button</param>
        /// <param name="Image">image of button</param>
        /// <param name="eventarg">The eventarg.</param>
        /// <param name="key">shortcut key</param>
        public ToolBarMenuItem(string txt, ResourceReference Image, EventHandler eventarg,Keys key):this()
        {
            this.Text = txt;
            this.Image = Image;
            this.Click += eventarg;
            this.ShortcutKeys = key;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarMenuItem"/> class.
        /// </summary>
        /// <param name="txt">The text.</param>
        public ToolBarMenuItem(string txt):this()
        {
            this.Text = txt;
        }



        public bool Checked
        {
            get;
            set;
        }

        

        [DefaultValue(Keys.None)]
        public Keys ShortcutKeys
        {
            get { return _shortcutKeys; }
            set { _shortcutKeys = value; }
        }



        public bool CheckOnClick
        {
            get;
            set;
        }



        public CheckState CheckState
        {
            get;
            set;
        }


        /// <summary>
        /// CheckStateChanged event handler.
        /// </summary>
        private event EventHandler _CheckStateChanged;

        /// <summary>
        /// Add or remove CheckStateChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler CheckStateChanged
        {
            add
            {
                bool needNotification = (_CheckStateChanged == null);

                _CheckStateChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("CheckStateChanged");
                }

            }
            remove
            {
                _CheckStateChanged -= value;

                if (_CheckStateChanged == null)
                {
                    OnEventHandlerDeattached("CheckStateChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has CheckStateChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasCheckStateChangedListeners
        {
            get { return _CheckStateChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:CheckStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="CheckStateChanged"/> instance containing the event data.</param>
        protected virtual void OnCheckStateChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_CheckStateChanged != null)
            {
                _CheckStateChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:CheckStateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformCheckStateChanged(EventArgs args)
        {

            // Raise the CheckStateChanged event.
            OnCheckStateChanged(args);
        }


        /// <summary>
        /// Gets the index of the ToolBarMenuItem
        /// </summary>
        /// <param name="toolBarMenuItem">The tool bar menu item.</param>
        /// <returns></returns>
        public int IndexOf(ToolBarMenuItem toolBarMenuItem)
        {
            if (this.Items != null && toolBarMenuItem != null)
            {
                return this.Items.IndexOf(toolBarMenuItem);
            }

            return -1;
        }



        /// <summary>
        /// Shows the menu over the specified control element.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        public void Show(ControlElement controlElement)
        {
            // TODO: ApplicationElement.EnqueueAction(new ContextMenuActionArgs(this, controlElement, null));
        }



        /// <summary>
        /// Shows the menu over the specified control element.
        /// </summary>
        /// <param name="controlElement">The control element.</param>
        /// <param name="callback">The callback.</param>
        public void Show(ControlElement controlElement, EventHandler callback)
        {
            // TODO: ApplicationElement.EnqueueAction(new ContextMenuActionArgs(this, controlElement, callback));
        }
    }
}
