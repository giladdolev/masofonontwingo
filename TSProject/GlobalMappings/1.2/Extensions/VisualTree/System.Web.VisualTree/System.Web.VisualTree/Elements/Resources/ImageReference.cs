﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class ImageReference : ResourceReference
    {
        private readonly string _source;
        private readonly Image _image;

        
        /// <summary>
        /// The blank image
        /// </summary>
        private readonly static ImageReference _blank = null;

        public ImageReference()
        {

        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ImageReference" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="image">The image.</param>
        public ImageReference(string source, Image image)
        {
            _source = source;
            _image = image;            
        }


        /// <summary>
        /// Initializes the <see cref="ImageListStreamer"/> class.
        /// </summary>
        static ImageReference()
        {
            // Create blank image
            Bitmap blank = new Bitmap(16, 16);
            blank.SetPixel(1, 1, Color.Red);
            blank.SetPixel(2, 2, Color.Red);
            blank.SetPixel(3, 3, Color.Red);
            blank.SetPixel(4, 4, Color.Red);
            blank.SetPixel(5, 5, Color.Red);
            blank.SetPixel(6, 6, Color.Red);
            blank.SetPixel(7, 7, Color.Red);

            // Set the blank image
            _blank = blank;
        }


        /// <summary>
        /// Gets the blank.
        /// </summary>
        /// <value>
        /// The blank.
        /// </value>
        public static ImageReference Blank
        {
            get { return _blank; }
        }

        /// <summary>
        /// Gets the size of the image.
        /// </summary>
        /// <value>
        /// The size of the image.
        /// </value>
        public Size ImageSize
        {
            get { return _image.Size; }
        }


        /// <summary>
        /// Performs an implicit conversion from <see cref="ImageReference"/> to <see cref="Image"/>.
        /// </summary>
        /// <param name="imageReference">The image reference.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Image(ImageReference imageReference)
        {
            // If there is a valid image reference
            if (imageReference != null)
            {
                // Return the image reference image
                return imageReference._image;
            }

            return null;
        }

        /// <summary>
        /// Companion to the operator overload image.
        /// </summary>
        /// <param name="imageReference">The image reference.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static Image ToImage(ImageReference imageReference)
        {
            // If there is a valid image reference
            if (imageReference != null)
            {
                // Return the image reference image
                return imageReference._image;
            }

            return null;
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="Image"/> to <see cref="ImageReference"/>.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ImageReference(Image image)
        {

            return ((ResourceReference)image) as ImageReference;
        }

        /// <summary>
        /// Companion to the operator overload imageReference.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static Image ToImageReference(Image image)
        {

            return ((ResourceReference)image) as ImageReference;
        }


        /// <summary>
        /// Performs an implicit conversion from <see cref="Icon"/> to <see cref="ImageReference"/>.
        /// </summary>
        /// <param name="icon">The icon.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ImageReference(Icon icon)
        {
            return ((ResourceReference)icon) as ImageReference;
        }

        /// <summary>
        /// Companion to the operator overload imagereference.
        /// </summary>
        /// <param name="icon">The icon.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static ImageReference ToImageReference(Icon icon)
        {
            return ((ResourceReference)icon) as ImageReference;
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public override string Source
        {
            get
            {
                return _source;
            }
        }
        
    }
}
