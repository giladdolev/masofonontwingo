﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace System.Web.VisualTree.Elements
{
    public class GridTextBoxCell : GridCellElement
    {
        /// <summary>
        /// Initializes the editing control.
        /// </summary>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="initialFormattedValue">The initial formatted value.</param>
        /// <param name="dataGridViewCellStyle">The data grid view cell style.</param>
        public virtual void InitializeEditingControl(int rowIndex, object
            initialFormattedValue, GridCellStyle dataGridViewCellStyle)
        {
            
        }

        /// <summary>
        /// Gets or sets the type of the edit.
        /// </summary>
        /// <value>
        /// The type of the edit.
        /// </value>
        public virtual Type EditType { get; set; }

        /// <summary>
        /// Gets or sets the type of the value.
        /// </summary>
        /// <value>
        /// The type of the value.
        /// </value>
        public virtual Type ValueType { get; set; }

        /// <summary>
        /// Gets or sets the default new row value.
        /// </summary>
        /// <value>
        /// The default new row value.
        /// </value>
        public virtual object DefaultNewRowValue { get; set; }
    }
}
