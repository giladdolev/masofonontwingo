﻿using System.Drawing;
using System.Web.VisualTree.Common;

namespace System.Web.VisualTree.Elements
{
    public class WindowMdiContainer : ControlElement, IInternalImplementationElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WindowMdiContainer"/> class.
        /// </summary>
        public WindowMdiContainer()
        {
            BackColor = SystemColors.AppWorkspace;
            Dock = Dock.Fill;

        }
    }
}
