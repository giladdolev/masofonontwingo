﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ImageCollection : VisualElementCollection<ResourceReference>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageCollection"/> class.
        /// </summary>
        /// <param name="parentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ImageCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {            
        }

        /// <summary>
        /// Indexes the of key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public int IndexOfKey(string key)
        {
            ImageList imageList = this.Owner as ImageList;

            if (imageList != null)
            {
                foreach (KeyItem keyItem in imageList.Keys)
                {
                    if (string.Equals(keyItem.Key, key))
                    {
                        return keyItem.ImageIndex;
                    }
                }
            }
            return -1;
        }



        /// <summary>
        /// Sets the image key.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="key">The key.</param>
        public void SetKeyName(int index, string key)
        {
            if (index >= 0)
            {
                ImageList imageList = this.Owner as ImageList;

                if(imageList != null)
                {
                    imageList.Keys.Add(new KeyItem(key, index));
                }
            }
        }



        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="images">The images.</param>
        public void AddRange(IEnumerable<Image> images)
        {

            if (images == null)
            {
                return;
            }

            foreach (Image image in images)
            {
                // Add the image
                if (!this.Contains(image))
                {
                    Add(image);
                }
            }
        }



        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="images">The images.</param>
        internal void AddRange(IEnumerable<ImageReference> images)
        {
            foreach (Image image in images)
            {
                if (!this.Contains(image))
                {
                    Add(image);
                }
            }
        }

        
        public void Add(string key, UrlReference urlRef)
        {
            if (string.IsNullOrWhiteSpace(key) || urlRef == null)
            {
                return;
            }

            ImageReference imgRef = new ImageReference(urlRef.Source, null);

            if(!this.Contains(imgRef))
            {
                this.Add(imgRef);

                int index = this.IndexOf(imgRef);
                if (index >= 0)
                {
                    this.SetKeyName(index, key);
                }
            }
            
        }
    }
}
