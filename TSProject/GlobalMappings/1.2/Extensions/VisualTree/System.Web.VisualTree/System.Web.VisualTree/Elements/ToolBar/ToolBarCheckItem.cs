using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ToolBarCheckItem : ToolBarDropDownItem
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarCheckItem" /> class.
        /// </summary>
        public ToolBarCheckItem()
        {
   
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarCheckItem"/> class.
        /// </summary>
        /// <param name="txt">The text.</param>
        public ToolBarCheckItem(string txt)
        {
            this.Text = txt;
        }


    }
}
