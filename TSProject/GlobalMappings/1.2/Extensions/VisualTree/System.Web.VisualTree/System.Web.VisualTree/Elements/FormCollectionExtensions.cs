﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public static class FormCollectionExtensions
    {
        public static WindowElement Item(this FormCollection objFormCollection, int intIndex)
        {
            // Null reference checking and check if there are nodes in the collection
            if (objFormCollection != null && objFormCollection.Count() > 0)
            {
                return objFormCollection[intIndex];
            }
            else
            {
                //Throw null reference exception
                throw new ArgumentException("Form Collection is either null or empty", "objFormCollection");
            }
        }
    }
}
