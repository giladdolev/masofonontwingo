using System.Drawing;

namespace System.Web.VisualTree.Elements
{
	public class SystemInformation : VisualElement
	{
		public static Size FrameBorderSize
		{
			get { return default(Size); }
		}
	}
}
