﻿using System.IO;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class FileUploadElement : ControlElement
    {
        private string _uploadPath = null;



        /// <summary>
        /// Gets or sets the upload path.
        /// </summary>
        /// <value>
        /// The upload path.
        /// </value>
        public string UploadPath
        {
            get
            {
                return _uploadPath;
            }
            set
            {
                _uploadPath = value;
            }
        }



        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>
        /// The file.
        /// </value>
        public string File
        {
            get;
            set;
        }


        /// <summary>
        /// FileUploaded event handler.
        /// </summary>
        private event EventHandler _FileUploaded;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove FileUploaded action.
        /// </summary>
        public event EventHandler FileUploaded
        {
            add
            {
                bool needNotification = (_FileUploaded == null);

                _FileUploaded += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("FileUploaded");
                }

            }
            remove
            {
                _FileUploaded -= value;

                if (_FileUploaded == null)
                {
                    OnEventHandlerDeattached("FileUploaded");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has FileUploaded listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasFileUploadedListeners
        {
            get { return _FileUploaded != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:FileUploaded" /> event.
        /// </summary>
        /// <param name="args">The <see cref="FileUploaded"/> instance containing the event data.</param>
        protected virtual void OnFileUploaded(EventArgs args)
        {

            // Check if there are listeners.
            if (_FileUploaded != null)
            {
                _FileUploaded(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:FileUploaded" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformFileUploaded(EventArgs args)
        {
            // Get the file args
            ValueChangedArgs<string> fileArgs = args as ValueChangedArgs<string>;

            // If there is a valid file args
            if (fileArgs != null)
            {
                // Set the file property
                File = fileArgs.Value;

                // Raise the FileUploaded event.
                OnFileUploaded(args);
            }
        }




        /// <summary>
        /// Performs the file uploaded.
        /// </summary>
        /// <param name="postedFile">The posted file.</param>
        public void PerformFileUploaded(HttpPostedFile postedFile)
        {
            // If there is a valid upload path
            if(!string.IsNullOrEmpty(_uploadPath))
            {
                // If we need to create directory
                if(!Directory.Exists(_uploadPath))
                {
                    // Create directory
                    Directory.CreateDirectory(_uploadPath);
                }

                // Get the unique file
                string uniqueFile = GetUniqueFileName(_uploadPath, Path.GetFileNameWithoutExtension(postedFile.FileName), Path.GetExtension(postedFile.FileName));

                // If there is a valid unique file
                if(!string.IsNullOrEmpty(uniqueFile))
                {
                    // Save posted file
                    postedFile.SaveAs(uniqueFile);

                    // Raise the file event
                    PerformFileUploaded(new ValueChangedArgs<string>(uniqueFile));
                }
            }
        }



        /// <summary>
        /// Gets the name of the unique file.
        /// </summary>
        /// <param name="uploadPath">The upload path.</param>
        /// <param name="fileName">The file name.</param>
        /// <param name="fileExtension">The file extension.</param>
        /// <returns></returns>
        private static string GetUniqueFileName(string uploadPath, string fileName, string fileExtension)
        {
            // Get the initial unique name
            string uniqueName = string.Concat(fileName, fileExtension);

            // The file index
            int fileIndex = 0;

            // Loop untill there is a valid unique name
            while (IO.File.Exists(Path.Combine(uploadPath, uniqueName)))
            {
                // Increment index
                fileIndex++;

                // Create new unique name
                uniqueName = string.Concat(fileName, "(", fileIndex, ")", fileExtension);
            }

            // Create full name
            return Path.Combine(uploadPath, uniqueName);
        }


        /// <summary>
        /// Get,Set text property .
        /// </summary>
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                OnPropertyChanged("Items");
            }
        }
    }
}
