﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for model binding
    /// </summary>
    internal class BindingModelManager : BindingManager
    {
        /// <summary>
        /// The columns
        /// </summary>
        private Dictionary<string, IBindingDataColumn> _columns = new Dictionary<string, IBindingDataColumn>();

        /// <summary>
        /// The has index columns
        /// </summary>
        private bool? _hasIndexColumns = null;


        /// <summary>
        /// The data source
        /// </summary>
        private object _dataSource = null;

        /// <summary>
        /// The item type
        /// </summary>
        private Type _itemType = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="BindingModelManager"/> class.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public BindingModelManager(VisualElement visualElement)
            : base(visualElement)
        {
        }



        /// <summary>
        /// Gets the item by item identifier.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns></returns>
        public override object GetItemByItemId(object itemId)
        {
            // If item id is integer
            if (itemId is int)
            {
                /// Get index
                int index = (int)itemId;

                // If there is a valid index
                if (index > -1)
                {
                    // Get data source as list
                    IList dataList = _dataSource as IList;

                    // If there is a valid list
                    if (dataList != null)
                    {
                        // If there is a valid index
                        if (dataList.Count > index)
                        {
                            // Return item from index
                            return dataList[index];
                        }
                    }
                    else
                    {
                        // If there is a valid data source
                        if (_dataSource != null)
                        {
                            // If the index is the first
                            if (index == 0)
                            {
                                // Return data source
                                return _dataSource;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <value>
        /// The items.
        /// </value>
        public override IEnumerable Items
        {
            get
            {
                // Get items from data source
                IEnumerable items = _dataSource as IEnumerable;

                // If there are no valid items
                if (items == null)
                {
                    // If there is a valid data source
                    if (_dataSource != null)
                    {
                        // Set items
                        items = new object[] { _dataSource };
                    }
                    else
                    {
                        // Set empty list
                        items = EmptyItems;
                    }
                }

                // Return items
                return items;
            }
        }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>
        /// The data source.
        /// </value>
        public override object DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                _dataSource = value;
                if ((value == null) && (this.ParentElement != null))
                {
                    VisualElementExtender.RemoveExtender<BindingManager>(this.ParentElement);
                }
                else
                {
                    OnDataSourceChanged(value);
                }
            }
        }

        /// <summary>
        /// Called when data source changed.
        /// </summary>
        /// <param name="dataContext">The value.</param>
        private void OnDataSourceChanged(object dataContext)
        {
            // If there is a valid value
            if (dataContext != null)
            {
                // Get the item type
                Type itemType = BindingDataReader.GetItemType(dataContext);

                // If there is a valid item type
                if(itemType != null)
                {
                    // Set item type
                    this.SetItemType(itemType);
                }                
            }
        }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        /// <value>
        /// The item type.
        /// </value>
        public override Type ItemType
        {
            get
            {
                return _itemType;
            }
        }

        /// <summary>
        /// Sets the type of the item.
        /// </summary>
        /// <param name="value">The value.</param>
        private void SetItemType(Type value)
        {
            // Set item type
            _itemType = value;

            // Loop all columns
            foreach (IBindingDataColumn column in this.Columns)
            {
                // Update the column type
                column.Bind(this.GetProperty(column.Name));
            }
        }

        /// <summary>
        /// Register the used column.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <returns></returns>
        internal override IBindingDataColumn RegisterUsedColumn(string columnName)
        {
            // The registered column
            IBindingDataColumn column = null;

            // If there is a valid column name
            if (!string.IsNullOrEmpty(columnName))
            {
                // Try to get existing column
                if (!_columns.TryGetValue(columnName, out column))
                {
                    // Get property from column
                    PropertyInfo property = GetProperty(columnName);

                    // If there is a valid property
                    if (property != null)
                    {
                        // Register column
                        _columns[columnName] = column = new BindingDataColumn(property);
                    }
                    else
                    {
                        // Register column
                        _columns[columnName] = column = new BindingDataColumn(columnName);
                    }

                    ResetHasIndexColumns();
                }
            }

            return column;
        }



        /// <summary>
        /// Resets the has index columns.
        /// </summary>
        internal override void ResetHasIndexColumns()
        {
            // Reset has index columns flag
            _hasIndexColumns = null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has index columns.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has index columns; otherwise, <c>false</c>.
        /// </value>
        public override bool HasIndexColumns
        {
            get
            {
                if (_hasIndexColumns == null)
                {
                    foreach (BindingDataColumn column in _columns.Values)
                    {
                        if (column.IsIndex)
                        {
                            _hasIndexColumns = true;
                            break;
                        }
                    }

                    if (_hasIndexColumns == null)
                    {
                        _hasIndexColumns = false;
                    }
                }

                return _hasIndexColumns.Value;
            }
        }

        /// <summary>
        /// Gets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public override IEnumerable<IBindingDataColumn> Columns
        {
            get
            {
                return _columns.Values;
            }
        }
    }

}
