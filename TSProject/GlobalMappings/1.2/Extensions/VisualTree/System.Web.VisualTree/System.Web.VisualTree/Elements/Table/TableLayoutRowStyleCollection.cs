namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="TableLayoutStyleCollection{RowStyle}" />
    public class TableLayoutRowStyleCollection : TableLayoutStyleCollection<RowStyle>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableLayoutRowStyleCollection"/> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TableLayoutRowStyleCollection(TablePanel objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }
    }	
}
