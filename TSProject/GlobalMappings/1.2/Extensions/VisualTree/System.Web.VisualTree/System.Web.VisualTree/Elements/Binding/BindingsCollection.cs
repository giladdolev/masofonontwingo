namespace System.Web.VisualTree.Elements
{
	public class BindingsCollection : VisualElementCollection<BindingElement>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="BindingsCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal BindingsCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }
	}
}
