﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.VisualTree.Common.Attributes;
using System.Linq;
using System.Globalization;

namespace System.Web.VisualTree.Elements
{
    public class TreeColumn : ControlElement, IBindingDataColumn
    {


        private string _headerText;
        private string _dataMember;
        private bool _IsAutoID;
       
        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        public TreeColumn()
            : this(null, null, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn" /> class.
        /// We used a default constructor how use 2 string
        /// </summary>
        /// <param name="columnName">The column name.</param>
        public TreeColumn(string columnName)
            : this(columnName, columnName)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        public TreeColumn(string columnName, string headerText)
            : this(columnName, headerText, typeof(string))
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumn"/> class.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <param name="headerText">The header text.</param>
        /// <param name="dataType">The column data type.</param>
        public TreeColumn(string columnName, string headerText, System.Type dataType)
        {
            if (!String.IsNullOrWhiteSpace(columnName))
            {
                ID = NormalizePropertyName(columnName);
                _IsAutoID = false;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(headerText))
                {
                    ID = NormalizePropertyName(headerText);
                    _IsAutoID = false;
                }
                else
                {
                    ID = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
                    _IsAutoID = true;
                }

            }

            HeaderText = headerText;
            DataType = dataType;


        }

        /// <summary>
        /// Gets or sets the header text.
        /// </summary>
        /// <value>
        /// The header text.
        /// </value>
        public string HeaderText
        {
            get
            {
                return this._headerText;
            }
            set
            {
                this._headerText = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>
        /// The type of the data.
        /// </value>
        public System.Type DataType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get
            {
                TreeElement tree = this.Parent as TreeElement;
                if (tree != null)
                {
                    return tree.Columns.IndexOf(this);
                }
                return -1;
            }
        }



        /// <summary>
        /// Gets a value indicating whether this column is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this column is valid; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsValid
        {
            get
            {
                return true;
            }
        }


        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        object IBindingDataColumn.GetValue(object item, int index)
        {
            return null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is index.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is index; otherwise, <c>false</c>.
        /// </value>
        bool IBindingDataColumn.IsIndex
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Binds the specified property information.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        void IBindingDataColumn.Bind(PropertyInfo propertyInfo)
        {

        }

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <value>
        /// The name of the data type.
        /// </value>
        [DesignerIgnore]
        public virtual string DataTypeName
        {
            get
            {
                return this.DataType.Name;
            }
        }


        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string IBindingDataColumn.Name
        {
            get
            {
                return this.DataMember;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        Type IBindingDataColumn.Type
        {
            get
            {
                return this.DataType;
            }
        }

        /// <summary>
        /// Gets or sets the name of the data property.
        /// </summary>
        /// <value>
        /// The name of the data property.
        /// </value>
        [RendererPropertyDescription]
        public string DataMember
        {
            get
            {
                return this._dataMember;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    this._dataMember = NormalizePropertyName(value);
                }
                OnPropertyChanged("DataMember");
            }
        }

        /// <summary>
        ///  Normalize property name.
        /// </summary>
        /// <param name="name">The id name</param>
        /// <returns>correct id </returns>
        private static string NormalizePropertyName(string name)
        {

            string newName = new string((from c in name where char.GetUnicodeCategory(c) != UnicodeCategory.OtherLetter && (char.IsLetterOrDigit(c) || c == '_') select c).ToArray());

            if (!newName.Equals(name))
            {
                newName = String.Format("Column_{0}", Guid.NewGuid().ToString("N"));
            }

            return newName;
        }

        private ColumnSortMode _sortType = ColumnSortMode.StringAscending;

        /// <summary>
        /// Gets the name of the Sort type.
        /// </summary>
        /// <value>
        /// The name of the Sort type.
        /// </value>
        public ColumnSortMode SortType
        {
            get
            {
                return _sortType;
            }
            set
            {
                this._sortType = value;
            }
        }

    }
}


