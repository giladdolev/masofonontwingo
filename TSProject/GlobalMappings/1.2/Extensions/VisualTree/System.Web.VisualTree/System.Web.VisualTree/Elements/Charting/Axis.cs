using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public enum AxisType
    {
        Default,
        Category,
        Numeric,
        Time
    }

    public class Axis : ChartNamedElement
    {

        private readonly CustomLabelsCollection _customLabels = null;
        private ChartingGrid _minorGrid = null;
        private double _maximum;
        private double _minimum;

        public Axis()
        {
            ScaleView = new AxisScaleView();
            LabelStyle = new AxisLabelStyle();
            ScrollBar = new AxisScrollBar();
            MinorGrid = new ChartingGrid();
            _customLabels = new CustomLabelsCollection(this, "CustomLabels");

        }


        /// <summary>
        /// Creates the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Axis Create(AxisType type)
        {
            return new Axis() { AxisType = type };
        }


        /// <summary>
        ///Gets the name of the axis, which can be either X, Y, X2 or Y2.
        /// </summary>
        public virtual AxisName AxisName
        {
            get { return default(AxisName); }
        }

        /// <summary>
        /// Gets or sets the type of the axis.
        /// </summary>
        /// <value>
        /// The type of the axis.
        /// </value>
        public AxisType AxisType
        {
            get;
            set;
        }

        /// <summary>
        ///Gets a T:System.Windows.Forms.DataVisualization.Charting.CustomLabelsCollection object used to store T:System.Windows.Forms.DataVisualization.Charting.CustomLabel objects.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerType(typeof(IList<CustomLabel>))]
        public CustomLabelsCollection CustomLabels
        {
            get
            {
                return _customLabels;
            }
        }

        /// <summary>
        ///Gets or sets a value that indicates whether an axis is enabled.
        /// </summary>
        public AxisEnabled Enabled
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets a flag that determines if a fixed number of intervals is used on the axis, or if the number of intervals depends on the axis size.
        /// </summary>
        public IntervalAutoMode IntervalAutoMode
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets a flag that determines whether axis labels are automatically fitted.
        /// </summary>
        public bool IsLabelAutoFit
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets a flag which indicates whether the axis is logarithmic. Zeros or negative data values are not allowed on logarithmic charts.
        /// </summary>
        public bool IsLogarithmic
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets a flag that determines whether to add a margin to the axis. 
        /// </summary>
        public bool IsMarginVisible
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the T:System.Windows.Forms.DataVisualization.Charting.LabelStyle properties of an axis.
        /// </summary>
        public AxisLabelStyle LabelStyle
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the maximum value of an axis.
        /// </summary>
        public double Maximum
        {
            get { return _maximum; }
            set
            {
                if (_maximum != value)
                {
                    _maximum = value;

                    OnPropertyChanged("Maximum");
                }
            }
        }
        /// <summary>
        ///Gets or sets the minimum value of an axis.
        /// </summary>
        public double Minimum
        {
            get { return _minimum; }
            set
            {
                if (_minimum != value)
                {
                    _minimum = value;

                    OnPropertyChanged("Minimum");
                }
            }
        }

        /// <summary>
        ///Gets or sets a T:System.Windows.Forms.DataVisualization.Charting.Grid object used to specify the minor grid lines attributes of an axis.
        /// </summary>
        public ChartingGrid MinorGrid
        {
            get { return _minorGrid; }
            set
            {
                if (_minorGrid != null)
                {
                    _minorGrid.ParentElement = null;
                }

                if (value != null)
                {
                    value.ParentElement = this;
                }

                _minorGrid = value;
            }
        }
        /// <summary>
        ///Gets or sets the view of an axis.
        /// </summary>
        public AxisScaleView ScaleView
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets an axis scrollbar.
        /// </summary>
        public AxisScrollBar ScrollBar
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the orientation of the text in the axis title.
        /// </summary>
        public TextOrientation TextOrientation
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the title of the axis.
        /// </summary>
        public string Title
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the length of the gap.
        /// </summary>
        /// <value>
        /// The length of the gap.
        /// </value>
        public double GapLength
        {
            get;
            set;
        }


        /// <summary>
        ///Gets or sets the title font properties of an axis.
        /// </summary>
        public Font TitleFont
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index
        {
            get
            {
                ChartArea chartArea = this.ParentElement as ChartArea;

                if (chartArea != null)
                {
                    return Array.IndexOf(chartArea.Axes, this);
                }

                return -1;
            }
        }
    }
}
