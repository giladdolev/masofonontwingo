﻿namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides data for the <see cref="E:System.Windows.Forms.Control.Layout"/> event. This class cannot be inherited.
    /// </summary>
    /// <filterpriority>2</filterpriority>
    public sealed class LayoutEventArgs : EventArgs
    {
        private readonly IControlElement _affectedComponent;
        private readonly string _affectedProperty;

        /// <summary>
        /// Gets the <see cref="T:System.ComponentModel.Component"/> affected by the layout change.
        /// </summary>
        /// 
        /// <returns>
        /// An <see cref="T:System.ComponentModel.IComponent"/> representing the <see cref="T:System.ComponentModel.Component"/> affected by the layout change.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public IControlElement AffectedComponent
        {
            get
            {
                return this._affectedComponent;
            }
        }

        /// <summary>
        /// Gets the child control affected by the change.
        /// </summary>
        /// 
        /// <returns>
        /// The child <see cref="T:System.Windows.Forms.Control"/> affected by the change.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public ControlElement AffectedControl
        {
            get
            {
                return this._affectedComponent as ControlElement;
            }
        }

        /// <summary>
        /// Gets the property affected by the change.
        /// </summary>
        /// 
        /// <returns>
        /// The property affected by the change.
        /// </returns>
        /// <filterpriority>1</filterpriority>
        public string AffectedProperty
        {
            get
            {
                return this._affectedProperty;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.LayoutEventArgs"/> class with the specified component and property affected.
        /// </summary>
        /// <param name="affectedComponent">The <see cref="T:System.ComponentModel.Component"/> affected by the layout change. </param><param name="affectedProperty">The property affected by the layout change. </param>
        public LayoutEventArgs(IControlElement affectedComponent, string affectedProperty)
        {
            this._affectedComponent = affectedComponent;
            this._affectedProperty = affectedProperty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.LayoutEventArgs"/> class with the specified control and property affected.
        /// </summary>
        /// <param name="affectedControl">The <see cref="T:System.Windows.Forms.Control"/> affected by the layout change.</param><param name="affectedProperty">The property affected by the layout change.</param>
        public LayoutEventArgs(ControlElement affectedControl, string affectedProperty)
            : this((IControlElement)affectedControl, affectedProperty)
        {
        }
    }
}
