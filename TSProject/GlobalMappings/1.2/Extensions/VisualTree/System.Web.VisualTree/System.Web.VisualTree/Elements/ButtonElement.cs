﻿using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements.Touch;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Represents a System.Web.VisualTree button control
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ButtonBaseElement" />
    /// <seealso cref="System.Web.VisualTree.Elements.Touch.ITVisualElement" />
    public class ButtonElement : ButtonBaseElement, ITVisualElement
    {
        private DialogResult _dialogResult = DialogResult.None;
        private bool _enableToggle = false;
        private bool _isPressed = false;
        private string _cssClass = string.Empty;


        /// <summary>
        /// CssClass property.
        /// </summary>
        /// <value>
        /// One or more space separated CSS classes to be applied to the icon element .
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue("")]
        public string CssClass
        {
            get
            {
                return _cssClass;
            }
            set
            {
                if (_cssClass != value)
                {
                    _cssClass = value;

                    OnPropertyChanged("CssClass");
                }
            }
        }



        /// <summary>
        /// PressedChange event handler.
        /// Fires when the 'IsPressed' state of this button changes (only if EnableToggle = true)
        /// </summary>
        private event EventHandler<ButtonPressedChangeEventArgs> _PressedChange;

        /// <summary>
        /// Add or remove PressedChange action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ButtonPressedChangeEventArgs> PressedChange
        {
            add
            {
                bool needNotification = (_PressedChange == null);

                _PressedChange += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("PressedChange");
                }

            }
            remove
            {
                _PressedChange -= value;

                if (_PressedChange == null)
                {
                    OnEventHandlerDeattached("PressedChange");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has PressedChange listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasPressedChangeListeners
        {
            get { return _PressedChange != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:PressedChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="PressedChange"/> instance containing the event data.</param>
        protected virtual void OnPressedChange(ButtonPressedChangeEventArgs args)
        {

            // Check if there are listeners.
            if (_PressedChange != null)
            {
                _PressedChange(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:PressedChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformPressedChange(ButtonPressedChangeEventArgs args)
        {
            if (args != null)
            {
                _isPressed = args.Pressed;
            }

            // Raise the PressedChange event.
            OnPressedChange(args);
        }


        /// <summary>
        /// IsPressed Description.
        /// Get, set the state of the "check button" (only for cases that EnableToggle = true.)
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is pressed; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        public bool IsPressed
        {
            get
            {
                return _isPressed;
            }
            set
            {
                if (_isPressed != value)
                {
                    //Raise PressedChange event .
                    this.PerformPressedChange(new ButtonPressedChangeEventArgs());
                    _isPressed = value;
                    OnPropertyChanged("IsPressed");
                }
            }
        }




        

      
        /// <summary>
        /// EnableToggle Description.
        /// Determine if the button will be "check button" type.
        /// </summary>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool EnableToggle
        {
            get
            {
                return _enableToggle;
            }
            set
            {
                if (_enableToggle != value)
                {
                    _enableToggle = value;
                    if (_enableToggle == false)
                       IsPressed = false;
                    OnPropertyChanged("EnableToggle");
                }
            }
        }









        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonElement"/> class.
        /// </summary>
        public ButtonElement()
        {
            
        }



        /// <summary>
        /// Gets or sets the dialog result.
        /// </summary>
        /// <value>
        /// The dialog result.
        /// </value>
        [DefaultValue(DialogResult.None)]
        public DialogResult DialogResult
        {
            get { return _dialogResult; }
            set { _dialogResult = value; }
        }

        private ScaleConstants _scale = ScaleConstants.Small;


        /// <summary>
        /// Controls the button scale
        /// </summary>
        [DefaultValue(ScaleConstants.Small)]
        [RendererPropertyDescription]
        public ScaleConstants Scale
        {
            get { return _scale; }
            set
            {
                if (_scale != value)
                {
                    _scale = value;
                    OnPropertyChanged("Scale");
                }
            }
        }
    }
}
