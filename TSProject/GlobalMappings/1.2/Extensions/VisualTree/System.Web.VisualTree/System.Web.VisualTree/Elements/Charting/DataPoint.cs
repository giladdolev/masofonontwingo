namespace System.Web.VisualTree.Elements
{
    public class DataPoint : DataPointCustomProperties
    {
        public DataPoint()
        {
            YValues = new double[] { };
        }

        public DataPoint(object xValue, params double[] yValue)
        {
            if (xValue is string)
            {
                this.XValue = 0;
                this.AxisLabel = xValue as string;
            }
            else
            {
                this.XValue = Convert.ToDouble(xValue);
            }
            this.YValues = yValue;
        }

        /// <summary>
        ///Gets or sets the Y-value(s) of a data point.
        /// </summary>
        public double[] YValues
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the X-value of a data point.
        /// </summary>
        public double XValue
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets the Y-value of a data point.
        /// </summary>
        public double? YValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string AxisLabel
        {
            get;
            set;
        }


    }
}
