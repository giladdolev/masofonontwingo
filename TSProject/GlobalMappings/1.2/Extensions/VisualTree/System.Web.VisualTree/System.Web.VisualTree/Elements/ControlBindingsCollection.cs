﻿namespace System.Web.VisualTree.Elements
{
    public class ControlBindingsCollection : BindingsCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlBindingsCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal ControlBindingsCollection(VisualElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
        {

        }
    }
}
