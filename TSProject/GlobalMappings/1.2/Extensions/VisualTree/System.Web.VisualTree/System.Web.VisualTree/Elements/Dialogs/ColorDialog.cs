using System.Drawing;

namespace System.Web.VisualTree.Elements
{
	public class ColorDialog : CommonDialog
	{
		public Color Color
		{
			get;
			set;
		}
	}
}
