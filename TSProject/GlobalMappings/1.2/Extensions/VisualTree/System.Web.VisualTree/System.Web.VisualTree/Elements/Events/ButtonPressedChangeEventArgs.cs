using System.Drawing;

namespace System.Web.VisualTree.Elements
{
	public class ButtonPressedChangeEventArgs : EventArgs
	{

        private bool _pressed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonPressedChangeEventArgs"/> class.
        /// </summary>
        public ButtonPressedChangeEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonPressedChangeEventArgs"/> class.
        /// </summary>
        public ButtonPressedChangeEventArgs(bool isPress)
		{
            Pressed = isPress;
		}

		/// <summary>
        /// Gets or sets the pressed State.
		/// </summary>
		/// <value>
        /// The pressed.
		/// </value>
        public bool Pressed
		{
            get { return _pressed; }
            set { _pressed = value; }
		}
	}
}
