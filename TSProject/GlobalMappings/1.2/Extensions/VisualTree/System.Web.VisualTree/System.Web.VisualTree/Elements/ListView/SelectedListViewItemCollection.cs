﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace System.Web.VisualTree.Elements
{
    public class SelectedListViewItemCollection : Collection<ListViewItem>
    {
        public SelectedListViewItemCollection(IList<ListViewItem> list)
            : base(list)
        {

        }

        public SelectedListViewItemCollection()
            : base()
        {
        }

    }
}
