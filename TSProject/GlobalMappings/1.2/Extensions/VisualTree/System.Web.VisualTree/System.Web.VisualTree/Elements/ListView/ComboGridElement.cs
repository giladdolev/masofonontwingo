﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class ComboGridElement : GridElement, IBindingViewModel
    {
        private string _displayMember = "";
        private string _valueMember = "";
        private bool _droppedDown = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboGridElement"/> class.
        /// </summary>
        public ComboGridElement()
        {
            this.WaitMaskDisabled = true;
        }

        /// <summary>
        /// Gets or sets the display member.
        /// </summary>
        /// <value>
        /// The display member.
        /// </value>
        [RendererPropertyDescription]
        public string DisplayMember
        {
            get
            {
                return _displayMember;
            }
            set
            {
                _displayMember = value;
                OnPropertyChanged("DisplayMember");
            }
        }

        /// <summary>
        /// Gets or sets the value member.
        /// </summary>
        /// <value>
        /// The value member.
        /// </value>
        [RendererPropertyDescription]
        public string ValueMember
        {
            get
            {
                return _valueMember;
            }
            set
            {
                _valueMember = value;
                OnPropertyChanged("ValueMember");
            }

        }

        /// <summary>
        /// SelectedIndexChange event handler.
        /// </summary>
        private event EventHandler<GridCellMouseEventArgs> _selectedIndexChange;

        /// <summary>
        /// Add or remove SelectedIndexChange action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<GridCellMouseEventArgs> SelectedIndexChange
        {
            add
            {
                bool needNotification = (_selectedIndexChange == null);

                _selectedIndexChange += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChange");
                }

            }
            remove
            {
                _selectedIndexChange -= value;

                if (_selectedIndexChange == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChange");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChange"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChange(GridCellMouseEventArgs args)
        {

            // Check if there are listeners.
            if (_selectedIndexChange != null)
            {
                _selectedIndexChange(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChange" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSelectedIndexChange(GridCellMouseEventArgs args)
        {
            if (args != null && args.RowIndex >= 0 && args.RowIndex < this.Rows.Count)
            {
                if (this.SelectedIndex != -1)
                {
                    PreviousValue = this.Rows[this.SelectedIndex].Cells[this.ValueMember].Value;
                }
                var raise = _selectedIndex != args.RowIndex;
                UpdateSelectedTextValue(args.RowIndex);

                if (raise)
                {
                    // Raise the SelectedIndexChange event.
                    OnSelectedIndexChange(args);
                }
            }

        }

        /// <summary>
        /// Update Selected text,value,item .
        /// </summary>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="itemGound">if set to <c>true</c> [item gound].</param>
        private void UpdateSelectedTextValue(int rowIndex, bool itemGound = true)
        {

            var item = new ListItem();

            if (rowIndex > -1 && Rows.Count > rowIndex && !string.IsNullOrEmpty(DisplayMember) && !string.IsNullOrEmpty(ValueMember) && itemGound == true)
            {
                var cells = Rows[rowIndex].Cells;
                // update related properties of ListItem
                item.Value = cells[ValueMember].Value;
                item.Text = Convert.ToString(cells[DisplayMember].Value);

                // update related properties of ComboGrid
                _selectedItem = item;
                _selectedText = item.Text;
                _selectedValue = item.Value;
                _selectedIndex = rowIndex;
                Text = item.Text;
            }
            else
            {
                // reset related properties of ComboGrid
                Text = "";
                _selectedItem = null;
                _selectedText = "";
                _selectedValue = "";
                _selectedIndex = -1;
            }
        }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        private ListItem _selectedItem = null;

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        /// <value>
        /// The selected item.
        /// </value>
        public ListItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                //if (_selectedItem == value)
                //{
                //    return;
                //}
                var index = 0;
                bool itemFound = false;
                if (Rows.Count > -1)
                {
                    foreach (var row in Rows)
                    {
                        if (Convert.ToString(row.Cells[DisplayMember].Value) == value.Text &&
                            Convert.ToString(row.Cells[ValueMember].Value) == Convert.ToString(value.Value))
                        {
                            itemFound = true;
                            break;
                        }
                        index++;
                    }
                    UpdateSelectedTextValue(index, itemFound);
                }
            }
        }

        private string _selectedText = "";
        /// <summary>
        /// Get ,set SelectedText property .
        /// </summary>
        public string SelectedText
        {
            get
            {
                return this._selectedText;
            }
            set
            {
                //if (_selectedText == value)
                //{
                //    return;
                //}
                var index = 0;
                bool itemFound = false;
                if (Rows.Count > -1)
                {
                    foreach (var row in Rows)
                    {
                        if (Convert.ToString(row.Cells[DisplayMember].Value) == value)
                        {
                            itemFound = true;
                            break;
                        }
                        index++;
                    }

                    UpdateSelectedTextValue(index, itemFound);
                }

            }
        }

        private object _selectedValue = null;
        /// <summary>
        /// Gets or sets the selected value.
        /// </summary>
        /// <value>
        /// The selected value.
        /// </value>
        public object SelectedValue
        {
            get
            {
                return this._selectedValue;
            }
            set
            {
                //if (_selectedValue == value)
                //{
                //    return;
                //}
                var index = 0;
                bool itemFound = false;
                if (Rows.Count > -1)
                {
                    foreach (var row in Rows)
                    {
                        if (Equals(row.Cells[ValueMember].Value, value))
                        {
                            itemFound = true;
                            break;
                        }
                        index++;
                    }
                    UpdateSelectedTextValue(index, itemFound);
                }
            }
        }

        private int _selectedIndex = -1;
        /// <summary>
        /// Gets or sets the index of the selected.
        /// </summary>
        /// <value>
        /// The index of the selected.
        /// </value>
        public int SelectedIndex
        {
            get { return this._selectedIndex; }
            set
            {
                if (_selectedIndex != value)
                {
                    this._selectedIndex = value;
                    GridCellMouseEventArgs args = new GridCellMouseEventArgs(0, this._selectedIndex);
                    PerformSelectedIndexChange(args);
                    SetSelection();
                }
            }
        }

        /// <summary>
        /// Select row from grid element
        /// </summary>
        [RendererMethodDescription]
        private void SetSelection()
        {
            this.InvokeMethodOnce("SetSelection");
        }


        /// <summary>
        /// Get ,set DroppedDown property .
        /// </summary>
        /// <value>
        /// true if the drop down list is dropped , otherwise false .
        /// </value>
        public bool DroppedDown
        {
            get
            {
                return _droppedDown;
            }
            set
            {
                _droppedDown = value;
                // If the _droppedDown is true .
                if (_droppedDown)
                {
                    // expand the drop down list 
                    ExpandDropDownList();
                }
            }
        }

        /// <summary>
        /// Expand the drop down list .
        /// </summary>
        [RendererMethodDescription]
        private void ExpandDropDownList()
        {
            this.InvokeMethodOnce("ExpandDropDownList");
        }

        private bool _showHeader = false;
        /// <summary>
        /// Gets or sets a value indicating whether column headers visible .
        /// </summary>
        /// <value>
        /// true if column headers visible, otherwise false.
        /// </value>

        public bool ShowHeader
        {
            get { return _showHeader; }
            set
            {
                if (_showHeader != value)
                {
                    _showHeader = value;
                }
            }
        }

        /// <summary>
        /// SowHeaderMethod  .
        /// </summary>
        [RendererMethodDescription]
        private void SowHeaderMethod()
        {
            this.InvokeMethodOnce("SowHeaderMethod");
        }

        /// <summary>
        /// Get , set PreviousValue
        /// </summary>
        public object PreviousValue
        {
            get;
            set;
        }

        /// <summary>
        /// SelectedIndexChanged event handler.
        /// </summary>
        private event EventHandler _selectedIndexChanged;

        /// <summary>
        /// Add or remove SelectedIndexChanged action.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler SelectedIndexChanged
        {
            add
            {
                bool needNotification = (_selectedIndexChanged == null);

                _selectedIndexChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedIndexChanged");
                }

            }
            remove
            {
                _selectedIndexChanged -= value;

                if (_selectedIndexChanged == null)
                {
                    OnEventHandlerDeattached("SelectedIndexChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has SelectedIndexChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasSelectedIndexChangedListeners
        {
            get { return _selectedIndexChange != null; }
        }


        /// <summary>
        /// Raises the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedIndexChanged"/> instance containing the event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_selectedIndexChanged != null)
            {
                _selectedIndexChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:SelectedIndexChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformSelectedIndexChanged(EventArgs args)
        {
            // Try to get string value changed arguments
            ValueChangedArgs<int> valueChangedArgs = args as ValueChangedArgs<int>;

            // If there is a valid string value changed arguments

            if (valueChangedArgs != null)
            {
                int value = valueChangedArgs.Value;

                // Raise the selected index changed event
                OnSelectedIndexChanged(valueChangedArgs);

            }
        }

        private bool _expandOnFocus = false;
        /// <summary>
        /// Gets or Sets a value indicating whether this component will expand on focus.
        /// </summary>
        /// <value>
        /// <c>true</c> if this component will exapnd on focus; otherwise, <c>false</c>.
        /// </value>
        [RendererPropertyDescription]
        public bool ExpandOnFocus
        {
            get
            {
                return _expandOnFocus;
            }
            set
            {
                if (_expandOnFocus != value)
                {
                    _expandOnFocus = value;
                    OnPropertyChanged("ExpandOnFocus");
                }
            }
        }

    }
}
