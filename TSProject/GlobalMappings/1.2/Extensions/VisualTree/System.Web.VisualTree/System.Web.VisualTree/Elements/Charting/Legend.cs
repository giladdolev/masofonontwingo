﻿namespace System.Web.VisualTree.Elements
{
    public enum LegendPosition
    {
        Top,
        Bottom,
        Left,
        Right
    }

    public class Legend : ChartNamedElement
    {
        private bool _enabled;
        private LegendPosition _position;

        public Legend()
        {

        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Legend"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    OnPropertyChanged("Enabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public LegendPosition Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged("Position");
                }
            }
        }


    }
}
