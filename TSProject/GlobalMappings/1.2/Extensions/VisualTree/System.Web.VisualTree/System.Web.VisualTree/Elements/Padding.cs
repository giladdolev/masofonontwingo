using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Globalization;
using System.Reflection;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    ///     Represents padding or margin information associated with a user interface (UI)
    ///     element.
    /// </summary>
    [TypeConverter(typeof(PaddingConverter))]
    public class Padding
    {
        private bool _all;
        private int _top;
        private int _left;
        private int _right;
        private int _bottom;

        /// <summary>
        /// Provides a System.Web.VisualTree.Elements.Padding object with no padding.
        /// </summary>
        public static readonly Padding Empty = new Padding(0);

        /// <summary>
        /// Initializes a new instance of the <see cref="Padding"/> class.
        /// </summary>
        /// <param name="all">All.</param>
        public Padding(int all)
        {
            _all = true;
            _top = _left = _right = _bottom = all;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Padding"/> class.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="top">The top.</param>
        /// <param name="right">The right.</param>
        /// <param name="bottom">The bottom.</param>
        public Padding(int left, int top, int right, int bottom)
        {
            _top = top;
            _left = left;
            _right = right;
            _bottom = bottom;
            _all = ((_top == _left) && (_top == _right)) && (_top == _bottom);
        }

        /// <summary>
        /// Gets or sets the padding value for all the edges.
        /// </summary>
        /// <value>
        /// The padding, in pixels, for all edges if the same; otherwise, -1.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(0)]
        public int All
        {
            get
            {
                if (!_all)
                {
                    return -1;
                }
                return _top;
            }
            set
            {
                if (!_all || (_top != value))
                {
                    _all = true;
                    _top = _left = _right = _bottom = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the padding value for the bottom edge.
        /// </summary>
        /// <value>
        /// The padding, in pixels, for the bottom edge.
        /// </value>
        [DefaultValue(0)]
        public int Bottom
        {
            get
            {
                if (_all)
                {
                    return _top;
                }
                return _bottom;
            }
            set
            {
                if (_all || (_bottom != value))
                {
                    _all = false;
                    _bottom = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the padding value for the left edge.
        /// </summary>
        /// <value>
        /// The padding, in pixels, for the left edge.
        /// </value>
        [DefaultValue(0)]
        public int Left
        {
            get
            {
                if (_all)
                {
                    return _top;
                }
                return _left;
            }
            set
            {
                if (_all || (_left != value))
                {
                    _all = false;
                    _left = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the padding value for the right edge.
        /// </summary>
        /// <value>
        /// The padding, in pixels, for the right edge.
        /// </value>
        [DefaultValue(0)]
        public int Right
        {
            get
            {
                if (_all)
                {
                    return _top;
                }
                return _right;
            }
            set
            {
                if (_all || (_right != value))
                {
                    _all = false;
                    _right = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the padding value for the top edge.
        /// </summary>
        /// <value>
        /// The padding, in pixels, for the top edge.
        /// </value>
        [DefaultValue(0)]
        public int Top
        {
            get
            {
                return _top;
            }
            set
            {
                if (_all || (_top != value))
                {
                    _all = false;
                    _top = value;
                }
            }
        }

        /// <summary>
        /// Gets the combined padding for the right and left edges.
        /// </summary>
        /// <value>
        /// Gets the sum, in pixels, of the System.Web.VisualTree.Elements.Padding.Left and System.Web.VisualTree.Elements.Padding.Right
        ///     padding values.
        /// </value>
        public int Horizontal
        {
            get
            {
                return (Left + Right);
            }
        }

        /// <summary>
        /// Gets the combined padding for the top and bottom edges.
        /// </summary>
        /// <value>
        ///     Gets the sum, in pixels, of the System.Web.VisualTree.Elements.Padding.Top and System.Web.VisualTree.Elements.Padding.Bottom
        ///     padding values.
        /// </value>
        public int Vertical
        {
            get
            {
                return (Top + Bottom);
            }
        }

        /// <summary>
        /// Gets the padding information in the form of a System.Drawing.Size.
        /// </summary>
        /// <value>
        /// A System.Drawing.Size containing the padding information.
        /// </value>
        public Size Size
        {

            get
            {
                return new Size(Horizontal, Vertical);
            }
        }

        /// <summary>
        /// Computes the sum of the two specified System.Web.VisualTree.Elements.Padding values
        /// </summary>
        /// <param name="p1">The System.Web.VisualTree.Elements.Padding.</param>
        /// <param name="p2">The System.Web.VisualTree.Elements.Padding.</param>
        /// <returns>A System.Web.VisualTree.Elements.Padding that contains the sum of the two specified System.Web.VisualTree.Elements.Padding
        ///     values.</returns>
        public static Padding Add(Padding p1, Padding p2)
        {
            return (p1 + p2);
        }

        /// <summary>
        /// Subtracts one specified System.Web.VisualTree.Elements.Padding value from another
        /// </summary>
        /// <param name="p1">The System.Web.VisualTree.Elements.Padding.</param>
        /// <param name="p2">The System.Web.VisualTree.Elements.Padding.</param>
        /// <returns>A System.Web.VisualTree.Elements.Padding that contains the result of the subtraction of one specified System.Web.VisualTree.Elements.Padding from another
        /// </returns>
        public static Padding Subtract(Padding p1, Padding p2)
        {
            return (p1 - p2);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object other)
        {
            return ((other is Padding) && (((Padding)other) == this));
        }

        /// <summary>
        /// Performs vector addition on the two specified System.Web.VisualTree.Elements.Padding objects,
        ///     resulting in a new System.Web.VisualTree.Elements.Padding.
        /// </summary>
        /// <param name="p1">The first System.Web.VisualTree.Elements.Padding to add.</param>
        /// <param name="p2">The second System.Web.VisualTree.Elements.Padding to add.</param>
        /// <returns>
        /// A new System.Web.VisualTree.Elements.Padding that results from adding p1 and p2.
        /// </returns>
        public static Padding operator +(Padding p1, Padding p2)
        {
            return new Padding(p1.Left + p2.Left, p1.Top + p2.Top, p1.Right + p2.Right, p1.Bottom + p2.Bottom);
        }

        /// <summary>
        /// Performs vector subtraction on the two specified System.Web.VisualTree.Elements.Padding
        ///     objects, resulting in a new System.Web.VisualTree.Elements.Padding.
        /// </summary>
        /// <param name="p1">The first System.Web.VisualTree.Elements.Padding to add.</param>
        /// <param name="p2">The second System.Web.VisualTree.Elements.Padding to add.</param>
        /// <returns>
        /// The System.Web.VisualTree.Elements.Padding result of subtracting p2 from p1.
        /// </returns>
        public static Padding operator -(Padding p1, Padding p2)
        {
            return new Padding(p1.Left - p2.Left, p1.Top - p2.Top, p1.Right - p2.Right, p1.Bottom - p2.Bottom);
        }

        /// <summary>
        /// Tests whether two specified System.Web.VisualTree.Elements.Padding objects are equivalent.
        /// </summary>
        /// <param name="p1">The first System.Web.VisualTree.Elements.Padding to add.</param>
        /// <param name="p2">The second System.Web.VisualTree.Elements.Padding to add.</param>
        /// <returns>
        /// true if the two System.Web.VisualTree.Elements.Padding objects are equal; otherwise, false.
        /// </returns>
        public static bool operator ==(Padding p1, Padding p2)
        {
            return ((((p1.Left == p2.Left) && (p1.Top == p2.Top)) && (p1.Right == p2.Right)) && (p1.Bottom == p2.Bottom));
        }

        /// <summary>
        /// Tests whether two specified System.Web.VisualTree.Elements.Padding objects are not equivalent.
        /// </summary>
        /// <param name="p1">The first System.Web.VisualTree.Elements.Padding to add.</param>
        /// <param name="p2">The second System.Web.VisualTree.Elements.Padding to add.</param>
        /// <returns>
        /// true if the two System.Web.VisualTree.Elements.Padding objects are different; otherwise,
        ///     false.
        /// </returns>
        public static bool operator !=(Padding p1, Padding p2)
        {
            return !(p1 == p2);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return (((Left ^ RotateLeft(Top, 8)) ^ RotateLeft(Right, 0x10)) ^ RotateLeft(Bottom, 0x18));
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return ("{Left=" + Left.ToString(CultureInfo.CurrentCulture) + ",Top=" + Top.ToString(CultureInfo.CurrentCulture) + ",Right=" + Right.ToString(CultureInfo.CurrentCulture) + ",Bottom=" + Bottom.ToString(CultureInfo.CurrentCulture) + "}");
        }

        /// <summary>
        /// Rotates the left.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="nBits">The n bits.</param>
        /// <returns></returns>
        private static int RotateLeft(int value, int nBits)
        {
            nBits = nBits % 0x20;
            return ((value << nBits) | (value >> (0x20 - nBits)));
        }

        /// <summary>
        /// Resets all.
        /// </summary>
        private void ResetAll()
        {
            All = 0;
        }

        /// <summary>
        /// Resets the bottom.
        /// </summary>
        private void ResetBottom()
        {
            Bottom = 0;
        }

        /// <summary>
        /// Resets the left.
        /// </summary>
        private void ResetLeft()
        {
            Left = 0;
        }

        /// <summary>
        /// Resets the right.
        /// </summary>
        private void ResetRight()
        {
            Right = 0;
        }

        /// <summary>
        /// Resets the top.
        /// </summary>
        private void ResetTop()
        {
            Top = 0;
        }

        /// <summary>
        /// Scales the specified dx.
        /// </summary>
        /// <param name="dx">The dx.</param>
        /// <param name="dy">The dy.</param>
        public void Scale(float dx, float dy)
        {
            _top = (int)(_top * dy);
            _left = (int)(_left * dx);
            _right = (int)(_right * dx);
            _bottom = (int)(_bottom * dy);
        }

        /// <summary>
        /// Should serialize all.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldSerializeAll()
        {
            return _all;
        }
    }

    /// <summary>
    /// Provides a type converter to convert <see cref="T:System.Web.VisualTree.Elements.Padding"/> values to and from various other representations.
    /// </summary>
    public class PaddingConverter : TypeConverter
    {
        /// <summary>
        /// Returns whether this converter can convert an object of one type to the type of this converter.
        /// </summary>
        /// <returns>
        /// true if this object can perform the conversion; otherwise, false.
        /// </returns>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.
        /// </param>
        /// <param name="sourceType">A <see cref="T:System.Type"/> that represents the type you wish to convert from.
        /// </param>
        public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Returns whether this converter can convert the object to the specified type, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="destinationType">A <see cref="T:System.Type" /> that represents the type you want to convert to.</param>
        /// <returns>
        /// true if this converter can perform the conversion; otherwise, false.
        /// </returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, System.Type destinationType)
        {
            if (destinationType == typeof(InstanceDescriptor))
                return true;
            else
                return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// Converts the given object to the type of this converter, using the specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo" /> to use as the current culture.</param>
        /// <param name="value">The <see cref="T:System.Object" /> to convert.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that represents the converted value.
        /// </returns>
        /// <exception cref="System.ArgumentException"></exception>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string str1 = value as string;
            if (str1 == null)
                return base.ConvertFrom(context, culture, value);

            string str2 = str1.Trim();
            if (str2.Length == 0)
                return null;

            if (culture == null)
                culture = CultureInfo.CurrentCulture;

            string str3 = str2;
            string[] strArray = str3.Split(',');
            int[] numArray = new int[strArray.Length];
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(int));
            for (int index2 = 0; index2 < numArray.Length; ++index2)
                numArray[index2] = (int)converter.ConvertFromString(context, culture, strArray[index2]);
            if (numArray.Length == 4)
                return new Padding(numArray[0], numArray[1], numArray[2], numArray[3]);
            else if (numArray.Length == 1)
                return new Padding(numArray[0]);

            throw new ArgumentException(String.Format("Parse of Text(\"{0}\") expected text in the format \"left, top, right, bottom\" or \"all\" did not succeed.", str2), "value");
        }

        /// <summary>
        /// Converts the given value object to the specified type, using the specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="culture">A <see cref="T:System.Globalization.CultureInfo" />. If null is passed, the current culture is assumed.</param>
        /// <param name="value">The <see cref="T:System.Object" /> to convert.</param>
        /// <param name="destinationType">The <see cref="T:System.Type" /> to convert the <paramref name="value" /> parameter to.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that represents the converted value.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">destinationType</exception>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, System.Type destinationType)
        {
            if (destinationType == (System.Type)null)
                throw new ArgumentNullException("destinationType");
            if (value is Padding)
            {
                if (destinationType == typeof(string))
                {
                    Padding padding = (Padding)value;
                    if (culture == null)
                        culture = CultureInfo.CurrentCulture;
                    string separator = culture.TextInfo.ListSeparator + " ";
                    TypeConverter converter = TypeDescriptor.GetConverter(typeof(int));
                    string[] strArray1 = new string[4];
                    int num1 = 0;
                    string[] strArray2 = strArray1;
                    int index1 = num1;
                    int num2 = 1;
                    int num3 = index1 + num2;
                    string str1 = converter.ConvertToString(context, culture, (object)padding.Left);
                    strArray2[index1] = str1;
                    string[] strArray3 = strArray1;
                    int index2 = num3;
                    int num4 = 1;
                    int num5 = index2 + num4;
                    string str2 = converter.ConvertToString(context, culture, (object)padding.Top);
                    strArray3[index2] = str2;
                    string[] strArray4 = strArray1;
                    int index3 = num5;
                    int num6 = 1;
                    int num7 = index3 + num6;
                    string str3 = converter.ConvertToString(context, culture, (object)padding.Right);
                    strArray4[index3] = str3;
                    string[] strArray5 = strArray1;
                    int index4 = num7;
                    int num8 = 1;
                    int num9 = index4 + num8;
                    string str4 = converter.ConvertToString(context, culture, (object)padding.Bottom);
                    strArray5[index4] = str4;
                    return (object)string.Join(separator, strArray1);
                }
                else if (destinationType == typeof(InstanceDescriptor))
                {
                    Padding padding = (Padding)value;
                    if (padding.ShouldSerializeAll())
                    {
                        System.Type type1 = typeof(Padding);
                        System.Type[] types = new System.Type[1];
                        int index1 = 0;
                        System.Type type2 = typeof(int);
                        types[index1] = type2;
                        // ISSUE: explicit non-virtual call
                        ConstructorInfo constructor = type1.GetConstructor(types);
                        object[] objArray = new object[1];
                        int index2 = 0;
                        objArray[index2] = padding.All;
                        return (object)new InstanceDescriptor((MemberInfo)constructor, (ICollection)objArray);
                    }
                    else
                    {
                        System.Type type1 = typeof(Padding);
                        System.Type[] types = new System.Type[4];
                        int index1 = 0;
                        System.Type type2 = typeof(int);
                        types[index1] = type2;
                        int index2 = 1;
                        System.Type type3 = typeof(int);
                        types[index2] = type3;
                        int index3 = 2;
                        System.Type type4 = typeof(int);
                        types[index3] = type4;
                        int index4 = 3;
                        System.Type type5 = typeof(int);
                        types[index4] = type5;
                        // ISSUE: explicit non-virtual call
                        ConstructorInfo constructor = type1.GetConstructor(types);
                        object[] objArray = new object[4] {padding.Left, padding.Top, padding.Right, padding.Bottom};
                        return new InstanceDescriptor(constructor, objArray);
                    }
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Creates an instance of the type that this <see cref="T:System.ComponentModel.TypeConverter" /> is associated with, using the specified context, given a set of property values for the object.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="propertyValues">An <see cref="T:System.Collections.IDictionary" /> of new property values.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> representing the given <see cref="T:System.Collections.IDictionary" />, or null if the object cannot be created. This method always returns null.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// context
        /// or
        /// propertyValues
        /// </exception>
        public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (propertyValues == null)
                throw new ArgumentNullException("propertyValues");
            Padding padding = (Padding)context.PropertyDescriptor.GetValue(context.Instance);
            int all = (int)propertyValues[(object)"All"];
            if (padding.All != all)
                return new Padding(all);
            else
                return new Padding((int)propertyValues[(object)"Left"], (int)propertyValues[(object)"Top"], (int)propertyValues[(object)"Right"], (int)propertyValues[(object)"Bottom"]);
        }

        /// <summary>
        /// Returns whether changing a value on this object requires a call to <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" /> to create a new value, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <returns>
        /// true if changing a property on this object requires a call to <see cref="M:System.ComponentModel.TypeConverter.CreateInstance(System.Collections.IDictionary)" /> to create a new value; otherwise, false.
        /// </returns>
        public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// Returns a collection of properties for the type of array specified by the value parameter, using the specified context and attributes.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="value">An <see cref="T:System.Object" /> that specifies the type of array for which to get properties.</param>
        /// <param name="attributes">An array of type <see cref="T:System.Attribute" /> that is used as a filter.</param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> with the properties that are exposed for this data type, or null if there are no properties.
        /// </returns>
        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(Padding), attributes);
            string[] names = new string[5] { "All", "Left", "Top", "Right", "Bottom" };
            return properties.Sort(names);
        }

        /// <summary>
        /// Returns whether this object supports properties, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <returns>
        /// true if <see cref="M:System.ComponentModel.TypeConverter.GetProperties(System.Object)" /> should be called to find the properties of this object; otherwise, false.
        /// </returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
    }
}
