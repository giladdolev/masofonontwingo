﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class GridComboBoxCellSelectionData
    {
        /// <summary>
        /// Cell's Row index
        /// </summary>
        public int RowIndex { get; set; }

        /// <summary>
        /// Cell's Column index
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// Cell's Combobox selected index
        /// </summary>
        public int SelectedIndex { get; set; }


        /// <summary>
        /// Default constructor
        /// </summary>
        public GridComboBoxCellSelectionData()
        {

        }

        /// <summary>
        /// GridCellSelectionData with parameters
        /// </summary>
        /// <param name="rowIndex">Row index</param>
        /// <param name="colIndex">Coloumn Index</param>
        /// <param name="selectedIndex">Selected Index</param>
        public GridComboBoxCellSelectionData(int rowIndex, int colIndex, int selectedIndex)
        {
            RowIndex = rowIndex;
            ColumnIndex = colIndex;
            SelectedIndex = selectedIndex;
        }
    }
}
