﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class WidgetColumnMultiComboBoxElement : GridMultiComboBoxElement
    {
        private bool _hasItemChanged = false;
        public bool HasItemChanged
        {
            get
            {
                return this._hasItemChanged;
            }
            set
            {
                this._hasItemChanged = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has key press listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has key press listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasKeyPressListeners
        {
            get { return true; }
        }
        public override bool HasWidgetControlItemChangedListeners
        {
            get
            {
                return HasItemChanged;
            }
        }
        private bool _hasEditChange = false;
        public override bool HasWidgetControlEditChangedListeners
        {
            get
            {
                return HasEditChange;
            }
        }

        public bool HasEditChange
        {
            get
            {
                return this._hasEditChange;
            }
            set
            {
                this._hasEditChange = value;
            }

        }

        public override void Focus()
        {
            WidgetColumn wdColunm = this.Parent as WidgetColumn;
            GridElement grd = null;
            if (wdColunm != null)
            {
                grd = wdColunm.Parent as GridElement;
                if (grd != null)
                {
                    wdColunm.SetFocus(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex() });

                }
            }

        }

        private bool _enabled = true;
        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public override bool Enabled
        {
            get { return _enabled; }
            set
            {
                // If is a different value
              //  if (_enabled != value)
               // {
                    // Set enabled
                    _enabled = value;

                    WidgetColumn wdColunm = this.Parent as WidgetColumn;
                    GridElement grd = null;
                    if (wdColunm != null)
                    {
                        grd = wdColunm.Parent as GridElement;
                        if (grd != null)
                        {
                            wdColunm.SetEnabled(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex(), Enabled = value });
                        }
                    }
               // }
            }
        }

        private string _store = null;
        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public override string Store
        {
            get { return _store; }
            set
            {
                
                _store = value;

                WidgetColumn wdColunm = this.Parent as WidgetColumn;
                GridElement grd = null;
                if (wdColunm != null)
                {
                    grd = wdColunm.Parent as GridElement;
                    if (grd != null)
                    {
                        wdColunm.SetStore(new WidgetColumnData() { DataMember = this.DataMember, SelectedRowIndex = this.GetRowIndex(), Grid = grd, SelectedColumnIndex = grd.Columns.IndexOf(wdColunm), WidgetControlIndex = this.GetWidgetControlIndex(), Store = value });
                    }
                }
                // }
            }
        }
    }
}
