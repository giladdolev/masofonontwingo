using System.Collections.Generic;
using System.Web.VisualTree.Utilities;

namespace System.Web.VisualTree.Elements
{
	public class ToolBarItemCollection : VisualElementCollection<ToolBarItem>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarItemCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
		internal ToolBarItemCollection(ToolBarElement objParentElement, string propertyName) : base(objParentElement, propertyName)
		{

		}



        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarItemCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The object parent element.</param>
        /// <param name="propertyName">The property name.</param>
        public ToolBarItemCollection(ToolBarItem objParentElement, string propertyName)
            : base(objParentElement, propertyName)
		{

		}


        /// <summary>
        ///Adds the specified item to the end of the collection.
        /// </summary>
        /// <param name="value">The T:System.Windows.Forms.ToolStripItem to add to the end of the collection. </param>as
        ///  <returns>An T:System.Int32 representing the zero-based index of the new item in the collection.</returns>
        public int Add(ToolBarItem value)
        {
            base.Add(value);
            return this.Count;
        }

        /// <summary>
        /// Finds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="searchAllChildren">if set to <c>true</c> search all children.</param>
        /// <returns></returns>
        public ToolBarItem[] Find(string key, bool searchAllChildren)
        {
            List<ToolBarItem> list = FindInternal(key, searchAllChildren, this, new List<ToolBarItem>());
            return list.ToArray();
        }

        /// <summary>
        /// Finds the internal.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="searchAllChildren">if set to <c>true</c> search all children.</param>
        /// <param name="toolBarItemCollectionToLookIn">The tree item collection to look in.</param>
        /// <param name="foundTreeItems">The found tree items.</param>
        /// <returns></returns>
        private List<ToolBarItem> FindInternal(string key, bool searchAllChildren, ToolBarItemCollection toolBarItemCollectionToLookIn, List<ToolBarItem> foundTreeItems)
        {
            if ((toolBarItemCollectionToLookIn == null) || (foundTreeItems == null))
            {
                return null;
            }
            for (int i = 0; i < toolBarItemCollectionToLookIn.Count; i++)
            {
                if ((toolBarItemCollectionToLookIn[i] != null) && Strings.SafeCompareStrings(toolBarItemCollectionToLookIn[i].ID, key, true))
                {
                    foundTreeItems.Add(toolBarItemCollectionToLookIn[i]);
                }
            }
            if (searchAllChildren)
            {
                for (int j = 0; j < toolBarItemCollectionToLookIn.Count; j++)
                {
                    if (((toolBarItemCollectionToLookIn[j] != null) && (toolBarItemCollectionToLookIn[j].Items != null)) && (toolBarItemCollectionToLookIn[j].Items.Count > 0))
                    {
                        foundTreeItems = FindInternal(key, searchAllChildren, toolBarItemCollectionToLookIn[j].Items, foundTreeItems);
                    }
                }
            }
            return foundTreeItems;
        }


	}
}
