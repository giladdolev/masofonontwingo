﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    public class RadioGroupItemElement : RadioButtonElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItemElement"/> class.
        /// </summary>
        public RadioGroupItemElement()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItemElement"/> class.
        /// </summary>
        public RadioGroupItemElement(object value, string text)
        {
            _value = value;
            Text = text;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItemElement"/> class.
        /// </summary>
        public RadioGroupItemElement(object value, string text, bool enabled)
        {
            _value = value;
            Text = text;
            Enabled = enabled;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioGroupItemElement"/> class.
        /// </summary>
        public RadioGroupItemElement(object value, string text, bool enabled, object tag)
        {
            _value = value;
            Text = text;
            Enabled = enabled;
            Tag = tag;
        }
     

        private object _value;

        [RendererPropertyDescription]
         /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value != value)
                {
                    _value = value;

                    OnPropertyChanged("Value");
                }
            }
        }


    }
}
