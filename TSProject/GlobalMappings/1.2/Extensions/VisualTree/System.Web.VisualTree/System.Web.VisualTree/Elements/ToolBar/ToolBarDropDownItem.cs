using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public abstract class ToolBarDropDownItem : ToolBarItem
	{

        /// <summary>
        /// The drop down items
        /// </summary>
        private readonly ToolBarItemCollection _dropDownItems = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="ToolBarDropDownItem"/> class.
        /// </summary>
        public ToolBarDropDownItem()
        {
            _dropDownItems = new ToolBarItemCollection(this, "DropDownItems");
        }



        /// <summary>
        /// Gets the drop down items.
        /// </summary>
        /// <value>
        /// The drop down items.
        /// </value>
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        [DesignerType(typeof(IList<ToolBarItem>))]
        public ToolBarItemCollection DropDownItems
        {
            get
            {
                return _dropDownItems;
            }
        }



        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                return DropDownItems;
            }
        }



        /// <summary>
        /// Gets the child visual element.
        /// </summary>
        /// <typeparam name="TVisualElement">The type of the visual element.</typeparam>
        /// <param name="strVisualElementID">The visual element identifier.</param>
        /// <returns></returns>
        internal override TVisualElement GetChildVisualElement<TVisualElement>(string strVisualElementID)
        {
            // Loop all child controls
            foreach (VisualElement visualElement in _dropDownItems)
            {
                // If there is a valid child control
                if (IsMatchingVisualElement<TVisualElement>(visualElement, strVisualElementID))
                {
                    // Return element
                    return visualElement as TVisualElement;
                }
            }

            return null;
        }


        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement,bool insert = false)
        {
            // Get toolbar item
            ToolBarItem objToolBarItem = visualElement as ToolBarItem;

            // If there is a valid toolbar item
            if (objToolBarItem != null)
            {
                // Add toolbar item
                DropDownItems.Add(objToolBarItem);
            }
            else
            {
                base.Add(visualElement,insert);
            }
        }

        /// <summary>
        /// Gets whether control has DropDownItems.
        /// </summary>
        /// <value>
        /// The HasDropDownItems.
        /// </value>
        [RendererPropertyDescription]
        [Category("Appearance")]
        public bool HasDropDownItems
        {
            get { return Children.Any(); }
        }

	}
}
