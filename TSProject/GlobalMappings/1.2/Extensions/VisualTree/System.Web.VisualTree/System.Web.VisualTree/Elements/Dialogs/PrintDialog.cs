using System.Drawing.Printing;

namespace System.Web.VisualTree.Elements
{
	public class PrintDialog : CommonDialog
	{
        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
		public PrintDocument Document
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether [use ex dialog].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use ex dialog]; otherwise, <c>false</c>.
        /// </value>
		public bool UseEXDialog
		{
			get;
			set;
		}
	}
}
