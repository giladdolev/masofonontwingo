﻿namespace System.Web.VisualTree.Elements
{
    public interface IListItems
    {
        string GetItemText(object item);
        object GetItemValue(object item);
    }
}
