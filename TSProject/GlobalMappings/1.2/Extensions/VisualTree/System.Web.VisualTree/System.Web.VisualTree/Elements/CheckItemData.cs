﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// CheckItemData data to store Index , IsChecked.
    /// </summary>
    public class CheckItemData
    {

    //    private int _index = -1;
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckItemData"/> class.
        /// </summary>
        /// <param name="index"> The index. </param>
        /// <param name="isChecked">Whether is checked.</param>
        public CheckItemData(int index, bool isChecked)
        {
            Index = index;
            IsChecked = isChecked;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckItemData"/> class.
        /// </summary>
        public CheckItemData()
        {
        }

        /// <summary>
        /// Gets the Index.
        /// </summary>
        public int Index
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets if is checked.
        /// </summary>
        public bool IsChecked
        {
            get;
            private set;
        }
    }
}
