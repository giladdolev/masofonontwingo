﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    public class TableControlContainerElement : ControlElementCollection
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="TableControlContainerElement"/> class.
        /// </summary>
        /// <param name="tablePanel">The table panel.</param>
        /// <param name="propertyName">The property name.</param>
        public TableControlContainerElement(TablePanel tablePanel, string propertyName)
            : base(tablePanel, propertyName)
        {

        }


        /// <summary>
        /// Adds the specified control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="column">The column.</param>
        /// <param name="row">The row.</param>
        public void Add(ControlElement control, int column, int row)
        {
            // Add control
            Add(control);

            // Get the table element
            TablePanel tableElement = Owner as TablePanel;

            // If there is a valid table element
            if (tableElement != null)
            {
                // Set cell position
                tableElement.SetCellPosition(control, new TableLayoutPanelCellPosition(column, row));
            }
        }
    }
}
