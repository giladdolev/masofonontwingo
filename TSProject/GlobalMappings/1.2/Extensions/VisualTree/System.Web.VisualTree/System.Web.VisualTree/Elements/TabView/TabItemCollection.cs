namespace System.Web.VisualTree.Elements
{
	public class TabItemCollection : ControlElementCollection
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TabItemCollection" /> class.
        /// </summary>
        /// <param name="objParentElement">The parent element.</param>
        /// <param name="propertyName">The property name.</param>
        internal TabItemCollection(TabElement objParentElement, string propertyName)
            : base(objParentElement, propertyName)
		{
		}




        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public TabItem this[int index]
        {
            get
            {
                return base[index] as TabItem;
            }
            set
            {
                base[index] = value;
            }
        }

        /// <summary>
        /// Creates a new TabItem with the specified text and appends it to the TabItemCollection
        /// </summary>
        /// <param name="text"></param>
        public void Add(string text)
        {
            Add(new TabItem(text));
        }

	}
}
