using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignTimeVisible(false)]
    public class ScrollableControl : ContentControlElement
    {
        private bool _autoScroll = false;


        /// <summary>
        ///  Gets or sets a value indicating whether the container enables the user to scroll to any
        /// controls placed outside of its visible boundaries.
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        public virtual bool AutoScroll
        {
            get
            {
                return _autoScroll;
            }
            set
            {
                _autoScroll = value;
                OnPropertyChanged("AutoScroll");
            }
        }


    }
}
