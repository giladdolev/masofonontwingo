﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    [DesignerComponent]
    public class UrlReference : ResourceReference
    {
        private string _source;


        /// <summary>
        /// Initializes a new instance of the <see cref="UrlReference" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        public UrlReference(string source)
        {
            _source = source;
        }

        public UrlReference()
        {
         
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public override string Source
        {
            get
            {
                return _source;
            }
            set
            {
                _source = value;
            }
        }
    }
}
