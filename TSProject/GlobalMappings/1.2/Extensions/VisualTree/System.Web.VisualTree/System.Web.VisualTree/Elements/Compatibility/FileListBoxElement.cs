﻿using System.ComponentModel;
using System.IO;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements.Compatibility
{
    
    public class FileListBoxElement : CompositeElementBase
    {
        /// <summary>
        /// The list box
        /// </summary>
        private readonly ListViewElement _listElement;


        /// <summary>
        /// The directory
        /// </summary>
        private string _directory;

        /// <summary>
        /// The selected index
        /// </summary>
        private int _SelectedIndex = -1;

        /// <summary>
        /// The file upload element
        /// </summary>
        private readonly FileUploadElement _fileUploadElement;


        /// <summary>
        /// Changed event handler.
        /// </summary>
        private event EventHandler _SelectedItemChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Changed action.
        /// </summary>
        public event EventHandler SelectedItemChanged
        {
            add
            {
                bool needNotification = (_SelectedItemChanged == null);

                _SelectedItemChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SelectedItemChanged");
                }

            }
            remove
            {
                _SelectedItemChanged -= value;

                if (_SelectedItemChanged == null)
                {
                    OnEventHandlerDeattached("SelectedItemChanged");
                }

            }
        }

        
        /// <summary>
        /// FileUploaded event handler.
        /// </summary>
        private event EventHandler<EventArgs> _FileUploaded;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove FileUploaded action.
        /// </summary>
        public event EventHandler<EventArgs> FileUploaded
        {
            add
            {
                bool needNotification = (_FileUploaded == null);

                _FileUploaded += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("FileUploaded");
                }

            }
            remove
            {
                _FileUploaded -= value;

                if (_FileUploaded == null)
                {
                    OnEventHandlerDeattached("FileUploaded");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has FileUploaded listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasFileUploadedListeners
        {
            get { return _FileUploaded != null; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Changed listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasChangedListeners
        {
            get { return _SelectedItemChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SelectedItemChanged"/> instance containing the event data.</param>
        protected virtual void OnChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_SelectedItemChanged != null)
            {
                _SelectedItemChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Changed" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs" /> instance containing the event data.</param>
        public void PerformSelectedItemChanged(ValueChangedArgs<int> args)
        {
            _SelectedIndex = args.Value;

            // Raise the Changed event.
            OnChanged(args);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="FileListBoxElement"/> class.
        /// </summary>
        public FileListBoxElement()
        {
            
            // Create the control list view
            _listElement = new ListViewElement();
            _listElement.Dock = Dock.Fill;
            _listElement.BorderStyle = BorderStyle.None;
            _listElement.ItemClick += OnListControlItemClick;
            Add(_listElement, false);

            _fileUploadElement = new FileUploadElement();
            _fileUploadElement.Height = 100;
            _fileUploadElement.Dock = Dock.Bottom;
            _fileUploadElement.FileUploaded += OnFileUploaded;
            _fileUploadElement.Visible = false;
            Add(_fileUploadElement, false);

        }



        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        public string Path
        {
            get
            {
                return _directory;
            }
            set
            {

                // If there is a valid directory
                if (Directory.Exists(value))
                {
                    // Set the value
                    _directory = value;

                    // Set the upload path
                    _fileUploadElement.UploadPath = _directory;
                    _fileUploadElement.Visible = this.EnableUpload;

                    // Clear items
                    _listElement.Items.Clear();

                    int counter = 0;
                    // Loop all directory files
                    foreach (string file in Directory.GetFiles(value))
                    {
                        // If there is a valid file
                        if (file != null)
                        {
                            // Create item
                            ListViewItem item = new ListViewItem(IO.Path.GetFileName(file));
                            item.Tag = file;
                            _listElement.Items.Add(item);
                            counter++;
                        }
                    }

                    // Refresh list
                    _listElement.Refresh();
                }
                else
                {
                    _directory = null;
                    _fileUploadElement.Visible = false;
                }
            }
        }




        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>
        /// The file.
        /// </value>
        [DesignerIgnore]
        public string FileName
        {
            get
            {
                if (_listElement != null && _SelectedIndex >= 0 && _SelectedIndex < _listElement.Items.Count)
                {
                    // Get the selected item
                    ListViewItem selectedItem = _listElement.Items[_SelectedIndex];

                    // If there is a valid selected item
                    if (selectedItem != null)
                    {
                        // Get selected file entry
                        string _fullPath = selectedItem.Tag as String;

                        // Check if selected file is valid and exists 
                        if (!String.IsNullOrEmpty(_fullPath) && File.Exists(_fullPath))
                        {
                            // return file name
                            return IO.Path.GetFileName(_fullPath);
                        }
                    }
                }

                return string.Empty;
            }
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override sealed void Add(VisualElement visualElement, bool insert)
        {
            base.Add(visualElement, false);
        }



        /// <summary>
        /// Called when file uploaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnFileUploaded(object sender, EventArgs e)
        {
            // Get the file from the 
            string file = _fileUploadElement.File;

            // If there is a valid file
            if (file != null && File.Exists(file))
            {
                // Create new item
                ListViewItem item = new ListViewItem(IO.Path.GetFileName(file));

                // Set the file tag 
                item.Tag = file;

                // Add file to list element
                _listElement.Items.Add(item);

                // Set the selected item
                item.Selected = true;

                // Update the list element
                _listElement.Refresh();

                // Raise the events
                OnListControlItemClick(_listElement, new ValueChangedArgs<int>(_listElement.Items.IndexOf(item)));

                if (_FileUploaded != null)
                {
                    _FileUploaded(sender, e);
                }
            }
        }


        
        private bool _EnableUpload = true;
        /// <summary>
        /// Gets or sets the EnableUpload.
        /// </summary>
        /// <value>
        /// The EnableUpload.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool EnableUpload
        {
            get { return _EnableUpload; }
            set
            {
                if (_EnableUpload != value)
                {
                    _EnableUpload = value;

                    OnPropertyChanged("EnableUpload");
                }
            }
        }


        /// <summary>
        /// Called when list click
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="ItemCheckEventArgs" /> instance containing the event data.</param>
        private void OnListControlItemClick(object sender, ValueChangedArgs<int> args)
        {
            PerformClick(args);
            PerformSelectedItemChanged(args);
        }
    }
}
