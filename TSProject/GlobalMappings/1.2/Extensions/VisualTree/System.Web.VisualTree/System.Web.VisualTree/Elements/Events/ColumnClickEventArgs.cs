namespace System.Web.VisualTree.Elements
{
	public class ColumnClickEventArgs : EventArgs
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnClickEventArgs"/> class.
        /// </summary>
        /// <param name="column">The column.</param>
		public ColumnClickEventArgs(int column)
		{
            Column = column;
		}



        /// <summary>
        /// Gets or sets the column.
        /// </summary>
        /// <value>
        /// The column.
        /// </value>
        public int Column
        {
            get;
            set;
        }
	}
}
