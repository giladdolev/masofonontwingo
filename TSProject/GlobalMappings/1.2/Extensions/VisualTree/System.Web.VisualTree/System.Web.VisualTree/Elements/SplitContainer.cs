using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ControlContainerElement" />
    public class SplitContainer : ControlContainerElement
    {
        private bool _panel2Collapsed;
        private bool _panel1Collapsed;
        private int _splitterDistance = 50;
        private int _panel1MinSize;
        private int _panel2MinSize;
        private Orientation _orientation = Orientation.Horizontal;
        private SplitterPanelElement _panel1 = null;
        private SplitterPanelElement _panel2 = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="SplitContainer"/> class.
        /// </summary>
        public SplitContainer()
        {

        }




        private FixedPanel _FixedPanel = FixedPanel.None;

        /// <summary>
        ///  Gets or sets which System.Windows.Forms.SplitContainer panel remains the same
        ///  size when the container is resized.</summary>
        [RendererPropertyDescription]
        public FixedPanel FixedPanel
        {
            get
            {
                return _FixedPanel;
            }
            set
            {
                if (_FixedPanel != value)
                {
                    _FixedPanel = value;

                    OnPropertyChanged("FixedPanel");
                }
            }
        }



        /// <summary>
        /// Gets or sets the splitter distance.
        /// </summary>
        /// <value>
        /// The splitter distance.
        /// </value>
        [DefaultValue(50)]
        public int SplitterDistance
        {
            get { return _splitterDistance; }
            set { _splitterDistance = value; }
        }


        /// <summary>
        /// Gets or sets the panel1 MinSize .
        /// </summary>
        /// <value>
        /// The panel1 MinSize .
        /// </value>
        public int Panel1MinSize
        {
            get { return _panel1MinSize; }
            set
            {
                if (_panel1MinSize != value)
                {
                    _panel1MinSize = value;
                    if (this.Panel1 != null)
                    {
                        _panel1.MinWidth = _panel1MinSize;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the panel2 MinSize .
        /// </summary>
        /// <value>
        /// The panel2 MinSize .
        /// </value>
        public int Panel2MinSize
        {
            get { return _panel2MinSize; }
            set
            {
                if (_panel2MinSize != value)
                {
                    _panel2MinSize = value;
                    if (this.Panel2 != null)
                    {
                        _panel2.MinWidth = _panel2MinSize;
                    }
                }
            }
        }


        /// <summary>
        /// Gets or sets the orientation.
        /// </summary>
        /// <value>
        /// The orientation.
        /// </value>
        [DefaultValue(Orientation.Horizontal)]
        public Orientation Orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                // Set the splitter orientation
                _orientation = value;

                // Update the panels
                UpdatePanels();
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether [panel2 collapsed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if panel2 collapsed; otherwise, <c>false</c>.
        /// </value>
        public bool Panel2Collapsed
        {
            get { return _panel2Collapsed; }
            set
            {
                _panel2Collapsed = value;

                // Update the panels
                UpdatePanels();

            }
        }

        /// <summary>
        /// Gets or sets the background color for the control.
        /// </summary>
        /// <value>
        /// A System.Drawing.Color that represents the background color of the control.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                this.Panel1.BackColor = value;
                this.Panel2.BackColor = value;
                base.BackColor = value;
            }
        }



        /// <summary>
        /// Gets or sets a value indicating whether [panel1 collapsed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [panel1 collapsed]; otherwise, <c>false</c>.
        /// </value>
        public bool Panel1Collapsed
        {
            get { return _panel1Collapsed; }
            set
            {
                _panel1Collapsed = value;

                // Update the panels
                UpdatePanels();
            }
        }



        /// <summary>
        /// Gets the panel1.
        /// </summary>
        /// <value>
        /// The panel1.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        public SplitterPanelElement Panel1
        {
            get
            {
                if (_panel1 == null)
                {
                    InitializePanels();
                }

                return _panel1;
            }
        }



        /// <summary>
        /// Gets the panel2.
        /// </summary>
        /// <value>
        /// The panel2.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [System.Web.UI.PersistenceMode(System.Web.UI.PersistenceMode.InnerProperty)]
        public SplitterPanelElement Panel2
        {
            get
            {
                if (_panel2 == null)
                {
                    InitializePanels();
                }

                return _panel2;
            }
        }



        private void InitializePanels()
        {
            if (Controls.Count == 0)
            {
                _panel1 = new SplitterPanelElement();
                _panel1.ParentElement = this;
                Controls.Add(_panel1);
                _panel2 = new SplitterPanelElement();
                _panel2.ParentElement = this;
                Controls.Add(_panel2);
            }
            else
            {
                SplitterPanelElement[] splitterPanels = Controls.OfType<SplitterPanelElement>().ToArray();
                if (splitterPanels.Length > 0)
                {
                    _panel1 = splitterPanels[0];

                    if (splitterPanels.Length > 1)
                    {
                        _panel2 = splitterPanels[1];
                    }
                }
            }

            UpdatePanels();
        }



        /// <summary>
        /// Updates the panels.
        /// </summary>
        private void UpdatePanels()
        {
            if (_panel1 == null || _panel2 == null)
            {
                return;
            }

            Dock defaultDock = GetDefaultDock(this.Orientation);
            // If panel 1 is collapsed
            if (_panel1Collapsed && !_panel2Collapsed)
            {
                // Set the panel visiblity
                _panel1.Visible = false;
                _panel2.Visible = true;

                _panel2.SetDock(Dock.Fill, defaultDock);
                _panel1.ResetDock(defaultDock);
            }
            // If panel 2 is collapsed
            else if (!_panel1Collapsed && _panel2Collapsed)
            {
                // Set the panel visiblity
                _panel2.Visible = false;
                _panel1.Visible = true;
                _panel1.SetDock(Dock.Fill, defaultDock);
                _panel2.ResetDock(defaultDock);
            }
            // If both panels are visible
            else if (!_panel1Collapsed && !_panel2Collapsed)
            {
                // Set the panels visiblities
                _panel1.Visible = true;
                _panel2.Visible = true;

                _panel1.ResetDock(defaultDock);
                _panel2.ResetDock(Dock.Fill);
            }
        }

        /// <summary>
        /// Gets the default dock.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private Dock GetDefaultDock(Orientation orientation)
        {
            return (orientation == Orientation.Vertical) ? Dock.Top : Dock.Left;
        }
    }
}
