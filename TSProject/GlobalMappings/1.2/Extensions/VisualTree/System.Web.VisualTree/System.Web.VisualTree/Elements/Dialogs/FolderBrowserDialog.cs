namespace System.Web.VisualTree.Elements
{
	public class FolderBrowserDialog : CommonDialog
	{
        /// <summary>
        /// Gets or sets the selected path.
        /// </summary>
        /// <value>
        /// The selected path.
        /// </value>
		public string SelectedPath
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
		public string Description
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the root folder.
        /// </summary>
        /// <value>
        /// The root folder.
        /// </value>
		public Environment.SpecialFolder RootFolder
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets a value indicating whether [show new folder button].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show new folder button]; otherwise, <c>false</c>.
        /// </value>
		public bool ShowNewFolderButton
		{
			get;
			set;
		}
	}
}
