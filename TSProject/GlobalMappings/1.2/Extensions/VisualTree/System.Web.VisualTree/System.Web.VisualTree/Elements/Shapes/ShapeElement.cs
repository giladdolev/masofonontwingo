﻿namespace System.Web.VisualTree.Elements
{
    public class ShapeElement : ControlElement
    {
        public ShapeElement()
        {

        }

        public ShapeType Type { get; set; }
    }
}
