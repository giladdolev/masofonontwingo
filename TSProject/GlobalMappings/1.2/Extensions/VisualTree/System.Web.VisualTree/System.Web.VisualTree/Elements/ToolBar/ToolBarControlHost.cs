using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
	public abstract class ToolBarControlHost : ToolBarItem
	{
        private ControlElement _control;


        public ControlElement Control
        {
            get { return _control; }
        }



        public ToolBarControlHost(ControlElement objControl)
        {
            _control = objControl;
        }
	}
}
