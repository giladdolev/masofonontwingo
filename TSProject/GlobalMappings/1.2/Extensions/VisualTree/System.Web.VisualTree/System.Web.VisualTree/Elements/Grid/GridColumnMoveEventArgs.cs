﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for grid element cell related events
    /// </summary>
    public class GridColumnMoveEventArgs : EventArgs
    {

        /// <summary>
        /// The previous column index.
        /// </summary>
        private int _fromIndex;

        /// <summary>
        /// The current column index.
        /// </summary>
        private int _toIndex;

        /// <summary>
        /// The new dataSource.
        /// </summary>
        private object _dataSource;


        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        /// <value>
        /// The index of the column.
        /// </value>
        public int FromIndex
        {
            get
            {
                return _fromIndex;
            }
            private set
            {
                _fromIndex = value;
            }
        }


        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        public int ToIndex
        {
            get
            {
                return _toIndex;
            }
            private set
            {
                _toIndex = value;
            }
        }

        /// <summary>
        /// Gets the dataSource.
        /// </summary>
        /// <value>
        /// The dataSource.
        /// </value>
        public object DataSource
        {
            get
            {
                return _dataSource;
            }
            private set
            {
                _dataSource = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnMoveEventArgs"/> class.
        /// </summary>
        /// <param name="fromIndex">Index of the column.</param>
        /// <param name="toIndex">Index of the row.</param>
        public GridColumnMoveEventArgs(int fromIndex, int toIndex)
        {
            FromIndex = fromIndex;
            ToIndex = toIndex;
        }

    }
}
