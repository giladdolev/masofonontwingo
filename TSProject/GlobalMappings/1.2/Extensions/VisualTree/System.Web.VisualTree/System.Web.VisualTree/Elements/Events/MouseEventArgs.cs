using System.Drawing;

namespace System.Web.VisualTree.Elements
{
	public class MouseEventArgs : EventArgs
	{
        /// <summary>
        /// The empty
        /// </summary>
        public static new readonly MouseEventArgs Empty = new MouseEventArgs();

        // In-case the mouse was on Control/GridCell we will save the content in this property
        private string _mouseTargetInnerText;

        // In-case the mouse was on GridCell we will save the content of the row in this property ( for NestedGridElement )
        private string _mouseTargetParentInnerText;


        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        public MouseEventArgs()
		{

		}


        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="button">The button.</param>
        /// <param name="clicks">The clicks.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="delta">The delta.</param>
        public MouseEventArgs(MouseButtons button, int clicks, int x, int y, int delta)
        {
            _button = button;
            _clicks = clicks;
            _x = x;
            _y = y;
            _delta = delta;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="button">The button.</param>
        /// <param name="clicks">The clicks.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public MouseEventArgs(MouseButtons button, int clicks, int x, int y)
        {
            _button = button;
            _clicks = clicks;
            _x = x;
            _y = y;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public MouseEventArgs(int x, int y)
        {
            _x = x;
            _y = y;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="mouseTargetInnerText">The content of the controller (if exists).</param>
        /// <param name="mouseTargetParentInnerText">The content of the parentEvent (if exists).</param>
        public MouseEventArgs(int x, int y, string mouseTargetInnerText, string mouseTargetParentInnerText)
        {
            _button = MouseButtons.Left;
            _clicks = 1;
            _x = x;
            _y = y;
            _mouseTargetInnerText = mouseTargetInnerText;
            _mouseTargetParentInnerText = mouseTargetParentInnerText;
        }

        private MouseButtons _button;
		/// <summary>
		/// Gets or sets the button.
		/// </summary>
		/// <value>
		/// The button.
		/// </value>
		public MouseButtons Button
		{
            get { return _button; }
            set { _button = value; }
		}


        private int _clicks;
		/// <summary>
		/// Gets the clicks.
		/// </summary>
		/// <value>
		/// The clicks.
		/// </value>
		public int Clicks
		{
            get { return _clicks; }
            set { _clicks = value; }
		}


        private int _x;
		/// <summary>
		/// Gets or sets the x.
		/// </summary>
		/// <value>
		/// The x.
		/// </value>
		public int X
		{
            get { return _x; }
            set { _x = value; }
		}


        private int _y;
		/// <summary>
		/// Gets or sets the y.
		/// </summary>
		/// <value>
		/// The y.
		/// </value>
		public int Y
		{
            get { return _y; }
            set { _y = value; }
		}


        private int _delta;
		/// <summary>
		/// Gets or sets the delta.
		/// </summary>
		/// <value>
		/// The delta.
		/// </value>
		public int Delta
		{
            get { return _delta; }
            set { _delta = value; }
		}
	}
}
