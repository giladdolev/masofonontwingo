using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.ChartNamedElement" />
    [DesignerComponent]
    public class ChartAnnotation : ChartNamedElement
	{
		/// <summary>
		///Resizes an annotation according to its content size.
		/// </summary>
		public virtual void ResizeToContent()
		{
		}
		/// <summary>
		///Gets or sets the data point to which an annotation is anchored.
		/// </summary>
		public virtual DataPoint AnchorDataPoint
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the color of an annotation background.
		/// </summary>
		public virtual Color BackColor
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the font to use for the text of an annotation.
		/// </summary>
		public virtual Font Font
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the text color of an annotation.
		/// </summary>
		public virtual Color ForeColor
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the height of an annotation.
		/// </summary>
		public virtual double Height
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the line color of an annotation.
		/// </summary>
		public virtual Color LineColor
		{
			get;
			set;
		}
		/// <summary>
		///Gets or sets the width, in pixels, of an annotation.
		/// </summary>
		public virtual double Width
		{
			get;
			set;
		}
	}
}
