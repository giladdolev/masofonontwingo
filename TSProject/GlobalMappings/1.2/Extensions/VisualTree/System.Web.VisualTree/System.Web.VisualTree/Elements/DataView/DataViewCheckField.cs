﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    public class DataViewCheckField : DataViewField
    {

        private object _CheckedValue = true;
        private object _UncheckedValue = false;
        /// <summary>
        /// Gets or sets the CheckedValue.
        /// </summary>
        /// <value>
        /// The CheckedValue.
        /// </value>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        [Category("Data")]
        public object CheckedValue
        {
            get { return _CheckedValue; }
            set
            {
                if (_CheckedValue != value)
                {
                    _CheckedValue = value;

                    OnPropertyChanged("CheckedValue");
                }
            }
        }
        /// <summary>
        /// Gets or sets the UncheckedValue.
        /// </summary>
        /// <value>
        /// The UncheckedValue.
        /// </value>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        [Category("Data")]
        public object UncheckedValue
        {
            get { return _UncheckedValue; }
            set
            {
                if (_UncheckedValue != value)
                {
                    _UncheckedValue = value;

                    OnPropertyChanged("UncheckedValue");
                }
            }
        }


    }
}
