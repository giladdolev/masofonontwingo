using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.VisualElement" />
    [DesignerComponent]
    public class ListViewSubitem : VisualElement
	{
        /// <summary>
        /// The text
        /// </summary>
        private string _text;

		public Color BackColor
		{
			get;
			set;
		}


        private Color _foreColor = Color.Empty;

        public Color ForeColor
        {
            get {
                return _foreColor;
            }
            set {
                if (value != _foreColor)
                {
                    _foreColor = value;
                    if (this.ParentElement != null)
                    {
                        ListViewItem parent = this.ParentElement as ListViewItem;
                        parent.ListView.SetItemForeColor(value, parent.Index, parent.Subitems.IndexOf(this));
                    }
                }
            }
        }
        

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        private ListViewItem Item
        {
            get
            {
                return this.ParentElement as ListViewItem;
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
		public string Text
		{
			get
            {
                return _text;
            }
			set
            {
                // If the text had changed
                if (_text != value)
                {
                    // Change text
                    _text = value;

                    // Get item
                    ListViewItem item = this.Item;
                    if(item != null)
                    {
                        // Notify item update
                        item.NotifyUpdate(this, "Text");
                    }
                }
            }
		}



	    public string Default
	    {
	        get { return Text; }
	    }
	}
}
