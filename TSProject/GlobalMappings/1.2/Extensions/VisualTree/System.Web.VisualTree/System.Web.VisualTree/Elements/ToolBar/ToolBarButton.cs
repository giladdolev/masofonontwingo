namespace System.Web.VisualTree.Elements
{
    public class ToolBarButton : ToolBarItem, IButtonElement
	{
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ToolBarButton"/> is checked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
		public bool Checked
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the width of the drop down button.
        /// </summary>
        /// <value>
        /// The width of the drop down button.
        /// </value>
		public int DropDownButtonWidth
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the width of the button.
        /// </summary>
        /// <value>
        /// The width of the button.
        /// </value>
        public float ButtonWidth
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the height of the button.
        /// </summary>
        /// <value>
        /// The height of the button.
        /// </value>
        public float ButtonHeight
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets a value indicating whether user customization allowed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if user customization allowed; otherwise, <c>false</c>.
        /// </value>
        public bool AllowCustomize
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ToolBarButton"/> is wrappable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if wrappable; otherwise, <c>false</c>.
        /// </value>
        public bool Wrappable
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public AppearanceConstants Style
        {
            get;
            set;

        }



        public ImageList GetImageList()
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Sets the ImageList object on TreeElement
        /// </summary>
        /// <param name="objImageList">ImageList object</param>
        /// <exception cref="NotImplementedException"></exception>
        public void SetImageList(ImageList objImageList)
        {
            throw new NotImplementedException();
        }
	}
}
