﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.FileSystem;

namespace System.Web.VisualTree.Elements.Compatibility
{
    public class FileSystemBase : CompositeElementBase
    {
        /// <summary>
        /// Gets or sets the file system identifier.
        /// </summary>
        /// <value>
        /// The file system identifier.
        /// </value>
        public string FileSystemID { get; set; }

        /// <summary>
        /// Gets the file system.
        /// </summary>
        /// <value>
        /// The file system.
        /// </value>
        protected VirtualFileSystem FileSystem
        {
            get
            {
                return VirtualFileSystem.GetFileSystem(FileSystemID);
            }
        }
    }
}
