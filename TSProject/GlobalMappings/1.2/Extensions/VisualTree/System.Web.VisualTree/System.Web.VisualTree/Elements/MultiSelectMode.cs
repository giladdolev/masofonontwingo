﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// List box selection modes
    /// </summary>
    public enum MultiSelectMode
    {
        /// <summary>
        /// Standard Windows ListBox appearance
        /// </summary>
        ListBoxStandard,

        /// <summary>
        /// Listbox entries contain selectable CheckBoxes
        /// </summary>
        ListBoxCheckBox,

        /// <summary>
        /// No multiple selection
        /// </summary>
        MultiSelectNone,

        /// <summary>
        /// Simple multiple selection
        /// </summary>
        MultiSelectSimple,

        /// <summary>
        /// Extended multiple selection
        /// </summary>
        MultiSelectExtended
    }
}
