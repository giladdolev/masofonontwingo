﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// GridCellInfo data to store cell data .
    /// </summary>
    public class GridCellInfo
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData"/> class.
        /// </summary>
        public GridCellInfo()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="GridColumnData" /> class.
        /// </summary>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="apped">if set to <c>true</c> [apped].</param>
        public GridCellInfo(int rowIndex, int columnIndex, string cssClass, bool apped)
        {
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
            CssClass = cssClass;
            AllowAppend = apped;
        }

        public GridCellInfo(int rowIndex, int columnIndex, string cssClass, bool appned, int dataMember)
        {
            // TODO: Complete member initialization
            this.RowIndex = rowIndex;
            this.ColumnIndex = columnIndex;
            this.CssClass = cssClass;
            this.AllowAppend = appned;
            this.DataMemberIndex = dataMember;
        }


        /// <summary>
        /// Get,Set the Row Index.
        /// </summary>
        public int RowIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the Row Index.
        /// </summary>
        public int ColumnIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Get,Set the Row Index.
        /// </summary>
        public string CssClass
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool AllowAppend
        {
            get;
            private set;
        }

        public int DataMemberIndex { get; set; }
    }
}
