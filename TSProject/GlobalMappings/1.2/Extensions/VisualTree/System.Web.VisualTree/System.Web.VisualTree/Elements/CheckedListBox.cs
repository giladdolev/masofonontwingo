using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for checked list box element item related events
    /// </summary>
    /// <param name="sender">The sender</param>
    /// <param name="e">The <see cref="ItemCheckEventArgs"/> instance containing the event data.</param>
    public delegate void CheckedListBoxLItemEventHandler(object sender, ItemCheckEventArgs e);

    public class CheckedListBoxElement : ListControlElement
    {
        /// <summary>
        /// The 3D check boxes
        /// </summary>
        private bool _3DCheckBoxes = true;
        private int[] _selectedIndexArray = { -1 };
        private bool _multiColumn = false;
        private bool _autoScroll = false;

        private ListItemCollection _CheckedItems;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedListBoxElement"/> class.
        /// </summary>
        public CheckedListBoxElement()
        {
            _CheckedItems = new ListItemCollection(this, "checked");
        }

        /// <summary>
        /// Gets or sets a value indicating whether the CheckedListBoxElement supports multiple columns.
        /// </summary>
        [DefaultValue(false)]
        public bool MultiColumn
        {
            get { return this._multiColumn; }
            set
            {
                this._multiColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [check on click].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [check on click]; otherwise, <c>false</c>.
        /// </value>
        public bool CheckOnClick
        {
            get;
            set;
        }

        /// <summary>
        /// GetItemChecked Return true if item is checked.
        /// </summary>
        /// <param name="selectedItemIndex">Index of the selected item.</param>
        /// <returns></returns>
        public bool GetItemChecked(int selectedItemIndex)
        {
            return this.CheckedItems.Contains(this.Items[selectedItemIndex]);
        }

        /// <summary>
        /// Set Item Checked state
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="isChecked">Whether to check item.</param>
        [RendererMethodDescription]
        public void SetItemChecked(int index, bool isChecked)
        {
            if (index!=-1)
            {
                if (isChecked)
                {
                    if (!CheckedItems.Contains(this.Items[index]))
                    {
                        CheckedItems.Add(this.Items[index]);
                    }
                }
                else
                {
                    if (CheckedItems.Contains(this.Items[index]))
                    {
                        CheckedItems.Remove(this.Items[index]);
                    }
                }
                this.InvokeMethodOnce("SetItemChecked",new CheckItemData(index,isChecked));
            }
        }

        /// <summary>
        /// Gets or sets the zero-based index of the currently selected items in a CheckedListBox.
        /// </summary>
        [DefaultValue(-1)]
        public int[] SelectedIndexsArray
        {
            get { return this._selectedIndexArray; }
            set { this._selectedIndexArray = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [three d check boxes].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [three d check boxes]; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool ThreeDCheckBoxes
        {
            get { return _3DCheckBoxes; }
            set { _3DCheckBoxes = value; }
        }

        /// <summary>
        /// Collection of checked items in this CheckedListBox..
        /// </summary>
        public ListItemCollection CheckedItems
        {
            get
            {
                return _CheckedItems;
            }
        }


        /// <summary>
        /// ItemCheck event handler.
        /// </summary>
        private event CheckedListBoxLItemEventHandler _ItemCheck;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ItemCheck action.
        /// </summary>
        public event CheckedListBoxLItemEventHandler ItemCheck
        {
            add
            {
                bool needNotification = (_ItemCheck == null);

                _ItemCheck += value;

                if (needNotification)
            {
                    OnEventHandlerAttached("ItemCheck");
                }

            }
            remove
            {
                _ItemCheck -= value;

                if (_ItemCheck == null)
            {
                    OnEventHandlerDeattached("ItemCheck");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:ItemCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ItemCheck"/> instance containing the event data.</param>
        protected virtual void OnItemCheck(ItemCheckEventArgs args)
        {

            // Check if there are listeners.
            if (_ItemCheck != null)
            {
                _ItemCheck(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ItemCheck" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ItemCheckEventArgs"/> instance containing the event data.</param>
        public void PerformItemCheck(ItemCheckEventArgs args)
        {
            // if ItemCheckEventArgs != null 
            if (args != null)
            {
                // Get the selected index of the cheeked items.
                int[] indexArray = args.Indexs;
                if (indexArray != null)
                {
                    // clear previous checked items .
                    _CheckedItems.Clear();
                    for (int index = 0; index < indexArray.Length; index++)
                    {
                        if (index < this.Items.Count)
                        {
                            _CheckedItems.Add(this.Items[indexArray[index]]);
                        }
                    }
                }
                else
        {
                    // clear previous checked items .
                    _CheckedItems.Clear();
                }
            }
            // Raise the ItemCheck event.
            OnItemCheck(args);
        }

        /// <summary>
        ///  Gets or sets a value indicating whether the container enables the user to scroll to any
        /// controls placed outside of its visible boundaries.
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        public virtual bool AutoScroll
        {
            get
            {
                return _autoScroll;
            }
            set
            {
                _autoScroll = value;
                OnPropertyChanged("AutoScroll");
            }
        }
    }
}
