﻿using System.Runtime.InteropServices;

namespace System.Web.VisualTree.Elements
{
    [ComVisible(true)]
    public enum WindowState
    {
        /// <summary>A default sized window.</summary>
        Normal,
        /// <summary>A minimized window.</summary>
        Minimized,
        /// <summary>A maximized window.</summary>
        Maximized
    }
}
