﻿namespace System.Web.VisualTree.Elements
{
    public enum ChartImageWrapMode
    {
        Scaled = 4,
        Tile = 0,
        TileFlipX = 1,
        TileFlipXY = 3,
        TileFlipY = 2,
        Unscaled = 100
    }
}
