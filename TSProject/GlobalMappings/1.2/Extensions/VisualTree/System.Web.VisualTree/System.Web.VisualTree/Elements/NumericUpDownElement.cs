using System.Web.VisualTree.Common.Attributes;
namespace System.Web.VisualTree.Elements
{
    public class NumericUpDownElement : UpDownElement
    {


        private decimal _Value = 0;
        private decimal _maximum = decimal.MaxValue;
        private decimal _minimum = decimal.MinValue;

        [RendererPropertyDescription]
        /// <summary>
        /// The current value at the numericUpDown
        /// </summary>
        public decimal Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if (_Value != value)
                {
                    _Value = value;

                    OnPropertyChanged("Value");
                }
            }
        }



        [RendererPropertyDescription]
        /// <summary>
        /// The maximum value the numericUpDown can get
        /// </summary>
        public decimal Maximum
        {
            get
            {
                return _maximum;
            }
            set
            {
                if (_maximum != value)
                {
                    _maximum = value;

                    OnPropertyChanged("Maximum");
                }
            }
        }





        [RendererPropertyDescription]
        /// <summary>
        ///The Minimum value the numericUpDown can get
        /// </summary>
        public decimal Minimum
        {
            get
            {
                return _minimum;
            }
            set
            {
                if (_minimum != value)
                {
                    _minimum = value;

                    OnPropertyChanged("Minimum");
                }
            }
        }


        public bool Hexadecimal
        {
            get;
            set;
        }


        public decimal Increment
        {
            get;
            set;
        }

        /// <summary>
        /// Error text to display if the maximum value validation fails
        /// </summary>
        private string _maxText;

        /// <summary>
        /// Error text to display if the maximum value validation fails
        /// </summary>
        [RendererPropertyDescription]
        public string MaxText
        {
            get
            {
                return _maxText;
            }
            set
            {
                _maxText = value; OnPropertyChanged("MaxText");
            }
        }


        /// <summary>
        /// ValueChanged event handler.
        /// </summary>
        private event EventHandler _ValueChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ValueChanged action.
        /// </summary>
        public event EventHandler ValueChanged
        {
            add
            {
                bool needNotification = (_ValueChanged == null);

                _ValueChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ValueChanged");
                }

            }
            remove
            {
                _ValueChanged -= value;

                if (_ValueChanged == null)
                {
                    OnEventHandlerDeattached("ValueChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ValueChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasValueChangedListeners
        {
            get { return _ValueChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ValueChanged"/> instance containing the event data.</param>
        protected virtual void OnValueChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_ValueChanged != null)
            {
                _ValueChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:ValueChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformValueChanged(NumericUpDownEventArgs args)
        {
            if (args != null)
            {
                string direction = args.Direction.ToLowerInvariant();
                if (direction == "up")
                {
                    this.Value = this.Value + 1;
                }
                else if (direction == "down")
                {
                    this.Value = this.Value - 1;
                }
            }
            // Raise the ValueChanged event.
            OnValueChanged(args);
        }
    }
}
