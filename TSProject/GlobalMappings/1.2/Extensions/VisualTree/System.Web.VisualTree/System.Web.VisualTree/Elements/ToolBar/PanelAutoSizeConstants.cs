﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Panel AutoSize constants
    /// </summary>
    public enum PanelAutoSizeConstants
    {
        /// <summary>
        /// None. No auto sizing occurs. The width of the panel is always and exactly that specified by the Width property.
        /// </summary>
        NoAutoSize,

        /// <summary>
        /// Spring. When the parent form resizes and there is extra space available, all panels with this setting divide the space and grow accordingly.
        /// However, the panels' width never falls below that specified by the MinWidth property.
        /// </summary>
        Spring,

        /// <summary>
        /// Content. The panel is resized to fit its contents.
        /// </summary>
        Contents
    }
}
