﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements.Touch
{
    public enum TVisualLayout
    {
        Default,

        HBox,

        VBox,

        Card,

        Float,

        Dock,

        Fit
    }
}
