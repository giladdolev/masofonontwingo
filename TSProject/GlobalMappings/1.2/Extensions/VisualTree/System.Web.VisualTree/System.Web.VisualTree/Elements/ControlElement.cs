using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Web.VisualTree.Utilities;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;
using System.Web.UI;
using System.Web.VisualTree.Common;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{

    public delegate void MethodInvoker();


    /// <summary>
    /// Defines the base class for controls, which are components with visual representation.
    /// </summary>
    /// <seealso cref="System.Web.VisualTree.Elements.VisualElement" />
    /// <seealso cref="IEnumerable{ControlElement}" />
    /// <seealso cref="System.ComponentModel.ISupportInitialize" />
    /// <seealso cref="System.Web.VisualTree.Elements.IControlElement" />
    [DesignTimeVisible(false)]
    [DesignerComponent(AddPartial = true)]
    public class ControlElement : VisualElement, IEnumerable<ControlElement>, ISupportInitialize, IControlElement
    {
        // Field to suppress FxCop

        private int _temp2SuppressCA1822;
        private int _tabIndex;
        private string _text = string.Empty;
        private object _tag;
        private Font _font;
        private bool _enabled = true;
        private string _store = null;
        private bool _tabStop = true;
        private bool _autoSize = true;
        private AnchorStyles _anchor = AnchorStyles.Top | AnchorStyles.Left;
        private AccessibleRole _accessibleRole = AccessibleRole.Default;
        private AppearanceConstants _appearance = AppearanceConstants.cc3D;
        private BorderStyle _borderStyle = BorderStyle.NotSet;
        private Color _borderColor = Color.Empty;
        private ImageLayout _backgroundImageLayout = ImageLayout.Tile;
        private ControlElementCollection _controls;
        private ControlElementState _state = ControlElementState.None;
        private ControlElementLayout _controlLayout = null;
        private Dock _dock = Dock.None;
        private Unit _height;
        private ImeMode _imeMode = ImeMode.Inherit;
        private Unit _left;
        private RightToLeft _rightToLeft = RightToLeft.Inherit;
        private Unit _top;
        private Unit _width;
        private Color _backColor = Color.Empty;
        private ControlBindingsCollection _dataBindings = null;
        private Size _minimumSize;
        private Size _maximumSize;
        private Color _foreColor = Color.Empty;
        private int _zIndex = -1;
        private string _cssClass;
        private bool _waitMaskDisabled = false;
       
        private event EventHandler _click;
        private event EventHandler _mouseHover;
        private event EventHandler _load;
        private event EventHandler _mouseEnter;
        private event EventHandler<LayoutEventArgs> _layout;
        private event EventHandler _visibleChanged;
        private event EventHandler _mouseLeave;
        private event EventHandler _validating;
        private event EventHandler _textChanged;
        private event EventHandler _dragDrop;
        private event EventHandler _dragOver;
        private event EventHandler _sizeChanged;
        private event EventHandler<MouseEventArgs> _mouseMove;
        private event EventHandler<MouseEventArgs> _mouseDown;
        private event EventHandler<MouseEventArgs> _mouseUp;
        private event EventHandler<TreeItemMouseClickEventArgs> _mouseClick;
        private event EventHandler _locationChanged;
        private event EventHandler _leave;
        private event EventHandler _doubleClick;
        private event EventHandler _dragEnter;
        private event EventHandler _enter;
        private event EventHandler<ControlEventArgs> _controlAdded;
        private event EventHandler<ControlEventArgs> _controlRemoved;
        private event EventHandler<KeyPressEventArgs> _keyPress;
        private event EventHandler<KeyEventArgs> _keyUp;
        private event EventHandler<KeyDownEventArgs> _keyDown;
        private event EventHandler _gotFocus;
        private event EventHandler<EventArgs> _afterrender;
        private event EventHandler _validated;


        private string _errorString = "";
        /// <summary>
        /// The history enabled
        /// </summary>
        private bool _historyEnabled = false;

        /// <summary>
        /// Gets or sets a value indicating whether history enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if history enabled; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        public bool HistoryEnabled
        {
            get { return _historyEnabled; }
            set { _historyEnabled = value; }
        }

        /// <summary>
        /// Determine whether the control is focused
        /// </summary>
        public bool IsFocused
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show wait mask.
        /// </summary>
        /// <value>
        ///   <c>true</c> if mask enabled; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(false)]
        [RendererPropertyDescription]
        public bool WaitMaskDisabled
        {
            get { return _waitMaskDisabled; }
            set
            {
                if (_waitMaskDisabled != value)
                {
                    _waitMaskDisabled = value;

                    OnPropertyChanged("WaitMaskDisabled");
                }
            }
        }

        /// <summary>
        /// Should report history change.
        /// </summary>
        /// <returns></returns>
        protected bool ShouldReportHistoryChange()
        {
            // If we need to update the client 
            if (ApplicationElement.GlobalRevision != Revision)
            {
                return _historyEnabled;
            }

            return false;

        }

        /// <summary>
        /// Gets the IContainer that contains the Component.
        /// </summary>
        public IControlElement Container
        {
            get;
            set;
        }

        /// <summary>
        /// Provides support for suspend history session
        /// </summary>
        private class SuspendHistorySession : IDisposable
        {
            /// <summary>
            /// The control element
            /// </summary>
            private readonly ControlElement _controlElement;

            /// <summary>
            /// Initializes a new instance of the <see cref="SuspendHistorySession"/> class.
            /// </summary>
            /// <param name="controlElement">The control element.</param>
            public SuspendHistorySession(ControlElement controlElement)
            {
                // Set the control element
                _controlElement = controlElement;
                _controlElement.HistoryEnabled = false;
            }


            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                _controlElement.HistoryEnabled = true;
            }
        }

        /// <summary>
        /// Creates the suspend history session.
        /// </summary>
        /// <returns></returns>
        internal IDisposable CreateSuspendHistorySession()
        {
            // If history is enabled
            if (_historyEnabled)
            {
                return new SuspendHistorySession(this);
            }

            return null;
        }


        private string _toolTipText = String.Empty;
        [RendererPropertyDescription]
        [DefaultValue("")]
        [Category("Appearance")]
        public string ToolTipText
        {
            get
            {

                // The tool tip id
                return _toolTipText;
            }

            set
            {
                if (_toolTipText != value)
                {
                    _toolTipText = value;
                    OnPropertyChanged("ToolTipText");
                }
            }
        }


        public static readonly Font DefaultFont = SystemFonts.DefaultFont;
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlElement"/> class.
        /// </summary>
        public ControlElement()
        {
            // Initialize controls collection
            _font = DefaultFont;
            _enabled = true;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ControlElement"/> class.
        /// </summary>
        public ControlElement(string text)
            : this()
        {
            Text = text;
        }

        [RendererPropertyDescription]
        public string CssClass
        {
            get { return _cssClass; }
            set
            {
                if (_cssClass != value)
                {
                    _cssClass = value;

                    OnPropertyChanged("CssClass");
                }
            }
        }

        [RendererPropertyDescription]
        public virtual Size MaximumSize
        {
            get { return _maximumSize; }
            set
            {
                if (_maximumSize != value)
                {
                    _maximumSize = value;

                    OnPropertyChanged("MaximumSize");
                }
            }
        }

        /// <summary>
        /// Gets or sets the minimum size.
        /// </summary>
        /// <value>
        /// The minimum size.
        /// </value>
        [RendererPropertyDescription]
        public virtual Size MinimumSize
        {
            get { return _minimumSize; }
            set
            {
                if (_minimumSize != value)
                {
                    _minimumSize = value;

                    OnPropertyChanged("MinimumSize");
                }
            }
        }


        /// <summary>
        /// Gets or sets the index of the z.
        /// </summary>
        /// <value>
        /// The index of the z.
        /// </value>
        [DefaultValue(-1)]
        [RendererPropertyDescription]
        public int ZIndex
        {
            get { return _zIndex; }
            set
            {
                if (_zIndex != value)
                {
                    _zIndex = value;

                    OnPropertyChanged("ZIndex");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether element causes Tab Stop.
        /// </summary>
        /// <value>
        ///   <c>true</c> if causes Tab Stop; otherwise, <c>false</c>.
        /// </value>
        [DefaultValue(true)]
        public bool TabStop
        {
            get { return _tabStop; }
            set
            {
                if (_tabStop != value)
                {
                    _tabStop = value;
                    if (_tabStop)
                    {
                        OnPropertyChanged("TabIndex");
                    }
                }
            }
        }

        private bool _draggable = false;
        /// <summary>
        /// Draggable Property 
        /// </summary>
        [RendererPropertyDescription]
        [DefaultValue(false)]
        public bool Draggable
        {
            get { return _draggable; }
            set
            {
                if (_draggable != value)
                {
                    _draggable = value;

                    OnPropertyChanged("Draggable");
                }
            }
        }

        [RendererPropertyDescription]
        [DefaultValue(true)]
        public virtual bool AutoSize
        {
            get { return _autoSize; }
            set
            {
                if (_autoSize != value)
                {
                    _autoSize = value;
                    OnPropertyChanged("AutoSize");
                }
            }
        }

        /// <summary>
        /// Gets or sets the size of the client.
        /// </summary>
        /// <value>
        /// The size of the client.
        /// </value>
        [DesignerIgnoreAttribute()]
        public virtual Size ClientSize
        {
            get
            {
                // Return pixel size
                return new Size(PixelWidth, PixelHeight);
            }
            set
            {
                // Set the pixel height and pixel width
                PixelHeight = value.Height;
                PixelWidth = value.Width;
            }
        }



        [DesignerIgnore]
        public bool IsDisposed
        {
            get;
            private set;
        }



        [DesignerIgnore]
        public virtual bool InvokeRequired
        {
            get { return default(bool); }
        }




        [DesignerIgnore]
        [RendererPropertyDescription]
        public virtual Font Font
        {
            get { return _font; }
            set
            {
                _font = value;

                // Notify property Font changed
                OnPropertyChanged("Font");
            }
        }



        [RendererPropertyDescription]
        [DefaultValue(RightToLeft.Inherit)]
        public virtual RightToLeft RightToLeft
        {
            get { return _rightToLeft; }
            set
            {
                if (value == RightToLeft.Yes)
                {
                    RightToLeftDecoration = true;
                }
                _rightToLeft = value;

            }
        }

        [RendererPropertyDescription]
        [DefaultValue(false)]

        public bool RightToLeftDecoration
        {
            get;
            set;
        }


        public virtual bool AllowDrop
        {
            get;
            set;
        }



        public Point MousePosition
        {
            get;
            private set;
        }




        protected virtual CreateParamsElement CreateParams
        {
            get { return default(CreateParamsElement); }
        }



        [DefaultValue(AccessibleRole.Default)]
        public AccessibleRole AccessibleRole
        {
            get { return _accessibleRole; }
            set { _accessibleRole = value; }
        }


        //
        // Summary:
        // Gets or sets the size and location of the control including its nonclient
        // elements, in pixels, relative to the parent control.
        //
        // Returns:
        // A System.Drawing.Rectangle in pixels relative to the parent control that
        // represents the size and location of the control including its nonclient elements.
        [DesignerIgnore]
        public virtual Rectangle Bounds
        {
            get { return new Rectangle() { Width = this.PixelWidth, Height = this.PixelHeight, X = this.PixelLeft, Y = this.PixelTop }; }
            set { this.SetBounds(value.X, value.Y, value.Width, value.Height); }
        }



        [DefaultValue(ImeMode.Inherit)]
        public ImeMode ImeMode
        {
            get { return _imeMode; }
            set { _imeMode = value; }
        }


        private ResourceReference _backgroundImage;
        [RendererPropertyDescription]
        public virtual ResourceReference BackgroundImage
        {
            get { return _backgroundImage; }
            set
            {
                _backgroundImage = value;
                // Notify property Image changed
                OnPropertyChanged("BackgroundImage");
            }
        }




        [DefaultValue(ImageLayout.Tile)]
        public virtual ImageLayout BackgroundImageLayout
        {
            get { return _backgroundImageLayout; }
            set
            {
                _backgroundImageLayout = value;
                OnPropertyChanged("BackgroundImageLayout");
            }
        }



        [DesignerIgnore]
        public ControlElement Parent
        {
            get { return base.ParentElement as ControlElement; }
        }




        public virtual IntPtr Handle
        {
            get { return default(IntPtr); }
        }



        [DesignerIgnore]
        public bool Disposing
        {
            get;
            private set;
        }



        public Rectangle ClientRectangle
        {
            get { return new Rectangle(Location, Size); }
        }




        /// <summary>
        /// Gets or sets the menu.
        /// </summary>
        /// <value>
        /// The menu.
        /// </value>
        [IDReferenceProperty]
        public virtual MenuElement Menu
        {
            get;
            set;
        }



        [DesignerIgnore]
        public virtual CursorElement Cursor
        {
            get;
            set;
        }




        [DesignerIgnore]
        public virtual ControlBindingsCollection DataBindings
        {
            get
            {
                if (_dataBindings == null)
                {
                    _dataBindings = new ControlBindingsCollection(this, "DataBindings");
                }
                return _dataBindings;
            }
        }


        /// <summary>
        /// Gets a value indicating whether has text changed listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has text changed listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasTextChangedListeners
        {
            get { return _textChanged != null; }
        }




        /// <summary>
        /// Gets a value indicating whether [has key press listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has key press listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasKeyPressListeners
        {
            get { return _keyDown != null || _keyUp != null || _keyPress != null; }
        }

        /// <summary>
        /// Gets a value indicating whether the GotFocus event has listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if the GotFocus event has listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasGotFocusListeners
        {
            get { return (_gotFocus != null || _enter != null); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has afterrendere listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has resize listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasAfterrenderListeners
        {
            get { return (this.ContextMenuStrip != null || this._afterrender != null); }
        }


        /// <summary>
        ///Gets or sets a value indicating whether the user can drag column headers to reorder columns in the control.
        /// </summary>
        public bool AllowColumnReorder
        {
            get;
            set;
        }




        /// <summary>
        ///Gets or sets a value indicating whether this control should redraw its surface using a secondary buffer to reduce or prevent flicker.
        /// </summary>
        protected virtual bool DoubleBuffered
        {
            get;
            set;
        }




        /// <summary>
        ///Gets or sets a value indicating whether the control causes validation to be performed on any controls that require validation when it receives focus.
        /// </summary>
        public bool CausesValidation
        {
            get;
            set;
        }



        public virtual float ScaleHeight
        {
            get;
            set;
        }




        public float ScaleWidth
        {
            get;
            private set;
        }




        public virtual int Index
        {
            get { return default(int); }
        }




        [DefaultValue(AppearanceConstants.cc3D)]
        public AppearanceConstants Appearance
        {
            get { return _appearance; }
            set { _appearance = value; }
        }




        /// <summary>
        /// Gets or sets the border style.
        /// </summary>
        /// <value>
        /// The border style.
        /// </value>
        [DefaultValue(BorderStyle.NotSet)]
        [DesignerIgnore]
        [RendererPropertyDescription]
        public BorderStyle BorderStyle
        {
            get { return _borderStyle; }
            set
            {
                _borderStyle = value;

                // Notify property BorderStyle changed
                OnPropertyChanged("BorderStyle");
            }
        }


        /// <summary>
        /// Gets or sets the color of the border.
        /// </summary>
        /// <value>
        /// The color of the border.
        /// </value>
        [DesignerIgnore]
        [DefaultValue(typeof(Color), "")]
        [TypeConverter(typeof(WebColorConverter))]
        [RendererPropertyDescription]
        public virtual Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;

                // Notify property BackColor changed
                OnPropertyChanged("BorderColor");
            }
        }


        public int HelpContextID
        {
            get;
            set;
        }




        /// <summary>
        /// Gets a value indicating whether children order reverted.
        /// </summary>
        /// <value>
        ///   <c>true</c> if children order reverted; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public override bool ChildrenOrderReverted
        {
            get
            {
                return true;
            }
        }




        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <value>
        /// The children.
        /// </value>
        [DesignerIgnore]
        public override IEnumerable<VisualElement> Children
        {
            get
            {
                return Controls;
            }
        }




        /// <summary>
        /// Gets or sets the control layout.
        /// </summary>
        /// <value>
        /// The control layout.
        /// </value>
        public ControlElementLayout ControlLayout
        {
            get { return _controlLayout; }
            set { _controlLayout = value; }
        }




        /// <summary>
        /// Gets or sets a value indicating whether this control is loaded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this control is loaded; otherwise, <c>false</c>.
        /// </value>
        private bool IsLoaded
        {
            get { return (_state & ControlElementState.Loaded) != ControlElementState.None; }
            set
            {
                // If is loaded
                if (value)
                {
                    // Set combined value
                    _state = _state | ControlElementState.Loaded;
                }
                else
                {
                    // Set excluded value
                    _state = _state & ~ControlElementState.Loaded;
                }
            }
        }




        /// <summary>
        /// Gets or sets a value indicating whether this control is created.
        /// </summary>
        /// <value>
        /// <c>true</c> if this control is created; otherwise, <c>false</c>.
        /// </value>
        private bool IsCreated
        {
            get { return (_state & ControlElementState.Created) != ControlElementState.None; }
            set
            {
                // If is created
                if (value)
                {
                    // Set combined value
                    _state = _state | ControlElementState.Created;
                }
                else
                {
                    // Set excluded value
                    _state = _state & ~ControlElementState.Created;
                }
            }
        }




        /// <summary>
        /// Gets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>     
        [RendererPropertyDescription]
        [DesignerIgnore]
        public ControlElementCollection Controls
        {
            get
            {
                if (_controls == null)
                {
                    _controls = CreateControlElementCollection();
                }
                return _controls;
            }
        }




        /// <summary>
        /// Gets or sets which control borders are docked to its parent control and determines
        ///     how a control is resized with its parent.
        /// </summary>
        /// <value>
        /// One of the System.Web.VisualTree.Dock values. The default is System.Web.VisualTree.Dock.None.
        /// </value>
        [DefaultValue(Dock.None)]
        public Dock Dock
        {
            get { return _dock; }
            set
            {
                if (_dock != value)
                {
                    _dock = value;
                    OnPropertyChanged("Dock");
                }
            }
        }




        /// <summary>
        /// Gets or sets the foreground color of the control.
        /// </summary>
        /// <value>
        /// The foreground System.Drawing.Color of the control.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Color ForeColor
        {
            get { return _foreColor; }
            set
            {
                if (_foreColor != value)
                {
                    _foreColor = value;
                    OnPropertyChanged("ForeColor");
                }
            }
        }




        /// <summary>
        /// Gets or sets the background color for the control.
        /// </summary>
        /// <value>
        /// A System.Drawing.Color that represents the background color of the control.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public virtual Color BackColor
        {
            get { return _backColor; }
            set
            {
                _backColor = value;

                // Notify property BackColor changed
                OnPropertyChanged("BackColor");
            }
        }




        /// <summary>
        /// Gets or sets the edges of the container to which a control is bound and determines
        ///     how a control is resized with its parent.
        /// </summary>
        /// <value>
        /// A bitwise combination of the System.Web.VisualTree.AnchorStyles values. The
        ///     default is Top and Left.
        /// </value>
        [DefaultValue(AnchorStyles.Top | AnchorStyles.Left)]
        public AnchorStyles Anchor
        {
            get { return _anchor; }
            set { _anchor = value; }
        }









        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Unit Width
        {
            get { return _width; }
            set
            {
                // If there is a new width value
                if (_width != value)
                {
                    // Set the width
                    _width = value;

                    OnPropertyChanged("Width");
                }
            }
        }




        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        [DesignerType(typeof(string))]
        [RendererPropertyDescription]
        public object Tag
        {
            get { return _tag; }
            set
            {
                // If there is a new width value
                if (_tag != value)
                {
                    // Set the width
                    _tag = value;

                    OnPropertyChanged("Tag");
                }
            }
        }




        /// <summary>
        /// Gets or sets the width of the pixel.
        /// </summary>
        /// <value>
        /// The width of the pixel.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelWidth
        {
            get { return UnitTypeConverter.GetPixels(Width); }
            set
            {

                Width = Unit.Pixel(value);
                OnPropertyChanged("Width");
            }
        }




        /// <summary>
        /// Gets or sets the pixel left.
        /// </summary>
        /// <value>
        /// The pixel left.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelLeft
        {
            get { return UnitTypeConverter.GetPixels(Left); }
            set
            {

                Left = Unit.Pixel(value);
                OnPropertyChanged("Left");
            }
        }




        /// <summary>
        /// Gets or sets the pixel top.
        /// </summary>
        /// <value>
        /// The pixel top.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelTop
        {
            get { return UnitTypeConverter.GetPixels(Top); }
            set
            {

                Top = Unit.Pixel(value);
            }
        }




        /// <summary>
        /// Gets or sets the height of the pixel.
        /// </summary>
        /// <value>
        /// The height of the pixel.
        /// </value>
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public int PixelHeight
        {
            get { return UnitTypeConverter.GetPixels(Height); }
            set
            {

                Height = Unit.Pixel(value);
            }
        }




        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [RendererPropertyDescription]
        [DefaultValue("")]
        public virtual string Text
        {
            get { return _text; }
            set
            {
                // If the new value is not same as the old value
                if (_text != value)
                {
                    // Set the text value
                    _text = value;

                    // Raise the text changed event
                    OnTextChanged(EventArgs.Empty);

                    // Notify property changed
                    OnPropertyChanged("Text");
                }
            }
        }

        /// <summary>
        /// Gets or sets the skin to apply to the control.
        /// </summary>
        /// <value>
        /// The name of the skin to apply to the control. The default is System.String.Empty.
        /// </value>
        [DesignerIgnore]
        [DefaultValue("")]
        public string SkinID { get; set; }


        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        public Unit Height
        {
            get { return _height; }
            set
            {
                if (_height != value)
                {
                    _height = value;

                    OnPropertyChanged("Height");
                }
            }
        }




        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        [DesignerIgnore]
        public Size Size
        {
            get
            {
                // Return pixel size
                return new Size(PixelWidth, PixelHeight);
            }
            set
            {
                // Set the pixel height and pixel width
                PixelHeight = value.Height;
                PixelWidth = value.Width;

                // Raise resize event .
                PerformResize(new ResizeEventArgs(PixelWidth, PixelHeight));
            }
        }




        /// <summary>
        /// Gets or sets the top.
        /// </summary>
        /// <value>
        /// The top.
        /// </value>
        public Unit Top
        {
            get { return _top; }
            set
            {
                if (_top != value)
                {
                    _top = value;

                    OnPropertyChanged("Top");
                }
            }
        }




        /// <summary>
        /// Gets or sets the left.
        /// </summary>
        /// <value>
        /// The left.
        /// </value>
        public Unit Left
        {
            get { return _left; }
            set
            {
                if (_left != value)
                {
                    _left = value;

                    OnPropertyChanged("Left");
                }
            }
        }


        /// <summary>
        /// Gets or sets the tab index
        /// </summary>
        /// <value>
        /// The tab index
        /// </value>
        [RendererPropertyDescription]
        [DesignerIgnore]
        [DefaultValue(0)]
        public int TabIndex
        {
            get { return _tabIndex; }
            set
            {
                if (_tabIndex != value)
                {
                    _tabIndex = value;

                    OnPropertyChanged("TabIndex");
                }
            }
        }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Point Location
        {
            get
            {
                // Return pixel point
                return new Point(PixelLeft, PixelTop);
            }
            set
            {
                // Set the pixel top and pixel left
                PixelLeft = value.X;
                PixelTop = value.Y;
            }
        }


        /// <summary>
        /// Gets or sets the Visible.
        /// </summary>
        /// <value>
        /// The Visible.
        /// </value>
        [DesignerIgnore]
        [RendererPropertyDescription]
        [DefaultValue(false)]
        public bool Visible
        {
            get
            {
                // If is visible
                return GetVisibleCore();
            }
            set
            {
                // If is visible
                if (value)
                {
                    // If is not visible
                    if (!Visible)
                    {
                        // Set the visible core
                        SetVisibleCore(true);
                        // Load the control if needed
                        LoadIfNeeded();
                    }

                }
                else
                {
                    // If is visible
                    if (Visible)
                    {
                        SetVisibleCore(false);
                    }
                }

                // Notify visibility change
                OnVisibleChanged(EventArgs.Empty);

                // Indicate property changed
                OnPropertyChanged("Visible");
            }
        }




        private Padding _Margin = new Padding(0);
        /// <summary>
        /// Specifies the margin for this component. 
        /// </summary>
        /// <example>Example: '10,5,3,10' (left, top, right, bottom).</example>
        /// <value>
        /// The control margin.
        /// </value>
        [Category("Appearance")]
        public Padding Margin
        {
            get { return _Margin; }
            set
            {
                if (_Margin != value)
                {
                    _Margin = value;
                }
            }
        }


        private Padding _Padding = new Padding(0);
        /// <summary>
        /// Specifies the padding for this component. 
        /// </summary>
        /// <example>Example: '10,5,3,10' (left, top, right, bottom).</example>
        /// <value>
        /// The control margin.
        /// </value>
        [Category("Appearance")]
        [RendererPropertyDescription]
        public Padding Padding
        {
            get { return _Padding; }
            set
            {
                if (_Padding != value)
                {
                    _Padding = value;
                    OnPropertyChanged("Padding");
                }
            }
        }


        /// <summary>
        /// Navigates the bookmark.
        /// </summary>
        /// <param name="historyToken">The history token.</param>
        internal void NavigateBookmark(string historyToken)
        {
            // Try to get client handler
            IClientHistoryHandler historyHandler = this as IClientHistoryHandler;
            if (historyHandler != null)
            {
                historyHandler.Navigate(historyToken);
            }

            // Loop all child controls
            foreach (ControlElement controlElement in this.Controls)
            {
                // If there is a valid control
                if (controlElement != null)
                {
                    // Navigate book mark
                    controlElement.NavigateBookmark(historyToken);
                }
            }
        }


        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public virtual bool Enabled
        {
            get { return _enabled; }
            set
            {
                // If is a different value
                if (_enabled != value)
                {
                    // Set enabled
                    _enabled = value;

                    // Indicate property changed
                    OnPropertyChanged("Enabled");
                }
            }
        }


        /// <summary>
        /// Gets or sets the Enabled.
        /// </summary>
        /// <value>
        /// The Enabled.
        /// </value>
        [DesignerIgnore]
        public virtual string Store
        {
            get { return _store; }
            set
            {
                // If is a different value
                if (_store != value)
                {
                    // Set enabled
                    _store = value;

                    // Indicate property changed
                    OnPropertyChanged("Store");
                }
            }
        }
        /// <summary>
        /// Gets or sets the custom UI
        /// </summary>
        [RendererPropertyDescription]
        public virtual string CustomUI { get; set; }

        /// <summary>
        /// Gets or sets the load action.
        /// </summary>
        /// <value>
        /// The load action.
        /// </value>
        [RendererEventDescription]
        public event EventHandler Load
        {
            add
            {
                bool needNotification = (_load == null);

                _load += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Load");
                }

            }
            remove
            {
                _load -= value;

                if (_load == null)
                {
                    OnEventHandlerDeattached("Load");
                }

            }
        }


        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load"/> instance containing the event data.</param>
        protected virtual void OnLoad(EventArgs args)
        {

            // Check if there are listeners.
            if (_load != null)
            {
                _load(this, args);
            }
        }



        public virtual object Invoke(Delegate method, params object[] args)
        {
            return default(object);
        }



        public object Invoke(Delegate method)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(object);
        }




        /// <summary>
        /// Shows this control.
        /// </summary>
        public virtual void Show()
        {
            Visible = true;
        }




        /// <summary>
        /// Creates the control element collection.
        /// </summary>
        /// <returns></returns>
        protected virtual ControlElementCollection CreateControlElementCollection()
        {
            return new ControlElementCollection(this, "Controls");
        }




        /// <summary>
        /// Removes the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        public override void Remove(VisualElement visualElement)
        {
            // Try to get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                // Add to control list
                Controls.Remove(controlElement);
            }
            else
            {
                base.Remove(visualElement);
            }
        }




        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
        {
            // Try to get control element
            ControlElement controlElement = visualElement as ControlElement;

            // If there is a valid control element
            if (controlElement != null)
            {
                if (insert)
                {
                    Controls.Insert(0, controlElement);
                }
                else
                {
                    // Add to control list
                    Controls.Add(controlElement);

                }

                // Perform controll added
                this.PerformControlAdded(new ControlEventArgs(controlElement));
            }
            else
            {
                // Try get control settings
                ControlSettings objControlSettings = visualElement as ControlSettings;

                // If there is a valid control settings
                if (objControlSettings != null)
                {
                    // Set settings
                    Dock = objControlSettings.Dock;
                    Height = objControlSettings.Height;
                    Width = objControlSettings.Width;
                }

                // Add visual element
                base.Add(visualElement, insert);
            }

            OnLayout(new LayoutEventArgs(controlElement, ""));
        }









        /// <summary>
        /// Loads control if needed.
        /// </summary>
        private void LoadIfNeeded()
        {
            // If control was not loaded
            if (!IsLoaded)
            {
                // Mark control as loaded
                IsLoaded = true;

                // Load this control
                PerformLoad(EventArgs.Empty);
            }
        }




        /// <summary>
        /// Hides this control
        /// </summary>
        public virtual void Hide()
        {
            Visible = false;
        }




        public void SuspendLayout()
        {

        }



        public void ResumeLayout()
        {

        }



        public void ResumeLayout(bool blnPerformLayout)
        {

        }


        /// <summary>
        /// Gets or sets the layout action.
        /// </summary>
        /// <value>
        /// The layout action.
        /// </value>

        public event EventHandler<LayoutEventArgs> Layout
        {
            add
            {
                bool needNotification = (_layout == null);

                _layout += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Layout");
                }

            }
            remove
            {
                _layout -= value;

                if (_layout == null)
                {
                    OnEventHandlerDeattached("Layout");
                }

            }
        }
        /// <summary>
        /// Raises the <see cref="E:Layout" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Layout"/> instance containing the event data.</param>
        protected virtual void OnLayout(LayoutEventArgs args)
        {

            // Check if there are listeners.
            if (_layout != null)
            {
                _layout(this, args);
            }
        }


        /// <summary>
        /// Performs the layout event.
        /// </summary>
        public void PerformLayout(LayoutEventArgs args)
        {
            OnLayout(args);
        }




        /// <summary>
        /// Called when view initialized.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected internal virtual void OnInitialize(EventArgs args)
        {

        }




        /// <summary>
        /// Performs the view initialized.
        /// </summary>
        public void PerformInitialize()
        {
            this.OnInitialize(EventArgs.Empty);
        }

        /// <summary>
        /// Performs the view restored.
        /// </summary>
        public void PerformViewRestored()
        {
            this.OnViewRestored();
        }

        /// <summary>
        /// Called when view restored.
        /// </summary>
        protected internal virtual void OnViewRestored()
        {

        }


        /// <summary>
        /// Performs the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformLoad(EventArgs args)
        {
            // Raise the Load event.
            this.OnLoad(args);
        }




        /// <summary>
        /// Gets the visible core.
        /// </summary>
        /// <returns></returns>
        private bool GetVisibleCore()
        {
            return (_state & ControlElementState.Hidden) == ControlElementState.None;
        }




        /// <summary>
        /// Focuses this instance.
        /// </summary>
        /// <returns></returns>
        [RendererMethodDescription]
        public virtual void Focus()
        {
            // TODO: focus implementaion actions / operations / methods
            //// Get the focus operation
            //FocusActionArgs focusOperation = ApplicationElement.EnqueueUniqueAction<FocusActionArgs>();

            //// If there is a valid focus operation
            //if (focusOperation != null)
            //{
            //    // Set the focused element
            //    focusOperation.FocusedElement = this;
            //}

            this.InvokeMethod("Focus", null);
        }

        /// <summary>
        /// Set loading indicator.
        /// </summary>
        /// <returns></returns>
        [RendererMethodDescription]
        public void SetLoading(bool state)
        {
            this.InvokeMethodOnce("SetLoading", state);
        }

        /// <summary>
        /// Set loading indicator.
        /// </summary>
        /// <returns></returns>
        [RendererMethodDescription]
        public void SetLoading(string message)
        {
            this.InvokeMethodOnce("SetLoading", message);
        }

        /// <summary>
        /// Occurs when data source item updated.
        /// </summary>
        public event EventHandler DataSourceItemUpdated;


        /// <summary>
        /// Called when data source item updated.
        /// </summary>
        /// <param name="item">The item.</param>
        public override void OnDataSourceItemUpdated(object item)
        {
            if (this.DataSourceItemUpdated != null)
            {
                this.DataSourceItemUpdated(item, EventArgs.Empty);
            }
            base.OnDataSourceItemUpdated(item);
        }

        /// <summary>
        /// Occurs when data source item removed.
        /// </summary>
        public event EventHandler DataSourceItemDeleted;

        /// <summary>
        /// Called when data source item deleted.
        /// </summary>
        /// <param name="item">The item.</param>
        protected internal override void OnDataSourceItemDeleted(object item)
        {
            if (this.DataSourceItemDeleted != null)
            {
                this.DataSourceItemDeleted(item, EventArgs.Empty);
            }
            base.OnDataSourceItemDeleted(item);
        }

        /// <summary>
        /// Occurs when data source item created.
        /// </summary>
        public event EventHandler DataSourceItemCreated;

        /// <summary>
        /// Called when data source item created.
        /// </summary>
        /// <param name="item">The item.</param>
        protected internal override void OnDataSourceItemCreated(object item)
        {
            if (this.DataSourceItemCreated != null)
            {
                this.DataSourceItemCreated(item, EventArgs.Empty);
            }
            base.OnDataSourceItemCreated(item);
        }

        /// <summary>
        /// Occurs when data source item created.
        /// </summary>
        public event EventHandler DataSourceCurrentItemChanged;

        /// <summary>
        /// Called when data source current item changed.
        /// </summary>
        /// <param name="item">The item.</param>
        protected internal override void OnDataSourceCurrentItemChanged(object item)
        {
            if (this.DataSourceCurrentItemChanged != null)
            {
                this.DataSourceCurrentItemChanged(item, EventArgs.Empty);
            }
            base.OnDataSourceCurrentItemChanged(item);
        }


        /// <summary>
        /// MouseTapHold event
        /// </summary>
        private event EventHandler<MouseEventArgs> _mouseTapHold;

        /// <summary>
        /// Occurs when holding the mouse's left click for more than 1 second
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<MouseEventArgs> MouseTapHold
        {
            add
            {
                bool needNotification = (_mouseTapHold == null);

                _mouseTapHold += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseTapHold");
                }

            }
            remove
            {
                _mouseTapHold -= value;

                if (_mouseTapHold == null)
                {
                    OnEventHandlerDeattached("MouseTapHold");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:MouseTapHold" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public virtual void OnMouseTapHold(MouseEventArgs args)
        {
            // Check if there are listeners.
            if (_mouseTapHold != null)
            {
                _mouseTapHold(this, args);
            }
        }


        /// <summary>
        /// Gets a value indicating whether this instance has MouseTapHold listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after TapHold listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasMouseTapHoldListeners
        {
            get { return _mouseTapHold != null; }
        }

        /// <summary>
        /// Performs the date Cell click event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformMouseTapHold(MouseEventArgs args)
        {          
            OnMouseTapHold(args);
        }


        [DesignerIgnore]
        public string ClientClick { get; set; }

        /// <summary>
        /// Gets or sets the click action.
        /// </summary>
        /// <value>
        /// The click action.
        /// </value>
        [RendererEventDescription]
        public event EventHandler Click
        {
            add
            {
                bool needNotification = (_click == null);

                _click += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Click");
                }

            }
            remove
            {
                _click -= value;

                if (_click == null)
                {
                    OnEventHandlerDeattached("Click");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Click"/> instance containing the event data.</param>
        protected virtual void OnClick(EventArgs args)
        {

            // Check if there are listeners.
            if (_click != null)
            {
                _click(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Click" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformClick(EventArgs args)
        {

            // Raise the Click event.
            OnClick(args);
        }

        /// <summary>
        /// Gets a value indicating whether has click listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has click listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasClickListeners
        {
            get { return _click != null; }
        }

        /// <summary>
        /// Gets a value indicating whether has MouseEnter listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has MouseEnter listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasMouseEnterListeners
        {
            get { return _mouseEnter != null; }
        }

        /// <summary>
        /// Gets a value indicating whether has MouseLeave listeners.
        /// </summary>
        /// <value>
        ///   <c>true</c> if has MouseLeave listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasMouseLeaveListeners
        {
            get { return _mouseLeave != null; }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        IEnumerator<ControlElement> IEnumerable<ControlElement>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        protected IEnumerator<ControlElement> GetEnumerator()
        {
            return Controls.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Controls.GetEnumerator();
        }




        /// <summary>
        /// Creates the Graphics for the control.
        /// </summary>
        /// <returns></returns>
        public Graphics CreateGraphics()
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(Graphics);
        }




        /// <summary>
        /// Supports rendering to the specified bitmap
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="targetBounds">The target bounds.</param>
        public void DrawToBitmap(Bitmap bitmap, Rectangle targetBounds)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }




        /// <summary>
        /// Invalidates a specific region of the control and causes a paint message to be sent to the control.
        /// </summary>
        public void Invalidate()
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }




        public Point PointToClient(Point p)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(Point);
        }




        /// <summary>
        /// Creates the control.
        /// </summary>
        public void CreateControl()
        {
            // If control had not been created
            if (!IsCreated)
            {
                // Indicate created
                IsCreated = true;

                // Call on create control
                OnCreateControl();
            }
        }




        public bool SelectNextControl(ControlElement ctl, bool forward, bool tabStopOnly, bool nested, bool wrap)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(bool);
        }




        [RendererMethodDescription]
        public virtual void Refresh()
        {
        }

        /// <summary>
        /// Sets the bounds of the control to the specified location and size
        /// </summary>
        /// <param name="x">x location .</param>
        public void SetBounds(int x)
        {
            SetBounds(x, this.PixelTop);
        }

        /// <summary>
        /// Sets the bounds of the control to the specified location and size
        /// </summary>
        /// <param name="x"> x location </param>
        /// <param name="y">y location</param>
        public void SetBounds(int x, int y)
        {
            SetBounds(x, y, this.PixelWidth);
        }

        /// <summary>
        /// Sets the bounds of the control to the specified location and size
        /// </summary>
        /// <param name="x">x location</param>
        /// <param name="y">y location</param>
        /// <param name="width">The width.</param>
        public void SetBounds(int x, int y, int width)
        {
            SetBounds(x, y, width, this.PixelHeight);
        }

        /// <summary>
        /// Sets the bounds of the control to the specified location and size
        /// </summary>
        /// <param name="x">x location</param>
        /// <param name="y">y location</param>
        /// <param name="width">The width.</param>
        /// <param name="height">height size</param>
        public void SetBounds(int x, int y, int width, int height)
        {
            this.PixelTop = y;
            this.PixelLeft = x;
            this.PixelWidth = width;
            this.PixelHeight = height;
        }

        protected virtual void OnEnabledChanged(EventArgs e)
        {
        }




        /// <summary>
        /// Sets the visible core.
        /// </summary>
        /// <param name="value">if set to <c>true</c> value.</param>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        protected virtual void SetVisibleCore(bool value)
        {
            // If there is a valid value
            if (value)
            {
                // Remove the hidden flag
                _state = _state & ~ControlElementState.Hidden;
            }
            else
            {
                // Remove the hidden flag
                _state = _state | ControlElementState.Hidden;
            }

        }




        /// <summary>
        /// Called when control is created.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        protected virtual void OnCreateControl()
        {
            // Load if needed
            LoadIfNeeded();
        }




        /// <summary>
        /// Updates this control.
        /// </summary>
        public void Update()
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }




        /// <summary>
        /// Begins the invoke.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public virtual IAsyncResult BeginInvoke(Delegate method, params object[] args)
        {
            return default(IAsyncResult);
        }




        public Point PointToScreen(Point p)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(Point);
        }




        public void BringToFront()
        {
            ControlElement controlElement = Parent;

            if (controlElement != null)
            {

            }
        }




        protected virtual void OnMouseWheel(MouseEventArgs e)
        {
        }




        protected virtual void CreateHandle()
        {
        }




        protected virtual void OnHandleCreated(EventArgs e)
        {
        }




        public void Invalidate(Rectangle rc)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }




        protected virtual void WndProc(Message m)
        {
        }



        public event EventHandler VisibleChanged
        {
            add
            {
                bool needNotification = (_visibleChanged == null);

                _visibleChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("VisibleChanged");
                }

            }
            remove
            {
                _visibleChanged -= value;

                if (_visibleChanged == null)
                {
                    OnEventHandlerDeattached("VisibleChanged");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:VisibleChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="VisibleChanged"/> instance containing the event data.</param>
        protected virtual void OnVisibleChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_visibleChanged != null)
            {
                _visibleChanged(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:VisibleChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformVisibleChanged(EventArgs args)
        {
            // Raise the VisibleChanged event.
            this.OnVisibleChanged(args);
        }


        [RendererEventDescription]
        public event EventHandler MouseHover
        {
            add
            {
                bool needNotification = (_mouseHover == null);

                _mouseHover += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseHover");
                }

            }
            remove
            {
                _mouseHover -= value;

                if (_mouseHover == null)
                {
                    OnEventHandlerDeattached("MouseHover");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:MouseHover" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseHover"/> instance containing the event data.</param>
        protected virtual void OnMouseHover(EventArgs args)
        {

            // Check if there are listeners.
            if (_mouseHover != null)
            {
                _mouseHover(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseHover" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformMouseHover(EventArgs args)
        {
            // Raise the MouseHover event.
            this.OnMouseHover(args);
        }


        /// <summary>
        /// Gets a value indicating whether [has MouseHover listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has MouseHover listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasMouseHoverListeners
        {
            get
            {
                return (_mouseHover != null);
            }
        }


        [RendererEventDescription]
        public event EventHandler MouseEnter
        {
            add
            {
                bool needNotification = (_mouseEnter == null);

                _mouseEnter += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseEnter");
                }

            }
            remove
            {
                _mouseEnter -= value;

                if (_mouseEnter == null)
                {
                    OnEventHandlerDeattached("MouseEnter");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEnter"/> instance containing the event data.</param>
        protected virtual void OnMouseEnter(EventArgs args)
        {

            // Check if there are listeners.
            if (_mouseEnter != null)
            {
                _mouseEnter(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformMouseEnter(EventArgs args)
        {
            // Raise the MouseEnter event.
            this.OnMouseEnter(args);
        }




        [RendererEventDescription]
        public event EventHandler MouseLeave
        {
            add
            {
                bool needNotification = (_mouseLeave == null);

                _mouseLeave += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseLeave");
                }

            }
            remove
            {
                _mouseLeave -= value;

                if (_mouseLeave == null)
                {
                    OnEventHandlerDeattached("MouseLeave");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseLeave" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseLeave"/> instance containing the event data.</param>
        protected virtual void OnMouseLeave(EventArgs args)
        {

            // Check if there are listeners.
            if (_mouseLeave != null)
            {
                _mouseLeave(this, args);
            }
        }



        /// <summary>
        /// EventName event handler.
        /// </summary>
        private event EventHandler<ResizeEventArgs> _Resize;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove Resize action.
        /// </summary>
        public event EventHandler<ResizeEventArgs> Resize
        {
            add
            {
                bool needNotification = (_Resize == null);

                _Resize += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Resize");
                }

            }
            remove
            {
                _Resize -= value;

                if (_Resize == null)
                {
                    OnEventHandlerDeattached("Resize");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has Resize listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasResizeListeners
        {
            get { return _Resize != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:Resize" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Resize"/> instance containing the event data.</param>
        protected virtual void OnResize(ResizeEventArgs args)
        {

            // Check if there are listeners.
            if (_Resize != null)
            {
                Width = args.Width;
                Height = args.Height;
                _Resize(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Resize" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ResizeEventArgs"/> instance containing the event data.</param>
        public void PerformResize(ResizeEventArgs args)
        {
            // Raise the Resize event.
            OnResize(args);
        }




        public event EventHandler Validating
        {
            add
            {
                bool needNotification = (_validating == null);

                _validating += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Validating");
                }

            }
            remove
            {
                _validating -= value;

                if (_validating == null)
                {
                    OnEventHandlerDeattached("Validating");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:Validating" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Validating"/> instance containing the event data.</param>
        protected virtual void OnValidating(EventArgs args)
        {

            // Check if there are listeners.
            if (_validating != null)
            {
                _validating(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:Validating" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformValidating(EventArgs args)
        {
            // Raise the Validating event.
            this.OnValidating(args);
        }




        /// <summary>
        /// Gets or sets the text changed action.
        /// </summary>
        /// <value>
        /// The text changed action.
        /// </value>
        [RendererEventDescription]
        public event EventHandler TextChanged
        {
            add
            {
                bool needNotification = (_textChanged == null);

                _textChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("TextChanged");
                }

            }
            remove
            {
                _textChanged -= value;

                if (_textChanged == null)
                {
                    OnEventHandlerDeattached("TextChanged");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:TextChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="TextChanged"/> instance containing the event data.</param>
        protected virtual void OnTextChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_textChanged != null)
            {
                _textChanged(this, args);
            }
        }


        /// <summary>
        /// Performs the <see cref="E:TextChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformTextChanged(EventArgs args)
        {
            // Try to get string value changed arguments
            ValueChangedArgs<string> stringValueChangedArgs = args as ValueChangedArgs<string>;

            // If there is a valid string value changed arguments
            if (stringValueChangedArgs != null && !String.Equals(this._text, stringValueChangedArgs.Value, StringComparison.OrdinalIgnoreCase))
            {
                // Set text value
                this._text = string.IsNullOrEmpty(stringValueChangedArgs.Value) ? string.Empty : stringValueChangedArgs.Value;

                // Raise the TextChanged event.
                this.OnTextChanged(args);
            }
        }


        /// <summary>
        /// Gets a value indicating whether [has DragDrop listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has DragDrop listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDragDragDropListeners
        {
            get
            {
                return _dragDrop != null;
            }
        }


        [RendererEventDescription]
        public event EventHandler DragDrop
        {
            add
            {
                bool needNotification = (_dragDrop == null);

                _dragDrop += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DragDrop");
                }

            }
            remove
            {
                _dragDrop -= value;

                if (_dragDrop == null)
                {
                    OnEventHandlerDeattached("DragDrop");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:DragDrop" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DragDrop"/> instance containing the event data.</param>
        protected virtual void OnDragDrop(DragEventArgs args)
        {
            // Check if there are listeners.
            if (_dragDrop != null)
            {
                _dragDrop(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:DragDrop" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void PerformDragDrop(DragEventArgs args)
        {

            // Raise the DragDrop event.
            this.OnDragDrop(args);
        }


        /// <summary>
        /// Gets a value indicating whether [has drag over listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has drag over listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDragOverListeners
        {
            get
            {
                return _dragOver != null;
            }
        }


        [RendererEventDescription]
        public event EventHandler DragOver
        {
            add
            {
                bool needNotification = (_dragOver == null);

                _dragOver += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DragOver");
                }

            }
            remove
            {
                _dragOver -= value;

                if (_dragOver == null)
                {
                    OnEventHandlerDeattached("DragOver");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:DragOver" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DragOver"/> instance containing the event data.</param>
        protected virtual void OnDragOver(DragEventArgs args)
        {
            // Check if there are listeners.
            if (_dragOver != null)
            {
                _dragOver(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:DragOver" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDragOver(DragEventArgs args)
        {
            // Raise the DragOver event.
            OnDragOver(args);
        }




        public event EventHandler SizeChanged
        {
            add
            {
                bool needNotification = (_sizeChanged == null);

                _sizeChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("SizeChanged");
                }

            }
            remove
            {
                _sizeChanged -= value;

                if (_sizeChanged == null)
                {
                    OnEventHandlerDeattached("SizeChanged");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:SizeChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="SizeChanged"/> instance containing the event data.</param>
        protected virtual void OnSizeChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_sizeChanged != null)
            {
                _sizeChanged(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:SizeChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformSizeChanged(EventArgs args)
        {
            // Raise the SizeChanged event.
            OnSizeChanged(args);
        }




        [RendererEventDescription]
        public event EventHandler<MouseEventArgs> MouseMove
        {
            add
            {
                bool needNotification = (_mouseMove == null);

                _mouseMove += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseMove");
                }

            }
            remove
            {
                _mouseMove -= value;

                if (_mouseMove == null)
                {
                    OnEventHandlerDeattached("MouseMove");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseMove" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseMove"/> instance containing the event data.</param>
        protected virtual void OnMouseMove(MouseEventArgs args)
        {

            // Check if there are listeners.
            if (_mouseMove != null)
            {
                _mouseMove(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseMove" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public void PerformMouseMove(MouseEventArgs args)
        {
            // Raise the MouseMove event.
            OnMouseMove(args);
        }




        [RendererEventDescription]
        public event EventHandler<MouseEventArgs> MouseDown
        {
            add
            {
                bool needNotification = (_mouseDown == null);

                _mouseDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseDown");
                }

            }
            remove
            {
                _mouseDown -= value;

                if (_mouseDown == null)
                {
                    OnEventHandlerDeattached("MouseDown");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseDown"/> instance containing the event data.</param>
        protected virtual void OnMouseDown(MouseEventArgs args)
        {

            // Check if there are listeners.
            if (_mouseDown != null)
            {
                _mouseDown(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public void PerformMouseDown(MouseEventArgs args)
        {

            // Raise the MouseDown event.
            OnMouseDown(args);
        }




        [RendererEventDescription]
        public event EventHandler<MouseEventArgs> MouseUp
        {
            add
            {
                bool needNotification = (_mouseUp == null);

                _mouseUp += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseUp");
                }

            }
            remove
            {
                _mouseUp -= value;

                if (_mouseUp == null)
                {
                    OnEventHandlerDeattached("MouseUp");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseUp"/> instance containing the event data.</param>
        protected virtual void OnMouseUp(MouseEventArgs args)
        {

            // Check if there are listeners.
            if (_mouseUp != null)
            {
                _mouseUp(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public void PerformMouseUp(MouseEventArgs args)
        {

            // Raise the MouseUp event.
            OnMouseUp(args);
        }




        [RendererEventDescription]
        public event EventHandler<TreeItemMouseClickEventArgs> MouseClick
        {
            add
            {
                bool needNotification = (_mouseClick == null);

                _mouseClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("MouseClick");
                }

            }
            remove
            {
                _mouseClick -= value;

                if (_mouseClick == null)
                {
                    OnEventHandlerDeattached("MouseClick");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:MouseClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseClick"/> instance containing the event data.</param>
        protected virtual void OnMouseClick(TreeItemMouseClickEventArgs args)
        {

            // Check if there are listeners.
            if (_mouseClick != null)
            {
                _mouseClick(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:MouseClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public void PerformMouseClick(TreeItemMouseClickEventArgs args)
        {

            // Raise the MouseClick event.
            OnMouseClick(args);
        }




        public event EventHandler LocationChanged
        {
            add
            {
                bool needNotification = (_locationChanged == null);

                _locationChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("LocationChanged");
                }

            }
            remove
            {
                _locationChanged -= value;

                if (_locationChanged == null)
                {
                    OnEventHandlerDeattached("LocationChanged");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:LocationChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="LocationChanged"/> instance containing the event data.</param>
        protected virtual void OnLocationChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_locationChanged != null)
            {
                _locationChanged(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:LocationChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformLocationChanged(EventArgs args)
        {
            // Raise the LocationChanged event.
            OnLocationChanged(args);
        }


        /// <summary>
        /// Occurs when the control is finished validating.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler Validated
        {
            add
            {
                bool needNotification = (_validated == null);

                _validated += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Validated");
                }

            }
            remove
            {
                _validated -= value;

                if (_validated == null)
                {
                    OnEventHandlerDeattached("Validated");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Validated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Validated"/> instance containing the event data.</param>
        protected virtual void OnValidated(EventArgs args)
        {

            // Check if there are listeners.
            if (_validated != null)
            {
                _validated(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has leave listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has leave listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasValidatedListeners
        {
            get { return _validated != null; }
        }



        /// <summary>
        /// Performs the <see cref="E:Validated" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformValidated(EventArgs args)
        {
            // Raise the Leave event.
            OnValidated(args);
        }



        [RendererEventDescription]
        public event EventHandler Leave
        {
            add
            {
                bool needNotification = (_leave == null);

                _leave += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Leave");
                }
            }
            remove
            {
                _leave -= value;

                if (_leave == null)
                {
                    OnEventHandlerDeattached("Leave");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:Leave" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Leave"/> instance containing the event data.</param>
        protected virtual void OnLeave(EventArgs args)
        {

            // Check if there are listeners.
            if (_leave != null)
            {
                _leave(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has leave listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has leave listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasLeaveListeners
        {
            get { return (_leave != null || _validated != null); }
        }



        /// <summary>
        /// Performs the <see cref="E:Leave" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformLeave(EventArgs args)
        {
            // Raise the Leave event.
            OnLeave(args);
        }


        [DesignerIgnore]
        public string ClientLostFocus { get; set; }


        /// <summary>
        /// LostFocus event handler.
        /// </summary>
        private event EventHandler _LostFocus;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove LostFocus action.
        /// </summary>
        public event EventHandler LostFocus
        {
            add
            {
                bool needNotification = (_LostFocus == null);

                _LostFocus += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("LostFocus");
                }

            }
            remove
            {
                _LostFocus -= value;

                if (_LostFocus == null)
                {
                    OnEventHandlerDeattached("LostFocus");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has LostFocus listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasLostFocusListeners
        {
            get { return _LostFocus != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:LostFocus" /> event.
        /// </summary>
        /// <param name="args">The <see cref="LostFocus"/> instance containing the event data.</param>
        protected virtual void OnLostFocus(EventArgs args)
        {

            // Check if there are listeners.
            if (_LostFocus != null)
            {
                _LostFocus(this, args);
             
            }
        }

        /// <summary>
        /// Performs the <see cref="E:LostFocus" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformLostFocus(EventArgs args)
        {
            this.IsFocused = false;
            // Raise the LostFocus event.
            OnLostFocus(args);
        }





        [RendererEventDescription]
        public event EventHandler DoubleClick
        {
            add
            {
                bool needNotification = (_doubleClick == null);

                _doubleClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DoubleClick");
                }

            }
            remove
            {
                _doubleClick -= value;

                if (_doubleClick == null)
                {
                    OnEventHandlerDeattached("DoubleClick");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:DoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DoubleClick"/> instance containing the event data.</param>
        protected virtual void OnDoubleClick(EventArgs args)
        {

            // Check if there are listeners.
            if (_doubleClick != null)
            {
                _doubleClick(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:DoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDoubleClick(EventArgs args)
        {

            // Raise the DoubleClick event.
            OnDoubleClick(args);
        }

        /// <summary>
        /// Gets a value indicating whether [has DragEnter listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has DragEnter listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDragEnterListeners
        {
            get
            {
                return _dragEnter != null;
            }
        }



        [RendererEventDescription]
        public event EventHandler DragEnter
        {
            add
            {
                bool needNotification = (_dragEnter == null);

                _dragEnter += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DragEnter");
                }

            }
            remove
            {
                _dragEnter -= value;

                if (_dragEnter == null)
                {
                    OnEventHandlerDeattached("DragEnter");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:DragEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DragEnter"/> instance containing the event data.</param>
        protected virtual void OnDragEnter(DragEventArgs args)
        {

            // Check if there are listeners.
            if (_dragEnter != null)
            {
                _dragEnter(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:DragEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDragEnter(DragEventArgs args)
        {

            // Raise the DragEnter event.
            OnDragEnter(args);
        }




        [RendererEventDescription]
        public event EventHandler Enter
        {
            add
            {
                bool needNotification = (_enter == null);

                _enter += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Enter");
                }

            }
            remove
            {
                _enter -= value;

                if (_enter == null)
                {
                    OnEventHandlerDeattached("Enter");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Enter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Enter"/> instance containing the event data.</param>
        protected virtual void OnEnter(EventArgs args)
        {

            // Check if there are listeners.
            if (_enter != null)
            {
                _enter(this, args);
            }
        }



        /// <summary>
        /// Performs the <see cref="E:Enter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformEnter(EventArgs args)
        {

            // Raise the Enter event.
            OnEnter(args);
        }




        /// <summary>
        /// Occurs when [control added].
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ControlEventArgs> ControlAdded
        {
            add
            {
                bool needNotification = (_controlAdded == null);

                _controlAdded += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ControlAdded");
                }

            }
            remove
            {
                _controlAdded -= value;

                if (_controlAdded == null)
                {
                    OnEventHandlerDeattached("ControlAdded");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:ControlAdded" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ControlEventArgs"/> instance containing the event data.</param>
        protected virtual void OnControlAdded(ControlEventArgs args)
        {

            // Check if there are listeners.
            if (_controlAdded != null)
            {
                _controlAdded(this, args);
            }
        }




        /// <summary>
        /// Performs the control added.
        /// </summary>
        /// <param name="args">The <see cref="ControlEventArgs"/> instance containing the event data.</param>
        public void PerformControlAdded(ControlEventArgs args)
        {
            OnControlAdded(args);
        }




        /// <summary>
        /// Occurs when [control removed].
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<ControlEventArgs> ControlRemoved
        {
            add
            {
                bool needNotification = (_controlRemoved == null);

                _controlRemoved += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("ControlRemoved");
                }

            }
            remove
            {
                _controlRemoved -= value;

                if (_controlRemoved == null)
                {
                    OnEventHandlerDeattached("ControlRemoved");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:ControlRemoved" /> event.
        /// </summary>
        /// <param name="args">The <see cref="ControlEventArgs"/> instance containing the event data.</param>
        protected virtual void OnControlRemoved(ControlEventArgs args)
        {

            // Check if there are listeners.
            if (_controlRemoved != null)
            {
                _controlRemoved(this, args);
            }
        }




        /// <summary>
        /// Performs the control removed.
        /// </summary>
        /// <param name="args">The <see cref="ControlEventArgs"/> instance containing the event data.</param>
        public void PerformControlRemoved(ControlEventArgs args)
        {

            OnControlRemoved(args);
        }




        /// <summary>
        /// Gets the key press masks.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Keys> GetKeyPressMasks()
        {
            // If there key down listeners
            if (_keyDown != null)
            {
                // Return all key masks
                foreach (Keys keyMask in GetKeyMasks(_keyDown.GetInvocationList()))
                {
                    yield return keyMask;
                }
            }

            // If there key up listeners
            if (_keyUp != null)
            {
                // Return all key masks
                foreach (Keys keyMask in GetKeyMasks(_keyUp.GetInvocationList()))
                {
                    yield return keyMask;
                }
            }

            // If there key press listeners
            if (_keyPress != null)
            {
                // Return all key masks
                foreach (Keys keyMask in GetKeyMasks(_keyPress.GetInvocationList()))
                {
                    yield return keyMask;
                }
            }
        }




        /// <summary>
        /// Gets the key masks.
        /// </summary>
        /// <param name="delegates">The delegates.</param>
        /// <returns></returns>
        private IEnumerable<Keys> GetKeyMasks(IEnumerable<Delegate> delegates)
        {
            // If there is a valid delegates list
            if (delegates != null)
            {
                // Loop delegates
                foreach (Delegate invocationDelegate in delegates)
                {
                    // Get method info
                    MethodInfo methodInfo = invocationDelegate.Method;

                    // If there is a valid method info
                    if (methodInfo != null)
                    {
                        // Get mask attribute
                        foreach (KeyMaskAttribute maskAttribute in methodInfo.GetCustomAttributes<KeyMaskAttribute>(false))
                        {
                            // If there is a valid attribute
                            if (maskAttribute != null)
                            {
                                // Get the key masks
                                Keys[] keyMasks = maskAttribute.KeyMasks;

                                // If there are valid key masks
                                if (keyMasks != null)
                                {
                                    // Loop all key masks
                                    foreach (Keys keyMask in keyMasks)
                                    {
                                        // Return key mask
                                        yield return keyMask;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [DesignerIgnore]
        public string ClientKeyPress { get; set; }

        [RendererEventDescription]
        public event EventHandler<KeyPressEventArgs> KeyPress
        {
            add
            {
                bool needNotification = (_keyPress == null);

                _keyPress += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("KeyPress");
                }

            }
            remove
            {
                _keyPress -= value;

                if (_keyPress == null)
                {
                    OnEventHandlerDeattached("KeyPress");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:KeyPress" /> event.
        /// </summary>
        /// <param name="args">The <see cref="KeyPress"/> instance containing the event data.</param>
        protected virtual void OnKeyPress(KeyPressEventArgs args)
        {

            // Check if there are listeners.
            if (_keyPress != null)
            {
                _keyPress(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:KeyPress" /> event.
        /// </summary>
        /// <param name="args">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
        public void PerformKeyPress(KeyPressEventArgs args)
        {

            // Raise the KeyPress event.
            OnKeyPress(args);
        }




        /// <summary>
        /// Signals the object that initialization is starting.
        /// </summary>
        void ISupportInitialize.BeginInit()
        {
            BeginInit();
        }




        protected void BeginInit()
        {

        }




        /// <summary>
        /// Signals the object that initialization is complete.
        /// </summary>
        void ISupportInitialize.EndInit()
        {
            EndInit();
        }




        protected void EndInit()
        {

        }




        /// <summary>
        ///Ensures that the specified item is visible within the control, scrolling the contents of the control if necessary.
        /// </summary>
        /// <param name="index">The zero-based index of the item to scroll into view. </param>
        public void EnsureVisible(int index)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }

        [DesignerIgnore]
        public string ClientGotFocus { get; set; }

        /// <summary>
        ///Occurs when the control receives focus.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler GotFocus
        {
            add
            {
                bool needNotification = (_gotFocus == null);

                _gotFocus += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("GotFocus");
                }

            }
            remove
            {
                _gotFocus -= value;

                if (_gotFocus == null)
                {
                    OnEventHandlerDeattached("GotFocus");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:GotFocus" /> event.
        /// </summary>
        /// <param name="args">The <see cref="GotFocus"/> instance containing the event data.</param>
        protected virtual void OnGotFocus(EventArgs args)
        {

            // Check if there are listeners.
            if (_gotFocus != null)
            {
                _gotFocus(this, args);
               
            }
        }




        /// <summary>
        /// Performs the <see cref="E:GotFocus" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformGotFocus(EventArgs args)
        {
            this.IsFocused = true;
            // Raise the Enter event
            PerformEnter(args);

            // Raise the GotFocus event.
            OnGotFocus(args);
        }




        /// <summary>
        ///Resets the Control.Text property to its default value.
        /// </summary>
        public virtual void ResetText()
        {
            this.Text = string.Empty;
        }




        /// <summary>
        ///Sends the control to the back of the z-order.
        /// </summary>
        public void SendToBack()
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }




        /// <summary>
        ///Retrieves the form that the control is on.
        /// </summary>
        ///  <returns>The T:System.Windows.Forms.Form that the control is on.</returns>
        public WindowElement FindForm()
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(WindowElement);
        }


        /// <summary>
        /// Gets the current active element
        /// </summary>
        /// <returns></returns>
        [RendererMethodDescription]
        public Task<ControlElement> GetActiveControl()
        {
            TaskCompletionSource<ControlElement> taskSource = new TaskCompletionSource<ControlElement>();

            Action<object> methodCallback = (result) =>
            {
                ControlElement controlElement = new ControlElement();
                if (result != null)
                {
                    controlElement = this.GetVisualElementById<ControlElement>(result.ToString());
                }

                RenderingContext.SetSynchronizationCallbackResult(taskSource, controlElement);
            };
            this.InvokeMethod("GetActiveControl", null, methodCallback);

            return taskSource.Task;
        }


        /// <summary>
        /// Sets the active control
        /// </summary>
        [RendererMethodDescription]
        public void SetActiveControl(string controlId)
        {
            this.InvokeMethodOnce("SetActiveControl", controlId);
        }




        /// <summary>
        ///Executes the specified delegate asynchronously on the thread that the control's underlying handle was created on.
        /// </summary>
        /// <param name="method">A delegate to a method that takes no parameters. </param>
        ///  <returns>An T:System.IAsyncResult that represents the result of the M:System.Windows.Forms.Control.BeginInvoke(System.Delegate) operation.</returns>
        public IAsyncResult BeginInvoke(Delegate method)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
            return default(IAsyncResult);
        }




        /// <summary>
        ///Creates a new instance of the control collection for the control.
        /// </summary>
        ///  <returns>A new instance of T:System.Windows.Forms.Control.ControlCollection assigned to the control.</returns>
        protected virtual ControlElementCollection CreateControlsInstance()
        {
            return default(ControlElementCollection);
        }




        /// <summary>
        ///Occurs when a key is released while the control has focus.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<KeyEventArgs> KeyUp
        {
            add
            {
                bool needNotification = (_keyUp == null);

                _keyUp += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("KeyUp");
                }

            }
            remove
            {
                _keyUp -= value;

                if (_keyUp == null)
                {
                    OnEventHandlerDeattached("KeyUp");
                }

            }
        }



        /// <summary>
        /// Raises the <see cref="E:KeyUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="KeyUp"/> instance containing the event data.</param>
        protected virtual void OnKeyUp(KeyEventArgs args)
        {

            // Check if there are listeners.
            if (_keyUp != null)
            {
                _keyUp(this, args);
            }
        }




        /// <summary>
        /// Performs the <see cref="E:KeyUp" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformKeyUp(KeyEventArgs args)
        {
            // Raise the KeyUp event.
            OnKeyUp(args);
        }


        [DesignerIgnore]
        public string ClientKeyDown { get; set; }

        /// <summary>
        ///Occurs when a key is pressed while the control has focus.
        /// </summary>
        [RendererEventDescription]
        public event EventHandler<KeyDownEventArgs> KeyDown
        {
            add
            {
                bool needNotification = (_keyDown == null);

                _keyDown += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("KeyDown");
                }

            }
            remove
            {
                _keyDown -= value;

                if (_keyDown == null)
                {
                    OnEventHandlerDeattached("KeyDown");
                }

            }
        }




        /// <summary>
        /// Raises the <see cref="E:KeyDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="KeyDown"/> instance containing the event data.</param>
        protected virtual void OnKeyDown(KeyDownEventArgs args)
        {

            // Check if there are listeners.
            if (_keyDown != null)
            {
                _keyDown(this, args);
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has key press listeners].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has key press listeners]; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasKeyDownListeners
        {
            get { return _keyDown != null; }
        }


        /// <summary>
        /// Performs the <see cref="E:KeyDown" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformKeyDown(KeyDownEventArgs args)
        {

            // Raise the KeyDown event.
            OnKeyDown(args);
        }




        /// <summary>
        ///Sets a specified T:System.Windows.Forms.ControlStyles flag to either true or false.
        /// </summary>
        /// <param name="flag">The T:System.Windows.Forms.ControlStyles bit to set. </param>
        /// <param name="value">true to apply the specified style to the control; otherwise, false. </param>
        protected void SetStyle(ControlStyles flag, bool value)
        {
            _temp2SuppressCA1822 = 0; // Suppress CA1822: Mark members as static
        }

        /// <summary>
        ///
        /// </summary>
        [RendererEventDescription]
        public virtual event EventHandler<EventArgs> Afterrender
        {
            add
            {
                bool needNotification = (_afterrender == null);

                _afterrender += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("Afterrender");
                }

            }
            remove
            {
                _afterrender -= value;

                if (_afterrender == null)
                {
                    OnEventHandlerDeattached("Afterrender");
                }

            }
        }

        /// <summary>
        /// Raises the <see cref="E:Afterrender" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Afterrender"/> instance containing the event data.</param>
        protected virtual void OnAfterrender(EventArgs args)
        {
            // Check if there are listeners.
            if (_afterrender != null )
            {
                _afterrender(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:Afterrender" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformAfterrender(EventArgs args)
        {
            // Raise the Afterrender event.
            OnAfterrender(args);
        }

        /// <summary>
        /// Context Menu Strip property
        /// </summary>
        [DefaultValue(null)]
        [System.Web.UI.IDReferenceProperty]
        public ContextMenuStripElement ContextMenuStrip
        {
            get;
            set;
        }

        [RendererMethodDescription]
        internal void SetError(string value)
        {
            this._errorString = value;
            this.InvokeMethod("SetError", value);
        }

        private bool _Resizable = false;

        [RendererPropertyDescription]
        /// <summary>
        /// Resizable Description.
        /// </summary>
        [DefaultValue(false)]
        public virtual bool Resizable
        {
            get
            {
                return _Resizable;
            }
            set
            {
                if (_Resizable != value)
                {
                    _Resizable = value;

                    OnPropertyChanged("Resizable");
                }
            }
        }

        /// <summary>
        /// image list 
        /// </summary>
        [System.Web.UI.IDReferenceProperty]
        public ImageList ImageList
        {
            get;
            set;
        }

        private string _dataMember = "";
        [RendererPropertyDescription]
        public virtual string DataMember
        {
            get
            {
                return this._dataMember;
            }
            set
            {
                this._dataMember = value;
            }
        }


        private string FocusedWidgetcontrol
        {
            get;
            set;
        }

        private string ValueWidgetcontrol
        {
            get;
            set;
        }

        /// <summary>
        /// WidgetControlEditChanged event handler.
        /// </summary>
        private event EventHandler<EventArgs> _WidgetControlEditChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove WidgetControlEditChanged action.
        /// </summary>
        public event EventHandler<EventArgs> WidgetControlEditChanged
        {
            add
            {
                bool needNotification = (_WidgetControlEditChanged == null);

                _WidgetControlEditChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlEditChanged");
                }

            }
            remove
            {
                _WidgetControlEditChanged -= value;

                if (_WidgetControlEditChanged == null)
                {
                    OnEventHandlerDeattached("WidgetControlEditChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has WidgetControlEditChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasWidgetControlEditChangedListeners
        {
            get { return _WidgetControlEditChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:WidgetControlEditChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlEditChanged"/> instance containing the event data.</param>
        protected virtual void OnWidgetControlEditChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_WidgetControlEditChanged != null)
            {
                _WidgetControlEditChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WidgetControlEditChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWidgetControlEditChanged(EventArgs args)
        {

            // Raise the WidgetControlEditChanged event.
            OnWidgetControlEditChanged(args);
        }



        /// <summary>
        /// ItemChanged event handler.
        /// </summary>
        private event EventHandler<EventArgs> _widgetControlItemChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove ItemChanged action.
        /// </summary>
        public event EventHandler<EventArgs> WidgetControlItemChanged
        {
            add
            {
                bool needNotification = (_widgetControlItemChanged == null);

                _widgetControlItemChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlItemChanged");
                }

            }
            remove
            {
                _widgetControlItemChanged -= value;

                if (_widgetControlItemChanged == null)
                {
                    OnEventHandlerDeattached("WidgetControlItemChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ItemChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasWidgetControlItemChangedListeners
        {
            get { return _widgetControlItemChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:WidgetControlItemChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlItemChanged"/> instance containing the event data.</param>
        public virtual void OnWidgetControlItemChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_widgetControlItemChanged != null)
            {
                _widgetControlItemChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WidgetControlItemChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public virtual void PerformWidgetControlItemChanged(EventArgs args)
        {

            // Raise the ItemChanged event.
            OnWidgetControlItemChanged(args);
        }


        /// <summary>
        /// WidgetControlFocusEnter event handler.
        /// </summary>
        private event EventHandler<GridCellEventArgs> _widgetControlFocusEnter;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove WidgetControlFocusEnter action.
        /// </summary>
        public event EventHandler<GridCellEventArgs> WidgetControlFocusEnter
        {
            add
            {
                bool needNotification = (_widgetControlFocusEnter == null);

                _widgetControlFocusEnter += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlFocusEnter");
                }

            }
            remove
            {
                _widgetControlFocusEnter -= value;

                if (_widgetControlFocusEnter == null)
                {
                    OnEventHandlerDeattached("WidgetControlFocusEnter");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has WidgetControlFocusEnter listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasWidgetControlFocusEnterListeners
        {
            get { return _widgetControlFocusEnter != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:WidgetControlFocusEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlFocusEnter"/> instance containing the event data.</param>
        protected virtual void OnWidgetControlFocusEnter(GridCellEventArgs args)
        {

            // Check if there are listeners.
            if (_widgetControlFocusEnter != null)
            {
                _widgetControlFocusEnter(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WidgetControlFocusEnter" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWidgetControlFocusEnter(GridCellEventArgs args)
        {
            this.FocusedWidgetcontrol = args.DataIndex;
            this.ValueWidgetcontrol = args.Value;
            // Raise the WidgetControlFocusEnter event.
            OnWidgetControlFocusEnter(args);
        }


        /// <summary>
        /// WidgetControlDoubleClick event handler.
        /// </summary>
        private event EventHandler<GridCellEventArgs> _WidgetControlDoubleClick;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove WidgetControlDoubleClick action.
        /// </summary>
        public event EventHandler<GridCellEventArgs> WidgetControlDoubleClick
        {
            add
            {
                bool needNotification = (_WidgetControlDoubleClick == null);

                _WidgetControlDoubleClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlDoubleClick");
                }

            }
            remove
            {
                _WidgetControlDoubleClick -= value;

                if (_WidgetControlDoubleClick == null)
                {
                    OnEventHandlerDeattached("WidgetControlDoubleClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has WidgetControlDoubleClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasWidgetControlDoubleClickListeners
        {
            get { return _WidgetControlDoubleClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:WidgetControlDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlDoubleClick"/> instance containing the event data.</param>
        protected virtual void OnWidgetControlDoubleClick(GridCellEventArgs args)
        {

            // Check if there are listeners.
            if (_WidgetControlDoubleClick != null)
            {
                _WidgetControlDoubleClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WidgetControlDoubleClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWidgetControlDoubleClick(GridCellEventArgs args)
        {
            
            // Raise the WidgetControlDoubleClick event.
            OnWidgetControlDoubleClick(args);
        }


        /// <summary>
        /// WidgetControlClick event handler.
        /// </summary>
        private event EventHandler<GridCellEventArgs> _widgetControlClick;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove WidgetControlClick action.
        /// </summary>
        public event EventHandler<GridCellEventArgs> WidgetControlClick
        {
            add
            {
                bool needNotification = (_widgetControlClick == null);

                _widgetControlClick += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("WidgetControlClick");
                }

            }
            remove
            {
                _widgetControlClick -= value;

                if (_widgetControlClick == null)
                {
                    OnEventHandlerDeattached("WidgetControlClick");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has WidgetControlClick listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public virtual bool HasWidgetControlClickListeners
        {
            get { return _widgetControlClick != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:WidgetControlClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="WidgetControlClick"/> instance containing the event data.</param>
        protected virtual void OnWidgetControlClick(GridCellEventArgs args)
        {

            // Check if there are listeners.
            if (_widgetControlClick != null)
            {
                _widgetControlClick(this, args);
            }
        }

        /// <summary>
        /// Performs the <see cref="E:WidgetControlClick" /> event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformWidgetControlClick(GridCellEventArgs args)
        {

            // Raise the WidgetControlClick event.
            OnWidgetControlClick(args);
        }





        private int RowIndex
        {
            get;
            set;
        }

        internal void SetRowIndex(int index)
        {
            RowIndex = index;
        }
        internal int GetRowIndex()
        {
            return RowIndex ;
        }

        private int WidgetControlIndex
        {
            get;
            set;
        }

        internal void SetWidgetControlIndex(int index)
        {
            WidgetControlIndex = index;
        }

        internal int GetWidgetControlIndex()
        {
            return WidgetControlIndex ;
        }


        [RendererMethodDescription]
        public virtual void RaiseUpdateClient()
        {
            this.InvokeMethod("RaiseUpdateClient", null);
        }

        public string ClientLeave { get; set; }

    }



    /// <summary>
    /// Provides interface for controls
    /// </summary>
    public interface IControlElement
    {

        /// <summary>
        /// Gets or sets the font.
        /// </summary>
        /// <value>
        /// The font.
        /// </value>
        Font Font { get; set; }

        /// <summary>
        /// Gets or sets the color of the back.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        Color BackColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the back.
        /// </summary>
        /// <value>
        /// The color of the back.
        /// </value>
        Color BorderColor { get; set; }

        /// <summary>
        /// Gets or sets the color of the fore.
        /// </summary>
        /// <value>
        /// The color of the fore.
        /// </value>
        [DefaultValue(typeof(Color), "")]
        Color ForeColor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IControlElement"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IControlElement"/> is visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if visible; otherwise, <c>false</c>.
        /// </value>
        bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the index of the tab.
        /// </summary>
        /// <value>
        /// The index of the tab.
        /// </value>
        int TabIndex { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        Unit Width { get; set; }

        /// <summary>
        /// Gets or sets the border style.
        /// </summary>
        /// <value>
        /// The border style.
        /// </value>
        BorderStyle BorderStyle { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        Unit Height { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        string ID { get; set; }

        /// <summary>
        /// Gets or sets the skin to apply to the control.
        /// </summary>
        /// <value>
        /// The name of the skin to apply to the control. The default is System.String.Empty.
        /// </value>
        string SkinID { get; set; }
    }
}
