using System.Drawing;

namespace System.Web.VisualTree.Elements
{
    public class DataPointCustomProperties : ChartNamedElement
    {
        /// <summary>
        ///Gets or sets the background hatching style.
        /// </summary>
        public ChartHatchStyle BackHatchStyle
        {
            get;
            set;
        }



        /// <summary>
        /// Gets or sets the color of the border.
        /// </summary>
        /// <value>
        /// The color of the border.
        /// </value>
        public Color BorderColor
        {
            get;
            set;
        }



        /// <summary>
        ///Gets or sets the border style of the data point.
        /// </summary>
        public ChartDashStyle BorderDashStyle
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the color of the data point.
        /// </summary>
        public Color Color
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the marker color.
        /// </summary>
        public Color MarkerColor
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the size of the marker.
        /// </summary>
        public int MarkerSize
        {
            get;
            set;
        }
        /// <summary>
        ///Gets or sets the marker style.
        /// </summary>
        public MarkerStyle MarkerStyle
        {
            get;
            set;
        }

    }
}
