﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Web.VisualTree.Elements
{
    public class KeyCollection : VisualElementCollection<KeyItem>
    {        
        internal KeyCollection(VisualElement parentElement, string propertyName)
            : base(parentElement, propertyName)
        {

        }
    }
}
