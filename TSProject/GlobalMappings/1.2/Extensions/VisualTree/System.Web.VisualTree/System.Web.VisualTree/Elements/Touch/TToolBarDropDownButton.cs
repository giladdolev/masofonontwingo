﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements.Touch
{
    public class TToolBarDropDownButton : ToolBarDropDownButton, ITVisualElement
    {
        /// <summary>
        /// The button appearance
        /// </summary>
        private TButtonAppearance _ButtonAppearance = TButtonAppearance.Normal;


        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        [RendererPropertyDescription]
        public TButtonAppearance ButtonAppearance
        {
            get { return _ButtonAppearance; }
            set { _ButtonAppearance = value; }
        }
    }
}
