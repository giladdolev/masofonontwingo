namespace System.Web.VisualTree.Elements
{
	public class GridColumnHeaderCell : GridHeaderCell
	{
        /// <summary>
        /// Gets or sets the automatic size mode.
        /// </summary>
        /// <value>
        /// The automatic size mode.
        /// </value>
		public GridAutoSizeColumnMode AutoSizeMode
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the header cell.
        /// </summary>
        /// <value>
        /// The header cell.
        /// </value>
		public GridHeaderCell HeaderCell
		{
			get;
			set;
		}



        /// <summary>
        /// Gets or sets the sort glyph direction.
        /// </summary>
        /// <value>
        /// The sort glyph direction.
        /// </value>
		public SortOrder SortGlyphDirection
		{
			get;
			set;
		}
	}
}
