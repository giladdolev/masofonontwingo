﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.Elements
{
    /// <summary>
    /// Provides support for specifying required binding
    /// </summary>
    public class Binding
    {
        /// <summary>
        /// The source
        /// </summary>
        private string _source;

        /// <summary>
        /// The is parent binding flag
        /// </summary>
        private bool _isParentBinding;

        /// <summary>
        /// Initializes a new instance of the <see cref="Binding" /> class.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="isParentBinding">if set to <c>true</c> [is parent binding].</param>
        public Binding(string source, bool isParentBinding = false)
        {

            _source = source;

            _isParentBinding = isParentBinding;
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Source
        {
            get
            {
                return _source;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is parent binding.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is parent binding; otherwise, <c>false</c>.
        /// </value>
        public bool IsParentBinding
        {
            get
            {
                return _isParentBinding;
            }
        }

        /// <summary>
        /// Provides support for extending elements bindings
        /// </summary>
        private class DataBindingsExtender : Dictionary<string, Binding>, IVisualElementExtender
        {

            /// <summary>
            /// Gets the extender.
            /// </summary>
            /// <value>
            /// The extender.
            /// </value>
            IVisualElementExtender IVisualElementExtender.Extender
            {
                get;
                set;
            }

            /// <summary>
            /// Adds the extender.
            /// </summary>
            /// <param name="extender">The extender.</param>
            void IVisualElementExtender.AddExtender(IVisualElementExtender extender)
            {
                VisualElementExtender.AddExtender(this, extender);
            }

            /// <summary>
            /// Removes the extender.
            /// </summary>
            /// <param name="extender">The extender.</param>
            void IVisualElementExtender.RemoveExtender(IVisualElementExtender extender)
            {
                VisualElementExtender.RemoveExtender(this, extender);
            }
        }

        /// <summary>
        /// Gets the data bindings.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static IEnumerable<Binding> GetDataBindings(VisualElement element)
        {
            // Get the extender
            DataBindingsExtender extender = VisualElementExtender.GetExtender<DataBindingsExtender>(element);

            // If there is a valid extender
            if (extender != null)
            {
                // Loop all bindings
                foreach (Binding dataBinding in extender.Values)
                {
                    // Return the binding
                    yield return dataBinding;
                }
            }
        }

        /// <summary>
        /// Sets the data binding.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="bindToParentData">if set to <c>true</c> [bind to parent data].</param>
        public static void SetDataBinding(VisualElement element, string source, string target, bool bindToParentData = false)
        {
            // If there is a valid source and target
            if (!string.IsNullOrEmpty(target) && !string.IsNullOrEmpty(source))
            {
                // Get the extender
                DataBindingsExtender extender = VisualElementExtender.GetOrCreateExtender<DataBindingsExtender>(element, () => new DataBindingsExtender());

                // If there is a valid extender
                if (extender != null)
                {
                    Binding dataBinding;

                    // Set the new data binding
                    extender[target] = dataBinding = new Binding(source, bindToParentData);

                    // Get the data source element
                    BindingManager dataSourceElement = null;

                    // If should bind to parent data
                    if (bindToParentData)
                    {
                        // Get parent data source
                        dataSourceElement = BindingManager.GetBindingManager(element.ParentElement);
                    }
                    else
                    {
                        // Get current data source
                        dataSourceElement = BindingManager.GetBindingManager(element);
                    }

                    // If there is a valid data source element
                    if (dataSourceElement != null)
                    {
                        // Bind data source to data binding
                        dataSourceElement.Bind(dataBinding);
                    }
                }
            }
        }



        /// <summary>
        /// Gets the data binding.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        internal static Binding GetDataBinding(VisualElement element, string target)
        {
            // The binding
            Binding binding = null;

            // If there is a valid target
            if (!string.IsNullOrEmpty(target))
            {
                // Get the extender
                DataBindingsExtender extender = VisualElementExtender.GetExtender<DataBindingsExtender>(element);

                // If there is a valid extender
                if (extender != null)
                {
                    // Get the new data binding
                    extender.TryGetValue(target, out binding);
                }
            }

            return binding;
        }
    }

}
