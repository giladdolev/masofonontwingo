﻿using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    public class MonthCalendarElement : ControlElement
    {

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        public DateTime Date
        {
            get;
            set;
        }


        /// <summary>
        /// DateChanged event handler.
        /// </summary>
        private event EventHandler _DateChanged;

        [RendererEventDescription]
        /// <summary>
        /// Add or remove DateChanged action.
        /// </summary>
        public event EventHandler DateChanged
        {
            add
            {
                bool needNotification = (_DateChanged == null);

                _DateChanged += value;

                if (needNotification)
                {
                    OnEventHandlerAttached("DateChanged");
                }

            }
            remove
            {
                _DateChanged -= value;

                if (_DateChanged == null)
                {
                    OnEventHandlerDeattached("DateChanged");
                }

            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has DateChanged listeners.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has after select listeners; otherwise, <c>false</c>.
        /// </value>
        [DesignerIgnore]
        public bool HasDateChangedListeners
        {
            get { return _DateChanged != null; }
        }

        /// <summary>
        /// Raises the <see cref="E:DateChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="DateChanged"/> instance containing the event data.</param>
        protected virtual void OnDateChanged(EventArgs args)
        {

            // Check if there are listeners.
            if (_DateChanged != null)
            {
                _DateChanged(this, args);
            }
        }

        /// <summary>
        /// Performs the date changed event.
        /// </summary>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void PerformDateChanged(EventArgs args)
        {

            // Try to get string value changed arguments
            ValueChangedArgs<DateTime> dateValueChangedArgs = args as ValueChangedArgs<DateTime>;

            // If there is a valid string value changed arguments
            if (dateValueChangedArgs != null)
            {
                // Set text value
                Date = dateValueChangedArgs.Value;
            }
            
            // Raise the SelectionChangeCommitted event.
            OnDateChanged(args);
        }

    }
}
