using System.Drawing;
using System.Web.VisualTree.Common.Attributes;

namespace System.Web.VisualTree.Elements
{
	public abstract class ControlContainerElement : ControlElement
	{
        [DesignerIgnore]
        public override bool IsContainer
		{
			get { return true; }
		}



        public AutoScaleMode AutoScaleMode
        {
            get;
            set;
        }



        public SizeF AutoScaleDimensions
        {
            get;
            set;
        }



        /// <summary>
        /// Adds the specified visual element.
        /// </summary>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="insert">if set to <c>true</c> insert child item in the first position of Children collection.</param>
        public override void Add(VisualElement visualElement, bool insert)
		{
			
			base.Add(visualElement, insert);
			
			// Try casting the visual element in to ControlContainer settings
			ControlContainerSettings objContainerSettings = visualElement as ControlContainerSettings;
			
			// If casting is valid
			if (objContainerSettings != null)
			{

			}
		}	
	}
}
