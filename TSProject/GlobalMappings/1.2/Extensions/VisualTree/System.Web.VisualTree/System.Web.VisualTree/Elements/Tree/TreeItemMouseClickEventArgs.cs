namespace System.Web.VisualTree.Elements
{
	public class TreeItemMouseClickEventArgs : MouseEventArgs
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeItemMouseClickEventArgs"/> class.
        /// </summary>
        /// <param name="objTreeNode">The object tree node.</param>
        /// <param name="enmButton">The enm button.</param>
        /// <param name="intClicks">The int clicks.</param>
        /// <param name="intX">The int x.</param>
        /// <param name="intY">The int y.</param>
        /// <exception cref="ArgumentNullException">objTreeNode</exception>
        public TreeItemMouseClickEventArgs(TreeItem objTreeNode, MouseButtons enmButton, int intClicks, int intX, int intY)
		{
            if (objTreeNode == null)
            {
                throw new ArgumentNullException("objTreeNode");
            }
		}



		/// <summary>
		/// Gets or sets the node.
		/// </summary>
		/// <value>
		/// The node.
		/// </value>
		public TreeItem Item
		{
			get;
			set;
		}

        /// <summary>
        /// Gets the node.
        /// </summary>
        /// <value>
        /// The node.
        /// </value>
        public TreeItem Node { get; private set; }
	}
}
