﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TextTemplating.VSHost;
using System.Reflection;
using Microsoft.Win32;
using System.IO;
using System.Diagnostics;

namespace System.Web.VisualTree.CodeGenerator
{
    public abstract class CodeGeneratorBase : BaseCodeGeneratorWithSite
    {
        /// <summary>
        /// List of Visual Studio versions and their registry paths in HKEY_LOCAL_MACHINE.
        /// http://stackoverflow.com/questions/10922913/visual-studio-express-2012-editions-exe-names-and-registry-path
        /// </summary>
        private class VSType
        {
            public readonly string Name;
            public readonly double VersionNumber;
            public readonly string RegistryPath;
            public readonly string SpecificLanguage;
            public readonly bool IsPro;
            public VSType(string name, double versionNumber, string registryPath, bool pro = false, string language = null)
            {
                Name = name;
                VersionNumber = versionNumber;
                RegistryPath = registryPath;
                SpecificLanguage = language;
                IsPro = pro;
            }
            public string RegistryPath64
            {
                get
                {
                    return RegistryPath.Insert("SOFTWARE\\".Length, "Wow6432Node\\");
                }
            }

            public static readonly VSType VS2002 = new VSType("VS 2002", 7.0, "SOFTWARE\\Microsoft\\VisualStudio\\7.0", true);
            public static readonly VSType VS2003 = new VSType("VS 2003", 7.1, "SOFTWARE\\Microsoft\\VisualStudio\\7.1", true);

            // 2005  ***********************************************************************
            public static readonly VSType VS2005 = new VSType("VS 2005", 8.0, "SOFTWARE\\Microsoft\\VisualStudio\\8.0", true);
            public static readonly VSType VSExpress2005CSharp = new VSType("Visual C# 2005 Express", 8.0, "SOFTWARE\\Microsoft\\VCSExpress\\8.0", false, "C#");
            public static readonly VSType VSExpress2005VB = new VSType("Visual Basic 2005 Express", 8.0, "SOFTWARE\\Microsoft\\VBExpress\\8.0", false, "VB");
            public static readonly VSType VSExpress2005Web = new VSType("Visual Web Developer 2005 Express", 8.0, "SOFTWARE\\Microsoft\\VWDExpress\\8.0", false, "Web");

            // 2008  ***********************************************************************
            public static readonly VSType VS2008 = new VSType("VS 2008", 9.0, "SOFTWARE\\Microsoft\\VisualStudio\\9.0", true);
            public static readonly VSType VSExpress2008CSharp = new VSType("Visual C# 2008 Express", 9.0, "SOFTWARE\\Microsoft\\VCSExpress\\9.0", false, "C#");
            public static readonly VSType VSExpress2008VB = new VSType("Visual Basic 2008 Express", 9.0, "SOFTWARE\\Microsoft\\VBExpress\\9.0", false, "VB");
            public static readonly VSType VSExpress2008Web = new VSType("Visual Web Developer 2008 Express", 9.0, "SOFTWARE\\Microsoft\\VWDExpress\\9.0", false, "Web");

            // 2010  ***********************************************************************
            public static readonly VSType VS2010 = new VSType("VS 2010", 10.0, "SOFTWARE\\Microsoft\\VisualStudio\\10.0", true);
            public static readonly VSType VSExpress2010CSharp = new VSType("Visual C# 2010 Express", 10.0, "SOFTWARE\\Microsoft\\VCSExpress\\10.0", false, "C#");
            public static readonly VSType VSExpress2010VB = new VSType("Visual Basic 2010 Express", 10.0, "SOFTWARE\\Microsoft\\VBExpress\\10.0", false, "VB");
            public static readonly VSType VSExpress2010Web = new VSType("Visual Web Developer 2010 Express", 10.0, "SOFTWARE\\Microsoft\\VWDExpress\\10.0", false, "Web");

            // 2012 ***********************************************************************
            public static readonly VSType VS2012 = new VSType("VS 2012", 11.0, "SOFTWARE\\Microsoft\\VisualStudio\\11.0", true);
            // This is unverified:
            public static readonly VSType VSExpress2012Desktop = new VSType("VS Express 2012 for Desktop", 11.0, "SOFTWARE\\Microsoft\\WDExpress\\11.0");
            public static VSType VSExpress2012WIn8 = new VSType("VS Express 2012 for Windows 8", 11.0, "SOFTWARE\\Microsoft\\VSWinExpress\\11.0");
            public static VSType VSExpress2012Web = new VSType("VS Express 2012 for Web", 11.0, "SOFTWARE\\Microsoft\\VWDExpress\\11.0");
            public static VSType VSExpressTFS2012 = new VSType("VS TFS Express 2012", 11.0, "SOFTWARE\\Microsoft\\TeamFoundationServer\\11.0", false, "TFS");

            // 2013 ***********************************************************************
            public static readonly VSType VS2013 = new VSType("VS 2013", 12.0, "SOFTWARE\\Microsoft\\VisualStudio\\12.0", true);
            public static readonly VSType VSExpress2013Desktop = new VSType("VS Express 2013 for Desktop", 12.0, "SOFTWARE\\Microsoft\\WDExpress\\12.0");

            // 2015 ***********************************************************************
            public static readonly VSType VS2015 = new VSType("VS 2015", 14.0, "SOFTWARE\\Microsoft\\VisualStudio\\14.0", true);

            public static readonly List<VSType> VSEditions = new List<VSType> {
			VS2002, VS2003,
			VS2005, VSExpress2005CSharp, VSExpress2005VB, VSExpress2005Web,
			VS2008, VSExpress2008CSharp, VSExpress2008VB, VSExpress2008Web,
			VS2010, VSExpress2010CSharp, VSExpress2010VB, VSExpress2010Web,
			VS2012, VSExpress2012Desktop,
			VS2013, VSExpress2013Desktop,
            VS2015
		    };
        }

        /// <summary>
        /// Registers the single file generators.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="unregister">if set to <c>true</c> un-register.</param>
        /// <returns>Flag indicating if registered the single file generator.</returns>
        protected static bool RegisterSingleFileGenerators(Type type, bool unregister = false)
        {
            bool registered = false;

            try
            {
                foreach (VSType vs in VSType.VSEditions.Where(t => t.VersionNumber >= 8.0))
                {
                    string path = null;
                    RegistryKey key = null;

                    // In a 32-bit process, Windows adds "Wow6432Node" automatically
                    if (IntPtr.Size == 8)
                    {
                        key = Registry.LocalMachine.OpenSubKey(path = Path.Combine(vs.RegistryPath64, "Generators"), false);
                    }

                    key = key ?? Registry.LocalMachine.OpenSubKey(path = Path.Combine(vs.RegistryPath, "Generators"), false);

                    if (key != null)
                    {
                        using (key)
                        {

                            bool isRegisterSingleFileGenerators = RegisterSingleFileGenerators(type, path, unregister);
                            // The generator was registered successfully
                            if (isRegisterSingleFileGenerators)
                            {
                                registered = true;
                            }
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                System.Windows.Forms.MessageBox.Show("moshe:" + exception.ToString());
            }

            // Returns true if the generator was registered in at least 1 VSEditor
            return registered;
        }

        /// <summary>
        /// Registers the single file generators.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="generatorsKey">The generators key.</param>
        /// <param name="unregister">if set to <c>true</c> un-register.</param>
        /// <returns>Flag indicating if registered the single file generator.</returns>
        private static bool RegisterSingleFileGenerators(Type type, string generatorsKey, bool unregister)
        {
            bool ok = false;

            if (type != null)
            {
                var a = type.GetCustomAttributes(typeof(CodeGeneratorRegistrationAttribute), true);
                foreach (CodeGeneratorRegistrationAttribute attr in a)
                {
                    string subKey = string.Format(@"{0}\{1}", attr.ContextGuid, attr.GeneratorType.Name);
                    string path = Path.Combine(generatorsKey, subKey);
                    if (unregister)
                    {
                        try
                        {
                            Registry.LocalMachine.DeleteSubKey(path, true);
                            ok = true;
                        }
                        catch { }
                    }
                    else
                    {
                        using (RegistryKey key = Registry.LocalMachine.CreateSubKey(path))
                        {
                            if (key == null)
                            {
                                throw new Exception("Failed to create registry key");
                            }
                            else
                            {
                                key.SetValue("", attr.GeneratorName);
                                key.SetValue("CLSID", "{" + attr.GeneratorGuid.ToString() + "}");
                                key.SetValue("GeneratesDesignTimeSource", 1);
                                ok = true;
                            }
                        }
                    }
                }
            }

            return ok;
        }

    }
}
