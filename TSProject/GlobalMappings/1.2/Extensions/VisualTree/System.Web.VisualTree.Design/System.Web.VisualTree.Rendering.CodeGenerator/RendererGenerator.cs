﻿using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell.Design;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace System.Web.VisualTree.CodeGenerator
{

    // Note: the class name is used as the name of the Custom Tool from the end-user's perspective.
    [ComVisible(true)]
    [Guid("1C5C759D-3F8B-4508-9074-20AE9BF517D4")]
    [CodeGeneratorRegistration(typeof(RendererGenerator), "VisualTree code generator.", VSContextGuids.vsContextGuidVCSProject, GeneratesDesignTimeSource = true)]
    [CodeGeneratorRegistration(typeof(RendererGenerator), "VisualTree code generator.", VSContextGuids.vsContextGuidVBProject, GeneratesDesignTimeSource = true)]
    [CodeGeneratorRegistration(typeof(RendererGenerator), "VisualTree code generator.", VSContextGuids.vsContextGuidVJSProject, GeneratesDesignTimeSource = true)]
    public class RendererGenerator : CodeGeneratorBase
    {
        #region Code Generation

        protected override byte[] GenerateCode(string inputFileName, string inputFileContent)
        {
            try
            {
                DynamicTypeService dynamicTypeService = this.GlobalServiceProvider.GetService(typeof(DynamicTypeService)) as DynamicTypeService;

                if (dynamicTypeService != null)
                {
                    ITypeResolutionService typeResolutionService = dynamicTypeService.GetTypeResolutionService((IVsHierarchy)this.GetService(typeof(IVsHierarchy)));

                    if (typeResolutionService != null)
                    {
                        Type rendererFactoryType = typeResolutionService.GetType("System.Web.VisualTree.Common.CodeGenerator.GeneratorManager", false);

                        if (rendererFactoryType != null)
                        {
                            // Get project item from solution
                            ProjectItem projectItem = this.Dte.Solution.FindProjectItem(inputFileName);

                            // If there is a valid project item
                            if (projectItem != null)
                            {
                                FileCodeModel codeModel = projectItem.FileCodeModel;

                                if (codeModel != null)
                                {
                                    string rendererNamespace = null;
                                    string rendererName = null;
                                    bool classFound = false;

                                    // Create a code scope for the provided file
                                    CodeScope codeScope = new CodeScope(new ResourceProvider(projectItem, inputFileName), null);

                                    object[] metadataInstances = ExtractMetadata(codeScope, typeResolutionService, codeModel.CodeElements, ref rendererNamespace, ref rendererName, ref classFound);

                                    object instance = Activator.CreateInstance(rendererFactoryType, new object[] { typeResolutionService, codeScope as List<string>, rendererNamespace, rendererName, metadataInstances });

                                    if (instance != null)
                                    {
                                        return Encoding.UTF8.GetBytes(instance.ToString());
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                StackTrace st = new StackTrace(ex, true);

                // Get the top stack frame
                Diagnostics.StackFrame frame = st.GetFrame(0);

                // Get the exception location from the stack frame
                int line = frame.GetFileLineNumber();
                int column = frame.GetFileColumnNumber();

                // Generate error for the calling visual studio instance
                this.GeneratorErrorCallback(false, 10, ex.Message, line, column);

                // Create default implementation for the user
                string defaultImplementation = GetDefaultImplementation(inputFileName, inputFileContent, ex);

                return Encoding.UTF8.GetBytes(defaultImplementation);
            }

            // If all else fails
            return Encoding.UTF8.GetBytes(string.Empty);
        }





        /// <summary>
        /// Gets the default base renderer implementation.
        /// </summary>
        /// <param name="inputFileName">Name of the input file.</param>
        /// <param name="inputFileContent">Content of the input file.</param>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        private string GetDefaultImplementation(string inputFileName, string inputFileContent, Exception ex = null)
        {
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(inputFileName);
            string defaultNamespace = ManuallyParseNamespace(inputFileContent);
            string defExtension = GetDefaultExtension();

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("namespace {0} {{\n", defaultNamespace);
            builder.AppendFormat("\tclass {0}{1} {{\n", fileNameWithoutExtension, defExtension.Remove(defExtension.Length - 3, 3));
            builder.AppendLine("\t// Exception was thrown during generation. Please make sure the syntax is correct");

            if (ex != null)
            {
                builder.AppendLine("\t/*" + ex.Message);
                builder.AppendLine("\n" + ex.StackTrace + "*/");
            }

            builder.AppendLine("\t}");
            builder.AppendLine("}");

            return builder.ToString();
        }




        /// <summary>
        /// Parses the namespace from the content.
        /// </summary>
        /// <param name="inputFileContent">Content of the input file.</param>
        /// <returns></returns>
        private string ManuallyParseNamespace(string inputFileContent)
        {
            string namespaceKeyword = "namespace ";

            int indexOfNamespaceKeyword = inputFileContent.IndexOf(namespaceKeyword);
            if (indexOfNamespaceKeyword < 0)
                return String.Empty;

            int indexOfNamespaceValue = indexOfNamespaceKeyword + namespaceKeyword.Length;

            string contentStartingAtNamespaceValue = inputFileContent.Substring(indexOfNamespaceValue);

            int indexOfReturn = contentStartingAtNamespaceValue.IndexOf("\r");
            int indexOfEmptyString = contentStartingAtNamespaceValue.IndexOf(" ");

            int lengthOfNamespaceValue = indexOfReturn;
            if (indexOfEmptyString < indexOfReturn)
                lengthOfNamespaceValue = indexOfEmptyString;

            return contentStartingAtNamespaceValue.Substring(0, lengthOfNamespaceValue);
        }




        /// <summary>
        /// Provides support for reading resources
        /// </summary>
        private class ResourceProvider
        {
            /// <summary>
            /// The resources
            /// </summary>
            private Dictionary<string, string> resources = null;

            /// <summary>
            /// The working file
            /// </summary>
            private string _workingFile = null;

            /// <summary>
            /// The project item
            /// </summary>
            private ProjectItem _projectItem = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="ResourceProvider" /> class.
            /// </summary>
            /// <param name="projectItem">The project item.</param>
            /// <param name="workingFile">The working file.</param>
            public ResourceProvider(ProjectItem projectItem, string workingFile)
            {
                _workingFile = workingFile;
                _projectItem = projectItem;
            }

            /// <summary>
            /// Ensures the resources.
            /// </summary>
            private void EnsureResources()
            {
                // If we need to initialize dictionary
                if (resources == null)
                {
                    // Create resources dictionary
                    resources = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

                    try
                    {
                        // The working file name
                        string workingFileName = Path.GetFileNameWithoutExtension(_workingFile);

                        // Get working file name
                        string workingFilePattern = string.Concat(workingFileName, ".*");

                        // Get working file directory
                        string workingDirectory = Path.GetDirectoryName(_workingFile);

                        // Get working file extension position
                        int workingFileExtensionPosition = workingFileName.Length + 1;

                        // If there is a valid directory
                        if (Directory.Exists(workingDirectory))
                        {
                            // Loop all related files
                            foreach (string relatedFile in Directory.EnumerateFiles(workingDirectory, workingFilePattern))
                            {
                                // If is not the working file
                                if (!string.Equals(relatedFile, _workingFile, StringComparison.OrdinalIgnoreCase))
                                {
                                    // Get the related file name
                                    string relatedFileName = Path.GetFileName(relatedFile);

                                    // If there is a valid file name
                                    if (!string.IsNullOrEmpty(relatedFileName))
                                    {
                                        // If there is a valid length of file name
                                        if (relatedFileName.Length > workingFileExtensionPosition)
                                        {
                                            // Get resource name
                                            string relatedResourceName = relatedFileName.Substring(workingFileExtensionPosition);

                                            // If there is a valid resource name
                                            if (!string.IsNullOrEmpty(relatedResourceName))
                                            {
                                                // Set resource
                                                resources[relatedResourceName] = File.ReadAllText(relatedFile);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        // TODO: report error
                    }
                }
            }


            /// <summary>
            /// Gets the resource as string.
            /// </summary>
            /// <param name="resourceName">The resource name.</param>
            /// <returns></returns>
            internal string GetResourceAsString(string resourceName)
            {
                string value = null;

                // If there is a valid resource name
                if (!string.IsNullOrEmpty(resourceName))
                {
                    // Ensure resources
                    this.EnsureResources();

                    // Try to get resource by name
                    if (!resources.TryGetValue(resourceName, out value))
                    {
                        // Set default value
                        resources[resourceName] = value = string.Empty;

                        // Check if user wants to create file
                        if (System.Windows.Forms.MessageBox.Show(string.Format("Do you want to create '{0}' resource?", resourceName), "Renderer Generator", Windows.Forms.MessageBoxButtons.YesNo) == Windows.Forms.DialogResult.Yes)
                        {
                            // The working file name
                            string workingFileName = Path.GetFileNameWithoutExtension(_workingFile);

                            // Get working file directory
                            string workingDirectory = Path.GetDirectoryName(_workingFile);

                            // Get the wanted file
                            string relatedFile = Path.Combine(workingDirectory, string.Concat(workingFileName, ".", resourceName));

                            // Create file
                            File.WriteAllText(relatedFile, string.Empty);

                            // If there is a valid project item
                            if (_projectItem != null)
                            {
                                // Add the related file
                                _projectItem.ProjectItems.AddFromFile(relatedFile);
                            }
                        }
                    }
                }

                return value;
            }
        }

        private class CodeScope : List<string>
        {
            /// <summary>
            /// The parent
            /// </summary>
            private readonly CodeScope _parent;

            /// <summary>
            /// The code DOM provider
            /// </summary>
            private readonly CodeDomProvider _codeDomProvider;

            /// <summary>
            /// The resource provider
            /// </summary>
            private readonly ResourceProvider _resourceProvider;

            /// <summary>
            /// Initializes a new instance of the <see cref="CodeScope" /> class.
            /// </summary>
            /// <param name="codeDomProvider">The code DOM provider.</param>
            /// <param name="parent">The parent.</param>
            public CodeScope(ResourceProvider resourceProvider, CodeDomProvider codeDomProvider, CodeScope parent = null)
            {
                _codeDomProvider = codeDomProvider;
                _resourceProvider = resourceProvider;
                _parent = parent;
            }

            /// <summary>
            /// Gets the parent.
            /// </summary>
            /// <value>
            /// The parent.
            /// </value>
            public CodeScope Parent
            {
                get { return _parent; }
            }


            /// <summary>
            /// Creates the scope.
            /// </summary>
            /// <returns></returns>
            internal CodeScope CreateScope()
            {
                return new CodeScope(_resourceProvider, _codeDomProvider, this);
            }

            internal void AddNamespace(string namespaceValue)
            {
                if (!string.IsNullOrWhiteSpace(namespaceValue))
                {
                    StringBuilder namespaceBuilder = new StringBuilder();

                    foreach (string namespacePart in namespaceValue.Split('.'))
                    {
                        if (namespaceBuilder.Length > 0)
                        {
                            namespaceBuilder.Append(".");
                        }

                        namespaceBuilder.Append(namespacePart);

                        AddImport(namespaceBuilder.ToString());
                    }
                }
            }

            internal void AddImport(string importValue)
            {
                if (!string.IsNullOrWhiteSpace(importValue))
                {
                    this.Add(importValue);
                }
            }

            private const string TYPEOF_PREFIX = "typeof(";

            internal object Parse(ITypeResolutionService typeResolutionService, string argumentCode)
            {
                if (!string.IsNullOrWhiteSpace(argumentCode))
                {
                    switch (argumentCode)
                    {
                        case "true":
                            return true;
                        case "false":
                            return false;
                    }

                    if (argumentCode.StartsWith("\"") && argumentCode.EndsWith("\""))
                    {
                        // Return formatted string
                        return FormatStringArgument(argumentCode.Substring(1, argumentCode.Length - 2).Replace("\\\"", "\""));
                    }

                    if (argumentCode.StartsWith(TYPEOF_PREFIX))
                    {
                        return ResolveType(typeResolutionService, argumentCode.Substring(TYPEOF_PREFIX.Length, argumentCode.Length - TYPEOF_PREFIX.Length - 1));
                    }

                    if (!char.IsDigit(argumentCode[0]) && argumentCode.Contains("."))
                    {
                        int typeSeperatorIndex = argumentCode.LastIndexOf('.');

                        Type containerType = this.ResolveType(typeResolutionService, argumentCode.Substring(0, typeSeperatorIndex));

                        if (containerType != null)
                        {
                            FieldInfo memberInfo = containerType.GetMember(argumentCode.Substring(typeSeperatorIndex + 1)).FirstOrDefault() as FieldInfo;

                            if (memberInfo != null)
                            {
                                return memberInfo.GetRawConstantValue();
                            }
                        }
                    }

                    int integerValue = 0;

                    if (int.TryParse(argumentCode, out integerValue))
                    {
                        return integerValue;
                    }
                }

                return null;
            }

            /// <summary>
            /// The resource expression
            /// </summary>
            private static readonly Regex resourceExpression = new Regex(@"<#(\s)*resource(\s)*:(\s)*(?<name>[^\s]+)(\s)*#>", RegexOptions.Compiled);


            /// <summary>
            /// Formats the string argument.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns></returns>
            private string FormatStringArgument(string value)
            {
                // Replace resource commands with resource content
                value = resourceExpression.Replace(value, FormatStringArgumentResource);

                return value;
            }

            /// <summary>
            /// Formats the string argument resource.
            /// </summary>
            /// <param name="match">The match.</param>
            /// <returns></returns>
            private string FormatStringArgumentResource(Match match)
            {
                // If there is a valid resource provider
                if (_resourceProvider != null)
                {
                    // Get the resource name
                    string resourceName = match.Groups["name"].Value;

                    // If there is a valid resource name
                    if (!string.IsNullOrWhiteSpace(resourceName))
                    {
                        // Return the resource as string
                        return _resourceProvider.GetResourceAsString(resourceName);
                    }
                }

                return "?";
            }



            private Type ResolveType(ITypeResolutionService typeResolutionService, string type)
            {
                if (typeResolutionService != null && !string.IsNullOrWhiteSpace(type))
                {
                    switch (type)
                    {
                        case "int":
                            return typeResolutionService.GetType("System.Int32", false);
                        case "string":
                            return typeResolutionService.GetType("System.String", false);
                        case "bool":
                            return typeResolutionService.GetType("System.Boolean", false);
                    }
                    foreach (string namespaceValue in this)
                    {
                        Type resolvedType = typeResolutionService.GetType(string.Concat(namespaceValue, ".", type), false);

                        if (resolvedType != null)
                        {
                            return resolvedType;
                        }
                    }

                    if (_parent != null)
                    {
                        return _parent.ResolveType(typeResolutionService, type);
                    }
                    else
                    {
                        return typeResolutionService.GetType(type, false);
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Extracts the metadata.
        /// </summary>
        /// <param name="codeScope">The code scope.</param>
        /// <param name="typeResolutionService">The type resolution service.</param>
        /// <param name="codeElements">The code elements.</param>
        /// <param name="rendererNamespace">The renderer namespace.</param>
        /// <param name="rendererName">Name of the renderer.</param>
        /// <param name="classFound">if set to <c>true</c> class found.</param>
        /// <returns></returns>
        private object[] ExtractMetadata(CodeScope codeScope, ITypeResolutionService typeResolutionService, CodeElements codeElements, ref string rendererNamespace, ref string rendererName, ref bool classFound)
        {
            List<object> metadataInstances = new List<object>();

            if (codeElements != null)
            {
                foreach (CodeElement codeElement in codeElements)
                {
                    CodeImport codeImport = codeElement as CodeImport;

                    if (codeImport != null)
                    {

                        codeScope.AddImport(codeImport.Namespace);
                    }
                    else
                    {
                        CodeClass codeClass = codeElement as CodeClass;

                        if (codeClass != null && !classFound)
                        {
                            classFound = true;

                            rendererNamespace = codeClass.Namespace.FullName;

                            foreach (CodeElement baseElement in codeClass.Bases)
                            {
                                rendererName = baseElement.Name;


                            }

                            List<object> attributeInstances = new List<object>();

                            foreach (CodeAttribute codeAttribute in codeClass.Attributes)
                            {
                                var attributeType = typeResolutionService.GetType(codeAttribute.FullName, false);

                                if (attributeType != null)
                                {
                                    Dictionary<string, object> namedArguments;

                                    object[] codeArguments = ExtractArguments(codeScope, typeResolutionService, codeAttribute.Children, out namedArguments);

                                    try
                                    {
                                        object metadataInstance = Activator.CreateInstance(attributeType, codeArguments);

                                        if (metadataInstance != null)
                                        {

                                            if (namedArguments != null && namedArguments.Count > 0)
                                            {
                                                foreach (KeyValuePair<string, object> namedArgument in namedArguments)
                                                {
                                                    PropertyInfo propertyInfo = attributeType.GetProperty(namedArgument.Key);

                                                    if (propertyInfo != null)
                                                    {
                                                        propertyInfo.SetValue(metadataInstance, namedArgument.Value);
                                                    }
                                                }
                                            }

                                            metadataInstances.Add(metadataInstance);
                                        }
                                    }
                                    catch (Exception exception)
                                    {
                                        this.GeneratorErrorCallback(false, 1, string.Format("Failed to create '{0}' attribute [message={1}].", attributeType.Name, exception.Message), codeAttribute.GetStartPoint().Line, codeAttribute.GetStartPoint().LineCharOffset);
                                    }
                                }
                            }

                        }
                        else
                        {
                            CodeNamespace codeNamespace = codeElement as CodeNamespace;

                            if (codeNamespace != null)
                            {
                                CodeScope namespaceScope = codeScope.CreateScope();

                                if (namespaceScope != null)
                                {
                                    namespaceScope.AddNamespace(codeNamespace.Name);

                                    metadataInstances.AddRange(ExtractMetadata(codeScope.CreateScope(), typeResolutionService, codeNamespace.Children, ref rendererNamespace, ref rendererName, ref classFound));
                                }
                            }
                        }
                    }
                }
            }

            return metadataInstances.ToArray();
        }

        private object[] ExtractArguments(CodeScope codeScope, ITypeResolutionService typeResolutionService, CodeElements codeElements, out Dictionary<string, object> namedArguments)
        {
            List<object> arguments = new List<object>();

            // Set named arguments dictionary
            namedArguments = new Dictionary<string, object>();

            foreach (object codeElement in codeElements)
            {

                CodeAttributeArgument codeArgument = codeElement as CodeAttributeArgument;

                if (codeArgument != null)
                {
                    string argumentName = null;

                    object argumentValue = ExtractArgument(codeScope, typeResolutionService, codeArgument, out argumentName);

                    if (string.IsNullOrWhiteSpace(argumentName))
                    {
                        arguments.Add(argumentValue);
                    }
                    else
                    {
                        namedArguments[argumentName] = argumentValue;
                    }


                }
            }

            return arguments.ToArray();
        }

        private object ExtractArgument(CodeScope codeScope, ITypeResolutionService typeResolutionService, CodeAttributeArgument codeArgument, out string name)
        {
            // Set the default name
            name = string.Empty;

            if (codeArgument != null)
            {
                string argumentCode = codeArgument.Value;

                // Set the argument name
                name = codeArgument.Name;

                if (!string.IsNullOrWhiteSpace(argumentCode))
                {
                    return codeScope.Parse(typeResolutionService, argumentCode);
                }
            }

            return null;
        }


        /// <summary>
        /// Evaluates the expression.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        private static object EvaluateExpression(string language, string expression)
        {
            return expression;
        }

        public override string GetDefaultExtension()
        {
            return "Base.cs";
        }

        #endregion

        ///	<summary>
        ///	Register the class as a	control	and	set	it's CodeBase entry
        ///	</summary>
        ///	<param name="key">The registry key of the control</param>
        [ComRegisterFunction()]
        public static void RegisterClass(string key)
        {
            RegisterSingleFileGenerators(typeof(RendererGenerator), false);
        }

        ///	<summary>
        ///	Called to un-register the control
        ///	</summary>
        ///	<param name="key">The registry key</param>
        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            RegisterSingleFileGenerators(typeof(RendererGenerator), true);
        }
    }
}
