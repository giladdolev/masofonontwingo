﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using HtmlAgilityPack;
using System.Xml;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using WinForms = System.Windows.Forms;
using System.Web.VisualTree.Common;
using System.Configuration;
using System.IO.Compression;

namespace System.Web.VisualTree.Design
{
    public class ResourcesLoader
    {
        private List<string> mScriptNameList = new List<string>();
        private List<string> mCssNameList = new List<string>();
        private bool mIsPackageLoaded = false;
        private List<string> mCssResourceList = new List<string>();
        private List<string> mScriptList = new List<string>();
        DateTime mTimeLastDownload;
        private static Regex cssImport = new Regex(@"import ([""'])(?<url>[^""']+)");


        private string mstrRootSite;
        private string mstrSiteMaster;

        #region Configuration Values
        private static string _ArchiveDirectory = null;
        private static string _ArchiveFileName = null;
        private bool _CompressResources = true;
       // private int _CacheExpirationDays = 30;
        private FileInfo _ArchiveFile = null;

        //     For single handler add following keys (resources virtual root folder and archive file virtual path) to appSettings:
        //     <add key="ResourceFolder" value="/extjs/" />
        //     <add key="ResourceArchive" value="/ext-5.1.0-gpl.zip" />

        #endregion

        /// <summary>
        /// Sets the CSS URL.
        /// </summary>
        /// <value>
        /// The CSS URL.
        /// </value>
        public Regex CssUrl { set; private get; }

        /// <summary>
        /// Gets the style CSS list.
        /// </summary>
        /// <value>
        /// The style CSS list.
        /// </value>
        public List<string> CssResourceList { get { return mCssResourceList; } }

        /// <summary>
        /// Gets the script list.
        /// </summary>
        /// <value>
        /// The script list.
        /// </value>
        public List<string> ScriptList { get { return mScriptList; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourcesLoader"/> class.
        /// </summary>
        /// <param name="strRootSite">The string root site.</param>
        /// <param name="strSiteMaster">The string site master.</param>
        public ResourcesLoader(string strRootSite, string strSiteMaster)
        {
            mstrRootSite = strRootSite;
            mstrSiteMaster = strSiteMaster;
            mIsPackageLoaded = false;
            mTimeLastDownload = DateTime.MinValue;
        }

        /// <summary>
        /// Gets the value from web configuration.
        /// </summary>
        /// <param name="pathCollection">The path collection.</param>
        /// <param name="strKeyName">Name of the string key.</param>
        /// <returns></returns>
        string GetValueFromWebConfig(XmlNodeList pathCollection, string strKeyName)
        {
            if (pathCollection == null && pathCollection.Count < 1 || String.IsNullOrEmpty(strKeyName))
            {
                return null; 
            }

            foreach (XmlNode script in pathCollection)
            {
                XmlNodeList list = script.ChildNodes;

                if (list != null)
                {
                    foreach (XmlNode node in list)
                    {
                        if (node != null)
                        {
                            XmlElement el = node as XmlElement;

                            if (el != null)
                            {
                                if (String.Equals(el.Attributes["key"].Value.ToLower(), strKeyName.ToLower()))
                                {
                                    XmlAttributeCollection attlist = el.Attributes;

                                    if (attlist != null)
                                    {
                                        foreach (XmlAttribute att in attlist)
                                        {
                                            if (att != null)
                                            {
                                                if (att.Name.ToLower() == "value")
                                                {
                                                    return att.Value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Parsings the webconfig.
        /// </summary>
        
        void WebConfigFileParsing()
        {
            string strWebconfigPath = RootComponentDesigner.GetAbsolutePathLongPath(mstrRootSite, "Web.config");
            string archiveFileName = "";

            try 
            {
                if (File.Exists(strWebconfigPath))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(strWebconfigPath);
                    XmlNodeList pathCollection = doc.SelectNodes("configuration//appSettings");
                    // Get archive file name
                    archiveFileName = GetValueFromWebConfig(pathCollection, "ResourceArchive");

                    if (String.IsNullOrEmpty(archiveFileName))
                    {
                       throw new Exception("ArchiveFileName is not defined. Check Web config file attribute 'ResourceArchive'");
                    }

                    // Get archive folder
                    _ArchiveDirectory = GetValueFromWebConfig(pathCollection, "ResourceFolder");

                    if (String.IsNullOrEmpty(_ArchiveDirectory))
                    {
                        // if not configured - assume it is archive file containing folder
                        _ArchiveDirectory = mstrRootSite;
                    }

                    string resourceFolder = _ArchiveDirectory.Replace('/', '\\');

                    archiveFileName = archiveFileName.Replace('/', '\\');

                    if (archiveFileName.StartsWith(@"\") && resourceFolder.EndsWith(@"\"))
                    {
                        archiveFileName = archiveFileName.Substring(1);
                    }
                    String zipFilePath = RootComponentDesigner.GetAbsolutePathLongPath(resourceFolder, archiveFileName);
                    if (zipFilePath.StartsWith(@"\") && mstrRootSite.EndsWith(@"\"))
                    {
                        zipFilePath = zipFilePath.Substring(1);
                    }
                    // Get archive full path

                    _ArchiveFileName = RootComponentDesigner.GetAbsolutePathLongPath(mstrRootSite, zipFilePath);
                    // Get root folder
                    _ArchiveDirectory = GetValueFromWebConfig(pathCollection, "ResourceFolder");
                    if (String.IsNullOrEmpty(_ArchiveDirectory))
                    {
                        // if not configured - assume it is archive file containing folder
                        _ArchiveDirectory = Path.GetDirectoryName(archiveFileName);
                    }

                    // Initialize compression flag. Default is true. Should be disabled if IIS compression for dynamic content configured
                    string compressResources = GetValueFromWebConfig(pathCollection, "CompressResources");
                    bool compress = false;
                    if (Boolean.TryParse(compressResources, out compress))
                    {
                        _CompressResources = compress;
                    }

                    // Initialize handler
                    Initialize();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Web config file {0} parsing is failed.\n Exception {1}", strWebconfigPath, ex.Message)); ; 
            }
           
        }

        /// <summary>
        /// Initialize()
        /// </summary>
        private void Initialize()
        {
            // Check validity of archive
            if (!String.IsNullOrEmpty(_ArchiveFileName))
            {
                // Check is archive file existed
                if (!File.Exists(_ArchiveFileName))
                {
                    throw new Exception(String.Format("ArchiveFileName {0} is not existed.\n Check Web config file attributes 'ResourceArchive' 'ResourceFolder'," ,  _ArchiveFileName));
                }
                try
                {
                    // Load archive info
                    _ArchiveFile = new FileInfo(_ArchiveFileName);
                }
                     
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    throw new Exception(String.Format("ArchiveFile {0} Initialize error.\n Exception {1}", _ArchiveFileName, ex.Message));
                }
            }
        }


        /// <summary>
        /// Determines whether [is need reload packages] [the specified site master file].
        /// </summary>
        /// <param name="siteMasterFile">The site master file.</param>
        /// <returns></returns>
        private Boolean IsResourceModified(string siteMasterFile)
        {
            if (mIsPackageLoaded == false)
            {
                return true;
            }

            FileInfo fileInfo = new FileInfo(siteMasterFile);

            //Less than zero t1 is earlier than t2.
            if (DateTime.Compare(fileInfo.LastWriteTimeUtc, mTimeLastDownload) < 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Resourceses the processing.
        /// </summary>
        public void ResourcesProcessing()
        {
            string strSiteMaster = RootComponentDesigner.GetAbsolutePathLongPath(mstrRootSite, mstrSiteMaster);

            if (String.IsNullOrEmpty(strSiteMaster))
            {
                throw new Exception("ResourcesProcessing is failed.SiteMaster file name is empty");
            }

            if (IsResourceModified(strSiteMaster))
            {
                try
                {
                    WebConfigFileParsing();

                    SiteMasterFileParsing(strSiteMaster);

                    CreateResourceList(ref mCssNameList, ref mCssResourceList);

                    CreateResourceList(ref mScriptNameList, ref mScriptList);

                    mIsPackageLoaded = true;

                    mTimeLastDownload = DateTime.UtcNow;
               }
               catch (Exception ex)
               {
                    Debug.WriteLine(ex.Message);
                    throw;

               }
            }
        }

        /// <summary>
        /// Creates the resource list.
        /// </summary>
        /// <param name="nameList">The name list.</param>
        /// <param name="resourceList">The resource list.</param>
        void CreateResourceList(ref List<string> nameList, ref List<string> resourceList)
        {
            if (nameList != null)
            {
                if (resourceList == null)
                {
                    resourceList = new List<string>();
                }

                foreach (string name in nameList)
                {
                    string strFileName = name;
                    if (!String.IsNullOrEmpty(strFileName))
                    {
                        if (strFileName.Trim().StartsWith("http"))
                        {
                            CreateResourceFileListByUrl(strFileName, mstrRootSite, ref resourceList);
                        }
                        else
                        {
                            if (mCssResourceList.IndexOf(strFileName) < 0)
                            {
                                if (strFileName.StartsWith("/") || strFileName.StartsWith("\\"))
                                {
                                    strFileName = strFileName.Substring(1);
                                }
                                int pos = strFileName.IndexOf('?');

                                if (pos > -1)
                                {
                                    strFileName = strFileName.Substring(0, pos);
                                }
                                resourceList.Add(strFileName.Replace("//", "/"));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sites the master file parsing.
        /// </summary>
        /// <param name="baseDirectory">The base directory.</param>
        /// <param name="strSiteMaster">The string site master.</param>
        void SiteMasterFileParsing(string strSiteMaster)
        {
            HtmlDocument doc = new HtmlDocument();
            try
            {
                doc.Load(strSiteMaster);
                // Targets a specific node
                if (doc.DocumentNode.SelectNodes("html//script") != null)
                {
                    mScriptNameList.Clear();

                    foreach (HtmlNode script in doc.DocumentNode.SelectNodes("html//script"))
                    {
                        HtmlAttribute att = script.Attributes["src"];
                        if (att != null)
                        {
                            string strVal = att.Value;
                            if (!String.IsNullOrEmpty(strVal) && mScriptNameList.IndexOf(strVal) < 0)
                            {
                                mScriptNameList.Add(strVal);
                            }
                        }
                    }
                }

                if (doc.DocumentNode.SelectNodes("html//link") != null)
                {
                    mCssNameList.Clear();

                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("html//link"))
                    {
                        if (link.HasAttributes)
                        {
                            string rel = link.GetAttributeValue("rel", "");
                            if (rel == "stylesheet")
                            {
                                string strhref = link.GetAttributeValue("href", "");
                                if (!String.IsNullOrEmpty(strhref) && mCssNameList.IndexOf(strhref) < 0)
                                {
                                    mCssNameList.Add(strhref);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(String.Format("SiteMasterFileParsing error, file {0}\n Exception: {1}", strSiteMaster, ex.Message ) );
            }
        }

        /// <summary>
        /// Creates the resource file list by URL.
        /// </summary>
        /// <param name="strResourceUrl">The string resource URL.</param>
        /// <param name="strBasePath">The string base path.</param>
        /// <param name="fileList">The file list.</param>
        void CreateResourceFileListByUrl(string strResourceUrl, string strBasePath, ref List<string> fileList)
        {
            try
            {
                if (!String.IsNullOrEmpty(strResourceUrl))
                {
                    MatchCollection matchCollection = null;

                    Uri uriDownload = new Uri(strResourceUrl);

                    if (!String.IsNullOrEmpty(uriDownload.LocalPath))
                    {
                        string resourcePath = uriDownload.LocalPath.Replace("//", "/");

                        if (resourcePath.StartsWith("/") || resourcePath.StartsWith("\\"))
                        {
                            resourcePath = resourcePath.Substring(1);
                        }

                        string content = GetContentFileFromZipArchive(resourcePath);

                        int importCount = 0;

                        if (!String.IsNullOrEmpty(content))
                        {
                            matchCollection = cssImport.Matches(content);
                            if (matchCollection == null || matchCollection.Count < 1 || importCount > 50)
                            {
                                if (fileList.IndexOf(resourcePath) < 0)
                                {
                                    fileList.Add(resourcePath);
                                }
                            }
                            else
                            {
                                foreach (Match item in matchCollection)
                                {
                                    string strVal = item.Groups[2].Value;
                                    importCount++;
                                    //create uri path
                                    if (!String.IsNullOrEmpty(strVal))
                                    {
                                        Uri uri = new Uri(uriDownload, strVal);
                                        string strUri = uri.ToString();
                                        CreateResourceFileListByUrl(strUri, strBasePath, ref fileList);
                                    }
                                }

                                //check if file contained text data exept imports
                                char[] charSeparators = new char[] { ';' };
                                string[] slist = content.Trim().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

                                if (slist != null && slist.Length > importCount)
                                {
                                    if (fileList.IndexOf(resourcePath) < 0)
                                    {
                                        fileList.Add(resourcePath);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                String strException = String.Format("CreateResourceFileListByUrl;\nstrResourceUrl {0} \n ArchiveFileName: {1} \n Exception {2}", strResourceUrl, _ArchiveFileName, ex.Message);
                throw new Exception(strException);
            }
        }

        /// <summary>
        /// Gets the content file from zip archive.
        /// </summary>
        /// <param name="requestedResourcePath">The requested resource path.</param>
        /// <returns></returns>
        public static string GetContentFileFromZipArchive(string requestedResourcePath)
        {
            if (!String.IsNullOrEmpty(requestedResourcePath))
            {
                requestedResourcePath = requestedResourcePath.ToLowerInvariant();

                // Look for requested resource in archive
                using (FileStream zipToOpen = new FileStream(_ArchiveFileName, FileMode.Open, FileAccess.Read))
                {
                    // Initialize archive stream
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Read))
                    {
                        // Scan entries
                        foreach (ZipArchiveEntry archiveEntry in archive.Entries)
                        {
                            // Check if entry name match to requested resource
                            if (String.Equals(archiveEntry.FullName, requestedResourcePath, StringComparison.OrdinalIgnoreCase))
                            {
                                // Get resource stream
                                using (Stream resourceStream = archiveEntry.Open())
                                {
                                    // Prepare buffer for content
                                    int contentLength = (int)archiveEntry.Length;
                                    byte[] data = new byte[contentLength];

                                    string strContent = null;

                                    // Read resource content
                                    resourceStream.Read(data, 0, contentLength);
                                    if (data != null)
                                    {
                                        strContent = System.Text.Encoding.Default.GetString(data);
                                    }

                                    // Serve retrieved content
                                    //   WriteData(response, data, resourceType, isCompressionRequired);

                                    //   // Set cache headers
                                    //   SetResponseCache(response);

                                    // Store content in output cache
                                    //  SetCache(context, virtualResourcePath, data, isCompressionRequired);

                                    return strContent;
                                }

                            }
                        }
                    }
                }
            }


            return null;
        }

   
    }
}
