﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.Design;

namespace System.Web.VisualTree.Design
{
    public class VisualTreeDesigner : WebFormsRootDesigner, IDesignerHost
    {
        private string _TagPrefix;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualTreeDesigner"/> class.
        /// </summary>
        /// <param name="tagPrefix">The tag prefix.</param>
        public VisualTreeDesigner(string tagPrefix)
        {
            _TagPrefix = tagPrefix;
        }
        public override void AddClientScriptToDocument(ClientScriptItem scriptItem)
        {
            throw new NotImplementedException();
        }

        public override string AddControlToDocument(UI.Control newControl, UI.Control referenceControl, ControlLocation location)
        {
            throw new NotImplementedException();
        }

        public override string DocumentUrl
        {
            get { throw new NotImplementedException(); }
        }

        public override ClientScriptItemCollection GetClientScriptsInDocument()
        {
            throw new NotImplementedException();
        }

        protected override void GetControlViewAndTag(UI.Control control, out IControlDesignerView view, out IControlDesignerTag tag)
        {
            throw new NotImplementedException();
        }

        public override bool IsDesignerViewLocked
        {
            get { return false; }
        }

        public override bool IsLoading
        {
            get { return false; }
        }

        /// <summary>
        /// When overridden in a derived class, gets a <see cref="T:System.Web.UI.Design.WebFormsReferenceManager" /> object that has information about the current Web Forms page.
        /// </summary>
        public override WebFormsReferenceManager ReferenceManager
        {
            get { return new VisualTreeReferenceManager(_TagPrefix); }
        }

        public override void RemoveClientScriptFromDocument(string clientScriptId)
        {
        }

        public override void RemoveControlFromDocument(UI.Control control)
        {
        }

        public void Activate()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Activated;

        public ComponentModel.IContainer Container
        {
            get { throw new NotImplementedException(); }
        }

        public ComponentModel.IComponent CreateComponent(Type componentClass, string name)
        {
            throw new NotImplementedException();
        }

        public ComponentModel.IComponent CreateComponent(Type componentClass)
        {
            throw new NotImplementedException();
        }

        public DesignerTransaction CreateTransaction(string description)
        {
            throw new NotImplementedException();
        }

        public DesignerTransaction CreateTransaction()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Deactivated;

        public void DestroyComponent(ComponentModel.IComponent component)
        {
            throw new NotImplementedException();
        }

        public IDesigner GetDesigner(ComponentModel.IComponent component)
        {
            throw new NotImplementedException();
        }

        public Type GetType(string typeName)
        {
            throw new NotImplementedException();
        }

        public bool InTransaction
        {
            get { throw new NotImplementedException(); }
        }

        public bool Loading
        {
            get { throw new NotImplementedException(); }
        }

        public ComponentModel.IComponent RootComponent
        {
            get { throw new NotImplementedException(); }
        }

        public string RootComponentClassName
        {
            get { throw new NotImplementedException(); }
        }

        public event DesignerTransactionCloseEventHandler TransactionClosed;

        public event DesignerTransactionCloseEventHandler TransactionClosing;

        public string TransactionDescription
        {
            get { throw new NotImplementedException(); }
        }

        public event EventHandler TransactionOpened;

        public event EventHandler TransactionOpening;

        public void AddService(Type serviceType, ServiceCreatorCallback callback, bool promote)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, ServiceCreatorCallback callback)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, object serviceInstance, bool promote)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, object serviceInstance)
        {
            throw new NotImplementedException();
        }

        public void RemoveService(Type serviceType, bool promote)
        {
            throw new NotImplementedException();
        }

        public void RemoveService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public new object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}
