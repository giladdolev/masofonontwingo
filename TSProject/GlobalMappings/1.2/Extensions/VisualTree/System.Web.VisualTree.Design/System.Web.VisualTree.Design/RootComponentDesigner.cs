﻿using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Design;
using Microsoft.VisualStudio.Shell.Interop;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Common;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.WebControls;
using System.Xml.Linq;
using WinForms = System.Windows.Forms;


namespace System.Web.VisualTree.Design
{

    /// <summary>
    /// /
    /// </summary>
    public class RootComponentDesigner : ContainerControlDesigner
    {
        private class ViewerComponent
        {
            /// <summary>
            /// Gets or sets the rect.
            /// </summary>
            /// <value>
            /// The rect.
            /// </value>
            public Rectangle Region
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the identifier.
            /// </summary>
            /// <value>
            /// The identifier.
            /// </value>
            public string Id
            {
                get;
                set;
            }

            /// <summary>
            /// Determines whether contains.
            /// </summary>
            /// <param name="location">The location.</param>
            /// <returns></returns>
            public bool Contains(Point location)
            {
                return this.Region.Contains(location);
            }

            /// <summary>
            /// Determines whether contains.
            /// </summary>
            /// <param name="region">The region.</param>
            /// <returns></returns>
            public bool Contains(Rectangle region)
            {
                return this.Region.Contains(region);
            }


            /// <summary>
            /// Determines whether contains.
            /// </summary>
            /// <param name="component">The component.</param>
            /// <returns></returns>
            public bool Contains(ViewerComponent component)
            {
                // If there is a valid component
                if (component != null)
                {
                    // If component contains other component
                    return this.Contains(component.Region);
                }

                return false;
            }
        }

        /// <summary>
        /// The viewer components
        /// </summary>
        private List<ViewerComponent> _viewerComponents = null;


        /// <summary>
        /// The component viewer
        /// </summary>
        /// <value>
        /// The component viewer.
        /// </value>
        public dynamic ComponentViewer
        {
            get;
            set;
        }


        /// <summary>
        /// Gets the HTML that is used to represent the control at design time.
        /// </summary>
        /// <returns>
        /// The HTML that is used to represent the control at design time.
        /// </returns>
        public override string GetDesignTimeHtml()
        {
            string strError = DesignerResourcesProcessing();

            if (!String.IsNullOrEmpty(strError))
            {
                return this.GetErrorDesignTimeHtml(new Exception(strError));
            }

            return GetDesignTimeHtmlApply();
        }

        /// <summary>
        /// Returns the HTML markup that is used to represent the control at design time.
        /// </summary>
        /// <param name="regions">A collection of designer regions.</param>
        /// <returns>
        /// An HTML markup string that represents the control.
        /// </returns>
        public override string GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            WebControl component = (WebControl)base.Component;
            Unit width = component.Width;
            Unit height = component.Height;
            component.Width = 0;
            component.Height = 0;
            string structureHtmlBase = base.GetDesignTimeHtml(regions);
            component.Width = width;
            component.Height = height;
            string structureHtml = this.GetDesignTimeHtml();

            if (String.IsNullOrEmpty(structureHtml))
            {
                return structureHtmlBase;
            }
            else
            {
                return structureHtml;
            }
        }
       
        /// <summary>
        /// Gets the design time HTML apply.
        /// </summary>
        /// <returns></returns>
        protected string GetDesignTimeHtmlApply()
        {
            try
            {
                // Get visual component
                IVisualTreeComponent component = this.Component as IVisualTreeComponent;
                // If there is a valid visual component

                if (component != null)
                {
                    string strError = String.Empty;

                    DesignerDataProcessing(ref strError);

                    if (!String.IsNullOrEmpty(strError))
                    {
                        return this.GetErrorDesignTimeHtml(new Exception(strError));
                    }

                    // Return the surfact code
                    return string.Format("<OBJECT height=\"{0}\" width=\"{1}\" classid=\"clsid:1302335C-AF4E-4F43-8A31-94978F08F29A\"></object>", component.Height, component.Width);
                }
                else
                {
                    // Report the error 
                    return this.GetErrorDesignTimeHtml(new Exception("Unkown component found."));
                }
            }
            catch (Exception exception)
            {
                // Report the error 
                return this.GetErrorDesignTimeHtml(exception);
            }
        }



        /// <summary>
        /// Gets the root site.
        /// </summary>
        /// <returns></returns>
        protected string GetRootSite()
        {
            // Get the web application
            IWebApplication webApplication = this.GetService(typeof(IWebApplication)) as IWebApplication;

            // If there is a valid web application
            if (webApplication != null)
            {
                // Get project item
                IProjectItem projectItem = webApplication.RootProjectItem;

                // If there is a valid project item
                if (projectItem != null)
                {
                    // Get physical path
                    return projectItem.PhysicalPath;
                }
            }

            return @"D:\TSProject\GlobalMappings\1.2\Extensions\AspMvc\System.Web.VisualTree.Tests\";
        }

        /// <summary>
        /// Generates the design time HTML script.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="baseDirectory">The base directory.</param>
        /// <param name="file">The file.</param>
        protected static void GenerateDesignTimeHtmlScript(StringWriter buffer, string baseDirectory, string file)
        {
            string resourcePath = file;
            file = file.Replace('/', '\\');
            string filePath = Path.Combine(baseDirectory, file);

            if (File.Exists(filePath))
            {
                buffer.WriteLine("<script>");
                buffer.WriteLine(File.ReadAllText(filePath));
                buffer.WriteLine("</script>");
            }
            else
            {
                //try read scrip content from ZipArchive file
                string content = ResourcesLoader.GetContentFileFromZipArchive(resourcePath);
                if (!String.IsNullOrEmpty(content))
                {
                    buffer.WriteLine("<script>");
                    buffer.WriteLine(content);
                    buffer.WriteLine("</script>");
                }
            }
         }

        /// <summary>
        /// Generates the design time HTML style.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="baseDirectory">The base directory.</param>
        /// <param name="file">The file.</param>
        protected virtual void GenerateDesignTimeHtmlStyle(StringWriter buffer, string baseDirectory, string file)
        {
            string resourcePath = file;

            file = file.Replace('/', '\\');
            // Get the css file
            string cssContent = null;
            string filePath = Path.Combine(baseDirectory, file);

            if (File.Exists(filePath))
            {
                cssContent = GetOrCreateCssContent(filePath);
            }
            else
            {
                //try to read from ZipArchive
                cssContent = GetOrCreateCssContentFromZipArchive(resourcePath);
            }

            // If there is a valid css content
            if (!string.IsNullOrEmpty(cssContent))
            {
                // Create the style content
                buffer.WriteLine("<style>");
                buffer.WriteLine(cssContent);
                buffer.WriteLine("</style>");
            }

        }

        /// <summary>
        /// The CSS documents
        /// </summary>
        private static readonly Dictionary<string, string> CssDocuments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets the content of the or create CSS.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        private static string GetOrCreateCssContent(string filePath)
        {
            string content = null;

            // If there is a valid file path
            if (File.Exists(filePath))
            {
                // Try to get css document
                if (!CssDocuments.TryGetValue(filePath, out content))
                {
                    // Create embeded css and cache it
                    content = File.ReadAllText(filePath);
                    content = EmbedImages(content, Path.GetDirectoryName(filePath), Path.GetExtension(filePath));
                    CssDocuments[filePath] = content;
                }
            }

            return content;
        }

        private static string GetOrCreateCssContentFromZipArchive(string resourcePath)
        {
            string content = null; 

            // Try to get css document
            if (!CssDocuments.TryGetValue(resourcePath, out content))
            {
                // Create embeded css and cache it
                content = ResourcesLoader.GetContentFileFromZipArchive(resourcePath);

                if (!String.IsNullOrEmpty(content))
                {
                    content = EmbedImages(content, Path.GetDirectoryName(resourcePath), Path.GetExtension(resourcePath));
                    CssDocuments[resourcePath] = content;
                }
            }

            return content;
        }

        /// <summary>
        /// Provides support for mattching CSS urls
        /// </summary>
        private static Regex CssUrl = new Regex("url\\(['\"]?(?<url>[^\"')]*(gif|png|jpg|jpeg))['\"]?\\)", RegexOptions.Compiled);
        public static Regex CssUrlRegex { get { return CssUrl; } }

        /// <summary>
        /// Embeds the images.
        /// </summary>
        /// <param name="cssContent">The CSS content.</param>
        /// <param name="cssDirectory">The CSS directory.</param>
        /// <param name="imageExtension">The image extension.</param>
        /// <returns></returns>
        private static string EmbedImages(string cssContent, string cssDirectory, string imageExtension)
        {
            // Return the embeded css version
            return CssUrl.Replace(cssContent, delegate(Match match)
            {
                // Get the css URL
                string cssUrl = match.Groups["url"].Value;

                // Get css absolute path
                string cssAbsolutePath = GetAbsolutePathLongPath(cssDirectory, cssUrl.Replace('\\', '/'));

                // If there is a valid file
                if (File.Exists(cssAbsolutePath))
                {
                    // Create embeded file url syntax
                    StringBuilder embededFile = new StringBuilder();
                    embededFile.Append("url(data:");
                    embededFile.Append("image/png");
                    embededFile.Append(";base64,");
                    embededFile.Append(Convert.ToBase64String(File.ReadAllBytes(cssAbsolutePath)));
                    embededFile.Append(")");
                    return embededFile.ToString();
                }

                // Return the original value as is
                return match.Value;
            });
        }

        /// <summary>
        /// Get the absolute path relative to the base path
        /// </summary>
        /// <param name="strBasePath">The string base path.</param>
        /// <param name="strPath">The string path.</param>
        /// <returns></returns>
        static internal string GetAbsolutePathLongPath(string strBasePath, string strPath)
        {
            if (!string.IsNullOrEmpty(strPath))
            {
                if (Path.IsPathRooted(strPath))
                {
                    return strPath;
                }
                else
                {
                    if (!string.IsNullOrEmpty(strBasePath))
                    {
                        // For cases where the relative path to the file is ../<some file>
                        // This means the the path to the directory is .. and it is not handled when creating a directory.
                        // Fixes #1741
                        if (strPath.EndsWith(".."))
                        {
                            strPath += "/";
                        }

                        // If is parent directory prefix
                        if (strPath.StartsWith("../") || strPath.StartsWith(@"..\"))
                        {
                            DirectoryInfo objDirectoryInfo = Directory.GetParent(strBasePath);
                            if (objDirectoryInfo != null)
                            {
                                return GetAbsolutePathLongPath(objDirectoryInfo.FullName, strPath.Substring(3));
                            }
                            else
                            {
                                return Path.Combine(strBasePath, strPath);
                            }
                        }
                        // If is same directory prefix
                        else if (strPath.StartsWith("./") || strPath.StartsWith(@".\"))
                        {
                            // Remove same directory prefix
                            return Path.Combine(strBasePath, strPath.Substring(2));
                        }
                        else
                        {
                            return Path.Combine(strBasePath, strPath);
                        }
                    }
                    else
                    {
                        return strPath;
                    }
                }
            }
            else
            {
                return strBasePath;
            }
        }


        
        /// <summary>
        /// Releases the unmanaged resources that are used by the <see cref="T:System.Web.UI.Design.HtmlControlDesigner" /> object and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);


        }



        /// <summary>
        /// Generates the HTML.
        /// </summary>
        /// <returns></returns>
        public string GenerateHTML()
        {
            // Get visual component
            IVisualTreeComponent component = this.Component as IVisualTreeComponent;

            // If there is a valid visual component
            if (component != null)
            {
                // Get the HTML of the designer                
                return GenerateHTML(component, this.GetRootSite());
            }

            // Return default method
            return "<html><body>Component was not found.</body></html>";
        }

        /// <summary>
        /// Generates the HTML.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="baseDirectory">The base directory.</param>
        /// <returns></returns>
        /// 
        protected virtual string GenerateHTML(IVisualTreeComponent component, string baseDirectory)
        {
            string generatedContent = null;

            try
            {
                StringWriter buffer = new StringWriter();
                buffer.WriteLine("<html>");
                buffer.WriteLine("<head>");
                GenerateDesignTimeHtmlScript(buffer, baseDirectory, @"Scripts\visualtree.js");
                GenerateDesignTimeHtmlScript(buffer, baseDirectory, @"Scripts\visualtree-designer.js");

                foreach (string fileName in m_ResourcesLoader.ScriptList)
                {
                    if (!String.IsNullOrEmpty(fileName))
                    {
                        if (fileName.Contains("ext-visualtree.js") || fileName.Contains("ext-visualtree-designer.js"))
                            continue;

                        GenerateDesignTimeHtmlScript(buffer, baseDirectory, fileName);
                    }
                }

                foreach (string fileName in m_ResourcesLoader.CssResourceList)
                {
                    GenerateDesignTimeHtmlStyle(buffer, baseDirectory, fileName);
                }

                buffer.WriteLine("</head>");
                buffer.WriteLine("<body>");

                // Assemblies to use in the MEF process of finding renderers and their providers
                HashSet<Assembly> assemblies = new HashSet<Assembly>();

                // Get the type resolution service
                DynamicTypeService dynamicTypeService = this.GetService(typeof(DynamicTypeService)) as DynamicTypeService;
                IVsHierarchy vsHierarchy = (IVsHierarchy)this.GetService(typeof(IVsHierarchy));

                ITypeResolutionService typeResolutionService = dynamicTypeService.GetTypeResolutionService(vsHierarchy);

                // Get all references from the calling project (including project references)
                dynamic references = GetReferencesFromCallerProject(vsHierarchy);

                // If references are valid
                if (references != null)
                {
                    // Loop through all the references
                    foreach (VSLangProj.Reference reference in references)
                    {

                        //patch for function CombineCatalog() function of the class LoadingHelper
                        //EntityFramework.SqlServer.dll, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' for the 'System.Data.SqlClient' 
                        //could not be loaded.
                        
                        if (reference.Name.IndexOf("EntityFramework.SqlServer") > -1)
                        {
                            continue;
                        }

                        // Get the assembly from the type resolution service
                        Assembly assembly = typeResolutionService.GetAssembly(new AssemblyName(reference.Name), false);
                        if (assembly != null)
                        {
                            assemblies.Add(assembly);
                        }
                    }
                }

                RenderingContext renderingContext = new RenderingContext(RenderingEnvironment.ExtJS, true, baseDirectory, assemblies);

                RenderingUtils.Render(renderingContext, component.Element, buffer);
                buffer.WriteLine("</body>");
                buffer.WriteLine("</html>");

                // Temporary fix
                generatedContent = buffer.ToString();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }


            File.WriteAllText(@"C:\rootComponentDesigner", generatedContent);

            return generatedContent;
        }




        /// <summary>
        /// Gets the references from caller project.
        /// </summary>
        /// <param name="vsHierarchy">The vs hierarchy.</param>
        /// <param name="includeProjectReferences">if set to <c>true</c> [include project references].</param>
        /// <returns></returns>
        private object GetReferencesFromCallerProject(IVsHierarchy vsHierarchy)
        {
            try
            {
                // Get the vsProject
                object vsProject;
                vsHierarchy.GetProperty(VSConstants.VSITEMID_ROOT, (int)__VSHPROPID.VSHPROPID_ExtObject, out vsProject);
                if (vsProject != null)
                {
                    // Cast to DTE project
                    EnvDTE.Project dteProject = vsProject as EnvDTE.Project;
                    if (dteProject != null)
                    {
                        // Get the actual project
                        var actualProject = dteProject.Object;
                        if (actualProject != null)
                        {
                            // Get the references
                            var references = actualProject.References;
                            if (references != null)
                            {
                                return references;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return null;
        }

        /// <summary>
        /// Called by the design host when the user clicks the associated control at design time.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Web.UI.Design.DesignerRegionMouseEventArgs" /> object that specifies the location and, possibly, the control designer region that the user clicked.</param>

        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            base.OnClick(e);

            // Get current control
            WebControl control = this.Component as WebControl;

            // If there is a valid control
            if (control != null)
            {

                // Get the click location
                Point location = e.Location;

                ControlElementCollection controlElementList = null;

                System.Web.VisualTree.WebControls.Control controlElementControl = this.Component as System.Web.VisualTree.WebControls.Control;

                if (controlElementControl != null)
                {
                    controlElementList = controlElementControl.Element.Controls;
                }
                System.Web.VisualTree.Elements.ControlElement currentElement = null;

                currentElement = FindClickedElement(controlElementList, location);

                // If there is a valid component
                if (currentElement != null)
                {
                    // Get clicked control
                    System.Web.VisualTree.Elements.ControlElement clickedControl = currentElement;

                    // If there is a valid clicked control
                    if (clickedControl != null)
                    {
                        String strMsg = string.Format("{0}\nLocation:{1}", clickedControl.ID, clickedControl.Location.ToString());
                        String strTitel = string.Format("ID: {0}", clickedControl.ID);
                        strMsg = string.Format("Text: {0}\nLocation:{1}\nLeft:{2}, Top: {3}\nWidth:{4}, Height: {5}",
                                                        clickedControl.Text,
                                                        clickedControl.Location.ToString(),
                                                        clickedControl.Left.ToString(), clickedControl.Top.ToString(),
                                                        clickedControl.Width.ToString(), clickedControl.Height.ToString());

                        WinForms.MessageBox.Show(strMsg, strTitel);

                        ISelectionService selectionService = this.GetService(typeof(ISelectionService)) as ISelectionService;

                        if (selectionService != null)
                        {
                            ArrayList a = new ArrayList();
                            a.Add(clickedControl);
                            selectionService.SetSelectedComponents(a, SelectionTypes.Replace);
                        }
                    }
                }
            }

            return;
        }


        /// <summary>
        /// Ensures the viewer components.
        /// </summary>
        private void EnsureViewerComponents()
        {
            // If there is a valid components list
            if (_viewerComponents == null)
            {
                // Create components list
                _viewerComponents = new List<ViewerComponent>();

                try
                {
                    // If there is a valid component viewer
                    if (this.ComponentViewer != null)
                    {
                        // Get the manifest
                        string componentsManifest = this.ComponentViewer.GetComponentsManifest();

                        // If there is a valid manifest
                        if (!string.IsNullOrEmpty(componentsManifest))
                        {
                            // Get manifest element
                            XElement manifestElement = XElement.Parse(componentsManifest);

                            // If there is a valid manifest element
                            if (manifestElement != null)
                            {
                                // Loop all components
                                foreach (XElement manifestComponentElement in manifestElement.Elements("component"))
                                {
                                    // Add components
                                    _viewerComponents.Add(new ViewerComponent()
                                    {
                                        // Get the component id
                                        Id = GetAttribute(manifestComponentElement, "id"),

                                        // Set the component region
                                        Region = new Rectangle(
                                                GetIntegerAttribute(manifestComponentElement, "x"),
                                                GetIntegerAttribute(manifestComponentElement, "y"),
                                                GetIntegerAttribute(manifestComponentElement, "w"),
                                                GetIntegerAttribute(manifestComponentElement, "h")
                                            )
                                    });
                                }
                            }

                        }
                    }
                }
                catch (Exception exception)
                {
                    WinForms.MessageBox.Show(exception.Message, "Visual Tree Designer Exception", WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private static int GetIntegerAttribute(XElement element, string name)
        {
            int value;
            int.TryParse(GetAttribute(element, name), out value);
            return value;
        }

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private static string GetAttribute(XElement element, string name)
        {
            if (element != null)
            {
                XAttribute attribute = element.Attribute(name);

                if (attribute != null)
                {
                    return attribute.Value;
                }
            }

            return null;
        }

        /// <summary>
        /// Invokes the script method.
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private object InvokeScriptMethod(string method, params object[] args)
        {
            // If there is a valid component viewer
            if (this.ComponentViewer != null)
            {
                // Invoke method
                return this.ComponentViewer.InvokeScriptMethod(method, args);
            }

            return null;
        }

        SharedMemoryInvokeWrapper sharedMemoryWrapper = new SharedMemoryInvokeWrapper();

        readonly int INT_ACTION_ASP_VIEWER = 100;

        private void DesignerDataProcessing(ref string strError)
        {

            String htmlData;

            htmlData = GenerateHTML();

            if (String.IsNullOrEmpty(htmlData))
            {
                strError = "HTML empty string was generated";
                return;
            }

            sharedMemoryWrapper.SetSharedMemoryData(htmlData, INT_ACTION_ASP_VIEWER, ref strError);

        }


        /// <summary>
        /// Called by the design host when the user clicks the associated control at design time.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Web.UI.Design.DesignerRegionMouseEventArgs" /> object that specifies the location and, possibly, the control designer region that the user clicked.</param>


        ControlElement FindClickedElement(ControlElementCollection controlElementList, Point location)
        {
            ControlElement currentElement = null;

            if (controlElementList != null)
            {
                foreach (System.Web.VisualTree.Elements.ControlElement element in controlElementList)
                {
                    if (element != null)
                    {
                        // element
                        Rectangle region = element.ClientRectangle;
                        if (region.Contains(location))
                        {
                            if (element.Controls != null && element.Controls.Count > 1)
                                currentElement = FindClickedElement(element.Controls, location);

                            // If there is no current component
                            if (currentElement == null)
                            {
                                // Set the current component
                                currentElement = element;
                            }
                            else
                            {
                                // If component contains component
                                if (currentElement.Contains(element))
                                {
                                    // Set current component
                                    currentElement = element;
                                }
                            }
                        }
                    }
                }
            }

            return currentElement;
        }

        private static ResourcesLoader m_ResourcesLoader = null;

        /// <summary>
        /// Designers the resources processing.
        /// </summary>
        protected virtual string DesignerResourcesProcessing()
        {
           String strError = String.Empty;
            try
            {
                if (m_ResourcesLoader == null)
                {
                    m_ResourcesLoader = new ResourcesLoader(this.GetRootSite(), "Site.Master");
                    m_ResourcesLoader.CssUrl = CssUrlRegex;
                }

                m_ResourcesLoader.ResourcesProcessing();
            }
            catch(Exception ex)
            {
                strError = String.Format("DesignerResourcesProcessing is failed with error: {0} ", ex.Message);
            }
            return strError;
        }
    }
}

