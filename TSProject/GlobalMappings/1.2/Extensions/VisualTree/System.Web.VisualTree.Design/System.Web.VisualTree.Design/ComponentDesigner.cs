﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace System.Web.VisualTree.Design
{
    public class ComponentDesigner : ControlDesigner
    {
        public override string GetDesignTimeHtml()
        {
            WebControl component = (WebControl)base.Component;
            Unit width = component.Width;
            Unit height = component.Height;
            component.Width = 0;
            component.Height = 0;
            string structureHtml = base.GetDesignTimeHtml();
            component.Width = width;
            component.Height = height;
            return structureHtml;
        }
    }
    public class ContainerDesigner : ContainerControlDesigner
    {

    }
}
