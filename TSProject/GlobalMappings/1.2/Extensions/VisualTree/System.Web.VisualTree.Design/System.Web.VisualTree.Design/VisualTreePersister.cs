﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.VisualTree.Design;
using System.Web.VisualTree.WebControls;
using System.Web.UI;


namespace System.Web.VisualTree
{
    public class VisualTreePersister : IDesignerHost
    {
        /// <summary>
        /// Provides support for writing indented markup
        /// </summary>
        private class MarkupWriter : StringWriter
        {

            /// <summary>
            /// The tag level
            /// </summary>
            private int _indentLevel = 0;


            /// <summary>
            /// Initializes a new instance of the <see cref="MarkupWriter"/> class.
            /// </summary>
            /// <param name="initialIndent">The initial indent.</param>
            public MarkupWriter(int initialIndent)
            {
                _indentLevel = initialIndent;
            }

            /// <summary>
            /// Writes a character to the string.
            /// </summary>
            /// <param name="value">The character to write.</param>
            public override void Write(char value)
            {
                switch (value)
                {
                    case '>':
                        this.WriteCloseTag();
                        break;
                    case '<':
                        this.WriteStartTag();
                        break;
                    default:
                        base.WriteLine(value);
                        break;
                }


            }

            /// <summary>
            /// Writes a character followed by a line terminator to the text string or stream.
            /// </summary>
            /// <param name="value">The character to write to the text stream.</param>
            public override void WriteLine(char value)
            {
                switch (value)
                {
                    case '>':
                        this.WriteCloseTag();
                        break;                        
                    default:
                        base.WriteLine(value);
                        break;
                }
            }

            /// <summary>
            /// Writes a string followed by a line terminator to the text string or stream.
            /// </summary>
            /// <param name="value">The string to write. If <paramref name="value" /> is null, only the line terminator is written.</param>
            public override void WriteLine(string value)
            {
                // Choose action based on value
                switch (value)
                {
                    case "<":
                        this.WriteStartTag();
                        break;
                    case ">":
                        this.WriteCloseTag();
                        break;
                    case "/>":
                        this.WriteCloseEmptyTag();
                        break;
                    case "</":
                        this.WriteStartClosingTag();
                        break;
                    default:
                        base.WriteLine(value);
                        break;
                }
            }


            /// <summary>
            /// Writes a string to the current string.
            /// </summary>
            /// <param name="value">The string to write.</param>
            public override void Write(string value)
            {
                // Choose action based on value
                switch (value)
                {
                    case "<":
                        this.WriteStartTag();
                        break;
                    case ">":
                        this.WriteCloseTag();
                        break;
                    case "/>":
                        this.WriteCloseEmptyTag();
                        break;
                    case "</":
                        this.WriteStartClosingTag();
                        break;
                    default:
                        base.Write(value);
                        break;
                }
            }

            /// <summary>
            /// Writes the start closing tag.
            /// </summary>
            private void WriteStartClosingTag()
            {
                // Add indent
                _indentLevel--;

                // Write the tabs
                this.WriteTabs();

                // Write the start tag
                base.Write("</");

            }

            /// <summary>
            /// Writes the close empty tag.
            /// </summary>
            private void WriteCloseEmptyTag()
            {
                // Write the start tag
                base.WriteLine("/>");

                // Add indent
                _indentLevel--;
            }

            /// <summary>
            /// Writes the close tag.
            /// </summary>
            private void WriteCloseTag()
            {
                // Write the start tag
                base.WriteLine(">");
            }

            /// <summary>
            /// Writes the start tag.
            /// </summary>
            private void WriteStartTag()
            {
                // Write the tabs
                this.WriteTabs();

                // Write the start tag
                base.Write("<");

                // Add indent
                _indentLevel++;
            }

            /// <summary>
            /// Writes the tabs.
            /// </summary>
            private void WriteTabs()
            {
                // Loop all indent levels
                for(int index=0; index< _indentLevel; index++)
                {
                    // Add tab
                    base.Write("\t");
                }
            }
        }

        private string _TagPrefix;
        /// <summary>
        /// Persists the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="tagPrefix">The tag prefix.</param>
        /// <param name="initialIndent">The initial indent.</param>
        /// <returns></returns>
        public static string PersistControl(UI.Control control, string tagPrefix, int initialIndent = 0)
        {
            // Get valid visual element
            IVisualTreeComponent visualElementComponent = control as IVisualTreeComponent;

            // If there is a valid valid visual element
            if(visualElementComponent != null)
            {
                // Ensure element is visible
                visualElementComponent.EnsureVisible();
            }

            StringWriter writer = new MarkupWriter(initialIndent);
            ControlPersister.PersistControl(writer, control, new VisualTreePersister(tagPrefix));
            writer.Flush();
            return writer.ToString();
        }

        /// <summary>
        /// Persists the control.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="tagPrefix">The tag prefix.</param>
        /// <param name="persistParameters">The persist parameters.</param>
        /// <param name="components">The components.</param>
        /// <returns></returns>
        public static string PersistControlToCode(VisualElement element, string tagPrefix, IDictionary<string, object> persistParameters, params VisualElement[] components)
        {
            // The component manager
            ComponentManager componentManager = new ComponentManager(persistParameters);

            // If there are components
            if (components != null)
            {
                // Loop all components
                foreach (VisualElement component in components)
                {
                    // Register the component
                    componentManager.RegisterComponent(component);
                }
            }

            // Create the web control from the 
            WebControl control = System.Web.VisualTree.WebControls.ControlUtils.GetControl(element, componentManager);

            // The page content buffer
            StringBuilder objContent = new StringBuilder();

            // Check if persisted control is a root Window View
            bool isRootWindowView = control is WindowView;

            if (isRootWindowView)
            {
                // Add page instruction
                objContent.AppendLine(string.Format(
                    "<%@ Page Language=\"{0}\" MasterPageFile=\"{1}\" Inherits=\"{2}\" %>",
                    componentManager.Language,
                    componentManager.MasterPageFile,
                    componentManager.Inherits));
            }
            else
            {
                // Add page instruction
                objContent.AppendLine(string.Format(
                    "<%@ Page Language=\"{0}\" Inherits=\"{1}\" %>",
                    componentManager.Language,
                    componentManager.Inherits));
            }
            // Add the register assembly instruction
            objContent.AppendLine(string.Format(
                "<%@ Register Assembly=\"{0}\" Namespace=\"{1}\" TagPrefix=\"{2}\" %>",
                componentManager.VTAssembly,
                componentManager.VTNamespace,
                tagPrefix));

            if (isRootWindowView)
            {
                objContent.AppendLine("<asp:Content ContentPlaceHolderID=\"Content\" runat=\"server\">");
            }
            // Get the control content
            objContent.AppendLine(PersistControl(control, tagPrefix, isRootWindowView ? 1 : 0));

            // If there are components
            if (componentManager.HasControls())
            {
                // Add the component manager code
                objContent.AppendLine(PersistControl(componentManager, tagPrefix, isRootWindowView ? 1 : 0));
            }

            // Close content control
            if (isRootWindowView)
            {
                objContent.AppendLine("</asp:Content>");
            }

            // Return content string
            return objContent.ToString();
        }


        /// <summary>
        /// Persists the control.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element">The element.</param>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="tagPrefix">The tag prefix.</param>
        public static void PersistControl<T>(T element, string folderName, string tagPrefix, params VisualElement[] components) where T : VisualElement
        {
            // The component manager
            ComponentManager componentManager = new ComponentManager();

            // If there are components
            if(components != null)
            {
                // Loop all components
                foreach(VisualElement component in components)
                {
                    // Register the component
                    componentManager.RegisterComponent(component);
                }
            }

            // Create the web control from the 
            WebControl control = System.Web.VisualTree.WebControls.ControlUtils.GetControl(element, componentManager);

            // Get the control content
            string content = PersistControl(control, tagPrefix);

            // If there are components
            if(componentManager.HasControls())
            {                
                // Add the component manager code
                content += "\n" + PersistControl(componentManager, tagPrefix);
            }

            // Create the file name
            string file = String.Concat(element.ID, ".aspx");

            // Write the file content
            File.WriteAllText(Path.Combine(folderName, file), content);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="VisualTreePersister"/> class.
        /// </summary>
        /// <param name="tagPrefix">The tag prefix.</param>
        public VisualTreePersister(string tagPrefix)
        {
            _TagPrefix = tagPrefix;
        }
        public void Activate()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Activated;

        public ComponentModel.IContainer Container
        {
            get { throw new NotImplementedException(); }
        }

        public ComponentModel.IComponent CreateComponent(Type componentClass, string name)
        {
            throw new NotImplementedException();
        }

        public ComponentModel.IComponent CreateComponent(Type componentClass)
        {
            throw new NotImplementedException();
        }

        public DesignerTransaction CreateTransaction(string description)
        {
            throw new NotImplementedException();
        }

        public DesignerTransaction CreateTransaction()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Deactivated;

        public void DestroyComponent(ComponentModel.IComponent component)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the designer instance that contains the specified component.
        /// </summary>
        /// <param name="component">The <see cref="T:System.ComponentModel.IComponent" /> to retrieve the designer for.</param>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.Design.IDesigner" />, or null if there is no designer for the specified component.
        /// </returns>
        public IDesigner GetDesigner(ComponentModel.IComponent component)
        {
            return new VisualTreeDesigner(_TagPrefix);
        }

        public Type GetType(string typeName)
        {
            throw new NotImplementedException();
        }

        public bool InTransaction
        {
            get { throw new NotImplementedException(); }
        }

        public event EventHandler LoadComplete;

        public bool Loading
        {
            get { throw new NotImplementedException(); }
        }

        ComponentModel.IComponent _RootComponent = new System.Web.UI.WebControls.PlaceHolder();
        /// <summary>
        /// Gets the instance of the base class used as the root component for the current design.
        /// </summary>
        public ComponentModel.IComponent RootComponent
        {
            get { return _RootComponent; }
        }

        public string RootComponentClassName
        {
            get { throw new NotImplementedException(); }
        }

        public event DesignerTransactionCloseEventHandler TransactionClosed;
        public event DesignerTransactionCloseEventHandler TransactionClosing;
        public event EventHandler TransactionOpened;
        public event EventHandler TransactionOpening;

        // public event DesignerTransactionCloseEventHandler TransactionClosing;

        public string TransactionDescription
        {
            get { throw new NotImplementedException(); }
        }

       // public event EventHandler TransactionOpened;

       // public event EventHandler TransactionOpening;

        public void AddService(Type serviceType, ServiceCreatorCallback callback, bool promote)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, ServiceCreatorCallback callback)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, object serviceInstance, bool promote)
        {
            throw new NotImplementedException();
        }

        public void AddService(Type serviceType, object serviceInstance)
        {
            throw new NotImplementedException();
        }

        public void RemoveService(Type serviceType, bool promote)
        {
            throw new NotImplementedException();
        }

        public void RemoveService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType">An object that specifies the type of service object to get.</param>
        /// <returns>
        /// A service object of type <paramref name="serviceType" />.-or- null if there is no service object of type <paramref name="serviceType" />.
        /// </returns>
        public object GetService(Type serviceType)
        {
            if (serviceType is IWebFormReferenceManager)
            {
                return new VisualTreeReferenceManager(_TagPrefix);
            }
            return null;
        }
    }
}
