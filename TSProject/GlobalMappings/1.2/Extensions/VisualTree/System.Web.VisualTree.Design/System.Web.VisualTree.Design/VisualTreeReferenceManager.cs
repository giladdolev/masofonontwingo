﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.Design;

namespace System.Web.VisualTree
{
    public class VisualTreeReferenceManager : WebFormsReferenceManager
    {

        private string _TagPrefix;

        public VisualTreeReferenceManager(string tagPrefix)
        {
            _TagPrefix = tagPrefix;
        }
        public override Collections.ICollection GetRegisterDirectives()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the tag prefix for the specified object type.
        /// </summary>
        /// <param name="objectType">The type of the object.</param>
        /// <returns>
        /// The tag prefix for the specified object type, if found; otherwise, null.
        /// </returns>
        public override string GetTagPrefix(Type objectType)
        {
            return _TagPrefix;
        }

        public override Type GetType(string tagPrefix, string tagName)
        {
            throw new NotImplementedException();
        }

        public override string GetUserControlPath(string tagPrefix, string tagName)
        {
            throw new NotImplementedException();
        }

        public override string RegisterTagPrefix(Type objectType)
        {
            throw new NotImplementedException();
        }
    }
}
