﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;

namespace System.Web.VisualTree
{
    public class VisualTreeReviser
    {
        /// <summary>
        /// Revises the image list.
        /// </summary>
        /// <param name="objImageListStreamer">The object image list streamer.</param>
        /// <param name="strImagePath">The string image path.</param>
        /// <returns></returns>
        public static object ReviseImageList(System.Windows.Forms.ImageListStreamer objImageListStreamer, string strImagePath)
        {
            if (objImageListStreamer == null)
            {
                return null;
            }

            // Create an image list
            System.Windows.Forms.ImageList objImageList = new System.Windows.Forms.ImageList();

            // Fill the image list
            objImageList.ImageStream = objImageListStreamer;

            // Create url reference list
            IList<UrlReference> objUrlReferenceList = new List<UrlReference>();

            // Get the image index
            int intImageIndex = 0;

            // Loop on image list
            foreach (Image objImage in objImageList.Images)
            {
                if (objImage != null)
                {
                    // The image extension
                    string strImageExtension = null;
                    
                    // The image fomat
                    System.Drawing.Imaging.ImageFormat objImageFormat = objImage.RawFormat;

                    // Get the image settings
                    GetImageSettings(ref strImageExtension, objImageFormat);

                    // Create source
                    string strSource = string.Concat(strImagePath, intImageIndex, strImageExtension);

                    // Create new url reference
                    UrlReference objUrlReference = new UrlReference(strSource);

                    // Add it to image reference list
                    objUrlReferenceList.Add(objUrlReference);

                    intImageIndex++;
                }
            }

            return objUrlReferenceList;
        }

        /// <summary>
        /// Gets the image settings.
        /// </summary>
        /// <param name="strImageExtension">The string image extension.</param>
        /// <param name="objImageFormat">The object image format.</param>
        private static void GetImageSettings(ref string strImageExtension, ImageFormat objImageFormat)
        {
            if (objImageFormat != null)
            {
                // Convert BMPs to png
                if (objImageFormat.Equals(ImageFormat.Bmp) || objImageFormat.Equals(ImageFormat.MemoryBmp))
                {
                    // Set png image extension
                    strImageExtension = ".png";
                }
                else if (objImageFormat.Equals(ImageFormat.Jpeg))
                    strImageExtension = ".jpg";

                else if (objImageFormat.Equals(ImageFormat.Gif))
                    strImageExtension = ".gif";

                else if (objImageFormat.Equals(ImageFormat.Emf))
                {
                    // Set png image extension
                    strImageExtension = ".png";
                }
                else if (objImageFormat.Equals(ImageFormat.Icon))
                {
                    // Set png image extension
                    strImageExtension = ".png";
                }
                else if (objImageFormat.Equals(ImageFormat.Png))
                    strImageExtension = ".png";

                else if (objImageFormat.Equals(ImageFormat.Wmf))
                {
                    // Set png image extension
                    strImageExtension = ".png";
                }

                else if (objImageFormat.Equals(ImageFormat.Tiff))
                    strImageExtension = ".tif";
            }

            // If could not detect image extension
            if (strImageExtension == null)
            {
                // Set png image extension
                strImageExtension = ".png";
            }
        }
    }
}
