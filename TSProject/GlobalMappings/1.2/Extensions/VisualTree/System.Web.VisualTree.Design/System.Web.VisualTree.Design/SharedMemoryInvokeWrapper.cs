﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System.ComponentModel;
using WinForms = System.Windows.Forms;

namespace System.Web.VisualTree.Design
{
    partial  class SharedMemoryInvokeWrapper
    {
        private class shareMapImplement
        {



            private IntPtr hMapFile;
            private IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
            private int MAX_SHMEMORY_SIZE = 9000000;
            private const string SHMEMORY_FILENAME = @"Global\VTSurfaceDesignerMemoryMap";


            public unsafe void SetSharedMemoryData(String htmlData, int actionViewer, ref string strError)
            {
                IntPtr pShMemBuffer = IntPtr.Zero;
                IntPtr buffer = IntPtr.Zero;
                int length = 0;
                uint error = 0;

                strError = String.Empty;

                //set htmlBuffer
                if (String.IsNullOrEmpty(htmlData) || actionViewer == 0)
                {
                    strError = "HTML empty string was generated or Action viewer value = 0";
                    return ;
                }

                // remember the byte[] is not null terminated
                byte[] strbuf = Encoding.UTF8.GetBytes(htmlData);

                // common length contented,  1- byte for terminating null C++ string and 256 bytes -the common information
                length = strbuf.Length + 1 + 256;

                if (length >= MAX_SHMEMORY_SIZE)
                {
                    strError = String.Format("HTML string length more than available shared memory size\nWhere length: {0}, Shared memory size: {1}", length, MAX_SHMEMORY_SIZE);
                    return;
                }

                //hHandle becomes non-zero
                try
                {
                    hMapFile = UnsafeNativeMethods.CreateFileMapping(INVALID_HANDLE_VALUE, IntPtr.Zero,
                                                UnsafeNativeMethods.FileMapProtection.PageReadWrite,
                                                (uint)0, (uint)MAX_SHMEMORY_SIZE, SHMEMORY_FILENAME);
                     if (hMapFile == IntPtr.Zero)
                     {
                        //error is 2 ERROR_FILE_NOT_FOUND
                        error = UnsafeNativeMethods.GetLastError();
                        strError = String.Format("Error: Create file mapping failed\n Error code: {0}", error);
                        return;
                     }


                     pShMemBuffer = UnsafeNativeMethods.MapViewOfFile(hMapFile, UnsafeNativeMethods.FileMapAccess.FileMapWrite, 0, 0, (int)MAX_SHMEMORY_SIZE);

                    if (pShMemBuffer == IntPtr.Zero)
                    {
                        strError = String.Format("Error: Write file mapping failed\n Error code: {0}", error);
                        return;
                    }

                    // .. so add one more byte for the null termination
                    buffer = Marshal.AllocHGlobal((int)length);

                    //add action value INT_ACTION_ASP_VIEWER to the buffer
                    Marshal.WriteInt32(buffer, actionViewer);

                    IntPtr newPtrBuffer = IntPtr.Add(buffer, 256);

                    Marshal.Copy(strbuf, 0, newPtrBuffer, strbuf.Length);

                    //add terminating null for C++ string
                    Marshal.WriteByte(newPtrBuffer + strbuf.Length, 0);

                    //Copies the values of num bytes from the location pointed to by source buffer to the memory block pointed to by destination hMVF.
                    UnsafeNativeMethods.MemCopy(pShMemBuffer, buffer, (uint)(length));

                    UnsafeNativeMethods.FlushViewOfFile(pShMemBuffer, length);

                }
                catch (Exception e)
                {
                    strError = String.Format("Error: Shared memory writing\n, Exception: {0}", e.Message);
                    // be sure to free the buffer if it was allocated
                    if (buffer != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(buffer);
                    }
                }
                finally
                {
                    if (pShMemBuffer != IntPtr.Zero)
                    {
                        UnsafeNativeMethods.UnmapViewOfFile(pShMemBuffer);
                    }

                    //if (hMapFile != IntPtr.Zero)
                    //{
                    //    CloseHandle(hMapFile);
                    //}

                    if (buffer != IntPtr.Zero)
                    {
                        Marshal.FreeHGlobal(buffer);
                    }
                }
            }
        }





        public unsafe void SetSharedMemoryData(String htmlData, int actionViewer, ref string strError)
        {
            shareMapImplement sharedIml = new shareMapImplement();
            try
            {
                sharedIml.SetSharedMemoryData(htmlData, actionViewer, ref strError);
            }
            catch (Exception ex)
            {
                WinForms.MessageBox.Show("SetSharedMemoryData: " + ex.Message);
            }
        }
    }
}
