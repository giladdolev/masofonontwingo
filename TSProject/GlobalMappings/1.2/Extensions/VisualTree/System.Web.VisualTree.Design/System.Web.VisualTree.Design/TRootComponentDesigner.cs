﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.WebControls;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Common;
using System.Diagnostics;

namespace System.Web.VisualTree.Design
{
    /// <summary>
    /// Provides support  for touch designer
    /// </summary>
    public class TRootComponentDesigner : RootComponentDesigner
    {
      
        /// <summary>
        /// Gets the HTML that is used to represent the control at design time.
        /// </summary>
        /// <returns>
        public override string GetDesignTimeHtml()
        {
            String strError = DesignerResourcesProcessing();

            if (!String.IsNullOrEmpty(strError))
            {
                return base.GetErrorDesignTimeHtml(new Exception(strError));
            }

            return base.GetDesignTimeHtmlApply();

 //           return base.GetDesignTimeHtmlApply();
        }

        /// <summary>
        /// Generates the HTML.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="baseDirectory">The base directory.</param>
        /// <returns></returns>
        protected override string GenerateHTML(IVisualTreeComponent component, string baseDirectory)
        { 
            StringWriter buffer = new StringWriter();

            buffer.WriteLine("<html>");
            buffer.WriteLine("<head>");

            GenerateDesignTimeHtmlScript(buffer, baseDirectory, @"Scripts\ext-visualtree.js");
            GenerateDesignTimeHtmlScript(buffer, baseDirectory, @"Scripts\ext-visualtree.mobile.js");


            foreach (string fileName in m_ResourcesLoader.ScriptList)
            {
                if (!String.IsNullOrEmpty(fileName))
                {
                    if (fileName.Contains("ext-visualtree.js") || fileName.Contains("ext-visualtree.mobile.js"))
                        continue;

                    GenerateDesignTimeHtmlScript(buffer, baseDirectory, fileName);
                }
            }


            foreach (string fileName in m_ResourcesLoader.CssResourceList)
            {
                GenerateDesignTimeHtmlStyle(buffer, baseDirectory, fileName);
            }

            buffer.WriteLine("</head>");
            buffer.WriteLine("<body>");
            WindowElement windowElement = component.Element as WindowElement;

            if(windowElement != null)
            {
                windowElement.IsTouchWindow = true;
            }

            RenderingUtils.Render(new Common.RenderingContext(RenderingEnvironment.ExtJSTouch, true), component.Element, buffer);
            buffer.WriteLine("</body>");
            buffer.WriteLine("</html>");

            // Temporary fix
            string content = buffer.ToString();
            content = content.Replace("Ext.Loader.setPath('VT.ux', 'Scripts/touch/vt');", "");
            content = content.Replace("Ext.require('VT.ux.MultiLineTabs');", "");

            File.WriteAllText(@"c:\test.htm", content);
            return content;
        }

        /// <summary>
        /// Starts the package download process.
        /// </summary>
        protected override string DesignerResourcesProcessing()
        {
            String strError = String.Empty;
            try
            {
                if (m_ResourcesLoader == null)
                {
                    m_ResourcesLoader = new ResourcesLoader(this.GetRootSite(), "Site.Mobile.Master");
                    m_ResourcesLoader.CssUrl = RootComponentDesigner.CssUrlRegex;
                }
                m_ResourcesLoader.ResourcesProcessing();
            }
            catch (Exception ex)
            {
                strError = String.Format("Mobile DesignerResourcesProcessing is failed with error: {0} ", ex.Message);
            }
            return strError;
        }

        private static ResourcesLoader m_ResourcesLoader = null;
 
    }
}
