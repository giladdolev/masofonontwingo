﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public abstract class ExtJSRendererMemberData
    {
        /// <summary>
        /// The class data
        /// </summary>
        private readonly ExtJSRendererClassData _classData;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererMemberData"/> class.
        /// </summary>
        /// <param name="classData">The class data.</param>
        public ExtJSRendererMemberData(ExtJSRendererClassData classData)
        {
            this._classData = classData;
        }

        /// <summary>
        /// Gets or sets the member description.
        /// </summary>
        /// <value>
        /// The member description.
        /// </value>
        public ExtJSMemberDescriptionAttribute MemberDescription { get; set; }

        /// <summary>
        /// Gets the name of the visual element type.
        /// </summary>
        /// <value>
        /// The name of the visual element type.
        /// </value>
        public string VisualElementTypeName
        {
            get
            {
                return this._classData.VisualElementTypeName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this member is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this member is inherited; otherwise, <c>false</c>.
        /// </value>
        public abstract bool IsInherited
        {
            get;
        }

        /// <summary>
        /// Should render the member.
        /// </summary>
        /// <returns></returns>
        public bool ShouldRenderMember()
        {
            return !this.IsInherited || this.MemberDescription != null;
        }

        /// <summary>
        /// The template command expression
        /// </summary>
        private static readonly Regex templateCommandExpression = new Regex(@"(\s)*<#(\s)*(?<command>.+)(\s)*#>(\s)*", RegexOptions.Compiled);

        
        /// <summary>
        /// Determines whether is empty code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        protected bool IsEmptyCode(string code)
        {
            return string.IsNullOrWhiteSpace(code) || string.Equals("empty", GetTemplateCommand(code), StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the template command.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        private string GetTemplateCommand(string code)
        {
            // Match command
            Match match = templateCommandExpression.Match(code);

            // If there is a valid match
            if (match.Success)
            {
                // Get the command group
                string command = match.Groups["command"].Value;

                // If there is a valid command
                if (!string.IsNullOrWhiteSpace(command))
                {
                    // Return the trimmed command
                    return command.Trim();
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the member modifiers code.
        /// </summary>
        /// <value>
        /// The member modifiers code.
        /// </value>
        public virtual string MemberModifiersCode
        {
            get
            {
                if (this.IsInherited)
                {
                    return "protected override";
                }
                else
                {
                    return "protected virtual";
                }
            }
        }

        /// <summary>
        /// Indents the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="indentation">The indentation.</param>
        internal void Indent(StringBuilder builder, int indentation)
        {
            for (int i = 0; i < indentation; i++)
            {
                builder.Append("\t");
            }
        }

    }
}
