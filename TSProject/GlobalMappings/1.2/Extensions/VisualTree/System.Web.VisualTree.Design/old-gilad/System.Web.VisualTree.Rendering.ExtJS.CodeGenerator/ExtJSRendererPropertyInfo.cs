﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererPropertyInfo
    {
        /// <summary>
        /// The property name
        /// </summary>
        private string _propertyName;

        /// <summary>
        /// The visual element type
        /// </summary>
        private readonly Type _visualElementType;

        /// <summary>
        /// The is inherited
        /// </summary>
        private readonly bool _isInherited = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererPropertyInfo"/> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="isInherited">if set to <c>true</c> [is inherited].</param>
        public ExtJSRendererPropertyInfo(Type visualElementType, string propertyName, bool isInherited)
            : this(visualElementType, propertyName)
        {
            _isInherited = isInherited;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererPropertyInfo"/> class.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        public ExtJSRendererPropertyInfo(Type visualElementType, string propertyName)
        {
            this._propertyName = propertyName;
            this._visualElementType = visualElementType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererPropertyInfo"/> class.
        /// </summary>
        /// <param name="propertyInfo">The property information.</param>
        public ExtJSRendererPropertyInfo(Type visualElementType, PropertyInfo propertyInfo)
            : this(visualElementType, propertyInfo.Name)
        {

            // If there is a valid property info
            if (propertyInfo != null)
            {
                // Set the inherited flag based on the visual element type
                _isInherited = (visualElementType != propertyInfo.DeclaringType);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererPropertyInfo"/> class.
        /// </summary>
        /// <param name="visualElementType"></param>
        public ExtJSRendererPropertyInfo(Type visualElementType)
            : this(visualElementType,string.Empty)
        {
            this._visualElementType = visualElementType;
        }


        /// <summary>
        /// Gets a value indicating whether this property is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this property is inherited; otherwise, <c>false</c>.
        /// </value>
        public bool IsInherited
        {
            get
            {
                return _isInherited;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _propertyName;
            }
        }
    }
}
