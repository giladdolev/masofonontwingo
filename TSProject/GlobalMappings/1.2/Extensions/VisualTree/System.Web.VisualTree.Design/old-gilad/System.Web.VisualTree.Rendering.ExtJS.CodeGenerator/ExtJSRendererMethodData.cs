﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererMethodData : ExtJSRendererMemberData
    {

        /// <summary>
        /// The action information
        /// </summary>
        private ExtJSRendererMethodInfo _methodInfo;

        /// <summary>
        /// The renderer action description
        /// </summary>
        private RendererMethodDescriptionAttribute _rendererActionDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererMethodData"/> class.
        /// </summary>
        /// <param name="classData">The class data.</param>
        /// <param name="methodInfo">The method information.</param>
        public ExtJSRendererMethodData(ExtJSRendererClassData classData, RendererMethodDescriptionAttribute rendererActionDescription, ExtJSRendererMethodInfo methodInfo)
            : base(classData)
        {
            this._methodInfo = methodInfo;
            this._rendererActionDescription = rendererActionDescription;
        }

        /// <summary>
        /// Gets a value indicating whether this member is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this member is inherited; otherwise, <c>false</c>.
        /// </value>
        public override bool IsInherited
        {
            get
            {
                return _methodInfo.IsInherited;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this method is static.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this method is static; otherwise, <c>false</c>.
        /// </value>
        public bool IsStatic
        {
            get { return _methodInfo.IsStatic; }
        } 


        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _methodInfo.Name;
            }
        }




        /// <summary>
        /// Should render method.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderMethod()
        {
            return base.ShouldRenderMember();
        }

        /// <summary>
        /// Should render method implementation.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderMethodImplementation()
        {
            return false;
        }

        /// <summary>
        /// Renders the static method implementation.
        /// </summary>
        /// <param name="methodDataParameterName">The method data parameter name.</param>
        /// <param name="setJavaScriptBuilderParameterName">The java script builder parameter name.</param>
        /// <returns></returns>
        internal object RenderStaticMethodImplementation(string methodDataParameterName, string setJavaScriptBuilderParameterName)
        {
            // Get method description
            ExtJSMethodDescriptionAttribute methodDescription = this.MemberDescription as ExtJSMethodDescriptionAttribute;

            // If there is a valid method description
            if (methodDescription != null && !this.IsEmptyCode(methodDescription.MethodCode))
            {
                StringBuilder propertyCode = new StringBuilder();
                propertyCode.AppendLine(string.Format("\t\t\t\tvar builder = {0}(true);", setJavaScriptBuilderParameterName));
                propertyCode.AppendLine("\t\t\t\tif (builder != null) {");
                propertyCode.AppendLine(string.Format("\t\t\t\t{0} data = ({0}){1};", methodDescription.MethodDataTypeName, methodDataParameterName));

                FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(ReplaceCallbackCommand(methodDescription.MethodCode));
                string code = formatCodeResult.Code;

                if (formatCodeResult.IsFormatted)
                {
                    propertyCode.AppendLine(string.Format("\t\t\t\tbuilder.AppendFormattedRawValue({0});", code));
                }

                propertyCode.AppendLine("\t\t\t\t}");
                return propertyCode.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Renders the method implementation.
        /// </summary>
        /// <param name="visualElementParameterName">The visual element parameter name.</param>
        /// <param name="methodDataParameterName">The method data parameter name.</param>
        /// <param name="componentVariableParameterName">The component variable parameter name.</param>
        /// <param name="setJavaScriptBuilderParameterName">The java script builder parameter name.</param>
        /// <returns></returns>
        internal object RenderMethodImplementation(string visualElementParameterName, string methodDataParameterName, string componentVariableParameterName, string setJavaScriptBuilderParameterName)
        {
            // Get method description
            ExtJSMethodDescriptionAttribute methodDescription = this.MemberDescription as ExtJSMethodDescriptionAttribute;

            // If there is a valid method description
            if (methodDescription != null && !this.IsEmptyCode(methodDescription.MethodCode))
            {
                StringBuilder propertyCode = new StringBuilder();
                propertyCode.AppendLine("\t\t\t// Get typed element");
                propertyCode.AppendLine(string.Format("\t\t\t{0} element = {1} as {0};", this.VisualElementTypeName, visualElementParameterName));
                propertyCode.AppendLine();
                propertyCode.AppendLine("\t\t\t// If there is a valid typed element");
                propertyCode.AppendLine("\t\t\tif(element != null)");
                propertyCode.AppendLine("\t\t\t{");
                propertyCode.AppendLine(string.Format("\t\t\t\tvar builder = {0}(true);", setJavaScriptBuilderParameterName));
                propertyCode.AppendLine("\t\t\t\tif (builder != null) {");
                propertyCode.AppendLine(string.Format("\t\t\t\t{0} data = ({0}){1};", methodDescription.MethodDataTypeName, methodDataParameterName));

                FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(ReplaceCallbackCommand(methodDescription.MethodCode), false, componentVariableParameterName);
                string code = formatCodeResult.Code;

                if (formatCodeResult.IsFormatted)
                {
                    propertyCode.AppendLine(string.Format("\t\t\t\t\tbuilder.AppendFormattedRawValue({0});", code));
                }

                propertyCode.AppendLine("\t\t\t\t}");
                propertyCode.AppendLine("\t\t\t}");
                return propertyCode.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// The callback data expression
        /// </summary>
        private static readonly Regex callbackDataExpression = new Regex(@"<#(\s)*callback(\s)*:(?<data>.*)(\s)*#>", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase);

        
        /// <summary>
        /// Replaces the callback command.
        /// </summary>
        /// <param name="methodCode">The method code.</param>
        /// <returns></returns>
        private string ReplaceCallbackCommand(string methodCode)
        {
            // The method code
            if(methodCode != null)
            {
                // Replace callback command with the full code.
                methodCode = callbackDataExpression.Replace(methodCode, ReplaceCallbackCommand);
            }

            return methodCode;
        }

        /// <summary>
        /// Replaces the callback command.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        private string ReplaceCallbackCommand(Match match)
        {
            return string.Format("VT_raiseEvent(<#= System.Web.VisualTree.Elements.VisualElement.GetElementId(methodInvoker) #>, 'Callback', {0});", match.Groups["data"].Value);
        }

        /// <summary>
        /// Renders the method extract callback data implementation.
        /// </summary>
        /// <param name="eventObjectParameterName">The event object parameter name.</param>
        /// <returns></returns>
        internal string RenderMethodExtractCallbackDataImplementation(string eventObjectParameterName)
        {
            // Get the method description
            ExtJSMethodDescriptionAttribute methodDescription = this.MemberDescription as ExtJSMethodDescriptionAttribute;

            // If there is a valid method description
            if (methodDescription != null)
            {
                // Return server callback data code
                if (!string.IsNullOrWhiteSpace(methodDescription.ServerCallbackDataCode))
                {
                    // Return the callback data code
                    return string.Format("return {0};", FormatDataCode(methodDescription.ServerCallbackDataCode));
                }
            }

            // Return default value
            return "return null;";
        }




        /// <summary>
        /// The arguments formatter
        /// </summary>
        private static readonly Regex DataFormatter = new Regex(@"this[.](?<param>[a-zA-Z0-9_]+)([.]To(?<action>[a-zA-Z0-9_]+)\(\))?", RegexOptions.Compiled);

        /// <summary>
        /// Formats the arguments code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        private string FormatDataCode(string code)
        {
            // If there is a valid code
            if (!string.IsNullOrWhiteSpace(code))
            {
                return DataFormatter.Replace(code, FormatDataCodeReplace);
            }

            return string.Empty;
        }

        /// <summary>
        /// Formats the arguments code replace.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        private string FormatDataCodeReplace(Match match)
        {
            string action = match.Groups["action"].Value;
            string param = match.Groups["param"].Value;

            if (string.IsNullOrEmpty(action))
            {
                action = "String";
            }
            if (string.IsNullOrEmpty(param))
            {
                param = "default";
            }

            return string.Format("eventObject.Value<System.{0}>(\"{1}\")", action, param);
        }

    }
}
