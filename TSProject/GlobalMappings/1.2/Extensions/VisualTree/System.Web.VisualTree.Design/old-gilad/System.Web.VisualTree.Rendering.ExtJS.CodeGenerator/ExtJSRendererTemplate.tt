﻿<#@ template language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<# foreach(var usingStamenent in this.ClassData.GetUsings()) { #>
using <#=usingStamenent#>;
<#}#>


namespace <#= this.ClassData.Namespace #>
{

	<#= this.ClassData.ModifiersCode #> class <#= this.ClassData.Name #> : <#= this.ClassData.Base #>
	{
<#
if(!string.IsNullOrWhiteSpace(this.ClassData.ExtJSType)) {
#>
		/// <summary>
        /// Gets the ExtJS type.
        /// </summary>
        /// <value>
        /// The ExtJS type.
        /// </value>
		public override string ExtJSType
        {
            get 
			{
				return "<#= this.ClassData.ExtJSType #>";
			}
        }


<#
}
if(!string.IsNullOrWhiteSpace(this.ClassData.XType)) {
#>
		/// <summary>
        /// Gets the xtype name.
        /// </summary>
        /// <value>
        /// The xtype name.
        /// </value>
        public override string XType
        {
            get 
			{
				return "<#= this.ClassData.XType #>";
			}
        }


<#
}

if(this.ClassData.ShouldGenerateRenderEventsOverride()) 
{
#>
		/// <summary>
        /// Renders the events.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="writer">The json writer.</param>
        protected override void RenderEvents(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter writer)
        {
			// Call the base implementation
			base.RenderEvents(context, visualElement, writer);
<# 
    foreach(var eventData in this.ClassData.GetEvents())
    {
        if (eventData.ShouldRenderAttachEvent()) 
        {
#>
			// Renders the <#=eventData.Name#> event attach code.
			RenderAttach<#=eventData.Name#>Event(context, visualElement, writer); 
<#
        }
    }
#>
		}


		/// <summary>
        /// Extracts the callback event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="serverEventName">The server event name.</param>
        /// <param name="clientEventArgs">The client event arguments.</param>
        /// <returns></returns>
        protected override EventArgs ExtractCallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject clientEventArgs)
        {
			// Choose event name
            switch (serverEventName)
            {
<#
    foreach(var eventData in this.ClassData.GetEvents()) 
    {
        if (eventData.ShouldRenderAttachEvent())
        {
#>
				case "<#=eventData.Name#>":
					return this.Extract<#=eventData.Name#>CallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
<#
        }
    }
#>
                default:
					// Call the base implementation
					return base.ExtractCallbackEventArgs(context, visualElement, serverEventName, clientEventArgs);
            }										
        }


<#
}

//render only viewConfig properties
if(this.ClassData.ShouldGenerateRenderViewConfigOverride()) {
#>
		/// <summary>
        /// Renders the viewConfig content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderViewConfigContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderViewConfigContentProperties(context, visualElement, styleWriter);

			<#= this.ClassData.RenderParentElement("visualElement") #>

<#
			foreach(var propertyData in this.ClassData.GetProperties()) 
			{
			
				if(propertyData.ShouldRenderViewConfigInitialization()) 
				{
#>			this.Render<#=propertyData.SafeName#>Property(context, visualElement, styleWriter);
<#
			
				}
			}
#>
		}


<#
}

//render only style or body style properties
if(this.ClassData.ShouldGenerateRenderStyleOverride()) {
#>
		/// <summary>
        /// Renders the style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderStyleContentProperties(context, visualElement, styleWriter);

			<#= this.ClassData.RenderParentElement("visualElement") #>

<#
			foreach(var propertyData in this.ClassData.GetProperties()) 
			{
			
				if(propertyData.ShouldRenderStyleInitialization()) 
				{
#>			this.Render<#=propertyData.SafeName#>Property(context, visualElement, styleWriter);
<#
			
				}
			}
#>
		}


<#
}
//render only field style properties
if(this.ClassData.ShouldGenerateRenderFieldStyleOverride()) {
#>
		/// <summary>
        /// Renders the field style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderFieldStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderFieldStyleContentProperties(context, visualElement, styleWriter);

			<#= this.ClassData.RenderParentElement("visualElement") #>
<#
			foreach(var propertyData in this.ClassData.GetProperties()) 
			{
				if(propertyData.ShouldRenderFieldStyleInitialization()) 
				{
#>			this.Render<#=propertyData.SafeName#>Property(context, visualElement, styleWriter);
<#
			
				}
			}
#>
        }


<#
}
//render only body style properties
if(this.ClassData.ShouldGenerateRenderBodyStyleOverride()) {
#>
		/// <summary>
        /// Renders the body style content properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderBodyStyleContentProperties(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter styleWriter)
        {
			base.RenderBodyStyleContentProperties(context, visualElement, styleWriter);

			<#= this.ClassData.RenderParentElement("visualElement") #>
<#
			foreach(var propertyData in this.ClassData.GetProperties()) 
			{
				if(propertyData.ShouldRenderBodyStyleInitialization()) 
				{
#>			this.Render<#=propertyData.SafeName#>Property(context, visualElement, styleWriter);
<#
			
				}
			}
#>
        }


<#
}
// should render property changed or style property changed 
if(this.ClassData.ShouldRenderPropertyChangeOverride() || this.ClassData.ShouldGenerateRenderStyleChangedOverride()) {
#>
		/// <summary>
        /// Renders the property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="property">The property.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderPropertyChange(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string property, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            <#= this.ClassData.RenderParentElement("visualElement") #>

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

			// Choose property
            switch (property)
            {
<#
foreach(var propertyData in this.ClassData.GetProperties()) {
if( propertyData.ShouldRenderMember() && propertyData.ShouldRenderPropertyChange() ) {
#>
				 case "<#=propertyData.Name#>":
					this.Render<#=propertyData.SafeName#>PropertyChange(renderingContext, visualElement, builder, componentVariable);
					break;
<#
}}
#>
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, property, builder, componentVariable);
                    break;
            }
        }


<#
}
if(this.ClassData.ShouldRenderPropertiesOverride()) {
#>
		/// <summary>
        /// Renders the properties.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        public override void RenderProperties(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
            // Call the base class for properties rendering
            base.RenderProperties(context, visualElement, jsonWriter);	

			// Set ExtJS rendering context
			ExtJSRenderingContext renderingContext = ExtJSRenderingContext.SetContext(context, ExtJSRenderingType.Property);	

<#
foreach(var propertyData in this.ClassData.GetProperties()) {
if(!propertyData.IsInherited && propertyData.ShouldRenderPropertyInitialization() && !propertyData.ShouldRenderStyleProperty()&& !propertyData.ShouldRenderViewConfigProperty()) {
#>
			this.Render<#=propertyData.SafeName#>Property(renderingContext, visualElement, jsonWriter);
<#
}}
#>
		}


<#
}

foreach(var eventData in this.ClassData.GetEvents()) {
if (eventData.ShouldRenderAttachEvent()) {
#>
		/// <summary>
        /// Renders the <#=eventData.Name#> event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        <#=eventData.MemberModifiersCode#> void RenderAttach<#=eventData.Name#>Event(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {			
<#
if(eventData.ShouldImplementRenderAttachMethod()) {
#> 
			// Get the typed element
			<#= this.ClassData.VisualElementTypeName #> element = visualElement as <#= this.ClassData.VisualElementTypeName #>;

			// If there is a valid element
			if(element != null)
			{
				// Check if the event should be registered
				if (this.ShouldRender<#=eventData.Name#>Event(context, element)) 
				{
					// If there is a valid JSON writer
					if(jsonWriter != null)
					{
						// The current event behavior
						var eventBehaviorType = <#= eventData.EventBehaviorType #>;

						// Revise the event behavior
						eventBehaviorType = this.Revise<#=eventData.Name#>EventBehavior(context, element, eventBehaviorType);

						// Render the client event handler
						JavaScriptBuilder javaScriptBuilder = new JavaScriptBuilder();
						// Write anonymous function
						javaScriptBuilder
							.AppendAnonymousFunction(
								<#= eventData.ClientEventParametersCode #>, 
									() => { <#= eventData.RenderRenderAttachEventImplementation("element", "javaScriptBuilder", "eventBehaviorType") #> 
									}
							);
						JavaScriptCode jsCode = new JavaScriptCode(javaScriptBuilder.ToString());

						javaScriptBuilder = new JavaScriptBuilder();
						javaScriptBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);


						jsonWriter.WriteProperty("<#= eventData.ClientEventName #>", new JavaScriptCode(javaScriptBuilder.ToString()));

					}
				}
			}
<#
}
#>
		}


		/// <summary>
        /// Revise the event behavior type of <#=eventData.Name#> event.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
		/// <param name="eventBehaviorType">The current event behavior type.</param>
        <#=eventData.MemberModifiersCode#> System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType Revise<#=eventData.Name#>EventBehavior(RenderingContext context, <#= eventData.DeclaringTypeName #> element, System.Web.VisualTree.Rendering.ExtJS.Common.ExtJSEventBehaviorType eventBehaviorType)
		{
			return eventBehaviorType;
		}


		/// <summary>
        /// Indicates if we should render the <#=eventData.Name#> event attach code.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="element">The visual element.</param>
        <#=eventData.MemberModifiersCode#> bool ShouldRender<#=eventData.Name#>Event(RenderingContext context, <#= eventData.DeclaringTypeName #> element)
		{
			<#=eventData.ShouldRenderListener()#>
		}


		/// <summary>
        /// Extracts the <#=eventData.Name#> event event arguments.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
		/// <param name="serverEventName">The server event name.</param>
        /// <param name="args">The client arguments.</param>
		<#=eventData.MemberModifiersCode#> EventArgs Extract<#=eventData.Name#>CallbackEventArgs(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, string serverEventName, Newtonsoft.Json.Linq.JObject args)
        {
			return <#= eventData.ServerEventArgsCreateCode #>;
		}


		/// <summary>
        /// Renders the <#=eventData.Name#> event add listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        <#=eventData.MemberModifiersCode#> void Render<#=eventData.Name#>AddListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {

<#
if(eventData.ShouldImplementRenderAttachMethod() && !eventData.ShouldRenderOnlyClientCode()) {
#> 
            // Get the typed element
            <#= this.ClassData.VisualElementTypeName #> element = visualElement as <#= this.ClassData.VisualElementTypeName #>;

            if(element != null && javaScriptBuilder != null && this.ShouldRender<#=eventData.Name#>Event(context, element))
            {

                // The current event behavior
                var eventBehaviorType = <#= eventData.EventBehaviorType #>;

                // Revise the event behavior
                eventBehaviorType = this.Revise<#=eventData.Name#>EventBehavior(context, element, eventBehaviorType);

                JavaScriptBuilder jsBuilder = new JavaScriptBuilder();
                // Write anonymous function
                jsBuilder
                    .AppendAnonymousFunction(
                        <#= eventData.ClientEventParametersCode #>, 
                            () => { <#= eventData.RenderRenderAttachEventImplementation("visualElement", "jsBuilder", "eventBehaviorType") #> }
                    );
                JavaScriptCode jsCode = new JavaScriptCode(jsBuilder.ToString());
                jsBuilder = new JavaScriptBuilder();
                jsBuilder.AppendInvocation(false, "VT_CreateMarkedListener", jsCode);

                javaScriptBuilder.AppendFormattedRawValue("{0}.addListener('<#= eventData.ClientEventName  #>', {1});", new JavaScriptCode(componentVariableName), new JavaScriptCode(jsBuilder.ToString()) );
                
            
               
            }
<#
}#>
        }

        /// <summary>
        /// Renders the <#=eventData.Name#> event remove listener.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        <#=eventData.MemberModifiersCode#> void Render<#=eventData.Name#>RemoveListener(RenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder javaScriptBuilder, string componentVariableName)
        {
<#
if(eventData.ShouldImplementRenderAttachMethod()) {
#> 
            // Get the typed element
            <#= this.ClassData.VisualElementTypeName #> element = visualElement as <#= this.ClassData.VisualElementTypeName #>;

            if(element != null && javaScriptBuilder != null && this.ShouldRender<#=eventData.Name#>Event(context, element))
            {
                <#= eventData.RenderRenderRemoveListenerImplementation("visualElement", "javaScriptBuilder") #>
            }
<#
}#>
        }

<#
}
}

if(this.ClassData.ShouldGenerateRenderEventsOverride()) {
#>
		/// <summary>
        /// Renders the  Add Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderAddListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            <#= this.ClassData.RenderParentElement("visualElement") #>

			// Choose event
            switch (eventName)
            {
<#
foreach(var eventData in this.ClassData.GetEvents()) {
if( eventData.ShouldRenderMember() && eventData.ShouldImplementRenderAttachMethod() && !eventData.ShouldRenderOnlyClientCode() ) {
#>
				 case "<#=eventData.Name#>":
					this.Render<#=eventData.Name#>AddListener(renderingContext, visualElement, builder, componentVariable);
					break;
<#
}}
#>
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }

		/// <summary>
        /// Renders the  Remove Listener.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="eventName">The event Name.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        protected override void RenderRemoveListener(RenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, string eventName, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
            <#= this.ClassData.RenderParentElement("visualElement") #>

			// Choose event
            switch (eventName)
            {
<#
foreach(var eventData in this.ClassData.GetEvents()) {
if( eventData.ShouldRenderMember() && eventData.ShouldImplementRenderAttachMethod() && !eventData.ShouldRenderOnlyClientCode() ) {
#>
				 case "<#=eventData.Name#>":
					this.Render<#=eventData.Name#>RemoveListener(renderingContext, visualElement, builder, componentVariable);
					break;
<#
}}
#>
                default:
                    base.RenderPropertyChange(renderingContext, visualElement, eventName, builder, componentVariable);
                    break;
            }
        }
<#
}


foreach(var propertyData in this.ClassData.GetProperties())
{
    if (propertyData.ShouldRenderPropertyInitialization())
    {
#>
		/// <summary>
        /// Renders the <#=propertyData.Name#> property.
        /// </summary>
		/// <param name="renderingContext">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="jsonWriter">The JSON writer.</param>
        <#=propertyData.MemberModifiersCode#> void Render<#=propertyData.SafeName#>Property(ExtJSRenderingContext renderingContext, System.Web.VisualTree.Elements.VisualElement visualElement, Newtonsoft.Json.JsonTextWriter jsonWriter)
        {
			<#= propertyData.RenderPropertyInitializationImplementation("visualElement", "jsonWriter") #>
		}

<#
    }
    if (propertyData.ShouldRenderPropertyChange() )
    {
#>
		/// <summary>
        /// Renders the <#=propertyData.Name#> property change.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="builder">The builder.</param>
        /// <param name="componentVariable">The component variable.</param>
        <#=propertyData.MemberModifiersCode#> void Render<#=propertyData.SafeName#>PropertyChange(ExtJSRenderingContext context, System.Web.VisualTree.Elements.VisualElement visualElement, System.Web.VisualTree.Rendering.JavaScriptBuilder builder, string componentVariable)
        {
			<#= propertyData.RenderPropertyChangeImplementation("visualElement", "builder", "componentVariable") #>
		}


<#
    }
}
if(this.ClassData.ShouldGenerateGetMethodCallbackDataOverride()) {
#>
		/// <summary>
        /// Extract the method callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        protected override object ExtractMethodCallbackData(RenderingContext context, string methodName, Newtonsoft.Json.Linq.JObject eventObject)
        {
			// Choose method
            switch (methodName)
            {
<#
foreach(var methodData in this.ClassData.GetMethods()) {
if(!methodData.IsInherited && methodData.ShouldRenderMethod()) {
#>
				case "<#=methodData.Name#>":
					return this.Extract<#=methodData.Name#>MethodCallbackData(context, eventObject);						
<#
}}
#>
                default:
                    return base.ExtractMethodCallbackData(context, methodName, eventObject);
            }
        }


<#
}

if(this.ClassData.ShouldGenerateRenderMethodsOverride()) {
#>
		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, string methodName, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
<#
foreach(var methodData in this.ClassData.GetMethods()) {
if(!methodData.IsStatic && !methodData.IsInherited && methodData.ShouldRenderMember()) {
#>
				case "<#=methodData.Name#>":
					this.Render<#=methodData.Name#>Method(context, methodInvoker, visualElement, methodData, componentVariableName, getJavaScriptBuilder);
					break;
<#
}}
#>
                default:
                    base.RenderMethod(context, methodInvoker, visualElement, methodName, methodData, componentVariableName, getJavaScriptBuilder);
                    break;
            }
        }


<#
}

foreach(var methodData in this.ClassData.GetMethods()) {
if (!methodData.IsStatic &&  methodData.ShouldRenderMethod()) {
#>
        /// Renders the <#=methodData.Name#> method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="visualElement">The visual element.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="componentVariableName">The client component variable.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        <#=methodData.MemberModifiersCode#> void Render<#=methodData.Name#>Method(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, System.Web.VisualTree.Elements.VisualElement visualElement, object methodData, string componentVariableName, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			<#= methodData.RenderMethodImplementation("visualElement", "methodData", "componentVariableName", "getJavaScriptBuilder") #>
		}


		/// <summary>
        /// Gets the method <#=methodData.Name#> callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        <#=methodData.MemberModifiersCode#> object Extract<#=methodData.Name#>MethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			<#= methodData.RenderMethodExtractCallbackDataImplementation("eventObject") #>
		}


<#
}}
#>
<#
if(this.ClassData.ShouldGenerateRenderStaticMethodsOverride()) {
#>
		/// <summary>
        /// Renders the method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodName">The method name.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        public override void RenderStaticMethod(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, string methodName, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			// Choose method
            switch (methodName)
            {
<#
foreach(var methodData in this.ClassData.GetMethods()) {
if(methodData.IsStatic && !methodData.IsInherited && methodData.ShouldRenderMember()) {
#>
				case "<#=methodData.Name#>":
					this.Render<#=methodData.Name#>Method(context, methodInvoker, methodData, getJavaScriptBuilder);
					break;
<#
}}
#>
                default:
                    base.RenderStaticMethod(context, methodInvoker, methodName, methodData, getJavaScriptBuilder);
                    break;
            }
        }


<#
}

foreach(var methodData in this.ClassData.GetMethods()) {
if (methodData.IsStatic && methodData.ShouldRenderMethod()) {
#>
        /// Renders the <#=methodData.Name#> method.
        /// </summary>
		/// <param name="context">The context.</param>
		/// <param name="methodInvoker">The method invoker.</param>
        /// <param name="methodData">The method data.</param>
        /// <param name="getJavaScriptBuilder">The get java script builder method.</param>
        <#=methodData.MemberModifiersCode#> void Render<#=methodData.Name#>Method(RenderingContext context, System.Web.VisualTree.Common.IVisualElementMethodInvoker methodInvoker, object methodData, Func<bool, System.Web.VisualTree.Rendering.JavaScriptBuilder> getJavaScriptBuilder)
        {
			<#= methodData.RenderStaticMethodImplementation("methodData", "getJavaScriptBuilder") #>
		}


		/// <summary>
        /// Gets the method <#=methodData.Name#> callback data.
        /// </summary>
		/// <param name="context">The context.</param>
        /// <param name="eventObject">The event object.</param>
        /// <returns></returns>
        <#=methodData.MemberModifiersCode#> object Extract<#=methodData.Name#>MethodCallbackData(RenderingContext context, Newtonsoft.Json.Linq.JObject eventObject)
        {
			<#= methodData.RenderMethodExtractCallbackDataImplementation("eventObject") #>
		}


<#
}}
#>
	}
}