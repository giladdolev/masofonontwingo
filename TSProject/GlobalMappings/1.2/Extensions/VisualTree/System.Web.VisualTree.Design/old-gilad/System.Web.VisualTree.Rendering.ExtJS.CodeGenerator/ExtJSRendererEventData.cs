﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererEventData : ExtJSRendererMemberData
    {
        /// <summary>
        /// The event information
        /// </summary>
        private ExtJSRendererEventInfo _eventInfo;

        /// <summary>
        /// The renderer event description
        /// </summary>
        private RendererEventDescriptionAttribute _rendererEventDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererEventData"/> class.
        /// </summary>
        /// <param name="classData">The class data.</param>
        /// <param name="eventInfo">The event information.</param>
        public ExtJSRendererEventData(ExtJSRendererClassData classData, RendererEventDescriptionAttribute rendererEventDescription, ExtJSRendererEventInfo eventInfo)
            : base(classData)
        {
            this._eventInfo = eventInfo;
            this._rendererEventDescription = rendererEventDescription;
        }

        /// <summary>
        /// Gets a value indicating whether this member is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this member is inherited; otherwise, <c>false</c>.
        /// </value>
        public override bool IsInherited
        {
            get
            {
                return _eventInfo.IsInherited;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _eventInfo.Name;
            }
        }


        /// <summary>
        /// Gets a value indicating whether supports has listeners property.
        /// </summary>
        /// <value>
        /// <c>true</c> if supports has listeners property; otherwise, <c>false</c>.
        /// </value>
        public bool SupportsHasListenersProperty
        {
            get { return _eventInfo.SupportsHasListenersProperty; }
        } 

        /// <summary>
        /// Should render attach event.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderAttachEvent()
        {
            return this.ShouldRenderMember();
        }

        /// <summary>
        /// Gets the name of the client event.
        /// </summary>
        /// <value>
        /// The name of the client event.
        /// </value>
        public string ClientEventName
        {
            get
            {
                // Get the event description
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

                // If there is a valid events description
                if(eventDescription != null)
                {
                    // Return client event name
                    return eventDescription.ClientEventName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the client event handler callback data.
        /// </summary>
        /// <value>
        /// The client event handler callback data.
        /// </value>
        public string ClientEventHandlerCallbackData
        {
            get
            {
                // Get the event description
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

                // If there is a valid events description
                if (eventDescription != null)
                {
                    // Return client event name
                    return eventDescription.ClientHandlerCallbackData;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the client event parameters code.
        /// </summary>
        /// <value>
        /// The client event parameters code.
        /// </value>
        public string ClientEventParametersCode
        {
            get
            {
                StringBuilder buffer = new StringBuilder();

                // Open new string array
                buffer.Append("new string[]{");

                // Get the event description
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

                // If there is a valid events description
                if (eventDescription != null)
                {
                    int parameterIndex = 0;

                    // Loop all parameter names
                    foreach(string parameterName in eventDescription.ClientEventHandlerParameters)
                    {
                        // If we need to add comma
                        if (parameterIndex > 0)
                        {
                            buffer.Append(", ");
                        }

                        // Start string
                        buffer.Append("\"");

                        // Write parameter name
                        if (!string.IsNullOrWhiteSpace(parameterName))
                        {
                            buffer.Append(parameterName.Trim());
                        }
                        else
                        {
                            buffer.AppendFormat("p{0}", parameterIndex);
                        }

                        // End string
                        buffer.Append("\"");

                        parameterIndex++;
                    }
                }

                // Close new string 
                buffer.Append("}");

                return buffer.ToString(); ;
            }
        }

        internal string RenderClientCode()
        {
            return null;
        }
        /// <summary>
        /// Renders the render attach event implementation.
        /// </summary>
        /// <param name="elementParameterName">The element parameter name.</param>
        /// <param name="builderParameterName">The builder parameter name.</param>
        /// <returns></returns>
        internal string RenderRenderAttachEventImplementation(string elementParameterName, string builderParameterName, string eventBehaviorTypeParameterName)
        {
            //Create stringBuilder 
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(" ");

            // Get event description
            ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

            int indentation = 10;

            AppendBlock(builder, eventDescription.ClientCode, indentation);

            if (_eventInfo.SupportsClientListener)
            {
                Indent(builder, indentation);
                builder.AppendLine("// Create the client callback code." + this.Name);
                Indent(builder, indentation);
                builder.AppendFormat("if (!String.IsNullOrEmpty(element.Client{0}))", eventDescription.Name);
                builder.AppendLine();
                Indent(builder, indentation++);
                builder.AppendLine("{");
                Indent(builder, indentation);
                builder.AppendFormat("javaScriptBuilder.AppendRawValue(element.Client{0});", eventDescription.Name);
                builder.AppendLine();
                Indent(builder, indentation);
                builder.AppendLine("javaScriptBuilder.AppendSemicolon();");
                Indent(builder, --indentation);
                builder.AppendLine("}");
            }
            Indent(builder, indentation);
            builder.AppendLine("// Create the server callback code." + this.Name);
            Indent(builder, indentation);
            builder.AppendLine();
            FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(string.Concat("{", eventDescription.ClientHandlerCallbackData, "}"));
            string code = formatCodeResult.Code;
            Indent(builder, indentation);
            if (eventDescription.ClientEventBehaviorType == ExtJSEventBehaviorType.Delayed)
            {
                builder.AppendFormat("{0}.AppendInvocationStatement(GetClientEventCallbackMethod({4}), System.Web.VisualTree.Elements.VisualElement.GetElementId({1}), \"{2}\", 100, System.Web.VisualTree.Rendering.JavaScriptCode.Format({3}));", builderParameterName, elementParameterName, this.Name, code, eventBehaviorTypeParameterName);

            }
            else
            {
                builder.AppendFormat("{0}.AppendInvocationStatement(GetClientEventCallbackMethod({4}), System.Web.VisualTree.Elements.VisualElement.GetElementId({1}), \"{2}\", System.Web.VisualTree.Rendering.JavaScriptCode.Format({3}));", builderParameterName, elementParameterName, this.Name, code, eventBehaviorTypeParameterName);
            
            }
            builder.AppendLine();
            Indent(builder, --indentation);

            return builder.ToString();
        }

        internal string EventBehaviorType
        {
            get
            {
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;
                ExtJSEventBehaviorType clientEventBehaviorType = ExtJSEventBehaviorType.Immediate;
                if (eventDescription != null)
                {
                    clientEventBehaviorType = eventDescription.ClientEventBehaviorType;
                }
                
                var type = clientEventBehaviorType.GetType();
                return string.Join(".", type.FullName, Enum.GetName(type, clientEventBehaviorType));
            }
        }

        /// <summary>
        /// Appends the block.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="content">The content.</param>
        /// <param name="indentation">The indentation.</param>
        private void AppendBlock(StringBuilder code, string content, int indentation)
        {
            if (!String.IsNullOrEmpty(content))
            {
                Indent(code, indentation);
                FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(content);
                content = formatCodeResult.Code;
                code.AppendFormat("javaScriptBuilder.AppendFormattedRaw({0});", content);
                code.AppendLine();
                Indent(code, indentation);
                code.AppendLine("javaScriptBuilder.AppendSemicolon();");
                code.AppendLine();
            }
        }

        /// <summary>
        /// Renders the render remove listener implementation.
        /// </summary>
        /// <param name="elementParameterName">Name of the parameter.</param>
        /// <param name="builderParameterName">Name of the builder parameter.</param>
        /// <returns></returns>
         internal string RenderRenderRemoveListenerImplementation(string elementParameterName, string builderParameterName)
        {
            //Create stringBuilder 
            StringBuilder builder = new StringBuilder();

            if (!this.ShouldRenderOnlyClientCode())
            {
                builder.AppendFormat("{0}.AppendInvocationStatement(\"VT_RemoveListener\", System.Web.VisualTree.Elements.VisualElement.GetElementId({1}), \"{2}\");", builderParameterName, elementParameterName, this.ClientEventName);
            }

            return builder.ToString();
        }


        /// <summary>
        /// Gets the server event arguments create code.
        /// </summary>
        /// <value>
        /// The server event arguments create code.
        /// </value>
        public string ServerEventArgsCreateCode
        {
            get
            {
                // Get the event description
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

                // If there is a valid events description
                if (eventDescription != null)
                {
                    // Return server args code
                    if(!string.IsNullOrWhiteSpace(eventDescription.ServerEventArgsCreateCode))
                    {
                        // Return the args code
                        return FormatArgsCode(eventDescription.ServerEventArgsCreateCode);
                    }
                }

                // Return default value
                return "new System.EventArgs()";
            }
        }

        /// <summary>
        /// The arguments formatter
        /// </summary>
        private static readonly Regex ArgsFormatter = new Regex(@"this[.](?<param>[a-zA-Z0-9_]+)([.]To(?<action>[a-zA-Z0-9_]+)\(\))?", RegexOptions.Compiled);

        /// <summary>
        /// Formats the arguments code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        private string FormatArgsCode(string code)
        {
            // If there is a valid code
            if (!string.IsNullOrWhiteSpace(code))
            {
                return ArgsFormatter.Replace(code, FormatArgsCodeReplace);
            }

            return string.Empty;
        }

        /// <summary>
        /// Formats the arguments code replace.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        private string FormatArgsCodeReplace(Match match)
        {
            string action = match.Groups["action"].Value;
            string param = match.Groups["param"].Value;

            if (string.IsNullOrEmpty(action))
            {
                action = "String";
            }
            if (string.IsNullOrEmpty(param))
            {
                param = "default";
            }

            return string.Format("args.Value<System.{0}>(\"{1}\")", action, param);
        }



        /// <summary>
        /// Should implement render attach method.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldImplementRenderAttachMethod()
        {
            return this.MemberDescription != null && !this.IsEmptyCode(this.ClientEventName);
        }



        /// <summary>
        /// Gets declaring type name.
        /// </summary>
        /// <value>
        /// The declaring type name.
        /// </value>
        public string DeclaringTypeName
        {
            get
            {
                return _eventInfo.DeclaringTypeName;
            }
        }

        /// <summary>
        /// check if the event description have OnlyClientCode to renderer
        /// and empty ServerEventArgsCreateCode
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderOnlyClientCode()
        {
            ExtJSEventDescriptionAttribute prop = this.MemberDescription as ExtJSEventDescriptionAttribute;

            if (prop != null)
            {
                if (!string.IsNullOrEmpty(prop.ClientCode))
                {
                    FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(string.Concat("{", prop.ClientCode, "}"));
                    return (this.MemberDescription != null && (formatCodeResult.IsRawScript || formatCodeResult.IsFormatted) );
                }
            }
            return false;
        }

        /// <summary>
        /// Should the render listener.
        /// </summary>
        /// <returns></returns>
        internal string ShouldRenderListener()
        {
                // Get the event description
                ExtJSEventDescriptionAttribute eventDescription = this.MemberDescription as ExtJSEventDescriptionAttribute;

                // If there is a valid events description
                if (eventDescription == null || !eventDescription.RenderAlways)
                {
                    StringBuilder code = new StringBuilder();

                    code.Append("return (");

                    if (_eventInfo.SupportsClientListener)
                    {
                        code.AppendFormat("!String.IsNullOrEmpty(element.Client{0})", _eventInfo.Name);
                    }
                    if (this.SupportsHasListenersProperty)
                    {
                        if (code.Length > 8)
                        {
                            code.Append(" || ");
                        }
                        code.AppendFormat("element.Has{0}Listeners", this.Name);
                    }
                    if (code.Length > 8)
                    {
                        code.Append(");");
                        return code.ToString();
                    }
                }
            return "return true;";
        }
    }
}
