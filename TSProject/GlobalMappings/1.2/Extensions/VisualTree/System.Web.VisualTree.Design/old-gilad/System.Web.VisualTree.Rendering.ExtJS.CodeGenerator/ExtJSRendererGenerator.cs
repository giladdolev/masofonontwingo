﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.CodeGenerator;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    /// <summary>
    /// Provides support for generating ExtJS renderers.
    /// </summary>
    public class ExtJSRendererGenerator : GeneratorProvider
    {
        /// <summary>
        /// Determines whether this instance can generate code given the specific metadata.
        /// </summary>
        /// <param name="metadata">The metadata.</param>
        /// <returns></returns>
        public override bool CanGenerateCode(object[] metadata)
        {

            return true;
        }

        /// <summary>
        /// Generate code given the specific metadata and class name.
        /// </summary>
        /// <param name="name">The class name.</param>
        /// <param name="metadata">The class metadata.</param>
        /// <returns></returns>
        public override string GenerateCode(List<string> fileCodeScope, string classNamespace, string className, object[] metadata)
        {
            // Create class data based on parameters
            ExtJSRendererClassData classData = new ExtJSRendererClassData(fileCodeScope, classNamespace, className, metadata);

            // Create text from class data
            return ExtJSRendererTemplate.TransformText(classData);            
        }


 
    }
}
