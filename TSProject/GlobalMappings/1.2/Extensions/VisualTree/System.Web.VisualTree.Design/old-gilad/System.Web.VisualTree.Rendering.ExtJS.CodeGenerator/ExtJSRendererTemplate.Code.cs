﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Rendering;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public partial class ExtJSRendererTemplate
    {
        /// <summary>
        /// The generator data
        /// </summary>
        private readonly ExtJSRendererClassData _classData;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererTemplate"/> class.
        /// </summary>
        /// <param name="classData">The data.</param>
        internal ExtJSRendererTemplate(ExtJSRendererClassData classData)
        {
            this._classData = classData;
        }





        internal static FormatCodeResult FormatCode(string code, bool forceFormattedCode = false, string componentVariableParameterName = null)
        {
            // Initialize the result structure
            FormatCodeResult result = new FormatCodeResult() {Code = "\"\""};

            // If there is a valid code template
            if (!string.IsNullOrWhiteSpace(code))
            {
                // The code index
                int codeIndex = 1;

                // The parameter offset
                int parameterIndex = 0;

                // The code parameters
                List<string> codeParameters = new List<string>();

                // If there is a valid component variable parameter name
                if (!string.IsNullOrEmpty(componentVariableParameterName))
                {
                    // Take care of escaped (local) this
                    string alternatedCode = code.Replace("[this]", "[thi$]");

                    if (alternatedCode.LastIndexOf("this") >= 0)
                    {
                        // TODO: use regular expression to validate we are looking on script this
                        // Replace this with the variable identifier expression
                        alternatedCode = alternatedCode.Replace("this", "<#=$THIS$#>");

                        // If alternative was returned after this replacement
                        if (!string.Equals(alternatedCode, code))
                        {
                            // Set the alternative code
                            code = alternatedCode;

                            // Add the first parameter
                            codeParameters.Add(string.Format("new JavaScriptCode({0})", componentVariableParameterName));

                            // Increment the parameter offset
                            parameterIndex += 1;
                        }
                    }
                    // Restore local this
                    code = alternatedCode.Replace("[thi$]", "this");
                }

                // The formatted code
                StringBuilder codeFormat = new StringBuilder();

                // Add string starting
                codeFormat.Append("@\"");

                // Split template code
                string[] parts = code.Split(new string[] { "<#=", "#>" }, StringSplitOptions.None);

                bool hasServerCode = false;
                bool hasJsCode = false;

                // Loop through all code parts
                foreach (string codePart in parts)
                {
                    // If is a server code and not client code
                    if (codeIndex % 2 == 0)
                    {
                        hasServerCode = true;

                        // If is the this part
                        if (string.Equals(codePart, "$THIS$", StringComparison.OrdinalIgnoreCase))
                        {
                            result.IsRawScript = false;
                            result.IsFormatted = true;

                            // Add the this placeholder
                            codeFormat.Append("{0}");
                        }
                        else
                        {
                            // Append place holder to formatted string
                            codeFormat.AppendFormat("{{{0}}}", parameterIndex);

                            // Add parameter
                            codeParameters.Add(codePart);

                            // Add to the parameter index
                            parameterIndex++;
                        }
                    }
                    else
                    {
                        if (!hasJsCode && !String.IsNullOrWhiteSpace(codePart))
                        {
                            hasJsCode = true;
                        }


                        // Create code reader
                        StringReader reader = new StringReader(codePart.Replace("\"", "\"\""));

                        // The current line
                        string line = null;

                        // The line index
                        int lineIndex = 0;

                        // Loop all lines
                        while ((line = reader.ReadLine()) != null)
                        {
                            // If we need to add line separator
                            if (lineIndex > 0)
                            {
                                // Add line separator
                                codeFormat.AppendLine();
                            }

                            // Add the line
                            codeFormat.Append(line.Replace("{", "{{").Replace("}", "}}"));

                            // Increment line index
                            lineIndex++;
                        }


                    }

                    codeIndex++;
                }

                codeFormat.Append("\"");

                // If we have both server and JS codes (or if should force formatted code)
                if ((hasServerCode && hasJsCode) || forceFormattedCode)
                {
                    result.IsFormatted = true;
                    if (codeParameters.Count > 0)
                    {
                        codeFormat.Append(", ");
                        codeFormat.Append(string.Join(", ", codeParameters.ToArray()));
                        result.Code = codeFormat.ToString();
                    }
                }
                else if (hasServerCode)  // If we only have server code
                {
                    result.IsOnlyServerCode = true;
                    result.IsFormatted = false;
                    result.IsRawScript = false;
                    if (codeParameters.Count > 0)
                    {
                        result.Code = codeParameters[0];
                    }
                }
                else if (hasJsCode) // If we only have JS code
                {
                    result.IsFormatted = false;
                    result.IsRawScript = true;
                    result.Code = codeFormat.ToString();
                }
            }

            return result;
        }




        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public ExtJSRendererClassData ClassData
        {
            get
            {
                return _classData;
            }
        }

        /// <summary>
        /// Transforms the text.
        /// </summary>
        /// <param name="classData">The class data.</param>
        /// <returns></returns>
        public static string TransformText(ExtJSRendererClassData classData)
        {
            ExtJSRendererTemplate template = new ExtJSRendererTemplate(classData);
            return template.TransformText();
        }
    }
}
