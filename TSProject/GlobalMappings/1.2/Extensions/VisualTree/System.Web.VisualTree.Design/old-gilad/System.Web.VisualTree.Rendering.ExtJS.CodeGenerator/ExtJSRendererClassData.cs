﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering.ExtJS.Common;


namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererClassData : IEnumerable<ExtJSRendererMemberData>
    {
        /// <summary>
        /// The class members
        /// </summary>
        private readonly Dictionary<string, ExtJSRendererMemberData> _members = new Dictionary<string, ExtJSRendererMemberData>();


        /// <summary>
        /// The file code scope
        /// </summary>
        private List<string> _fileCodeScope;

        /// <summary>
        /// The class namespace
        /// </summary>
        private readonly string _classNamespace;

        /// <summary>
        /// The class name
        /// </summary>
        private readonly string _className;


        /// <summary>
        /// The metadata
        /// </summary>
        private readonly object[] _metadata;


        /// <summary>
        /// The members initialized flag
        /// </summary>
        private bool _membersInitialized = false;


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererClassData"/> class.
        /// </summary>
        /// <param name="classNamespace">The class namespace.</param>
        /// <param name="className">The class name.</param>
        /// <param name="metadata">The class metadata.</param>
        public ExtJSRendererClassData(List<string> fileCodeScope, string classNamespace, string className, object[] metadata)
        {
            _fileCodeScope = fileCodeScope;
            _classNamespace = classNamespace;
            _className = className;
            _metadata = metadata;
        }

        /// <summary>
        /// Ensures the members.
        /// </summary>
        private void EnsureMembers()
        {
            // If we need to initialize members
            if (!_membersInitialized)
            {
                // Indicate members initialized
                _membersInitialized = true;

                // Get the renderer description 
                RendererDescriptionAttribute rendererDescription = this.GetMetadataInstance<RendererDescriptionAttribute>();

                // If there is a valid renderer description 
                if (rendererDescription != null)
                {

                    // Get the visual element type
                    Type visualElementType = rendererDescription.VisualElementType;

                    // If there is a valid visual element type
                    if (visualElementType != null)
                    {
                        // Loop all properties and load them
                        EnsureProperties(visualElementType, visualElementType.GetProperties(BindingFlags.Public | BindingFlags.Instance));

                        // Ensure property metadata is loaded
                        EnsurePropertyImplementations(visualElementType, this.GetMetadataInstances<ExtJSPropertyDescriptionAttribute>());

                        // Loop all events and load them
                        EnsureEvents(visualElementType, visualElementType.GetEvents(BindingFlags.Public | BindingFlags.Instance));

                        // Ensure event metadata is loaded
                        EnsureEventImplementations(visualElementType, this.GetMetadataInstances<ExtJSEventDescriptionAttribute>());

                        // Loop all methods and load them
                        EnsureMethods(visualElementType, visualElementType.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Instance));

                        // Ensure method metadata is loaded
                        EnsureMethodImplementations(visualElementType, this.GetMetadataInstances<ExtJSMethodDescriptionAttribute>());

                    }
                }

            }
        }

        /// <summary>
        /// Store all the attributes that look like [ExtJSPropertyDescription(PropertyInitializeCode = ...)]
        /// to _configMembers collection
        /// </summary>
        private void EnsureConfigProperties(Type visualElementType)
        {
            int index = 0;
            foreach (var metaDataItem in this._metadata)
            {
                if (metaDataItem != null)
                {
                    if (CheckCongifAttribute(metaDataItem))
                    {
                        //Create new ExtJSRendererPropertyInfo 
                        ExtJSRendererPropertyInfo propertyInfo = new ExtJSRendererPropertyInfo(visualElementType);
                        // The member data
                        ExtJSRendererPropertyData memberData = new ExtJSRendererPropertyData(this, null, propertyInfo);
                        // Try to get member data by the property name
                        if (memberData != null)
                        {
                            // Set the member description
                            memberData.MemberDescription = (ExtJSPropertyDescriptionAttribute)metaDataItem;
                            this._members[index.ToString()] = memberData;
                            index++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// check if the attribute look like [ExtJSPropertyDescription(PropertyInitializeCode = ...)] 
        /// </summary>
        /// <param name="metaDataItem">metadata object </param>
        /// <returns>true if the attribute look like [ExtJSPropertyDescription(PropertyInitializeCode = ...)],otherwise false </returns>
        private bool CheckCongifAttribute(object metaDataItem)
        {
            try
            {
                ExtJSPropertyDescriptionAttribute propertyDescriptionAttribute =
                    (ExtJSPropertyDescriptionAttribute)metaDataItem;
                if (propertyDescriptionAttribute != null)
                {
                    if (string.IsNullOrEmpty(propertyDescriptionAttribute.PropertyChangeCode))
                    {
                        if (!string.IsNullOrEmpty(propertyDescriptionAttribute.PropertyInitializeCode))
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Ensures the method implementations.
        /// </summary>
        /// <param name="visualElementType">The visual element type.</param>
        /// <param name="methodDescriptions">The actions list.</param>
        private void EnsureMethodImplementations(Type visualElementType, IEnumerable<ExtJSMethodDescriptionAttribute> methodDescriptions)
        {
            // Loop all method descriptions 
            foreach (ExtJSMethodDescriptionAttribute methodDescription in methodDescriptions)
            {
                // If there is a valid method description
                if (methodDescription != null)
                {
                    // Get method name
                    string methodName = methodDescription.Name;

                    // If there is a valid method name
                    if (!string.IsNullOrEmpty(methodName))
                    {

                        // The member data
                        ExtJSRendererMemberData memberData = null;

                        // Try to get member data by the method name
                        if (this._members.TryGetValue(methodName, out memberData))
                        {
                            // If there is a valid member data
                            if (memberData != null)
                            {
                                // Set the member description
                                memberData.MemberDescription = methodDescription;
                            }
                        }
                        else
                        {
                            // Create fake property
                            this._members[methodName] = memberData = new ExtJSRendererMethodData(this, null, new ExtJSRendererMethodInfo(visualElementType, methodName, false));

                            // Set member description attribute
                            memberData.MemberDescription = methodDescription;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ensures the actions.
        /// </summary>
        /// <param name="visualElementType">The visual element type.</param>
        /// <param name="methodInfos">The method information.</param>
        private void EnsureMethods(Type visualElementType, MethodInfo[] methodInfos)
        {
            // If there is a valid method information instances
            if (methodInfos != null)
            {
                // Loop all method information instances
                foreach (MethodInfo methodInfo in methodInfos)
                {
                    // Get base implementation or self
                    MethodInfo baseOrSelfMethodInfo = GetBaseOrSelfMethod(methodInfo);

                    // If there is a valid method information
                    if (baseOrSelfMethodInfo != null)
                    {
                        // Get render action description
                        RendererMethodDescriptionAttribute rendererActionDescription = methodInfo.GetCustomAttribute<RendererMethodDescriptionAttribute>();

                        // If there is a valid render action description
                        if (rendererActionDescription != null)
                        {
                            // Create action data fro given member
                            _members[methodInfo.Name] = new ExtJSRendererMethodData(this, rendererActionDescription, new ExtJSRendererMethodInfo(visualElementType, baseOrSelfMethodInfo));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the base or self method.
        /// </summary>
        /// <param name="methodInfo">The method information.</param>
        /// <returns></returns>
        private MethodInfo GetBaseOrSelfMethod(MethodInfo methodInfo)
        {
            // If there is a valid method info
            if (methodInfo != null)
            {
                // Get base definition of current method
                MethodInfo baseMethodInfo = methodInfo.GetBaseDefinition();

                // If there is a base definition
                if (baseMethodInfo != null && methodInfo != baseMethodInfo)
                {
                    // Get base or self from base definition
                    return GetBaseOrSelfMethod(baseMethodInfo);
                }
                else
                {
                    // Return method info
                    return methodInfo;
                }
            }

            return null;
        }



        /// <summary>
        /// Ensures the events.
        /// </summary>
        /// <param name="visualElementType">The visual element type.</param>
        /// <param name="eventInfos">The event info's.</param>
        private void EnsureEvents(Type visualElementType, EventInfo[] eventInfos)
        {
            // If there is a valid event information instances
            if (eventInfos != null)
            {
                // Loop all event information instances
                foreach (EventInfo eventInfo in eventInfos)
                {
                    // If there is a valid event information
                    if (eventInfo != null)
                    {
                        // Get render event description
                        RendererEventDescriptionAttribute rendererEventDescription = eventInfo.GetCustomAttribute<RendererEventDescriptionAttribute>();

                        // If there is a valid render event description
                        if (rendererEventDescription != null)
                        {
                            // Create event data fro given member
                            _members[eventInfo.Name] = new ExtJSRendererEventData(this, rendererEventDescription,
                                new ExtJSRendererEventInfo(visualElementType, eventInfo, this.SupportHasListenerProperty(eventInfo), this.SupportClientListener(eventInfo)));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Supports the has listener property.
        /// </summary>
        /// <param name="eventInfo">The event information.</param>
        /// <returns></returns>
        private bool SupportHasListenerProperty(EventInfo eventInfo)
        {
            return SupportProperty(eventInfo, "Has{0}Listeners");
        }

        /// <summary>
        /// Supports the client listener.
        /// </summary>
        /// <param name="eventInfo">The event information.</param>
        /// <returns></returns>
        private bool SupportClientListener(EventInfo eventInfo)
        {
            return SupportProperty(eventInfo, "Client{0}");
        }

        /// <summary>
        /// Supports the property.
        /// </summary>
        /// <param name="eventInfo">The event information.</param>
        /// <param name="propertyNamePattern">The property name pattern.</param>
        /// <returns></returns>
        private static bool SupportProperty(EventInfo eventInfo, string propertyNamePattern)
        {
            if (eventInfo != null)
            {
                Type declaringType = eventInfo.DeclaringType;

                if (declaringType != null)
                {
                    return declaringType.GetProperty(string.Format(propertyNamePattern, eventInfo.Name), BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance) != null;
                }
            }

            return false;
        }

        /// <summary>
        /// Ensures the event implementations.
        /// </summary>
        /// <param name="eventDescriptions">The event descriptions.</param>
        private void EnsureEventImplementations(Type visualElementType, IEnumerable<ExtJSEventDescriptionAttribute> eventDescriptions)
        {
            // Loop all event descriptions 
            foreach (ExtJSEventDescriptionAttribute eventDescription in eventDescriptions)
            {
                // If there is a valid event description
                if (eventDescription != null)
                {
                    // Get event name
                    string eventName = eventDescription.Name;

                    // If there is a valid event name
                    if (!string.IsNullOrEmpty(eventName))
                    {
                        // The member data
                        ExtJSRendererMemberData memberData = null;

                        // Try to get member data by the event name
                        if (this._members.TryGetValue(eventName, out memberData))
                        {
                            // If there is a valid member data
                            if (memberData != null)
                            {
                                // Set the member description
                                memberData.MemberDescription = eventDescription;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ensures the property implementations.
        /// </summary>
        /// <param name="propertyDescriptions">The render property descriptions.</param>
        private void EnsurePropertyImplementations(Type visualElementType, IEnumerable<ExtJSPropertyDescriptionAttribute> propertyDescriptions)
        {
            // Loop all property descriptions 
            foreach (ExtJSPropertyDescriptionAttribute propertyDescription in propertyDescriptions)
            {
                // If there is a valid property description
                if (propertyDescription != null)
                {
                    // Get property name
                    string propertyName = propertyDescription.Name;

                    // If there is a valid property name
                    if (!string.IsNullOrEmpty(propertyName))
                    {
                        // The member data
                        ExtJSRendererMemberData memberData = null;

                        // Try to get member data by the property name
                        if (this._members.TryGetValue(propertyName, out memberData))
                        {
                            // If there is a valid member data
                            if (memberData != null)
                            {
                                // Set the member description
                                memberData.MemberDescription = propertyDescription;
                            }
                        }
                        else
                        {
                            // Create fake property
                            this._members[propertyName] = memberData = new ExtJSRendererPropertyData(this, null, new ExtJSRendererPropertyInfo(visualElementType, propertyName, propertyDescription.IsInherited));

                            // Set member description attribute
                            memberData.MemberDescription = propertyDescription;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Ensures the properties.
        /// </summary>
        /// <param name="visualElementType">The visual element type.</param>
        /// <param name="propertyInfos">The property information.</param>
        private void EnsureProperties(Type visualElementType, PropertyInfo[] propertyInfos)
        {
            // If there is a valid property information instances
            if (propertyInfos != null)
            {
                // Loop all property information instances
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    // If there is a valid property information
                    if (propertyInfo != null)
                    {
                        // Get render property description
                        RendererPropertyDescriptionAttribute rendererPropertyDescription = propertyInfo.GetCustomAttribute<RendererPropertyDescriptionAttribute>();

                        // If there is a valid render property description
                        if (rendererPropertyDescription != null)
                        {
                            // Create property data fro given member
                            _members[propertyInfo.Name] = new ExtJSRendererPropertyData(this, rendererPropertyDescription, new ExtJSRendererPropertyInfo(visualElementType, propertyInfo));
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Gets or sets the class namespace.
        /// </summary>
        /// <value>
        /// The class namespace.
        /// </value>
        public string Namespace
        {
            get
            {
                return _classNamespace;
            }
        }

        /// <summary>
        /// Gets or sets the class name.
        /// </summary>
        /// <value>
        /// The class name.
        /// </value>
        public string Name
        {
            get
            {
                return _className;
            }
        }

        /// <summary>
        /// Gets the XType.
        /// </summary>
        /// <value>
        /// The XType.
        /// </value>
        public string XType
        {
            get
            {
                // Get the renderer description 
                ExtJSRendererDescriptionAttribute rendererDescription = this.GetMetadataInstance<ExtJSRendererDescriptionAttribute>();

                // If there is a valid renderer description 
                if (rendererDescription != null)
                {
                    // Return the XType
                    return rendererDescription.XType;
                }

                return "no_xtype";
            }
        }

        /// <summary>
        /// Gets the EXTJS type
        /// </summary>
        /// <value>
        /// The EXTJS type.
        /// </value>
        public string ExtJSType
        {
            get
            {
                // Get the renderer description 
                ExtJSRendererDescriptionAttribute rendererDescription = this.GetMetadataInstance<ExtJSRendererDescriptionAttribute>();

                // If there is a valid renderer description 
                if (rendererDescription != null)
                {
                    // Return the EXTJS Type
                    return rendererDescription.ExtJsType;
                }

                return "no_extjstype";
            }
        }

        /// <summary>
        /// Gets or sets the visual element.
        /// </summary>
        /// <value>
        /// The visual element.
        /// </value>
        public string VisualElementTypeName
        {
            get
            {
                // Get the renderer description 
                RendererDescriptionAttribute rendererDescription = this.GetMetadataInstance<RendererDescriptionAttribute>();

                // If there is a valid renderer description 
                if (rendererDescription != null)
                {
                    // If there is a valid visual element type
                    if (rendererDescription.VisualElementType != null)
                    {
                        // Return the full visual element type name
                        return rendererDescription.VisualElementType.FullName;
                    }
                }

                return "System.Object";
            }
        }


        /// <summary>
        /// Gets or sets the base.
        /// </summary>
        /// <value>
        /// The base.
        /// </value>
        public string Base
        {
            get
            {
                // Get the renderer description 
                RendererDescriptionAttribute rendererDescription = this.GetMetadataInstance<RendererDescriptionAttribute>();

                // If there is a valid renderer description 
                if (rendererDescription != null)
                {
                    // If there is a valid base type
                    if (!string.IsNullOrEmpty(rendererDescription.RendererBaseType))
                    {
                        // Return the full base type name
                        return rendererDescription.RendererBaseType;
                    }
                }

                return "System.Object";
            }
        }

        /// <summary>
        /// Shows debug message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void DebugShow(string message)
        {
            System.Windows.Forms.MessageBox.Show(message);
        }

        /// <summary>
        /// Gets the metadata instance.
        /// </summary>
        /// <typeparam name="TMetadataType">The metadata instance type.</typeparam>
        /// <returns></returns>
        private TMetadataType GetMetadataInstance<TMetadataType>()
        {
            return this.GetMetadataInstances<TMetadataType>().FirstOrDefault();
        }

        /// <summary>
        /// Gets the metadata instances.
        /// </summary>
        /// <typeparam name="TMetadataType">The metadata instance type.</typeparam>
        /// <returns></returns>
        private IEnumerable<TMetadataType> GetMetadataInstances<TMetadataType>()
        {
            // If there is a valid metadata
            if (_metadata != null)
            {
                // Loop all metadata instances
                foreach (object metadataInstance in _metadata)
                {
                    // If we found metadata of type
                    if (metadataInstance is TMetadataType)
                    {
                        // Return the metadata instance       
                        yield return (TMetadataType)metadataInstance;
                    }
                }
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<ExtJSRendererMemberData> GetEnumerator()
        {
            // Ensure members
            this.EnsureMembers();

            // Return all members
            return _members.Values.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            // Ensure members
            this.EnsureMembers();

            // Return all members
            return _members.Values.GetEnumerator();
        }



        /// <summary>
        /// Gets the using entries of the file processed by the renderer generator provider.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<object> GetUsings()
        {
            // If file code scope is valid
            if (_fileCodeScope != null)
            {
                // Loop through all the using statements
                foreach (string usingStatement in _fileCodeScope)
                {
                    yield return usingStatement;
                }
            }
        }



        /// <summary>
        /// Gets the events.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<ExtJSRendererEventData> GetEvents()
        {
            // Loop all members
            foreach (ExtJSRendererMemberData memberData in this)
            {
                // If is a event
                if (memberData is ExtJSRendererEventData)
                {
                    // Return event data
                    yield return (ExtJSRendererEventData)memberData;
                }
            }
        }


        /// <summary>
        /// Gets the methods.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<ExtJSRendererMethodData> GetMethods()
        {
            // Loop all members
            foreach (ExtJSRendererMemberData memberData in this)
            {
                // If is a method
                if (memberData is ExtJSRendererMethodData)
                {
                    // Return method data
                    yield return (ExtJSRendererMethodData)memberData;
                }
            }
        }


        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<ExtJSRendererPropertyData> GetProperties()
        {
            // Loop all members
            foreach (ExtJSRendererMemberData memberData in this)
            {
                // If is a property
                if (memberData is ExtJSRendererPropertyData)
                {
                    // Return property data
                    yield return (ExtJSRendererPropertyData)memberData;
                }
            }
        }

        /// <summary>
        /// Should generate render ViewConfig override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderViewConfigOverride()
        {
            // Find at ViewConfig attribute with "initialize" property
            return this.GetProperties().Any(propertyData => propertyData != null && propertyData.ShouldRenderViewConfigProperty());
        }

        /// <summary>
        /// Should generate render style override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderStyleOverride()
        {
            // Find at least 1 style or body style attribute with "initialize" property
            return this.GetProperties().Any(propertyData => propertyData != null && propertyData.ShouldRenderStyleInitialization());
        }

        /// <summary>
        /// Should generate render field style override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderFieldStyleOverride()
        {
            // Find at least 1 style attribute with "initialize" property
            return this.GetProperties().Any(propertyData => propertyData != null && propertyData.ShouldRenderFieldStyleInitialization());
        }

        /// <summary>
        /// Should generate render body style override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderBodyStyleOverride()
        {
            // Find at least 1 style attribute with "initialize" property
            return this.GetProperties().Any(propertyData => propertyData != null && propertyData.ShouldRenderBodyStyleInitialization());
        }

        /// <summary>
        /// Should generate render content properties override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderStyleChangedOverride()
        {
            // Find at least 1 style attribute with "change" property
            bool shouldGenerateRender = this.GetProperties().Any(propertyData => propertyData != null && propertyData.ShouldRenderStyleChange());

            return shouldGenerateRender;
        }

        internal string RenderParentElement(string visualElementParameterName)
        {
            StringBuilder configCode = new StringBuilder();
            configCode.AppendLine("// Get typed element");
            configCode.AppendLine(string.Format("\t\t\t{0} element = {1} as {0};", this.VisualElementTypeName, visualElementParameterName));
            configCode.AppendLine();
            configCode.AppendLine("\t\t\t// If there is a valid typed element");
            configCode.AppendLine("\t\t\tif(element == null)");
            configCode.AppendLine("\t\t\t{");
            configCode.AppendLine("\t\t\t\treturn;");
            configCode.Append("\t\t\t}");
            return configCode.ToString();
        }



        /// <summary>
        /// Should render property change override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderPropertyChangeOverride()
        {
            // Loop all properties
            foreach (ExtJSRendererPropertyData propertyData in this.GetProperties())
            {
                // Property should be updated
                if (propertyData != null && !propertyData.IsInherited && propertyData.ShouldRenderPropertyChange())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Should render add listener override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderAddListenerOverride()
        {
            // Loop all properties
            foreach (ExtJSRendererEventData eventData in this.GetEvents())
            {
                // Property should be updated
                if (eventData != null && !eventData.IsInherited && eventData.ShouldRenderAttachEvent())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Should render remove listener override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderRemoveListenerOverride()
        {
            // Loop all properties
            foreach (ExtJSRendererEventData eventData in this.GetEvents())
            {
                // Property should be updated
                if (eventData != null && !eventData.IsInherited && eventData.ShouldRenderAttachEvent())
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Should generate render methods override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderMethodsOverride()
        {

            // Loop all methods
            foreach (ExtJSRendererMethodData methodData in this.GetMethods())
            {
                // Property should be initialized
                if (methodData != null && !methodData.IsStatic && (!methodData.IsInherited || methodData.ShouldRenderMethodImplementation()))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Should generate render static methods override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderStaticMethodsOverride()
        {

            // Loop all methods
            foreach (ExtJSRendererMethodData methodData in this.GetMethods())
            {
                // Property should be initialized
                if (methodData != null && methodData.IsStatic && (!methodData.IsInherited || methodData.ShouldRenderMethodImplementation()))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Should render properties override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderPropertiesOverride()
        {
            // Loop all properties
            foreach (ExtJSRendererPropertyData propertyData in this.GetProperties())
            {
                // Property should be initialized
                if (propertyData != null && !propertyData.IsInherited && propertyData.ShouldRenderPropertyInitialization())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Should generate render events override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateRenderEventsOverride()
        {
            // Loop all events
            foreach (ExtJSRendererEventData eventData in this.GetEvents())
            {
                // Event should be attached
                if (eventData != null && !eventData.IsInherited && eventData.ShouldRenderAttachEvent())
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Should  generate abstract class.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateAbstractClass()
        {
            bool generateAbstract = false;
            generateAbstract = generateAbstract || string.IsNullOrWhiteSpace(this.ExtJSType);
            generateAbstract = generateAbstract || string.IsNullOrWhiteSpace(this.XType);
            return generateAbstract;
        }

        /// <summary>
        /// Gets the modifiers code.
        /// </summary>
        /// <value>
        /// The modifiers code.
        /// </value>
        public string ModifiersCode
        {
            get
            {
                return string.Concat("public", this.ShouldGenerateAbstractClass() ? " abstract" : "");
            }
        }

        /// <summary>
        /// Should generate the get method callback data override.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldGenerateGetMethodCallbackDataOverride()
        {
            // Loop all methods
            foreach (ExtJSRendererMethodData methodData in this.GetMethods())
            {
                // Method should be routed
                if (methodData != null && !methodData.IsInherited)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
