﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererEventInfo
    {
        /// <summary>
        /// The event name
        /// </summary>
        private readonly string _eventName;

        /// <summary>
        /// The supports has listeners property
        /// </summary>
        private readonly bool _supportsHasListenersProperty;

        /// <summary>
        /// The supports client listener
        /// </summary>
        private readonly bool _supportsClientListener;

        /// <summary>
        /// The is inherited
        /// </summary>
        private readonly bool _isInherited = false;

        /// <summary>
        /// The visual element type
        /// </summary>
        private readonly Type _visualElementType;

        /// <summary>
        /// The declaring visual element type
        /// </summary>
        private readonly Type _declaringVisualElementType;


        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererEventInfo" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="eventName">The event name.</param>
        public ExtJSRendererEventInfo(Type visualElementType, string eventName)
        {
            this._eventName = eventName;
            this._visualElementType = visualElementType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererEventInfo" /> class.
        /// </summary>
        /// <param name="visualElementType">Type of the visual element.</param>
        /// <param name="eventInfo">The event information.</param>
        /// <param name="supportsHasListenersProperty">if set to <c>true</c> supports has listeners property.</param>
        /// <param name="supportsClientListener">if set to <c>true</c> supports client listener.</param>
        public ExtJSRendererEventInfo(Type visualElementType, EventInfo eventInfo, bool supportsHasListenersProperty, bool supportsClientListener)
            : this(visualElementType, eventInfo.Name)
        {
            _supportsHasListenersProperty = supportsHasListenersProperty;
            _supportsClientListener = supportsClientListener;

            // If there is a valid event info
            if(eventInfo != null)
            {
                // Set the declaring type
                _declaringVisualElementType = eventInfo.DeclaringType;

                // Set the inherited flag based on the visual element type
                _isInherited = (visualElementType != _declaringVisualElementType);
            }
        }


        /// <summary>
        /// Gets a value indicating whether this event is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this event is inherited; otherwise, <c>false</c>.
        /// </value>
        public bool IsInherited
        {
            get
            {
                return _isInherited;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _eventName;
            }
        }

        /// <summary>
        /// Gets a value indicating whether supports has listeners property.
        /// </summary>
        /// <value>
        /// <c>true</c> if supports has listeners property; otherwise, <c>false</c>.
        /// </value>
        public bool SupportsHasListenersProperty
        {
            get { return _supportsHasListenersProperty; }
        }

        /// <summary>
        /// Gets a value indicating whether supports client listener.
        /// </summary>
        /// <value>
        ///   <c>true</c> if supports client listener; otherwise, <c>false</c>.
        /// </value>
        public bool SupportsClientListener
        {
            get { return _supportsClientListener; }
        }


        /// <summary>
        /// Gets declaring type name.
        /// </summary>
        /// <value>
        /// The declaring type name.
        /// </value>
        public string DeclaringTypeName
        {
            get
            {
                if(_declaringVisualElementType != null)
                {
                    return _declaringVisualElementType.FullName;
                }
                else if(_visualElementType != null)
                {
                    return _visualElementType.FullName;
                }
                else
                {
                    return "System.Object";
                }
            }
        }
    }
}
