﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.VisualTree.Common.Attributes;
using System.Web.VisualTree.Rendering;
using System.Web.VisualTree.Rendering.ExtJS.Common;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererPropertyData : ExtJSRendererMemberData
    {
        /// <summary>
        /// The property information
        /// </summary>
        private ExtJSRendererPropertyInfo _propertyInfo;

        /// <summary>
        /// The renderer property description
        /// </summary>
        private RendererPropertyDescriptionAttribute _rendererPropertyDescription;

        /// <summary>
        /// variable to save value type 
        /// </summary>
        private bool _haveServerSideCode;

        /// <sumsmary>
        /// Initializes a new instance of the <see cref="ExtJSRendererPropertyData"/> class.
        /// </summary>
        /// <param name="classData">The class data.</param>
        /// <param name="propertyInfo">The property information.</param>
        public ExtJSRendererPropertyData(ExtJSRendererClassData classData, RendererPropertyDescriptionAttribute rendererPropertyDescription, ExtJSRendererPropertyInfo propertyInfo)
            : base(classData)
        {
            this._propertyInfo = propertyInfo;
            this._rendererPropertyDescription = rendererPropertyDescription;
        }

        /// <summary>
        /// Gets a value indicating whether this member is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this member is inherited; otherwise, <c>false</c>.
        /// </value>
        public override bool IsInherited
        {
            get
            {
                return _propertyInfo.IsInherited;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _propertyInfo.Name;
            }
        }

        /// <summary>
        /// Gets the name of the safe.
        /// </summary>
        /// <value>
        /// The name of the safe.
        /// </value>
        public string SafeName
        {
            get
            {
                string name = this.Name;

                // If is an action name
                if (name != null && name.StartsWith("$"))
                {
                    // Return valid action name
                    return string.Concat(name.Substring(1), "Action");
                }

                return name;
            }
        }

        /// <summary>
        /// Renders the property change implementation.
        /// </summary>
        /// <param name="visualElementParameterName">The visual element parameter name.</param>
        /// <param name="builderParameterName">The builder parameter name.</param>
        /// <param name="componentVariableParameterName">The component variable parameter name.</param>
        /// <returns></returns>
        internal string RenderPropertyChangeImplementation(string visualElementParameterName, string builderParameterName, string componentVariableParameterName)
        {
            // Get property description
            ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

            // If there is a valid property description
            if (propertyDescription != null && !this.IsEmptyCode(propertyDescription.PropertyChangeCode))
            {
                StringBuilder propertyCode = new StringBuilder();
                propertyCode.AppendLine("// Get typed element");
                propertyCode.AppendLine(string.Format("\t\t\t{0} element = {1} as {0};", this.VisualElementTypeName, visualElementParameterName));
                propertyCode.AppendLine();
                propertyCode.AppendLine("\t\t\t// If there is a valid typed element");
                propertyCode.AppendLine("\t\t\tif(element != null)");
                string t = OpenBlock(propertyCode, "\t\t\t");

                FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(propertyDescription.PropertyChangeCode, false, componentVariableParameterName);

                string code = formatCodeResult.Code;

                if (formatCodeResult.IsFormatted)
                {
                    propertyCode.AppendFormat("{0}{1}.AppendFormattedRaw({2});", t, builderParameterName, code);
                    propertyCode.AppendLine();
                }

                CloseBlock(propertyCode, t);
                return propertyCode.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Renders the property initialization implementation.
        /// </summary>
        /// <param name="visualElementParameterName">The element parameter name.</param>
        /// <param name="jsonWriterParameterName">The JSON writer parameter name.</param>
        /// <returns></returns>
        internal string RenderPropertyInitializationImplementation(string visualElementParameterName, string jsonWriterParameterName)
        {
            // Get property description
            ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

            // If there is a valid property description
            if (propertyDescription != null && !this.IsEmptyCode(propertyDescription.PropertyInitializeCode))
            {
                // Get formatted parameters
                Dictionary<string, FormatCodeResult> propertyParams = FormatInitializeParams(propertyDescription.PropertyInitializeCode);
                StringBuilder propertyCode = new StringBuilder();
                int indent = 3;
                propertyCode.AppendLine("// Get typed element");
                Indent(propertyCode, indent);
                propertyCode.AppendLine(string.Format("{0} element = {1} as {0};", this.VisualElementTypeName, visualElementParameterName));
                propertyCode.AppendLine();
                Indent(propertyCode, indent);
                propertyCode.AppendLine("// If there is a valid typed element");
                Indent(propertyCode, indent);
                propertyCode.AppendLine("if(element != null)");
                string t = OpenBlock(propertyCode, "\t\t\t");

                // Loop all property parameters
                foreach (KeyValuePair<string, FormatCodeResult> propertyParamsEntry in propertyParams)
                {
                    string jsPropertyName = propertyParamsEntry.Key;
                    FormatCodeResult formatCodeResult = propertyParamsEntry.Value;
                    string code = formatCodeResult.Code;

                    ExtJSRenderingType propertyType = GetStyleType(jsPropertyName);
                    if (propertyType != ExtJSRenderingType.Property)
                    {
                        int dotIndex = jsPropertyName.IndexOf('.');
                        if (dotIndex >= 0 && dotIndex < jsPropertyName.Length - 1)
                        {
                            jsPropertyName = jsPropertyName.Substring(dotIndex + 1);
                        }
                    }



                    bool isInitStyle = (propertyType == ExtJSRenderingType.Style);
                    bool isInitFieldStyle = (propertyType == ExtJSRenderingType.FieldStyle || propertyType == ExtJSRenderingType.BodyStyle);
                    bool isInitViewConfig = (propertyType == ExtJSRenderingType.ViewConfig);

                    string propertyValidationFunction = GetPropertyValidationFunction(code);


                    t = CheckPropertyRenderingType(propertyCode, propertyType, "\t\t\t\t");
                    t = CheckPropertyDefault(propertyDescription, propertyCode, t);

                    if (!String.IsNullOrEmpty(propertyValidationFunction))
                    {
                        propertyCode.AppendFormat("{0}// If property is valid", t);
                        propertyCode.AppendLine();
                        propertyCode.AppendFormat("{0}if({1})", t, propertyValidationFunction);
                        propertyCode.AppendLine();
                        propertyCode.Append(t);
                        propertyCode.AppendLine("{");
                        t += "\t";
                    }

                    if (isInitViewConfig)
                    {
                        propertyCode.AppendFormat("{0}{1}.WritePropertyWithRawValue(\"{2}\", {3});",
                                   t, jsonWriterParameterName, jsPropertyName, JavaScriptCode.Format(code));
                    }
                    else if (isInitFieldStyle)
                    {
                        propertyCode.AppendFormat("{0}{1}.WriteRaw(String.Format(\"{2}:",
                            t, jsonWriterParameterName, jsPropertyName);
                        propertyCode.Append("{0};\"");
                        propertyCode.AppendFormat(", NormalizeStyleValue({0})));", code);
                    }
                    else
                    {
                        if (formatCodeResult.IsJavaScript)
                        {
                            propertyCode.AppendFormat("{0}{1}.WritePropertyWithRawValue(\"{2}\", new JavaScriptCode({3}));",
                                t, jsonWriterParameterName, jsPropertyName, JavaScriptCode.Format(code));
                        }
                        else if (formatCodeResult.IsRawScript)
                        {
                            if (isInitStyle && jsPropertyName.Contains('-'))
                            {

                                propertyCode.AppendFormat("{0}{1}.WritePropertyWithRawValue(\"'{2}'\", {3});",
                                   t, jsonWriterParameterName, jsPropertyName, JavaScriptCode.Format(code));
                            }
                            else
                            {
                                propertyCode.AppendFormat("{0}{1}.WritePropertyWithRawValue(\"{2}\", {3});",
                                    t, jsonWriterParameterName, jsPropertyName, JavaScriptCode.Format(code));
                            }
                        }
                        else
                        {
                            propertyCode.AppendFormat("{0}{1}.WriteProperty(\"{2}\", {3});", t, jsonWriterParameterName, jsPropertyName, FormatCode(code));
                        }
                    }
                    propertyCode.AppendLine();

                    t = CloseBlock(propertyCode, propertyValidationFunction, t);
                    t = CloseBlock(propertyCode, propertyDescription.ClientNotDefaultCode, t);
                    t = CloseBlock(propertyCode, t);
                }

                CloseBlock(propertyCode, t);
                return propertyCode.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Formats the code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        private string FormatCode(string code)
        {
            if (code.Contains("{{"))
            {
                return String.Format("new JavaScriptCode(String.Format({0}))", code);
            }

            return code;
        }
        /// <summary>
        /// Closes the block.
        /// </summary>
        /// <param name="propertyCode">The property code.</param>
        /// <param name="condition">The condition.</param>
        /// <param name="indentation">The indentation.</param>
        /// <returns></returns>
        private static string CloseBlock(StringBuilder propertyCode, string condition, string indentation)
        {
            if (!String.IsNullOrEmpty(condition))
            {
                indentation = CloseBlock(propertyCode, indentation);
            }
            return indentation;
        }

        /// <summary>
        /// Closes the block.
        /// </summary>
        /// <param name="propertyCode">The property code.</param>
        /// <param name="indentation">The indentation.</param>
        /// <returns></returns>
        private static string CloseBlock(StringBuilder propertyCode, string indentation)
        {
            if (indentation.Length >= 1)
            {
                indentation = indentation.Substring(1);
            }
            propertyCode.Append(indentation);
            propertyCode.AppendLine("}");
            return indentation;
        }

        /// <summary>
        /// Gets the property validation function.
        /// </summary>
        /// <param name="visualElementPropertyName">Name of the visual element property.</param>
        /// <returns></returns>
        private string GetPropertyValidationFunction(string visualElementPropertyName)
        {
            // Get the visual element type
            try
            {
                Type visualElementType = Type.GetType(VisualElementTypeName);


                if (visualElementType != null)
                {
                    // Get the property from the type of the visual element 
                    PropertyInfo visualElementPropery = visualElementType.GetProperty(Name);

                    if (visualElementPropery != null)
                    {
                        // Get the type of the property
                        Type visualElementProperyType = visualElementPropery.PropertyType;

                        // Get the name of the type of the property
                        string visualElementProperyTypeName = visualElementProperyType.Name ?? string.Empty;

                        // If property is array or is string or is list or object then return the function for validation
                        if (visualElementProperyType.IsArray ||
                            visualElementProperyTypeName.Equals("String") ||
                            visualElementProperyTypeName.Equals("Object") ||
                            typeof(ICollection).IsAssignableFrom(visualElementProperyType))
                        {
                            return string.Format("ValidateProperty(element.{0})", Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        /// <summary>
        /// Formats the initialize parameters.
        /// </summary>
        /// <param name="code">The initialize code.</param>
        /// <remarks>Sample initialization code: "checked = <#=element.IsChecked#> + Math.Min(3,2, {d:2,g:2}), showCheckbox = 'true = 2' "</remarks>
        /// <returns></returns>
        private Dictionary<string, FormatCodeResult> FormatInitializeParams(string code)
        {
            // The initialize parameters
            Dictionary<string, FormatCodeResult> initializeParams = new Dictionary<string, FormatCodeResult>();

            // If there is a valid code template
            if (!string.IsNullOrWhiteSpace(code))
            {
                bool isInDoubleQuotation = false;
                bool isInSingleQuotation = false;
                int nestingLevel = 0;

                int lastIndex = 0;

                string name = null;

                for (int index = 0; index < code.Length; index++)
                {
                    if (isInSingleQuotation)
                    {
                        if (code[index] == '\'')
                        {
                            isInSingleQuotation = false;
                        }
                    }
                    else if (isInDoubleQuotation)
                    {
                        if (code[index] == '"')
                        {
                            isInDoubleQuotation = false;
                        }
                    }
                    else
                    {
                        switch (code[index])
                        {
                            case '\'':
                                isInSingleQuotation = true;
                                break;
                            case '"':
                                isInDoubleQuotation = true;
                                break;
                            case '(':
                            case '<':
                            case '[':
                            case '{':
                                nestingLevel++;
                                break;
                            case ')':
                            case '>':
                            case ']':
                            case '}':
                                nestingLevel--;
                                break;
                            case ',':
                                if (nestingLevel == 0)
                                {
                                    if (!string.IsNullOrEmpty(name))
                                    {
                                        FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(code.Substring(lastIndex, index - lastIndex).Trim());
                                        initializeParams[name] = formatCodeResult;
                                        lastIndex = index + 1;
                                    }
                                }
                                break;
                            case '=':
                                if (nestingLevel == 0)
                                {
                                    name = code.Substring(lastIndex, index - lastIndex).Trim();

                                    lastIndex = index + 1;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(name))
                {
                    FormatCodeResult formatCodeResult = ExtJSRendererTemplate.FormatCode(code.Substring(lastIndex, code.Length - lastIndex).Trim());

                    if (formatCodeResult.IsFormatted || formatCodeResult.IsOnlyServerCode || lastIndex > 0)
                    {
                        _haveServerSideCode = formatCodeResult.IsOnlyServerCode;
                    }
                    else
                    {
                        formatCodeResult.IsJavaScript = true;
                    }
                    initializeParams[name] = formatCodeResult;
                }
            }

            return initializeParams;
        }


        /// <summary>
        /// Should render property initialization.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderPropertyInitialization()
        {
            if (!this.IsInherited)
            {
                return true;
            }
            ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

            if (propertyDescription != null)
            {
                if (!this.ShouldRenderStyleProperty())
                {
                    return true;
                }
                return !string.IsNullOrWhiteSpace(propertyDescription.PropertyInitializeCode);
            }
            return false;
        }

        /// <summary>
        /// Should render property change.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderPropertyChange()
        {
            if (!this.IsInherited)
            {
                return true;
            }

            if (this.ShouldRenderMember())
            {
                ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

                if (propertyDescription != null)
                {
                    return !string.IsNullOrWhiteSpace(propertyDescription.PropertyChangeCode);
                }
            }
            return false;
        }

        #region Style

        /// <summary>
        /// should render style Initialization 
        /// only if the style is a style or body style 
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderStyleInitialization()
        {
            bool shouldRenderStyleInitialization = false;
            if (ShouldRenderStyleProperty())
            {
                // Get property description
                ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;
                if (propertyDescription != null)
                    shouldRenderStyleInitialization = CheckIfIsStyle(propertyDescription.PropertyInitializeCode, "style.");
            }
            return shouldRenderStyleInitialization;
        }


        /// <summary>
        /// check if the property is a  style 
        /// </summary>
        /// <param name="propertyInitializeCode"></param>
        /// <returns></returns>
        private bool CheckIfIsStyle(string propertyInitializeCode, params string[] stylesType)
        {
            bool isStyleConfig = false;
            if (!string.IsNullOrEmpty(propertyInitializeCode))
            {
                isStyleConfig = StartWith(propertyInitializeCode, stylesType);
            }
            return isStyleConfig;
        }


        /// <summary>
        /// Check if the property InitializeCode start with ExtJSRenderingType.
        /// </summary>
        /// <param name="propertyInitializeCode"></param>
        /// <param name="stylesType"></param>
        /// <returns></returns>
        private bool StartWith(string propertyInitializeCode, params string[] stylesType)
        {
            propertyInitializeCode = propertyInitializeCode.ToLower().Trim();
            foreach (string item in stylesType)
            {
                if (propertyInitializeCode.StartsWith(item.ToLowerInvariant()))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks the type of the property rendering.
        /// </summary>
        /// <param name="propertyCode">The property code.</param>
        /// <param name="renderingType">Type of the rendering.</param>
        /// <param name="indentation">The indentation.</param>
        /// <returns></returns>
        private static string CheckPropertyRenderingType(StringBuilder propertyCode, ExtJSRenderingType renderingType, string indentation)
        {
            propertyCode.AppendFormat("{0}// Check rendering type", indentation);
            propertyCode.AppendLine();
            propertyCode.AppendFormat("{0}if (renderingContext.RenderingType == ExtJSRenderingType.{1})", indentation, renderingType);
            propertyCode.AppendLine();
            indentation = OpenBlock(propertyCode, indentation);
            return indentation;
        }

        /// <summary>
        /// Checks the property default.
        /// </summary>
        /// <param name="propertyDescription">The style description.</param>
        /// <param name="propertyCode">The property code.</param>
        /// <param name="indentation">The indentation.</param>
        /// <returns></returns>
        private static string CheckPropertyDefault(ExtJSPropertyDescriptionAttribute propertyDescription, StringBuilder propertyCode, string indentation)
        {
            if (!String.IsNullOrEmpty(propertyDescription.ClientNotDefaultCode))
            {
                propertyCode.AppendFormat("{0}// If {1} value is not default", indentation, propertyDescription.Name);
                propertyCode.AppendLine();
                propertyCode.AppendFormat("{0}if ({1})", indentation, propertyDescription.ClientNotDefaultCode);
                propertyCode.AppendLine();
                indentation = OpenBlock(propertyCode, indentation);
            }
            return indentation;
        }

        /// <summary>
        /// Opens the block.
        /// </summary>
        /// <param name="propertyCode">The property code.</param>
        /// <param name="indentation">The indentation.</param>
        /// <returns></returns>
        private static string OpenBlock(StringBuilder propertyCode, string indentation)
        {
            propertyCode.Append(indentation);
            propertyCode.AppendLine("{");
            indentation += "\t";
            return indentation;
        }

        /// <summary>
        /// Get the style type
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        private ExtJSRenderingType GetStyleType(string propertyName)
        {
            ExtJSRenderingType styletype = ExtJSRenderingType.Property;
            if (!string.IsNullOrEmpty(propertyName))
            {
                if (StartWith(propertyName, "style."))
                    styletype = ExtJSRenderingType.Style;
                else if (StartWith(propertyName, "fieldstyle."))
                    styletype = ExtJSRenderingType.FieldStyle;
                else if (StartWith(propertyName, "bodystyle."))
                    styletype = ExtJSRenderingType.BodyStyle;
                else if (StartWith(propertyName, "viewconfig."))
                    styletype = ExtJSRenderingType.ViewConfig;
            }
            return styletype;
        }

        /// <summary>
        /// Should render style property change.
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderStyleChange()
        {
            // Get property description
            ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;
            if (propertyDescription != null)
                if (CheckIfIsStyle(propertyDescription.PropertyInitializeCode, "style.", "bodystyle.", "fieldstyle."))
                {
                    return !string.IsNullOrEmpty(propertyDescription.PropertyChangeCode);
                }
            return false;
        }

        /// <summary>
        /// should render style property 
        /// </summary>
        /// <returns></returns>
        public bool ShouldRenderStyleProperty()
        {
            bool isStyle = false;
            // Get property description
            ExtJSPropertyDescriptionAttribute styleDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

            if (styleDescription == null)
            {
                return isStyle;
            }
            ExtJSRenderingType styleType = GetStyleType(styleDescription.PropertyInitializeCode);
            isStyle = (styleType == ExtJSRenderingType.BodyStyle || styleType == ExtJSRenderingType.Style || styleType == ExtJSRenderingType.FieldStyle);
            return isStyle;
        }

        /// <summary>
        /// should render field style initialization
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderFieldStyleInitialization()
        {
            bool shouldRenderStyleInitialization = false;
            if (ShouldRenderStyleProperty())
            {
                // Get property description
                ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;
                if (propertyDescription != null)
                    shouldRenderStyleInitialization = CheckIfIsStyle(propertyDescription.PropertyInitializeCode, "fieldstyle.");
            }
            return shouldRenderStyleInitialization;
        }

        internal bool ShouldRenderBodyStyleInitialization()
        {
            bool shouldRenderStyleInitialization = false;
            if (ShouldRenderStyleProperty())
            {
                // Get property description
                ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;
                if (propertyDescription != null)
                    shouldRenderStyleInitialization = CheckIfIsStyle(propertyDescription.PropertyInitializeCode, "bodystyle.");
            }
            return shouldRenderStyleInitialization;
        }

        #endregion


        #region ViewConfig

        /// should render ViewConfig Initialization      
        /// </summary>
        /// <returns></returns>
        internal bool ShouldRenderViewConfigInitialization()
        {
            bool shouldRenderViewConfigInitialization = false;
            if (ShouldRenderViewConfigProperty())
            {
                // Get property description
                ExtJSPropertyDescriptionAttribute propertyDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;
                if (propertyDescription != null)
                    shouldRenderViewConfigInitialization = CheckIfIsViewConfig(propertyDescription.PropertyInitializeCode, "ViewConfig.");
            }
            return shouldRenderViewConfigInitialization;
        }


        /// <summary>
        /// check if the property is a  ViewConfig .  
        /// </summary>
        /// <param name="propertyInitializeCode"></param>
        /// <returns></returns>
        private bool CheckIfIsViewConfig(string propertyInitializeCode, params string[] viewConfigType)
        {
            bool isViewConfigConfig = false;
            if (!string.IsNullOrEmpty(propertyInitializeCode))
            {
                isViewConfigConfig = StartWith(propertyInitializeCode, viewConfigType);
            }
            return isViewConfigConfig;
        }



        /// <summary>
        /// should render ViewConfig property .
        /// </summary>
        /// <returns></returns>
        public bool ShouldRenderViewConfigProperty()
        {
            bool isViewConfig = false;
            // Get property description
            ExtJSPropertyDescriptionAttribute viewConfigDescription = this.MemberDescription as ExtJSPropertyDescriptionAttribute;

            if (viewConfigDescription == null)
            {
                return isViewConfig;
            }
            ExtJSRenderingType viewConfigType = GetViewConfigType(viewConfigDescription.PropertyInitializeCode);
            isViewConfig = (viewConfigType == ExtJSRenderingType.ViewConfig);
            return isViewConfig;
        }


        /// <summary>
        /// Get the ViewConfig type .
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        private ExtJSRenderingType GetViewConfigType(string propertyName)
        {
            return GetStyleType(propertyName);
        }


        #endregion


    }
}

