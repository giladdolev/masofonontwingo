﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    internal struct FormatCodeResult
    {
        public string Code;
        public bool IsFormatted;
        public bool IsRawScript;
        public bool IsOnlyServerCode;
        public bool IsJavaScript;

    }
}
