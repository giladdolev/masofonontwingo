﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.VisualTree.CodeGenerator.ExtJS
{
    public class ExtJSRendererMethodInfo
    {
        /// <summary>
        /// The method name
        /// </summary>
        private string _methodName;

        /// <summary>
        /// The visual element type
        /// </summary>
        private readonly Type _visualElementType;

        /// <summary>
        /// The is inherited
        /// </summary>
        private readonly bool _isInherited = false;

        /// <summary>
        /// The is static method flag
        /// </summary>
        private readonly bool _isStatic = false;



        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererMethodInfo"/> class.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        public ExtJSRendererMethodInfo(Type visualElementType, string methodName, bool isStatic)
        {
            this._methodName = methodName;
            this._visualElementType = visualElementType;
            this._isStatic = isStatic;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtJSRendererMethodInfo"/> class.
        /// </summary>
        /// <param name="methodInfo">The method information.</param>
        public ExtJSRendererMethodInfo(Type visualElementType, MethodInfo methodInfo)
            : this(visualElementType, methodInfo.Name, methodInfo.IsStatic)
        {

            // If there is a valid action info
            if (methodInfo != null)
            {
                // Set the inherited flag based on the visual element type
                _isInherited = (visualElementType != methodInfo.DeclaringType);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this action is inherited.
        /// </summary>
        /// <value>
        /// <c>true</c> if this action is inherited; otherwise, <c>false</c>.
        /// </value>
        public bool IsInherited
        {
            get
            {
                return _isInherited;
            }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return _methodName;
            }
        }


        /// <summary>
        /// Gets a value indicating whether this method is static.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this method is static; otherwise, <c>false</c>.
        /// </value>
        public bool IsStatic
        {
            get { return _isStatic; }
        } 

    }
}
