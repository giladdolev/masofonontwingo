using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.WebPages;

namespace Mesofon.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("WP")
            {
                ContextCondition = (ctx => ctx.GetOverriddenUserAgent()
                .IndexOf("Windows Phone OS", StringComparison.OrdinalIgnoreCase) > 0)
            });

            //DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("iPhone")
            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("Android")
            {
                ContextCondition = (ctx => ctx.GetOverriddenUserAgent()
                .IndexOf("iPhone", StringComparison.OrdinalIgnoreCase) > 0)
            });

            DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode("Android")
            {
                ContextCondition = (ctx => ctx.GetOverriddenUserAgent()
                .IndexOf("Android", StringComparison.OrdinalIgnoreCase) > 0)
            });


            // Set up a simple configuration that logs on the console.
            log4net.Config.XmlConfigurator.Configure();

        }


        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            //Only access session state if it is available
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                //If we are authenticated AND we dont have a session here.. redirect to login page.
                HttpCookie authenticationCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authenticationCookie != null)
                {
                    FormsAuthenticationTicket authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);
                    if (!authenticationTicket.Expired)
                    {
                        //check if session is invalid.
                        if (Session["Common.Globals.UserRegistryItems"] == null)
                        {
                            //This means for some reason the session expired before the authentication ticket. Force a login.
                            FormsAuthentication.SignOut();
                            var redirectTo = string.Format("{0}?{1}", FormsAuthentication.LoginUrl, MvcSite.Common.Globals.QueryStringState);
                            Response.Redirect(redirectTo, true);
                            return;
                        }
                    }
                }
            }
        }

        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        //Let us set the Pricipal with our user specific details
                        e.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                var ctx = HttpContext.Current;
                StringBuilder sb = new StringBuilder();
                sb.Append(ctx.Request.Url.ToString() + System.Environment.NewLine);
                sb.Append("Source:" + System.Environment.NewLine + ctx.Server.GetLastError().Source.ToString());
                sb.Append("Message:" + System.Environment.NewLine + ctx.Server.GetLastError().Message.ToString());
                sb.Append("Stack Trace:" + System.Environment.NewLine + ctx.Server.GetLastError().StackTrace.ToString());

                MvcSite.Common.Globals.log.DebugFormat(sb.ToString());
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Error in Application_Error Event" + System.Environment.NewLine);
                sb.Append("Source:" + System.Environment.NewLine + ex.Source.ToString());
                sb.Append("Message:" + System.Environment.NewLine + ex.Message.ToString());
                sb.Append("Stack Trace:" + System.Environment.NewLine + ex.StackTrace.ToString());
                MvcSite.Common.Globals.log.DebugFormat(sb.ToString());
            }
        }

    }
}
