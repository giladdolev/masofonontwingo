using System.Web.Mvc;
using System.Web.Routing;

namespace Mesofon.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("GetPictureHandler");
            routes.IgnoreRoute("ResourceHttpHandler");
            routes.MapPageRoute("HttpCombiner", "HttpCombinerURL", "~/Common/HttpCombiner.ashx");
            routes.MapPageRoute("PDFDownload", "PDFDownloadURL", "~/Common/PDFDownload.ashx");
            routes.MapRoute(
                name: "Terminal",
                url: "Terminal/",
                defaults: new { controller = "w_mini_terminal", action = "w_mini_terminal", id = UrlParameter.Optional },
                namespaces: new[] { "" }
            ).DataTokens.Add("area", "mini_terminal_mini_terminal");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional },
                namespaces: new[] { "" }
            ).DataTokens.Add("area", "masofon_masofon");

        }
    }
}
