﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.w_mini_terminal_msg>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" LoadAction="w_mini_terminal_msg\Form_Load" ID="w_mini_terminal_msg" BackColor="ButtonFace" Height="185px" Width="275px" CssClass="w_mini_terminal_msgClass">
        <vt:GroupBox runat="server" Top="24px" Left="1px" Visible="False" ID="gb_message" ForeColor="WindowText" Height="101px" TabIndex="30" Width="268px">
        </vt:GroupBox>
        <vt:TextBox runat="server" Multiline="true" PasswordChar="" MaxLength="32767" ReadOnly="True" Top="44px" Left="1px" ID="mle_message" BackColor="ButtonFace" ForeColor="WindowText" Height="75px" TabIndex="20" Width="268px">
        </vt:TextBox>
        <vt:Image runat="server" Top="5px" Left="4px" ID="p_icon" Height="17px" Width="19px">
        </vt:Image>
        <vt:Button runat="server" Text="ביטול" Top="122px" Left="139px" ID="cb_cancel" ClickAction="w_mini_terminal_msg\cb_cancel_clicked" Height="25px" TabIndex="20" Width="66px"></vt:Button>
        <vt:Button runat="server" Text="אישור" Top="122px" Left="62px" ID="cb_ok" ClickAction="w_mini_terminal_msg\cb_ok_clicked" Height="25px" TabIndex="20" Width="66px"></vt:Button>
        <vt:Button runat="server" TextAlign="TopLeft" Tag="NoMove" Text="More >>" Top="8px" Left="209px" ID="st_more" ClickAction="w_mini_terminal_msg\st_more_clicked" BackColor="ButtonFace" BorderStyle="None" ForeColor="Black" Font-Names="Arial" Font-Size="11pt" Height="26px" Width="73px">
        </vt:Button>
        <vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Visible="false" ReadOnly="True" Top="33px" Left="1px" ID="mle_error" BackColor="ButtonFace" ForeColor="WindowText" Height="66px" TabIndex="20" Width="268px">
        </vt:TextBox>
    </vt:WindowView>

</asp:Content>
