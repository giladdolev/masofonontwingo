<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_number_by_branch_dddwRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="35px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך&quot;" TemplateText="&quot;תאריך&quot;" Visible="True" Left="295px" Top="1px" Alignment="Center" ID="date_move_t" BorderStyle="None" Enabled="False" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="33px" Width="152px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר סניף&quot;" TemplateText="&quot;מספר סניף&quot;" Visible="True" Left="172px" Top="1px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Enabled="False" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="33px" Width="121px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר החזרה&quot;" TemplateText="&quot;מספר החזרה&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="33px" Width="168px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר ספק&quot;" TemplateText="&quot;מספר ספק&quot;" Visible="True" Left="449px" Top="1px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Enabled="False" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="33px" Width="115px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="449px" Top="0px" Alignment="Center" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" Height="33px" Width="115px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="172px" Top="0px" Alignment="Center" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" Height="33px" Width="121px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" Height="33px" Width="168px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="296px" Top="0px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" Height="33px" Width="151px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

