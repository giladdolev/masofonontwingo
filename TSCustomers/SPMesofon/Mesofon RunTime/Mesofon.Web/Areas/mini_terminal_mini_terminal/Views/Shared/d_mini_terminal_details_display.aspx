<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_details_displayRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="32px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewDropDownField runat="server" Visible="True" Left="1px" Top="16px" Alignment="Center" ID="doc_no" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="56px"></vt:DataViewDropDownField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="126px" Top="1px" Alignment="Center" ID="barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="80px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_name" Visible="True" Left="2px" Top="1px" ID="material_name" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="123px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;מ&quot;" TemplateText="&quot;מ&quot;" Visible="True" Left="198px" Top="16px" Alignment="Center" ID="expected_material_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="8px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" Format="####" DataMember="expected_material_quantity" Visible="True" Left="164px" Top="16px" Alignment="Center" ID="expected_material_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="33px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;ה&quot;" TemplateText="&quot;ה&quot;" Visible="True" Left="155px" Top="16px" Alignment="Center" ID="order_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="8px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="119px" Top="16px" Alignment="Center" ID="order_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="35px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;ח&quot;" TemplateText="&quot;ח&quot;" Visible="True" Left="110px" Top="16px" Alignment="Center" ID="invoice_details_invoice_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="8px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" Format="####" DataMember="invoice_pack_quantity" Visible="True" Left="74px" Top="16px" Alignment="Center" ID="invoice_pack_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="35px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="59px" Top="16px" Alignment="Center" ID="doc_no_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="14px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="row_no" Visible="True" Left="208px" Top="1px" Alignment="Center" ID="row_no" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="20px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doc_no_en" Left="46px" Top="17px" Alignment="Right" ID="doc_no_en" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="12px" Width="10px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

