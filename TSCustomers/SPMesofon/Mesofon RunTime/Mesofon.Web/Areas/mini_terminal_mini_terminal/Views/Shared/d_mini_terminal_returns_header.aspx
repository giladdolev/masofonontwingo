<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_returns_headerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="53px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מפיץ&quot;" TemplateText="&quot;מפיץ&quot;" Visible="True" Left="82px" Top="2px" Alignment="Right" ID="distributor_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="24px" Width="32px"></vt:DataViewLabelField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="3px" Top="2px" ID="distributor" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="24px" Width="77px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="3px" Top="29px" ID="supplier" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="24px" Width="77px"></vt:DataViewDropDownField>
<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="82px" Top="29px" Alignment="Right" ID="supplier_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="24px" Width="32px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

