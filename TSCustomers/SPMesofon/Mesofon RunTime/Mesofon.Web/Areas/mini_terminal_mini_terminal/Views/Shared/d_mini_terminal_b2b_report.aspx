<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_b2b_reportRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="153px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="16px" Top="5px" ID="date" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="66px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;דוח קליטת מסופון&quot;" TemplateText="&quot;דוח קליטת מסופון&quot;" Visible="True" Left="386px" Top="17px" Alignment="Center" ID="t_title" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="149px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:מס סניף&quot;" TemplateText="&quot;:מס סניף&quot;" Visible="True" Left="928px" Top="50px" Alignment="Right" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:ספק&quot;" TemplateText="&quot;:ספק&quot;" Visible="True" Left="928px" Top="79px" Alignment="Right" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם ספק&quot;" TemplateText="&quot;:שם ספק&quot;" Visible="True" Left="821px" Top="79px" Alignment="Right" ID="supplier_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם סניף&quot;" TemplateText="&quot;:שם סניף&quot;" Visible="True" Left="821px" Top="50px" Alignment="Right" ID="branch_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם עובד&quot;" TemplateText="&quot;:שם עובד&quot;" Visible="True" Left="582px" Top="50px" Alignment="Right" ID="employee_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="62px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="874px" Top="50px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="51px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="874px" Top="79px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="51px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;text&quot;" TemplateText="&quot;text&quot;" Visible="True" Left="10px" Top="167px" ID="t_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="22px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:מס הזמנה&quot;" TemplateText="&quot;:מס הזמנה&quot;" Visible="True" Left="582px" Top="79px" Alignment="Right" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="62px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="489px" Top="79px" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_name" Visible="True" Left="493px" Top="50px" Alignment="Right" ID="employee_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="86px"></vt:DataViewTextField>
<vt:DataViewComboField runat="server" Visible="True" Left="417px" Top="79px" ID="doc_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="62px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="doc_no" Visible="True" Left="285px" Top="79px" ID="doc_no" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="128px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="647px" Top="50px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_name" Visible="True" Left="647px" Top="79px" Alignment="Right" ID="supplier_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש הזמנה / חשבונית&quot;" TemplateText="&quot;הפרש הזמנה / חשבונית&quot;" Visible="True" Left="227px" Top="115px" Alignment="Center" ID="order_inv_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="185px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש הזמנה / מסופון&quot;" TemplateText="&quot;הפרש הזמנה / מסופון&quot;" Visible="True" Left="416px" Top="115px" Alignment="Center" ID="order_mt_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="185px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש חשבונית / מסופון&quot;" TemplateText="&quot;הפרש חשבונית / מסופון&quot;" Visible="True" Left="38px" Top="115px" Alignment="Center" ID="mt_inv_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="185px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="4px" Top="115px" Alignment="Center" ID="t_1" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="972px" Top="115px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="859px" Top="115px" Alignment="Center" ID="materials_barcode_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="109px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם פריט&quot;" TemplateText="&quot;שם פריט&quot;" Visible="True" Left="605px" Top="115px" Alignment="Center" ID="materials_name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="250px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הזמנה&quot;" TemplateText="&quot;הזמנה&quot;" Visible="True" Left="542px" Top="135px" Alignment="Center" ID="order1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מסופון&quot;" TemplateText="&quot;מסופון&quot;" Visible="True" Left="479px" Top="135px" Alignment="Center" ID="mt1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="416px" Top="135px" Alignment="Center" ID="diff1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הזמנה&quot;" TemplateText="&quot;הזמנה&quot;" Visible="True" Left="353px" Top="135px" Alignment="Center" ID="order2_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חשבונית&quot;" TemplateText="&quot;חשבונית&quot;" Visible="True" Left="290px" Top="135px" Alignment="Center" ID="inv1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="227px" Top="135px" Alignment="Center" ID="diff2_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חשבונית&quot;" TemplateText="&quot;חשבונית&quot;" Visible="True" Left="164px" Top="135px" Alignment="Center" ID="inv3_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מסופון&quot;" TemplateText="&quot;מסופון&quot;" Visible="True" Left="101px" Top="135px" Alignment="Center" ID="mt3_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="38px" Top="135px" Alignment="Center" ID="diff3_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Left="39px" Top="0px" ID="compute" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="20px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;פ.ת&quot;" TemplateText="&quot;פ.ת&quot;" Left="4px" Top="1px" ID="t_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="972px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="859px" Top="1px" Alignment="Center" ID="materials_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="109px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_name" Visible="True" Left="605px" Top="1px" Alignment="Right" ID="materials_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="250px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="order_quantity_1" Visible="True" Left="542px" Top="1px" Alignment="Center" ID="order_quantity_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="mt_quantity_1" Visible="True" Left="479px" Top="1px" Alignment="Center" ID="mt_quantity_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="diff_1" Visible="True" Left="416px" Top="1px" Alignment="Center" ID="diff_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="order_quantity_2" Visible="True" Left="353px" Top="1px" Alignment="Center" ID="order_quantity_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="inv_quantity_2" Visible="True" Left="290px" Top="1px" Alignment="Center" ID="inv_quantity_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="diff_2" Visible="True" Left="227px" Top="1px" Alignment="Center" ID="diff_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="inv_quantity_3" Visible="True" Left="164px" Top="1px" Alignment="Center" ID="inv_quantity_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="mt_quantity_3" Visible="True" Left="101px" Top="1px" Alignment="Center" ID="mt_quantity_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="diff_3" Visible="True" Left="38px" Top="1px" Alignment="Center" ID="diff_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

<vt:ComponentManager runat="server" Visible="False">
	<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Visible="True" Left="605px" Top="2px" ID="t_4" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="32px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="164px" Top="2px" ID="sum_inv_quantity_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="101px" Top="2px" ID="sum_mt_quantity_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="38px" Top="2px" ID="sum_diff_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="416px" Top="2px" ID="sum_diff_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="353px" Top="2px" ID="sum_order_quantity_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="290px" Top="2px" ID="sum_inv_quantity_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="227px" Top="2px" ID="sum_diff_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="542px" Top="2px" ID="sum_order_quantity_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="479px" Top="2px" ID="sum_mt_quantity_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px">
	</vt:DataViewLabelField>
</vt:ComponentManager>

