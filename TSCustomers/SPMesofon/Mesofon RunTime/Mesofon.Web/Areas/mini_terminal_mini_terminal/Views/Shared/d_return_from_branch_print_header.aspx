<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_from_branch_print_headerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#FFFFFF">
	<Bands>
<vt:DataViewBand runat="server" Height="189px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="4px" Top="79px" ID="l_5" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="467px"></vt:DataViewShapeField>
<vt:DataViewLabelField runat="server" Text="&quot;&#39;החזרה מס&quot;" TemplateText="&quot;&#39;החזרה מס&quot;" Visible="True" Left="322px" Top="86px" Alignment="Right" ID="t_19" BorderStyle="None" Font-Names="Arial" Font-Size="14pt" ForeColor="#000000" Height="23px" Width="153px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;נתקבל ע&quot;י&quot;" TemplateText="&quot;נתקבל ע&quot;י&quot;" Visible="True" Left="387px" Top="164px" Alignment="Right" ID="t_10" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סטטוס&quot;" TemplateText="&quot;סטטוס&quot;" Visible="True" Left="418px" Top="139px" Alignment="Right" ID="t_27" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך החזרה&quot;" TemplateText="&quot;תאריך החזרה&quot;" Visible="True" Left="383px" Top="114px" Alignment="Right" ID="t_29" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="92px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="employee_name" Visible="True" Left="192px" Top="165px" Alignment="Right" ID="employee_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="129px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="327px" Top="165px" Alignment="Right" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="57px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="195px" Top="6px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="280px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="vat_number" Visible="True" Left="208px" Top="33px" Alignment="Right" ID="vat_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="phone_number" Visible="True" Left="6px" Top="33px" Alignment="Right" ID="phone_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="98px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="fax_number" Visible="True" Left="7px" Top="55px" Alignment="Right" ID="fax_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="98px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;עוסק מורשה&quot;" TemplateText="&quot;עוסק מורשה&quot;" Visible="True" Left="324px" Top="33px" Alignment="Right" ID="t_36" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="151px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;טלפון&quot;" TemplateText="&quot;טלפון&quot;" Visible="True" Left="111px" Top="33px" Alignment="Right" ID="t_37" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="89px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;פקס&quot;" TemplateText="&quot;פקס&quot;" Visible="True" Left="111px" Top="55px" Alignment="Right" ID="t_38" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="89px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="return_datemove_1" Visible="True" Left="218px" Top="116px" ID="return_datemove_1" BorderStyle="None" Font-Names="Tahoma" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="154px"></vt:DataViewTextField>
<vt:DataViewComboField runat="server" Visible="True" Left="295px" Top="140px" ID="state_1" BorderStyle="None" Font-Names="Tahoma" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="79px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="288px" Top="86px" ID="invoice_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="14pt" ForeColor="WindowText" Height="23px" Width="77px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="2px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="return_datemove" Visible="True" Left="723px" Top="0px" ID="return_datemove" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="553px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="1276px" Top="0px" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="7px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

