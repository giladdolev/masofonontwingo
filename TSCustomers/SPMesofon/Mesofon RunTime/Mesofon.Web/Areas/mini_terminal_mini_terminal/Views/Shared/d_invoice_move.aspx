<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_invoice_moveRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך&quot;" TemplateText="&quot;תאריך&quot;" Visible="True" Left="105px" Top="3px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Visible="True" Left="41px" Top="3px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;לוג בוק&quot;" TemplateText="&quot;לוג בוק&quot;" Visible="True" Left="1px" Top="3px" Alignment="Center" ID="log_book_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="156px" Top="3px" Alignment="Center" ID="number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="69px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="43px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="b2b_status" Visible="True" Left="163px" Top="1px" ID="b2b_status" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="34px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="log_book" Visible="True" Left="1px" Top="2px" Alignment="Right" ID="log_book" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="supply_date" Visible="True" Left="108px" Top="2px" ID="supply_date" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_total" Visible="True" Left="52px" Top="0px" ID="invoice_total" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="51px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="124px" Top="25px" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="204px" Top="1px" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="22px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="194px" Top="25px" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="225px" Top="25px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="station_num" Visible="True" Left="238px" Top="3px" ID="station_num" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="19px" Top="25px" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="99px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="309px" Top="0px" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="254px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_saved" Visible="True" Left="563px" Top="0px" ID="row_saved" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="254px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="declines" Visible="True" Left="817px" Top="0px" ID="declines" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="254px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="payment_code_number" Visible="True" Left="1071px" Top="0px" ID="payment_code_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="30px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="payments_number" Visible="True" Left="1101px" Top="0px" ID="payments_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="30px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="1131px" Top="0px" ID="distributor_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="254px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_reference_number" Visible="True" Left="1385px" Top="0px" ID="b2b_reference_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="467px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

