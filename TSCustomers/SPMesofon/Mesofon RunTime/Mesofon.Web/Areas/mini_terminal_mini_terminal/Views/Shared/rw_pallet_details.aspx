<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.rw_pallet_details>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Text="בחירת מרלו&quot;ג" Resizable="True" ID="rw_pallet_details" LoadAction="rw_pallet_details\Form_Load" BackColor="ButtonFace" Height="304px" Width="238px">
		<vt:DataView runat="server" Top="8px" Left="5px" ID="dw_details" Height="231px" TabIndex="10" Width="228px">
		</vt:DataView>
		<vt:Button runat="server" Text="אישור" Top="245px" Left="83px" ID="cb_ok" ClickAction="rw_pallet_details\cb_ok_clicked" Height="34px" TabIndex="10" Width="72px">
		</vt:Button>
	</vt:WindowView>

</asp:Content>
