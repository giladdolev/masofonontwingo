<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_crossdoc_pack_list>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="none" ID="uo_mini_terminal_crossdoc_pack_list" LoadAction="uo_mini_terminal_crossdoc_pack_list\Form_Load"  BackColor="ButtonFace" Height="523px" Width="360px" >
	<vt:Grid runat="server" ReadOnly="true" RowHeaderVisible="false" Top="93px" Left="10px" ID="crossdoc_pack_list_dw_pallets_list" CellContentClickAction="uo_mini_terminal_crossdoc_pack_list\dw_pallets_list_clicked" Height="270px" TabIndex="20" Width="340px" SortableColumns="false">
	</vt:Grid>


    <vt:ComboGrid runat="server" ExpandOnFocus="true"  ValueMember = "shipment_pallet_number" DisplayMember = "shipment_pallet_number" Top="50px" Left="130px" ShowHeader="false" ID="crossdoc_pack_list_dw_shipments_list" SelectedIndexChangeAction="uo_mini_terminal_crossdoc_pack_list\dw_shipments_list_itemchanged" TextChangedAction="uo_mini_terminal_crossdoc_pack_list\dw_shipments_list_editchanged"  Height="35px" TabIndex="10" Width="220px">
    </vt:ComboGrid>

	<vt:Label runat="server" RightToLeft="No" TextAlign="MiddleRight" Font-Bold="true"  Text="����� ������� �������" Top="10px" Left="90px" ID="st_title" BackColor="#A6CAF0" Font-Names="Tahoma" Font-Size="14pt" ForeColor="WindowText" Height="40px" Width="260px">
	</vt:Label>
	<vt:Label runat="server" RightToLeft="No"  Text=":���� �����" Top="54px" Left="240px" ID="st_shipment_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="15pt" ForeColor="WindowText" Height="30px" Width="110px">
	</vt:Label>
	
	<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="365px" Left="8px" ID="sle_scan_line" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="40px" TabIndex="50" Width="162px" LostFocusAction="uo_mini_terminal_pallets_list\sle_scan_line_losefocus" GotFocusAction="uo_mini_terminal_pallets_list\sle_scan_line_getfocus" TextChangedAction="uo_mini_terminal_crossdoc_pack_list\scan_line_enter" KeyDownAction="uo_mini_terminal_crossdoc_pack_list\sle_scan_line_ue_enter">
	</vt:TextBox>
	<vt:Button runat="server" Text="���� ���� ���" Top="365px" Left="175px" ID="cb_finish" ClickAction="uo_mini_terminal_crossdoc_pack_list\cb_finish_clicked" Height="40px" TabIndex="50" Width="115px">
	</vt:Button>
    <vt:Button runat="server" Text="�����" Top="365px" Left="292px" ID="cb_exit" ClickAction="uo_mini_terminal_crossdoc_pack_list\cb_exit_clicked" Height="40px" TabIndex="40" Width="60px">
	</vt:Button>

    <vt:Button runat="server" Text="���� ��� ���" Top="250px" Left="147px" Visible="False" ID="cb_print" ClickAction="uo_mini_terminal_crossdoc_pack_list\cb_print_clicked" Height="25px" TabIndex="30" Width="80px">
	</vt:Button>
</vt:CompositeView>

