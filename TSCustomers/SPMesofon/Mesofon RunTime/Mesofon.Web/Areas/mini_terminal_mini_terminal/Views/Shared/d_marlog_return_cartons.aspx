<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_marlog_return_cartonsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="16px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;כמות&quot;" TemplateText="&quot;כמות&quot;" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="material_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="43px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר קרטון&quot;" TemplateText="&quot;מספר קרטון&quot;" Visible="True" Left="90px" Top="1px" Alignment="Center" ID="carton_barcode_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="117px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סטטוס&quot;" TemplateText="&quot;סטטוס&quot;" Visible="True" Left="47px" Top="1px" Alignment="Center" ID="state_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="41px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="17px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="carton_barcode" Visible="True" Left="90px" Top="2px" Alignment="Right" ID="carton_barcode" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="14px" Width="117px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="material_quantity" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="14px" Width="43px"></vt:DataViewTextField>
<vt:DataViewComboField runat="server" Visible="True" Left="47px" Top="2px" Alignment="Center" ID="state" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="14px" Width="41px"></vt:DataViewComboField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

