<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_pallets_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;סריקה&quot;" TemplateText="&quot;סריקה&quot;" Visible="True" Left="1px" Top="0px" Alignment="Center" ID="auto_scan_status_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="71px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;אישור&quot;" TemplateText="&quot;אישור&quot;" Visible="True" Left="74px" Top="0px" Alignment="Center" ID="approve_ind_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="26px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס קרט&quot;" TemplateText="&quot;מס קרט&quot;" Visible="True" Left="102px" Top="0px" Alignment="Center" ID="pack_quantity_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="25px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס משטח&quot;" TemplateText="&quot;מס משטח&quot;" Visible="True" Left="129px" Top="0px" Alignment="Center" ID="short_pallet_number_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="77px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Left="27px" Top="1px" ID="compute_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="125px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="ButtonFace" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="short_pallet_number" Left="129px" Top="0px" Alignment="Center" ID="short_pallet_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="77px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pack_quantity" Visible="True" Left="102px" Top="0px" Alignment="Center" ID="pack_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="25px"></vt:DataViewTextField>
<vt:DataViewCheckField runat="server" Visible="True" Left="74px" Top="0px" Alignment="Center" ID="approve_ind" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="26px"></vt:DataViewCheckField>
        <vt:DataViewButtonField runat="server" ID="b_manual" Text="ידני" Font-Size="10pt" Height="24px" TabIndex="30" Width="88px">
        </vt:DataViewButtonField>
        <vt:DataViewButtonField runat="server" ID="b_automatic" Text="אוט" Font-Size="10pt" Height="24px" TabIndex="30" Width="88px">
        </vt:DataViewButtonField>
<vt:DataViewTextField runat="server" DataMember="pallet_number" Visible="True" Left="129px" Top="0px" ID="pallet_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="77px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

