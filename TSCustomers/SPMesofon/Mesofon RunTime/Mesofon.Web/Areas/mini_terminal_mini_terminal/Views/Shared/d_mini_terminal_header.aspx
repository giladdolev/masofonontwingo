<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_headerRepository>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt"  %>
<vt:ControlView runat="server">
    <vt:DataView runat="server" Height="75px" Width="345px" AutoSize="True" DataSource="<%#Model.PrimaryList%>" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
        <Bands>
            <vt:DataViewBand runat="server" Height="75px" Width="345px" ForeColor="#000000" Type="Details" ID="detail">
                <Fields>
                    <vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="153px" Top="5px" Alignment="Right" ID="supplier_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="48px"></vt:DataViewTextField>
                    <vt:DataViewDropDownField runat="server" Visible="True" Left="4px" Top="5px" Alignment="Right" ID="supplier_name" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="103px"></vt:DataViewDropDownField>
                    <vt:DataViewLabelField runat="server" Text="ספק" TemplateText="ספק" Visible="True" Left="204px" Top="5px" Alignment="Center" ID="supplier_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="21px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="ש. ספק" TemplateText="ש. ספק" Visible="True" Left="110px" Top="5px" Alignment="Center" ID="supplier_name_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="34px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="מספר הזמנה" TemplateText="מספר הזמנה" Visible="True" Left="166px" Top="26px" Alignment="Center" ID="order_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="60px"></vt:DataViewLabelField>
                    <vt:DataViewDropDownField runat="server" Visible="True" Left="80px" Top="26px" Alignment="Right" ID="order_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="82px"></vt:DataViewDropDownField>
                </Fields>
            </vt:DataViewBand>
        </Bands>
    </vt:DataView>
</vt:ControlView>

