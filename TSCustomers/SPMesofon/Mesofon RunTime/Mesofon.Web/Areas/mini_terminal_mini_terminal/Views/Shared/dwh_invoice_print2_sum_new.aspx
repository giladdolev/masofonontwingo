<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dwh_invoice_print2_sum_newRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#FFFFFF">
	<Bands>
<vt:DataViewBand runat="server" Height="66px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;סיכום מחלקות&quot;" TemplateText="&quot;סיכום מחלקות&quot;" Visible="True" Left="852px" Top="9px" Alignment="Right" ID="t_35" BorderStyle="None" Font-Names="Arial" Font-Size="14pt" ForeColor="#000000" Height="23px" Width="171px"></vt:DataViewLabelField>
<vt:DataViewShapeField runat="server" Type="Rectangle" Visible="True" Left="3px" Top="39px" ID="r_2" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="26px" Width="1031px"></vt:DataViewShapeField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מחלקה&quot;" TemplateText="&quot;שם מחלקה&quot;" Visible="True" Left="746px" Top="41px" Alignment="Center" ID="t_36" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="245px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות&quot;" TemplateText="&quot;כמות&quot;" Visible="True" Left="638px" Top="41px" Alignment="Center" ID="t_46" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;בונוס&quot;" TemplateText="&quot;בונוס&quot;" Visible="True" Left="530px" Top="41px" Alignment="Center" ID="t_47" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מע&#39;&#39;מ&quot;" TemplateText="&quot;מע&#39;&#39;מ&quot;" Visible="True" Left="314px" Top="41px" Alignment="Center" ID="t_39" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סכום כולל מע&#39;&#39;מ&quot;" TemplateText="&quot;סכום כולל מע&#39;&#39;מ&quot;" Visible="True" Left="187px" Top="41px" Alignment="Center" ID="t_40" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="124px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ערך מכירה&quot;" TemplateText="&quot;ערך מכירה&quot;" Visible="True" Left="79px" Top="41px" Alignment="Center" ID="t_43" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;GP&quot;" TemplateText="&quot;GP&quot;" Visible="True" Left="8px" Top="41px" Alignment="Center" ID="t_41" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="68px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&#39;מח&quot;" TemplateText="&quot;&#39;מח&quot;" Visible="True" Left="995px" Top="41px" Alignment="Center" ID="t_45" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="35px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ערך עלות&quot;" TemplateText="&quot;ערך עלות&quot;" Visible="True" Left="422px" Top="41px" Alignment="Center" ID="t_42" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&#39;מס&quot;" TemplateText="&quot;&#39;מס&quot;" Left="995px" Top="41px" Alignment="Center" ID="t_53" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="35px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מחלקה&quot;" TemplateText="&quot;שם מחלקה&quot;" Visible="True" Left="746px" Top="41px" Alignment="Center" ID="t_52" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="245px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות&quot;" TemplateText="&quot;כמות&quot;" Visible="True" Left="638px" Top="41px" Alignment="Center" ID="t_51" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;בונוס&quot;" TemplateText="&quot;בונוס&quot;" Visible="True" Left="530px" Top="41px" Alignment="Center" ID="t_50" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ערך עלות&quot;" TemplateText="&quot;ערך עלות&quot;" Visible="True" Left="422px" Top="41px" Alignment="Center" ID="t_49" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מע&#39;&#39;מ&quot;" TemplateText="&quot;מע&#39;&#39;מ&quot;" Visible="True" Left="314px" Top="41px" Alignment="Center" ID="t_48" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סכום כולל מע&#39;&#39;מ&quot;" TemplateText="&quot;סכום כולל מע&#39;&#39;מ&quot;" Visible="True" Left="187px" Top="41px" Alignment="Center" ID="t_44" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="124px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ערך מכירה&quot;" TemplateText="&quot;ערך מכירה&quot;" Visible="True" Left="79px" Top="41px" Alignment="Center" ID="t_38" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;GP&quot;" TemplateText="&quot;GP&quot;" Visible="True" Left="8px" Top="41px" Alignment="Center" ID="t_37" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="68px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="27px" ForeColor="#000000" Type="Summary" ID="summary"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="26px" Top="33px" ID="invoice_total_cf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="87px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ לתשלום&quot;" TemplateText="&quot;סה&quot;כ לתשלום&quot;" Visible="True" Left="134px" Top="33px" Alignment="Right" ID="t_12" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="75px"></vt:DataViewLabelField>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="8px" Top="26px" ID="l_2" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="1023px"></vt:DataViewShapeField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="248px" Top="33px" ID="total_vat" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="87px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מע&quot;מ&quot;" TemplateText="&quot;מע&quot;מ&quot;" Visible="True" Left="367px" Top="33px" Alignment="Right" ID="t_11" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="64px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="465px" Top="31px" ID="compute_5" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="87px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כולל מע&quot;מ&quot;" TemplateText="&quot;כולל מע&quot;מ&quot;" Visible="True" Left="584px" Top="31px" Alignment="Right" ID="t_14" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="64px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Left="540px" Top="55px" Alignment="Right" ID="t_18" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="35px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="320px" Top="55px" ID="compute_vat" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="309px" Top="55px" ID="compute_total" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="446px" Top="55px" ID="mam_total_cf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="446px" Top="55px" ID="total_cf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="252px" Top="55px" ID="compute_4" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="mam" Left="237px" Top="55px" Alignment="Right" ID="mam" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="40px"></vt:DataViewTextField>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="8px" Top="1px" ID="l_3" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="1023px"></vt:DataViewShapeField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="638px" Top="6px" ID="total_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="530px" Top="6px" ID="compute_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="422px" Top="6px" ID="compute_14" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="187px" Top="6px" ID="compute_12" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="124px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="79px" Top="6px" ID="compute_3" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="8px" Top="6px" ID="sum_gp" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="68px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="8px" Top="6px" ID="sum_gp_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="68px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="sell_price" Left="563px" Top="1px" Alignment="Right" ID="sell_price" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="65px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_bonus_connect" Left="378px" Top="1px" Alignment="Right" ID="material_bonus_connect" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="56px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price" Left="305px" Top="1px" Alignment="Right" ID="material_price" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="70px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_discount_percent" Left="260px" Top="1px" Alignment="Right" ID="material_discount_percent" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="42px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_new_price" Left="184px" Top="1px" Alignment="Right" ID="material_new_price" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="73px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="35px" Top="1px" ID="compute_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="51px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="vat_types_percentage" Left="899px" Top="2px" ID="vat_types_percentage" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="3px" Width="8px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="color" Left="884px" Top="2px" ID="color" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="2px" Width="11px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="import_type" Left="872px" Top="2px" Alignment="Right" ID="import_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="1px" Width="7px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Left="494px" Top="1px" Alignment="Right" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="66px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="bonus_quantity" Left="437px" Top="1px" Alignment="Right" ID="bonus_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="54px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="89px" Top="1px" ID="material_value" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="92px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="catalog_entities_entity_number" Left="631px" Top="1px" Alignment="Center" ID="catalog_entities_entity_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="31px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Left="840px" Top="1px" Alignment="Right" ID="materials_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="104px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="947px" Top="1px" ID="line_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="29px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="bonus_discount" Left="932px" Top="0px" ID="bonus_discount" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="29px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_name" Left="665px" Top="1px" Alignment="Right" ID="material_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="171px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="campaign_new_price" Left="996px" Top="0px" ID="campaign_new_price" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="3px" Width="27px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

<vt:ComponentManager runat="server" Visible="False">
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="8px" Top="1px" ID="compute_gp" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="68px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="638px" Top="1px" ID="compute_6" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="530px" Top="1px" ID="compute_7" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="422px" Top="1px" ID="compute_8" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="314px" Top="1px" ID="compute_13" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="187px" Top="1px" ID="compute_11" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="124px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="79px" Top="1px" ID="compute_10" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="105px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Left="8px" Top="1px" ID="compute_gp_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="68px">
	</vt:DataViewLabelField>
	<vt:DataViewTextField runat="server" DataMember="catalog_entities_entity_number_1" Visible="True" Left="1006px" Top="1px" Alignment="Right" ID="catalog_entities_entity_number_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="24px">
	</vt:DataViewTextField>
	<vt:DataViewTextField runat="server" DataMember="entity_name" Visible="True" Left="747px" Top="1px" Alignment="Right" ID="entity_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="253px">
	</vt:DataViewTextField>
</vt:ComponentManager>

