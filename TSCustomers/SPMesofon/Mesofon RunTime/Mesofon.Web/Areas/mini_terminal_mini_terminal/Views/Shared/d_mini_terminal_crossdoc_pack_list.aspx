<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_crossdoc_pack_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;אישור&quot;" TemplateText="&quot;אישור&quot;" Visible="True" Left="98px" Top="0px" Alignment="Center" ID="state_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="26px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד קרטון&quot;" TemplateText="&quot;ברקוד קרטון&quot;" Visible="True" Left="126px" Top="0px" Alignment="Center" ID="carton_barcode_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="91px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="carton_barcode" Visible="True" Left="126px" Top="0px" ID="carton_barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="91px"></vt:DataViewTextField>
<vt:DataViewCheckField runat="server" Visible="True" Left="98px" Top="0px" Alignment="Center" ID="state" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="26px"></vt:DataViewCheckField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

