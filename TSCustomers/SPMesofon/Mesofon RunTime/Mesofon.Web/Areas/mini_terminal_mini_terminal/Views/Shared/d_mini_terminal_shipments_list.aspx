<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_shipments_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewDropDownField runat="server" Visible="True" Left="0px" Top="0px" Alignment="Right" ID="shipment_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="116px"></vt:DataViewDropDownField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

