<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_driver_report_excessRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="48px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מספר חשבונית&quot;" TemplateText="&quot;מספר חשבונית&quot;" Visible="True" Left="341px" Top="30px" Alignment="Center" ID="invoice_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="114px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תיאור&quot;" TemplateText="&quot;תיאור&quot;" Visible="True" Left="458px" Top="30px" Alignment="Center" ID="decline_description_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="171px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="632px" Top="30px" Alignment="Center" ID="item_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="110px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:עודפים&quot;" TemplateText="&quot;:עודפים&quot;" Visible="True" Left="698px" Top="3px" Alignment="Right" ID="diff_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="45px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="279px" Top="30px" Alignment="Center" ID="b2b_declines_move_reject_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="b2b_declines_move_parent_doc_number" Visible="True" Left="341px" Top="1px" Alignment="Center" ID="b2b_declines_move_parent_doc_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="114px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_item_barcode" Visible="True" Left="632px" Top="1px" ID="materials_item_barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="110px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="description" Visible="True" Left="458px" Top="1px" Alignment="Right" ID="description" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_declines_move_reject_quantity" Visible="True" Left="279px" Top="1px" Alignment="Center" ID="b2b_declines_move_reject_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

