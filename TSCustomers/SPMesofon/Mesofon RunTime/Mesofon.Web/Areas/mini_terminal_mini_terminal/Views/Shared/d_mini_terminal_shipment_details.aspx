<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_shipment_detailsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;State&quot;" TemplateText="&quot;State&quot;" Visible="True" Left="3px" Top="2px" Alignment="Center" ID="state_t" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="35px" Top="1px" ID="branch_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="503px" Top="1px" ID="supplier_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Order Number&quot;" TemplateText="&quot;Order Number&quot;" Visible="True" Left="971px" Top="1px" ID="order_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Invoice Number&quot;" TemplateText="&quot;Invoice Number&quot;" Visible="True" Left="1439px" Top="1px" ID="invoice_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Shipment Number&quot;" TemplateText="&quot;Shipment Number&quot;" Visible="True" Left="1907px" Top="1px" ID="shipment_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Pallet Number&quot;" TemplateText="&quot;Pallet Number&quot;" Visible="True" Left="2375px" Top="1px" ID="pallet_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="117px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Item Number&quot;" TemplateText="&quot;Item Number&quot;" Visible="True" Left="2494px" Top="1px" ID="item_number_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Item Barcode&quot;" TemplateText="&quot;Item Barcode&quot;" Visible="True" Left="2962px" Top="1px" ID="item_barcode_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="88px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="3px" Top="2px" ID="state" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="30px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="36px" Top="0px" ID="branch_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="504px" Top="0px" ID="supplier_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="972px" Top="0px" ID="order_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="1440px" Top="0px" ID="invoice_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="shipment_number" Visible="True" Left="1908px" Top="0px" ID="shipment_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pallet_number" Visible="True" Left="2376px" Top="0px" ID="pallet_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="116px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_number" Visible="True" Left="2495px" Top="0px" ID="item_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_barcode" Visible="True" Left="2963px" Top="0px" ID="item_barcode" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="87px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

