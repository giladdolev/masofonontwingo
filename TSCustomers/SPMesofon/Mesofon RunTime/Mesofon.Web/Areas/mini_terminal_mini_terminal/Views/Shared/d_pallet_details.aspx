<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_pallet_detailsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;חשבונית&quot;" TemplateText="&quot;חשבונית&quot;" Visible="True" Left="55px" Top="2px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="15px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות&quot;" TemplateText="&quot;כמות&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="quantity_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="15px" Width="49px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד ושם פריט&quot;" TemplateText="&quot;ברקוד ושם פריט&quot;" Visible="True" Left="123px" Top="2px" Alignment="Center" ID="barcode_name_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="81px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="36px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="item_barcode" Visible="True" Left="123px" Top="2px" Alignment="Center" ID="item_barcode" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="12px" Width="81px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="55px" Top="2px" Alignment="Center" ID="invoice_number" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="12px" Width="63px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="units_qty" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="units_qty" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="12px" Width="49px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="items_name" Visible="True" Left="2px" Top="20px" Alignment="Right" ID="items_name" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="12px" Width="202px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

