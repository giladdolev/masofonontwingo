<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_debit_for_b2b_xmlRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="27px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewTextField runat="server" DataMember="sender" Visible="True" Left="3px" Top="8px" ID="sender" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="receiver" Visible="True" Left="48px" Top="8px" ID="receiver" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[General]" DataMember="messdate" Visible="True" Left="99px" Top="8px" ID="messdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="testind" Visible="True" Left="162px" Top="8px" ID="testind" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="43px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="ackn" Visible="True" Left="209px" Top="8px" ID="ackn" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="30px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="snrf" Visible="True" Left="243px" Top="8px" ID="snrf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="26px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="aprf" Visible="True" Left="273px" Top="8px" ID="aprf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="25px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doctype" Visible="True" Left="302px" Top="8px" ID="doctype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="50px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[General]" DataMember="messtime" Visible="True" Left="356px" Top="8px" ID="messtime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="61px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoicetype" Visible="True" Left="421px" Top="8px" ID="invoicetype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="64px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="debitinvoiceno" Visible="True" Left="489px" Top="8px" ID="debitinvoiceno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoicefunc" Visible="True" Left="577px" Top="8px" ID="invoicefunc" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="67px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="datetime" Visible="True" Left="648px" Top="8px" ID="datetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="52px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplierinvoiceno" Visible="True" Left="704px" Top="8px" ID="supplierinvoiceno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="89px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplierinvoicedate" Visible="True" Left="796px" Top="8px" ID="supplierinvoicedate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="109px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="referenceno" Visible="True" Left="909px" Top="8px" ID="referenceno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="71px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="referencedate" Visible="True" Left="984px" Top="8px" ID="referencedate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="81px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pricelistno" Visible="True" Left="1069px" Top="8px" ID="pricelistno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="61px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pricelistdate" Visible="True" Left="1134px" Top="8px" ID="pricelistdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplierno" Visible="True" Left="1210px" Top="8px" ID="supplierno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="62px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="suppliername" Visible="True" Left="1276px" Top="8px" ID="suppliername" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="79px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="storeno" Visible="True" Left="1359px" Top="8px" ID="storeno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="storename" Visible="True" Left="1410px" Top="8px" ID="storename" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="65px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discountpercent" Visible="True" Left="1479px" Top="8px" ID="discountpercent" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="94px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discountamount" Visible="True" Left="1577px" Top="8px" ID="discountamount" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="96px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="linessum" Visible="True" Left="1677px" Top="8px" ID="linessum" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="54px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="docsum" Visible="True" Left="1734px" Top="8px" ID="docsum" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invsum" Visible="True" Left="1786px" Top="8px" ID="invsum" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="56px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="lineno" Visible="True" Left="62px" Top="3px" ID="lineno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="37px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="itembarcode" Visible="True" Left="103px" Top="3px" ID="itembarcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="73px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="proddesc" Visible="True" Left="180px" Top="3px" ID="proddesc" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="58px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packno" Visible="True" Left="243px" Top="3px" ID="packno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="unitsqty" Visible="True" Left="294px" Top="3px" ID="unitsqty" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="52px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="reasoncode" Visible="True" Left="350px" Top="3px" ID="reasoncode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="unitsqtymea" Visible="True" Left="413px" Top="3px" ID="unitsqtymea" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="76px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="itempricenetosph" Visible="True" Left="493px" Top="3px" ID="itempricenetosph" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="107px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pricedifference" Visible="True" Left="740px" Top="1px" ID="pricedifference" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="86px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="linesum" Visible="True" Left="831px" Top="1px" ID="linesum" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="itempricenetosupplier" Visible="True" Left="606px" Top="2px" ID="itempricenetosupplier" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="131px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

