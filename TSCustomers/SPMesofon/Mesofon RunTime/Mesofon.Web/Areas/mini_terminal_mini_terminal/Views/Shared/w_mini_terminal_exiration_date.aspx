﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.w_mini_terminal_exiration_date>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <vt:WindowView runat="server" Opacity="1" Resizable="false" CssClass="expiration_date_form" LoadAction="w_mini_terminal_exiration_date\Form_Load" ID="w_mini_terminal_exiration_date" BackColor="ButtonFace" Height="250px" Width="275px">
        <vt:Button runat="server" TextAlign="TopLeft" Visible="false" Tag="NoMove" Text="More &gt;&gt;" Top="16px" Left="209px" ID="st_more" ClickAction="w_mini_terminal_exiration_date\st_more_clicked" BackColor="ButtonFace" BorderStyle="None" ForeColor="Black" Font-Names="Arial" Font-Size="11pt" Height="20px" Width="80px">
        </vt:Button>
        <vt:Image runat="server" Tag="NoMove" Top="5px" Left="4px" ID="p_icon" Height="17px" Width="19px">
        </vt:Image>
        <vt:TextBox runat="server" Tag="NoMove" PasswordChar="" MaxLength="32767" Visible="false" ReadOnly="True" Top="44px" Left="1px" ID="mle_error" BackColor="ButtonFace" ForeColor="WindowText" Height="75px" TabIndex="20" Width="268px">
        </vt:TextBox>


        <vt:TextBox runat="server" Tag="NoMove" Multiline="true" PasswordChar="" MaxLength="32767" ReadOnly="True" Top="5px" Left="1px" ID="mle_message" BackColor="ButtonFace" ForeColor="WindowText" Height="125px" TabIndex="20" Width="280px">
        </vt:TextBox>

        <vt:Label runat="server" Tag="NoMove" TextAlign="TopLeft" Text="תאריך תפוגה:" Top="144px" Left="48px" ID="st_expiration" Font-Bold="true" BackColor="ButtonFace" Font-Names="Arial" Font-Size="12pt" Height="15px" Width="61px">
        </vt:Label>
        <vt:TextBox runat="server" Tag="NoMove" Visible="True" CssClass="maskedStyle" Left="144px" Top="134px" ID="txt_expiration_date" TextChangedAction="w_mini_terminal_exiration_date\txt_expiration_date_TextChanged" KeyDownAction="w_mini_terminal_exiration_date\txt_expiration_date_KeyDown" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="34px" Width="117px"></vt:TextBox>
        <vt:MaskedTextBox runat="server" Tag="NoMove" Enabled="false" PasswordChar="" MaxLength="32767" ReadOnly="false" ZIndex="0" Top="170px" Left="144px" ID="em_expiration_date" BackColor="White" ForeColor="WindowText" Height="28px" Mask="##/##/####" TabIndex="20" Width="117px" CssClass="maskedStyle">
        </vt:MaskedTextBox>

        <vt:GroupBox runat="server" Tag="NoMove" Top="24px" Left="1px" Visible="False" ID="gb_message" BackColor="ButtonFace" ForeColor="WindowText" Height="101px" TabIndex="30" Width="268px">
        </vt:GroupBox>


        <vt:Button runat="server" Tag="NoMove" Text="אישור" Top="210px" Left="62px" ID="cb_ok" ClickAction="w_mini_terminal_exiration_date\cb_ok_clicked" Height="25px" TabIndex="20" Width="61px"></vt:Button>
        <vt:Button runat="server" Tag="NoMove" Text="ביטול" Top="210px" Left="139px" ID="cb_cancel" ClickAction="w_mini_terminal_exiration_date\cb_cancel_clicked" Height="25px" TabIndex="20" Width="61px"></vt:Button>

    </vt:WindowView>

</asp:Content>
