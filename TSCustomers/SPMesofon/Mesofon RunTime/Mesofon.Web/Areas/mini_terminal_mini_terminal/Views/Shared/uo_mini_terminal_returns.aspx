<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_returns>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="none" ID="uo_mini_terminal_returns" BackColor="ButtonFace" Height="537px" Width="360px" LoadAction="uo_mini_terminal_returns\Form_Load" >
	<vt:Label runat="server" Text="���� �����" Top="7px" Left="7px" ID="return_number_t"   Font-Names="Arial" AutoSize="True" Font-Size="15pt" Height="30px" Width="200px">
	</vt:Label>
	<vt:Grid runat="server" Top="171px"  Left ="5px" ID="dw_cartons" RowHeaderVisible="false" ReadOnly="true" Height="230px" TabIndex="10" Width="350px" SortableColumns="false">
	</vt:Grid>
	<vt:Grid runat="server" Top="47px" Left="135px" ID="dw_summary_Returns" RowHeaderVisible="false" ReadOnly="true" Height="120px"  TabIndex="10" Width="220px" SortableColumns="false">
	</vt:Grid>
    <vt:ComboGrid runat="server" ExpandOnFocus="true"  ValueMember = "shipment_number" DisplayMember = "shipment_number"  ID="dw_return_num" TextChangedAction="uo_mini_terminal_returns\dw_return_num_editchanged" SelectedIndexChangeAction="uo_mini_terminal_returns\dw_return_num_itemchanged"  Top="5px" Left="125px" Height="37px"  TabIndex="20" Width="212px">
	</vt:ComboGrid>
    <vt:ComboGrid runat="server"  ExpandOnFocus="true"  ID="dw_1" ValueMember = "param_value" DisplayMember = "param_value" Top="47px" left="58px" Height="33px"  TabIndex="20" Width="70px">
	</vt:ComboGrid>
    <vt:Label runat="server" Text="" Anchor="right" Margin-All="100" Top="47px" BackColor="White" Left="5px" ID="dw_1Label"   Font-Names="Arial" AutoSize="True" Font-Size="18pt" Height="200px" Width="200px">
	</vt:Label>
	<vt:Button runat="server" Text="�����" Top="406px" Left="255px" ID="cb_exit" Height="60px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_returns\cb_exit_clicked">>
	</vt:Button>
	<vt:Button runat="server" Text="��� ������ �����" Top="471px" Left="220px" ID="cb_finish"  Height="60px" TabIndex="20" Width="134px" ClickAction="uo_mini_terminal_returns\cb_finish_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="���� �����" Top="471px" Left="110px" ID="cb_print" Height="60px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_returns\cb_print_clicked">
	</vt:Button>
    	<vt:Button runat="server" Text="��� �����" Top="406px" Left="110px" ID="cb_cancel_carton" Height="60px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_returns\cb_cancel_carton_clicked">
	</vt:Button>
    	<vt:Button runat="server" Text="���� �����" Top="406px" Left="5px" ID="cb_add_carton" Height="60px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_returns\cb_add_carton_clicked">
	</vt:Button>
    	<vt:Button runat="server" Text="��� �����" Top="471px" Left="5px" ID="cb_carton" Height="60px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_returns\cb_carton_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="���" Top="86px" Left="5px" ID="cb_new_return" Height="60px" TabIndex="40" Width="60px" ClickAction="uo_mini_terminal_returns\cb_new_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="�����" Top="86px" Left="69px" ID="cb_cancel_return" Height="60px" TabIndex="50" Width="60px" ClickAction="uo_mini_terminal_returns\cb_cancel_clicked">
	</vt:Button>
</vt:CompositeView>

