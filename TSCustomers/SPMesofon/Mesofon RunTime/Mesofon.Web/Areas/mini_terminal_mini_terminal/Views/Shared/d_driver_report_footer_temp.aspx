<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_driver_report_footer_tempRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="48px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מספר חשבונית&quot;" TemplateText="&quot;מספר חשבונית&quot;" Visible="True" Left="341px" Top="30px" Alignment="Center" ID="invoice_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="114px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר משטח&quot;" TemplateText="&quot;מספר משטח&quot;" Visible="True" Left="237px" Top="30px" Alignment="Center" ID="display_name_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="101px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות בחשבונית&quot;" TemplateText="&quot;כמות בחשבונית&quot;" Visible="True" Left="151px" Top="30px" Alignment="Center" ID="original_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="83px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות שנקלטה&quot;" TemplateText="&quot;כמות שנקלטה&quot;" Visible="True" Left="65px" Top="30px" Alignment="Center" ID="actual_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="83px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="3px" Top="30px" Alignment="Center" ID="reject_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:הפרשים לפי משטחים&quot;" TemplateText="&quot;:הפרשים לפי משטחים&quot;" Visible="True" Left="622px" Top="3px" Alignment="Right" ID="diff_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="121px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תיאור&quot;" TemplateText="&quot;תיאור&quot;" Visible="True" Left="458px" Top="30px" Alignment="Center" ID="decline_description_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="171px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="632px" Top="30px" Alignment="Center" ID="item_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="111px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="198px" ForeColor="#000000" Type="Footer" ID="footer"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="560px" Top="148px" Alignment="Right" ID="line_driver_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מקבל הסחורה&quot;" TemplateText="&quot;שם מקבל הסחורה&quot;" Visible="True" Left="415px" Top="148px" Alignment="Right" ID="accept_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="94px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="560px" Top="179px" Alignment="Right" ID="line_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חתימה&quot;" TemplateText="&quot;חתימה&quot;" Visible="True" Left="468px" Top="179px" Alignment="Right" ID="employee_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="265px" Top="148px" Alignment="Right" ID="line_accept_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="265px" Top="179px" Alignment="Right" ID="line_employee_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:הערות&quot;" TemplateText="&quot;:הערות&quot;" Visible="True" Left="704px" Top="23px" Alignment="Right" ID="notes_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם נהג&quot;" TemplateText="&quot;שם נהג&quot;" Visible="True" Left="702px" Top="148px" Alignment="Right" ID="driver_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חתימה&quot;" TemplateText="&quot;חתימה&quot;" Visible="True" Left="708px" Top="179px" Alignment="Right" ID="signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="35px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="parent_doc_number" Visible="True" Left="341px" Top="1px" Alignment="Center" ID="parent_doc_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="114px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="display_name" Visible="True" Left="237px" Top="1px" Alignment="Center" ID="display_name" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="101px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="original_quantity" Visible="True" Left="151px" Top="1px" Alignment="Center" ID="original_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="83px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="actual_quantity" Visible="True" Left="65px" Top="1px" Alignment="Center" ID="actual_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="83px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="3px" Top="1px" ID="rejected_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="description" Visible="True" Left="458px" Top="1px" Alignment="Right" ID="description" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_barcode" Visible="True" Left="632px" Top="1px" ID="item_barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="111px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

