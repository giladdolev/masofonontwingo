<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_branches_list_exRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Num&quot;" TemplateText="&quot;Branch Num&quot;" Visible="True" Left="189px" Top="3px" Alignment="Center" ID="branch_num_t" BorderStyle="Inset" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="46px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewDropDownField runat="server" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="branch_num_1" BorderStyle="Inset" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="184px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="189px" Top="2px" Alignment="Right" ID="branch_num" BorderStyle="Inset" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="46px"></vt:DataViewDropDownField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

