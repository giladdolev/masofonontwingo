<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_detail_items_invoiceRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="29px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="290px" Top="1px" Alignment="Center" ID="num_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="264px" Top="1px" ID="t_1" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="178px" Top="1px" ID="materials_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="84px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="doc_no_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="54px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ח&quot;" TemplateText="&quot;ח&quot;" Visible="True" Left="58px" Top="1px" Alignment="Center" ID="invoice_details_invoice_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ה&quot;" TemplateText="&quot;ה&quot;" Visible="True" Left="98px" Top="1px" Alignment="Center" ID="order_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מ&quot;" TemplateText="&quot;מ&quot;" Visible="True" Left="138px" Top="1px" Alignment="Center" ID="expected_material_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="30px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="290px" Top="1px" ID="compute_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="178px" Top="1px" Alignment="Center" ID="materials_barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="84px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;קוד&quot;" TemplateText="&quot;קוד&quot;" Visible="True" Left="264px" Top="1px" ID="t_2" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם&quot;" TemplateText="&quot;שם&quot;" Visible="True" Left="264px" Top="16px" ID="code_desc_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="13px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_material_name" Visible="True" Left="178px" Top="16px" ID="materials_material_name" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="13px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="98px" Top="1px" Alignment="Center" ID="order_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="28px" Width="38px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="314px" Top="0px" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="531px" Top="0px" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="216px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="expected_material_quantity" Visible="True" Left="747px" Top="0px" ID="expected_material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="216px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_status" Visible="True" Left="47px" Top="11px" ID="b2b_status" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="35px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_no" Visible="True" Left="963px" Top="0px" ID="row_no" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doc_no_en" Visible="True" Left="1180px" Top="0px" ID="doc_no_en" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

