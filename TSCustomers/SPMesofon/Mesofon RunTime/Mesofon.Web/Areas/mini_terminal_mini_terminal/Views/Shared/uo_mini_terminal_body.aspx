<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_body>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="none" ID="uo_mini_terminal_base_body" BackColor="ButtonFace" Height="640px" Width="360px" LoadAction="uo_mini_terminal_body\Form_Load" >
	<vt:Label runat="server" RightToLeft="No" Text="����� ������ ������ ����" Top="6px" Left="135px" ID="st_order" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="14pt" Height="26px" Width="196px">
	</vt:Label>
	<vt:Label runat="server" RightToLeft="No" Top="6px" Left="32px" ID="st_order_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="14pt" Height="26px" Width="75px">
	</vt:Label>
	<vt:WidgetGrid runat="server" Top="41px" RowHeaderVisible="false" ID="dw_inv_pack_details" Height="377px" TabIndex="10" Width="350px" SortableColumns="false"
        BeforeSelectionChangedAction="uo_mini_terminal_body\dw_inv_pack_details_rowfocuschanging"
        SelectionChangedAction="uo_mini_terminal_body\dw_inv_pack_details_rowfocuschanged"
        WidgetControlClickAction="uo_mini_terminal_body\dw_inv_pack_details_clicked"
        WidgetControlEditChangedAction="uo_mini_terminal_body\dw_inv_pack_details_EditChanged"
        WidgetControlItemChangedAction="uo_mini_terminal_body\dw_inv_pack_details_itemchanged"
        WidgetControlDoubleClickAction="uo_mini_terminal_body\dw_inv_pack_details_doubleclicked" 
        WidgetControlSpecialKeyAction="uo_mini_terminal_body\dw_inv_pack_details_CellkeyDown"
        CellkeyDownAction="uo_mini_terminal_body\dw_inv_pack_details_CellkeyDown2"
        WidgetTemplateModel="vm-spviewmodelBody"
        WidgetTemplateDataAttachMethod ="VT_WidgetColumnAttachBody"
        WidgetTemplate="spgridcomponentBody"
        WidgetTemplateDataMembers="row_no,barcode,material_name,order_quantity,expected_material_quantity,invoice_pack_quantity,doc_no_en,doc_no,invoice_data"
        WidgetTemplateWidth ="350">
	</vt:WidgetGrid>
	<vt:Grid runat="server" Top="475px" Left="125px" RowHeaderVisible="false" ReadOnly="true" ID="dw_summary" Height="120px" TabIndex="20" Width="230px" SortableColumns="false">
	</vt:Grid>
	<vt:Button runat="server" Text="���� ������" Top="524px" Left="0px" ID="cb_return_to_header" ClickAction="uo_mini_terminal_body\cb_return_to_header_clicked" Height="43px" TabIndex="30" Width="121px">
	</vt:Button> 
	<vt:Button runat="server" Text="����" Top="430px" Left="300px" ID="cb_finish" ClickAction="uo_mini_terminal_body/cb_finish_clicked"  Enabled="False" Height="43px" TabIndex="20" Width="54px">
	</vt:Button>
	<vt:Button runat="server" Text="��&quot;� ������" Top="477px" Left="0px" ID="cb_def_report" Height="43px" TabIndex="30" Width="121px" ClickAction="uo_mini_terminal_body\cb_def_report_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="��� ������ �� ������" Top="430px" Left="0px" ID="cb_show_all" Height="43px" TabIndex="40" Width="168px"  ClickAction="uo_mini_terminal_body\cb_show_all_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="����� ����" Top="430px" Left="170px" ID="cb_delete_item" Enabled="False" Height="43px" TabIndex="50" Width="128px" ClickAction="uo_mini_terminal_body\cb_delete_item_clicked">
	</vt:Button>
</vt:CompositeView>

