<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_sql_mini_terminal_input_bodyRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;פריטי חשבונית&quot;" TemplateText="&quot;פריטי חשבונית&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="t_3" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="15px" Width="170px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="38px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="material_name" Visible="True" Left="88px" Top="21px" Alignment="Right" ID="material_name" Font-Names="Arial" Font-Size="7pt" ForeColor="#000000" Height="15px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="88px" Top="2px" Alignment="Right" ID="barcode" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="####.00" DataMember="material_quantity" Visible="True" Left="2px" Top="21px" Alignment="Right" ID="material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="38px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="####.00" DataMember="expected_material_quantity" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="expected_material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="38px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;שם&quot;" TemplateText="&quot;שם&quot;" Visible="True" Left="176px" Top="21px" Alignment="Right" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="25px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;קוד&quot;" TemplateText="&quot;קוד&quot;" Visible="True" Left="176px" Top="2px" Alignment="Right" ID="barcode_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="25px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כ. בפועל&quot;" TemplateText="&quot;כ. בפועל&quot;" Visible="True" Left="43px" Top="2px" Alignment="Right" ID="material_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כ. בח-ן&quot;" TemplateText="&quot;כ. בח-ן&quot;" Visible="True" Left="43px" Top="21px" Alignment="Right" ID="t_2" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="41px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

