<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_branches_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="1px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="serial_number" Visible="True" Left="410px" Top="1px" Alignment="Right" ID="serial_number" Font-Names="Arial" Font-Size="20pt" ForeColor="#000000" Height="30px" Width="52px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="4px" Top="0px" Alignment="Right" ID="branch_name" Font-Names="Arial" Font-Size="20pt" ForeColor="#000000" Height="30px" Width="402px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

