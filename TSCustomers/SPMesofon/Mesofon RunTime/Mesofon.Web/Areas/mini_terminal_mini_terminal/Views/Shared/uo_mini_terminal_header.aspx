<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_header>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Tag="�������� �������" Text="none" ID="uo_mini_terminal_header" BackColor="ButtonFace" Height="536px" Width="360px" LoadAction="uo_mini_terminal_header\Load_Form" >
    <vt:GroupBox runat="server" RightToLeft="No" Text="PDF" Top="80px" Left="80px" ID="gb_1" BackColor="ButtonFace" ForeColor="WindowText" Height="50px" TabIndex="30" Width="275px">
    </vt:GroupBox>
    <vt:RadioButton runat="server" RightToLeft="No" CheckAlign="MiddleLeft" Text="�����" Top="97px" Left="150px" ID="rb_print_pdf" BackColor="ButtonFace"   ForeColor="WindowText" Height="16px" Width="60px">
        </vt:RadioButton>
        <vt:RadioButton runat="server" RightToLeft="No" CheckAlign="MiddleLeft" Text="�����" Top="97px" Left="250px" ID="rb_open_pdf" BackColor="ButtonFace"  ForeColor="WindowText" Height="16px" Width="60px">
        </vt:RadioButton>

   
    <vt:Label runat="server" CssClass="HeaderLabels" Text="���" RightToLeft="Yes" TemplateText="���" Visible="True" Left="4px" Top="3px" Alignment="Center" ID="supplier_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="15pt" ForeColor="#000000" Height="36px" Width="50px" ></vt:Label>
    <vt:Label runat="server" CssClass="HeaderLabels" Text="�. ���" RightToLeft="Yes" TemplateText="�. ���" Visible="True" Left="135px" Top="5px" Alignment="Center" ID="supplier_name_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="15pt" ForeColor="#000000" Height="36px" Width="70px" ></vt:Label>
    <vt:Label runat="server" CssClass="HeaderLabels" Text="���� �����" RightToLeft="Yes" TemplateText="���� �����" Visible="True" Left="4px" Top="45px" Alignment="Center" ID="order_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="15pt" ForeColor="#000000" Height="36px" Width="100px"></vt:Label>

    <vt:TextBox runat="server" DataMember="supplier_number" RightToLeft="Yes" Visible="True" Left="41px" Top="3px" Alignment="Right" ID="supplier_number" TextChangedAction="uo_mini_terminal_header\dw_header_editchanged" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="34px" Width="94px" KeyDownAction="uo_mini_terminal_header\header_KeyDown" DirtychangeAction="uo_mini_terminal_header\dw_header_itemchanged"></vt:TextBox>
    <vt:Combobox runat="server" ExpandOnFocus="true" Visible="True" RightToLeft="Yes" Enabled="true" Left="201px" Top="3px" Alignment="Right" ID="supplier_name" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="34px" Width="155px" SelectedIndexChangedAction="uo_mini_terminal_header\dw_header_itemchanged" DropDownWidth="155" DropDownHeight="130"   KeyDownAction="uo_mini_terminal_header\header_KeyDown" ClickAction="uo_mini_terminal_header\header_KeyDow" ></vt:Combobox >
    <vt:ComboGrid runat="server" ExpandOnFocus="true" ShowHeader="false" DisplayMember = "order_number" ValueMember = "order_number" Visible="True" RightToLeft="Yes" Left="118px" Top="43px" Alignment="Right" ID="order_number" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="34px" Width="220px" SelectedIndexChangeAction="uo_mini_terminal_header\dw_header_itemchanged" TextChangedAction="uo_mini_terminal_header\dw_header_editchanged"  KeyPressAction="uo_mini_terminal_header\header_KeyPress"  ></vt:ComboGrid>


    <vt:Label runat="server" Text="��������/�.�����  �������" Top="147px" Left="4px" ID="st_invoices" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="20px" Width="155px">
    </vt:Label>


    <vt:WidgetGrid runat="server" RowHeaderVisible="false" ScrollBars="Both"  Top="169px" ID="dw_body" 
          Height="300px" Left="5px"
        WidgetTemplateModel="vm-spviewmodelSapak_dw_body"
        WidgetTemplateDataAttachMethod ="VT_WidgetColumnAttachSapak_dw_body"
        WidgetTemplate="spgridcomponentSapak_dw_body"
        WidgetTemplateDataMembers="doc_type,number,supply_date,invoice_total,b2b_reference_number,state,create_mode,b2b_status,payment_code_number,payments_number,distributor_number,row_saved,row_no"
        WidgetTemplateWidth ="350"
        TabIndex="10" Width="350px" SortableColumns="false"
        WidgetControlItemChangedAction="uo_mini_terminal_header\dw_body_itemchanged"
        >
	</vt:WidgetGrid>

    <vt:Button runat="server" Text="����� ������" Top="480px" Left="4px" ID="cb_details" Enabled="False" Height="50px" TabIndex="30" Width="105px" ClickAction="uo_mini_terminal_header\cb_details_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="����" Top="480px" Left="180px" ID="cb_add" Height="50px" TabIndex="50" Width="50px" ClickAction="uo_mini_terminal_header\cb_add_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="�����" Top="480px" Left="295px" ID="cb_close" Height="50px" TabIndex="40" Width="60px" ClickAction="uo_mini_terminal_header\cb_close_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="���" Top="480px" Left="120px" ID="cb_new" Height="50px" TabIndex="60" Width="50px" ClickAction="uo_mini_terminal_header\cb_new_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="���" Top="480px" Left="240px" ID="cb_delete" Height="50px" TabIndex="50" Width="50px" ClickAction="uo_mini_terminal_header\cb_delete_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="PDF" Top="89px" Font-Size="10px" Left="290px" ID="cb_pdf" Height="53px" TabIndex="20" Width="62px" ClickAction="uo_mini_terminal_header\cb_pdf_clicked">
    </vt:Button>
    
</vt:CompositeView>

