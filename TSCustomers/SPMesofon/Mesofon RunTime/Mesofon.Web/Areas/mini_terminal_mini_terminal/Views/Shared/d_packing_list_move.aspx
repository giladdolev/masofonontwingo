<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_packing_list_moveRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;לוג בוק&quot;" TemplateText="&quot;לוג בוק&quot;" Visible="True" Left="1px" Top="3px" Alignment="Center" ID="log_book_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Visible="True" Left="41px" Top="3px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך&quot;" TemplateText="&quot;תאריך&quot;" Visible="True" Left="105px" Top="3px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="156px" Top="3px" Alignment="Center" ID="number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="54px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="log_book" Visible="True" Left="1px" Top="1px" Alignment="Center" ID="log_book" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="supply_date" Visible="True" Left="107px" Top="2px" ID="supply_date" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="210px" Top="2px" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="259px" Top="2px" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="308px" Top="2px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discount" Visible="True" Left="357px" Top="2px" ID="discount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="mam" Visible="True" Left="406px" Top="2px" ID="mam" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="station_num" Visible="True" Left="455px" Top="2px" ID="station_num" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="store_number" Visible="True" Left="504px" Top="2px" ID="store_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="stock_number" Visible="True" Left="553px" Top="2px" ID="stock_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="expected_total_amount" Visible="True" Left="44px" Top="3px" ID="expected_total_amount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pack_list_number" Visible="True" Left="159px" Top="2px" ID="pack_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discount_percent" Visible="True" Left="602px" Top="2px" ID="discount_percent" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="price_list_number" Visible="True" Left="651px" Top="2px" ID="price_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="create_mode" Visible="True" Left="700px" Top="2px" ID="create_mode" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="van_sale" Visible="True" Left="749px" Top="2px" ID="van_sale" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="external_account_tx" Visible="True" Left="798px" Top="2px" ID="external_account_tx" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="external_tx_number" Visible="True" Left="847px" Top="2px" ID="external_tx_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packing_type" Visible="True" Left="898px" Top="2px" ID="packing_type" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pack_list_total" Visible="True" Left="947px" Top="2px" ID="pack_list_total" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="996px" Top="2px" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_saved" Visible="True" Left="1041px" Top="0px" ID="row_saved" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="declines" Visible="True" Left="1258px" Top="0px" ID="declines" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="payment_code_number" Visible="True" Left="1475px" Top="0px" ID="payment_code_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="25px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="payments_number" Visible="True" Left="1500px" Top="0px" ID="payments_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="25px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="1525px" Top="0px" ID="distributor_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="216px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_reference_number" Visible="True" Left="1741px" Top="0px" ID="b2b_reference_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="398px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

