<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dddw_mini_terminal_crossdoc_returns_shipmentsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Shipment Number&quot;" TemplateText="&quot;Shipment Number&quot;" Visible="True" Left="2px" Top="1px" ID="shipment_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="145px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" Format="[General]" DataMember="date_move" Visible="True" Left="150px" Top="1px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="32px" Width="142px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="shipment_number" Visible="True" Left="2px" Top="1px" ID="shipment_number" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="32px" Width="145px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

