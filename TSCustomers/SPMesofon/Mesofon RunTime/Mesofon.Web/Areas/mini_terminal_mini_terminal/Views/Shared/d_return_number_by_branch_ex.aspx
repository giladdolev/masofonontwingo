<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_number_by_branch_exRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Return Number&quot;" TemplateText="&quot;Return Number&quot;" Visible="True" Left="3px" Top="2px" Alignment="Center" ID="return_number_t" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="151px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewDropDownField runat="server" Visible="True" Left="3px" Top="2px" Alignment="Right" ID="return_number" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="151px"></vt:DataViewDropDownField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

