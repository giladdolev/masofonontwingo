<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_reportRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="153px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="16px" Top="5px" ID="date" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="66px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;דוח קליטת מסופון&quot;" TemplateText="&quot;דוח קליטת מסופון&quot;" Visible="True" Left="333px" Top="22px" Alignment="Center" ID="t_title" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="149px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:מס סניף&quot;" TemplateText="&quot;:מס סניף&quot;" Visible="True" Left="591px" Top="50px" Alignment="Right" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:ספק&quot;" TemplateText="&quot;:ספק&quot;" Visible="True" Left="591px" Top="79px" Alignment="Right" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="537px" Top="50px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="51px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="537px" Top="79px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="51px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם סניף&quot;" TemplateText="&quot;:שם סניף&quot;" Visible="True" Left="484px" Top="50px" Alignment="Right" ID="branch_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם עובד&quot;" TemplateText="&quot;:שם עובד&quot;" Visible="True" Left="245px" Top="50px" Alignment="Right" ID="employee_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="62px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:מס הזמנה&quot;" TemplateText="&quot;:מס הזמנה&quot;" Visible="True" Left="245px" Top="79px" Alignment="Right" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="62px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="employee_name" Visible="True" Left="156px" Top="50px" Alignment="Right" ID="employee_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="86px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="522px" Top="115px" Alignment="Center" ID="materials_barcode_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="109px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="635px" Top="115px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש הזמנה / מסופון&quot;" TemplateText="&quot;הפרש הזמנה / מסופון&quot;" Visible="True" Left="38px" Top="115px" Alignment="Center" ID="order_mt_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="185px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם פריט&quot;" TemplateText="&quot;שם פריט&quot;" Visible="True" Left="227px" Top="115px" Alignment="Center" ID="materials_name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="291px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הזמנה&quot;" TemplateText="&quot;הזמנה&quot;" Visible="True" Left="164px" Top="135px" Alignment="Center" ID="order1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מסופון&quot;" TemplateText="&quot;מסופון&quot;" Visible="True" Left="101px" Top="135px" Alignment="Center" ID="mt1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הפרש&quot;" TemplateText="&quot;הפרש&quot;" Visible="True" Left="38px" Top="135px" Alignment="Center" ID="diff1_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="3px" Top="115px" Alignment="Center" ID="ok_valid_months_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="36px" Width="31px"></vt:DataViewLabelField>
<vt:DataViewComboField runat="server" Visible="True" Left="86px" Top="79px" ID="doc_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="62px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="doc_no" Visible="True" Left="19px" Top="79px" ID="doc_no" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="62px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="310px" Top="50px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;:שם ספק&quot;" TemplateText="&quot;:שם ספק&quot;" Visible="True" Left="484px" Top="79px" Alignment="Right" ID="supplier_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="supplier_name" Visible="True" Left="311px" Top="79px" Alignment="Right" ID="supplier_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="171px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="152px" Top="79px" Alignment="Right" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="91px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="635px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="522px" Top="1px" Alignment="Center" ID="materials_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="109px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_name" Visible="True" Left="227px" Top="1px" Alignment="Right" ID="materials_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="291px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="164px" Top="1px" ID="order_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="mt_quantity" Visible="True" Left="101px" Top="1px" ID="mt_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="diff" Visible="True" Left="38px" Top="1px" ID="diff" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="59px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Left="38px" Top="0px" ID="compute" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="31px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;פ.ת&quot;" TemplateText="&quot;פ.ת&quot;" Left="3px" Top="0px" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="31px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

