<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_sql_mt_input_invoice_bodyRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;פריטי חשבונית&quot;" TemplateText="&quot;פריטי חשבונית&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="t_3" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="15px" Width="170px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="38px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" Format="####" DataMember="material_quantity" Visible="True" Left="2px" Top="21px" Alignment="Right" ID="material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="38px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="####" DataMember="expected_material_quantity" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="expected_material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="38px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;כ&quot;" TemplateText="&quot;כ&quot;" Visible="True" Left="43px" Top="2px" Alignment="Right" ID="material_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="6px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כ. בח-ן&quot;" TemplateText="&quot;כ. בח-ן&quot;" Visible="True" Left="43px" Top="21px" Alignment="Right" ID="t_2" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="209px" Top="2px" ID="compute_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="18px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="88px" Top="21px" ID="material_name_com" BorderStyle="None" Font-Names="Arial" Font-Size="7pt" ForeColor="#000000" Height="15px" Width="84px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם&quot;" TemplateText="&quot;שם&quot;" Visible="True" Left="176px" Top="21px" Alignment="Right" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="29px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="178px" Top="2px" Alignment="Right" ID="barcode_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="26px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="53px" Top="2px" Alignment="Right" ID="barcode" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="123px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

