<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_header_packRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;לוג בוק&quot;" TemplateText="&quot;לוג בוק&quot;" Visible="True" Left="1px" Top="3px" Alignment="Center" ID="log_book_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Visible="True" Left="41px" Top="3px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך&quot;" TemplateText="&quot;תאריך&quot;" Visible="True" Left="105px" Top="3px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="156px" Top="3px" Alignment="Center" ID="number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="54px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="log_book" Visible="True" Left="1px" Top="1px" Alignment="Center" ID="log_book" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="supply_date" Visible="True" Left="107px" Top="2px" ID="supply_date" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="210px" Top="0px" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="5px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="220px" Top="0px" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="437px" Top="0px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="146px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discount" Visible="True" Left="583px" Top="0px" ID="discount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="mam" Visible="True" Left="800px" Top="0px" ID="mam" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="station_num" Visible="True" Left="1017px" Top="0px" ID="station_num" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="store_number" Visible="True" Left="1234px" Top="0px" ID="store_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="25px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="stock_number" Visible="True" Left="1259px" Top="0px" ID="stock_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="216px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="expected_total_amount" Visible="True" Left="1475px" Top="0px" ID="expected_total_amount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="216px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discount_percent" Visible="True" Left="1691px" Top="0px" ID="discount_percent" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="price_list_number" Visible="True" Left="1908px" Top="0px" ID="price_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="create_mode" Visible="True" Left="2125px" Top="0px" ID="create_mode" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="5px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="van_sale" Visible="True" Left="2130px" Top="0px" ID="van_sale" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="15px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="external_account_tx" Visible="True" Left="2196px" Top="0px" ID="external_account_tx" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="external_tx_number" Visible="True" Left="2413px" Top="0px" ID="external_tx_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packing_list_move_pack_list_number" Visible="True" Left="2630px" Top="0px" ID="packing_list_move_pack_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packing_list_move_packing_type" Visible="True" Left="2847px" Top="0px" ID="packing_list_move_packing_type" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="5px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packing_list_move_pack_list_total" Visible="True" Left="2852px" Top="0px" ID="packing_list_move_pack_list_total" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

