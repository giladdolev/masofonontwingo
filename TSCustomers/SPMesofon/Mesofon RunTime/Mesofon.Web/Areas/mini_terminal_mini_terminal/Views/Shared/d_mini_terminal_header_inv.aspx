<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_header_invRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="29px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך&quot;" TemplateText="&quot;תאריך&quot;" Visible="True" Left="105px" Top="3px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="50px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ&quot;" TemplateText="&quot;סה&quot;כ&quot;" Visible="True" Left="41px" Top="3px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;לוג בוק&quot;" TemplateText="&quot;לוג בוק&quot;" Visible="True" Left="1px" Top="3px" Alignment="Center" ID="log_book_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="156px" Top="3px" Alignment="Center" ID="number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="69px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="53px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="b2b_status" Visible="True" Left="237px" Top="1px" ID="b2b_status" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="49px" Width="20px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="log_book" Visible="True" Left="1px" Top="2px" Alignment="Right" ID="log_book" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="supply_date" Visible="True" Left="108px" Top="2px" ID="supply_date" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_total" Visible="True" Left="52px" Top="0px" ID="invoice_total" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="51px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="220px" Top="1px" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="14px" Width="63px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="300px" Top="4px" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="10px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="335px" Top="2px" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="370px" Top="0px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="station_num" Visible="True" Left="468px" Top="3px" ID="station_num" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="192px" Top="27px" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="254px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

