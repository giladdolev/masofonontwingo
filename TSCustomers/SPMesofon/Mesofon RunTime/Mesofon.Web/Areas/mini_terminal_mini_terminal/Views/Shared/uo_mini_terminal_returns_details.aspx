<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_returns_details>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server"  Text="none" ID="uo_mini_terminal_returns_details" LoadAction="uo_mini_terminal_returns_details\Form_Load" BackColor="ButtonFace" Height="540px" Width="360px">


    <vt:Label runat="server" Text="����� ������ ������ ����"  TextAlign="MiddleCenter" Top="35px" Left="5px" Visible="true" ID="st_carton" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="56px" Width="581px">
	</vt:Label>
     <vt:Label runat="server" Text="���� �����" TextAlign="MiddleCenter" Top="10px" Left="5px" Visible="true" ID="return_number_t" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="18px" Width="160px">
	</vt:Label>
     <vt:Label runat="server" Text="" Top="10px" TextAlign="MiddleCenter" Left="212px" Visible="true" ID="st_shipment_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="18px" Width="170px">
	</vt:Label>
     <vt:Label runat="server" Text="" Top="35px" TextAlign="MiddleCenter" Left="231px" Visible="true" ID="st_carton_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="18px" Width="140px">
	</vt:Label>
    

	<vt:WidgetGrid runat="server" Top="60px" RowHeaderVisible="false" ID="uo_mini_terminal_returns_details_dw_inv_pack_details" Height="325px" TabIndex="30000" Width="350px"
        SelectionChangedAction="uo_mini_terminal_returns_details\dw_inv_pack_details_rowfocuschanging"
        WidgetControlClickAction="uo_mini_terminal_returns_details\dw_inv_pack_details_clicked"
        WidgetControlItemChangedAction="uo_mini_terminal_returns_details\dw_inv_pack_details_itemchanged"
        CellkeyDownAction="uo_mini_terminal_returns_details\dw_inv_pack_details_CellkeyDown"
        WidgetTemplateModel="vm-spviewmodelReturnsDetails"
        WidgetTemplateDataAttachMethod ="VT_WidgetColumnAttachReturnsDetails"
        WidgetTemplate="spgridcomponentReturnsDetails"
        WidgetTemplateDataMembers="row_no,barcode,material_name,material_quantity,supplier_number,declines,invoice_data"
        WidgetTemplateWidth ="335">
	</vt:WidgetGrid>
    <vt:Button runat="server" Text="" Visible="true" Top="-100px" Left="10px" ID="ButtonPreventAutomaticScrolling" Height="1px" TabIndex="30000" Width="91px">
	</vt:Button>

	<vt:Grid runat="server" Top="45px" Left="135px" ID="dw_summary_details" RowHeaderVisible="false" Visible="false" ReadOnly="true" Height="100px"  TabIndex="31001" Width="220px" SortableColumns="false">
	</vt:Grid>
	<vt:Button runat="server" Text="����� ����" Top="390px" ClickAction="uo_mini_terminal_returns_details\cb_delete_item_clicked" Left="161px" ID="cb_delete_item" Height="47px" TabIndex="31002" Width="91px">
	</vt:Button>
    <vt:Button runat="server" Text="����" ClickAction="uo_mini_terminal_returns_details\cb_exit_clicked" Top="390px" Left="256px" ID="cb_exit" Height="47px" TabIndex="31003" Width="91px">
	</vt:Button>
</vt:CompositeView>

