<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_trade_order_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מספר הזמנת אתר סחר&quot;" TemplateText="&quot;מספר הזמנת אתר סחר&quot;" Visible="True" Left="102px" Top="0px" Alignment="Center" ID="trade_order_number_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="32px" Width="104px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;אישור&quot;" TemplateText="&quot;אישור&quot;" Visible="True" Left="68px" Top="0px" Alignment="Center" ID="approve_ind_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="32px" Width="32px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך הזמנה&quot;" TemplateText="&quot;תאריך הזמנה&quot;" Visible="True" Left="0px" Top="0px" Alignment="Center" ID="t_1" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="32px" Width="66px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="ButtonFace" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="site_order_number" Visible="True" Left="102px" Top="0px" ID="site_order_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="104px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="date_move" Visible="True" Left="0px" Top="0px" ID="date_move" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="66px"></vt:DataViewTextField>
<vt:DataViewCheckField runat="server" Visible="True" Left="68px" Top="0px" Alignment="Center" ID="approve_ind" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="32px"></vt:DataViewCheckField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

