<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_summary_inv_palletRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="14px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ כמות&quot;" TemplateText="&quot;סה&quot;כ כמות&quot;" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="49px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&#39; מסמך&quot;" TemplateText="&quot;מס&#39; מסמך&quot;" Visible="True" Left="53px" Top="1px" Alignment="Center" ID="invoice_move_invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="46px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="7px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="15px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="total" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="total" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="49px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doc_no" Visible="True" Left="53px" Top="1px" Alignment="Center" ID="doc_no" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="46px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

