<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_pallets_list>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="none" ID="uo_mini_terminal_pallets_list" BackColor="ButtonFace" Height="640px" Width="360px" LoadAction="uo_mini_terminal_pallets_list\Form_Load">
    <vt:ComboGrid runat="server"  ExpandOnFocus="true" Top="41px" ValueMember="shipment_number" DisplayMember="shipment_number" Left="120px" Height="40px" Width="215px" TabIndex="10" ID="dw_shipments_list" SelectedIndexChangeAction="uo_mini_terminal_pallets_list\Combobox_shipments_list_itemchanged" TextChangedAction="uo_mini_terminal_pallets_list\dw_shipments_list_editchanged" ShowHeader="false">
    </vt:ComboGrid>
    <vt:Grid runat="server" RowHeaderVisible="false" ScrollBars="Both" ReadOnly="true" Top="109px" Left="10px" ID="dw_pallets_list" Height="250px" TabIndex="20" Width="341px" CellContentClickAction="uo_mini_terminal_pallets_list/dw_pallets_list_clicked" SortableColumns="false" ClientEventAction="uo_mini_terminal_pallets_list/dw_pallets_list_buttonclicked">
    </vt:Grid>
    <vt:Label runat="server" Text="���� ����� �����&quot;�" Top="10px" Left="8px" ID="st_title" BackColor="#A6CAF0" Font-Names="Tahoma" Font-Size="15pt" ForeColor="WindowText" Height="30px" Width="230px">
    </vt:Label>
    <vt:Label runat="server" Text="���� �����:" Top="47px" Left="8px" ID="st_shipment_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="14pt" ForeColor="WindowText" Height="25px" Width="90px">
    </vt:Label>
    <vt:Label runat="server" Text="������:" Top="87px" Left="8px" ID="st_pallets_list" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" ForeColor="WindowText" Height="25px" Width="70px">
    </vt:Label>
    <vt:Button runat="server" Text="���� ��� ���" Top="362px" Left="8px" ID="cb_print" Height="40px" TabIndex="30" Width="100px" ClickAction="uo_mini_terminal_pallets_list\cb_print_clicked">
    </vt:Button>
    <vt:Button runat="server" Text="����" Top="362px" Left="270px" ID="cb_finish" Height="40px" TabIndex="40" Width="80px" ClickAction="uo_mini_terminal_pallets_list\cb_finish_clicked">
    </vt:Button>
    <vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="362px" Left="115px" CssClass="inputtypetext" ID="sle_scan_line" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="40px" TabIndex="50" Width="150px" LostFocusAction="uo_mini_terminal_pallets_list\sle_scan_line_losefocus" GotFocusAction="uo_mini_terminal_pallets_list\sle_scan_line_getfocus" TextChangedAction="uo_mini_terminal_pallets_list\scan_line_enter" KeyDownAction="uo_mini_terminal_pallets_list\sle_scan_line_ue_enter">
    </vt:TextBox>

</vt:CompositeView>

