<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_driver_report_mainRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Header" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Footer" ID="footer"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Left="12px" Top="3px" ID="page_1" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="116px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="5px" Top="3px" ID="page_2" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="116px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="151px" ForeColor="#000000" Type="Details" ID="detail"></vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

