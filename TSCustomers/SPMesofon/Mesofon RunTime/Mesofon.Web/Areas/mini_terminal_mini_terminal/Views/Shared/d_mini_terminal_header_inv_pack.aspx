<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_header_inv_packRepository>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
    <vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
        <Bands>
            <vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Header" ID="Header">
                <Fields>
                    <vt:DataViewLabelField runat="server" Text="��&#39; �����" TemplateText="��&#39; �����" Visible="True" Left="0px" Alignment="Center" ID="reference_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="63px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="���" TemplateText="���" Visible="True" Left="65px" Alignment="Center" ID="total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="43px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="�����" TemplateText="�����" Visible="True" Left="108px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="48px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="����" TemplateText="����" Visible="True" Left="157px" Alignment="Center" ID="number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="51px"></vt:DataViewLabelField>
                    <vt:DataViewLabelField runat="server" Text="���" TemplateText="���" Visible="True" Left="209px" Alignment="Center" ID="doc_type_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="51px"></vt:DataViewLabelField>
                </Fields>
            </vt:DataViewBand>
            <vt:DataViewBand runat="server" Height="17px" ForeColor="#000000" Type="Details" ID="detail1111">
                <Fields>
                    <vt:DataViewTextField runat="server" DataMember="b2b_reference_number" Visible="True" Left="0px" Top="2px" Alignment="Right" ID="b2b_reference_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="63px"></vt:DataViewTextField>
                    <vt:DataViewTextField runat="server" DataMember="total" Visible="True" Left="65px" Top="2px" Alignment="Center" ID="total" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="43px"></vt:DataViewTextField>
                    <vt:DataViewTextField runat="server" Format="[shortdate]" DataMember="supply_date" Visible="True" Left="108px" Top="2px" Alignment="Center" ID="supply_date" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="20px" Width="48px"></vt:DataViewTextField>
                    <vt:DataViewTextField runat="server" DataMember="doc_type" Visible="True" Left="209px" Top="2px" Alignment="Right" ID="doc_type" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="51px"></vt:DataViewTextField>
                    <vt:DataViewTextField runat="server" DataMember="number" Visible="True" Left="157px" Top="2px" Alignment="Right" ID="number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="20px" Width="51px"></vt:DataViewTextField>
                </Fields>
            </vt:DataViewBand>
        </Bands>
    </vt:DataView>
</vt:ControlView>

