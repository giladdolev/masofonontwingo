<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.rw_item_quantity>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" StartPosition="CenterScreen" Opacity="1" Text="���� �� ����" Resizable="True" ID="rw_item_quantity" LoadAction="rw_item_quantity\Form_Load" BackColor="ButtonFace" Height="79px" Width="176px">
		<vt:Button runat="server" Text="�����" Top="33px" Left="97px" ID="cb_cancel" ClickAction="rw_item_quantity\cb_cancel_clicked" Height="21px" TabIndex="20" Width="52px">
		</vt:Button>
		<vt:Label runat="server" Text="���� �� �����" Top="10px" Left="20px" ID="quantity_t" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="99px">
		</vt:Label>
		<vt:Button runat="server" Text="�����" Top="33px" Left="20px" ID="cb_ok" ClickAction="rw_item_quantity\cb_ok_clicked" Height="21px" TabIndex="20" Width="52px">
		</vt:Button>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="2" Text="0" Top="8px" Left="120px" ID="sle_quantity" BackColor="#FFFFFF" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="21px" TabIndex="10" Width="48px">
		</vt:TextBox>
	</vt:WindowView>

</asp:Content>
