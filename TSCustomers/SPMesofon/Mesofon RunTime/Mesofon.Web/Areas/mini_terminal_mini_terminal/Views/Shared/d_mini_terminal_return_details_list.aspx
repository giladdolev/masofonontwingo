<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_return_details_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Serial Number&quot;" TemplateText="&quot;Serial Number&quot;" Visible="True" Left="3px" Top="2px" Alignment="Center" ID="serial_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="82px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="88px" Top="2px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Invoice Number&quot;" TemplateText="&quot;Invoice Number&quot;" Visible="True" Left="181px" Top="2px" Alignment="Center" ID="invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Material Number&quot;" TemplateText="&quot;Material Number&quot;" Visible="True" Left="272px" Top="2px" Alignment="Center" ID="material_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="95px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Material Quantity&quot;" TemplateText="&quot;Material Quantity&quot;" Visible="True" Left="370px" Top="2px" Alignment="Center" ID="material_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="99px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="472px" Top="2px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="96px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Decline Number&quot;" TemplateText="&quot;Decline Number&quot;" Visible="True" Left="571px" Top="2px" Alignment="Center" ID="decline_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="92px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Name&quot;" TemplateText="&quot;Name&quot;" Visible="True" Left="665px" Top="1px" ID="name_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="294px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Barcode&quot;" TemplateText="&quot;Barcode&quot;" Visible="True" Left="961px" Top="1px" ID="barcode_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="117px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Date Move&quot;" TemplateText="&quot;Date Move&quot;" Visible="True" Left="1080px" Top="1px" ID="date_move_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Material Price&quot;" TemplateText="&quot;Material Price&quot;" Visible="True" Left="1548px" Top="1px" ID="material_price_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Material Price After Discount&quot;" TemplateText="&quot;Material Price After Discount&quot;" Visible="True" Left="2016px" Top="1px" ID="material_price_after_discount_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="serial_number" Visible="True" Left="3px" Top="2px" Alignment="Right" ID="serial_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="82px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="88px" Top="2px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="181px" Top="2px" Alignment="Right" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="88px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_number" Visible="True" Left="272px" Top="2px" Alignment="Right" ID="material_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="95px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="370px" Top="2px" Alignment="Right" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="99px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="472px" Top="2px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="96px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="decline_number" Visible="True" Left="571px" Top="2px" Alignment="Right" ID="decline_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="92px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="name" Visible="True" Left="666px" Top="0px" ID="name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="293px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="962px" Top="0px" ID="barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="116px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="1081px" Top="0px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price" Visible="True" Left="1549px" Top="0px" ID="material_price" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price_after_discount" Visible="True" Left="2017px" Top="0px" ID="material_price_after_discount" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

