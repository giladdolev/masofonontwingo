<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dwh_return_details_per_cartonRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="1px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="material_number" Visible="True" Left="115px" Top="4px" ID="material_number" Font-Names="Tahoma" Font-Size="9pt" ForeColor="#000000" Height="14px" Width="141px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="carton_barcode" Visible="True" Left="8px" Top="3px" ID="carton_barcode" Font-Names="Tahoma" Font-Size="9pt" ForeColor="#000000" Height="14px" Width="101px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="264px" Top="2px" ID="cc_carton" BorderStyle="None" Font-Names="Tahoma" Font-Size="9pt" ForeColor="#000000" Height="14px" Width="57px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

