<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_returns_headRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="43px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="debit_credit_number" Visible="True" Left="4px" Top="3px" ID="debit_credit_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="133px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="4px" Top="23px" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="133px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="146px" Top="3px" ID="invoice_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="109px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="146px" Top="23px" ID="supplier_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="109px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="serial_number" Visible="True" Left="263px" Top="3px" ID="serial_number" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="94px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supply_date" Visible="True" Left="263px" Top="23px" ID="supply_date" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="94px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

