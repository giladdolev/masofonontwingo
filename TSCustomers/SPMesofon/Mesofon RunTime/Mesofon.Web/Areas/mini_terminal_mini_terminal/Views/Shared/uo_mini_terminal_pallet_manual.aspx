<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<mini_terminal.uo_mini_terminal_pallet_manual>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:CompositeView runat="server" Text="none" ID="uo_mini_terminal_pallet_manual" BackColor="ButtonFace" Height="550px" Width="360px"  LoadAction="uo_mini_terminal_pallet_manual\Form_Load" >
	<vt:Label runat="server" Text="����� ������ ����� ����" Top="10px" Left="5px" ID="st_order" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="30px" Width="236px">
	</vt:Label>
	<vt:Label runat="server"  Top="10px" Left="237px" ID="st_order_number" BackColor="#A6CAF0" Font-Names="Arial" Font-Size="13pt" Height="30px" Width="75px">
	</vt:Label>
	<vt:WidgetGrid runat="server" RowHeaderVisible="false" ScrollBars="Both"  Top="42px" ID="uo_mini_terminal_pallet_manual_dw_inv_pack_details" 
          Height="325px" 
        CellkeyDownAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_CellkeyDown"
        BeforeSelectionChangedAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_rowfocuschanging"
        SelectionChangedAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_rowfocuschanged"
        WidgetControlClickAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_clicked"
        WidgetControlEditChangedAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_EditChanged"
        WidgetControlDoubleClickAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_doubleclicked" 
        WidgetControlItemChangedAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_itemchanged"
        WidgetControlFocusEnterAction="uo_mini_terminal_pallet_manual\dw_inv_pack_details_itemfocuschanged"
        WidgetTemplateModel="vm-spviewmodelPalletManual"
        WidgetTemplateDataAttachMethod ="VT_WidgetColumnAttachPalletManual"
        WidgetTemplate="spgridcomponentPalletManual"
        WidgetTemplateDataMembers="row_no,barcode,material_name,expected_material_quantity,order_quantity,invoice_pack_quantity,doc_no_en,doc_no,invoice_data,declines"
        WidgetTemplateWidth ="335"
        TabIndex="10" Width="350px" SortableColumns="false">
	</vt:WidgetGrid>
    <vt:Button runat="server" Text="��� ������ �� ������" Top="372px" Left="0px" ID="cb_show_all" Height="47px" TabIndex="40" Width="180px" ClickAction="uo_mini_terminal_pallet_manual\cb_show_all_clicked" >
	</vt:Button>
    <vt:Button runat="server" Text="����� ����" Top="372px" Left="184px" ID="cb_delete_item" Enabled="False" Height="47px" TabIndex="50" Width="93px" ClickAction="uo_mini_terminal_pallet_manual\cb_delete_item_clicked">
	</vt:Button>
    <vt:Button runat="server" Text="����" Top="372px" ID="cb_finish" ClickAction="uo_mini_terminal_pallet_manual\cb_finish_clicked" Left="280px" Enabled="False" Height="47px" TabIndex="20" Width="71px">
	</vt:Button>
    <vt:Grid runat="server" ReadOnly="true" RowHeaderVisible="false" Top="426px" ID="uo_mini_terminal_pallet_manual_dw_summary" Left="160px" Height="120px" TabIndex="20" Width="190px" SortableColumns="false">
	</vt:Grid>
    <vt:Button runat="server" Text="���� ������" Top="475px" Left="0px" ID="cb_return_to_header" ClickAction="uo_mini_terminal_pallet_manual\cb_return_to_header_clicked" Height="47px" TabIndex="30" Width="123px">
	</vt:Button>
    <vt:Button runat="server" Text="��� ����" Top="422px" Left="0px" ID="cb_item_diff" Height="49px" TabIndex="40" Width="75px" ClickAction="uo_mini_terminal_pallet_manual\cb_item_diff_clicked">
	</vt:Button>
    <vt:Button runat="server" Text="�� �����" Top="422px" Left="80px" ID="cb_not_scanned_items" Height="49px" TabIndex="40" Width="75px" ClickAction="uo_mini_terminal_pallet_manual\cb_not_scanned_items_clicked">
	</vt:Button>
	<vt:Button runat="server" Text="��&quot;� ������" Top="224px" Left="158px" Visible="False" ID="cb_def_report" Enabled="False" Height="28px" TabIndex="30" Width="75px">
	</vt:Button>
</vt:CompositeView>

