<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_shipment_invoicesRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="94px" Top="2px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="96px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Invoice Number&quot;" TemplateText="&quot;Invoice Number&quot;" Visible="True" Left="192px" Top="2px" Alignment="Center" ID="invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Order Number&quot;" TemplateText="&quot;Order Number&quot;" Visible="True" Left="282px" Top="2px" Alignment="Center" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="81px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Pallet Number&quot;" TemplateText="&quot;Pallet Number&quot;" Visible="True" Left="365px" Top="1px" ID="pallet_number_t" BorderStyle="None" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="117px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="94px" Top="2px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="96px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="192px" Top="2px" Alignment="Right" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="88px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="282px" Top="2px" Alignment="Right" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="81px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pallet_number" Visible="True" Left="366px" Top="0px" ID="pallet_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="116px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

