<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.w_mini_terminal>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Opacity="1" Tag="nmt" Resizable="false" WaitMaskDisabled="false" ID="w_mini_terminal" BackColor="ButtonFace" Height="640px" Width="360px" LoadAction="w_mini_terminal\Form_Load" AfterrenderAction="w_mini_terminal\Form_AfterRender" WindowClosingAction="w_mini_terminal\w_mini_terminal_WindowClosing">
        <vt:Panel ID="pnlHeader" runat="server" Height="13px" Top="0px">
            <vt:Label runat="server" RightToLeft="Yes" RightToLeftDecoration="true" Text="" ID="LblTitle" Height="13px" Width="50px"></vt:Label>
        </vt:Panel>
        <vt:Panel ID="pnlContent" runat="server" Height="626px" Top="14px" AutoScroll="true" BackColor="ButtonFace">
            <vt:PartialControl runat="server" Visible="false" Controller="uo_mini_terminal_header" Action="uo_mini_terminal_header" Area="mini_terminal_mini_terminal" Tag="" Text="none" ID="uo_header" BackColor="ButtonFace" Height="626px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Visible="false" Controller="uo_mini_terminal_returns" Action="uo_mini_terminal_returns" Area="mini_terminal_mini_terminal" Text="none" ID="uo_returns" BackColor="ButtonFace" Height="626px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Controller="uo_mini_terminal_entrance" Action="uo_mini_terminal_entrance" Area="mini_terminal_mini_terminal" Text="none" ID="uo_entrance" BackColor="ButtonFace" Height="626px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            <%-- 
            <vt:PartialControl runat="server"  Visible="false" Controller="uo_mini_terminal_body" Action="uo_mini_terminal_body" Area="mini_terminal_mini_terminal" ID="uo_body" BackColor="ButtonFace" Height="640px" TabIndex="60" Width="360px">
            </vt:PartialControl>
               <vt:PartialControl runat="server" Visible="false" Controller="uo_mini_terminal_pallet_manual" Action="uo_mini_terminal_pallet_manual" Area="mini_terminal_mini_terminal" Text="none" ID="uo_pallet_manual" BackColor="ButtonFace" Height="640px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Visible="false" Controller="uo_mini_terminal_trans_from_branch" Action="uo_mini_terminal_trans_from_branch" Area="mini_terminal_mini_terminal" Text="none" ID="uo_trans_from_branch" BackColor="ButtonFace" Height="640px" TabIndex="50" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Visible="false" Controller="uo_mini_terminal_trans_to_branch" Action="uo_mini_terminal_trans_to_branch" Area="mini_terminal_mini_terminal" Text="none" ID="uo_trans_to_branch" BackColor="ButtonFace" Height="640px" TabIndex="30" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server"  Visible="false"  Controller="uo_mini_terminal_crossdoc_returns_details" Action="uo_mini_terminal_crossdoc_returns_details" Area="mini_terminal_mini_terminal" Text="none" ID="uo_crossdoc_returns_details" BackColor="ButtonFace" Height="640px" TabIndex="30" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Visible="false"  Controller="uo_mini_terminal_crossdoc_returns" Action="uo_mini_terminal_crossdoc_returns" Area="mini_terminal_mini_terminal" Text="none" ID="uo_crossdoc_return" BackColor="ButtonFace" Height="640px" TabIndex="70" Width="219px">
            </vt:PartialControl>
            <vt:PartialControl runat="server" Visible="false"  Controller="uo_mini_terminal_returns_details" Action="uo_mini_terminal_returns_details" Area="mini_terminal_mini_terminal" Text="none" ID="uo_returns_details" BackColor="ButtonFace" Height="640px" TabIndex="30" Width="360px">
            </vt:PartialControl>
           
            <vt:PartialControl runat="server" Visible="false"  Controller="uo_mini_terminal_trade_site" Action="uo_mini_terminal_trade_site" Area="mini_terminal_mini_terminal" Text="none" ID="uo_trade_site" BackColor="ButtonFace" Height="640px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            <vt:PartialControl runat="server"  Visible="false" Controller="uo_mini_terminal_crossdoc_pack_list" Action="uo_mini_terminal_crossdoc_pack_list" Area="mini_terminal_mini_terminal" Text="none" ID="uo_crossdoc_list" BackColor="ButtonFace" Height="640px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            
            <vt:PartialControl runat="server"  Visible="false" Controller="uo_mini_terminal_pallets_list" Action="uo_mini_terminal_pallets_list" Area="mini_terminal_mini_terminal" Text="none" ID="uo_shipments_list" BackColor="ButtonFace" Height="640px" TabIndex="20" Width="360px">
            </vt:PartialControl>
            --%>
        </vt:Panel>
    </vt:WindowView>

</asp:Content>
