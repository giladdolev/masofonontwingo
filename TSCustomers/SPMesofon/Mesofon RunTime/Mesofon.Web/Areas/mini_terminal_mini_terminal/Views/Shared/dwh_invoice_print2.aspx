<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dwh_invoice_print2Repository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="25px" ForeColor="#000000" Type="Footer" ID="footer"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="3px" Top="1px" ID="page_2" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="22px" Width="285px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="73px" ForeColor="#000000" Type="Details" ID="detail"></vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

