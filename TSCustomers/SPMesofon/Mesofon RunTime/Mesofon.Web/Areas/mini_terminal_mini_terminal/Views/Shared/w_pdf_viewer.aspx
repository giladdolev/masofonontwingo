<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.w_pdf_viewer>" %>

<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">

    <vt:WindowView runat="server" Opacity="1" Resizable="false" ID="w_pdf_viewer" BackColor="ButtonFace" Height="500px" Width="300px" LoadAction="w_pdf_viewer\Form_Load">
         <vt:Panel runat="server" Top="0" Left="0" ID="Panel1" Dock="Fill"></vt:Panel>
        <vt:Button runat="server"  Top="320px" Left="140px" ID="btnClose" Text="����" Dock="Bottom" ClickAction="w_pdf_viewer\btnClose_Click"></vt:Button>
    </vt:WindowView>

</asp:Content>
