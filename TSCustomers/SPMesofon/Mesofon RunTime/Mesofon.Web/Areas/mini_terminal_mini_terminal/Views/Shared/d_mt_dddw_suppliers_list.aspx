<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mt_dddw_suppliers_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Number&quot;" TemplateText="&quot;Number&quot;" Visible="True" Left="171px" Top="8px" Alignment="Center" ID="number_t" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="61px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Name&quot;" TemplateText="&quot;Name&quot;" Visible="True" Left="67px" Top="8px" Alignment="Center" ID="name_t" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="44px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="name" Visible="True" Left="2px" Top="3px" Alignment="Right" ID="name" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="82px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

