﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<masofon.Login>" %>

<%@ Import Namespace="System.Web.Mvc.Html" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, user-scalable=0" />
    <title>Masofon</title>
    <style>
        body {
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            font-family: 'Roboto',sans-serif;
            padding: 0;
            background-color: #F7F7F7;
        }

        .login-card {
            width: 100%;
            height: 100%;
            border-radius: 2px;
            overflow: hidden;
        }

            .login-card h1 {
                text-align: center;
                font-size: 3.3em;
            }

        .input {
            width: 100%;
            font-size: 3.3em;
            -webkit-appearance: none;
            background: #fff;
            border: 1px solid #d9d9d9;
            border-top: 1px solid silver;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            direction: rtl;
        }

        .InputListBox {
            width: 100%;
            font-size: 3.3em;
            -webkit-appearance: none;
            background: #fff;
            border: 1px solid #d9d9d9;
            border-top: 1px solid silver;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            direction: rtl;
        }

            .InputListBox > option:checked {
                background-color: #ffffd0 !important;
            }

        .login-card input[type=submit] {
            width: 100%;
            margin-top: 5%;
            height: 100%;
            display: block;
            position: relative;
            font-size: 3.3em;
        }

        .validation1 {
            direction: rtl;
            color: red;
            font-size: 30px;
        }

        .validation2 {
            color: red;
            font-size: 30px;
        }

        .login-submit {
            border: 0;
            color: #fff;
            height: 70px;
            text-shadow: 0 1px rgba(0,0,0,0.1);
            background-color: #4d90fe;
        }

        .vt-tabpanel-multiline .x-box-inner {
            height: auto;
        }

        .vt-tabpanel-multiline .x-box-target {
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            -webkit-flex-wrap: wrap;
            height: auto;
        }

        .vt-tabpanel-multiline .x-box-item {
            position: static !important;
        }

        .vt-mask {
            background-color: transparent;
            z-index: 100;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .leaf {
            background-image: url(images/tree/elbow-minus.gif) !important;
        }

        .expanded {
            background-image: url(images/tree/elbow-plus.gif) !important;
        }

        .AppContentStyle {
            width: 2000px;
            height: 2000px;
            overflow: auto;
        }

        .x-form-type-checkbox * {
            text-decoration: inherit;
        }

        .x-form-type-radio * {
            text-decoration: inherit;
        }

        .x-panel-header-title-default {
            color: inherit;
        }

        .x-btn, .x-btn-button, .x-btn-inner {
            white-space: pre-line;
            word-break: break-all;
        }

        label, label.x-component {
            white-space: pre-line;
            word-break: break-all !important;
        }

        .x-column-header-inner {
            background: #add8e6 !important;
        }

        .x-form-trigger-wrap-default {
            border-width: 1px !important;
            border-style: solid !important;
            border-color: #000 !important;
        }

        #gridpanel-1414 {
            width: 140px !important;
        }

            #gridpanel-1414 .x-grid-cell {
                width: 140px !important;
            }

        .x-tool-close {
            display: none !important;
        }

        .x-tool-maximize {
            display: none !important;
        }

        .x-tool-minimize {
            display: none !important;
        }

        .x-panel-body-default {
            border-width: 0 !important;
            margin: 0 !important;
        }

        .x-grid-checkcolumn {
            opacity: 1 !important;
        }

        .GridHtmlContentColumn {
            direction: rtl !important;
        }

        .x-grid-cell-inner {
            text-align: center !important;
        }

        .x-column-header-inner {
            text-align: center !important;
        }

        .x-form-text-field-body-default {
            direction: rtl !important;
        }

        .label {
            direction: rtl !important;
            text-align: right !important;
        }

        .FalseFontWeightExpressionDeclineAttribute * {
            font-weight: 400 !important;
        }

        .TrueFontWeightExpressionDeclineAttribute * {
            font-weight: 700 !important;
        }

        .labels1 {
            width: 30px !important;
            padding-top: 15px !important;
            height: 50px !important;
            font-size: 18px !important;
            font-weight: 300;
            font-family: helvetica,arial,verdana,sans-serif !important;
            text-align: center !important;
            background-color: #d3d3d3 !important;
        }

        .labels {
            width: 20px !important;
            height: 24px !important;
            font-size: 13px !important;
            font-weight: 300;
            font-family: helvetica,arial,verdana,sans-serif !important;
            text-align: center !important;
            background-color: #add8e6 !important;
        }

        .btnGo {
            background-image: url(images/edit.png) !important;
            background-repeat: no-repeat;
        }

        .x-grid-cell-GridCustomColumnProperty > .x-grid-cell-inner {
            padding: 3px 1px !important;
        }

        .WidgetColumnLabelCssClass {
            background: #add8e6;
            width: 20px !important;
            height: 24px !important;
        }

        .WidgetColumnLabelRowNoCssClass {
            background: #d3d3d3;
            width: 30px !important;
            height: 50px !important;
        }

        .WidgetColumnDisableCssClass {
            background: #d3d3d3;
        }

        .WidgetColumnLabelRightToLeft {
            border-bottom: 1px solid #000;
        }

        .WidgetColumnLabelNoWrap {
            white-space: nowrap !important;
        }

        .x-grid-row {
            line-height: 25px;
        }

        .pallet_manual_backColor {
            background: #f0caa6 !important;
        }

        .key {
            display: inline-block;
            color: #444;
            border-radius: 5px;
            border: 1px solid #CCC;
            background: #DDD;
            box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
            cursor: pointer;
            vertical-align: middle;
            max-width: 100px;
            padding: 10px;
            margin: 5px;
            text-align: center;
        }

            .key:active {
                background: #9e9e9e;
                box-shadow: 0 0 5px -1px rgba(0,0,0,0.6);
            }

        .cellcss * {
            font-weight: 700 !important;
        }

        .cellcss1 * {
            font-weight: 100 !important;
        }

        .x-grid-checkcolumn.undefined::after {
            background-color: gray;
            background-image: none;
        }

        .keyboardButton {
            padding-top: 5px;
            padding-left: 5px;
        }

        .x-form-field[readonly] {
            background-color: lightgray !important;
        }

        .x-resizable-handle-west {
            visibility: hidden !important;
        }
    </style>
</head>
<body>
    <form runat="server" method="post">
        <div class="login-card">
            <h1>כניסה למסופון</h1>
            <asp:DropDownList runat="server" ID="BranchNumber" CssClass="InputListBox">
                <asp:ListItem Selected="True" Value="1">סניף 1</asp:ListItem>
                <asp:ListItem Value="6">סניף 6</asp:ListItem>
                <asp:ListItem Value="7">סניף 7</asp:ListItem>
                <asp:ListItem runat="server" Value="142">סניף 142</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <br />
            <asp:DropDownList runat="server" ID="StationNumber" CssClass="InputListBox">
                <asp:ListItem Selected="True" Value="2">תחנה 2</asp:ListItem>
                <asp:ListItem Value="1">תחנה 1</asp:ListItem>
                <asp:ListItem Value="11">1תחנה 1</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" CssClass="validation2"
                ControlToValidate="UserId"
                ErrorMessage="נדרש שם משתמש.">*
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="UserId" runat="server" CssClass="input" placeholder="ת.ז">741</asp:TextBox>
            <asp:RequiredFieldValidator runat="server" CssClass="validation2"
                ControlToValidate="password"
                ErrorMessage="נדרש סיסמה."> *
            </asp:RequiredFieldValidator>
            <asp:TextBox Text="741" ID="password" runat="server" CssClass="input" placeholder="סיסמה">741</asp:TextBox>
            <asp:Button ID="btnLogin" runat="server" class="login login-submit" Text="כניסה" />

            <asp:ValidationSummary runat="server" CssClass="validation1"
                HeaderText="שגיאות:" />
        </div>
    </form>
</body>
</html>
