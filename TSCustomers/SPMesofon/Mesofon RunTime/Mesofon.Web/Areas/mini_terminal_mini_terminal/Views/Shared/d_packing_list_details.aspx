<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_packing_list_detailsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="29px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="290px" Top="1px" Alignment="Center" ID="num_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="264px" Top="1px" ID="t_1" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="178px" Top="1px" ID="materials_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="84px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ה&quot;" TemplateText="&quot;ה&quot;" Visible="True" Left="98px" Top="1px" Alignment="Center" ID="t_6" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מ&quot;" TemplateText="&quot;מ&quot;" Visible="True" Left="138px" Top="1px" Alignment="Center" ID="expected_material_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="t_9" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="54px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ח&quot;" TemplateText="&quot;ח&quot;" Visible="True" Left="58px" Top="1px" Alignment="Center" ID="invoice_details_invoice_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="27px" Width="38px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="120px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="290px" Top="1px" ID="compute_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="178px" Top="1px" Alignment="Center" ID="materials_barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="84px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;קוד&quot;" TemplateText="&quot;קוד&quot;" Visible="True" Left="264px" Top="1px" ID="t_2" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם&quot;" TemplateText="&quot;שם&quot;" Visible="True" Left="264px" Top="16px" ID="code_desc_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="13px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_material_name" Visible="True" Left="178px" Top="16px" ID="materials_material_name" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="13px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="98px" Top="1px" Alignment="Center" ID="order_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="28px" Width="38px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="pack_list_number" Visible="True" Left="314px" Top="0px" ID="pack_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="94px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="expected_material_quantity" Visible="True" Left="467px" Top="6px" ID="expected_material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="129px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_status" Visible="True" Left="2px" Top="1px" ID="b2b_status" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="35px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_no" Visible="True" Left="418px" Top="5px" ID="row_no" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="48px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doc_no_en" Visible="True" Left="140px" Top="2px" ID="doc_no_en" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="43px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_number" Visible="True" Left="4px" Top="22px" ID="material_number" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="88px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="44px" Top="1px" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price" Visible="True" Left="290px" Top="18px" ID="material_price" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price_after_discount" Visible="True" Left="151px" Top="42px" ID="material_price_after_discount" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_discount_percent" Visible="True" Left="31px" Top="42px" ID="material_discount_percent" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="sell_price" Visible="True" Left="87px" Top="75px" ID="sell_price" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="current_catalog_sell_price" Visible="True" Left="241px" Top="73px" ID="current_catalog_sell_price" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="indicator" Visible="True" Left="272px" Top="41px" ID="indicator" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="21px" Top="67px" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="mini_terminal" Visible="True" Left="68px" Top="72px" ID="mini_terminal" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="color" Visible="True" Left="144px" Top="21px" ID="color" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="cng_doc_count" Visible="True" Left="308px" Top="40px" ID="cng_doc_count" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="80px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doc_state" Visible="True" Left="356px" Top="84px" ID="doc_state" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="113px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="declines" Visible="True" Left="396px" Top="41px" ID="declines" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="bonus_quantity" Visible="True" Left="1px" Top="98px" ID="bonus_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="407px" Top="24px" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="84px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="inv_pack_quantity" Visible="True" Left="363px" Top="103px" ID="inv_pack_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="expiration_date" Visible="True" Left="373px" Top="57px" ID="expiration_date" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="118px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_serial" Visible="True" Left="207px" Top="73px" ID="order_serial" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="28px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="258px" Top="94px" ID="distributor_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="93px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="449px" Top="44px" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="53px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="last_update_datetime" Visible="True" Left="112px" Top="97px" ID="last_update_datetime" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="146px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

