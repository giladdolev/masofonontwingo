<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_marlog_print_footerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#FFFFFF">
	<Bands>
<vt:DataViewBand runat="server" Height="137px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewShapeField runat="server" Type="Rectangle" Visible="True" Left="0px" Top="102px" ID="r_1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="33px" Width="723px"></vt:DataViewShapeField>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="0px" Top="8px" ID="l_5" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="723px"></vt:DataViewShapeField>
<vt:DataViewLabelField runat="server" Text="&quot;לפי החזרת ספק&quot;" TemplateText="&quot;לפי החזרת ספק&quot;" Visible="True" Left="552px" Top="73px" Alignment="Right" ID="t_19" BorderStyle="None" Font-Names="Arial" Font-Size="14pt" ForeColor="#000000" Height="23px" Width="171px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ כמות&quot;" TemplateText="&quot;סה&quot;כ כמות&quot;" Visible="True" Left="408px" Top="106px" Alignment="Center" ID="total_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="87px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&#39; החזרת ספק&quot;" TemplateText="&quot;מס&#39; החזרת ספק&quot;" Visible="True" Left="504px" Top="106px" Alignment="Center" ID="invoice_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="75px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם ספק&quot;" TemplateText="&quot;שם ספק&quot;" Visible="True" Left="589px" Top="106px" Alignment="Center" ID="supplier_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="91px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="688px" Top="106px" Alignment="Center" ID="supplier_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="29px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="9px" ForeColor="#000000" Type="Summary" ID="summary"><Fields>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="0px" Top="2px" ID="l_3" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="723px"></vt:DataViewShapeField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="199px" ForeColor="#000000" Type="Footer" ID="footer"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="535px" Top="145px" Alignment="Right" ID="line_driver_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מקבל הסחורה&quot;" TemplateText="&quot;שם מקבל הסחורה&quot;" Visible="True" Left="390px" Top="146px" Alignment="Right" ID="accept_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="94px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="535px" Top="177px" Alignment="Right" ID="line_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חתימה&quot;" TemplateText="&quot;חתימה&quot;" Visible="True" Left="443px" Top="176px" Alignment="Right" ID="employee_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="240px" Top="145px" Alignment="Right" ID="line_accept_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;______________&quot;" TemplateText="&quot;______________&quot;" Visible="True" Left="240px" Top="176px" Alignment="Right" ID="line_employee_signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:הערות&quot;" TemplateText="&quot;:הערות&quot;" Visible="True" Left="682px" Top="29px" Alignment="Right" ID="notes_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם נהג&quot;" TemplateText="&quot;שם נהג&quot;" Visible="True" Left="682px" Top="145px" Alignment="Right" ID="driver_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="41px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;חתימה&quot;" TemplateText="&quot;חתימה&quot;" Visible="True" Left="688px" Top="176px" Alignment="Right" ID="signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="35px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="14px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="shipment_marlog_return_supplier_number" Visible="True" Left="688px" Top="1px" Alignment="Center" ID="shipment_marlog_return_supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="29px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_name" Visible="True" Left="589px" Top="1px" Alignment="Right" ID="supplier_name" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="91px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="shipment_marlog_return_invoice_number" Visible="True" Left="504px" Top="1px" Alignment="Center" ID="shipment_marlog_return_invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="total_quantity" Visible="True" Left="408px" Top="1px" Alignment="Center" ID="total_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="87px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

