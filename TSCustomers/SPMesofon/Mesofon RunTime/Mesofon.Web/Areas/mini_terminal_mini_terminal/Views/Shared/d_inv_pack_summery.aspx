<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_inv_pack_summeryRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="1px" Top="1px" Alignment="Center" ID="invoice_details_serial_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="83px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="85px" Top="1px" Alignment="Center" ID="invoice_details_branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="176px" Top="1px" Alignment="Center" ID="invoice_move_invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="265px" Top="1px" Alignment="Center" ID="invoice_move_branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="356px" Top="1px" Alignment="Center" ID="invoice_move_supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="96px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="453px" Top="1px" Alignment="Center" ID="invoice_move_invoice_type_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="73px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="527px" Top="1px" Alignment="Center" ID="invoice_move_order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="81px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="609px" Top="1px" Alignment="Center" ID="invoice_details_material_quantity_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="100px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="invoice_details_serial_number" Visible="True" Left="1px" Top="1px" Alignment="Right" ID="invoice_details_serial_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_details_branch_number" Visible="True" Left="85px" Top="1px" Alignment="Right" ID="invoice_details_branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_move_invoice_number" Visible="True" Left="176px" Top="1px" Alignment="Right" ID="invoice_move_invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_move_branch_number" Visible="True" Left="265px" Top="1px" Alignment="Right" ID="invoice_move_branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_move_supplier_number" Visible="True" Left="356px" Top="1px" Alignment="Right" ID="invoice_move_supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_move_invoice_type" Visible="True" Left="453px" Top="1px" ID="invoice_move_invoice_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="12px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_move_order_number" Visible="True" Left="527px" Top="1px" Alignment="Right" ID="invoice_move_order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_details_material_quantity" Visible="True" Left="609px" Top="1px" Alignment="Right" ID="invoice_details_material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

