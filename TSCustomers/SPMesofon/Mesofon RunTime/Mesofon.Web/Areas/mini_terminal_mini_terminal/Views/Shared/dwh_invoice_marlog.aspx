<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dwh_invoice_marlogRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="22px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך החזרה&quot;" TemplateText="&quot;תאריך החזרה&quot;" Visible="True" Left="4px" Top="3px" Alignment="Center" ID="supply_date_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="83px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר החזרה&quot;" TemplateText="&quot;מספר החזרה&quot;" Visible="True" Left="91px" Top="3px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="109px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מפיץ&quot;" TemplateText="&quot;שם מפיץ&quot;" Visible="True" Left="204px" Top="3px" Alignment="Center" ID="distributor_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="125px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Left="349px" Top="31px" Alignment="Right" ID="original_discount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="9px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Left="230px" Top="30px" Alignment="Right" ID="original_invoice_total" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="21px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="state" Visible="True" Left="597px" Top="1px" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="108px"></vt:DataViewTextField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="202px" Top="1px" Alignment="Right" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="125px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="91px" Top="1px" Alignment="Right" ID="shipment_number" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="19px" Width="109px"></vt:DataViewDropDownField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="move_date" Visible="True" Left="4px" Top="1px" ID="move_date" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="83px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

