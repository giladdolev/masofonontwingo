<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_sql_mt_material_codeRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;שם החומר&quot;" TemplateText="&quot;שם החומר&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="t_1" Font-Names="Arial" Font-Size="8pt" ForeColor="#FFFFFF" Height="14px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="111px" Top="2px" Alignment="Center" ID="t_2" Font-Names="Arial" Font-Size="8pt" ForeColor="#FFFFFF" Height="14px" Width="84px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="name" Visible="True" Left="2px" Top="1px" Alignment="Right" ID="name" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="105px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="111px" Top="1px" Alignment="Right" ID="barcode" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="84px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

