<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_sql_mini_terminal_input_headRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="ButtonFace">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="56px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;ש. ספק&quot;" TemplateText="&quot;ש. ספק&quot;" Visible="True" Left="109px" Top="2px" Alignment="Right" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="34px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="146px" Top="2px" Alignment="Right" ID="supplier_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="189px" Top="2px" Alignment="Right" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="supplier_name" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="supplier_name" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="103px"></vt:DataViewTextField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="117px" Top="39px" Alignment="Right" ID="order_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="68px"></vt:DataViewDropDownField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ לח-ן&quot;" TemplateText="&quot;סה&quot;כ לח-ן&quot;" Visible="True" Left="53px" Top="39px" Alignment="Right" ID="invoice_total_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="59px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="146px" Top="21px" Alignment="Right" ID="invoice_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;.ת&quot;" TemplateText="&quot;.ת&quot;" Visible="True" Left="126px" Top="21px" Alignment="Right" ID="supply_date_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="16px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" Format="dd/mm/yy" DataMember="supply_date" Visible="True" Left="75px" Top="21px" Alignment="Center" ID="supply_date" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="48px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;ח-ן&quot;" TemplateText="&quot;ח-ן&quot;" Visible="True" Left="189px" Top="21px" Alignment="Right" ID="invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הזמנה&quot;" TemplateText="&quot;הזמנה&quot;" Visible="True" Left="189px" Top="39px" Alignment="Right" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="30px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;לוגבוק&quot;" TemplateText="&quot;לוגבוק&quot;" Visible="True" Left="43px" Top="21px" Alignment="Right" ID="t_2" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="28px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="total" Visible="True" Left="2px" Top="39px" Alignment="Right" ID="total" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="48px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="log_book" Visible="True" Left="2px" Top="21px" Alignment="Right" ID="log_book" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="219px" Top="0px" ID="distributor_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="217px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

<vt:ComponentManager runat="server" Visible="False">
	<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="1px" Top="2px" ID="l_1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="210px">
	</vt:DataViewShapeField>
</vt:ComponentManager>

