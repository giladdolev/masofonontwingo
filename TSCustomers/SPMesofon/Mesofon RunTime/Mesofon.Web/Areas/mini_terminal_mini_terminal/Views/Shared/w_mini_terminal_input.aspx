<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<mini_terminal.w_mini_terminal_input>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Resizable="True" ID="w_mini_terminal_input" LoadAction="w_mini_terminal_input\Form_Load" BackColor="ButtonFace" Height="286px" Width="238px">
		<vt:Label runat="server" TextAlign="TopLeft" Text="0" Top="250px" Left="157px" ID="total_quantity" BackColor="#130000" Enabled="False" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="20px" Width="69px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" Text="��&quot;� ������" Top="229px" Left="158px" ID="st_1" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="17px" Width="69px">
		</vt:Label>
		<vt:DataView runat="server" Top="271px" Left="46px" Visible="False" ID="dw_print" Height="12px" TabIndex="30" Width="9px">
		</vt:DataView>
		<vt:Button runat="server" Text="��� ������" Top="250px" Left="2px" ID="cb_difference_report" Height="20px" TabIndex="50" Width="151px">
		</vt:Button>
		<vt:Button runat="server" Text="���" Top="228px" Left="106px" ID="cb_new" Height="20px" TabIndex="40" Width="46px">
		</vt:Button>
		<vt:RadioButton runat="server" IsChecked="True" CheckAlign="MiddleLeft" Text="�������" Top="5px" Left="5px" ID="rb_invoice" BackColor="ButtonFace" Font-Names="Arial" ForeColor="WindowText" Height="20px" Width="95px">
		</vt:RadioButton>
		<vt:RadioButton runat="server" CheckAlign="MiddleLeft" Text="����� �����" Top="6px" Left="101px" ID="rb_packing" BackColor="ButtonFace" Font-Names="Arial" ForeColor="WindowText" Height="20px" Width="116px">
		</vt:RadioButton>
		<vt:Button runat="server" Text="�����" Top="228px" Left="54px" ID="cb_save" Enabled="False" Height="20px" TabIndex="30" Width="51px">
		</vt:Button>
	</vt:WindowView>

</asp:Content>
