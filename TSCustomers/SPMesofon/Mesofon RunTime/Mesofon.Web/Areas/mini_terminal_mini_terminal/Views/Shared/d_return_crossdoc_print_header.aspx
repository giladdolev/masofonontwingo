<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_crossdoc_print_headerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#FFFFFF">
	<Bands>
<vt:DataViewBand runat="server" Height="189px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewShapeField runat="server" Type="Line" Visible="True" Left="0px" Top="81px" ID="l_5" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Width="723px"></vt:DataViewShapeField>
<vt:DataViewTextField runat="server" DataMember="shipment_number" Visible="True" Left="449px" Top="86px" Alignment="Right" ID="shipment_number" BorderStyle="None" Font-Names="Arial" Font-Size="14pt" ForeColor="#000000" Height="23px" Width="110px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;&#39;החזרת קרוסדוק מס&quot;" TemplateText="&quot;&#39;החזרת קרוסדוק מס&quot;" Visible="True" Left="564px" Top="86px" Alignment="Right" ID="t_19" BorderStyle="None" Font-Names="Arial" Font-Size="14pt" ForeColor="#000000" Height="23px" Width="158px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;נתקבל ע&quot;י&quot;" TemplateText="&quot;נתקבל ע&quot;י&quot;" Visible="True" Left="634px" Top="164px" Alignment="Right" ID="t_10" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סטטוס&quot;" TemplateText="&quot;סטטוס&quot;" Visible="True" Left="665px" Top="139px" Alignment="Right" ID="t_27" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך החזרה&quot;" TemplateText="&quot;תאריך החזרה&quot;" Visible="True" Left="630px" Top="114px" Alignment="Right" ID="t_29" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="92px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="employee_name" Visible="True" Left="439px" Top="165px" Alignment="Right" ID="employee_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="129px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="shipment_marlog_return_date_move" Visible="True" Left="470px" Top="115px" Alignment="Right" ID="shipment_marlog_return_date_move" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="161px"></vt:DataViewTextField>
<vt:DataViewComboField runat="server" Visible="True" Left="580px" Top="140px" Alignment="Right" ID="shipment_marlog_return_state" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="51px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="employee_number" Visible="True" Left="574px" Top="165px" Alignment="Right" ID="employee_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="57px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="442px" Top="6px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="280px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="vat_number" Visible="True" Left="455px" Top="33px" Alignment="Right" ID="vat_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="110px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="phone_number" Visible="True" Left="253px" Top="33px" Alignment="Right" ID="phone_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="98px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="fax_number" Visible="True" Left="254px" Top="55px" Alignment="Right" ID="fax_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="98px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;עוסק מורשה&quot;" TemplateText="&quot;עוסק מורשה&quot;" Visible="True" Left="571px" Top="33px" Alignment="Right" ID="t_36" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="151px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;טלפון&quot;" TemplateText="&quot;טלפון&quot;" Visible="True" Left="358px" Top="33px" Alignment="Right" ID="t_37" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="89px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;פקס&quot;" TemplateText="&quot;פקס&quot;" Visible="True" Left="358px" Top="55px" Alignment="Right" ID="t_38" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="89px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="2px" ForeColor="#000000" Type="Details" ID="detail"></vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

