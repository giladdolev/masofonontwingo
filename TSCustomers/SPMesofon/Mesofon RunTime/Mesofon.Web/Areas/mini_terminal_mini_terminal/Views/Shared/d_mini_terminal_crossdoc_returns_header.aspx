<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_crossdoc_returns_headerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="58px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewDropDownField runat="server" Visible="True" Left="5px" Top="3px" Alignment="Right" ID="supplier_1" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="24px" Width="126px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="6px" Top="32px" Alignment="Right" ID="distributor" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="24px" Width="25px"></vt:DataViewDropDownField>
<vt:DataViewLabelField runat="server" Text="&quot;מפיץ  &quot;" TemplateText="&quot;מפיץ  &quot;" Visible="True" Left="37px" Top="32px" Alignment="Right" ID="distributor_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="35px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="194px" Top="3px" Alignment="Right" ID="supplier_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="24px" Width="32px"></vt:DataViewLabelField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="136px" Top="3px" ID="supplier" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="24px" Width="52px"></vt:DataViewDropDownField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

