<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_mini_terminal_return_details_diffRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="16px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;דו&quot;ח הפרשים&quot;" TemplateText="&quot;דו&quot;ח הפרשים&quot;" Visible="True" Left="2px" Top="3px" Alignment="Center" ID="t_title" BorderStyle="None" Font-Names="Tahoma" Font-Size="8pt" ForeColor="WindowText" Height="13px" Width="226px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="31px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="126px" Top="1px" Alignment="Center" ID="barcode" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="80px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_name" Visible="True" Left="2px" Top="1px" ID="material_name" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="123px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_no" Visible="True" Left="208px" Top="1px" Alignment="Center" ID="row_no" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="20px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="86px" Top="16px" ID="supplier_number" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="53px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="149px" Top="16px" ID="material_quantity" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="48px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;כ&quot;" TemplateText="&quot;כ&quot;" Visible="True" Left="198px" Top="16px" Alignment="Center" ID="material_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="8px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ח&quot;" TemplateText="&quot;ח&quot;" Visible="True" Left="140px" Top="16px" Alignment="Center" ID="supplier_number_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="8px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

