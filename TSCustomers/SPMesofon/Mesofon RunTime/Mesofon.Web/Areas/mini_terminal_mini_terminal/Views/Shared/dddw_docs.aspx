<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dddw_docsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="16px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;סוג&quot;" TemplateText="&quot;סוג&quot;" Visible="True" Left="68px" Top="2px" Alignment="Center" ID="doc_type_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="12px" Width="35px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="doc_number_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="12px" Width="64px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="16px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewComboField runat="server" Visible="True" Left="68px" Top="1px" ID="doc_type" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="35px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="doc_number" Visible="True" Left="2px" Top="1px" Alignment="Right" ID="doc_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="64px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

