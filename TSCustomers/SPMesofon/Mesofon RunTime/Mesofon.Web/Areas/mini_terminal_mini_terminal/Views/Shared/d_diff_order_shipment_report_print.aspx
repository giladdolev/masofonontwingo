<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_diff_order_shipment_report_printRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="165px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;דוח פריטים שהוזמנו ולא התקבלו&quot;" TemplateText="&quot;דוח פריטים שהוזמנו ולא התקבלו&quot;" Visible="True" Left="455px" Top="12px" Alignment="Right" ID="t_title" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="209px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="675px" Top="130px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="564px" Top="130px" Alignment="Center" ID="materials_barcode_name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="107px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="252px" Top="39px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="20px" Width="300px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;שם פריט&quot;" TemplateText="&quot;שם פריט&quot;" Visible="True" Left="267px" Top="130px" Alignment="Center" ID="materials_name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="293px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות שהוזמנה&quot;" TemplateText="&quot;כמות שהוזמנה&quot;" Visible="True" Left="151px" Top="130px" Alignment="Center" ID="material_quantity_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="112px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס ספק&quot;" TemplateText="&quot;מס ספק&quot;" Visible="True" Left="95px" Top="130px" Alignment="Center" ID="supplier_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="52px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס הזמנה&quot;" TemplateText="&quot;מס הזמנה&quot;" Visible="True" Left="5px" Top="130px" Alignment="Center" ID="order_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="86px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:סניף&quot;" TemplateText="&quot;:סניף&quot;" Visible="True" Left="621px" Top="39px" Alignment="Right" ID="branch_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="42px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:משלוח&quot;" TemplateText="&quot;:משלוח&quot;" Visible="True" Left="611px" Top="67px" Alignment="Right" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="52px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="559px" Top="39px" Alignment="Center" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="20px" Width="48px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="537px" Top="67px" ID="t_shipment_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="19px" Width="70px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="675px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="151px" Top="1px" Alignment="Center" ID="order_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="564px" Top="1px" Alignment="Center" ID="barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="107px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_name" Visible="True" Left="267px" Top="1px" Alignment="Right" ID="material_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="293px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="95px" Top="1px" Alignment="Center" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="52px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="5px" Top="1px" Alignment="Right" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="86px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

