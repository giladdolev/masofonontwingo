<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_invoices_lockRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="94px" Top="2px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="96px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Create Mode&quot;" TemplateText="&quot;Create Mode&quot;" Visible="True" Left="192px" Top="2px" Alignment="Center" ID="create_mode_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="74px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Last Update Datetime&quot;" TemplateText="&quot;Last Update Datetime&quot;" Visible="True" Left="268px" Top="2px" Alignment="Center" ID="last_update_datetime_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="132px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Invoice Number&quot;" TemplateText="&quot;Invoice Number&quot;" Visible="True" Left="402px" Top="1px" ID="invoice_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="252px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Invoice Type&quot;" TemplateText="&quot;Invoice Type&quot;" Visible="True" Left="656px" Top="1px" ID="invoice_type_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="4px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="94px" Top="2px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="96px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="create_mode" Visible="True" Left="192px" Top="2px" ID="create_mode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="74px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="last_update_datetime" Visible="True" Left="268px" Top="2px" ID="last_update_datetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="132px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="402px" Top="0px" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="252px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_type" Visible="True" Left="656px" Top="0px" ID="invoice_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="4px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

