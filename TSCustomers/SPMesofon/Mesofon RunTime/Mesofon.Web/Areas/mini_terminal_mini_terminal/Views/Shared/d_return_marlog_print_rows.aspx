<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_return_marlog_print_rowsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#FFFFFF">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="608px" Top="1px" ID="materials_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="80px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_name" Visible="True" Left="476px" Top="1px" Alignment="Right" ID="material_name" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="122px"></vt:DataViewTextField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="173px" Top="1px" Alignment="Right" ID="decline_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="87px"></vt:DataViewDropDownField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="269px" Top="1px" Alignment="Center" ID="invoice_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_name" Visible="True" Left="352px" Top="1px" Alignment="Right" ID="supplier_name" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="80px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="441px" Top="1px" Alignment="Center" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="29px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_price" Left="39px" Top="1px" Alignment="Center" ID="material_price" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="60px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="110px" Top="1px" Alignment="Center" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="51px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="693px" Top="1px" ID="line_number" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="16px" Width="24px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

<vt:ComponentManager runat="server" Visible="False">
	<vt:DataViewShapeField runat="server" Type="Rectangle" Visible="True" Left="0px" Top="25px" ID="r_1" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="33px" Width="723px">
	</vt:DataViewShapeField>
	<vt:DataViewLabelField runat="server" Text="&quot;&#39;מס&quot;" TemplateText="&quot;&#39;מס&quot;" Visible="True" Left="694px" Top="29px" Alignment="Center" ID="t_17" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="24px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="606px" Top="29px" Alignment="Center" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="80px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;תאור פריט&quot;" TemplateText="&quot;תאור פריט&quot;" Visible="True" Left="476px" Top="29px" Alignment="Center" ID="t_2" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="122px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;כמות&quot;" TemplateText="&quot;כמות&quot;" Visible="True" Left="107px" Top="29px" Alignment="Center" ID="t_4" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="51px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;סיבת החזרה&quot;" TemplateText="&quot;סיבת החזרה&quot;" Visible="True" Left="170px" Top="29px" Alignment="Center" ID="decline_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="87px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;מס&#39; החזרת ספק&quot;" TemplateText="&quot;מס&#39; החזרת ספק&quot;" Visible="True" Left="266px" Top="29px" Alignment="Center" ID="invoice_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="75px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;שם ספק&quot;" TemplateText="&quot;שם ספק&quot;" Visible="True" Left="352px" Top="29px" Alignment="Center" ID="supplier_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="80px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="438px" Top="29px" Alignment="Center" ID="supplier_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="29px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;עלות&quot;" TemplateText="&quot;עלות&quot;" Left="36px" Top="29px" Alignment="Center" ID="t_3" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="26px" Width="60px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;&#39;קרטון מס&quot;" TemplateText="&quot;&#39;קרטון מס&quot;" Visible="True" Left="655px" Top="1px" Alignment="Right" ID="t_5" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="67px">
	</vt:DataViewLabelField>
	<vt:DataViewTextField runat="server" DataMember="carton_barcode" Visible="True" Left="531px" Top="1px" Alignment="Right" ID="carton_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="125px">
	</vt:DataViewTextField>
	<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ לפי קרטון&quot;" TemplateText="&quot;סה&quot;כ לפי קרטון&quot;" Visible="True" Left="168px" Top="2px" Alignment="Right" ID="quantity_sum_t" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="68px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="110px" Top="2px" ID="cf_quantity_sum" BorderStyle="None" Font-Names="Tahoma" Font-Size="8pt" ForeColor="WindowText" Height="13px" Width="51px">
	</vt:DataViewLabelField>
</vt:ComponentManager>

