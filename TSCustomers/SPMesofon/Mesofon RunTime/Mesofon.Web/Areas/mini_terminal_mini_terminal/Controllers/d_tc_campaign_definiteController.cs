using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_tc_campaign_definiteController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_tc_campaign_definite()
		{
			d_tc_campaign_definiteRepository repository = new d_tc_campaign_definiteRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
