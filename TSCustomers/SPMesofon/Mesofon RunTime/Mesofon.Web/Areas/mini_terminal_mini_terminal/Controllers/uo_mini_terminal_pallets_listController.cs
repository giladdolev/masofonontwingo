using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using masofon;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using MvcSite.ServicePrintBOS;
using System.Xml;
using System.Data;
using MvcSite.Common;
using System.Linq;
using System.Xml.Linq;

namespace Mesofon.MiniTerminal.Controllers
{
    public class uo_mini_terminal_pallets_listController : MvcSite.Common.SPBaseController
    {

        public ActionResult uo_mini_terminal_pallets_list()
        {
            return this.View(new uo_mini_terminal_pallets_list());
        }
        private uo_mini_terminal_pallets_list ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_pallets_list; }
        }

        public static bool enterKeyFlag { get; private set; }

        public void Form_Load(object sender, EventArgs e)
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            this.ViewModel.constructor();
            dw_pallets_list.SetRepository(new d_marlog_return_cartonsRepository(), false, true);
        }



        public int uf_write_log(string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_write_log
            //*Purpose: 			Write into log file
            //*Arguments: 		String - as_message - message
            //*Return:				Integer
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************

            SystemHelper.WriteToLog(as_message, string.Empty);
            return 1;
        }
        public async Task<int> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_show_message
            //*Purpose: 			Display a messagebox with the given message text.
            //*Arguments: 		String 	as_title 					
            //*						String	as_error_text		
            //*						String 	as_buttons	
            //*						String 	as_message	
            //*Return:				Integer	1 - Success
            //*									-1 -Otherwise
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138				Initial version
            //************************************************************************************************

            int li_rtn_code = 0;
            //debug -Start
            //Write to log
            string ls_message = null;
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "Start of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);
            //debug -END

            var msg = await MessageBox.Show(as_message, as_title, MessageBoxButtons.OK, MessageBoxDirection.Rtl);
            //debug -Start
            //Write to log
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "End of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);
            //debug -END
            return li_rtn_code;
        }
        public async Task<int> uf_close_window() //********************************************************************
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_close_window
            //*Purpose: 			close window
            //*Arguments: 		none
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            //  bool lb_save_before_close = false;
            string ls_message = null;
            string ls_params = null;
            int li_rtn_code = 0;
            long ll_shipment_number = 0;
            // AlexKh - 1.2.46.5 - 2010-12-20 - SPUCM00002425 - Get params and prepare order diff report
            if (!string.IsNullOrEmpty(Convert.ToString(dw_shipments_list.SelectedValue)) && Convert.ToString(dw_shipments_list.SelectedValue) != "הבא הכל")
            {
                ll_shipment_number = Convert.ToInt64(dw_shipments_list.SelectedValue);
            }
            if (ll_shipment_number > 0)
            {
                await uf_send_msg(ll_shipment_number); //RonY@12/01/2015 1.12.49.12 - SPUCM00005157
                ls_params = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "@" + ll_shipment_number.ToString() + "@" + "-1" + "@";
                uf_write_log(this.ViewModel.GetType().Name.ToString() + " - uf_close_window Function, Open rw_diff_order_mt_report.");
                //Show items that were ordered and not supplied
                await WindowHelper.Open<rw_diff_order_mt_report>("global_global", "ls_params", ls_params);
                // Vika 2012-05-29 SPUCM00003580 ביטול חשבונית מרלוג בסניף
                uf_write_log(this.ViewModel.GetType().Name.ToString() + " - uf_close_window Function, Open rw_not_arrived_inv_report.");
                //Show items that were ordered and not supplied

                await WindowHelper.Open<rw_not_arrived_inv_report>("global_global", "ls_params", ls_params);
                // End Vika 2012-05-29 SPUCM00003580 ביטול חשבונית מרלוג בסניף
            }
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
            li_rtn_code = uf_write_log(ls_message);
            if (li_rtn_code < 1)
            {
                await uf_display("ENTRANCE");
                return -1;
            }
            await uf_display("ENTRANCE");
            dw_shipments_list.Focus();
            return 1;
        }
        public async void uf_set_pallet_scan_type(long al_row) //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_set_pallet_scan_type
            //*Purpose: 			Generates decision if to scan pallet automatically
            //*Arguments: 		long	 	al_row - row number
            //*Return:				None
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*01-06-2010		AlexKh			1.2.46.0	CR#1138			Initial version
            //********************************************************************

            decimal ldec_percent = default(decimal);
            decimal ldec_random_percent = default(decimal);
            decimal ldec_total_valuables = default(decimal);
            long ll_scan_status = 0;
            long ll_value_indicator = 0;
            long ll_sum_value_indicator = 0;
            long ll_count = 0;
            Random random = new Random();
            ldec_random_percent = (random.Next(100) / (decimal)100);
            //Eitan; SPUCM00005205; Ver 1.2.49.64; 26/1/2015 - CR1180 - Separate parameters for branches 999 & 997
            if (this.ViewModel.il_marlog_branchProperty == 999)
            {
                ldec_percent = Convert.ToDecimal(await f_get_parameter_valueClass.f_get_parameter_value("full_scan_percent", "decimal", "1.00", "Pallets to be scanned automatically.", "gds_find"));
            }
            else if (this.ViewModel.il_marlog_branchProperty == 997)
            {
                ldec_percent = Convert.ToDecimal(await f_get_parameter_valueClass.f_get_parameter_value("full_scan_percent_997", "decimal", "1.00", "Pallets to be scanned automatically.", "gds_find"));
            }
            //Eitan; SPUCM00005205; Ver 1.2.49.64; 26/1/2015 - CR1180

            ll_sum_value_indicator = 0;// Convert.ToInt64(dw_pallets_list.GetItemValue<string>(al_row, "compute_1"));
            foreach (Models.d_mini_terminal_pallets_list item in dw_pallets_list.Repository().GetPrimaryList())
            {
                ll_sum_value_indicator += item.value_indicator;
            }

            ll_count = dw_pallets_list.RowCount();
            ldec_total_valuables = (decimal)ll_sum_value_indicator / (decimal)ll_count;
            ldec_percent -= ldec_total_valuables;
            if (ldec_percent < 0)
            {
                ldec_percent = 0;
            }

            ll_value_indicator = dw_pallets_list.GetItemValue<long>(al_row, "value_indicator");
            //Scan pallet manually
            if (ll_value_indicator == 1) //Eitan; SPUCM00005205; Ver 1.2.49.64; 26/1/2015 - CR1180
            {
                ll_scan_status = mini_terminal.uo_mini_terminal_pallets_list.ACTIVE_MANUAL_NOT_SCANNED; //Eitan; SPUCM00005205; Ver 1.2.49.64; 26/1/2015 - CR1180
            }
            else if (ldec_random_percent <= ldec_percent)
            {
                ll_scan_status = mini_terminal.uo_mini_terminal_pallets_list.ACTIVE_MANUAL_NOT_SCANNED;
            }
            else
            {
                ll_scan_status = mini_terminal.uo_mini_terminal_pallets_list.ACTIVE_AUTOMATIC_NOT_SCANNED;
            }

            dw_pallets_list.SetItem(al_row, "auto_scan_status", ll_scan_status);
        }
        public async Task<bool> uf_automatic_scan(int al_row) //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_automatic_scan
            //*Purpose: 			Perform automatic scan for all invoices in current pallet
            //*Arguments: 		long	 	al_row - row number
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*03-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_shipment_number = 0;
            long ll_ret = 0;
            long ll_rows = 0;
            long ll_i = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            long ll_material_number = 0;
            long? ll_found = 0;
            long ll_order_number = 0;
            long ll_count = 0;
            long ll_j = 0;
            long ll_row_no = 0;
            long ll_document_type = 0;
            long ll_min_valid_month = 0;
            long ll_error_number = 0;
            string ls_message = null;
            string ls_pallet_number = null;
            string ls_find = null;
            string ls_lock_mode = null;
            string ls_state = null;
            string ls_error_text = null;
            decimal ldec_material_quantity = default(decimal);
            decimal ldec_sell_price = default(decimal);
            decimal ldec_material_price = default(decimal);
            decimal ldec_quantity = default(decimal);
            IRepository lds_pallet_invoices;
            IRepository lds_pallet_b2b_invoice_details = null;
            IRepository lds_pallet_invoice_details;
            IRepository lds_pallet_invoice_move;
            DateTime? ldt_expiration_date = null;
            bool ib_update = false;

            ll_shipment_number = (long)dw_pallets_list.GetItemValue<double>(al_row, "shipment_number");
            ls_pallet_number = dw_pallets_list.GetItemValue<string>(al_row, "pallet_number");
            ls_message = "*************************** uo_mini_terminal_pallets_list *************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "uf_automatic_scan The Data:  " + "Start Automatic Scan Shipment:  " + ll_shipment_number.ToString() + "\r" + "\n" + "Pallet:  " + ls_pallet_number + "\r" + "\n";
            ll_ret = uf_write_log(ls_message);




            lds_pallet_invoices = new d_mini_terminal_pallet_invoicesRepository();



            lds_pallet_b2b_invoice_details = new d_mini_terminal_pallet_b2b_invoiceRepository();




            lds_pallet_invoice_details = new d_sql_mini_terminal_input_bodyRepository();




            lds_pallet_invoice_move = new d_sql_mini_terminal_input_headRepository();
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //Retrieve all the invoices for specific pallet
            //ll_rows = lds_pallet_invoices.retrieve(gs_vars.branch_number, ll_shipment_number, ls_pallet_number)

            ll_rows = lds_pallet_invoices.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_shipment_number, ls_pallet_number, this.ViewModel.il_marlog_branchProperty);
            if (ll_rows <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", "לא נמצאו חשבוניות למשטח " + ls_pallet_number);
                return false;
            }
            ib_update = true;
            ll_row_no = 0;
            for (ll_i = 0; ll_i < ll_rows; ll_i++) //For every invoice in pallet
            {

                if (lds_pallet_invoices.GetItemValue<long>((int)ll_i, "auto_scan_status") == mini_terminal.uo_mini_terminal_pallets_list.ACTIVE_AUTOMATIC_SCANNED)
                {
                    continue;
                }

                ll_supplier_number = lds_pallet_invoices.GetItemValue<long>((int)ll_i, "supplier_number");

                ll_invoice_number = (long)lds_pallet_invoices.GetItemValue<double>((int)ll_i, "invoice_number");

                ll_order_number = (long)lds_pallet_invoices.GetItemValue<double>((int)ll_i, "order_number");

                ll_document_type = lds_pallet_invoices.GetItemValue<long>((int)ll_i, "document_type");
                //Retrieve invoice message information

                ll_count = lds_pallet_b2b_invoice_details.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, Convert.ToInt32(ll_supplier_number), ll_invoice_number, ls_pallet_number, ll_shipment_number);
                if (ll_count <= 0)
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצאו פריטים המשוייכים למשטח - " + ls_pallet_number + " במסר חשבונית - " + ll_invoice_number.ToString());
                    ib_update = false;
                    continue;
                }
                //Retrieve invoice header

                ll_count = lds_pallet_invoice_move.Retrieve(ll_invoice_number, ll_supplier_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
                if (ll_count != 1)
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצאה כותרת חשבונית המשוייכת למשטח  - " + ls_pallet_number + " בחשבונית - " + ll_invoice_number.ToString());
                    ib_update = false;
                    continue;
                }

                ls_lock_mode = lds_pallet_invoice_move.GetItemValue<string>(0, "create_mode");
                //Check invoice is not locked for the change	
                //In case it's not locked the invoice will not be locked because of fast activity
                //In the future optionally the lock can be added for the invoice while automatic scan performed
                if (ls_lock_mode == "1")
                {
                    await uf_show_message("שגיאה", "", "OK", " חשבונית - " + ll_invoice_number.ToString() + "  נעולה");
                    ib_update = false;
                    continue;
                }

                ls_state = lds_pallet_invoice_move.GetItemValue<string>(0, "state");
                //Check invoice is not locked for the change	
                if (ls_state.ToUpper() != "O")
                {
                    await uf_show_message("שגיאה", "", "OK", " חשבונית - " + ll_invoice_number.ToString() + "  סגורה, ולא ניתן לסרוק פריטים");
                    continue;
                }
                //Retrieve invoice details

                ll_count = lds_pallet_invoice_details.Retrieve(ll_invoice_number, ll_supplier_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
                if (ll_count <= 0)
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצאו פריטים המשוייכים למשטח - " + ls_pallet_number + " בחשבונית - " + ll_invoice_number.ToString());
                    ib_update = false;
                    continue;
                }
                //Check if invoice is CrossDoc then no need to scan their items
                if (ll_document_type == mini_terminal.uo_mini_terminal_pallets_list.INVOICE_TYPE_CROSSDOC)
                {
                    await uf_show_message("שגיאה", "", "OK", " חשבונית " + ll_invoice_number.ToString() + " היא חשבונית קרוסדוק, לא ניתן לסרוק אוטומטית");
                    continue;
                }
                this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_number);
                this.ViewModel.iuo_material_updateProperty.uf_set_supplier_number(ll_supplier_number);
                this.ViewModel.iuo_material_updateProperty.uf_set_order_number(ll_order_number);

                for (ll_j = 0; ll_j < lds_pallet_b2b_invoice_details.RowCount(); ll_j++)
                {

                    ll_material_number = lds_pallet_b2b_invoice_details.GetItemValue<long>((int)ll_j, "b2b_invoice_details_item_number");
                    ls_find = "material_number == " + ll_material_number.ToString();


                    ll_found = lds_pallet_invoice_details.Find(ls_find, 0, lds_pallet_invoice_details.RowCount());
                    if (ll_found >= 0)
                    {

                        ldt_expiration_date = lds_pallet_b2b_invoice_details.GetItemValue<DateTime>((int)ll_j, "b2b_invoice_details_validity_date");

                        ll_min_valid_month = lds_pallet_invoice_details.GetItemValue<long>((int)ll_found, "min_valid_month");
                        //In case no expiration date provided in message and no expiration date inserted by user, then stop the scan

                        if (!await uf_handle_item_expiration_date(ldt_expiration_date, ll_min_valid_month, lds_pallet_invoice_details.GetItemValue<string>(ll_found.Value, "material_name"), ll_material_number))
                        {
                            await uf_show_message("שגיאה", "", "OK", "  חסר תאריך תפוגה בחשבונית " + ll_invoice_number.ToString() + " עבור פריט " + ll_material_number.ToString());
                            //RETURN FALSE
                        }

                        ldec_material_quantity = lds_pallet_b2b_invoice_details.GetItemValue<decimal>((int)ll_j, "b2b_invoice_details_units_qty");
                        //Set invoice details

                        if (lds_pallet_b2b_invoice_details.GetItemValue<decimal>((int)ll_j, "b2b_invoice_details_scanned_quantity") == 0)
                        {

                            if (lds_pallet_b2b_invoice_details.GetItemValue<string>(ll_j, "b2b_invoice_details_item_bonus_code") == "6")
                            {

                                ldec_quantity = lds_pallet_invoice_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");
                                if (ldec_quantity == 0)
                                {
                                    ldec_quantity = 0;
                                }

                                lds_pallet_invoice_details.SetItem(ll_found.Value, "bonus_quantity", ldec_quantity + ldec_material_quantity);
                            }
                            else
                            {

                                ldec_quantity = lds_pallet_invoice_details.GetItemValue<decimal>((int)ll_found, "material_quantity");
                                if (ldec_quantity == 0)
                                {
                                    ldec_quantity = 0;
                                }

                                lds_pallet_invoice_details.SetItem(ll_found.Value, "material_quantity", Convert.ToString(ldec_quantity + ldec_material_quantity));
                            }

                            lds_pallet_b2b_invoice_details.SetItem(ll_j, "b2b_invoice_details_scanned_quantity", ldec_material_quantity.ToString());
                            //If quantity not updated , there is no need to update other fields	
                        }
                        else
                        {
                            continue;
                        }
                        // AlexKh - 1.1.27.0 - 2015-02-04 - SPUCM00005224 - save employee
                        //lds_pallet_invoice_details.setItem(ll_found, "details", "automatic_scan")

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "details", Convert.ToString("automatic_scan" + " EmpNo<" + this.ViewModel.il_employee_numberProperty.ToString() + ">"));

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "expected_material_quantity", ldec_material_quantity.ToString());

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "mini_terminal", "1");

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "expiration_date", ldt_expiration_date.ToString());

                        ldec_sell_price = lds_pallet_invoice_details.GetItemValue<decimal>((int)ll_found, "sell_price");

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "current_catalog_sell_price", ldec_sell_price.ToString());

                        ll_row_no++;

                        lds_pallet_invoice_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());
                        //Set order details
                        ll_ret = this.ViewModel.iuo_order_searchProperty.uf_set_quantity_within_invoice_no_upd(ll_material_number, ldec_material_quantity);

                        ldec_material_price = lds_pallet_invoice_details.GetItemValue<decimal>((int)ll_found, "material_price");
                        // Load material_suppliers connect data for the material
                        await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers_connect_new(ll_material_number);
                        // Load material_suppliers data for the material
                        await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers_new(ll_material_number, ldec_material_price);
                    }
                    else
                    {
                    }
                }
                await f_begin_tranClass.f_begin_tran();
                //Update material_suppliers and material_suppliers_connect
                ll_ret = this.ViewModel.iuo_material_updateProperty.uf_update_new(ref ll_error_number, ref ls_error_text);
                if (ll_ret != 1)
                {
                    await f_rollbackClass.f_rollback();
                    await uf_show_message("שגיאה", "", "OK", "לפריט material_suppliers עדכון " + ll_material_number.ToString() + " נכשל " + ls_error_text);
                    continue;
                }
                //Update order state
                ll_ret = this.ViewModel.iuo_order_searchProperty.uf_set_order_state("P");
                if (ll_ret != 1)
                {
                    await f_rollbackClass.f_rollback();
                    await uf_show_message("שגיאה", "", "OK", " עדכון כותרת הזמנה  " + ll_order_number.ToString() + " נכשל ");
                    continue;
                }
                //Update order details
                ll_ret = this.ViewModel.iuo_order_searchProperty.uf_update_order_details();
                if (ll_ret != 1)
                {
                    await f_rollbackClass.f_rollback();
                    await uf_show_message("שגיאה", "", "OK", " עדכון פרטי הזמנה " + ll_order_number.ToString() + " נכשל ");
                    continue;
                }
                //Update items in invoice details

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = lds_pallet_invoice_details.Update(unitOfWork);
                }
                if (ll_ret != 1)
                {
                    await f_rollbackClass.f_rollback();
                    await uf_show_message("שגיאה", "", "OK", " סריקת פרטי חשבונית  " + ll_invoice_number.ToString() + " נכשלה ");
                    continue;
                }
                //Update items in b2b invoice details
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = lds_pallet_b2b_invoice_details.Update(unitOfWork);
                }
                if (ll_ret != 1)
                {
                    await f_rollbackClass.f_rollback();
                    await uf_show_message("שגיאה", "", "OK", " עדכון כמות במסר חשבונית " + ll_invoice_number.ToString() + " נכשלה ");
                    continue;
                }
                await f_commitClass.f_commit();
                ls_message = "*************************** uo_mini_terminal_pallets_list *************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "uf_automatic_scan The Data:  " + "Start Automatic Scan Shipment:  " + ll_shipment_number.ToString() + "\r" + "\n" + "Pallet:  " + ls_pallet_number + "\r" + "\n" + "Invoice:  " + ll_invoice_number.ToString() + "successfully scanned" + "\r" + "\n";
                ll_ret = uf_write_log(ls_message);
                //uf_show_message("הודעה", "", "OK", " חשבונית " + String(ll_invoice_number) + " נסרקה בהצלחה ")	
            }
            ls_message = "*************************** uo_mini_terminal_pallets_list*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "uf_automatic_scan The Data:  " + "End Automatic Scan Shipment:  " + ll_shipment_number.ToString() + "\r" + "\n" + "Pallet:  " + ls_pallet_number + "\r" + "\n";
            ll_ret = uf_write_log(ls_message);



            return ib_update;
        }
        public async Task<bool> uf_manual_scan(long al_row) //**********************************************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_manual_scan
            //*Purpose: 			Handle Manual Pallet scan item by item
            //*Arguments: 		Long	al_row
            //*Return:				
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*28-06-2010		AlexKh			1.2.46.0	CR#1138			Initial version
            //************************************************************************************************

            long ll_shipment_number = 0;
            string ls_pallet_number = null;

            ll_shipment_number = (long)dw_pallets_list.GetItemValue<double>(al_row, "shipment_number");
            ls_pallet_number = dw_pallets_list.GetItemValue<string>(al_row, "pallet_number");



            (this.ViewModel.Parent.Parent as w_mini_terminal).wf_set_parameters(ll_shipment_number, ls_pallet_number);
            await uf_display("PALLET");
            return true;
        }
        public async Task<int> uf_display(string as_name) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_display
            //*Purpose: 			open appropriate user object
            //*Arguments: 		String	as_name
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*17-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            int li_ret = 0;
            long ll_row_count = 0;


            li_ret = await ((this.ViewModel.Parent.Parent as w_mini_terminal).wf_display(as_name));
            return 1;
        }
        public async Task<bool> uf_print_driver_report() //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_print_driver_report
            //*Purpose: 			Print driver report, only scanned pallets will be included
            //*Arguments: 		None
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*26-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_i = 0;
            long ll_ret = 0;
            long ll_shipment_number = 0;
            long ll_rows = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            long ll_print_copies_number = 0;
            long? ll_found = 0;
            string[] ls_pallets_list = null;
            string ls_ret = null;
            string ls_pallet_number = "";
            bool lb_not_scanned_pallet = false;
            GridElement lds_driver_report;
            IRepository lds_shipment_invoices;
            IRepository lds_footer_temp;
            IRepository lds_excess;
            GridElement ldwc_head;
            GridElement ldwc_footer;
            GridElement ldwc_excess;




            lds_shipment_invoices = new d_shipment_invoicesRepository();




            lds_footer_temp = new d_mini_terminal_driver_report_footerRepository();
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items




            lds_excess = new d_mini_terminal_driver_report_excessRepository();




            lds_driver_report = VisualElementHelper.CreateFromView<GridElement>("d_mini_terminal_driver_report_main", "d_mini_terminal_driver_report_main", "mini_terminal_mini_terminal");

            ldwc_head = lds_driver_report.GetVisualElementById<GridElement>("dw_1");
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items

            ldwc_excess = lds_driver_report.GetVisualElementById<GridElement>("dw_2");
            //lds_driver_report.GetChild("dw_2", ldwc_footer)			

            ldwc_footer = lds_driver_report.GetVisualElementById<GridElement>("dw_3");
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items

            ll_shipment_number = dw_shipments_list.GetItemValue<long>(0, "shipment_number");
            if (ll_shipment_number == 0 || ll_shipment_number <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " מספר משלוח לא תקין ");



                return false;
            }
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_rows = lds_shipment_invoices.Retrieve(gs_vars.branch_number, ll_shipment_number)

            ll_rows = lds_shipment_invoices.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_shipment_number, this.ViewModel.il_marlog_branchProperty);
            if (ll_rows <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " לא נמצאו נתונים עבור מספר משלוח זה ");
                return false;
            }

            for (ll_i = 0; ll_i < dw_pallets_list.RowCount(); ll_i++)
            {

                if (dw_pallets_list.GetItemValue<int>(ll_i, "auto_scan_status") > mini_terminal.uo_mini_terminal_pallets_list.NEW_NOT_APPROVED)
                {

                    ls_pallets_list[CollectionExtensions.UBound(ls_pallets_list) + 1] = dw_pallets_list.GetItemValue<string>(ll_i, "pallet_number");
                }
            }
            if (ls_pallets_list.Length == 0)
            {
                await uf_show_message("שגיאה", "", "OK", " אין משטחים שכבר נסרקו במשלוח זה ");
                return false;
            }
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_ret = ldwc_head.Retrieve(gs_vars.branch_number, ll_shipment_number, il_employee_number)

            ll_ret = ldwc_head.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_shipment_number, this.ViewModel.il_employee_numberProperty, this.ViewModel.il_marlog_branchProperty);
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - sort the pallets list

            lds_shipment_invoices.SetSort("pallet_number ASC");

            lds_shipment_invoices.Sort();
            for (ll_i = 1; ll_i <= ll_rows; ll_i++)
            {
                // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - check pallet's items were scanned

                if (ls_pallet_number != lds_shipment_invoices.GetItemValue<string>(ll_i, "pallet_number"))
                {

                    ls_pallet_number = lds_shipment_invoices.GetItemValue<string>(ll_i, "pallet_number");
                    lb_not_scanned_pallet = false;
                    if (uf_check_pallet_items_scanned(ls_pallet_number))
                    {
                        lb_not_scanned_pallet = true;
                        continue;
                    }
                }
                else
                {
                    if (lb_not_scanned_pallet)
                    {
                        continue;
                    }
                }

                ll_supplier_number = lds_shipment_invoices.GetItemValue<long>((int)ll_i, "supplier_number");

                ll_invoice_number = lds_shipment_invoices.GetItemValue<long>((int)ll_i, "invoice_number");
                // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - prevent duplicate rows


                ll_found = ldwc_footer.Find("b2b_declines_move_parent_doc_number == " + ll_invoice_number.ToString(), 0, ldwc_footer.RowCount());
                if (ll_found < 0)
                {

                    ll_ret = lds_footer_temp.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_invoice_number);
                    if (ll_ret > 0)
                    {
                        lds_footer_temp.RowsMove(1, lds_footer_temp.RowCount(), ModelBuffer.Primary, ldwc_footer.GetDataObject(), ldwc_footer.RowCount() + 1, ModelBuffer.Primary);
                    }
                }
                // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items


                ll_found = ldwc_excess.Find("b2b_declines_move_parent_doc_number == " + ll_invoice_number.ToString(), 0, ldwc_excess.RowCount());
                if (ll_found < 0)
                {

                    ll_ret = lds_excess.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_invoice_number);
                    if (ll_ret > 0)
                    {
                        lds_excess.RowsMove(1, lds_excess.RowCount(), ModelBuffer.Primary, ldwc_excess.GetDataObject(), ldwc_excess.RowCount() + 1, ModelBuffer.Primary);
                    }
                }
            }

            ls_ret = lds_driver_report.Modify("datawindow.print", "Orientation", "2"); // 1-landscape , 2-portrait

            if (ldwc_footer.RowCount() == 0)
            {

                ldwc_footer.Insert(0);

                ldwc_footer.Modify("item_barcode_t", "visible", "0");

                ldwc_footer.Modify("materials_item_barcode", "visible", "0");

                ldwc_footer.Modify("description", "visible", "0");

                ldwc_footer.Modify("decline_description_t", "visible", "0");

                ldwc_footer.Modify("invoice_number_t", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_parent_doc_number", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_original_quantity_t", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_original_quantity", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_actual_quantity_t", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_actual_quantity", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_reject_quantity_t", "visible", "0");

                ldwc_footer.Modify("b2b_declines_move_reject_quantity", "visible", "0");

                ldwc_footer.Modify("b2b_decline_types_display_name_t", "visible", "0");

                ldwc_footer.Modify("b2b_decline_types_display_name", "visible", "0");

                ldwc_footer.Modify("diff_t", "visible", "0");
            }

            ll_ret = ldwc_footer.SetSort("item_barcode ASC");

            ll_ret = ldwc_footer.Sort();
            // AlexKh - 1.2.48.0.13 - 2011-11-07 - SPUCM00002930 - choose copies number

            await WindowHelper.Open<rw_print_copies_number>("mini_terminal_mini_terminal", "rowsCount", 0);

            ll_print_copies_number = Convert.ToInt64(WindowHelper.GetParam<string>(this.ViewModel));
            for (ll_i = 1; ll_i <= ll_print_copies_number; ll_i++)
            {

                ll_ret = lds_driver_report.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName);
            }



            return true;
        }
        public async Task<bool> uf_print_temp_driver_report() //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_print_temp_driver_report
            //*Purpose: 			Print temporary driver report use differences instead Histaeguyot
            //*Arguments: 		None
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*09-11-2010		AlexKh			1.2.46.3	SPUCM00002425	Initial version
            //********************************************************************

            long ll_i = 0;
            long ll_ret = 0;
            long ll_shipment_number = 0;
            long ll_rows = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            long ll_print_copies_number = 0;
            long? ll_found = 0;
            string[] ls_pallets_list = null;
            string ls_ret = null;
            string ls_pallet_number = "";
            bool lb_not_scanned_pallet = false;
            GridElement lds_driver_report;
            IRepository lds_shipment_invoices;
            IRepository lds_footer_temp;
            IRepository lds_excess;
            IRepository ldwc_head;
            IRepository ldwc_footer;
            IRepository ldwc_excess;
            lds_shipment_invoices = new d_mini_terminal_pallet_invoicesRepository();
            lds_footer_temp = new d_driver_report_footer_tempRepository();
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items
            lds_excess = new d_mini_terminal_driver_report_excessRepository();
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items
            ldwc_footer = new d_driver_report_footer_tempRepository(); // lds_driver_report.GetVisualElementById<GridElement>("dw_3");
            ldwc_head = new d_mini_terminal_driver_report_headerRepository();
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items
            ldwc_excess = new d_mini_terminal_driver_report_excessRepository();
            // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items

            ll_shipment_number = string.IsNullOrEmpty(dw_shipments_list.SelectedText) ? 0 : Convert.ToInt64(dw_shipments_list.SelectedText);
            if (ll_shipment_number == 0 || ll_shipment_number <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " מספר משלוח לא תקין ");

                return false;
            }
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_rows = lds_shipment_invoices.Retrieve(gs_vars.branch_number, ll_shipment_number, '')

            ll_rows = lds_shipment_invoices.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_shipment_number, "", this.ViewModel.il_marlog_branchProperty);
            if (ll_rows <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " לא נמצאו נתונים עבור מספר משלוח זה ");


                return false;
            }
            ls_pallets_list = new string[dw_pallets_list.RowCount()];

            for (ll_i = 0; ll_i < dw_pallets_list.RowCount(); ll_i++)
            {

                if (dw_pallets_list.GetItemValue<Int64>(ll_i, "auto_scan_status") > mini_terminal.uo_mini_terminal_pallets_list.NEW_NOT_APPROVED)
                {
                    ls_pallets_list[ll_i] = dw_pallets_list.GetItemValue<string>(ll_i, "pallet_number");
                }
            }
            if (CollectionExtensions.UBound(ls_pallets_list) == -1)
            {
                await uf_show_message("שגיאה", "", "OK", " אין משטחים שכבר נסרקו במשלוח זה ");
                return false;
            }
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_ret = ldwc_head.Retrieve(gs_vars.branch_number, ll_shipment_number, il_employee_number)

            ll_ret = ldwc_head.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_employee_numberProperty, ll_shipment_number, this.ViewModel.il_marlog_branchProperty);
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - sort the pallets list

            lds_shipment_invoices.SetSort("pallet_number ASC");

            lds_shipment_invoices.Sort();
            for (ll_i = 0; ll_i < ll_rows; ll_i++)
            {
                ll_supplier_number = lds_shipment_invoices.GetItemValue<long>((int)ll_i, "supplier_number");
                ll_invoice_number = (long)lds_shipment_invoices.GetItemValue<double>((int)ll_i, "invoice_number");
                //ls_pallet_number = lds_shipment_invoices.GetItem<string>(ll_i, "pallet_number")
                // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - check pallet's items were scanned
                if (ls_pallet_number != lds_shipment_invoices.GetItemValue<string>(ll_i, "pallet_number"))
                {

                    ls_pallet_number = lds_shipment_invoices.GetItemValue<string>(ll_i, "pallet_number");
                    lb_not_scanned_pallet = false;
                    if (uf_check_pallet_items_scanned(ls_pallet_number))
                    {
                        lb_not_scanned_pallet = true;
                        continue;
                    }
                }
                else
                {
                    if (lb_not_scanned_pallet)
                    {
                        continue;
                    }
                }

                ll_ret = lds_footer_temp.Retrieve(ls_pallet_number, masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_invoice_number);
                if (ll_ret > 0)
                {
                    lds_footer_temp.RowsMove(0, lds_footer_temp.RowCount(), ModelBuffer.Primary, ldwc_footer, ldwc_footer.RowCount() + 1, ModelBuffer.Primary);
                }
                // AlexKh - 1.1.26.0 - 2015-01-27 - SPUCM00005189 - add part with excess items


                ll_found = ldwc_excess.Find("b2b_declines_move_parent_doc_number == " + ll_invoice_number.ToString(), 0, ldwc_excess.RowCount());
                if (ll_found < 0)
                {

                    ll_ret = lds_excess.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_invoice_number);
                    if (ll_ret > 0)
                    {
                        lds_excess.RowsMove(0, lds_excess.RowCount(), ModelBuffer.Primary, ldwc_excess, ldwc_excess.RowCount() + 1, ModelBuffer.Primary);
                    }
                }
            }


            //GalilCS - filter repository like source
            ll_ret = ldwc_footer.SetFilter("(actual_quantity-original_quantity) != 0");
            ll_ret = ldwc_footer.Filter();

            ll_ret = ldwc_footer.SetSort("item_barcode ASC");

            ll_ret = ldwc_footer.Sort();
            // AlexKh - 1.1.28.0 - 2015-02-17 - SPUCM00005224 - remove duplicate barcodes

            ll_rows = ldwc_footer.RowCount();
            for (ll_i = 1; ll_i < ll_rows; ll_i++)
            {
                if (ldwc_footer.GetItemValue<string>((int)ll_i, "item_barcode") == ldwc_footer.GetItemValue<string>((int)(ll_i - 1), "item_barcode") &&
                    (ldwc_footer.GetItemValue<Int64>((int)ll_i, "parent_doc_number") == ldwc_footer.GetItemValue<Int64>((int)(ll_i - 1), "parent_doc_number") &&
                    (ldwc_footer.GetItemValue<decimal>((int)ll_i, "original_quantity") == 0 &&
                    ldwc_footer.GetItemValue<decimal>((int)(ll_i - 1), "original_quantity") == 0)))
                {

                    ldwc_footer.Delete((int)ll_i);
                    ll_rows = ll_rows - 1;
                    ll_i = ll_i - 1;
                }
            }
            // AlexKh - 1.2.48.0.13 - 2011-11-07 - SPUCM00002930 - choose copies number


            var copies = await WindowHelper.Open<rw_print_copies_number>("mini_terminal_mini_terminal", "rowsCount", 0);


            ll_print_copies_number = Convert.ToInt64(WindowHelper.GetParam<string>(copies));
            for (ll_i = 1; ll_i <= ll_print_copies_number; ll_i++)
            {
                ldwc_head.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName, lds_excess, ldwc_footer);
            }
            return true;
        }




        public async Task<bool> uf_scan_pallet(string as_pallet_number, long al_row) //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_scan_pallet
            //*Purpose: 			Scan pallet using masofon
            //*Arguments: 		String	as_pallet_number
            //*						Long		al_row
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*14-11-2010		AlexKh			1.2.46.3	SPUCM00002425	Initial version
            //********************************************************************

            long ll_ret = 0;
            long? ll_found = 0;
            long ll_scan_status = 0;
            string ls_find = null;
            if (isempty_stringClass.isempty_string(as_pallet_number) || as_pallet_number == "0")
            {
                return false;
            }
            if (al_row == -1)
            {
                ls_find = "pallet_number == \"" + as_pallet_number.ToUpper() + "\"";
                ll_found = dw_pallets_list.Find(ls_find, 0, dw_pallets_list.RowCount());
                if (ll_found >= 0)
                {
                    ll_scan_status = dw_pallets_list.GetItemValue<long>(ll_found.Value, "auto_scan_status");
                    if (ll_scan_status == mini_terminal.uo_mini_terminal_pallets_list.NEW_NOT_APPROVED)
                    {
                        al_row = ll_found.Value;
                    }
                    else
                    {
                        await uf_show_message("הודעה", "", "OK", "משטח זה נסרק בעבר");
                        dw_pallets_list.Rows[(int)ll_found.Value].Selected = true;
                        return false;
                    }
                }
                else
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצא משטח מספר " + as_pallet_number);
                    return false;
                }
                if (ll_found == -1)
                {
                    return false;
                }
            }
            if (as_pallet_number.ToUpper() == dw_pallets_list.GetItemValue<string>(al_row, "pallet_number"))
            {
                dw_pallets_list.SetItem(al_row, "approve_ind", Convert.ToInt64("1"));
                dw_pallets_list.SetItem(al_row, "arrive_datetime", Convert.ToDateTime(masofonAlias.masofon.Instance.set_machine_time().ToString()));
                dw_pallets_list.SetItem(al_row, "approve_employee", Convert.ToInt64(this.ViewModel.il_employee_numberProperty.ToString()));
                dw_pallets_list.SetItem(al_row, "state", "a");
                dw_pallets_list.SetItem(al_row, "new_scan", Convert.ToInt64("1")); //SPUCM00005249@@25/02/2015
                uf_set_pallet_scan_type(al_row);
                dw_pallets_list.Rows[(int)al_row].Selected = true;

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = dw_pallets_list.Update(unitOfWork);
                }

                if (ll_ret != 1)
                {
                    await uf_show_message("שגיאה", "", "OK", "עדכון סטאטוס משטח נכשל");
                    return false;
                }
                dw_pallets_list.SetRepository(dw_pallets_list.Repository());
            }
            else
            {
                await uf_show_message("שגיאה", "", "OK", "מספר משטח שהוקלד, לא תקין");
                return false;
            }
            return true;
        }

        public bool uf_check_pallet_items_scanned(string as_pallet_number) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_check_pallet_items_scanned
            //*Purpose: 			Check if at least one item scanned for specific pallet
            //*Arguments: 		Value		String as_pallet_number
            //*Return:				Boolean	
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*20-02-2013		AlexKh			1.0.0.1	SPUCM00004018	Initial version
            //********************************************************************

            long ll_counter = 0;
            if (true)
            {
                ViewModel.LoadData(as_pallet_number, ref ll_counter);
            }
            if (ll_counter > 0)
            {
                return false;
            }
            return true;
        }
        public async Task<bool> uf_handle_item_expiration_date(DateTime? adt_expiration_date, long al_min_valid_month, string as_material_name, long al_material_number) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_handle_item_expiration_date
            //*Purpose: 			Check if item requires expiration date
            //*Arguments: 		ref		datetime		adt_expiration_date
            //*						value		long			al_min_valid_month
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*15-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            bool lb_303_department = false;


            lb_303_department = await (this.ViewModel.Parent.Parent as w_mini_terminal).wf_check_303_department(al_material_number);
            // AlexKh 1.1.1.3 - 2013-03-07 - SPUCM00004018 - check department 303 for expiration date
            // Only if the material has minimum valid months, it should have expiration date
            //IF NOT(IsNull(al_min_valid_month) OR al_min_valid_month = 0) THEN
            if (!(al_min_valid_month == null || al_min_valid_month == 0) && (adt_expiration_date == default(DateTime) || (lb_303_department)))
            {

                var result = (await (this.ViewModel.Parent.Parent as w_mini_terminal).wf_show_expiration_msg(adt_expiration_date, al_min_valid_month, as_material_name, false)).Retrieve(out adt_expiration_date);
                if (!result)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
        public async Task<int> uf_send_msg(long al_shipment_number) //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:	uo_mini_terminal_pallets_list
            //*Function Name:	(uf_send_msg)
            //*Purpose: 	Check if the message already sent if no then if it scand' send it
            //*Arguments: 	(none)
            //*Return:	integer
            //*Programmer	Date   Version	Task#	 Description
            //*---------------------------------------------------------------------
            //*RonY@12/01/2015 -  1.12.49.12 SPUCM00005157
            //********************************************************************

            int li_approve_ind = 0;
            long ll_index = 0;
            long ll_rowcount = 0;
            string ls_message_state = null;
            string ls_messagebox_text = null;
            IRepository lds_pallets_list = null;




            dw_pallets_list.PerformValidated(null);


            lds_pallets_list = dw_pallets_list.GetDataObject();

            //lds_pallets_list.SetDataRows(dw_pallets_list.DataRows());



            ls_messagebox_text = await this.ViewModel.invuo_b2b_palletsProperty.uf_b2b_send_pallets_approve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_shipment_number, this.ViewModel.il_marlog_branchProperty, lds_pallets_list);
            if (!isempty_stringClass.isempty_string(ls_messagebox_text))
            {
                await uf_show_message("הודעה", "", "OK", ls_messagebox_text);
            }



            return 1;
        }

        public void destructor()
        {
            if (this.ViewModel.iuo_order_searchProperty != null)
            {


            }
            if (this.ViewModel.iuo_material_updateProperty != null)
            {



            }
            if (this.ViewModel.invuo_b2b_palletsProperty != null) //RonY@12/01/2015 1.12.49.63 - SPUCM00005157
            {


            }
        }

        public async Task sle_scan_line_ue_enter(object sender, KeyEventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - SPUCM00002425  - ue_enter
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            if (key == Keys.Enter || key == Keys.Tab)
            {
                enterKeyFlag = true;
                await scan_line_enter(sender, e);
            }
            else
            {
                //		CHOOSE CASE Key
                //			CASE Key1!, Key2!,Key3!, Key4!, Key5!, Key6!, Key7!, Key8!, Key9!, Key0!
                //				RETURN 1
                //			CASE KeyNumPad1!, KeyNumPad2!,KeyNumPad3!, KeyNumPad4!, KeyNumPad5!, KeyNumPad6!, KeyNumPad7!, KeyNumPad8!, KeyNumPad9!, KeyNumPad0!
                //				RETURN 1
                //		END CHOOSE		
                //		sle_scan_line.text = ""
                //return 1;
            }
            //return 1;
        }
        public async Task scan_line_enter(object sender, EventArgs e)
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_scan_line.Text != "")
                {
                    if (await uf_scan_pallet(sle_scan_line.Text, -1))
                    {
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, ee) =>
                        {
                            sle_scan_line.Focus();
                        }, 1);
                    }
                    sle_scan_line.Text = "";
                    enterKeyFlag = false;
                }
            }
        }
        public void sle_scan_line_getfocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - getfocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            sle_scan_line.Text = "";
        }

        public void sle_scan_line_losefocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - losefocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            sle_scan_line.Text = "";

        }

        public async Task cb_print_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_print.clicked event
            //*Purpose:							Print Driver report
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*26-07-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            if (await f_get_parameter_valueClass.f_get_parameter_value("run_temp_driver_report", "boolean", "false", "Run temporary report.", "gds_find") == "true")
            {
                await uf_print_temp_driver_report();
            }
            else
            {
                await uf_print_driver_report();
            }

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.DroppedDown = true;
                dw_shipments_list.Focus();

            }
        }

        public void st_pallets_list_getfocus(object sender, EventArgs e)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public void st_shipment_number_getfocus(object sender, EventArgs e)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public void st_title_getfocus(object sender, EventArgs e)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public int dw_shipments_list_ue_enter(Keys key, uint keyflags) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - ue_enter
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
            {
                dw_pallets_list.Focus();
            }
            return 1;
        }

        //gilad test calld after shipment number selected
        public async Task Combobox_shipments_list_itemchanged(object sender, EventArgs e) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138  - ItemChanged
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            string data = (sender as ComboGridElement).SelectedItem.ToString();
            if (data == "הבא הכל")//to get all shipment list
                await uf_display("SHIPMENT_RESET_ALL");
            else
            {
                int row = (sender as ComboGridElement).SelectedIndex;
                await dw_shipments_list_itemchanged(row, data);
                sle_scan_line.Focus();
            }
        }

        public async Task dw_shipments_list_itemchanged(int row, string data) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138  - ItemChanged
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            IRepository _order_list = new d_mini_terminal_pallets_listRepository();
            long ll_ret = 0;
            long ll_shipment_number = 0;
            string ls_params = null;

            // AlexKh - 1.2.46.5 - 2010-12-20 - SPUCM00002425 - Get params and prepare order diff report
            if (!string.IsNullOrEmpty(Convert.ToString(dw_shipments_list.PreviousValue)))
            {
                ll_shipment_number = Convert.ToInt64(dw_shipments_list.PreviousValue);
                if (ll_shipment_number > 0)
                {
                    await uf_send_msg(ll_shipment_number); //RonY@12/01/2015 1.12.49.12 - SPUCM00005157
                    ls_params = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "@" + ll_shipment_number.ToString() + "@" + "-1" + "@";
                    uf_write_log(dw_shipments_list.GetType().Name.ToString() + " - itemchanged Event, Open rw_diff_order_mt_report.");
                    //Show items that were ordered and not supplied
                    var diff_order_mt_report = await WindowHelper.Open<rw_diff_order_mt_report>("global_global", "ls_params", ls_params);
                }

            }

            if (!isempty_stringClass.isempty_string(data) && SystemFunctionsExtensions.IsNumber(data))
            {
                // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
                //ll_ret = dw_pallets_list.Retrieve(gs_vars.branch_number, Long(data))
                ll_ret = _order_list.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, Convert.ToInt64(data), this.ViewModel.il_marlog_branchProperty);
                if (ll_ret < 0)
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצאו משטחים למספר משלוח " + data);
                }
                else
                {
                    dw_pallets_list.SetRepository(_order_list);
                    // dw_pallets_list.Focus();
                    //dw_pallets_list.setcolumn('pallet_input_barcode')
                }
            }
            else if (isempty_stringClass.isempty_string(data))
            {

                dw_pallets_list.Reset();
                dw_shipments_list.Focus();
            }
            else
            {
                await uf_show_message("שגיאה", "", "OK", " מספר משלוח לא תקין ");
                dw_shipments_list.Focus();
                //dw_shipments_list.SelectText(1, 30);
            }
            dw_pallets_list.Columns["pack_quantity"].ReadOnly = true;
            dw_pallets_list.Columns["pallet_number"].ReadOnly = true;

            //}
        }

        public int dw_shipments_list_editchanged(object sender, EventArgs e) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138  - EditChanged
        {
            int row = 0;
            var dwo = sender as ControlElement;
            ValueChangedArgs<string> args = e as ValueChangedArgs<string>;
            if (args == null) return 0;
            string data = args.Value;
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");

            if (dwo.ID == "dw_shipments_list")
            {

                if (!SystemFunctionsExtensions.IsNumber(data))
                {
                    //dw_shipments_list.SelectText(1, 99);
                    dw_shipments_list.Text = "";
                }
                if (data?.Length > 20)
                {
                    dw_shipments_list.Text = dw_shipments_list.Text.Substring(0, dw_shipments_list.Text.Length - 1);

                    //dw_shipments_list.SetItem(0, "shipment_number", Convert.ToInt64(data.Substring(0, 19)).ToString());
                    //dw_shipments_list.SelectText(20, 1);
                    //dw_shipments_list.Clear();
                    return 0;
                }
            }
            return 0;
        }

        public void dw_shipments_list_clicked(int xpos, int ypos, int row, ControlElement dwo)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");

            //dw_shipments_list.SelectText(1, 30);
        }

        public async Task cb_finish_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish the masofon input.
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            await this.uf_close_window();
        }

        public async Task<int> dw_pallets_list_ue_enter(Keys key, uint keyflags) // AlexKh - 1.2.46.4 - 2010-11-11 - SPUCM00002425  - ue_enter
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
            {
                await uf_show_message("הודעה", "", "OK", dw_pallets_list.GetItemValue<string>(0, "pallet_input_barcode"));
            }
            return 1;
        }

        public async Task dw_pallets_list_buttonclicked(object sender, EventArgs e) //**********************************************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            int row = dw_pallets_list.SelectedRowIndex;
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			buttonclicked event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************

            long ll_ret = 0;
            long ll_shipment_number = 0;
            string ls_pallet_number = null;
            ValueChangedArgs<string> data = e as ValueChangedArgs<string>;
            if (data != null)
            {
                switch (data.Value)
                {
                    case "click,b_manual":
                        await uf_manual_scan(Convert.ToInt64(row));
                        break;
                    case "click,b_automatic":
                        if (await uf_automatic_scan(Convert.ToInt32(row)))
                        {

                            dw_pallets_list.SetItem((long)row, "auto_scan_status", mini_terminal.uo_mini_terminal_pallets_list.ACTIVE_AUTOMATIC_SCANNED);

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                ll_ret = dw_pallets_list.Update(unitOfWork);
                            }

                        }
                        //GalilCS - Render grid again 
                        int _selectedRow = dw_pallets_list.SelectedRowIndex;
                        dw_pallets_list.SetRepository(dw_pallets_list.Repository());
                        if (_selectedRow > -1 && _selectedRow < dw_pallets_list.Rows.Count)
                            dw_pallets_list.Rows[_selectedRow].Selected = true;
                        break;
                    default:
                        break;
                }
            }
        }

        public void dw_pallets_list_getfocus(object sender, EventArgs e) //**********************************************************************************************
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("dw_shipments_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			getfocus event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************


            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
                sle_scan_line.Text = "";
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public async Task dw_pallets_list_clicked(object sender, GridElementCellEventArgs e) //**********************************************************************************************
        {
            int row = e.RowIndex;
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");

            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			clicked event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //************************************************************************************************

            string ls_pallet_number = null;
            long ll_scan_status = 0;
            long ll_ret = 0;
            long ll_shipment_number = 0;

            if (dw_pallets_list.Columns[e.ColumnIndex].ID == "approve_ind")
            {

                ll_scan_status = dw_pallets_list.GetItemValue<long>(row, "auto_scan_status");

                ll_shipment_number = (long)dw_pallets_list.GetItemValue<double>(row, "shipment_number");
                if (ll_scan_status == mini_terminal.uo_mini_terminal_pallets_list.NEW_NOT_APPROVED)
                {
                    var rw_pallet = await WindowHelper.Open<rw_pallet>("mini_terminal_mini_terminal", "rowCount", 0);

                    ls_pallet_number = WindowHelper.GetParam<string>(rw_pallet);
                    if (await uf_scan_pallet(ls_pallet_number, row))
                    {
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, ee) =>
                        {
                            sle_scan_line.Focus();
                        }, 1);
                    }
                }
            }
        }

        public async Task dw_pallets_list_itemchanged(int row, ControlElement dwo, string data)
        {

            if (dwo.ID == "pallet_input_barcode")
            {
                await uf_show_message("הודעה", "", "OK", data);
            }
        }

        public void dw_pallets_list_doubleclicked(int xpos, int ypos, int row, ControlElement dwo) //Eitan; SPUCM00005359;
        {
            long ll_row = 0;
            s_array_arguments lstr_parms = default(s_array_arguments);
            //
            //IF row <= 0 THEN
            //	RETURN
            //END IF
            //
            //lstr_parms.a_string[1] = dw_pallets_list.GetItem<string>(row, "pallet_number")
            //lstr_parms.a_long[1] = dw_pallets_list.GetItem<int>(row, "branch_number")
            //
            //OpenWithParm(rw_pallet_details, lstr_parms)
            //
        }

        private async void dw_shipments_list_ClickedAction(object sender, EventArgs e)
        {

        }


        private async void dw_shipments_list_CellClickedAction(object sender, GridElementCellEventArgs e)
        {

        }

        private async void dw_shipments_list_MouseDownAction(object sender, MouseEventArgs e)
        {

        }


        private async void dw_shipments_list_ComboCellSelectedIndexChangedAction(object sender, GridComboBoxColumnEventArgs e)
        {

        }
        

    }
}
