using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Threading.Tasks;
using global;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class uo_mini_terminal_trade_siteController : MvcSite.Common.SPBaseController
    {

        public ActionResult uo_mini_terminal_trade_site()
        {
            return this.View(new uo_mini_terminal_trade_site());
        }
        private uo_mini_terminal_trade_site ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_trade_site; }
        }

        public static bool enterKeyFlag { get; private set; }

        public void Form_Load(object sender, EventArgs e)
        {

            //this.ViewModel.constructor();
        }
        public int uf_write_log(string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_write_log
            //*Purpose: 			Write into log file
            //*Arguments: 		String - as_message - message
            //*Return:				Integer
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************

            SystemHelper.WriteToLog(as_message, string.Empty);
            return 1;
        }
        public async Task<int> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_show_message
            //*Purpose: 			Display a messagebox with the given message text.
            //*Arguments: 		String 	as_title 					
            //*						String	as_error_text		
            //*						String 	as_buttons	
            //*						String 	as_message	
            //*Return:				Integer	1 - Success
            //*									-1 -Otherwise
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138				Initial version
            //************************************************************************************************

            int li_rtn_code = 0;
            //debug -Start
            //Write to log
            string ls_message = null;
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "Start of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);
            //debug -END

            await MessageBox.Show(as_message, as_title, MessageBoxButtons.OK);
            //debug -Start
            //Write to log
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "End of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);
            //debug -END
            return li_rtn_code;
        }
        public async Task<int> uf_close_window() //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_close_window
            //*Purpose: 			close window
            //*Arguments: 		none
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

         //   bool lb_save_before_close = false;
            string ls_message = null;
            string ls_params = null;
            int li_rtn_code = 0;
            long ll_shipment_number = 0;
            // AlexKh - 1.2.46.5 - 2010-12-20 - SPUCM00002425 - Get params and prepare order diff report
            //ll_shipment_number = dw_shipments_list.GetItem<int>(1, "shipment_number")
            if (ll_shipment_number > 0)
            {
                ls_params = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "@" + ll_shipment_number.ToString() + "@" + "-1" + "@";
                uf_write_log(this.ViewModel.GetType().Name.ToString() + " - uf_close_window Function, Open rw_diff_order_mt_report.");
                //Show items that were ordered and not supplied
                await WindowHelper.Open<rw_diff_order_mt_report>("global_global", "ls_params", ls_params);
                // Vika 2012-05-29 SPUCM00003580 ביטול חשבונית מרלוג בסניף
                uf_write_log(this.ViewModel.GetType().Name.ToString() + " - uf_close_window Function, Open rw_not_arrived_inv_report.");
                //Show items that were ordered and not supplied
                await WindowHelper.Open<rw_not_arrived_inv_report>("global_global", "ls_params", ls_params);
                // End Vika 2012-05-29 SPUCM00003580 ביטול חשבונית מרלוג בסניף
            }
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
            li_rtn_code = uf_write_log(ls_message);
            if (li_rtn_code < 1)
            {
                await uf_display("ENTRANCE");
                return -1;
            }
            await uf_display("ENTRANCE");
            this.ViewModel.Visible = false;
            //dw_shipments_list.setfocus()
            return 1;
        }
        public async Task<int> uf_display(string as_name) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_display
            //*Purpose: 			open appropriate user object
            //*Arguments: 		String	as_name
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*17-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            int li_ret = 0;
            long ll_row_count = 0;


            li_ret = await (w_mini_terminal.Instance().wf_display(as_name));
            return 1;
        }
      
        public async Task<bool> uf_handle_item_expiration_date(DateTime? adt_expiration_date, long al_min_valid_month, string as_material_name, long al_material_number) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_handle_item_expiration_date
            //*Purpose: 			Check if item requires expiration date
            //*Arguments: 		ref		datetime		adt_expiration_date
            //*						value		long			al_min_valid_month
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*15-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            var lb_303_department = await (ViewModel.Parent.Parent as w_mini_terminal).wf_check_303_department(al_material_number);
            // AlexKh 1.1.1.3 - 2013-03-07 - SPUCM00004018 - check department 303 for expiration date
            // Only if the material has minimum valid months, it should have expiration date
            //IF NOT(IsNull(al_min_valid_month) OR al_min_valid_month = 0) THEN
            if (!(al_min_valid_month == null || al_min_valid_month == 0) && (adt_expiration_date == default(DateTime) || (lb_303_department)))
            {
                if (! (await ((this.ViewModel.Parent.Parent.Parent as w_mini_terminal).wf_show_expiration_msg(adt_expiration_date, al_min_valid_month, as_material_name, false))).Retrieve(out adt_expiration_date))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }
        public async Task<bool> uf_scan_pallet(string as_site_order_number, long al_row) //********************************************************************
        {
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            //*Object:				uo_mini_terminal_trade_site
            //*Function Name:	uf_scan_pallet
            //*Purpose: 			Scan site order using masofon
            //*Arguments: 		String	as_site_order_number
            //*						Long		al_row
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version		Task#	 	Description
            //*---------------------------------------------------------------------
            //*02-05-2013		AlexKh			1.2.48.49	CR#1169	Initial version
            //********************************************************************

            long ll_ret = 0;
            long? ll_found = 0;
            string ls_find = null;
            string ls_state = null;
            if (isempty_stringClass.isempty_string(as_site_order_number) || as_site_order_number == "0")
            {
                return false;
            }
            // AlexKh - 1.1.1.15 - 2013-12-04 - SPUCM00004086 - remove last digit from the barcode, leave 12
            //Eitan - comment
            //as_site_order_number = left(as_site_order_number, len(as_site_order_number) - 1)
            if (al_row == -1)
            {
                ls_find = "site_order_number == " + as_site_order_number;


                ll_found = dw_orders_list.Find(ls_find, 0, dw_orders_list.RowCount());
                if (ll_found >= 0)
                {

                    ls_state = dw_orders_list.GetItemValue<string>(ll_found.Value, "state");
                    if (ls_state == mini_terminal.uo_mini_terminal_trade_site.NOT_APPROVED || ls_state == mini_terminal.uo_mini_terminal_trade_site.PARTIAL) //Eitan; Build 29; 19/2/2015; Add PARTIAL state
                    {
                        al_row = ll_found.Value;
                    }
                    else
                    {
                        await uf_show_message("הודעה", "", "OK", "הזמנה זו נסרקה בעבר");
                    }
                }
                else
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצאה הזמנה מספר " + as_site_order_number);
                }
                if (al_row == -1)
                {
                    return false;
                }
            }

            if (Convert.ToDouble(as_site_order_number) == dw_orders_list.GetItemValue<double>(al_row, "site_order_number"))
            {

                dw_orders_list.SetItem(al_row, "approve_ind", 1);

                dw_orders_list.SetItem(al_row, "target_branch_datetime", masofonAlias.masofon.Instance.set_machine_time());
                if (ls_state != mini_terminal.uo_mini_terminal_trade_site.PARTIAL) //Eitan; Build 34; 15/4/2015;
                {

                    dw_orders_list.SetItem(al_row, "state", Convert.ToString(mini_terminal.uo_mini_terminal_trade_site.APPROVED));
                }
                // AlexKh - 1.1.23.0 - 2014-08-27 - SPUCM00004984 - move update inside send function
                //ll_ret = dw_orders_list.Update()
                //IF ll_ret<> 1 THEN


                if (!await uf_send_trade_order_status((long)dw_orders_list.GetItemValue<double>(al_row, "site_order_number"), dw_orders_list.GetItemValue<DateTime>((int)al_row, "date_move_short")))
                {
                    await uf_show_message("שגיאה", "", "OK", "עדכון סטטוס הזמנה נכשל");
                    return false;
                }
            }
            else
            {
                await uf_show_message("שגיאה", "", "OK", " מספר הזמנה שהוקלד, לא תקין ");
                return false;
            }
            // AlexKh - 1.1.23.0 - 2014-08-27 - SPUCM00004984 - move send function
            //uf_send_trade_order_status(dw_orders_list.GetItem<int>(al_row, "site_order_number"), dw_orders_list.GetItemDate(al_row, "date_move_short"))
            //Eitan; Build 34; 15/4/2015;
            if (ls_state == mini_terminal.uo_mini_terminal_trade_site.PARTIAL)
            {
                await uf_show_message("סריקה", "", "OK", "הזמנה חלקית נקלטה בהצלחה");
            }
            else
            {
                await uf_show_message("סריקה", "", "OK", "הזמנה נקלטה בהצלחה");
            }
            return true;
        }
        public async Task<bool> uf_send_trade_order_status(long al_site_order_number, DateTime? ad_site_order_date)
        {
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            string ls_edi = null;
            string ls_message_type = null;
            char lc_site_state = '\0';
            long ll_row = 0;
            long ll_ret = 0;
            int li_order_status = 0;
            int li_message_type = 0;
            int ll_status = 0;
            IRepository lds_web_trade_header;
            IRepository[] lds_data = new IRepository[2];
            nvuo_b2b_web_trade lnvuo_b2b_web_trade = default(nvuo_b2b_web_trade);
            lnvuo_b2b_web_trade = new nvuo_b2b_web_trade();




            lds_web_trade_header = new d_web_trade_headerRepository();


            ls_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(157); //check for b2b supplier for professional
            if (string.IsNullOrEmpty(ls_edi))
            {
                uf_write_log("no edi number returned for supplier: " + 157.ToString());
                return false;
            }
            //Eitan; 12/3/2015; Build 32; Check for Partial Delivery
            if (true)
            {
                ViewModel.LoadData(al_site_order_number, ref lc_site_state);
            }
            if (lc_site_state.ToString() == "p")
            {
                ls_message_type = "STOR12";
            }
            else
            {
                ls_message_type = "STOR02";
            }
            //ls_message_type = 'STOR02'

            ll_row = lds_web_trade_header.Insert(0);

            lds_web_trade_header.SetItem(ll_row, "statusname", "Order_in_branch");

            lds_web_trade_header.SetItem(ll_row, "statuscode", "00011");

            lds_web_trade_header.SetItem(ll_row, "statusdatetime", Convert.ToInt64(masofonAlias.masofon.Instance.set_machine_time().Value.ToString("yyyyMMddHHmm")));

            lds_web_trade_header.SetItem(ll_row, "orderorgno", al_site_order_number.ToString());

            lds_web_trade_header.SetItem(ll_row, "orderdate", Convert.ToDouble(ad_site_order_date.Value.ToString("yyyyMMdd")));
            lds_data[1] = lds_web_trade_header;
            await lnvuo_b2b_web_trade.uf_init_web_trade(ls_message_type, al_site_order_number);
            if ((await lnvuo_b2b_web_trade.uf_send_transaction(ll_status, ls_edi, lds_data)).Retrieve(out ll_status, out lds_data))
            {
                // AlexKh - 1.1.23.0 - 2014-08-27 - SPUCM00004984 - move update inside send function

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = dw_orders_list.Update(unitOfWork);
                }

                if (ll_ret != 1)
                {
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("שגיאה", "", "StopSign!", "OK", 1, "כישלון בעדכון סטטוס הזמנת אתר סחר");
                    return false;
                }
            }
            else
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("שגיאה", "", "StopSign!", "OK", 1, "כישלון בשליחת מסר לאתר סחר");
                return false;
            }
            li_order_status = 11;
            li_message_type = 4;
            //Eitan; 12/3/2015; Build 32;
            //IF NOT lnvuo_b2b_web_trade.uf_update_outgoing(li_message_type, li_order_status, "STOR02", al_site_order_number, al_site_order_number, 157, gs_vars.branch_number) THEN
            if (!(await lnvuo_b2b_web_trade.uf_update_outgoing(li_message_type, li_order_status, Convert.ToString(ls_message_type), al_site_order_number, al_site_order_number, 157, masofonAlias.masofon.Instance.gs_vars.branch_number)).Retrieve(out li_order_status))
            {
                uf_write_log("Function: uf_update_outgoing: failed." + "\r" + "\n");
                return false;
            }
            return true;
        }

        public void destructor()
        {
            if (this.ViewModel.iuo_order_searchProperty != null)
            {


            }
            if (this.ViewModel.iuo_material_updateProperty != null)
            {

            }
        }

        public async Task<int> sle_scan_line_ue_enter(object sender, KeyEventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - SPUCM00002425  - ue_enter
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            if (key == Keys.Enter || key == Keys.Tab)
            {
                enterKeyFlag = true;
                await scan_line_enter(sender, e);
            }
            else
            {
                                return 1;
            }
            return 1;
        }
        public async Task scan_line_enter(object sender, EventArgs e)
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_scan_line.Text != "")
                {
                    await uf_scan_pallet(sle_scan_line.Text, -1);
                    sle_scan_line.Text = "";
                    enterKeyFlag = false;
                }
            }
        }
        public void sle_scan_line_getfocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - getfocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            //Rectangle r_pallets_list = this.GetVisualElementById<Rectangle>("r_pallets_list");
            // 
            //r_pallets_list.set_LineColor(134217729);
            sle_scan_line.Text = "";
        }

        public void sle_scan_line_losefocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - losefocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            //Rectangle r_pallets_list = this.GetVisualElementById<Rectangle>("r_pallets_list");
            // 
            //r_pallets_list.set_LineColor(67108864);
            sle_scan_line.Text = "";
        }

        public void st_title_getfocus(object sender, EventArgs e)
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");

            if (dw_orders_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
        }

        public async Task cb_finish_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish the masofon input.
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            await this.uf_close_window();
        }

        public async Task<int> dw_orders_list_ue_enter(Keys key, uint keyflags) // AlexKh - 1.2.46.4 - 2010-11-11 - SPUCM00002425  - ue_enter
        {
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
            {

                await uf_show_message("הודעה", "", "OK", dw_orders_list.GetItemValue<string>(0, "pallet_input_barcode"));
            }
            return 1;
        }

        public void dw_orders_list_getfocus(object sender, EventArgs e) //**********************************************************************************************
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			getfocus event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************


            if (dw_orders_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
                sle_scan_line.Text = "";
            }
        }

        public async Task dw_orders_list_clicked(object sender, GridElementCellEventArgs e) //**********************************************************************************************
        {

            int row = e.RowIndex;

            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			clicked event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //************************************************************************************************

            string ls_site_order_number = null;
            string ls_state = null;
            long ll_scan_status = 0;
            long ll_ret = 0;
            long ll_site_order_number = 0;

            if (dw_orders_list.Columns[e.ColumnIndex].ID == "approve_ind")
            {

                ls_state = dw_orders_list.GetItemValue<string>(row, "state");

                ll_site_order_number = (long)dw_orders_list.GetItemValue<double>(row, "site_order_number");
                if (ls_state == mini_terminal.uo_mini_terminal_trade_site.NOT_APPROVED)
                {
                    var pallet = await WindowHelper.Open<rw_pallet>("mini_terminal_mini_terminal", "rowCount", 0);

                    ls_site_order_number = WindowHelper.GetParam<string>(pallet);
                    await uf_scan_pallet(ls_site_order_number, row);
                }
            }
        }

        public async Task dw_orders_list_itemchanged(int row, ControlElement dwo, string data)
        {

            if (Convert.ToString(dwo.ID) == "site_order_number")
            {
                await uf_show_message("הודעה", "", "OK", data);
            }
        }
    }
}
