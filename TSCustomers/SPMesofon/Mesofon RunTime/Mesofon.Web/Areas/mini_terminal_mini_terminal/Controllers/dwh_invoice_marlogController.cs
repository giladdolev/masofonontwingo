using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_invoice_marlogController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_invoice_marlog()
		{
			dwh_invoice_marlogRepository repository = new dwh_invoice_marlogRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
