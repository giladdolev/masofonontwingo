using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_packing_list_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_packing_list_details()
		{
			d_packing_list_detailsRepository repository = new d_packing_list_detailsRepository();
			
			return this.View(repository);
		}
	}
}
