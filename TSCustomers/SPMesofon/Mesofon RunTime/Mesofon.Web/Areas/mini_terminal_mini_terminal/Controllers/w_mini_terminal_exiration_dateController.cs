using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Extensions;
using System.Threading.Tasks;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;
using System.Web.VisualTree;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Mesofon.MiniTerminal.Controllers
{
    public class w_mini_terminal_exiration_dateController : Controller
    {

        public ActionResult w_mini_terminal_exiration_date(s_array_arguments istr_arg)
        {
            return this.View(new w_mini_terminal_exiration_date(istr_arg));
        }
        private w_mini_terminal_exiration_date ViewModel
        {
            get { return this.GetRootVisualElement() as w_mini_terminal_exiration_date; }
        }
        //need to subscribe in aspx (missing) ---avi 28/6
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public async Task ue_close_with_return(bool ab_variable) //--------------------------------------------------------------------
        {
            MaskedTextBoxElement em_expiration_date = this.GetVisualElementById<MaskedTextBoxElement>("em_expiration_date");
            //Function:			public w_mini_terminal_exiration_date.ue_close_with_return()
            //
            // Returns:         Integer
            //
            // Parameters:      value Boolean ab_variable -> 
            // 								if true close with return value, otherwise close without return
            // 
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					On Ok button/Enter clicked checkes inserted expiration date 
            //					if it greate than  idt_min_expiration_date return it to parent windowstate,
            //					otherwise display error message and return from this event.
            //					On Cancel button click do not return value
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_message = null;
            DateTime ldt_expiration_date;
            s_array_arguments lstr_mess = new s_array_arguments();
            TextBoxElement txt_expiration_date = this.GetVisualElementById<TextBoxElement>("txt_expiration_date");
            CultureInfo provider = CultureInfo.InvariantCulture;
            bool isParsed = false;

            if (!DateTime.TryParseExact(em_expiration_date.Text, "ddMMyyyy", provider, DateTimeStyles.None, out ldt_expiration_date) && ab_variable)
            {
                ls_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("יש להזין תאריך חוקי");
                await wf_show_message("", "", "OK", ls_message);
                txt_expiration_date.Text = "";
                em_expiration_date.Text = "00000000";
                txt_expiration_date.Focus();
                return;
            }


            if (ldt_expiration_date < DateTime.Now && ab_variable)
            {
                ls_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("יש להזין תאריך תוקף שגדול מהיום");
                await wf_show_message("", "", "OK", ls_message);
                return;
            }
            ls_message = "";
            if (ab_variable)
            {
                if (this.ViewModel.idt_min_expiration_dateProperty > ldt_expiration_date)
                {
                    ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("שים לב, תאריך התוקף").Result + " ";
                    ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אינו עומד בדרישות").Result + ", ";
                    ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("הפריט יופיע בדוח הפרשים").Result;
                    await wf_show_message("", "", "OK", ls_message);
                    lstr_mess.a_boolean[0] = false;
                    lstr_mess.a_string[0] = "DateNotValid";
                }
                else
                {
                    lstr_mess.a_boolean[0] = true;
                }
                lstr_mess.a_datetime[0] = ldt_expiration_date;
                lstr_mess.arg_ok = true;
            }
            else
            {

                lstr_mess.a_datetime[0] = default(DateTime);
                lstr_mess.arg_ok = false;
                lstr_mess.a_boolean[0] = false;
            }

            WindowHelper.Close(this.ViewModel, lstr_mess);
        }
        public void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        public async Task<long> wf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_exiration_date.wf_show_message()
            //
            // Returns:         Long
            //
            // Parameters:      value String as_column
            // 
            // Copyright  - Stas
            //
            // Date Created: 01/08/2005
            //
            // Description:	
            // 
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            return await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox(as_title, as_error_text, "", as_buttons, 1, as_message);
        }
        public async Task open() //--------------------------------------------------------------------
        {


            //base  w_mini_terminal_msg

            TextBoxElement mle_error = this.GetVisualElementById<TextBoxElement>("mle_error");
            //Event:			public w_mini_terminal_msg.open()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 									ancestor of all message objects
            //							gets Message.PowerObjectParm from caller window
            //					a)	istr_arg.a_string[1] ---> window title
            //					b) istr_arg.a_string[2] ---> error text
            //					c) istr_arg.a_string[3] ---> message icon 
            //					d) istr_arg.a_string[4] ---> buttons to be displayed
            //					e) istr_arg.a_string[5] ---> sets default button focus
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_buttons_on_window = 0;

            if (this.ViewModel.istr_arg == null)
            {
                return;
            }

            if (masofonAlias.masofon.Instance.nvo_translator != null)
            {
                //.. multi_lingual_is_active = yes + current system language not Hebrew
                if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
                {
                    await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel);
                }
            }
            //.. window title
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 1)
            {
                this.ViewModel.is_window_titleProperty = this.ViewModel.istr_arg.a_string[1];
                this.ViewModel.is_window_titleProperty = await this.ViewModel.wf_translate_message(this.ViewModel.is_window_titleProperty);
                if (!isempty_stringClass.isempty_string(this.ViewModel.is_window_titleProperty))
                {
                    this.ViewModel.Text = this.ViewModel.is_window_titleProperty;
                }
            }
            //.. error text
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 2)
            {
                mle_error.Text = await this.ViewModel.wf_translate_message(this.ViewModel.istr_arg.a_string[2]);
                if (masofonAlias.masofon.Instance.nvo_translator != null)
                {
                    if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_string(mle_error.Text))
                    {

                        mle_error.TextAlign = HorizontalAlignment.Right;
                    }
                    else
                    {

                        mle_error.TextAlign = HorizontalAlignment.Left;
                    }
                }
                mle_error.Text = this.ViewModel.istr_arg.a_string[2];
            }
            //.. message icon 
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 3)
            {
                wf_show_picture(this.ViewModel.istr_arg.a_string[3]);
            }
            //.. buttons to be displayed
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 4)
            {
                await wf_show_buttons(this.ViewModel.istr_arg.a_string[4]);
            }
            //.. sets default button focus
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 5)
            {
                wf_set_default_button(this.ViewModel.istr_arg.a_string[5]);
            }
            //GalilCS - No need because exiration date always without mle_error
            //if (isempty_stringClass.isempty_string(mle_error.Text))
            //{
            //    wf_without_error_title();
            //}
            TextBoxElement mle_message = this.GetVisualElementById<TextBoxElement>("mle_message");
            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 6)
            {
                mle_message.Text = await this.ViewModel.wf_translate_message(Convert.ToString(this.ViewModel.istr_arg.a_string[6]));
              
            }
           


            //expiration_date
            MaskedTextBoxElement em_expiration_date = this.GetVisualElementById<MaskedTextBoxElement>("em_expiration_date");
            TextBoxElement txt_expiration_date = this.GetVisualElementById<TextBoxElement>("txt_expiration_date");
            //Function:			public w_mini_terminal_exiration_date.Open()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					at the parent of parent (w_mini_terminal_msg) open event , gets values :
            // 						a)	istr_arg.a_string[1] ---> window title
            //						b) istr_arg.a_string[2] ---> error text
            //						c) istr_arg.a_string[3] ---> message icon 
            //						d) istr_arg.a_string[4] ---> buttons to be displayed
            //						e) istr_arg.a_string[5] ---> sets default button focus
            //					at the parent (w_mini_terminal_message_box) open event , gets values :
            //						f)	istr_arg.a_string[6] ---> message text
            //					at this event :
            //						Gets Current Expiration date of a material and minimum 
            //						material expiration date
            //					   Pay attention:	material-expiration-date must be >= minimum-exiration-date
            //						g) istr_arg.a_datetime[1] ---> current exiration date of a material
            //						h) istr_arg.a_datetime[2] ---> minimum exiration date of a material
            //						
            //						if some message is like '#number' (for example '#125') then 
            //							message will retrieve from DB by its number
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_date = null;
            DateTime? ld_expiration_date = null;
            //.. current exiration date of a material
            if (CollectionExtensions.UBound(ViewModel.istr_arg.a_datetime) >= 1)
            {
                ld_expiration_date = ViewModel.istr_arg.a_datetime[0];
            }
            //.. minimum exiration date of a material
            if (CollectionExtensions.UBound(ViewModel.istr_arg.a_datetime) >= 2)
            {
                this.ViewModel.idt_min_expiration_dateProperty = ViewModel.istr_arg.a_datetime[1];
            }
            if (ld_expiration_date == null)
            {
                em_expiration_date.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
                em_expiration_date.Text = "00000000";
            }
            else
            {
                ls_date = ld_expiration_date.Value.ToString("dd/MM/yyyy");
                string s = ls_date.Replace('/', ' ');
                s = Regex.Replace(s, @"\s+", "");
                em_expiration_date.Text = s;
            }

            txt_expiration_date.Focus();

            //st_expiration.text = st_expiration.tag + " " + String(idt_min_expiration_date)
        }

        public async Task cb_cancel_clicked(object sender, EventArgs e)
        {
            await this.ue_close_with_return(false);
        }

        public async Task cb_ok_clicked(object sender, EventArgs e)
        {
            await this.ue_close_with_return(true);
        }
        public void st_more_constructor(object sender, EventArgs e)
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            st_more.BringToFront();
        }

        public void st_more_clicked(object sender, EventArgs e) //--------------------------------------------------------------------
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            //Event:			public w_mini_terminal_msg.clicked()
            //
            // Returns:      Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            //				 if More >> (st_more) clicked then move all window controls down,
            //					otherwise move up.
            //				 if a control not need to be moved , its tag value must be 'NoMove'
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (st_more.Text == "More >>")
            {
                st_more.Text = "Less <<";
                wf_show_error_title(true);
            }
            else
            {
                st_more.Text = "More >>";
                wf_show_error_title(false);
            }
        }
        public void cb_ok_constructor(object sender, EventArgs e)
        {
            u_cb_ok cb_ok = this.GetVisualElementById<u_cb_ok>("cb_ok");
            cb_ok.constructor();
            cb_ok.Focus();
        }


        public void ue_key(object sender, KeyEventArgs e) //--------------------------------------------------------------------
        {
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            //object keyflags = null;
            //Event:			public w_mini_terminal_msg.ue_key()
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            //					if enter clicked --> activate cb_ok click 
            //					if escape clicked --> activate cb_cancel click
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            //if (Convert.ToInt32(keyflags) == 0)
            //{
            Keys key = e.KeyCode;
            if (key == Keys.Enter || key == Keys.Tab)
            {
                cb_ok.PerformClick(e);
            }

            else if (key == Keys.Escape)
            {
                cb_cancel.PerformClick(e);
            }
            //}
        }
        protected void wf_get_window_position() //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_msg.wf_get_window_position()
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					set start window position
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_xpos = 0;
            long ll_ypos = 0;
            string ls_window_height = null;
            string ls_window_width = null;
            string ls_window_xpos = null;
            string ls_window_ypos = null;
            string ls_lang = null;
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
            {
                ls_lang = "alignment_left";
            }
            else
            {
                ls_lang = "alignment_right";
            }
            if (Convert.ToInt64(ls_window_xpos) > 0)
            {
                ll_xpos = Convert.ToInt64(ls_window_xpos);
            }
            else
            {
                ll_xpos = 65;
            }
            if (Convert.ToInt64(ls_window_ypos) > 0)
            {
                ll_ypos = Convert.ToInt64(ls_window_ypos);
            }
            else
            {
                ll_ypos = 300;
            }
            if (Convert.ToInt64(ls_window_width) > 0)
            {
                this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
            }
            if (Convert.ToInt64(ls_window_height) > 0)
            {
                this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
            }
            //ll_ypos += (il_max_window_height - this.height) /2
            //ll_xpos += (il_max_window_width - this.width) /2
            WindowElement root = this.GetRootVisualElement() as WindowElement;
            root.Location = new Point((int)ll_xpos, (int)ll_ypos);
            this.ViewModel.Visible = true;
        }
        protected void wf_show_error_title(bool ab_flag) //--------------------------------------------------------------------
        {
            TextBoxElement mle_error = this.GetVisualElementById<TextBoxElement>("mle_error");
            //Function:			public w_mini_terminal_msg.wf_show_error_title()
            //
            // Returns:         None
            //
            // Parameters:      value Boolean ab_flag --> If true move down , otherwise up
            // 
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					if More >> (st_more) clicked then move all window controls down,
            //					otherwise move up.
            //					if a control not need to be moved , its tag value must be 'NoMove'
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll = 0;
            long ll_y_move = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            TextBoxElement txt_expiration_date = default(TextBoxElement);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            mle_error.Visible = ab_flag;
            if (ab_flag)
            {
                ll_y_move = (mle_error.PixelHeight - 10);
            }
            else
            {
                ll_y_move = -1 * (mle_error.PixelHeight - 10);
            }
            for (ll = 1; ll <= CollectionExtensions.UBound(this.ViewModel.Controls); ll++)
            {
                if (!(this.ViewModel.Controls[(int)ll] != null))
                {
                    continue;
                }
                if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    sle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    if (Convert.ToString(sle.Tag) != "NoMove")
                    {
                        sle.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LabelElement))
                {
                    st = this.ViewModel.Controls[(int)ll] as LabelElement;
                    if (Convert.ToString(st.Tag) != "NoMove")
                    {
                        st.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    cb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    if (Convert.ToString(cb.Tag) != "NoMove")
                    {
                        cb.PixelTop += (int)ll_y_move;
                    }
                }

                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CompositeElement))
                {
                    uo = this.ViewModel.Controls[(int)ll] as CompositeElement;
                    if (Convert.ToString(uo.Tag) != "NoMove")
                    {
                        uo.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(GridElement))
                {
                    dw = this.ViewModel.Controls[(int)ll] as GridElement;
                    if (Convert.ToString(dw.Tag) != "NoMove")
                    {
                        dw.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    pb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    if (Convert.ToString(pb.Tag) != "NoMove")
                    {
                        pb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CheckBoxElement))
                {
                    cbx = this.ViewModel.Controls[(int)ll] as CheckBoxElement;
                    if (Convert.ToString(cbx.Tag) != "NoMove")
                    {
                        cbx.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RadioButtonElement))
                {
                    rb = this.ViewModel.Controls[(int)ll] as RadioButtonElement;
                    if (Convert.ToString(rb.Tag) != "NoMove")
                    {
                        rb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TabElement))
                {
                    t = this.ViewModel.Controls[(int)ll] as TabElement;
                    if (Convert.ToString(t.Tag) != "NoMove")
                    {
                        t.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RichTextBox))
                {
                    gb = this.ViewModel.Controls[(int)ll] as GroupBoxElement;
                    if (Convert.ToString(gb.Tag) != "NoMove")
                    {
                        gb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ListBoxElement))
                {
                    lb = this.ViewModel.Controls[(int)ll] as ListBoxElement;
                    if (Convert.ToString(lb.Tag) != "NoMove")
                    {
                        lb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LinkLabelElement))
                {
                    shl = this.ViewModel.Controls[(int)ll] as LinkLabelElement;
                    if (Convert.ToString(shl.Tag) != "NoMove")
                    {
                        shl.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = this.ViewModel.Controls[(int)ll] as ComboBoxElement;
                    if (Convert.ToString(ddlb.Tag) != "NoMove")
                    {
                        ddlb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    mle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    if (Convert.ToString(mle.Tag) != "NoMove")
                    {
                        mle.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    txt_expiration_date = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    if (Convert.ToString(txt_expiration_date.Tag) != "NoMove")
                    {
                        txt_expiration_date.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = this.ViewModel.Controls[(int)ll] as MaskedTextBoxElement;
                    if (Convert.ToString(em.Tag) != "NoMove")
                    {
                        em.PixelTop += (int)ll_y_move;
                    }
                }
            }
            this.ViewModel.PixelHeight += (int)ll_y_move;
        }

        protected void wf_set_default_button(string as_default_button)
        {
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            long ll_default_button = 0;
            ll_default_button = Convert.ToInt64(as_default_button);
            if (ll_default_button > 2 || ll_default_button <= 0)
            {
                ll_default_button = 1;
            }
            if (cb_ok.Visible && cb_cancel.Visible)
            {
                if (ll_default_button == 1)
                {
                    cb_ok.Focus();
                }
                else
                {
                    cb_cancel.Focus();
                }
            }
            else if (cb_ok.Visible)
            {
                cb_ok.Focus();
            }
            else if (cb_cancel.Visible)
            {
                cb_cancel.Focus();
            }
        }
        protected async Task wf_show_buttons(string as_buttons)
        {
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            switch (as_buttons.ToLower(CultureInfo.InvariantCulture))
            {
                case "ok":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = false;
                    cb_ok.PixelLeft = (this.ViewModel.PixelWidth - cb_ok.PixelWidth) / 2;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אישור");
                    }
                    else
                    {
                        cb_ok.Text = "Ok";
                    }
                    break;
                case "cancel":
                    cb_ok.Visible = false;
                    cb_cancel.Visible = true;
                    cb_cancel.PixelLeft = 85;
                    cb_cancel.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
                    }
                    else
                    {
                        cb_cancel.Text = "Cancel";
                    }
                    break;
                case "okcancel":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = true;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    cb_cancel.Tag = -1;//.uf_set_return_value(-1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אישור");
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
                    }
                    else
                    {
                        cb_ok.Text = "Ok";
                        cb_cancel.Text = "Cancel";
                    }
                    break;
                case "yes":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = false;
                    cb_ok.PixelLeft = (this.ViewModel.PixelWidth - cb_ok.PixelWidth) / 2;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("כן");
                    }
                    else
                    {
                        cb_ok.Text = "Yes";
                    }
                    break;
                case "no":
                    cb_ok.Visible = false;
                    cb_cancel.Visible = true;
                    cb_cancel.PixelLeft = 85;
                    cb_cancel.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא");
                    }
                    else
                    {
                        cb_cancel.Text = "No";
                    }
                    break;
                case "yesno":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = true;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    cb_cancel.Tag = -1;//.uf_set_return_value(-1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("כן");
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא");
                    }
                    else
                    {
                        cb_ok.Text = "Yes";
                        cb_cancel.Text = "No";
                    }
                    break;
            }
        }
        public void wf_show_picture(string as_picture_name)
        {
            ImageElement p_icon = this.GetVisualElementById<ImageElement>("p_icon");
            if (isempty_stringClass.isempty_string(as_picture_name))
            {
                return;
            }
            switch (as_picture_name.ToLower(CultureInfo.InvariantCulture))
            {
                case "information!":
                case "stopsign!":
                case "exclamation!":
                case "question!":
                    p_icon.Image = new ResourceReference(as_picture_name);
                    p_icon.Visible = true;
                    break;
                default:
                    if (!System.IO.File.Exists(as_picture_name))
                    {
                        return;
                    }
                    p_icon.Image = new ResourceReference(as_picture_name);
                    p_icon.Visible = true;
                    break;
            }
        }
        public void wf_without_error_title() //--------------------------------------------------------------------
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            //Function:			public w_mini_terminal_msg.wf_without_error_title()
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll = 0;
            long ll_y_move = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            ll_y_move = -1 * st_more.PixelHeight;
            for (ll = 1; ll <= CollectionExtensions.UBound(this.ViewModel.Controls); ll++)
            {
                if (!(this.ViewModel.Controls[(int)ll] != null))
                {
                    continue;
                }
                if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    sle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    sle.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LabelElement))
                {
                    st = this.ViewModel.Controls[(int)ll] as LabelElement;
                    st.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    cb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    cb.PixelTop += (int)ll_y_move;
                }

                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CompositeElement))
                {
                    uo = this.ViewModel.Controls[(int)ll] as CompositeElement;
                    uo.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(GridElement))
                {
                    dw = this.ViewModel.Controls[(int)ll] as GridElement;
                    dw.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    pb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    pb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CheckBoxElement))
                {
                    cbx = this.ViewModel.Controls[(int)ll] as CheckBoxElement;
                    cbx.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RadioButtonElement))
                {
                    rb = this.ViewModel.Controls[(int)ll] as RadioButtonElement;
                    rb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TabElement))
                {
                    t = this.ViewModel.Controls[(int)ll] as TabElement;
                    t.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RichTextBox))
                {
                    gb = this.ViewModel.Controls[(int)ll] as GroupBoxElement;
                    gb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ListBoxElement))
                {
                    lb = this.ViewModel.Controls[(int)ll] as ListBoxElement;
                    lb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LinkLabelElement))
                {
                    shl = this.ViewModel.Controls[(int)ll] as LinkLabelElement;
                    shl.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = this.ViewModel.Controls[(int)ll] as ComboBoxElement;
                    ddlb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    mle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    mle.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = this.ViewModel.Controls[(int)ll] as MaskedTextBoxElement;
                    em.PixelTop += (int)ll_y_move;
                }
            }
            st_more.Visible = false;
            this.ViewModel.PixelHeight += (int)ll_y_move;
        }


        public void txt_expiration_date_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Keys key = ((KeyEventArgs)e).KeyCode;
                if (key == Keys.Enter || key == Keys.Tab)
                {
                    ue_key(sender, e);
                }
            }
            catch { }
        }
        public void txt_expiration_date_TextChanged(object sender, EventArgs e)
        {
            try
            {
                MaskedTextBoxElement em_expiration_date = this.GetVisualElementById<MaskedTextBoxElement>("em_expiration_date");
                TextBoxElement txt_expiration_date = this.GetVisualElementById<TextBoxElement>("txt_expiration_date");
                em_expiration_date.Text = txt_expiration_date.Text;
            }
            catch { }
        }

    }
}
