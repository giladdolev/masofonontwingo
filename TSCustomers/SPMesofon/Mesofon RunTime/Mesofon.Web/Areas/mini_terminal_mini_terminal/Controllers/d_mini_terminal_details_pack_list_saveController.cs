using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_details_pack_list_saveController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_details_pack_list_save()
		{
			d_mini_terminal_details_pack_list_saveRepository repository = new d_mini_terminal_details_pack_list_saveRepository();
			//repository.Retrieve();
			return this.View(repository);
		}
	}
}
