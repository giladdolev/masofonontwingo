using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_details_invoice_packingController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_details_invoice_packing()
		{
			d_mini_terminal_details_invoice_packingRepository repository = new d_mini_terminal_details_invoice_packingRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
