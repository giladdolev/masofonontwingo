using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_return_from_branch_print_rowsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_return_from_branch_print_rows()
		{
			d_return_from_branch_print_rowsRepository repository = new d_return_from_branch_print_rowsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
