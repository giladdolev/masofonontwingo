using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mini_terminal_input_headController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mini_terminal_input_head()
		{
			d_sql_mini_terminal_input_headRepository repository = new d_sql_mini_terminal_input_headRepository();
		
			return this.View(repository);
		}
	}
}
