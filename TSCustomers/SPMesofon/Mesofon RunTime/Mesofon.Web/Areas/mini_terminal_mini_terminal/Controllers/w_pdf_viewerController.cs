﻿using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using global;
using System.Web.VisualTree.Elements;
using System.Globalization;
using System.IO;
using masofon;
using System.Drawing;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using System.Web.VisualTree.Extensions;
using System.Extensions;
using Mesofon.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class w_pdf_viewerController : MvcSite.Common.SPBaseController
    {


        public ActionResult w_pdf_viewer(string pdf)
        {



            return this.View(new w_pdf_viewer(pdf));
        }
        private w_pdf_viewer ViewModel
        {
            get { return this.GetRootVisualElement() as w_pdf_viewer; }
        }

        public void Form_Load(object sender, EventArgs e)
        {
            PanelElement pdfPanel = this.GetVisualElementById<PanelElement>("Panel1");
            pdfPanel.Content = getHTML();
        }

        private string getHTML()
        { 
            string template = "<img id='pdfloading' src='\\widget\\images\\loading.gif' /><object onload='hideimage()' data='/PDFDownloadURL?location=" + this.ViewModel.pdf + "' width='100%' height='100%'>" +
                              "</object>";

            return template;
        }


        public void btnClose_Click(object sender, EventArgs e)
        {
            this.ViewModel.Close();
        }




    }
}
