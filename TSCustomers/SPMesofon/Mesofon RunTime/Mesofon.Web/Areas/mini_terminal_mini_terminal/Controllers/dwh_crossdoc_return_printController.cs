using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_crossdoc_return_printController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_crossdoc_return_print()
		{
			dwh_crossdoc_return_printRepository repository = new dwh_crossdoc_return_printRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
