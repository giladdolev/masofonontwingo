using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_branches_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_branches_list()
		{
			d_branches_listRepository repository = new d_branches_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
