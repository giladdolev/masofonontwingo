using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mt_input_pack_headController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mt_input_pack_head()
		{
			d_sql_mt_input_pack_headRepository repository = new d_sql_mt_input_pack_headRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
