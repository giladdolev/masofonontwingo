using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dddw_mini_terminal_crossdoc_returns_shipmentsController : MvcSite.Common.SPBaseController
	{
		public ActionResult dddw_mini_terminal_crossdoc_returns_shipments()
		{
			dddw_mini_terminal_crossdoc_returns_shipmentsRepository repository = new dddw_mini_terminal_crossdoc_returns_shipmentsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
