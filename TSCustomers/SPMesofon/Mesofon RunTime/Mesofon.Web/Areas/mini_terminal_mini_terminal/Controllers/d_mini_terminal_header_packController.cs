using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_header_packController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_header_pack()
		{
			d_mini_terminal_header_packRepository repository = new d_mini_terminal_header_packRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
