using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_invoices_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_invoices_details()
		{
			d_invoices_detailsRepository repository = new d_invoices_detailsRepository();
			
			return this.View(repository);
		}
	}
}
