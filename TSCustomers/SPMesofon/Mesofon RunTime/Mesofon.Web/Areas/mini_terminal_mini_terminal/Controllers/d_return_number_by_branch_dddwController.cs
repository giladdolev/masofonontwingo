using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_return_number_by_branch_dddwController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_return_number_by_branch_dddw()
		{
			d_return_number_by_branch_dddwRepository repository = new d_return_number_by_branch_dddwRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
