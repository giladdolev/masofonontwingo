using System;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.VisualTree.MVC;
using global;
using mini_terminal;
using masofonAlias = masofon;
using Mesofon.Common;
using Mesofon.Global.Controllers;

namespace Mesofon.MiniTerminal.Controllers
{
	public class w_mini_terminal_id_checkController : w_password_idController
	{

        public ActionResult w_mini_terminal_id_check(s_password password)
		{
			return this.View(new w_mini_terminal_id_check(password));
		}
		private w_mini_terminal_id_check ViewModel
		{
			get { return this.GetRootVisualElement() as w_mini_terminal_id_check; }
		}
        //need to subscribe in aspx (missing) ---avi 28/6
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }

        public void open()
        {
            this.ViewModel.Text += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(" למסופון ").Result;
         
        }
    }
}
