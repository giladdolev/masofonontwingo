using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_branches_list_exController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_branches_list_ex()
		{
			d_branches_list_exRepository repository = new d_branches_list_exRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
