using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_header_invController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_header_inv()
		{
			d_mini_terminal_header_invRepository repository = new d_mini_terminal_header_invRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
