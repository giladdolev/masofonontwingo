using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_shipment_return_print2Controller : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_shipment_return_print2()
		{
			dwh_shipment_return_print2Repository repository = new dwh_shipment_return_print2Repository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
