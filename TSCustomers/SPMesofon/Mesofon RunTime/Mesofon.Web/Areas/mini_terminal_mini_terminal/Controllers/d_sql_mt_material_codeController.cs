using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mt_material_codeController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mt_material_code()
		{
			d_sql_mt_material_codeRepository repository = new d_sql_mt_material_codeRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
