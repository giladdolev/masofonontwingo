using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_diff_pallet_reportController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_diff_pallet_report()
		{
			d_diff_pallet_reportRepository repository = new d_diff_pallet_reportRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
