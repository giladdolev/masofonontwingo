using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_summary_returnsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_summary_returns()
		{
			d_summary_returnsRepository repository = new d_summary_returnsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
