using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_return_marlog_print_headerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_return_marlog_print_header()
		{
			d_return_marlog_print_headerRepository repository = new d_return_marlog_print_headerRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
