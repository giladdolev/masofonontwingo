using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_return_from_branchController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_return_from_branch()
		{
			dwh_return_from_branchRepository repository = new dwh_return_from_branchRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
