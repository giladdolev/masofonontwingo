using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_return_details_diffController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_return_details_diff()
		{
			d_mini_terminal_return_details_diffRepository repository = new d_mini_terminal_return_details_diffRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
