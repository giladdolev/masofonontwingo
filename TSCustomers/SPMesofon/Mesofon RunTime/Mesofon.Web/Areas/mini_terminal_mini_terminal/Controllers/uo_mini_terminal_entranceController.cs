using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Threading.Tasks;
using masofonAlias = masofon;
using mini_terminal;
using MvcSite.Common;
using System.Web.Security;

namespace Mesofon.MiniTerminal.Controllers
{
	public class uo_mini_terminal_entranceController : MvcSite.Common.SPBaseController
	{

		public ActionResult uo_mini_terminal_entrance()
		{
			return this.View(new uo_mini_terminal_entrance());
		}
		private uo_mini_terminal_entrance ViewModel
		{
			get { return this.GetRootVisualElement() as uo_mini_terminal_entrance; }
		}

	    public async Task Form_Load(object sender, EventArgs e)
	    {
	       await this.ViewModel.constructor();
	    }
        public async Task uf_display(string as_name) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138 - open appropriate user object
		{
			int li_ret = 0;
			 
			 
			li_ret = await (this.ViewModel.Parent.Parent as w_mini_terminal).wf_display(as_name);
		}
        public async Task uf_close_window() // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138 - close window
        {
            await masofonAlias.masofon.Instance.shut_down();
        }



        public async Task cb_recieve_clicked(object sender, EventArgs e) //RonY@02/02/2015 -  SPUCM00005163
		{
		    await uf_display("TRANS_FROM_BRANCH");
		}
		
		public async Task cb_transfer_clicked(object sender, EventArgs e) //RonY@02/02/2015 -  SPUCM00005163
		{
		    await uf_display("TRANS_TO_BRANCH");
		}
		
		public async Task cb_corssdoc_return_clicked(object sender, EventArgs e) //RonY@15/01/2015 -  SPUCM00005162
		{
		    await uf_display("CROSSDOC_RETURN");
		}
		
		public async Task cb_return_marlog_clicked(object sender, EventArgs e) // AlexKh - 1.1.1.10 - 2013-06-26 - CR#1160 - open header uo
		{
		    await uf_display("RETURN_MARLOG");
		}
		
		public async Task cb_return_supplier_clicked(object sender, EventArgs e) // AlexKh - 1.1.1.10 - 2013-06-26 - CR#1160 - open header uo
		{
		    await uf_display("RETURN_SUPPLIER");
		}
		
		public async Task cb_sahar_clicked(object sender, EventArgs e) // AlexKh - 1.1.1.6 - 2012-02-16 - CR#1169 - open entrance uo
		{
		    await uf_display("SAHAR_RESET");
		}
		
		public async Task cb_crossdoc_clicked(object sender, EventArgs e) // AlexKh - 1.2.48.15 - 2012-02-16 - CR#1152 - open entrance uo
		{
		    await uf_display("CROSSDOC_RESET");
		}
        
        public async Task cb_finish_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish the masofon input.
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-05-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            await uf_close_window();


        }
        
        public async Task cb_marlog_supply_clicked(object sender, EventArgs e) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138 - open entrance uo
		{
			await uf_display("SHIPMENT_RESET");
		}
		
		public async Task cb_supplier_supply_clicked(object sender, EventArgs e) // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138 - open header uo
		{
		    await uf_display("HEADER");
		}
	}
}
