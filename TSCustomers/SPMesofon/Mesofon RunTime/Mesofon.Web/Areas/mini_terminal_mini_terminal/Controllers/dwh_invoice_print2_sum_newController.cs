using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_invoice_print2_sum_newController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_invoice_print2_sum_new()
		{
			dwh_invoice_print2_sum_newRepository repository = new dwh_invoice_print2_sum_newRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
