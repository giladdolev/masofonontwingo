using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_debit_marlog_for_b2b_xmlController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_debit_marlog_for_b2b_xml()
		{
			d_debit_marlog_for_b2b_xmlRepository repository = new d_debit_marlog_for_b2b_xmlRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
