﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Globalization;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.IO;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Models;
using Mesofon.Repository;
using System.Web.VisualTree.Utilities;
using System.Extensions.Printing;

namespace Mesofon.MiniTerminal.Controllers
{
    public class uo_mini_terminal_headerController : Controller
    {

        public ActionResult uo_mini_terminal_header()
        {
            return this.View(new uo_mini_terminal_header());
        }
        private uo_mini_terminal_header ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_header; }
        }

        public async Task Load_Form(object sender, EventArgs e)
        {
            WidgetGridElement uo_header_dw_body = this.ViewModel.GetVisualElementById<WidgetGridElement>("dw_body");
            await this.ViewModel.constructor();
            uo_header_dw_body.SetRepository(new d_mini_terminal_header_inv_packRepository(), false, true);
        }


        private async Task header_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((KeyEventArgs)e).KeyCode == Keys.Enter || ((KeyEventArgs)e).KeyCode == Keys.Tab)
            {
                if(Convert.ToString(((ComboGridElement)sender).Tag) == "firing" && ((KeyEventArgs)e).KeyCode == Keys.Enter)
                {
                    ((ComboGridElement)sender).Tag = "";
                    return;
                }
                
                ((ComboGridElement)sender).Tag = "firing";

                await dw_header_itemchanged(sender, e);
                
            }
        }

        private async Task header_KeyDown(object sender, KeyDownEventArgs e)
        {
            if (((KeyEventArgs)e).KeyCode == Keys.Enter || ((KeyEventArgs)e).KeyCode == Keys.Tab)
            {
                await dw_header_itemchanged(sender, e);
            }
        }

        private async Task ClickAction(object sender, EventArgs e)
        {
            return;
        }

        public async Task<bool> ue_validation_check() //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			ue_validation_check
            //*Purpose:							Perform validation checks
            //*  
            //*Arguments:						None
            //*Return:								Boolean: TRUE - ok 
            //*													FALSE - event otherwise
            //*Date				Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*21-08-2007		SHARP.			0.2.0 			B2B 			Initiation
            //************************************************************************************************

            long? ll_doc_number = null;
            long? ll_supplier_number = null;
            long ll_order_number = 0;
            long ll_error_number = 0;
            long ll_header_row_count = 0;
            long ll_body_row_count = 0;
            long ll_cntr = 0;
            long ll_new_log_book = 0;
            long ll_orig_log_book = 0;
            DateTime? ldt_supply_date = default(DateTime);
            decimal? ldec_total = default(decimal);
            string ls_supplier_name = null;
            string lc_doc_type = "";
            ModelAction ldwis_doc_type = default(ModelAction);
            ModelAction ldwis_number = default(ModelAction);
            ModelAction ldwis_supply_date = default(ModelAction);
            ModelAction ldwis_total = default(ModelAction);
            ModelAction ldwis_log_book = default(ModelAction);

            ll_header_row_count = order_number.RowCount();

            ll_body_row_count = dw_body.RowCount();
            //if (ll_header_row_count == 0 && ll_body_row_count == 0)
            //{
            //    return false;
            //}

            //dw_header.PerformValidated(null);

            dw_body.PerformValidated(null);

            //ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
            if (!string.IsNullOrEmpty(supplier_number.Text))
                ll_supplier_number = Convert.ToInt64(supplier_number.Text);

            if (!(await uf_is_supplier_exist("NUMBER", ll_supplier_number, ls_supplier_name)).Retrieve(out ll_supplier_number, out ls_supplier_name))
            {
                this.ViewModel.uf_set_focus("dw_header", 1, "supplier_number");
                return false;
            }
            for (ll_cntr = 0; ll_cntr < ll_body_row_count; ll_cntr++)
            {
                // Get the header row status


                ldwis_doc_type = dw_body.GetItemStatus((int)ll_cntr, "doc_type", ModelBuffer.Primary);


                ldwis_number = dw_body.GetItemStatus((int)ll_cntr, "number", ModelBuffer.Primary);


                ldwis_supply_date = dw_body.GetItemStatus((int)ll_cntr, "supply_date", ModelBuffer.Primary);


                ldwis_total = dw_body.GetItemStatus((int)ll_cntr, "invoice_total", ModelBuffer.Primary);


                ldwis_log_book = dw_body.GetItemStatus((int)ll_cntr, "log_book", ModelBuffer.Primary);





                if (ldwis_doc_type == ModelAction.None && (ldwis_number == ModelAction.None && (ldwis_supply_date == ModelAction.None && (ldwis_total == ModelAction.None && ldwis_log_book == ModelAction.None))))
                {
                    continue;
                }
                ll_doc_number = dw_body.GetItemValue<Int64?>(ll_cntr, "number");
                lc_doc_type = dw_body.GetItemValue<string>(ll_cntr, "doc_type");
                if (ll_doc_number == 0 || ll_doc_number == null)
                {
                    if (lc_doc_type.ToString() == "I")
                    {
                        //Invoice
                        await this.ViewModel.uf_show_message("", "", "OK", "#20014");
                    }
                    else
                    {
                        //Packing list
                        await this.ViewModel.uf_show_message("", "", "OK", "#20013");
                    }
                    this.ViewModel.uf_set_focus("dw_body", ll_cntr, "number");
                    return false;
                }

                ldt_supply_date = dw_body.GetItemValue<DateTime>((int)ll_cntr, "supply_date");

                var isDate = SystemFunctionsExtensions.IsDate(ldt_supply_date.Value.ToString("yyyy-MM-dd"));
                if (ll_supplier_number == 0 || isDate != null && !isDate.Value)
                {
                    await this.ViewModel.uf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תאריך אספקה לא יכול להיות ריק"));
                    this.ViewModel.uf_set_focus("dw_body", ll_cntr, "supply_date");
                    return false;
                }
                //Can not  continue if same log number already exists in DB
                // AlexKh - 1.1.26.0 - 2015-01-11 - SPUCM00005045 - Check if we need the log_book field
                if (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Administration", "log_book_in_purchase_header", "yes", "Show the log_book field in the header") == "yes")
                {


                    ll_new_log_book = dw_body.GetItemValue<long>(ll_cntr, "log_book");
                    if (!await this.dw_body_ue_log_book_exists(ll_cntr, ll_new_log_book))
                    {
                        this.ViewModel.uf_set_focus("dw_body", ll_cntr, "log_book");
                        return false;
                    }
                }
                //Check total value only if it invoice input ...

                ldec_total = dw_body.GetItemValue<decimal>(ll_cntr, "expected_total_amount");
                if (ldec_total == 0 || ldec_total == null)
                {
                    if (lc_doc_type.ToString() == "P")
                    {

                        dw_body.SetItem(ll_cntr, "total", "0");
                    }
                    else
                    {
                        await this.ViewModel.uf_show_message("", "", "OK", masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("סה\"כ ח-ן לא יכול להיות ריק").Result);
                        this.ViewModel.uf_set_focus("dw_body", ll_cntr, "invoice_total");
                        return false;
                    }
                }
            }
            if (ll_supplier_number == 0)
            {
                await this.ViewModel.uf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("מס  ספק לא יכול להיות ריק"));
                this.ViewModel.uf_set_focus("dw_header", 1, "supplier_number");
                return false;
            }
            //Check if order number exists

            //ll_order_number = dw_header.GetItemValue<long>(0, "order_number");
            ll_order_number = Convert.ToInt64(order_number.Text == "" ? "0" : order_number.Text);
            if (!(ll_order_number == 0))
            {
                if (!(await this.ViewModel.iuo_order_searchProperty.uf_order_for_purchase_exists(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_order_number, ll_error_number)).Retrieve(out ll_error_number))
                {
                    //this.ViewModel.uf_set_focus("dw_header", 1, "order_number");
                    return false;
                }
                // AlexKh - 1.2.43.1 - 2009-03-30 - SPUCM00001084 - disable button if order number is 0 or null
            }
            else
            {
                await this.ViewModel.uf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("מס הזמנה לא יכול להיות ריק"));
                //this.ViewModel.uf_set_focus("dw_header", 1, "order_number");
                return false;
            }
            return true;
        }
        public async Task<int> uf_close_window() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_close_window
            //*Purpose:							Close the window.
            //*  
            //*Arguments:						None.
            //*Return:								Integer: 1 - The window stays open.
            //*												   -1 - Error.
            //*
            //*Date 			   	Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007			SHARP.				0.2.0  		B2B 			Initiation
            //************************************************************************************************



            return Convert.ToInt32(await ViewModel.getMiniTerminal().wf_close_window());
        }
        public async Task<FuncResults<long?, string, bool>> uf_is_supplier_exist(string as_type, long? al_supplier_number, string al_supplier_name) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_is_supplier_exists
            //*Purpose:							Get the supplier number or supplier name (according to the as_type) 
            //*										check if exist and return by reference 
            //*										the supplier name or supplier no. respectivly.
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value 		String 				as_type: Number- for supplier number Name -for supplier name
            //*										Value 		Long	 				al_supplier_number: supplier number
            //*										Value 		String 				al_supplier_name: supplier name
            //*
            //*Return:								Boolean: TRUE - Success,
            //*													FALSE - Otherwise.
            //*
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.			0.2.0 			B2B 			Initiation
            //************************************************************************************************

            long ll_RowCount = 0;
            long? ll_found = 0;
            string ls_find = null;
            //Get the no of rows of the suppliers list

            ll_RowCount = this.ViewModel.D_mt_dddw_suppliers_listRepository.RowCount();
            if (ll_RowCount < 1)
            {
                await this.ViewModel.uf_show_message("Error", "", "OK", "Error, No suppliers list in the DB");
                return FuncResults.Return(al_supplier_number, al_supplier_name, false);
            }
            //According to the type check if the number or name is empty - if so return false and display an error message 
            if (as_type.ToUpper() == "NUMBER")
            {
                if (al_supplier_number == null)
                {
                    await ViewModel.getMiniTerminal().wf_show_message("", "", "Ok", masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ספק").Result + " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אינו קיים").Result.ToString());
                    return FuncResults.Return(al_supplier_number, al_supplier_name, false);
                }
                ls_find = "number == " + al_supplier_number.ToString();
                ll_found = this.ViewModel.D_mt_dddw_suppliers_listRepository.Find(ls_find, 0, (int)ll_RowCount);
                //as_type =name
            }
            else
            {
                if (isempty_stringClass.isempty_string(al_supplier_name))
                {
                    await ViewModel.getMiniTerminal().wf_show_message("", "", "Ok", masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ספק").Result + " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אינו קיים").Result);
                    return FuncResults.Return(al_supplier_number, al_supplier_name, false);
                }
                //ls_find = "name == \"" + al_supplier_name + "\"";
                ll_found = this.ViewModel.D_mt_dddw_suppliers_listRepository.Find("name", al_supplier_name, 0, (int)ll_RowCount);
            }



            switch (ll_found)
            {
                case -1:
                    // Supplier not found
                    await this.ViewModel.uf_show_message("Informational", "", "OK", "The supplier was not found");
                    return FuncResults.Return(al_supplier_number, al_supplier_name, false);
                case -2:
                case -5:
                    // Error
                    await this.ViewModel.uf_show_message("Error", "", "OK", "Error while retrieving the supplier.");
                    return FuncResults.Return(al_supplier_number, al_supplier_name, false);
                default:
                    if (as_type.ToUpper() == "NUMBER")
                    {
                        al_supplier_name = this.ViewModel.D_mt_dddw_suppliers_listRepository.GetItemValue<string>((int)ll_found, "name");
                    }
                    else
                    {

                        //al_supplier_number = Convert.ToInt64(this.ViewModel.D_mt_dddw_suppliers_listRepository.GetItemValue<int>((int)ll_found, "number"));
                        d_mt_dddw_suppliers_list temp_suppliers_list = this.ViewModel.D_mt_dddw_suppliers_listRepository.GetItem<d_mt_dddw_suppliers_list>((int)ll_found);
                        al_supplier_number = temp_suppliers_list.number;
                    }
                    break;
            }
            return FuncResults.Return(al_supplier_number, al_supplier_name, true);
        }
        public async Task<int> uf_retrieve_supplier_order(long? al_supplier_number) //**********************************************************************************************
        {

            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_retrieve_supplier_order
            //*Purpose:							Retrieve the Drop Down DW of the orders.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long  					al_supplier_number
            //*		
            //*Return:								Integer: 1 - Success
            //*												  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_ret_code = 0;
            ComboGridElement ldwc_order;

            ldwc_order = this.ViewModel.GetVisualElementById<ComboGridElement>("order_number");


            if (li_ret_code < 0)
            {

                await this.ViewModel.uf_show_message("SetTransObject Error", "", "OK", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                return -1;
            }
            this.ViewModel.supplier_orderRepository = new d_sql_supplier_order_listRepository();
            li_ret_code = Convert.ToInt32(this.ViewModel.supplier_orderRepository.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number));
            ldwc_order.SetRepository(this.ViewModel.supplier_orderRepository);
            ldwc_order.SelectedText = "";
            //li_ret_code = (int)ldwc_order.Retrieve(al_supplier_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
            if (li_ret_code < 0)
            {
                await this.ViewModel.uf_show_message("Retrieve Error", "", "OK", "Falid to retrieve supplier's orders");
                return -1;
            }
            return 1;
        }
        public async Task<int> uf_display(string as_show_mode) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_display
            //*Purpose:							Display uo_mini_terminal_body.
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			String					as_show_mode
            //*		
            //*Return:								Integer: 1 - Success, -1 - Failed
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            long ll_row_count = 0;
            // Show the items screen.

            li_rtn_code = await ViewModel.getMiniTerminal().wf_display(as_show_mode);
            if (li_rtn_code < 1)
            {
                await this.ViewModel.uf_show_message("ERROR", "", "OK", "Error in displaying:" + as_show_mode + ".");
                return -1;
            }
            // SharonS - 1.2.42.1 - 2009-02-11 - SPUCM00000861


            li_rtn_code = await ViewModel.getMiniTerminal().wf_body_new_row();
            // Set the focus on the barcode column in the first row.
            // AlexKh - 1.2.42.4 - 2009-03-11 - Version Fix
            if (li_rtn_code > 0)
            {


                ll_row_count = await ViewModel.getMiniTerminal().wf_items_row_count();


                li_rtn_code = await ViewModel.getMiniTerminal().wf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", ll_row_count, "barcode");
            }
            // End
            if (li_rtn_code < 1)
            {
                await this.ViewModel.uf_show_message("ERROR", "", "OK", "Error in set focus on barcode column.");
                return -1;
            }
            return 1;
        }

        public async Task<int> uf_new_doc() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_new_doc
            //*Purpose:							
            //*  
            //*Arguments:						None.
            //*		
            //*Return:								Integer: 1- Success, -1-Failed
            //*
            //*
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007		SHARP.				0.2.0    		B2B 			Initiation
            //************************************************************************************************


            var res = await ViewModel.getMiniTerminal().wf_new_doc();
            return res;
        }

        public async Task<int> uf_delete_empty_rows() //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_delete_empty_rows
            //*Purpose:							Delete empty rows from dw_body.
            //*  
            //*Arguments:						None.
            //*		
            //*Return:								Integer: 1 - Success, -1 - Failed.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*04-09-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            long ll_row_count = 0;
            long? ll_doc_no = null;

            ll_row_count = dw_body.RowCount();
            if (ll_row_count < 1)
            {
                await this.ViewModel.uf_show_message("Error", "", "OK", "Can not scan items when no docs are entered");
                return -1;
            }



            ll_doc_no = dw_body.GetItemValue<Int64?>(ll_row_count - 1, "number");
            if (ll_doc_no < 1 || ll_doc_no == 0)
            {

                dw_body.Delete((int)ll_row_count - 1);
            }
            return 1;
        }
        public int uf_set_row_no() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_set_row_no
            //*Purpose:							Set row number.
            //*  
            //*Arguments:						None.
            //*		
            //*Return:								Boolean: TRUE - Success
            //*													FALSE - Failed 
            //*
            //* Date 		 	    Programer		   Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-09-2007			SHARP.			   0.2.0 			B2B 			Initiation
            //************************************************************************************************


            // TODO: Missing function  wf_set_row_no() - returns default value -1 for compilation
            // return ViewModel.getMiniTerminal().wf_set_row_no();
            return -1;
        }

        public async Task<bool> uf_update_doc(long al_row_no) //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_update_doc
            //*Purpose:							Call the function wf_update_doc in the window.
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value 		Long 					al_row_no 					
            //*
            //*Return:								Boolean: TRUE - Success
            //*												 	FALSE - Otherwise
            //*
            //*Date		     	Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-10-2007		SHARP.			     0.2.0 			B2B 			Initiation
            //************************************************************************************************

            bool lb_ret = false;


            lb_ret = await ViewModel.getMiniTerminal().wf_update_doc(al_row_no);
            if (lb_ret)
            {
                this.ViewModel.ic_old_doc_typeProperty = default(char);
                this.ViewModel.ic_new_doc_typeProperty = default(char);



                dw_body.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None);
            }
            return lb_ret;
        }
        public async Task<int> uf_set_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_set_order_number
            //*Purpose:							Call the function wf_set_order_number in the window.
            //*
            //*
            //*Arguments:						None.
            //*		
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed 
            //*
            //* Date 		 	    Programer		   Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //* 16-10-2007		SHARP.			   0.2.0 		 B2B 			Initiation
            //************************************************************************************************



            return await ViewModel.getMiniTerminal().wf_set_order_number(al_order_number);
        }
        public async Task<int> uf_set_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_set_order_number
            //*Purpose:							Call the function wf_set_supplier_number in the window.
            //*
            //*
            //*Arguments:						None.
            //*		
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed 
            //*
            //* Date 		 	    Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //* 16-10-2007		SHARP.				0.2.0 		 	B2B 			Initiation
            //************************************************************************************************



            return await ViewModel.getMiniTerminal().wf_set_supplier_number(al_supplier_number);
        }

        public async Task<bool> uf_check_invoice_in_shipment(int al_supplier_number) //********************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:				uo_mini_terminal_header
            //*Function Name:	uf_check_invoice_in_shipment
            //*Purpose: 			Check if at least one b2b invoice was sent by shipment
            //*Arguments: 		long - al_supplier_number
            //*Return:				TRUE/FALSE
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*06-12-2010		AlexKh			1.2.46.4	SPUCM00002425	Initial version
            //********************************************************************

            long ll_i = 0;
            long? ll_doc_number = null;
            long ll_shipment_number = 0;
            long ll_b2b_status = 0;
            long ll_count = 0;
            long ll_distributor_number = 0;
            GridElement lds_b2b_invoice_details;
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_shipment", "boolean", "false", "Supply Merchandise from marlog.", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }
            // AlexKh - 1.2.46.7 - 2011-01-27 - SPUCM00002425 - check distributor is marlog

            if (dw_body.RowCount() > 0)
            {

                ll_distributor_number = dw_body.GetItemValue<long>(0, "distributor_number");
            }
            if (ll_distributor_number == 0)
            {
                ll_distributor_number = 0;
            }
            // AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - check if marlog distributor
            //IF ll_distributor_number <> il_marlog_distributor THEN
            if (await f_is_marlog_distributorClass.f_is_marlog_distributor(0, ll_distributor_number) == 0)
            {
                return true;
            }

            try
            {
                lds_b2b_invoice_details = VisualElementHelper.CreateFromView<GridElement>("d_b2b_invoice_details", "d_b2b_invoice_details", "global_global");
            }
            catch (Exception)
            {
                return true;
            }



            for (ll_i = 0; ll_i < dw_body.RowCount(); ll_i++)
            {

                ll_doc_number = dw_body.GetItemValue<Int64?>(ll_i, "number");

                ll_b2b_status = dw_body.GetItemValue<long>(ll_i, "b2b_status");
                if (ll_b2b_status == 1 && ll_doc_number > 0)
                {

                    ll_count = lds_b2b_invoice_details.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, ll_doc_number);
                    if (ll_count > 0)
                    {

                        ll_shipment_number = lds_b2b_invoice_details.GetItemValue<long>(0, "b2b_invoice_move_shipment_number");
                        if (ll_shipment_number > 0)
                        {
                            await this.ViewModel.uf_show_message("הודעה", "", "OK", "החשבונית סופקה במשלוח, לא ניתן לקלוט במסופון ישן");
                            return false;
                        }
                    }
                }
            }


            return true;
        }

        public async Task<bool> uf_check_crossdoc_invoice() //********************************************************************
        {
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:				uo_mini_terminal_header
            //*Function Name:	uf_check_crossdoc_invoice
            //*Purpose: 			Check if invoice belongs to crossdoc (distributor 100)
            //*Arguments: 		None
            //*Return:				TRUE/FALSE
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*04-01-2015		AlexKh			1.1.26.0	SPUCM00005131	Initial version
            //********************************************************************

            long ll_distributor_number = 0;
            long ll_supplier_number = 0;
            long? ll_invoice_number = null;
            long ll_invoices_count = 0;
            long ll_i = 0;
            string ls_carton_barcode = null;

            //GalilCS - return false if there is no rows
            if (dw_body.Repository().RowCount() == 0)
                return false;

            ll_distributor_number = dw_body.GetItemValue<long>(0, "distributor_number");
            if (ll_distributor_number == mini_terminal.uo_mini_terminal_header.CROSSDOC_DISTRIBUTOR)
            {

                ll_supplier_number = Convert.ToInt64(supplier_number.Text);

                ll_invoices_count = dw_body.RowCount();
                for (ll_i = 0; ll_i < ll_invoices_count; ll_i++)
                {

                    ll_invoice_number = dw_body.GetItemValue<Int64?>(ll_i, "number");
                    ls_carton_barcode = "";
                    // AlexKh - 1.1.35.0 - 2015-04-27 - SPUCM00005318 - check only state 's', remove 'n', 'a'
                    if (ll_invoice_number > 0)
                    {
                        ViewModel.LoadData(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_invoice_number.Value, ref ls_carton_barcode);
                        if (!isempty_stringClass.isempty_string(ls_carton_barcode))
                        {
                            // AlexKh - 1.1.27.0 - 2015-02-04 - SPUCM00005224 - improve the message
                            //uf_show_message(" שגיאה בקרטון " + ls_carton_barcode, "", "OK", "לא ניתן לקלוט חשבונית, יש לסרוק את הקרטון קרוסדוק במסופון" )
                            await this.ViewModel.uf_show_message(" שגיאה בקרטון " + ls_carton_barcode, "", "OK", "לא ניתן לקלוט את החשבונית,יש לסרוק את קרטון הקרוסדוק במסופון.");
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        public async Task uf_print_open_pdf() //Rony@22/03/2015>>SPUCM00005274 - Basaed on click event in the btn that was remarked
        {
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            RadioButtonElement rb_print_pdf = this.GetVisualElementById<RadioButtonElement>("rb_print_pdf");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            string ls_null = null;
            string ls_status = null;
            string ls_invoice_type = null;
            string ls_path = null;
            string ls_file = null;
            string ls_supplier_edi_number = null;
            string ls_branch_edi_number = null;
            string ls_full_path = null;
            string ls_dir = null;
            long ll_rc = 0;
            long ll_supplier_number = 0;
            long? ll_invoice_number = null;
            long ll_row = 0;
            long ll_distributor_number = 0;
            DateTime? ld_date = default(DateTime);
            bool lb_print = false;

            ls_null = null;

            ll_row = dw_body.GetSelectedRow(0);
            if (ll_row < 0)
            {
                return;
            }
            if (rb_print_pdf.IsChecked) //Rony@22/03/2015>>SPUCM00005274
            {
                lb_print = true;
            }
            else
            {
                lb_print = false;
            }

            ll_supplier_number = Convert.ToInt64(supplier_number.Text);

            ll_invoice_number = dw_body.GetItemValue<Int64?>(ll_row, "number");

            ls_status = dw_body.GetItemValue<string>(ll_row, "state");

            ls_invoice_type = dw_body.GetItemValue<string>(ll_row, "type");
            // AlexKh - 1.1.27.0 - 2015-02-04 - SPUCM00005224 - take supply date instead date_move //added 29.7.12 by Eitan
            //ld_date				=	Date(dw_body.GetItem<DateTime>(ll_row, "date_move"))


            ld_date = dw_body.GetItemValue<DateTime>((int)ll_row, "supply_date");
            // AlexKh - 1.1.33.0 - 2015-03-18 - SPUCM00005224 - get distributor		//added 29.7.12 by Eitan

            ll_distributor_number = dw_body.GetItemValue<long>(ll_row, "distributor_number");
            // AlexKh - 1.1.33.0 - 2015-03-18 - SPUCM00005224 - get distributor		//added 29.7.12 by Eitan
            if (ll_distributor_number > 0)
            {
                ls_supplier_edi_number = f_get_supplier_distributor_ediClass.f_get_supplier_distributor_edi(ll_supplier_number, ll_distributor_number);
            }
            if (isempty_stringClass.isempty_string(ls_supplier_edi_number)) //added 29.7.12 by Eitan
            {
                ls_supplier_edi_number = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_supplier_number);
            }
            ls_branch_edi_number = masofonAlias.masofon.Instance.qs_parameters.edi_number;
            if (ls_invoice_type != "p")
            {
                return;
            }
            if (ll_supplier_number > 0 && ll_invoice_number > 0)
            {
                // AlexKh - 1.1.22.0 - 2014-07-27 - SPUCM00004921 - use PDF path saved in invoice_move
                if (await f_get_parameter_valueClass.f_get_parameter_value("pdf_absolute_path", "boolean", "false", "Use absolute PDF path for invoice.", "gds_find") == "true")
                {
                    ViewModel.LoadData1(ll_supplier_number, ll_invoice_number.Value, ref ls_full_path);
                    if (isempty_stringClass.isempty_string(ls_full_path))
                    {
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא קיים מסמך ממוחשב");
                        return;
                    }
                }
                else
                {
                    ls_file = "SPIN" + SystemFunctionsExtensions.Fill("0", 13 - ls_supplier_edi_number.Length) + ls_supplier_edi_number + SystemFunctionsExtensions.Fill("0", 13 - ls_branch_edi_number.ToString().Length) + ls_branch_edi_number.ToString() + SystemFunctionsExtensions.Fill("0", 2 - ld_date.Value.Year.ToString().Substring(ld_date.Value.Year.ToString().Length - 2).ToString().Length) + ld_date.Value.Year.ToString().Substring(ld_date.Value.Year.ToString().Length - 2).ToString() + SystemFunctionsExtensions.Fill("0", 2 - ld_date.Value.Month.ToString().Length) + ld_date.Value.Month.ToString() + SystemFunctionsExtensions.Fill("0", 10 - ll_invoice_number.ToString().Length) + ll_invoice_number.ToString() + ".pdf";
                    ls_path = masofonAlias.masofon.Instance.gs_vars.digital_invoice_folder + SystemFunctionsExtensions.Fill("0", 3 - masofonAlias.masofon.Instance.gs_vars.branch_number.ToString().Length) + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "\\" + SystemFunctionsExtensions.Fill("0", 3 - ll_supplier_number.ToString().Length) + ll_supplier_number.ToString() + "\\" + SystemFunctionsExtensions.Fill("0", 2 - ld_date.Value.Year.ToString().Substring(ld_date.Value.Year.ToString().Length - 2).ToString().Length) + ld_date.Value.Year.ToString().Substring(ld_date.Value.Year.ToString().Length - 2).ToString() + SystemFunctionsExtensions.Fill("0", 2 - ld_date.Value.Month.ToString().Length) + ld_date.Value.Month.ToString() + "\\";
                    ls_full_path = ls_path + ls_file;
                }
                //ls_dir = f_get_parameter_value("pdf_dir", "string", "c:\", "Directory for PDF files", "gds_find") 
                ls_dir = await f_get_parameter_valueClass.f_get_parameter_value("pdf_dir_invoice", "string", "c:\\", "Directory for PDF files invoices", "gds_find");
                if (lb_print) //Ron@22/03/2015>>PSUCM00005274
                {
                    //ll_rc = SystemHelper.ShellExecuteA(this.ViewModel, "print", ls_dir + ls_full_path, ls_null, ls_null, 1);
                    //Call PrintBOS API
                    var file = ls_dir + ls_full_path;
                    byte[] pdfBytes = System.IO.File.ReadAllBytes(file);
                    string pdfBase64 = Convert.ToBase64String(pdfBytes);

                    var layout = PdfToImage.GetLayout(file);

                    var ls_printer = MvcSite.Common.Globals.PrinterName;
                    SystemHelper.Print_Pdf(ls_printer, ls_file, pdfBase64,layout : layout);
                }
                else
                {
                    //ll_rc = SystemHelper.ShellExecuteA(this.ViewModel, "open", ls_dir + ls_full_path, ls_null, ls_null, 1);
                    Extensions.DownloadPDF(ls_dir + ls_full_path);
                }
            }
            else
            {
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא נבחרה חשבונית במסך");
            }
        }




        public void destructor() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			destructor
            //*Purpose:							Destroy non visual objects.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            if (this.ViewModel.iuo_order_searchProperty != null)
            {


            }
        }
        public void ue_save_data()
        {
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_pdf_clicked(object sender, EventArgs e)
        {
      
            await uf_print_open_pdf();
          
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async void cb_delete_clicked(object sender, EventArgs e) // AlexKh - 1.2.43.1 - 2009-04-01 - SPUCM00000090 - check if no items exist on the invoice
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");


            // 			then invoice can be deleted
            long ll_row = 0;
            long? ll_doc_number = null;
            long ll_b2b_status = 0;
            long ll_row_saved = 0;
            string ls_state = null;
            bool lb_status = false;

            ll_row = dw_body.SelectedRowIndex;
            if (ll_row < 0)
            {
                return;
            }

            ll_doc_number = dw_body.GetItemValue<Int64?>(ll_row, "number");

            ll_b2b_status = dw_body.GetItemValue<long>(ll_row, "b2b_status");
            ls_state = dw_body.GetItemValue<string>(ll_row, "state");

            ll_row_saved = dw_body.GetItemValue<long>(ll_row, "row_saved");
            if (ll_b2b_status == 1 || (ls_state != "O" || ll_row_saved == 1))
            {
                lb_status = true;
            }
            else
            {
                lb_status = false;
            }
            if (lb_status && ll_doc_number > 0)
            {
                await this.ViewModel.uf_show_message("Error", "", "OK", "קיימים פריטים בחשבונית זו");
            }
            else
            {

                dw_body.Delete((int)ll_row);

            }
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async void cb_new_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			cb_new.Clicked() 
            //*Purpose:							Reset uo_mini_terminal_header
            //*Arguments:						None.
            //*Return:								Integer: 1 - succeeded.
            //*												  -1 - failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*11-07-2007    SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            bool lb_save_before_close = false;
         //   bool lb_item_scanned = false;
            string ls_message = null;
            int li_rtn_code;
            long ll_row_count = 0;
            //Write to log
            ls_message = "*************************** Click - cb_new*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "cb_new The Data:  " + "Clicked on cb_new" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            ll_row_count = dw_body.RowCount();
            if (ll_row_count > 0)
            {
                //li_rtn_code = await MessageBoxExtensions.MsgBox("שים לב נתונים שלא נשמרו -לא ישמרו.האם אתה בטוח?", MessageBoxStyle.YesNo, "אזהרה");
                li_rtn_code = (int)await ViewModel.getMiniTerminal().wf_show_message("אזהרה", "", "OKCancle", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("שים לב נתונים שלא נשמרו -לא ישמרו.האם אתה בטוח?"));
                //li_rtn_code = (int)this.ViewModel.uf_show_message("אזהרה", "", "OKCancel", "שים לב נתונים שלא נשמרו -לא ישמרו.האם אתה בטוח?");
                if (li_rtn_code == 1)
                {
                    //New
                    this.uf_new_doc();
                }
                else
                {
                    return;
                }
            }
            else
            {
                this.uf_new_doc();
            }
            supplier_number.Focus();
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async void cb_close_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			cb_close.event clicked event
            //*Purpose:							close the window.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007    SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            //Call the function wf_close_window in order to close the window
            await this.uf_close_window();
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_add_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			cb_add.clicked event
            //*Purpose:							Add new row to the window.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            //dw_body.TRIGGER EVENT ue_new_row()
            if (await this.ue_validation_check())
            {
                this.dw_body_ue_new_row();
            }
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_details_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			cb_details.clicked event
            //*Purpose:							Display uo_mini_terminal_details.
            //*  
            //*Arguments:						None.
            //*Return:								Long: 1 - Success. -1 - Failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007    SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            bool lb_return_code = false;
            long ll_order_number = 0;
            long ll_supplier_number = 0;
            this.ViewModel.checkOneSetOreder = "";

            //GalilCS 
            if (dw_body.GetPreventClickOnDetails())
            {
                dw_body.SetPreventClickOnDetails(false);
                return;
            }

            if (!await this.ue_validation_check())
            {
                return;
            }
            // ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
            ll_supplier_number = Convert.ToInt64(supplier_number.Text);
            // AlexKh - 1.2.46.4 - 2010-12-06 - SPUCM00002425 - check invoice supplied by shipment
            if (!await uf_check_invoice_in_shipment((int)ll_supplier_number))
            {
                return;
            }
            // AlexKh - 1.1.26.0 - 2015-01-04 - SPUCM00005131 - check karton status in shipment crossdoc
            if (!(await uf_check_crossdoc_invoice()))
            {
                return;
            }
            li_rtn_code = await this.uf_display("DETAILS");
            li_rtn_code = await this.uf_delete_empty_rows();
            //ll_order_number = dw_header.GetItemValue<long>(0, "order_number");
            ll_order_number = Convert.ToInt64(order_number.Text);
            if (!(ll_order_number == 0))
            {
                await this.uf_set_order_number(ll_order_number);
            }
            if (!(ll_supplier_number == 0))
            {
                await this.uf_set_supplier_number(ll_supplier_number);
            }
        }
        public void dw_body_ue_new_row() //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.ue_new_row
            //*Purpose:							Insert new row into the DataWindow
            //*  
            //*Arguments:						None.
            //*Return:								None.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            long ll_row = 0;
            long ll_supplier_number = 0;
            long ll_row_count = 0;
            long? ll_doc_no = null;
            long ll_order_number = 0;
            decimal ldec_discount = default(decimal);
            string lc_doc_type = "";

            ll_row_count = dw_body.RowCount();
            if (ll_row_count > 0)
            {
                //number
                ll_doc_no = dw_body.GetItemValue<Int64?>(ll_row_count - 1, "number");
                if (ll_doc_no == 0 || ll_doc_no == 0)
                {
                    return;
                }
                lc_doc_type = dw_body.GetItemValue<string>(ll_row_count - 1, "doc_type");
            }

            ll_row = dw_body.Insert(0);
            //GalilCS - Take the last row number
            if (ll_row > ll_row_count)
            {
                ll_row = ll_row_count;
            }
            ll_order_number = Convert.ToInt64(order_number.Text);

            ll_supplier_number = Convert.ToInt64(supplier_number.Text);
            ldec_discount = this.ViewModel.iuo_order_searchProperty.uf_get_order_discount();

            dw_body.SetItem(ll_row, "employee_number", this.ViewModel.il_employee_numberProperty);

            dw_body.SetItem(ll_row, "branch_number", Convert.ToInt64(masofonAlias.masofon.Instance.gs_vars.branch_number));

            dw_body.SetItem(ll_row, "supplier_number", ll_supplier_number);

            dw_body.SetItem(ll_row, "station_num", Convert.ToInt64(masofonAlias.masofon.Instance.gs_station.station_number));

            dw_body.SetItem(ll_row, "supply_date", DateTime.Now);

            dw_body.SetItem(ll_row, "date_move", DateTime.Now);

            dw_body.SetItem(ll_row, "state", "O");

            dw_body.SetItem(ll_row, "discount_percent", ldec_discount);

            dw_body.SetItem(ll_row, "order_number", ll_order_number);
            if (ll_row_count > 0)
            {

                dw_body.SetItem(ll_row, "doc_type", lc_doc_type.ToString());
            }
            // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - CR#1119 - set create mode means locking invoice

            dw_body.SetItem(ll_row, "create_mode", "1");
            //THIS.Object.log_book.Protect = '0'

            dw_body.Modify("NoUserPrompt=Yes");
            this.ViewModel.uf_set_focus("dw_body", ll_row, "number");
            // AlexKh - 1.2.45.3 - 2009-12-09 - SPUCM00001763 - the position of Horizonal-Scroll. 





            //dw_body.set_HorizontalScrollPosition(dw_body.get_HorizontalScrollMaximum());
        }
        public async Task<bool> dw_body_ue_log_book_exists(long al_row, long al_new_log_book) //**********************************************************************************************
        {
            GridElement dw_header = this.GetVisualElementById<GridElement>("dw_header");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.ue_log_book_exists
            //*Purpose:							Check if LogBook number exist already in the system
            //*  										(in an invoice or a packlist).
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long 						al_row
            //*										Value			long 						al_new_log_book
            //*			
            //*Return:								Boolean: True - LogBook number exist already.
            //*													False - LogBook number does not exist.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007   SHARP.					   0.2.0 		B2B 			Initiation
            //************************************************************************************************

            bool lb_log_book_by_supplier = false;
            long ll_count = 0;
            long ll_row_count = 0;
            long ll_cnt = 0;
            long ll_supplier_number = 0;
            long ll_orig_log_book = 0;
            long? ll_doc_number = null;
            lb_log_book_by_supplier = masofonAlias.masofon.Instance.gs_vars.log_book_defined_by_supplier.ToLower(CultureInfo.InvariantCulture) == "yes";

            if (dw_body.RowCount() <= 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "חסר נתונים התחלתיים");
                return false;
            }
            if (al_new_log_book == 0 || al_new_log_book == 0)
            {
                //.. please enter log number
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#20001");
                return false;
            }

            ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");

            ll_doc_number = dw_body.GetItemValue<Int64?>(al_row, "number");
            //Invoice
            if (lb_log_book_by_supplier)
            {
                ViewModel.LoadData2(al_new_log_book, ll_supplier_number, ll_doc_number.Value, ref ll_count);
            }
            else
            {
                ViewModel.LoadData3(al_new_log_book, ll_doc_number.Value, ref ll_count);
            }
            if (ll_count > 0) // "Error", "Log book number already exists"
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#5212");

                dw_body.SetItem(al_row, "log_book", "");

                //dw_body.SetColumn("log_book");
                //dw_body.Focus();

                return false;
            }
            // Packing list
            if (lb_log_book_by_supplier)
            {
                ViewModel.LoadData4(al_new_log_book, ll_supplier_number, ll_doc_number.Value, ref ll_count);
            }
            else
            {
                ViewModel.LoadData5(al_new_log_book, ll_doc_number.Value, ref ll_count);
            }
            if (ll_count > 0)
            {
                // "Error", "Log book number already exists"
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#5212");
              
                return false;
            }
            //Check if logbook exists on the window and not saved yet in the DB

            ll_row_count = dw_body.RowCount();
            if (ll_row_count > 1)
            {
                for (ll_cnt = 1; ll_cnt <= ll_row_count; ll_cnt++)
                {


                    ll_orig_log_book = dw_body.GetItemValue<long>(ll_cnt, "log_book");
                    if (ll_orig_log_book == al_new_log_book && ll_cnt != al_row)
                    {
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#5212");

                        dw_body.SetItem(al_row, "log_book", "");

                        //dw_body.SetColumn("log_book");
                        //dw_body.Focus();
                        return false;
                    }
                }
            }
            return true;
        }
        
        public int dw_body_ue_invoice_pack_exists(string as_doc_type, long al_doc_number) //**********************************************************************************************
        {
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.ue_invoice_pack_exists
            //*Purpose:							Check if doc number alread exists in the system:
            //*										an invoice or a packlist or a return wuth the same number.
            //*										First check in the DW for new (inserted by the user) documents
            //*										and then check also in the DB for existing documents.
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			String						as_doc_type
            //*										Value			Long						al_doc_number
            //*
            //*Return:								Integer: 0 - Invoice/PackList/Return does not exist.
            //*												   1 - Invoice exist.
            //*												   2 - PackList exist.
            //*												   3 - Return exist.
            //*												   -1 - Error.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-08-2007		SHARP.					0.2.0 		B2B 			Initiation
            //************************************************************************************************

            string ls_find = null;
            long? ll_found = 0;
            long ll_supplier_number = 0;
            long ll_temp = 0;

            if (dw_body.RowCount() > 1)
            {
                ls_find = "doc_type == \"" + as_doc_type + "\" and number == " + al_doc_number.ToString();


                ll_found = dw_body.Find(ls_find, 0, dw_body.RowCount());
                while (ll_found >= 0)
                {

                    if (ll_found != dw_body.GetRow())
                    {
                        if (as_doc_type == "I")
                        {
                            return 1;
                        }
                        if (as_doc_type == "P")
                        {
                            return 2;
                        }
                    }

                    if (ll_found + 1 > dw_body.RowCount())
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }


                    ll_found = dw_body.Find(ls_find, (int)(ll_found + 1), dw_body.RowCount());
                }
            }

            ll_supplier_number = Convert.ToInt64(supplier_number.Text);
            if (as_doc_type == "P")
            {
                ViewModel.LoadData6(al_doc_number, ll_supplier_number, ref ll_temp);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                {
                    return -1;
                }
                if (ll_temp == 1)
                {
                    return 1;
                }
            }
            else if (as_doc_type == "I")
            {
                ViewModel.LoadData7(al_doc_number, ll_supplier_number, ref ll_temp);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                {
                    return -1;
                }
                if (ll_temp == 1)
                {
                    return 2;
                }

                if (masofonAlias.masofon.Instance.sqlca.SqlCode != -1 && ll_temp != 1)
                {
                    ViewModel.LoadData8(al_doc_number, ll_supplier_number, ref ll_temp);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                    {
                        return -1;
                    }
                    if (ll_temp == 1)
                    {
                        return 3;
                    }
                }
            }
            return 0;
        }
        // TODO: Could not add event handler for handler 'itemerrorEvent'. (CODE=1017)
        public int dw_body_itemerror(int row, VisualElement dwo, string data) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.ItemError
            //*Purpose:							Reject the data value with no message box
            //*  
            //*Arguments:						
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            //Reject the data value with no message box
            return 1;
        }
        // TODO: Could not add event handler for handler 'itemchangedEvent'. (CODE=1017)
        public async Task<int> dw_body_itemchanged(object sender, EventArgs e) //**********************************************************************************************
        {
            ButtonElement cb_details = this.GetVisualElementById<ButtonElement>("cb_details");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.itemchanged
            //*Purpose:							a) Invoice/PackList Number - Check if it is a valid number,
            //* 											if no, return -1. If yes, check if it exists already, if yes,
            //*											return -1.
            //*										b) Suppliy Date - Check if it is a valid date, and not null.
            //*										c) Total - Check if it is null or 0.
            //*										d) Logbook - checks if same log book exists.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long						row
            //*										Value			ControlElement				dwo
            //*										Value			String						data
            //*
            //*Return:								Long							
            //*Date 			    Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		   SHARP 			    0.2.0 			B2B 			Initiation
            //************************************************************************************************

            long ll_doc_number = 0;
            long ll_temp = 0;
            decimal? ll_temp1 = 0;
            long ll_order_number = 0;
            long ll_error_number = 0;
            long ll_null = 0;
            long ll_doc_num_exists = 0;
            long ll_ret = 0;
            long ll_row = 0;
            long ll_new_log_book = 0;
            long ll_orig_log_book = 0;
            long? ll_old_number = null;
            long ll_supplier_number = 0;
            DateTime? ldt_date_null = default(DateTime);
            DateTime? ldt_supply_date = default(DateTime);
            DateTime? ldt_current_datetime = default(DateTime);
            DateTime? ld_supply_date = default(DateTime);
            string ls_column = null;
            string ls_doc_type = null;
            string ls_null = null;
            string ls_message = null;
            decimal ldec_discount = default(decimal);
            bool lb_log_book_exists = false;
            ModelAction ldw_row_status = default(ModelAction);
            ComboBoxElement ldwc_supplier_name;

            int row = 0;
            var dwo = sender as ControlElement;
            GridCellEventArgs args = e as GridCellEventArgs;
            string data = args.Value;
            if (args.DataIndex == null)
            {
                ls_column = dw_body.CurrentColumn.ID.ToString();
                row = dw_body.SelectedRowIndex;
            }
            else
            {
                ls_column = args.DataIndex;
                row = args.RowIndex;
            }

            ls_message = "*************************** Item Changed - dw_head*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + dwo.ID.ToString() + "\r" + "\n" + "Row:        " + row.ToString() + "\r" + "\n" + "The Data:  " + data + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //uf_calculate_declines
            ldt_current_datetime = DateTime.Now; //RonY@14/04/2015 - SPUCM00005298
            switch (ls_column)
            {
                case "number":

                    try
                    {
                        ll_temp = Convert.ToInt64(data);
                    }
                    catch
                    {
                        data = "";
                        ll_temp = 0;
                    }

                    ll_old_number = dw_body.GetItemValue<Int64?>(row, "number");

                    if (ll_old_number == ll_temp)
                    {
                        return 2;
                    }
                    ls_doc_type = dw_body.GetItemValue<string>(row, "doc_type");
                    ll_supplier_number = Convert.ToInt64(supplier_number.Text);

                    if ((data == null || ll_temp == 0 && data == "") && !(ls_doc_type == "I" && (ll_supplier_number == 836 || ll_supplier_number == 777 || ll_supplier_number == 338))) 
                    {
                        dw_body.SetItem(row, "number", Convert.ToString(ll_temp));
                        dw_body.Rows[row].Cells["number"].Value = Convert.ToString(data);

                        if (ls_doc_type == "P")
                        {
                            //.. Packing number cannot be empty
                            await this.ViewModel.uf_show_message("", "", "OK", "#20013");
                        }
                        else if (ls_doc_type == "I")
                        {
                            dw_body.SetPreventClickOnDetails(true);
                            //.. Invoice number cannot be empty
                            await this.ViewModel.uf_show_message("", "", "OK", "#20014");
                            
                        }
                       
                        return 2;
                    }
                    else
                    {
                        //  prevent invoice for suppliers 836 and 777 and 338
                        if (ls_doc_type == "I" && (ll_supplier_number == 836 || ll_supplier_number == 777 || ll_supplier_number == 338))
                        {
                            dw_body.Rows[row].Cells["number"].Value = "";
                            await this.ViewModel.uf_show_message("", "", "OK", "לספק זה ניתן להקליד רק תעודת משלוח");
                            this.ViewModel.EditCellByPosition(dw_body, "number", row);
                            return 2;
                        }
                        ll_doc_num_exists = this.dw_body_ue_invoice_pack_exists(ls_doc_type, Convert.ToInt64(data));
                        if (ll_doc_num_exists == 0)
                        {
                            if (ls_doc_type == "P")
                            {
                                ls_doc_type = "I";
                            }
                            else
                            {
                                ls_doc_type = "P";
                            }
                            ll_doc_num_exists = this.dw_body_ue_invoice_pack_exists(ls_doc_type, Convert.ToInt64(data));
                        }
                        switch (ll_doc_num_exists)
                        {
                            case 0:
                                // Set the button קליטת פריטים Enabled
                                if (cb_details.Enabled == false)
                                {
                                    cb_details.Enabled = true;
                                }
                                dw_body.SetItem(row, "number", ll_temp.ToString());
                                dw_body.Rows[row].Cells["number"].Value = Convert.ToString(ll_temp);
                                this.ViewModel.EditCellByPosition(dw_body, "supply_date", row);
                                
                                return 0;
                            case 1:
                                // Invoice already exists
                                await this.ViewModel.uf_show_message("", "", "OK", "#5310");
                                //RETURN 2
                                break;
                            case 2:
                                // Number already exists, should be PackList
                                await this.ViewModel.uf_show_message("", "", "OK", "#5277");
                                //RETURN 2
                                break;
                            case 3:
                                // Recallment already exists
                                await this.ViewModel.uf_show_message("", "", "OK", "#5309");
                                //RETURN 2
                                break;
                            case -1:
                                // DB Error

                                await this.ViewModel.uf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
                                //RETURN 2
                                break;
                        }

                        dw_body.SetItem(row, "number", ll_old_number);
                        dw_body.Rows[row].Cells["number"].Value = Convert.ToString(ll_old_number);
                        this.ViewModel.EditCellByPosition(dw_body, "number", row);

                        //POST uf_set_focus("dw_body",row, "number")
                        return 2;
                    }



                    ldw_row_status = dw_body.GetItemStatus(dw_body.CurrentRow.Index, "number", ModelBuffer.Primary);

                    if (ldw_row_status == ModelAction.Insert)
                    {
                        await ViewModel.getMiniTerminal().wf_build_doc_number_list();
                    }

                    long ll_row_saved = 0;
                    long ll_log_book = 0;


                    ll_row_saved = dw_body.row_saved(1);


                    ll_log_book = dw_body.log_book(1);
                    break;
                case "supply_date":

                    //RonY@14/04/2015 -  Add and Remark  - SPUCM00005298 based on the validation in office - start				

                    //(SystemFunctionsExtensions.DaysAfter(Convert.ToDateTime((1 - 1).Substring(10 - 1)), ldt_current_datetime) > 365 || ((String.Concat(Convert.ToDateTime((1 - 1).Substring(10 - 1)), "yyyyMMdd") <= "19000101") || (Convert.ToDateTime((1 - 1).Substring(10 - 1)) > ldt_current_datetime)))
                    if (isempty_stringClass.isempty_string(data))
                    {
                        await this.ViewModel.uf_show_message("", "", "OK", "תאריך לא תקין");
                        dw_body.SetItem(0, "supply_date", ldt_current_datetime.ToString());
                        return 2;
                    }
                    dw_body.Rows[row].Cells["supply_date"].Value = Convert.ToDateTime(data);

                    //RonY@14/04/2015 -  Add and Remark  - SPUCM00005298 - end
                    this.ViewModel.EditCellByPosition(dw_body, "invoice_total", row);

                    break;
                case "invoice_total":

                    try
                    {
                        ll_temp1 = Convert.ToDecimal(data);
                    }
                    catch
                    {
                        data = "";
                        ll_temp1 = 0;
                    }
                    var expected_total_amount = dw_body.GetItemValue<decimal>(row, "expected_total_amount");
                    if ((data == null || ll_temp1 == 0) && expected_total_amount != 0)
                    {
                        await this.ViewModel.uf_show_message("", "", "OK", ".סה\"כ לא יכול להיות ריק");

                        dw_body.SetItem(row, "expected_total_amount", ll_temp1.ToString());
                        return 2;
                    }

                    dw_body.SetItem(row, "expected_total_amount", ll_temp1.ToString());
                    dw_body.SetItem(row, "invoice_total", string.Format("{0:F2}", ll_temp1.Value));
                    break;
                case "log_book":

                    ll_row = dw_body.GetRow();
                    ll_new_log_book = Convert.ToInt64(data);
                    lb_log_book_exists = await this.dw_body_ue_log_book_exists(ll_row, ll_new_log_book);
                    if (!lb_log_book_exists)
                    {
                        //POST SetColumn("log_book")

                        dw_body.SetItem(row, "log_book", "0");

                        //dw_body.SetColumn("log_book");
                        //dw_body.Focus();
                        return 2;
                    }
                    else
                    {

                        dw_body.PerformValidated(new EventArgs());
                    }
                    break;
                case "doc_type":
                    this.ViewModel.ic_old_doc_typeProperty = !string.IsNullOrEmpty(dw_body.GetItemValue<string>(row, "doc_type")) ?  Convert.ToChar(dw_body.GetItemValue<string>(row, "doc_type")) : default(char);
                    // AlexKh - 1.1.22.0 - 2014-07-16 - SPUCM00004921 - prevent invoice for suppliers 836 and 777

                    //ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
                    ll_supplier_number = Convert.ToInt64(supplier_number.Text);
                    if (data == "I" && (ll_supplier_number == 836 || ll_supplier_number == 777))
                    {
                        await this.ViewModel.uf_show_message("", "", "OK", "לספק זה ניתן להקליד רק תעודת משלוח");

                        dw_body.SetItem(row, "doc_type", this.ViewModel.ic_old_doc_typeProperty.ToString());

                        //dw_body.SetRow(row);
                        //dw_body.SetColumn("doc_type");
                        //dw_body.Focus();
                        return 2;
                    }



                    ldw_row_status = dw_body.GetItemStatus(dw_body.CurrentRow.Index, "doc_type", ModelBuffer.Primary);

                    if (ldw_row_status != ModelAction.Insert || data != Convert.ToString(this.ViewModel.ic_old_doc_typeProperty))
                    {
                        this.ViewModel.ic_old_doc_typeProperty = dw_body.GetItemValue<string>(row, "doc_type") == null ? default(char) : Convert.ToChar(dw_body.GetItemValue<string>(row, "doc_type"));
                        this.ViewModel.ic_new_doc_typeProperty = data == null ? default(char) : Convert.ToChar(data);
                        dw_body.SetItem(row, "doc_type", data);
                    }
                    break;
            }



            ldw_row_status = dw_body.GetItemStatus(dw_body.CurrentRow.Index, 0, ModelBuffer.Primary);

            if (ldw_row_status != ModelAction.Insert)
            {
                await this.uf_update_doc(row);
            }
            return 0;
        }



        // TODO: Could not add event handler for handler 'losefocusEvent'. (CODE=1017)
        public async Task dw_body_losefocus(object sender, EventArgs e) //**********************************************************************************************
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_body.ItemError
            //*Purpose:							Reject the data value with no message box.
            //*  
            //*Arguments:						
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            long ll_row = 0;

            dw_body.PerformValidated(e);

            ll_row = dw_body.GetRow();


            ModelAction ldwis_doc = dw_body.GetItemStatus((int)ll_row, 0, ModelBuffer.Primary);

            if (ldwis_doc == ModelAction.None)
            {
                await this.uf_update_doc(ll_row);
            }
        }
        // TODO: Could not add event handler for handler 'doubleclickedEvent'. (CODE=1017)
        public async Task dw_body_doubleclicked(int xpos, int ypos, int row, VisualElement dwo) // AlexKh - 1.2.46.5 - 2011-01-03 - CR#1143 - if automatic declines, the don't allow manual on invoice level
        {
            GridElement dw_header = this.GetVisualElementById<GridElement>("dw_header");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("automatic_declines", "boolean", "false", "Use automatic declines .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                // SharonS - 1.2.26.8 - 2008-03-18 - Only if ib_send_decline_msg is BOTH or DB
                if (this.ViewModel.is_send_decline_msgProperty == "DB_MSG" || this.ViewModel.is_send_decline_msgProperty == "DB")
                {
                    //
                    // PninaSG - 1.2.23.0 - 2008-01-14 - TASK#10003 - open decline for doc window
                    long ll_order_number = 0;
                    long ll_supplier_number = 0;
                    long ll_b2b_status = 0;
                    long ll_row_saved = 0;
                    long ll_decline_exist = 0;
                    long ll_serial_number = 0;
                    s_array_arguments lstr_mess = default(s_array_arguments);
                    string ls_state = null;
                    ModelAction ldw_row_status = default(ModelAction);


                    ldw_row_status = dw_body.GetItemStatus(row, 0, ModelBuffer.Primary);

                    if (ldw_row_status != ModelAction.Insert)
                    {

                        if (Convert.ToString(dwo.ID) == "number")
                        {
                            ll_b2b_status = dw_body.GetItemValue<long>(row, "b2b_status");
                            ls_state = dw_body.GetItemValue<string>(row, "state");
                            ll_row_saved = Convert.ToInt64(dw_body.GetItemValue<string>(row, "row_saved"));
                            // SharonS - 1.2.31.1 - 2008-06-01 - Bug, Open decline window only for open doc
                            //			IF ll_b2b_status = 1 OR ls_state <> 'O' OR  ll_row_saved  = 1 THEN
                            if (ll_b2b_status == 1 && (ls_state == "O" && ll_row_saved == 1))
                            {
                                // End
                                ll_order_number = dw_header.GetItemValue<long>(0, "order_number");
                                ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
                                lstr_mess.a_string[1] = dw_body.GetItemValue<char>(row, "doc_type").ToString(); //parent_doc_type 
                                                                                                                // SharonS - 1.2.26.8 - 2008-03-18 - Change decline_level
                                                                                                                //lstr_mess.a_long[1] = 1 													//decline_level 
                                lstr_mess.a_long[1] = 0; //decline_level 
                                                         //
                                lstr_mess.a_long[2] = ll_order_number; //order_number 
                                lstr_mess.a_long[3] = ll_supplier_number; //supplier_number 



                                lstr_mess.a_long[4] = dw_body.GetItemValue<Int64?>(row, "number").Value; //parent_doc_number
                                lstr_mess.a_long[5] = 0; //material_number 
                                lstr_mess.a_long[6] = this.ViewModel.il_employee_numberProperty;
                                lstr_mess.a_long[7] = ll_serial_number;
                                // SharonS - 1.2.42.0 - 2009-01-19 - CR#1100 - SPUCM00000115 - Send distributor_number 
                                // lstr_mess.a_long[8] & lstr_mess.a_long[9] are for item level
                                lstr_mess.a_long[10] = dw_body.GetItemValue<long>(row, "distributor_number");
                                // End
                                var frm = await WindowHelper.Open<rw_declines_reasons>("global_global", "args", lstr_mess);


                                ll_decline_exist = frm.ll_decline_exist_return;
                                if (ll_decline_exist == 1)
                                {
                                    dw_body.SetItem(row, "declines", 1);
                                }
                                else
                                {
                                    dw_body.SetItem(row, "declines", 0);
                                }
                            }
                        }
                    }
                    //
                    // SharonS - 1.2.26.8 - 2008-03-18 - Only if ib_send_decline_msg is BOTH or DB
                }
                //
            }
        }
        // TODO: Could not add event handler for handler 'constructorEvent'. (CODE=1017)
        public void dw_body_constructor(object sender, EventArgs e) // AlexKh - 1.2.45.3 - 2009-12-09 - SPUCM00001763 - the position of Horizonal-Scroll. 
        {
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");





            dw_body.set_HorizontalScrollPosition(dw_body.get_HorizontalScrollMaximum());
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public void dw_body_clicked(object sender, GridElementCellEventArgs e) //Eitan; SPUCM00004319; Ver 1.1.1.14; 7/11/2013
        {
            int row = e.RowIndex;
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            int li_rc = 0;
            int li_selected_row = 0;

            li_selected_row = dw_body.GetSelectedRow(0);

            if (row > 0 && row <= dw_body.RowCount())
            {

                if (dw_body.IsSelected(row))
                {

                    //dw_body.SelectRow(row, false);
                    return;
                }
                else
                {

                    //dw_body.SelectRow(0, false);

                    //dw_body.SelectRow(row, true);
                }
            }
            else
            {

                //dw_body.SelectRow(0, false);
                return;
            }
        }
        // TODO: Could not add event handler for handler '__SYS_EVENTpbm_dwnkey'. (CODE=1017)
        public void dw_header_ue_enter_key(Keys key, uint keyflags) //**********************************************************************************************
        {
            GridElement dw_header = this.GetVisualElementById<GridElement>("dw_header");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.ue_enter_key
            //*Purpose:							Set Enter key as Tab key.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Keycode  				Key
            //*										Value			unsignedlong			Keyflags
            //*		
            //*Return:								None.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
            {
                f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_header);
            }
        }

        public async Task dw_header_itemchanged(object sender, EventArgs e) //**********************************************************************************************

        {
            int row = 0;
            var dwo = sender as ControlElement;
            string data = dwo.Text;
            if (sender is ComboBoxElement)
            {
                data = Convert.ToString(((ComboBoxElement)sender).SelectedText);

            }
           else if (sender is ComboGridElement)
            {
                data = Convert.ToString(((ComboGridElement)sender).Text);
                
            }

            ButtonElement cb_details = this.GetVisualElementById<ButtonElement>("cb_details");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.ItemChanged
            //*Purpose:							a) supplier_number - Check if exist.
            //*										b) supplier_name - Check if exist.
            //*										c) order_number - Check if exist.
            //*										d) if Supplier number and Order number are populated,
            //*											then retrieve documents and documents details.
            //*  
            //*Arguments:						
            //*Return:								
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************
            string ls_column = null;
            string ls_message = null;
            string ls_supplier_name = "";
            string ls_doc_type = null;
            int li_rtn_code = 0;
            long? ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_error_number = 0;
            ComboGridElement ldwc_order;
            //dw_header.PerformValidated(null);

            ls_column = Convert.ToString(dwo.ID);
            //Write into the log file

            ls_message = "*************************** Item Changed - dw_head*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + dwo.ID.ToString() + "\r" + "\n" + "Row:         " + row.ToString() + "\r" + "\n" + "The Data:  " + data + "\r" + "\n";
            //GalilCS - TODO : WriteToLog should be return int
            SystemHelper.WriteToLog(ls_message);
            if (li_rtn_code < 1)
            {
                //GalilCS - TODO : 2 Reject the data value but allow the focus to change
                //return 2;
            }
            switch (ls_column)
            {
                case "supplier_number":
                    if (data == "")
                    {
                        data = "0";
                    }
                    if (data != "")
                    {

                        //.. checks if supplier exists and get the supplier name
                        try
                        {
                            ll_supplier_number = Convert.ToInt64(data);
                        }
                        catch (Exception)
                        {
                            ll_supplier_number = 0;
                        }

                        ll_order_number = order_number.GetItemValue<long>(0, "order_number");
                        if (!(await this.uf_is_supplier_exist("NUMBER", ll_supplier_number, ls_supplier_name)).Retrieve(out ll_supplier_number, out ls_supplier_name))
                        {

                            supplier_name.SetItem(row, "supplier_name", "");
                            //this.ViewModel.uf_set_focus("dw_header", row, "order_number");
                            //return 0;
                        }
                        else
                        {

                            if (!isempty_stringClass.isempty_string(ls_supplier_name))
                            {

                                //supplier_name.SetItem(row, "supplier_name", Convert.ToString(ls_supplier_name));
                                supplier_name.Locked = true;
                                supplier_name.Text = Convert.ToString(ls_supplier_name);
                                supplier_name.SelectedText = Convert.ToString(ls_supplier_name);
                                supplier_name.Locked = false;
                                this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);

                            }
                            // Get supplier's orders
                            if (await this.uf_retrieve_supplier_order(ll_supplier_number) < 0)
                            {
                                //GalilCS - TODO : 2 Reject the data value but allow the focus to change
                                //return 2;
                            }
                            // AlexKh - 1.2.43.1 - 2009-03-30 - SPUCM00001084 - set order number immediately
                            //THIS.POST SetItem(row, "order_number", 0)
                            if (order_number.Repository()?.RowCount() > 0 || ll_supplier_number == 0)
                            {
                                order_number.SelectedText = "";
                                order_number.DroppedDown = false;
                                order_number.DroppedDown = true;
                                //order_number.SetItem(row, "order_number", "0");
                            }

                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(0);
                        }
                        // AlexKh - 1.1.31.0 - 2015-03-03 - SPUCM00005045 - Check if we need the log_book field

                        if (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Administration", "log_book_in_purchase_header", "yes", "Show the log_book field in the header") != "yes")
                        {

                            //dw_body.Columns["log_book"].Visible = false;
                            //dw_body.Columns["log_book_t"].Visible = false;
                        }
                    }
                    break;
                case "supplier_name":

                    ls_supplier_name = data;
                    if ((await this.uf_is_supplier_exist("NAME", ll_supplier_number, ls_supplier_name)).Retrieve(out ll_supplier_number, out ls_supplier_name))
                    {

                        //supplier_number.SetItem(0, "supplier_number", ll_supplier_number.ToString());
                        supplier_number.Text = Convert.ToString(ll_supplier_number);
                        ll_supplier_number = Convert.ToInt64(supplier_number.Text);
                        //supplier_name.SetItem(0, "supplier_name", Convert.ToString(ls_supplier_name));
                        supplier_name.Text = Convert.ToString(ls_supplier_name);
                        this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                        // Get supplier's orders
                        if (await this.uf_retrieve_supplier_order(ll_supplier_number) < 0)
                        {
                            //GalilCS - TODO : 2 Reject the data value but allow the focus to change
                            //return 2;
                        }
                    }
                    else
                    {
                        // Reset fields

                        supplier_name.SetItem(row, "supplier_name", "");

                        //dw_header.SetItem(row, "supplier_number", "");
                        supplier_number.Text = "";

                        this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(0);
                        // Galilcs - TODO: uf_set_focus
                        //this.ViewModel.uf_set_focus("dw_header", row, "supplier_number");
                        //return 0;
                    }
                    // AlexKh - 1.2.43.1 - 2009-03-30 - SPUCM00001084 - set order number immediately

                    //order_number.SetItem(row, "order_number", 0);
                    order_number.SelectedText = "";
                    order_number.DroppedDown = true;

                    this.ViewModel.iuo_order_searchProperty.uf_set_order_number(0);
                    break;
                case "order_number":

                    //.. checks if order exists in DB
                    if (data != "")
                    {
                        ll_order_number = Convert.ToInt64(data);
                        //ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
                        try
                        {
                            ll_supplier_number = Convert.ToInt64(supplier_number.Text);
                        }
                        catch (Exception)
                        {
                            ll_supplier_number = 0;
                        }


                        if (ll_order_number != 0)
                        {
                            if (!(await this.ViewModel.iuo_order_searchProperty.uf_order_for_purchase_exists(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_order_number, ll_error_number)).Retrieve(out ll_error_number))
                            {

                                order_number.Text = "";
                                order_number.Focus();
                                return;
                            }
                            else
                            {
                                this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_number);
                                // SharonS - 1.2.31.0 - 2008-04-21 - CR#1075 - Get order payment agreement
                                long ll_pay_agr = 0;
                                long ll_num_of_pays = 0;
                                if (this.ViewModel.iuo_order_searchProperty.uf_get_order_pay_agr(ll_pay_agr, ll_num_of_pays))
                                {
                                    dw_body.SetItem(0, "payment_code_number", ll_pay_agr);
                                    dw_body.SetItem(0, "payments_number", ll_num_of_pays);

                                }
                                // End
                            }
                        }
                    }
                    break;
                default:
                    //
                    break;
            }
            // If order number and supplier number are populated already, 
            // retrieve the related invoices and placking lists, and their items

            //ll_order_number = order_number.GetItemValue<long>(0, "order_number");
            ll_order_number = Convert.ToInt64(Convert.ToString(order_number.Text) == "" ? "0" : order_number.Text);
            //ll_order_number = 28185665;
            //ll_supplier_number = dw_header.GetItemValue<long>(0, "supplier_number");
            ll_supplier_number = Convert.ToInt64(supplier_number.Text == "" ? "0" : supplier_number.Text);

            if (ll_supplier_number != 0 && ll_order_number != 0)
            {
                // Galilcs - TODO: uf_retrieve_data
                if (!this.ViewModel.oneSelectedOrderNum)
                {
                    cb_details.Enabled = true;
                    li_rtn_code = await this.ViewModel.uf_retrieve_data(ll_supplier_number, ll_order_number);
                }
                else
                {
                    this.ViewModel.oneSelectedOrderNum = false;
                }

                if (li_rtn_code < 1)
                {
                    //GalilCS - TODO : 2 Reject the data value but allow the focus to change
                    //return 2;
                }

                if (dw_body.RowCount() == 0)
                {
                    // Galilcs - TODO: dw_body_ue_new_row
                    this.dw_body_ue_new_row();
                    // AlexKh - 1.2.43.1 - 2009-03-30 - SPUCM00001084 - disable button if no invoices exist
                    cb_details.Enabled = false;
                    if (this.ViewModel.oneSelectedOrderNum)
                        this.ViewModel.oneSelectedOrderNum = false;
                    else;
                    //this.ViewModel.oneSelectedOrderNum = true;
                }
                else
                {
                    // Galilcs - TODO: uf_set_focus
                    //this.ViewModel.uf_set_focus("dw_body", 1, "log_book");
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, ee) =>
                    {
                        dw_body.Focus();
                    }, delay: 300);
                }
                // AlexKh - 1.2.43.1 - 2009-03-30 - SPUCM00001084 - remove empty records in case order or supplier are not valid
            }
            else
            {

                if (dw_body.RowCount() > 0)
                {

                    dw_body.Reset();
                }
                cb_details.Enabled = false;
                // AlexKh - 2009-03-30 - End
                // Galilcs - TODO: uf_set_focus
                //this.ViewModel.uf_set_focus("dw_header", row, "order_number");
                // AlexKh - 2009-03-30 - End
            }
            // AlexKh - 1.1.1.6 - 2013-03-31 - SPUCM00004018 - insert empty row to prevent application crash
            if (ls_column != "order_number" && this.ViewModel.checkOneSetOreder != supplier_name.Text && supplier_number.Text != "")
            {
                order_number.DisplayMember = "order_number";
                order_number.ValueMember = "order_number";
                order_number.SetRepository(this.ViewModel.supplier_orderRepository);

                order_number.DroppedDown = true;
                order_number.Focus();
                this.ViewModel.checkOneSetOreder = supplier_name.Text;
                //ldwc_order.Focus();
            }
            if (this.ViewModel.supplier_orderRepository != null)
            {

                if (this.ViewModel.supplier_orderRepository.RowCount() == 0)
                {
                    order_number.Insert(0);
                }
            }

            //if (ldwc_order.RowCount() == 0)
            //{

            //    ldwc_order.Insert(0);
            //}
            //return 0;
        }

        // TODO: Could not add event handler for handler 'editchangedEvent'. (CODE=1017)
        public async Task<int> dw_header_editchanged(object sender, EventArgs e) //**********************************************************************************************
        {
            //
            int row = 0;
            int validation_num;
            var dwo = sender as ControlElement;
            ValueChangedArgs<string> args = e as ValueChangedArgs<string>;
            if (args == null) return 0;
            string data = args.Value;
            long ll_rows = 0;
            int order_number_Length = 7;
            int supplier_number_Length = 9;
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.EditChanged
            //*Purpose:							Validate that the supplier name does not contain empty strings.
            //*  
            //*Arguments:						
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*11-07-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************
            switch (dwo.ID)
            {
                case "order_number":
                    if (!int.TryParse(data, out validation_num))
                    {
                        order_number.Text = "";
                    }
                    if (order_number.Text.Length > supplier_number_Length)
                    {
                        order_number.Text = data.Substring(0, data.Length - 1);
                    }
                    break;
                case "order_name":
                    if (data.IndexOf("'") > 0)
                    {

                        //supplier_name.SetItem(0, "supplier_name", "");
                        supplier_name.Text = "";
                        return 2;
                    }
                    if (data.Length == 1)
                    {
                        //uf_calculate_declines

                        //(dwo).DDDW.ShowList = "Yes";

                        this.ViewModel.idwc_supplier_nameProperty = supplier_name;
                        //ll_rows = this.ViewModel.idwc_supplier_nameProperty.Retrieve(default(object));
                        if (ll_rows == 0 || ll_rows <= 0)
                        {
                            await this.ViewModel.uf_show_message("Database Error", "", "OK", "No rows retrieved.");
                            return 2;
                        }
                        // Galilcs TODO: Selects text in an edit control.
                        //dw_header.SelectText(2, 99);
                    }
                    break;
                case "supplier_number":
                    if (!int.TryParse(supplier_number.Text, out validation_num))
                    {
                        if (supplier_number.Text != null)
                        {
                            if (supplier_number.Text.Length > 0)
                            {
                                supplier_number.Text = supplier_number.Text.Substring(0, supplier_number.Text.Length - 1);
                            }
                        }
                    }
                    if (supplier_number.Text != null)
                    {
                        if (supplier_number.Text.Length > order_number_Length)
                        {
                            supplier_number.Text = supplier_number.Text.Substring(0, supplier_number.Text.Length - 1);
                        }
                    }
                    break;
            }
            return 0;
        }
        // TODO: Could not add event handler for handler 'itemerrorEvent'. (CODE=1017)
        public int dw_header_itemerror(int row, VisualElement dwo, string data) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.ItemError
            //*Purpose:							//Reject the data value with no message box
            //*  
            //*Arguments:						
            //*Return:								
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*11-07-2007     SHARP.					0.2.0 		B2B 			Initiation
            //************************************************************************************************

            //Reject the data value with no message box
            return 1;
        }
        // TODO: Could not add event handler for handler 'constructorEvent'. (CODE=1017)
        public void dw_header_constructor(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.Constructor
            //*Purpose:							Set a new row in the header DW.
            //*  
            //*Arguments:						
            //*Return:								
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            this.ViewModel.dw_header_ue_new_row();
        }
    }
}
