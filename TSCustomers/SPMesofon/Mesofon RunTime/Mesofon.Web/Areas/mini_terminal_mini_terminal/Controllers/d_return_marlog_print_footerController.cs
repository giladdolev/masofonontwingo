using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_return_marlog_print_footerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_return_marlog_print_footer()
		{
			d_return_marlog_print_footerRepository repository = new d_return_marlog_print_footerRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
