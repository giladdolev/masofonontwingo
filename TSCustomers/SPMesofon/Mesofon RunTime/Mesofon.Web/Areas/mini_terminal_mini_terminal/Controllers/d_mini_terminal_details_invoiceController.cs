using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_details_invoiceController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_details_invoice()
		{
			d_mini_terminal_details_invoiceRepository repository = new d_mini_terminal_details_invoiceRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
