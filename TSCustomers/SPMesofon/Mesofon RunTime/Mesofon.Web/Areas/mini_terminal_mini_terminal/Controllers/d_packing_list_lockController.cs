using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_packing_list_lockController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_packing_list_lock()
		{
			d_packing_list_lockRepository repository = new d_packing_list_lockRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
