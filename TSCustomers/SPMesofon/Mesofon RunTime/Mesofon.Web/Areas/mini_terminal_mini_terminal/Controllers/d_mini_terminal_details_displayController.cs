using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_details_displayController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_details_display()
		{
			d_mini_terminal_details_displayRepository repository = new d_mini_terminal_details_displayRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
