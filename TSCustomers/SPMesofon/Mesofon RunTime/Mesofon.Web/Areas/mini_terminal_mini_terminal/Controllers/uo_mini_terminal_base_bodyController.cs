using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofon;
using global;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
	public class uo_mini_terminal_base_bodyController : MvcSite.Common.SPBaseController
	{

		public ActionResult uo_mini_terminal_base_body()
		{
			return this.View(new uo_mini_terminal_base_body());
		}
		private uo_mini_terminal_base_body ViewModel
		{
			get { return this.GetRootVisualElement() as uo_mini_terminal_base_body; }
		}
		public async Task ue_diff_report() //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			ue_diff_report
			//*Purpose:							Print difference reports.
			//*  
			//*Arguments:						None 
			//*Return:								None 
			//*Date 			Programer				Version		Task#			Description
			//*-------------------------------------------------------------------------------------------------
			//*25-07-2007    SHARP.    				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			string ls_ret = null;
			string ls_params = null;
			long ll_branch_number = 0;
			long ll_supplier_number = 0;
			long ll_order_number = 0;
			long ll_ret = 0;
			ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            ll_ret = (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            ll_ret = (await this.ViewModel.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
			ls_params = ll_branch_number.ToString() + "@" + ll_supplier_number.ToString() + "@" + ll_order_number.ToString() + "@";
            var diff_report = await WindowHelper.Open<rw_diff_report_choose_type>("global_global");
            
			
			ls_ret = WindowHelper.GetParam<string>(diff_report);
			switch (ls_ret) {
				case "DOC":
                    await WindowHelper.Open<rw_diff_inv_mt_report>("global_global", "ls_params", ls_params, "FormType", "base_body");

                    break;
				case "ORDER":
                    await WindowHelper.Open<rw_diff_order_mt_report>("global_global", "ls_params", ls_params, "FormType", "base_body");

                    break;
				default:
					break;
			}
		}
		
		
		
		
		
		
		public bool uf_delete_null_values() //**********************************************************************************************
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			uf_delete_null_values
			//*Purpose:							Delete rows with empty or null barcode
			//*  
			//*Arguments:						None.
			//*Return:								Boolean: TRUE - Success 
			//*													FALSE - Otherwise
			//*DAte				Programer			Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*20-08-2007		SHARP.				0.2.0  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_row_cnt = 0;
			
			ll_row_cnt = dw_inv_pack_details.RowCount();
			
			
			if (dw_inv_pack_details.Find("barcode == null  OR barcode == \"\"", 0, dw_inv_pack_details.RowCount()) >= 0)
			{
				

				
				dw_inv_pack_details.SetFilter("barcode == null OR barcode = ''");
				
				dw_inv_pack_details.Filter();
                dw_inv_pack_details.RowsMove(1, dw_inv_pack_details.RowCount(), ModelBuffer.Primary, dw_inv_pack_details, Convert.ToInt32(dw_inv_pack_details.DeletedCount()) + 1, ModelBuffer.Delete);
				
				dw_inv_pack_details.SetFilter("");
				
				dw_inv_pack_details.Filter();
				this.ViewModel.il_row_noProperty -= 1;
				

			}
			return true;
		}
		
		public async Task<bool> uf_allow_change_doc_no(char ac_doc_type, long al_current_doc_no) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			uf_allow_change_doc_no
			//*Purpose:							Check if doc no can be changed.
			//*  										If only one item is assigned to the doc then a change is not allowed.
			//*
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*										--------------------------------------------------------------------------
			//*										Value			Character				ac_doc_type		
			//*										Value			Long						al_current_doc_no		
			//*
			//*Return:								Boolean: TRUE - The doc no can be changed 
			//*												 	FALSE - Otherwise
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*24-09-2007		SHARP.   				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
		    IRepository lds_mini_terminal_detail_item_packing_list;
		    IRepository lds_mini_terminal_detail_items_invoice;
			long ll_retrieved_rows = 0;
			long ll_branch_number = 0;
			long ll_supplier_number = 0;
			long ll_order_number = 0;
			ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
			(await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            (await this.ViewModel.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
            switch (ac_doc_type) {
				case 'I':
					lds_mini_terminal_detail_items_invoice = new d_mini_terminal_detail_items_invoiceRepository();
					
					
					
					ll_retrieved_rows = lds_mini_terminal_detail_items_invoice.Retrieve(ll_supplier_number, ll_branch_number, ll_order_number, al_current_doc_no);
					if (ll_retrieved_rows < 0)
					{
                        await this.ViewModel.uf_show_message("Retrieve Error", "", "OK", "Error in retrieving invoice items.");
						return false;
					}
					// SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
					if (lds_mini_terminal_detail_items_invoice != null)
					{


					}
					// End
					break;
				case 'P':
					lds_mini_terminal_detail_item_packing_list = new d_mini_terminal_detail_item_packing_listRepository();
					
					
					
					ll_retrieved_rows = lds_mini_terminal_detail_item_packing_list.Retrieve(ll_branch_number, ll_supplier_number, ll_order_number, al_current_doc_no);
					if (ll_retrieved_rows < 0)
					{
                        await this.ViewModel.uf_show_message("Retrieve Error", "", "OK", "Error in retrieving invoice items.");
						return false;
					}
					// SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
					if (lds_mini_terminal_detail_item_packing_list != null)
					{

					}
					// End
					break;
			}
			if (ll_retrieved_rows > 1)
			{
				return true;
			}
				return false;
			}
		public async Task<bool> uf_change_doc_no(char ac_doc_type, long al_new_doc_number, string as_barcode) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			uf_change_doc_no
			//*Purpose:							Change doc no for a material.
			//*
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*										--------------------------------------------------------------------------
			//*										Value			Character				ac_doc_type		
			//*										Value			Long						al_new_doc_number		
			//*										Value			String						as_barcode
			//*
			//*Return:								Boolean: TRUE - The doc no has been changed 
			//*												 	FALSE - Otherwise
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*23-09-2007		SHARP.    				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
		    IRepository lds_mini_terminal_details_pack_list_save;
		    IRepository lds_mini_terminal_details_invoice_save;
			long ll_retrieved_rows = 0;
			long ll_branch_number = 0;
			long ll_supplier_number = 0;
			long ll_order_number = 0;
			long ll_update_result = 0;
			long ll_material_number = 0;
			string ls_barcode = null;
			ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            (await this.ViewModel.uf_get_selected_order_number( ll_order_number)).Retrieve(out ll_order_number);
            ll_material_number =await this.ViewModel.iuo_material_searchProperty.uf_get_material_number(as_barcode);
			switch (ac_doc_type) {
				case 'I':
					lds_mini_terminal_details_invoice_save = new d_mini_terminal_details_invoice_saveRepository();
					
					
					
					ll_retrieved_rows = lds_mini_terminal_details_invoice_save.Retrieve(ll_branch_number, ll_supplier_number, ll_order_number, ll_material_number);
					if (ll_retrieved_rows < 0)
					{
                        await this.ViewModel.uf_show_message("Retrieve Error", "", "OK", "Error in retrieving invoice items.");
						return false;
					}
					 
					lds_mini_terminal_details_invoice_save.SetItem(0, "invoice_number", al_new_doc_number.ToString());
				    await f_begin_tranClass.f_begin_tran();
				    using (UnitOfWork unitOfWork = new UnitOfWork())
				    {
                        ll_update_result = lds_mini_terminal_details_invoice_save.Update(unitOfWork);
				    }
				    if (ll_update_result < 0)
					{
					    await f_rollbackClass.f_rollback();
                        await this.ViewModel.uf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון פריטי חשבונית נכשל"));
						return false;
					}
				    await f_commitClass.f_commit();
					// SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
					if (lds_mini_terminal_details_invoice_save != null)
					{


					}
					// End
					break;
				case 'P':
					lds_mini_terminal_details_pack_list_save = new d_mini_terminal_details_pack_list_saveRepository();
					
					
					
					ll_retrieved_rows = lds_mini_terminal_details_pack_list_save.Retrieve(ll_branch_number, ll_supplier_number, ll_order_number, ll_material_number);
					if (ll_retrieved_rows < 0)
					{
                        await this.ViewModel.uf_show_message("Retrieve Error", "", "OK", "Error in retrieving invoice items.");
						return false;
					}
					 
					lds_mini_terminal_details_pack_list_save.SetItem(0, "invoice_number", al_new_doc_number.ToString());
				    await f_begin_tranClass.f_begin_tran();
				    using (UnitOfWork unitOfWork = new UnitOfWork())
				    {
                        ll_update_result = lds_mini_terminal_details_pack_list_save.Update(unitOfWork);
				    }
				    if (ll_update_result < 0)
					{
					    await f_rollbackClass.f_rollback();
						await this.ViewModel.uf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון פריטי חשבונית נכשל"));
						return false;
					}
				    await f_commitClass.f_commit();
					// SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
					if (lds_mini_terminal_details_pack_list_save != null)
					{


					}
					// End
					break;
			}
			return true;
		}

       
		public bool uf_concatenate_values(ref long al_row, ref decimal al_quantity) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			uf_concatenate_values
			//*Purpose:							
			//*  
			//*Arguments:						None.
			//*Return:								BOOLEAN: TRUE  - Success 
			//*												   	  FALSE - Otherwise
			//*
			//*DAte				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*21-08-2007    SHARP.   				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			string ls_old_filter = null;
			string ls_old_sort = null;
			string ls_barcode = null;
			string ls_filter = null;
			string ls_find = null;
			decimal ldec_old_qty = default(decimal);
			decimal ldec_new_qty = default(decimal);
			long ll_rows = 0;
			long ll_row = 0;
			long ll_material_number = 0;
			int li_b2b_status = 0;
			int ll_ret = 0;
		
			return true;
			
		}
       
	
		public virtual int uf_get_doc_no_en(long al_row_no, long al_material_no) //MUST BE OVERRIDDEN
		{
			return 1;
		}
		public virtual int uf_set_quantity_and_doc_no(string as_expected_material_quantity, long al_row) //Must be overriden
		{
			return 1;
		}
		public virtual int uf_is_item_in_invoice(long al_doc_number, long al_material_number) //MUST BE OVERRIDDEN
		{
			return 0;
		}
      
		public async Task<bool> uf_check_material_main_supplier(long al_material_number, long al_supplier_number, long al_row, long al_distributor_number) //************************************************************************
		{
			//*Object:				uo_mini_terminal_base_body
			//*Function Name:	uf_check_material_main_supplier
			//*Purpose: 			validate material against main supplier and distributor
			//*Arguments: 		Long	 		- al_material_number	- material number
			//*						Long	 		- al_supplier_number	- supplier number
			//*						Long			- al_row
			//*Return:				Boolean 	- TRUE/FALSE
			//*Date				Programer		Version	Task#	 				Description
			//*--------------------------------------------------------------------------
			//*07-10-2010		AlexKh			1.2.46.3	SPUCM00002425 		Initial version
			//**************************************************************************
			
			long ll_supplier_number = 0;
			long ll_distributor_number = 0;
			long ll_invoice_distributor = 0;
			if (true)
			{
				ViewModel.LoadData(al_material_number, ref ll_supplier_number, ref ll_distributor_number);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{

                await this.ViewModel.uf_show_message("uo_mini_terminal_base_body.uf_check_material_main_supplier - Sql Error", "", "OK", masofonAlias.masofon.Instance.sqlca.SqlErrText);
				return false;
			}
			//Validate material against distributor
			ll_invoice_distributor = al_distributor_number;
			if (ll_invoice_distributor == 0)
			{
				ll_invoice_distributor = 0;
			}
			// Check if validation needed
			if (await f_get_parameter_valueClass.f_get_parameter_value("ord_items_by_distributor", "boolean", "true", "Validate item against distributor, True or False.", "gds_find") == "true")
			{
				if (ll_invoice_distributor != ll_distributor_number)
				{
					if (ll_invoice_distributor > 0)
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", " חומר " + al_material_number.ToString() + " אינו מוגדר למפיץ " + ll_invoice_distributor.ToString());
					}
					else if (ll_invoice_distributor == 0)
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", " חומר " + al_material_number.ToString() + " מוגדר למפיץ " + ll_invoice_distributor.ToString());
					}
					return false;
				}
			}
			//Check if validation needed
			if (await f_get_parameter_valueClass.f_get_parameter_value("ord_items_by_main_supplier", "boolean", "false", "Use main supplier instead last supplier, True or False.", "gds_find") == "false")
			{
				//	OR wf_check_supplier_as_branch(al_supplier_number) THEN // check if supplier as branch
				return true;
			}
			if (ll_supplier_number == al_supplier_number)
			{
				return true;
			}
			else
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", " חומר " + al_material_number.ToString() + " אינו שייך לספק ראשי מספר " + al_supplier_number.ToString());
				return false;
			}
		}
        
		
       
		public void destructor() //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			destructor
			//*Purpose:							Destroy all non visual objects.
			//*  
			//*Arguments:						None.
			//*
			//*Return:								Long.
			//*
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*14-08-2007		SHARP.					0.2.0  			B2B 			Initiation
			//************************************************************************************************
			
			if (this.ViewModel.ids_details_invoiceProperty != null)
			{


			}
			if (this.ViewModel.ids_details_pack_listProperty != null)
			{


			}
			if (this.ViewModel.ids_details_invoice_saveProperty != null)
			{


			}
			if (this.ViewModel.ids_details_pack_list_saveProperty != null)
			{


			}
			if (this.ViewModel.ids_inv_pack_detailsProperty != null)
			{



			}
			if (this.ViewModel.iuo_material_searchProperty != null)
			{



			}
		}


        public async Task cb_delete_item_clicked(object sender, EventArgs e)
        {
            await this.ViewModel.uf_delete_item(sender, e);
        }

        public async Task cb_show_all_clicked(object sender, EventArgs e) //**********************************************************************************************
		{
			ButtonElement cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			cb_show_all.clicked event
			//*Purpose:							Set the text of the button to:"Show all Items" if it was "Show items with differences" and the opposite. 
			//*										Also retrieve dw_inv_pack_details.
			//*  
			//*Arguments:						None.
			//*Return:								None.
			//*Date 			Programer		Version		Task#			Description
			//*-----------------------------------------------------------------------------------------------------------------------
			//*17-07-2007    SHARP.   		0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			//Set the text of the button
			if (cb_show_all.Text == "הצג את כל הפריטים")
			{
                //Show all items
                await this.ViewModel.uf_show_items(true);
			}
			else
			{
                //Show diff items
                await this.ViewModel.uf_show_items(false);
			}
		}
		
		public async Task cb_def_report_clicked(object sender, EventArgs e) //**********************************************************************************************
		{
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			cb_def_report.clicked event
            //*Purpose:							Display difference reports.
            //*  
            //*Arguments:						
            //*Return:								None.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-08-2007		SHARP.   			0.2.0  		B2B 			Initiation
            //************************************************************************************************

		    await ue_diff_report();
		}

        public async Task cb_finish_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish the order items input.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-08-2007		SHARP.			    	0.2.0  		B2B 			Initiation
            //************************************************************************************************

            await this.ViewModel.uf_finish_input();
        }

        public virtual void cb_return_to_header_clicked(object sender, EventArgs e) //MUST BE OVERRIDDEN
		{
		}
		
		public async Task<int> dw_inv_pack_details_ue_enter(GridElement dw_inv_pack_details, GridElementCellEventArgs e) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			dw_inv_pack_details.event ue_enter
			//*Purpose:
			//*
			//*Arguments:						Pass By			Argument Type 		Argument Name
			//*										-------------------------------------------------------------------------------
			//*										Value 			Keycode 				Key
			//*										Value				Unsignedlong 			Keyflags
			//*									
			//*Return:								Long.
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-08-2007		SHARP.					0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			string ls_column = null;
			long ll_row = 0;
			if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
			{
                ls_column = e.WidgetDataMember;
                ll_row = e.RowIndex;
                if (e.RowIndex != -1)
                {
                    await this.ViewModel.uf_enter(ls_column, ll_row);
                }
			}
			return 1;
		}

       
        public int dw_inv_pack_details_ue_new_row() //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			dw_inv_pack_details.ue_new_row
			//*Purpose:							Insert new row into the DataWindow.
			//*  
			//*Arguments:						Pass By			Argument Type		Argument Name		
			//*										--------------------------------------------------------------------------
			//*										Value				Long					al_doc_number
			//*
			//*Return:								Integer: 1 - Success, -1 - Failed.
			//*Date 			     Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-08-2007  		SHARP.  				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			//RETURN PARENT.uf_new_row()
			return 1;
		}
		
		public async Task<int> dw_inv_pack_details_doubleclicked(int xpos, int ypos, int row, GridElement dw_inv_pack_details, string dwoID) //**********************************************************************************************
		{
			//*Object:							uo_mini_terminal_base_body
			//*Function/Event  Name:		dw_inv_pack_details.DoubleClicked
			//*Purpose:						Insert material expiration date.
			//*  
			//*Description:					Display the Expiration date window to the user, 
			//*									and save the data in the correct table.
			//*									 
			//*Return:							Long
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*25-07-2007		SHARP.    				0.2.0  			B2B 			Initiation
			//************************************************************************************************
			
			char lc_doc_type = '\0';
			long ll_min_valid_month = 0;
			long ll_branch_number = 0;
			long ll_doc_no = 0;
			long ll_supplier_number = 0;
			long ll_material_number = 0;
			long ll_serial_number = 0;
			long? ll_doc_quantity = 0;
			long ll_actual_quantity = 0;
			DateTime? ldt_curr_expiration_date = null;
			if (row >= 0)
			{
				// Only if the user doubleclicked the expected_material_quantity field.
				
				if (Convert.ToString(dwoID) == "expected_material_quantity")
				{
					
					ldt_curr_expiration_date = dw_inv_pack_details.GetItemValue<DateTime?>(row, "expiration_date");
					
					ll_min_valid_month = dw_inv_pack_details.GetItemValue<long>(row, "min_valid_months");
					// Only if the material has minimum valid months, it should have expiration date
					if (!(ll_min_valid_month == 0 || ll_min_valid_month == 0))
					{
                            if (!((await this.ViewModel.uf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, row, false, dw_inv_pack_details))).Retrieve(out ldt_curr_expiration_date))
                            {
							return -1;
						}
						else
						{
							// Set the expiration date in the DW
							
							dw_inv_pack_details.SetItem(row, "expiration_date", ldt_curr_expiration_date.ToString());
							
							ll_branch_number = dw_inv_pack_details.GetItemValue<long>(row, "branch_number");
                            (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
							
							ll_doc_no = dw_inv_pack_details.GetItemValue<long>(row, "doc_no");
							lc_doc_type = this.ViewModel.uf_get_doc_type(ll_doc_no).Result;
							
							ll_material_number = dw_inv_pack_details.GetItemValue<long>(row, "material_number");
							if (lc_doc_type.ToString() == "I")
							{
								ViewModel.UpdateData(ldt_curr_expiration_date, ll_branch_number, ll_supplier_number, ll_doc_no, ll_material_number);
								
								if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
								{

                                    await this.ViewModel.uf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
									return 0;
								}
							}
							else if (lc_doc_type.ToString() == "P")
							{
								ViewModel.UpdateData1(ldt_curr_expiration_date, ll_branch_number, ll_supplier_number, ll_doc_no, ll_material_number);
								
								if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
								{

                                    await this.ViewModel.uf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "OK",await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
									return 0;
								}
							}
						}
					}
				}
			}
			// SharonS - 1.2.26.8 - 2008-03-18 - Only if ib_send_decline_msg is BOTH or DB
			if (this.ViewModel.is_send_decline_msgProperty == "DB_MSG" || this.ViewModel.is_send_decline_msgProperty == "DB")
			{
				//
				// PninaSG - 1.2.23.0 - 2008-01-14 - TASK#10003 - open decline for doc window
				long ll_order_number = 0;
				long ll_mini_terminal = 0;
				long ll_decline_exist = 0;
				s_array_arguments lstr_mess = new s_array_arguments();
				ModelAction ldw_row_status = default(ModelAction);
				
				
				ldw_row_status = dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);
				if (ldw_row_status != ModelAction.Insert)
				{
					//IF dwo.ID = "expected_material_quantity" THEN
					
					if (Convert.ToString(dwoID) == "barcode")
					{
						ll_mini_terminal = dw_inv_pack_details.GetItemValue<long>(row, "mini_terminal");
						ll_material_number = Convert.ToInt64(dw_inv_pack_details.GetItemValue<long>(row, "material_number"));
						ll_serial_number = dw_inv_pack_details.GetItemValue<long>(row,"serial_number");
						ll_doc_quantity = (long)dw_inv_pack_details.GetItemValue<decimal?>(row, "invoice_pack_quantity");
						ll_actual_quantity = (long)dw_inv_pack_details.GetItemValue<decimal>(row, "expected_material_quantity");
						if (ll_mini_terminal == 1)
						{
                            (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
                            (await this.ViewModel.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
                            ll_doc_no = dw_inv_pack_details.GetItemValue<long>(row, "doc_no");
							lc_doc_type = this.ViewModel.uf_get_doc_type(ll_doc_no).Result;
							lstr_mess.a_string[1] = lc_doc_type.ToString(); //parent_doc_type 
							// SharonS - 1.2.26.8 - 2008-03-18 - Change decline_level
																//decline_level
							lstr_mess.a_long[1] = 1; //decline_level 
							//
							lstr_mess.a_long[2] = ll_order_number; //order_number 
							lstr_mess.a_long[3] = ll_supplier_number; //supplier_number 
							lstr_mess.a_long[4] = ll_doc_no; //parent_doc_number
							lstr_mess.a_long[5] = ll_material_number; //material_number 
							lstr_mess.a_long[6] = this.ViewModel.il_employee_numberProperty; //employee_number
							lstr_mess.a_long[7] = ll_serial_number; //serial_number
							lstr_mess.a_long[8] = ll_doc_quantity.Value; //doc_quantity
							lstr_mess.a_long[9] = ll_actual_quantity; //actual_quantity
							// SharonS - 1.2.42.0 - 2009-01-19 - CR#1100 - SPUCM00000115 - Send distributor_number 
							lstr_mess.a_long[10] = dw_inv_pack_details.GetItemValue<long>(row, "distributor_number");
                            // End
                            rw_declines_reasons frm =  await WindowHelper.Open<rw_declines_reasons>("global_global", "args", lstr_mess);
                            ll_decline_exist = frm.ll_decline_exist_return;
                            if (ll_decline_exist == 1)
							{
								dw_inv_pack_details.SetItem(row,"declines",1);
							}
							else
							{
								dw_inv_pack_details.SetItem(row, "declines", 0);
							}
                        }
					}
				}
				//
				// SharonS - 1.2.26.8 - 2008-03-18 - Only if ib_send_decline_msg is BOTH or DB
			}

            var wgrid = dw_inv_pack_details as WidgetGridElement;
            if(wgrid != null)
            {
                wgrid.SetFocus(row, dwoID);
            }
            //
            return 0;
		}
		
		public void dw_inv_pack_details_constructor(GridElement dw_inv_pack_details,object sender, EventArgs e) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			dw_inv_pack_details.constructor
			//*Purpose:							Set the position of Horizonal-Scroll. 
			//*  
			//*Arguments:						
			//*Return:								
			//*Date 				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*14-08-2007			SHARP.					0.2.0 		B2B 			Initiation
			//************************************************************************************************
			
			//Set the position of Horizonal-Scroll. 
			
			 
			
			 
			
			dw_inv_pack_details.set_HorizontalScrollPosition(dw_inv_pack_details.get_HorizontalScrollMaximum());
		}
		
		public void dw_inv_pack_details_losefocus(GridElement dw_inv_pack_details,object sender, EventArgs e) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			dw_inv_pack_details.event losefocus
			//*Purpose:							AcceptText. 
			//*  
			//*Arguments:						None.
			//*Return:								Long.				
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*14-08-2007		SHARP.					0.2.0  		B2B 			Initiation
			//************************************************************************************************
			
			
			dw_inv_pack_details.PerformValidated(e);
		}
		
		public async Task dw_inv_pack_details_clicked(int xpos, int ypos, int row, ControlElement dwo) //**********************************************************************************************
		{
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			dw_inv_pack_details.clicked
            //*Purpose:							Update the Document numbers DropDownListBox.
            //*  
            //*Arguments:						
            //*Return:								Long.
            //*Date 				Programer				Version		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*-2007			   SHARP.					0.2.0 			B2B 		Initiation
            //************************************************************************************************

            await this.ViewModel.uf_build_doc_number_list();
		}
		
		public void dw_inv_pack_details_editchanged(object sender, GridCellEventArgs e) // AlexKh - 1.2.44.1 - 2009-05-11 - SPUCM00001251 - calculate time for barcode insertion
		{
			long ll_temp_start = 0;
			long ll_used = 0;
			string ls_string = null;
			
			if (Convert.ToString(e.DataIndex) == "barcode")
			{
				if (!string.IsNullOrEmpty(e.Value) &&  Convert.ToString(e.Value).Length == 1)
				{
                   
                    this.ViewModel.il_start_timeProperty = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;
                    this.ViewModel.is_start_timeProperty = DateTime.Today.ToString("yyMMdd hh:mm:ss.fff");
				}
			}
		}
		
		public void dw_inv_pack_details_rowfocuschanging(int l1, int l2) //**********************************************************************************************
		{
			//*Object:								uo_mini_terminal_base_body
			//*Function/Event  Name:			dw_inv_pack_details.rowfocuschanging
			//*Purpose:							In case a change was done save it.
			//*
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*										--------------------------------------------------------------------------
			//*										Value			Long						currentrow
			//*										Value			Long						newrow
			//*
			//*Return:								Long							
			//*Date 			    Programer			Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*15-10-2007		    SHARP.				0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			//return 0;
		}
	}
}
