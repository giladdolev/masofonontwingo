﻿using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using global;
using System.Web.VisualTree.Elements;
using System.Globalization;
using System.IO;
using masofon;
using System.Drawing;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using System.Web.VisualTree.Extensions;
using System.Extensions;
using Mesofon.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class w_mini_terminalController : MvcSite.Common.SPBaseController
    {


        public ActionResult w_mini_terminal(decimal qty = 0)
        {
            Globals.BranchNumber = Globals.BranchNumberState;
            Globals.StationNumber = Globals.StationNumberState;
            Globals.UserRegistryItems = Globals.UserRegistryItemsState;
            Globals.UserID = Globals.UserIDState;
            Globals.EmployeeNumber = Convert.ToString(Globals.EmployeeNumberState);
            Globals.Password = Globals.PasswordState;
            Globals.DeviceID = Globals.DeviceIDState;
            Globals.QueryString = Globals.QueryStringState;
            Globals.PrinterName = Globals.PrinterNameState?.Replace('/','\\');
            Globals.BarcodePrinterName = Globals.BarcodePrinterNameState?.Replace('/', '\\');
            masofonAlias.masofon.Instance.open();

            
            return this.View(new w_mini_terminal(qty));
        }
        private w_mini_terminal ViewModel
        {
            get { return this.GetRootVisualElement() as w_mini_terminal; }
        }

        public async Task w_mini_terminal_WindowClosing(object sender, EventArgs e )
        {
            await this.ViewModel.UnlockInvoices();
        }

        public async Task Form_Load(object sender, EventArgs e)
        {
            //      await this.ViewModel.open();
            var LblTitle = this.GetVisualElementById<LabelElement>("LblTitle");
            string database = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            System.Text.RegularExpressions.Match databaseMatch = System.Text.RegularExpressions.Regex.Match(database, @"database=([A-Za-z0-9_]+)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            string strVersion = System.Configuration.ConfigurationManager.AppSettings["Version"];
            if (databaseMatch.Success)
            {
                var db = databaseMatch.Value.Split('=')[1];
                LblTitle.Text = string.Format("סניף : {0}	-תחנה: {1}	-סביבה: {2}	-גרסה: {3}", Globals.BranchNumber, Globals.StationNumber, db, strVersion);
            }
            
            //await this.ViewModel.open();
        }

        public async Task Form_AfterRender(object sender, EventArgs e)
        {
            await this.ViewModel.open();
        }
    }
}
