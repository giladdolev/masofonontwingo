using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using global;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;
using MvcSite.Common;
using System.Web.VisualTree;

namespace Mesofon.MiniTerminal.Controllers
{
	public class rw_validate_cartonController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_validate_carton(string barcode)
		{
            return this.View(new rw_validate_carton(barcode));
		}
		private rw_validate_carton ViewModel
		{
			get { return this.GetRootVisualElement() as rw_validate_carton; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void ue_close()
		{
			if (isempty_stringClass.isempty_string(this.ViewModel.is_carton_barcode_scannedProperty))
			{
				this.ViewModel.is_carton_barcode_scannedProperty = "";
			}
			
			WindowHelper.Close(this.ViewModel, Convert.ToString(this.ViewModel.is_carton_barcode_scannedProperty));
		}
		//public int uf_resize() // AlexKh - function for dynamic resize of the window and all it's elementes
		//{

		//    int ii_WinBolderWidth;
		//    int ii_WinBolderHeight;
		//    double Radio = WindowHelper.GetRadio(this.ViewModel, out ii_WinBolderWidth, out ii_WinBolderHeight);

		//	if (Radio == 1.0) // if the screen is default 800 * 600
		//	{
		//		return 0;
		//	}
		//	//AlexKh - decrease zoom to 90%
		//	Radio = Convert.ToInt64(Radio * 0.9);
			
		//	this.ViewModel.Hide();
		//	this.ViewModel.PixelWidth = (int)((this.ViewModel.PixelWidth - ii_WinBolderWidth) * Radio + ii_WinBolderWidth);
		//	this.ViewModel.PixelHeight = (int)((this.ViewModel.PixelHeight - ii_WinBolderHeight) * Radio + ii_WinBolderHeight);
		//	int i = 0;
		//	ControlElement temp = default(ControlElement); // used to check a variety of control
		//	for (i = 1; i <= CollectionExtensions.UBound(this.ViewModel.Controls); i++)
		//	{
		//		temp = this.ViewModel.Controls[i]; // adjust the size, location
		//		temp.PixelWidth = (int)(temp.PixelWidth * Radio);
		//		temp.PixelLeft = (int)(temp.PixelLeft * Radio);
		//		temp.PixelTop = (int)(temp.PixelTop * Radio);
		//		temp.PixelHeight = (int)(temp.PixelHeight * Radio);
		//		if (temp.GetType() == typeof(TabElement))
		//		{
		//			TabElement mtab = default(TabElement);
		//			mtab = temp as TabElement;
		//			mtab.Font = new Font(mtab.Font.FontFamily, (float)Math.Abs(mtab.Font.Size * Radio)); // set the font
		//		}
		//		else if (temp.GetType() == typeof(ButtonElement))
		//		{
		//			ButtonElement cb = default(ButtonElement);
		//			cb = temp as ButtonElement;
		//		    cb.Font = new Font(cb.Font.FontFamily, (float)(cb.Font.Size * Radio));
		//		}
		//		else if (temp.GetType() == typeof(TextBoxElement))
		//		{
		//			TextBoxElement sle = default(TextBoxElement);
		//			sle = temp as TextBoxElement;
		//			sle.Font = new Font(sle.Font.Name, (float)(sle.Font.SizeInPoints * Radio), sle.Font.Style, GraphicsUnit.Point, sle.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(MaskedTextBoxElement))
		//		{
		//			MaskedTextBoxElement em = default(MaskedTextBoxElement);
		//			em = temp as MaskedTextBoxElement;
		//			em.Font = new Font(em.Font.Name, (float)(em.Font.SizeInPoints * Radio), em.Font.Style, GraphicsUnit.Point, em.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(LabelElement))
		//		{
		//			LabelElement st = default(LabelElement);
		//			st = temp as LabelElement;
		//			st.Font = new Font(st.Font.Name, (float)(st.Font.SizeInPoints * Radio), st.Font.Style, GraphicsUnit.Point, st.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(GridElement))
		//		{
		//			// datawindows get zoomed
		//			GridElement DW = default(GridElement);
		//			DW = temp as GridElement;
					 
		//			//DW.set_Zoom(Convert.ToInt32(Math.Round(Radio * 100)).ToString()); // Note DATAWINDOW different with other controls
		//		}
		//		else if (temp.GetType() == typeof(ButtonElement))
		//		{
		//			ButtonElement pb = default(ButtonElement);
		//			pb = temp as ButtonElement;
  //                  pb.Font = new Font(pb.Font.FontFamily, (float)(pb.Font.Size * Radio));
		//		}
		//		else if (temp.GetType() == typeof(CheckBoxElement))
		//		{
		//			CheckBoxElement cbx = default(CheckBoxElement);
		//			cbx = temp as CheckBoxElement;
		//			cbx.Font = new Font(cbx.Font.Name, (float)(cbx.Font.SizeInPoints * Radio), cbx.Font.Style, GraphicsUnit.Point, cbx.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(ComboBoxElement))
		//		{
		//			ComboBoxElement ddlb = default(ComboBoxElement);
		//			ddlb = temp as ComboBoxElement;
		//			ddlb.Font = new Font(ddlb.Font.Name, (float)(ddlb.Font.SizeInPoints * Radio), ddlb.Font.Style, GraphicsUnit.Point, ddlb.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(RichTextBox))
		//		{
		//			GroupBoxElement gb = default(GroupBoxElement);
		//			gb = temp as GroupBoxElement;
		//			gb.Font = new Font(gb.Font.FontFamily, (float)Math.Abs(gb.Font.Size * Radio));
		//		}
		//		else if (temp.GetType() == typeof(ListBoxElement))
		//		{
		//			ListBoxElement lb = default(ListBoxElement);
		//			lb = temp as ListBoxElement;
		//			lb.Font = new Font(lb.Font.Name, (float)(lb.Font.SizeInPoints * Radio), lb.Font.Style, GraphicsUnit.Point, lb.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(TextBoxElement))
		//		{
		//			TextBoxElement mle = default(TextBoxElement);
		//			mle = temp as TextBoxElement;
		//			mle.Font = new Font(mle.Font.Name, (float)(mle.Font.SizeInPoints * Radio), mle.Font.Style, GraphicsUnit.Point, mle.Font.GdiCharSet);
		//		}
		//		else if (temp.GetType() == typeof(RadioButtonElement))
		//		{
		//			RadioButtonElement rb = default(RadioButtonElement);
		//			rb = temp as RadioButtonElement;
		//			rb.Font = new Font(rb.Font.FontFamily, (float)Math.Abs(rb.Font.Size * Radio));
		//		}
		//	}
			 
		//	this.ViewModel.Show();
		//	return 0;
		//}
		public void close()
		{
			ue_close();
		}
		public void open()
		{
            TextBoxElement sle_carton = this.GetVisualElementById<TextBoxElement>("sle_carton");
            this.ViewModel.is_cartone_barcode_receivedProperty = this.ViewModel.Barcode;
            Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
            {
                sle_carton.Focus();
            }, delay: 120);
            //uf_resize();
        }
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
			this.ViewModel.is_carton_barcode_scannedProperty = "";
			 
			
			WindowHelper.Close(this.ViewModel as WindowElement, Convert.ToString(this.ViewModel.is_carton_barcode_scannedProperty));
		}
        public void enter_click(object sender, KeyEventArgs e)
        {
            if (((KeyEventArgs)e).KeyCode == Keys.Enter || ((KeyEventArgs)e).KeyCode == Keys.Tab)
            {
                enterKeyFlag = true;
                enter_TextChanged(sender, e);
            }

        }
        public static bool enterKeyFlag { get; private set; }
        public void enter_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_carton");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_scan_line.Text != "")
                {
                    cb_ok_clicked(sender, e);
                    enterKeyFlag = false;
                }
            }

        }
        
        public void cb_ok_clicked(object sender, EventArgs e)
		{
			TextBoxElement sle_carton = this.GetVisualElementById<TextBoxElement>("sle_carton");

            this.ViewModel.is_carton_barcode_scannedProperty = sle_carton.Text;
			if (isempty_stringClass.isempty_string(this.ViewModel.is_carton_barcode_scannedProperty))
			{
				this.ViewModel.is_carton_barcode_scannedProperty = "";
			}
			if (this.ViewModel.is_carton_barcode_scannedProperty != this.ViewModel.is_cartone_barcode_receivedProperty)
			{
				this.ViewModel.is_carton_barcode_scannedProperty = "0";
			}
			 
			
			WindowHelper.Close(this.ViewModel as WindowElement, Convert.ToString(this.ViewModel.is_carton_barcode_scannedProperty));
		}
	}
}
