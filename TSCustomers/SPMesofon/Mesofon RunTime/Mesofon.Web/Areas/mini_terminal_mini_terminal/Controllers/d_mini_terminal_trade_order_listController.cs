using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_trade_order_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_trade_order_list()
		{
			d_mini_terminal_trade_order_listRepository repository = new d_mini_terminal_trade_order_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
