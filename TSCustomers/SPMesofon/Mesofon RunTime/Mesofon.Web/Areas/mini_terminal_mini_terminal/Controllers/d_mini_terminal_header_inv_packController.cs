using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_header_inv_packController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_header_inv_pack()
		{
			d_mini_terminal_header_inv_packRepository repository = new d_mini_terminal_header_inv_packRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
