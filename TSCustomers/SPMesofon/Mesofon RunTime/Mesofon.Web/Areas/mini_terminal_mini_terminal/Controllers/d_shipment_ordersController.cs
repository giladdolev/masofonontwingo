using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_shipment_ordersController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_shipment_orders()
		{
			d_shipment_ordersRepository repository = new d_shipment_ordersRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
