using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Drawing;
using System.Extensions;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;

namespace Mesofon.MiniTerminal.Controllers
{
	public class rw_print_copies_numberController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_print_copies_number(int rowsCount)
		{
            return this.View(new rw_print_copies_number(rowsCount));
		}
		private rw_print_copies_number ViewModel
		{
			get { return this.GetRootVisualElement() as rw_print_copies_number; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void enterdown()
		{
			wf_handle();
		}
		public void wf_handle() //*******************************************************************
		{
			TextBoxElement sle_copies_number = this.GetVisualElementById<TextBoxElement>("sle_copies_number");
			//*Object:				rw_print_copies_number
			//*Function Name:	wf_handle
			//*Purpose: 			handle copies number
			//*Arguments: 		None
			//*Return:				None
			//*Date				Programer		Version		Task#	 			Description
			//*-------------------------------------------------------------------
			//*07-11-2011		AlexKh			1.2.48.0.13	SPUCM00002930	Initial version
			//*******************************************************************
			
			if (!isempty_stringClass.isempty_string(sle_copies_number.Text))
			{
				
				WindowHelper.Close(this.ViewModel, Convert.ToString(sle_copies_number.Text));
			}
			else
			{
				
				WindowHelper.Close(this.ViewModel, "0");
			}
		}
	
		public void open()
		{
			TextBoxElement sle_copies_number = this.GetVisualElementById<TextBoxElement>("sle_copies_number");
			long ll_copies = 0;
					
			ll_copies = (long)WindowHelper.GetParam<double>(this.ViewModel);;
			if (ll_copies > 0)
			{
				sle_copies_number.Text = ll_copies.ToString();
			}
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
			WindowHelper.Close<string>(this.ViewModel as WindowElement, "0");
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			wf_handle();
		}
	}
}
