using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_packing_list_moveController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_packing_list_move()
		{
			d_packing_list_moveRepository repository = new d_packing_list_moveRepository();
			
			return this.View(repository);
		}
	}
}
