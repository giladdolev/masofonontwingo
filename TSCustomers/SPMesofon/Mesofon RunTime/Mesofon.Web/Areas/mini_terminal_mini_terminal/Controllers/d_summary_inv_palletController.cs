using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_summary_inv_palletController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_summary_inv_pallet()
		{
			d_summary_inv_palletRepository repository = new d_summary_inv_palletRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
