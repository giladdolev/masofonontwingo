using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_supplier_order_moveController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_supplier_order_move()
		{
			d_sql_supplier_order_moveRepository repository = new d_sql_supplier_order_moveRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
