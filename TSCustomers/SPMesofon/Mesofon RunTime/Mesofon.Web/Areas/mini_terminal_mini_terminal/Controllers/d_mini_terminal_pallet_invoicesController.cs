using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_pallet_invoicesController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_pallet_invoices()
		{
			d_mini_terminal_pallet_invoicesRepository repository = new d_mini_terminal_pallet_invoicesRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
