using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_detail_items_invoiceController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_detail_items_invoice()
		{
			d_mini_terminal_detail_items_invoiceRepository repository = new d_mini_terminal_detail_items_invoiceRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
