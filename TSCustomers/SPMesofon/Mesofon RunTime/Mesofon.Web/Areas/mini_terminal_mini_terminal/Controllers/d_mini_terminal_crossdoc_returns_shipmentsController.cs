using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_crossdoc_returns_shipmentsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_crossdoc_returns_shipments()
		{
			d_mini_terminal_crossdoc_returns_shipmentsRepository repository = new d_mini_terminal_crossdoc_returns_shipmentsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
