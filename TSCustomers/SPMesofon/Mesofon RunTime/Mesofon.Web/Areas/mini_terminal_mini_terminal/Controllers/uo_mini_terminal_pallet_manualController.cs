﻿using System;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using global;
using masofon;
using Mesofon.Repository;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Data;
using Mesofon.MiniTerminal.Controllers;
using Mesofon.Models;
using System.Data;
using System.Extensions;
using MvcSite.Common;
using System.Threading;
using System.IO;

namespace Mesofon.Controllers
{
    public class uo_mini_terminal_pallet_manualController : uo_mini_terminal_base_bodyController
    {

        public ActionResult uo_mini_terminal_pallet_manual()
        {
            return this.View(new uo_mini_terminal_pallet_manual());
        }
        private uo_mini_terminal_pallet_manual ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_pallet_manual; }
        }

        public async Task Form_Load(object sender, EventArgs e)
        {
            await this.ViewModel.constructor();
            dw_inv_pack_details_constructor(null, null);

        }
        public void dw_inv_pack_details_constructor(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			dw_inv_pack_details.constructor
            //*Purpose:							Set the position of Horizonal-Scroll.
            //*Arguments:							None
            //*Return:								None
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************
            //(this.ViewModel.dw_inv_pack_details as GridElement).constructor();
            //this.ViewModel.dw_inv_pack_details.set_HorizontalScrollPosition(this.ViewModel.dw_inv_pack_details.get_HorizontalScrollMaximum());
            this.ViewModel.ids_mini_terminal_pallet_invoicesProperty = new d_mini_terminal_pallet_invoicesRepository();


            if (!(this.ViewModel.invuo_doc_item_searchProperty != null))
            {
                this.ViewModel.invuo_doc_item_searchProperty = new nvuo_pallet_item_search();
            }
            this.ViewModel.invuo_doc_item_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.ViewModel.iuo_order_searchProperty = new nvuo_pallet_orders_search();
            this.ViewModel.iuo_order_searchProperty.uf_set_mt_message(true);
            this.ViewModel.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);

            this.ViewModel.dw_inv_pack_details.SetRepository(new d_mini_terminal_details_displayRepository(), false, true);
            this.ViewModel.dw_summary.SetRepository(new d_summery_inv_packRepository(), false);



        }

        public async Task<bool> uf_update_item(int al_row_no)
        {
            ButtonElement cb_item_diff = this.GetVisualElementById<ButtonElement>("cb_item_diff");
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_update_item
            //*Purpose:							Update item details
            //*Arguments:							Long	al_row_no
            //*Return:								Boolean
            //*Date 			Programer		Version		Task#			Description
            //*---------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //*********************************************************************

            bool lb_status = false;
            string ls_barcode = null;
            string ls_material_name = null;
            string ls_find = null;
            string ls_find_dw = null;
            string ls_error_text = null;
            string ls_last_sql_err_text = null;
            string ls_details = null;
            decimal? ld_expected_material_quantity = default(decimal);
            decimal ld_expected_material_quantity_temp = 0;
            decimal? ld_order_quantity = default(decimal);
            decimal ld_material_price = default(decimal);
            decimal ld_material_price_after_discount = default(decimal);
            decimal ld_material_discount_percent = default(decimal);
            decimal ld_sell_price = default(decimal);
            decimal ld_bonus_quantity = default(decimal);
            decimal ld_inv_pack_quantity = default(decimal);
            decimal ld_material_quantity = default(decimal);
            decimal ld_expected_material_quantity_diff = default(decimal);
            long? ll_doc_no = 0;
            long ll_rows_count = 0;
            long? ll_found = 0;
            long? ll_found_dw = 0;
            long ll_rtn_code = 0;
            long ll_supplier_number = 0;
            long ll_order_num = 0;
            long ll_material_number = 0;
            long ll_error_number = 0;
            long ll_last_sql_db_code = 0;
            long ll_min_valid_month = 0;
            long ll_order_material_color = 0;
            long ll_distributor_number = 0;
            long ll_serial_number = 0;
            DateTime? ldt_curr_expiration_date = null;
            int li_b2b_status = 0;
            ModelAction ldwis_doc = default(ModelAction);
            //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_update_item", "Start.", Convert.ToString("Row:      " + al_row_no.ToString()), "");


            ldwis_doc = this.ViewModel.dw_inv_pack_details.GetItemStatus(al_row_no, 0, ModelBuffer.Primary);

            ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row_no, "barcode");

            ls_material_name = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row_no, "material_name");

            ld_expected_material_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal?>(al_row_no, "expected_material_quantity");

            ld_order_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal?>(al_row_no, "order_quantity");
            //ld_invoice_pack_quantity			=	dw_inv_pack_details.GetItem<int>(al_row_no, "invoice_pack_quantity")

            ll_doc_no = this.ViewModel.dw_inv_pack_details.GetItemValue<long?>(al_row_no, "doc_no");

            li_b2b_status = (int)this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "b2b_status");

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "material_number");

            ld_material_price = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_price");
            // Get material details

            ld_material_price_after_discount = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_price_after_discount");

            ld_material_discount_percent = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_discount_percent");

            ll_min_valid_month = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "min_valid_months");

            ldt_curr_expiration_date = this.ViewModel.dw_inv_pack_details.GetItemValue<DateTime?>((int)al_row_no, "expiration_date");

            ld_sell_price = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "sell_price");

            ll_order_material_color = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "color");
            //ldt_last_update_datetime 			=  dw_inv_pack_details.GetItem<DateTime>(al_row_no, "last_update_datetime")
            // Copy item details 

            ls_details = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row_no, "details");

            ll_supplier_number = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number");

            ll_order_num = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "order_number");
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

            ll_serial_number = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row_no, "serial_number");
            if (ll_serial_number == 0)
            {
                ll_serial_number = 0;
            }
            // AlexKh - 1.2.48.34 - 2012-12-05 - SPUCM00003402 - check if negative value, then set zero
            if (ld_expected_material_quantity < 0 || ld_expected_material_quantity == null)
            {
                ld_expected_material_quantity = 0;
            }
            // Set supplier number and order number in u_material_update non-visual onject
            this.ViewModel.iuo_material_updateProperty.uf_set_supplier_number(ll_supplier_number);
            if (ll_order_num == 0)
            {
                ll_order_num = 0;
            }
            this.ViewModel.iuo_material_updateProperty.uf_set_order_number(ll_order_num);
            // Load material_suppliers_connect data for the material
            await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers_connect_new(ll_material_number);
            // Load material_suppliers data for the material
            await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers_new(ll_material_number, ld_material_price);
            // Update material_suppliers_connect & material_suppliers
            this.ViewModel.iuo_material_updateProperty.uf_update_new(ref ll_error_number, ref ls_error_text);
            // Update order state in supplier_order_move
            this.ViewModel.iuo_order_searchProperty.uf_set_order_state("P");

            //GalilCS - Update order number
            if (ll_order_num != 0)
            {
                this.ViewModel.iuo_order_searchProperty.il_order_number = ll_order_num;
            }
            else if (this.ViewModel.dw_inv_pack_details.GetItemStatus(al_row_no, 0, ModelBuffer.Primary) == ModelAction.Insert)
            {
                return false;
            }

            // AlexKh - 1.2.46.3 - 2010-11-7 - SPUCM00002425 - check main supplier and distributor for the item
            if (this.ViewModel.iuo_order_searchProperty.uf_get_order_material_quantity(ll_material_number) <= 0)
            {
                // Get distributor number of current invoice
                ls_find = SystemHelper.Concat("invoice_number == ", ll_doc_no == null ? null : Convert.ToString(ll_doc_no));


                ll_found = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_move.Find(ls_find, 0, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_move.RowCount());
                if (ll_found != null && ll_found >= 0)
                {

                    ll_distributor_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_move.GetItemValue<long>((int)ll_found, "distributor_number");
                }
                if (!await this.uf_check_material_main_supplier(ll_material_number, ll_supplier_number, al_row_no, ll_distributor_number))
                {
                    goto RESET;
                }
            }

            ll_rows_count = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.RowCount();
            if (ll_rows_count < 1)
            {
                await this.ViewModel.uf_show_message("uf_update_item - Error", "", "OK", "כשלון בשמירת נתונים, אין חשבוניות פתוחות במשטח");
                return false;
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////
            //Find in masofon the same item with the same invoice number but different serial number
            ls_find_dw = "material_number == " + ll_material_number.ToString();
            ls_find_dw = SystemHelper.Concat(ls_find_dw, SystemHelper.Concat(" and doc_no == ", ll_doc_no == null ? null : Convert.ToString(ll_doc_no.Value)));
            if (ll_serial_number > 0)
            {
                ls_find_dw = SystemHelper.Concat(ls_find_dw, SystemHelper.Concat(" and serial_number <> ", ll_serial_number.ToString()));

            }


            ll_found_dw = this.ViewModel.dw_inv_pack_details.Find(ls_find_dw, 0, this.ViewModel.dw_inv_pack_details.RowCount());
            //Find row number of the item in global pallets structure
            ls_find = SystemHelper.Concat(
                SystemHelper.Concat("material_number == ", ll_material_number),
                SystemHelper.Concat(" and invoice_number == ", ll_doc_no));
            if (ll_serial_number > 0)
            {
                ls_find = SystemHelper.Concat(ls_find, SystemHelper.Concat(" and invoice_details_serial_number == ", ll_serial_number));
            }


            ll_found = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
            if (ll_found < 0)
            {
                await this.ViewModel.uf_show_message("uf_update_item - Error", "", "OK", ".אי אפשר לשמור את השינויים");
                return false;
            }
            //Check if working on new row, then only one row must be handled

            if (ldwis_doc == ModelAction.Insert)
            {
                // Update quantity_within_invoice in supplier_order_details
                this.ViewModel.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ld_expected_material_quantity.Value);
                //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_update_item", Convert.ToString("Act:      " + "Update Invoice Details, row status = NewModified!."), Convert.ToString("Invoice No: " + ll_doc_no.ToString()), "");
                if (ll_found >= 0)
                {

                    ld_bonus_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                    ld_inv_pack_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");

                    ld_material_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "material_quantity");
                    if (ld_material_quantity == 0)
                    {
                        ld_material_quantity = 0;
                    }
                    //Set Row number for future items sort
                    //GalilCS -  Increase the indicator by 1 because find method returns zero base value.
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "indicator", (al_row_no + 1).ToString());
                    //Set quantities for invoice_details table update

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expected_material_quantity", Convert.ToString(ld_expected_material_quantity + ld_material_quantity));

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", Convert.ToString(ld_expected_material_quantity + ld_material_quantity));
                    if (li_b2b_status == 1 && ld_expected_material_quantity + ld_material_quantity == ld_inv_pack_quantity)
                    {

                        this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", (Convert.ToDecimal(ld_expected_material_quantity + ld_material_quantity) - ld_bonus_quantity).ToString());
                    }
                    // Set material details
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "mini_terminal", "1");
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_price", ld_material_price.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_price_after_discount", ld_material_price_after_discount.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_discount_percent", ld_material_discount_percent.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "materials_min_valid_months", ll_min_valid_month.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date);
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "sell_price", ld_sell_price.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "current_catalog_sell_price", ld_sell_price.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "color", ll_order_material_color.ToString());
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "supplier_number", ll_supplier_number.ToString());
                    // Copy item details
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "invoice_details_details", ls_details ?? "");
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "state", Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row_no, "state")));
                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "last_update_datetime", this.ViewModel.dw_inv_pack_details.GetItemValue<DateTime?>((int)al_row_no, "last_update_datetime"));
                    if (ll_found_dw >= 0 && ll_found_dw != al_row_no) //concatenate is needed
                    {

                        ld_expected_material_quantity_temp = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(ll_found_dw.Value, "expected_material_quantity");
                        if (ld_expected_material_quantity_temp == 0)
                        {
                            ld_expected_material_quantity_temp = 0;
                        }

                        if (await uf_set_b2b_scanned_quantity(al_row_no, this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "expected_material_quantity") + ld_expected_material_quantity_temp, this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_invoice_numberProperty.Value))
                        {

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                ll_rtn_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Update(unitOfWork);
                            }
                        }
                    }
                    else
                    {
                        // AlexKh - 1.1.1.1 - 2013-02-28 - SPUCM00004018 - Handle two invoices for one item in one pallet
                        await uf_split_to_new_invoice(al_row_no);

                        if (await uf_set_b2b_scanned_quantity(al_row_no, this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "expected_material_quantity"), this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_invoice_numberProperty.Value))
                        {
                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                ll_rtn_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Update(unitOfWork);
                            }
                        }
                    }
                }
                else
                {




                    ll_rtn_code = this.ViewModel.dw_inv_pack_details.RowsCopy((int)al_row_no, (int)al_row_no + 1, ModelBuffer.Primary, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Repository(), this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount() + 1, ModelBuffer.Primary);
                    // AlexKh - 1.2.48.0.12 - 2011-11-03 - Fix for pallets - set quantity for item that not in invoice
                    if (ll_rtn_code == 1)
                    {


                        this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount() - 1, "material_quantity", ld_expected_material_quantity.ToString());

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Update(unitOfWork);
                        }
                    }
                }
                // Check if Update failed, and update log
                ll_last_sql_db_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.uf_get_last_sql_db_code();
                ls_last_sql_err_text = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.uf_get_last_sql_err_text();
                //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_update_item", Convert.ToString("Update Invoice Details, found row: " + ll_found.ToString()), Convert.ToString("Return code: " + ll_rtn_code.ToString()), Convert.ToString("Error:    " + ls_last_sql_err_text));
                // If Update failed, then stop the action
                if (ll_rtn_code < 0)
                {
                    return false;
                }
                //Handle changes in existing row
            }
            else
            {
                if (this.ViewModel.idec_item_quantity_change_differenceProperty != 0)
                {
                    // Update quantity_within_invoice in supplier_order_details
                    this.ViewModel.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, this.ViewModel.idec_item_quantity_change_differenceProperty);
                }
                if (ll_found >= 0)
                {

                    ld_expected_material_quantity_temp = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");

                    ld_material_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "material_quantity");
                    if (ld_expected_material_quantity_temp == 0)
                    {
                        ld_expected_material_quantity_temp = 0;
                    }
                    if (ll_found_dw >= 0 && ll_found_dw != al_row_no) //concatenate is needed
                    {
                        ld_expected_material_quantity = ld_expected_material_quantity + ld_material_quantity;
                    }
                    else
                    {
                        ld_expected_material_quantity_diff = ld_expected_material_quantity.Value - ld_expected_material_quantity_temp;
                        ld_expected_material_quantity = ld_material_quantity + ld_expected_material_quantity_diff;
                    }

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity.ToString());

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "materials_barcode", Convert.ToString(ls_barcode));

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "order_quantity", ld_order_quantity.ToString());

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "invoice_number", ll_doc_no.ToString());

                    ld_bonus_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                    ld_inv_pack_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                    if (li_b2b_status == 1)
                    {
                        if (ld_expected_material_quantity == ld_inv_pack_quantity)
                        {

                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                        }
                    }
                    // Copy item details

                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "invoice_details_details", Convert.ToString(ls_details));


                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "state", Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row_no, "state")));


                    this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "last_update_datetime", this.ViewModel.dw_inv_pack_details.GetItemValue<DateTime?>((int)al_row_no, "last_update_datetime"));
                    //ELSE		
                    //ll_rtn_code = dw_inv_pack_details.RowsCopy(al_row_no, al_row_no, Primary!, invuo_doc_item_search.ids_pallet_inv_details, invuo_doc_item_search.ids_pallet_inv_details.RowCount() + 1, Primary!)
                }
                // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets - invoice number changed for existing record
                if (this.ViewModel.il_old_doc_no > 0 && this.ViewModel.il_old_doc_no != this.ViewModel.il_invoice_numberProperty)
                {

                    lb_status = await uf_set_b2b_scanned_quantity(al_row_no, this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "expected_material_quantity"), this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_old_doc_no);
                }
                else
                {

                    lb_status = await uf_set_b2b_scanned_quantity(al_row_no, this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "expected_material_quantity"), this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_invoice_numberProperty.Value);
                }
                if (lb_status)
                {

                    using (UnitOfWork unitOfWork = new UnitOfWork())
                    {
                        ll_rtn_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Update(unitOfWork);
                    }

                    ll_last_sql_db_code = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.uf_get_last_sql_db_code();
                    ls_last_sql_err_text = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.uf_get_last_sql_err_text();
                    //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_update_item", Convert.ToString("Act:      " + "Update Invoice Details, row status <> NewModified!"), Convert.ToString("Return code: " + ll_rtn_code.ToString()), Convert.ToString("Error:    " + ls_last_sql_err_text));
                }
            }
            RESET:

            // ls_orig_filter = this.ViewModel.dw_inv_pack_details.GetFilter();

            // ll_rtn_code = this.ViewModel.dw_inv_pack_details.Reset("dw_inv_pack_details");
            // this.ViewModel.invuo_doc_item_searchProperty.uf_retrieve();

            //this.ViewModel.invuo_doc_item_searchProperty.ResetPallet();
            try
            {
                bool res = await this.ViewModel.uf_populate_row<decimal>(al_row_no);
            }
            catch (Exception ex)
            {

            }


            //     this.ViewModel.dw_inv_pack_details.ResetUpdate();

            // Kamel temporary
            //this.ViewModel.dw_inv_pack_details.SetFilter(ls_orig_filter);
            //this.ViewModel.dw_inv_pack_details.Filter();


            var cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");
            if (!await this.ViewModel.uf_is_new_row_exist(this.ViewModel.dw_inv_pack_details) && !(cb_item_diff.Text == "הצג את כל הפריטים" || cb_show_all.Text == "הצג את כל הפריטים"))
            {
                await this.ViewModel.dw_inv_pack_details_ue_new_row();
            }


            this.ViewModel.is_new_barcode = "";
            this.ViewModel.is_old_barcode = "";
            this.ViewModel.il_old_doc_no = 0;
            this.ViewModel.idec_item_quantity_change_differenceProperty = 0m;
            this.ViewModel.uf_set_doc_no(-1, 0, true);
            return true;
        }


        public int uf_retrieve_data() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_retrieve_data
            //*Purpose:							Retrieve  data
            //*Arguments:							None.
            //*Return:								Integer : 	1 - Success
            //*													-1 - Otherwise.
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            //gilad - items removed
            long ll_SetSort = 0;
            long ll_sort = 0;
            long ll_i = 0;
            long? ll_found = 0;
            long ll_count = 0;
            string[,] ls_locs = new string[101, 4];
            // save_current locks

            ll_count = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.RowCount();
            for (ll_i = 1; ll_i <= ll_count; ll_i++)
            {

                ls_locs[ll_i, 1] = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<int>((int)ll_i, "invoice_number").ToString();
                ls_locs[ll_i, 2] = "I";

                ls_locs[ll_i, 3] = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<string>(ll_i, "create_mode");
            }
            // Save_current locks
            for (ll_i = 1; ll_i <= ll_count; ll_i++)
            {
                if (ls_locs[ll_i, 3].ToString() == "1")
                {


                    ll_found = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.Find("invoice_number == " + ls_locs[ll_i, 1].ToString(), 0, this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.RowCount());
                    if (ll_found >= 0)
                    {

                        this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.SetItem(ll_found.Value, "create_mode", "1");
                    }
                }
            }

            ll_SetSort = this.ViewModel.dw_inv_pack_details.SetSort("indicator ASC");

            ll_sort = this.ViewModel.dw_inv_pack_details.Sort();
            return 1;
        }



        public int uf_get_doc_no_en(long al_row_no, long al_material_no) //*********************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_doc_no_en
            //*Purpose:							decide if doc number should be enabled
            //*Arguments:							Long	al_row_no
            //*										Long	al_material_no
            //*Return:								INTEGER:    1 - Success
            //*													 	-1 - Otherwise
            //*Date 			Programer		Version		Task#			Description
            //*---------------------------------------------------------------------
            //*01-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //*********************************************************************

            long ll_docs = 0;
            long ll_cnt_docs = 0;
            long ll_cnt_b2b_docs = 0;
            long[] ll_doc_number = null;
            long ll_doc_no_en = 0;

            //gilad - items removed
            // Check if no rows or first item

            ll_docs = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.RowCount();
            if (ll_docs == 0)
            {
                return 2;
            }
            // If material number is not 0, check which docs and how many the material is related to.
            if (al_material_no != 0)
            {
                ll_cnt_docs = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number(al_material_no, ref ll_doc_number);
            }
            ll_cnt_b2b_docs = ll_docs;
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
            if (ll_cnt_b2b_docs >= 1)
            {
                ll_doc_no_en = 1;
            }
            else
            {
                ll_doc_no_en = 2;
            }
            return (int)ll_doc_no_en;
        }
        public async Task<bool> uf_delete_item(long al_row) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_manual
            //*Function Name:	uf_delete_item
            //*Purpose: 			delete item from masofon (copy it to delete table)
            //*Arguments: 		Long	al_row
            //*Return:				Return Boolean
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*11-08-2010		AlexKh			1.2.46.1	CR#1138		Initial version
            //********************************************************************

            long ll_ret = 0;

            //gilad - items removed
            long ll_material_number = 0;
            long ll_doc_no = 0;
            long ll_serial_number = 0;
            long ll_material_quantity = 0;

            //gilad - items removed

            this.ViewModel.il_supplier_numberProperty = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "supplier_number");

            this.ViewModel.il_invoice_numberProperty = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");

            this.ViewModel.il_order_numberProperty = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "order_number");
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");

            ll_doc_no = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "doc_no");

            ll_serial_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "serial_number");

            ll_material_quantity = (long)this.ViewModel.dw_inv_pack_details.GetItemValue<Decimal>(al_row, "expected_material_quantity");

            this.ViewModel.ids_invpack_deleted_itemsProperty.Reset();
            // Check if current document locked
            if (!await this.ViewModel.uf_validate_doc_change(al_row, "", "", this.ViewModel.dw_inv_pack_details))
            {
                return false;
            }
            await f_begin_tranClass.f_begin_tran();
            if (await uf_copy_item_2_delete_table(al_row))
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = this.ViewModel.ids_invpack_deleted_itemsProperty.Update(unitOfWork);
                }

                if (ll_ret < 0)
                {
                    await this.ViewModel.uf_show_message("uf_delete_item", "DB Error", "OK", "Failed to update invpack_deleted_items table.");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
            }
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
            this.ViewModel.idec_item_quantity_change_differenceProperty += (-1) * ll_material_quantity;

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "material_quantity", Convert.ToDecimal(0));

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "bonus_quantity", Convert.ToDecimal(0));

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "expected_material_quantity", Convert.ToDecimal(0));
            // AlexKh - 1.2.48.0.12 - 2011-11-3 - Prevent row status update
            //dw_inv_pack_details.SetItem(al_row, "state", "d")

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time());
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
            //SetNull(ll_null)
            //dw_inv_pack_details.SetItem(al_row, "mini_terminal", ll_null)
            if (!await uf_update_item((int)al_row))
            {
                await this.ViewModel.uf_show_message("uf_delete_item", "Error", "OK", "Failed to delete item.");
                await f_rollbackClass.f_rollback();
                return false;
            }
            await f_commitClass.f_commit();
            return true;
        }
        public async Task<bool> uf_copy_item_2_delete_table(long al_row) //************************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_copy_item_2_delete_table
            //*Purpose: 			Insert a row into the ids_invpack_deleted_items DS.
            //*Arguments: 		Long - al_row - Row number in uo_body.dw_inv_pack_details.
            //*Return:				Boolean - TRUE - Success to Insert the row.
            //*									 FALSE - Failed to Insert the row.
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------------
            //*11-08-2010		AlexKh			1.2.46.1	CR#1138		Initial version
            //*************************************************************************

            long ll_new_row = 0;
            long? ll_found = 0;
            long ll_supplier = 0;
            long ll_order_number = 0;
            long? ll_doc_number = 0;
            long ll_docs = 0;
            long ll_count = 0;
            long ll_serial_number = 0;
            long ll_material_number = 0;
            long ll_batch_count = 0;
            long ll_batch_row = 0;
            string ls_find = null;
            char lc_doc_type = '\0';

            ll_new_row = this.ViewModel.ids_invpack_deleted_itemsProperty.Insert(0);
            if (ll_new_row < 0)
            {
                return false;
            }
            // Header

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "delete_option", "1");
            (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier)).Retrieve(out ll_supplier);

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_number", ll_supplier.ToString());
            (await this.ViewModel.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "order_number", ll_order_number.ToString());

            ll_doc_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
            lc_doc_type = this.ViewModel.uf_get_doc_type(ll_doc_number.Value).Result;
            if (lc_doc_type.ToString() == "I")
            {

                this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_name", "Invoice");
            }

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_number", ll_doc_number.ToString());

            ll_docs = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.RowCount();
            ls_find = "invoice_number == " + ll_doc_number.ToString();

            ll_found = this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.Find(ls_find, 0, (int)ll_docs);

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "employee_number", this.ViewModel.il_employee_number.ToString());


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "move_datetime", this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<DateTime>((int)ll_found, "date_move").ToString());


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_datetime", this.ViewModel.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<DateTime>((int)ll_found, "arrive_datetime").ToString());

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "state", "O");

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "b2b_status", "1");

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_total_amount", this.ViewModel.invuo_doc_item_searchProperty.uf_get_doc_total_amount(ll_doc_number.Value).ToString());

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "discount_percent", this.ViewModel.invuo_doc_item_searchProperty.uf_get_doc_discount_percent(ll_doc_number.Value).ToString());
            // Item

            ll_serial_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "serial_number");

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "serial_number", ll_serial_number.ToString());

            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "item_state", this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row, "state"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "last_update_datetime", this.ViewModel.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "last_update_datetime"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "indicator", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "indicator"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "packing_list_number", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "order_number"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "packing_serial", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "order_serial"));
            if (lc_doc_type.ToString() == "I")
            {


                this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "import_type", this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row, "import_type"));
            }


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "color", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "color"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "mini_terminal", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "mini_terminal"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "expiration_date", this.ViewModel.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "expiration_date"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_row", this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row, "bonus_row"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_bonus_connect", this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "material_bonus_connect"));
            if (lc_doc_type.ToString() == "I")
            {

                this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_within_invoice", "0");
            }


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "expected_material_quantity", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_quantity", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_quantity"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_quantity", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "bonus_quantity"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_price", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_price"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_discount_percent", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "supplier_discount_percent"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_discount_percent", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_discount_percent"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_discount_amount", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_discount_amount"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_discount", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "bonus_discount"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_price_after_disc", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_price_after_discount"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "sell_price", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "sell_price"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "current_catalog_sell_price", this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "current_catalog_sell_price"));


            this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "details", this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row, "details"));
            // Batch

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(al_row, "material_number");
            if (true)
            {
                ViewModel.LoadData1(ll_serial_number, ll_material_number, ref ll_count);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await this.ViewModel.uf_show_message("uf_copy_item_2_delete_table", "DB Error", "OK", Convert.ToString(masofonAlias.masofon.Instance.sqlca.SqlErrText));
                return false;
            }
            if (ll_count > 0)
            {
                if (al_row != 0)
                {
                    if (this.ViewModel.ids_material_batchsProperty == null)
                    {
                        this.ViewModel.ids_material_batchsProperty = new d_upd_material_batch_argRepository("", "", "", "", "");
                    }

                    ll_batch_count = this.ViewModel.ids_material_batchsProperty.Retrieve(ll_serial_number, masofonAlias.masofon.Instance.gs_vars.branch_number, ll_material_number);
                    for (ll_batch_row = 1; ll_batch_row <= ll_batch_count; ll_batch_row++)
                    {


                        this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "batch_number", Convert.ToString(this.ViewModel.ids_material_batchsProperty.GetItemValue<string>(ll_batch_row, "batch_number")));


                        this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "recieved_date", this.ViewModel.ids_material_batchsProperty.GetItemValue<DateTime>((int)ll_batch_row, "recieved_date").ToString());


                        this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "batch_expiry_date", this.ViewModel.ids_material_batchsProperty.GetItemValue<DateTime>((int)ll_batch_row, "expiry_date").ToString());


                        this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_in", this.ViewModel.ids_material_batchsProperty.GetItemValue<int>((int)ll_batch_row, "quantity_in").ToString());


                        this.ViewModel.ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_out", this.ViewModel.ids_material_batchsProperty.GetItemValue<int>((int)ll_batch_row, "quantity_out").ToString());
                    }

                    this.ViewModel.ids_material_batchsProperty.Reset();
                }
                ViewModel.UpdateData2(ll_serial_number, ll_material_number);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await this.ViewModel.uf_show_message("uf_copy_item_2_delete_table", "DB Error", "OK", Convert.ToString(masofonAlias.masofon.Instance.sqlca.SqlErrText));
                    return false;
                }
            }
            return true;
        }

        public async Task uf_filter_docs_list(long al_row) //************************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_filter_docs_list
            //*Purpose: 			Filter invoices list in the DDDW by item if exist in current row
            //*Arguments: 		Long - al_row - Row number
            //*Return:				None.
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------------
            //*19-08-2010		AlexKh			1.2.46.1	CR#1138		Initial version
            //*************************************************************************

            long ll_material_number = 0;
            long[] ll_doc_number = null;
            long ll_i = 0;
            long ll_docs_count = 0;
            string ls_barcode = null;
            string ls_barcode_main = null;
            string ls_filter = null;
            GridColumn ldwc_docs;
            if (al_row == -1)
            {
                return;
            }

            ldwc_docs = this.ViewModel.GetDocNo(this.ViewModel.dw_inv_pack_details);

            ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");

            ldwc_docs.GetRepository().SetFilter("");

            ldwc_docs.GetRepository().Filter();
            if (isempty_stringClass.isempty_string(ls_barcode))
            {
                return;
            }

            if (ldwc_docs.GetRepository().RowCount() == 0)
            {
                return;
            }
            ls_barcode_main = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
            ll_material_number = await this.ViewModel.iuo_material_search.uf_get_material_number(ls_barcode_main);
            ll_docs_count = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
            // AlexKh - 1.2.46.6 - 2011-01-23 - SPUCM00002425 - create docs list by item's main supplier
            if (ll_docs_count == 0)
            {
                ll_docs_count = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number_by_supplier(ll_material_number, ref ll_doc_number);
            }
            if (ll_docs_count > 0)
            {
                for (ll_i = 1; ll_i <= ll_docs_count; ll_i++)
                {
                    ls_filter += "doc_number == " + ll_doc_number[ll_i].ToString();
                    if (ll_i < ll_docs_count)
                    {
                        ls_filter += " OR ";
                    }
                }

                ldwc_docs.GetRepository().SetFilter(ls_filter);
            }
            else
            {

                ldwc_docs.GetRepository().SetFilter("doc_number == 0");
            }

            ldwc_docs.GetRepository().Filter();
            return;
        }


        public async Task<bool> uf_show_item_invoices() //**********************************************************************************************
        {
            ButtonElement cb_item_diff = this.GetVisualElementById<ButtonElement>("cb_item_diff");
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_show_item_invoices
            //*Purpose:							filter rows by item and it's invoices
            //*  
            //*Arguments:							None.
            //*Return:								Boolean
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*6-09-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            string ls_filter = null;
            string ls_orig_filter = null;
            int ll_row_count = 0;
            long ll_current_row = 0;
            long ll_material_number = 0;

            ll_current_row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
            if (ll_current_row < 0)
            {
                return false;
            }

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(ll_current_row, "material_number");
            if (ll_material_number <= 0 || ll_material_number == 0)
            {
                return false;
            }

            ll_row_count = this.ViewModel.dw_inv_pack_details.RowCount();
            //Load original filter of dw



            ls_orig_filter = Convert.ToString(this.ViewModel.dw_inv_pack_details.GetFilter());
            //Show item for all it's invoices
            ls_filter = "material_number == " + ll_material_number.ToString();
            if (ls_orig_filter != ls_filter)
            {
                ViewModel.dw_inv_pack_details.SetFilter(ls_filter);
            }

            this.ViewModel.dw_inv_pack_details.Filter();
            if (ll_row_count == 0)
            {
                if (!await this.ViewModel.uf_is_new_row_exist(this.ViewModel.dw_inv_pack_details))
                {
                    await this.ViewModel.uf_new_row(this.ViewModel.dw_inv_pack_details);
                }
            }

            ll_row_count = this.ViewModel.dw_inv_pack_details.RowCount();

            (this.ViewModel.dw_inv_pack_details).SetSort("row_no");

            (this.ViewModel.dw_inv_pack_details).Sort();
            if (await this.ViewModel.uf_is_new_row_exist(this.ViewModel.dw_inv_pack_details))
            {
                Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                {
                    this.ViewModel.dw_inv_pack_details.SetFocus((int)ll_row_count, "barcode");
                });
            }
            else
            {
                Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                {
                    this.ViewModel.dw_inv_pack_details.SetFocus((int)ll_row_count, "expected_material_quantity");
                });
            }
            var cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");
            cb_show_all.Text = "הצג פריטים עם הפרשים";
            cb_item_diff.Text = "הצג את כל הפריטים";
            return true;
        }
        public async Task<bool> uf_split_item(long al_row) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_split_item
            //*Purpose:							If item was already scanned for other invoice check if scanned
            //*										less then expected then complete the missing quantity
            //*Arguments:							Long		al_row
            //*Return:								Boolean	TRUE/FALSE
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*7-09-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_material_number = 0;
            long ll_start = 0;
            long? ll_invoice_number = 0;
            long? ll_current_invoice_number = 0;
            long? ll_found = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            decimal? ldec_quantity = null;
            decimal ldec_expected_material_quantity = default(decimal);
            decimal? ldec_material_quantity = default(decimal);
            decimal? ldec_diff = 0;
            decimal ldec_material_all_pallets_quantity = default(decimal);
            decimal ldec_changed_pallets_quantity = default(decimal);

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");

            ll_current_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");

            ldec_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal?>(al_row, "expected_material_quantity");
            ll_start = -1;
            //Check if item already scanned
            do
            {
                ll_start = ll_start + 1;
                ll_start = this.ViewModel.uf_check_item_scanned_before(ll_material_number, ll_start, al_row, this.ViewModel.dw_inv_pack_details);
                if (ll_start > -1)
                {

                    ll_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(ll_start, "doc_no");


                    ll_found = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find("invoice_number == " + ll_invoice_number.ToString() + " AND material_number == " + ll_material_number.ToString(), 0, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
                    if (ll_found >= 0)
                    {

                        ldec_expected_material_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");

                        ldec_material_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");

                        ldec_material_all_pallets_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "material_quantity");
                        if ((ldec_material_quantity - ldec_expected_material_quantity) > 0)
                        {

                            ll_supplier_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "supplier_number");

                            ll_invoice_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "invoice_number");

                            ll_order_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "packing_list_number");
                            if (!await this.ViewModel.uf_invoice_lock("LOCK", "I", ll_invoice_number.Value))
                            {
                                return false;
                            }
                            if (ldec_quantity != null && (ldec_material_quantity - ldec_expected_material_quantity) >= ldec_quantity.Value)
                            {
                                ldec_diff = ldec_quantity;
                                ldec_quantity = 0;
                                ldec_material_quantity = ldec_expected_material_quantity + ldec_diff.Value;
                            }
                            else if (ldec_quantity != null && (ldec_material_quantity - ldec_expected_material_quantity) < ldec_quantity)
                            {
                                ldec_diff = ldec_material_quantity - ldec_expected_material_quantity;
                                ldec_quantity = ldec_quantity - (ldec_material_quantity - ldec_expected_material_quantity);
                            }
                            // AlexKh - 1.2.48.34 - 2012-12-05 - SPUCM00003402 - check if negative value, then set zero
                            if ((ldec_material_quantity) < 0)
                            {
                                ldec_material_quantity = 0;
                            }
                            if ((ldec_quantity) <= 0)
                            {
                                ldec_quantity = null;
                            }
                            ldec_changed_pallets_quantity = ldec_material_all_pallets_quantity + ldec_diff.Value;
                            if ((ldec_changed_pallets_quantity) < 0)
                            {
                                ldec_changed_pallets_quantity = 0;
                            }

                            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                            (this.ViewModel.dw_inv_pack_details).SetItem(ll_start, "expected_material_quantity", ldec_material_quantity);

                            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "expected_material_quantity", ldec_quantity);

                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expected_material_quantity", ldec_changed_pallets_quantity.ToString());

                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", ldec_changed_pallets_quantity.ToString());
                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_number);
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                            // Update quantity_within_invoice in supplier_order_details
                            this.ViewModel.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ldec_diff.Value);
                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(this.ViewModel.il_order_numberProperty);
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(this.ViewModel.il_supplier_numberProperty);
                            await uf_set_b2b_scanned_quantity(ll_start, ldec_material_quantity.Value, ll_supplier_number, ll_invoice_number.Value);

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Update(unitOfWork);
                            }
                        }
                    }
                }
            }
            while (ll_start >= 0 && ldec_quantity > 0);
            return true;
        }
        public async Task<bool> uf_set_b2b_scanned_quantity(long al_row, decimal adec_quantity, long al_supplier_number, long al_invoice_number) //************************************************************************
        {

            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_set_b2b_scanned_quantity
            //*Purpose: 			Update scanned_quanntity in b2b_invoice_details for specific pallet
            //*Arguments: 		Long - al_row - Row number in masofon
            //*						Decimal - adec_quantity - new quantity
            //*Return:				Boolean - TRUE - Success to Update the row.
            //*									 FALSE - Failed to Update the row.
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------------
            //*22-09-2010		AlexKh			1.2.46.1	CR#1138		Initial version
            //*************************************************************************

            long ll_item_number = 0;

            ll_item_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");
            ViewModel.UpdateData3(adec_quantity, al_supplier_number, al_invoice_number, ll_item_number);

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {

                await this.ViewModel.uf_show_message("uf_set_b2b_scanned_quantity", "DB Error", "OK", Convert.ToString(masofonAlias.masofon.Instance.sqlca.SqlErrText));
                return false;
            }
            return true;
        }


        public int uf_populate_not_scanned_items() //*********************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_populate_not_scanned_items
            //*Purpose:							populate data
            //*Arguments:							None
            //*Return:								INTEGER:    1 - Success
            //*													 	-1 - Otherwise
            //*Date 			Programer		Version		Task#				Description
            //*---------------------------------------------------------------------
            //*23-01-2011		AlexKh			1.2.46.6  	SPUCM00002425 	Initial Version
            //*********************************************************************

            string ls_filter = null;
            long ll_row_count = 0;
            long ll_rows_copy = 0;
            // ...Invoice Details
            // Set filter
            ls_filter = "expected_material_quantity = 0 or expected_material_quantity == null ";

            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter(ls_filter);

            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();
            // Get the related invoices items and copy into dw_inv_pack_details datawindow

            ll_row_count = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
            if (ll_row_count > 0)
            {





                ll_rows_copy = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowsCopy(1, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount(), ModelBuffer.Primary, this.ViewModel.dw_inv_pack_details, this.ViewModel.dw_inv_pack_details.RowCount() + 1, ModelBuffer.Primary);
            }
            // Set filter - Original

            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter("");

            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();
            return 1;
        }
        public async Task<bool> uf_choose_invoice_number(int al_row, long al_material_number, string as_barcode) //************************************************************************
        {

            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_choose_invoice_number
            //*Purpose: 			Set invoice number in masofon for item
            //*Arguments: 		Long 		- al_row - Row number dw_inv_pack_details
            //*						Long		- al_material_number
            //*Return:				Boolean 	- TRUE - Success to update invoice number.
            //*									 FALSE - Failed to update invoice number.
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------------
            //*27-10-2011		AlexKh			1.2.48.0.11				Initial version
            //*************************************************************************

            long ll_ret = 0;
            long? ll_found = 0;
            long ll_scanned_before_row = 0;
            long ll_invoice_count = 0;
            long[] ll_doc_number = null;
            long ll_i = 0;
            long ll_index = -1;

            //gilad - items removed
            long? ll_current_invoice_number = 0;

            //gilad - items removed
            bool lb_result = false;
            decimal? ldec_expected_material_quantity = default(decimal);
            //If invoice already set, then no need to set again

            ll_current_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
            if (ll_current_invoice_number > 0)
            {
                return true;
            }
            //Bring all invoices for this item
            ll_invoice_count = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number(al_material_number, ref ll_doc_number);
            if (ll_invoice_count == 0) //No invoices for this item
            {

                (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "doc_no_en", 2); //enable
                return false;
            }
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "b2b_status", 1);
            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "doc_no_en", 1); //disabled
            ll_scanned_before_row = this.ViewModel.uf_check_item_scanned_before(al_material_number, 0, al_row, this.ViewModel.dw_inv_pack_details);
            if (ll_scanned_before_row >= 0)
            {
                lb_result = await uf_split_item(al_row);
            }
            //Check current quantity, if bigger then 0, then proceed to determine appropriate invoice number
            ldec_expected_material_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal?>(al_row, "expected_material_quantity");
            if (ldec_expected_material_quantity.HasValue && ldec_expected_material_quantity > 0)
            {
                //FOR every eligible invoice check if any item scanned before for this invoice 
                for (ll_i = 0; ll_i < ll_invoice_count; ll_i++)
                {
                    //Check if item already scanned for this invoice


                    ll_found = this.ViewModel.dw_inv_pack_details.Find("material_number == " + al_material_number.ToString() + "and doc_no == " + ll_doc_number[ll_i].ToString(), 0, this.ViewModel.dw_inv_pack_details.RowCount() + 1);
                    //If scanned, nothing to do with this invoice number
                    if (ll_found >= 0)
                    {
                        if (ll_index == -1)
                        {
                            ll_index = ll_i;
                        }
                        continue;
                    }
                    //Check any other item scanned for this invoice, but not current item
                    ll_found = this.ViewModel.dw_inv_pack_details.Find("material_number <> " + al_material_number.ToString() + "and doc_no == " + ll_doc_number[ll_i].ToString(), 0, this.ViewModel.dw_inv_pack_details.RowCount() + 1);
                    //If scanned, we can use this invoice
                    if (ll_found >= 0)
                    {
                        ll_index = ll_i;
                        break; //This invoice will be used in case no items scanned for one of the relevant invoices
                    }
                    else
                    {
                        ll_index = ll_i;
                    }
                }
                if (ll_index == -1)
                {
                    throw new Exception("GalilCS - ll_index out of range");
                }
                //set invoice number relevant data
                this.ViewModel.uf_set_doc_no(al_row, ll_doc_number[ll_index], true);
                await this.ViewModel.uf_set_material_details(al_row, as_barcode, 'I', 0, "NEW");
                await this.ViewModel.uf_handle_expiration_date(al_row);

                //gilad - items removed
                if (!await this.ViewModel.uf_is_new_row_exist(this.ViewModel.dw_inv_pack_details))
                {
                    //(this.ViewModel.dw_inv_pack_details).dw_inv_pack_details_ue_new_row();
                    await this.ViewModel.dw_inv_pack_details_ue_new_row();
                }
                else
                {
                    await this.ViewModel.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", this.ViewModel.dw_inv_pack_details.RowCount(), "barcode");
                }
                //uf_update_item(al_row)
                return true;
            }
            else
            {
                (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "barcode", "");
                (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "material_name", "");
                // AlexKh - 1.1.26.0 - 2015-01-04 - SPUCM00005168  - set focus on new row

                Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                {
                    this.ViewModel.dw_inv_pack_details.SetFocus((int)al_row, "barcode");
                });

                //gilad - items removed
            }
            //item quantity was splited to other invoices
            this.ViewModel.uf_set_doc_no(-1, 0, true);
            return false;
        }

        public async Task<bool> uf_split_to_new_invoice(long al_row) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_split_to_new_invoice
            //*Purpose:							If inserted quantity bigger the in invoice and there is another invoice still not scanned
            //*										create new row for new invoice and split the quantity
            //*Arguments:							Long		al_row
            //*Return:								Boolean	TRUE/FALSE
            //*Date 			Programer		Version		Task#				Description
            //*------------------------------------------------------------------------------------------------
            //*26-02-2013		AlexKh			1.1.1.1  	SPUCM00004018 	Initial Version
            //************************************************************************************************
            long ll_material_number = 0;
            long ll_current_invoice_number = 0;
            long ll_docs_count = 0;
            long[] ll_doc_number = null;
            long ll_i = 0;
            long? ll_found = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_new_invoice_number = 0;
            long? ll_current_invoice_found = 0;
            long ll_ret = 0;
            decimal ldec_quantity = default(decimal);
            decimal ldec_original_invoice_quantity = default(decimal);

            ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");

            ll_current_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(al_row, "doc_no");

            ldec_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity");
            ll_docs_count = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
            if (ll_docs_count < 2)
            {
                return true;
            }
            for (ll_i = 0; ll_i < ll_docs_count; ll_i++)
            {
                if (ll_doc_number[ll_i] != ll_current_invoice_number)
                {
                    ll_new_invoice_number = ll_doc_number[ll_i];
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            // Check if item was not scanned before
            if (this.ViewModel.uf_check_item_scanned_before(ll_material_number, 0, al_row, this.ViewModel.dw_inv_pack_details) == -1)
            {
                //Find current row number
                ll_current_invoice_found = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find("invoice_number == " + ll_current_invoice_number.ToString() + " AND material_number == " + ll_material_number.ToString(), 0, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
                if (ll_current_invoice_found >= 0)
                {
                    ldec_original_invoice_quantity = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_current_invoice_found, "inv_pack_quantity");
                    if (ldec_quantity > ldec_original_invoice_quantity)
                    {
                        //Find new row number
                        ll_found = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find("invoice_number == " + ll_new_invoice_number.ToString() + " AND material_number == " + ll_material_number.ToString(), 0, this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
                        if (ll_found >= 0)
                        {
                            ll_supplier_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "supplier_number");
                            //ll_invoice_number = invuo_doc_item_search.ids_pallet_inv_details.GetItem<int>(ll_found, "invoice_number")

                            ll_order_number = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "packing_list_number");
                            //Update quantity for original invoice
                            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "expected_material_quantity", ldec_original_invoice_quantity);
                            (this.ViewModel.dw_inv_pack_details).SetItem(al_row, "material_quantity", ldec_original_invoice_quantity);

                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_current_invoice_found.Value, "expected_material_quantity", ldec_original_invoice_quantity.ToString());
                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_current_invoice_found.Value, "material_quantity", ldec_original_invoice_quantity.ToString());

                            //Update quantity for new row
                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expected_material_quantity", (ldec_quantity - ldec_original_invoice_quantity).ToString());
                            this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "material_quantity", (ldec_quantity - ldec_original_invoice_quantity).ToString());

                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_number);
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                            // Update quantity_within_invoice in supplier_order_details for new row
                            this.ViewModel.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ldec_quantity - ldec_original_invoice_quantity);
                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(this.ViewModel.il_order_numberProperty);
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(this.ViewModel.il_supplier_numberProperty);
                            // Update quantity_within_invoice in supplier_order_details for original row
                            this.ViewModel.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, (-1) * (ldec_quantity - ldec_original_invoice_quantity));
                            // AlexKh - 1.1.1.9 - 2013-04-23 - SPUCM00004055 - copy new row to the end
                            // Add new row to be presented in masofon


                            ll_ret = this.ViewModel.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowsCopy((int)ll_found, Convert.ToInt32(ll_found) + 1, ModelBuffer.Primary, this.ViewModel.dw_inv_pack_details, this.ViewModel.dw_inv_pack_details.RowCount() + 1, ModelBuffer.Primary);
                            //Primary!, dw_inv_pack_details, dw_inv_pack_details.RowCount(), Primary!)
                            // AlexKh - 1.1.1.9 - 2013-04-23 - SPUCM00004055 - set vaild row number to update

                            await uf_set_b2b_scanned_quantity(this.ViewModel.dw_inv_pack_details.RowCount() - 1, ldec_quantity - ldec_original_invoice_quantity, ll_supplier_number, ll_new_invoice_number);
                            //uf_set_b2b_scanned_quantity(ll_found, ldec_quantity - ldec_original_invoice_quantity, ll_supplier_number, ll_new_invoice_number)
                        }
                    }
                }
            }
            return true;
        }

        public void destructor()
        {
            base.destructor();
            if (this.ViewModel.iuo_material_updateProperty != null)
            {


            }
            if (this.ViewModel.iuo_order_searchProperty != null)
            {



            }
            if (this.ViewModel.ids_invpack_deleted_itemsProperty != null)
            {


            }
            if (this.ViewModel.ids_material_batchsProperty != null)
            {



            }
        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_show_all_clicked(object sender, EventArgs e)
        {
            ButtonElement cb_item_diff = this.GetVisualElementById<ButtonElement>("cb_item_diff");

            await base.cb_show_all_clicked(sender, e);
            cb_item_diff.Text = "הצג פריט";

        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_return_to_header_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			cb_return_to_header.clicked event
            //*Purpose:							Finish the masofon input.
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            this.ViewModel.idec_item_quantity_change_differenceProperty = 0;
            await this.ViewModel.uf_display("SHIPMENT");
        }

        public async Task dw_inv_pack_details_itemfocuschanged(object sender, GridCellEventArgs e) //**********************************************************************************************
        {//gilad 
            return;


            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			dw_inv_pack_details.itemfocuschanged
            //*Purpose:							Set Focus
            //*Arguments:							None
            //*Return:								Long
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_serial_number = 0;
            string ls_doc_state = null;
            int row = e.RowIndex;
            if (row != null && Convert.ToInt32(row) != -1)
            {

                ls_doc_state = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(row), "doc_state");
                // AlexKh - 1.2.46.2 - 2010-10-11 - SPUCM00002425 - check in addition serial number

                ll_serial_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(Convert.ToInt64(row), "serial_number");
                var cb_delete_item = this.GetVisualElementById<ButtonElement>("cb_delete_item");
                if (!string.IsNullOrEmpty(ls_doc_state) && ls_doc_state.ToUpper() == "O" && ll_serial_number > 0)
                {
                    cb_delete_item.Enabled = true;
                }
                else
                {
                    cb_delete_item.Enabled = false;
                }
            }

            string ls_barcode = null;
            if (this.ViewModel.Visible == true)
            {
                ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(row), "barcode");
                if (string.IsNullOrEmpty(ls_barcode) && e.DataIndex != "barcode")
                {
                    await this.ViewModel.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", (Int64)row, "barcode");
                }
            }
        }
        // TODO: Could not add event handler for handler 'rowfocuschangedEvent'. (CODE=1017)
        public async Task dw_inv_pack_details_rowfocuschanged(object sender, GridElementSelectionChangeEventArgs e) //**********************************************************************************************
        {


            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			dw_inv_pack_details.rowfocuschanged
            //*Purpose:							Delete Rows
            //*Arguments:							None
            //*Return:								None
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_serial_number = 0;
            string ls_doc_state = null;
            object currentrow = e.SelectedRows?[0];
            if (currentrow != null && Convert.ToInt32(currentrow) != -1)
            {

                ls_doc_state = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(currentrow), "doc_state");
                // AlexKh - 1.2.46.2 - 2010-10-11 - SPUCM00002425 - check in addition serial number

                ll_serial_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(Convert.ToInt64(currentrow), "serial_number");
                var cb_delete_item = this.GetVisualElementById<ButtonElement>("cb_delete_item");
                if (!string.IsNullOrEmpty(ls_doc_state) && ls_doc_state.ToUpper() == "O" && ll_serial_number > 0)
                {
                    cb_delete_item.Enabled = true;
                }
                else
                {
                    cb_delete_item.Enabled = false;
                }
            }
            //gilad  
            //string ls_barcode = null;
            //if (this.ViewModel.Visible == true)
            //{
            //    int row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;

            //    ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(row), "barcode");
            //    if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
            //    {
            //        //this.ViewModel.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", (Int64)row, "barcode");
            //    }
            //}
        }






        public async Task<int> dw_inv_pack_details_rowfocuschanging(object sender, GridElementSelectionChangeEventArgs e) //**************************************************************************
        {
            //gilad
            return 0;


            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			dw_inv_pack_details.rowfocuschanging
            //*Purpose:							n case a change was done save it.
            //*Arguments:							Value			Long	currentrow
            //*										Value			Long	newrow
            //*Return:								Long
            //*Date 			Programer			Version		Task#			Description
            //*---------------------------------------------------------------------------
            //*04-07-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //***************************************************************************


            //gilad - items removed
            long? ll_invoice_number = 0;
            string ls_barcode = null;

            //gilad - items removed
            ModelAction ldw_row_status = default(ModelAction);
            object currentrow = null;
            if (this.ViewModel.dw_inv_pack_details.PreviousRow != null)
            {
                currentrow = this.ViewModel.dw_inv_pack_details.PreviousRow.Index;
            }
            if (currentrow == null || Convert.ToInt32(currentrow) == -1)
            {
                return 0;
            }


            ldw_row_status = this.ViewModel.dw_inv_pack_details.GetItemStatus((int)currentrow, 0, ModelBuffer.Primary);
            if (ldw_row_status == ModelAction.UpdateByKeys || ldw_row_status == ModelAction.Insert)
            {
                ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(currentrow), "barcode");
                if (!isempty_stringClass.isempty_string(ls_barcode))
                {
                    //kamel
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((o, ee) =>
                    {
                        this.uf_update_item((int)Convert.ToInt64(currentrow));
                    }, 1);
                }
            }
            object newrow = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
            if (Convert.ToInt32(newrow) >= 0)
            {

                ll_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(Convert.ToInt64(newrow), "doc_no");
                if (ll_invoice_number > 0)
                {
                    //Kamel
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((o, ee) =>
                    {
                        this.ViewModel.uf_set_doc_no(Convert.ToInt64(newrow), ll_invoice_number.Value, false);
                    }, 2);
                }
            }
            return 0;
        }
        public async Task<int> dw_inv_pack_details_itemchanged(object sender, GridCellEventArgs e) //********************************************************************
        {
            string dwoID = e.DataIndex;
            string data = e.Value;
            int row = e.RowIndex;
            if (row == -1)
                return 0;
            if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, dwoID)) == data)
            {
                return 0;
            }
            GridElement elem = sender as GridElement;

            try
            {
                //*Object:				uo_mini_terminal_pallets_manual
                //*Function Name:	dw_inv_pack_details.ItemChanged
                //*Purpose: 			Check validation and call the uf_set_items function
                //*Arguments: 		Value 			Long						row
                //*						Value				ControlElement				dwo
                //*						Value 			String 					data
                //*Return:				Long
                //*Date				Programer		Version	Task#	 		Description
                //*---------------------------------------------------------------------
                //*01-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
                //********************************************************************
                elem.Tag = "FiringWidgetControlItemChanged";


                var ll_order_num = (long?)this.ViewModel.dw_inv_pack_details.GetItemValue<long?>(row, "doc_no");
                if (ll_order_num != null && ll_order_num.Value > 0)
                {
                    this.ViewModel.uf_set_doc_no(row, ll_order_num.Value, false);
                }

                char lc_doc_type = '\0';
                char? lc_doc_state = '\0';
                string ls_col_name = null;
                string ls_barcode = null;
                string ls_string = null;
                string ls_message = null;
                int li_len = 0;
                int li_index = 0;
                int li_rtn_code = 0;

                //gilad - items removed
                int li_status = 0;
                long ll_material_number = 0;

                //gilad - items removed
                long? ll_doc_no = 0;

                //gilad - items removed
                long? ll_current_doc_no = 0;
                long ll_new_doc_no = 0;
                long ll_actual_quantity = 0;
                long ll_i = 0;
                long ll_b2b_docs_count = 0;
                long[] ll_doc_number = null;
                long ll_supplier_number = 0;
                long ll_ret = 0;

                //gilad - items removed
                decimal? ldec_expected_material_quantity = null;
                s_array_arguments lstr_mess = default(s_array_arguments);
                IRepository lds_package;
                ModelAction ldis_status = default(ModelAction);

                ldis_status = this.ViewModel.dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);

                ls_col_name = Convert.ToString(dwoID);
                // Disable items in closed invoice

                lc_doc_state = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "doc_state") != null ? Convert.ToChar(this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "doc_state")) : '\0';
                if (lc_doc_state.ToString() == "C" || lc_doc_state.ToString() == "S")
                {
                    return 2;
                }
                switch (ls_col_name)
                {
                    case "barcode":
                        this.ViewModel.is_new_barcode = data;

                        this.ViewModel.is_old_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode") ?? "";
                        // Calculate time for barcode insertion
                        if (this.ViewModel.il_start_time > 0)
                        {
                            // AlexKh - 1.1.26.0 - 2015-01-11 - SPUCM00005185 - save employee number in addition
                            //ls_string = String(CPU() - il_start_time) + "ms, " + is_start_time

                            ls_string = ((DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond) - this.ViewModel.il_start_time) + "ms, " + " EmpNo<" + this.ViewModel.il_employee_number.ToString() + ">";

                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "details", ls_string);

                            // TODO: Type '[dynamic]' does not have a 'SetNull' member. (CODE=30002)
                            //(this.ViewModel.dw_inv_pack_details).SetNull();
                            ll_i = 0;
                            this.ViewModel.il_start_time = 0;
                        }
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "CASE barcode ", Convert.ToString("Row:      " + row.ToString()), Convert.ToString("The Data: " + data));
                        ls_barcode = data;
                        // Check if barcode overriden then give a message
                        if (this.ViewModel.is_old_barcode.Length > 0)
                        {
                            li_rtn_code = await this.ViewModel.uf_show_message("אזהרה", "", "OKCancel", Convert.ToString(masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("שים לב אחד הפריטים הולך להדרס, האם להמשיך?").Result));
                            if (li_rtn_code == -1)
                            {

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.ViewModel.dw_inv_pack_details).SetItem(row, "barcode", this.ViewModel.is_old_barcode);
                                return 1;
                            }
                        }
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "Before uf_get_barcode_main function.", "", "");
                        // Get full barcode if it was particulaly inserted
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        // If barcode does not exist, drop leading zeros
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            li_len = data.Length;
                            for (li_index = 1; li_index <= li_len; li_index++)
                            {
                                if (data.Substring(0, 1) == "0")
                                {
                                    data = data.Substring(data.Length - (data.Length - 1));

                                    li_index++;
                                }
                                else
                                {
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }
                        }
                        ls_barcode = data;
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_get_barcode_main of ItemChanged: CASE barcode ", "", "");
                        ll_material_number = await this.ViewModel.iuo_material_search.uf_get_material_number(ls_barcode);
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_get_material_number of ItemChanged: CASE barcode ", "", "");

                        ll_doc_no = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(row, "doc_no");
                        // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
                        //uf_prepare_material_details(ll_material_number, 0.00, ll_doc_no, row)
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_prepare_material_details of ItemChanged: CASE barcode ", "", "");
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            lstr_mess = await f_is_pack_barcodeClass.f_is_pack_barcode(true, data, this.ViewModel.il_supplier_numberProperty);
                            if (lstr_mess.arg_ok == false)
                            {
                                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OK", 1, await f_get_error_message_numberClass.f_get_error_message_number(5092));
                            }
                            else
                            {
                                //Set item
                                lds_package = new d_packageRepository();
                                lds_package = lstr_mess.a_obj[1] as IRepository;
                                ls_barcode = lds_package.GetItem<d_package>(1).materials_barcode;
                                ll_actual_quantity = Convert.ToInt64(lds_package.GetItem<d_package>(1).cf_total_quantity);
                                this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", ls_barcode);
                                this.ViewModel.dw_inv_pack_details.SetItem(row, "expected_material_quantity", ll_actual_quantity);
                                if (lds_package != null)
                                {


                                }
                            }
                        }
                        this.ViewModel.is_new_barcode = ls_barcode;
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            string ls = null;

                            ls = this.ViewModel.dw_inv_pack_details.Parent.GetType().Name;
                            await this.ViewModel.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", row, "barcode");
                        }
                        if (isempty_stringClass.isempty_string(ls_barcode))
                        {
                            //If material with this barcode not exists fill empty values into this row
                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "barcode", "");
                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "material_name", "");
                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "material_price", 0);
                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "min_valid_months", 0);
                            if (this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(row, "indicator") == 0)
                            {
                                (this.ViewModel.dw_inv_pack_details).SetItem(row, "indicator", Convert.ToInt64(this.ViewModel.dw_inv_pack_details.Describe<long>("Evaluate('MAX(indicator for all)',1)")) + 1);
                            }
                            return 1;
                        }
                        // Check the material status after getting the material number
                        if (!(await this.ViewModel.iuo_material_search.uf_material_status_allow_purches(ll_material_number, ls_message)).Retrieve(out ls_message))
                        {
                            await this.ViewModel.uf_show_message("Error", "", "1", Convert.ToString(ls_message));
                            //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_material_status_allow_purches of ItemChanged: CASE barcode ", "", "");

                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "barcode", "");
                            this.ViewModel.dw_inv_pack_details.SetFocus(row, "barcode");
                            return 2;
                        }
                        // Check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        // AlexKh - 1.2.48.0.17 - 2011-11-13 - Fix for pallets - Prevent item that is no in pallet
                        ll_b2b_docs_count = this.ViewModel.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
                        if (ll_b2b_docs_count < 1)
                        {
                            // AlexKh - 1.1.26.0 - 2015-01-13 - SPUCM00005189 - check item in other pallets
                            ll_supplier_number = this.ViewModel.iuo_material_search.uf_get_material_main_supplier(ll_material_number);
                            ls_message = "";
                            long tmp_doc_no = ll_doc_no == null ? 0 : ll_doc_no.Value;
                            ll_ret = this.ViewModel.invuo_doc_item_searchProperty.uf_check_item_in_shipment(ll_material_number, this.ViewModel.il_shipment_numberProperty, this.ViewModel.is_pallet_numberProperty, ll_supplier_number, this.ViewModel.il_marlog_branchProperty, ref ls_message, ref tmp_doc_no);
                            ll_doc_no = tmp_doc_no;
                            if (ll_ret == 3) //Added to the invoice with the same supplier
                            {
                                // AlexKh - 1.1.28.0 - 2015-02-08 - SPUCM00005224 - replace message and change YES/NO and default
                                //li_rtn_code = uf_show_message("אזהרה", "", "YesNo", "הפריט לא קיים במשלוח, האם להחזיר את הפריט?")
                                li_rtn_code = await this.ViewModel.uf_show_message("אזהרה", "", "YesNo", "הפריט לא קיים במשלוח, האם לקלוט?");
                                if (li_rtn_code != -1) //YES
                                {
                                    //Add this item to the invoice with the same supplier
                                    if ((await this.ViewModel.invuo_doc_item_searchProperty.uf_add_item_to_invoice(ll_doc_no.Value, ll_material_number, ll_supplier_number, ls_message)).Retrieve(out ls_message))
                                    {
                                        await this.ViewModel.uf_show_message("הודעה", "", "OK", Convert.ToString(" הפריט התווסף לחשבונית " + ll_doc_no.ToString() + " של אותו ספק במשלוח "));
                                    }
                                    else
                                    {
                                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כשלון בהוספת פריט לחשבונית של אותו ספק");
                                        return 1;
                                    }

                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no_en", 1);
                                    return 1;
                                    //NO
                                }
                                else
                                {
                                    // Create decline on one of the invoices in pallet
                                    // AlexKh - 1.1.27.0 - 2015-02-04 - SPUCM00005224 - replace message

                                    //gilad - items removed
                                    //gilad - items removed
                                    li_rtn_code = await this.ViewModel.uf_show_message("הודעה", "", "OK", "עודף –לא נקלט למלאי,יש למסור לנהג(לא להקליד החזרה)");

                                    //gilad - items removed
                                    (await this.ViewModel.invuo_doc_item_searchProperty.uf_create_decline_for_item(ll_material_number, this.ViewModel.il_shipment_numberProperty, this.ViewModel.is_pallet_numberProperty, ll_supplier_number, this.ViewModel.il_marlog_branchProperty, ls_message, this.ViewModel.il_marlog_distributorProperty)).Retrieve(out ls_message);

                                    //gilad - items removed

                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no_en", 1);
                                    return 1;
                                }
                            }
                            if (ll_ret == 2)
                            {
                                // AlexKh - 1.1.28.0 - 2015-02-08 - SPUCM00005224 - replace message
                                // Create decline on one of the invoices in pallet
                                //li_rtn_code = uf_show_message("הודעה", "", "YesNo", "תדווח הסתייגות  על הפריט הזה, האם להמשיך?")
                                li_rtn_code = await this.ViewModel.uf_show_message("הודעה", "", "OK", "עודף –לא נקלט למלאי,יש למסור לנהג(לא להקליד החזרה)");

                                //gilad - items removed
                                (await this.ViewModel.invuo_doc_item_searchProperty.uf_create_decline_for_item(ll_material_number, this.ViewModel.il_shipment_numberProperty, this.ViewModel.is_pallet_numberProperty, ll_supplier_number, this.ViewModel.il_marlog_branchProperty, ls_message, this.ViewModel.il_marlog_distributorProperty)).Retrieve(out ls_message);

                                //gilad - items removed

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no_en", 1);
                                return 1;
                            }
                            if (ll_ret == 1)
                            {
                                if (!isempty_stringClass.isempty_string(ls_message))
                                {
                                    await this.ViewModel.uf_show_message("שים לב", "", "OK", Convert.ToString(ls_message));

                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no_en", 1);
                                }
                                return 1;
                            }
                            if (ll_ret == 0)
                            {
                                await this.ViewModel.uf_show_message("שים לב", "", "OK", "הפריט לא שייך למשטח");

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no_en", 1);
                                return 1;
                            }
                        }
                        await this.ViewModel.uf_set_items("barcode", ls_barcode, row);
                        if (ll_actual_quantity > 0)
                        {
                            await this.ViewModel.uf_set_items("expected_material_quantity", ll_actual_quantity.ToString(), row);
                        }

                        //gilad 
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_set_items of ItemChanged: CASE barcode ", "", "");
                        await this.ViewModel.uf_enter(ls_col_name, row);
                        //gilad
                        object val1 = this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex);
                        if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex)) != Convert.ToString(val1))
                        {
                            this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, val1);
                        }
                        break;
                    case "expected_material_quantity":


                        if (string.IsNullOrEmpty(this.ViewModel.is_new_barcode))
                        {

                            this.ViewModel.is_new_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                            this.ViewModel.is_old_barcode = this.ViewModel.is_new_barcode;
                        }
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "Start CASE expected_material_quantity ", Convert.ToString("Row:      " + row.ToString()), Convert.ToString("The Data: " + data));

                        ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            await this.ViewModel.uf_show_message("", "טעות", "OK", Convert.ToString(masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("יש להכניס ברקוד תחילה").Result));

                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "expected_material_quantity", 0);
                            return 2;
                        }
                        // Check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        ll_material_number = await this.ViewModel.iuo_material_search.uf_get_material_number(ls_barcode);
                        // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

                        //gilad - items removed

                        ldec_expected_material_quantity = this.ViewModel.dw_inv_pack_details.GetItemValue<decimal?>(row, "expected_material_quantity");

                        //gilad - items removed
                        // AlexKh 1.1.1.1 - 2013-02-26 - SPUCM00004018 - set 0 in case of null
                        if (ldec_expected_material_quantity == null)
                        {
                            ldec_expected_material_quantity = 0;
                        }
                        // AlexKh - 1.2.48.0.13 - 2011-11-07 - Fix for pallets - Handle expiration date
                        //gilad  check  data !IsNullOrEmpty
                        if (!string.IsNullOrEmpty(data) &&Convert.ToInt64(data) > 0)
                        {
                            if (await this.ViewModel.uf_handle_expiration_date(row) < 0)
                            {

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.ViewModel.dw_inv_pack_details).SetItem(row, "expected_material_quantity", 0);
                                return 2;
                            }
                        }

                        if (ldis_status != ModelAction.Insert)
                        {
                            this.ViewModel.idec_item_quantity_change_differenceProperty += Convert.ToDecimal(data) - ldec_expected_material_quantity.Value;

                            //gilad 



                            if (!string.IsNullOrEmpty(data) && Convert.ToDecimal(data) != 0 && ldec_expected_material_quantity != 0 && Convert.ToDecimal(data) != ldec_expected_material_quantity)
                            {

                                ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                                ll_material_number = await this.ViewModel.iuo_material_search.uf_get_material_number(ls_barcode);

                                long invoice_number = Convert.ToInt64(this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(row, "doc_no"));

                                ViewModel.UpdateData3(Convert.ToDecimal(data), this.ViewModel.il_supplier_numberProperty, invoice_number, ll_material_number);


                            }


                        }
                        // Check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "After uf_set_items of ItemChanged: CASE expected_material_quantity ", "", "");
                        // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
                        //GalilCS - value will updated after run uf_choose_invoice_number because in source we have a post call
                        this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, e.Value);
                        await uf_choose_invoice_number(row, ll_material_number, ls_barcode);
                        //gilad  insted of dw_inv_pack_details_rowfocuschanging
                        //---------------start----
                        val1 = this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex);
                        if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex)) != Convert.ToString(val1))
                        {
                            this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, val1);
                        }

                        long? ll_invoice_number = 0;
                        ls_barcode = null;
                        ModelAction ldw_row_status = default(ModelAction);
                        object currentrow = null;
                        if (this.ViewModel.dw_inv_pack_details.PreviousRow != null)
                        {
                            currentrow = this.ViewModel.dw_inv_pack_details.PreviousRow.Index;
                        }
                        if (currentrow == null || Convert.ToInt32(currentrow) == -1)
                        {
                            return 0;
                        }


                        ldw_row_status = this.ViewModel.dw_inv_pack_details.GetItemStatus((int)currentrow, 0, ModelBuffer.Primary);
                        if (ldw_row_status == ModelAction.UpdateByKeys || ldw_row_status == ModelAction.Insert)
                        {
                            ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(currentrow), "barcode");
                            if (!isempty_stringClass.isempty_string(ls_barcode))
                            {
                                await this.uf_update_item((int)Convert.ToInt64(currentrow));
                            }
                        }
                        object newrow = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
                        if (Convert.ToInt32(newrow) >= 0)
                        {

                            ll_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(Convert.ToInt64(newrow), "doc_no");
                            if (ll_invoice_number > 0)
                            {
                                this.ViewModel.uf_set_doc_no(Convert.ToInt64(newrow), ll_invoice_number.Value, false);
                            }
                        }


                        //---------------end--------




                        break;
                    case "doc_no":
                        if (this.ViewModel.is_new_barcode == null || string.IsNullOrEmpty(this.ViewModel.is_new_barcode))
                        {

                            this.ViewModel.is_new_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                            this.ViewModel.is_old_barcode = this.ViewModel.is_new_barcode;
                        }

                        ll_current_doc_no = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(row, "doc_no");
                        ll_current_doc_no = ll_current_doc_no == null ? 0 : ll_current_doc_no.Value;
                        this.ViewModel.il_old_doc_no = ll_current_doc_no.Value;
                        ll_new_doc_no = Convert.ToInt64(data);
                        this.ViewModel.il_new_doc_no = ll_new_doc_no;


                        ldis_status = this.ViewModel.dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);


                        //gilad - items removed
                        lc_doc_type = this.ViewModel.uf_get_doc_type(Convert.ToInt64(data)).Result;
                        this.ViewModel.ic_new_doc_type = lc_doc_type;
                        char lc_curr_doc_type = '\0';
                        if (!(ll_current_doc_no == 0))
                        {
                            lc_curr_doc_type = this.ViewModel.uf_get_doc_type(ll_current_doc_no.Value).Result;
                            this.ViewModel.ic_old_doc_type = lc_curr_doc_type;
                        }

                        ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        // If new item does't belong to invoice, then give a message

                        ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(row, "material_number");
                        li_status = await this.ViewModel.uf_is_item_in_invoice(ll_new_doc_no, ll_material_number);
                        if (li_status == -1)
                        {

                            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                            (this.ViewModel.dw_inv_pack_details).SetItem(row, "doc_no", ll_current_doc_no);
                            Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e5) =>
                            {
                                this.ViewModel.dw_inv_pack_details.SetFocus(row, "doc_no");
                            });
                            return 2;
                        }
                        // Check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }

                        this.ViewModel.uf_prepare_material_details(ll_material_number, this.ViewModel.dw_inv_pack_details.GetItemValue<decimal>(row, "expected_material_quantity"), this.ViewModel.il_new_doc_no, row);
                        await this.ViewModel.uf_set_items("doc_no", data, row);
                        //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "End of ItemChanged: CASE doc_no ", "", "");
                        break;
                    default:
                        break;
                }
                if (this.ViewModel.uf_enable_cb_finish(true) < 1)
                {
                    return 2;
                }
                //this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.ItemChanged", "End of ItemChanged", "", "");
                //gilad
                object val = this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex);
                if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex)) != Convert.ToString(val))
                {
                    this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, val);
                }

                return 0;
            }
            finally
            {
                //GalilCS   Update value in repository

                //gilad this part move up

                //object val = this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex);
                //if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex)) != Convert.ToString(val))
                //{
                //    this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, val);
                //}
                //gilad end move up 
                //Tuple<string, GridElementCellEventArgs> elemTag = elem.Tag as Tuple<string, GridElementCellEventArgs>;
                //if (elemTag != null && elemTag.Item1 == "FireKeyDownAfterWidgetControlItemChanged")
                //{
                //    elem.PerformCellkeyDown(elemTag.Item2);
                //}
                elem.Tag = null;
            }
        }
        public async Task cb_finish_clicked(object sender, EventArgs e)
        {
            await base.cb_finish_clicked(sender, e);
        }

        private async Task dw_inv_pack_details_CellkeyDown(object sender, GridElementCellEventArgs e)
        {

            GridElement elem = sender as GridElement;

            if (Convert.ToString(elem.Tag) == "FiringWidgetControlItemChanged")
            {
                elem.Tag = Tuple.Create<string, GridElementCellEventArgs>("FireKeyDownAfterWidgetControlItemChanged", e);
                return;
            }

            if (e.KeyCode == System.Web.VisualTree.Keys.Enter || e.KeyCode == System.Web.VisualTree.Keys.Tab)
            {
                await dw_inv_pack_details_ue_enter(this.ViewModel.dw_inv_pack_details, e);
            }
            else
            {
                dw_inv_pack_details_EditChanged(sender, new GridCellEventArgs(Convert.ToString(e.KeyChar), e.WidgetDataMember));
            }
        }

        public async Task dw_inv_pack_details_clicked(object sender, EventArgs e)
        {
            int row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
            await uf_filter_docs_list(row);
            await base.dw_inv_pack_details_clicked(0, 0, row, this.ViewModel.dw_inv_pack_details);
        }



        public async Task dw_inv_pack_details_doubleclicked(object sender, GridCellEventArgs e) //**********************************************************************************************
        {
            int row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
            string dwoID = e.DataIndex;
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			dw_inv_pack_details.doubleclicked
            //*Purpose:							set instance values
            //*Arguments:							None
            //*Return:								None
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-08-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long? ll_invoice_number = 0;
            if (row >= 0)
            {

                ll_invoice_number = this.ViewModel.dw_inv_pack_details.GetItemValue<Int64?>(row, "doc_no");
                if (ll_invoice_number != null && ll_invoice_number > 0)
                {
                    this.ViewModel.uf_set_doc_no(row, ll_invoice_number.Value, false);
                }
            }




            await base.dw_inv_pack_details_doubleclicked(0, 0, this.ViewModel.dw_inv_pack_details.SelectedRowIndex, this.ViewModel.dw_inv_pack_details, dwoID);


        }

        public void dw_inv_pack_details_EditChanged(object sender, GridCellEventArgs e)
        {
            base.dw_inv_pack_details_editchanged(sender, e);
        }

        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_item_diff_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            ButtonElement cb_item_diff = this.GetVisualElementById<ButtonElement>("cb_item_diff");
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			cb_item_diff.clicked event
            //*Purpose:							Present current item with all it's invoices
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*6-09-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            //Set the text of the button
            if (cb_item_diff.Text == "הצג את כל הפריטים")
            {
                //Show all items
                await this.ViewModel.uf_show_items(true);
                cb_item_diff.Text = "הצג פריט";
            }
            else
            {
                //Show item invoices
                await this.uf_show_item_invoices();
            }

        }
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async Task cb_not_scanned_items_clicked(object sender, EventArgs e) //********************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			cb_not_scanned_items.clicked event
            //*Purpose:							Items that were not scanned yet
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#				Description
            //*---------------------------------------------------------------------------------
            //*24-01-2011		AlexKh			   1.2.46.6  	SPUCM00002425 	Initial Version
            //*********************************************************************************

            string ls_params = null;
            // AlexKh - 1.1.1.20 - 2014-05-04 - CR#1171(SPUCM00004718) - set marlog branch_number
            //ls_params = String(gs_vars.branch_number) + "@" + String(il_shipment_number) + "@" + String(-1) + "@" + is_pallet_number +"@" 
            ls_params = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "@" + this.ViewModel.il_shipment_numberProperty.ToString() + "@" + "-1" + "@" + this.ViewModel.is_pallet_numberProperty + "@" + this.ViewModel.il_marlog_branchProperty.ToString() + "@";
            // Show differences between message and MT 

            await WindowHelper.Open<rw_diff_inv_mt_report>("global_global", "ls_params", ls_params);
            var al_row = this.ViewModel.dw_inv_pack_details.RowCount - 1;
            Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e1) =>
            {
                this.ViewModel.dw_inv_pack_details.SetFocus((int)al_row, "barcode");
            });
        }

        public async Task cb_delete_item_clicked(object sender, EventArgs e)
        {
            var row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
            if (row == -1)
            {
                return;
            }
            await uf_delete_item(row);

        }



    }
}
