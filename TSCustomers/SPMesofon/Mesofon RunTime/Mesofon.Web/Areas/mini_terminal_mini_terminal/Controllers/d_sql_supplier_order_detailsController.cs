using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_supplier_order_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_supplier_order_details()
		{
			d_sql_supplier_order_detailsRepository repository = new d_sql_supplier_order_detailsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
