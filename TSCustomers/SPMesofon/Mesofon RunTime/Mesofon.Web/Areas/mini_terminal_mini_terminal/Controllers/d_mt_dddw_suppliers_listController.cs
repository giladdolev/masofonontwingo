using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mt_dddw_suppliers_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mt_dddw_suppliers_list()
		{
			d_mt_dddw_suppliers_listRepository repository = new d_mt_dddw_suppliers_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
