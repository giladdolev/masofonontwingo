using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_driver_report_mainController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_driver_report_main()
		{
			d_mini_terminal_driver_report_mainRepository repository = new d_mini_terminal_driver_report_mainRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
