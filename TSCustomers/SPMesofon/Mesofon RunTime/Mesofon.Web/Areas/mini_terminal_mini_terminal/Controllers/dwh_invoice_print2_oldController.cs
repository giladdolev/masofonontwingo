using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_invoice_print2_oldController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_invoice_print2_old()
		{
			dwh_invoice_print2_oldRepository repository = new dwh_invoice_print2_oldRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
