﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofon;
using global;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;


namespace Mesofon.MiniTerminal.Controllers
{
	public class uo_mini_terminal_crossdoc_returns_detailsController : uo_mini_terminal_base_bodyController
	{

		public ActionResult uo_mini_terminal_crossdoc_returns_details()
		{
			return this.View(new uo_mini_terminal_crossdoc_returns_details());
		}
		private uo_mini_terminal_crossdoc_returns_details ViewModel
		{
			get { return this.GetRootVisualElement() as uo_mini_terminal_crossdoc_returns_details; }
		}

	    public void Form_Load(object sender, EventArgs e)
	    {
	        //this.ViewModel.constructor();
	    }
        public void ue_delete_row() //Ron@02/08/2015>>SPUCM00005419
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			if (this.ViewModel.il_delete_rowProperty > 0)
			{
				
				dw_inv_pack_details.Delete((int)this.ViewModel.il_delete_rowProperty);
				this.ViewModel.il_delete_rowProperty = 0;
			}
		}
		public async Task<bool> uf_update_item(long al_row_no) //********************************************************************
		{
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details");
			LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
			//	*Object:	            uo_mini_terminal_returns_details
			//	*Function Name:	   (uf_update_item)
			//	*Purpose: 	
			//	*Arguments: 	      long al_row_no
			//	*Return:	            boolean
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*19/08/2013	 AlexKh        1.1.1.	  CR#        Initial version
			//********************************************************************
			
			string ls_barcode = null;
			string ls_carton_number = null;
		//	bool lb_return_marlog = false;
			decimal ldc_qty = default(decimal);
			decimal ldc_curr_qty = default(decimal);
			decimal ldc_total_qty = default(decimal);
			decimal ldc_total_qty_1 = default(decimal);
			decimal ldec_qty = default(decimal);
			long ll_row_exists = 0;
			long ll_material_number = 0;
			long ll_supplier = 0;
			long? ll_find = 0;
			long ll_i = 0;
			long ll_count = 0;
			long ll_row = 0;
			long? ll_find_summary = 0;
			long ll_supplier_1 = 0;
			long ll_ret = 0;
			int li_cnt = 0;
			DateTime? ldt_time = null;
		    ldt_time = masofonAlias.masofon.Instance.set_machine_time();
			if (this.ViewModel.il_return_number == 0)
			{
				this.ViewModel.il_return_number = await f_calculate_datatable_rowClass.f_calculate_datatable_row(144, 0, masofonAlias.masofon.Instance.sqlca);
				st_shipment_number.Text = this.ViewModel.il_return_number.ToString();
				this.ViewModel.ib_new_returnProperty = true;
			}
			//basic data
			
			ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row_no, "barcode");
			
			ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_quantity");
			
			ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "material_number");
			
			ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number");
			ls_carton_number = dw_inv_pack_details.GetItemValue<string>(al_row_no, "carton_barcode");
			//first check if the barcode already appears on the dw
			
			
			ll_find = dw_inv_pack_details.Find("barcode == \"" + ls_barcode + "\"" + "AND carton_barcode == \"" + ls_carton_number + "\"", 0, dw_inv_pack_details.RowCount());
			if (ll_find == al_row_no)
			{
				
				if (ll_find < dw_inv_pack_details.RowCount())
				{
					
					
					ll_find = dw_inv_pack_details.Find("barcode == \"" + ls_barcode + "\"" + "AND carton_barcode == \"" + ls_carton_number + "\"", (int)(ll_find + 1), dw_inv_pack_details.RowCount());
				}
				else
				{
					ll_find = 0;
				}
			}
			//if yes then first merge the rows into 1 row and continue
			if (ll_find > 0) //existing barcode; all data should appear (marlog or supplier)
			{
				//Release adhoc for merged row.
				
				
				if (this.ViewModel.ids_adhoc_update_listProperty.Retrieve(dw_inv_pack_details.GetItemValue<string>(al_row_no, "adhoc_key"), masofonAlias.masofon.Instance.gs_vars.branch_number, dw_inv_pack_details.GetItemValue<int>(al_row_no, "supplier_number")) > 0)
				{
					 
					this.ViewModel.ids_adhoc_update_listProperty.SetItem(0, "is_active", "1");
				    await f_update_adhocClass.f_update_adhoc(1, this.ViewModel.il_return_number, this.ViewModel.ids_adhoc_update_listProperty);
				}
				
				ldc_curr_qty = dw_inv_pack_details.GetItemValue<decimal>(ll_find.Value, "material_quantity");
				ldc_qty += ldc_curr_qty;
				
				dw_inv_pack_details.SetItem(ll_find.Value, "material_quantity", ldc_qty.ToString());
				// AlexKh
				
				
				
				dw_inv_pack_details.SetItemStatus((int)ll_find, 0, ModelBuffer.Primary, ModelAction.None);
				//add and remark - Ron@02/08/2015>>SPUCM00005419 - start
				//the remark is because the delete makes PB crash, because its delete in rowfocuschanging so I just makes the row invisible' anyway it will not be saved to the DB
				this.ViewModel.il_delete_rowProperty = al_row_no;
				// dw_inv_pack_details.Delete( il_delete_row) 
				//dw_inv_pack_details.SetItem(al_row_no,"visible", 0)
				//dw_inv_pack_details.Delete( al_row_no) //???
				//add and remark - Ron@02/08/2015>>SPUCM00005419 - end
				al_row_no = ll_find.Value;
				if (this.ViewModel.ib_marlog_returnProperty) //for marlog update dw_summary existing row
				{
					ldc_total_qty_1 = 0;
					
					for (li_cnt = 1; li_cnt <= dw_inv_pack_details.RowCount(); li_cnt++)
					{
						
						ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(li_cnt, "supplier_number");
						if (ll_supplier_1 == 0 || ll_supplier_1 != ll_supplier)
						{
							continue;
						}
						
						ldc_total_qty_1 += dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_quantity");
					}
					
					ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(ll_find.Value, "supplier_number");
					
					
					ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier_1.ToString(), 0, dw_summary.RowCount());
					
					dw_summary.SetItem(ll_find_summary.Value, "total", ldc_total_qty_1.ToString());
					//for supplier dw_inv_pack_details already updated with ldc_qty
				}
				else
				{
					ldc_total_qty = ldc_qty;
				}
                //UPDATE tables
			    await uf_update_tables(al_row_no, ldc_qty, 1);
				//only row for barcode
			}
			else
			{
				
				ldc_curr_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_quantity");
				if (this.ViewModel.ib_marlog_returnProperty) //return to Marlog
				{
					//search dw_summary and add this supplier qty to a new/existing row
					
					
					ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier.ToString(), 1, dw_summary.RowCount());
					if (ll_find_summary > 0) //Marlog, new barcode, existing supplier
					{
						//update existing row
						ldc_total_qty_1 = 0;
						
						for (li_cnt = 1; li_cnt <= dw_inv_pack_details.RowCount(); li_cnt++)
						{
							
							ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(li_cnt, "supplier_number");
							if (ll_supplier_1 != ll_supplier)
							{
								continue;
							}
							
							ldec_qty = dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_quantity");
							if (ldec_qty == 0)
							{
								ldec_qty = 0;
							}
							ldc_total_qty_1 += ldec_qty;
						}
						
						dw_summary.SetItem(ll_find_summary.Value, "total", ldc_total_qty_1.ToString());
						
						
						dw_inv_pack_details.SetItem(al_row_no, "doc_no", dw_summary.GetItemValue<int>(ll_find_summary.Value, "doc_no").ToString());
						// AlexKh - 1.1.1.11 - 09/09/2013 - SPUCM00004291 - Check rowstatus instead row number			
						
						
						ModelAction ldw_row_status = dw_inv_pack_details.GetItemStatus((int)al_row_no, 0, ModelBuffer.Primary);
						
						
						if (ldw_row_status == ModelAction.Insert || ldw_row_status == ModelAction.Insert)
						{
							//IF al_row_no = dw_inv_pack_details.RowCount() THEN //New row added
							//UPDATE tables
							if ( await uf_update_tables(al_row_no, ldc_curr_qty, 2) == -1)
							{
								return false;
							}
							//Update existing row
						}
						else
						{
							//UPDATE tables
							if (await uf_update_tables(al_row_no, ldc_curr_qty, 1) == -1)
							{
								return false;
							}
						}
					}
					else
					{
						//Marlog, new barcode, new supplier
						//	il_return_son = f_calculate_datatable_row(145, 0, SQLCA) //Remark //Ron@06/08/2015>>SPUCM00005419
						//Ron@06/08/2015>>SPUCM00005419
						this.ViewModel.il_return_sonProperty = this.ViewModel.il_return_number;
						if ( await uf_update_tables(al_row_no, ldc_curr_qty, 3) == -1)
						{
							return false;
						}
						//open new row; create return number; set new quantity = new
						
						ll_row = dw_summary.Insert(0);
						
						dw_summary.SetItem(ll_row, "doc_no", this.ViewModel.il_return_sonProperty.ToString());
						
						dw_summary.SetItem(ll_row, "supplier_number", ll_supplier.ToString());
						
						dw_summary.SetItem(ll_row, "total", ldc_curr_qty.ToString());
					}
				}
				else
				{
				    ViewModel.LoadData1(ll_supplier, ref ll_row_exists, ref ldc_qty);
					if (ll_row_exists > 0)
					{
						//ldc_total_qty = ldc_qty + ldc_curr_qty
						if ( await uf_update_tables(al_row_no, ldc_curr_qty, 2) == -1)
						{
							return false;
						}
					}
					else
					{
						if ( await uf_update_tables(al_row_no, ldc_curr_qty, 3) == -1)
						{
							return false;
						}
					}
				}
				// AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row
				
				
				
				dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None);
			}
			if (!await this.ViewModel.uf_is_new_row_exist(dw_inv_pack_details))
			{
				// AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row
				await this.ViewModel.uf_new_row(dw_inv_pack_details);
				//dw_inv_pack_details.TRIGGER EVENT ue_new_row()
			}

            ue_delete_row();  //Ron@02/08/2015>>SPUCM00005419
            return true;
		}
		public bool uf_print_carton_barcode(string as_barcode) //waiting for code from Eduard
		{
			return false;
		}
		public async Task<bool> uf_set_material_prices(long al_material_number, long row, EventArgs e)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			decimal vat_percentage = default(decimal);
			decimal ldec_active_price = default(decimal);
			decimal ld_global_discount_percent = default(decimal);
			decimal ldec_material_discount = default(decimal);
			decimal ldec_supplier_discount = default(decimal);
			decimal ldec_percent = default(decimal);
			decimal ldec_price_before = default(decimal);
			decimal ldec_price_after = default(decimal);
			decimal ldec_sell_price = default(decimal);
			long[] la_suppliers = new long[6];
			long ll_index = 0;
			long ll_first_free = 0;
			long vat_number = 0;
			string ls_syntax = null;
			string ls_material_name = null; //, ls_discount_amount
			bool lb_ok = false;
			
			dw_inv_pack_details.PerformValidated(e);
			if (true)
			{
			    ViewModel.LoadData2(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				
				await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
				return false;
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100) // get the last prices received 
			{
			    ViewModel.LoadData3(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
				{
					
					await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
					return false;
				}
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{
				ldec_price_before = 0;
				ldec_price_after = 0;
				ldec_active_price = 0;
			}
			else
			{
				if (ldec_price_before == 0)
				{
					ldec_price_before = 0;
				}
				if (ldec_price_after == 0)
				{
					ldec_price_after = 0;
				}
				if (ldec_material_discount == 0)
				{
					ldec_material_discount = 0;
				}
				if (ldec_supplier_discount == 0)
				{
					ldec_supplier_discount = 0;
				}
				if (ldec_price_after == 0 && (ldec_price_before != 0 && (ldec_material_discount == 0 && ldec_supplier_discount == 0)))
				{
					ldec_price_after = ldec_price_before;
				}
				ldec_active_price = ldec_price_after;
			}
					dw_inv_pack_details.SetItem(row, "material_price", ldec_active_price.ToString());
			if (true)
			{
			    ViewModel.LoadData4(al_material_number, ref ldec_sell_price);
			}
			
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0 || masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
			{
				
				await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
				return false;
			}
			
			dw_inv_pack_details.SetItem(row, "sell_price", ldec_sell_price.ToString());
			return true;
		}
		public bool uf_set_total(long al_supplier, ref decimal adc_total)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			decimal ldc_price = default(decimal);
			decimal ldc_qty = default(decimal);
			long ll_supplier = 0;
			int li_cnt = 0;
			//SELECT 
			
			for (li_cnt = 1; li_cnt <= dw_inv_pack_details.RowCount(); li_cnt++)
			{
				
				ll_supplier = dw_inv_pack_details.GetItemValue<long>(li_cnt, "supplier_number");
				if (ll_supplier != al_supplier)
				{
					continue;
				}
				
				ldc_price = dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_price");
				
				ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_quantity");
				// AlexKh 1.1.1.10 - 2013-06-30 - CR#1160 - check null values
				if (ldc_price == 0)
				{
					ldc_price = 0;
				}
				if (ldc_qty == 0)
				{
					ldc_qty = 0;
				}
				adc_total += ldc_price * ldc_qty;
			}
			return true;
		}
		public async Task<bool> uf_delete_item(long al_row) //String	ls_barcode
		{
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//Boolean	lb_return_marlog
			decimal ldc_qty = default(decimal);
			decimal ldc_total_qty = default(decimal);
			long ll_supplier = 0;
			long ll_i = 0;
			long? ll_find_summary = 0;
			long ll_supplier_1 = 0;
			//basic data
			
			ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row, "supplier_number");
			
			ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_quantity");
			//find dw_summary row for supplier - re-calc row
			
			
			ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier.ToString(), 0, dw_summary.RowCount());
			if (ll_find_summary >= 0)
			{
				
				ldc_total_qty = dw_summary.GetItemValue<decimal>(ll_find_summary.Value, "total");
				//re-set total for summary
				
				dw_summary.SetItem(ll_find_summary.Value, "total", (ldc_total_qty - ldc_qty).ToString());
                //actual deletion of the row in the calling function
			    await uf_update_tables(al_row, ldc_total_qty - ldc_qty, 4);
			}
			return true;
		}
		public bool uf_check_marlog_distributer(long al_distributor_number)
		{
			bool lb_marlog = false;
			 
			 
			lb_marlog = Convert.ToBoolean((this.ViewModel.Parent.Parent.Parent as w_mini_terminal).wf_check_marlog_distributor(-2, "MARLOG_YES", al_distributor_number));
			return lb_marlog;
		}
		public async Task<int> uf_close_window() //********************************************************************
		{
			//*Object:				uo_mini_terminal_returns
			//*Function Name:	uf_close_window
			//*Purpose: 			close window
			//*Arguments: 		none
			//*Return:				None
			//*Date				Programer		Version	Task#	 		Description
			//*---------------------------------------------------------------------
			//*27-06-2013		AlexKh			1.1.1.10	CR#1160		Initial version
			//********************************************************************
			
			string ls_message = null;
			int li_rtn_code = 0;
			ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
			li_rtn_code = this.ViewModel.uf_write_log(Convert.ToString(ls_message));
            await this.ViewModel.uf_display("ENTRANCE");
			return 1;
		}
		public async Task< int> uf_enter(string as_column, long al_row)
		{
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details");
			long ll_doc_no = 0;
			long ll_doc_no_en = 0;
			long ll_row_no = 0;
			long ll_cntr = 0;
			long ll_serial_number = 0;
			long ll_RowCount = 0;
			long ll_row = 0;
			long ll_material_number = 0;
			long ll_material_quantity = 0;
			string ls_barcode = null;
			string ls_b2b_status = null;
			string ls_string = null;
		    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Start. Column: " + as_column), Convert.ToString("Row:      " + al_row.ToString()), "");
			f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_inv_pack_details);
			
			if (dw_inv_pack_details.RowCount() < 1)
			{
				return 0;
			}
			switch (as_column) {
				case "barcode":
					ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
				    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Barcode:  " + ls_barcode), "");
					if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
					{
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "barcode");
					}
					else
					{
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "material_quantity");
					}
					break;
				case "material_quantity":
					
					ll_row_no = dw_inv_pack_details.GetItemValue<long>(al_row, "row_no");
				    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Row no:   " + ll_row_no.ToString()), "");
					// SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - Check if the quantity is null
					
					ll_material_quantity = dw_inv_pack_details.GetItemValue<long>(al_row, "material_quantity");
					if (ll_material_quantity == 0 || ll_material_quantity == 0)
					{
                        //.. כמות בפעול לא יכולה להיות ריקה
                        await this.ViewModel.uf_show_message("", "", "OK", "#20011");
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "material_quantity");
						return 0;
					}
					// End
					// SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - If at least 1 doc is not b2b, set focus to the doc_no field
					
					ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row, "material_number");
					
					ll_RowCount = dw_inv_pack_details.RowCount();
					this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "declines");
					return 1;
				
				case "declines":
				    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), "", "");
					//Check if decline is null - the user did not choose a decline reason.
					
					ll_doc_no = dw_inv_pack_details.GetItemValue<long>(al_row, "declines");
					if (ll_doc_no == 0 || ll_doc_no == 0)
					{
                        await this.ViewModel.uf_show_message("שים לב", "", "OK", "נא לבחור סיבה להחזרה");
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "declines");
						return 1;
					}
					// End
					if (!await this.ViewModel.uf_is_new_row_exist(dw_inv_pack_details))
					{
                        // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row
                        await this.ViewModel.uf_new_row(dw_inv_pack_details);

					}
					else
					{
						
						ll_RowCount = dw_inv_pack_details.RowCount();
					    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("RowCount: " + ll_RowCount.ToString()), "");
						for (ll_cntr = 1; ll_cntr <= ll_RowCount; ll_cntr++)
						{
							ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_cntr, "barcode");
							if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
							{
								ll_row = ll_cntr;
								break; // TODO: might not be correct. Was : Exit For
							}
						}
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", ll_row, "barcode");
					}
					break;
				default:
					break;
			}
			return 1;
		}
		protected async Task<int> uf_update_tables(long al_row_no, decimal adc_qty, int al_case)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			int li_rc = 0;
			long ll_material_number = 0;
			long ll_supplier = 0;
			long ll_serial = 0;
			long ll_details_serial = 0;
			long ll_marlog_serial = 0;
			long ll_invoice = 0;
			long ll_distributor = 0;
			long ll_decline_number = 0;
			long ll_return_son = 0;
			long ll_return_serial = 0;
			long ll_row_serial_number = 0;
			decimal ldc_price = default(decimal);
			decimal ldc_total = default(decimal);
			decimal ldc_sell_price = default(decimal);
			DateTime? ldt_time = null;
			string ls_adhoc_key = null;
			ldt_time = masofonAlias.masofon.Instance.set_machine_time();
			
			ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "material_number");
			
			ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number");
			
			ll_distributor = dw_inv_pack_details.GetItemValue<long>(al_row_no, "distributor_number");
			ls_adhoc_key = dw_inv_pack_details.GetItemValue<string>(al_row_no, "adhoc_key");
			
			ll_decline_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "declines");
			//ll_return_son = dw_inv_pack_details.GetItem<int>( al_row_no, "doc_no") //Remark //Ron@06/08/2015>>SPUCM00005419
			//Ron@06/08/2015>>SPUCM00005419 - start
			ll_return_son = this.ViewModel.il_return_number;
			this.ViewModel.il_return_sonProperty = this.ViewModel.il_return_number;
            //Ron@06/08/2015>>SPUCM00005419 - end
            //ll_invoice = dw_inv_pack_details.GetItem<int>( al_row_no, "serial_number")
            EventArgs e = new EventArgs();
		    await uf_set_material_prices(ll_material_number, al_row_no,e);
			
			ldc_price = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_price");
			
			ldc_sell_price = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "sell_price");
			// AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - get row serial number
			
			ll_row_serial_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "row_serial_number");
            uf_set_total(ll_supplier, ref ldc_total);
			if (this.ViewModel.ib_new_returnProperty && (al_case == 1 || al_case == 2))
			{
				al_case = 3;
				this.ViewModel.ib_new_returnProperty = false;
			}
		    await f_begin_tranClass.f_begin_tran();
			switch (al_case) {
				case 1:
					//scenario 1: 
					//existing barcode, All Update
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - update invoice_details also by row_serial_number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - update by marlog_number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					break;
				case 2:
					//scenario 2: 
					//existing supplier, Update invoice_move, Insert invoice_details, Insert shipment_marlog_return
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
					// AlexKh - 1.1.23.0 - 2014-09-04 - SPUCM00004984 - use valid numerator for invoice_details table
					//ll_serial = f_calculate_datatable_row(143, 0, SQLCA)
					ll_serial = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(masofonAlias.masofon.Instance.gs_vars.branch_number, 4);
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
					ll_return_serial = await f_calculate_datatable_rowClass.f_calculate_datatable_row(146, 0, masofonAlias.masofon.Instance.sqlca);
					// AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
					//marlog number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					
					li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "row_serial_number", ll_serial.ToString())); //RonY@06/08/2015  - SPUCM00005419
					
                    if (!await f_update_adhocClass.f_update_adhoc(0, dw_inv_pack_details.GetItemValue<long>(al_row_no, "serial_number"),  this.ViewModel.ids_adhoc_update_listProperty))
					{
						goto error;
					}
					break;
				case 3:
					//scenario 3: 
					//non existing, All Insert
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.25.0 - 2014-21-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
					// AlexKh - 1.1.23.0 - 2014-09-04 - SPUCM00004984 - use valid numerator for invoice_details table
					//ll_serial = f_calculate_datatable_row(143, 0, SQLCA)
					ll_serial = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(masofonAlias.masofon.Instance.gs_vars.branch_number, 4);
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					
					li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "doc_no", this.ViewModel.il_return_sonProperty.ToString())); //RonY@26/03/2015  - SPUCM00005270
					
					li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "row_serial_number", ll_serial.ToString())); //RonY@26/03/2015  - SPUCM00005270
					// AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
					// AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
					ll_return_serial = await f_calculate_datatable_rowClass.f_calculate_datatable_row(146, 0, masofonAlias.masofon.Instance.sqlca);
					//marlog number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					
                    if (!await f_update_adhocClass.f_update_adhoc(0, dw_inv_pack_details.GetItemValue<long>(al_row_no, "serial_number"),  this.ViewModel.ids_adhoc_update_listProperty))
					{
						goto error;
					}
					break;
				case 4:
					//deleted row
					if (adc_qty <= 0)
					{
					    ViewModel.UpdateData2(ldt_time, ll_supplier, ll_return_son);
						
						if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
						{
							goto error;
						}
					}
					// AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - update invoice_details also by row_serial_number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					// AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
					{
						goto error;
					}
					break;
				default:
					break;
			}
			this.ViewModel.il_return_son_for_displayProperty = this.ViewModel.il_return_sonProperty; //Ron@05/05/2015>>SPUCM00005327
		    await f_commitClass.f_commit();
			
			
			
			dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None); //RonY@26/03/2015  - SPUCM00005270
			
			
			
			dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None); //RonY@26/03/2015  - SPUCM00005270
			this.ViewModel.ib_new_returnProperty = false; //Ron@10/08/2015
			return 1;
			error:
		    await f_rollbackClass.f_rollback();
            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "עדכון נתוני פריט נכשל");
			return -1;
		}
		public string uf_check_data(long al_row) //********************************************************************
		{
            EventArgs e = new EventArgs() ;

            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//*Object:	uo_mini_terminal_crossdoc_returns_details
			//*Function Name:	(uf_check_data)
			//*Purpose: 	
			//*Arguments: 	long al_row
			//*Return:	string
			//*Programmer	Date   Version	Task#	 Description
			//*---------------------------------------------------------------------
			//*RonY@12/03/2015 -  1.12.49.12 SPUCM0000
			//********************************************************************
			
			long ll_material_quantity = 0;
			long ll_decline_num = 0;
			string ls_barcode = null;
			string ls_material_name = null;
			
			dw_inv_pack_details.PerformValidated(e);
			
			ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
			if (isempty_stringClass.isempty_string(ls_barcode))
			{
				return "ברקוד";
			}
			ls_material_name = dw_inv_pack_details.GetItemValue<string>(al_row, "material_name");
			if (isempty_stringClass.isempty_string(ls_material_name))
			{
				return "שם פריט";
			}
			
			ll_material_quantity = dw_inv_pack_details.GetItemValue<long>(al_row, "material_quantity");
			if (ll_material_quantity == 0 || ll_material_quantity == 0)
			{
				return "כמות";
			}
			
			ll_decline_num = dw_inv_pack_details.GetItemValue<long>(al_row, "declines");
			if (ll_decline_num == 0 || ll_decline_num == 0)
			{
				return "סיבה";
			}
			return "";
		}
		public int uf_set_row_no() //********************************************************************
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//*Object:	uo_mini_terminal_crossdoc_returns_details
			//*Function Name:	(uf_set_row_no)
			//*Purpose: 	when the user delete a row there is a need to reorder the row no
			//*Arguments: 	(none)
			//*Return:	integer
			//*Programmer	Date   Version	Task#	 Description
			//*---------------------------------------------------------------------
			//*RonY@24/03/2015 -   SPUCM00005270
			//********************************************************************
			
			long ll_index = 0;
			long ll_rowcount = 0;
			
			ll_rowcount = dw_inv_pack_details.RowCount();
			for (ll_index = 1; ll_index <= ll_rowcount; ll_index++)
			{
				
				dw_inv_pack_details.SetItem(ll_index, "row_no", ll_index.ToString());
				
				
				
				dw_inv_pack_details.SetItemStatus((int)ll_index, 0, ModelBuffer.Primary, ModelAction.None); //RonY@26/03/2015  - SPUCM00005270
			}
			return 1;
		}
		public bool uf_return_adhoc_needed(long al_branch_number, long al_supplier_number, long al_distributor_number) //**********************************************************************************************
		{
			//*Object:				nvuo_trade_conditions
			//*Function Name:	uf_return_addhock_needed
			//*Purpose: 			Check if adhoc needed for return
			//*Arguments: 		Long 		- al_branch_number - branch number
			//*						Long 		- al_supplier_number - supplier number
			//*						Long 		- al_distributor_number - distributor number
			//*Return:				Boolean	TRUE/FALSE
			//*Date				Programer		Version	Task#	 					Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*1-12-2015		Eitan				39.3		SPUCM00005483			copied from office -> uf_okef_adhoc_needed
			//************************************************************************************************
			
			string ls_filter = null;
			string ls_value = null;
			long? ll_ret = 0;
			bool lb_return_adhoc_needed = false;
			if (al_distributor_number == 0)
			{
				al_distributor_number = 0;
			}
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.SetFilter("");
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.Filter();
			// AlexKh - 1.2.45.15 - 2010-02-16 - SPUCM00001944 - set -1 if no distributor
			if (al_distributor_number == 0 || al_distributor_number == 0)
			{
				al_distributor_number = -1;
			}
			ls_filter = "branch_number IN (0, " + al_branch_number.ToString() + ") AND supplier_number IN (0, " + al_supplier_number.ToString() + ") AND distributor_number IN (0, " + al_distributor_number.ToString() + ")";
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.SetFilter(ls_filter);
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.Filter();
			// AlexKh - 1.2.45.15 - 2010-02-16 - SPUCM00001944 - in addition set sort by distributor
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.SetSort("branch_number DESC, supplier_number DESC, distributor_number DESC");
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.Sort();
			
			
			ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.Find("distributor_number == " + al_distributor_number.ToString(), 1, this.ViewModel.ids_supplier_distributors_returnProperty.RowCount());
			if (ll_ret <= 0)
			{
				
				
				ll_ret = this.ViewModel.ids_supplier_distributors_returnProperty.Find("distributor_number == 0", 1, this.ViewModel.ids_supplier_distributors_returnProperty.RowCount());
			}
			if (ll_ret > 0)
			{
				 
				ls_value = this.ViewModel.ids_supplier_distributors_returnProperty.GetItemValue<string>(ll_ret.Value, "value");
				if (ls_value == "ADHOC_RETURN_YES")
				{
					lb_return_adhoc_needed = true;
				}
				else
				{
					lb_return_adhoc_needed = false;
				}
			}
			else
			{
				lb_return_adhoc_needed = false;
			}
			return lb_return_adhoc_needed;
		}
	
		public void destructor() //Eitan; SPUCM00005483; Build 39.3; 1/12/2015
		{
			base.destructor();
			if (this.ViewModel.ids_supplier_distributors_returnProperty != null)
			{

			}
		}
		
		public async Task<int> cb_delete_item_clicked(object sender, EventArgs e)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			long ll_row = 0;
			long ll_serial = 0;
			long ll_material_num = 0;
			string ls_doc_state = null;
			//uf_delete_null_values()
			//IF dw_inv_pack_details.RowCount() = 1 THEN
			//	uf_show_message("שגיאה", "", "OK", "לא ניתן למחוק שורה אחרונה בקרטון")
			//	IF NOT uf_is_new_row_exist() THEN
			//			dw_inv_pack_details.TRIGGER EVENT ue_new_row()	
			//	END IF
			//	dw_inv_pack_details.TRIGGER EVENT ue_new_row()
			//	RETURN
			//END IF
			
			ll_row = dw_inv_pack_details.GetRow();
			if (ll_row > 0)
			{
				//RonY@10/03/2015 1.12.49.12 - SPUCM00005247 - start
				
				ll_material_num = dw_inv_pack_details.GetItemValue<long>(ll_row, "material_number");
				if (ll_material_num == 0 || ll_material_num == 0)
				{
					
					dw_inv_pack_details.Delete((int)ll_row);
					return 1;
				}
				//RonY@10/03/2015 1.12.49.12 - SPUCM00005247 - end
				// AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row
				ls_doc_state = dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state");
				if (isempty_stringClass.isempty_string(ls_doc_state))
				{
					ls_doc_state = mini_terminal.uo_mini_terminal_crossdoc_returns_details.RETURN_STATE_OPEN.ToString();
				}
				//IF dw_inv_pack_details.GetItem<string>(ll_row, "doc_state") <> RETURN_STATE_OPEN OR IsEmpty_string(dw_inv_pack_details.GetItem<string>(ll_row, "doc_state")) THEN 
				if (dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state") != mini_terminal.uo_mini_terminal_crossdoc_returns_details.RETURN_STATE_OPEN.ToString())
				{
					//dw_inv_pack_details.TRIGGER EVENT ue_new_row()
					return 0;
				}
			}
			else
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא נבחרה שורה למחיקה");
				//dw_inv_pack_details.TRIGGER EVENT ue_new_row()
				return 0;
			}
			
			ll_serial = dw_inv_pack_details.GetItemValue<long>(ll_row, "serial_number");
			if (!(ll_serial == 0))
			{
                //dw_inv_pack_details.setitem( ll_row, "material_quantity", 0)
			    await uf_delete_item(ll_row);
			}
			
			
			if (this.ViewModel.ids_adhoc_update_listProperty.Retrieve(dw_inv_pack_details.GetItemValue<string>(ll_row, "adhoc_key"), masofonAlias.masofon.Instance.gs_vars.branch_number, dw_inv_pack_details.GetItemValue<int>(ll_row, "supplier_number")) > 0)
			{
				 
				this.ViewModel.ids_adhoc_update_listProperty.SetItem(0, "is_active", "1");
			    await f_update_adhocClass.f_update_adhoc(1, ll_serial,  this.ViewModel.ids_adhoc_update_listProperty);
			}
			
			dw_inv_pack_details.Delete((int)ll_row);
			uf_set_row_no(); //renumberd the row no ron@24/03/2015>>SPUCM00005270
			//IF NOT uf_is_new_row_exist() THEN
			//			dw_inv_pack_details.TRIGGER EVENT ue_new_row()	
			//END IF
            return 0;
		}
		
		public async Task<int> dw_inv_pack_details_itemchanged(int row, VisualElement dwo, string data) //********************************************************************
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//	*Object:	            dw_inv_pack_details
			//	*Event Name:	   (itemchanged)
			//	*Purpose: 	
			//	*Arguments: 	      long row, ControlElement dwo, string data
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*21/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
			//********************************************************************
			
			 
			
			
			
			dw_inv_pack_details.itemchanged(0, default(ControlElement), default(string));
			string ls_column = null;
			string ls_barcode = null;
			string ls_material_name = null;
			string ls_null = null;
			long ll_supplier = 0;
			long ll_quantity = 0;
			long ll_row = 0;
			long ll_temp = 0;
			long ll_return_son = 0;
			long ll_material_number = 0;
			long ll_distributor = 0;
			long ll_return_allowed = 0;
			long ll_msg = 0;
			long ll_row_exists = 0;
			long ll_declines = 0;
			long ll_find = 0;
			int li_len = 0;
			int li_index = 0;
			DateTime? ldt_time = null;
			DateTime? ldt_last_return_date = null;
			s_array_arguments lstr_parms = new s_array_arguments();
			
			ls_column = Convert.ToString(dwo.ID);
            if (row <= 0)
			{
				return 2;
			}
			if (this.ViewModel.il_supplierProperty == 0)
			{
				return 2;
			}
			switch (ls_column) {
				case "barcode":
					ldt_time = masofonAlias.masofon.Instance.set_machine_time();
					ls_barcode = data;
					// Get full barcode if it was particulaly inserted
					ls_barcode =await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
					// If barcode does not exist, drop leading zeros
					if (string.IsNullOrEmpty(ls_barcode))
					{
						li_len = data.Length;
						for (li_index = 1; li_index <= li_len; li_index++)
						{
							if (data.Substring(0, 1) == "0")
							{
								data = data.Substring(data.Length - (data.Length - 1));
								
								li_index++;
							}
							else
							{
								break; // TODO: might not be correct. Was : Exit For
							}
						}
						ls_barcode = data;
						ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
					}
					
					// AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - update barcode using POST
					
					dw_inv_pack_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
					// AlexKh - 1.1.40.0 - 2015-12-13 - add last_return_date
					//get supplier number for barcode from material and set	
					if (true)
					{
					    ViewModel.LoadData6(ls_barcode, ref ll_supplier, ref ls_material_name, ref ll_material_number, ref ll_distributor, ref ll_return_allowed, ref ldt_last_return_date);
					}
					if (ll_supplier != this.ViewModel.il_supplierProperty)
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך לספק שנבחר");
						
						dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
						return 2;
					}
					//RonY@12/03/2015 1.12.49.12 - SPUCM00005247 - end
					// AlexKh - 1.1.1.11 - 21/08/2013 - CR#1160 - add parameter to disable adhoc validation 		
					//Eitan; SPUCM00005483; Build 39.3; 1/12/2015
							if (uf_return_adhoc_needed(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier, ll_distributor))
					{
						//IF IsNull(ll_return_allowed) OR ll_return_allowed <> 1 THEN 
						if ((ll_return_allowed == 0 || ll_return_allowed != 1) || (ll_return_allowed == 1 && (!(ldt_last_return_date == default(DateTime)) && masofonAlias.masofon.Instance.set_machine_time() > ldt_last_return_date)))
						{
							ll_msg = await this.ViewModel.uf_show_message("התראה", "", "yesno", "פריט זה אינו מורשה להחזרה. האם תרצה לנצל אדהוק");
							if (ll_msg == 1) // yes
							{
								lstr_parms.a_long[1] = masofonAlias.masofon.Instance.gs_vars.branch_number;
								lstr_parms.a_long[2] = ll_supplier;
								lstr_parms.a_long[3] = this.ViewModel.il_return_number;
								lstr_parms.a_long[4] = ll_material_number; //item_number
								lstr_parms.a_long[6] = 0; //serial_number
								lstr_parms.a_long[5] = 0; // "adhoc_serial_number")
								lstr_parms.a_string[1] = ""; // "adhoc_key")
								lstr_parms.a_datastore[1] = this.ViewModel.ids_adhoc_update_listProperty;

                                await WindowHelper.Open<rw_addhock>("global_global", "args", lstr_parms);


                                lstr_parms = (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
								if (lstr_parms.a_boolean[0]) //succeed to update the adhoc status
								{
									if (row > 0) //update 
									{
										
										dw_inv_pack_details.SetItem(row, "adhoc_serial_number", lstr_parms.a_long[1].ToString());
										
										dw_inv_pack_details.SetItem(row, "adhoc_key", Convert.ToString(lstr_parms.a_string[1]));
									}
									this.ViewModel.ids_adhoc_update_listProperty = lstr_parms.a_datastore[1] as IRepository;
								}
								else
								{
									//failed to update the adhoc status
									
									dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
									return 2;
								}
							}
							else
							{
								
								dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
								return 2; // - not allowed
							}
						}
					}
					
					dw_inv_pack_details.SetItem(row, "serial_number", this.ViewModel.il_return_number.ToString());
					
					dw_inv_pack_details.SetItem(row, "carton_barcode", Convert.ToString(this.ViewModel.is_carton_barcode));
					
					dw_inv_pack_details.SetItem(row, "supplier_number", ll_supplier.ToString());
					
					dw_inv_pack_details.SetItem(row, "distributor_number", ll_distributor.ToString());
					
					dw_inv_pack_details.SetItem(row, "material_number", ll_material_number.ToString());
					
					dw_inv_pack_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
					
					dw_inv_pack_details.SetItem(row, "date_move", ldt_time.ToString());
					break;
				case "material_quantity":
					ls_barcode = dw_inv_pack_details.GetItemValue<string>(row, "barcode");
					if (isempty_stringClass.isempty_string(ls_barcode))
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "נא לסרוק ברקוד קודם");
						return 2;
					}
					
					dw_inv_pack_details.SetItem(row, "material_quantity", (Convert.ToDecimal(data)).ToString());
					break;
				case "declines":
					ls_barcode = dw_inv_pack_details.GetItemValue<string>(row, "barcode");
					
					ll_quantity = dw_inv_pack_details.GetItemValue<long>(row, "material_quantity");
					if (isempty_stringClass.isempty_string(ls_barcode) || ll_quantity <= 0)
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "חסרים נתוני ברקוד / כמויות");
						return 2;
					}
					
					dw_inv_pack_details.SetItem(row, "declines", Convert.ToInt64(data).ToString());
					break;
				default:
					break;
			}
            return 0;
		}
		
		public async Task<int> dw_inv_pack_details_ue_new_row()
		{
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
            return 0;
  
		}
        
        public async Task<int> dw_inv_pack_details_rowfocuschanging(int l1, int l2)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
                long ll_rtn_code = 0;
            long ll_decline_reason = 0;
            decimal ldc_qty = default(decimal);
            string ls_barcode = null;
            string ls_doc_state = null;
            ModelAction ldw_row_status = default(ModelAction);
            object currentrow = null;
            if (Convert.ToInt32(currentrow) == 0)
            {
                return 0;
            }


            ldw_row_status = dw_inv_pack_details.GetItemStatus((int)currentrow, 0, ModelBuffer.Primary);


            if (ldw_row_status == ModelAction.None || ldw_row_status == ModelAction.Insert)
            {
                ls_barcode = dw_inv_pack_details.GetItemValue<string>((long)currentrow, "barcode");

                ldc_qty = dw_inv_pack_details.GetItemValue<decimal>((long)currentrow, "material_quantity");

                ll_decline_reason = dw_inv_pack_details.GetItemValue<long>((long)currentrow, "declines");
                //The user tries to leave last empty row, it's can be done

                if (Convert.ToInt32(currentrow) == dw_inv_pack_details.RowCount() && (isempty_stringClass.isempty_string(ls_barcode) && ((ldc_qty == 0 || ldc_qty == 0) && (ll_decline_reason == 0 || ll_decline_reason <= 0))))
                {
                    return 0;
                }
                if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
                {
                    return 1;
                }
                if (ldc_qty == 0 || ldc_qty == 0)
                {
                    return 1;
                }
                if (ll_decline_reason <= 0)
                {
                    return 1;
                }
                await uf_update_item(Convert.ToInt64(currentrow));
            }
            return 0;
        }
        
        public int dw_inv_pack_details_itemerror(int row, VisualElement dwo, string data) //RonY@22/01/2015 1.12.49.12 - SPUCM00005162
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");




            dw_inv_pack_details.ItemError(0, dwo, data);
			return 1;
		}
		
		public async Task<int> dw_inv_pack_details_ue_enter(KeyEventArgs e)
		{
		    Keys key = e.KeyCode;
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			long ll_row = 0;
			long ll_rowcount = 0;
			string ls_col_name = null;
			string ls_message_rc = null;
			object keyflags = null;
			// TODO: Field 'Key of type 'Sybase.PowerBuilder.TrigEvent' is unmapped'. (CODE=1004)
			if (Convert.ToInt32(keyflags) == 0 && key == Keys.Enter || key == Keys.Tab)
			{
				
				dw_inv_pack_details.PerformValidated(e);
				
				ll_rowcount = dw_inv_pack_details.RowCount();
				
				ll_row = dw_inv_pack_details.GetRow();
				 
				ls_col_name = dw_inv_pack_details.GetColumnName();
				if (ls_col_name == "declines" && ll_row == ll_rowcount) //if its the last row and last col then insert new row and save current row
				{
					ls_message_rc = uf_check_data(ll_row);
					if (!string.IsNullOrEmpty(ls_message_rc))
					{
						await MessageBox.Show("חסר נתון בשדה " + ls_message_rc, "");
						return 1;
					}
					//			uf_save_data(ll_row)
					
					ll_row = dw_inv_pack_details.Insert(0);
					
					dw_inv_pack_details.SetItem(ll_row, "row_no", ll_row.ToString());
					 
					dw_inv_pack_details.SetColumn("barcode"); //Ron@02/08/2015
				}
				else
				{
					f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_inv_pack_details);
					return 1; //the return msut be here in order to prevent going to the next row ןin each tab
				}
			}
            return 0;
		}
		
		public void cb_print_clicked(object sender, EventArgs e) //Long		invoice_number, &
		{
			
			return;
		}
		
		public async Task<int> cb_exit_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//	*Object:	            cb_exit
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*22/08/2013	 AlexKh        1.1.1.11	  CR#1160 	 Initial version
			//********************************************************************
			
			long ll_decline_reason = 0;
			long ll_row = 0;
			decimal ldc_qty = default(decimal);
			string ls_barcode = null;
			ModelAction ldw_row_status = default(ModelAction);
			
			ll_row = dw_inv_pack_details.GetRow();
			if (ll_row == 0 || ll_row == 0)
			{
				return 0;
			}
			
			
			ldw_row_status = dw_inv_pack_details.GetItemStatus((int)ll_row, 0, ModelBuffer.Primary);
			
			
			if (ldw_row_status == ModelAction.None || ldw_row_status == ModelAction.Insert)
			{
				ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_row, "barcode");
				
				ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(ll_row, "material_quantity");
				
				ll_decline_reason = dw_inv_pack_details.GetItemValue<long>(ll_row, "declines");
				if (!isempty_stringClass.isempty_string(ls_barcode) && (ldc_qty > 0 && ll_decline_reason > 0))
				{
				    await uf_update_item(ll_row);
				}
			}
            await this.ViewModel.uf_display("CROSSDOC_RETURN");
			// AlexKh - 1.1.1.12 - 09/09/2013 - SPUCM00004291 - reset the window parameters
			this.ViewModel.il_return_number = 0;
			this.ViewModel.is_carton_barcode = "";
			
			dw_inv_pack_details.Reset();
			this.ViewModel.il_return_sonProperty = 0; //RonY@26/03/2015  - SPUCM00005270
            return 0;
		}

        public async Task dw_details_itemchanged(int row, VisualElement dwo, string data)
        {
            GridElement dw_details = this.GetVisualElementById<GridElement>("dw_details");
            string ls_column = null;
            string ls_barcode = null;
            string ls_material_name = null;
            long ll_supplier = 0;
            long ll_quantity = 0;
            int li_len = 0;
            int li_index = 0;

            ls_column = Convert.ToString(dwo.ID);
            switch (ls_column)
            {
                case "barcode":
                    ls_barcode = data;
                    // Get full barcode if it was particulaly inserted
                    ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    // If barcode does not exist, drop leading zeros
                    if (string.IsNullOrEmpty(ls_barcode))
                    {
                        li_len = data.Length;
                        for (li_index = 1; li_index <= li_len; li_index++)
                        {
                            if (data.Substring(0, 1) == "0")
                            {
                                data = data.Substring(data.Length - (data.Length - 1));

                                li_index++;
                            }
                            else
                            {
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        ls_barcode = data;
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    }

                    dw_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
                    //get supplier number for barcode from material and set
                    if (true)
                    {
                        ViewModel.LoadData7(ls_barcode, ref ll_supplier, ref ls_material_name);
                    }

                    dw_details.SetItem(row, "supplier_number", ll_supplier.ToString());

                    dw_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
                    break;
                case "material_quantity":
                    ll_quantity = Convert.ToInt64(data);

                    dw_details.SetItem(row, "material_quantity", ll_quantity.ToString());
                    break;
                default:
                    break;
            }
            
        }
    }
}
