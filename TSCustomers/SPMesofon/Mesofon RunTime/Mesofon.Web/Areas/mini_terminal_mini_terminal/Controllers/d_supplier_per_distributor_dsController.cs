using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_supplier_per_distributor_dsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_supplier_per_distributor_ds()
		{
			d_supplier_per_distributor_dsRepository repository = new d_supplier_per_distributor_dsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
