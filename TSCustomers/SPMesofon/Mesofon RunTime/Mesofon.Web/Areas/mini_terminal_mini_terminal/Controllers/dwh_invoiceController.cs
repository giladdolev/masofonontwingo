using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_invoiceController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_invoice()
		{
			dwh_invoiceRepository repository = new dwh_invoiceRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
