using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using masofonAlias = masofon;
using mini_terminal;
using MvcSite.Common;
using Mesofon.Common;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using System.Xml;

namespace Mesofon.MiniTerminal.Controllers
{
    public class uo_mini_terminal_crossdoc_pack_listController : MvcSite.Common.SPBaseController
    {

        public ActionResult uo_mini_terminal_crossdoc_pack_list()
        {
            return this.View(new uo_mini_terminal_crossdoc_pack_list());
        }
        private uo_mini_terminal_crossdoc_pack_list ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_crossdoc_pack_list; }
        }

        public static bool enterKeyFlag { get; private set; }

        public void Form_Load(object sender, EventArgs e)
        {
            GridElement crossdoc_pack_list_dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            this.ViewModel.constructor();
            crossdoc_pack_list_dw_pallets_list.SetRepository(new d_marlog_return_cartonsRepository(), false, true);

        }
        public int uf_write_log(string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_write_log
            //*Purpose: 			Write into log file
            //*Arguments: 		String - as_message - message
            //*Return:				Integer
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138					Initial version
            //************************************************************************************************

            SystemHelper.WriteToLog(as_message, string.Empty);
            return 1;
        }
        public async Task<DialogResult> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_show_message
            //*Purpose: 			Display a messagebox with the given message text.
            //*Arguments: 		String 	as_title 					
            //*						String	as_error_text		
            //*						String 	as_buttons	
            //*						String 	as_message	
            //*Return:				Integer	1 - Success
            //*									-1 -Otherwise
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-05-2010		AlexKh			1.2.46.0	CR#1138				Initial version
            //************************************************************************************************

            DialogResult li_rtn_code = 0;
            //debug -Start
            //Write to log
            string ls_message = null;
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "Start of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);

            //debug -END
            switch (as_buttons)
            {
                case "OK":
                    li_rtn_code = await MessageBox.Show(as_message, as_title, MessageBoxButtons.OK);
                    break;
                case "okcancel":
                    li_rtn_code = await MessageBox.Show(as_message, as_title, MessageBoxButtons.OKCancel);
                    break;
            }

            //debug -Start
            //Write to log
            ls_message = "*************************** uo_mini_terminal_pallets_list - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "End of uf_show_message" + "\r" + "\n";
            uf_write_log(ls_message);
            //debug -END
            return li_rtn_code;
        }
        public async Task<int> uf_close_window() //********************************************************************
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            //*Object:				uo_mini_terminal_crossdoc_pack_list
            //*Function Name:	uf_close_window
            //*Purpose: 			close window
            //*Arguments: 		none
            //*Return:				None
            //*Date				Programer	Version		Task#	 		Description
            //*---------------------------------------------------------------------
            //*21-02-2012		AlexKh		1.2.48.15	CR#1152		Initial version
            //********************************************************************

            string ls_message = null;
            int li_rtn_code = 0;
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
            li_rtn_code = uf_write_log(ls_message);
            if (li_rtn_code < 1)
            {
                await uf_display("ENTRANCE");
                return -1;
            }
            await uf_display("ENTRANCE");
            dw_shipments_list.Focus();
            return 1;
        }
        public async Task<FuncResults<DateTime?, bool>> uf_handle_item_expiration_date(DateTime? adt_expiration_date, long al_min_valid_month, string as_material_name) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_handle_item_expiration_date
            //*Purpose: 			Check if item requires expiration date
            //*Arguments: 		ref		datetime		adt_expiration_date
            //*						value		long			al_min_valid_month
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*15-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            // Only if the material has minimum valid months, it should have expiration date
            if (!(al_min_valid_month == 0 || al_min_valid_month == 0))
            {

                if (await MessageBox.Show(adt_expiration_date.ToString(), al_min_valid_month.ToString(), MessageBoxButtons.OKCancel) == DialogResult.No)
                {
                    return FuncResults.Return(adt_expiration_date, false);
                }
                return FuncResults.Return(adt_expiration_date, true);
            }
            return FuncResults.Return(adt_expiration_date, true);
        }
        public async Task<int> uf_display(string as_name) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	uf_display
            //*Purpose: 			open appropriate user object
            //*Arguments: 		String	as_name
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*17-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            int li_ret = 0;
            long ll_row_count = 0;


            li_ret = await ((this.ViewModel.Parent.Parent as w_mini_terminal).wf_display(as_name));
            return 1;
        }
        public async Task<bool> uf_print_driver_report() //********************************************************************
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            //*Object:				uo_mini_terminal_crossdoc_pack_list
            //*Function Name:	uf_print_driver_report
            //*Purpose: 			Print driver report
            //*Arguments: 		None
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*21-02-2012		AlexKh			1.2.48.15	CR#1152	Initial version
            //********************************************************************

            long ll_i = 0;
            long ll_ret = 0;
            long ll_shipment_number = 0;
            long ll_rows = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            long ll_print_copies_number = 0;
            string[] ls_pallets_list = null;
            string ls_ret = null;

            var DriverReportHeader = new d_crossdoc_driver_report_headerRepository();

            ll_shipment_number = Convert.ToInt64(dw_shipments_list.SelectedValue);// dw_shipments_list.GetItemValue<long>(0, "shipment_number");
            if (ll_shipment_number == 0 || ll_shipment_number <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " מספר משלוח לא תקין ");

                return false;
            }
            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_rows = lds_driver_report.Retrieve(gs_vars.branch_number, ll_shipment_number, il_employee_number)

            ll_rows = DriverReportHeader.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_employee_numberProperty, ll_shipment_number, this.ViewModel.il_marlog_branchProperty);
            if (ll_rows <= 0)
            {
                await uf_show_message("שגיאה", "", "OK", " לא נמצאו נתונים עבור מספר משלוח זה ");

                return false;
            }

            //ls_ret = lds_driver_report.Modify("datawindow", "print.Orientation", "2"); // 1-landscape , 2-portrait

            var print = await WindowHelper.Open<rw_print_copies_number>("mini_terminal_mini_terminal", "rowsCount", 0);
            ll_print_copies_number = Convert.ToInt64(WindowHelper.GetParam<string>(print));

            for (ll_i = 1; ll_i <= ll_print_copies_number; ll_i++)
            {
                DriverReportHeader.Print(Globals.DeviceID, Globals.UserID, Globals.Password, MvcSite.Common.Globals.PrinterName);
            }

            return true;
        }
        public async Task<bool> uf_scan_pallet(string as_carton_barcode, long al_row) //********************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            //*Object:				uo_mini_terminal_crossdoc_pack_list
            //*Function Name:	uf_scan_pallet
            //*Purpose: 			Scan crossdoc karton using masofon
            //*Arguments: 		String	carton_barcode
            //*						Long		al_row
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer	Version		Task#	 	Description
            //*---------------------------------------------------------------------
            //*21-02-2012		AlexKh			1.2.48.15	CR#1152	Initial version
            //********************************************************************

            long ll_ret = 0;
            long? ll_found = 0;
            string ls_find = null;
            string ls_state = null;
            if (isempty_stringClass.isempty_string(as_carton_barcode) || as_carton_barcode == "0")
            {
                return false;
            }
            if (al_row == 0)
            {
                ls_find = "carton_barcode == \"" + as_carton_barcode.ToUpper() + "\"";


                ll_found = dw_pallets_list.Find(ls_find, 0, dw_pallets_list.RowCount());
                if (ll_found >= 0)
                {
                    ls_state = dw_pallets_list.GetItemValue<string>(ll_found.Value, "state");
                    if (ls_state == mini_terminal.uo_mini_terminal_crossdoc_pack_list.SENT_BY_MARLOG.ToString())
                    {
                        al_row = ll_found.Value;
                    }
                    else
                    {
                        await uf_show_message("הודעה", "", "OK", "קרטון זה נסרק בעבר");
                        return false;
                    }
                }
                else
                {
                    await uf_show_message("שגיאה", "", "OK", " לא נמצא קרטון מספר " + as_carton_barcode);
                    return false;
                }
                if (al_row == -1)
                {
                    return false;
                }
            }

            if (as_carton_barcode.ToUpper() == dw_pallets_list.GetItemValue<string>(al_row, "carton_barcode"))
            {

                dw_pallets_list.SetItem(al_row, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time());

                dw_pallets_list.SetItem(al_row, "branch_accept_datetime", masofonAlias.masofon.Instance.set_machine_time()); //RonY@15/01/2015 1.12.49.63 - SPUCM00005162

                dw_pallets_list.SetItem(al_row, "state", "c");

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = dw_pallets_list.Update(unitOfWork);
                }

                if (ll_ret != 1)
                {
                    await uf_show_message("שגיאה", "", "OK", "עדכון סטאטוס קרטון נכשל");
                    return false;
                }
            }
            else
            {
                await uf_show_message("שגיאה", "", "OK", "מספר קרטון שהוקלד, לא תקין");
                return false;
            }
            return true;
        }

        public void destructor()
        {
            //IF IsValid(iuo_order_search) THEN DESTROY iuo_order_search
            //IF IsValid(iuo_material_update) THEN DESTROY iuo_material_update
        }

        public async Task cb_finish_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish crossdoc scan and send the message to the marlog
            //*  
            //*Arguments:							None
            //*Return:								None
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*16-02-2012		AlexKh			   1.2.48.15  	CR#1152 		Initial Version
            //************************************************************************************************

            long ll_shipment_number = 0;
            long ll_i = 0;
            DialogResult ll_ret = 0;
            string ls_message = null;
            string ls_state = null;

            ll_shipment_number = Convert.ToInt64(dw_shipments_list.SelectedValue);// dw_shipments_list.GetItemValue<long>(0, "shipment_number");
            if (ll_shipment_number == 0 || ll_shipment_number <= 0)
            {
                await uf_show_message("מספר משלוח לא תקין", "", "OK", " שגיאה ");
                return;
            }
            // AlexKh

            for (ll_i = 0; ll_i < dw_pallets_list.RowCount(); ll_i++)
            {
                ls_state = dw_pallets_list.GetItemValue<string>(ll_i, "state");
                if (ls_state == mini_terminal.uo_mini_terminal_crossdoc_pack_list.NEW_NOT_APPROVED_MARLOG.ToString() || (ls_state == mini_terminal.uo_mini_terminal_crossdoc_pack_list.NEW_APPROVED_MARLOG.ToString() || ls_state == mini_terminal.uo_mini_terminal_crossdoc_pack_list.SENT_BY_MARLOG.ToString()))
                {
                    ll_ret = await uf_show_message("הודעה", "", "okcancel", "שים לב, לא כל הקרטונים נסרקו, האם להמשיך?");
                    if (ll_ret == DialogResult.OK)
                    {
                        break; // TODO: might not be correct. Was : Exit For
                    }
                    else
                    {
                        return;
                    }
                }
            }
            // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
            //ls_message = invuo_b2b_crossdoc.uf_b2b_send_crossdoc_approve( gs_vars.branch_number, ll_shipment_number)
            ls_message = await this.ViewModel.invuo_b2b_crossdocProperty.uf_b2b_send_crossdoc_approve(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_shipment_number, this.ViewModel.il_marlog_branchProperty);
            if (!isempty_stringClass.isempty_string(ls_message))
            {
                await uf_show_message("הודעה", "", "OK", ls_message);
            }
            await uf_print_driver_report();
            await this.uf_close_window();
        }

        public async Task sle_scan_line_ue_enter(object sender, KeyEventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - SPUCM00002425  - ue_enter
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            if (key == Keys.Enter || key == Keys.Tab)
            {
                enterKeyFlag = true;
                await scan_line_enter(sender, e);
            }
            else
            {
               
            }
         
        }
        public async Task scan_line_enter(object sender, EventArgs e)
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_scan_line.Text != "")
                {
                    await uf_scan_pallet(sle_scan_line.Text, 0);
                    sle_scan_line.Text = "";
                    enterKeyFlag = false;
                }
            }
        }

        public void sle_scan_line_getfocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - getfocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            //Rectangle r_pallets_list = this.GetVisualElementById<Rectangle>("r_pallets_list");
            // 
            //r_pallets_list.set_LineColor(134217729);
            sle_scan_line.Text = "";
        }

        public void sle_scan_line_losefocus(object sender, EventArgs e) // AlexKh - 1.2.46.3 - 2010-11-14 - spucm00002425  - losefocus event
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            //Rectangle r_pallets_list = this.GetVisualElementById<Rectangle>("r_pallets_list");
            // 
            //r_pallets_list.set_LineColor(67108864);
            sle_scan_line.Text = "";
        }

        public async Task cb_print_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            //*Object:								uo_mini_terminal_crossdoc_pack_list
            //*Function/Event  Name:			cb_print.clicked event
            //*Purpose:							Print Driver report
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer	Version		Task#		Description
            //*------------------------------------------------------------------------------------------------
            //*21-02-2012		AlexKh		1.2.48.15  	CR#1152 	Initial Version
            //************************************************************************************************

            await uf_print_driver_report();

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public void st_shipment_number_getfocus(object sender, EventArgs e)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public void st_title_getfocus(object sender, EventArgs e)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public int dw_shipments_list_ue_enter(Keys key, uint keyflags) // AlexKh - 1.2.48.15 - 2012-02-21 - CR#1152  - ue_enter
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
            {
                dw_pallets_list.Focus();
            }
            return 1;
        }

        public async Task dw_shipments_list_itemchanged(object sender, EventArgs e) // AlexKh - 1.2.48.15 - 2012-02-21 - CR#1152  - ItemChanged
        {
            int row;
            ComboGridElement dwo = sender as ComboGridElement;
            string data = Convert.ToString(dwo.SelectedValue);
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            long ll_ret = 0;

            // if (dwo.ID == "shipment_number")
            {

                if (!isempty_stringClass.isempty_string(data) && SystemFunctionsExtensions.IsNumber(data))
                {
                    // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
                    //ll_ret = dw_pallets_list.Retrieve(gs_vars.branch_number, Long(data))
                    IRepository crossdoc_pack_list = new d_mini_terminal_crossdoc_pack_listRepository();
                    ll_ret = crossdoc_pack_list.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, Convert.ToInt64(data), this.ViewModel.il_marlog_branchProperty);
                    dw_pallets_list.SetRepository(crossdoc_pack_list);
                    if (ll_ret < 0)
                    {
                        await uf_show_message("שגיאה", "", "OK", " לא נמצאו קרטונים למספר משלוח " + data);
                    }
                    else
                    {
                        sle_scan_line.Focus();
                    }
                }
                else if (isempty_stringClass.isempty_string(data))
                {

                    dw_pallets_list.Reset();
                    dw_shipments_list.Focus();
                }
                else
                {
                    await uf_show_message("שגיאה", "", "OK", " מספר משלוח לא תקין ");
                    dw_shipments_list.Focus();

                    //dw_shipments_list.SelectText(1, 30);
                }
            }
        }

        public int dw_shipments_list_editchanged(object sender, EventArgs e) // AlexKh - 1.2.48.15 - 2012-02-21 - CR#1152  - EditChanged
        {
            int row = 0;
            var dwo = sender as ControlElement;
            ValueChangedArgs<string> args = e as ValueChangedArgs<string>;
            if (args == null) return 0;
            string data = args.Value;
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");

            if (dw_shipments_list != null && dwo.ID == "crossdoc_pack_list_dw_shipments_list")
            {

                if (!SystemFunctionsExtensions.IsNumber(data))
                {
                    dw_shipments_list.Text = "";

                   
                }
                if (data?.Length > 20)
                {
                    dw_shipments_list.Text = dw_shipments_list.Text.Substring(0, dw_shipments_list.Text.Length - 1);

                   
                    return 0;
                }
            }
            return 0;
        }

        public void dw_shipments_list_clicked(int xpos, int ypos, int row, VisualElement dwo)
        {
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");

            //dw_shipments_list.SelectText(1, 30);
        }

        public async Task cb_exit_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_entrance
            //*Function/Event  Name:			cb_finish.clicked event
            //*Purpose:							Finish the masofon input.
            //*  
            //*Arguments:							None.
            //*Return:								Long.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_shipment_number = 0;
            // AlexKh - 1.2.48.30 - 2012-08-26 - SPUCM00003402 - don't print report on exit
            //ll_shipment_number = dw_shipments_list.GetItem<int> (1, "shipment_number")
            //
            //IF ll_shipment_number > 0 THEN
            //	uf_print_driver_report()
            //END IF
            await this.uf_close_window();
        }

        public void dw_pallets_list_getfocus(object sender, EventArgs e) //*********************************************************************
        {
            TextBoxElement sle_scan_line = this.GetVisualElementById<TextBoxElement>("sle_scan_line");
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            ComboGridElement dw_shipments_list = this.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
            //*Object:				uo_mini_terminal_crossdoc_pack_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			getfocus event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version		Task#	 	Description
            //*---------------------------------------------------------------------
            //*21-02-2012		AlexKh			1.2.48.15	CR#1152	Initial version
            //*********************************************************************


            dw_shipments_list.PerformValidated(e);

            if (dw_pallets_list.RowCount() > 0)
            {
                sle_scan_line.Focus();
                sle_scan_line.Text = "";
            }
            else
            {
                dw_shipments_list.Focus();
            }
        }

        public async Task dw_pallets_list_clicked(object sender, GridElementCellEventArgs e) //******************************************************************
        {
            int row = e.RowIndex;

            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
            //*Object:				uo_mini_terminal_crossdoc_pack_list
            //*Function Name:	dw_pallets_list
            //*Purpose: 			clicked event
            //*Arguments: 		
            //*Return:				
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------
            //*21-02-2012		AlexKh			1.2.48.15	CR#1152	Initial version
            //*******************************************************************

            string ls_barcode_carton = null;
            string ll_state = null;
            long ll_ret = 0;

            if (dw_pallets_list.Columns[e.ColumnIndex].ID == "state")
            {
                ll_state = dw_pallets_list.GetItemValue<string>(row, "state");
                if (ll_state == mini_terminal.uo_mini_terminal_crossdoc_pack_list.SENT_BY_MARLOG.ToString())
                {

                    var pallet = await WindowHelper.Open<rw_pallet>("mini_terminal_mini_terminal", "rowCount", 0);


                    ls_barcode_carton = WindowHelper.GetParam<string>(pallet);
                    await uf_scan_pallet(ls_barcode_carton, row);
                }
            }
        }

        public void dw_pallets_list_ue_enter(Keys key, uint keyflags)
        {
        }
    }
}
