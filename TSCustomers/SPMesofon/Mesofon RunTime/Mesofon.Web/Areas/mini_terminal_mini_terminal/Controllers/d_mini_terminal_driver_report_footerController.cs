using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_driver_report_footerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_driver_report_footer()
		{
			d_mini_terminal_driver_report_footerRepository repository = new d_mini_terminal_driver_report_footerRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
