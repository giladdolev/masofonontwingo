using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_crossdoc_returns_for_b2b_xmlController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_crossdoc_returns_for_b2b_xml()
		{
			d_crossdoc_returns_for_b2b_xmlRepository repository = new d_crossdoc_returns_for_b2b_xmlRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
