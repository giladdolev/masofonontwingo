﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using masofon;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree;
using Common.Transposition.Extensions;
using mini_terminal;
using Mesofon.Models;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.Repository;
using masofonAlias = masofon;


namespace Mesofon.MiniTerminal.Controllers
{
	public class uo_mini_terminal_trans_from_branchController : uo_mini_terminal_base_bodyController
	{

		public ActionResult uo_mini_terminal_trans_from_branch()
		{
			return this.View(new uo_mini_terminal_trans_from_branch());
		}
		private uo_mini_terminal_trans_from_branch ViewModel
		{
			get { return this.GetRootVisualElement() as uo_mini_terminal_trans_from_branch; }
		}

	    public void Form_Load(object sender, EventArgs e)
	    {
	        //this.ViewModel.constructor();
	    }

        public async Task ue_print()
		{
			GridElement dw_print = this.GetVisualElementById<GridElement>("dw_print");
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			long invoice_number = 0;
			long supplier_number = 0;
			long ll_doc = 0;
			long ll_order_number = 0;
			long ll = 0;
			long ll_emp_number = 0;
			long ll_row = 0;
			long ll_dddw_width = 0;
			long ll_dddw_width_1 = 0;
			long[] ll_introduction_campaign = null;
			int li = 0;
			int li_mini_terminal = 0;
			int li_min_valid_months = 0;
			string ls_user_logo = null;
			string ls_colname = null;
			string ls_fix_vat = null;
			string ls_import_type = null;
			string ls_emp_name = null;
			string ls_barcode = null;
			string ls_text = null;
			string ls_column = null;
			bool lb_diff_dw = false;
			//bool lb_debit_dw = false;
			string ls_comp_number = null;
			string ls_comp_name = null;
			string ls_address = null;
			GridElement ldwc;
			GridElement ldwc_decline;
			GridElement ldw = default(GridElement);
			s_array_arguments lstr_parms = default(s_array_arguments);
			//Ron@05/05/2015>>SPUCM00005332
			long ll_msg = 0;
			long ll_ret = 0;
			
			if (dw_return_details.RowCount() == 0)
			{
				return;
			}
			if (this.ViewModel.il_return_number == 0 || (this.ViewModel.il_return_number <= 0))
			{
				return;
			}
			if (this.ViewModel.il_branch_as_supp_numProperty == 0 || (this.ViewModel.il_branch_as_supp_numProperty <= 0))
			{
				return;
			}
			// Set document (invoice or packlist) data and dw_pricing DataWindow object
			ls_colname = "invoice_number";
			lstr_parms.a_string[1] = "invoice";

		    dw_print.SetDataObject(new dwh_invoice_print2Repository());


            //dw_pricing.DataObject = "dwh_invoice_pricing_page"
            //invoice_number = dw_header.GetItem<int>(1, ls_colname)
            //IF IsNull(invoice_number) OR (invoice_number <= 0) THEN RETURN
            //
            //supplier_number = dw_header.GetItem<int>(1, "supplier_number")
            //IF IsNull(supplier_number) OR (supplier_number < 0) THEN RETURN
            // Set Difference report data object
            if (true)
			{
			    ViewModel.LoadData1(supplier_number, invoice_number, ref li_mini_terminal);
			}
			//IF li_mini_terminal > 0 THEN 
			//		dw_diff.DataObject = "d_inv_order_details_report_mini_terminal"
			//	ELSE
			//		dw_diff.DataObject = "d_inv_order_details_report"
			//	END IF
			// Get logo
			ls_user_logo = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Report", "client_logo_path", "C:\\Pictures\\net-pos_logo.bmp", "The path of the logo");
			// Retrieve document (invoice or packlist) details
			//IF dw_print.Retrieve(supplier_number, invoice_number, il_branch_number, '', ls_user_logo) <= 0 THEN RETURN	
			
			if (dw_print.Retrieve(this.ViewModel.il_supplier_numProperty, this.ViewModel.il_return_number, masofonAlias.masofon.Instance.gs_vars.branch_number, "", ls_user_logo) <= 0)
			{
				return;
			}
			//IF NOT ib_print_dw_is_rotated THEN 
			//  	ib_print_dw_is_rotated = TRUE
			//  	nvo_translator.fnv_rotate_dw(dw_print)
			//END IF

            dw_print.Modify("DataWindow.Print", "Orientation", "1"); // 1-landscape , 2-portrait
			//..Rami(13.07.2004) - Set the fonts of prices that differ from the catalog to bold
			if (masofonAlias.masofon.Instance.gs_vars.bold_diff_in_purchase_prints.ToLower(CultureInfo.InvariantCulture) == "yes")
			{
				 
				ldwc = dw_print.GetVisualElementById<GridElement>("dw_2");
				
				for (ll = 1; ll <= ldwc.RowCount(); ll++)
				{
					 
					ldwc.SetItem(ll, "flag_color", "1");
				}
			}
			//..Rami (18.08.2004)
			if (masofonAlias.masofon.Instance.gs_vars.update_sell_price_by_gp.ToLower(CultureInfo.InvariantCulture) == "yes" && masofonAlias.masofon.Instance.gs_vars.alternitive_gp_calculation.ToLower(CultureInfo.InvariantCulture) == "yes")
			{
				 
				ldwc = dw_print.GetVisualElementById<GridElement>("dw_2");

			    ldwc.Modify("compute_gp", "Visible", "0");

			    ldwc.Modify("compute_gp_1", "Visible", "1");
				 
				ldwc = dw_print.GetVisualElementById<GridElement>("dw_1");

			    ldwc.Modify("compute_gp", "Visible", "0");

			    ldwc.Modify("compute_gp_1", "Visible", "1");

			    ldwc.Modify("sum_gp", "Visible", "0");

			    ldwc.Modify("sum_gp_1", "Visible", "1");
			}
			//..Rami(07.04.2005) - in case of a long barcode - modify the cloumns width
			if (masofonAlias.masofon.Instance.gs_vars.long_barcode_in_purchase.ToLower(CultureInfo.InvariantCulture) == "yes")
			{
				 
				ldwc = dw_print.GetVisualElementById<GridElement>("dw_2");
				
				for (ll = 1; ll <= ldwc.RowCount(); ll++)
				{
					
					ls_barcode = ldwc.GetItemValue<string>((int)ll, "materials_barcode");
					if (ls_barcode.Length > 13)
					{
						 
						ldwc.SetItem(ll, "long_barcode_ind", "1");
					}
				}
			}
			//
			//// oren - settings for the column "introduction_campaign"
			//IF gs_vars.show_introduction_campaign_ord THEN
			//	dw_print.GetChild("dw_2", ldwc)
			//	ldwc( "introduction_campaign_t.Visible = 1")
			//	ldwc( "introduction_campaign.Visible = 1")
			//	wf_set_introduction_print(ldwc)
			//ELSE
			//	dw_print.GetChild("dw_2", ldwc)
			//	ldwc( "introduction_campaign_t.Visible = 0")
			//	ldwc( "introduction_campaign.Visible = 0")
			//END IF
			//
			//ib_wide_print = TRUE
			//IF ib_return_mode THEN
			ldw = dw_print;
			 
			ll_dddw_width_1 = Convert.ToInt64(ldw.Describe("dw_1", "Width")); // * long( ldw.Describe(column, "dddw.percentwidth")) /100
			 
			ldwc = ldw.GetVisualElementById<GridElement>("dw_1");
		    await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_dwc(ldwc, (int)ll_dddw_width_1);
		    await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dwc(ldwc, (int)ll_dddw_width_1);
			 
			ldwc = ldw.GetVisualElementById<GridElement>("dw_2");
			 
			ll_dddw_width_1 = Convert.ToInt64(ldw.Describe("dw_2", "Width")); // * long( ldw.Describe(column, "dddw.percentwidth")) /100
		    await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_dwc(ldwc, (int)ll_dddw_width_1);
		    await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dwc(ldwc, (int)ll_dddw_width_1);
			// AlexKh - 1.2.45.0 - 2009-06-29 - SPUCM00001395 - handle doc type RETURN
			 
			ldwc = dw_print.GetVisualElementById<GridElement>("dw_2");
			 
			ldwc_decline = ldwc.GetVisualElementById<GridElement>("decline_number");
			
			
			
			ldwc_decline.Retrieve(3);
			// AlexKh - End
			
			ll_ret = dw_print.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName);
            if (ll_ret == 1)
			{
				ll_ret = await this.ViewModel.uf_show_message("", "", "OK", "הודפס בהצלחה");
			}
			//	f_dw_wide_print(dw_print)????????
			return;
		
		}
		public int uf_set_data() //Ron@02/02/2015 - SPUCM0005163
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			//set data to display according to return number
			
			long ll_row = 0;
			long ll_rowcount = 0;
			long ll_index = 0;
			long ll_supplier_number = 0;
			long ll_decline_number = 0;
			long ll_material_quantity = 0;
			long ll_branch_number = 0;
			long ll_serial_number = 0;
			long ll_material_number = 0;
			long ll_rowcount1 = 0;
			decimal ldec_origimal_quantity = default(decimal);
			string ls_name = null;
			string ls_barcode = null;
			
			dw_return_details.Reset();
			
			ll_row = this.ViewModel.idwc_returnProperty.GetRow();
			
			this.ViewModel.il_supplier_numProperty = this.ViewModel.idwc_returnProperty.GetItemValue<long>((int)ll_row, "supplier_number");
			
			this.ViewModel.il_return_number = this.ViewModel.idwc_returnProperty.GetItemValue<long>((int)ll_row, "invoice_number");
			 
			ll_rowcount = this.ViewModel.ids_returns_listProperty.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_return_number, this.ViewModel.il_supplier_numProperty, "p");
			//ids_soure_returns_list contains the quantity as the source branch entered
			 
			ll_rowcount1 = this.ViewModel.ids_source_returns_listProperty.Retrieve(this.ViewModel.il_branch_as_supp_numProperty, this.ViewModel.il_return_number, this.ViewModel.il_source_supplier_numberProperty, "r"); //Ron@20/07/2015>>SPUCM00005413
			//Ron@10/06/2015>>SPUCM00005384 - add and remark  - start
			 
			this.ViewModel.ids_returns_0_listProperty.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_return_number, this.ViewModel.il_supplier_numProperty, "p");
			if (ll_rowcount == 0)
			{
				
				ll_row = dw_return_details.Insert(0);
				
				dw_return_details.SetItem(ll_row, "row_no", ll_row.ToString());
				return 1;
			}
			//Ron@10/06/2015>>SPUCM00005384 - add and remark  - end
			for (ll_index = 1; ll_index <= ll_rowcount; ll_index++)
			{
				
				ll_serial_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "serial_number");
				
				ll_material_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "material_number");
				
				ll_branch_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "branch_number");
				
				ls_barcode = this.ViewModel.ids_returns_listProperty.GetItemValue<string>(ll_index, "barcode");
				
				ls_name = this.ViewModel.ids_returns_listProperty.GetItemValue<string>(ll_index, "name");
				
				ll_supplier_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "supplier_number");
				
				ll_decline_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "decline_number");
				
				ll_material_quantity = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_index, "material_quantity");
				
				ll_row = dw_return_details.Insert(0);
				
				dw_return_details.SetItem(ll_row, "serial_number", ll_serial_number.ToString());
				
				dw_return_details.SetItem(ll_row, "material_number", ll_material_number.ToString());
				
				dw_return_details.SetItem(ll_row, "branch_number", ll_branch_number.ToString());
				
				dw_return_details.SetItem(ll_row, "row_no", ll_row.ToString());
				
				dw_return_details.SetItem(ll_row, "barcode", Convert.ToString(ls_barcode));
				
				dw_return_details.SetItem(ll_row, "material_name", Convert.ToString(ls_name));
				//Ron@22/07/2015>>SPUCM00005413 - start
				//dw_return_details.SetItem(ll_row,"supplier_number", ll_supplier_number)
				ldec_origimal_quantity = uf_set_original_quantity(ll_material_number);
				
				dw_return_details.SetItem(ll_row, "supplier_number", ldec_origimal_quantity.ToString());
				//Ron@22/07/2015>>SPUCM00005413 - end
				
				dw_return_details.SetItem(ll_row, "declines", ll_decline_number.ToString());
				
				dw_return_details.SetItem(ll_row, "material_quantity", ll_material_quantity.ToString());
				
				
				
				dw_return_details.SetItemStatus((int)ll_row, 0, ModelBuffer.Primary, ModelAction.None);
				
				
				
				dw_return_details.SetItemStatus((int)ll_row, 0, ModelBuffer.Primary, ModelAction.None);
			}
			return 1;
		}

	    public string uf_check_data(long al_row)
	    {
	        long ll_material_quantity;
            long ll_decline_num;
	        string ls_barcode;
            string ls_material_name;

	        GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
	        ls_barcode = dw_return_details.GetItemValue<string>(al_row, "barcode");
	        if (ls_barcode == "")
	        {
	            return "ברקוד";
	        }

	        ls_material_name = dw_return_details.GetItemValue<string>(al_row, "material_name");
	        if (ls_material_name == "")
	        {
	            return "שם פריט";
	        }

	        ll_material_quantity = dw_return_details.GetItemValue<long>(al_row, "material_quantity");
	        if (ll_material_quantity == 0)
	        {
	            return "כמות";
	        }
	        

            //Ron@20/07/2015>>SPUCM00005413
            /*ll_decline_num =  dw_return_details.GetItemNumber(al_row,"declines")
            if ( ISNULL(ll_decline_num) OR ll_decline_num = 0 
	            return "סיבה"
            END if (*/

	        return "";




	    }

	    public async Task<int> uf_save_data(long al_row)
		{
		    EventArgs e =new EventArgs();

            GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
			long ll_material_quantity = 0;
			long ll_decline_num = 0;
			long ll_branch_number = 0;
			long ll_serial_number = 0;
			long ll_material_number = 0;
			long ll_orig_serial_number = 0;
			long? ll_found = 0;
			long ll_line_num = 0;
			decimal ldec_material_price = default(decimal);
			decimal ldec_active_price = default(decimal);
			DateTime? ldt_current_datetime = null;
			bool lb_replace_item = false;
			bool lb_exist = false;
			string ls_find = null;
			string ls_barcode = null;
			ModelAction l_status = default(ModelAction);
			
			dw_returns.PerformValidated(e);
			
			dw_return_details.PerformValidated(e);
			
			
			l_status = dw_return_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
			
			
			if (l_status == ModelAction.None || l_status == ModelAction.Insert)
			{
				return 1;
			}
			//Ron@10/06/2015>>SPUCM00005384 - start
			
			ls_barcode = dw_return_details.GetItemValue<string>(al_row, "barcode");
			if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
			{
				return 1;
			}
			//Ron@10/06/2015>>SPUCM00005384 - end
			
			ll_serial_number = dw_return_details.GetItemValue<long>(al_row, "serial_number");
			
			ll_branch_number = dw_return_details.GetItemValue<long>(al_row, "branch_number");
			
			ll_material_quantity = dw_return_details.GetItemValue<long>(al_row, "material_quantity");
			
			ll_decline_num = dw_return_details.GetItemValue<long>(al_row, "declines");
			
			ll_material_number = dw_return_details.GetItemValue<long>(al_row, "material_number");
			//ll_supplier_number = dw_return_details.GetItem<int>(al_row,"supplier_number")
			//uf_calculate_declines
			ldt_current_datetime =DateTime.Now;
			//RonY@25/05/2015 - SPUCM00005371 - start
			
			ldec_material_price = dw_return_details.GetItemValue<decimal>(al_row, "material_price");
			
			ldec_active_price = dw_return_details.GetItemValue<decimal>(al_row, "material_price_after_discount");
			//RonY@25/05/2015 - SPUCM00005371 - end
			//save data
			if (this.ViewModel.ib_new_returnProperty)
			{
				this.ViewModel.ib_new_returnProperty = false; //do insert to invoice_move, invoice_details
				if (this.ViewModel.il_return_number == 0 || this.ViewModel.il_return_number == 0)
				{
					this.ViewModel.il_return_number = await f_calculate_datatable_rowClass.f_calculate_datatable_row(144, 0, masofonAlias.masofon.Instance.sqlca);
					
					dw_returns.SetItem(0, "return_number", this.ViewModel.il_return_number.ToString());
				}
			    ViewModel.UpdateData2(ll_branch_number, ldt_current_datetime);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{

				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
					return 1;
				}
				//insert into invoice_details
				ll_serial_number = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(ll_branch_number, 4);
			    ViewModel.UpdateData3(ll_serial_number, ll_branch_number, ll_material_number, ll_material_quantity, ldec_material_price, ldec_active_price, ll_decline_num, ldt_current_datetime);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{

				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
					return 1;
				}
				//refresh
				
				this.ViewModel.idwc_returnProperty.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_branch_as_supp_numProperty, "p");
				uf_set_data();
			}
			else
			{
				//update or insert to invoice_details, no need to insert invoice_move
				//l_status = dw_return_details.GetItemStatus(al_row , 0, Primary!)
				//RonY@28/05/2015>>SPUCM00005371 - start
				
				ll_orig_serial_number = dw_return_details.GetItemValue<long>(al_row, "orig_serial_number");
				if (ll_orig_serial_number > 0 && !(ll_orig_serial_number == 0))
				{
					lb_replace_item = true;
				}
				else
				{
					lb_replace_item = false;
				}
				//RonY@28/05/2015>>SPUCM00005371 - end
				//RonY@10/06/2015>>SPUCM00005384 - start
				//if the item exists already with quantity 0 means it was created from the source then need just update
				ls_find = "material_number == " + ll_material_number.ToString();
				
				
				ll_found = this.ViewModel.ids_returns_0_listProperty.Find(ls_find, 0, this.ViewModel.ids_returns_0_listProperty.RowCount());
				if (ll_found >= 0)
				{
					
					ll_serial_number = this.ViewModel.ids_returns_0_listProperty.GetItemValue<long>((int)ll_found, "serial_number");
					lb_exist = true;
					//new code
				}
				else
				{
					
					
					ll_found = this.ViewModel.ids_returns_listProperty.Find(ls_find, 0, this.ViewModel.ids_returns_listProperty.RowCount());
					if (ll_found >= 0)
					{
						
						ll_serial_number = this.ViewModel.ids_returns_listProperty.GetItemValue<long>((int)ll_found, "serial_number");
						lb_exist = true;
					}
				}
				//RonY@10/06/2015>>SPUCM00005384 - end
				
				if ((l_status == ModelAction.None && !lb_replace_item) || lb_exist) //update invoice_details
				{
				    ViewModel.UpdateData4(ll_material_quantity, ll_decline_num, ldt_current_datetime, ldec_material_price, ldec_active_price, ll_serial_number, ll_branch_number);
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
					{

					    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
						return 1;
					}
									}
				else
				{
					//insert into invoice_details
					//RonY@28/05/2015>>SPUCM00005371 (delete previous item because the user change the barcode) - start
					if (lb_replace_item)
					{
					    ViewModel.UpdateData5(ldt_current_datetime, ll_orig_serial_number, ll_branch_number);
					}
					//RonY@28/05/2015>>SPUCM00005371 - end
					ll_serial_number = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(ll_branch_number, 4);
				    ViewModel.UpdateData6(ll_serial_number, ll_branch_number, ll_material_number, ll_material_quantity, ldec_material_price, ldec_active_price, ll_decline_num, ldt_current_datetime);
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
					{

					    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
						return 1;
					}
					// AlexKh - 1.1.38.0 - 2015-08-20 - SPUCM00005425- remove b2b_invoice_details insert
				
				}
			}
			
			
			
			dw_return_details.SetItemStatus((int)al_row, 0, ModelBuffer.Primary, ModelAction.None);
			return 1;
		}
		
		public async Task<bool> uf_update_stock(long stock_number, long material_number, decimal quantity, long store_number) //based on wf_update_stock from w_pack_order_invoice_from_supplier in OFFICE
		{
			decimal average_value_from_stock = default(decimal);
			decimal ldec_tmp_avg_value = default(decimal);
			decimal ldec_stock_average = default(decimal);
			decimal ldec_average_value = default(decimal);
			decimal ldec_stock_balance = default(decimal);
			decimal ldec_units_amount = default(decimal);
			decimal ldec_quantity_in_package = default(decimal);
			decimal ldec_curr_balance = default(decimal);
			decimal ldec_quantity = default(decimal);
			decimal ldec_price_after_discount = default(decimal);
			decimal ldec_stock_average_value = default(decimal);
			decimal ldec_total_stock_purchase = default(decimal);
			decimal ldec_SAV = default(decimal);
			long material_num = 0;
			long ll_stock_a = 0;
			long ll_stock_type = 0;
			int li_sqlcode = 0;
			string material_name = null;
			ldec_price_after_discount = 0;
			average_value_from_stock = 0;
			if (true)
			{
			    ViewModel.LoadData2(material_number, ref ldec_quantity_in_package);
			}
			//
			// Avi Alfassi - 14/09/2005
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				
				await MessageBox.Show("Line - 39, " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
				return false;
				//	
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0 || ldec_quantity_in_package == 0)
			{
				ldec_quantity_in_package = 1;
			}
			quantity = ldec_quantity_in_package * quantity;
			// Checks if the material exists in this store and stock
			// Avi Alfassi - 14/09/2005
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				
				await MessageBox.Show("Line - 65, material_number = " + material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
				return false;
			}
			//	
			if (true)
			{
                ViewModel.LoadData3(ref ldec_stock_balance, ref ldec_stock_average, ref ll_stock_a, masofonAlias.masofon.Instance.gs_vars.branch_number, material_number, store_number, stock_number);
			}
			if (isempty_stringClass.isempty_string(ldec_stock_balance.ToString()))
			{
				ldec_stock_balance = 0;
			}
			if (isempty_stringClass.isempty_string(ldec_stock_average.ToString()))
			{
				ldec_stock_average = 0;
			}
			
			li_sqlcode = masofonAlias.masofon.Instance.sqlca.SqlCode;
			if (li_sqlcode == -1)
			{
				return false;
			}
			if (li_sqlcode == 100) // does not exist in stock_control
			{
				//create new stock control row
				if (average_value_from_stock > 0)
				{
					ldec_tmp_avg_value = average_value_from_stock;
				}
				else
				{
					ldec_tmp_avg_value = 0;
				}
				//..Rami (03.10.04)
				ldec_stock_average_value = ldec_price_after_discount;
				if (ldec_stock_average_value < 0)
				{
					ldec_stock_average_value = 0;
				}
				ldec_total_stock_purchase = ldec_stock_average_value * quantity / ldec_quantity_in_package;
			    ViewModel.UpdateData9(material_number, store_number, stock_number, ldec_tmp_avg_value, quantity, ldec_total_stock_purchase);
				// Avi Alfassi - 14/09/2005
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
				{
					
					await MessageBox.Show("Line - 103, material_number = " + material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
					return false;
				}
				//
			}
			else
			{
			    ViewModel.LoadData4(material_number, store_number, stock_number, ref ldec_total_stock_purchase);
				// Avi Alfassi - 14/09/2005
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
				{
					
					await MessageBox.Show("Line - 120, material_number = " + material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
					return false;
				}
				//
				if (ldec_total_stock_purchase == 0)
				{
					ldec_total_stock_purchase = 0;
				}
				ldec_total_stock_purchase = ldec_total_stock_purchase / ldec_quantity_in_package;
				//..Rami <- end
			}
			// if the quantity is negative, don't update the average value
			if (quantity < 0)
			{
				ldec_average_value = ldec_stock_average;
			}
			else
			{
				ldec_units_amount = ldec_stock_balance;
				if (ldec_units_amount > 0)
				{
					ldec_average_value = ((ldec_stock_average * ldec_units_amount) + (quantity * average_value_from_stock)) / (ldec_units_amount + quantity);
				}
				else
				{
					ldec_average_value = average_value_from_stock;
				}
			}
			if (true)
			{
			    ViewModel.LoadData5(material_number, store_number, stock_number, ref ldec_curr_balance);
			}
			// Avi Alfassi - 14/09/2005
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				
				await MessageBox.Show("Line - 161, material_number = " + material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
				return false;
				//
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
			{
				if (ldec_total_stock_purchase == 0)
				{
					ldec_total_stock_purchase = (ldec_curr_balance / ldec_quantity_in_package) * ldec_price_after_discount;
				}
			}
			ldec_total_stock_purchase += ((quantity / ldec_quantity_in_package) * ldec_price_after_discount);
			if ((ldec_curr_balance + quantity) != 0)
			{
				ldec_SAV = ldec_total_stock_purchase / ((ldec_curr_balance + quantity) / ldec_quantity_in_package);
				if (ldec_SAV < 0)
				{
					ldec_SAV = 0;
				}
			}
			else
			{
				ldec_SAV = 0;
			}
			ldec_quantity = quantity;
		    ViewModel.UpdateData10(ldec_quantity, ldec_average_value, ldec_total_stock_purchase, material_number, ll_stock_a, store_number);
			// Avi Alfassi - 14/09/2005
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				
				await MessageBox.Show("Line - 198, material_number = " + material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_update_stock - Sql Error");
				return false;
			}
			//
			return true;
		}
		public async Task<int> uf_new_invoice_target_branch(EventArgs e) //Ron@15/02/2015 SPUM00005163
		{
			GridElement dw_branch_list = this.GetVisualElementById<GridElement>("dw_branch_list");
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
			//set new invoice in the target branch
			long ll_material_quantity = 0;
			long ll_decline_num = 0;
			long ll_branch_number = 0;
			long ll_serial_number = 0;
			long ll_material_number = 0;
			long ll_supplier_number = 0;
			long ll_rowcount = 0;
			long ll_index = 0;
			long ll_invoice_number = 0;
			decimal ldec_price_before = default(decimal);
			decimal ldec_price_after = default(decimal);
			decimal ldec_material_price = default(decimal);
			decimal ldec_activate_price = default(decimal);
			DateTime? ldt_current_datetime = null;
			
			dw_returns.PerformValidated(e);
			
			dw_return_details.PerformValidated(e);
			
			ll_rowcount = dw_returns.RowCount();
			if (ll_rowcount < 1)
			{
				return 1;
			}
			//get supplier number of the source branch
			if (true)
			{
			    ViewModel.LoadData6(ref ll_supplier_number);
			}
			if (ll_supplier_number == 0)
			{
				return 1;
			}
			//uf_calculate_declines
			ldt_current_datetime =DateTime.Now;
			
			ll_rowcount = dw_return_details.RowCount();
			ll_invoice_number = await f_calculate_datatable_rowClass.f_calculate_datatable_row(144, 0, masofonAlias.masofon.Instance.sqlca);
		    ViewModel.UpdateData11(ll_invoice_number, ll_supplier_number, ldt_current_datetime);
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{

			    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
				return 1;
			}
		    ViewModel.UpdateData12(ll_invoice_number, ldt_current_datetime);
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{

			    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
				return 1;
			}
			
			ll_rowcount = dw_return_details.RowCount();
			
			
			ll_branch_number = dw_branch_list.GetItemValue<long>(dw_branch_list.GetRow(), "branch_num");
			for (ll_index = 1; ll_index <= ll_rowcount; ll_index++)
			{
				
				ll_serial_number = dw_return_details.GetItemValue<long>(ll_index, "serial_number"); //just for checking if its empty row or not
				if (ll_serial_number == 0 || ll_serial_number == 0) //if its empty row continue
				{
					continue;
				}
				ll_serial_number = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(ll_branch_number, 4);
				
				ll_material_quantity = dw_return_details.GetItemValue<long>(ll_index, "material_quantity");
				
				ll_decline_num = dw_return_details.GetItemValue<long>(ll_index, "declines");
				
				ll_material_number = dw_return_details.GetItemValue<long>(ll_index, "material_number");
				//RonY@28/05/2015 - SPUCM00005371 - start
                uf_set_material_prices(ll_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_price, ref ldec_activate_price);
				ldec_material_price = Math.Round(ldec_material_price, 2);
				ldec_activate_price = Math.Round(ldec_activate_price, 2);
			    ViewModel.UpdateData13(ll_serial_number, ll_invoice_number, ll_material_number, ll_material_quantity, ldec_material_price, ll_supplier_number, ldec_activate_price, ll_decline_num, ldt_current_datetime);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{

				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, masofonAlias.masofon.Instance.sqlca.SqlErrText);
					return 1;
				}
			}
			return 1;
		}
		public int uf_set_material_prices(long al_material_number, ref decimal adec_price_before, ref decimal adec_price_after, ref decimal adec_material_price, ref decimal adec_active_price) //based on w_pack_ord_inv_from_suppliers.wf_set_material_prices in OFFICE - Ron@25/05/2015>>SPUCM00005371
		{
			decimal vat_percentage = default(decimal);
			decimal ld_global_discount_percent = default(decimal);
			decimal ldec_material_discount = default(decimal);
			decimal ldec_supplier_discount = default(decimal);
			decimal ldec_percent = default(decimal);
			decimal ldec_sell_price = default(decimal);
			long[] la_suppliers = new long[6];
			long ll_index = 0;
			long ll_first_free = 0;
			long vat_number = 0;
			string ls_syntax = null;
			string ls_material_name = null;
		//	bool lb_ok = false;
			bool lb_net_price_on_return = false;
			lb_net_price_on_return = (masofonAlias.masofon.Instance.gs_vars.net_price_on_purchase_return.ToLower(CultureInfo.InvariantCulture) == "yes");
			//depends on parameter retrieve price by last price and last supplier
			//IF  f_get_parameter_value("return_items_by_last_supplier", "boolean", "false", "Bring price by last supplier for returned items, True or False.", "gds_find") = "true" THEN
			if (true)
			{
			    ViewModel.LoadData7(al_material_number, ref adec_price_before, ref adec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{
				adec_price_before = 0;
				adec_price_after = 0;
				adec_active_price = 0;
			}
			else
			{
				if (adec_price_before == 0)
				{
					adec_price_before = 0;
				}
				if (adec_price_after == 0)
				{
					adec_price_after = 0;
				}
				if (ldec_material_discount == 0)
				{
					ldec_material_discount = 0;
				}
				if (ldec_supplier_discount == 0)
				{
					ldec_supplier_discount = 0;
				}
				// SPR224
				if (adec_price_after == 0 && (adec_price_before != 0 && (ldec_material_discount == 0 && ldec_supplier_discount == 0)))
				{
					adec_price_after = adec_price_before;
				}
				if (lb_net_price_on_return)
				{
					//.. On returns - use the net cost price.
					adec_active_price = adec_price_after;
				}
				else
				{
					adec_active_price = adec_price_before;
				}
			}
			if (adec_price_before > 0)
			{
///dw_main.SetItem(row, "material_suppliers_price_before_discount", Round(adec_price_before, 2))
///dw_main_materials.SetItem(row, "material_suppliers_price_before_discount", Round(adec_price_before, 2))
			}
			if (adec_price_after > 0)
			{
///	dw_main.SetItem(row, "material_suppliers_price_after_discount", Round(adec_price_after, 2))
///	dw_main_materials.SetItem(row, "material_suppliers_price_after_discount", Round(adec_price_after, 2))
			}
			//..Rami (11.01.2005)---->
			if (masofonAlias.masofon.Instance.gs_vars.material_discount_in_purchase.ToLower(CultureInfo.InvariantCulture) == "yes")
			{
				if (ldec_material_discount > 0)
				{
///dw_main.SetItem(row, "material_discount_percent", ldec_material_discount)
				}
			}
			if (ldec_supplier_discount == 0)
			{
///dw_main.SetItem(row, "supplier_discount_percent", idec_supplier_discount)
			}
			else
			{
////	dw_main.SetItem(row, "supplier_discount_percent", ldec_supplier_discount)
			}
			//..Rami <----------------
			//IF ib_discount_amount THEN
			///ld_global_discount_percent = dw_header.GetItem<decimal>(1, "discount_percent")
			//ELSE
			////ld_global_discount_percent = dw_header.GetItem<decimal>(1, "discount_percentage")
			//END IF
			if (ld_global_discount_percent == 0)
			{
				ld_global_discount_percent = 0;
			}
			adec_material_price = adec_active_price;
			///dw_main.SetItem(row, "material_price", adec_active_price)
			// Check if the material gets a global discount
			if (ld_global_discount_percent > 0)
			{
				adec_active_price = adec_active_price * ((100 - ld_global_discount_percent) / 100);
				///dw_main.SetItem(row, "material_new_price", adec_active_price)
			}
			// Check if the material gets a specific discount
			///ldec_percent = dw_main.GetItem<decimal>(row, "material_discount_percent")
			if (ldec_percent == 0)
			{
////dw_main.SetItem(row, "material_new_price", adec_active_price)
			}
			else if (ldec_percent >= 100)
			{
				// "Error", "100% discount is not allowed"
				///	MessageBox(f_get_error_message_number(2650), f_get_error_message_number(5210))
				return 1;
			}
			else
			{
				adec_active_price = adec_active_price * ((100 - ldec_percent) / 100);
				///dw_main.SetItem (row, "material_new_price", adec_active_price)
			}
			// Check if the material gets a supplier discount
			///ldec_percent = dw_main.GetItem<decimal>(row, "supplier_discount_percent")
			if (ldec_percent == 0)
			{
///	dw_main.SetItem(row, "material_new_price", adec_active_price)
			}
			else if (ldec_percent >= 100)
			{
				// "Error", "100% discount is not allowed"
				///MessageBox(f_get_error_message_number(2650), f_get_error_message_number(5210))
				return 1;
			}
			else
			{
				adec_active_price = adec_active_price * ((100 - ldec_percent) / 100);
				////	dw_main.SetItem (row, "material_new_price", adec_active_price)
			}
			//
			////..Rami (18.08.2004)
			//IF Lower(gs_vars.update_sell_price_by_gp) = 'yes' AND &
			//	Lower(gs_vars.alternitive_gp_calculation) = 'yes' THEN
			//////	dw_main.SetItem(row, "gp", dw_main.GetItem<decimal>(row, "compute_gp_1"))
			//ELSE
			//////	dw_main.SetItem(row, "gp", dw_main.GetItem<decimal>(row, "compute_gp"))
			//	
			//END IF
			//
			return 1;
		}
		public decimal uf_set_original_quantity(long al_material_num)
		{
			//Retrun the original quantity that inserted from the source branch //Ron@20/07/2015>>SPSUM000000005413
			long ll_rowcount = 0;
			long? ll_found = 0;
			decimal ldc_quantity = default(decimal);
			string ls_find = null;
			
			ll_rowcount = this.ViewModel.ids_source_returns_listProperty.RowCount();
			ls_find = "material_number == " + al_material_num.ToString();
			
			ll_found = this.ViewModel.ids_source_returns_listProperty.Find(ls_find, 0, (int)ll_rowcount);
			if (ll_found >= 0)
			{
				
				ldc_quantity = this.ViewModel.ids_source_returns_listProperty.GetItemValue<decimal>((int)ll_found, "material_quantity");
			}
			else
			{
				ldc_quantity = 0;
			}
			return ldc_quantity;
		}
		public int uf_set_diff() //Ron@02/08/2015>>SPUCM00005412
		{
			GridElement dw_diff = this.GetVisualElementById<GridElement>("dw_diff");
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			//add rows that were not scanned and filter rows that the quantity are the same in the source and in the target
			long ll_index = 0;
			long ll_rowcount = 0;
			long? ll_found = 0;
			long ll_material_num = 0;
			long ll_new_row = 0;
			decimal ldc_quantity = default(decimal);
			string ls_find = null;
			string ls_name = null;
			string ls_barcode = null;
			string ls_filter = null;
			ls_filter = "supplier_number <> material_quantity";
			
			ll_rowcount = this.ViewModel.ids_source_returns_listProperty.RowCount();
			for (ll_index = 1; ll_index <= ll_rowcount; ll_index++)
			{
				
				ll_material_num = this.ViewModel.ids_source_returns_listProperty.GetItemValue<long>((int)ll_index, "material_number");
				ls_find = "material_number == " + ll_material_num.ToString();
				
				
				ll_found = dw_return_details.Find(ls_find, 0, dw_return_details.RowCount());
				if (ll_found == -1) //The user did not scan the barcode, so insert it to the diff report
				{
					
					ldc_quantity = this.ViewModel.ids_source_returns_listProperty.GetItemValue<decimal>((int)ll_index, "material_quantity");
					
					ls_barcode = this.ViewModel.ids_source_returns_listProperty.GetItemValue<string>(ll_index, "barcode");
					
					ls_name = this.ViewModel.ids_source_returns_listProperty.GetItemValue<string>(ll_index, "name");
					
					ll_new_row = dw_diff.Insert(0);
					
					dw_diff.SetItem(ll_new_row, "supplier_number", ldc_quantity.ToString());
					
					dw_diff.SetItem(ll_new_row, "barcode", Convert.ToString(ls_barcode));
					
					dw_diff.SetItem(ll_new_row, "material_name", Convert.ToString(ls_name));
					
					dw_diff.SetItem(ll_new_row, "material_quantity", "0");
				}
			}
			
			dw_diff.SetFilter(ls_filter);
			
			dw_diff.Filter();
			return 1;
		}
	
		public void destructor()
		{
			base.destructor();



		}
		
		public async Task<int> cb_delete_item_clicked(object sender, EventArgs e)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			long ll_row = 0;
			long ll_serial_number = 0;
			long ll_branch_number = 0;
			DateTime? ldt_current_datetime = null;
			if (this.ViewModel.is_stateProperty == "S")
			{
			    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "לא ניתן לשנות סטטוס מלאי סגור");
				return 1;
			}
			//uf_calculate_declines
			ldt_current_datetime =DateTime.Now;
			
			ll_row = dw_return_details.GetRow();
			if (ll_row > 0)
			{
				
				ll_serial_number = dw_return_details.GetItemValue<long>(ll_row, "serial_number");
				
				ll_branch_number = dw_return_details.GetItemValue<long>(ll_row, "branch_number");
			    ViewModel.UpdateData14(ldt_current_datetime, ll_serial_number, ll_branch_number);
				//UPDATE invoice_details
				//	SET state = "d", last_update_datetime = :ldt_current_datetime
				//	WHERE serial_number = :ll_serial_number AND branch_number = :ll_branch_number USING(SQLCA);
				//Ron@10/06/2015>>SPUCM00005384 - add and remark - end
				
				dw_return_details.Delete((int)ll_row);
			}
            return 0;
		}
		
		public async Task cb_finish_clicked(object sender, EventArgs e)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
			GridElement dw_branch_list = this.GetVisualElementById<GridElement>("dw_branch_list");
			
			dw_branch_list.Reset();
			
			dw_returns.Reset();
			
			dw_return_details.Reset();
		    ViewModel.uf_create();
			await this.ViewModel.uf_display("ENTRANCE");
		}
		
		public void dw_branch_list_itemchanged(int row, VisualElement dwo, string data)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
			long ll_rowcount = 0;
			
			if (Convert.ToString(dwo.ID) == "branch_num")
			{
				if (data != "0" && !(data == null))
				{
					this.ViewModel.il_branch_as_supp_numProperty = Convert.ToInt64(data);
					
					this.ViewModel.idwc_returnProperty.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_branch_as_supp_numProperty, "p");
					dw_returns.Visible = true;
					//Ron@20/07/2015>>SPUCM00005413 - start	
					//get supplier number of the source branch
					if (true)
					{
					    ViewModel.LoadData8();
					}
					//Ron@20/07/2015>>SPUCM00005413 - end
				}
				else
				{
					dw_returns.Visible = false;
				}
			}
			
			dw_returns.Reset();
			
			dw_return_details.Reset();
			
			dw_returns.Insert(0);
		}
		
		public void dw_returns_ue_enter(Keys key, uint keyflags)
		{
			return;
			//
			//long ll_row
			//
			//IF  keyflags = 0 AND key = KeyEnter! THEN
			//	dw_return_details.Reset()
			//	This.AcceptText()
			//	il_return_number = This.GetItem<int>(1,"return_number")
			//	IF ISNULL(il_return_number) OR il_return_number = 0 THEN
			//		ll_row = dw_return_details.Insert(0)
			//		dw_return_details.SetItem(ll_row,"row_no",ll_row)
			//		ib_new_return = TRUE
			//		dw_return_details.Enabled = TRUE
			//		is_state = ""
			//	END IF	
			//END IF
			//
		}
		
		public async Task<int> dw_returns_itemchanged(int row, VisualElement dwo, string data)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			long ll_return_number = 0;
			long? ll_found_row = 0;
			long ll_new_row = 0;
			long ll_rowcount = 0;
			string ls_find = null;
			
			dw_return_details.Reset();
			ll_return_number = Convert.ToInt64(data);
			if (this.ViewModel.ib_user_type_invoice_numProperty)
			{
				this.ViewModel.ib_user_type_invoice_numProperty = false;
				if (data == null)
				{
					dw_return_details.Enabled = true;
					return 0;
				}
				ls_find = "invoice_number  == " + ll_return_number.ToString();
				
				
				ll_found_row = this.ViewModel.idwc_returnProperty.Find(ls_find, 0, this.ViewModel.idwc_returnProperty.RowCount());
				if (ll_found_row == -1)
				{
				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "מספר החזרה לא קיים במערכת");
					return 1;
				}
                //ib_new_return = TRUE
                //		ll_new_row = dw_return_details.Insert(0)
                //		dw_return_details.SetItem(ll_new_row,"row_no",ll_new_row)
                //		RETURN
                //	END IF
                return 0;
            }
			
			
			this.ViewModel.is_stateProperty = this.ViewModel.idwc_returnProperty.GetItemValue<string>(this.ViewModel.idwc_returnProperty.GetRow(), "state");
			if (this.ViewModel.is_stateProperty == "S")
			{
				dw_return_details.Enabled = false;
			}
			else
			{
				dw_return_details.Enabled = true;
			}
			//get here  only if its not new return
			uf_set_data();
			dw_return_details.Focus(); //Ron@20/07/2015>>SPUCM00005413
            return 0;
		}
		
		public void dw_returns_editchanged(int row, VisualElement dwo, string data)
		{
			this.ViewModel.ib_user_type_invoice_numProperty = true;
		}
		
		public int dw_returns_itemerror(int row, VisualElement dwo, string data)
		{
			return 1;
		}
		
		public async Task<int> dw_return_details_ue_enter(Keys key, uint keyflags, EventArgs e)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			long ll_row = 0;
			long ll_rowcount = 0;
			string ls_col_name = null;
			string ls_message_rc = null;
			if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
			{
				
				dw_return_details.PerformValidated(e);
				
				ll_rowcount = dw_return_details.RowCount();
				
				ll_row = dw_return_details.GetRow();
				 
				ls_col_name = dw_return_details.GetColumnName();
				if (ls_col_name == "material_quantity" && ll_row == ll_rowcount) //if its the last row and last col then insert new row and save current row
				{
					ls_message_rc = uf_check_data(ll_row);
					if (!string.IsNullOrEmpty(ls_message_rc))
					{
						await MessageBox.Show("חסר נתון בשדה " + ls_message_rc, "");
						return 1;
					}
				    await uf_save_data(ll_row);
					
					ll_row = dw_return_details.Insert(0);
					
					dw_return_details.SetItem(ll_row, "row_no", ll_row.ToString());
					 
					dw_return_details.SetColumn("barcode"); //Ron@20/07/2015
				}
				else
				{
					f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_return_details);
					return 1; //the return msut be here in order to prevent going to the next row ןin each tab
				}
			}
            return 0;
        }
		
		public async Task<int> dw_return_details_itemchanged(int row, VisualElement dwo, string data)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			int li_index = 0;
			int li_len = 0;
			long ll_material_number = 0;
			long ll_serial_number = 0;
			string ls_material_name = null;
			string ls_barcode = null;
			string ls_material_status = null;
			decimal ldec_price_before = default(decimal);
			decimal ldec_price_after = default(decimal);
			decimal ldec_material_price = default(decimal);
			decimal ldec_activate_price = default(decimal);
			decimal ldec_origimal_quantity = default(decimal);
			
			if (dwo.ID == "barcode")
			{
				//RonY@26/04/2015 - SPUCM00005314 - start		
				ls_barcode = data;
				// Get full barcode if it was particulaly inserted
				ls_barcode =await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
				// If barcode does not exist, drop leading zeros
				if (string.IsNullOrEmpty(ls_barcode))
				{
					li_len = data.Length;
					for (li_index = 1; li_index <= li_len; li_index++)
					{
						if (data.Substring(0, 1) == "0")
						{
							data = data.Substring(data.Length - (data.Length - 1));
							
							li_index++;
						}
						else
						{
							break; // TODO: might not be correct. Was : Exit For
						}
					}
					ls_barcode = data;
					ls_barcode =await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
				}
				//RonY@26/04/2015 - SPUCM00005314 - end
				if (true)
				{
				    ViewModel.LoadData9(ls_barcode, ref ls_material_name, ref ll_material_number, ref ls_material_status);
				}
				//Ron@27/05/2015>>SPUCM00005370 - start
				if (ls_material_status.ToUpper() == "D")
				{
					this.ViewModel.ib_ignore_err_msgProperty = true;
				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "לא ניתן להחזיר פריט בסטטוס מבוטל");
					return 2;
				}
				else if (ls_material_status.ToUpper() == "E")
				{
					this.ViewModel.ib_ignore_err_msgProperty = true;
				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "לא ניתן להחזיר פריט בסטטוס  שגוי לוגית");
					return 2;
				}
				//Ron@27/05/2015>>SPUCM00005370 - end	
				//Ron@20/06/2015>>SPUCM00005413 - start
				//This.SetItem(row,"supplier_number",il_supplier_num)
				ldec_origimal_quantity = uf_set_original_quantity(ll_material_number);
				
				dw_return_details.SetItem(row, "supplier_number", ldec_origimal_quantity.ToString());
				//Ron@20/06/2015>>SPUCM00005413 - end
				
				dw_return_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
				
				dw_return_details.SetItem(row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
				
				dw_return_details.SetItem(row, "material_number", ll_material_number.ToString());
				//Ron@28/05/2015>>SPUCM00005371 - start
                uf_set_material_prices(ll_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_price, ref ldec_activate_price);
				ldec_material_price = Math.Round(ldec_material_price, 2);
				ldec_activate_price = Math.Round(ldec_activate_price, 2);
				
				dw_return_details.SetItem(row, "material_price", ldec_material_price.ToString());
				
				dw_return_details.SetItem(row, "material_price_after_discount", ldec_activate_price.ToString());
				
				ll_serial_number = dw_return_details.GetItemValue<long>(row, "serial_number");
				if (ll_serial_number > 0 && !(ll_serial_number == 0))
				{
					
					dw_return_details.SetItem(row, "orig_serial_number", ll_serial_number.ToString());
				}
				//Ron@28/05/2015>>SPUCM00005371 - end
				// update barcode using POST
				//Ron@28/05/2015>>SPUCM00005371 - end
				// update barcode using POST
				
				dw_return_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
			}
            return 0;
        }
		
		public int dw_return_details_itemerror(int row, VisualElement dwo, string data) //Ron@27/05/2015>>SPUCM00005370
		{
			if (this.ViewModel.ib_ignore_err_msgProperty)
			{
				this.ViewModel.ib_ignore_err_msgProperty = false;
				return 1;
			}
            return 0;
        }
		
		public async Task cb_stock_clicked(object sender, EventArgs e)
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			long ll_rowcount = 0;
			long ll_index = 0;
			
			ll_rowcount = dw_return_details.RowCount();
			for (ll_index = 1; ll_index <= ll_rowcount; ll_index++)
			{
			    await uf_save_data(ll_index);
			}
			//ron@02/08/2015>>SPUCM00005412 - start
			if (await this.ViewModel.uf_show_message("הודעה", "", "YESNO", "האם להדפיס החזרה?") == 1)
			{
			    await cb_pri_clicked(null, EventArgs.Empty);
			}
            //ron@02/08/2015>>SPUCM00005412 - end
		    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "עודכן בהצלחה");
		}
		
		public void cb_print_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			//	*Object:	            uo_mini_terminal_returns.cb_print
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*12/09/2013	 AlexKh        1.1.1.12	  CR#1160    Initial version
			//********************************************************************

		    GridElement lds_print;
			GridElement ldwc;
			GridElement ldwc_decline;
			GridElement ldwc_head;
			GridElement ldwc_footer;
			GridElement ldwc_rows;
			long ll_print_copies_number = 0;
			long ll_i = 0;
			long ll_ret = 0;
			
			if (dw_return_details.RowCount() == 0)
			{
				return;
			}
			if (this.ViewModel.il_return_number == 0 || (this.ViewModel.il_return_number <= 0))
			{
				return;
			}
			if (this.ViewModel.il_branch_as_supp_numProperty == 0 || (this.ViewModel.il_branch_as_supp_numProperty <= 0))
			{
				return;
			}
			// Retrieve document (shipment) details
			
            lds_print = VisualElementHelper.CreateFromView<GridElement>("dwh_return_from_branch", "dwh_return_from_branch", "global_global");
			
			// AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch_number
			//IF lds_print.Retrieve(il_return_number, gs_vars.branch_number) <= 0 THEN RETURN	
			//IF lds_print.Retrieve(il_return_number, gs_vars.branch_number, il_marlog_branch) <= 0 THEN RETURN	
			
			ldwc_head = lds_print.GetVisualElementById<GridElement>("dw_1");
			
			ldwc_rows = lds_print.GetVisualElementById<GridElement>("dw_2");
			//			
			ll_ret = ldwc_head.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_return_number, this.ViewModel.il_supplier_numProperty);
			ll_ret = ldwc_rows.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_return_number, this.ViewModel.il_supplier_numProperty);
			//ldwc_footer.Retrieve(gs_vars.branch_number, il_return_number,il_supplier_num)
			//nvo_translator.fnv_rotate_dw(ldw_print)

            lds_print.Modify("DataWindow.Print", "Orientation", "2"); // 1-landscape , 2-portrait	
			
			lds_print.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName);
            return;
		}
		
		public void cb_diff_clicked(object sender, EventArgs e) //Ron@02/08/2015>>SPUCM00005412
		{
			ButtonElement cb_pri = this.GetVisualElementById<ButtonElement>("cb_pri");
			GridElement dw_diff = this.GetVisualElementById<GridElement>("dw_diff");
			GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
			ButtonElement cb_diff = this.GetVisualElementById<ButtonElement>("cb_diff");
			if (cb_diff.Text == "דו\"ח הפרשים")
			{
				dw_return_details.Visible = false;
				dw_diff.Visible = true;
				
				
				
				
				dw_return_details.RowsCopy(1, dw_return_details.RowCount(), ModelBuffer.Primary, dw_diff, 1, ModelBuffer.Primary);
				uf_set_diff(); //add rows that were not scanned and filter rows that the quantity are the same in the source and in the target
				cb_diff.Text = "סגור דו\"ח";
				cb_pri.Visible = false;
			}
			else
			{
				dw_return_details.Visible = true;
				dw_diff.Visible = false;
				
				dw_diff.Reset();
				cb_diff.Text = "דו\"ח הפרשים";
				cb_pri.Visible = true;
			}
		}
		
		public async Task cb_pri_clicked(object sender, EventArgs e) //Ron@02/08/2015
		{
		    await this.ue_print();
		}
	}
}
