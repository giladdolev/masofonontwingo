using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_invoices_lockController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_invoices_lock()
		{
			d_invoices_lockRepository repository = new d_invoices_lockRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
