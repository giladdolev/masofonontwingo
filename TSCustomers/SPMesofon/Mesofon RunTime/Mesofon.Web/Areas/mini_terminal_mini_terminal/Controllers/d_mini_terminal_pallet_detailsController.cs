using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_pallet_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_pallet_details()
		{
			d_mini_terminal_pallet_detailsRepository repository = new d_mini_terminal_pallet_detailsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
