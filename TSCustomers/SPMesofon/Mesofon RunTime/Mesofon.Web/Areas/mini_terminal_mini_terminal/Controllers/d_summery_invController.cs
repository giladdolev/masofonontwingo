using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_summery_invController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_summery_inv()
		{
			d_summery_invRepository repository = new d_summery_invRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
