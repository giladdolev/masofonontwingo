using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using global;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;
using MvcSite.Common;
using Mesofon.Repository;

namespace Mesofon.MiniTerminal.Controllers
{
	public class w_mt_materials_codesController : MvcSite.Common.SPBaseController
	{

        public ActionResult w_mt_materials_codes(s_array_arguments parms)
		{
            return this.View(new w_mt_materials_codes(parms));
		}
		private w_mt_materials_codes ViewModel
		{
			get { return this.GetRootVisualElement() as w_mt_materials_codes; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void ue_close_window(long al_selected_row)
		{
			GridElement dw_report = this.GetVisualElementById<GridElement>("dw_report");
			long ll_item_number = 0;
			if (al_selected_row >= 0)
			{
				
				ll_item_number = dw_report.GetItemValue<long>(al_selected_row, "number");
			}
			
			WindowHelper.Close(this.ViewModel, ll_item_number);
		}
		public void wf_get_window_position() //--------------------------------------------------------------------
		{
			//Function:			public w_mt_materials_codes.wf_get_window_position()
			//
			// Returns:         None
			//
			// Copyright  - Stas
			//
			// Date Created: 15/09/2005
			//
			// Description:	
			// 					set start window position
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 65;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 300;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			//ll_ypos += (il_max_window_height - this.height) /2
			//ll_xpos += (il_max_window_width - this.width) /2
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
		
		public void open()
		{
			GridElement dw_report = this.ViewModel.GetVisualElementById<GridElement>("dw_report");
			string ls_reverse_material_barcode = null;
			s_array_arguments lstr_parms = new s_array_arguments();
            dw_report.SetRepository(new d_sql_mt_material_codeRepository());

            if (!(this.ViewModel.Parms != null))
			{
				return;
			}

		    try
		    {
                lstr_parms = this.ViewModel.Parms;

            }
		    catch (Exception e)
		    {
		        return;
		    }


			if (lstr_parms.a_string.Length == 0)
			{
				return;
			}
			//wf_resize();
			//.. set start window position
			//wf_get_window_position();
			ls_reverse_material_barcode = lstr_parms.a_string[0];
			if (!isempty_stringClass.isempty_string(ls_reverse_material_barcode))
			{
				dw_report.Focus();
                
                dw_report.Retrieve(ls_reverse_material_barcode);
			}
		}
        public void Key(Keys key)
		{
			if (key == Keys.Escape)
			{
				this.ue_close_window(0);
			}
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
			this.ue_close_window(0);
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			GridElement dw_report = this.GetVisualElementById<GridElement>("dw_report");
			
			this.ue_close_window(dw_report.SelectedRowIndex);
		}
		
		public void dw_report_ue_key(Keys key, uint keyflags)
		{
			u_dw_report dw_report = this.GetVisualElementById<u_dw_report>("dw_report");
			if (key == Keys.Down || (key == Keys.Up || (key == Keys.PageDown || key == Keys.PageUp)))
			{
				this.dw_report_ue_post_key();
			}
			else if (key == Keys.Enter || key == Keys.Tab)
			{
				
				this.ue_close_window(dw_report.GetSelectedRow(0));
			}
		}
		public void dw_report_ue_post_key()
		{
			u_dw_report dw_report = this.GetVisualElementById<u_dw_report>("dw_report");
			
			if (dw_report.RowCount() > 0)
			{
				
				dw_report.SelectRow(0, false);
				
				
				dw_report.SelectRow((int)dw_report.GetRow(), true);
			}
		}
		
		public void dw_report_doubleclicked(int xpos, int ypos, int row, ControlElement dwo)
		{
			u_dw_report dw_report = this.GetVisualElementById<u_dw_report>("dw_report");
            dw_report.PerformDoubleClick(new EventArgs());
			if (row > 0)
			{
				
				this.ue_close_window(dw_report.GetSelectedRow(0));
			}
		}
		
		public void dw_report_retrieveend(int l)
		{
			u_dw_report dw_report = this.GetVisualElementById<u_dw_report>("dw_report");
			
			dw_report.retrieveend(default(long));
			
			if (dw_report.RowCount() > 0)
			{
				
				dw_report.SelectRow(1, true);
			}
		}
	}
}
