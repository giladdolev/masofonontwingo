using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mini_terminal_input_bodyController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mini_terminal_input_body()
		{
			d_sql_mini_terminal_input_bodyRepository repository = new d_sql_mini_terminal_input_bodyRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
