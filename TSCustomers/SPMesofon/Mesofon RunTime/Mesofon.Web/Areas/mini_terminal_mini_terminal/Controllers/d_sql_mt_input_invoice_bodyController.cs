using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mt_input_invoice_bodyController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mt_input_invoice_body()
		{
			d_sql_mt_input_invoice_bodyRepository repository = new d_sql_mt_input_invoice_bodyRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
