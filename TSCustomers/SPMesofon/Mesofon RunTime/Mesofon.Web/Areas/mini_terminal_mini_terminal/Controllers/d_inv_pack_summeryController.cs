using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_inv_pack_summeryController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_inv_pack_summery()
		{
			d_inv_pack_summeryRepository repository = new d_inv_pack_summeryRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
