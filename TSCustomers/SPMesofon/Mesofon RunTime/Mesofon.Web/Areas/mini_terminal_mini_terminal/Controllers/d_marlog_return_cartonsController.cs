using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_marlog_return_cartonsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_marlog_return_cartons()
		{
			d_marlog_return_cartonsRepository repository = new d_marlog_return_cartonsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
