using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_return_details_0_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_return_details_0_list()
		{
			d_mini_terminal_return_details_0_listRepository repository = new d_mini_terminal_return_details_0_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
