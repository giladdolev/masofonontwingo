using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_returns_headController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_returns_head()
		{
			d_mini_terminal_returns_headRepository repository = new d_mini_terminal_returns_headRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
