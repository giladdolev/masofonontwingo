using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_return_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_return_details()
		{
			d_mini_terminal_return_detailsRepository repository = new d_mini_terminal_return_detailsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
