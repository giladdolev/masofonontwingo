using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_invoice_moveController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_invoice_move()
		{
			d_invoice_moveRepository repository = new d_invoice_moveRepository();
			
			return this.View(repository);
		}
	}
}
