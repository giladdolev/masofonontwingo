using System;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using global;
using Mesofon.Repository;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Models;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.MiniTerminal.Controllers;
using MvcSite.Common;

namespace Mesofon.Controllers
{
    public class uo_mini_terminal_bodyController : uo_mini_terminal_base_bodyController
    {

        public ActionResult uo_mini_terminal_body()
        {
            return this.View(new uo_mini_terminal_body());
        }
        private uo_mini_terminal_body ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_body; }
        }

        public bool flagForLockedInvoice = false;

        public void Form_Load(object sender, EventArgs e)
        {
            if (ViewModel.PreventLoadEvent)
                return;
            GridElement dw_inv_pack_details = this.ViewModel.GetVisualElementById<GridElement>("dw_inv_pack_details");
            this.ViewModel.constructor();
            dw_inv_pack_details.SetRepository(new d_mini_terminal_details_displayRepository(), false, true);
        }

        public async void cb_def_report_clicked(object sender, EventArgs e)
        {
            await base.cb_def_report_clicked(sender, e);
            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e1) =>
            {
                this.ViewModel.dw_inv_pack_details.SetFocus(this.ViewModel.dw_inv_pack_details.RowCount - 1, "barcode");
            });
        }

        public async void cb_finish_clicked(object sender, EventArgs e)
        {
            //var test = base.cb_finish_clicked(sender, e);
            await base.cb_finish_clicked(sender, e);
        }

        public async Task cb_show_all_clicked(object sender, EventArgs e)
        {
            await base.cb_show_all_clicked(sender, e);
        }

        public async Task cb_delete_item_clicked(object sender, EventArgs e)
        {
            await base.cb_delete_item_clicked(sender, e);
        }


        public int uf_get_doc_no_en(long al_row_no, long al_material_no) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_get_doc_no_en
            //*Purpose:							Get the value that should be in "doc_no_en" column.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long						al_row_no
            //*										Value			Long						al_material_no
            //*		
            //*Return:								Integer: The number of potential invoices for the item.
            //*
            //*Date 			Programer		Version					SPUCM		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*03-08-2008		SharonS   		1.2.36.0->1.2.37.8 	00000094	10012 		Initiation
            //************************************************************************************************

            int li_rtn = 0;
            return Convert.ToInt32((this.ViewModel.Parent.Parent.Parent as w_mini_terminal).wf_update_item(al_row_no));

        }
       


        public void destructor()
        {
            base.destructor();
            if (this.ViewModel.iuo_order_searchProperty != null)
            {


            }
        }
        public async Task cb_return_to_header_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			cb_return_to_header.clicked event
            //*Purpose:							Display uo_mini_terminal_header.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.			0.2.0  		B2B 			Initiation
            //************************************************************************************************


            //this.ViewModel.cb_return_to_header.clicked();
            await this.ViewModel.uf_display("HEADER");
        }

        public async Task dw_inv_pack_details_itemfocuschanged(object sender, EventArgs e) //Defect No.132-Start
        {
            //dw_inv_pack_details_itemfocuschanged(0, default(ControlElement));
            string ls_barcode = null;
            if (this.ViewModel.Visible == true)
            {
                // Galilcs : TODO SelectedRowIndex
                //long row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;
                //if (row == -1)
                //    row = 0;
                long row = 0;
                object currentRow = this.ViewModel.dw_inv_pack_details.CurrentRow?.Index;
                if (currentRow != null && Convert.ToInt32(currentRow) != -1)
                {
                    ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>((Convert.ToInt64(currentRow)), "barcode");
                    if (string.IsNullOrEmpty(ls_barcode) )
                    {
                        await this.ViewModel.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", (Convert.ToInt64(currentRow)), "barcode");
                    }
                }
            }
            //Defect No.132-End 
        }
        public async Task dw_inv_pack_details_clicked(object sender, EventArgs e)
        {
            int row = this.ViewModel.dw_inv_pack_details.SelectedRowIndex;

            await base.dw_inv_pack_details_clicked(0, 0, row, this.ViewModel.dw_inv_pack_details);
        }

        public void dw_inv_pack_details_EditChanged(object sender, GridCellEventArgs e) // AlexKh - 1.2.44.1 - 2009-05-11 - SPUCM00001251 - calculate time for barcode insertion
        {
            base.dw_inv_pack_details_editchanged(sender, e);
        }

        public void dw_inv_pack_details_rowfocuschanged(object sender, GridElementSelectionChangeEventArgs e) //**********************************************************************************************
        {
            //dw_inv_pack_details_rowfocuschanged(0);
            string ls_doc_state = null;
            object currentrow = e.SelectedRows?[0];
            // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Delete items
            if (currentrow != null && Convert.ToInt32(currentrow) != -1)
            {

                ls_doc_state = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(currentrow), "doc_state");
                var cb_delete_item = this.GetVisualElementById<ButtonElement>("cb_delete_item");

                if (ls_doc_state == "O")
                {
                    cb_delete_item.Enabled = true;
                    //GalilCS - Update currentrow
                    cb_delete_item.Tag = currentrow;
                }
                else
                {
                    cb_delete_item.Enabled = false;
                    //GalilCS - Update currentrow
                    cb_delete_item.Tag = currentrow;
                }
            }
            // End
        }


        public async Task<int> dw_inv_pack_details_rowfocuschanging(object sender, GridElementSelectionChangeEventArgs e) //**************************************************************************
        {
            //*Object:								uo_mini_terminal_נםגט
            //*Function/Event  Name:			dw_inv_pack_details.rowfocuschanging
            //*Purpose:							In case a change was done save it.
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long						currentrow
            //*										Value			Long						newrow
            //*
            //*Return:								Long							
            //*Date 			    Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*15-10-2007		    SHARP.				0.2.0 			B2B 			Initiation
            //************************************************************************************************




            //dw_inv_pack_details_rowfocuschanging(0, 0);
            bool lb_update = false;
            long ll_rtn_code = 0;
            string ls_barcode = null;
            string ls_doc_state = null;
            object currentrow = null;
            if (this.ViewModel.dw_inv_pack_details.PreviousRow != null)
            { 
                 currentrow = this.ViewModel.dw_inv_pack_details.PreviousRow.Index;
            }
            if (currentrow == null || Convert.ToInt32(currentrow) == -1)
            {
                return 0;
            }
            //// SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Delete items
            //ls_doc_state = THIS.GetItem<string>(newrow, "doc_state")
            //IF ls_doc_state = 'O' THEN
            //	cb_delete_row.Enabled = True
            //ELSE
            //	cb_delete_row.Enabled = False
            //END IF
            //// End


            ModelAction ldw_row_status = this.ViewModel.dw_inv_pack_details.GetItemStatus((int)currentrow, 0, ModelBuffer.Primary);
            if (ldw_row_status == ModelAction.UpdateByKeys || ldw_row_status == ModelAction.Insert)
            {
                ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(currentrow), "barcode");
                if (isempty_stringClass.isempty_string(ls_barcode))
                {
                    return 0;
                }
                flagForLockedInvoice = false;
                Mesofon.Common.Extensions.ExecuteOnResponeEx((o, ee) =>
                {
                    this.ViewModel.uf_update_item(Convert.ToInt64(currentrow));
                }, 1);
                return 0;
            }
            return 0;
        }

        public async Task<int> dw_inv_pack_details_itemchanged(object sender, GridCellEventArgs e) //**********************************************************************************************
        {

            string dwoID = e.DataIndex;
            string data = e.Value;
            int row = e.RowIndex;
            if (row == -1)
                return 0;

            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			dw_inv_pack_details.ItemChanged
            //*Purpose:							Check validation and call the uf_set_items function
            //*										
            //*  
            //*Arguments:						Pass By			Argument Type 		Argument Name
            //*										-------------------------------------------------------------------------------
            //*										Value 			Long						row
            //*										Value				ControlElement				dwo
            //*										Value 			String 					data
            //*Return:								Long.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*03-08-2007    SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************



            GridElement elem = sender as GridElement;
            elem.Tag = "FiringWidgetControlItemChanged";

            //await dw_inv_pack_details_itemchanged(0, default(ControlElement), default(string));
            char lc_doc_type = '\0';
            string lc_doc_state = "";
            string ls_col_name = null;
            string ls_barcode = null;
            string ls_string = null;
            string ls_message = null;
            int li_len = 0;
            int li_index = 0;
            int li_rtn_code = 0;
            int li_b2b_status = 0;
            int li_status = 0;
            long ll_supplier_number = 0;
            long ll_material_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_order_number = 0;
            long ll_doc_no = 0;
            long ll_next_serial_number = 0;
            long? ll_current_doc_no = 0;
            long ll_new_doc_no = 0;
            long ll_actual_quantity = 0;
            bool lb_material_assigned_for_order = false;
            bool lb_allowed_change = false;
            bool lb_change_doc = false;
            s_array_arguments lstr_mess = default(s_array_arguments);
            IRepository lds_package;
            ModelAction ldis_status = default(ModelAction);
            if (Convert.ToString(this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, dwoID)) == data)
            {
                return 0;
            }
            try
            {

                ldis_status = this.ViewModel.dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);

                ls_col_name = dwoID;
                // SharonS - 1.2.33.1 - 2008-07-10- CR#1098 - Disable items in closed invoice/packing list
                // SharonS - 1.2.33.1 - 2008-07-10- CR#1098 - Disable items in closed invoice/packing list

                lc_doc_state = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "doc_state");
                if (lc_doc_state == "C" || lc_doc_state == "S")
                {
                    return 2;
                }
                // End
                switch (ls_col_name)
                {
                    case "barcode":
                        this.ViewModel.is_new_barcode = data;

                        this.ViewModel.is_old_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        // AlexKh - 1.2.44.1 - 2009-05-11 - SPUCM00001251 - calculate time for barcode insertion
                        if (this.ViewModel.il_start_time > 0)
                        {
                            // AlexKh - 1.1.26.0 - 2015-01-11 - SPUCM00005185 - save employee number in addition
                            //ls_string = String(CPU() - il_start_time) + "ms, " + is_start_time

                            ls_string = ((DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond) - this.ViewModel.il_start_time) + "ms, " + " EmpNo<" + this.ViewModel.il_employee_number.ToString() + ">";

                            this.ViewModel.dw_inv_pack_details.SetItem(row, "details", Convert.ToString(ls_string));

                            this.ViewModel.is_start_time = null;
                            this.ViewModel.il_start_time = 0;
                        }
                        // AlexKh - 2009-05-11 - End
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "CASE barcode ", Convert.ToString("Row:      " + row.ToString()), Convert.ToString("The Data: " + data));
                        ls_barcode = data;
                        // AlexKh - 1.2.44.1 - 2009-05-13 - SPUCM00001250 - check if barcode overriden then give a message
                        if (this.ViewModel.is_old_barcode != null && this.ViewModel.is_old_barcode.Length > 0)
                        {
                            li_rtn_code = await this.ViewModel.uf_show_message("אזהרה", "", "OKCancel", (masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("שים לב אחד הפריטים הולך להדרס, האם להמשיך?").Result));
                            if (li_rtn_code == -1)
                            {

                                this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", Convert.ToString(this.ViewModel.is_old_barcode));
                                return 1;
                            }
                        }
                        // AlexKh - 2009-05-13 - End
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "Before uf_get_barcode_main function.", "", "");
                        // Get full barcode if it was particulaly inserted
                        // PninaSG - 1.2.23.0 - 2008-01-02- TASK#10000 - Start
                        //ls_barcode = PARENT.iuo_material_search.uf_get_barcode_main(ls_barcode)
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        // SharonS - 1.2.32.0 - 2008-07-08- CR#1095 - If barcode does not exist, drop leading zeros
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            li_len = data.Length;
                            for (li_index = 1; li_index <= li_len; li_index++)
                            {
                                if (data.Substring(0, 1) == "0")
                                {
                                    data = data.Substring(data.Length - (data.Length - 1));

                                    li_index++;
                                }
                                else
                                {
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }
                            //AlexKh - fix - 11/10/2012 - moved inside IF
                            ls_barcode = data;
                            ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        }
                        // SharonS - 1.2.42.1 - 2009-03-01 - Add supplier number to the select 
                        // Get supplier number
                        li_rtn_code = (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_get_selected_supplier_number of ItemChanged: CASE barcode ", "", "");
                        if (li_rtn_code < 1)
                        {
                            return 2;
                        }
                        // End
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            // SharonS - 1.2.42.1 - 2009-03-01 - Add supplier number to the select 
                            //			lstr_mess = f_is_pack_barcode(TRUE, data)
                            lstr_mess = await f_is_pack_barcodeClass.f_is_pack_barcode(true, data, ll_supplier_number);
                            // End
                            if (lstr_mess.arg_ok == false)
                            {
                                //Message
                                await (f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, await f_get_error_message_numberClass.f_get_error_message_number(5092)));
                            }
                            else
                            {
                                //Set item
                                lds_package = new d_packageRepository();
                                lds_package = lstr_mess.a_obj[1] as IRepository;
                                ls_barcode = lds_package.GetItem<d_package>(1).materials_barcode;
                                ll_actual_quantity = Convert.ToInt64(lds_package.GetItem<d_package>(1).cf_total_quantity);

                                this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));

                                this.ViewModel.dw_inv_pack_details.SetItem(row, "expected_material_quantity", ll_actual_quantity.ToString());
                                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
                                if (lds_package != null)
                                {

                                }
                                // End
                            }
                        }
                        // END
                        this.ViewModel.is_new_barcode = ls_barcode;
                        //Defect No.132 - Start
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            string ls = null;

                            ls = this.ViewModel.dw_inv_pack_details.Parent.GetType().Name;
                            await this.ViewModel.uf_set_focus("uo_mini_terminal_base_body", "dw_inv_pack_details", row, "barcode");
                        }
                        //Defect No.132 - End 
                        //this.object.barcode[row] = ls_barcode
                        long ll_rtn_code = 0;

                        ll_rtn_code = this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", ls_barcode);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_get_barcode_main of ItemChanged: CASE barcode ", "", "");
                        if (isempty_stringClass.isempty_string(ls_barcode))
                        {
                            //If material with this barcode not exists fill empty values into this row
                            this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", "");
                            this.ViewModel.dw_inv_pack_details.SetItem(row, "material_name", "");
                            this.ViewModel.dw_inv_pack_details.SetItem(row, "material_price", 0);
                            this.ViewModel.dw_inv_pack_details.SetItem(row, "min_valid_months", 0);
                            if (this.ViewModel.dw_inv_pack_details.GetItemValue<Int64>(row, "indicator") == 0)
                            {
                                this.ViewModel.dw_inv_pack_details.SetItem(row, "indicator", Convert.ToString(Convert.ToInt64(this.ViewModel.dw_inv_pack_details.Describe("Evaluate('MAX(indicator for all)',1)")) + 1));
                            }
                            return 1;
                        }
                        ll_material_number = await this.ViewModel.iuo_material_search.uf_get_material_number(ls_barcode);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_get_material_number of ItemChanged: CASE barcode ", "", "");
                        // SharonS - 1.2.24.0 - 2008-02-28 - Bug No. 214 - Check the material status after getting the material number
                        bool statusAllowPurches;
                        statusAllowPurches = (await this.ViewModel.iuo_material_search.uf_material_status_allow_purches(ll_material_number, ls_message)).Retrieve(out ls_message);
                        if (!statusAllowPurches)
                        {
                            // SharonS - 1.2.24.0 - 2008-02-28 - Bug No. 214 - Show the message before writing to the log
                            await this.ViewModel.uf_show_message("Error", "", "1", Convert.ToString(ls_message));
                            this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_material_status_allow_purches of ItemChanged: CASE barcode ", "", "");
                            //

                            this.ViewModel.dw_inv_pack_details.SetItem(row, "barcode", "");

                            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e3) =>
                            {
                                this.ViewModel.dw_inv_pack_details.SetFocus(row, "barcode");
                            });

                            return 2;
                        }
                        //
                        (await this.ViewModel.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_get_selected_order_number of ItemChanged: CASE barcode ", "", "");
                        lb_material_assigned_for_order = this.ViewModel.iuo_order_searchProperty.uf_material_assigns_to_order(ll_material_number);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_material_assigns_to_order of ItemChanged: CASE barcode ", "", "");
                        // Get supplier number
                        li_rtn_code = (await this.ViewModel.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_get_selected_supplier_number of ItemChanged: CASE barcode ", "", "");
                        if (li_rtn_code < 1)
                        {
                            return 2;
                        }
                        // Check last supplier for this material - ls_message get the message from the function  " X  החומר נקנה לאחרונה מספק
                        if (!(await this.ViewModel.iuo_material_search.uf_material_belong_to_current_supplier(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_material_number, ls_message)).Retrieve(out ls_message))
                        {
                            await this.ViewModel.uf_show_message("Error", "", "OK", Convert.ToString(ls_message));
                        }
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_material_belong_to_current_supplier of ItemChanged: CASE barcode ", "", "");
                        // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        //uf_set_items("barcode", data, row)
                        await this.ViewModel.uf_set_items("barcode", ls_barcode, row);
                        // PninaSG - 1.2.23.0 - 2008-01-02- TASK#10000 - Start
                        if (ll_actual_quantity > 0)
                        {
                            await this.ViewModel.uf_set_items("expected_material_quantity", ll_actual_quantity.ToString(), row);
                        }
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_set_items of ItemChanged: CASE barcode ", "", "");
                        break;
                    case "expected_material_quantity":
                        if (string.IsNullOrEmpty(this.ViewModel.is_new_barcode))
                        {

                            this.ViewModel.is_new_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                            this.ViewModel.is_old_barcode = this.ViewModel.is_new_barcode;
                        }
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "Start CASE expected_material_quantity ", Convert.ToString("Row:      " + row.ToString()), Convert.ToString("The Data: " + data));

                        ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            await this.ViewModel.uf_show_message("", "טעות", "OK", (masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("יש להכניס ברקוד תחילה").Result));

                            this.ViewModel.dw_inv_pack_details.SetItem(row, "expected_material_quantity", "0");
                            return 2;
                        }
                        // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        long ll_rtncode = 0;
                        ll_rtncode = await this.ViewModel.uf_set_items("expected_material_quantity", data, row);
                        if (ll_rtncode < 0)
                        {

                            this.ViewModel.dw_inv_pack_details.SetItem(row, "expected_material_quantity", "0");
                            return 2;
                        }
                        // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            return 2;
                        }
                        this.ViewModel.dw_inv_pack_details.SetItem(row, "expected_material_quantity", data);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After uf_set_items of ItemChanged: CASE expected_material_quantity ", "", "");
                        break;
                    case "doc_no":
                        if (this.ViewModel.is_new_barcode == null || string.IsNullOrEmpty(this.ViewModel.is_new_barcode))
                        {

                            this.ViewModel.is_new_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                            this.ViewModel.is_old_barcode = this.ViewModel.is_new_barcode;
                        }
                        //Penny Bug no 15 -Start

                        ll_current_doc_no = this.ViewModel.dw_inv_pack_details.GetItemValue<long?>(row, "doc_no");
                        if (ll_current_doc_no.Equals(null))
                        {
                            ll_current_doc_no = 0;
                        }
                        this.ViewModel.il_old_doc_no = ll_current_doc_no.Value;
                        //PARENT.Dynamic POST wf_set_old_doc_no(ll_current_doc_no)
                        ll_new_doc_no = Convert.ToInt64(data);
                        this.ViewModel.il_new_doc_no = ll_new_doc_no;
                        //PARENT.Dynamic POST wf_set_new_doc_no(LONG(data))


                        ldis_status = this.ViewModel.dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);

                        if (ldis_status == ModelAction.Insert)
                        {
                            ll_current_doc_no = 0;
                        }
                        lc_doc_type = await this.ViewModel.uf_get_doc_type(Convert.ToInt64(data));
                        this.ViewModel.ic_new_doc_type = lc_doc_type;
                        //PARENT.Dynamic POST wf_set_new_doc_type(lc_doc_type)
                        char lc_curr_doc_type = '\0';
                        if (!(ll_current_doc_no == 0))
                        {
                            lc_curr_doc_type = await this.ViewModel.uf_get_doc_type(ll_current_doc_no.Value);
                            this.ViewModel.ic_old_doc_type = lc_curr_doc_type;
                            //PARENT.Dynamic POST wf_set_old_doc_type(lc_curr_doc_type)
                        }

                        ls_barcode = this.ViewModel.dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        //THIS.AcceptText()
                        if (!(ll_current_doc_no == 0) && (lc_doc_type.ToString() == "I" || lc_doc_type.ToString() == "P"))
                        {
                            //lb_allowed_change = PARENT.uf_allow_change_doc_no(lc_doc_type, ll_current_doc_no)
                            lb_allowed_change = await this.uf_allow_change_doc_no(lc_curr_doc_type, ll_current_doc_no.Value);
                            if (lb_allowed_change)
                            {
                                //ib_doc_no_changed = TRUE
                                //ic_doc_type = lc_doc_type
                                this.ViewModel.il_new_doc_no = ll_new_doc_no;
                                this.ViewModel.is_barcode = ls_barcode;
                            }
                            else
                            {
                                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא ניתן לשנות את מספר המסמך מכיוון שזהו הפריט היחיד שמקושר למסמך זה");

                                this.ViewModel.dw_inv_pack_details.SetItem(row, "doc_no", ll_current_doc_no.ToString());
                                //ib_save_doc_no = FALSE
                                return 2;
                            }
                        }
                        //Penny Bug no 15 -End
                        // AlexKh - 1.2.43.1 - 2009-04-02 - SPUCM00000950 - if new item does't belong to invoice, then give a message

                        ll_material_number = this.ViewModel.dw_inv_pack_details.GetItemValue<long>(row, "material_number");
                        li_status = await this.ViewModel.uf_is_item_in_invoice(ll_new_doc_no, ll_material_number);
                        if (li_status == -1)
                        {

                            this.ViewModel.dw_inv_pack_details.SetItem(row, "doc_no", ll_current_doc_no.ToString());

                            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e2) =>
                            {
                                this.ViewModel.dw_inv_pack_details.SetFocus(row, "doc_no");
                            });
                            return 2;
                        }
                        // AlexKh - 2009-04-02 - End
                        // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - check if current document locked
                        if (!await this.ViewModel.uf_validate_doc_change(row, Convert.ToString(ls_col_name), Convert.ToString(data), this.ViewModel.dw_inv_pack_details))
                        {
                            flagForLockedInvoice = true;
                            return 2;
                        }
                        this.ViewModel.dw_inv_pack_details.SetItem(row, "doc_no", data);
                        await this.ViewModel.uf_set_items("doc_no", data, row);
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "After: uf_get_material_number of ItemChanged: CASE doc_no ", "", "");
                        this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "End of ItemChanged: CASE doc_no ", "", "");
                        break;
                    default:
                        //
                        break;
                }
                if (this.ViewModel.uf_enable_cb_finish(true) < 1)
                {
                    return 2;
                }
                this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "dw_inv_pack_details.itemchanged", "End of ItemChanged", "", "");

            }
            catch (Exception ex)
            {

            }
            finally
            {
                //GalilCS   Update value in repository
                object val = this.ViewModel.dw_inv_pack_details.GetItemValue<object>(row, e.DataIndex);
                this.ViewModel.dw_inv_pack_details.SetItem(row, e.DataIndex, val);

                Tuple<string, GridElementCellEventArgs> elemTag = elem.Tag as Tuple<string, GridElementCellEventArgs>;
                if (elemTag != null && elemTag.Item1 == "FireKeyDownAfterWidgetControlItemChanged")
                {
                    ((WidgetGridElement)elem).PerformWidgetControlSpecialKey(elemTag.Item2);
                }

                elem.Tag = null;
            }
            return 0;
        }

        public async Task dw_inv_pack_details_doubleclicked(object sender, GridCellEventArgs e) //**********************************************************************************************
        {
            string dwoID = e.DataIndex;
            await base.dw_inv_pack_details_doubleclicked(0, 0, this.ViewModel.dw_inv_pack_details.SelectedRowIndex, this.ViewModel.dw_inv_pack_details, dwoID);
        }

        private async Task dw_inv_pack_details_CellkeyDown(object sender, GridElementCellEventArgs e)
        {
            GridElement elem = sender as GridElement;

            if (Convert.ToString(elem.Tag) == "FiringWidgetControlItemChanged")
            {
                elem.Tag = Tuple.Create<string, GridElementCellEventArgs>("FireKeyDownAfterWidgetControlItemChanged", e);
                return;
            }

            if (e.KeyCode == System.Web.VisualTree.Keys.Enter || e.KeyCode == System.Web.VisualTree.Keys.Tab)
            {
                await dw_inv_pack_details_ue_enter(this.ViewModel.dw_inv_pack_details, e);
            }
            else
            {
                dw_inv_pack_details_EditChanged(sender, new GridCellEventArgs(Convert.ToString(e.KeyChar), e.WidgetDataMember));
            }
        }

        private async Task dw_inv_pack_details_CellkeyDown2(object sender, GridElementCellEventArgs e)
        {
            GridElement elem = sender as GridElement;

            if (e.KeyCode == System.Web.VisualTree.Keys.Enter || e.KeyCode == System.Web.VisualTree.Keys.Tab)
            {
            }
            else
            {
                dw_inv_pack_details_EditChanged(sender, new GridCellEventArgs(Convert.ToString(e.KeyChar), e.WidgetDataMember));
            }
        }
    }
}
