using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dddw_mini_terminal_shipments_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult dddw_mini_terminal_shipments_list()
		{
			dddw_mini_terminal_shipments_listRepository repository = new dddw_mini_terminal_shipments_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
