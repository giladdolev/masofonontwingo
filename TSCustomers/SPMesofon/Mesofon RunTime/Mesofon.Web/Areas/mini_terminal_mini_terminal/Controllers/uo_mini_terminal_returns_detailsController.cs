﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofon;
using global;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Repository;
using System.Web.VisualTree;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class uo_mini_terminal_returns_detailsController : uo_mini_terminal_base_bodyController
    {

        public ActionResult uo_mini_terminal_returns_details(GridElement dw_summary)
        {
            return this.View(new uo_mini_terminal_returns_details(dw_summary));
        }
        private uo_mini_terminal_returns_details ViewModel
        {
            get { return this.GetRootVisualElement() as uo_mini_terminal_returns_details; }
        }

        public void Form_Load(object sender, EventArgs e)
        {
            GridElement dw_inv_pack_details = this.ViewModel.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary_details");
            this.ViewModel.constructor();
            dw_inv_pack_details.SetRepository(new d_mini_terminal_return_detailsRepository(), false, true);
            dw_summary.SetRepository(new d_summary_returnsRepository(), false, true);
        }

        public async Task<bool> uf_update_item(long al_row_no) //********************************************************************
        {
            GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary_details");
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
            //	*Object:	            uo_mini_terminal_returns_details
            //	*Function Name:	   (uf_update_item)
            //	*Purpose: 	
            //	*Arguments: 	      long al_row_no
            //	*Return:	            boolean
            //	*Date		    Programmer    Version	  Task#	    Description
            //	*---------------------------------------------------------------------
            //	*19/08/2013	 AlexKh        1.1.1.	  CR#        Initial version
            //********************************************************************

            string ls_barcode = null;
            string ls_carton_number = null;
            bool lb_return_marlog = false;
            decimal ldc_qty = default(decimal);
            decimal ldc_curr_qty = default(decimal);
            decimal ldc_total_qty = default(decimal);
            decimal ldc_total_qty_1 = default(decimal);
            decimal ldec_qty = default(decimal);
            long ll_row_exists = 0;
            long ll_material_number = 0;
            long ll_supplier = 0;
            long? ll_find = 0;
            long ll_i = 0;
            long ll_count = 0;
            long ll_row = 0;
            long? ll_find_summary = 0;
            long? ll_supplier_1 = 0;
            long ll_ret = 0;
            int li_cnt = 0;
            DateTime? ldt_time = null;
            ModelAction ldw_row_status = default(ModelAction);
            ldt_time = masofonAlias.masofon.Instance.set_machine_time();
            if (this.ViewModel.il_return_number == 0)
            {
                this.ViewModel.il_return_number = await f_calculate_datatable_rowClass.f_calculate_datatable_row(144, 0, masofonAlias.masofon.Instance.sqlca);
                st_shipment_number.Text = this.ViewModel.il_return_number.ToString();
            }
            //basic data

            ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row_no, "barcode");

            ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_quantity");

            ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "material_number");

            ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number");

            ls_carton_number = dw_inv_pack_details.GetItemValue<string>(al_row_no, "carton_barcode");
            //first check if the barcode already appears on the dw


            ll_find = dw_inv_pack_details.Find("barcode == \"" + ls_barcode + "\"" + "AND carton_barcode == \"" + ls_carton_number + "\"", 0, dw_inv_pack_details.RowCount());
            if (ll_find == al_row_no)
            {

                if (ll_find < dw_inv_pack_details.RowCount() - 1)
                {
                    ll_find = dw_inv_pack_details.Find("barcode == \"" + ls_barcode + "\"" + "AND carton_barcode == \"" + ls_carton_number + "\"", (int)(ll_find) + 1, dw_inv_pack_details.RowCount());
                }
                else
                {
                    ll_find = -1;
                }
            }
            //if yes then first merge the rows into 1 row and continue
            if (ll_find >= 0) //existing barcode; all data should appear (marlog or supplier)
            {
                //Release adhoc for merged row.



                if (this.ViewModel.ids_adhoc_update_listProperty.Retrieve(dw_inv_pack_details.GetItemValue<string>(al_row_no, "adhoc_key"), masofonAlias.masofon.Instance.gs_vars.branch_number, dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number")) > 0)
                {

                    this.ViewModel.ids_adhoc_update_listProperty.SetItem(0, "is_active", "1");
                    await f_update_adhocClass.f_update_adhoc(1, this.ViewModel.il_return_number, this.ViewModel.ids_adhoc_update_listProperty);
                }

                ldc_curr_qty = dw_inv_pack_details.GetItemValue<decimal>(ll_find.Value, "material_quantity");
                ldc_qty += ldc_curr_qty;

                dw_inv_pack_details.SetItem(ll_find.Value, "material_quantity", ldc_qty.ToString());
                // AlexKh



                dw_inv_pack_details.SetItemStatus((int)ll_find, 0, ModelBuffer.Primary, ModelAction.None);

                dw_inv_pack_details.Delete((int)al_row_no);
                al_row_no = ll_find.Value;
                if (this.ViewModel.ib_marlog_returnProperty) //for marlog update dw_summary existing row
                {
                    ldc_total_qty_1 = 0;

                    for (li_cnt = 0; li_cnt < dw_inv_pack_details.RowCount(); li_cnt++)
                    {

                        ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(li_cnt, "supplier_number");
                        if (ll_supplier_1 == 0 || ll_supplier_1 != ll_supplier)
                        {
                            continue;
                        }

                        ldc_total_qty_1 += dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_quantity");
                    }

                    ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(ll_find.Value, "supplier_number");


                    ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier_1.ToString(), 0, dw_summary.RowCount());

                    dw_summary.SetItem(ll_find_summary.Value, "total", ldc_total_qty_1.ToString());
                    //for supplier dw_inv_pack_details already updated with ldc_qty
                }
                else
                {
                    ldc_total_qty = ldc_qty;
                }
                //UPDATE tables
                await uf_update_tables(al_row_no, ldc_qty, 1);
                //only row for barcode
            }
            else
            {

                ldc_curr_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_quantity");
                if (this.ViewModel.ib_marlog_returnProperty) //return to Marlog
                {
                    //search dw_summary and add this supplier qty to a new/existing row


                    ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier.ToString(), 0, dw_summary.RowCount());
                    if (ll_find_summary >= 0) //Marlog, new barcode, existing supplier
                    {
                        //update existing row
                        ldc_total_qty_1 = 0;

                        for (li_cnt = 0; li_cnt < dw_inv_pack_details.RowCount(); li_cnt++)
                        {

                            ll_supplier_1 = dw_inv_pack_details.GetItemValue<long?>(li_cnt, "supplier_number");
                            if (ll_supplier_1 != ll_supplier)
                            {
                                continue;
                            }

                            ldec_qty = dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_quantity");
                            if (ldec_qty == 0)
                            {
                                ldec_qty = 0;
                            }
                            ldc_total_qty_1 += ldec_qty;
                        }

                        dw_summary.SetItem(ll_find_summary.Value, "total", ldc_total_qty_1.ToString());


                        dw_inv_pack_details.SetItem(al_row_no, "doc_no", dw_summary.GetItemValue<double>(ll_find_summary.Value, "doc_no").ToString());
                        // AlexKh - 1.1.1.11 - 09/09/2013 - SPUCM00004291 - Check rowstatus instead row number			


                        ldw_row_status = dw_inv_pack_details.GetItemStatus((int)al_row_no, 0, ModelBuffer.Primary);


                        if (ldw_row_status == ModelAction.Insert || ldw_row_status == ModelAction.Insert)
                        {
                            //IF al_row_no = dw_inv_pack_details.RowCount() THEN //New row added
                            //UPDATE tables
                            if (await uf_update_tables(al_row_no, ldc_curr_qty, 2) == -1)
                            {
                                return false;
                            }
                            //Update existing row
                        }
                        else
                        {
                            //UPDATE tables
                            if (await uf_update_tables(al_row_no, ldc_curr_qty, 1) == -1)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        //Marlog, new barcode, new supplier
                        this.ViewModel.il_return_sonProperty = await f_calculate_datatable_rowClass.f_calculate_datatable_row(145, 0, masofonAlias.masofon.Instance.sqlca);
                        if (await uf_update_tables(al_row_no, ldc_curr_qty, 3) == -1)
                        {
                            return false;
                        }
                        //open new row; create return number; set new quantity = new

                        ll_row = dw_summary.Insert(0);

                        dw_summary.SetItem(ll_row, "doc_no", this.ViewModel.il_return_sonProperty.ToString());

                        dw_summary.SetItem(ll_row, "supplier_number", ll_supplier.ToString());

                        dw_summary.SetItem(ll_row, "total", ldc_curr_qty.ToString());
                    }
                }
                else
                {
                    ViewModel.LoadData1(ll_supplier, ref ll_row_exists, ref ldc_qty);
                    if (ll_row_exists > 0)
                    {
                        //ldc_total_qty = ldc_qty + ldc_curr_qty
                        if (await uf_update_tables(al_row_no, ldc_curr_qty, 2) == -1)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (await uf_update_tables(al_row_no, ldc_curr_qty, 3) == -1)
                        {
                            return false;
                        }
                    }
                }
                // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row



                dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None);
            }
            if (!await this.ViewModel.uf_is_new_row_exist(dw_inv_pack_details))
            {
                await this.ViewModel.uf_new_row(dw_inv_pack_details);
            }
            return true;
        }
        public bool uf_print_carton_barcode(string as_barcode) //waiting for code from Eduard
        {
            return false;
        }
        public async Task<bool> uf_set_material_prices(long al_material_number, long row, EventArgs e)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            decimal vat_percentage = default(decimal);
            decimal ldec_active_price = default(decimal);
            decimal ld_global_discount_percent = default(decimal);
            decimal ldec_material_discount = default(decimal);
            decimal ldec_supplier_discount = default(decimal);
            decimal ldec_percent = default(decimal);
            decimal ldec_price_before = default(decimal);
            decimal ldec_price_after = default(decimal);
            decimal ldec_sell_price = default(decimal);
            long[] la_suppliers = new long[6];
            long ll_index = 0;
            long ll_first_free = 0;
            long vat_number = 0;
            string ls_syntax = null;
            string ls_material_name = null; //, ls_discount_amount
            bool lb_ok = false;

            dw_inv_pack_details.PerformValidated(e);
            if (true)
            {
                ViewModel.LoadData2(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
                return false;
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100) // get the last prices received 
            {
                ViewModel.LoadData3(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
                    return false;
                }
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                ldec_price_before = 0;
                ldec_price_after = 0;
                ldec_active_price = 0;
            }
            else
            {
                if (ldec_price_before == 0)
                {
                    ldec_price_before = 0;
                }
                if (ldec_price_after == 0)
                {
                    ldec_price_after = 0;
                }
                if (ldec_material_discount == 0)
                {
                    ldec_material_discount = 0;
                }
                if (ldec_supplier_discount == 0)
                {
                    ldec_supplier_discount = 0;
                }
                if (ldec_price_after == 0 && (ldec_price_before != 0 && (ldec_material_discount == 0 && ldec_supplier_discount == 0)))
                {
                    ldec_price_after = ldec_price_before;
                }
                ldec_active_price = ldec_price_after;
            }
       
            dw_inv_pack_details.SetItem(row, "material_price", ldec_active_price.ToString());
            if (true)
            {
                ViewModel.LoadData4(al_material_number, ref ldec_sell_price);
            }


            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0 || masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
            {

                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
                return false;
            }

            dw_inv_pack_details.SetItem(row, "sell_price", ldec_sell_price.ToString());
            return true;
        }
        public bool uf_set_total(long al_supplier, ref decimal adc_total)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            decimal ldc_price = default(decimal);
            decimal? ldc_qty = default(decimal);
            long? ll_supplier = 0;
            int li_cnt = 0;
            //SELECT 

            for (li_cnt = 0; li_cnt < dw_inv_pack_details.RowCount(); li_cnt++)
            {

                ll_supplier = dw_inv_pack_details.GetItemValue<long?>(li_cnt, "supplier_number");
                if (ll_supplier != al_supplier)
                {
                    continue;
                }

                ldc_price = dw_inv_pack_details.GetItemValue<decimal>(li_cnt, "material_price");

                ldc_qty = dw_inv_pack_details.GetItemValue<decimal?>(li_cnt, "material_quantity");
                // AlexKh 1.1.1.10 - 2013-06-30 - CR#1160 - check null values
                if (ldc_price == 0)
                {
                    ldc_price = 0;
                }
                if (ldc_qty == 0 || ldc_qty == null)
                {
                    ldc_qty = 0;
                }
                adc_total += Convert.ToDecimal(ldc_price * ldc_qty);
            }
            return true;
        }
        public async Task<bool> uf_delete_item(long al_row) //String	ls_barcode
        {


            GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary_details");
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            //Boolean	lb_return_marlog
            decimal ldc_qty = default(decimal);
            decimal ldc_total_qty = default(decimal);
            long ll_supplier = 0;
            long ll_i = 0;
            long? ll_find_summary = 0;
            long ll_supplier_1 = 0;
            //basic data

            ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row, "supplier_number");
            ldc_qty = dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_quantity");
            //find dw_summary row for supplier - re-calc row


            ll_find_summary = dw_summary.Find("supplier_number == " + ll_supplier.ToString(), 0, dw_summary.RowCount());
            if (ll_find_summary >= 0)
            {

                ldc_total_qty = (decimal)dw_summary.GetItemValue<Int64>(ll_find_summary.Value, "total");
                //re-set total for summary

                dw_summary.SetItem(ll_find_summary.Value, "total", (ldc_total_qty - ldc_qty).ToString());
                //actual deletion of the row in the calling function
                await uf_update_tables(al_row, ldc_total_qty - ldc_qty, 4);
            }
            return true;
        }
        public bool uf_check_marlog_distributer(long al_distributor_number)
        {
            bool lb_marlog = false;


            lb_marlog = Convert.ToBoolean((this.ViewModel.Parent.Parent.Parent as w_mini_terminal).wf_check_marlog_distributor(-2, "MARLOG_YES", al_distributor_number));
            return lb_marlog;
        }
        public async Task<int> uf_close_window() //********************************************************************
        {
            //*Object:				uo_mini_terminal_returns
            //*Function Name:	uf_close_window
            //*Purpose: 			close window
            //*Arguments: 		none
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*27-06-2013		AlexKh			1.1.1.10	CR#1160		Initial version
            //********************************************************************

            string ls_message = null;
            int li_rtn_code = 0;
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
            li_rtn_code = this.ViewModel.uf_write_log(Convert.ToString(ls_message));
            await this.ViewModel.uf_display("ENTRANCE");
            return 1;
        }

        protected async Task<int> uf_update_tables(long al_row_no, decimal adc_qty, int al_case)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            int li_rc = 0;
            long ll_material_number = 0;
            long ll_supplier = 0;
            long ll_serial = 0;
            long ll_details_serial = 0;
            long ll_marlog_serial = 0;
            long ll_invoice = 0;
            long ll_distributor = 0;
            long ll_decline_number = 0;
            long ll_return_son = 0;
            long ll_return_serial = 0;
            long ll_row_serial_number = 0;
            decimal ldc_price = default(decimal);
            decimal ldc_total = default(decimal);
            decimal ldc_sell_price = default(decimal);
            DateTime? ldt_time = null;
            string ls_adhoc_key = null;
            ldt_time = masofonAlias.masofon.Instance.set_machine_time();

            ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "material_number");

            ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row_no, "supplier_number");

            ll_distributor = dw_inv_pack_details.GetItemValue<long>(al_row_no, "distributor_number");

            ls_adhoc_key = dw_inv_pack_details.GetItemValue<string>(al_row_no, "adhoc_key");

            ll_decline_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "declines");

            ll_return_son = dw_inv_pack_details.GetItemValue<long>(al_row_no, "doc_no");
            //ll_invoice = dw_inv_pack_details.GetItem<int>( al_row_no, "serial_number")
            await uf_set_material_prices(ll_material_number, al_row_no, new EventArgs());

            ldc_price = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "material_price");

            ldc_sell_price = dw_inv_pack_details.GetItemValue<decimal>(al_row_no, "sell_price");
            // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - get row serial number

            ll_row_serial_number = dw_inv_pack_details.GetItemValue<long>(al_row_no, "row_serial_number");
            uf_set_total(ll_supplier, ref ldc_total);
            // AlexKh - 1.1.29.7.4 - 2015-10-25 - SPUCM00005245 - in case carton barcode empty, stop the update
            if (isempty_stringClass.isempty_string(this.ViewModel.is_carton_barcode))
            {
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "עדכון נכשל, מספר קרטון ריק");
                return -1;
            }
            await f_begin_tranClass.f_begin_tran();

            switch (al_case)
            {
                case 1:
                    //scenario 1: 
                    //existing barcode, All Update
                    this.ViewModel.UpdateDataS11(ldt_time, ldc_total, ll_return_son, ll_supplier);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.29.7.6 - 2015-11-10 - SPUCM00005270 - check serial number not empty
                    if (isempty_stringClass.isempty_string(ll_row_serial_number.ToString()))
                    {
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "עדכון נכשל, מספר סריאלי של פריט ריק");
                        goto error;
                    }
                    // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - update invoice_details also by row_serial_number
                    this.ViewModel.UpdateDataS12(ldt_time, adc_qty, ldc_price, ll_decline_number, ll_material_number, ll_return_son, ll_supplier, ll_row_serial_number);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - update by marlog_number
                    this.ViewModel.UpdateDataS13(adc_qty, ldt_time, ll_material_number, ll_return_son, this.ViewModel.is_carton_barcode, this.ViewModel.il_return_number, ll_supplier, this.ViewModel.il_marlog_branch);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    break;
                case 2:
                    //scenario 2: 
                    //existing supplier, Update invoice_move, Insert invoice_details, Insert shipment_marlog_return
                    this.ViewModel.UpdateDataS21(ldt_time, ldc_total, ll_return_son, ll_supplier);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
                    // AlexKh - 1.1.23.0 - 2014-09-04 - SPUCM00004984 - use valid numerator for invoice_details table
                    //ll_serial = f_calculate_datatable_row(143, 0, SQLCA)
                    ll_serial = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(masofonAlias.masofon.Instance.gs_vars.branch_number, 4);
                    this.ViewModel.UpdateDataS22(ll_serial, ll_return_son, ll_material_number, adc_qty, ldc_price, ll_supplier, ll_return_serial, ll_decline_number, ldc_sell_price, al_row_no + 1);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.29.7.6 - 2015-11-10 - SPUCM00005270 - fill serial number on the preview datawindow

                    li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "row_serial_number", ll_serial.ToString())); //RonY@26/03/2015  - SPUCM00005270
                                                                                                                                // AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
                    ll_return_serial = await f_calculate_datatable_rowClass.f_calculate_datatable_row(146, 0, masofonAlias.masofon.Instance.sqlca);
                    // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
                    //marlog number
                    this.ViewModel.UpdateDataS23(ll_return_serial, this.ViewModel.il_return_number, ldt_time, ll_supplier, ll_return_son, this.ViewModel.is_carton_barcode, ll_serial, ll_material_number, adc_qty, ls_adhoc_key, this.ViewModel.il_marlog_branch);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }

                    if (!await f_update_adhocClass.f_update_adhoc(0, dw_inv_pack_details.GetItemValue<long>(al_row_no, "serial_number"), this.ViewModel.ids_adhoc_update_listProperty))
                    {
                        goto error;
                    }
                    break;
                case 3:
                    //scenario 3: 
                    //non existing, All Insert
                    this.ViewModel.UpdateDataS31(this.ViewModel.il_return_son, ll_supplier, ldt_time, ldc_total, ll_distributor);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.1.25.0 - 2014-21-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
                    // AlexKh - 1.1.23.0 - 2014-09-04 - SPUCM00004984 - use valid numerator for invoice_details table
                    //ll_serial = f_calculate_datatable_row(143, 0, SQLCA)
                    ll_serial = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(masofonAlias.masofon.Instance.gs_vars.branch_number, 4);
                    this.ViewModel.UpdateDataS32(ll_serial, this.ViewModel.il_return_son, ll_material_number, adc_qty, ldc_price, ll_supplier, ll_return_serial, ll_decline_number, ldc_sell_price, al_row_no + 1);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }

                    li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "doc_no", this.ViewModel.il_return_sonProperty.ToString())); //RonY@26/03/2015 					li_rc = dw_inv_pack_details.SetItem(al_row_no, "doc_no", this.ViewModel.il_return_sonProperty.ToString()); //RonY@26/03/2015 					li_rc = dw_inv_pack_details.SetItem(al_row_no, "doc_no", this.ViewModel.il_return_sonProperty.ToString()); //RonY@26/03/2015  - SPUCM00005270

                    li_rc = Convert.ToInt32(dw_inv_pack_details.SetItem(al_row_no, "row_serial_number", ll_serial.ToString())); //RonY@26/03/2015  - SPUCM00005270
                                                                                                                                // AlexKh - 1.1.25.0 - 2014-12-18 - SPUCM00005164 - save serial number from invoice_details to connect the records
                                                                                                                                // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
                    ll_return_serial = await f_calculate_datatable_rowClass.f_calculate_datatable_row(146, 0, masofonAlias.masofon.Instance.sqlca);
                    //marlog number
                    this.ViewModel.UpdateDataS33(ll_return_serial, this.ViewModel.il_return_number, ldt_time, ll_supplier, this.ViewModel.il_return_son, this.ViewModel.is_carton_barcode, ll_serial, ll_material_number, adc_qty, ls_adhoc_key, this.ViewModel.il_marlog_branch);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }

                    if (!await f_update_adhocClass.f_update_adhoc(0, dw_inv_pack_details.GetItemValue<long>(al_row_no, "serial_number"), this.ViewModel.ids_adhoc_update_listProperty))
                    {
                        goto error;
                    }
                    break;
                case 4:
                    //deleted row
                    if (adc_qty <= 0)
                    {
                        ViewModel.UpdateData4(ldt_time, ll_supplier, ll_return_son);

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                        {
                            goto error;
                        }
                    }
                    // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - update invoice_details also by row_serial_number
                    this.ViewModel.UpdateDataS42(ldt_time, ll_material_number, ll_return_son, ll_row_serial_number);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - insert also marlog_number
                    this.ViewModel.UpdateDataS43(ldt_time, ll_material_number, this.ViewModel.il_return_number, this.ViewModel.is_carton_barcode, ll_supplier, ll_return_son, this.ViewModel.il_marlog_branch);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {
                        goto error;
                    }
                    break;
                default:
                    break;
            }

            await f_commitClass.f_commit();
            dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.UpdateByKeys); //RonY@26/03/2015  - SPUCM00005270
            dw_inv_pack_details.SetItemStatus((int)al_row_no, 0, ModelBuffer.Primary, ModelAction.None); //RonY@26/03/2015  - SPUCM00005270
            return 1;
            error:
            await f_rollbackClass.f_rollback();
            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "עדכון נתוני פריט נכשל");
            return -1;
        }


        public async Task cb_delete_item_clicked(object sender, EventArgs e)
        {
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            ButtonElement cb_delete = sender as ButtonElement;

            long ll_row = 0;
            long? ll_serial = 0;
            string ls_doc_state = null;
            var count = dw_inv_pack_details.Rows.Count;
         
            if (cb_delete == null || cb_delete.Tag == null)
            {
                ll_row = dw_inv_pack_details.SelectedRowIndex;
            }
            else
            {
                //GalilCS - Restore row index from  tag
                ll_row = Convert.ToInt32(cb_delete.Tag);
            }

            if (ll_row >= count || ll_row < 0)
            {
                ll_row = count - 1;
                //return;
            }
                      if (ll_row >= 0)
            {
                // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row

                ls_doc_state = dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state");
                if (isempty_stringClass.isempty_string(ls_doc_state))
                {
                    ls_doc_state = mini_terminal.uo_mini_terminal_returns_details.RETURN_STATE_OPEN.ToString();
                }
                //IF dw_inv_pack_details.GetItem<string>(ll_row, "doc_state") <> RETURN_STATE_OPEN OR IsEmpty_string(dw_inv_pack_details.GetItem<string>(ll_row, "doc_state")) THEN 

                if (dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state") != null && dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state") != mini_terminal.uo_mini_terminal_returns_details.RETURN_STATE_OPEN.ToString())
                {
                    //dw_inv_pack_details.TRIGGER EVENT ue_new_row()
                    return;
                }
            }
            else
            {
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא נבחרה שורה למחיקה");
                //dw_inv_pack_details.TRIGGER EVENT ue_new_row()
                return;
            }

            ll_serial = dw_inv_pack_details.GetItemValue<long?>(ll_row, "serial_number");
            if (ll_serial != null && ll_serial != 0)
            {
                //dw_inv_pack_details.setitem( ll_row, "material_quantity", 0)
                await uf_delete_item(ll_row);
            }


            var res_supplier_number = dw_inv_pack_details.GetItemValue<long?>(ll_row, "supplier_number");
            if (res_supplier_number == null)
            {
                res_supplier_number = 0;
            }
            if (this.ViewModel.ids_adhoc_update_listProperty.Retrieve(dw_inv_pack_details.GetItemValue<string>(ll_row, "adhoc_key"), masofonAlias.masofon.Instance.gs_vars.branch_number, res_supplier_number) > 0)
            {

                this.ViewModel.ids_adhoc_update_listProperty.SetItem(0, "is_active", "1");
                await f_update_adhocClass.f_update_adhoc(1, ll_serial.Value, this.ViewModel.ids_adhoc_update_listProperty);
            }

            dw_inv_pack_details.Delete((int)ll_row);

            // AlexKh - 1.48.0 - 2018-05-16 - SPUCM00006207 - in case no empty rows exit, add new row
            if (!await this.ViewModel.uf_is_new_row_exist(dw_inv_pack_details))
            {
                await this.ViewModel.uf_new_row(dw_inv_pack_details);
            }

            //IF NOT uf_is_new_row_exist() THEN
            //			dw_inv_pack_details.TRIGGER EVENT ue_new_row()	
            //END IF
            ll_row = this.ViewModel.UpdateComboGridAndDeclines(dw_inv_pack_details, (int)ll_row);

        }

        public async Task<int> dw_inv_pack_details_itemchanged(object sender, GridCellEventArgs e) //********************************************************************
        {
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            string dwoID = e.DataIndex;
            string data = e.Value;
            int row = e.RowIndex;
            if (row == -1)
                return 0;
            var newValue = Convert.ToString(dw_inv_pack_details.GetItemValue<object>(row, dwoID));
            if (newValue == data)
            {
                return 0;
            }

            if (dwoID == "declines")
            {
                //condition 1 
                if (string.IsNullOrEmpty(data))
                    return 0;
                if (string.IsNullOrEmpty(newValue) || Convert.ToInt64(newValue) == this.ViewModel.GetDeclinesNumber(this.ViewModel.GetDeclines(), data))
                    return 0;
            }

            GridElement elem = sender as GridElement;
            elem.Tag = "FiringWidgetControlItemChanged";
            try
            {

                //	*Object:	            dw_inv_pack_details
                //	*Event Name:	   (itemchanged)
                //	*Purpose: 	
                //	*Arguments: 	      long row, ControlElement dwo, string data
                //	*Return:	            long
                //	*Date		    Programmer    Version	  Task#	    Description
                //	*---------------------------------------------------------------------
                //	*21/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
                //********************************************************************





                //dw_inv_pack_details.itemchanged(0, default(ControlElement), default(string));
                string ls_column = null;
                string ls_barcode = null;
                string ls_material_name = null;
                string ls_null = null;
                string ls_dist1 = null;
                string ls_dist2 = null;
                string ls_supplier = null;
                long ll_supplier = 0;
                long ll_quantity = 0;
                long ll_row = 0;
                long ll_temp = 0;
                long ll_return_son = 0;
                long ll_row_exists2 = 0;
                long ll_material_number = 0;
                long ll_distributor = 0;
                long ll_return_allowed = 0;
                long ll_msg = 0;
                long ll_row_exists = 0;
                long ll_declines = 0;
                long ll_row_exists3 = 0;
                int li_len = 0;
                int li_index = 0;
                DateTime? ldt_time = null;
                DateTime? ldt_last_return_date = null;
                s_array_arguments lstr_parms = new s_array_arguments();
                ls_column = e.DataIndex;
                if (row < 0)
                {
                    return 2;
                }
                if (this.ViewModel.ib_marlog_returnProperty)
                {
                    if (this.ViewModel.il_distributorProperty == 0)
                    {
                        return 2;
                    }
                }

                switch (ls_column)
                {
                    case "barcode":
                        ldt_time = masofonAlias.masofon.Instance.set_machine_time();
                        ls_barcode = data;
                        // Get full barcode if it was particulaly inserted
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        // If barcode does not exist, drop leading zeros
                        if (string.IsNullOrEmpty(ls_barcode))
                        {
                            li_len = data.Length;
                            for (li_index = 1; li_index <= li_len; li_index++)
                            {
                                if (data.Substring(0, 1) == "0")
                                {
                                    data = data.Substring(data.Length - (data.Length - 1));

                                    li_index++;
                                }
                                else
                                {
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }
                            ls_barcode = data;
                            ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                        }
                        // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - update barcode using POST
                        // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - update barcode using POST

                        dw_inv_pack_details.SetItem(row, "barcode", ls_barcode);
                        // AlexKh - 1.1.40.0 - 2015-12-13 - add last_return_date
                        //get supplier number for barcode from material and set	
                        if (true)
                        {
                            ViewModel.LoadData6(ls_barcode, ref ll_supplier, ref ls_material_name, ref ll_material_number, ref ll_distributor, ref ll_return_allowed, ref ldt_last_return_date);
                        }
                        if (this.ViewModel.ib_marlog_returnProperty)
                        {
                            ViewModel.LoadData7(ll_distributor, ll_supplier, ref ll_row_exists);
                            //Eitan; SPUCM00005629; Ver 39.4; 28/3/2016;
                            if (ll_distributor != this.ViewModel.il_distributorProperty)
                            {
                                ls_dist1 = this.ViewModel.il_distributorProperty.ToString();
                                ls_dist2 = ll_distributor.ToString();
                                if (true)
                                {
                                    ViewModel.LoadData8(ls_dist1, ls_dist2, ref ll_row_exists2);
                                }
                            }
                            if (((ll_distributor != this.ViewModel.il_distributorProperty) && ll_row_exists2 != 1) || (ll_row_exists != 1)) //Eitan; SPUCM00005629; Ver 29.7.8; 28/3/2016;
                            {
                                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך למפיץ שנבחר");

                                dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), "");
                                return 2;
                            }
                            //Eitan; SPUCM00005629; Ver 39.7 ; 11/9/2016;
                            ls_supplier = ll_supplier.ToString();
                            if (true)
                            {
                                ViewModel.LoadData9(ls_supplier, ref ll_row_exists3);
                            }
                            if ((ll_distributor != this.ViewModel.il_distributorProperty) && (ll_row_exists3 != 1)) //Eitan; SPUCM00005629; Ver 39.8 ; 14/9/2016;
                            {
                                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך למפיץ שנבחר");

                                dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
                                return 2;
                            }
                            //
                        }
                        else
                        {
                            if (ll_supplier != this.ViewModel.il_supplierProperty)
                            {
                                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך לספק שנבחר");

                                dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
                                return 2;
                            }
                        }
                        // AlexKh - 1.1.1.11 - 21/08/2013 - CR#1160 - add parameter to disable adhoc validation 		
                        string temp = await f_get_parameter_valueClass.f_get_parameter_value("check_return_allowed", "boolean", "false", "Check return adhoc needed.", "gds_find");
                        if (temp.ToLower(CultureInfo.InvariantCulture) == "true")
                        {
                            // AlexKh - 1.1.40.0 - 2015-12-13 - check last_return_date is not passed
                            if ((ll_return_allowed == 0 || ll_return_allowed != 1) || (ll_return_allowed == 1 && (!(ldt_last_return_date == default(DateTime)) && masofonAlias.masofon.Instance.set_machine_time() > ldt_last_return_date)))
                            {
                                //IF IsNull(ll_return_allowed) OR ll_return_allowed <> 1 THEN 
                                ll_msg = await this.ViewModel.uf_show_message("התראה", "", "yesno", "פריט זה אינו מורשה להחזרה. האם תרצה לנצל אדהוק");
                                if (ll_msg == 1) // yes
                                {
                                    lstr_parms.a_long[1] = masofonAlias.masofon.Instance.gs_vars.branch_number;
                                    lstr_parms.a_long[2] = ll_supplier;
                                    lstr_parms.a_long[3] = this.ViewModel.il_return_number;
                                    lstr_parms.a_long[4] = ll_material_number; //item_number
                                    lstr_parms.a_long[6] = 0; //serial_number
                                    lstr_parms.a_long[5] = 0; // "adhoc_serial_number")
                                    lstr_parms.a_string[1] = ""; // "adhoc_key")
                                    lstr_parms.a_datastore[1] = this.ViewModel.ids_adhoc_update_listProperty;
                                    var view = await WindowHelper.Open<rw_addhock>("global_global", "args", lstr_parms);


                                    lstr_parms = view.Args;
                                    if (lstr_parms.a_boolean[0]) //succeed to update the adhoc status
                                    {
                                        if (row >= 0) //update 
                                        {

                                            dw_inv_pack_details.SetItem(row, "adhoc_serial_number", lstr_parms.a_long[1].ToString());

                                            dw_inv_pack_details.SetItem(row, "adhoc_key", Convert.ToString(lstr_parms.a_string[1]));
                                        }
                                        this.ViewModel.ids_adhoc_update_listProperty = lstr_parms.a_datastore[1] as IRepository;
                                    }
                                    else
                                    {
                                        //failed to update the adhoc status

                                        dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
                                        e.Value = null;
                                        return 2;
                                    }
                                }
                                else
                                {

                                    dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
                                    return 2; // - not allowed
                                }
                            }
                        }

                        dw_inv_pack_details.SetItem(row, "serial_number", this.ViewModel.il_return_number);

                        dw_inv_pack_details.SetItem(row, "carton_barcode", Convert.ToString(this.ViewModel.is_carton_barcode));

                        dw_inv_pack_details.SetItem(row, "supplier_number", ll_supplier);
                        //Eitan; SPUCM00005629; Ver 29.7.10; 28/3/2016;
                        //dw_inv_pack_details.SetItem(row, "distributor_number", ll_distributor)

                        dw_inv_pack_details.SetItem(row, "distributor_number", this.ViewModel.il_distributorProperty);

                        dw_inv_pack_details.SetItem(row, "material_number", ll_material_number);

                        dw_inv_pack_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));

                        dw_inv_pack_details.SetItem(row, "date_move", ldt_time);
                        break;
                    case "material_quantity":

                        ls_barcode = dw_inv_pack_details.GetItemValue<string>(row, "barcode");
                        if (isempty_stringClass.isempty_string(ls_barcode))
                        {
                            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "נא לסרוק ברקוד קודם");
                            return 2;
                        }

                        dw_inv_pack_details.SetItem(row, "material_quantity", (Convert.ToDecimal(data)));
                        break;
                    case "declines":

                        ls_barcode = dw_inv_pack_details.GetItemValue<string>(row, "barcode");

                        ll_quantity = Convert.ToInt64(dw_inv_pack_details.GetItemValue<decimal?>(row, "material_quantity"));
                        if (isempty_stringClass.isempty_string(ls_barcode) || ll_quantity <= 0)
                        {
                            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "חסרים נתוני ברקוד / כמויות");
                            return 2;
                        }
                        //var declineNumber = this.ViewModel.GetDeclinesNumber(this.ViewModel.GetDeclines(), data);
                        //dw_inv_pack_details.SetItem(row, "declines", Convert.ToInt64(declineNumber));
                        break;
                    default:
                        break;
                }
            }
            finally
            {
                if (row < dw_inv_pack_details.RowCount())
                {
                    //GalilCS   Update value in repository
                    if (e.DataIndex == "declines")
                    {
                        var declineNumber = this.ViewModel.GetDeclinesNumber(this.ViewModel.GetDeclines(), data);
                        dw_inv_pack_details.SetItem(row, e.DataIndex, declineNumber);
                        //var JsonData = GridMultiComboBoxElement.GetComboBoxData(this.ViewModel.GetDeclinesName(this.ViewModel.GetDeclines(), dw_inv_pack_details.Rows[row].Cells["declines"].Value), ((System.Data.DataTable)this.ViewModel.GetDeclines().DataSource));
                        dw_inv_pack_details.Rows[row].Cells["declines"].Value = e.Value;
                    }

                    //Save 
                    var ls_doc_state = dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(row), "doc_state");
                    if (!string.IsNullOrEmpty(ls_doc_state))
                    {
                        if (!ls_doc_state.Equals("n"))
                        {
                            goto exit;
                        }
                    }

                    var ldw_row_status = dw_inv_pack_details.GetItemStatus(row, 0, ModelBuffer.Primary);


                    if (ldw_row_status == ModelAction.UpdateByKeys || ldw_row_status == ModelAction.Insert)
                    {
                        var ls_barcode = dw_inv_pack_details.GetItemValue<string>(Convert.ToInt64(row), "barcode");

                        var ldc_qty = dw_inv_pack_details.GetItemValue<decimal?>(Convert.ToInt64(row), "material_quantity");

                        var ll_decline_reason = dw_inv_pack_details.GetItemValue<long>(Convert.ToInt64(row), "declines");
                        //The user tries to leave last empty row, it's can be done

                        if (Convert.ToInt32(row) == dw_inv_pack_details.RowCount() && (isempty_stringClass.isempty_string(ls_barcode) && ((ldc_qty == 0 || ldc_qty == null) && (ll_decline_reason == 0 || ll_decline_reason <= 0))))
                        {
                            goto exit;
                        }
                        if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
                        {

                            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e2) =>
                            {
                                dw_inv_pack_details.SetFocus(row, "barcode");
                            });
                            goto exit;
                        }
                        if (ldc_qty == 0 || ldc_qty == null)
                        {

                            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e2) =>
                            {
                                dw_inv_pack_details.SetFocus(row, "material_quantity");
                            });
                            goto exit;
                        }
                        if (ll_decline_reason <= 0)
                        {
                            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e2) =>
                            {
                                dw_inv_pack_details.SetFocus(row, "declines");
                            });

                            goto exit;
                        }
                        await uf_update_item(Convert.ToInt64(row));
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e2) => { dw_inv_pack_details.SetFocus(dw_inv_pack_details.Rows.Count - 1, "barcode"); },delay:50);

                    }
                    exit:;


                    //GalilCS - if cellkeydown fired from itemchange , cancel it and then fire.
                    Tuple<string, GridElementCellEventArgs> elemTag = elem.Tag as Tuple<string, GridElementCellEventArgs>;
                    if (elemTag != null && elemTag.Item1 == "FireKeyDownAfterWidgetControlItemChanged")
                    {
                        elem.PerformCellkeyDown(elemTag.Item2);
                    }

                    elem.Tag = null;
                }
            }
            return 0;
        }

        private async Task dw_inv_pack_details_CellkeyDown(object sender, GridElementCellEventArgs e)
        {
            GridElement elem = sender as GridElement;

            if (Convert.ToString(elem.Tag) == "FiringWidgetControlItemChanged")
            {
                elem.Tag = Tuple.Create<string, GridElementCellEventArgs>("FireKeyDownAfterWidgetControlItemChanged", e);
                return;
            }

            u_dw_update dw_inv_pack_details = this.GetVisualElementById<u_dw_update>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            await dw_inv_pack_details_ue_enter(dw_inv_pack_details, e);
        }

        public async Task<int> dw_inv_pack_details_ue_new_row()
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
            return 0;
            //u_dw_update dw_inv_pack_details = this.GetVisualElementById<u_dw_update>("uo_mini_terminal_returns_details_dw_inv_pack_details");

            
            //uf_calculate_declines
            //return await this.ViewModel.uf_new_row(dw_inv_pack_details);
        }



        public async Task<int> dw_inv_pack_details_rowfocuschanging(object sender, GridElementSelectionChangeEventArgs e)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            ButtonElement cb_delete_item = this.GetVisualElementById<ButtonElement>("cb_delete_item");


            //dw_inv_pack_details.rowfocuschanging(0, 0);
         //   bool lb_update = false;
            long ll_rtn_code = 0;
            long ll_decline_reason = 0;
            decimal ldc_qty = default(decimal);
            string ls_barcode = null;
            string ls_doc_state = null;
            ModelAction ldw_row_status = default(ModelAction);

            object currentrow = e.SelectedRows?[0];
            //GalilCS - Update currentrow
            cb_delete_item.Tag = currentrow;
       return 0;
        }

        public void cb_print_clicked(object sender, EventArgs e) //Long		invoice_number, &
        {
          
            return;
        }

        public async Task<int> cb_exit_clicked(object sender, EventArgs e) //********************************************************************
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            //	*Object:	            cb_exit
            //	*Event Name:	   (clicked)
            //	*Purpose: 	
            //	*Arguments: 	      (none)
            //	*Return:	            long
            //	*Date		    Programmer    Version	  Task#	    Description
            //	*---------------------------------------------------------------------
            //	*22/08/2013	 AlexKh        1.1.1.11	  CR#1160 	 Initial version
            //********************************************************************

            long ll_decline_reason = 0;
            long ll_row = 0;
            decimal? ldc_qty = null;
            string ls_barcode = null;
            ModelAction ldw_row_status = default(ModelAction);

            ll_row = dw_inv_pack_details.RowCount() - 1;
            if (ll_row < 0)
            {
                return 0;
            }


            ldw_row_status = dw_inv_pack_details.GetItemStatus((int)ll_row, 0, ModelBuffer.Primary);


            if (ldw_row_status == ModelAction.UpdateByKeys || ldw_row_status == ModelAction.Insert)
            {

                ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_row, "barcode");

                ldc_qty = dw_inv_pack_details.GetItemValue<decimal?>(ll_row, "material_quantity");
                if (ldc_qty == null)
                {
                    ldc_qty = 0;
                }


                ll_decline_reason = dw_inv_pack_details.GetItemValue<long>(ll_row, "declines");
                if (!isempty_stringClass.isempty_string(ls_barcode) && (ldc_qty > 0 && (ldc_qty > 0 && ll_decline_reason > 0)))

                {
                    await uf_update_item(ll_row);
                }
            }
            await this.ViewModel.uf_display("RETURN_MARLOG");
            // AlexKh - 1.1.1.12 - 09/09/2013 - SPUCM00004291 - reset the window parameters
            this.ViewModel.il_return_number = 0;
            this.ViewModel.is_carton_barcode = "";
            this.ViewModel.il_return_sonProperty = 0; //RonY@26/03/2015  - SPUCM00005270
            return 0;
        }

        public async Task dw_details_itemchanged(int row, VisualElement dwo, string data)
        {
            GridElement dw_details = this.GetVisualElementById<GridElement>("dw_details");
            string ls_column = null;
            string ls_barcode = null;
            string ls_material_name = null;
            long ll_supplier = 0;
            long ll_quantity = 0;
            int li_len = 0;
            int li_index = 0;

            ls_column = Convert.ToString(dwo.ID);
            switch (ls_column)
            {
                case "barcode":
                    ls_barcode = data;
                    // Get full barcode if it was particulaly inserted
                    ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    // If barcode does not exist, drop leading zeros
                    if (string.IsNullOrEmpty(ls_barcode))
                    {
                        li_len = data.Length;
                        for (li_index = 1; li_index <= li_len; li_index++)
                        {
                            if (data.Substring(0, 1) == "0")
                            {
                                data = data.Substring(data.Length - (data.Length - 1));

                                li_index++;
                            }
                            else
                            {
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        ls_barcode = data;
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    }

                    dw_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
                    //get supplier number for barcode from material and set
                    if (true)
                    {
                        ViewModel.LoadData10(ls_barcode, ref ll_supplier, ref ls_material_name);
                    }

                    dw_details.SetItem(row, "supplier_number", ll_supplier.ToString());

                    dw_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
                    break;
                case "material_quantity":
                    ll_quantity = Convert.ToInt64(data);

                    dw_details.SetItem(row, "material_quantity", ll_quantity.ToString());
                    break;
                default:
                    break;
            }
         
        }
    }

    public class await
    {
    }
}
