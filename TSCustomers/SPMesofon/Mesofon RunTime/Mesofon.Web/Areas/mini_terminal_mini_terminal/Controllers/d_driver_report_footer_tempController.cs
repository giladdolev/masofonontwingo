using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_driver_report_footer_tempController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_driver_report_footer_temp()
		{
			d_driver_report_footer_tempRepository repository = new d_driver_report_footer_tempRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
