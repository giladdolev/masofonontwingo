using System.Web.Mvc;
using Mesofon.Repository;
using Common.Transposition.Extensions;

namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_headerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_header()
		{
			d_mini_terminal_headerRepository repository = new d_mini_terminal_headerRepository();
            repository.SetDataRows(new System.Collections.Generic.List<ModelBase> { new Models.d_mini_terminal_header() });

            return this.View(repository);
		}
	}
}
