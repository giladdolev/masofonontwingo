using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_return_details2Controller : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_return_details2()
		{
			dwh_return_details2Repository repository = new dwh_return_details2Repository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
