using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Extensions;
using System.Drawing;
using System.Web.VisualTree.Extensions;
using mini_terminal;
using Mesofon.Common;
using System.Web.VisualTree;

namespace Mesofon.MiniTerminal.Controllers
{
	public class rw_palletController : MvcSite.Common.SPBaseController
	{

		public ActionResult rw_pallet()
		{
			return this.View(new rw_pallet());
		}
		private rw_pallet ViewModel
		{
			get { return this.GetRootVisualElement() as rw_pallet; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            TextBoxElement sle_pallet_number = this.GetVisualElementById<TextBoxElement>("sle_pallet_number");
            sle_pallet_number.Focus();
        }
        public static bool enterKeyFlag { get; private set; }

        public void sle_pallet_number_enterdown(object sender, KeyDownEventArgs e)
		{
            if(((KeyEventArgs)e).KeyCode == Keys.Enter || ((KeyEventArgs)e).KeyCode == Keys.Tab)
            {
                enterKeyFlag = true;
                sle_pallet_number_TextChanged(sender, e);
            }
		}

        public void sle_pallet_number_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement sle_pallet_number = this.GetVisualElementById<TextBoxElement>("sle_pallet_number");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_pallet_number.Text != "")
                {
                    wf_handle_pallet();
                    sle_pallet_number.Text = "";
                    enterKeyFlag = false;
                }
            }
        }

        public void wf_handle_pallet() //*******************************************************************
		{
			TextBoxElement sle_pallet_number = this.GetVisualElementById<TextBoxElement>("sle_pallet_number");
			//*Object:				rw_pallet
			//*Function Name:	wf_handle_pallet
			//*Purpose: 			handle pallet number
			//*Arguments: 		None
			//*Return:				None
			//*Date				Programer		Version	Task#	 		Description
			//*-------------------------------------------------------------------
			//*31-05-2010		AlexKh			1.2.46.0	CR#1138		Initial version
			//*******************************************************************
			
			if (!isempty_stringClass.isempty_string(sle_pallet_number.Text))
			{
				WindowHelper.Close<string>(this.ViewModel, Convert.ToString(sle_pallet_number.Text));
			}
			else
			{
				WindowHelper.Close<string>(this.ViewModel, "0");
			}
		}
		 
		public void open() // AlexKh - 1.2.45.6 - 2010-05-31 - CR#1138 - open window
		{
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			wf_handle_pallet();
		}
	}
}
