using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using global;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
	public class rw_pallet_detailsController : MvcSite.Common.SPBaseController
	{

		public ActionResult rw_pallet_details()
		{
			return this.View(new rw_pallet_details());
		}
		private rw_pallet_details ViewModel
		{
			get { return this.GetRootVisualElement() as rw_pallet_details; }
		}
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_report_choose_type
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 0;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 0;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			ll_ypos += (mini_terminal.rw_pallet_details.il_max_window_height - this.ViewModel.PixelHeight) / 2;
			ll_xpos += (mini_terminal.rw_pallet_details.il_max_window_width - this.ViewModel.PixelWidth) / 2;
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
		
		public async Task open() //Eitan; SPUCM00005359;
		{
			GridElement dw_details = this.GetVisualElementById<GridElement>("dw_details");
			long ll_branch = 0;
			long ll_row = 0;
			string ls_pallet = null;
			s_array_arguments lstr_arg = default(s_array_arguments);
			
			lstr_arg = (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
			ls_pallet = lstr_arg.a_string[1];
			ll_branch = lstr_arg.a_long[1];
			
			
			
			ll_row = dw_details.Retrieve(ls_pallet, ll_branch);
			if (ll_row <= 0)
			{
				await MessageBox.Show("אירעה שגיאה בשליפת הרשומות", "Error");
			}
		}
		
		public void cb_ok_clicked(object sender, EventArgs e) //Eitan; SPUCM00005359;
		{
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, "0");
		}
	}
}
