using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_return_number_by_branch_exController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_return_number_by_branch_ex()
		{
			d_return_number_by_branch_exRepository repository = new d_return_number_by_branch_exRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
