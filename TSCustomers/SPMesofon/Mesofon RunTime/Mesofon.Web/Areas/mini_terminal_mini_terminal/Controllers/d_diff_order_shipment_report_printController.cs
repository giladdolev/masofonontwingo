using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_diff_order_shipment_report_printController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_diff_order_shipment_report_print()
		{
			d_diff_order_shipment_report_printRepository repository = new d_diff_order_shipment_report_printRepository();
			 
			return this.View(repository);
		}
	}
}
