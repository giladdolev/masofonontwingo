using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_pallet_detailsController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_pallet_details()
		{
			d_pallet_detailsRepository repository = new d_pallet_detailsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
