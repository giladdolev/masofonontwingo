using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_b2b_reportController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_b2b_report()
		{
			d_mini_terminal_b2b_reportRepository repository = new d_mini_terminal_b2b_reportRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
