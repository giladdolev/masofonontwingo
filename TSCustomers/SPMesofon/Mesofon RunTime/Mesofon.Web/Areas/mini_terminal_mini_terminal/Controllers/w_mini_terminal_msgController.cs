using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using global;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Globalization;
using System.Drawing;
using System.Threading.Tasks;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class w_mini_terminal_msgController : MvcSite.Common.SPBaseController
    {


        public ActionResult w_mini_terminal_msg(s_array_arguments istr_arg)
        {
            return this.View(new w_mini_terminal_msg(istr_arg));
        }
        public w_mini_terminal_msg ViewModel
        {
            get { return this.GetRootVisualElement() as w_mini_terminal_msg; }
        }
        public void ue_key(object sender, GridElementCellEventArgs e) //--------------------------------------------------------------------
        {
            u_cb_cancel cb_cancel = this.GetVisualElementById<u_cb_cancel>("cb_cancel");
            u_cb_ok cb_ok = this.GetVisualElementById<u_cb_ok>("cb_ok");
            object keyflags = null;
            //Event:			public w_mini_terminal_msg.ue_key()
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            //					if enter clicked --> activate cb_ok click 
            //					if escape clicked --> activate cb_cancel click
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (Convert.ToInt32(keyflags) == 0)
            {

                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                {
                    cb_ok.clicked(null, null);
                }

                else if (e.KeyCode == Keys.Escape)
                {
                    cb_cancel.clicked(null, null);
                }
            }
        }
        protected void wf_get_window_position() //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_msg.wf_get_window_position()
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					set start window position
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_xpos = 0;
            long ll_ypos = 0;
            string ls_window_height = null;
            string ls_window_width = null;
            string ls_window_xpos = null;
            string ls_window_ypos = null;
            string ls_lang = null;
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
            {
                ls_lang = "alignment_left";
            }
            else
            {
                ls_lang = "alignment_right";
            }
            if (Convert.ToInt64(ls_window_xpos) > 0)
            {
                ll_xpos = Convert.ToInt64(ls_window_xpos);
            }
            else
            {
                ll_xpos = 65;
            }
            if (Convert.ToInt64(ls_window_ypos) > 0)
            {
                ll_ypos = Convert.ToInt64(ls_window_ypos);
            }
            else
            {
                ll_ypos = 300;
            }
            if (Convert.ToInt64(ls_window_width) > 0)
            {
                this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
            }
            if (Convert.ToInt64(ls_window_height) > 0)
            {
                this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
            }
            //ll_ypos += (il_max_window_height - this.height) /2
            //ll_xpos += (il_max_window_width - this.width) /2
            WindowElement root = this.GetRootVisualElement() as WindowElement;
            root.Location = new Point((int)ll_xpos, (int)ll_ypos);
            this.ViewModel.Visible = true;
        }
        protected void wf_show_error_title(bool ab_flag) //--------------------------------------------------------------------
        {
            TextBoxElement mle_error = this.GetVisualElementById<TextBoxElement>("mle_error");
            //Function:			public w_mini_terminal_msg.wf_show_error_title()
            //
            // Returns:         None
            //
            // Parameters:      value Boolean ab_flag --> If true move down , otherwise up
            // 
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					if More >> (st_more) clicked then move all window controls down,
            //					otherwise move up.
            //					if a control not need to be moved , its tag value must be 'NoMove'
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll = 0;
            long ll_y_move = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            mle_error.Visible = ab_flag;
            if (ab_flag)
            {
                ll_y_move = (mle_error.PixelHeight - 10);
            }
            else
            {
                ll_y_move = -1 * (mle_error.PixelHeight - 10);
            }
            for (ll = 1; ll <= CollectionExtensions.UBound(this.ViewModel.Controls); ll++)
            {
                if (!(this.ViewModel.Controls[(int)ll] != null))
                {
                    continue;
                }
                if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    sle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    if (Convert.ToString(sle.Tag) != "NoMove")
                    {
                        sle.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LabelElement))
                {
                    st = this.ViewModel.Controls[(int)ll] as LabelElement;
                    if (Convert.ToString(st.Tag) != "NoMove")
                    {
                        st.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    cb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    if (Convert.ToString(cb.Tag) != "NoMove")
                    {
                        cb.PixelTop += (int)ll_y_move;
                    }
                }

                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CompositeElement))
                {
                    uo = this.ViewModel.Controls[(int)ll] as CompositeElement;
                    if (Convert.ToString(uo.Tag) != "NoMove")
                    {
                        uo.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(GridElement))
                {
                    dw = this.ViewModel.Controls[(int)ll] as GridElement;
                    if (Convert.ToString(dw.Tag) != "NoMove")
                    {
                        dw.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    pb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    if (Convert.ToString(pb.Tag) != "NoMove")
                    {
                        pb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CheckBoxElement))
                {
                    cbx = this.ViewModel.Controls[(int)ll] as CheckBoxElement;
                    if (Convert.ToString(cbx.Tag) != "NoMove")
                    {
                        cbx.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RadioButtonElement))
                {
                    rb = this.ViewModel.Controls[(int)ll] as RadioButtonElement;
                    if (Convert.ToString(rb.Tag) != "NoMove")
                    {
                        rb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TabElement))
                {
                    t = this.ViewModel.Controls[(int)ll] as TabElement;
                    if (Convert.ToString(t.Tag) != "NoMove")
                    {
                        t.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RichTextBox))
                {
                    gb = this.ViewModel.Controls[(int)ll] as GroupBoxElement;
                    if (Convert.ToString(gb.Tag) != "NoMove")
                    {
                        gb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ListBoxElement))
                {
                    lb = this.ViewModel.Controls[(int)ll] as ListBoxElement;
                    if (Convert.ToString(lb.Tag) != "NoMove")
                    {
                        lb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LinkLabelElement))
                {
                    shl = this.ViewModel.Controls[(int)ll] as LinkLabelElement;
                    if (Convert.ToString(shl.Tag) != "NoMove")
                    {
                        shl.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = this.ViewModel.Controls[(int)ll] as ComboBoxElement;
                    if (Convert.ToString(ddlb.Tag) != "NoMove")
                    {
                        ddlb.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    mle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    if (Convert.ToString(mle.Tag) != "NoMove")
                    {
                        mle.PixelTop += (int)ll_y_move;
                    }
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = this.ViewModel.Controls[(int)ll] as MaskedTextBoxElement;
                    if (Convert.ToString(em.Tag) != "NoMove")
                    {
                        em.PixelTop += (int)ll_y_move;
                    }
                }
            }
            this.ViewModel.PixelHeight += (int)ll_y_move;
        }

        protected void wf_set_default_button(string as_default_button)
        {
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            long ll_default_button = 0;
            ll_default_button = Convert.ToInt64(as_default_button);
            if (ll_default_button > 2 || ll_default_button <= 0)
            {
                ll_default_button = 1;
            }
            if (cb_ok.Visible && cb_cancel.Visible)
            {
                if (ll_default_button == 1)
                {
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                    {
                        cb_ok.Focus();
                    }, delay: 120);
                }
                else
                {
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                    {
                        cb_cancel.Focus();
                    }, delay: 120);
                }
            }
            else if (cb_ok.Visible)
            {
                    Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                    {
                        cb_ok.Focus();
                    }, delay: 120);
            }
            else if (cb_cancel.Visible)
            {
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            cb_cancel.Focus();
                        }, delay: 120);
            }
        }
        protected async Task wf_show_buttons(string as_buttons)
        {
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            switch (as_buttons.ToLower(CultureInfo.InvariantCulture))
            {
                case "ok":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = false;
                    cb_ok.PixelLeft = (this.ViewModel.PixelWidth - cb_ok.PixelWidth) / 2;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אישור");
                    }
                    else
                    {
                        cb_ok.Text = "Ok";
                    }
                    break;
                case "cancel":
                    cb_ok.Visible = false;
                    cb_cancel.Visible = true;
                    cb_cancel.PixelLeft = 85;
                    cb_cancel.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
                    }
                    else
                    {
                        cb_cancel.Text = "Cancel";
                    }
                    break;
                case "okcancel":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = true;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    cb_cancel.Tag = -1;//.uf_set_return_value(-1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אישור");
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
                    }
                    else
                    {
                        cb_ok.Text = "Ok";
                        cb_cancel.Text = "Cancel";
                    }
                    break;
                case "yes":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = false;
                    cb_ok.PixelLeft = (this.ViewModel.PixelWidth - cb_ok.PixelWidth) / 2;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("כן");
                    }
                    else
                    {
                        cb_ok.Text = "Yes";
                    }
                    break;
                case "no":
                    cb_ok.Visible = false;
                    cb_cancel.Visible = true;
                    cb_cancel.PixelLeft = 85;
                    cb_cancel.Tag = 1;//.uf_set_return_value(1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא");
                    }
                    else
                    {
                        cb_cancel.Text = "No";
                    }
                    break;
                case "yesno":
                    cb_ok.Visible = true;
                    cb_cancel.Visible = true;
                    cb_ok.Tag = 1;//.uf_set_return_value(1);
                    cb_cancel.Tag = -1;//.uf_set_return_value(-1);
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        cb_ok.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("כן");
                        cb_cancel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא");
                    }
                    else
                    {
                        cb_ok.Text = "Yes";
                        cb_cancel.Text = "No";
                    }
                    break;
            }
        }
        public void wf_show_picture(string as_picture_name)
        {
            ImageElement p_icon = this.GetVisualElementById<ImageElement>("p_icon");
            if (isempty_stringClass.isempty_string(as_picture_name))
            {
                return;
            }
            switch (as_picture_name.ToLower(CultureInfo.InvariantCulture))
            {
                case "information!":
                case "stopsign!":
                case "exclamation!":
                case "question!":
                    p_icon.Image = new ResourceReference(as_picture_name);
                    p_icon.Visible = true;
                    break;
                default:
                    if (!System.IO.File.Exists(as_picture_name))
                    {
                        return;
                    }
                    p_icon.Image = new ResourceReference(as_picture_name);
                    p_icon.Visible = true;
                    break;
            }
        }
        public void wf_without_error_title() //--------------------------------------------------------------------
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            //Function:			public w_mini_terminal_msg.wf_without_error_title()
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll = 0;
            long ll_y_move = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            ll_y_move = -1 * st_more.PixelHeight;
            for (ll = 1; ll <= CollectionExtensions.UBound(this.ViewModel.Controls); ll++)
            {
                if (!(this.ViewModel.Controls[(int)ll] != null))
                {
                    continue;
                }
                if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    sle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    sle.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LabelElement))
                {
                    st = this.ViewModel.Controls[(int)ll] as LabelElement;
                    st.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    cb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    cb.PixelTop += (int)ll_y_move;
                }

                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CompositeElement))
                {
                    uo = this.ViewModel.Controls[(int)ll] as CompositeElement;
                    uo.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(GridElement))
                {
                    dw = this.ViewModel.Controls[(int)ll] as GridElement;
                    dw.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ButtonElement))
                {
                    pb = this.ViewModel.Controls[(int)ll] as ButtonElement;
                    pb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(CheckBoxElement))
                {
                    cbx = this.ViewModel.Controls[(int)ll] as CheckBoxElement;
                    cbx.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RadioButtonElement))
                {
                    rb = this.ViewModel.Controls[(int)ll] as RadioButtonElement;
                    rb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TabElement))
                {
                    t = this.ViewModel.Controls[(int)ll] as TabElement;
                    t.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(RichTextBox))
                {
                    gb = this.ViewModel.Controls[(int)ll] as GroupBoxElement;
                    gb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ListBoxElement))
                {
                    lb = this.ViewModel.Controls[(int)ll] as ListBoxElement;
                    lb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(LinkLabelElement))
                {
                    shl = this.ViewModel.Controls[(int)ll] as LinkLabelElement;
                    shl.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = this.ViewModel.Controls[(int)ll] as ComboBoxElement;
                    ddlb.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(TextBoxElement))
                {
                    mle = this.ViewModel.Controls[(int)ll] as TextBoxElement;
                    mle.PixelTop += (int)ll_y_move;
                }
                else if (this.ViewModel.Controls[(int)ll].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = this.ViewModel.Controls[(int)ll] as MaskedTextBoxElement;
                    em.PixelTop += (int)ll_y_move;
                }
            }
            st_more.Visible = false;
            this.ViewModel.PixelHeight += (int)ll_y_move;
        }
     
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public async Task open() //--------------------------------------------------------------------
        {
            TextBoxElement mle_error = this.GetVisualElementById<TextBoxElement>("mle_error");
            //Event:			public w_mini_terminal_msg.open()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 									ancestor of all message objects
            //							gets Message.PowerObjectParm from caller window
            //					a)	istr_arg.a_string[1] ---> window title
            //					b) istr_arg.a_string[2] ---> error text
            //					c) istr_arg.a_string[3] ---> message icon 
            //					d) istr_arg.a_string[4] ---> buttons to be displayed
            //					e) istr_arg.a_string[5] ---> sets default button focus
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_buttons_on_window = 0;

            if (this.ViewModel.istr_argProperty == null)
            {
                return;
            }

            if (masofonAlias.masofon.Instance.nvo_translator != null)
            {
                //.. multi_lingual_is_active = yes + current system language not Hebrew
                if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
                {
                    await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel);
                }
            }
            //.. window title
            if (CollectionExtensions.UBound(this.ViewModel.istr_argProperty.a_string) >= 1)
            {
                this.ViewModel.is_window_titleProperty = this.ViewModel.istr_argProperty.a_string[1];
                this.ViewModel.is_window_titleProperty = await this.ViewModel.wf_translate_message(this.ViewModel.is_window_titleProperty);
                if (!isempty_stringClass.isempty_string(this.ViewModel.is_window_titleProperty))
                {
                    this.ViewModel.Text = "";
                }
            }
            //.. error text
            if (CollectionExtensions.UBound(this.ViewModel.istr_argProperty.a_string) >= 2)
            {
                mle_error.Text = await this.ViewModel.wf_translate_message(this.ViewModel.istr_argProperty.a_string[2]);
                if (masofonAlias.masofon.Instance.nvo_translator != null)
                {
                    if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_string(mle_error.Text))
                    {

                        mle_error.TextAlign = HorizontalAlignment.Right;
                    }
                    else
                    {

                        mle_error.TextAlign = HorizontalAlignment.Left;
                    }
                }
                mle_error.Text = this.ViewModel.istr_argProperty.a_string[2];
            }
            //.. message icon 
            if (CollectionExtensions.UBound(this.ViewModel.istr_argProperty.a_string) >= 3)
            {
                wf_show_picture(this.ViewModel.istr_argProperty.a_string[3]);
            }
            //.. buttons to be displayed
            if (CollectionExtensions.UBound(this.ViewModel.istr_argProperty.a_string) >= 4)
            {
                await wf_show_buttons(this.ViewModel.istr_argProperty.a_string[4]);
            }
            //.. sets default button focus
            if (CollectionExtensions.UBound(this.ViewModel.istr_argProperty.a_string) >= 5)
            {
                wf_set_default_button(this.ViewModel.istr_argProperty.a_string[5]);
            }
            if (isempty_stringClass.isempty_string(mle_error.Text))
            {
                wf_without_error_title();
            }
            //.. set start window position
            //this.ViewModel.Visible = false;
            //this.ViewModel.Hide();
            //wf_resize();
            //wf_get_window_position();


            TextBoxElement mle_message = this.GetVisualElementById<TextBoxElement>("mle_message");
            //Function:			public w_mini_terminal_message_box.open()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 					at the parent (w_mini_terminal_msg) open event , gets values :
            // 						a)	istr_arg.a_string[1] ---> window title
            //						b) istr_arg.a_string[2] ---> error text
            //						c) istr_arg.a_string[3] ---> message icon 
            //						d) istr_arg.a_string[4] ---> buttons to be displayed
            //						e) istr_arg.a_string[5] ---> sets default button focus
            //					at this event gets values
            //						f)	istr_arg.a_string[6] ---> message text
            //						
            //					if some message is like '#number' (for example '#125') then 
            //						message will retrieve from DB by its number
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------


            if (CollectionExtensions.UBound(this.ViewModel.istr_arg.a_string) >= 6)
            {
                mle_message.Text = await this.ViewModel.wf_translate_message(Convert.ToString(this.ViewModel.istr_arg.a_string[6]));
                //	IF nvo_translator.fnv_is_hebrew_string(mle_message.text) THEN
                //		mle_message.alignment = Right!
                //	ELSE
                //		mle_message.alignment = Left!
                //	END IF
            }

        }
        public void activate() //--------------------------------------------------------------------
        {
            //Event:			public w_mini_terminal_msg.open()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            // 									ancestor of all message objects
            //							gets Message.PowerObjectParm from caller window
            //					a)	istr_arg.a_string[1] ---> window title
            //					b) istr_arg.a_string[2] ---> error text
            //					c) istr_arg.a_string[3] ---> message icon 
            //					d) istr_arg.a_string[4] ---> buttons to be displayed
            //					e) istr_arg.a_string[5] ---> sets default button focus
            //					
            // if some message is like '#number' (for example '#125') then 
            //	 message will retrieve from DB by its number
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

        }

        public void cb_ok_clicked(object sender, EventArgs e)
        {
            WindowHelper.Close(this.ViewModel, this.ViewModel.il_return_valueProperty.ToString());
        }
        public void cb_cancel_clicked(object sender, EventArgs e)
        {
            this.ViewModel.il_return_valueProperty = -1;
            WindowHelper.Close(this.ViewModel, this.ViewModel.il_return_valueProperty.ToString());
        }

        public void st_more_clicked(object sender, EventArgs e) //--------------------------------------------------------------------
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            //Event:			public w_mini_terminal_msg.clicked()
            //
            // Returns:      Long
            //
            // Copyright  - Stas
            //
            // Date Created: 02/08/2005
            //
            // Description:	
            //				 if More >> (st_more) clicked then move all window controls down,
            //					otherwise move up.
            //				 if a control not need to be moved , its tag value must be 'NoMove'
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------
            if (st_more.Text == "More >>")
            {
                st_more.Text = "Less <<";
                wf_show_error_title(true);
            }
            else
            {
                st_more.Text = "More >>";
                wf_show_error_title(false);
            }
        }

        public void st_more_constructor(object sender, EventArgs e)
        {
            ButtonElement st_more = this.GetVisualElementById<ButtonElement>("st_more");
            st_more.BringToFront();
        }
    }
}
