using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_detail_item_packing_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_detail_item_packing_list()
		{
			d_mini_terminal_detail_item_packing_listRepository repository = new d_mini_terminal_detail_item_packing_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
