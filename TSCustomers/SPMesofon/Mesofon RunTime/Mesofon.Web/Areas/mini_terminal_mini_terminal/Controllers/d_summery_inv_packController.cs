using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_summery_inv_packController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_summery_inv_pack()
		{
			d_summery_inv_packRepository repository = new d_summery_inv_packRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
