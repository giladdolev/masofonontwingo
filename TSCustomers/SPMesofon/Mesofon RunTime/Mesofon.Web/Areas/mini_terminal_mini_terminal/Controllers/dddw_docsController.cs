using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dddw_docsController : MvcSite.Common.SPBaseController
	{
		public ActionResult dddw_docs()
		{
			dddw_docsRepository repository = new dddw_docsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
