using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_shipment_invoicesController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_shipment_invoices()
		{
			d_shipment_invoicesRepository repository = new d_shipment_invoicesRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
