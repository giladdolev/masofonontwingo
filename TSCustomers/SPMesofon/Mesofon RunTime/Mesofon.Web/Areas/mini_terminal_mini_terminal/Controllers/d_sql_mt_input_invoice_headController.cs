using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mt_input_invoice_headController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mt_input_invoice_head()
		{
			d_sql_mt_input_invoice_headRepository repository = new d_sql_mt_input_invoice_headRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
