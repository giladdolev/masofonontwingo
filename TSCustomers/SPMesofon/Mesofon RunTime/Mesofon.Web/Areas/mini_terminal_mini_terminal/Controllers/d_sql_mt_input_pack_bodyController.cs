using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_sql_mt_input_pack_bodyController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_mt_input_pack_body()
		{
			d_sql_mt_input_pack_bodyRepository repository = new d_sql_mt_input_pack_bodyRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
