using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class dwh_return_details_per_cartonController : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_return_details_per_carton()
		{
			dwh_return_details_per_cartonRepository repository = new dwh_return_details_per_cartonRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
