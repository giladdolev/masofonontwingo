using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using System.Drawing;
using System.Extensions;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;

namespace Mesofon.MiniTerminal.Controllers
{
	public class rw_item_quantityController : MvcSite.Common.SPBaseController
	{

		public ActionResult rw_item_quantity()
		{
			return this.View(new rw_item_quantity());
		}
		private rw_item_quantity ViewModel
		{
			get { return this.GetRootVisualElement() as rw_item_quantity; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void enterdown()
		{
			wf_handle();
		}
		public void wf_handle() //*******************************************************************
		{
			TextBoxElement sle_quantity = this.GetVisualElementById<TextBoxElement>("sle_quantity");
			//*Object:				rw_print_copies_number
			//*Function Name:	wf_handle
			//*Purpose: 			handle copies number
			//*Arguments: 		None
			//*Return:				None
			//*Date				Programer		Version		Task#	 			Description
			//*-------------------------------------------------------------------
			//*07-11-2011		AlexKh			1.2.48.0.13	SPUCM00002930	Initial version
			//*******************************************************************
			
			
			if (!isempty_stringClass.isempty_string(sle_quantity.Text) && SystemFunctionsExtensions.IsNumber(sle_quantity.Text.ToString()))
			{
				
				WindowHelper.Close(this.ViewModel, Convert.ToDecimal(sle_quantity.Text));
			}
			else
			{
				
				WindowHelper.Close(this.ViewModel, Convert.ToDecimal(0));
			}
		}

		public void open()
		{
			TextBoxElement sle_quantity = this.GetVisualElementById<TextBoxElement>("sle_quantity");
			decimal ldec_copies = default(decimal);
			// AlexKh - 1.2.48.0.13 - 2011-11-07 - SPUCM00002930 - open window
		  		
			ldec_copies = (decimal)WindowHelper.GetParam<double>(this.ViewModel);;
			if (ldec_copies > 0)
			{
				sle_quantity.Text = ldec_copies.ToString();
			}
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, "0");
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			wf_handle();
		}
	}
}
