﻿using System;
using System.DataAccess;
using System.Drawing;
using System.Extensions;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using masofon;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Models;
using MvcSite.Common;

namespace Mesofon.MiniTerminal.Controllers
{
    public class w_mini_terminal_inputController : MvcSite.Common.SPBaseController
    {

        public ActionResult w_mini_terminal_input()
        {
            return this.View(new w_mini_terminal_input());
        }
        private w_mini_terminal_input ViewModel
        {
            get { return this.GetRootVisualElement() as w_mini_terminal_input; }
        }
        public void Form_Load(object sender, EventArgs e)
        {
            
        }
        public async Task<bool> ue_save_data(bool ab_quiet_save) //--------------------------------------------------------------------
        {
            ButtonElement cb_save = this.GetVisualElementById<ButtonElement>("cb_save");
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_checkbox_query cbx_query = this.GetVisualElementById<u_checkbox_query>("cbx_query");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Function:		public w_mini_terminal_input.ue_save_data()
            //
            //input:			ab_quiet_save - true - no success message, no printing and no reset for the window
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Description:	
            //			if user not enter order number for invoice or packing list, than 
            //			a) if it is invoice input -> packing_list_number = 0 and import_type = 'o'
            //			b) if it is paking input -> oreer_number = 0 and order_serial > 0
            //			
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_head_update_result = 0;
            long ll_order_num = 0;
            long ll_body_update_result = 0;
            long[] ll_material_number_arr = null;
            decimal ldec_discount = default(decimal);
            string ls_message = null;
            if (!dw_head.ue_check_validation())
            {
                return false;
            }
            this.dw_body_ue_delete_null_values();
            //.. if same items found concatenate them to single row
            //.. if same items found concatenate them to single row
            this.dw_body_ue_concatenate_values();
            if (!dw_body.ue_check_validation())
            {
                return false;
            }


            if (!(dw_head.RowCount() > 0 && dw_body.RowCount() > 0))
            {
                return false;
            }




            if (dw_head.DeletedCount() + dw_head.ModifiedCount() + dw_body.DeletedCount() + dw_body.ModifiedCount() <= 0)
            {
                return false;
            }
            if (!(this.ViewModel.iuo_material_updateProperty != null))
            {
                this.ViewModel.iuo_material_updateProperty = new u_material_update();
            }
            this.ViewModel.iuo_material_updateProperty.uf_set_sql_syntax(cbx_query.IsChecked);
            this.ViewModel.iuo_material_updateProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);

            this.ViewModel.iuo_material_updateProperty.uf_set_supplier_number(dw_head.GetItemValue<long>(0, "supplier_number"));

            ll_order_num = dw_head.GetItemValue<long>(0, "order_number");
            if (ll_order_num == 0)
            {
                ll_order_num = 0;
            }
            this.ViewModel.iuo_material_updateProperty.uf_set_order_number(ll_order_num);


            ll_material_number_arr = dw_body.GetItemValues<long>("material_number");
            (await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers_connect( ll_material_number_arr)).Retrieve(out ll_material_number_arr);
            await this.ViewModel.iuo_material_updateProperty.uf_load_material_suppliers(dw_body);
            this.ViewModel.iuo_material_updateProperty.uf_load_supplier_order_details(dw_body);
            await f_begin_tranClass.f_begin_tran();
            long ll_error_number = 0;
            long ll_result = 0;
            string ls_error_text = null;
            ll_result = this.ViewModel.iuo_material_updateProperty.uf_update(ref ll_error_number, ref ls_error_text);
            if (ll_result < 0)
            {
                await f_rollbackClass.f_rollback();
                //************************************************************
                // oren 7/11/2005 - write to log
                ls_message = "*************************** ue_save_data*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "iuo_material_update.uf_update The Data:   " + "Material Save Failed - Rollback" + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                //************************************************************
                await wf_show_message("", ls_error_text, "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון חומר נכשל"));
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
                if (this.ViewModel.iuo_material_updateProperty != null)
                {



                }
                // End
                return false;
            }


            if (dw_head.DeletedCount() + dw_head.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_head_update_result = dw_head.Update(unitOfWork);
                }

            }
            if (ll_head_update_result < 0)
            {
                await f_rollbackClass.f_rollback();
                //************************************************************
                // oren 7/11/2005 - write to log
                ls_message = "*************************** ue_save_data*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "dw_head.Update The Data:   " + "Saved Failed - Rollback" + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                //************************************************************
                if (rb_packing.IsChecked)
                {
                    await wf_show_message("", dw_head.uf_get_last_sql_error_text(), "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון תעודה נכשל"));
                }
                else
                {
                    await wf_show_message("", dw_head.uf_get_last_sql_error_text(), "OK",await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון חשבונית נכשל"));
                }
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
                if (this.ViewModel.iuo_material_updateProperty != null)
                {

 

                }
                // End
                return false;
            }


            if (dw_body.DeletedCount() + dw_body.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_body_update_result = dw_body.Update(unitOfWork);
                }
            }
            if (ll_body_update_result < 0)
            {
                await f_rollbackClass.f_rollback();
                //************************************************************
                // oren 7/11/2005 - write to log
                ls_message = "*************************** ue_save_data*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "dw_body.Update The Data:   " + "Saved Failed - Rollback" + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                //************************************************************
                if (rb_packing.IsChecked)
                {
                    await wf_show_message("", dw_body.uf_get_last_sql_error_text(), "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון פריטי תעודה נכשל"));
                }
                else
                {
                    await wf_show_message("", dw_body.uf_get_last_sql_error_text(), "OK",await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון פריטי חשבונית נכשל"));
                }
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
                if (this.ViewModel.iuo_material_updateProperty != null)
                {


                }
                // End
                return false;
            }
            await f_commitClass.f_commit();
            if (!ab_quiet_save)
            {
                if (rb_packing.IsChecked)
                {
                    await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון תעודה בוצע בהצלחה"));
                }
                else
                {
                    await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עדכון חשבונית בוצע בהצלחה"));
                }
            }
            this.ViewModel.ib_new_valuesProperty = false;
            cb_save.Enabled = false;
            if (this.ViewModel.iuo_material_updateProperty != null)
            {

   

            }
            if (!ab_quiet_save)
            {
                await this.ue_show_difference_report();
                if (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "start_new_doc_after_save", "yes", "reset all dw`s after save action") == "yes")
                {
                    await this.ue_new_doc(rb_packing.IsChecked);
                }
            }
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** ue_save_data*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "ue_save_data The Data:  " + "Saved Successfuly" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            return true;
        }
        public async Task<bool> ue_id_password_check()
        {
            s_password pass_t = new s_password();
            pass_t.action_title = 1;
            pass_t.a_mini_terminal_mode = true;

            await WindowHelper.Open<w_mini_terminal_id_check>("mini_terminal_mini_terminal", "password", pass_t);


            this.ViewModel.il_employee_numberProperty = (long)WindowHelper.GetParam<double>(this.ViewModel); ;
            if (this.ViewModel.il_employee_numberProperty > 0)
            {
                //.. update action_control
                await f_update_actionClass.f_update_action(781, this.ViewModel.il_employee_numberProperty);
            }
            return (this.ViewModel.il_employee_numberProperty > 0);
        }
        public async Task ue_new_doc(bool ab_packing_flag)
        {
            LabelElement total_quantity = this.GetVisualElementById<LabelElement>("total_quantity");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            string ls_message = null;
            string ls_doc_type = null;


            dw_head.Visible = false;
            if (ab_packing_flag)
            {
                dw_head.SetDataObject(new d_sql_mt_input_pack_headRepository());
            }
            else
            {
                dw_head.SetDataObject(new d_sql_mt_input_invoice_headRepository());
            }
            //.. multi_lingual_is_active = yes + current system language not Hebrew
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_dw(dw_head);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_head);
            }
            dw_head.Visible = true;


            dw_head.ue_new_row();


            dw_body.Visible = false;
            if (ab_packing_flag)
            {
                dw_body.SetDataObject(new d_sql_mt_input_pack_bodyRepository());
            }
            else
            {
                dw_body.SetDataObject(new d_sql_mt_input_invoice_bodyRepository());
            }
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_dw(dw_body);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_body);
            }
            dw_body.Visible = true;


            total_quantity.Text = "0";
            //************************************************************
            // oren 7/11/2005 - write to log
            if (ab_packing_flag)
            {
                ls_doc_type = "packing list";
            }
            else
            {
                ls_doc_type = "invoice";
            }
            ls_message = "*************************** ue_new_doc*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "ue_new_doc Doc Type:" + "\t" + ls_doc_type + "\r" + "\n" + "The Data:  " + "Event new doc" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
        }
        public async Task ue_show_difference_report() //--------------------------------------------------------------------
        {
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Event:			public w_mini_terminal_input.ue_show_difference_report()
            //
            // Returns:         None
            //
            // Copyright  - Stas
            //
            // Date Created: 24/08/2005
            //
            // Description:	
            //						 				print difference report
            //				for each of items at mini-terminal invoice/packing-list, checks its
            //				order quantity (supplier_order_details table)	
            //
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll = 0;
            long ll_rows = 0;
            long ll_material_number = 0;
            long ll_order_number = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            long ll_min_valid_month = 0;
            int li_report_copies = 0;
            decimal ldec_qty = default(decimal);
            string ls_supplier_name = null;
            string ls_employee_name = null;
            string ls_message = null;
            DateTime? ldt_curr_expiration_date = default(DateTime);
            GridElement lds_diff;
            li_report_copies = Convert.ToInt32(RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "copies_of_difference_report", "2", "the number of copies of difference report"));
            if (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "show_difference_report", "yes", "show difference button and print difference report after all mini-terminal save action") != "yes")
            {
                return;
            }


            lds_diff = VisualElementHelper.CreateFromView<GridElement>("d_ext_order_difference",
                "d_ext_order_difference", "global_global");

            ll_order_number = dw_head.GetItemValue<long>(0, "order_number");

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");

            ls_supplier_name = dw_head.GetItemValue<string>(0, "supplier_name");
            if (ll_order_number == 0)
            {
                ll_order_number = 0;
            }
            //.. employee name
            if (true)
            {
                ViewModel.LoadData(ref ls_employee_name);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1, "#20000");
                goto ERROR;
            }

            ll_rows = dw_body.RowCount();
            ls_message =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("פריט פג תוקף");
            for (ll = 1; ll <= ll_rows; ll++)
            {

                ll_material_number = dw_body.GetItemValue<long>(ll, "material_number");

                lds_diff.Insert((int)ll);


                lds_diff.SetItem(ll, "barcode", Convert.ToString(dw_body.GetItemValue<string>(ll, "barcode")));


                lds_diff.SetItem(ll, "name", Convert.ToString(dw_body.GetItemValue<string>(ll, "material_name")));
                if (ll_order_number != 0)
                {
                    ViewModel.LoadData1(ll_order_number, ll_material_number, ll_supplier_number, ref ldec_qty);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1, "#20000");
                        goto ERROR;
                    }

                    else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
                    {
                        ldec_qty = 0m;
                    }
                }
                
                lds_diff.SetItem(ll, "order_qty", ldec_qty.ToString());


                lds_diff.SetItem(ll, "in_qty", dw_body.GetItemValue<decimal>(ll, "expected_material_quantity").ToString());


                lds_diff.SetItem(ll, "invoice_qty", dw_body.GetItemValue<decimal>(ll, "material_quantity").ToString());

                lds_diff.SetItem(ll, "supplier_name", Convert.ToString(ls_supplier_name));

                lds_diff.SetItem(ll, "employee_name", Convert.ToString(ls_employee_name));
                // Lyoha 04.11.05 - Add expiry date item

                ll_min_valid_month = dw_body.GetItemValue<long>(ll, "min_valid_month");

                ldt_curr_expiration_date = dw_body.GetItemValue<DateTime>((int)ll, "expiration_date");
                if (ll_min_valid_month == 0 || ll_min_valid_month == 0)
                {
                    continue;
                }
                if (! await wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, ll, true))
                {

                    lds_diff.SetItem(ll, "message", Convert.ToString(ls_message));
                }
            }
            if (rb_packing.IsChecked)
            {
                //.. qty in packing list

                //uf_calculate_declines
                var invoice_qty_t = lds_diff.GetVisualElementById<TextBoxElement>("invoice_qty_t");
                invoice_qty_t.Text = await f_get_error_message_numberClass.f_get_error_message_number(20004);
                //.. pakcing list number

                //uf_calculate_declines
                var invoice_t = lds_diff.GetVisualElementById<TextBoxElement>("invoice_t");
                invoice_t.Text = ":" + f_get_error_message_numberClass.f_get_error_message_number(20003);

                ll_invoice_number = dw_head.GetItemValue<long>(0, "pack_list_number");
            }
            else
            {

                ll_invoice_number = dw_head.GetItemValue<long>(0, "invoice_number");
            }


            var date_t = lds_diff.GetVisualElementById<TextBoxElement>("date_t");
            //uf_calculate_declines
            date_t.Text = dw_head.GetItemValue<DateTime>(0, "supply_date").ToString();

            var supplier_number_t = lds_diff.GetVisualElementById<TextBoxElement>("supplier_number_t");
            //uf_calculate_declines
            supplier_number_t.Text = ll_supplier_number.ToString();

            var invoice_number_t = lds_diff.GetVisualElementById<TextBoxElement>("invoice_number_t");
            //uf_calculate_declines
            invoice_number_t.Text = ll_invoice_number.ToString();

            var employee_number_t = lds_diff.GetVisualElementById<TextBoxElement>("employee_number_t");
            //uf_calculate_declines
            employee_number_t.Text = this.ViewModel.il_employee_numberProperty.ToString();

            var branch_number_t = lds_diff.GetVisualElementById<TextBoxElement>("branch_number_t");
            //uf_calculate_declines
            branch_number_t.Text = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString();
            var order_number_t = lds_diff.GetVisualElementById<TextBoxElement>("order_number_t");
            if (ll_order_number != 0)
            {
                //uf_calculate_declines
                order_number_t.Text = ll_order_number.ToString();
            }
            else
            {
                //uf_calculate_declines
                order_number_t.Visible = false;

                var order_number_title_t = lds_diff.GetVisualElementById<TextBoxElement>("order_number_title_t");
                //uf_calculate_declines
                order_number_title_t.Visible = false;
            }

            lds_diff.Modify("datawindow.print", "Orientation", "1");

            //uf_calculate_declines

            //ToDo - move to WCF PrintBOS
            //(lds_diff).DataWindow.Print.Copies = li_report_copies;

            lds_diff.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName);
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
            if (lds_diff != null)
            {

            }
            // End
            ERROR:
            if (lds_diff != null)
            {

          
            }
        }
        public void wf_get_window_position()
        {
            long ll_xpos = 0;
            long ll_ypos = 0;
            string ls_window_height = null;
            string ls_window_width = null;
            string ls_window_xpos = null;
            string ls_window_ypos = null;
            string ls_lang = null;
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
            {
                ls_lang = "alignment_left";
            }
            else
            {
                ls_lang = "alignment_right";
            }
            if (Convert.ToInt64(ls_window_xpos) > 0)
            {
                ll_xpos = Convert.ToInt64(ls_window_xpos);
            }
            else
            {
                ll_xpos = 0;
            }
            if (Convert.ToInt64(ls_window_ypos) > 0)
            {
                ll_ypos = Convert.ToInt64(ls_window_ypos);
            }
            else
            {
                ll_ypos = 0;
            }
            if (Convert.ToInt64(ls_window_width) > 0)
            {
                this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
            }
            if (Convert.ToInt64(ls_window_height) > 0)
            {
                this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
            }

            WindowElement root = this.GetRootVisualElement() as WindowElement;
            root.Location = new Point((int)ll_xpos, (int)ll_ypos);
            this.ViewModel.Visible = true;
        }
        public async Task<long> wf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_input.wf_show_message()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 03/08/2005
            //
            // Description:	
            // 						a) istr_arg.a_string[1] ---> window title
            //						b) istr_arg.a_string[2] ---> error text
            //						c) istr_arg.a_string[3] ---> message icon 
            //						d) istr_arg.a_string[4] ---> sets default button focus
            //						e) istr_arg.a_string[5] ---> default buttons to be displayed
            //						f) istr_arg.a_string[6] ---> message text
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            return await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox(as_title, as_error_text, "", as_buttons, 1, as_message);
        }
        public void wf_set_focus(ControlElement ago_control, long al_row, string as_column)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            string ls_message = null;
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_focus *************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Column: " + as_column + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            switch (ago_control.GetType().Name)
            {
                case "dw_body":
                    dw_body.Focus();

                    dw_body.SetRow((int)al_row);

                    dw_body.SetColumn(as_column);

                    dw_body.SelectText(1, 20);
                    break;
                case "dw_head":
                    dw_head.Focus();

                    dw_head.SetRow((int)al_row);

                    dw_head.SetColumn(as_column);

                    dw_head.SelectText(1, 20);
                    break;
            }
        }
        public async Task<bool> wf_show_expiration_msg(DateTime? adt_curr_expiration_date, long al_min_valid_months, long al_row, bool ab_check_only) //
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            //						a)	istr_arg.a_string[1] ---> window title
            //						b) istr_arg.a_string[2] ---> error text
            //						c) istr_arg.a_string[3] ---> message icon 
            //						d) istr_arg.a_string[4] ---> buttons to be displayed
            //						e) istr_arg.a_string[5] ---> sets default button focus
            //						f)	istr_arg.a_string[6] ---> message text
            //						g) istr_arg.a_datetime[1] ---> current exiration date of a material
            //						h) istr_arg.a_datetime[2] ---> minimum exiration date of a material
            //

            s_array_arguments lstr_arg = new s_array_arguments();
            s_array_arguments lstr_ret = new s_array_arguments();
            long ll_return = 0;
            string ls_message = null;
            DateTime? ldt_current_date = default(DateTime);
            DateTime? ld_min_valid_date = default(DateTime);
            ldt_current_date = masofonAlias.masofon.Instance.set_machine_time();
            ld_min_valid_date = ldt_current_date.Value.AddDays(al_min_valid_months * 30);
            //IF Date(adt_curr_expiration_date) < ld_min_valid_date OR IsNull(adt_curr_expiration_date) THEN
            if (ab_check_only)
            {
                return false;
            }
            //	ls_message += nvo_translator.fnv_translate_exp('תאריך התוקף של הפריט שהוזן') + "~" "
            //	ls_message += dw_body.GetItem<string>(al_row, "material_name") + "~" "
            //	ls_message += nvo_translator.fnv_translate_exp('.אינו עומד בדרישות חודשי התוקף המינימליים') + " "
            //	ls_message += nvo_translator.fnv_translate_exp(': תאריך התוקף המינימלי הוא') + " "	 
            //	ls_message += String(ld_min_valid_date, "dd/mm/yyyy")
            //
            ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("נא הזן תאריך פג תוקף") + " ";

            ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לפריט ") + dw_body.GetItemValue<string>(al_row, "material_name");
            lstr_arg.a_string[6] = "";
            lstr_arg.a_string[1] = "";
            lstr_arg.a_string[2] = "";
            lstr_arg.a_string[3] = "";
            lstr_arg.a_string[4] = "OKCancel";
            lstr_arg.a_string[5] = "1";
            lstr_arg.a_string[6] = ls_message;
            lstr_arg.a_datetime[1] = adt_curr_expiration_date;
            lstr_arg.a_datetime[2] = ld_min_valid_date.Value.Add(Convert.ToDateTime("00:00").TimeOfDay);


            await WindowHelper.Open<w_mini_terminal_exiration_date>("mini_terminal_mini_terminal", "args", lstr_arg);

            lstr_ret = (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
            if (lstr_ret.arg_ok)
            {
                //..user has entered a date

                dw_body.SetItem(al_row, "expiration_date", lstr_ret.a_datetime[1].ToString());
                if (CollectionExtensions.UBound(lstr_ret.a_boolean) >= 0)
                {
                    return lstr_ret.a_boolean[0];
                }
            }
            else
            {
                //Datetime 	ldt_null_date
                //SetNull(ldt_null_date)
                //dw_body.SetItem(al_row, "expiration_date", ldt_null_date)		
                //..user didn't enter a date
                return false;
            }
            //END IF
            return true;
        }
                
        public async Task<FuncResults<decimal, decimal, decimal, long, object>> wf_get_order_material_price(long al_supplier_number, long al_order_number, long al_material_number, decimal adec_material_price, decimal adec_material_price_after_discount, decimal adec_material_discount_percent, long al_error_number)
        {
            //--------------------------------------------------------------------
            //Function:			public w_mini_terminal_input.wf_get_order_material_price()
            //
            // Returns:         None
            //
            // Parameters:      value Long		al_supplier_number
            // 						value Long 		al_order_number
            //						value Long 		al_material_number	---> current material number
            //						ref 	dec{2}	adec_material_price	 
            //						ref 	dec{2}	adec_adec_material_price_after_discount
            //						ref	Long	al_error_number		---> 
            //							if error occurce gets -1, if material not assigned to order gets 100
            //							otherwise gets 0 value
            // 
            // Copyright  - Stas
            //
            // Date Created: 01/08/2005
            //
            // Description:	
            // 					IF order_number exists and ib_get_material_price_from_order_if_it_exists = TRUE
            //					gets material-prices from supplier_order_details table.
            //					otherwise gets material-prices from material_supplier table
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            bool lb_material_in_order = false;
            if (al_material_number > 0)
            {
                if (this.ViewModel.ib_get_material_price_from_order_if_it_existsProperty)
                {
                    if (!(al_order_number == 0 || (al_order_number == 0 || (al_supplier_number == 0 || al_supplier_number == 0))))
                    {
                        this.ViewModel.iuo_order_searchProperty.uf_set_order_number(al_order_number);
                        lb_material_in_order = this.ViewModel.iuo_order_searchProperty.uf_get_order_material_price(al_material_number, ref adec_material_price, ref adec_material_price_after_discount, ref adec_material_discount_percent);
                        if (!lb_material_in_order)
                        {
                            //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                            adec_material_price = (await this.ViewModel.iuo_material_searchProperty.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                            adec_material_price_after_discount = adec_material_price;
                        }
                    }
                    else
                    {
                        //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                        adec_material_price = (await this.ViewModel.iuo_material_searchProperty.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                        adec_material_price_after_discount = adec_material_price;
                    }
                }
                else
                {
                    //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                    adec_material_price = (await this.ViewModel.iuo_material_searchProperty.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                    adec_material_price_after_discount = adec_material_price;
                }
            }
            return FuncResults.Return(adec_material_price, adec_material_price_after_discount, adec_material_discount_percent, al_error_number, (object)null);
        }
        public decimal wf_calc_total_quantity()
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            decimal ldec_total_quantity = default(decimal);
            decimal ldec_expected_quantity = default(decimal);
            long ll_index = 0;
            string ls_message = null;
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_calc_total_quantity *************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_calc_total_quantity Function " + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            ldec_total_quantity = 0;

            for (ll_index = 1; ll_index <= dw_body.RowCount(); ll_index++)
            {

                ldec_expected_quantity = dw_body.GetItemValue<decimal>(ll_index, "expected_material_quantity");
                if (!(ldec_expected_quantity == 0))
                {
                    ldec_total_quantity += ldec_expected_quantity;
                }
            }
            return ldec_total_quantity;
        }
        public async Task open()
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            string ls_message = null;
            string ls_log_mode = null;
            //if (!this.ue_id_password_check().Result)
            //{
            //    //MessageBox("", nvo_translator.fnv_translate_exp("עובד לא קיים"))
            //    this.close();
            //    return;
            //}
            //---------------------------------------------------------------------------
            this.ViewModel.ib_get_material_price_from_order_if_it_existsProperty = (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "get_material_price_from_order_if_it_exists", "yes", "if order exists gets material price from it").ToLower(CultureInfo.InvariantCulture) == "yes");

            if (dw_head.RowCount() == 1)
            {
                dw_head.Focus();
            }
            //.. no update made to dw_head
            this.ViewModel.ib_new_valuesProperty = true;
            this.ViewModel.iuo_material_searchProperty = new u_material_search();
            this.ViewModel.iuo_material_searchProperty.uf_set_mt_message(true);
            this.ViewModel.iuo_order_searchProperty = new u_order_search();
            this.ViewModel.iuo_order_searchProperty.uf_set_mt_message(true);
            this.ViewModel.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.ViewModel.ib_auto_current_qtyProperty = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "auto_expected_qty_copy", "yes", "").ToLower(CultureInfo.InvariantCulture) == "yes";
            //.. multi_lingual_is_active = yes + current system language not Hebrew
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_head);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_body);
            }
            // SharonS - 0.0.0.1 - 2007-04-10 - CR#1010 - Set the computed field material_name_com to
            //			have an expression that will Align Hebrew (or mixed) text to the Right and English text to the Left

            dw_body.Modify("material_name_com", "Alignment", "0");
            dw_body.Modify("if(IsHebrew(material_name), 1, 0)'");

            // SharonS - 1.2.30.19 - 2008-07-03- CR#1096 - Log on/off
            //ls_log_mode = f_get_parameter_value("log_modules", "string", "NONE", "MINI_TERMINAL, REPORTS, SUPPLIERS_FILE, ORDER, STOCK_COUNT,  or None.", "gds_find") 	
            //ib_log_mode = Pos(ls_log_mode, "MINI_TERMINAL") > 0 
            // SharonS - 1.2.32.3 - 2008-07-16 - Change "log_modules" parameter
            // SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Change the Log on/off paramenter to be global
            //ls_log_mode = f_get_parameter_value("log_modules", "string", "NONE", "MT (MINI_TERMINAL), SF (SUPPLIERS_FILE), SC (STOCK_COUNT),  or None.", "gds_find") 	
            //ib_log_mode = Pos(ls_log_mode, "MT") > 0
            this.ViewModel.ib_log_modeProperty = masofonAlias.masofon.Instance.gb_mt_log;
            // End
            // End
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Open*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "Open" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            //************************************************************
            wf_get_window_position();
        }
        public void close()
        {
            string ls_message = null;
            if (this.ViewModel.iuo_material_searchProperty != null)
            {

          

            }
            if (this.ViewModel.iuo_order_searchProperty != null)
            {


            }
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Close*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "Close" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
        }
        public void ue_key()
        {
        }

        public int dw_print_printpage(int l1, int l2)
        {
            return 1;
        }

        public async Task cb_difference_report_clicked(object sender, EventArgs e)
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - cb_difference_report*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "cb_difference_report The Data:  " + "clicked on difference report" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            if (!dw_head.ue_check_validation())
            {
                return;
            }
            this.dw_body_ue_delete_null_values();
            //.. if same items found concatenate them to single row
            //.. if same items found concatenate them to single row
            this.dw_body_ue_concatenate_values();
            if (!dw_body.ue_check_validation())
            {
                return;
            }
            await this.ue_show_difference_report();
        }

        public void cb_difference_report_constructor(object sender, EventArgs e)
        {
            ButtonElement cb_difference_report = this.GetVisualElementById<ButtonElement>("cb_difference_report");
            cb_difference_report.Enabled = (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "show_difference_report", "yes", "show difference button and print difference report after all mini-terminal save action") == "yes");
        }

        public async Task cb_new_clicked(object sender, EventArgs e)
        {
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_cb_closewindow cb_close = this.GetVisualElementById<u_cb_closewindow>("cb_close");
            bool lb_save_before_close = false;
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - cb_new*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "cb_new The Data:  " + "clicked on new doc" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            cb_close.Enabled = false;




            lb_save_before_close = ((dw_head.DeletedCount() + dw_head.ModifiedCount() + dw_body.DeletedCount() + dw_body.ModifiedCount()) > 0);

            if (lb_save_before_close && dw_body.RowCount() > 0)
            {
                //.. would you like to save changes ?
                if (await wf_show_message("", "", "OKCancel", "#1010") == 1)
                {
                    if ( await this.ue_save_data(false))
                    {
                        await this.ue_new_doc(rb_packing.IsChecked);
                    }
                }
                else
                {
                    await this.ue_new_doc(rb_packing.IsChecked);
                }
            }
            else
            {
                await this.ue_new_doc(rb_packing.IsChecked);
            }
            cb_close.Enabled = true;
        }

        public async Task rb_invoice_clicked(object sender, EventArgs e) //.. sets invoice  
        {
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - rb_invoice*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "rb_packing The Data:  " + "clicked on invoice" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            //************************************************************
            await this.ue_new_doc(false);
        }

        public async Task rb_packing_clicked(object sender, EventArgs e) //.. sets packing list 
        {
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - rb_packing*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "rb_packing The Data:  " + "clicked on packing list" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************

            //************************************************************
            await this.ue_new_doc(true);

           
        }

        public async Task cb_save_clicked(object sender, EventArgs e)
        {
            u_cb_closewindow cb_close = this.GetVisualElementById<u_cb_closewindow>("cb_close");
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - cb_save*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "cb_save The Data:  " + "clicked on save doc" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            cb_close.Enabled = false;
            await this.ue_save_data(false);
            cb_close.Enabled = true;
        }

        public async Task cb_close_clicked(object sender, EventArgs e)
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_cb_closewindow cb_close = this.GetVisualElementById<u_cb_closewindow>("cb_close");
            bool lb_save_before_close = false;
            string ls_message = null;
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "cb_close The Data:  " + "clicked on close" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************
            cb_close.Enabled = false;




            lb_save_before_close = ((dw_head.DeletedCount() + dw_head.ModifiedCount() + dw_body.DeletedCount() + dw_body.ModifiedCount()) > 0);

            if (lb_save_before_close && dw_body.RowCount() > 0)
            {
                //.. would you like to save changes ?
                if (await wf_show_message("", "", "OKCancel", "#1010") == 1)
                {
                    if ( await this.ue_save_data(false))
                    {


                        ((WindowElement)this.ViewModel.Parent).Close();
                    }
                    else
                    {
                        cb_close.Enabled = true;
                    }
                }
                else
                {


                    ((WindowElement)this.ViewModel.Parent).Close();
                }
            }
            else
            {


                ((WindowElement)this.ViewModel.Parent).Close();
            }
        }
        public async Task<bool> dw_body_ue_check_validation_for_row(long al_row, string as_current_column)
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            string ls_barcode = null;
            string ls_message = null;
            long ll_material_number = 0;
            decimal ldec_material_qty = default(decimal);

            if (dw_body.RowCount() == 0)
            {
                return false;
            }

            ls_barcode = dw_body.GetItemValue<string>(al_row, "barcode");
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_check_validation_for_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Barcode: " + ls_barcode + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            if (isempty_stringClass.isempty_string(ls_barcode))
            {
                await wf_show_message("", "", "OK",await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ברקוד לא יכול להיות ריק"));
                wf_set_focus(dw_body, al_row, "barcode");
                return false;
            }

            ldec_material_qty = dw_body.GetItemValue<decimal>(al_row, "expected_material_quantity");
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_check_validation_for_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Expected material quantity: " + ldec_material_qty.ToString() + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            // fix oren 9/11/05
            if (ldec_material_qty == 0)
            {
                //.. כמות בפעול לא יכולה להיות ריקה
                await wf_show_message("", "", "OK", "#20011");
                wf_set_focus(dw_body, al_row, "expected_material_quantity");
                return false;
            }
            else if (as_current_column == "expected_material_quantity")
            {
                return true;
            }

            ll_material_number = dw_body.GetItemValue<long>(al_row, "material_number");
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_check_validation_for_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Material number: " + ll_material_number.ToString() + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            if (ll_material_number == 0 || ll_material_number == 0)
            {
                if (!this.ViewModel.iuo_material_searchProperty.uf_material_exists(ls_barcode))
                {
                    if (!isempty_stringClass.isempty_string(ls_barcode))
                    {
                        await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ברקוד אינו קיים"));
                    }
                    else
                    {
                        await wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ברקוד לא יכול להיות ריק"));
                    }
                    wf_set_focus(dw_body, al_row, "barcode");
                    return false;
                }
            }

            ldec_material_qty = dw_body.GetItemValue<decimal>(al_row, "material_quantity");
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_check_validation_for_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Material quantity: " + ldec_material_qty.ToString() + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            if (ldec_material_qty == 0 || ldec_material_qty == 0)
            {
                //.. כמות בחשבונית לא יכולה להיות ריקה
                await wf_show_message("", "", "OK", "#20012");
                wf_set_focus(dw_body, al_row, "material_quantity");
                return false;
            }
            long ll_min_valid_month = 0;
            DateTime? ldt_curr_expiration_date = default(DateTime);

            ldt_curr_expiration_date = dw_body.GetItemValue<DateTime>((int)al_row, "expiration_date");
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_check_validation_for_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Expiration date: " + ldt_curr_expiration_date.ToString() + "\r" + "\n" + "Row:         " + al_row.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End

            ll_min_valid_month = dw_body.GetItemValue<long>(al_row, "min_valid_month");
            if (!(ll_min_valid_month == 0 || ll_min_valid_month == 0) && ldt_curr_expiration_date == default(DateTime) || ldt_curr_expiration_date == null)
            {
                if (! await wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, al_row, false))
                {
                    return false;
                }
            }
            return true;
        }
        public void dw_body_ue_delete_null_values()
        {
            LabelElement total_quantity = this.GetVisualElementById<LabelElement>("total_quantity");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");


            if (dw_body.Find("IsNull(barcode) OR barcode == \"\"", 1, dw_body.RowCount()) > 0)
            {



                dw_body.SetFilter("barcode == null OR barcode = ''");

                dw_body.Filter();
                dw_body.RowsMove(1, dw_body.RowCount(), ModelBuffer.Primary, dw_body, Convert.ToInt32(dw_body.DeletedCount()) + 1, ModelBuffer.Delete);

                dw_body.SetFilter("");

                dw_body.Filter();


                total_quantity.Text = wf_calc_total_quantity().ToString();
            }
        }
        public void dw_body_ue_concatenate_values()
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            GridElement lds_temp = null;
            string ls_old_sort = null;
            string ls_barcode = null;
            long ll = 0;
            long? ll_row = 0;
            decimal ldec_qty = default(decimal);
            //.. Display only distinct rows
            ls_old_sort = dw_body.GetSort();

            dw_body.SetSort("material_number ASC");

            dw_body.Sort();


            if (dw_body.Find("NOT (material_number[-1] ==null OR material_number[-1] <> material_number)", 1, dw_body.RowCount()) == 0)
            {

                dw_body.SetSort(ls_old_sort);

                dw_body.Sort();


                return;
            }

            dw_body.SetFilter("NOT ((material_number[-1] == null) OR material_number[-1] <> material_number)");

            dw_body.Filter();




            lds_temp = dw_body;




            dw_body.RowsCopy(1, dw_body.RowCount(), ModelBuffer.Primary, lds_temp, 1, ModelBuffer.Primary);
            dw_body.RowsMove(1, dw_body.RowCount(), ModelBuffer.Primary, dw_body, Convert.ToInt32(dw_body.DeletedCount()) + 1, ModelBuffer.Delete);

            dw_body.SetFilter("");

            dw_body.Filter();

            for (ll = 1; ll <= lds_temp.RowCount(); ll++)
            {

                ls_barcode = lds_temp.GetItemValue<string>(ll, "barcode");

                ldec_qty = lds_temp.GetItemValue<decimal>((int)ll, "expected_material_quantity");


                ll_row = dw_body.Find("barcode == \"" + ls_barcode + "\"", 0, dw_body.RowCount());
                if (ll_row >= 0)
                {


                    dw_body.SetItem(ll_row.Value, "expected_material_quantity", Convert.ToString(ldec_qty.ToString() + dw_body.GetItemValue<decimal>(ll_row.Value, "expected_material_quantity").ToString()));


                    dw_body.SetItem(ll_row.Value, "material_quantity", Convert.ToString(ldec_qty.ToString() + dw_body.GetItemValue<decimal>(ll_row.Value, "material_quantity").ToString()));
                    //.. if item removed from dw have min-idicator number than ohter -> save it indicaror counter
                    long ll_old_indicator = 0;
                    long ll_new_indicator = 0;

                    ll_old_indicator = lds_temp.GetItemValue<long>((int)ll, "indicator");

                    ll_new_indicator = dw_body.GetItemValue<long>(ll_row.Value, "indicator");
                    if (ll_old_indicator < ll_new_indicator)
                    {

                        dw_body.SetItem(ll_row.Value, "indicator", ll_old_indicator.ToString());
                    }
                }
            }

            dw_body.SetSort(ls_old_sort);

            dw_body.Sort();


            if (lds_temp != null)
            {


            }
            //.. 
        }
        public async Task<long> dw_body_ue_get_max_serial_number()
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            long ll_max_serial_number = 0;
            if (rb_packing.IsChecked)
            {
                await f_begin_tranClass.f_begin_tran();
                ll_max_serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(27);
                await f_commitClass.f_commit();

                dw_head.SetItem(0, "packing_details_serial_number", ll_max_serial_number.ToString());
            }
            else
            {

                if (dw_body.RowCount() > 0)
                {
                    await f_begin_tranClass.f_begin_tran();
                    ll_max_serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(4);
                    await f_commitClass.f_commit();
                }
            }
            return ll_max_serial_number;
        }
        public async Task dw_body_ue_load_values_after_order_change() //--------------------------------------------------------------------
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Event:			public w_mini_terminal_input.ue_load_values_after_order_change()
            //
            // Returns:      None
            //
            // Copyright  - Stas
            //
            // Date Created: 30/08/2005
            //
            // Description:	
            //				if order-number was changed after invoice/packing-list filled
            //				run all over the body and change all material-prices
            //				(gets value from supplier_order_details or matrerial-price)
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_order_number = 0;
            long ll_supplier_number = 0;
            long ll = 0;
            string ls_barcode = null;

            ll_order_number = dw_head.GetItemValue<long>(0, "order_number");

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            //.. checks if order_number and supplier_number exists
            //IF IsNull(ll_order_number) OR ll_order_number = 0 THEN return
            if (ll_supplier_number == 0 || ll_supplier_number == 0)
            {
                return;
            }

            for (ll = dw_body.RowCount(); ll >= 1; ll += -1)
            {

                ls_barcode = dw_body.GetItemValue<string>(ll, "barcode");
                if (!isempty_stringClass.isempty_string(ls_barcode))
                {
                    EventArgs e = new EventArgs();
                    await this.dw_body_ue_new_material_number_inserted(ls_barcode, ll,e);
                }
            }
        }
        public async Task dw_body_ue_new_material_number_inserted(string as_barcode, long al_row, EventArgs e) //--------------------------------------------------------------------
        {
            ButtonElement cb_save = this.GetVisualElementById<ButtonElement>("cb_save");
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            //Function:			public w_mini_terminal_input.ue_new_material_number_inserted()
            // 
            // Returns:         None
            //  
            // Parameters:      value String as_barcode				--> current inserted barcode
            // 						value Long	 al_row					--> current row
            // 
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            // 
            // Description:	
            // 					1) check if material with that barcode exists (check material  number)
            //					2) if material exists sets sell price, cost price, atc
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_material_name = null;
            long ll_material_number = 0;
            long ll_order_number = 0;
            long ll_supplier_number = 0;
            long ll_min_valid_month = 0;
            long ll_error_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_order_material_color = 0;
            bool lb_material_assigned_for_order = false;
            decimal ldec_sell_price = default(decimal); //.. from branch_item_price
            decimal ldec_material_price = default(decimal); //.. from supplier_order_details if order exists 
            decimal ldec_material_price_after_discount = default(decimal); //.. from supplier_order_details
            decimal ldec_material_discount_percent = default(decimal); //.. from supplier_order_details

            dw_body.PerformValidated(e);

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");

            ll_order_number = dw_head.GetItemValue<long>(0, "order_number");
            //.. get material-number, material-name, minimum valid month for material return by barcode
            (await this.ViewModel.iuo_material_searchProperty.uf_get_material_info(as_barcode, ll_material_number, ls_material_name, ll_min_valid_month)).Retrieve(out ll_material_number, out ls_material_name, out ll_min_valid_month);
            if (ll_material_number > 0)
            {
                //.. gets material-prices from material_supplier OR supplier_order_details table
                (await wf_get_order_material_price(ll_supplier_number, ll_order_number, ll_material_number, ldec_material_price, ldec_material_price_after_discount, ldec_material_discount_percent, ll_error_number)).Retrieve(out ldec_material_price, out ldec_material_price_after_discount, out ldec_material_discount_percent, out ll_error_number);
                //.. error occurce (for example DB error)
                if (ll_error_number == -1)
                {
                    return;
                }
                lb_material_assigned_for_order = this.ViewModel.iuo_order_searchProperty.uf_material_assigns_to_order(ll_material_number);
                //.. gets value from branch_item_price table for this branch and material number
                ldec_sell_price =await this.ViewModel.iuo_material_searchProperty.uf_get_material_sell_price(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_material_number);
                ll_order_material_color = this.ViewModel.iuo_order_searchProperty.uf_get_order_material_color(ll_material_number);

                dw_body.SetItem(al_row, "material_number", ll_material_number.ToString());

                dw_body.SetItem(al_row, "barcode", Convert.ToString(as_barcode));

                dw_body.SetItem(al_row, "material_name", Convert.ToString(ls_material_name));

                dw_body.SetItem(al_row, "material_price", ldec_material_price.ToString());

                dw_body.SetItem(al_row, "material_price_after_discount", ldec_material_price_after_discount.ToString());

                dw_body.SetItem(al_row, "material_discount_percent", ldec_material_discount_percent.ToString());

                dw_body.SetItem(al_row, "min_valid_month", ll_min_valid_month.ToString());

                dw_body.SetItem(al_row, "sell_price", ldec_sell_price.ToString());

                dw_body.SetItem(al_row, "current_catalog_sell_price", ldec_sell_price.ToString());

                if (!(dw_body.GetItemValue<int>(al_row, "indicator") > 0))
                {


                    dw_body.SetItem(al_row, "indicator", Convert.ToString(Convert.ToInt64(dw_body.Describe<long>("Evaluate('MAX(indicator for all)',1)")) + 1));
                }

                dw_body.SetItem(al_row, "supplier_number", ll_supplier_number.ToString());
                //.. SPR = P0000098 ..... flag that indicates mini terminal input

                dw_body.SetItem(al_row, "mini_terminal", "1");
                //.. end
                //.. if material assigned to order sets its order-coor-number , otherwise sets null

                dw_body.SetItem(al_row, "color", ll_order_material_color.ToString());
                if (rb_packing.IsChecked)
                {
                    ll_material_serial_number_in_order = this.ViewModel.iuo_order_searchProperty.uf_get_order_material_serial_number(ll_material_number);
                    //.. sets pack_list_number from packing_list_details table


                    dw_body.SetItem(al_row, "pack_list_number", dw_head.GetItemValue<int>(0, "pack_list_number").ToString());
                    //.. serial number of supplier_order_details for this material number

                    dw_body.SetItem(al_row, "order_serial", ll_material_serial_number_in_order.ToString());
                    if (lb_material_assigned_for_order)
                    {

                        dw_body.SetItem(al_row, "order_number", ll_order_number.ToString()); //.. order number
                        //this.SetItem(al_row, "import_type", "o")					//.. order type
                    }
                    else
                    {
                        //.. if material not assigned

                        dw_body.SetItem(al_row, "order_number", "0");
                    }
                }
                else
                {
                    if (lb_material_assigned_for_order)
                    {

                        dw_body.SetItem(al_row, "packing_list_number", ll_order_number.ToString()); //.. order number

                        dw_body.SetItem(al_row, "import_type", "o"); //.. order type
                    }
                    else
                    {
                        //.. if material not assigned

                        dw_body.SetItem(al_row, "packing_list_number", "0");
                    }


                    dw_body.SetItem(al_row, "invoice_number", dw_head.GetItemValue<int>(0, "invoice_number").ToString());


                    dw_body.SetItem(al_row, "packing_serial", Convert.ToString(Convert.ToInt64(dw_body.Describe<long>("Evaluate('MAX(indicator for all)',1)")) + 1));
                }
                wf_set_focus(dw_body, al_row, "expected_material_quantity");
            }
            else if (ll_material_number == 0)
            {
                await wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא נמצא חומר מתאים"));

                dw_body.SetItem(al_row, "material_quantity", "0");
                this.ViewModel.igo_Focus_NextProperty = dw_body;
                this.ViewModel.is_dw_column_for_focusProperty = "barcode";
            }
            cb_save.Enabled = true;
        }
        public async Task dw_body_ue_new_row()
        {
            ButtonElement cb_save = this.GetVisualElementById<ButtonElement>("cb_save");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            dw_body.ue_new_row();
            long ll_row = 0;
            long ll_next_serial_number = 0;
            string ls_message = null;

            ll_row = dw_body.Insert(0);
            ll_next_serial_number = await this.dw_body_ue_get_max_serial_number();

            dw_body.SetItem(ll_row, "serial_number", ll_next_serial_number.ToString());

            dw_body.SetItem(ll_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** ue_new_row - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "ue_new_row Event Row:         " + "New Row: " + ll_row.ToString() + "Serial number: " + ll_next_serial_number.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            // End

            dw_body.SetRow((int)ll_row);

            dw_body.SetColumn("barcode");
            dw_body.Focus();

            dw_body.ScrollToRow((int)ll_row);
            cb_save.Enabled = true;
            //.. SPR = P0000092 ..............

            dw_body.Modify("DataWindow", "NoUserPrompt", "Yes");
            //.......................................
        }
        public async Task<int> dw_body_ue_enter(object sender, GridElementCellEventArgs e)
        {
            u_cb_closewindow cb_close = this.GetVisualElementById<u_cb_closewindow>("cb_close");
            LabelElement total_quantity = this.GetVisualElementById<LabelElement>("total_quantity");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            string ls_column = null;
            string ls_auto_save = null;
            string ls_message = null;
            long ll_row = 0;
            bool lb_save_is_needed = false;
            ls_auto_save = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "\\Office\\Application\\Toolbar\\w_mini_terminal_input", "auto_save_after_every_row", "yes", "save data after every new row");
            lb_save_is_needed = dw_head.DeletedCount() + dw_head.ModifiedCount() + dw_body.DeletedCount() + dw_body.ModifiedCount() > 0;
            object keyflags = null;

            if (Convert.ToInt32(keyflags) == 0 && e.KeyCode == Keys.Enter)
            {

                ll_row = dw_body.GetRow();

                dw_body.PerformValidated(e);

                if (dw_body.RowCount() > 0)
                {

                    ls_column = dw_body.GetColumnName();
                    if (ls_column == "expected_material_quantity")
                    {
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** ue_enter - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Before ue_check_validation_for_row, Column: " + ls_column + "\r" + "\n" + "Row:         " + ll_row.ToString() + "\r" + "\n" + "Data:" + "\t" + "\t" + "Registry: auto_save: " + ls_auto_save + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                        if (await this.dw_body_ue_check_validation_for_row(ll_row, "expected_material_quantity"))
                        {
                            total_quantity.Text = wf_calc_total_quantity().ToString();

                            if (dw_body.RowCount() >= ll_row)
                            {
                                wf_set_focus(dw_body, ll_row, "material_quantity");
                            }
                            else
                            {
                                // oren 20/11/2005 
                                if (ls_auto_save == "yes" && lb_save_is_needed)
                                {
                                    // quiet save - without questions and messages
                                    cb_close.Enabled = false;
                                    if (! await this.ue_save_data(true))
                                    {
                                        cb_close.Enabled = true;
                                        return 0;
                                    }
                                    cb_close.Enabled = true;
                                    this.ViewModel.ib_new_inv_packProperty = false;
                                }
                                //
                                //
                                dw_body.ue_new_row();
                            }
                        }
                    }
                    else if (ls_column == "material_quantity")
                    {
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** ue_enter - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Before ue_check_validation_for_row, Column: " + ls_column + "\r" + "\n" + "Row:         " + ll_row.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                        if ( await this.dw_body_ue_check_validation_for_row(ll_row, "material_quantity"))
                        {

                            if (dw_body.RowCount() > ll_row)
                            {
                                wf_set_focus(dw_body, ll_row + 1, "barcode");
                            }
                            else
                            {


                                if (dw_head.GetItemValue<string>(1, "state").ToLower(CultureInfo.InvariantCulture) != "c" || isempty_stringClass.isempty_string(dw_head.GetItemValue<string>(0, "state")))
                                {
                                    // oren 20/11/2005 
                                    if (ls_auto_save == "yes" && lb_save_is_needed)
                                    {
                                        // quiet save - without questions and messages
                                        if (!await this.ue_save_data(true))
                                        {
                                            cb_close.Enabled = true;
                                            return 0;
                                        }
                                        cb_close.Enabled = true;
                                        this.ViewModel.ib_new_inv_packProperty = false;
                                    }
                                    // 
                                    // 
                                    dw_body.ue_new_row();
                                }
                            }
                        }
                    }
                    else if (ls_column == "barcode")
                    {
                        string ls_barcode = null;
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** ue_enter - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Column: " + ls_column + "\r" + "\n" + "Row:         " + ll_row.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End

                        ls_barcode = dw_body.GetItemValue<string>(ll_row, "barcode");

                        if (isempty_stringClass.isempty_string(ls_barcode) && dw_body.RowCount() > 1)
                        {
                            // oren 10/11/05 - fix stay on barcode insted of save
                            wf_set_focus(dw_body, ll_row, "barcode");
                            //parent.TriggerEvent("ue_save_data")
                            //
                            return 0;
                        }

                        if (dw_body.RowCount() > 0)
                        {

                            if (dw_body.GetItemValue<int>(ll_row, "material_number") > 0)
                            {
                                wf_set_focus(dw_body, ll_row, "expected_material_quantity");
                                return 1;
                            }
                        }
                    }
                }
            }
            return 0;
        }
        public async Task<bool> dw_body_ue_check_validation()
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            long? ll_row = 0;

            if (dw_body.RowCount() == 0)
            {
                await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא נמצאו פריטים"));
                wf_set_focus(dw_head, 1, "total");
                return false;
            }


            ll_row = dw_body.Find("IsNull(barcode) OR barcode == \"\"", 0, dw_body.RowCount());
            if (ll_row >= 0)
            {
                await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ברקוד לא יכול להיות ריק"));
                wf_set_focus(dw_body, ll_row.Value, "barcode");
                return false;
            }


            ll_row = dw_body.Find("IsNull(material_number) OR material_number == 0", 0, dw_body.RowCount());
            if (ll_row >= 0)
            {
                string ls_barcode = null;

                ls_barcode = dw_body.GetItemValue<string>(ll_row.Value, "barcode");
                if (!this.ViewModel.iuo_material_searchProperty.uf_material_exists(ls_barcode))
                {
                    await wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ברקוד ריק/לא קיים"));
                    wf_set_focus(dw_body, ll_row.Value, "barcode");
                    return false;
                }
            }


            ll_row = dw_body.Find("IsNull(expected_material_quantity)", 1, dw_body.RowCount());
            if (ll_row > 0)
            {
                //.. כמות בפעול לא יכולה להיות ריקה
                await wf_show_message("", "", "OK", "#20011");
                wf_set_focus(dw_body, ll_row.Value, "expected_material_quantity");
                return false;
            }


            ll_row = dw_body.Find("IsNull(material_quantity) OR material_quantity == 0", 0, dw_body.RowCount());
            if (ll_row >= 0)
            {
                //.. כמות בחשבונית לא יכולה להיות ריקה
                await wf_show_message("", "", "OK", "#20012");
                wf_set_focus(dw_body, ll_row.Value, "material_quantity");
                return false;
            }


            ll_row = dw_body.Find("NOT (IsNull(min_valid_month) OR min_valid_month == 0)", 0, dw_body.RowCount());
            if (ll_row >= 0)
            {
                DateTime? ldt_curr_expiration_date = default(DateTime);
                long ll_min_valid_month = 0;

                ll_min_valid_month = dw_body.GetItemValue<long>(ll_row.Value, "min_valid_month");

                ldt_curr_expiration_date = dw_body.GetItemValue<DateTime>((int)ll_row, "expiration_date");
                if (ldt_curr_expiration_date == default(DateTime))
                {
                    string ls_material_name = null;
                    string ls_mess = null;

                    ls_material_name = dw_body.GetItemValue<string>(ll_row.Value, "material_name");
                    ls_mess = masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("יש להזין תאריך תפוגה לפריט") + " - " + ls_material_name;
                    await wf_show_message("", "", "OK", ls_mess);
                    wf_set_focus(dw_body, ll_row.Value, "material_quantity");
                    return false;
                }
            }
            return true;
        }

        public async Task<int> dw_body_itemchanged(int row, ControlElement dwo, string data, EventArgs e) //--------------------------------------------------------------------
        {
            ButtonElement cb_save = this.GetVisualElementById<ButtonElement>("cb_save");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            //Function:			public w_mini_terminal_input.itemchanged()
            //
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            // 
            // Description:	
            // 					1) If material barcode was changed, check if material with that barcode exists
            //						if it exists fill all needed data by ue_new_material_number_inserted
            //						if material not exists fill row data with empty values
            //					2) If expected_material_quantity was changed then
            //						if ib_auto_current_qty is true sets its value in material_quantity
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------


            
            
            
            dw_body.itemchanged(0, default(ControlElement), default(string));
            long ll_supplier = 0;
            long ll_material_number = 0;
            long ll_actual_quantity = 0;
            string ls_barcode = null;
            string ls_message = null;
            int li_len = 0;
            int li_index = 0;
            IRepository lds_package;
            s_array_arguments lstr_mess = default(s_array_arguments);

            dw_body.PerformValidated(e);

            if (Convert.ToString(dwo.ID) == "barcode")
            {

                ls_barcode = dw_body.GetItemValue<string>(row, "barcode");
             
                ls_barcode = await this.ViewModel.iuo_material_searchProperty.uf_get_barcode_main(ls_barcode, false);
                // END
                // SharonS - 1.2.32.0 - 2008-07-08- CR#1095 - If barcode does not exist, drop leading zeros
                if (string.IsNullOrEmpty(ls_barcode))
                {
                    li_len = data.Length;
                    for (li_index = 1; li_index <= li_len; li_index++)
                    {
                        if (data.Substring(0, 1) == "0")
                        {
                            data = data.Substring(data.Length - (data.Length - 1));

                            li_index++;
                        }
                        else
                        {
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                }
                ls_barcode = data;
                ls_barcode =await this.ViewModel.iuo_material_searchProperty.uf_get_barcode_main(ls_barcode, false);
                // End
                // PninaSG - 1.2.23.0 - 2008-01-02- TASK#10000 - Start
                if (string.IsNullOrEmpty(ls_barcode))
                {
                    // SharonS - 1.2.42.1 - 2009-03-01 - Add supplier number to the select 
                    //		lstr_mess = f_is_pack_barcode(TRUE, data)

                    ll_supplier = dw_head.GetItemValue<long>(0, "supplier_number");
                    lstr_mess =await f_is_pack_barcodeClass.f_is_pack_barcode(true, data, ll_supplier);
                    // End
                    if (lstr_mess.arg_ok == false)
                    {
                        //Message
                       await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1,await f_get_error_message_numberClass.f_get_error_message_number(5092));
                    }
                    else
                    {
                        //Set item
                        lds_package = new d_packageRepository();
                        lds_package = lstr_mess.a_obj[1] as IRepository;
                        ls_barcode = lds_package.GetItem<d_package>(1).materials_barcode;
                        ll_actual_quantity = Convert.ToInt64(lds_package.GetItem<d_package>(1).cf_total_quantity);

                        dw_body.SetItem(row, "barcode", Convert.ToString(ls_barcode));

                        dw_body.SetItem(row, "expected_material_quantity", ll_actual_quantity.ToString());
                        if (this.ViewModel.ib_auto_current_qtyProperty)
                        {

                            dw_body.SetItem(row, "material_quantity", ll_actual_quantity.ToString());
                        }
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
                        if (lds_package != null)
                        {

               

                        }
                        // End
                    }
                }
                // END
                ll_material_number =await this.ViewModel.iuo_material_searchProperty.uf_get_material_number(ls_barcode);
                ls_message = "";
                if (!isempty_stringClass.isempty_string(ls_barcode))
                {
                    //.. oren 06/12/05 - check material status
                    if (! (await this.ViewModel.iuo_material_searchProperty.uf_material_status_allow_purches(ll_material_number,  ls_message)).Retrieve(out ls_message))
                    {
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, ls_message);

                        dw_body.SetItem(row, "barcode", "");

                        dw_body.SetColumn("barcode");

                        dw_body.SetRow(row);
                        dw_body.Focus();
                        return 2;
                    }
                    //
                    ls_message = "";
                    //.. oren - 06/12/05 - check last supplier for this material - ls_message get the message from the function  " X  החומר נקנה לאחרונה מספק

                    ll_supplier = dw_head.GetItemValue<long>(0, "supplier_number");
                    if (!(await this.ViewModel.iuo_material_searchProperty.uf_material_belong_to_current_supplier(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier, ll_material_number, ls_message)).Retrieve(out ls_message))
                    {
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, ls_message);
                    }
                    //
                    //.. if barcode exists and good for purchase fill all data (sell price, cost price, order information)
                    //
                    //.. if barcode exists and good for purchase fill all data (sell price, cost price, order information)
                    await this.dw_body_ue_new_material_number_inserted(ls_barcode, row,e);
                }
                else
                {
                    //.. if material with this barcode not exists fill empty values into this row

                    dw_body.SetItem(row, "material_number", "0");

                    dw_body.SetItem(row, "material_name", "");

                    dw_body.SetItem(row, "material_price", "0");

                    dw_body.SetItem(row, "min_valid_month", "0");

                    if (dw_body.GetItemValue<int>(row, "indicator") == 0)
                    {


                        dw_body.SetItem(row, "indicator", Convert.ToString(Convert.ToInt64(dw_body.Describe<long>("Evaluate('MAX(indicator for all)',1)")) + 1));
                    }
                }
            }

            else if (Convert.ToString(dwo.ID) == "expected_material_quantity")
            {
                if (this.ViewModel.ib_auto_current_qtyProperty)
                {
                    // oren 22/11/05 - fix - step over material_quantity on every change in  expected_material_quantity
                    //IF IsNull(this.GetItem<decimal>(row, "material_quantity")) THEN

                    dw_body.SetItem(row, "material_quantity", (Convert.ToDecimal(data)).ToString());
                    //END IF
                }
            }
            cb_save.Enabled = true;
            //*************************************************************************
            // oren 7/11/2005 - write to log

            ls_message = "*************************** Item Changed - dw_body*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + dwo.ID.ToString() + "\r" + "\n" + "Row:         " + row.ToString() + "\r" + "\n" + "The Data:  " + data + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            return 0;
        }

        public void dw_body_retrieveend(int l) //Long 		ll,		ll_material_number
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            
            dw_body.retrieveend(default(long));
            //String 	ls_material_name
            //
            //FOR ll=this.RowCount() TO 1 STEP -1
            //	ll_material_number = this.GetItem<int>(ll, "material_number")
            //	ls_material_name = iuo_material_search.uf_get_material_name(ll_material_number)
            //	this.SetItem(ll, "material_name", ls_material_name)
            //NEXT
        }

        public void dw_body_clicked(int xpos, int ypos, int row, ControlElement dwo) //
        {
        }

        public async Task dw_body_dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row) // oren 7/11/2005
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            await dw_body.dberror(0, default(string), default(string), default(object), 0);
            var ls_message = "******************************************************************" + "DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "SqlDbCode: " + sqldbcode.ToString() + "'. SqlErrorText: " + sqlerrtext + "'. SqlSyntax: " + sqlsyntax + "'." + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
        }

        public async Task dw_body_doubleclicked(int xpos, int ypos, int row, ControlElement dwo)
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");

            
            
            
            
            var e = new EventArgs();
            dw_body.PerformDoubleClick(e);//0, 0, 0, default(ControlElement));
            // SharonS - 0.0.0.1 - 2007-05-02 - CR#1027 - // Check row (Add the IF statement) - Start
            if (row > 1)
            {

                var ldt_curr_expiration_date = dw_body.GetItemValue<DateTime>(row, "expiration_date");

                if (Convert.ToString(dwo.ID) != "expected_material_quantity")
                {
                    return;
                }

                var ll_min_valid_month = dw_body.GetItemValue<long>(row, "min_valid_month");
                if (!(ll_min_valid_month == 0 || ll_min_valid_month == 0))
                {
                    await wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, row, false);
                }
            }
            // End
            //Long 	ll_min_valid_month
            //DateTime ldt_curr_expiration_date
            //
            //ldt_curr_expiration_date = dw_body.GetItem<DateTime>(al_row, "expiration_date")
            //ll_min_valid_month = this.GetItem<int>(al_row, "min_valid_month")
            //IF  NOT(IsNull(ll_min_valid_month) OR ll_min_valid_month = 0) AND IsNull(ldt_curr_expiration_date) THEN
            //	IF NOT wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, al_row, false) THEN
            //		Return False
            //	END IF
            //END IF
        }
        public async Task<long> dw_head_ue_invoice_pack_exists() //--------------------------------------------------------------------
        {
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Event:			public w_mini_terminal_input.ue_invoice_pack_exists()
            //
            // Returns:         Long
            //
            // Copyright  - Stas
            //
            // Date Created: 22/08/2005
            //
            // Description:	
            // 				check if invoice or packing_list already exists.
            //				return :
            //				a)  1   ---> if invoice/packing_list exists
            //				b) -1  --->  if db error occures
            //				c) -2  --->  if return-invoice exists
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_serial_number = 0;
            long ll_supplier_number = 0;
            long ll_temp = 0;
            long ll_return_flag = 0;
            ll_return_flag = 1;

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            if (rb_packing.IsChecked)
            {

                ll_serial_number = dw_head.GetItemValue<long>(0, "pack_list_number");
                if (true)
                {
                    ViewModel.LoadData2(ll_serial_number, ll_supplier_number, ref ll_temp);
                }
            }
            else
            {

                ll_serial_number = dw_head.GetItemValue<long>(0, "invoice_number");
                if (true)
                {
                    ViewModel.LoadData3(ll_serial_number, ll_supplier_number, ref ll_temp);
                }

                if (masofonAlias.masofon.Instance.sqlca.SqlCode != -1 && ll_temp != 1)
                {
                    ViewModel.LoadData4(ll_serial_number, ll_supplier_number, ref ll_temp);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != -1 && ll_temp == 1)
                    {

                        await wf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "OK", "#5309");
                        return -2;
                    }
                }
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await wf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
                ll_return_flag = -1;
            }
            else if (ll_temp == 1)
            {
                ll_return_flag = 1;
            }
            else if (ll_temp == 0)
            {
                ll_return_flag = 0;
            }
            return ll_return_flag;
        }
        public async Task<FuncResults<string, bool>> dw_head_ue_supplier_exists(long al_supplier_number, string al_supplier_name) //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_input.ue_supplier_exists()
            //
            // Returns:         boolean
            //
            // Parameters:      value Long al_supplier_number ---> suppliers number entered by user
            // 
            // Copyright  - Stas
            //
            // Date Created: 07/08/2005
            //
            // Description:	
            // 					checks if entered supplier number exists in DB
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_temp = 0;
            if (al_supplier_number == 0)
            {
                await wf_show_message("", "", "Ok", masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ספק") + " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אינו קיים"));
                return FuncResults.Return(al_supplier_name, false);
            }
            if (true)
            {
                ViewModel.LoadData5(al_supplier_number, ref ll_temp, ref al_supplier_name);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {

                await wf_show_message("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "Ok",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                return FuncResults.Return(al_supplier_name, false);
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
            {
                await wf_show_message("", "", "Ok", masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ספק") + " " + al_supplier_number.ToString() + " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אינו קיים"));
                return FuncResults.Return(al_supplier_name, false);
            }
            return FuncResults.Return(al_supplier_name, true);
        }
        public async Task<bool> dw_head_ue_log_book_exists()
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            bool lb_log_book_by_supplier = false;
            long ll_log_book = 0;
            long ll_supplier_number = 0;
            long ll_count = 0;
            lb_log_book_by_supplier = (masofonAlias.masofon.Instance.gs_vars.log_book_defined_by_supplier.ToLower(CultureInfo.InvariantCulture) == "yes");

            if (dw_head.RowCount() <= 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "חסר נתוני התחלתיים");
                return false;
            }

            ll_log_book = dw_head.GetItemValue<long>(0, "log_book");
            if (ll_log_book == 0 || ll_log_book == 0)
            {
                //.. please enter log number
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#20001");
                return false;
            }

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            //......invoice
            if (lb_log_book_by_supplier)
            {
                ViewModel.LoadData6(ll_log_book, ll_supplier_number, ref ll_count);
            }
            else
            {
                ViewModel.LoadData7(ll_log_book, ref ll_count);
            }
            if (ll_count > 0) // "Error", "Log book number already exists"
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#5212");

                dw_head.SetItem(0, "log_book", "");

                dw_head.SetColumn("log_book");
                dw_head.Focus();
                return false;
            }
            if (lb_log_book_by_supplier)
            {
                ViewModel.LoadData8(ll_log_book, ll_supplier_number, ref ll_count);
            }
            else
            {
                ViewModel.LoadData9(ll_log_book, ref ll_count);
            }
            if (ll_count > 0)
            {
                // "Error", "Log book number already exists"
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#5212");

                dw_head.SetItem(0, "log_book", "");

                dw_head.SetColumn("log_book");
                dw_head.Focus();
                return false;
            }
            return true;
        }
        public async Task<long> dw_head_ue_packing_list_item_changed() //--------------------------------------------------------------------
        {
            LabelElement total_quantity = this.GetVisualElementById<LabelElement>("total_quantity");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Event:			public w_mini_terminal_input.ue_packing_list_item_changed()
            //
            // Returns:         none
            //
            // Date Created: 15/09/2005
            // 
            // Description:	
            //	 					if user change packing number or supplier number
            //						checks if packing list already exists 
            //						a) if exists gets order number from dw_body to dw_head
            //						b) if not exists reset all data in both dw`s						
            //--------------------------------------------------------------------------------
            // Modifications:
            //  Date				Author			CR/Version	Comments
            // 11/04/2007		SharonS			1008/0.0.1	Event will return a value.
            //------------------------------------------------------------------------------

            long ll_pack_list_number = 0;
            long ll_pack_exists_flag = 0;
            long ll_temp = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;

            ll_pack_list_number = dw_head.GetItemValue<long>(0, "pack_list_number");

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            if (ll_supplier_number > 0 && ll_pack_list_number > 0)
            {
                //.. (a)  1 -> if exists;	(b) -1 -> db error occures;	(c) -2 -> exists but in RETURN-STATE
                ll_pack_exists_flag = await this.dw_head_ue_invoice_pack_exists();
                if (ll_pack_exists_flag > 0)
                {

                    dw_head.Retrieve(ll_pack_list_number, ll_supplier_number);

                    dw_body.Retrieve(ll_pack_list_number, ll_supplier_number);
                    // SharonS - 2007-04-10 - CR#1010 - Set the computed field material_name_com to
                    //			have an expression that will Align Hebrew (or mixed) text to the Right and English text to the Left

                    dw_body.Modify("material_name_com", "Alignment", "0");
                    dw_body.Modify("if(IsHebrew(material_name), 1, 0)'");
                    //
                    //.. order number saved at order_number column of pack_list_details table


                    if (dw_body.RowCount() > 0 && dw_head.RowCount() > 0)
                    {

                        ll_order_number = dw_body.GetItemValue<long>(0, "order_number");

                        dw_head.SetItem(0, "order_number", ll_order_number.ToString());
                        if (!(ll_order_number == 0 || ll_order_number == 0))
                        {

                            dw_head.Modify("order_number.Protect = 1");

                        }
                        dw_head.Modify("log_book.Protect = 1");

                        ll_temp = dw_body.GetItemValue<long>(0, "serial_number");

                        dw_head.SetItem(0, "packing_details_serial_number", ll_temp.ToString());
                        this.ViewModel.ib_new_inv_packProperty = false; //.. data retrieved from DB

                        if (dw_head.GetItemValue<string>(0, "state").ToLower(CultureInfo.InvariantCulture) == "c")
                        {
                            await wf_show_message("", "", "Ok", "שים לב, תעודה סגורה. אין אפשרות להוסיף פריטים");
                            dw_body.Enabled = false;
                        }

                        dw_head.ResetUpdate();

                        dw_body.ResetUpdate();
                    }
                    total_quantity.Text = wf_calc_total_quantity().ToString();
                }
                else
                {
                    string ls_supplier_name = null;

                    ls_supplier_name = dw_head.GetItemValue<string>(0, "supplier_name");

                    dw_body.Reset();
                    dw_head.ue_new_row();
                    //.. save only supplier and invoice number if it is new invoice

                    dw_head.SetItem(0, "supplier_number", ll_supplier_number.ToString());

                    dw_head.SetItem(0, "pack_list_number", ll_pack_list_number.ToString());

                    dw_head.SetItem(0, "supplier_name", Convert.ToString(ls_supplier_name));
                }
            }
            // SharonS - 0.0.0.1 - 2007-04-11 - CR#1008 - Return the Flag to the itemchanged event
            return ll_pack_exists_flag;
            //
        }
        public async Task<long> dw_head_ue_invoice_item_changed() //--------------------------------------------------------------------
        {
            LabelElement total_quantity = this.GetVisualElementById<LabelElement>("total_quantity");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Function:			public w_mini_terminal_input.ue_invoice_item_changed()
            //
            // Returns:         none
            //
            // Copyright  - Stas
            //
            // Date Created: 15/09/2005
            // 
            // Description:	
            //	 					if user change invoice number or supplier number
            //						checks if invoice already exists 
            //						a) if exists gets order number from dw_body to dw_head
            //						b) if not exists reset all data in both dw`s						
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date				Author				CR/Version	Comments
            // 11/04/2007		SharonS				1008/0.0.1	Event will return a value.
            //------------------------------------------------------------------------------

            long ll_invoice_number = 0;
            long ll_supplier_number = 0;
            long ll_invoice_exists_flag = 0;
            long ll_order_num = 0;

            ll_invoice_number = dw_head.GetItemValue<long>(0, "invoice_number");

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            if (ll_supplier_number > 0 && ll_invoice_number > 0)
            {
                //.. (a)  1 -> if exists;	(b) -1 -> db error occures;	(c) -2 -> exists but in RETURN-STATE
                ll_invoice_exists_flag =await this.dw_head_ue_invoice_pack_exists();
                if (ll_invoice_exists_flag > 0)
                {

                    dw_head.Retrieve(ll_invoice_number, ll_supplier_number);

                    dw_body.Retrieve(ll_invoice_number, ll_supplier_number);
                    // SharonS - 2007-04-10 - CR#1010 - Set the computed field material_name_com to
                    //			have an expression that will Align Hebrew (or mixed) text to the Right and English text to the Left

                    dw_body.Modify("material_name_com", "Alignment", "0");
                    dw_body.Modify("if(IsHebrew(material_name), 1, 0)'");
                    //
                    //.. order number saved at packing_list_number column of invoice_details table


                    if (dw_body.RowCount() > 0 && dw_head.RowCount() > 0)
                    {

                        ll_order_num = dw_body.GetItemValue<long>(0, "packing_list_number");

                        dw_head.SetItem(0, "order_number", ll_order_num.ToString());
                        //.. cannot change order number if it old invoice
                        if (!(ll_order_num == 0 || ll_order_num == 0))
                        {


                            dw_head.Modify("order_number.Protect = 1");
                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_num);
                        }
                        dw_head.Modify("log_book.Protect = 1");
                        this.ViewModel.ib_new_inv_packProperty = false; //.. data retrieved from DB

                        if (dw_head.GetItemValue<string>(0, "state").ToLower(CultureInfo.InvariantCulture) == "c")
                        {
                            await wf_show_message("", "", "Ok", "שים לב, חשבונית סגורה. אין אפשרות להוסיף פריטים");
                            dw_body.Modify("barcode.Protect = 1");
                            dw_body.Modify("expected_material_quantity.Protect = 1");
                            dw_body.Modify("material_quantity.Protect = 1");
                        }

                        dw_head.ResetUpdate();

                        dw_body.ResetUpdate();
                    }
                    total_quantity.Text = wf_calc_total_quantity().ToString();
                }
                else
                {
                    string ls_supplier_name = null;

                    ls_supplier_name = dw_head.GetItemValue<string>(0, "supplier_name");

                    dw_body.Reset();
                    dw_head.ue_new_row();
                    //.. save only supplier and invoice number if it is new invoice

                    dw_head.SetItem(0, "supplier_number", ll_supplier_number.ToString());
                    // SharonS - 0.0.0.1 - 2007-03-22 - CR#1008 - Set 0 in the invoice_number field
                    if (ll_invoice_exists_flag == -2)
                    {

                        dw_head.SetItem(0, "invoice_number", "0");
                        //
                    }
                    else
                    {

                        dw_head.SetItem(0, "invoice_number", ll_invoice_number.ToString());
                    }

                    dw_head.SetItem(0, "supplier_name", Convert.ToString(ls_supplier_name));
                }
            }
            // SharonS - 0.0.0.1 - 2007-04-11 - CR#1008 - Return the Flag to the itemchanged event
            return ll_invoice_exists_flag;
            //
        }

        public void dw_head_enterkey() //IF  this.getObjectatpointer( ) = "suppier_name" THEN dw_head.object.supplier_name.DDDW.ShowList ='No'
        {
            //F_DW_ENTER_KEY_MOVE(dw_head)
        }

        public void dw_head_constructor(object sender, EventArgs e)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //dw_head.constructor();
            dw_head.ue_new_row();
        }
        public void dw_head_ue_new_row()
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            dw_head.ue_new_row();
            long ll_row = 0;
            string C_S_OPEN_STATE = "O";

            dw_head.Reset();

            ll_row = dw_head.Insert(0);

            dw_head.SetItem(ll_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());

            dw_head.SetItem(ll_row, "employee_number", this.ViewModel.il_employee_numberProperty.ToString());

            dw_head.SetItem(ll_row, "station_number", masofonAlias.masofon.Instance.gs_station.station_number.ToString());

            dw_head.SetItem(ll_row, "date_move", DateTime.Now);

            dw_head.SetItem(ll_row, "supply_date", DateTime.Now);

            dw_head.SetItem(ll_row, "state", Convert.ToString(C_S_OPEN_STATE));

            dw_head.SetRow((int)ll_row);

            dw_head.SetColumn("supplier_number");
            dw_head.Focus();
            dw_head.SelectText(1, 20);

            dw_head.Modify("order_number.Protect = 0");
            dw_head.Modify("log_book.Protect = 0");
            this.ViewModel.ib_new_inv_packProperty = true;
            //.. SPR = P0000092 ..............

            dw_head.Modify("DataWindow", "NoUserPrompt", "Yes");
            //................................
        }
        public async Task<bool> dw_head_ue_check_validation(EventArgs e)
        {
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            long ll_invoice_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_error_number = 0;
            DateTime? ldt_supply_date = default(DateTime);
            decimal ldec_total = default(decimal);
            string ls_supplier_name = null;

            if (dw_head.RowCount() == 0)
            {
                return false;
            }

            dw_head.PerformValidated(e);
            if (rb_packing.IsChecked)
            {

                ll_invoice_number = dw_head.GetItemValue<long>(0, "pack_list_number");
            }
            else
            {

                ll_invoice_number = dw_head.GetItemValue<long>(0, "invoice_number");
            }

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");

            ldt_supply_date = dw_head.GetItemValue<DateTime>(0, "supply_date");

            ll_order_number = dw_head.GetItemValue<long>(0, "order_number");

            ldec_total = dw_head.GetItemValue<decimal>(0, "total");
            if (!(await this.dw_head_ue_supplier_exists(ll_supplier_number, ls_supplier_name)).Retrieve(out ls_supplier_name))
            {

                wf_set_focus(dw_head, dw_head.GetRow(), "supplier_number");
                return false;
            }
            if (ll_invoice_number == 0 || ll_invoice_number == 0)
            {
                //.. SPR = P00000158 .... 
                if (rb_packing.IsChecked)
                {
                    //.. Packing number cannot be empty
                    await wf_show_message("", "", "OK", "#20013");

                    wf_set_focus(dw_head, dw_head.GetRow(), "pack_list_number");
                }
                else
                {
                    //.. Invoice number cannot be empty
                    await wf_show_message("", "", "OK", "#20014");

                    wf_set_focus(dw_head, dw_head.GetRow(), "invoice_number");
                }
                //....
                return false;
            }
            if (ll_supplier_number == 0 || ll_supplier_number == 0)
            {
                await wf_show_message("", "", "OK",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("מס  ספק לא יכול להיות ריק"));

                wf_set_focus(dw_head, dw_head.GetRow(), "supplier_number");
                return false;
            }

            var isDate = SystemFunctionsExtensions.IsDate(ldt_supply_date.Value.ToString("yyyy-MM-dd"));
            if (ll_supplier_number == 0 || isDate != null && !isDate.Value)
            {
                await wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תאריך אספקה לא יכול להיות ריק"));

                wf_set_focus(dw_head, dw_head.GetRow(), "supply_date");
                return false;
            }
            //.. cant continue if same log number already exists in DB
            //.. check this value only if it is new data, (not recieved from db)
            if (this.ViewModel.ib_new_inv_packProperty)
            {
                if (! await this.dw_head_ue_log_book_exists())
                {

                    wf_set_focus(dw_head, dw_head.GetRow(), "log_book");
                    return false;
                }
            }
            //.. check if order number exists
            //.. check this value only if it is new data, (not recieved from db)
            if (this.ViewModel.ib_new_inv_packProperty)
            {
                if (!(ll_order_number == 0 || ll_order_number == 0))
                {
                    if (!(await this.ViewModel.iuo_order_searchProperty.uf_order_for_purchase_exists(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_order_number, ll_error_number)).Retrieve(out ll_error_number))
                    {

                        wf_set_focus(dw_head, dw_head.GetRow(), "order_number");
                        return false;
                    }
                }
            }
            //.. SPR = P0000237 ..... check total value only if it invoice input ...
            if (ldec_total == 0 || ldec_total == 0)
            {
                if (rb_packing.IsChecked)
                {


                    dw_head.SetItem(dw_head.GetRow(), "total", "0");
                }
                else
                {
                    await wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("סה''כ ח-ן לא יכול להיות ריק"));

                    wf_set_focus(dw_head, dw_head.GetRow(), "total");
                    return false;
                }
            }
            //...
            return true;
        }
        public void dw_head_ue_enter(object sender, GridElementCellEventArgs e)
        {
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            if (dw_head.GetObjectAtPointer() == "suppier_name")
            {

                // Code was removed based on 'DWDddwColumn.Dddw : DWDddwStyle' mappings.
                // mini_terminal.Extensions.BundleResolutions.get_supplier_name(dw_head.GetObject()).Dddw.ShowList = "No"

            }
            object keyflags = null;
            //F_DW_ENTER_KEY_MOVE(dw_head)

            if (Convert.ToInt32(keyflags) == 0 && e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                //.. if dw has rows

                if (dw_head.RowCount() == 1)
                {

                    if (dw_head.GetColumnName() == "total")
                    {
                        if (dw_head.ue_check_validation())
                        {

                            if (dw_body.RowCount() == 0)
                            {
                                dw_body.ue_new_row();

                                dw_head.ue_enter(this, e);
                            }
                            else
                            {

                                dw_body.SetRow(1);

                                dw_head.ue_enter(this, e);
                            }
                        }
                    }
                    else
                    {

                        dw_head.ue_enter(this, e);
                    }
                }
            }
        }

        public async Task dw_head_itemchanged(int row, dynamic dwo, string data, EventArgs e) //--------------------------------------------------------------------
        {
            RadioButtonElement rb_packing = this.GetVisualElementById<RadioButtonElement>("rb_packing");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            ButtonElement cb_save = this.GetVisualElementById<ButtonElement>("cb_save");
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            //Event:			public w_mini_terminal_input.itemchanged()
            //
            // Returns:         long
            //
            // Copyright  - Stas
            //
            // Date Created: 28/08/2005
            //
            // Description:	
            // 					on some item change, checks its value.
            // 					a) supplier-number, checks if supplier exists. 
            //					 	 if yes gets supplier name
            //						 if no show error message
            //					b) order-number, checks if order number exists at supplier_order_move
            //					    if no show error message
            //					c) log-book. checks if same log book exists return from event
            //					d) if supplier-number and invoice/packing-list number entered,
            //						checks if invoice/packing list exists.
            //						if exists so retrieve appropriate data from DB
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------


            
            
            
            dw_head.itemchanged(0, default(ControlElement), default(string));
            long ll_supplier_number = 0;
            long ll_temp = 0;
            long ll_order_number = 0;
            long ll_error_number = 0;
            long ll_null = 0;
            long ll_invoice_exists_flag = 0;
            long ll_ret = 0;
            DateTime? ldt_date_null = default(DateTime);
            string ls_column = null;
            string ls_supplier_name = null;
            string ls_null = null;
            string ls_message = null;
            decimal ldec_discount = default(decimal);
            GridElement ldwc_supplier_name;

            dw_head.PerformValidated(e);
            ls_column = Convert.ToString(dwo.ID);
            //************************************************************
            // oren 7/11/2005 - write to log
            ls_message = "*************************** Item Changed - dw_head*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + dwo.ID.ToString() + "\r" + "\n" + "Row:             " + row.ToString() + "\r" + "\n" + "The Data:  " + data + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //************************************************************

            ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
            switch (ls_column)
            {
                case "supply_date":
                    cb_save.Enabled = true;
                    return;
                case "total":
                    cb_save.Enabled = true;

                    dw_head.SetItem(row, "expected_total_amount", (Convert.ToDecimal(data)).ToString());
                    return;
                case "supplier_number":
                    //.. checks if supplier exists in DB
                    if (!(await this.dw_head_ue_supplier_exists(ll_supplier_number, ls_supplier_name)).Retrieve(out ls_supplier_name))
                    {

                        dw_head.SetItem(row, "supplier_name", "");
                        wf_set_focus(dw_head, row, "supplier_number");
                        return;
                    }
                    else
                    {
                        if (!isempty_stringClass.isempty_string(ls_supplier_name))
                        {

                            dw_head.SetItem(row, "supplier_name", Convert.ToString(ls_supplier_name));
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                        }
                    }
                    break;
                case "supplier_name":

                    ldwc_supplier_name = dw_head.GetVisualElementById<GridElement>("supplier_name");

                    ls_supplier_name = dw_head.GetItemValue<string>(row, "supplier_name");
                    if (!isempty_stringClass.isempty_string(ls_supplier_name))
                    {


                        ll_supplier_number = ldwc_supplier_name.GetItemValue<long>(ldwc_supplier_name.GetRow(), "number");


                        ls_supplier_name = ldwc_supplier_name.GetItemValue<string>(ldwc_supplier_name.GetRow(), "name");
                        if (ll_supplier_number > 0)
                        {

                            dw_head.SetItem(0, "supplier_number", ll_supplier_number.ToString());

                            dw_head.SetItem(0, "supplier_name", Convert.ToString(ls_supplier_name));
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(ll_supplier_number);
                        }
                        else
                        {

                            dw_head.SetItem(row, "supplier_name", "");

                            dw_head.SetItem(row, "supplier_number", "");
                            this.ViewModel.iuo_order_searchProperty.uf_set_supplier_number(0);
                            wf_set_focus(dw_head, row, "supplier_number");
                            return;
                        }
                    }

                    // Code was removed based on 'DWColumn.Dddw : DWDddwStyle' mappings.
                    // DirectCast(dwo, System.Web.VisualTree.Elements.DataViewTextField).Dddw.ShowList = "No"

                    break;
                case "order_number":
                    //.. checks if order exists in DB

                    ll_order_number = dw_head.GetItemValue<long>(row, "order_number");
                    if (!(ll_order_number == 0 || ll_order_number == 0))
                    {
                        if (!(await this.ViewModel.iuo_order_searchProperty.uf_order_for_purchase_exists(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_order_number, ll_error_number)).Retrieve(out ll_error_number))
                        {

                            dw_head.SetItem(row, "order_number", "0");

                            dw_head.SetItem(0, "discount_percent", "0");
                            wf_set_focus(dw_head, row, "order_number");
                            return;
                        }
                        else
                        {
                            this.ViewModel.iuo_order_searchProperty.uf_set_order_number(ll_order_number);
                            ldec_discount = this.ViewModel.iuo_order_searchProperty.uf_get_order_discount();

                            dw_head.SetItem(0, "discount_percent", ldec_discount.ToString());
                            // SharonS - 1.2.31.0 - 2008-04-21 - CR#1075 - Get order payment agreement
                            long ll_pay_agr = 0;
                            long ll_num_of_pays = 0;
                            if (this.ViewModel.iuo_order_searchProperty.uf_get_order_pay_agr(ll_pay_agr, ll_num_of_pays))
                            {

                                dw_head.SetItem(0, "payment_code_number", ll_pay_agr.ToString());

                                dw_head.SetItem(0, "payments_number", ll_num_of_pays.ToString());
                            }
                            // End				

                            if (dw_body.RowCount() > 0)
                            {
                                await this.dw_body_ue_load_values_after_order_change();
                            }
                            cb_save.Enabled = true;
                            return;
                        }
                    }
                    else
                    {

                        if (dw_body.RowCount() > 0)
                        {
                            await this.dw_body_ue_load_values_after_order_change();
                        }
                        cb_save.Enabled = true;
                        return;
                    }
                    break;
                case "log_book":
                    //.. SPR = P0000094 ........................
                    //dw_head.TriggerEvent("ue_log_book_exists");
                    await this.dw_head_ue_log_book_exists();
                    return;
                    //..........................................
            }
            //.. SPR = P00000089 .............................	
            if (ll_supplier_number > 0)
            {
                if (ls_column == "supplier_number" || ls_column == "supplier_name")
                {
                    GridElement ldwc_order;

                    ldwc_order = dw_head.GetVisualElementById<GridElement>("order_number");



                    ldwc_order.Retrieve(ll_supplier_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
                }
            }
            //.. if supplier-number of invoice/pack number was changed retrieve or create new invoice/pack
            if (rb_packing.IsChecked)
            {

                if (dw_head.GetItemValue<int>(0, "pack_list_number") > 0)
                {
                    dw_body.Modify("barcode.Protect = 0");
                    dw_body.Modify("expected_material_quantity.Protect = 0");
                    dw_body.Modify("material_quantity.Protect = 0");
                    ll_ret =await this.dw_head_ue_packing_list_item_changed();
                    if (ll_ret < 0)
                    {

                        dw_head.SetColumn("pack_list_number");

                        //GalilCS - TODO - Reject the data value but allow the focus to change
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                        //return 2; 
                    }
                }
            }
            else
            {

                if (dw_head.GetItemValue<int>(0, "invoice_number") > 0)
                {
                    dw_body.Modify("barcode.Protect = 0");
                    dw_body.Modify("expected_material_quantity.Protect = 0");
                    dw_body.Modify("material_quantity.Protect = 0");
                    ll_ret =await  this.dw_head_ue_invoice_item_changed();
                    if (ll_ret < 0)
                    {

                        dw_head.SetColumn("invoice_number");

                        //GalilCS - TODO - Reject the data value but allow the focus to change
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                        //return 2;
                    }
                }
            }
        }

        public async Task dw_head_retrieveend(int l)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");

            dw_head.retrieveend(default(long));
            long ll_supplier_number = 0;
            string ls_supplier_name = null;

            if (dw_head.RowCount() == 1)
            {

                ll_supplier_number = dw_head.GetItemValue<long>(0, "supplier_number");
                ls_supplier_name = await this.ViewModel.iuo_material_searchProperty.uf_get_supplier_name(ll_supplier_number);
                if (!isempty_stringClass.isempty_string(ls_supplier_name))
                {

                    dw_head.SetItem(0, "supplier_name", Convert.ToString(ls_supplier_name));
                }
            }
        }

        public void dw_head_editchanged(int row, dynamic dwo, string data)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");

            
            
            
           // dw_head.editchanged(0, default(ControlElement), default(string));
            if (dwo.ID == "supplier_name")
            {
                if (data.IndexOf("'") > 0)
                {

                    dw_head.SetItem(0, "supplier_name", "");
                    //GalilCS - TODO - Reject the data value but allow the focus to change
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                    //return 2;
                }


                if (data.Length == 1)
                {

                    // Code was removed based on 'DWColumn.Dddw : DWDddwStyle' mappings.
                    // DirectCast(dwo, System.Web.VisualTree.Elements.DataViewTextField).Dddw.ShowList = "Yes"


                    dw_head.SelectText(2, 99);
                }
            }
            //IF string(dwo.ID) = "supplier_number" THEN
            //	IF not isNumber(data) THEN
            //		this.SelectText(1, 99)
            //		this.Clear()
            //	ELSE
            //		IF len(data) = 1 THEN 
            //			//dwo.DDDW.ShowList ='Yes'
            //			this.SelectText(2, 99)
            //		END IF
            //	END IF
            //END IF
            //


        }

        public int dw_head_itemerror(int row, ControlElement dwo, string data)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");




            dw_head.ItemError(row, dwo, data);
            return 1;
        }

        public void cbx_query_clicked(object sender, EventArgs e)
        {
            u_dw_update dw_head = this.GetVisualElementById<u_dw_update>("dw_head");
            u_dw_update dw_body = this.GetVisualElementById<u_dw_update>("dw_body");
            u_checkbox_query cbx_query = this.GetVisualElementById<u_checkbox_query>("cbx_query");
            // TODO: PB Clicked Event: Occurs when the user clicks on the control.
            //cbx_query.clicked();
            cbx_query.PerformClick(e);
            dw_body.uf_set_sql_error_flag(cbx_query.IsChecked);
            
            //TODO: The method 'uf_set_sql_preview_flag' is missing in u_dw_update class
            //dw_body.uf_set_sql_preview_flag(cbx_query.IsChecked);
            dw_head.uf_set_sql_error_flag(cbx_query.IsChecked);

            //TODO: The method 'uf_set_sql_preview_flag' is missing in u_dw_update class
            //dw_head.uf_set_sql_preview_flag(cbx_query.IsChecked);
        }
    }
}
