using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_driver_report_excessController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_driver_report_excess()
		{
			d_mini_terminal_driver_report_excessRepository repository = new d_mini_terminal_driver_report_excessRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
