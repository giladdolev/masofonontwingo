using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.MiniTerminal.Controllers
{
	public class d_mini_terminal_crossdoc_returns_headerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_mini_terminal_crossdoc_returns_header()
		{
			d_mini_terminal_crossdoc_returns_headerRepository repository = new d_mini_terminal_crossdoc_returns_headerRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
