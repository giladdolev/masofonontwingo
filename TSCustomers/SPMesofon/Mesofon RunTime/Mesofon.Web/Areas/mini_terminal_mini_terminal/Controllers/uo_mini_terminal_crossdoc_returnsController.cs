﻿using System;
using System.DataAccess;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using global;
using masofon;
using System.Web.VisualTree;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using masofonAlias = masofon;
using mini_terminal;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.MiniTerminal.Controllers;
using Mesofon.Models;


namespace Mesofon.Controllers
{
	public class uo_mini_terminal_crossdoc_returnsController : uo_mini_terminal_base_bodyController
	{

		public ActionResult uo_mini_terminal_crossdoc_returns()
		{
			return this.View(new uo_mini_terminal_crossdoc_returns());
		}
		private uo_mini_terminal_crossdoc_returns ViewModel
		{
			get { return this.GetRootVisualElement() as uo_mini_terminal_crossdoc_returns; }
		}
	    public void Form_Load(object sender, EventArgs e)
	    {
	        //this.ViewModel.constructor();
	    }

        public bool uf_update_item(long al_row_no) //String	ls_barcode, ls_carton_number
		{
			
			return true;
		}
        public async Task<bool> uf_set_material_prices(long al_material_number, long row, EventArgs e)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
            decimal vat_percentage = default(decimal);
            decimal ldec_active_price = default(decimal);
            decimal ld_global_discount_percent = default(decimal);
            decimal ldec_material_discount = default(decimal);
            decimal ldec_supplier_discount = default(decimal);
            decimal ldec_percent = default(decimal);
            decimal ldec_price_before = default(decimal);
            decimal ldec_price_after = default(decimal);
            decimal ldec_sell_price = default(decimal);
            long[] la_suppliers = new long[6];
            long ll_index = 0;
            long ll_first_free = 0;
            long vat_number = 0;
            string ls_syntax = null;
            string ls_material_name = null; //, ls_discount_amount
            bool lb_ok = false;

            dw_inv_pack_details.PerformValidated(e);
            //
            //ls_discount_amount = f_RegistryGet(s_reg_key + "Office\Administration", "discount_amount_in_purchase", "no", "if yes => can update a total sum of the invoice")
            //IF Lower(ls_discount_amount) = "yes" THEN
            //	ib_discount_amount = True
            //END IF
            //
            if (true)
            {
                ViewModel.LoadData1(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
                return false;
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100) // get the last prices received 
            {
                ViewModel.LoadData2(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_set_material_prices - Sql Error");
                    return false;
                }
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                ldec_price_before = 0;
                ldec_price_after = 0;
                ldec_active_price = 0;
            }
            else
            {
                if (ldec_price_before == 0)
                {
                    ldec_price_before = 0;
                }
                if (ldec_price_after == 0)
                {
                    ldec_price_after = 0;
                }
                if (ldec_material_discount == 0)
                {
                    ldec_material_discount = 0;
                }
                if (ldec_supplier_discount == 0)
                {
                    ldec_supplier_discount = 0;
                }
                if (ldec_price_after == 0 && (ldec_price_before != 0 && (ldec_material_discount == 0 && ldec_supplier_discount == 0)))
                {
                    ldec_price_after = ldec_price_before;
                }
                ldec_active_price = ldec_price_after;
            }
                     dw_inv_pack_details.SetItem(row, "material_price", ldec_active_price.ToString());
           
            return true;
        }
        public bool uf_delete_item(long al_row) //String	ls_barcode
		{
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			//Boolean	lb_return_marlog
			decimal ldc_qty = default(decimal);
			decimal ldc_total_qty = default(decimal);
			long ll_supplier = 0;
			long ll_i = 0;
			long? ll_find_summary = 0;
			long ll_supplier_1 = 0;
			//basic data
			
			ll_supplier = dw_inv_pack_details.GetItemValue<long>(al_row, "supplier_number");
			if (this.ViewModel.ib_marlog_returnProperty)
			{
				ldc_total_qty = 0;
				//find dw_summary row for supplier - re-calc row
				
				
				ll_find_summary = dw_summary.Find("invoice_move_supplier_number == \"" + ll_supplier.ToString() + "\"", 1, dw_summary.RowCount());
				
				for (ll_i = 1; ll_i <= dw_inv_pack_details.RowCount(); ll_i++)
				{
					if (ll_i == al_row) //skip deleted row
					{
						continue;
					}
					
					ll_supplier_1 = dw_inv_pack_details.GetItemValue<long>(ll_i, "supplier_number");
					if (ll_supplier_1 == ll_supplier) //add current supplier quantities
					{
						
						ldc_total_qty += dw_inv_pack_details.GetItemValue<decimal>(ll_i, "material_quantity");
					}
				}
				//re-set total for summary 	
				
				dw_summary.SetItem(ll_find_summary.Value, "total", ldc_total_qty.ToString());
			}
			else
			{
			}
			//uf_update_tables( al_row, 0, 4)
			//actual deletion of the row in the calling function
			return true;
		}
		public bool uf_check_marlog_distributer(long al_distributor_number)
		{
			bool lb_marlog = false;

            lb_marlog = Convert.ToBoolean(w_mini_terminal.Instance().wf_check_marlog_distributor(-2, "MARLOG_YES", al_distributor_number));
			return lb_marlog;
		}
		public bool uf_reset()
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
			long ll_row = 0;
			long ll_null = 0;
			GridElement dwc_shipments;

		    ll_null = default(long); //Ron@06/08/2015
			//ll_row = dw_return_num.GetChild( "shipment_number", dwc_shipments)
			//dwc_shipments.SetTransObject(SQLCA)
			//ll_row = dwc_shipments.Retrieve(0,0,0)
			//dw_return_num.Insert(0)
			//dw_return_num.reset()
			//dw_1.reset()
			
			dw_summary.Reset();
			
			dw_cartons.Reset();
			 
			dw_return_num.Clear(); //Ron@06/08/2015
			
			dw_return_num.SetItem(0, "shipment_number", ll_null.ToString()); //Ron@06/08/2015
			this.ViewModel.il_return_number = 0;
			this.ViewModel.il_return_sonProperty = 0; //Ron@06/08/2015>>SPUCM00005419
			this.ViewModel.is_carton_barcode = "";
			return true;
		}
		public async Task<int> uf_close_window() //********************************************************************
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			//*Object:				uo_mini_terminal_returns
			//*Function Name:	uf_close_window
			//*Purpose: 			close window
			//*Arguments: 		none
			//*Return:				None
			//*Date				Programer		Version	Task#	 		Description
			//*---------------------------------------------------------------------
			//*27-06-2013		AlexKh			1.1.1.10	CR#1160		Initial version
			//********************************************************************
			
			string ls_message = null;
			int li_rtn_code = 0;
			ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
			li_rtn_code = this.ViewModel.uf_write_log(Convert.ToString(ls_message));
			uf_reset();
			
			dw_1.Reset();
			
			dw_return_num.Reset();
			await this.ViewModel.uf_display("ENTRANCE");
			return 1;
		}
		public async Task<int> uf_enter(string as_column, long al_row)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			long ll_doc_no = 0;
			long ll_doc_no_en = 0;
			long ll_row_no = 0;
			long ll_cntr = 0;
			long ll_serial_number = 0;
			long ll_RowCount = 0;
			long ll_row = 0;
			long ll_material_number = 0;
			long ll_material_quantity = 0;
			string ls_barcode = null;
			string ls_b2b_status = null;
			string ls_string = null;
		    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Start. Column: " + as_column), Convert.ToString("Row:      " + al_row.ToString()), "");
			ModelAction ldis_status = default(ModelAction);
			
			
			ldis_status = dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
			f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_inv_pack_details);
			
			if (dw_inv_pack_details.RowCount() < 1)
			{
				return 0;
			}
			switch (as_column) {
				case "barcode":
					ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
				    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Barcode:  " + ls_barcode), "");
					if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
					{
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns", "dw_inv_pack_details", al_row, "barcode");
					}
					else
					{
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns", "dw_inv_pack_details", al_row, "material_quantity");
					}
					break;
				case "material_quantity":
					
					ll_row_no = dw_inv_pack_details.GetItemValue<long>(al_row, "row_no");
				    this.ViewModel.uf_prepare_log(Convert.ToString(this.ViewModel.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Row no:   " + ll_row_no.ToString()), "");
					// SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - Check if the quantity is null
					
					ll_material_quantity = dw_inv_pack_details.GetItemValue<long>(al_row, "material_quantity");
					if (ll_material_quantity == 0 || ll_material_quantity == 0)
					{
                        //.. כמות בפעול לא יכולה להיות ריקה
                        await this.ViewModel.uf_show_message("", "", "OK", "#20011");
						this.ViewModel.uf_set_focus("uo_mini_terminal_returns", "dw_inv_pack_details", al_row, "material_quantity");
						return 0;
					}
					// End
					// SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - If at least 1 doc is not b2b, set focus to the doc_no field
					
					ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row, "material_number");
					
					ll_RowCount = dw_inv_pack_details.RowCount();
					this.ViewModel.uf_set_focus("uo_mini_terminal_returns", "dw_inv_pack_details", al_row, "declines");
					return 1;
				
				default:
					break;
			}
			return 1;
		}
		public async Task<bool> uf_b2b_send_return() //**********************************************************************************************
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			//*Object:				uo_mini_terminal_returns
			//*Function Name:	uf_b2b_send_return
			//*Purpose: 			Create the return xml (INVDBT) and send it to StoreNext
			//*						This will be done by calling the following functions in nvuo_b2b_rerturn_inv:
			//*						1. uf_set_message_type() - Set message type Debit (not Confirm or Return)
			//*						2. uf_init() - Check if the file target directory (outgoing messages) exist and create the file name
			//*						3. uf_send_transaction() - Create the B2B message XML file.
			//*						4. uf_update_outgoing() - Insert a new row into b2b_outgoing_messages table
			//*Arguments: 		String - as_sup_edi_no - Supplier EDI number
			//*Return:				Boolean - TRUE - Success to create the debit message (INVDBT)
			//*									 FALSE - Failure in create the debit message (INVDBT)
			//*Date					Programer		Version	Task#			Description
			//*-------------------------------------------------------------------------------------------------
			//*30-07-2013			AlexKh			1.1.1.10	CR#1160		Send return message to marlog
			//************************************************************************************************
			
			string ls_msg_type = null;
			string ls_message = null;
			string ls_text = null;
			string ls_sup_edi_no = null;
			string ls_marlog_number = null;
			string ls_filter = null;
			string ls_temp_xml1 = null;
			string ls_temp_xml2 = null;
			int li_ret_inv_sts = 0;
			int li_ret = 0;
			long ll_order_no = 0;
			long ll_ret_inv_no = 0;
			long ll_items = 0;
			long ll_material_number = 0;
			long ll_ret1 = 0;
			long ll_ret2 = 0;
			DateTime? ldt_last_update_datetime = null;
            IRepository lds_ret_inv_header;
            IRepository lds_ret_inv_header2;
            IRepository lds_ret_inv_body;
            IRepository lds_ret_inv_body2 = null;
			IRepository[] lds_debit_data = null;
			long ll_retrieve_header_row_count = 0;
			long ll_retrieve_body_row_count = 0;
			long ll_shipment_number = 0;
			long ll_supplier = 0;
			long ll_marlog_distributor_for_xml = 0;
			int li_rc = 0;
			decimal ldec_quantity = default(decimal);
			// Set message type to be a Return message (not Confirm or Debit) 
			if (!this.ViewModel.invuo_b2b_debit_marlogProperty.uf_set_message_type("RETURN"))
			{
				return false;
			}
			// Initiate invuo_b2b_debit
			ls_msg_type = "INVDBT";
			
			ll_shipment_number = dw_return_num.GetItemValue<long>(0, "shipment_number");
			ll_ret_inv_no = ll_shipment_number;
			// AlexKh - 1.1.1.18 - 2014-02-18 - CR#1171(SPUCM00004718) - set marlog distributor
			//IF NOT invuo_b2b_debit_marlog.uf_init(ls_msg_type, 99, ll_ret_inv_no, gs_vars.branch_number) THEN RETURN FALSE
			//RonY@12/04/2015 -  SPUCM00005270 - in the xml of crossdoc the SupplerNo must be the real suppler of the item
			if (true)
			{
			    ViewModel.LoadData3(ref ls_sup_edi_no);
			}
			//RonY@12/04/2015 -  SPUCM00005270
			this.ViewModel.invuo_b2b_debit_marlogProperty.uf_set_as_crossdoc_return(ls_sup_edi_no); //RonY@15/01/2015 -  SPUCM00005162
			if (!this.ViewModel.invuo_b2b_debit_marlogProperty.uf_init(Convert.ToString(ls_msg_type), this.ViewModel.il_marlog_distributorProperty, ll_ret_inv_no, masofonAlias.masofon.Instance.gs_vars.branch_number))
			{
				return false;
			}
			//Eitan; SPUCM00005489; 7/9/2016; 1.39.6;
			ls_temp_xml1 = this.ViewModel.invuo_b2b_debit_marlogProperty.is_file_spec;
			this.ViewModel.invuo_b2b_debit_marlogProperty.is_file_spec = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder_temp + this.ViewModel.invuo_b2b_debit_marlogProperty.is_file_name;
            // Create DataStores







            lds_ret_inv_header = new dwh_invoice_marlogRepository();// VisualElementHelper.CreateFromView<GridElement>("dwh_invoice_marlog", "dwh_invoice_marlog");
                                                                    //Ron@10/08/2015>>SPUCM00005419 - add and remark - start

            lds_ret_inv_body = new dwh_return_details_per_cartonRepository();//VisualElementHelper.CreateFromView<GridElement>("dwh_return_details_per_carton", "dwh_return_details_per_carton");

            //lds_ret_inv_body.DataObject = "dwh_return_details2"
            //Ron@10/08/2015>>SPUCM00005419 - add and remark - end





            ll_retrieve_header_row_count = lds_ret_inv_header.Retrieve(ll_shipment_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
			 
			ll_retrieve_body_row_count = lds_ret_inv_body.Retrieve(ll_shipment_number, masofonAlias.masofon.Instance.gs_vars.branch_number);
			//Ron@10/08/2015>>SPUCM00005419 - add and remark - start
			ls_filter = "cc_carton = 1";
			
			li_rc = lds_ret_inv_body.SetFilter(ls_filter);
			
			li_rc = lds_ret_inv_body.Filter();
			
			ll_retrieve_body_row_count = lds_ret_inv_body.RowCount();
			//Ron@10/08/2015>>SPUCM00005419 - add and remark - end
			if (ll_retrieve_header_row_count < 1 || ll_retrieve_body_row_count < 1)
			{
                await this.ViewModel.uf_show_message("uo_mini_terminal_return.wf_b2b_send_return", "", "OK", "שגיאה באיסוף נתונים לשליחת מסר");
			}
			// Populete lds_debit_data with lds_ret_inv_header and lds_ret_inv_body
			lds_debit_data[1] = lds_ret_inv_header;
			lds_debit_data[2] = lds_ret_inv_body;
			// AlexKh - 1.1.1.18 - 2014-02-18 - CR#1171(SPUCM00004718) - set marlog distributor
			//ls_sup_edi_no
			//ron@12/04/2015>>SPUCM00005270 - add and remark  - start
			ls_marlog_number = this.ViewModel.il_marlog_branchProperty.ToString();
			if (true)
			{
			    ViewModel.LoadData4(ls_marlog_number, ref ll_marlog_distributor_for_xml);
			}
			if (true)
			{
			    ViewModel.LoadData5(ll_marlog_distributor_for_xml, ref ls_sup_edi_no);
			}

            //
            //select edi_number
            //INTO :ls_sup_edi_no
            //FROM suppliers
            //WHERE number = :il_marlog_distributor;
            //
            //ron@12/04/2015>>SPUCM00005270 - add and remark  -  end
            //Call invuo_b2b_return_inv.uf_send_transaction(), in order to create the B2B message XML file.
            if ((await this.ViewModel.invuo_b2b_debit_marlogProperty.uf_send_transaction(li_ret_inv_sts, ls_sup_edi_no, lds_debit_data)).Retrieve(out li_ret_inv_sts, out lds_debit_data))
			{
				ls_message = "Return Invoice No: " + ll_ret_inv_no.ToString() + " - XML file was send to the supplier successfully.";
                await this.ViewModel.uf_show_message("Success!", "", "OK", Convert.ToString(ls_message));
			}
			else
			{
				ls_message = "Return No: " + ll_ret_inv_no.ToString() + " - XML file was faild.";
                await this.ViewModel.uf_show_message("Failure!", "", "OK", Convert.ToString(ls_message));
				return false; //Eitan; SPUCM00005489; 7/9/2016; 1.39.6;
			}
            if (!(await this.ViewModel.invuo_b2b_debit_marlogProperty.uf_update_outgoing((int)masofonAlias.masofon.Instance.gl_msg_type_debit,  li_ret_inv_sts, "RETURN", ll_ret_inv_no, ll_ret_inv_no, ll_supplier, masofonAlias.masofon.Instance.gs_vars.branch_number)).Retrieve(out li_ret_inv_sts))
			{
				//RETURN FALSE //Eitan; SPUCM00005361; Build 39; 31/8/2015;
			}
			//Eitan; SPUCM00005489; 7/9/2016; 1.39.6; // NEW SECTION
			//1) send xml to original supplier
			// Set message type to be a Return message (not Confirm or Debit)
			if (!this.ViewModel.invuo_b2b_debitProperty.uf_set_message_type("RETURN"))
			{
				return false;
			}
			if (true)
			{
			    ViewModel.LoadData6(ref ls_sup_edi_no);
			}
			ls_msg_type = "INVDBT";
			if (!this.ViewModel.invuo_b2b_debitProperty.uf_init(Convert.ToString(ls_msg_type), this.ViewModel.il_supplierProperty, ll_ret_inv_no, masofonAlias.masofon.Instance.gs_vars.branch_number))
			{
				return false;
			}
			ls_temp_xml2 = this.ViewModel.invuo_b2b_debitProperty.is_file_spec;
			this.ViewModel.invuo_b2b_debitProperty.is_file_spec = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder_temp + this.ViewModel.invuo_b2b_debitProperty.is_file_name;
            //get details for lds_ret_inv_header2
            lds_ret_inv_header2 = new dwh_invoiceRepository();// VisualElementHelper.CreateFromView<GridElement>("dwh_invoice", "dwh_invoice");
			 
			ll_retrieve_header_row_count = lds_ret_inv_header2.Retrieve(this.ViewModel.il_supplierProperty, ll_ret_inv_no, masofonAlias.masofon.Instance.gs_vars.branch_number, "r");
            //get details for lds_debit_data 2
            lds_ret_inv_body2 = new dwh_invoice_details2Repository();// VisualElementHelper.CreateFromView<GridElement>("dwh_invoice_details2", "dwh_invoice_details2");
			 
			ll_retrieve_body_row_count = lds_ret_inv_body2.Retrieve(this.ViewModel.il_supplierProperty, ll_ret_inv_no, masofonAlias.masofon.Instance.gs_vars.branch_number, "r");
			if (ll_retrieve_header_row_count < 1 || ll_retrieve_body_row_count < 1)
			{
                await this.ViewModel.uf_show_message("uo_mini_terminal_return.wf_b2b_send_return", "", "OK", Convert.ToString("שגיאה באיסוף נתונים לשליחת מסר" + " 2 "));
			}
			// Populate lds_debit_data with lds_ret_inv_header and lds_ret_inv_body
			lds_debit_data[1] = lds_ret_inv_header2;
			lds_debit_data[2] = lds_ret_inv_body2;
            if ((await this.ViewModel.invuo_b2b_debitProperty.uf_send_transaction( li_ret_inv_sts, ls_sup_edi_no,  lds_debit_data)).Retrieve(out li_ret_inv_sts, out lds_debit_data))
			{
				ls_message = "Return Invoice No: " + ll_ret_inv_no.ToString() + " - XML file was send to the original supplier successfully.";
                await this.ViewModel.uf_show_message("Success!", "", "OK", Convert.ToString(ls_message));
			}
			else
			{
				ls_message = "Return No: " + ll_ret_inv_no.ToString() + " - XML file was faild.";
                await this.ViewModel.uf_show_message("Failure!", "", "OK", Convert.ToString(ls_message));
			}
		    await f_begin_tranClass.f_begin_tran();
		    ViewModel.UpdateData2(ll_ret_inv_no);
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{
				
				await MessageBox.Show(", UPDATE state = S for invoice_move failed, invoice number = " + ll_ret_inv_no.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "uo_mini_terminal_crossdoc_return - Sql Error");
			    await f_rollbackClass.f_rollback();
				return false;
			}
			//3) set stock_control 
			
			for (ll_items = 1; ll_items <= lds_ret_inv_body2.RowCount(); ll_items++)
			{
				
				ll_material_number = lds_ret_inv_body2.GetItemValue<long>((int)ll_items, "material_number");
				
				ldec_quantity = lds_ret_inv_body2.GetItemValue<decimal>((int)ll_items, "material_quantity");
			    ViewModel.UpdateData3(ldec_quantity, ll_material_number);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{
				    await f_rollbackClass.f_rollback();
					
					await MessageBox.Show(", UPDATE stock_balance for stock_control failed, material number = " + ll_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "uo_mini_terminal_crossdoc_return - Sql Error");
					return false;
				}
			}
			//move both xml files 
		    bool result = true; 
		    try
			{
		        System.IO.File.Move(this.ViewModel.invuo_b2b_debit_marlogProperty.is_file_spec, ls_temp_xml1);
		        try
				{
		            System.IO.File.Move(this.ViewModel.invuo_b2b_debitProperty.is_file_spec, ls_temp_xml2);
				    await f_commitClass.f_commit();

				}
                catch (Exception e)
				{
				    result = false;
				}
                if (!result)
			    {
			        await MessageBox.Show("Error copying xml 1 ", "uo_mini_terminal_crossdoc_return - Error");
			        await f_rollbackClass.f_rollback();
			    }
		        await f_commitClass.f_commit();
			}
            catch (Exception e)
			{
			    result = false;
			}
		    if (!result)
		    {
		        await MessageBox.Show("Error copying xml 1 ", "uo_mini_terminal_crossdoc_return - Error");
		        await f_rollbackClass.f_rollback();
		    }
            return result;
		}
		public async Task<string> uf_get_prn_name_from_stations(long branch_number, long station_number) //string uf_get_prn_name_from_stations()
		{
			string ls_printer_name = null;
			if (true)
			{
			    ViewModel.LoadData7(station_number, branch_number, ref ls_printer_name);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
			{
				//MessageBox( "uo_mini_terminal_returns.uf_get_prn_name_from_stations()", " שם של מדפסת  גוי ")
				return "";
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
			{
				await MessageBox.Show("Database Error!", "uo_mini_terminal_returns.uf_get_prn_name_from_stations()");
				return "";
			}
			if (ls_printer_name == null)
			{
				ls_printer_name = "";
				//	MessageBox( "uo_mini_terminal_returns.uf_get_prn_name_from_stations()", "שם של מדפסת  יריק " )
			}
			return ls_printer_name;
		}
        protected async Task<bool> uf_print_carton_barcode(string as_title, string as_barcode, long al_branch_number, string as_branch_name, long al_supplier_number, string as_supplier_name) //********************************************************************
        {
            //	*Object:	            uo_mini_terminal_returns
            //	*Function Name:	   (uf_print_carton_barcode)
            //	*Purpose: 	
            //	*Arguments: 	      string as_barcode
            //	*Return:	            boolean
            //	*Date		    Programmer    Version	  Task#	    Description
            //	*---------------------------------------------------------------------
            //	*20/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
            //********************************************************************

            bool lb_ret = false;
            long ll_RetunType = 0;
            long ll_BranchNum = 0;
            long ll_StationNum = 0;
            long ll_SupplierNum = 0;
            string ls_PrnName = null;
            string ls_Barcode = null;
            string ls_BranchName = null;
            string ls_SupplierName = null;
            string ls_PrnType = null;
            ll_RetunType = 2;
            ll_BranchNum = al_branch_number;
            ll_StationNum = masofonAlias.masofon.Instance.gs_station.station_number;
            ll_SupplierNum = 0;
            ls_PrnName = await uf_get_prn_name_from_stations(ll_BranchNum, ll_StationNum);
            if (ls_PrnName == null)
            {
                ls_PrnName = "";
            }
            if (ls_PrnName.Length < 7)
            {
                await this.ViewModel.uf_show_message("", "", "OK", Convert.ToString("Invalidate Printer Name= " + ls_PrnName));
                return false;
            }
            //Eduardi 2015-10-20 v.1.1.39.2 SPUCM00005471-מדפסת החזרות מרלוג CITIZEN Err
            ls_PrnType = await uf_get_prn_type_from_stations(ll_BranchNum, ll_StationNum);
            if (ls_PrnType == null)
            {
                ls_PrnType = "";
            }
            if (ls_PrnType.Length != 7)
            {
                await this.ViewModel.uf_show_message("", "", "OK", Convert.ToString("Invalidate Printer Name = " + ls_PrnName));
                return false;
            }
            long ll_printer_type = 0;
            if (ls_PrnType.ToUpper() == "BTP_RET")
            {
                ll_printer_type = masofonAlias.masofon.BTP_PRINTER_TYPE;
            }
            else if (ls_PrnType.ToUpper() == "Citizen".ToUpper())
            {
                ll_printer_type = masofonAlias.masofon.CITIZEN_PRINTER_TYPE;
            }
            else
            {
                await this.ViewModel.uf_show_message("", "", "OK", Convert.ToString("Invalidate Printer Name : " + ls_PrnName));
                return false;
            }
            //--
            ls_PrnName = ls_PrnName + Convert.ToChar(0).ToString();
            ls_Barcode = as_barcode + Convert.ToChar(0).ToString();
            ls_BranchName = as_branch_name + Convert.ToChar(0).ToString();
            ls_SupplierName = as_supplier_name + Convert.ToChar(0).ToString();
            //Eduardi 2015-10-20 v.1.1.39.2 SPUCM00005471-מדפסת החזרות מרלוג CITIZEN Err
            ////Eduardi 2015-09-24 v.1.1.29.7.1 SPUCM00005427-מדפסת החזרות מרלוג CITIZEN
            //IF gb_printer_is_citizen THEN
            //	ll_printer_type = CITIZEN_PRINTER_TYPE
            //ELSE
            //	ll_printer_type = BTP_PRINTER_TYPE
            //END IF
            //--
            //--
            // AlexKh - 1.1.1.20 - 2014-05-05 - CR#1171(SPUCM00004718) - set marlog branch_number
            //Eduardi 2015-09-24 v.1.1.39.2 SPUCM00005427-מדפסת החזרות מרלוג CITIZEN
            //IF il_marlog_distributor = DISTRIBUTOR_BAIT_MIRKAHAT THEN
            //	lb_ret = Print_ItemReturn ( ls_PrnName, ll_RetunType, ls_Barcode, ll_BranchNum + 2000, ls_BranchName, ll_SupplierNum, ls_SupplierName )
            //ELSE
            //	lb_ret = Print_ItemReturn ( ls_PrnName, ll_RetunType, ls_Barcode, ll_BranchNum, ls_BranchName, ll_SupplierNum, ls_SupplierName )
            //END IF
            string ls_subject = "";
            if (this.ViewModel.il_marlog_distributorProperty == mini_terminal.uo_mini_terminal_crossdoc_returns.DISTRIBUTOR_BAIT_MIRKAHAT)
            {
                ls_subject = "החזרות מרלוג בימ\"ק";
                lb_ret = SystemHelper.Print_ItemReturn(ll_printer_type, ls_PrnName, ll_RetunType, ls_Barcode, ll_BranchNum, ls_BranchName, ll_SupplierNum, ls_SupplierName, ls_subject);
            }
            else
            {
                ls_subject = "החזרות מרלוג";
                lb_ret = SystemHelper.Print_ItemReturn(ll_printer_type, ls_PrnName, ll_RetunType, ls_Barcode, ll_BranchNum, ls_BranchName, ll_SupplierNum, ls_SupplierName, ls_subject);
            }
            //--
            return true;
        }
        public async Task<bool> uf_create_marlog_return_copy()
		{
			GridElement dw_summary = ViewModel.GetVisualElementById<GridElement>("dw_summary");
			long ll_i = 0;
			long ll_rows = 0;
			long ll_return_number = 0;
			long ll_supplier_number = 0;
			// AlexKh - 1.1.1.18 - 2014-02-19 - CR#1171(SPUCM00004718) - set marlog branch_number
			this.ViewModel.invuo_account_handleProperty.il_marlog_branch = this.ViewModel.il_marlog_branchProperty;
			
			ll_rows = dw_summary.RowCount();
			if (ll_rows > 0)
			{
				for (ll_i = 1; ll_i <= ll_rows; ll_i++)
				{
					
					ll_supplier_number = dw_summary.GetItemValue<long>(ll_i, "supplier_number");
					
					ll_return_number = dw_summary.GetItemValue<long>(ll_i, "doc_no");
					if (await this.ViewModel.invuo_account_handleProperty.uf_retrieve_account(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_supplier_number, ll_return_number, "r"))
					{
						if (!await this.ViewModel.invuo_account_handleProperty.uf_update_return())
						{
							break; // TODO: might not be correct. Was : Exit For
						}
					}
					else
					{
					    await f_rollbackClass.f_rollback();
					}
				}
			}
			return true;
		}
		public async Task<int> uf_cancel_return() //********************************************************************
		{
			GridElement dw_summary = ViewModel.GetVisualElementById<GridElement>("dw_summary");
			//	*Object:	            uo_mini_terminal_returns
			//	*Event Name:	   	uf_cancel_return
			//	*Purpose: 				Cancel all the return
			//	*Arguments: 	      none
			//	*Return:	            integer
			//	*Date		    Programmer    Version	  Task#	       Description
			//	*---------------------------------------------------------------------
			//	*23/12/2013	 AlexKh        1.1.1.16	  SPUCM00004291 Initial version
			//********************************************************************
			
			string ls_barcode_carton = null;
			long ll_i = 0;
			long ll_count = 0;
			long ll_supplier_number = 0;
			long ll_invoice_number = 0;
			DateTime? ldt_time = null;
			ldt_time = masofonAlias.masofon.Instance.set_machine_time();
		    await f_begin_tranClass.f_begin_tran();
		    ViewModel.UpdateData4(ldt_time);
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
			    await f_rollbackClass.f_rollback();
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "ביטול החזרה נכשל");
				return -1;
			}
			
			ll_count = dw_summary.RowCount();
			for (ll_i = 1; ll_i <= ll_count; ll_i++)
			{
				
				ll_supplier_number = dw_summary.GetItemValue<long>(ll_i, "supplier_number");
				
				ll_invoice_number = dw_summary.GetItemValue<long>(ll_i, "doc_no");
			    ViewModel.UpdateData5(ldt_time, ll_supplier_number, ll_invoice_number);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
				{
				    await f_rollbackClass.f_rollback();
                    await this.ViewModel.uf_show_message("שגיאה", "", "OK", "ביטול החזרה נכשל");
					return -1;
				}
			}
            await this.ViewModel.uf_show_message("הודעה", "", "OK", "ההחזרה בוטלה בהצלחה");
		    await f_commitClass.f_commit();
			return 0;
		}
		public async Task<bool> uf_set_total(long al_supplier_number, long al_invoice_number) //********************************************************************
		{
			//	*Object:	            cb_finish
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    	 Description
			//	*---------------------------------------------------------------------
			//	*11/02/2014	 AlexKh        1.1.1.17	  SPUCM00004702 Initial version
			//********************************************************************
			
			decimal ldec_invoice_total = default(decimal);
			decimal ldec_mam = default(decimal);
			long ll_supplier = 0;
			int li_cnt = 0;
			if (true)
			{
			    ViewModel.LoadData8(al_supplier_number, al_invoice_number, ref ldec_invoice_total, ref ldec_mam);
			}
			if (ldec_invoice_total > 0)
			{
				ldec_invoice_total = Math.Round(ldec_invoice_total * (1 + ldec_mam), 2);
			    ViewModel.UpdateData6(ldec_invoice_total, al_supplier_number, al_invoice_number);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{

                    await this.ViewModel.uf_show_message("שגיאה", "", "OK", Convert.ToString(masofonAlias.masofon.Instance.sqlca.SqlErrText));
					return false;
				}
			}
			else
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "קיימת החזרה עם סכום אפס");
			}
			return true;
		}
		public int uf_handle_retrun_num(long al_supplier) //********************************************************************
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			//*Object:	uo_mini_terminal_crossdoc_returns
			//*Function Name:	(uf_handle_retrun_num)
			//*Purpose: 	
			//*Arguments: 	(none)
			//*Return:	integer
			//*Programmer	Date   Version	Task#	 Description
			//*---------------------------------------------------------------------
			//*RonY@15/01/2015 -   SPUCM00005162
			//********************************************************************
			
			long ll_row = 0;
			GridElement dwc_shipments;
			 
			dwc_shipments = dw_return_num.GetVisualElementById<GridElement>("shipment_number");
			
			
			
			ll_row = dwc_shipments.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_marlog_branchProperty, al_supplier);
			
			if (dw_return_num.RowCount() < 1)
			{
				
				dw_return_num.Insert(0);
			}
			return 1;
		}
		public async Task<string> uf_get_prn_type_from_stations(long branch_number, long station_number) //Eduardi 2015-10-20 v.1.1.39.2 SPUCM00005471-מדפסת החזרות מרלוג CITIZEN Err
		{
			//string uf_get_prn_type_from_stations (long branch_number, long station_number)
			string ls_printer_type = null;
			if (true)
			{
			    ViewModel.LoadData9(station_number, branch_number, ref ls_printer_type);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
			{
				//MessageBox( "uo_mini_terminal_returns.uf_get_prn_type_from_stations()", " שם של מדפסת  גוי ")
				return "";
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
			{
				await MessageBox.Show("Database Error!", "uo_mini_terminal_returns.uf_get_prn_type_from_stations()");
				return "";
			}
			if (ls_printer_type == null)
			{
				ls_printer_type = "";
				//	MessageBox( "uo_mini_terminal_returns.uf_get_prn_type_from_stations()", "שם של מדפסת  יריק " )
			}
			return ls_printer_type;
		}
       
		public void destructor()
		{
			base.destructor();


		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task cb_finish_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			//	*Object:	            cb_finish
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*22/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
			//********************************************************************
			
           // this.ViewModel.cb_finish.clicked();
			long ll_ret = 0;
			long ll_row = 0;
			long ll_supplier_number = 0;
			long ll_invoice_number = 0;
			DateTime? ldt_datetime = null;
			
			 
			if (dw_cartons.RowCount() > 0 && dw_cartons.GetItemValue<string>(0, "state") == mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString())
			{
				//IF uf_b2b_send_return() THEN //Eitan; SPUCM00005361; Build 39; 31/8/2015;
				ldt_datetime = masofonAlias.masofon.Instance.set_machine_time();
				
				for (ll_row = 1; ll_row <= dw_cartons.RowCount(); ll_row++)
				{
					
					dw_cartons.SetItem(ll_row, "state", mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_SEND.ToString());
					
					dw_cartons.SetItem(ll_row, "last_update_datetime", ldt_datetime.ToString());
				}
			    await f_begin_tranClass.f_begin_tran();
				// AlexKh - 1.1.1.17 - 2014-02-11 - SPUCM00004702 - update invoice totals
				
				for (ll_row = 1; ll_row <= dw_summary.RowCount(); ll_row++)
				{
					
					ll_supplier_number = dw_summary.GetItemValue<long>(ll_row, "supplier_number");
					
					ll_invoice_number = dw_summary.GetItemValue<long>(ll_row, "doc_no");
					if (!await uf_set_total(ll_supplier_number, ll_invoice_number))
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כשלון בעדכון סכום החזרות");
					    await f_rollbackClass.f_rollback();
						return;
					}
				}

			    using (UnitOfWork unitOfWork = new UnitOfWork())
			    {
                    ll_ret = dw_cartons.Update(unitOfWork);
			    }

				if (ll_ret == 1)
				{
					if (await uf_create_marlog_return_copy())
					{
						//Eitan; SPUCM00005361; Build 39; 31/8/2015;
						if (await uf_b2b_send_return())
						{
						    await f_commitClass.f_commit();
							if (await this.ViewModel.uf_show_message("הודעה", "", "YESNO", "האם להדפיס החזרה?") == 1)
							{
                                await cb_print_clicked(null, EventArgs.Empty);
							}
							cb_exit_clicked(null, EventArgs.Empty);
						}
						else
						{
						    await f_rollbackClass.f_rollback();
                            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כשלון בשליחת המסר");
						}
					}
					else
					{
					    await f_rollbackClass.f_rollback();
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כשלום ביצירת החזרה  במרלוג");
					}
				}
				else
				{
				    await f_rollbackClass.f_rollback();
                    await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כשלון בעדכון סטטוס החזרה");
				}
			}
			//Eitan; SPUCM00005361; Build 39; 31/8/2015;
			//	IF uf_show_message("הודעה", "", "YESNO", "האם להדפיס החזרה?") = 1 THEN
			//		cb_print.TriggerEvent("clicked")
			//	END IF
			//cb_exit.TriggerEvent("clicked")
		}
		// TODO: Could not add event handler for handler 'itemchangedEvent'. (CODE=1017)
		public async Task<int> dw_inv_pack_details_itemchanged(int row, VisualElement dwo, string data)
		{
			ButtonElement cb_carton = this.GetVisualElementById<ButtonElement>("cb_carton");
			// TODO: Member 'dw_inv_pack_details' was retyped to dynamic cause it had reflected members. (CODE=30005)
			dynamic dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			 
			
			
			
			dw_inv_pack_details.itemchanged(0, default(ControlElement), default(string));
			string ls_column = null;
			string ls_barcode = null;
			string ls_material_name = null;
			string ls_null = null;
			long ll_supplier = 0;
			long ll_quantity = 0;
			long ll_row = 0;
			long ll_temp = 0;
			long ll_return_son = 0;
			long ll_material_number = 0;
			long ll_distributor = 0;
			long ll_return_allowed = 0;
			long ll_msg = 0;
			long ll_row_exists = 0;
			long ll_declines = 0;
			int li_len = 0;
			int li_index = 0;
			DateTime? ldt_time = null;
			s_array_arguments lstr_parms = default(s_array_arguments);
			
			ls_column = Convert.ToString(dwo.ID);
			 
			dw_inv_pack_details.SetNull();
			if (row <= 0)
			{
				return 2;
			}
			//IF no carton open then return
			if (cb_carton.Text == "פתח קרטון")
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "נא לפתוח קרטון");
				return 2;
			}
			if (this.ViewModel.ib_marlog_returnProperty)
			{
				if (this.ViewModel.il_supplierProperty == 0 || this.ViewModel.il_distributorProperty == 0)
				{
					return 2;
				}
			}
			else
			{
				if (this.ViewModel.il_supplierProperty == 0)
				{
					return 2;
				}
			}
			switch (ls_column) {
				case "barcode":
					ldt_time = masofonAlias.masofon.Instance.set_machine_time();
					ls_barcode = data;
					// Get full barcode if it was particulaly inserted
					ls_barcode =await  this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
					// If barcode does not exist, drop leading zeros
					if (string.IsNullOrEmpty(ls_barcode))
					{
						li_len = data.Length;
						for (li_index = 1; li_index <= li_len; li_index++)
						{
							if (data.Substring(0, 1) == "0")
							{
								data = data.Substring(data.Length - (data.Length - 1));
								
								li_index++;
							}
							else
							{
								break; // TODO: might not be correct. Was : Exit For
							}
						}
						ls_barcode = data;
						ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
					}
					
					dw_inv_pack_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
					//get supplier number for barcode from material and set	
					if (true)
					{
					    ViewModel.LoadData10(ls_barcode, ref ll_supplier, ref ls_material_name, ref ll_material_number, ref ll_distributor, ref ll_return_allowed);
					}
					if (this.ViewModel.ib_marlog_returnProperty)
					{
					    ViewModel.LoadData11(ll_distributor, ll_supplier, ref ll_row_exists);
						if ((ll_distributor != this.ViewModel.il_distributorProperty) || (ll_row_exists != 1))
						{
                            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך למפיץ שנבחר");
							
							dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
							return 2;
						}
					}
					else
					{
						if (ll_supplier != this.ViewModel.il_supplierProperty)
						{
                            await this.ViewModel.uf_show_message("שגיאה", "", "OK", "הפריט אינו שייך לספק שנבחר");
							
							dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
							return 2;
						}
					}
					if (ll_return_allowed == 0 || ll_return_allowed != 1)
					{
						ll_msg = await this.ViewModel.uf_show_message("התראה", "", "yesno", "פריט זה אינו מורשה להחזרה. האם תרצה לנצל אדהוק");
						if (ll_msg == 1) // yes
						{
							lstr_parms.a_long[1] = masofonAlias.masofon.Instance.gs_vars.branch_number;
							lstr_parms.a_long[2] = ll_supplier;
							lstr_parms.a_long[3] = this.ViewModel.il_return_number;
							lstr_parms.a_long[4] = ll_material_number; //item_number
							lstr_parms.a_long[6] = 0; //serial_number
							lstr_parms.a_long[5] = 0; // "adhoc_serial_number")
							lstr_parms.a_string[1] = ""; // "adhoc_key")
							lstr_parms.a_datastore[1] = this.ViewModel.ids_adhoc_update_listProperty;
                            await WindowHelper.Open<rw_addhock>("global_global", "args", lstr_parms);
							
							
							lstr_parms = (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
							if (lstr_parms.a_boolean[0]) //succeed to update the adhoc status
							{
								if (row > 0) //update 
								{
									
									dw_inv_pack_details.SetItem(row, "adhoc_serial_number", lstr_parms.a_long[1].ToString());
									
									dw_inv_pack_details.SetItem(row, "adhoc_key", Convert.ToString(lstr_parms.a_string[1]));
								}
								this.ViewModel.ids_adhoc_update_listProperty = lstr_parms.a_datastore[1] as IRepository;
							}
							else
							{
								//failed to update the adhoc status
								
								dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
								return 2;
							}
						}
						else
						{
							
							dw_inv_pack_details.SetItem(row, Convert.ToString(ls_column), Convert.ToString(ls_null));
							return 2; // - not allowed
						}
					}
					
					dw_inv_pack_details.SetItem(row, "serial_number", this.ViewModel.il_return_number.ToString());
					
					dw_inv_pack_details.SetItem(row, "carton_barcode", Convert.ToString(this.ViewModel.is_carton_barcode));
					
					dw_inv_pack_details.SetItem(row, "supplier_number", ll_supplier.ToString());
					
					dw_inv_pack_details.SetItem(row, "distributor_number", ll_distributor.ToString());
					
					dw_inv_pack_details.SetItem(row, "material_number", ll_material_number.ToString());
					
					dw_inv_pack_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
					
					dw_inv_pack_details.SetItem(row, "date_move", ldt_time.ToString());
					break;
				case "material_quantity":
					ls_barcode = dw_inv_pack_details.GetItem<string>(row, "barcode");
					if (isempty_stringClass.isempty_string(ls_barcode))
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "נא לסרוק ברקוד קודם");
						return 2;
					}
					
					dw_inv_pack_details.SetItem(row, "material_quantity", (Convert.ToDecimal(data)).ToString());
					break;
				case "declines":
					ls_barcode = dw_inv_pack_details.GetItem<string>(row, "barcode");
					
					ll_quantity = dw_inv_pack_details.GetItem<long>(row, "material_quantity");
					if (isempty_stringClass.isempty_string(ls_barcode) || ll_quantity <= 0)
					{
                        await this.ViewModel.uf_show_message("שגיאה", "", "OK", "חסרים נתוני ברקוד / כמויות");
						return 2;
					}
					
					dw_inv_pack_details.SetItem(row, "declines", Convert.ToInt64(data).ToString());
					break;
				default:
					break;
			}
			
			ll_declines = dw_inv_pack_details.GetItem<long>(row, "declines");
			
			ll_quantity = dw_inv_pack_details.GetItem<long>(row, "material_quantity");
			if (!isempty_stringClass.isempty_string(ls_barcode) && (ll_quantity > 0 && ll_declines > 0))
			{
				
				
				dw_inv_pack_details.dw_inv_pack_details_ue_new_row();
			}
            return 0;
        }
		// TODO: Could not add event handler for handler 'ue_new_rowEvent'. (CODE=1017)
		public async Task<int> dw_inv_pack_details_ue_new_row()
		{
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
            return 0;
		 //   u_dw_update dw_inv_pack_details = this.GetVisualElementById<u_dw_update>("dw_inv_pack_details");
			
			//// TODO: Type 'System.Object' does not have a 'ue_new_row' member. (CODE=30002)
			//// TODO: Type 'System.Object' does not have a 'ue_new_row' member. (CODE=30002)
			//(dw_inv_pack_details).ue_new_row();
			//return await this.ViewModel.uf_new_row(dw_inv_pack_details);
		}
		// TODO: Could not add event handler for handler 'rowfocuschangingEvent'. (CODE=1017)
		public int dw_inv_pack_details_rowfocuschanging(int l1, int l2)
		{
			GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
			 
			
			
			//dw_inv_pack_details.rowfocuschanging(0, 0);
			//bool lb_update = false;
			long ll_rtn_code = 0;
			long ll_decline_reason = 0;
			decimal ldc_qty = default(decimal);
			string ls_barcode = null;
			string ls_doc_state = null;
			ModelAction ldw_row_status = default(ModelAction);
			object currentrow = null;
			if (Convert.ToInt32(currentrow) == 0)
			{
				return 0;
			}
			
			
			ldw_row_status = dw_inv_pack_details.GetItemStatus((int)currentrow, 0, ModelBuffer.Primary);
			
			
			if (ldw_row_status == ModelAction.None || ldw_row_status == ModelAction.Insert)
			{
				ls_barcode = dw_inv_pack_details.GetItemValue<string>((long)currentrow, "barcode");
				if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
				{
					return 1;
				}
				
				ldc_qty = dw_inv_pack_details.GetItemValue<decimal>((long)currentrow, "material_quantity");
				if (ldc_qty == 0 || ldc_qty == 0)
				{
					return 1;
				}
				
				ll_decline_reason = dw_inv_pack_details.GetItemValue<long>((long)currentrow, "declines");
				if (ll_decline_reason <= 0)
				{
					return 1;
				}
				uf_update_item(Convert.ToInt64(currentrow));
			}
            return 1;
        }
		// TODO: Could not add event handler for handler 'itemchangedEvent'. (CODE=1017)
		public async Task<int> dw_1_itemchanged(int row, VisualElement dwo, string data)
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			//SPUCM00003614
			long ll_ret = 0;
			long ll_null = 0;
			long ll_data = 0;
			long? ll_find = 0;
			long ll_row = 0;
			long li_prev_supplier = 0;
		//	GridElement dwc_shipments;
			
			if (dwo.ID == "supplier")
			{
				
				if (SystemFunctionsExtensions.IsNumber(data))
				{
					
					li_prev_supplier = dw_1.GetItemValue<long>(row, "supplier");
					
					dw_return_num.Reset();
					//RonY@15/01/2015 - SPUCM00005162 - validate - start			
					
					ll_ret = this.ViewModel.ids_valid_supplierProperty.RowCount();
					if (ll_ret == 0)
					{
						await MessageBox.Show("אין ספקים עם מפיץ" + " " + this.ViewModel.il_marlog_distributorProperty.ToString(), "");
						uf_handle_retrun_num(li_prev_supplier); //RonY@15/01/2015 - SPUCM00005162 
						this.ViewModel.ib_vlaid_suppProperty = false;
						return 2;
					}
					ll_data = Convert.ToInt64(data);
					
					
					ll_find = this.ViewModel.ids_valid_supplierProperty.Find("supplier_number == " + ll_data.ToString(), 0, this.ViewModel.ids_valid_supplierProperty.RowCount());
					if (ll_find < 1)
					{
						await MessageBox.Show("ספק לא מתאים למפיץ " + this.ViewModel.il_marlog_distributorProperty.ToString(), "");
						uf_handle_retrun_num(li_prev_supplier); //RonY@15/01/2015 - SPUCM00005162 
						this.ViewModel.ib_vlaid_suppProperty = false;
						return 2;
					}
					//RonY@15/01/2015  - SPUCM00005162 - validate - end
					//set supplier
					
					dw_1.SetItem(row, "supplier", Convert.ToString(data));
					this.ViewModel.il_supplierProperty = Convert.ToInt64(data);
					this.ViewModel.ib_vlaid_suppProperty = true;
					uf_handle_retrun_num(this.ViewModel.il_supplierProperty); //RonY@15/01/2015 - SPUCM00005162 
					//			//retrieve new distributors
					//			dw_1.GetChild('distributor', idwc_distributor_num)
					//			idwc_distributor_num.SetTransObject(SQLCA)
					//			ll_ret = idwc_distributor_num.Retrieve(long(data), gs_vars.branch_number)
					//			
					//			//empty distributor if supplier changed
					//			dw_1.setitem( row, "distributor", ll_null) 
					//			setNull(il_distributor)
				}
			}
			else if (dwo.ID == "distributor")
			{
				
				if (SystemFunctionsExtensions.IsNumber(data))
				{
					//set distributor
					
					dw_1.SetItem(row, "distributor", Convert.ToString(data));
					this.ViewModel.il_distributorProperty = Convert.ToInt64(data);
				}
			}
			else
			{
				//
			}
            return 0;
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public void dw_1_clicked(int xpos, int ypos, int row, VisualElement dwo)
		{
			
			if (Convert.ToString(dwo.ID) == "distributor" && !this.ViewModel.ib_marlog_returnProperty)
			{
				return;
			}
		}
		// TODO: Could not add event handler for handler 'editchangedEvent'. (CODE=1017)
		public void dw_1_editchanged(int row, VisualElement dwo, string data) //the user type again the old valid data
		{
			
			if (Convert.ToString(dwo.ID) == "supplier")
			{
				if (Convert.ToInt64(data) == this.ViewModel.il_supplierProperty)
				{
					uf_handle_retrun_num(this.ViewModel.il_supplierProperty);
					this.ViewModel.ib_vlaid_suppProperty = true;
				}
			}
		}
		// TODO: Could not add event handler for handler 'itemerrorEvent'. (CODE=1017)
		public int dw_1_itemerror(int row, VisualElement dwo, string data)
		{
			return 1;
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task<int> cb_carton_clicked(object sender, EventArgs e)
		{
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			//string		ls_ret
			long ll_row = 0;
			
			ll_row = dw_cartons.GetSelectedRow(0);
			if (ll_row > 0)
			{
				this.ViewModel.is_carton_barcode = dw_cartons.GetItemValue<string>(ll_row, "carton_barcode");
				if (!isempty_stringClass.isempty_string(this.ViewModel.is_carton_barcode)) //AND il_return_number > 0
				{

                    await this.ViewModel.uf_display("CROSSDOC_RETURN_DETAILS");
				}
			}
			else
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "!יש לבחור קרטון");
			}
			return 0;
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task cb_print_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
			//	*Object:	            uo_mini_terminal_returns.cb_print
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*12/09/2013	 AlexKh        1.1.1.12	  CR#1160    Initial version
			//********************************************************************
			
			GridElement lds_print;
			GridElement ldwc;
			GridElement ldwc_decline;
			GridElement ldwc_head;
			GridElement ldwc_footer;
			GridElement ldwc_rows;
			long ll_print_copies_number = 0;
			long ll_i = 0;
			long ll_ret = 0;
			
			if (dw_summary.RowCount() == 0)
			{
				return;
			}
			if (this.ViewModel.il_return_number == 0 || (this.ViewModel.il_return_number <= 0))
			{
				return;
			}
			// Retrieve document (shipment) details


            lds_print = VisualElementHelper.CreateFromView<GridElement>("dwh_crossdoc_return_print", "dwh_crossdoc_return_print", "global_global");
			
			// AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch_number
			//IF lds_print.Retrieve(il_return_number, gs_vars.branch_number) <= 0 THEN RETURN	
			//IF lds_print.Retrieve(il_return_number, gs_vars.branch_number, il_marlog_branch) <= 0 THEN RETURN	
			
			ldwc_head = lds_print.GetVisualElementById<GridElement>("dw_2");
			
			ldwc_footer = lds_print.GetVisualElementById<GridElement>("dw_1");
			
			ldwc_rows = lds_print.GetVisualElementById<GridElement>("dw_3");
			//			
			//ldwc_head.DataObject = "d_return_crossdoc_print_header" //ron@12/04/2015>>SPUCM00005270
			
			
			
			
			
			
			// 
			
			ldwc_head.Retrieve(this.ViewModel.il_return_number, masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_marlog_branchProperty);
			
			ldwc_rows.Retrieve(this.ViewModel.il_return_number, masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_marlog_branchProperty);
			
			ldwc_footer.Retrieve(this.ViewModel.il_return_number, masofonAlias.masofon.Instance.gs_vars.branch_number, this.ViewModel.il_marlog_branchProperty);
			//nvo_translator.fnv_rotate_dw(ldw_print)

            lds_print.Modify("DataWindow.Print", "Orientation", "2"); // 1-landscape , 2-portrait	
			
			ldwc = lds_print.GetVisualElementById<GridElement>("dw_3");
			 
			ldwc_decline = ldwc.GetVisualElementById<GridElement>("decline_number");
			
			
			
			ldwc_decline.Retrieve(3);
            //f_dw_wide_print(dw_print)
            // AlexKh - 1.1.22.0 - 2014-07-16 - SPUCM00004690 - choose copies number

            await WindowHelper.Open<rw_print_copies_number>("mini_terminal_mini_terminal", "rowsCount", dw_cartons.RowCount() + 1);


            ll_print_copies_number = Convert.ToInt64(WindowHelper.GetParam<string>(this.ViewModel));
			for (ll_i = 1; ll_i <= ll_print_copies_number; ll_i++)
			{
				
				ll_ret = lds_print.Print(MvcSite.Common.Globals.DeviceID, MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.PrinterName);
            }
			//lds_print.Print ()
			return;
		}
        // TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
        public async void cb_exit_clicked(object sender, EventArgs e)
        {
            GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
            LabelElement st_return_office = this.GetVisualElementById<LabelElement>("st_return_office");
            string ls_find = null;
            long? ll_row = 0;
            st_return_office.Text = ""; //Ron@05/05/2015
                                        // AlexKh - 1.1.1.35 - 07/10/2013 - SPUCM00004291 - present a message if empty cartons exist
            ls_find = "material_quantity == 0";


            ll_row = dw_cartons.Find(ls_find, 0, dw_cartons.RowCount());
            if (ll_row >= 0)
            {
                if (await this.ViewModel.uf_show_message("שגיאה", "", "YESNO", " שים לב נתונים של קרטון ריק לא ישמרו האם לצאת?") == -1)
                {
                    return;
                }
            }
            await this.uf_close_window();
        }
        // TODO: Could not add event handler for handler 'itemchangedEvent'. (CODE=1017)
        public async Task dw_details_itemchanged(int row, VisualElement dwo, string data)
        {
            GridElement dw_details = this.GetVisualElementById<GridElement>("dw_details");
            string ls_column = null;
            string ls_barcode = null;
            string ls_material_name = null;
            long ll_supplier = 0;
            long ll_quantity = 0;
            int li_len = 0;
            int li_index = 0;

            ls_column = Convert.ToString(dwo.ID);
            switch (ls_column)
            {
                case "barcode":
                    ls_barcode = data;
                    // Get full barcode if it was particulaly inserted
                    ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    // If barcode does not exist, drop leading zeros
                    if (string.IsNullOrEmpty(ls_barcode))
                    {
                        li_len = data.Length;
                        for (li_index = 1; li_index <= li_len; li_index++)
                        {
                            if (data.Substring(0, 1) == "0")
                            {
                                data = data.Substring(data.Length - (data.Length - 1));

                                li_index++;
                            }
                            else
                            {
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        ls_barcode = data;
                        ls_barcode = await this.ViewModel.iuo_material_search.uf_get_barcode_main(ls_barcode, false);
                    }

                    dw_details.SetItem(row, "barcode", Convert.ToString(ls_barcode));
                    //get supplier number for barcode from material and set
                    if (true)
                    {
                        ViewModel.LoadData12(ls_barcode, ref ll_supplier, ref ls_material_name);
                    }

                    dw_details.SetItem(row, "supplier_number", ll_supplier.ToString());

                    dw_details.SetItem(row, "material_name", Convert.ToString(ls_material_name));
                    break;
                case "material_quantity":
                    ll_quantity = Convert.ToInt64(data);

                    dw_details.SetItem(row, "material_quantity", ll_quantity.ToString());
                    break;
                default:
                    break;
            }
        
        }
        // TODO: Could not add event handler for handler '__SYS_EVENTpbm_dwnkey'. (CODE=1017)
        public int dw_return_num_ue_enter(Keys key, uint keyflags, EventArgs e) // AlexKh - 1.2.48.68 - 2013-07-29 - CR#1160  - ue_enter
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			if (keyflags == 0 && key == Keys.Enter || key == Keys.Tab)
			{
				
				dw_return_num.PerformValidated(e);
			}
			return 1;
		}
		// TODO: Could not add event handler for handler 'itemchangedEvent'. (CODE=1017)
		public void dw_return_num_itemchanged(int row, VisualElement dwo, string data)
		{
			LabelElement st_return_office = this.GetVisualElementById<LabelElement>("st_return_office");
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			string ls_return_number = null;
			string ls_barcode = null;
			string ls_mat_name = null;
			string ls_carton_barcode = null;
			long ll_mat_num = 0;
			long ll_supplier = 0;
			long ll_dec_num = 0;
			long ll_doc_no = 0;
			long ll_invoice_num = 0;
			long ll_distributor = 0;
			long ll_find_summary = 0;
			long ll_supplier_1 = 0;
			long ll_ret = 0;
			long ll_child_row = 0;
			long ll_invoice_number = 0;
			//char lc_state = '\0';
			char lc_state_1 = '\0';
			decimal ldc_mat_qty = default(decimal);
			decimal ldc_total_qty_1 = default(decimal);
			int li_cnt = 0;
			int li_row = 0;
			DateTime? ldt_time = null;
			GridElement ldwc_return;
			
			if (!isempty_stringClass.isempty_string(data) && SystemFunctionsExtensions.IsNumber(data))
			{
				this.ViewModel.il_return_number = Convert.ToInt64(data);
				this.ViewModel.uf_refresh();
				if (this.ViewModel.ib_marlog_returnProperty)
				{
					// AlexKh - 1.1.1.18 - 2014-02-18 - CR#1171(SPUCM00004718) - set marlog distributor
					//dw_1.setitem( 1, "distributor", 99)
					
					dw_1.SetItem(0, "distributor", this.ViewModel.il_marlog_distributorProperty.ToString());
				}
				//Ron@05/05/2015>>SPUCM00005327 - start
				 
				ldwc_return = dw_return_num.GetVisualElementById<GridElement>("shipment_number");
				
				ll_child_row = ldwc_return.GetRow();
				
				ll_invoice_number = ldwc_return.GetItemValue<long>((int)ll_child_row, "invoice_number");
				
				if (ll_invoice_number == 0 || dw_cartons.RowCount() == 0)
				{
					st_return_office.Text = "";
				}
				else
				{
					st_return_office.Text = ll_invoice_number.ToString();
				}
				//Ron@05/05/2015>>SPUCM00005327 - end
				dw_cartons.Focus();
			}
			else
			{
				//Ron@05/05/2015
				st_return_office.Text = "";
				
				dw_cartons.Reset();
			}
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public void dw_return_num_clicked(int xpos, int ypos, int row, VisualElement dwo, EventArgs e)
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			
			dw_1.PerformValidated(e);
			 
			dw_return_num.SelectText(1, 30);
		}
		// TODO: Could not add event handler for handler 'editchangedEvent'. (CODE=1017)
		public int dw_return_num_editchanged(int row, VisualElement dwo, string data) // AlexKh - 1.2.48.64 - 2013-07-29 - CR#1160  - EditChanged
		{
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			
			if (dwo.ID == "shipment_number")
			{
				
				if (!SystemFunctionsExtensions.IsNumber(data))
				{
					 
					dw_return_num.SelectText(1, 99);
					 
					dw_return_num.Clear();
				}
				if (data.Length > 20)
				{
					
					dw_return_num.SetItem(0, "shipment_number", Convert.ToInt64(data.Substring(0, 19)).ToString());
					 
					dw_return_num.SelectText(20, 1);
					 
					dw_return_num.Clear();
					return 0;
				}
			}
            return 0;
        }
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public void dw_cartons_clicked(int xpos, int ypos, int row, VisualElement dwo)
		{
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			if (row > 0)
			{
				
				if (dw_cartons.IsSelected(row))
				{
					
					dw_cartons.SelectRow(0, false);
				}
				else
				{
					
					dw_cartons.SelectRow(0, false);
					
					dw_cartons.SelectRow(row, true);
				}
			}
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task<int> cb_add_carton_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			//	*Object:	            cb_add_carton
			//	*Event Name:	   (clicked)
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	    Description
			//	*---------------------------------------------------------------------
			//	*19/08/2013	 AlexKh        1.1.1.10	  CR#1160    Initial version
			//********************************************************************
			
			string ls_barcode = null;
			string ls_ret = null;
			long ll_carton_barcode = 0;
			long ll_row = 0;
			long ll_check_sum = 0;
			//RonY@15/01/2015  - SPUCM000051625 - start	
			
			dw_1.PerformValidated(e);
			if (!this.ViewModel.ib_vlaid_suppProperty)
			{
                await MessageBox.Show("נא להזין ספק חוקי", "");
				return 0;
			}
			//RonY@15/01/2015  - SPUCM000051625 - end	
			// AlexKh - 1.1.1.11 - 21/08/2013 - CR#1160 - prevent adding carton for closed document
			
			if (dw_cartons.RowCount() > 0)
			{
				if (dw_cartons.GetItemValue<string>(0, "state") != mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString())
				{
                    await this.ViewModel.uf_show_message("שגיאה", "", "OK", "ניתן להוסיף קרטון רק להחזרה פתוחה!");
					return -1;
				}
			}
			
			ll_row = dw_cartons.Insert(0);
			//get new barcode
			ll_carton_barcode = await f_calculate_datatable_rowClass.f_calculate_datatable_row(147, 0, masofonAlias.masofon.Instance.sqlca);
			if (ll_carton_barcode <= 0)
			{
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "כישלון ביצירת מספר ברקוד");
				
				dw_cartons.Delete((int)ll_row);
				return -1;
			}
			ls_barcode = ll_carton_barcode.ToString();
			 
			ls_barcode = "960" + SystemFunctionsExtensions.Fill("0", 8 - ls_barcode.Length) + ls_barcode;
			ll_check_sum = SystemHelper.CalculateCheckSumNum(ls_barcode);
			if (ll_check_sum.ToString().Length != 1)
			{
				
				dw_cartons.Delete((int)ll_row);
                await this.ViewModel.uf_show_message("שגיאה", "", "OK", "ספרת ביקורת לא תקינה לברקוד");
				return -1;
			}
			ls_barcode += String.Concat(ll_check_sum, "0");
			//print barcode
			if (await uf_print_carton_barcode("החזרת קרוסדוק", ls_barcode, masofonAlias.masofon.Instance.gs_vars.branch_number, masofonAlias.masofon.Instance.qs_parameters.company_name, 0, ""))
			{
                //scan printed barcode to continue
                await WindowHelper.Open<rw_validate_carton>("mini_terminal_mini_terminal", "barcode", ls_barcode);



                ls_ret = WindowHelper.GetParam<string>(this.ViewModel);
				if (isempty_stringClass.isempty_string(ls_ret) || ls_ret == "0")
				{
					
					dw_cartons.Delete((int)ll_row);
					return -1;
				}
			}
			else
			{
				
				dw_cartons.Delete((int)ll_row);
				return -1;
			}
			
			dw_cartons.SetItem(ll_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
			
			dw_cartons.SetItem(ll_row, "shipment_number", this.ViewModel.il_return_number.ToString());
			
			dw_cartons.SetItem(ll_row, "carton_barcode", Convert.ToString(ls_barcode));
			
			dw_cartons.SetItem(ll_row, "material_quantity", "0");
			
			dw_cartons.SetItem(ll_row, "state", mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString());
			// AlexKh - 1.1.1.12 - 09/09/2013 - SPUCM00004291 - update carton row status		
			
			
			
			dw_cartons.SetItemStatus((int)ll_row, 0, ModelBuffer.Primary, ModelAction.None);
            return 0;
        }
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task cb_new_return_clicked(object sender, EventArgs e) //********************************************************************
		{
			ButtonElement cb_add_carton = this.GetVisualElementById<ButtonElement>("cb_add_carton");
			GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			//	*Object:	            uo_mini_terminal_returns
			//	*Event Name:	   	cb_new_teturn
			//	*Purpose: 				New return
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	       Description
			//	*---------------------------------------------------------------------
			//	*23/12/2013	 AlexKh        1.1.1.16	  SPUCM00004291 Initial version
			//********************************************************************
			
			long ll_supp = 0;
			//RonY@15/01/2015  - SPUCM000051625 - start
			
			dw_1.PerformValidated(e);
			
			ll_supp = dw_1.GetItemValue<long>(0, "supplier");
			if (ll_supp == 0)
			{
				await MessageBox.Show("נא להזין ספק", "");
				return;
			}
			if (!this.ViewModel.ib_vlaid_suppProperty)
			{
				await MessageBox.Show("נא להזין ספק חוקי", "");
				return;
			}
			//RonY@15/01/2015  - SPUCM000051625 - end
			uf_reset();
			 
			dw_return_num.Clear();
			this.ViewModel.uf_create(true, 0, "", 0);
			 
			cb_add_carton.PerformClick(e);
        }
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task cb_cancel_return_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_1 = this.GetVisualElementById<GridElement>("dw_1");
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			ButtonElement cb_cancel_return = this.GetVisualElementById<ButtonElement>("cb_cancel_return");
			//	*Object:	            uo_mini_terminal_returns
			//	*Event Name:	   	cb_cancel_teturn
			//	*Purpose: 				Canel return button clicked
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	       Description
			//	*---------------------------------------------------------------------
			//	*23/12/2013	 AlexKh        1.1.1.16	  SPUCM00004291 Initial version
			//********************************************************************
			
			long ll_null = 0;
			
			if (dw_cartons.RowCount() > 0 && dw_cartons.GetItemValue<string>(0, "state") == mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString())
			{
			    await uf_cancel_return();
				uf_reset();
				
				dw_1.SetItem(0, "supplier", ll_null.ToString());
			}
		}
		// TODO: Could not add event handler for handler 'clickedEvent'. (CODE=1017)
		public async Task cb_cancel_carton_clicked(object sender, EventArgs e) //********************************************************************
		{
			GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
			//	*Object:	            uo_mini_terminal_returns
			//	*Event Name:	   	cb_cancel_carton
			//	*Purpose: 	
			//	*Arguments: 	      (none)
			//	*Return:	            long
			//	*Date		    Programmer    Version	  Task#	       Description
			//	*---------------------------------------------------------------------
			//	*23/12/2013	 AlexKh        1.1.1.16	  SPUCM00004291 Initial version
			//********************************************************************
			
			decimal ldec_quantity_in_carton = default(decimal);
			string ls_state = null;
			long ll_row = 0;
			
			ll_row = dw_cartons.GetSelectedRow(0);
			if (ll_row > 0)
			{
				
				ldec_quantity_in_carton = dw_cartons.GetItemValue<decimal>(ll_row, "material_quantity");
				if (ldec_quantity_in_carton == 0)
				{
					ldec_quantity_in_carton = 0;
				}
				ls_state = dw_cartons.GetItemValue<string>(ll_row, "state");
				if (ls_state == mini_terminal.uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString() && ldec_quantity_in_carton == 0)
				{
					
					dw_cartons.Delete((int)ll_row);
				}
				else
				{
                    await this.ViewModel.uf_show_message("שגיאה", "", "OK", "לא ניתן לבטל קרטון לא ריק");
				}
			}
		}
	}
}
