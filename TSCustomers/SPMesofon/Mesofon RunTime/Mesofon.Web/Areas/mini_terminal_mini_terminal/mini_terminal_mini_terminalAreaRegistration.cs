using System.Web.Mvc;

namespace  mini_terminal.Areas.mini_terminal_mini_terminal
{
    public class mini_terminal_mini_terminalAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "mini_terminal_mini_terminal";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "mini_terminal_mini_terminal_default",
                "mini_terminal_mini_terminal/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "mini_terminal" }
            );
        }
    }
}