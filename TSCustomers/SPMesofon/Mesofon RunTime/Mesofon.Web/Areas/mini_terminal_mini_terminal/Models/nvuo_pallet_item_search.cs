﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using global;
using Mesofon.Common;
using Mesofon.Repository;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;


namespace mini_terminal
{
    // Creation Time:   08/11/2015 10:21:08 (Utc)
    // Comments:        
    // 
    public class nvuo_pallet_item_search : nvuo_doc_item_search
    {
        public u_datastore_update ids_pallet_inv_move;
        //.. pallet data from invoice_move
        public u_datastore_update ids_pallet_inv_details;
        //.. pallet data from invoice_details
        public IRepository ids_pallet_invoices;
        //.. list of invoice numbers for pallet
        public IRepository ids_shipment_items;
        public void uf_set_invoice_number(IRepository ads_invoices_list) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_set_invoice_number
            //*Purpose:							Set Pallet invoices information
            //*Arguments:							Datstore	ads_invoices_list
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            ids_pallet_invoices = ads_invoices_list;
            uf_retrieve();
        }
        public bool uf_retrieve() //********************************************************************
        {
            //*Object:				nvuo_pallet_item_search
            //*Function Name:	uf_retrieve
            //*Purpose: 			Retrieve all invoice details for the pallet
            //*Arguments: 		None
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*21-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_count = 0;
            long ll_employee_no = 0;
            long ll_i = 0;
            long ll_supplier_number = 0;
            long ll_invoice_number = 0;
            string ls_pallet_number = null;
            u_datastore_update lds_pallet_inv_move = default(u_datastore_update);
            u_datastore_update lds_pallet_inv_details = default(u_datastore_update);
            if (il_branch_number == 0)
            {
                return false;
            }

            if (ids_pallet_invoices.RowCount() == 0)
            {
                return false;
            }





            // gilad check
            ids_pallet_inv_move.Reset("ids_pallet_inv_move");
            ids_pallet_inv_details.Reset("ids_pallet_inv_details");
            lds_pallet_inv_move = new u_datastore_update();

            lds_pallet_inv_move.SetRepository(new d_sql_mini_terminal_input_headRepository());


            lds_pallet_inv_details = new u_datastore_update();

            lds_pallet_inv_details.SetRepository(new d_mini_terminal_pallet_detailsRepository());

            for (ll_i = 0; ll_i < ids_pallet_invoices.RowCount(); ll_i++)
            {

                ll_supplier_number = ids_pallet_invoices.GetItemValue<long>((int)ll_i, "supplier_number");

                ll_invoice_number = (long)ids_pallet_invoices.GetItemValue<double>((int)ll_i, "invoice_number");

                ls_pallet_number = ids_pallet_invoices.GetItemValue<string>(ll_i, "pallet_number");
                if (ll_supplier_number == 0)
                {
                    return false;
                }
                if (ll_invoice_number == 0)
                {
                    return false;
                }
                if (isempty_stringClass.isempty_string(ls_pallet_number))
                {
                    return false;
                }
                //.. If error occures save error number and message

                if (lds_pallet_inv_move.Retrieve(ll_invoice_number,ll_supplier_number, il_branch_number) < 0)
                {
                    il_last_sql_db_code = lds_pallet_inv_move.uf_get_last_sql_db_code();
                    is_last_sql_err_text = lds_pallet_inv_move.uf_get_last_sql_err_text();
                    return false;
                }
                ll_count = lds_pallet_inv_move.RowsMove(0, lds_pallet_inv_move.RowCount(), ModelBuffer.Primary, ids_pallet_inv_move, ids_pallet_inv_move.RowCount() + 1, ModelBuffer.Primary);

                ids_pallet_inv_move.ResetUpdate();

                if (ids_pallet_inv_move.RowCount() > 0)
                {


                    ll_employee_no = ids_pallet_inv_move.GetItemValue<long>(ids_pallet_inv_move.RowCount()-1, "employee_number");
                }
                if (ll_employee_no == 0)
                {


                    ids_pallet_inv_move.SetItem(ids_pallet_inv_move.RowCount()-1, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner);
                }
                //.. If error occures save error number and message

                if (lds_pallet_inv_details.Retrieve(ls_pallet_number, il_branch_number, ll_supplier_number, ll_invoice_number) < 0)
                {
                    il_last_sql_db_code = lds_pallet_inv_details.uf_get_last_sql_db_code();
                    is_last_sql_err_text = lds_pallet_inv_details.uf_get_last_sql_err_text();
                    return false;
                }
                ll_count = lds_pallet_inv_details.RowsMove(0, lds_pallet_inv_details.RowCount(), ModelBuffer.Primary, ids_pallet_inv_details, ids_pallet_inv_details.RowCount() + 1, ModelBuffer.Primary);

                ids_pallet_inv_details.ResetUpdate();
            }
            //.. Pallet data succesfully retrieved
            ib_retrieve_needed = false;



            return true;
        }
        public long uf_get_item_doc_number(long al_material_number, ref long[] al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_item_doc_number
            //*Purpose:							If material exists in invoice, then return the invoice or list of invoices
            //*Arguments:							Value			Long	al_material_number
            //*										Reference	Long	al_doc_number[]
            //*Return:								LONG
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_doc_num = 0;
            long ll_row = 0;
            long ll_count = 0;
            string ls_orig_filter = null;
            string ls_filter = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            ls_filter = "material_number = " + al_material_number.ToString();

            ll_row = ids_pallet_inv_details.SetFilter(ls_filter);

            ll_row = ids_pallet_inv_details.Filter();

            if (ids_pallet_inv_details.RowCount() == 1)
            {

                ll_doc_num = ids_pallet_inv_details.GetItemValue<long>(0, "invoice_number");

                ll_row = ids_pallet_inv_move.SetFilter("invoice_number == " + ll_doc_num.ToString() + " AND state == \"O\"");

                ll_row = ids_pallet_inv_move.Filter();

                if (ids_pallet_inv_move.RowCount() == 1)
                {
                    al_doc_number = new long[1];
                    al_doc_number[0] = ll_doc_num;
                }
            }

            else if (ids_pallet_inv_details.RowCount() > 1)
            {
                al_doc_number = new long[ids_pallet_inv_details.RowCount()];
                for (ll_row = 0; ll_row < ids_pallet_inv_details.RowCount(); ll_row++)
                {
                    var index = ids_pallet_inv_details.RowCount() - 1;

                    ll_doc_num = ids_pallet_inv_details.GetItemValue<long>((int)ll_row, "invoice_number");

                    ids_pallet_inv_move.SetFilter("invoice_number == " + ll_doc_num.ToString() + " AND state == \"O\"");

                    ids_pallet_inv_move.Filter();

                    if (ids_pallet_inv_move.RowCount() == 1)
                    {
                       // ll_count = CollectionExtensions.UBound(al_doc_number);
                        al_doc_number[ll_row] = ll_doc_num;
                    }

                    ids_pallet_inv_move.SetFilter("");

                    ids_pallet_inv_move.Filter();
                }
            }

            ll_row = ids_pallet_inv_details.SetFilter("");

            ll_row = ids_pallet_inv_details.Filter();

            ll_row = ids_pallet_inv_move.SetFilter("");

            ll_row = ids_pallet_inv_move.Filter();
            if (al_doc_number == null)
            {
                return 0;
            }
            else
            {
                return CollectionExtensions.UBound(al_doc_number) + 1;
            }
        }
        public bool uf_is_item_b2b(long al_material_number, long al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_is_item_b2b
            //*Purpose:							If material exists in invoice, check if it's b2b item
            //*Arguments:							Value			Long					   al_material_number
            //*										Value			Long					   al_doc_number
            //*Return:								LONG
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString() + " AND material_number == " + al_material_number.ToString() + " AND b2b_status = 1 ";


            ll_row = ids_pallet_inv_details.Find(ls_find, 0, ids_pallet_inv_details.RowCount());
            return (ll_row >= 0);
        }
        public decimal uf_get_doc_item_quantity(long al_material_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_doc_item_quantity
            //*Purpose:							get item quantity in invoice
            //*Arguments:							Long	al_material_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long? ll_row = 0;
            decimal ldec_material_quantity = default(decimal);
            decimal ldec_material_bonus_quantity = default(decimal);
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            ls_find = "material_number == " + al_material_number.ToString();


            ll_row = ids_pallet_inv_details.Find(ls_find, 0, ids_pallet_inv_details.RowCount());
            if (ll_row < 0)
            {
                return -1;
            }
            if (ll_row >= 0)
            {

                ldec_material_quantity = ids_pallet_inv_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
            }
            else
            {
            }
            return ldec_material_quantity;
        }
        public bool uf_get_doc_item_price(long al_doc_number, long al_material_number, ref decimal adec_material_price) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_doc_item_price
            //*Purpose:							Get invoice item price
            //*Arguments:							Long	al_doc_number
            //*										Long	al_material_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*01-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString() + " and material_number == " + al_material_number.ToString() + " and b2b_status = 1";


            ll_row = ids_pallet_inv_details.Find(ls_find, 0, ids_pallet_inv_details.RowCount());
            if (ll_row < 0)
            {
                return false;
            }
            if (ll_row >= 0)
            {

                adec_material_price = ids_pallet_inv_details.GetItemValue<int>((int)ll_row, "material_price");
            }
            return true;
        }
        public decimal uf_get_doc_total_amount(long al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_doc_total_amount
            //*Purpose:							Get invoice total amount
            //*Arguments:							Long	al_doc_number
            //*Return:								Decimal	
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*11-08-2010		AlexKh			1.2.46.1  	CR#1138 		Initial Version
            //************************************************************************************************

            decimal ldec_total_amount = default(decimal);
            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return 0;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString();


            ll_row = ids_pallet_inv_move.Find(ls_find, 0, ids_pallet_inv_move.RowCount());
            if (ll_row < 0)
            {
                return 0;
            }
            if (ll_row >= 0)
            {

                ldec_total_amount = ids_pallet_inv_move.GetItemValue<decimal>((int)ll_row, "expected_total_amount");
            }
            return ldec_total_amount;
        }
        public decimal uf_get_doc_discount_percent(long al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_doc_discount_percent
            //*Purpose:							Get invoice discount percent
            //*Arguments:							Long	al_doc_number
            //*Return:								Decimal	
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*11-08-2010		AlexKh			1.2.46.1  	CR#1138 		Initial Version
            //************************************************************************************************

            decimal ldec_discount_percent = default(decimal);
            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return 0;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString();


            ll_row = ids_pallet_inv_move.Find(ls_find, 0, ids_pallet_inv_move.RowCount());
            if (ll_row < 0)
            {
                return 0;
            }
            if (ll_row >= 0)
            {

                ldec_discount_percent = ids_pallet_inv_move.GetItemValue<decimal>((int)ll_row, "discount_percent");
            }
            return ldec_discount_percent;
        }
        public void uf_set_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_set_order_number
            //*Purpose:							Set order number
            //*Arguments:							Long	al_order_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*29-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            il_order_number = al_order_number;
        }
        public void uf_set_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_set_supplier_number
            //*Purpose:							Set supplier number
            //*Arguments:							Long	al_supplier_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*29-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            il_supplier_number = al_supplier_number;
        }
        public int uf_get_item_doc_number_by_supplier(long al_material_number, ref long[] al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_get_item_doc_number_by_supplier
            //*Purpose:							Find invoices with the same supplier like item main supplier 
            //*Arguments:							Value			Long	al_material_number
            //*										Reference			Long	al_doc_number[]
            //*Return:								LONG			number of documents
            //*Date 			Programer		Version		Task#				Description
            //*------------------------------------------------------------------------------------------------
            //*23-01-2011		AlexKh			1.2.46.6  	SPUCM00002425 	Initial Version
            //************************************************************************************************

            long ll_doc_num = 0;
            long ll_row = 0;
            long ll_count = 0;
            long ll_supplier_number = 0;
            string ls_orig_filter = null;
            string ls_filter = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            if (true)
            {
                LoadData(al_material_number, ref ll_supplier_number);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                return 0;
            }
            ls_filter = "supplier_number = " + ll_supplier_number.ToString();

            ll_row = ids_pallet_inv_move.SetFilter(ls_filter);

            ll_row = ids_pallet_inv_move.Filter();

            if (ids_pallet_inv_move.RowCount() > 0)
            {

                for (ll_row = 1; ll_row <= ids_pallet_inv_move.RowCount(); ll_row++)
                {

                    ll_doc_num = ids_pallet_inv_move.GetItemValue<long>((int)ll_row, "invoice_number");
                    ll_count = CollectionExtensions.UBound(al_doc_number);
                    al_doc_number[ll_count + 1] = ll_doc_num;
                }
            }

            ll_row = ids_pallet_inv_move.SetFilter("");

            ll_row = ids_pallet_inv_move.Filter();
            return CollectionExtensions.UBound(al_doc_number);
        }
        public int uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_item_search
            //*Function/Event  Name:			uf_show_message
            //*Purpose:							Show message.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 			String 				as_title
            //*										Value 			String 				as_error_text
            //*										Value 			String 				as_buttons
            //*										Value 			String 				as_message
            //*		
            //*Return:								Integer:1 - Success
            //*												  -1 - Otherwise
            //*Date		     	Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007    SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;

            li_rtn_code = Convert.ToInt32(w_mini_terminal.Instance().wf_show_message(as_title, as_error_text, as_buttons, as_message));
            // AlexKh - 1.2.44.1 - 2009-05-13 - SPUCM00001250 - return function returned value
            //RETURN 1
            return li_rtn_code;
        }
        public async Task<FuncResults<string, bool>> uf_create_decline_for_item(long al_material_number, long al_shipment_number, string as_pallet_number, long al_supplier_number, long al_marlog_branch, string as_message, long al_marlog_distributor)
        {
            //********************************************************************
            //*Object:				nvuo_pallet_item_search
            //*Function Name:	uf_create_decline_for_item
            //*Purpose: 			Check if barcode exists in any pallet of current shipment
            //*Arguments: 		None
            //*Return:				TRUE/FALSE
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*12-01-2015		AlexKh			1.1.26.0	SPUCM00005189	Initial version
            //********************************************************************

            int li_rtn_code = 0;
            long ll_rowcount = 0;
            long? ll_found = 0;
            long ll_decline_exist = 0;
            string ls_find = null;
            string ls_pallet_number = null;
            s_array_arguments lstr_mess = new s_array_arguments();
            if (!(ids_shipment_items != null))
            {
                ids_shipment_items = new d_mini_terminal_shipment_detailsRepository();


            }
            else
            {
                ll_rowcount = ids_shipment_items.RowCount();
            }
            if (ll_rowcount > 0)
            {

                if (al_shipment_number != (long)ids_shipment_items.GetItemValue<double>(0, "shipment_number"))
                {

                    ll_rowcount = ids_shipment_items.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_shipment_number, al_marlog_branch);
                }
            }
            else
            {

                ll_rowcount = ids_shipment_items.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_shipment_number, al_marlog_branch);
            }
            if (ll_rowcount > 0)
            {
                ls_find = "pallet_number == \"" + as_pallet_number + "\"";

                ll_found = ids_shipment_items.Find(ls_find, 0, (int)ll_rowcount);
                if (ll_found >= 0)
                {
                    lstr_mess.a_string[1] = "I"; //parent_doc_type 
                    lstr_mess.a_long[1] = 1; //decline_level

                    lstr_mess.a_long[2] = (long)ids_shipment_items.GetItemValue<double>((int)ll_found, "order_number"); //order_number 

                    lstr_mess.a_long[3] = ids_shipment_items.GetItemValue<long>((int)ll_found, "supplier_number"); //supplier_number 

                    lstr_mess.a_long[4] = (long)ids_shipment_items.GetItemValue<double>((int)ll_found, "invoice_number"); //parent_doc_number
                    lstr_mess.a_long[5] = al_material_number; //material_number 
                    lstr_mess.a_long[6] = masofonAlias.masofon.Instance.gs_vars.active_owner; //employee_number
                    lstr_mess.a_long[7] = 0; //serial_number
                    lstr_mess.a_long[8] = 0; //doc_quantity
                    lstr_mess.a_long[9] = 0; //actual_quantity
                    lstr_mess.a_long[10] = al_marlog_distributor;
                    lstr_mess.a_boolean[0] = true; //Over supplied item indicator

                    rw_declines_reasons win = await WindowHelper.Open<rw_declines_reasons>("global_global", "args", lstr_mess);

                    ll_decline_exist = win.ll_decline_exist_return;
                }
                else
                {
                    return FuncResults.Return(as_message, false);

                }
            }
            return FuncResults.Return(as_message, true);
        }
        public bool uf_set_material_prices(long al_material_number, ref string as_message, ref decimal adec_material_price, ref decimal adec_sell_price) //********************************************************************
        {
            //*Object:				nvuo_pallet_item_search
            //*Function Name:	uf_set_material_prices
            //*Purpose: 			Add item to the invoice
            //*Arguments: 		None
            //*Return:				TRUE/FALSE
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*25-01-2015		AlexKh			1.1.26.0	SPUCM00005189	Initial version
            //********************************************************************

            decimal vat_percentage = default(decimal);
            decimal ldec_active_price = default(decimal);
            decimal ld_global_discount_percent = default(decimal);
            decimal ldec_material_discount = default(decimal);
            decimal ldec_supplier_discount = default(decimal);
            decimal ldec_percent = default(decimal);
            decimal ldec_price_before = default(decimal);
            decimal ldec_price_after = default(decimal);
            decimal ldec_sell_price = default(decimal);
            long[] la_suppliers = new long[6];
            long ll_index = 0;
            long ll_first_free = 0;
            long vat_number = 0;
            string ls_syntax = null;
            string ls_material_name = null; //, ls_discount_amount
        //    bool lb_ok = false;
            if (true)
            {
                LoadData1(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                as_message = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                return false;
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100) // get the last prices received 
            {
                LoadData2(al_material_number, ref ldec_price_before, ref ldec_price_after, ref ldec_material_discount, ref ldec_supplier_discount);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    as_message = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                    return false;
                }
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                ldec_price_before = 0;
                ldec_price_after = 0;
                ldec_active_price = 0;
            }
            else
            {
                if (ldec_price_before == 0)
                {
                    ldec_price_before = 0;
                }
                if (ldec_price_after == 0)
                {
                    ldec_price_after = 0;
                }
                if (ldec_material_discount == 0)
                {
                    ldec_material_discount = 0;
                }
                if (ldec_supplier_discount == 0)
                {
                    ldec_supplier_discount = 0;
                }
                if (ldec_price_after == 0 && (ldec_price_before != 0 && (ldec_material_discount == 0 && ldec_supplier_discount == 0)))
                {
                    ldec_price_after = ldec_price_before;
                }
                ldec_active_price = ldec_price_after;
            }
            //dw_inv_pack_details.SetItem(row, "material_price", ldec_active_price)
            adec_material_price = ldec_active_price;
            if (true)
            {
                LoadData3(al_material_number, ref ldec_sell_price);
            }


            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0 || masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
            {

                as_message = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                return false;
            }
            //dw_inv_pack_details.SetItem(row, "sell_price", ldec_sell_price)
            adec_sell_price = ldec_sell_price;
            return true;
        }
        public async Task<FuncResults<string, bool>> uf_add_item_to_invoice(long al_invoice_number, long al_material_number, long al_supplier_number, string as_message) //********************************************************************
        {
            //*Object:				nvuo_pallet_item_search
            //*Function Name:	uf_add_item_to_invoice
            //*Purpose: 			Add item to the invoice
            //*Arguments: 		None
            //*Return:				TRUE/FALSE
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*25-01-2015		AlexKh			1.1.26.0	SPUCM00005189	Initial version
            //********************************************************************

            long ll_serial = 0;
            string ls_details = null;
            decimal ldc_price = default(decimal);
            decimal ldc_sell_price = default(decimal);
            decimal adc_qty = default(decimal);
            this.uf_set_material_prices(al_material_number, ref as_message, ref ldc_price, ref ldc_sell_price);
            // AlexKh - 1.1.27.0 - 2015-02-04 - SPUCM00005224 - get item quantity

            rw_item_quantity win = await WindowHelper.Open<rw_item_quantity>("mini_terminal_mini_terminal");

            adc_qty = WindowHelper.GetParam<decimal>(win);
            if (adc_qty <= 0)
            {
                return FuncResults.Return(as_message, false);
            }
            ls_details = "<Excess> EmpNo<" + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + ">";
            ll_serial = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(masofonAlias.masofon.Instance.gs_vars.branch_number, 4);
            UpdateData(ll_serial, al_invoice_number, al_material_number, adc_qty, ldc_price, ls_details, al_supplier_number, ldc_sell_price);

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {
                return FuncResults.Return(as_message, false);
            }
            return FuncResults.Return(as_message, true);
        }
        public int uf_check_item_in_shipment(long al_material_number, long al_shipment_number, string as_pallet_number, long al_supplier_number, long al_marlog_branch, ref string as_message, ref long al_invoice_number)
        {
            //********************************************************************
            //*Object:				nvuo_pallet_item_search
            //*Function Name:	uf_check_item_in_shipment
            //*Purpose: 			Check if barcode exists in any pallet of current shipment
            //*Arguments: 		None
            //*Return:				TRUE/FALSE
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*12-01-2015		AlexKh			1.1.26.0	SPUCM00005189	Initial version
            //********************************************************************

            int li_rtn_code = 0;
            long ll_rowcount = 0;
            long? ll_found = 0;
            string ls_find = null;
            string ls_pallet_number = null;
            //Check item exist in shipment in another pallet
            if (!(ids_shipment_items != null))
            {
                ids_shipment_items = new d_mini_terminal_shipment_detailsRepository();

            }
            else
            {
                ll_rowcount = ids_shipment_items.RowCount();
            }
            if (ll_rowcount > 0)
            {
                if (al_shipment_number != (long)ids_shipment_items.GetItemValue<double>(0, "shipment_number"))
                {
                    ll_rowcount = ids_shipment_items.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_shipment_number, al_marlog_branch);
                }
            }
            else
            {
                ll_rowcount = ids_shipment_items.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, al_shipment_number, al_marlog_branch);
            }
            if (ll_rowcount > 0)
            {
                ls_find = "item_number == " + al_material_number.ToString();
                ll_found = ids_shipment_items.Find(ls_find, 0, (int)ll_rowcount);
                if (ll_found >= 0)
                {
                    ls_pallet_number = ids_shipment_items.GetItemValue<string>(ll_found.Value, "pallet_number");
                    as_message = " הפריט שייך למשטח " + ls_pallet_number;
                    //uf_show_message("שים לב", "", "OK", as_message)
                    return 1; // found in other pallet
                    //Item Not Exist in other pallets, so check if exist invoice with the same supplier in the shipment
                }
                else
                {
                    ls_find = "supplier_number == " + al_supplier_number.ToString();
                    ll_found = ids_shipment_items.Find(ls_find, 0, (int)ll_rowcount);
                    if (ll_found >= 0) //
                    {
                        al_invoice_number = Convert.ToInt64(ids_shipment_items.GetItemValue<double>((int)ll_found, "invoice_number"));
                        return 3;
                        // There is no invoice of the same supplier
                    }
                    else
                    {
                        //Create decline on one of the invoices in pallet
                        return 2;
                    }
                }
            }
            return 0;
        }
        public nvuo_pallet_item_search()
            : base()
        {
            // connect to sqlca by default
            i_transaction = masofonAlias.masofon.Instance.sqlca;
            ids_pallet_inv_move = new u_datastore_update();
            ids_pallet_inv_move.SetRepository(new d_sql_mini_terminal_input_headRepository(),false);
            ids_pallet_inv_details = new u_datastore_update();
            ids_pallet_inv_details.SetRepository(new d_mini_terminal_pallet_detailsRepository(),false);
            
            ids_pallet_invoices = new d_mini_terminal_pallet_invoicesRepository();
            

        }
        public void destructor()
        {



            if (ids_pallet_inv_move != null)
            {

      

            }
            if (ids_pallet_inv_details != null)
            {



            }
            if (ids_pallet_invoices != null)
            {

    

            }
        }
        public void LoadData(Int64 al_material_number, ref long ll_supplier_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT IsNull(supplier_number, 0) FROM materials WHERE (materials.number = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT MAX(b.price_before_discount),  MAX(b.price_after_discount),  MAX(b.material_discount),  MAX(b.supplier_discount_percent) FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 al_material_number, ref decimal ldec_sell_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT sell_price FROM branch_item_price WHERE branch_number = @0 AND item_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_sell_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(long ll_serial, Int64 al_invoice_number, Int64 al_material_number, decimal adc_qty, decimal ldc_price, string ls_details, Int64 al_supplier_number, decimal ldc_sell_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", al_invoice_number }, { "@3", al_material_number }, { "@4", adc_qty }, { "@5", ldc_price }, { "@6", ls_details }, { "@7", al_supplier_number }, { "@8", ldc_sell_price } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  material_price_after_discount,  packing_list_number,  packing_serial,  stock_number,  decline_number,  sell_price,  bonus_quantity,  material_bonus_connect,  material_discount_amount,  color,  bonus_discount,  indicator,  supplier_discount_percent,  expected_material_quantity,  mini_terminal) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  @6,  @7,  0,  @5,  0,  0,  1,  0,  @8,  0,  0,  0,  255,  0,  1,  0,  0,  1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void ResetPallet()
        {
            ids_pallet_inv_details.ResetUpdate();
        }
    }
}
