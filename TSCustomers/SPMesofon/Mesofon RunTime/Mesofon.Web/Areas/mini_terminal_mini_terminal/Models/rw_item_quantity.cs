using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using mini_terminalAlias = mini_terminal;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:08 (Utc)
	// Comments:        
	// 
	public class rw_item_quantity : WindowElement
	{
		public long il_branch_number;
		public long il_supplier_number;
		public long il_order_number;
		public long il_item_number;
		public long il_adhoc_serial_number;
		public long il_row_serial_number;
		public const long TEMPORARY_LOCKED = 2;
		public const long FINALY_LOCKED = 3;
		public const long UNLOCKED = 1;
		public string is_adhoc_key;
		public bool ib_return_value;
		public IRepository ids_adhoc_update_list;
		public delegate void enterdown_EventHandler();
		public event mini_terminalAlias.rw_item_quantity.enterdown_EventHandler enterdownEvent;
		public rw_item_quantity()
		{
		}

        

    }
}
