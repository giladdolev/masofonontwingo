using System;
using System.Drawing;
using System.Web.VisualTree.Elements;
using global;
using Mesofon.Common;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:10 (Utc)
	// Comments:        
	// 
	public class uo_mini_terminal_crossdoc_pack_list : CompositeElement
	{
		public const char NEW_NOT_APPROVED_MARLOG = 'n';
		public const char NEW_APPROVED_MARLOG = 'a';
		public const char SENT_BY_MARLOG = 's';
		public const char SCANNED_BY_BRANCH = 'c';
		public long il_employee_number;
		public nvuo_b2b_crossdoc invuo_b2b_crossdoc;
		// AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch_number
		public long il_marlog_branch;
		public long il_marlog_distributor;
		public uo_mini_terminal_crossdoc_pack_list()
		{
            this.invuo_b2b_crossdocProperty = new nvuo_b2b_crossdoc();
        }
	    public void constructor() // AlexKh - 1.2.48.15 - 2012-02-29 - CR#1152  - constructor
	    {
	        GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
	        long ll_ret = 0;
	    
	    }



        public long il_employee_numberProperty
		{
			get { return this.il_employee_number; }
			set { this.il_employee_number = value; }
		}
		public nvuo_b2b_crossdoc invuo_b2b_crossdocProperty
		{
			get { return this.invuo_b2b_crossdoc; }
			set { this.invuo_b2b_crossdoc = value; }
		}
		public long il_marlog_branchProperty
		{
			get { return this.il_marlog_branch; }
			set { this.il_marlog_branch = value; }
		}
	}
}

