using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using global;
using masofon;
using System.Web.VisualTree.MVC;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Common.Transposition.Extensions;
using Mesofon.Common;
using Mesofon.Repository;
using MvcSite.Common;
namespace mini_terminal
{
    // Creation Time:   08/11/2015 10:21:10 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_body : uo_mini_terminal_base_body
    {
        public u_order_search iuo_order_search;
        private bool _preventLoadEvent;

        public bool PreventLoadEvent { get { return _preventLoadEvent; } set { _preventLoadEvent = value; } }

        public WidgetGridElement dw_inv_pack_details
        {
            get { return this.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details"); }
        }
        public GridElement dw_summary
        {
            get { return this.GetVisualElementById<GridElement>("dw_summary"); }
        }

        public async Task<int> uf_set_quantity_and_doc_no(string as_expected_material_quantity, long al_row) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_set_quantity_and_doc_no
            //*Purpose:							Set the order and - invoice or packing list quantety 
            //*										Also set the doc number .
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										---------------------------------------------------------------------------------------------
            //*										Value			String					as_expected_material_quantity - the quantity the user has been entered
            //*										Value			Long					al_row
            //*		
            //*Return:								Integer:  1 - Success
            //*													-1 - Otherwise 
            //*
            //*Date 				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007			SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            int li_b2b_status = 0;
            int li_doc_no_en = 0;
            int li_status = 0;
         //   char lc_doc_type = '\0';
            string lc_doc_state = "";
            string ls_find = null;
            string ls_find_dw = null;
            string ls_barcode = null;
            long ll_found = 0;
            long? ll_found_dw = 0;
            long ll_b2b_status = 0;
            long ll_row_count = 0;
            long? ll_doc_no = 0;
            long ll_current_doc_no = 0;
            long ll_doc_no_en = 0;
            long[] ll_doc_number = new long[2];
            long ll_docs_count = 0;
            long ll_b2b_docs_count = 0;
            long ll_material_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_no_of_rows = 0;
            long ll_row = 0;
            long ll_no_of_rows_display = 0;
            long ll_no_of_rows_filtered = 0;
            long ll_row_no = 0;
            long ll_filtered_row_no = 0;
            long ll_displayed_row_no = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_cntr = 0;
            decimal ldc_order_quantity = default(decimal);
            decimal ldc_invoice_pack_quantity = default(decimal);
            decimal ldec_rtn_code = default(decimal);
            bool lb_is_item_b2b = false;
            bool lb_need_set_item = false;
            ModelAction ldw_status = default(ModelAction);
            GridColumn ldwc_docs;
            // AlexKh - 1.2.48.34 - 2012-12-05 - SPUCM00003402 - check if negative value, then set zero
            if (Convert.ToDecimal(as_expected_material_quantity) < 0)
            {
                as_expected_material_quantity = "0";
            }

            ll_no_of_rows_display = this.dw_inv_pack_details.RowCount() - 1;
            ll_no_of_rows_filtered = this.dw_inv_pack_details.FilteredCount();
            ll_row_no = ll_no_of_rows_display + ll_no_of_rows_filtered;
            ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
            ll_material_number = await this.iuo_material_search.uf_get_material_number(is_new_barcode);

            ldwc_docs = GetDocNo(dw_inv_pack_details);
            //Set order_quantity 
            ldc_order_quantity = iuo_order_searchProperty.uf_get_order_material_quantity(ll_material_number);
            if (ldc_order_quantity == -1)
            {
                await this.uf_show_message("Error", "", "OK", "שגיאה בשליפת כמות הפריט בהזמנה");
            }

            dw_inv_pack_details.SetItem(al_row, "order_quantity", ldc_order_quantity);
            li_rtn_code = (await this.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            li_rtn_code = (await this.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);


            li_b2b_status = Convert.ToInt32(this.dw_inv_pack_details.GetItemValue<long>(al_row, "b2b_status"));
            if (li_b2b_status == 1)
            {
                lb_is_item_b2b = true;
            }
            //Get invoice_pack_quantity and doc_no
            ldec_rtn_code = Convert.ToDecimal(((w_mini_terminal)this.Parent.Parent).wf_get_doc_item_quantity(ll_material_number));
            if (lb_is_item_b2b)
            {
                //Case of B2B
                //Set invoice_pack_quantity and doc_no

                dw_inv_pack_details.SetItem(al_row, "invoice_pack_quantity", (Convert.ToDecimal(as_expected_material_quantity)).ToString());

                dw_inv_pack_details.SetItem(al_row, "material_quantity", (Convert.ToDecimal(as_expected_material_quantity)).ToString());


                ll_b2b_docs_count = Convert.ToInt64(((w_mini_terminal)this.Parent.Parent).wf_get_item_doc_number(ll_material_number, ref ll_doc_number));


                li_doc_no_en = Convert.ToInt32(((w_mini_terminal)this.Parent.Parent).wf_get_doc_no_en(al_row, ll_material_number).Result);

                this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", li_doc_no_en.ToString());
                if (ll_b2b_docs_count == 0)
                {
                    if (li_doc_no_en > 1)
                    {


                        ldw_status = this.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);

                        if (this.dw_inv_pack_details.RowCount() == 1 && (al_row == 1 && ldw_status == ModelAction.Insert))
                        {
                            await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");
                            await this.uf_build_doc_number_list();

                            ll_doc_no = default(long);

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                        }
                        else
                        {
                            if (al_row != 0)
                            {

                                ll_doc_no = this.dw_inv_pack_details.GetItemValue<long>(al_row - 1, "doc_no");
                            }
                            // AlexKh - 1.2.43.1 - 2009-04-05 - SPUCM00000950 - if new item does't belong to invoice, then give a message
                            li_status = await uf_is_item_in_invoice(ll_doc_no.Value, ll_material_number);
                            if (li_status == -1)
                            {

                                ll_doc_no = default(long);

                                this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());

                                this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", "2");
                            }
                            else
                            {
                                // AlexKh - 2009-04-05 - End

                                this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                                lb_need_set_item = true;
                            }
                            for (ll_cntr = -1; ll_cntr <= ll_no_of_rows_display; ll_cntr++)
                            {

                                if (ll_cntr != al_row && dw_inv_pack_details.GetItemValue<long>(ll_cntr + 1, "material_number") == ll_material_number)
                                {
                                    await this.uf_build_doc_number_list();
                                    await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }
                        }
                    }
                    else
                    {
                        ll_doc_no = ll_doc_number[0];
                        // AlexKh - 1.2.43.1 - 2009-04-05 - SPUCM00000950 - if new item does't belong to invoice, then give a message
                        li_status = await uf_is_item_in_invoice(ll_doc_no.Value, ll_material_number);
                        if (li_status == -1)
                        {

                            ll_doc_no = default(long);

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", "2");
                        }
                        else
                        {
                            // AlexKh - 2009-04-05 - End

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                            lb_need_set_item = true;
                        }
                    }
                }
                else if (ll_b2b_docs_count > 1)
                {
                    await this.uf_build_doc_number_list();
                    await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");

                    ll_doc_no = this.dw_inv_pack_details.GetItemValue<long>(al_row, "doc_no");
                    if (ll_doc_no == 0 || ll_doc_no == 0)
                    {
                        if (al_row == 1)
                        {
                            ll_row = 1;
                        }
                        else
                        {
                            ll_row = al_row - 1;
                        }

                        lc_doc_state = this.dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state");
                        if (lc_doc_state == "O")
                        {

                            ll_doc_no = this.dw_inv_pack_details.GetItemValue<long>(ll_row, "doc_no");
                        }
                        else
                        {

                            ll_docs_count = ldwc_docs.GetRepository().RowCount();
                            if (ll_docs_count > 0)
                            {

                                ll_doc_no = ldwc_docs.GetRepository().GetItemValue<long>(0, "doc_number");
                            }
                        }
                        // AlexKh - 1.2.43.1 - 2009-04-05 - SPUCM00000950 - if new item does't belong to invoice, then give a message
                        li_status = await uf_is_item_in_invoice(ll_doc_no.Value, ll_material_number);
                        if (li_status == -1)
                        {

                            ll_doc_no = default(long);

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", "2");
                        }
                        else
                        {
                            // AlexKh - 2009-04-05 - End

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                            lb_need_set_item = true;
                        }
                    }
                    // End
                    return 1;
                }
                else
                {
                    return 1;
                }


                dw_inv_pack_details.SetItem(al_row, "b2b_status", 1);
            }
            else
            {
                //Not B2B

                dw_inv_pack_details.SetItem(al_row, "invoice_pack_quantity", (Convert.ToDecimal(as_expected_material_quantity)));

                dw_inv_pack_details.SetItem(al_row, "material_quantity", (Convert.ToDecimal(as_expected_material_quantity)));


                dw_inv_pack_details.SetItem(al_row, "b2b_status", 0);
                //Set the doc_no
                if (ll_row_no > 0)
                {


                    li_doc_no_en = Convert.ToInt32(((w_mini_terminal)this.Parent.Parent).wf_get_doc_no_en(al_row, ll_material_number).Result);

                    this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", li_doc_no_en.ToString());
                    await this.uf_build_doc_number_list();

                    ll_doc_no = this.dw_inv_pack_details.GetItemValue<long?>(al_row, "doc_no");
                    if (ll_doc_no.Equals(null))
                    {
                        ll_doc_no = 0;
                    }
                    if (ll_doc_no == 0)
                    {
                        if (al_row == 0)
                        {
                            ll_row = 0;
                        }
                        else
                        {
                            ll_row = al_row - 1;
                        }

                        lc_doc_state = this.dw_inv_pack_details.GetItemValue<string>(ll_row, "doc_state");
                        if (lc_doc_state == "O")
                        {

                            ll_doc_no = this.dw_inv_pack_details.GetItemValue<long>(ll_row, "doc_no");
                        }
                        else
                        {

                            ll_docs_count = ldwc_docs.GetRepository().RowCount();
                            if (ll_docs_count > 0)
                            {

                                ll_doc_no = ldwc_docs.GetRepository().GetItemValue<long>(0, "doc_number");
                            }
                        }
                        ls_find_dw = "material_number == " + ll_material_number.ToString();
                        ls_find_dw += " and doc_state == \"O\"";


                        ll_found_dw = this.dw_inv_pack_details.Find(ls_find_dw, 0, this.dw_inv_pack_details.RowCount());
                        if (ll_found_dw != -1 && (ll_found_dw != al_row && li_doc_no_en != 1))
                        {
                            await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");
                        }
                        // AlexKh - 1.2.43.1 - 2009-04-05 - SPUCM00000950 - if new item does't belong to invoice, then give a message
                        li_status = await uf_is_item_in_invoice(ll_doc_no.Value, ll_material_number);
                        if (li_status == -1)
                        {

                            ll_doc_no = default(long);

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                            //var JsonData = GridMultiComboBoxElement.GetComboBoxData(ll_doc_no.ToString(), ldwc_docs.GetDataObject().GetDataTable());
                            //dw_inv_pack_details.Rows[(int)al_row].Cells["doc_no"].Value = JsonData;

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", "2");
                        }
                        else
                        {
                            // AlexKh - 2009-04-05 - End

                            this.dw_inv_pack_details.SetItem(al_row, "doc_no", ll_doc_no.ToString());
                            //var JsonData = GridMultiComboBoxElement.GetComboBoxData(ll_doc_no.ToString() , ldwc_docs.GetDataObject().GetDataTable());
                            //dw_inv_pack_details.Rows[(int)al_row].Cells["doc_no"].Value = JsonData;
                            lb_need_set_item = true;
                        }
                    }
                    // End
                }
                else
                {
                    //First row
                    //Build the list box values
                    if (await this.uf_build_doc_number_list() < 1)
                    {
                        return -1;
                    }

                    ll_docs_count = ldwc_docs.GetRepository().RowCount();
                    ll_doc_no = 0;
                    // Set doc_no_en

                    this.dw_inv_pack_details.SetItem(al_row, "doc_no_en", 2);
                    await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");
                    // End
                }
            }
            // Call the function wf_set_items
            if (lb_need_set_item)
            {
                await this.uf_set_items("doc_no", ll_doc_no.ToString(), al_row);
            }
            return 1;
        }
        public override async Task<int> uf_set_items(string as_column_name, string as_data, long al_row) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_set_items
            //*Purpose:							Call the function wf_set_items in the window.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										-------------------------------------------------------------------------
            //*										Value 			string 				as_column_name
            //*										Value 			string 				as_data
            //*										Value 			Long	 				al_row
            //*		
            //*Return:								INTEGER :  -1 failed
            //*														1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14/10/2007		SHARP.    				0.2.0 			B2B			Initial version
            //************************************************************************************************



            return Convert.ToInt32(await ((w_mini_terminal)this.Parent.Parent).wf_set_items(as_column_name, as_data, al_row));
        }
        public async Task<int> uf_is_item_in_invoice(long al_doc_number, long al_material_number) // AlexKh - 1.2.43.1 - 2009-04-02 - SPUCM00000950 - call to the parent window function
        {
            long ll_status = 0;


            ll_status = Convert.ToInt64((Parent.Parent as w_mini_terminal).wf_is_item_in_invoice(al_doc_number, al_material_number).Result);
            if (ll_status == -1)
            {
                if (await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OkCancel", 1, " הפריט לא קיים בחשבונית " + al_doc_number.ToString() + ", האם לשייך?") != 1)
                {
                    return -1;
                }
            }
            return 1;
        }
        public override async Task<FuncResults<long, int>> uf_get_selected_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_get_selected_supplier_number
            //*Purpose:							Get the supplier number that the user has selected.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Reference	Long						al_supplier_number
            //*		
            //*Return:								Integer: 1 - Success
            //*												   -1 - Failure
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************


            int returnValue = (await ((w_mini_terminal)this.Parent.Parent).wf_get_selected_supplier_number(al_supplier_number)).Retrieve(out al_supplier_number);
            return FuncResults.Return(al_supplier_number, returnValue);
        }
        public override async Task<FuncResults<long, int>> uf_get_selected_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_get_selected_order_number
            //*Purpose:							Get the order number that the user has selected.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Reference	Long						al_order_number
            //*		
            //*Return:								Integer: 1 - Success
            //*												   -1 - Failure
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************


            int returnValue = (await ((w_mini_terminal)this.Parent.Parent).wf_get_selected_order_no(al_order_number)).Retrieve(out al_order_number);
            return FuncResults.Return(al_order_number, returnValue);
        }
        public async Task<long> uf_get_max_serial_number(long al_doc_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_get_max_serial_number
            //*Purpose:							Calculate the serial number
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 		Long						al_doc_number
            //*		
            //*Return:								Long: max_serial_number
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-08-2007		SHARP.   				0.2.0   		B2B 			Initiation
            //************************************************************************************************

            long ll_max_serial_number = 0;
            long ll_doc_no = 0;
            char lc_doc_type = '\0';


            lc_doc_type = Convert.ToChar((Parent.Parent as w_mini_terminal).wf_get_doc_type(al_doc_number).Result);
            if (lc_doc_type == 'I')
            {
                //Invoice

                if (dw_inv_pack_details.RowCount() > 0)
                {
                    masofonAlias.masofon.Instance.sqlca = new SQLError();
                    await f_begin_tranClass.f_begin_tran();
                    ll_max_serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(4);
                    await f_commitClass.f_commit();
                }
            }
            else if (lc_doc_type == 'P')
            {
                //Packing_list
                await f_begin_tranClass.f_begin_tran();
                ll_max_serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(27);
                await f_commitClass.f_commit();
            }
            else if (lc_doc_type == 'E')
            {
                //Error
            }
            else if (lc_doc_type == default(char))
            {
                //Not found
            }
            return ll_max_serial_number;
        }
        public override async Task<char> uf_get_doc_type(long? al_doc_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_get_doc_type
            //*Purpose:							Get document type: I for Invoice, P for PackList
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long						al_doc_number
            //*		
            //*Return:								Character: 'I' - Invoice
            //*													   'P' - PackList 
            //*													   'E' - Error
            //*													   '' - Not found
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            char lc_doc_type = '\0';


            lc_doc_type = Convert.ToChar(((w_mini_terminal)this.Parent.Parent).wf_get_doc_type(al_doc_number).Result);
            //Display message box
            if (lc_doc_type == 'E')
            {
                await this.uf_show_message("שגיאה", "", "OK", ".שגיאה בשליפת סוג מסמך");
            }
            else if (lc_doc_type == default(char))
            {
                await this.uf_show_message("שגיאה", "", "OK", ".מספר מסמך לא קיים");
            }
            return lc_doc_type;
        }
        public override async Task<int> uf_build_doc_number_list() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_build_doc_number_list
            //*Purpose:							Initiate the doc number list box with the invoices numbers and packung list numbers 
            //*										from uo_mini_terminal_header.dw_body
            //*  
            //*Arguments:						None.
            //*Return:								Integer: 1 - Success 
            //*												   -1 - Otherwise
            //*DAte				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;


            li_rtn_code = Convert.ToInt32(await ((w_mini_terminal)this.Parent.Parent).wf_build_doc_number_list());
            if (li_rtn_code < 1)
            {
                return -1;
            }
            return 1;
        }

        public override async Task<int> uf_new_row(WidgetGridElement dw_inv_pack_details) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_new_row
            //*Purpose:							Add new row in dw_inv_pack_details.
            //*  
            //*Arguments:						None 
            //*		
            //*Return:								Integer: -1 Failed
            //*													1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-08-2007		SHARP.   				0.2.0   		B2B 			Initiation
            //************************************************************************************************

            int li_rtn = 0;
            long ll_open_docs = 0;
            GridColumn ldwc_docs;


            //li_rtn = Convert.ToInt32((Parent as w_mini_terminal).wf_build_doc_number_list());
            li_rtn = await ((w_mini_terminal)this.Parent.Parent).wf_build_doc_number_list();
            if (li_rtn == 1)
            {

                ldwc_docs = GetDocNo(dw_inv_pack_details);

                ll_open_docs = ldwc_docs.GetRepository().RowCount();
                if (ll_open_docs == 0)
                {
                    await uf_show_message("!שים לב", "", "OK", "אין מסמכים פתוחים.");
                    return 1;
                }
            }

            return await base.uf_new_row(dw_inv_pack_details);
        }
        public override async Task<FuncResults<decimal, decimal, decimal, long, int>> uf_get_order_material_price(long al_supplier_number, long al_order_number, long al_material_number, decimal adec_material_price, decimal adec_material_price_after_discount, decimal adec_material_discount_percent, long al_error_number)
        {
            //****************************************************************************************************************
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_get_order_material_price
            //*Purpose:							Get the material's prices according to the following:
            //*										IF order_number exists and ib_get_material_price_from_order_if_it_exists = TRUE,
            //*										gets material-prices from supplier_order_details table.
            //*										Otherwise gets material-prices from material_supplier table.
            //*  
            //*Arguments:						Pass By			Argument Type		Argument Name								Remark
            //*										-------------------------------------------------------------------------------------------------------------------
            //*										Value				Long					al_supplier_number
            //*										Value				Long					al_order_number
            //*										Value				Long					al_material_number							current material number
            //*										Reference 		Decimal 				adec_material_price
            //*										Reference 		Decimal 				adec_material_price_after_discount	
            //*										Reference 		Decimal 				adec_material_discount_percent
            //*										Reference 		Long		 			al_error_number								-1 - If error occurce, 
            //*																																		100 - If material not assigned to order  
            //*																																		0 - Otherwise 
            //*		
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------------------------------
            //*06-08-2007		SHARP.					0.2.0   		B2B 			Initiation
            //****************************************************************************************************************

            bool lb_material_in_order = false;
            if (al_material_number > 0)
            {
                if (!(al_order_number == 0 || (al_order_number == 0 || (al_supplier_number == 0 || al_supplier_number == 0))))
                {
                    lb_material_in_order = iuo_order_searchProperty.uf_get_order_material_price(al_material_number, ref adec_material_price, ref adec_material_price_after_discount, ref adec_material_discount_percent);
                    if (!lb_material_in_order)
                    {
                        //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                        adec_material_price = (await iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                        adec_material_price_after_discount = adec_material_price;
                    }
                }
                else
                {
                    //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                    adec_material_price = (await iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                    adec_material_price_after_discount = adec_material_price;
                }
            }
            else
            {
                //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                adec_material_price = (await iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                adec_material_price_after_discount = adec_material_price;
            }
            return FuncResults.Return(adec_material_price, adec_material_price_after_discount, adec_material_discount_percent, al_error_number, 1);
        }
        public uo_mini_terminal_body()
        {

        }

        public void constructor() //********************************************************************
        {
            //*Object:				uo_mini_terminal_bosy
            //*Function Name:	constructor
            //*Purpose: 			Initiate datastores
            //*Arguments: 		None
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*08-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            base.constructor();
            this.iuo_order_searchProperty = new u_order_search();
            this.iuo_order_searchProperty.uf_set_mt_message(true);
            this.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
        }

        public u_order_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public void LoadData1(ref string ls_employee_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_employee_number } };
                    string sql = "SELECT IsNull(name, '') FROM employees WHERE branch_number = @0 AND number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_employee_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    


        public override async Task<bool> uf_invoice_lock(string as_mode, string as_doc_type, long al_doc_no) //********************************************************************
        {
            //*Object:				uo_mini_terminal_body
            //*Function Name:	uf_invoice_lock
            //*Purpose: 			Lock the invoice
            //*Arguments: 		String	as_mode
            //*						String	as_doc_type
            //*						Long		al_doc_no
            //*Return:				Return 	Boolean
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*06-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************


            return await (this.Parent.Parent as w_mini_terminal).wf_invoice_lock(as_mode, as_doc_type, al_doc_no);
        }

        public override async Task<bool> uf_delete_item(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_body
            //*Function Name:	uf_delete_item
            //*Purpose: 			Call the function wf_delete_item.
            //*Arguments: 		Long - al_row - Row number in uo_body.dw_inv_pack_details.
            //*Return:				Boolean - TRUE - Success to Insert the row.
            //*									 FALSE - Failed to Insert the row.
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*12-05-2009		SharonS			1.2.44.1	SPUCM00000075		Initial version
            //************************************************************************************************
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
            ButtonElement cb_delete = sender as ButtonElement;
            
            long al_row = 0;
            if (cb_delete == null || cb_delete.Tag == null) {
                al_row =  dw_inv_pack_details.SelectedRowIndex;
            }
            else
            {
                //GalilCS - Restore row index from  tag
                al_row = Convert.ToInt32(cb_delete.Tag);
            }


            return await (this.Parent.Parent as w_mini_terminal).wf_delete_item(al_row);
        }

        public override async Task<int> uf_enter(string as_column, long al_row) //**********************************************************************************************
        {
            //*Object:							uo_mini_terminal_body
            //*Function/Event  Name:		uf_enter
            //*Purpose:							
            //*  
            //*Arguments:					Pass By		Argument Type			Argument Name
            //*									--------------------------------------------------------------------------
            //*									Value 		String 					as_column
            //*																									
            //*Return:							Integer: 1- Success
            //*											  -1 -Failed.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10/10/2007		SHARP.					0.2.0			B2B 
            //************************************************************************************************

            long ll_b2b_status = 0;
            long? ll_doc_no = 0;
            long ll_doc_no_en = 0;
            long ll_row_no = 0;
            long ll_cntr = 0;
            long ll_serial_number = 0;
            long ll_RowCount = 0;
            long ll_row = 0;
            long ll_material_number = 0;
            decimal? ll_material_quantity = 0;
            string ls_barcode = null;
            string ls_b2b_status = null;
            string ls_string = null;
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Start. Column: " + as_column), Convert.ToString("Row:      " + al_row.ToString()), "");
            ModelAction ldis_status = default(ModelAction);


            ldis_status = this.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
            f_dw_enter_key_moveClass.f_dw_enter_key_move(this.dw_inv_pack_details);


            if (this.dw_inv_pack_details.RowCount() < 1)
            {
                return 0;
            }

            ll_b2b_status = this.dw_inv_pack_details.GetItemValue<long>(al_row, "b2b_status");
            // SharonS - 1.2.32.0 - 2008-07-08- CR#1096 - Update log
            ls_b2b_status = ll_b2b_status.ToString();
            if (ls_b2b_status == null)
            {
                ls_b2b_status = "Null (No)";
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("B2B status: " + ls_b2b_status), "");
            switch (as_column)
            {
                case "barcode":

                    ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Barcode:  " + ls_barcode), "");
                    if (string.IsNullOrEmpty(ls_barcode))
                    {
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "barcode");
                    }
                    else
                    {
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "expected_material_quantity");
                    }
                    break;
                case "expected_material_quantity":
               
                    ll_row_no = this.dw_inv_pack_details.GetItemValue<long>(al_row, "row_no");
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Row no:   " + ll_row_no.ToString()), "");
                    // SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - Check if the quantity is null

                    ll_material_quantity = this.dw_inv_pack_details.GetItemValue<decimal?>(al_row, "expected_material_quantity");
                    if (ll_material_quantity == 0 || ll_material_quantity == null)
                    {
                        //.. כמות בפעול לא יכולה להיות ריקה
                        await this.uf_show_message("", "", "OK", "#20011");
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "expected_material_quantity");
                        return 0;
                    }
                    // End
                    // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - If at least 1 doc is not b2b, set focus to the doc_no field

                    ll_material_number = this.dw_inv_pack_details.GetItemValue<long>(al_row, "material_number");

                    ll_doc_no_en = this.dw_inv_pack_details.GetItemValue<long>(al_row, "doc_no_en");

                    ll_RowCount = this.dw_inv_pack_details.RowCount();
                    //		IF al_row_no = 1 AND ll_b2b_status <> 1 THEN
                    // SharonS - 1.2.37.9 - 2008-12-08 - Task#10012 - SPUCM00000094
                    //		IF (ll_row_no = 1 AND ll_b2b_status <> 1) OR ll_doc_no_en > 1 THEN
                    if (ll_row_no == 1 && (ll_b2b_status != 1 && ll_doc_no_en > 1))
                    {
                        // End
                        // End
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "doc_no");
                        return 1;
                    }
                    else
                    {
                        ll_doc_no = this.dw_inv_pack_details.GetItemValue<long?>(al_row, "doc_no");
                        this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Doc no:   " + ll_doc_no.ToString()), "");
                        if ((ll_b2b_status == 1) || (ll_doc_no != null && (!(ll_doc_no == 0) || ll_doc_no > 0)))
                        {
                            ll_serial_number = Convert.ToInt64(this.dw_inv_pack_details.GetItemValue<long>(al_row, "serial_number"));
                            if (!(ll_serial_number > 0) || ll_serial_number == 0)
                            {
                                await getMiniTerminal().wf_set_items("doc_no", Convert.ToString(ll_doc_no), al_row);

                            }
                            // SharonS - 1.2.37.10 - 2008-12-10 - Task#10012 - SPUCM00000094
                            if (ll_doc_no_en > 1)
                            {
                                await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "doc_no");
                            }
                            else
                            {
                                // End	
                                if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                                {
                                    await dw_inv_pack_details_ue_new_row();
                                }
                                else
                                {
                                    al_row = this.dw_inv_pack_details.RowCount() - 1;
                                    await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "barcode");
                                }
                                // SharonS - 1.2.37.10 - 2008-12-10 - Task#10012 - SPUCM00000094	
                            }
                            // End	
                        }
                        else
                        {
                            await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "doc_no");
                        }
                    }
                    break;
                case "doc_no":
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), "", "");
                    // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Check if doc_no is null - the user did not choose a doc.

                    ll_doc_no = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
                    if (ll_doc_no == 0 || ll_doc_no == 0)
                    {
                        await this.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך");
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "doc_no");
                        return 1;
                    }
                    // End
                    if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                    {
                        await dw_inv_pack_details_ue_new_row();
                    }
                    else
                    {

                        ll_RowCount = this.dw_inv_pack_details.RowCount();
                        this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("RowCount: " + ll_RowCount.ToString()), "");
                        for (ll_cntr = 0; ll_cntr < ll_RowCount; ll_cntr++)
                        {
                            ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(ll_cntr, "barcode");
                            if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
                            {
                                ll_row = ll_cntr;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        await this.uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", ll_row, "barcode");
                    }
                    break;
                default:
                    break;
            }
            return 1;
        }

        public async Task<int> dw_inv_pack_details_ue_new_row() // AlexKh - 1.2.46.0 - 2010-07-01 - CR#1138 - Call uf_new_row
        {
            return await this.uf_new_row(this.dw_inv_pack_details);
        }


        public async Task<bool> uf_update_item(long al_row_no) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_update_item
            //*Purpose:							
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value 		Long 					al_row_no 					
            //*
            //*Return:								Boolean: TRUE - Success
            //*												  	FALSE - Otherwise
            //*
            //*Date		     	Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*15-10-2007		SHARP.			     0.2.0 			B2B 			Initiation
            //************************************************************************************************



            return Convert.ToBoolean(await ((w_mini_terminal)this.Parent.Parent).wf_update_item(al_row_no));
        }

        public w_mini_terminal getMiniTerminal()
        {
            return ((w_mini_terminal)this.Parent.Parent);
        }

        public override async Task<int> uf_finish_input() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_finish_input
            //*Purpose:							Finish the input process in the mini terminal.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer:	1 - Success
            //*												   -1- Failed.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.   				0.2.0  		B2B 			Initiation
            //************************************************************************************************

            string ls_doc_type = null;
            string ls_params = null;
            string ls_copies = null;
            string ls_command = null;
            string ls_ret = null;
            string ls_barcode = null;
            string ls_filter = null;
            string ls_old_filter = null;
            string ls_employee_name = null;
            string ls_message = null;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_row_count = 0;
            long ll_ret = 0;
            long ll_print_row = 0;
            long ll_cntr = 0;
            IRepository lds_mini_terminal_report;
            // Save current filter

            ls_old_filter = Convert.ToString(this.dw_inv_pack_details.GetFilter());
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "Start.", Convert.ToString("Old filter:" + ls_old_filter), "");
            // Set DW with no filter
            ls_filter = "";

            ll_ret = this.dw_inv_pack_details.SetFilter(ls_filter);

            ll_ret = this.dw_inv_pack_details.Filter();

            ll_row_count = this.dw_inv_pack_details.RowCount();
            // Set original filter back

            this.dw_inv_pack_details.SetFilter(ls_old_filter);

            this.dw_inv_pack_details.Filter();


            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            string ls_row_count = null;
            ls_row_count = ll_row_count.ToString();
            if (ls_row_count == null)
            {
                ls_row_count = "Null";
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "Check if there are items to save.", Convert.ToString("RowCount: " + ls_row_count), "");
            // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - CR#1119 - Unlock all locked invoices 
            if (!await this.uf_invoice_lock("UNLOCK", "", 0))
            {
                await this.uf_show_message("שגיאה", "", "1", ".שגיאה בשחרור חשבוניות נעולות");
                return 1;
            }
            //ll_row_count = THIS.dw_inv_pack_details.RowCount()
            // Check if there are items to save
            if (ll_row_count < 2)
            {
                if (ll_row_count == 1)
                {
                    ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(0, "barcode");
                    if (ls_barcode == null)
                    {
                        await this.uf_show_message("!שים לב", "", "OK", "אין פריטים לשמירה.");
                        await getMiniTerminal().wf_reset();
                        await getMiniTerminal().wf_display("HEADER");
                        await this.uf_set_focus("uo_mini_terminal_header", "dw_header", 1, "supplier_number");
                        return 1;
                    }
                }
                else
                {
                    await this.uf_show_message("!שים לב", "", "OK", "אין פריטים לשמירה.");
                    await (this.Parent.Parent.Parent as w_mini_terminal).wf_reset();
                    await (this.Parent.Parent.Parent as w_mini_terminal).wf_display("HEDAER");
                    await this.uf_set_focus("uo_mini_terminal_header", "dw_header", 1, "supplier_number");
                    return 1;
                }
            }
            // Get params
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            (await this.uf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            (await this.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
            ls_params = ll_branch_number.ToString() + "@" + ll_supplier_number.ToString() + "@" + ll_order_number.ToString() + "@";
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "Open rw_diff_order_mt_report.", "", "");
            // Show differences between order and MT 
            await WindowHelper.Open<rw_diff_order_mt_report>("global_global", "ls_params", ls_params, "FormType", "base_body");

            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "Open rw_diff_inv_mt_report.", "", "");
            // Show differences between message and MT 

            await WindowHelper.Open<rw_diff_inv_mt_report>("global_global", "ls_params", ls_params, "FormType", "base_body");
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "Open rw_mini_terminal_print_diff.", "", "");
            // Print copies window
            rw_mini_terminal_print_diff rw_print = await WindowHelper.Open<rw_mini_terminal_print_diff>("global_global");

            ls_copies = WindowHelper.GetParam<string>(rw_print);
            if (ls_copies == "CANCEL") // No printing
            {



                await getMiniTerminal().wf_reset();


                await getMiniTerminal().wf_display("HEADER");
                await this.uf_set_focus("uo_mini_terminal_header", "dw_header", 1, "supplier_number");
                return 1;
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "uf_finish_input Function, Print.", "", "");

            if (System.Extensions.SystemFunctionsExtensions.IsNumber(ls_copies))
            {
                // Print MT report



                if (masofonAlias.masofon.Instance.gb_b2b_enable)
                {
                    lds_mini_terminal_report = new d_mini_terminal_b2b_reportRepository();
                }
                else
                {
                    lds_mini_terminal_report = new d_mini_terminal_reportRepository();
                }
                lds_mini_terminal_report.Retrieve(ll_branch_number, ll_supplier_number, ll_order_number);
                lds_mini_terminal_report.SetSort("order_number ASC, doc_no ASC");
                lds_mini_terminal_report.Sort();
                lds_mini_terminal_report.RowsMove(0, 1, ModelBuffer.Primary, lds_mini_terminal_report, lds_mini_terminal_report.RowCount(), ModelBuffer.Primary);


                if (true)
                {
                    LoadData1(ref ls_employee_name);
                }

                ll_print_row = lds_mini_terminal_report.RowCount();
                for (ll_cntr = 0; ll_cntr < ll_print_row; ll_cntr++)
                {
                    lds_mini_terminal_report.SetItem(ll_cntr, "employee_name", ls_employee_name);
                }
                ls_command = "datawindow.print.copies = " + ls_copies;
                if ((lds_mini_terminal_report != null) && !(lds_mini_terminal_report == null))
                {

                    ls_ret = lds_mini_terminal_report.Modify("datawindow.print.copies=" + ls_copies);
                    if (ls_ret.Length > 0) // If error then display the "Error setting print options!" 
                    {
                        await this.uf_show_message(Convert.ToString(f_get_error_message_numberClass.f_get_error_message_number(2621)), "", "OK", Convert.ToString(f_get_error_message_numberClass.f_get_error_message_number(2622) + ls_ret + "\r" + "\n" + f_get_error_message_numberClass.f_get_error_message_number(2647) + ls_command));


                        return -1;
                    }
                    else
                    {

                        lds_mini_terminal_report.Print(Globals.DeviceID, Globals.UserID, Globals.Password, MvcSite.Common.Globals.PrinterName);
                    }
                }

            }
            else
            {
                await this.uf_show_message("Error", "Error in printing window.", "OK", "");
            }
            // Reset window
            // Reset window
            await getMiniTerminal().wf_reset();
            await getMiniTerminal().wf_display("HEADER");
            await this.uf_set_focus("uo_mini_terminal_header", "dw_header", 1, "supplier_number");
            return 1;
        }


    }
}
