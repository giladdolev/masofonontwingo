using System;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;
using System.DataAccess;
using System.Collections.Generic;
using System.Data;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using Mesofon.Common;

namespace mini_terminal
{
    // Creation Time:   08/20/2015 09:55:48 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_trans_to_branch : uo_mini_terminal_base_body
    {
        public long il_branch_as_supp_num;
        public long il_supplier_num;
        public string is_state;
        public bool ib_user_type_return;
        public bool ib_new_return;
        public bool ib_enter;
        public bool ib_ignore_err_msg;
        public GridElement idwc_return;
        public IRepository ids_returns_list;
        public IRepository ids_adhoc_update_list;
        //DataStore ids_material_supplier
        public const long il_990_supplier = 2990;
        public delegate void ue_print_EventHandler();
        public event mini_terminalAlias.uo_mini_terminal_trans_to_branch.ue_print_EventHandler ue_printEvent;
        public uo_mini_terminal_trans_to_branch()
        {
        }

        public void constructor()
        {
            base.constructor();
            uf_create();
        }
        public int uf_create()
        {
            GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
            GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
            GridElement dw_branch_list = this.GetVisualElementById<GridElement>("dw_branch_list");
            this.ids_adhoc_update_listProperty = new d_addhockRepository();
            dw_branch_list.Insert(0);
            GridElement ldwc_branch_num;
            GridElement ldwc_branch_name;
            GridElement ldwc_declines;

            this.ids_returns_listProperty = new d_mini_terminal_return_details_listRepository();
            ldwc_branch_num = dw_branch_list.GetVisualElementById<GridElement>("branch_num");

            ldwc_branch_name = dw_branch_list.GetVisualElementById<GridElement>("branch_num_1");

            ldwc_declines = dw_return_details.GetVisualElementById<GridElement>("declines");

            dw_returns.Insert(0);

            this.idwc_returnProperty = dw_returns.GetVisualElementById<GridElement>("return_number");
            ldwc_branch_num.Retrieve();
            ldwc_branch_name.Retrieve();
            ldwc_declines.Retrieve(3);
            //RonY@28/04/2015 - SPUCM00005322 - start
            //ids_material_supplier = Create datastore
            //ids_material_supplier.DataObject = "d_material_suppliers_ex_ds"
            //RonY@28/04/2015 - SPUCM00005322 - end
            return 1;
        }
        public long il_branch_as_supp_numProperty
        {
            get { return this.il_branch_as_supp_num; }
            set { this.il_branch_as_supp_num = value; }
        }
        public long il_supplier_numProperty
        {
            get { return this.il_supplier_num; }
            set { this.il_supplier_num = value; }
        }
        public string is_stateProperty
        {
            get { return this.is_state; }
            set { this.is_state = value; }
        }
        public bool ib_user_type_returnProperty
        {
            get { return this.ib_user_type_return; }
            set { this.ib_user_type_return = value; }
        }
        public bool ib_new_returnProperty
        {
            get { return this.ib_new_return; }
            set { this.ib_new_return = value; }
        }
        public bool ib_enterProperty
        {
            get { return this.ib_enter; }
            set { this.ib_enter = value; }
        }
        public bool ib_ignore_err_msgProperty
        {
            get { return this.ib_ignore_err_msg; }
            set { this.ib_ignore_err_msg = value; }
        }
        public GridElement idwc_returnProperty
        {
            get { return this.idwc_return; }
            set { this.idwc_return = value; }
        }
        public IRepository ids_returns_listProperty
        {
            get { return this.ids_returns_list; }
            set { this.ids_returns_list = value; }
        }
        public IRepository ids_adhoc_update_listProperty
        {
            get { return this.ids_adhoc_update_list; }
            set { this.ids_adhoc_update_list = value; }
        }
        public void LoadData1(long supplier_number, long invoice_number, ref int li_mini_terminal)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", supplier_number }, { "@2", invoice_number } };
                    string sql = "SELECT count(*) FROM invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2 AND mini_terminal = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        li_mini_terminal = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(long ll_branch_number, long ll_supplier_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_return_number }, { "@1", ll_branch_number }, { "@2", ll_supplier_number }, { "@3", masofonAlias.masofon.Instance.gs_vars.active_owner }, { "@4", ldt_current_datetime }, { "@5", masofonAlias.masofon.Instance.qs_parameters.vat_percent }, { "@6", masofonAlias.masofon.Instance.gs_station.station_number } };
                    string sql = "INSERT INTO invoice_move (invoice_number,  branch_number,  supplier_number,  invoice_type,  employee_number,  supply_date,  discount,  mam,  invoice_total,  stock_number,  store_number,  station_num,  state,  date_move,  last_update_datetime) VALUES (@0,  @1,  @2,  r,  @3,  @4,  0,  @5 / 100,  0,  1,  1,  @6,  O,  @4,  @4)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3(long ll_serial_number, long ll_branch_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, long ll_supplier_number, decimal ldec_active_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", ll_branch_number }, { "@2", il_return_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", ll_supplier_number }, { "@7", ldec_active_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime,  bonus_quantity) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  1,  @7,  0,  @8,  @9,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData4(long ll_material_quantity, long ll_decline_num, DateTime? ldt_current_datetime, decimal ldec_material_price, decimal ldec_active_price, long ll_serial_number, long ll_branch_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_material_quantity }, { "@1", ll_decline_num }, { "@2", ldt_current_datetime }, { "@3", ldec_material_price }, { "@4", ldec_active_price }, { "@5", ll_serial_number }, { "@6", ll_branch_number } };
                    string sql = "UPDATE invoice_details SET material_quantity = @0,  decline_number = @1,  last_update_datetime = @2,  material_price = @3,  material_price_after_discount = @4 WHERE serial_number = @5 AND branch_number = @6";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData5(long ll_serial_number, long ll_branch_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, long ll_supplier_number, decimal ldec_active_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", ll_branch_number }, { "@2", il_return_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", ll_supplier_number }, { "@7", ldec_active_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime,  bonus_quantity) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  1,  @7,  0,  @8,  @9,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(ref long ll_supplier_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_as_supp_numProperty } };
                    string sql = "SELECT number FROM suppliers WHERE supplier_as_branch = @0 AND number <> 990";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void UpdateData8(Int64 material_number, Int64 store_number, Int64 stock_number, decimal ldec_tmp_avg_value, decimal quantity, decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number }, { "@4", ldec_tmp_avg_value }, { "@5", quantity }, { "@6", ldec_total_stock_purchase } };
                    string sql = "INSERT INTO stock_control (branch_number,  number,  store_number,  buy_stock,  sale_stock,  average_value,  stock_balance,  stock_average_value) VALUES (@0,  @1,  @2,  1,  @3,  @4,  @5,  @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData9(decimal ldec_quantity, decimal ldec_average_value, decimal ldec_total_stock_purchase, Int64 material_number, long ll_stock_a, Int64 store_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_quantity }, { "@1", ldec_average_value }, { "@2", ldec_total_stock_purchase }, { "@3", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@4", material_number }, { "@5", ll_stock_a }, { "@6", store_number } };
                    string sql = "UPDATE stock_control SET stock_balance = (stock_balance + @0),  average_value = @1,  stock_average_value = @2 WHERE (stock_control.branch_number = @3) AND (stock_control.number = @4) AND (stock_control.sale_stock = @5) AND (stock_control.buy_stock = 1) AND (stock_control.store_number = @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 material_number, ref decimal ldec_quantity_in_package)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", material_number } };
                    string sql = "SELECT quantity_in_package FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_quantity_in_package = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(ref decimal ldec_stock_balance, ref decimal ldec_stock_average, ref long ll_stock_a, int p, long material_number, long store_number, long stock_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", p }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "(SELECT stock_control.stock_balance,  stock_control.average_value,  stock_control.sale_stock FROM stock_control WHERE (stock_control.branch_number = @0) AND (stock_control.number = @1) AND (stock_control.store_number = @2) AND (stock_control.sale_stock = @3) AND (stock_control.buy_stock = 1))";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_stock_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_stock_average = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ll_stock_a = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(Int64 material_number, Int64 store_number, Int64 stock_number, ref decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "SELECT IsNull(stock_average_value, 0) FROM stock_control WHERE branch_number = @0 AND number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_total_stock_purchase = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(Int64 material_number, Int64 store_number, Int64 stock_number, ref decimal ldec_curr_balance)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "SELECT SUM(stock_balance) FROM stock_control WHERE stock_control.branch_number = @0 AND stock_control.number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_curr_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData10(long ll_invoice_number, long ll_supplier_number, DateTime? ldt_current_datetime, string ls_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_invoice_number }, { "@1", il_return_number }, { "@2", il_branch_as_supp_numProperty }, { "@3", ll_supplier_number }, { "@4", masofonAlias.masofon.Instance.gs_vars.active_owner }, { "@5", ldt_current_datetime }, { "@6", masofonAlias.masofon.Instance.qs_parameters.vat_percent }, { "@7", masofonAlias.masofon.Instance.gs_station.station_number }, { "@8", ls_state } };
                    string sql = "INSERT INTO invoice_move (invoice_number,  order_number,  branch_number,  supplier_number,  invoice_type,  employee_number,  supply_date,  discount,  mam,  invoice_total,  stock_number,  store_number,  station_num,  state,  date_move,  last_update_datetime,  b2b_status) VALUES (@0,  @1,  @2,  @3,  p,  @4,  @5,  0,  @6 / 100,  0,  1,  1,  @7,  @8,  @5,  @5,  1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData11(long ll_invoice_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_invoice_number }, { "@1", ldt_current_datetime }, { "@2", il_return_number }, { "@3", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@4", il_branch_as_supp_numProperty } };
                    string sql = "UPDATE invoice_move SET order_number = @0,  last_update_datetime = @1 WHERE invoice_number = @2 AND branch_number = @3 AND supplier_number = @4 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData12(long ll_supplier_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_return_number }, { "@1", il_branch_as_supp_numProperty }, { "@2", ll_supplier_number }, { "@3", ldt_current_datetime } };
                    string sql = "INSERT INTO b2b_invoice_move (invoice_number,  branch_number,  supplier_number,  invoice_type,  status,  upd_date_time,  serial_number,  order_number,  msg_date_time,  invoice_func,  discount_percent,  discount_amount,  doc_sum,  surface_qty,  lines_sum,  reference_number,  price_list_number,  branch_edi,  supplier_edi,  input_file_path,  input_file_name) VALUES (@0,  @1,  @2,  r,  a,  @3,  1,  @0,  @3,  '0',  0,  0,  0,  0,  0,  0,  0,  '0',  '0',  ' ',  ' ')";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData13(long ll_serial_number, long ll_invoice_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, long ll_supplier_number, decimal ldec_activate_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", il_branch_as_supp_numProperty }, { "@2", ll_invoice_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", ll_supplier_number }, { "@7", ldec_activate_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime,  bonus_quantity) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  1,  @7,  0,  @8,  @9,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData14(long ll_material_number, long ll_material_quantity, decimal ldec_material_price, long ll_supplier_number, DateTime? ldt_current_datetime, string ls_barcode, long ll_index)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_as_supp_numProperty }, { "@1", il_return_number }, { "@2", ll_material_number }, { "@3", ll_material_quantity }, { "@4", ldec_material_price }, { "@5", ll_supplier_number }, { "@6", ldt_current_datetime }, { "@7", ll_index }, { "@8", ls_barcode } };
                    string sql = "INSERT INTO b2b_invoice_details (serial_number,  branch_number,  invoice_number,  item_number,  units_qty,  item_bruto_price,  supplier_number,  upd_date_time,  line_number,  order_number,  status,  item_barcode,  item_discount_percent,  item_discount_amount,  bonus_line,  line_summary,  packages_quantity,  price_list_number,  reference_number) VALUES (1,  @0,  @1,  @2,  @3,  @4,  @5,  @6,  @7,  0,  'a',  @8,  0,  0,  0,  0,  0,  0,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(ref long ll_supplier_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT number FROM suppliers WHERE supplier_as_branch = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(Int64 al_material_number, ref decimal adec_price_before, ref decimal adec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        adec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        adec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData15(decimal ldec_invoice_total, Int64 al_branch_number, Int64 al_supplier_number, Int64 al_invoice_number, string as_doc_type)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_invoice_total }, { "@1", al_branch_number }, { "@2", al_supplier_number }, { "@3", al_invoice_number }, { "@4", as_doc_type } };
                    string sql = "UPDATE invoice_move SET invoice_total = Round(@0 AND (1 + mam), 2),  expected_total_amount = @0 WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND invoice_type = @4";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData16(DateTime? ldt_current_datetime, long ll_serial_number, long ll_branch_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_current_datetime }, { "@1", ll_serial_number }, { "@2", ll_branch_number } };
                    string sql = "UPDATE invoice_details SET material_quantity = 0,  last_update_datetime = @0 WHERE serial_number = @1 AND branch_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(string ls_barcode, ref string ls_material_name, ref long ll_material_number, ref string ls_material_status)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT name,  number,  material_status FROM materials WHERE (barcode = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_material_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        ll_material_number = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        ls_material_status = UnitOfWork.GetValue(resultRS, 2, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData17(DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_current_datetime }, { "@1", il_return_number }, { "@2", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@3", il_supplier_numProperty } };
                    string sql = "UPDATE invoice_move SET state = 'S',  last_update_datetime = @0 WHERE invoice_number = @1 AND branch_number = @2 AND supplier_number = @3 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


    }
}
