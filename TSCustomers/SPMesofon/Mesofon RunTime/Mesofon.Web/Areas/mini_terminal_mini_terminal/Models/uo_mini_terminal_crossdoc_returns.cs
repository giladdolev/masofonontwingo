using System.Web.VisualTree.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Extensions;
using global;
using masofon;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Mesofon.Common;

namespace mini_terminal
{
    // Creation Time:   09/07/2016 10:58:04 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_crossdoc_returns : uo_mini_terminal_base_body
    {
        //SPUCM00003614
        public GridElement idwc_distributor_num;
        public long il_return_son;
        public long il_supplier;
        public long il_distributor;
        public bool ib_marlog_return;
        public bool ib_vlaid_supp;
        public IRepository ids_valid_supplier;
        public IRepository ids_adhoc_update_list;
        public nvuo_b2b_debit_marlog invuo_b2b_debit_marlog; //Eitan; SPUCM00005489; 7/9/2016; 1.39.6;
        public nvuo_b2b_debit invuo_b2b_debit;
        public nvuo_account_handle invuo_account_handle;
        // AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_marlog_branch;
        public long il_marlog_distributor;
        public const int DISTRIBUTOR_BAIT_MIRKAHAT = 770;
        public void uf_refresh() // AlexKh
        {
            GridElement dw_return_num = this.GetVisualElementById<GridElement>("dw_return_num");
            GridElement dw_cartons = this.GetVisualElementById<GridElement>("dw_cartons");
            GridElement dw_summary = this.GetVisualElementById<GridElement>("dw_summary");
            long ll_ret = 0;
            if (il_return_number > 0)
            {                
                
                // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
                //ll_ret = dw_summary.Retrieve(gs_vars.branch_number, il_return_number)
                
                ll_ret = dw_summary.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, il_return_number, il_marlog_branchProperty);
                
                
                // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
                //ll_ret = dw_cartons.retrieve(gs_vars.branch_number, il_return_number)
                
                ll_ret = dw_cartons.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, il_return_number, il_marlog_branchProperty);

                dw_return_num.SetItem(0, "shipment_number", il_return_number.ToString());
            }
            
            if (dw_cartons.RowCount() == 0 && !isempty_stringClass.isempty_string(is_carton_barcode))
            {
                
                ll_ret = dw_cartons.Insert(0);

                dw_cartons.SetItem(ll_ret, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());

                dw_cartons.SetItem(ll_ret, "shipment_number", il_return_number.ToString());

                dw_cartons.SetItem(ll_ret, "carton_barcode", Convert.ToString(is_carton_barcode));

                dw_cartons.SetItem(ll_ret, "material_quantity", "0");

                dw_cartons.SetItem(ll_ret, "state", uo_mini_terminal_crossdoc_returns.RETURN_STATE_OPEN.ToString());
                // AlexKh - 1.1.1.12 - 09/09/2013 - SPUCM00004291 - update carton row status		
                
                
                
                dw_cartons.SetItemStatus((int)ll_ret, 0, ModelBuffer.Primary, ModelAction.None);
            }
        }
        public int uf_create(bool ab_marlog_return, long al_return_number, string as_carton_barcode, long al_office_return_number_for_display) //********************************************************************
        {
            ComboGridElement dw_1 = this.GetVisualElementById<ComboGridElement>("dw_1");
            LabelElement st_return_office = this.GetVisualElementById<LabelElement>("st_return_office");
            //	*Object:	            uo_mini_terminal_returns
            //	*Function Name:	   (uf_create)
            //	*Purpose: 				Make window preparation
            //	*Arguments: 	      boolean ab_marlog_return, long al_return_number, string as_carton_barcode
            //	*Return:	            integer
            //	*Date		    Programmer    Version	  Task#	    Description
            //	*---------------------------------------------------------------------
            //	*19/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
            //********************************************************************

            GridElement dwc_suppliers;
            GridElement dwc_declines;
            GridElement dwc_distributors = null;
           // GridElement dwc_shipments;
            GridElement dwc_sname;
            long ll_row = 0;
            string ls_rc = null;
            if (il_return_number == 0)
            {
                il_return_number = al_return_number;
                is_carton_barcode = as_carton_barcode;
            }
            uf_refresh();
            //Ron@05/05/2015 - start
            if (al_office_return_number_for_display == 0)
            {
                st_return_office.Text = "";
            }
            else if (il_return_number == 0)
            {
                st_return_office.Text = "";
            }
            else
            {
                st_return_office.Text = al_office_return_number_for_display.ToString();
            }
            //Ron@05/05/2015 - start
            ib_marlog_returnProperty = ab_marlog_return;
            // AlexKh - 1.1.1.11 - 19/08/2013 - CR#1160 - make supplier field unvisible 
            //IF ib_marlog_return THEN
            //	dw_1("supplier.visible='0' supplier_t.visible='0'")
            //END IF
            
            if (dw_1.RowCount() <= 0)
            {
                
                dw_1.Insert(0);
            }
            if (ab_marlog_return)
            {
                f_get_param_valuesClass.f_get_param_values(19, ref dw_1, "distributor", false, masofonAlias.masofon.Instance.sqlca);
                 
                dwc_distributors = dw_1.GetVisualElementById<GridElement>("distributor");
                
         
            }
            else
            {
                //retrieve suppliers
                 
                dwc_suppliers = dw_1.GetVisualElementById<GridElement>("supplier");
                
                
                
                
                ll_row = dwc_suppliers.Retrieve();
                //RonY@11/03/2015  - SPUCM00005247 - start	
                 
                dwc_sname = dw_1.GetVisualElementById<GridElement>("supplier_1");
                
                
                
                
                ll_row = dwc_sname.Retrieve();
                //RonY@11/03/2015  - SPUCM00005247 - end
                
                ids_valid_supplierProperty.Retrieve(il_marlog_distributorProperty); //list of suppler with relevant distributor
                //set distributor

                dw_1.SetItem(0, "distributor", il_marlog_distributorProperty.ToString());

                //dw_1.Modify("distributor", "Protect", "1");

                //dw_1.Modify("distributor.Background", "Color", "80269524"); //ButtonFace

                //dw_1.Modify("supplier", "Protect", "0");

                //dw_1.Modify("supplier", "Background.Color", 1073741824.ToString()); //White
            }
            //RonY@15/01/2015  - SPUCM00005162 - re3mark because shipments depend on supplier
            //
            //ll_row = dw_return_num.GetChild( "shipment_number", dwc_shipments)
            //dwc_shipments.SetTransObject(SQLCA)
            //// AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            ////ll_row = dwc_shipments.Retrieve(gs_vars.branch_number)
            //ll_row = dwc_shipments.Retrieve(gs_vars.branch_number, il_marlog_branch)
            //dw_return_num.Insert(0)
            //
            //IF il_return_number > 0 THEN 
            //	dw_return_num.SetItem (1, "shipment_number", il_return_number)
            //ELSE
            //	dw_return_num.SetFocus()
            //END IF
            //
            dw_1.Focus(); //RonY@15/01/2015  - SPUCM00005162

            dwc_distributors.Modify("param_value", "Alignment", "0");
            return 1;
        }
        public uo_mini_terminal_crossdoc_returns()
        {
        }

        public void constructor() //********************************************************************
        {
            //	*Object:	            uo_mini_terminal_returns
            //	*Event Name:	   (constructor)
            //	*Purpose: 	
            //	*Arguments: 	      (none)
            //	*Return:	            long
            //	*Date		    Programmer    Version	  Task#	    Description
            //	*---------------------------------------------------------------------
            //	*19/08/2013	 AlexKh        1.1.1.11	  CR#1160    Initial version
            //********************************************************************

            base.constructor();


            this.ids_adhoc_update_listProperty = new d_addhockRepository();


            this.invuo_b2b_debit_marlogProperty = new nvuo_b2b_debit_marlog();
            this.invuo_b2b_debitProperty = new nvuo_b2b_debit(); //Eitan; SPUCM00005489; 7/9/2016; 1.39.6;
            this.invuo_account_handleProperty = new nvuo_account_handle();
            //RonY@19/01/2015  - SPUCM00005162 - start


            this.ids_valid_supplierProperty = new d_supplier_per_distributor_dsRepository();


            //RonY@19/01/2015  - SPUCM00005162 - end
        }
        public long il_return_sonProperty
        {
            get { return this.il_return_son; }
            set { this.il_return_son = value; }
        }
        public long il_supplierProperty
        {
            get { return this.il_supplier; }
            set { this.il_supplier = value; }
        }
        public long il_distributorProperty
        {
            get { return this.il_distributor; }
            set { this.il_distributor = value; }
        }
        public bool ib_marlog_returnProperty
        {
            get { return this.ib_marlog_return; }
            set { this.ib_marlog_return = value; }
        }
        public bool ib_vlaid_suppProperty
        {
            get { return this.ib_vlaid_supp; }
            set { this.ib_vlaid_supp = value; }
        }
        public IRepository ids_valid_supplierProperty
        {
            get { return this.ids_valid_supplier; }
            set { this.ids_valid_supplier = value; }
        }
        public IRepository ids_adhoc_update_listProperty
        {
            get { return this.ids_adhoc_update_list; }
            set { this.ids_adhoc_update_list = value; }
        }
        public nvuo_b2b_debit_marlog invuo_b2b_debit_marlogProperty
        {
            get { return this.invuo_b2b_debit_marlog; }
            set { this.invuo_b2b_debit_marlog = value; }
        }
        public nvuo_b2b_debit invuo_b2b_debitProperty
        {
            get { return this.invuo_b2b_debit; }
            set { this.invuo_b2b_debit = value; }
        }
        public nvuo_account_handle invuo_account_handleProperty
        {
            get { return this.invuo_account_handle; }
            set { this.invuo_account_handle = value; }
        }
        public long il_marlog_branchProperty
        {
            get { return this.il_marlog_branch; }
            set { this.il_marlog_branch = value; }
        }
        public long il_marlog_distributorProperty
        {
            get { return this.il_marlog_distributor; }
            set { this.il_marlog_distributor = value; }
        }
        public void LoadData1(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT MAX(b.price_before_discount),  MAX(b.price_after_discount),  MAX(b.material_discount),  MAX(b.supplier_discount_percent) FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(long ll_ret_inv_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_supplierProperty }, { "@2", ll_ret_inv_no } };
                    string sql = "UPDATE invoice_move SET state = S WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3(decimal ldec_quantity, long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_quantity }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_material_number } };
                    string sql = "UPDATE stock_control SET stock_balance = (stock_balance - @0) WHERE (stock_control.branch_number = @1) AND (stock_control.number = @2) AND (stock_control.sale_stock = 1) AND (stock_control.buy_stock = 1) AND (stock_control.store_number = 1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(ref string ls_sup_edi_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_supplierProperty } };
                    string sql = "SELECT edi_number FROM suppliers WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_sup_edi_no = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(string ls_marlog_number, ref long ll_marlog_distributor_for_xml)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_marlog_number } };
                    string sql = "SELECT param_name FROM param_values WHERE param_code = 34 AND param_value = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_marlog_distributor_for_xml = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(long ll_marlog_distributor_for_xml, ref string ls_sup_edi_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_marlog_distributor_for_xml } };
                    string sql = "SELECT edi_number FROM suppliers WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_sup_edi_no = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(ref string ls_sup_edi_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_supplierProperty } };
                    string sql = "SELECT edi_number FROM suppliers WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_sup_edi_no = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(Int64 station_number, Int64 branch_number, ref string ls_printer_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", station_number }, { "@1", branch_number } };
                    string sql = "SELECT prt9_str_name FROM stations WHERE (station_number = @0) AND (branch_number = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_printer_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData4(DateTime? ldt_time)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", uo_mini_terminal_crossdoc_returns.RETURN_STATE_CANCEL }, { "@1", ldt_time }, { "@2", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@3", il_return_number } };
                    string sql = "UPDATE shipment_marlog_return SET state = @0,  last_update_datetime = @1 WHERE branch_number = @2 AND shipment_number = @3";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData5(DateTime? ldt_time, long ll_supplier_number, long ll_invoice_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_time }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number }, { "@3", ll_invoice_number } };
                    string sql = "UPDATE invoice_move SET state = 'D',  last_update_datetime = @0 WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData6(decimal ldec_invoice_total, Int64 al_supplier_number, Int64 al_invoice_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_invoice_total }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", al_supplier_number }, { "@3", al_invoice_number } };
                    string sql = "UPDATE invoice_move SET invoice_total = @0 WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(Int64 al_supplier_number, Int64 al_invoice_number, ref decimal ldec_invoice_total, ref decimal ldec_mam)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_supplier_number }, { "@2", al_invoice_number } };
                    string sql = "SELECT SUM(IsNull(ID.material_quantity AND ID.material_price, 0)),  mam FROM invoice_details AS ID,  invoice_move AS IM WHERE IM.branch_number = ID.branch_number AND IM.supplier_number = ID.supplier_number AND IM.invoice_number = ID.invoice_number AND IM.branch_number = @0 AND IM.supplier_number = @1 AND IM.invoice_number = @2 AND IM.invoice_type = 'r'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_invoice_total = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_mam = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(Int64 station_number, Int64 branch_number, ref string ls_printer_type)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", station_number }, { "@1", branch_number } };
                    string sql = "SELECT prt9_type FROM stations WHERE (station_number = @0) AND (branch_number = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_printer_type = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData10(string ls_barcode, ref long ll_supplier, ref string ls_material_name, ref long ll_material_number, ref long ll_distributor, ref long ll_return_allowed)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name,  number,  distributor_number,  return_allowed FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        ll_material_number = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        ll_distributor = UnitOfWork.GetValue(resultRS, 3, Convert.ToInt64);
                        ll_return_allowed = UnitOfWork.GetValue(resultRS, 4, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData11(long ll_distributor, long ll_supplier, ref long ll_row_exists)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_distributor }, { "@1", ll_supplier } };
                    string sql = "SELECT 1 FROM supplier_distributors WHERE is_active = 1 AND distributor_number = @0 AND supplier_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData12(string ls_barcode, ref long ll_supplier, ref string ls_material_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
