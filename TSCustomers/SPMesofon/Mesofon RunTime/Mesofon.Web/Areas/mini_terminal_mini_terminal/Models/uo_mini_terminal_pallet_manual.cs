using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using global;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Mesofon.Common;
using Mesofon.Data;
using masofon;
using Mesofon.Repository;
using MvcSite.Common;
using System.IO;

namespace mini_terminal
{
    // Creation Time:   07/11/2016 13:14:27 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_pallet_manual : uo_mini_terminal_base_body
    {
        public string is_pallet_number;
        public long il_shipment_number;
        public long il_supplier_number;
        public long? il_invoice_number;
        public long il_order_number;
        public bool ib_refresh;
        public IRepository ids_mini_terminal_pallet_invoices;
        public nvuo_pallet_item_search invuo_doc_item_search;
        public nvuo_pallet_orders_search iuo_order_search;
        public u_material_update iuo_material_update;
        public IRepository ids_invpack_deleted_items;
        public IRepository ids_material_batchs;
        public decimal idec_item_quantity_change_difference;
        // AlexKh - 1.1.1.20 - 2014-05-04 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_marlog_branch;
        public long il_marlog_distributor;
        public override async Task<int> uf_build_doc_number_list() //**********************************************************************************************
        {

            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_build_doc_number_list
            //*Purpose:							Initiate the doc number list box with the invoices 
            //*Arguments:							None.
            //*Return:								Boolean 1/-1
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            int li_row_count = 0;
            int li_counter = 0;
            int li_b2b_status = 0;
            string ls_doc_type = null;
            long ll_doc_number = 0;
            long ll_row_no = 0;
            char lc_state = '\0';
            bool lb_doc_no_enable = false;

            li_row_count = (int)ids_mini_terminal_pallet_invoicesProperty.RowCount();
            switch (li_row_count)
            {
                case 0:
                    return -1;
                default:

                    GridColumn ldwc_docs = GetDocNo(dw_inv_pack_details);
                    //Build the DropDownListBox

                    ldwc_docs.SetRepository(new d_invoice_numberRepository());

                    ldwc_docs.GetRepository().Reset();

                    ldwc_docs.GetRepository().SetFilter("");

                    ldwc_docs.GetRepository().Filter();
                    for (li_counter = 0; li_counter < li_row_count; li_counter++)
                    {

                        lc_state = Convert.ToChar(ids_mini_terminal_pallet_invoicesProperty.GetItemValue<string>(li_counter, "state"));
                        if (lc_state.ToString().ToUpper() == "A")
                        {
                            ls_doc_type = "I";

                            ll_doc_number = (long)ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>(li_counter, "invoice_number");
                            li_b2b_status = 1;

                            ll_row_no = ldwc_docs.GetRepository().Insert(0);
                            if (ll_row_no < 0)
                            {
                                await uf_show_message("Error", "", "OK", "Error while building invoices list");
                                return -1;
                            }

                            ldwc_docs.GetRepository().SetItem(ll_row_no, "doc_type", Convert.ToString(ls_doc_type));

                            ldwc_docs.GetRepository().SetItem(ll_row_no, "doc_number", ll_doc_number);

                            ldwc_docs.GetRepository().SetItem(ll_row_no, "b2b_status", li_b2b_status);
                        }
                    }
                    return 1;
            }
            return 1;
        }

        public WidgetGridElement dw_inv_pack_details
        {
            get
            {
                return this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_pallet_manual_dw_inv_pack_details");

            }
        }

        public GridElement dw_summary
        {
            get { return this.GetVisualElementById<GridElement>("uo_mini_terminal_pallet_manual_dw_summary"); }
        }

        public async Task<int> uf_body_new_row() //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_manual
            //*Function Name:	uf_body_new_row
            //*Purpose: 			Set focus onto last row
            //*Arguments: 		None
            //*Return:				Return 1/-1
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*01-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            int li_ret = 0;
            long ll_row_count = 0;
            bool lb_new_row = false;
            lb_new_row = await uf_is_new_row_exist(dw_inv_pack_details);
            if (lb_new_row)
            {
                return 1;
            }
            return await uf_new_row(dw_inv_pack_details);

        }
        public override async Task<int> uf_new_row(WidgetGridElement dw_inv_pack_details) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_new_row
            //*Purpose:							dd new row in dw_inv_pack_details.
            //*Arguments:							None.
            //*Return:								Integer: -1 Failed
            //*													1 Success 
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*01-07-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            int li_rtn = 0;
            long ll_open_docs = 0;
            li_rtn = await uf_build_doc_number_list();
            if (li_rtn == 1)
            {

                //uf_calculate_declines
                var ldwc_docs = GetDocNo(dw_inv_pack_details);

                ll_open_docs = ldwc_docs.GetRepository().RowCount();
                if (ll_open_docs == 0)
                {
                    await uf_show_message("!שים לב", "", "OK", "אין מסמכים פתוחים.");
                    return 1;
                }
            }
            return await base.uf_new_row(dw_inv_pack_details);
            // return 1;
        }




        public async Task<long> uf_get_pallet_invoices() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_pallet_invoices
            //*Purpose:							Retrieve invoices for the pallet 
            //*Arguments:							None.
            //*Return:								Long number of invoices
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long li_row_count = 0;
            long ll_i = 0;
            long ll_index = 0;
            long ll_supplier_number = 0;
            IRepository ldwc_invoices;


            //uf_calculate_declines
            (Parent.Parent as w_mini_terminal).wf_get_parameters(ref il_shipment_number, ref is_pallet_number, ref ib_refresh);
            if (ib_refresh)
            {
                // AlexKh - 1.1.1.20 - 2014-05-04 - CR#1171(SPUCM00004718) - retrieve by marlog branch_number
                //li_row_count = ids_mini_terminal_pallet_invoices.Retrieve(gs_vars.branch_number, il_shipment_number, is_pallet_number)

                li_row_count = ids_mini_terminal_pallet_invoicesProperty.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, il_shipment_numberProperty, is_pallet_numberProperty, il_marlog_branchProperty);
                //AlexKh - 1.39.6.0 - 2016-07-07 - SPUCM00005717 - lock all the invoices of the current pallet
                for (ll_i = 0; ll_i < li_row_count; ll_i++)
                {

                    await uf_invoice_lock("LOCK", "I", (long)ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>((int)ll_i, "invoice_number"));
                }
            }
            else
            {
                //AlexKh - 1.39.6.0 - 2016-07-07 - SPUCM00005717 - lock all the invoices of the current pallet

                for (ll_i = 0; ll_i < ids_mini_terminal_pallet_invoicesProperty.RowCount(); ll_i++)
                {

                    await uf_invoice_lock("LOCK", "I", (long)ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>((int)ll_i, "invoice_number"));
                }

                return ids_mini_terminal_pallet_invoicesProperty.RowCount();
            }



            if (li_row_count > 0)
            {

                ids_mini_terminal_pallet_invoicesProperty.SetSort("supplier_number ASC");

                ids_mini_terminal_pallet_invoicesProperty.Sort();
                invuo_doc_item_searchProperty.uf_set_invoice_number(ids_mini_terminal_pallet_invoicesProperty);
                iuo_order_searchProperty.uf_set_invoice_number(ids_mini_terminal_pallet_invoicesProperty);
                await uf_populate();
                //AlexKh - 1.39.6.0 - 2016-07-10 - SPUCM00005717 - reset datawindows and set appropriate pallet number
            }
            else
            {
                var st_order_number = this.GetVisualElementById<ControlElement>("st_order_number");
                st_order_number.Text = is_pallet_numberProperty;


                dw_inv_pack_details.Reset();
                il_row_no = 1;
            }

            await uf_body_new_row();
            //uf_calculate_declines


            return li_row_count;
        }



        public int uf_retrieve_dw_summary() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_retrieve_dw_summary
            //*Purpose:							Retrieve  dw_summary
            //*Arguments:							None.
            //*Return:								Integer : 	1 - Success
            //*													-1 - Otherwise.
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long? ll_invoice_number = 0;
            long ll_retrieved_rows = 0;
            long ll_i = 0;
            int? ll_found = 0;
            int ll_row = 0;
            string ls_find = null;
            decimal ldec_total = default(decimal);


            dw_summary.Reset();
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            var temp = new Mesofon.Repository.d_summery_inv_packRepository();
            dw_summary.SetRepository(temp, false, true);
            for (ll_i = 0; ll_i < dw_inv_pack_details.RowCount(); ll_i++)
            {
                //gilad check
                // skip over empty lines
                string ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_i, "barcode");
                if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
                {
                    continue;
                }

                ll_supplier_number = dw_inv_pack_details.GetItemValue<Int64>(ll_i, "supplier_number");

                ll_invoice_number = dw_inv_pack_details.GetItemValue<long?>(ll_i, "doc_no");

                ldec_total = dw_inv_pack_details.GetItemValue<Decimal>(ll_i, "expected_material_quantity");


                ls_find = "invoice_move_supplier_number == " + ll_supplier_number.ToString() + " AND doc_no == " + ll_invoice_number.ToString();


                ll_found = dw_summary.Find(ls_find, 0, dw_summary.RowCount());
                if (ll_found >= 0)
                {
                    ll_row = ll_found.Value;

                    ldec_total += dw_summary.GetItemValue<decimal>(ll_row, "total");
                }
                else
                {

                    ll_row = dw_summary.Insert(0);

                    dw_summary.SetItem(ll_row, "invoice_details_branch_number", ll_branch_number);

                    dw_summary.SetItem(ll_row, "invoice_move_supplier_number", ll_supplier_number);

                    dw_summary.SetItem(ll_row, "doc_no", ll_invoice_number);
                }

                dw_summary.SetItem(ll_row, "total", ldec_total);
            }

            dw_summary.ResetUpdate();
            return 1;
        }
        public async Task<int> uf_populate() //*********************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_populate
            //*Purpose:							populate data
            //*Arguments:							None
            //*Return:								INTEGER:    1 - Success
            //*													 	-1 - Otherwise
            //*Date 			Programer		Version		Task#			Description
            //*---------------------------------------------------------------------
            //*05-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //*********************************************************************

            long ll_row_count = 0;
            long ll_rows_copy = 0;
            long ll_docs_count = 0;
            long ll_items_count = 0;
            long ll_cnt = 0;
            long ll_order_number = 0;
            long ll_SetSort = 0;
            long ll_sort = 0;
            long ll_doc = 0;
            long ll_item = 0;
            long ll_doc_no_en = 0;
            long ll_cnt_b2b_docs = 0;
            long ll_cnt_non_b2b_docs = 0;
            long[] ll_doc_number = null;
            long[] ll_empty_array = null;
            long ll_material_number = 0;
            int li_rtn_code = 0;
            int li_b2b_status = 0;
            string ls_filter = null;
         //   char lc_state = '\0';
            //Set the pallet number in the title
            var st_order_number = this.GetVisualElementById<ControlElement>("st_order_number");
            st_order_number.Text = is_pallet_numberProperty;

            //shimon
            dw_inv_pack_details.ResetWithoutClearGrid("uf_populate.dw_inv_pack_details");
            // ...Invoice Details
            // Set filter
            ls_filter = "not ( expected_material_quantity = 0 or expected_material_quantity == null)";

            invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter(ls_filter);

            invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();
            // Get the related invoices items and copy into dw_inv_pack_details datawindow

            ll_row_count = invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
            if (ll_row_count > 0)
            {
                ll_rows_copy = invuo_doc_item_searchProperty.ids_pallet_inv_details.RowsCopy(0, invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount(), ModelBuffer.Primary, dw_inv_pack_details.Repository(), 0, ModelBuffer.Primary);
                if (ll_rows_copy == 0 || ll_rows_copy < 0)
                {
                    await uf_show_message("RowsCopy Error", "", "OK", "Error in copying invoice items.");
                    return -1;
                }
            }
            // Set filter - Original

            invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter("");

            invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();
            //Set the row no

            ll_row_count = dw_inv_pack_details.RowCount();
            il_row_no = ll_row_count + 1;

            ll_SetSort = dw_inv_pack_details.SetSort("indicator ASC");

            ll_sort = dw_inv_pack_details.Sort();
            for (ll_cnt = 0; ll_cnt < ll_row_count; ll_cnt++)
            {

                //uf_calculate_declines
                dw_inv_pack_details.SetItem(ll_cnt, "row_no", ll_cnt + 1);

                ll_material_number = dw_inv_pack_details.GetItemValue<Int64>(ll_cnt, "material_number");
                ll_doc_number = ll_empty_array;
                // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
                //dw_inv_pack_details.setItem(ll_cnt, "doc_no_en", invuo_doc_item_search.uf_get_item_doc_number(ll_material_number, ll_doc_number[]))

                //uf_calculate_declines
                dw_inv_pack_details.SetItem(ll_cnt, "doc_no_en", 1); //Always disable doc number
            }

            ll_docs_count = ids_mini_terminal_pallet_invoicesProperty.RowCount();
            // Set the update flags off

            dw_inv_pack_details.ResetUpdate();

            //GalilCS - Update dw_inv_pack_details
            dw_inv_pack_details.SetLockUI(true);
            GridElementExtensions.SetDataSourceEx(dw_inv_pack_details, dw_inv_pack_details.Repository().GetDataTable());
            dw_inv_pack_details.SetLockUI(false);


            // Set summery data
            uf_retrieve_dw_summary();

            return 1;
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<bool> uf_populate_row<T>(int row) //*********************************************************************
        {
            if (dw_inv_pack_details.IsRowSynced(row, "barcode"))
                return true;

            bool updatedSucc = invuo_doc_item_searchProperty.ids_pallet_inv_details.UpdateRow(row, dw_inv_pack_details.Repository(), "material_number");


            if (!updatedSucc)
            {
                await uf_show_message("RowsCopy Error", "", "OK", "Error in copying invoice items.");
                return false;
            }


            //GalilCS - Update dw_inv_pack_details
            dw_inv_pack_details.SetLockUI(true);
            GridElementExtensions.SetDataSourceEx(dw_inv_pack_details, dw_inv_pack_details.Repository().GetDataTable());
            dw_inv_pack_details.SetLockUI(false);


            // Set summery data
            uf_retrieve_dw_summary();


            return true;
        }






        public override async Task<bool> uf_invoice_lock(string as_mode, string as_doc_type, long al_doc_no) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_invoice_lock
            //*Purpose: 			Lock the invoice
            //*Arguments: 		String	as_mode
            //*						String	as_doc_type
            //*						Long		al_doc_no
            //*Return:				Return 	Boolean
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*06-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long? ll_found = 0;
            long ll_i = 0;
            string ls_find = null;
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            as_doc_type = "I"; // allways invoice
            ////Choose the working mode
            switch (as_mode)
            {
                case "LOCK":
                    //Lock mode
                    ls_find = "invoice_number == " + al_doc_no.ToString();


                    ll_found = ids_mini_terminal_pallet_invoicesProperty.Find(ls_find, 0, ids_mini_terminal_pallet_invoicesProperty.RowCount());
                    if (ll_found < 0)
                    {
                        return false;
                    }

                    if (ids_mini_terminal_pallet_invoicesProperty.GetItemValue<string>(ll_found.Value, "create_mode") == "1")
                    {
                        return true;
                    }

                    ll_supplier_number = ids_mini_terminal_pallet_invoicesProperty.GetItemValue<long>((int)ll_found, "supplier_number");
                    if (!await f_get_set_invoice_lockClass.f_get_set_invoice_lock(ll_branch_number, ll_supplier_number, as_doc_type, al_doc_no, 4))
                    {
                        await uf_show_message("uf_invoice_lock", "Error", "OK", "לא ניתן לשנות חשבונית נעולה");
                        return false;
                    }

                    ids_mini_terminal_pallet_invoicesProperty.SetItem(ll_found.Value, "create_mode", "1");
                    break;
                case "UNLOCK":
                    //Unlock mode

                    for (ll_i = 0; ll_i < this.ids_mini_terminal_pallet_invoicesProperty.RowCount(); ll_i++)
                    {

                        if (ids_mini_terminal_pallet_invoicesProperty.GetItemValue<string>(ll_i, "create_mode") == "1")
                        {
                            as_doc_type = "I";

                            al_doc_no = (long)ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>((int)ll_i, "invoice_number");

                            ll_supplier_number = ids_mini_terminal_pallet_invoicesProperty.GetItemValue<long>((int)ll_i, "supplier_number");
                            if (!await f_get_set_invoice_lockClass.f_get_set_invoice_lock(ll_branch_number, ll_supplier_number, as_doc_type, al_doc_no, 3))
                            {
                                await uf_show_message("uf_invoice_lock", "Error", "OK", "שחרור חשבוניות נכשל");
                                return false;
                            }

                            ids_mini_terminal_pallet_invoicesProperty.SetItem(ll_i, "create_mode", "0");
                        }
                    }
                    break;
            }
            //
            return true;
        }
        public uo_mini_terminal_pallet_manual()
        {
        }

        public override async Task<int> uf_set_items(string as_column_name, string as_data, long al_row) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			wf_set_items
            //*Purpose:							For each column that is updated, if the row status is NewModified, 
            //*										(new item) set the relevant data. 
            //*Arguments:							Value 		string 					as_column_name
            //*										Value 		string 					as_data
            //*										Value 		Long	 					al_row
            //*Return:								INTEGER :  -1 failed
            //*														1 Success
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*01-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            int li_rtn_code = 0;
            int li_cng_doc_count = 0;
            long ll_material_number = 0;
            long ll_old_material_number = 0;
            long ll_order_number = 0;
            long? ll_doc_no = 0;
            long? ll_orig_doc_no = 0;
            long ll_docs_count = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_next_serial_number = 0;
            long ll_rows = 0;
            long? ll_found_row = 0;
            long? ll_found_prev_row = 0;
            long ll_item_b2b_status = 0;
            long ll_min_valid_month = 0;
            long ll_count = 0;
            long? ll_null = null;
            long ll_supplier_number = 0;
            long[] ll_doc_number = null;
            decimal ld_material_quantity = default(decimal);
            string ls_barcode = null;
            string ls_find = null;
            string ls_import_type = null;
            string ls_barcode_main = null;
            string ls_curr_expiration_date = null;
            char lc_doc_type = '\0';
            char lc_orig_doc_type = '\0';
            bool lb_is_item_b2b = false;
            bool lb_is_pre_item_b2b = false;
            bool lb_set_serial_number = false;
            DateTime? ldt_curr_expiration_date = default(DateTime);
            ModelAction ldws_status = default(ModelAction);
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "Start.", Convert.ToString("Row:      " + al_row.ToString()), Convert.ToString("Column:   " + as_column_name + " Data: " + as_data + "\r" + "\n"));
            // Get material number from barcode 
            if (as_column_name == "barcode")
            {
                ls_barcode = as_data;
            }
            else
            {

                ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "Start.", "Before uf_get_barcode_main", Convert.ToString("Barcode:  " + ls_barcode));
            // Get full barcode if it was particulaly inserted
            ls_barcode_main = await this.iuo_material_search.uf_get_barcode_main(this.is_new_barcode, false);
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "Start.", "Before uf_get_material_number", Convert.ToString("Barcode:  " + ls_barcode_main));
            ls_barcode = ls_barcode_main;
            ll_material_number = await this.iuo_material_search.uf_get_material_number(ls_barcode_main);
            // Get row status


            ldws_status = this.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
            // Get current or new documet number 
            if (as_column_name == "doc_no")
            {
                ll_doc_no = Convert.ToInt64(as_data);
            }

            else if (ldws_status != ModelAction.Insert)
            {

                ll_doc_no = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
            }

            else if (ldws_status == ModelAction.Insert)
            {
                ll_docs_count = this.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
                if (this.il_invoice_numberProperty > 0)
                {
                    ll_doc_no = this.il_invoice_numberProperty;
                }
            }
            if (!(ll_doc_no == 0) && ll_doc_no != 0)
            {
                lc_doc_type = uf_get_doc_type(ll_doc_no.Value).Result;
            }
            // Get item b2b status
            if (as_column_name == "barcode")
            {

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "barcode", as_data);
                lb_is_item_b2b = this.invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_doc_no.Value);
            }
            else if (as_column_name == "doc_no")
            {
                lb_is_item_b2b = this.invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_doc_no.Value);
            }
            else
            {

                ll_item_b2b_status = this.dw_inv_pack_details.GetItemValue<int>(al_row, "b2b_status");
                if (ll_item_b2b_status == 1)
                {
                    lb_is_item_b2b = true;
                }
            }

            if ((ldws_status != ModelAction.Insert && as_column_name != "barcode") || lb_is_item_b2b)
            {
                ls_find = "material_number == " + ll_material_number.ToString();
                if (lc_doc_type.ToString() == "I")
                {

                    ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                    // Find material by barcode and invoice number
                    ls_find += " and invoice_number == " + ll_doc_no.ToString();

                    ll_found_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);
                }
            }

            else if ((ldws_status != ModelAction.Insert && as_column_name == "barcode") || lb_is_item_b2b)
            {
                if (this.is_new_barcode != this.is_old_barcode)
                {
                    ll_old_material_number = await this.iuo_material_search.uf_get_material_number(this.is_old_barcode);
                    ls_find = "material_number = " + ll_old_material_number.ToString();
                    if (lc_doc_type.ToString() == "I")
                    {

                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                        ls_find += " and invoice_number = " + ll_doc_no.ToString();

                        ll_found_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);
                    }
                }
            }
            else
            {
                ls_find = "material_number == " + ll_material_number.ToString();
                if (ll_doc_no != 0 && !(ll_doc_no == 0))
                {
                    if (lc_doc_type.ToString() == "I")
                    {

                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                        ls_find += " and invoice_number == " + ll_doc_no.ToString();

                        ll_found_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);
                    }
                }
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "Start.", "Before CHOOSE CASE.", Convert.ToString("Material number: " + ll_material_number.ToString()));
            switch (as_column_name)
            {
                case "barcode":
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "uf_set_items Function, CASE barcode", "Before uf_material_assigns_to_order.", Convert.ToString("Material number: " + ll_material_number.ToString()));
                    ls_barcode = as_data;
                    if (this.il_supplier_numberProperty > 0)
                    {
                        //ll_order_number = uf_get_selected_order_number(REF il_invoice_number)
                        //ll_supplier_number = uf_get_selected_supplier_number(REF il_invoice_number)

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "supplier_number", this.il_supplier_numberProperty);
                        ls_import_type = "o";
                    }

                    if (ldws_status == ModelAction.Insert && !lb_is_item_b2b)
                    {

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "order_number", this.il_order_numberProperty); //.. order number

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "import_type", ls_import_type); //.. order type	
                        await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");
                    }
                    else
                    {

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "b2b_status", 1);
                        if (lc_doc_type.ToString() == "I")
                        {
                            //invuo_doc_item_search.ids_pallet_inv_details.SetItem(ll_found_row, "packing_list_number", il_order_number)

                            this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_row.Value, "import_type", Convert.ToString(ls_import_type));
                        }
                        // Set order_number and import_type in the DW also

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "order_number", this.il_order_numberProperty); //.. order number

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "import_type", ls_import_type); //.. order type	
                        if (ll_docs_count > 1)
                        {
                            await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");
                        }
                        else
                        {
                            await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "OLD");
                        }
                    }
                    break;
                case "expected_material_quantity":
                 
                    // AlexKh - 1.2.48.0.13 - 2011-11-07 - Fix for pallets - Handle expiration date
                    if (Convert.ToInt64(as_data) > 0)
                    {
                        if (await uf_handle_expiration_date(al_row) < 0)
                        {
                            return -1;
                        }
                    }
                    li_rtn_code = await uf_set_quantity_and_doc_no(as_data, al_row);
                    if (li_rtn_code < 1)
                    {
                        return -1;
                    }
                    if (ll_docs_count > 1)
                    {
                        await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");
                    }
                    else
                    {
                        await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "OLD");
                    }
                    break;
                case "doc_no":
                    // Get the document number
                    ll_doc_no = Convert.ToInt64(as_data);
                    // Get the document type (I - Invoice or P - Packlist)
                    lc_doc_type = uf_get_doc_type(ll_doc_no.Value).Result;
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "uf_set_items Function, CASE doc_no.", Convert.ToString("Current doc Number: " + ll_doc_no.ToString()), Convert.ToString("Doc type: " + lc_doc_type.ToString()));
                    if (lc_doc_type.ToString() != "I")
                    {
                        return -1;
                    }

                    ll_orig_doc_no = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, as_column_name);
                    if (!(ll_orig_doc_no == 0))
                    {
                        lc_orig_doc_type = uf_get_doc_type(ll_orig_doc_no.Value).Result;
                    }
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "uf_set_items Function, CASE doc_no.", Convert.ToString("Original doc Number: " + ll_orig_doc_no.ToString()), Convert.ToString("Doc type: " + lc_orig_doc_type.ToString()));
                    if (lc_orig_doc_type.ToString() == "E")
                    {
                        return -1;
                    }
                    // AlexKh - set item to delete status if invoice number changed on existing row
                    //		IF ldws_status = DataModified! THEN
                    //			uf_delete_item(al_row)
                    //		END IF
                    uf_set_doc_no(al_row, Convert.ToInt64(as_data), true);
                    await uf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");

                    ll_null = null;
                    ll_material_serial_number_in_order = this.iuo_order_searchProperty.uf_get_order_material_serial_number(ll_material_number);
                    if (ll_doc_no == ll_orig_doc_no || (ll_orig_doc_no == 0 || ll_orig_doc_no == 0))
                    {

                        if (ldws_status == ModelAction.Insert)
                        {
                            if (!lb_is_item_b2b)
                            {

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                lb_set_serial_number = true;
                            }
                        }
                        // Invoice number was changed	
                    }
                    else
                    {
                        lb_is_pre_item_b2b = this.invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_orig_doc_no.Value);
                        if (lc_doc_type == lc_orig_doc_type || (lc_orig_doc_type == default(char) || string.IsNullOrEmpty(lc_orig_doc_type.ToString())))
                        {
                            //	Invoice type was not changed

                            if (ldws_status == ModelAction.Insert)
                            {
                                //	The line is new	
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {
                                    }
                                }
                                // Existing line
                            }
                            else
                            {
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lc_doc_type.ToString() == "I")
                                    {

                                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());
                                        //invuo_doc_item_search.ids_pallet_inv_details.SetItem(ll_found_prev_row, "mini_terminal", 0)
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {


                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", this.dw_inv_pack_details.GetItemValue<int>(al_row, "expected_material_quantity"));


                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.dw_inv_pack_details).SetItem(al_row, "material_quantity", this.dw_inv_pack_details.GetItemValue<int>(al_row, "expected_material_quantity"));
                                    if (lc_doc_type.ToString() == "I")
                                    {

                                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Delete((int)ll_found_prev_row);
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //	Invoice type was changed

                            if (ldws_status == ModelAction.Insert)
                            {
                                //	Then line is new
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                            }
                            else
                            {
                                // Existing line
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lc_orig_doc_type.ToString() == "I")
                                    {

                                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());
                                        //invuo_doc_item_search.ids_pallet_inv_details.SetItem(ll_found_prev_row, "mini_terminal", 0)
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    lb_set_serial_number = true;


                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", this.dw_inv_pack_details.GetItemValue<int>(al_row, "expected_material_quantity"));


                                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                    (this.dw_inv_pack_details).SetItem(al_row, "material_quantity", this.dw_inv_pack_details.GetItemValue<int>(al_row, "expected_material_quantity"));
                                    if (lc_orig_doc_type.ToString() == "I")
                                    {

                                        ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Delete((int)ll_found_prev_row);
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                        (this.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                    }
                                }
                            }
                        }
                    }
                    if (lb_set_serial_number)
                    {
                        ll_next_serial_number = await uf_get_max_serial_number(ll_doc_no.Value);

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "serial_number", ll_next_serial_number);
                    }
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "uf_set_items Function, CASE doc_no.", "End of CASE", "");
                    break;
                default:
                    break;
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_set_items", "uf_set_items Function.", "End of uf_set_items", "");
            return 1;
        }

        public async Task<long> uf_get_max_serial_number(long al_doc_number) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_get_max_serial_number
            //*Purpose: 			Get next serial number
            //*Arguments: 		Long	al_doc_number
            //*Return:				Long	ll_max_serial_number
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*15-08-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_max_serial_number = 0;

            if (this.dw_inv_pack_details.RowCount() > 0)
            {
                await f_begin_tranClass.f_begin_tran();
                ll_max_serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(4);
                await f_commitClass.f_commit();
            }
            return ll_max_serial_number;
        }

        public override async Task<char> uf_get_doc_type(long? al_doc_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_doc_type
            //*Purpose:							Get document type: I for Invoice 
            //*Arguments:							Long	al_doc_no
            //*Return:								Character: 'I' - Invoice
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            char lc_doc_type = '\0';
            lc_doc_type = 'I';
            return lc_doc_type;
        }

        public async Task constructor() //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	constructor
            //*Purpose: 			Initiate datastores
            //*Arguments: 		None
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*08-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            base.constructor();


            this.iuo_material_updateProperty = new u_material_update();
            this.iuo_order_searchProperty = new nvuo_pallet_orders_search();



            this.ids_invpack_deleted_itemsProperty = new ds_invpack_deleted_itemsRepository();
            // Initialize value to model
            ids_invpack_deleted_itemsProperty.Modify("Initial,branch_number," + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,station_number," + this.il_employee_number.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,delete_point,1");
            ids_invpack_deleted_itemsProperty.Modify("Initial,del_emp_number," + this.il_employee_number.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,doc_type,p");

            this.iuo_order_searchProperty.uf_set_mt_message(true);
            this.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.iuo_material_updateProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.is_send_decline_msg = await f_get_parameter_valueClass.f_get_parameter_value("decline_operation", "string", "NONE", "DB or MSG or DB_MSG or None.", "gds_find");
        }

        public string is_pallet_numberProperty
        {
            get { return this.is_pallet_number; }
            set { this.is_pallet_number = value; }
        }
        public long il_shipment_numberProperty
        {
            get { return this.il_shipment_number; }
            set { this.il_shipment_number = value; }
        }
        public long il_supplier_numberProperty
        {
            get { return this.il_supplier_number; }
            set { this.il_supplier_number = value; }
        }
        public long? il_invoice_numberProperty
        {
            get { return this.il_invoice_number; }
            set { this.il_invoice_number = value; }
        }
        public long il_order_numberProperty
        {
            get { return this.il_order_number; }
            set { this.il_order_number = value; }
        }
        public IRepository ids_mini_terminal_pallet_invoicesProperty
        {
            get { return this.ids_mini_terminal_pallet_invoices; }
            set { this.ids_mini_terminal_pallet_invoices = value; }
        }
        public nvuo_pallet_item_search invuo_doc_item_searchProperty
        {
            get { return this.invuo_doc_item_search; }
            set { this.invuo_doc_item_search = value; }
        }
        public nvuo_pallet_orders_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public u_material_update iuo_material_updateProperty
        {
            get { return this.iuo_material_update; }
            set { this.iuo_material_update = value; }
        }
        public IRepository ids_invpack_deleted_itemsProperty
        {
            get { return this.ids_invpack_deleted_items; }
            set { this.ids_invpack_deleted_items = value; }
        }
        public IRepository ids_material_batchsProperty
        {
            get { return this.ids_material_batchs; }
            set { this.ids_material_batchs = value; }
        }
        public decimal idec_item_quantity_change_differenceProperty
        {
            get { return this.idec_item_quantity_change_difference; }
            set { this.idec_item_quantity_change_difference = value; }
        }
        public long il_marlog_branchProperty
        {
            get { return this.il_marlog_branch; }
            set { this.il_marlog_branch = value; }
        }
        public long il_marlog_distributorProperty
        {
            get { return this.il_marlog_distributor; }
            set { this.il_marlog_distributor = value; }
        }
        public void UpdateData2(long ll_serial_number, long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_material_number } };
                    string sql = " DELETE FROM material_batch WHERE ((material_batch.packing_detailes_serial_number = @0) OR (material_batch.invoice_detailes_serial_number = @0)) AND (material_batch.branch_number = @1) AND (material_batch.material_number = @2)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_serial_number, long ll_material_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_material_number } };
                    string sql = "SELECT count(material_batch.batch_number) FROM material_batch WHERE ((material_batch.packing_detailes_serial_number = @0) OR (material_batch.invoice_detailes_serial_number = @0)) AND (material_batch.branch_number = @1) AND (material_batch.material_number = @2)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3(decimal adec_quantity, Int64 al_supplier_number, Int64 al_invoice_number, long ll_item_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", adec_quantity }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", al_supplier_number }, { "@3", al_invoice_number }, { "@4", ll_item_number }, { "@5", is_pallet_numberProperty } };
                    string sql = "UPDATE b2b_invoice_details SET scanned_quantity = @0 WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND item_number = @4 AND pallet_number = @5";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public async Task<int> uf_set_material_details(long al_row, string as_barcode, char lc_doc_type, long? al_found_row, string as_row_status) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_set_material_details
            //*Purpose:							Get material details and set them in the DW if its a new material or in the DS if its an existing material
            //*Arguments:							Value			Long					al_row			The row number in the DW (for new row)
            //*										Value 		String				as_barcode		The barcode that was insert
            //*										Value 		Char 					lc_doc_type		The document type of the material document 
            //*										Value			Long					al_found_row	The row number in the DS (for exists row)
            //*										Value			String				as_row_status	The row status, NewModified or DataModified
            //*
            //*Return:								Long 		1-Success, -1 Failed
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*04-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            int li_rtn_code = 0;
            string ls_material_name = null;
            string ls_find = null;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_material_number = 0;
            long ll_min_valid_month = 0;
            long ll_error_number = 0;
            long? ll_order_material_color = 0;
            long ll_ret = 0;
            long ll_ind = 0;
            long ll_rows = 0;
            long? ll_invoice_number = 0;
            decimal ldec_sell_price = default(decimal);
            decimal ldec_material_price = default(decimal);
            decimal ldec_material_price_after_discount = default(decimal);
            decimal ldec_material_discount_percent = default(decimal);
        //    bool lb_is_item_b2b = false;
            DateTime? ldt_curr_expiration_date = default(DateTime);

            ll_invoice_number = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
            if (ll_invoice_number > 0)
            {
                var res = await uf_get_selected_supplier_number(ll_supplier_number);
                res.Retrieve(out ll_supplier_number);

                var res1 = await uf_get_selected_order_number(ll_order_number);
                res1.Retrieve(out ll_order_number);
            }
           // Get material-number, material-name, minimum valid month for material by barcode
           (await this.iuo_material_search.uf_get_material_info(as_barcode, ll_material_number, ls_material_name, ll_min_valid_month)).Retrieve(out ll_material_number, out ls_material_name, out ll_min_valid_month);
            if (ll_material_number == 0)
            {
                await this.uf_show_message("", "", "OK", Convert.ToString(masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("לא נמצא חומר מתאים").Result));

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", 0);
                return -1;
            }
            // Find material by material and invoice number
            ls_find = SystemHelper.Concat("material_number == ", ll_material_number);
            ls_find = SystemHelper.Concat(ls_find, SystemHelper.Concat(" and invoice_number == ", ll_invoice_number));


            al_found_row = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
            if (ll_order_number > 0 && ll_supplier_number > 0)
            {
                // Gets material-prices from material_supplier OR supplier_order_details table
                (await uf_get_order_material_price(ll_supplier_number, ll_order_number, ll_material_number, ldec_material_price, ldec_material_price_after_discount, ldec_material_discount_percent, ll_error_number)).Retrieve(out ldec_material_price, out ldec_material_price_after_discount, out ldec_material_discount_percent, out ll_error_number);
                // Error occure (for example DB error)
                if (ll_error_number == -1)
                {
                    await this.uf_show_message("שגיאה", "", "OK", ".שגיאה בשליפת מספר הזמנה");
                    return -1;
                }
            }
            // Gets value from branch_item_price table for this branch and material number
            ldec_sell_price = await this.iuo_material_search.uf_get_material_sell_price(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_material_number);
            // If material assigned to order sets its order-color-number, otherwise sets null

            ll_order_material_color = null;
            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "material_name", ls_material_name);

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "material_number", ll_material_number);

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "barcode", as_barcode);
            if (ll_order_number > 0 && ll_supplier_number > 0)
            {

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "material_price", ldec_material_price);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "material_price_after_discount", ldec_material_price_after_discount);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "material_discount_percent", ldec_material_discount_percent);
            }

           // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
           (this.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "sell_price", ldec_sell_price);

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "current_catalog_sell_price", ldec_sell_price);

            ll_rows = this.dw_inv_pack_details.RowCount();

            if (!(this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "indicator") > 0))
            {

                Int64 maxIndicator = 0;
                for (int i = 0; i < this.dw_inv_pack_details.RowCount(); i++)
                {
                    if (this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator") > maxIndicator)
                    {
                        maxIndicator = this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator");
                    }
                }
                (this.dw_inv_pack_details).SetItem(al_row, "indicator", maxIndicator + 1);
            }
            if (ll_supplier_number > 0)
            {

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "supplier_number", ll_supplier_number);
            }

           // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
           (this.dw_inv_pack_details).SetItem(al_row, "mini_terminal", 1);

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "color", ll_order_material_color);
            if (as_row_status == "NEW")
            {

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "doc_state", "O");

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "barcode", as_barcode);
                if (ll_order_number > 0 && ll_supplier_number > 0)
                {

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "material_price", ldec_material_price);

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "material_price_after_discount", ldec_material_price_after_discount);

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "material_discount_percent", ldec_material_discount_percent);
                }

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "sell_price", ldec_sell_price);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "current_catalog_sell_price", ldec_sell_price);

                ll_rows = this.dw_inv_pack_details.RowCount();

                if (!(this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "indicator") > 0))
                {
                    Int64 maxIndicator = 0;
                    for (int i = 0; i < this.dw_inv_pack_details.RowCount(); i++)
                    {
                        if (this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator") > maxIndicator)
                        {
                            maxIndicator = this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator");
                        }
                    }
                    (this.dw_inv_pack_details).SetItem(al_row, "indicator", maxIndicator + 1);
                }
                if (ll_supplier_number > 0)
                {

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "supplier_number", ll_supplier_number);
                }

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "mini_terminal", 1);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "color", ll_order_material_color);
            }
            else if (as_row_status == "OLD" && al_found_row != null && al_found_row != -1)
            {
                // Set min_valid_months in dw_inv_pack_details also.

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);
                // End 

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "material_number", ll_material_number);

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "materials_barcode", Convert.ToString(as_barcode));
                if (ll_order_number > 0 && ll_supplier_number > 0)
                {

                    this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "material_price", ldec_material_price.ToString());

                    this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "material_price_after_discount", ldec_material_price_after_discount.ToString());

                    this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "material_discount_percent", ldec_material_discount_percent.ToString());
                }

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "sell_price", ldec_sell_price);

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "current_catalog_sell_price", ldec_sell_price);

                ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();

                if (!(this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<Int64>((int)al_found_row, "indicator") > 0))
                {
                    Int64 maxIndicator = 0;
                    for (int i = 0; i < this.dw_inv_pack_details.RowCount(); i++)
                    {
                        if (this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator") > maxIndicator)
                        {
                            maxIndicator = this.dw_inv_pack_details.GetItemValue<Int64>(i, "indicator");
                        }
                    }
                    this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "indicator", maxIndicator + 1);
                }
                if (ll_supplier_number > 0)
                {

                    this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "supplier_number", ll_supplier_number.ToString());
                }

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "mini_terminal", Convert.ToInt64(1));

                this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(al_found_row.Value, "color", ll_order_material_color);
            }
            return 1;
        }

        public async Task<long> uf_handle_expiration_date(long al_row) //************************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_handle_expiration_date
            //*Purpose: 			Set expiration date for item
            //*Arguments: 		Long - al_row - Row number dw_inv_pack_details
            //*Return:				Boolean - None
            //*Date				Programer		Version			Task#	 		Description
            //*-------------------------------------------------------------------------
            //*06-11-2011		AlexKh			1.2.48.0.13						Initial version
            //*************************************************************************

            DateTime? ldt_curr_expiration_date = default(DateTime);
            long? ll_found = 0;
            long ll_min_valid_month = 0;
            long? ll_doc_no = 0;
            long ll_material_number = 0;
            long ll_docs_count = 0;
            long[] ll_doc_number = null;
            string ls_curr_expiration_date = null;
            string ls_find = null;
            bool lb_is_item_b2b = false;
            ModelAction ldws_status = default(ModelAction);
            // Get row status


            ldws_status = this.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);

            ll_material_number = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");

            ll_doc_no = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
            // AlexKh - 1.2.48.17 - 2012-03-18 - CR#153 - fill missed data if possible
            if (ll_doc_no == -1 || ll_doc_no == null)
            {
                ll_docs_count = this.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
                if (ll_docs_count > 0)
                {
                    ll_doc_no = ll_doc_number[0];
                    lb_is_item_b2b = this.invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_doc_no.Value);
                }
            }
            ls_find = "material_number == " + ll_material_number.ToString();
            if (ll_doc_no != 0 && !(ll_doc_no == 0) && ll_doc_no != null)
            {
                ls_find += " and invoice_number == " + ll_doc_no.ToString();


                ll_found = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
            }

            if ((ldws_status != ModelAction.Insert || lb_is_item_b2b) && ll_found != 0)
            {

                ldt_curr_expiration_date = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<DateTime?>((int)ll_found, "expiration_date");
            }
            else
            {

                ldt_curr_expiration_date = this.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "expiration_date");
            }
            if (ldt_curr_expiration_date == Convert.ToDateTime("01/01/1900").Add(Convert.ToDateTime("00:00:00").TimeOfDay))
            {

                ldt_curr_expiration_date = null;
            }

            ll_min_valid_month = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "min_valid_months");
            ls_curr_expiration_date = ldt_curr_expiration_date.ToString();
            if (ls_curr_expiration_date == null)
            {
                ls_curr_expiration_date = "";
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_handle_expiration_date", "Function: uf_handle_expiration_date", Convert.ToString(" Expiration date: " + ls_curr_expiration_date), "");
            // Vika 2012-02-23 SPUCM00003469 פג תוקף בקליטת מזון תינוקות במסופון
            bool lb_303_department = false;

            lb_303_department = await this.uf_check_303_department(ll_material_number);
            //IF NOT(IsNull(ll_min_valid_month) OR ll_min_valid_month = 0) AND IsNull(ldt_curr_expiration_date) THEN
            if (!(ll_min_valid_month == null || ll_min_valid_month == 0) && (ldt_curr_expiration_date == null || (lb_303_department)))
            {
                // End Vika 2012-02-23 SPUCM00003469 פג תוקף בקליטת מזון תינוקות במסופון

                if (!((await uf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, al_row, false, this.dw_inv_pack_details))).Retrieve(out ldt_curr_expiration_date))
                {
                    return -1;
                }
                else
                {

                    if (ldws_status == ModelAction.Insert && !lb_is_item_b2b)
                    {

                        // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                        (this.dw_inv_pack_details).SetItem(al_row, "expiration_date", ldt_curr_expiration_date);
                    }
                    else
                    {

                        this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date.ToString());
                    }
                }
            }
            // Set expiration_date in DW also

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "expiration_date", ldt_curr_expiration_date);
            return 0;
        }

        public async Task<int> uf_set_quantity_and_doc_no(string as_expected_material_quantity, long al_row) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_manual
            //*Function Name:	uf_set_quantity_and_doc_no
            //*Purpose: 			Set the order and invoice quantity
            //*Arguments: 		String	as_expected_material_quantity - the quantity the user has been entered
            //*						Long		al_row
            //*Return:				Integer:  1 - Success
            //*													-1 - Otherwise 
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*08-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            int li_rtn_code = 0;
            int li_b2b_status = 0;
            int li_doc_no_en = 0;
            int li_status = 0;
         //   char lc_doc_type = '\0';
         //   char lc_doc_state = '\0';
            string ls_find = null;
            string ls_find_dw = null;
            string ls_barcode = null;
            long? ll_found = 0;
            long ll_found_dw = 0;
            long ll_b2b_status = 0;
            long ll_row_count = 0;
            long? ll_doc_no = 0;
            long ll_current_doc_no = 0;
            long ll_doc_no_en = 0;
            long ll_docs_count = 0;
            long ll_b2b_docs_count = 0;
            long ll_material_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_no_of_rows = 0;
            long ll_row = 0;
            long ll_no_of_rows_display = 0;
            long ll_no_of_rows_filtered = 0;
            long ll_row_no = 0;
            long ll_filtered_row_no = 0;
            long ll_displayed_row_no = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_cntr = 0;
            long[] ll_doc_number = null;
            decimal ldc_order_quantity = default(decimal);
            decimal ldc_invoice_pack_quantity = default(decimal);
            bool lb_need_set_item = false;
            ModelAction ldw_status = default(ModelAction);

            ll_no_of_rows_display = this.dw_inv_pack_details.RowCount() - 1;
            ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
            ll_material_number = await this.iuo_material_search.uf_get_material_number(this.is_new_barcode);

            // TODO: Type '[dynamic]' does not have a 'GetChild' member. (CODE=30002)
            GridColumn ldwc_docs = this.GetDocNo(this.dw_inv_pack_details);
            // AlexKh - 1.2.48.34 - 2012-12-05 - SPUCM00003402 - check if negative value, then set zero
            if (Convert.ToDecimal(as_expected_material_quantity) < 0)
            {
                as_expected_material_quantity = "0";
            }

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", Convert.ToDecimal(as_expected_material_quantity));
            // Update material_quantity in the DW also in b2b items

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "material_quantity", Convert.ToDecimal(as_expected_material_quantity));
            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets
            //IF uf_choose_invoice_number(al_row, ll_material_number) THEN
            //	ll_doc_no = il_invoice_number
            ll_b2b_docs_count = this.invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
            if (ll_b2b_docs_count == 1)
            {
                ll_doc_no = ll_doc_number[1];
                uf_prepare_material_details(ll_material_number, Convert.ToDecimal(as_expected_material_quantity), ll_doc_no.Value, al_row);
                // If new item does't belong to invoice, then give a message
                li_status = await uf_is_item_in_invoice(ll_doc_no.Value, ll_material_number);
                if (li_status == -1)
                {

                    ll_doc_no = null;
                    uf_set_doc_no(al_row, ll_doc_no.Value, true);

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "doc_no_en", 2);
                }
                else
                {
                    uf_set_doc_no(al_row, ll_doc_no.Value, true);
                    lb_need_set_item = true;
                }
            }
            else if (ll_b2b_docs_count > 1)
            {
                //THIS.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך")

                ll_doc_no = this.dw_inv_pack_details.GetItemValue<int>(al_row, "doc_no");
                if (ll_doc_no == 0 || ll_doc_no == 0)
                {
                    uf_prepare_material_details(ll_material_number, Convert.ToDecimal(as_expected_material_quantity), ll_doc_no.Value, al_row);
                    uf_set_doc_no(al_row, this.il_invoice_numberProperty.Value, true);
                    // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_row, "doc_no_en", 1);
                    lb_need_set_item = true;
                }
            }
            else
            {
                //THIS.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך")
                await this.uf_show_message("שים לב", "", "OK", "הפריט לא שייך למשטח");

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "doc_no_en", 2);
                return 1;
            }
            ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + this.il_invoice_numberProperty.ToString();


            ll_found = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find(ls_find, 0, this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount());
            if (ll_found >= 0)
            {


                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity"));
            }
            ldc_order_quantity = this.iuo_order_searchProperty.uf_get_order_material_quantity(ll_material_number);
            if (ldc_order_quantity == -1)
            {
                await this.uf_show_message("Error", "", "OK", "שגיאה בשליפת כמות הפריט בהזמנה");
            }

            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
            (this.dw_inv_pack_details).SetItem(al_row, "order_quantity", ldc_order_quantity);
            if (lb_need_set_item)
            {
                await this.uf_set_items("doc_no", this.il_invoice_numberProperty.ToString(), al_row);
            }
            return 1;
        }

        public bool uf_set_doc_no(long al_row, long al_doc_no, bool ab_set_values) //************************************************************************
        {
            //*Object:				uo_mini_terminal_pallet_manual
            //*Function Name:	uf_set_doc_no
            //*Purpose: 			Set invoice number in masofon for item
            //*Arguments: 		Long - al_row - Row number dw_inv_pack_details
            //*Return:				Boolean - TRUE - Success to update invoice number.
            //*									 FALSE - Failed to update invoice number.
            //*Date				Programer		Version	Task#	 		Description
            //*-------------------------------------------------------------------------
            //*26-08-2010		AlexKh			1.2.46.1	CR#1138		Initial version
            //*************************************************************************

            long ll_ret = 0;
            long? ll_found = -1;
            string ls_find = null;
            if (al_doc_no > 0)
            {
                ls_find = "invoice_number == " + al_doc_no.ToString();


                ll_found = this.ids_mini_terminal_pallet_invoicesProperty.Find(ls_find, 0, this.ids_mini_terminal_pallet_invoicesProperty.RowCount());
            }
            if (ll_found >= 0)
            {

                this.il_order_numberProperty = (long)this.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>((int)ll_found, "order_number");

                this.il_supplier_numberProperty = this.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<long>((int)ll_found, "supplier_number");

                this.il_invoice_numberProperty = (long)this.ids_mini_terminal_pallet_invoicesProperty.GetItemValue<double>((int)ll_found, "invoice_number");
                this.invuo_doc_item_searchProperty.uf_set_order_number(this.il_order_numberProperty);
                this.invuo_doc_item_searchProperty.uf_set_supplier_number(this.il_supplier_numberProperty);
                this.iuo_order_searchProperty.uf_set_order_number(this.il_order_numberProperty);
                this.iuo_order_searchProperty.uf_set_supplier_number(this.il_supplier_numberProperty);
            }
            else if (al_row == -1)
            {
                this.il_order_numberProperty = 0;
                this.il_supplier_numberProperty = 0;
                this.il_invoice_numberProperty = 0;
            }
            if (al_row >= 0 && ab_set_values)
            {

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "order_number", this.il_order_numberProperty);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "supplier_number", this.il_supplier_numberProperty);

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_row, "doc_no", al_doc_no);
            }
            return true;
        }

        public int uf_prepare_material_details(long al_material_number, decimal al_quantity, long al_invoice_number, long al_current_row) //*********************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_prepare_material_details
            //*Purpose:							Find all the invoices containing this material
            //*										and try to determine with which invoice 
            //*										to associate material
            //*Arguments:							Long 	al_material_number
            //*										Long	al_quantity
            //*										Long	al_invoice_number
            //*										Long	al_current_row
            //*Return:								INTEGER:    ll_rows	- rows number
            //*													 	-1 - Error
            //*Date 			Programer		Version		Task#			Description
            //*---------------------------------------------------------------------
            //*03-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //*********************************************************************

            long ll_rows = 0;
            long ll_i = 0;
            long ll_start = 0;
            long ll_doc_no = 0;
            long? ll_found = 0;
            long ll_invoice_number = 0;
            string ls_filter = null;
            decimal ldec_expected_material_quantity = default(decimal);
            decimal ldec_material_quantity = default(decimal);
            decimal ldec_diff = default(decimal);
            bool lb_assigned = false;
            if (al_material_number <= 0 || al_material_number == 0)
            {
                return -1;
            }
            if (al_invoice_number == 0)
            {
                al_invoice_number = 0;
            }
            if (al_invoice_number > 0)
            {
                ls_filter = "invoice_number =" + al_invoice_number.ToString() + " AND material_number =" + al_material_number.ToString();
            }
            else
            {
                ls_filter = "material_number =" + al_material_number.ToString();
            }
            //Filter all the rows for material with all it's invoices

            this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter(ls_filter);

            this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();

            ll_rows = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.RowCount();
            if (ll_rows == 0)
            {
                //Nothing to associate

                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                (this.dw_inv_pack_details).SetItem(al_current_row, "doc_no_en", 2); //enabled
            }
            else if (ll_rows == 1)
            {

                this.il_supplier_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>(0, "supplier_number");

                this.il_invoice_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>(0, "invoice_number");

                this.il_order_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>(0, "packing_list_number");
                if (al_invoice_number == 0) //disabled
                {

                    // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                    (this.dw_inv_pack_details).SetItem(al_current_row, "doc_no_en", 1);
                }
                lb_assigned = true;
            }
            else if (ll_rows > 1)
            {
                if (al_quantity > 0) //user already inserted quantity
                {
                    //Check if item already scanned
                    ll_start = this.uf_check_item_scanned_before(al_material_number, ll_start, al_current_row, this.dw_inv_pack_details);
                    if (ll_start >= 0)
                    {

                        ll_invoice_number = this.dw_inv_pack_details.GetItemValue<int>(ll_start, "doc_no");

                        ll_found = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find("invoice_number == " + ll_invoice_number.ToString(), 0, (int)ll_rows);
                        if (ll_found >= 0)
                        {

                            ldec_expected_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");

                            ldec_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                            if ((ldec_material_quantity - ldec_expected_material_quantity) > 0)
                            {

                                this.il_supplier_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "supplier_number");

                                this.il_invoice_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "invoice_number");

                                this.il_order_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "packing_list_number");
                                // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.dw_inv_pack_details).SetItem(al_current_row, "doc_no_en", 1); //disabled
                                lb_assigned = true;
                            }
                        }
                    }
                    if (!lb_assigned && al_current_row > 1)
                    {
                        //Check if we can use invoice of 1 item before

                        ll_invoice_number = this.dw_inv_pack_details.GetItemValue<int>(al_current_row - 1, "doc_no");

                        ll_found = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Find("invoice_number == " + ll_invoice_number.ToString(), 0, (int)ll_rows);
                        if (ll_found >= 0)
                        {

                            this.il_supplier_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "supplier_number");

                            this.il_invoice_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "invoice_number");

                            this.il_order_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_found, "packing_list_number");
                            // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

                            // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                            (this.dw_inv_pack_details).SetItem(al_current_row, "doc_no_en", 1); //disabled
                            lb_assigned = true;
                        }
                    }
                    if (!lb_assigned)
                    {
                        for (ll_i = 1; ll_i <= ll_rows; ll_i++)
                        {

                            ldec_expected_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_i, "expected_material_quantity");

                            ldec_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_i, "inv_pack_quantity");
                            //will take the order and supplier with the biggest difference between supplied and expected 
                            if ((ldec_material_quantity - ldec_expected_material_quantity) > ldec_diff)
                            {
                                ldec_diff = (ldec_material_quantity - ldec_expected_material_quantity);

                                this.il_supplier_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "supplier_number");

                                this.il_invoice_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "invoice_number");

                                this.il_order_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "packing_list_number");
                                // AlexKh - 1.2.48.0.11 - 2011-10-27 - Fix for pallets

                                // TODO: Type '[dynamic]' does not have a 'SetItem' member. (CODE=30002)
                                (this.dw_inv_pack_details).SetItem(al_current_row, "doc_no_en", 1); //disabled
                                lb_assigned = true;
                            }
                        }
                    }
                }
                else if (al_quantity == 0) // only barcode was inserted by user
                {
                    for (ll_i = 1; ll_i <= ll_rows; ll_i++)
                    {

                        ldec_expected_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_i, "expected_material_quantity");

                        ldec_material_quantity = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<decimal>((int)ll_i, "inv_pack_quantity");
                        //will take the order and supplier with the biggest difference between supplied and expected 
                        if ((ldec_material_quantity - ldec_expected_material_quantity) > ldec_diff)
                        {
                            ldec_diff = (ldec_material_quantity - ldec_expected_material_quantity);

                            this.il_supplier_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "supplier_number");

                            this.il_invoice_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "invoice_number");

                            this.il_order_numberProperty = this.invuo_doc_item_searchProperty.ids_pallet_inv_details.GetItemValue<long>((int)ll_i, "packing_list_number");
                            lb_assigned = true;
                        }
                    }
                }
            }

            this.invuo_doc_item_searchProperty.ids_pallet_inv_details.SetFilter("");

            this.invuo_doc_item_searchProperty.ids_pallet_inv_details.Filter();
            return (int)ll_rows;
        }

        public override async Task<FuncResults<decimal, decimal, decimal, long, int>> uf_get_order_material_price(long al_supplier_number, long al_order_number, long al_material_number, decimal adec_material_price, decimal adec_material_price_after_discount, decimal adec_material_discount_percent, long al_error_number)
        {
            //****************************************************************************************************************
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_order_material_price
            //*Purpose:							Get the material's prices according to the following:
            //*										IF order_number exists and ib_get_material_price_from_order_if_it_exists = TRUE,
            //*										gets material-prices from supplier_order_details table.
            //*										Otherwise gets material-prices from material_supplier table.
            //*  
            //*Arguments:						Pass By			Argument Type		Argument Name								Remark
            //*										-------------------------------------------------------------------------------------------------------------------
            //*										Value				Long					al_supplier_number
            //*										Value				Long					al_order_number
            //*										Value				Long					al_material_number							current material number
            //*										Reference 		Decimal 				adec_material_price
            //*										Reference 		Decimal 				adec_material_price_after_discount	
            //*										Reference 		Decimal 				adec_material_discount_percent
            //*										Reference 		Long		 			al_error_number								-1 - If error occurce, 
            //*																																		100 - If material not assigned to order  
            //*																																		0 - Otherwise 
            //*		
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------------------------------
            //*06-08-2007		SHARP.					0.2.0   		B2B 			Initiation
            //****************************************************************************************************************

            bool lb_material_in_order = false;
            if (al_material_number > 0)
            {
                if (!(al_order_number == 0 || (al_order_number == 0 || (al_supplier_number == 0 || al_supplier_number == 0))))
                {
                    lb_material_in_order = this.iuo_order_searchProperty.uf_get_order_material_price(al_material_number, ref adec_material_price, ref adec_material_price_after_discount, ref adec_material_discount_percent);
                    if (!lb_material_in_order)
                    {
                        //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                        adec_material_price = (await this.iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                        adec_material_price_after_discount = adec_material_price;
                    }
                }
                else
                {
                    //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                    adec_material_price = (await this.iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                    adec_material_price_after_discount = adec_material_price;
                }
            }
            else
            {
                //.. gets value from material_suppliers table by branch_number & supplier_number & material_number
                adec_material_price = (await this.iuo_material_search.uf_get_material_price(masofonAlias.masofon.Instance.gs_vars.branch_number, al_supplier_number, al_material_number, al_error_number)).Retrieve(out al_error_number);
                adec_material_price_after_discount = adec_material_price;
            }
            return FuncResults.Return(adec_material_price, adec_material_price_after_discount, adec_material_discount_percent, al_error_number, 1);
        }

        public async Task<int> uf_is_item_in_invoice(long al_doc_number, long al_material_number) //********************************************************************
        {
            //*Object:				uo_mini_terminal_pallets_manual
            //*Function Name:	uf_is_item_in_invoice
            //*Purpose: 			Check if item belongs to the invoice 
            //*Arguments: 		Long	al_doc_number
            //*						Long	al_material_number
            //*Return:				Return 1/-1
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*01-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_found = 0;
            long ll_b2b_status = 0;
            string ls_find = null;
            string ls_rows = null;
            decimal ldec_item_pirce = default(decimal);
            bool lb_status = false;
            lb_status = this.invuo_doc_item_searchProperty.uf_get_doc_item_price(al_doc_number, al_material_number, ref ldec_item_pirce);
            if (!lb_status)
            {
                if (await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OkCancel", 1, " הפריט לא קיים בחשבונית " + al_doc_number.ToString() + ", האם לשייך?") != 1)
                {
                    return -1;
                }
                return 1;
            }
            return 1;
        }

        public override async Task<FuncResults<long, int>> uf_get_selected_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_selected_supplier_number
            //*Purpose:							Get the supplier number of current invoice_number. 
            //*Arguments:							Long	al_doc_no
            //*Return:								Integer: 1 - Success
            //*												   -1 - Failure
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_found = 0;
            long ll_row = 0;
            long ll_invoice_number = 0;
            al_supplier_number = this.il_supplier_numberProperty;
            return FuncResults.Return(al_supplier_number, 0);
        }

        public override async Task<FuncResults<long, int>> uf_get_selected_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_get_selected_order_number
            //*Purpose:							Get the order number of current invoice_number. 
            //*Arguments:							Long	al_doc_no
            //*Return:								Integer: 1 - Success
            //*												   -1 - Failure
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_found = 0;
            long ll_row = 0;
            long ll_invoice_number = 0;
            al_order_number = this.il_order_numberProperty;
            return FuncResults.Return(al_order_number, 0);
        }


        public override async Task<int> uf_show_items(bool ab_all_items) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_show_items
            //*Purpose:							show  items in dw_inv_pack_details :all items if ab_all_items = TRUE 
            //*										and invoice against message if ab_all_items is FALSE 
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										------------------------------------------------------------------
            //*										Value 		Boolean 					ab_all_items
            //*
            //*Return:								Integer: 1 - Success.
            //*												  -1 - Otherwise.
            //*Date 		Programer	Version		Task#				Description
            //*-----------------------------------------------------------------------------------------------
            //*4-10-2010  SHARP.      1.2.46.3   	SPUCM00002425 	Initiation
            //***********************************************************************************************

            string ls_filter = null;
            string ls_orig_filter = null;
            int ll_row_count = 0;

            ll_row_count = this.dw_inv_pack_details.RowCount();
            //Load origenal filter of dw



            ls_orig_filter = Convert.ToString(this.dw_inv_pack_details.GetFilter());
            if (ab_all_items)
            {
                //Show all items
                ls_filter = "";
                if (ls_orig_filter != ls_filter)
                {

                    (this.dw_inv_pack_details).SetFilter(ls_filter);
                }

                this.dw_inv_pack_details.Filter();
                if (ll_row_count == 0)
                {
                    if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                    {
                        await this.uf_new_row(this.dw_inv_pack_details);
                    }
                }
                var cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");
                cb_show_all.Text = "הצג פריטים עם הפרשים";
            }
            else
            {
                //Show diff items
                ls_filter = "((invoice_pack_quantity <> expected_material_quantity) and ";
                ls_filter += "(barcode <> \"\" OR  barcode <> null)) ";
                //ls_filter += "OR (invoice_pack_quantity == null AND expected_material_quantity == null";
                //ls_filter += " AND order_quantity == null AND barcode == null)";
                if (ls_orig_filter != ls_filter)
                {

                    (this.dw_inv_pack_details).SetFilter(ls_filter);
                }

                (this.dw_inv_pack_details).Filter();
                if (ll_row_count == 0)
                {
                    if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                    {
                        await this.uf_new_row(dw_inv_pack_details);
                    }
                }
                var cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");
                cb_show_all.Text = "הצג את כל הפריטים";
            }

            ll_row_count = this.dw_inv_pack_details.RowCount() - 1;

            (this.dw_inv_pack_details).SetSort("row_no");

            (this.dw_inv_pack_details).Sort();
            if (await this.uf_is_new_row_exist(this.dw_inv_pack_details))
            {
                Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e) =>
                {
                    this.dw_inv_pack_details.SetFocus((int)ll_row_count, "barcode");
                });
            }
            else
            {
                Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e) =>
                {
                    this.dw_inv_pack_details.SetFocus((int)ll_row_count, "expected_material_quantity");
                });
            }
            return 1;
        }

        public override async Task<int> uf_enter(string as_column, long al_row) //************************************************************************
        {
            //*Object:								uo_mini_terminal_pallet_manual
            //*Function/Event  Name:			uf_enter
            //*Purpose:							Enter Clicked, handle row changes
            //*Arguments:							Value 	String 	as_column
            //*										Value		Long		al_row
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed.
            //*Date 			Programer			Version		Task#			Description
            //*-------------------------------------------------------------------------
            //*20-06-2010		AlexKh			   1.2.46.0  	CR#1138 		Initial Version
            //*************************************************************************

            long ll_b2b_status = 0;
            Int64? ll_doc_no = 0;
            long ll_doc_no_en = 0;
            long ll_row_no = 0;
            long ll_cntr = 0;
            long ll_serial_number = 0;
            long ll_RowCount = 0;
            long ll_row = 0;
            long ll_material_number = 0;
            decimal? ll_material_quantity = 0;
            string ls_barcode = null;
            string ls_b2b_status = null;
            string ls_string = null;
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", "Start.", Convert.ToString("Column:   " + as_column), Convert.ToString("Row:      " + al_row.ToString()));
            ModelAction ldis_status = default(ModelAction);


            ldis_status = this.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
            f_dw_enter_key_moveClass.f_dw_enter_key_move((GridElement)this.dw_inv_pack_details);

            if (this.dw_inv_pack_details.RowCount() < 1)
            {
                return 0;
            }

            ll_b2b_status = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "b2b_status");
            ls_b2b_status = ll_b2b_status.ToString();
            if (ls_b2b_status == null)
            {
                ls_b2b_status = "Null (No)";
            }
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), Convert.ToString("B2B status:" + ls_b2b_status), "");
            switch (as_column)
            {
                case "barcode":

                    ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
                    ls_barcode = await this.iuo_material_search.uf_get_barcode_main(ls_barcode, false);


                    (this.dw_inv_pack_details).SetItem(al_row, "barcode", ls_barcode);
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), Convert.ToString("Barcode: " + ls_barcode), "");
                    if (string.IsNullOrEmpty(ls_barcode))
                    {
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "barcode");
                    }
                    else
                    {
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "expected_material_quantity");
                    }
                    break;
                case "expected_material_quantity":
                    var barcode = this.dw_inv_pack_details.GetItemValue<object>(al_row, "barcode");
                    ll_material_quantity = this.dw_inv_pack_details.GetItemValue<decimal?>(al_row, "expected_material_quantity");

                    if (string.IsNullOrEmpty(Convert.ToString(barcode)) && string.IsNullOrEmpty(Convert.ToString(ll_material_quantity)))
                    {
                        return 0;
                    }

                    ll_row_no = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "row_no");
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), Convert.ToString("Row no:   " + ll_row_no.ToString()), "");
                    // Check if the quantity is null

                    if (ll_material_quantity == 0 || ll_material_quantity == null)
                    {
                        await this.uf_show_message("", "", "OK", "#20011");
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "expected_material_quantity");
                        return 0;
                    }
                    // If at least 1 doc is not b2b, set focus to the doc_no field

                    ll_material_number = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");

                    ll_doc_no_en = this.dw_inv_pack_details.GetItemValue<Int64>(al_row, "doc_no_en");

                    ll_RowCount = this.dw_inv_pack_details.RowCount();
                    if (ll_row_no == 1 && (ll_b2b_status != 1 && ll_doc_no_en > 1))
                    {
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "doc_no");
                        return 1;
                    }
                    else
                    {

                        ll_doc_no = this.dw_inv_pack_details.GetItemValue<Int64?>(al_row, "doc_no");
                        this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), Convert.ToString("Doc no:   " + ll_doc_no.ToString()), "");
                        if ((ll_b2b_status == 1) && (!(ll_doc_no == 0) || ll_doc_no > 0))
                        {


                            ll_serial_number = this.dw_inv_pack_details.GetItemValue<long>(al_row, "serial_number");
                            if (!(ll_serial_number > 0) || ll_serial_number == 0)
                            {
                                await this.uf_set_items("doc_no", ll_doc_no.ToString(), al_row);
                            }
                            if (ll_doc_no_en > 1)
                            {
                                await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "doc_no");
                            }
                            else
                            {
                                if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                                {
                                    //this.ViewModel.dw_inv_pack_details.dw_inv_pack_details_ue_new_row();
                                    await dw_inv_pack_details_ue_new_row();
                                }
                                else
                                {
                                    al_row = this.dw_inv_pack_details.RowCount() - 1;
                                    await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "barcode");
                                }
                            }
                        }
                    }
                    break;
                case "doc_no":
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), "", "");
                    // Check if doc_no is null - the user did not choose a doc.

                    ll_doc_no = this.dw_inv_pack_details.GetItemValue<int>(al_row, "doc_no");
                    if (ll_doc_no == 0 || ll_doc_no == 0)
                    {
                        //THIS.uf_show_message("שים לב", "", "OK", "נא לבחור מספר מסמך")
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", al_row, "doc_no");
                        return 1;
                    }
                    if (!await this.uf_is_new_row_exist(this.dw_inv_pack_details))
                    {

                        // TODO: Type '[dynamic]' does not have a 'dw_inv_pack_details_ue_new_row' member. (CODE=30002)
                        //(this.ViewModel.dw_inv_pack_details).dw_inv_pack_details_ue_new_row();
                        await dw_inv_pack_details_ue_new_row();
                    }
                    else
                    {

                        ll_RowCount = this.dw_inv_pack_details.RowCount();
                        this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:    " + as_column), Convert.ToString("RowCount: " + ll_RowCount.ToString()), "");
                        for (ll_cntr = 0; ll_cntr < ll_RowCount; ll_cntr++)
                        {


                            ls_barcode = this.dw_inv_pack_details.GetItemValue<string>(ll_cntr, "barcode");
                            if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
                            {
                                ll_row = ll_cntr;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        await this.uf_set_focus("uo_mini_terminal_pallet_manual", "uo_mini_terminal_pallet_manual_dw_inv_pack_details", ll_row, "barcode");
                    }
                    break;
                default:
                    break;
            }
            return 1;
        }

        public async Task<int> dw_inv_pack_details_ue_new_row() // AlexKh - 1.2.46.0 - 2010-07-01 - CR#1138 - Call uf_new_row
        {
            return await this.uf_new_row(this.dw_inv_pack_details);
        }

        public override async Task<int> uf_finish_input() //************************************************************************
        {
                        //AlexKh - 1.2.46.0 - 2010-07-07 - CR#1138 - Unlock all locked invoices 
            if (!await this.uf_invoice_lock("UNLOCK", "", 0))
            {
                await this.uf_show_message("שגיאה", "", "1", ".שגיאה בשחרור חשבוניות נעולות");
                return 1;
            }
            //this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_finish_input", "uf_finish_input Function, Print.", "", "");
            // Reset window
            // Reset window

            //await w_mini_terminal.Instance().wf_reset();
            dw_inv_pack_details.Reset();
            dw_summary.Reset();
            this.idec_item_quantity_change_differenceProperty = 0;
            await this.uf_display("SHIPMENT");
            await this.uf_set_focus("uo_mini_terminal_pallets_list", "dw_pallets_list", 1, "pallet_number");
            return 1;
        }
    }
}
