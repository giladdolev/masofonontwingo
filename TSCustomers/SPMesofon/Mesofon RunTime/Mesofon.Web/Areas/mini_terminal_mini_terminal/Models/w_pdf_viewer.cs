using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using global;
using Mesofon.Common.Global;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;
using System.Data;
using System.Web.VisualTree.Extensions;
using Mesofon.Repository;
using Mesofon.Common;
using System.Drawing;
using System.Web.VisualTree.MVC;
using Mesofon.Data;
using Mesofon.Models;
using System.Threading.Tasks;
using MvcSite.Common;
using System.Web.VisualTree.WebControls;


namespace mini_terminal
{
    // Creation Time:   07/06/2016 11:46:20 (Utc)
    // Comments:        
    // 
    public class w_pdf_viewer : WindowElement
    {
        public string pdf { get; set; }
        public w_pdf_viewer(string pdf)
        {
            this.pdf = pdf;
        }
        
        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
