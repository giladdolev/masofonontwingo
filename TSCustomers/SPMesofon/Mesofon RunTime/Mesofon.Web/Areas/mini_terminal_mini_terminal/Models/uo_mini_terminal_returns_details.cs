using System.Web.VisualTree.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Extensions;
using global;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Mesofon.Common;
using System.Drawing;
using System.Threading.Tasks;
using MvcSite.Common;
namespace mini_terminal
{
    // Creation Time:   09/14/2016 11:10:14 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_returns_details : uo_mini_terminal_base_body
    {
        //SPUCM00003614
        public GridElement idwc_distributor_num;
        public long il_return_son;
        public long il_supplier;
        public long il_distributor;
        public bool ib_marlog_return;
        public IRepository ids_adhoc_update_list;
        //boolean	ib_return_marlog
        // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_marlog_branch;
        public bool uf_reset()
        {
            LabelElement st_carton_number = this.GetVisualElementById<LabelElement>("st_carton_number");
            LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            
            dw_inv_pack_details.Reset();
            st_shipment_number.Text = "";
            st_carton_number.Text = "";

            return true;
        }
        public override async Task<int> uf_new_row(WidgetGridElement dw_inv_pack_details)
        {
            long ll_row = 0;
            long ll_last_row = 0;
            // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call rowfocuschanging
            ll_row = dw_inv_pack_details.Insert(0);
            ll_row = dw_inv_pack_details.RowCount() - 1;
            var ll_row1 = ll_row + 1;
            dw_inv_pack_details.SetItem(ll_row, "row_no", ll_row1);
            dw_inv_pack_details.SetItem(ll_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number);
            // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call rowfocuschanging
            ll_last_row = dw_inv_pack_details.Rows.Count - 1;

            if ((int)ll_last_row > -1)
            {
                dw_inv_pack_details.SetEnabled((int)ll_last_row, "declines", true);
            }


            //GalilCS - Get json of decline list
            IRepository decline_List = new dddw_get_return_reasonsRepository();
            ll_row = decline_List.Retrieve(3);
            GetDeclines().SetRepository(decline_List);
            var dt = decline_List.GetDataTable();
            dt.Columns[0].ColumnName = "value";
            dt.Columns[1].ColumnName = "text";
            var JsonData = GridMultiComboBoxElement.GetComboBoxData("", dt);
            dw_inv_pack_details.Rows[(int)ll_last_row].Cells["invoice_data"].Value = JsonData;
            //Set the position of Horizonal-Scroll. 
            //dw_inv_pack_details.set_HorizontalScrollPosition(dw_inv_pack_details.get_HorizontalScrollMaximum());
            return 0;
        }
        public async System.Threading.Tasks.Task<int> uf_create(bool ab_marlog_return, string as_carton_barcode, long al_return_number, long al_distributor_number)
        {
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            LabelElement st_carton_number = this.GetVisualElementById<LabelElement>("st_carton_number");
            LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
          //  GridElement dwc_declines;
            long ll_row = 0;
            long ll_invoice_num = 0;
            long ll_distributor = 0;
            long ll_mat_num = 0;
            long ll_dec_num = 0;
            long ll_supplier = 0;
            long ll_row_serial_number = 0;
            string ls_rc = null;
            string ls_mat_name = null;
            string ls_barcode = null;
            string ls_adhock_key = null;
            char? lc_state_1 = null;
            char? lc_state = null;
            long li_row = 0;
            decimal ldc_mat_qty = default(decimal);
            uf_reset();
            //
            is_carton_barcode = as_carton_barcode;
            il_return_number = al_return_number;
            ib_marlog_returnProperty = ab_marlog_return;
            if (ib_marlog_returnProperty)
            {
                il_distributorProperty = al_distributor_number;
                // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - set marlog branch_number
                il_marlog_branch = await f_is_marlog_distributorClass.f_is_marlog_distributor(0, il_distributorProperty);
            }
            st_shipment_number.Text = il_return_number.ToString();
            st_carton_number.Text = is_carton_barcode;
            if (!isempty_stringClass.isempty_string(is_carton_barcode) && il_return_number > 0)
            {
                // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - retrieve row serial number

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_return_number }, { "@2", is_carton_barcode }, { "@3", il_marlog_branch } };
                    string sql = "SELECT SMR.supplier_number,  SMR.invoice_number,  SMR.state,  SMR.adhoc_key,  SMR.material_number,  SMR.material_quantity,  ID.decline_number,  M.barcode,  M.name,  M.distributor_number,  SMR.row_serial_number FROM shipment_marlog_return AS SMR,  invoice_details AS ID,  materials AS M WHERE SMR.branch_number = ID.branch_number AND SMR.supplier_number = ID.supplier_number AND SMR.invoice_number = ID.invoice_number AND SMR.material_number = ID.material_number AND SMR.material_quantity = ID.material_quantity AND IsNull(ID.state, '') <> 'd' AND ID.decline_number > 0 AND SMR.branch_number = @0 AND SMR.shipment_number = @1 AND SMR.carton_barcode = @2 AND SMR.state <> 'd' AND SMR.marlog_number = @3 AND SMR.material_number = M.number ORDER BY row_serial_number ";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ll_invoice_num = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        lc_state = UnitOfWork.GetValue(resultRS, 2, Convert.ToChar);
                        ls_adhock_key = UnitOfWork.GetValue(resultRS, 3, Convert.ToString);
                        ll_mat_num = UnitOfWork.GetValue(resultRS, 4, Convert.ToInt64);
                        ldc_mat_qty = UnitOfWork.GetValue(resultRS, 5, Convert.ToDecimal);
                        ll_dec_num = UnitOfWork.GetValue(resultRS, 6, Convert.ToInt64);
                        ls_barcode = UnitOfWork.GetValue(resultRS, 7, Convert.ToString);
                        ls_mat_name = UnitOfWork.GetValue(resultRS, 8, Convert.ToString);
                        ll_distributor = UnitOfWork.GetValue(resultRS, 9, Convert.ToUInt32);
                        ll_row_serial_number = UnitOfWork.GetValue(resultRS, 10, Convert.ToInt64);


                        if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        if (!await uf_is_new_row_exist(dw_inv_pack_details))
                        {
                            await uf_new_row(dw_inv_pack_details);
                        }

                        li_row = dw_inv_pack_details.RowCount() - 1;

                        dw_inv_pack_details.SetItem(li_row, "barcode", Convert.ToString(ls_barcode));
                        dw_inv_pack_details.SetItem(li_row, "serial_number", il_return_number);
                        dw_inv_pack_details.SetItem(li_row, "carton_barcode", Convert.ToString(is_carton_barcode));
                        dw_inv_pack_details.SetItem(li_row, "supplier_number", ll_supplier);
                        dw_inv_pack_details.SetItem(li_row, "distributor_number", ll_distributor); //+
                        dw_inv_pack_details.SetItem(li_row, "material_number", ll_mat_num); //+
                        dw_inv_pack_details.SetItem(li_row, "material_name", Convert.ToString(ls_mat_name)); //+
                        dw_inv_pack_details.SetItem(li_row, "material_quantity", ldc_mat_qty); //+
                        dw_inv_pack_details.SetItem(li_row, "declines", ll_dec_num); //+
                        dw_inv_pack_details.SetItem(li_row, "doc_state", lc_state.ToString()); //+
                        dw_inv_pack_details.SetItem(li_row, "doc_no", ll_invoice_num); //invoice_number
                        dw_inv_pack_details.SetItem(li_row, "adhoc_key", Convert.ToString(ls_adhock_key)); //adhoc number used for item return
                        dw_inv_pack_details.SetItem(li_row, "row_serial_number", ll_row_serial_number); //save row serial number connecting invoice_details and shipment_marlog_return rows
                        dw_inv_pack_details.SetItemStatus((int)li_row, 0, ModelBuffer.Primary, ModelAction.None);
                        dw_inv_pack_details.SetItemStatus((int)li_row, 0, ModelBuffer.Primary, ModelAction.None);
                    }
                }
            }
            if (!await uf_is_new_row_exist(dw_inv_pack_details) && (lc_state == uo_mini_terminal_returns_details.RETURN_STATE_OPEN || isempty_stringClass.isempty_string(Convert.ToString(lc_state))))
            {
                await uf_new_row(dw_inv_pack_details);
            }

            ll_row = UpdateComboGridAndDeclines(dw_inv_pack_details);
            
            return 1;
        }

        internal long UpdateComboGridAndDeclines(WidgetGridElement dw_inv_pack_details,int? deletedRow = null)
        {
            long ll_row;
            GridElementExtensions.SetDataSourceEx(dw_inv_pack_details, dw_inv_pack_details.Repository().GetDataTable());
            IRepository decline_List = new dddw_get_return_reasonsRepository();
            ll_row = decline_List.Retrieve(3);
            GetDeclines().SetRepository(decline_List);
            var dt = decline_List.GetDataTable();
            dt.Columns[0].ColumnName = "value";
            dt.Columns[1].ColumnName = "text";

            //GalilCS - Get json of decline list
            var JsonData = GridMultiComboBoxElement.GetComboBoxData("", dt);

            for (int i = 0; i < dw_inv_pack_details.Rows.Count; i++)
            {
                dw_inv_pack_details.Rows[i].Cells["invoice_data"].Value = JsonData;
                //Get decline name
                var declineName = GetDeclinesName(GetDeclines(), dw_inv_pack_details.Rows[i].Cells["declines"].Value);
                if (string.IsNullOrEmpty(declineName))
                {
                    dw_inv_pack_details.Rows[i].Cells["declines"].Value = "";
                }
                else
                    dw_inv_pack_details.Rows[i].Cells["declines"].Value = declineName;
            }
            var lastRow = dw_inv_pack_details.Rows.Count - 1;
            var selectRow = deletedRow == null ? lastRow : deletedRow.Value;
            Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e2) =>
            {
                dw_inv_pack_details.SetFocus(selectRow, "barcode");
            },delay:120);
            return ll_row;
        }

        public uo_mini_terminal_returns_details(GridElement dw_summary)
        {
            _dw_summary = dw_summary;
        }

        public void constructor()
        {
            base.constructor();
            this.ids_adhoc_update_listProperty = new d_addhockRepository();
        }
        public long il_return_sonProperty
        {
            get { return this.il_return_son; }
            set { this.il_return_son = value; }
        }
        public long il_supplierProperty
        {
            get { return this.il_supplier; }
            set { this.il_supplier = value; }
        }
        public long il_distributorProperty
        {
            get { return this.il_distributor; }
            set { this.il_distributor = value; }
        }
        public bool ib_marlog_returnProperty
        {
            get { return this.ib_marlog_return; }
            set { this.ib_marlog_return = value; }
        }
        public IRepository ids_adhoc_update_listProperty
        {
            get { return this.ids_adhoc_update_list; }
            set { this.ids_adhoc_update_list = value; }
        }
        public void LoadData1(long ll_supplier, ref long ll_row_exists, ref decimal ldc_qty)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_return_number }, { "@2", ll_supplier } };
                    string sql = "SELECT 1,  invoice_total FROM invoice_move WHERE branch_number = @0 AND invoice_number = @1 AND supplier_number = @2 AND invoice_type = 'r'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ldc_qty = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT MAX(b.price_before_discount),  MAX(b.price_after_discount),  MAX(b.material_discount),  MAX(b.supplier_discount_percent) FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(Int64 al_material_number, ref decimal ldec_sell_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT sell_price FROM branch_item_price WHERE branch_number = @0 AND item_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_sell_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateData4(DateTime? ldt_time, long ll_supplier, long ll_return_son)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_time }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier }, { "@3", ll_return_son } };
                    string sql = "UPDATE invoice_move SET date_move = @0,  state = 'D' WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(string ls_barcode, ref long ll_supplier, ref string ls_material_name, ref long ll_material_number, ref long ll_distributor, ref long ll_return_allowed, ref DateTime? ldt_last_return_date)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name,  number,  distributor_number,  return_allowed,  last_return_date FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        ll_material_number = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        ll_distributor = UnitOfWork.GetValue(resultRS, 3, Convert.ToInt64);
                        ll_return_allowed = UnitOfWork.GetValue(resultRS, 4, Convert.ToInt64);
                        ldt_last_return_date = UnitOfWork.GetValue(resultRS, 5, Convert.ToDateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(long ll_distributor, long ll_supplier, ref long ll_row_exists)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_distributor }, { "@1", ll_supplier } };
                    string sql = "SELECT 1 FROM supplier_distributors WHERE is_active = 1 AND distributor_number = @0 AND supplier_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(string ls_dist1, string ls_dist2, ref long ll_row_exists2)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_dist1 }, { "@1", ls_dist2 } };
                    string sql = "SELECT 1 FROM param_values WHERE param_code = 46 AND param_value = @0 AND param_name = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists2 = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(string ls_supplier, ref long ll_row_exists3)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_supplier } };
                    string sql = "SELECT 1 FROM param_values WHERE param_code = 49 AND param_value = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists3 = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData10(string ls_barcode, ref long ll_supplier, ref string ls_material_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        private GridElement _dw_summary;
        public GridElement dw_summary
        {
            get
            {
                return _dw_summary;
            }
        }
        internal ControlElement GetControlByID(string id)
        {
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            var col = dw_inv_pack_details.Columns[0] as WidgetColumn;
            if (col != null)
            {
                foreach (var item in col.Items)
                {
                    if (item.DataMember == id)
                        return item;
                }
            }
            return null;
        }
        public WidgetGridElement dw_inv_pack_details
        {
            get
            {
                return this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            }
        }
        public GridColumn GetDeclines()
        {
            return this.dw_inv_pack_details.Columns["declines"];
        }

        /// <summary>
        /// Get name of decline number
        /// </summary>
        /// <param name="col"></param>
        /// <param name="ll_dec_num"></param>
        /// <returns></returns>
        public string GetDeclinesName(GridColumn col, object ll_dec_num)
        {
            string returnValue = "";
            if (ll_dec_num == null)
            {
                return null;
            }
            long decline_number;
            bool isConverted = Int64.TryParse(Convert.ToString(ll_dec_num),out decline_number);
            if (isConverted)
            {
                int? find_row = col.GetRepository().Find(string.Format("decline_number == {0}", ll_dec_num), 0, col.GetRepository().RowCount());
                if (find_row != null && find_row > -1)
                {
                    returnValue = Convert.ToString(col.GetRepository().GetItemValue<object>((int)find_row, "display_name"));
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Get number of decline name
        /// </summary>
        /// <param name="combo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public long GetDeclinesNumber(GridColumn col, object data)
        {
            long returnValue = 0;
            string decline_number = Convert.ToString(data);
            int? find_row = col.GetRepository().Find("display_name == \"" + data.ToString() + "\"", 0, col.GetRepository().RowCount());
            if (find_row != null && find_row > -1)
            {
                returnValue = Convert.ToInt64(col.GetRepository().GetItemValue<object>((int)find_row, "decline_number"));
            }
            return returnValue;
        }


        public void UpdateDataS11(DateTime? ldt_time, decimal ldc_total, long ll_return_son, long ll_supplier)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ldt_time", ldt_time },
                        { "@ldc_total", ldc_total },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@invoice_number", ll_return_son },
                        { "@supplier_number", ll_supplier }
                    };

                    string sql = "UPDATE invoice_move " +
                        "SET last_update_datetime = @ldt_time,  " +
                        "invoice_total = @ldc_total " +
                        "WHERE branch_number = @branch_number AND " +
                        "invoice_number = @invoice_number AND " +
                        "supplier_number = @supplier_number AND " +
                        "invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void UpdateDataS12(DateTime? ldt_time, object adc_qty, object ldc_price, object ll_decline_number, object ll_material_number, object ll_return_son, object ll_supplier, object ll_row_serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ldt_time", ldt_time },
                        { "@adc_qty", adc_qty },
                        { "@ldc_price", ldc_price },
                        { "@ll_decline_number", ll_decline_number },
                        { "@ll_material_number", ll_material_number },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@ll_return_son", ll_return_son },
                        { "@ll_supplier", ll_supplier },
                        { "@ll_row_serial_number", ll_row_serial_number }
                    };

                    string sql = "UPDATE invoice_details " +
                        "	SET		last_update_datetime = @ldt_time, " +
                        "material_quantity = @adc_qty, " +
                        "material_price = @ldc_price, " +
                        "decline_number = @ll_decline_number, " +
                        "material_number = @ll_material_number " +
                        "WHERE       branch_number = @branch_number AND " +
                        "invoice_number = @ll_return_son AND " +
                        "material_number = @ll_material_number AND " +
                        "supplier_number = @ll_supplier AND " +
                        "serial_number = @ll_row_serial_number";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS13(object adc_qty, object ldt_time, object ll_material_number,
            object ll_return_son, object is_carton_barcode, object il_return_number, object ll_supplier,
            object il_marlog_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@adc_qty", adc_qty },
                        { "@ldt_time", ldt_time },
                        { "@ll_material_number", ll_material_number },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@ll_return_son", ll_return_son },
                        { "@is_carton_barcode", is_carton_barcode },
                        { "@il_return_number", il_return_number },
                        { "@ll_supplier", ll_supplier },
                        { "@il_marlog_branch", il_marlog_branch }

                    };

                    string sql = "UPDATE		shipment_marlog_return " +
                                "SET material_quantity = @adc_qty, " +
                                "last_update_datetime = @ldt_time, " +
                                "material_number = @ll_material_number " +
                                "WHERE       branch_number = @branch_number AND " +
                                "invoice_number = @ll_return_son AND " +
                                "carton_barcode = @is_carton_barcode AND " +
                                "shipment_number = @il_return_number AND " +
                                "supplier_number = @ll_supplier AND " +
                                "marlog_number = @il_marlog_branch AND " +
                                "material_number = @ll_material_number; ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS21(DateTime? ldt_time, decimal ldc_total, long ll_return_son, long ll_supplier)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ldt_time", ldt_time },
                        { "@ldc_total", ldc_total },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@invoice_number", ll_return_son },
                        { "@supplier_number", ll_supplier }
                    };

                    string sql = "UPDATE invoice_move  " +
                        "SET last_update_datetime = @ldt_time,  " +
                        "invoice_total = @ldc_total " +
                        "WHERE branch_number = @branch_number AND  " +
                        "invoice_number = @invoice_number AND  " +
                        "supplier_number = @supplier_number AND  " +
                        "invoice_type = 'r' ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS22(object ll_serial, object ll_return_son, object ll_material_number, object adc_qty, object ldc_price, object ll_supplier, object ll_return_serial, object ll_decline_number, object ldc_sell_price, object al_row_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ll_serial", ll_serial },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@ll_return_son", ll_return_son },
                        { "@ll_material_number", ll_material_number },
                        { "@adc_qty", adc_qty },
                        { "@ldc_price", ldc_price },
                        { "@ll_supplier", ll_supplier},
                        { "@ll_return_serial", ll_return_serial},
                        { "@ll_decline_number", ll_decline_number},
                        { "@ldc_sell_price", ldc_sell_price},
                        { "@al_row_no", al_row_no}
                    };

                    string sql = "INSERT INTO invoice_details( " +
                     "serial_number, branch_number, " +
                     "invoice_number, material_number, " +
                     "material_quantity, material_price, " +
                     "details, supplier_number, " +
                     "material_discount_percent, material_price_after_discount, " +
                     "packing_list_number, packing_serial, " +
                     "stock_number, decline_number, " +
                     "sell_price, bonus_quantity, " +
                     "material_bonus_connect, material_discount_amount, " +
                     "color, bonus_discount, " +
                     "indicator, supplier_discount_percent, " +
                     "mini_terminal) " +
         "VALUES(     @ll_serial, @branch_number,    " +
                        "@ll_return_son,			@ll_material_number, " +
                        "@adc_qty,				@ldc_price,  " +
                        "'',						@ll_supplier,  " +
                        "0,							@ldc_price,  " +
                        "@ll_return_serial,							0,  " +
                        "1, @ll_decline_number, " +
                        "@ldc_sell_price, 0, 0, 0, 255, 0, @al_row_no, 0, 1); ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void UpdateDataS23(object ll_return_serial, object il_return_number, object ldt_time, object ll_supplier
            , object ll_return_son, object is_carton_barcode,
            object ll_serial, object ll_material_number, object adc_qty, object ls_adhoc_key, object il_marlog_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ll_return_serial",ll_return_serial  },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@il_return_number", il_return_number },
                        { "@ldt_time",ldt_time  },
                        { "@ll_supplier", ll_supplier },
                        { "@ll_return_son",  ll_return_son},
                        { "@is_carton_barcode", is_carton_barcode},
                        { "@ll_serial",ll_serial },
                        { "@ll_material_number",ll_material_number },
                        { "@adc_qty", adc_qty},
                        { "@ls_adhoc_key", ls_adhoc_key},
                        {"@il_marlog_branch", il_marlog_branch}
                    };

                    string sql = "INSERT INTO shipment_marlog_return " +
                                "(serial_number, shipment_number, " +
                                "               branch_number, date_move, " +
                                "               state, supplier_number, " +
                                "               invoice_number, carton_barcode, " +
                                "               row_serial_number, material_number, " +
                                "               material_quantity, last_update_datetime, " +
                                "               adhoc_key, marlog_number) " +
                                "VALUES( " +
                                "           @ll_return_serial,		 " +                //serial_number
                                "           @il_return_number, 		 " +    //shipment_number ����� ��
                                "           @branch_number, " +
                                "           @ldt_time, 					 " +    //date_move
                                "           'n',                         " +        //state
                                "           @ll_supplier,				 " +    //supplier_number
                                "           @ll_return_son,				 " +//invoice_number ����� ��
                                "           @is_carton_barcode,			 " +//carton_barcode
                                "           @ll_serial,					 " +    //row_serial_number
                                "           @ll_material_number,		 " +    //material_number
                                "           @adc_qty,					 " +    //material_quantity    
                                "           @ldt_time,					 " +	//last_update_datetime 
                                "           @ls_adhoc_key,				 " +//adhoc key " +
                                "           @il_marlog_branch)";//marlog number " 
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }



        public void UpdateDataS31(object il_return_son, object ll_supplier, object ldt_time, object
            ldc_total, object ll_distributor)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@il_return_son",il_return_son  },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@ll_supplier", ll_supplier },
                        { "@active_owner", masofonAlias.masofon.Instance.gs_vars.active_owner },
                        { "@ldt_time",ldt_time  },
                        { "@vat_percent",  masofonAlias.masofon.Instance.qs_parameters.vat_percent},
                        { "@ldc_total", ldc_total},
                        { "@station_number",masofonAlias.masofon.Instance.gs_station.station_number },
                        { "@ll_distributor",ll_distributor }
                    };

                    string sql = "INSERT INTO invoice_move( " +
                         "invoice_number, branch_number, " +
                         "supplier_number, invoice_type, " +
                         "employee_number, supply_date, " +
                         "date_move, discount, " +
                         "mam, invoice_total, " +
                         "stock_number, store_number, " +
                         "station_num, state, " +
                         "distributor_number, b2b_status, " +
                         "log_book, expected_total_amount, " +
                         "discount_percent, last_update_datetime) " +
             "VALUES(@il_return_son,		@branch_number, " +
                        "@ll_supplier, 				'r', " +
                        "@active_owner,	@ldt_time, " +
                        "@ldt_time,					0,  " +
                        "@vat_percent / 100,	@ldc_total, " +
                        "1, 1,			 " +
                        "@station_number, 'O',  " +
                        "@ll_distributor,				1, " +
                        "0, 0, 0, @ldt_time)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS32(object ll_serial, object il_return_son, object ll_material_number, object adc_qty, object ldc_price, object ll_supplier, object ll_return_serial, object ll_decline_number, object ldc_sell_price, object al_row_no)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ll_serial", ll_serial },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@il_return_son", il_return_son },
                        { "@ll_material_number", ll_material_number },
                        { "@adc_qty", adc_qty },
                        { "@ldc_price", ldc_price },
                        { "@ll_supplier", ll_supplier},
                        { "@ll_return_serial", ll_return_serial},
                        { "@ll_decline_number", ll_decline_number},
                        { "@ldc_sell_price", ldc_sell_price},
                        { "@al_row_no", al_row_no}
                    };

                    string sql = "INSERT INTO invoice_details( " +
                     "serial_number, branch_number, " +
                     "invoice_number, material_number, " +
                     "material_quantity, material_price, " +
                     "details, supplier_number, " +
                     "material_discount_percent, material_price_after_discount, " +
                     "packing_list_number, packing_serial, " +
                     "stock_number, decline_number, " +
                     "sell_price, bonus_quantity, " +
                     "material_bonus_connect, material_discount_amount, " +
                     "color, bonus_discount, " +
                     "indicator, supplier_discount_percent, " +
                     "mini_terminal) " +
         "VALUES(     @ll_serial, @branch_number,    " +
                        "@il_return_son,			@ll_material_number, " +
                        "@adc_qty,				@ldc_price,  " +
                        "'',						@ll_supplier,  " +
                        "0,							@ldc_price,  " +
                        "@ll_return_serial,							0,  " +
                        "1, @ll_decline_number, " +
                        "@ldc_sell_price, 0, 0, 0, 255, 0, @al_row_no, 0, 1); ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS33(object ll_return_serial, object il_return_number, object ldt_time, object ll_supplier
           , object ll_return_son, object is_carton_barcode,
           object ll_serial, object ll_material_number, object adc_qty, object ls_adhoc_key, object il_marlog_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ll_return_serial",ll_return_serial  },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@il_return_number", il_return_number },
                        { "@ldt_time",ldt_time  },
                        { "@ll_supplier", ll_supplier },
                        { "@ll_return_son",  ll_return_son},
                        { "@is_carton_barcode", is_carton_barcode},
                        { "@ll_serial",ll_serial },
                        { "@ll_material_number",ll_material_number },
                        { "@adc_qty", adc_qty},
                        { "@ls_adhoc_key", ls_adhoc_key},
                        {"@il_marlog_branch", il_marlog_branch}
                    };

                    string sql = "INSERT INTO shipment_marlog_return " +
                                "(serial_number, shipment_number, " +
                                "               branch_number, date_move, " +
                                "               state, supplier_number, " +
                                "               invoice_number, carton_barcode, " +
                                "               row_serial_number, material_number, " +
                                "               material_quantity, last_update_datetime, " +
                                "               adhoc_key, marlog_number) " +
                                "VALUES( " +
                                "           @ll_return_serial,		 " +                //serial_number
                                "           @il_return_number, 		 " +    //shipment_number ����� ��
                                "           @branch_number, " +
                                "           @ldt_time, 					 " +    //date_move
                                "           'n',                         " +        //state
                                "           @ll_supplier,				 " +    //supplier_number
                                "           @ll_return_son,				 " +//invoice_number ����� ��
                                "           @is_carton_barcode,			 " +//carton_barcode
                                "           @ll_serial,					 " +    //row_serial_number
                                "           @ll_material_number,		 " +    //material_number
                                "           @adc_qty,					 " +    //material_quantity    
                                "           @ldt_time,					 " +	//last_update_datetime 
                                "           @ls_adhoc_key,				 " +//adhoc key " +
                                "           @il_marlog_branch)";//marlog number " 
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS43(object ldt_time, object ll_material_number, object il_return_number, object is_carton_barcode, object ll_supplier, object ll_return_son, object il_marlog_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ldt_time", ldt_time },
                        { "@ll_material_number", ll_material_number },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@il_return_number", il_return_number },
                        { "@is_carton_barcode", is_carton_barcode },
                        { "@ll_supplier", ll_supplier },
                        { "@ll_return_son", ll_return_son },
                        { "@il_marlog_branch", il_marlog_branch }
                    };

                    string sql = "UPDATE shipment_marlog_return " +
                        "SET state = 'd', last_update_datetime = @ldt_time " +
                        "WHERE material_number = @ll_material_number AND " +
                        "branch_number = @branch_number AND " +
                        "shipment_number = @il_return_number AND " +
                        "carton_barcode = @is_carton_barcode AND " +
                        "supplier_number = @ll_supplier AND " +
                        "invoice_number = @ll_return_son AND " +
                        "marlog_number = @il_marlog_branch ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        public void UpdateDataS42(object ldt_time, object ll_material_number, object ll_return_son, object ll_row_serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> {
                        { "@ldt_time", ldt_time },
                        { "@ll_material_number", ll_material_number },
                        { "@branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number },
                        { "@ll_return_son", ll_return_son },
                        { "@ll_row_serial_number", ll_row_serial_number }
                    };

                    string sql = "UPDATE invoice_details " +
                        "SET state = 'd', last_update_datetime = @ldt_time " +
                        "WHERE material_number = @ll_material_number AND " +
                        "branch_number = @branch_number AND " +
                        "invoice_number = @ll_return_son AND " +
                        "serial_number = @ll_row_serial_number ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public override async Task<int> uf_enter(string as_column, long al_row)
        {

            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
            long ll_doc_no = 0;
            long ll_doc_no_en = 0;
            long ll_row_no = 0;
            long ll_cntr = 0;
            long ll_serial_number = 0;
            long ll_RowCount = 0;
            long ll_row = 0;
            long ll_material_number = 0;
            decimal? ll_material_quantity = 0;
            string ls_barcode = null;
            string ls_b2b_status = null;
            string ls_string = null;
            this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Start. Column: " + as_column), Convert.ToString("Row:      " + al_row.ToString()), "");
            ModelAction ldis_status = default(ModelAction);


            ldis_status = dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
            f_dw_enter_key_moveClass.f_dw_enter_key_move(dw_inv_pack_details);

            if (dw_inv_pack_details.RowCount() < 1)
            {
                return 0;
            }
            switch (as_column)
            {
                case "barcode":

                    ls_barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Barcode:  " + ls_barcode), "");
                    if (ls_barcode == null || string.IsNullOrEmpty(ls_barcode))
                    {
                        await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "barcode");
                    }
                    else
                    {
                        await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "material_quantity");
                    }
                    break;
                case "material_quantity":

                    ll_row_no = dw_inv_pack_details.GetItemValue<long>(al_row, "row_no");
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("Row no:   " + ll_row_no.ToString()), "");
                    // SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - Check if the quantity is null

                    ll_material_quantity = dw_inv_pack_details.GetItemValue<decimal?>(al_row, "material_quantity");
                    if (ll_material_quantity == 0 || ll_material_quantity == 0)
                    {
                        //.. ���� ����� �� ����� ����� ����
                        await this.uf_show_message("", "", "OK", "#20011");
                        await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "material_quantity");
                        return 0;
                    }
                    // End
                    // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - If at least 1 doc is not b2b, set focus to the doc_no field

                    ll_material_number = dw_inv_pack_details.GetItemValue<long>(al_row, "material_number");

                    ll_RowCount = dw_inv_pack_details.RowCount();
                    await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "declines");
                    return 1;
                //	ELSE
                //			ll_doc_no = dw_inv_pack_details.GetItem<int>(al_row, "doc_no")
                //			
                //			uf_prepare_log(This.classname(), "uf_enter", "Column:   " + as_column, "Doc no:   " + String(ll_doc_no), "")
                //
                //			IF (ll_b2b_status = 1) OR (NOT(IsNull(ll_doc_no)) OR ll_doc_no > 0) THEN
                //	
                //				ll_serial_number = THIS.dw_inv_pack_details.Object.serial_number[al_row]
                //				IF NOT(ll_serial_number > 0 ) OR IsNull(ll_serial_number) THEN
                //					PARENT.DYNAMIC wf_set_items('doc_no', String(ll_doc_no), al_row)
                //				END IF
                //
                //				// SharonS - 1.2.37.10 - 2008-12-10 - Task#10012 - SPUCM00000094
                //				IF ll_doc_no_en > 1 THEN
                //					POST uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "doc_no")
                //				ELSE
                //				// End	
                //					IF NOT THIS.uf_is_new_row_exist() THEN
                //						dw_inv_pack_details.TRIGGER EVENT ue_new_row()
                //					ELSE
                //						al_row = dw_inv_pack_details.RowCount()
                //						POST uf_set_focus("uo_mini_terminal_body", "dw_inv_pack_details", al_row, "barcode")
                //					END IF
                //				// SharonS - 1.2.37.10 - 2008-12-10 - Task#10012 - SPUCM00000094	
                //				END IF
                //				// End	
                //			END IF
                //         END IF
                case "declines":
                    this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), "", "");
                    //Check if decline is null - the user did not choose a decline reason.

                    ll_doc_no = dw_inv_pack_details.GetItemValue<long>(al_row, "declines");
                    if (ll_doc_no == 0 || ll_doc_no == 0)
                    {
                        await this.uf_show_message("��� ��", "", "OK", "�� ����� ���� ������");
                        await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", al_row, "declines");
                        return 1;
                    }
                    // End
                    if (!await this.uf_is_new_row_exist(dw_inv_pack_details))
                    {
                        // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call uf_new_row
                        await this.uf_new_row(dw_inv_pack_details);
                        //dw_inv_pack_details.TRIGGER EVENT ue_new_row()	
                    }
                    else
                    {

                        ll_RowCount = dw_inv_pack_details.RowCount();
                        this.uf_prepare_log(Convert.ToString(this.GetType().Name), "uf_enter", Convert.ToString("Column:   " + as_column), Convert.ToString("RowCount: " + ll_RowCount.ToString()), "");
                        for (ll_cntr = 0; ll_cntr < ll_RowCount; ll_cntr++)
                        {
                            ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_cntr, "barcode");
                            if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
                            {
                                ll_row = ll_cntr;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        await this.uf_set_focus("uo_mini_terminal_returns_details", "dw_inv_pack_details", ll_row, "barcode");
                    }
                    break;
                default:
                    break;
            }
            return 1;
        }
    }
}
