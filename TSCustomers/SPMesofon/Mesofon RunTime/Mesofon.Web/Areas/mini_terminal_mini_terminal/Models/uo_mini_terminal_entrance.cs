using System;
using System.Drawing;
using System.Globalization;
using System.Web.VisualTree.Elements;
using global;
using Mesofon.Common;
using System.Threading.Tasks;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:12 (Utc)
	// Comments:        
	// 
	public class uo_mini_terminal_entrance : CompositeElement
	{
		public uo_mini_terminal_entrance()
		{

		}
        public async Task constructor()
        {
            LabelElement st_returns = this.GetVisualElementById<LabelElement>("st_returns");
            ButtonElement cb_corssdoc_return = this.GetVisualElementById<ButtonElement>("cb_corssdoc_return");
            ButtonElement cb_return_supplier = this.GetVisualElementById<ButtonElement>("cb_return_supplier");
            ButtonElement cb_return_marlog = this.GetVisualElementById<ButtonElement>("cb_return_marlog");
            ButtonElement cb_sahar = this.GetVisualElementById<ButtonElement>("cb_sahar");
            bool lb_return_marlog = true;
            bool lb_return_crossdoc = true;
            // AlexKh
            //uf_resize();
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("web_trade_on", "boolean", "false", "Turn on handling trade site orders.", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                cb_sahar.Visible = false;
            }
            // AlexKh - 1.1.1.11 - 08/09/2013 - SPUCM00004408 - parameter for marlog and supplier returns
            temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_return", "boolean", "false", "Turn on handling marlog return.", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                cb_return_marlog.Visible = false;
                cb_return_supplier.Visible = false;
                //st_returns.visible = FALSE
                lb_return_marlog = false;
            }
            //Ron@08/2015
            temp = await f_get_parameter_valueClass.f_get_parameter_value("crossdoc_return", "boolean", "false", "Turn on handling crossdoc return.", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                cb_corssdoc_return.Visible = false;
                lb_return_crossdoc = false;
                //st_returns.visible = FALSE
            }
            if (lb_return_marlog == false && lb_return_crossdoc == false)
            {
                st_returns.Visible = false;
            }
        }

    

    }
}
