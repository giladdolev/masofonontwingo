using System.Web.VisualTree.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using global;
using Mesofon.Common.Global;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;

namespace mini_terminal
{
    // Creation Time:   08/11/2015 10:21:20 (Utc)
    // Comments:        
    // 
    public class w_mini_terminal_input : WindowElement
    {
        public u_material_search iuo_material_search;
        public u_material_update iuo_material_update; //.. search at supplier_order_move/details
        public u_order_search iuo_order_search;
        public bool ib_message_on;
        //.. if no update for invoice_move/packing_list_move made, check if invoice/pack exists
        public bool ib_new_values;
        public ControlElement igo_Focus_Next;
        public string is_dw_column_for_focus;
        public long il_dw_row_for_focus;
        //.. if true then at same time user enter expected qty value , its value will copy to 
        //.. current material quantity		
        public bool ib_auto_current_qty;
        //.. if invoice/packing is new (not retrieved from db) then TRUE
        public bool ib_new_inv_pack;
        //.. current employee number
        public long il_employee_number;
        public bool ib_get_material_price_from_order_if_it_exists;
        // SharonS - 1.2.30.19 - 2008-07-03- CR#1096 - Log on/off
        public bool ib_log_mode;
        public delegate bool ue_save_data_EventHandler(bool ab_quiet_save);
     //   public event mini_terminalAlias.w_mini_terminal_input.ue_save_data_EventHandler ue_save_dataEvent;
        public delegate void ue_key_EventHandler();
       // public event mini_terminalAlias.w_mini_terminal_input.ue_key_EventHandler ue_keyEvent;
        public delegate void ue_new_doc_EventHandler(bool ab_packing_flag);
   //     public event mini_terminalAlias.w_mini_terminal_input.ue_new_doc_EventHandler ue_new_docEvent;
        public delegate void ue_show_difference_report_EventHandler();
     //   public event ue_show_difference_report_EventHandler ue_show_difference_reportEvent;
        public w_mini_terminal_input()
        {
        }

        public u_material_search iuo_material_searchProperty
        {
            get { return this.iuo_material_search; }
            set { this.iuo_material_search = value; }
        }
        public u_material_update iuo_material_updateProperty
        {
            get { return this.iuo_material_update; }
            set { this.iuo_material_update = value; }
        }
        public u_order_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public bool ib_new_valuesProperty
        {
            get { return this.ib_new_values; }
            set { this.ib_new_values = value; }
        }
        public ControlElement igo_Focus_NextProperty
        {
            get { return this.igo_Focus_Next; }
            set { this.igo_Focus_Next = value; }
        }
        public string is_dw_column_for_focusProperty
        {
            get { return this.is_dw_column_for_focus; }
            set { this.is_dw_column_for_focus = value; }
        }
        public bool ib_auto_current_qtyProperty
        {
            get { return this.ib_auto_current_qty; }
            set { this.ib_auto_current_qty = value; }
        }
        public bool ib_new_inv_packProperty
        {
            get { return this.ib_new_inv_pack; }
            set { this.ib_new_inv_pack = value; }
        }
        public long il_employee_numberProperty
        {
            get { return this.il_employee_number; }
            set { this.il_employee_number = value; }
        }
        public bool ib_get_material_price_from_order_if_it_existsProperty
        {
            get { return this.ib_get_material_price_from_order_if_it_exists; }
            set { this.ib_get_material_price_from_order_if_it_exists = value; }
        }
        public bool ib_log_modeProperty
        {
            get { return this.ib_log_mode; }
            set { this.ib_log_mode = value; }
        }
        public void LoadData(ref string ls_employee_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_employee_numberProperty } };
                    string sql = "SELECT IsNull(name, '') FROM employees WHERE branch_number = @0 AND number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_employee_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_order_number, long ll_material_number, long ll_supplier_number, ref decimal ldec_qty)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_order_number }, { "@2", ll_material_number }, { "@3", ll_supplier_number } };
                    string sql = "SELECT IsNull(material_quantity, 0) + IsNull(bonus_quantity, 0) FROM supplier_order_details WHERE branch_number = @0 AND order_number = @1 AND material_number = @2 AND supplier_number = @3";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_qty = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(long ll_serial_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM packing_list_move WHERE pack_list_number = @0 AND branch_number = @1 AND supplier_number = @2 AND packing_type = 'p'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(long ll_serial_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM invoice_move WHERE invoice_number = @0 AND branch_number = @1 AND supplier_number = @2 AND invoice_type = 'p'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(long ll_serial_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM invoice_move WHERE invoice_number = @0 AND branch_number = @1 AND supplier_number = @2 AND invoice_type = 'r'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(Int64 al_supplier_number, ref long ll_temp, ref string al_supplier_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_supplier_number } };
                    string sql = "SELECT number,  name FROM suppliers WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        al_supplier_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(long ll_log_book, long ll_supplier_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_log_book }, { "@2", ll_supplier_number } };
                    string sql = "SELECT COUNT(invoice_move.log_book) FROM invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.log_book = @1 AND invoice_move.supplier_number = @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(long ll_log_book, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_log_book } };
                    string sql = "SELECT COUNT(invoice_move.log_book) FROM invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.log_book = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(long ll_log_book, long ll_supplier_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_log_book }, { "@2", ll_supplier_number } };
                    string sql = "SELECT COUNT(packing_list_move.log_book) FROM packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.log_book = @1 AND packing_list_move.supplier_number = @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(long ll_log_book, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_log_book } };
                    string sql = "SELECT COUNT(packing_list_move.log_book) FROM packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.log_book = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
