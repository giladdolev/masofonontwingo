using System.Web.VisualTree.Elements;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:09 (Utc)
	// Comments:        
	// 
	public class rw_validate_carton : WindowElement
	{
		public string is_cartone_barcode_received;
		public string is_carton_barcode_scanned;
		public delegate void ue_close_EventHandler();
		public event ue_close_EventHandler ue_closeEvent;
        public rw_validate_carton(string barcode)
        {
            Barcode = barcode;
        }

	    public string Barcode { get; set; }

		public string is_cartone_barcode_receivedProperty
		{
			get { return this.is_cartone_barcode_received; }
			set { this.is_cartone_barcode_received = value; }
		}
		public string is_carton_barcode_scannedProperty
		{
			get { return this.is_carton_barcode_scanned; }
			set { this.is_carton_barcode_scanned = value; }
		}

         
    }
}
