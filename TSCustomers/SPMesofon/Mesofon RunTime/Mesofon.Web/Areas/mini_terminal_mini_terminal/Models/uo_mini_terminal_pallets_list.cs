using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Drawing;
using System.Web.VisualTree.Elements;
using global;
using Mesofon.Common;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Threading.Tasks;

namespace mini_terminal
{
    // Creation Time:   07/07/2016 13:46:15 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_pallets_list : CompositeElement
    {
        public const int NEW_NOT_APPROVED = 0;
        public const int ACTIVE_AUTOMATIC_NOT_SCANNED = 1;
        public const int ACTIVE_MANUAL_NOT_SCANNED = 2;
        public const int ACTIVE_AUTOMATIC_SCANNED = 3;
        public const int INVOICE_TYPE_CROSSDOC = 1;
        public u_order_search iuo_order_search;
        public u_material_update iuo_material_update;
        public nvuo_b2b_pallets invuo_b2b_pallets;
        public long il_employee_number;
        // AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_marlog_branch;
        public long il_marlog_distributor;
        public uo_mini_terminal_pallets_list()
        {

        }

        public void constructor() // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138  - constructor
        {
            GridElement dw_pallets_list = this.GetVisualElementById<GridElement>("dw_pallets_list");
            long ll_ret = 0;
            //GridElement	ldwc
            //dw_shipments_list.GetChild("shipment_number" ,ldwc)
            //ldwc.SetTransObject(SQLCA)
            // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
            //ll_ret = ldwc.retrieve(gs_vars.branch_number )
            //ll_ret = ldwc.retrieve(gs_vars.branch_number,  il_marlog_branch)
            // AlexKh - 1.2.48.0.13 - 2011-11-06 - Fix for pallets  - sort shipment numbers
            //ldwc.setsort("shipment_number D")
            //ldwc.Sort()
            //dw_shipments_list.Insert(0)
            //dw_shipments_list.setfocus()
            this.iuo_order_searchProperty = new u_order_search();
            this.iuo_order_searchProperty.uf_set_mt_message(true);
            this.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.iuo_material_updateProperty = new u_material_update();
            this.iuo_material_updateProperty.uf_set_transaction(masofonAlias.masofon.Instance.sqlca);
            this.iuo_material_updateProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            this.invuo_b2b_palletsProperty = new nvuo_b2b_pallets(); //Rony@12/01/2015>>SPUCM00005157
            //uf_resize();
        }

     

        public u_order_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public u_material_update iuo_material_updateProperty
        {
            get { return this.iuo_material_update; }
            set { this.iuo_material_update = value; }
        }
        public nvuo_b2b_pallets invuo_b2b_palletsProperty
        {
            get { return this.invuo_b2b_pallets; }
            set { this.invuo_b2b_pallets = value; }
        }
        public long il_employee_numberProperty
        {
            get { return this.il_employee_number; }
            set { this.il_employee_number = value; }
        }
        public long il_marlog_branchProperty
        {
            get { return this.il_marlog_branch; }
            set { this.il_marlog_branch = value; }
        }
        public void LoadData(string as_pallet_number, ref long ll_counter)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", as_pallet_number } };
                    string sql = "SELECT count(*) FROM b2b_invoice_details WHERE branch_number = @0 AND pallet_number = @1 AND IsNull(scanned_quantity, 0) <> 0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_counter = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
