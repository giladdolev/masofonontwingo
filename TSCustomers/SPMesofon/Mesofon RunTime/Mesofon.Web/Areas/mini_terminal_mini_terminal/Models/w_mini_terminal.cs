using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using global;
using Mesofon.Common.Global;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;
using System.Data;
using System.Web.VisualTree.Extensions;
using Mesofon.Repository;
using Mesofon.Common;
using System.Drawing;
using System.Web.VisualTree.MVC;
using Mesofon.Data;
using Mesofon.Models;
using System.Threading.Tasks;
using MvcSite.Common;
using System.Diagnostics;
using MvcSite.Common.Global;


namespace mini_terminal
{
    // Creation Time:   07/06/2016 11:46:20 (Utc)
    // Comments:        
    // 
    public class w_mini_terminal : WindowElement
    {
        public s_reports_db istct_reports_db;
        public long il_employee_number;
        private nvuo_doc_item_search invuo_doc_item_search;
        //.. current employee number
        public u_material_update iuo_material_update;
        // SharonS - 1.2.30.19 - 2008-07-03- CR#1096 - Log on/off
        public bool ib_log_mode;
        // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Delete items
        public IRepository ids_invpack_deleted_items;
        public IRepository ids_material_batchs;
        //
        // AlexKh - 1.2.46.0 - 2010-06-17 - CR#1138 - assign variables
        public string is_pallet_number;
        public long il_shipment_number;
        // AlexKh - 1.1.1.10 - 2013-06-26 - CR#1160 - open header uo
        public bool ib_return_marlog;
        public bool ib_return_supplier;
        public w_mini_terminal(decimal qty)
        {
            Qty = qty;
            _w_mini_terminal = this;
            //Set employee number
            il_employee_numberProperty = Convert.ToInt64(Globals.EmployeeNumber);
            masofonAlias.masofon.Instance.gs_vars.active_owner = il_employee_numberProperty;
        }



        public w_mini_terminal()
            : this(0)
        {

        }

        public decimal Qty { get; set; }

        public long il_employee_numberProperty
        {
            get { return this.il_employee_number; }
            set { this.il_employee_number = value; }
        }
        public nvuo_doc_item_search invuo_doc_item_searchProperty
        {
            get { return this.invuo_doc_item_search; }
            set { this.invuo_doc_item_search = value; }
        }
        public u_material_update iuo_material_updateProperty
        {
            get { return this.iuo_material_update; }
            set { this.iuo_material_update = value; }
        }
        public bool ib_log_modeProperty
        {
            get { return this.ib_log_mode; }
            set { this.ib_log_mode = value; }
        }




        public IRepository ids_invpack_deleted_itemsProperty
        {
            get { return this.ids_invpack_deleted_items; }
            set { this.ids_invpack_deleted_items = value; }
        }
        public IRepository ids_material_batchsProperty
        {
            get { return this.ids_material_batchs; }
            set { this.ids_material_batchs = value; }
        }
        public string is_pallet_numberProperty
        {
            get { return this.is_pallet_number; }
            set { this.is_pallet_number = value; }
        }
        public long il_shipment_numberProperty
        {
            get { return this.il_shipment_number; }
            set { this.il_shipment_number = value; }
        }
        public bool ib_return_marlogProperty
        {
            get { return this.ib_return_marlog; }
            set { this.ib_return_marlog = value; }
        }
        public bool ib_return_supplierProperty
        {
            get { return this.ib_return_supplier; }
            set { this.ib_return_supplier = value; }
        }
        public void UpdateData(long ll_serial_number, long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_material_number } };
                    string sql = " DELETE FROM material_batch WHERE ((material_batch.packing_detailes_serial_number = @0) OR (material_batch.invoice_detailes_serial_number = @0)) AND (material_batch.branch_number = @1) AND (material_batch.material_number = @2)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData(long ll_serial_number, long ll_material_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    bool noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_material_number } };
                    string sql = "SELECT count(material_batch.batch_number) FROM material_batch WHERE ((material_batch.packing_detailes_serial_number = @0) OR (material_batch.invoice_detailes_serial_number = @0)) AND (material_batch.branch_number = @1) AND (material_batch.material_number = @2)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_supplier_number, Int64 al_invoice_number, ref string ls_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_supplier_number }, { "@2", al_invoice_number } };
                    string sql = "SELECT state FROM invoice_move WHERE invoice_move.branch_number = @0 AND (invoice_move.supplier_number = @1) AND (invoice_move.invoice_number = @2)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_state = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public async System.Threading.Tasks.Task<bool> wf_update_item(long al_row_number) //**********************************************************************************************
        {
            string ls_barcode_main = null;
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_update_item
            //*Purpose:							
            //*
            //*Arguments:						
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										value 		long						al_row_number
            //*		
            //*Return:								BOOLEAN   - TRUE  - Success
            //*													   FALSE - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-10-2007    SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

          //  bool lb_concatenate = false;
            char lc_doc_type = '\0';
            char? lc_old_doc_type = null;
            string ls_barcode = null;
            string ls_material_name = null;
            string ls_find = null;
            string ls_find_dw = null;
            string ls_old_barcode = null;
            string ls_error_text = null;
            string ls_last_sql_err_text = null;
            string ls_message = null;
            string ls_state = null;
            string ls_details = null;
            decimal? ld_expected_material_quantity = null;
            decimal ld_expected_material_quantity_temp = default(decimal);
            decimal? ld_order_quantity = null;
            decimal? ld_invoice_pack_quantity = null;
            decimal ld_total = default(decimal);
            decimal ld_material_price = default(decimal);
            decimal ld_material_price_after_discount = default(decimal);
            decimal ld_material_discount_percent = default(decimal);
            decimal ld_sell_price = default(decimal);
            decimal ld_bonus_quantity = default(decimal);
            decimal ld_inv_pack_quantity = default(decimal);
            decimal ld_material_quantity = default(decimal);
            long? ll_doc_no = 0;
            long ll_rows_count = 0;
            long? ll_found = 0;
            long? ll_found_dw = 0;
            long ll_rtn_code = 0;
            long ll_logbook = 0;
            long ll_supplier_number = 0;
            long ll_order_num = 0;
            long ll_material_number = 0;
            long ll_result = 0;
            long ll_error_number = 0;
            long ll_update_status = 0;
            long ll_last_sql_db_code = 0;
            long ll_row_no = 0;
            long ll_cntr = 0;
            long ll_rowCount = 0;
            long ll_rows_retrieved = 0;
            long ll_min_valid_month = 0;
            long ll_order_material_color = 0;
            long ll_distributor_number = 0;
            DateTime? ldt_supply_date = default(DateTime);
            DateTime? ldt_curr_expiration_date = default(DateTime);
            DateTime? ldt_last_update_datetime = default(DateTime);
            long li_b2b_status = 0;
            int li_rtn_code = 0;
            ModelAction ldwis_doc;
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_update_item Function, Start. Row:         " + al_row_number.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End

            ll_row_no = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row_number, "row_no");

            ls_barcode = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "barcode");

            ls_material_name = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "material_name");

            ld_expected_material_quantity = uo_body.dw_inv_pack_details.GetItemValue<decimal?>(al_row_number, "expected_material_quantity");

            ld_order_quantity = uo_body.dw_inv_pack_details.GetItemValue<decimal?>(al_row_number, "order_quantity");

            ld_invoice_pack_quantity = uo_body.dw_inv_pack_details.GetItemValue<decimal?>(al_row_number, "invoice_pack_quantity");

            ll_doc_no = uo_body.dw_inv_pack_details.GetItemValue<long?>(al_row_number, "doc_no");

            li_b2b_status = Convert.ToInt32(uo_body.dw_inv_pack_details.GetItemValue<long>(al_row_number, "b2b_status"));

            ll_material_number = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row_number, "material_number");

            ld_material_price = uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row_number, "material_price");
            // SharonS - 1.2.36.4 - 2008-10-22 - Task#10012 - Set material details

            ld_material_price_after_discount = uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row_number, "material_price_after_discount");

            ld_material_discount_percent = uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row_number, "material_discount_percent");

            ll_min_valid_month = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row_number, "min_valid_months");

            ldt_curr_expiration_date = uo_body.dw_inv_pack_details.GetItemValue<DateTime?>(al_row_number, "expiration_date");

            ld_sell_price = uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row_number, "sell_price");

            ll_order_material_color = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row_number, "color");

            ls_state = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "state");

            ldt_last_update_datetime = uo_body.dw_inv_pack_details.GetItemValue<DateTime?>(al_row_number, "last_update_datetime");
            // End
            // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details 

            ls_details = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "details");
            // Set supplier number and order number in u_material_update non-visual onject
            (await this.wf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number); ;
            iuo_material_updateProperty.uf_set_supplier_number(ll_supplier_number);
            (await this.wf_get_selected_order_no(ll_order_num)).Retrieve(out ll_order_num);
            if (ll_order_num == 0)
            {
                ll_order_num = 0;
            }
            iuo_material_updateProperty.uf_set_order_number(ll_order_num);
            // AlexKh - 1.1.40.0 - 2015-12-09 - check if invoice is not closed
            if (ll_doc_no.Equals(null))
            {
                ll_doc_no = 0;
            }
            if (!await wf_check_invoice_state(ll_supplier_number, (long)ll_doc_no))
            {
                return false;
            }
            // Load material_suppliers_connect data for the material
            await iuo_material_updateProperty.uf_load_material_suppliers_connect_new(ll_material_number);
            // Load material_suppliers data for the material
            await iuo_material_updateProperty.uf_load_material_suppliers_new(ll_material_number, ld_material_price);
            // Update material_suppliers_connect & material_suppliers
            iuo_material_updateProperty.uf_update_new(ref ll_error_number, ref ls_error_text);
            // Update order state in supplier_order_move
            uo_body.iuo_order_searchProperty.uf_set_order_state("P");
            // Update quantity_within_invoice in supplier_order_details
            if (ld_expected_material_quantity.Equals(null))
            {
                ld_expected_material_quantity = default(decimal);
            }
            uo_body.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ld_expected_material_quantity.Value);
            lc_doc_type = await this.wf_get_doc_type(ll_doc_no.Value);
            if (uo_body.il_old_doc_no != 0)
            {
                lc_old_doc_type = await this.wf_get_doc_type(uo_body.il_old_doc_no);
            }
            // SharonS - 1.2.36.0 - 2008-07-27 - Task#10012 - SPUCM00000094 - If doc type is 'E' - doc number is not valid
            if (lc_doc_type.ToString() == "E")
            {
                return false;
            }
            // End
            // Debug
            List<ModelBase> lany_inv = null;
            List<ModelBase> lany_pack = null;
            List<ModelBase> lany_dis = null;
            // Debug
            // AlexKh - 1.2.45.19 - 2010-02-28 - SPUCM00001981 - retrieve distributor number
            (await this.wf_get_distributor_number(ll_distributor_number)).Retrieve(out ll_distributor_number);
            if (lc_doc_type.ToString() == "I")
            {
                // Invoice
                //In case this is the first item for its doc save the doc
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                // End
                ls_find = "number == " + ll_doc_no.ToString();

                ll_rows_count = uo_header_dw_body.RowCount();
                if (ll_rows_count < 1)
                {
                    await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                    return false;
                }

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_count);
                if (ll_found >= 0)
                {


                    ldwis_doc = uo_header_dw_body.GetItemStatus((int)ll_found, 0, ModelBuffer.Primary);

                    if (ldwis_doc == ModelAction.Insert)
                    {
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update header. found row: " + ll_found.ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End




                        ll_rtn_code = uo_header_dw_body.RowsCopy((int)ll_found, (int)ll_found + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_move.Repository(), invuo_doc_item_searchProperty.ids_inv_move.RowCount() + 1, ModelBuffer.Primary);
                        //invuo_doc_item_search.ids_inv_move.UPDATE()
                        // AlexKh - 1.2.45.19 - 2010-02-28 - SPUCM00001981 - set distributor number
                        // AlexKh - 1.2.45.28 - 2010-04-21 - SPUCM00002075 - wrong row number used for invuo_doc_item_search.ids_inv_move


                        if (ll_distributor_number > 0 && invuo_doc_item_searchProperty.ids_inv_move.GetItemValue<int>(invuo_doc_item_searchProperty.ids_inv_move.RowCount(), "distributor_number") == 0)
                        {


                            invuo_doc_item_searchProperty.ids_inv_move.SetItem(invuo_doc_item_searchProperty.ids_inv_move.RowCount(), "distributor_number", ll_distributor_number.ToString());
                        }
                        // AlexKh - 1.1.29.4 - 2015-05-19 - SPUCM00005045 - in case of null value set 0


                        if (invuo_doc_item_searchProperty.ids_inv_move.GetItemValue<int>(invuo_doc_item_searchProperty.ids_inv_move.RowCount(), "log_book") == 0)
                        {


                            invuo_doc_item_searchProperty.ids_inv_move.SetItem(invuo_doc_item_searchProperty.ids_inv_move.RowCount(), "log_book", "0");
                        }

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_update_status = invuo_doc_item_searchProperty.ids_inv_move.Update(unitOfWork);
                        }


                        if (ll_update_status < 0)
                        {
                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                            return false;
                        }
                        else
                        {



                            uo_header_dw_body.SetItemStatus((int)ll_found, 0, ModelBuffer.Primary, ModelAction.None);
                        }
                    }
                }
                //Details


                ldwis_doc = uo_body.dw_inv_pack_details.GetItemStatus((int)al_row_number, 0, ModelBuffer.Primary);
                //IF ldwis_doc = NewModified! AND li_b2b_status <> 1 AND NOT lb_concatenate THEN

                if (ldwis_doc == ModelAction.Insert)
                {
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, row status = NewModified!. Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    ls_barcode_main = await uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode, false);
                    ls_find = "material_number = " + ll_material_number.ToString() + " and invoice_number = " + ll_doc_no.ToString();
                    // Debug

                    if (uo_body.dw_inv_pack_details.RowCount() > 0)
                    {
                        lany_dis = uo_body.dw_inv_pack_details.DataRows();
                    }

                    if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                    {
                        lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                    }
                    // Debug


                    ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                    if (ll_found < -1)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    else if (ll_found >= 0)
                    {

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());
                        //concatenate is needed

                        ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //			DECIMAL ld_temp
                        //			ld_temp = ld_expected_material_quantity + ld_expected_material_quantity_temp
                        if (ld_expected_material_quantity_temp == 0)
                        {
                            ld_expected_material_quantity_temp = 0;
                        }

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expected_material_quantity", Convert.ToString(ld_expected_material_quantity + ld_expected_material_quantity_temp));

                        li_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row_number, "b2b_status");
                        // SharonS - 2008-04-30 - 1.2.30.4 - Bug in concatinate items - Remove the IF statement
                        //			IF li_b2b_status <> 1 THEN
                        ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());
                        //			END IF

                        ld_expected_material_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //ld_material_quantity = invuo_doc_item_search.ids_inv_details.GetItem<int>(ll_found, "material_quantity")

                        ld_bonus_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                        ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                        if (li_b2b_status == 1)
                        {
                            if (ld_expected_material_quantity == ld_inv_pack_quantity)
                            {

                                invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                            }
                        }
                        //29-JAN-2008-END
                        // SharonS - 1.2.36.4 -> 1.2.37.8 - 2008-10-22 - Task#10012 - SPUCM00000094 - Set material details

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "mini_terminal", "1");

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_price", ld_material_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_price_after_discount", ld_material_price_after_discount.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_discount_percent", ld_material_discount_percent.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "materials_min_valid_months", ll_min_valid_month.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date);

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "current_catalog_sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "color", ll_order_material_color.ToString());
                        // End
                        // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "invoice_details_details", Convert.ToString(ls_details));

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }

                    }
                    else
                    {
                        ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {


                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                        {
                            lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                        }
                        // Debug
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }
                    }
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Check if Update was faild, and update log
                    ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                    ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Update Invoice Details, found row: " + ll_found.ToString() + "\r" + "\n" + "Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    if (ll_rtn_code < 0)
                    {
                        return false;
                    }
                    // End
                }
                else
                {
                    ls_find = "material_number == " + ll_material_number.ToString();
                    ls_find += " and invoice_number == " + ll_doc_no.ToString();


                    ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                    if (ll_found < 0)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    // SharonS - 1.2.37.3 - 2008-11-16 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094 - Find item by item number instead of barcode
                    //		ls_find_dw = "barcode = '" + uo_body.is_new_barcode + "'"
                    ls_find_dw = "material_number == " + ll_material_number.ToString();
                    // End
                    ls_find_dw += " and doc_no == " + ll_doc_no.ToString();


                    ll_found_dw = uo_body.dw_inv_pack_details.Find(ls_find_dw, 0, uo_body.dw_inv_pack_details.RowCount());
                    // End 		
                    if (lc_old_doc_type == lc_doc_type || string.IsNullOrEmpty(lc_old_doc_type.ToString()))
                    {
                        // SharonS - 1.2.37.3 - 2008-11-16 - Task#10012 - SPUCM00000094 - SPUCM00000094
                        if (ll_found != 0)
                        {
                            if (ll_found_dw != al_row_number)
                            {

                                ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                                if (ld_expected_material_quantity_temp == 0)
                                {
                                    ld_expected_material_quantity_temp = 0;
                                }
                                ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;
                            }
                            // End 	

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity.ToString());

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "materials_barcode", Convert.ToString(ls_barcode));

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "order_quantity", ld_order_quantity.ToString());

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());
                            //				invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "material_quantity", ld_invoice_pack_quantity)

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "invoice_number", ll_doc_no.ToString());
                            // AlexKh - 1.2.46.1 - 30-08-2010 - CR#1138 - populate right indicator
                            //invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "indicator", ll_row_no )

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_found_dw.ToString());
                            //29-JAN-2008-Start
                            //			ld_expected_material_quantity = invuo_doc_item_search.ids_inv_details.GetItem<int>(ll_found, "expected_material_quantity")
                            //ld_material_quantity = invuo_doc_item_search.ids_inv_details.GetItem<int>(ll_found, "material_quantity")

                            ld_bonus_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                            ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                            if (li_b2b_status == 1)
                            {
                                if (ld_expected_material_quantity == ld_inv_pack_quantity)
                                {

                                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                                }
                            }
                            //29-JAN-2008-END
                            // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Set item's state and last_update_datetime
                            //				invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "state", ls_state)
                            //				invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "last_update_datetime", ldt_last_update_datetime)
                            // End
                            // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "invoice_details_details", Convert.ToString(ls_details));
                        }
                        else
                        {
                            // Debug

                            if (uo_body.dw_inv_pack_details.RowCount() > 0)
                            {


                                lany_dis = uo_body.dw_inv_pack_details.DataRows();
                            }

                            if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                            {


                                lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                            }
                            // Debug




                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);
                        }

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }


                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, row status <> NewModified!, doc type not changed. Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                    }
                    else
                    {
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList Details, doc type was changed. Return code: " + ll_last_sql_db_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                        // SharonS - 1.2.37.3 - 2008-11-17 - Task#10012 - SPUCM00000094 - SPUCM00000094
                        if (ll_found != 0)
                        {

                            ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                            if (ld_expected_material_quantity_temp == 0)
                            {
                                ld_expected_material_quantity_temp = 0;
                            }
                            ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity.ToString());

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "invoice_number", ll_doc_no.ToString());
                                     invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_found_dw.ToString());
                      
                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "invoice_details_details", Convert.ToString(ls_details));
                        }
                        else
                        {




                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);
                        }
                        // End

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }


                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, doc type was changed. Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                    }
                }
            }
            else if (lc_doc_type.ToString() == "P")
            {
                // Packlist
                //In case this is the first item for its doc save the doc
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                // End
                ls_find = "number == " + ll_doc_no.ToString();

                ll_rows_count = uo_header_dw_body.RowCount();
                if (ll_rows_count < 1)
                {
                    await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                    return false;
                }

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_count);
                if (ll_found >= 0)
                {


                    ldwis_doc = uo_header_dw_body.GetItemStatus((int)ll_found, 0, ModelBuffer.Primary);

                    if (ldwis_doc == ModelAction.Insert)
                    {
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList header. Doc type:  " + "PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End




                        ll_rtn_code = uo_header_dw_body.RowsCopy((int)ll_found, (int)ll_found + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_pack_move.Repository(), invuo_doc_item_searchProperty.ids_pack_move.RowCount() + 1, ModelBuffer.Primary);
                        // AlexKh - 1.2.46.2 - 2010-10-06 - SPUCM00002424 - don't copy distributor to the packing list from order if it's marlog distributor


                        if (ll_distributor_number > 0 && invuo_doc_item_searchProperty.ids_pack_move.GetItemValue<int>(invuo_doc_item_searchProperty.ids_pack_move.RowCount(), "distributor_number") == 0)
                        {
                            if (!await wf_check_marlog_distributor(-2, "MARLOG_YES", ll_distributor_number))
                            {


                                invuo_doc_item_searchProperty.ids_pack_move.SetItem(invuo_doc_item_searchProperty.ids_pack_move.RowCount(), "distributor_number", ll_distributor_number.ToString());
                            }
                        }
                        // AlexKh - 1.1.29.4 - 2015-05-19 - SPUCM00005045 - in case of null value set 0


                        if (invuo_doc_item_searchProperty.ids_pack_move.GetItemValue<int>(invuo_doc_item_searchProperty.ids_pack_move.RowCount(), "log_book") == 0)
                        {


                            invuo_doc_item_searchProperty.ids_pack_move.SetItem(invuo_doc_item_searchProperty.ids_pack_move.RowCount(), "log_book", "0");
                        }

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_update_status = invuo_doc_item_searchProperty.ids_pack_move.Update(unitOfWork);
                        }


                        if (ll_update_status < 0)
                        {
                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                            return false;
                        }
                        else
                        {



                            uo_header_dw_body.SetItemStatus((int)ll_found, 0, ModelBuffer.Primary, ModelAction.None);
                        }
                    }
                }
                //Details


                ldwis_doc = uo_body.dw_inv_pack_details.GetItemStatus((int)al_row_number, 0, ModelBuffer.Primary);
                // Debug

                if (uo_body.dw_inv_pack_details.RowCount() > 0)
                {


                    lany_dis = uo_body.dw_inv_pack_details.DataRows();
                }

                if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                {


                    lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                }
                // Debug

                if (ldwis_doc == ModelAction.Insert)
                {
                    //Check if concatenate is needed
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList Details, row status = NewModified!, Doc type:  " + "PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    // SharonS - 1.2.31.0 - 2008-05-01 - Task#10004 - Get main barcode number
                    ls_barcode_main = await uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode, false);
                    //		ls_find = "materials_barcode = '" + ls_barcode + "'"
                    // SharonS - 1.2.36.0 - 2008-07-27 - Task#10012 - SPUCM00000094 - Check concatenate, by barcode and doc num
                    //		ls_find = "materials_barcode = '" + ls_barcode_main + "'"
                    // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094 - Find item by item number instead of barcode
                    //		ls_find = "materials_barcode = '" + ls_barcode_main + "' and pack_list_number = " + String(ll_doc_no)
                    ls_find = "material_number == " + ll_material_number.ToString() + " and pack_list_number == " + ll_doc_no.ToString();
                    // End
                    // End
                    // End


                    ll_found = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_pack_details.RowCount());
                    if (ll_found < -1)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    else if (ll_found >= 0)
                    {
                        // SharonS - 1.2.31.0 - 2008-05-01 - Task#10004 - Set indicator

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());
                        // End
                        //concatenate is needed

                        ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //			DECIMAL ld_temp
                        //			ld_temp = ld_expected_material_quantity + ld_expected_material_quantity_temp
                        if (ld_expected_material_quantity_temp == 0)
                        {
                            ld_expected_material_quantity_temp = 0;
                        }

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", Convert.ToString(ld_expected_material_quantity + ld_expected_material_quantity_temp));

                        li_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row_number, "b2b_status");
                        // SharonS - 2008-04-30 - 1.2.30.4 - Bug in concatinate items - Remove the IF statement
                        //			IF li_b2b_status <> 1 THEN
                        ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity);
                        //			END IF
                        // End
                        // SharonS - 2008-04-30 - 1.2.30.4 - Bug in concatinate items - Call uf_set_item_quantity_within_invoice hear. 
                        // Update quantity_within_invoice in supplier_order_details
                        uo_body.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ld_expected_material_quantity.Value);
                        // End
                        //PninaSG - 2008-01-29 - Start

                        ld_expected_material_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //ld_material_quantity = invuo_doc_item_search.ids_inv_details.GetItem<int>(ll_found, "material_quantity")

                        ld_bonus_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                        ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                        if (li_b2b_status == 1)
                        {
                            if (ld_expected_material_quantity == ld_inv_pack_quantity)
                            {

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                            }
                        }
                        //PninaSG - 2008-01-29 - End
                        // SharonS - 1.2.36.4 -> 1.2.37.8 - 2008-10-22 - Task#10012 - SPUCM00000094 - Set material details

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "mini_terminal", "1");

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_price", ld_material_price);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_price_after_discount", ld_material_price_after_discount);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_discount_percent", ld_material_discount_percent);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "materials_min_valid_months", ll_min_valid_month);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "sell_price", ld_sell_price);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "current_catalog_sell_price", ld_sell_price);

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "color", ll_order_material_color);
                        // End
                        // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "packing_list_details_details", Convert.ToString(ls_details));

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }


                    }
                    else
                    {




                        ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_pack_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                    }
                }
                else
                {
                    ls_find = "material_number == " + ll_material_number.ToString();
                    ls_find += " and pack_list_number == " + ll_doc_no.ToString();


                    ll_found = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_pack_details.RowCount());
                    if (ll_found < 0)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    // SharonS - 1.2.37.3 - 2008-11-16 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094 - Find item by item number instead of barcode
                    //		ls_find_dw = "barcode = '" + uo_body.is_new_barcode + "'"
                    ls_find_dw = "material_number == " + ll_material_number.ToString();
                    // End
                    ls_find_dw += " and doc_no == " + ll_doc_no.ToString();


                    ll_found_dw = uo_body.dw_inv_pack_details.Find(ls_find_dw, 0, uo_body.dw_inv_pack_details.RowCount());
                    // End
                    if (lc_old_doc_type == lc_doc_type || string.IsNullOrEmpty(lc_old_doc_type.ToString()))
                    {
                        // SharonS - 1.2.37.3 - 2008-11-16 - Task#10012 - SPUCM00000094 - SPUCM00000094
                        if (ll_found != -1)
                        {
                            if (ll_found_dw != al_row_number)
                            {

                                ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                                if (ld_expected_material_quantity_temp == 0)
                                {
                                    ld_expected_material_quantity_temp = 0;
                                }
                                ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;
                            }
                            // End

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity);

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "materials_barcode", Convert.ToString(ls_barcode));

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "order_quantity", ld_order_quantity);
                            //				invuo_doc_item_search.ids_pack_details.SetItem(ll_found, "material_quantity", ld_invoice_pack_quantity)

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity);

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "pack_list_number", ll_doc_no);
                            // AlexKh - 1.2.46.1 - 30-08-2010 - CR#1138 - populate right indicator
                            //invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "indicator", ll_row_no )

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "indicator", ll_found_dw);
                            if (li_b2b_status == 1)
                            {
                                if (ld_expected_material_quantity == ld_inv_pack_quantity)
                                {

                                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity);

                                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                                }
                            }
                            //PninaSG - 2008-01-29 - End
                            // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "packing_list_details_details", Convert.ToString(ls_details));
                        }
                        else
                        {
                            // Debug

                            if (uo_body.dw_inv_pack_details.RowCount() > 0)
                            {


                                lany_dis = uo_body.dw_inv_pack_details.DataRows();
                            }

                            if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                            {


                                lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                            }
                            // Debug
                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_pack_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary);
                        }

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                    }
                    else
                    {

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }

                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, doc type was changed. Return code: " + ll_last_sql_db_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                        // SharonS - 1.2.37.3 - 2008-11-17 - Task#10012 - SPUCM00000094 - SPUCM00000094
                        if (ll_found != 0)
                        {

                            ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                            if (ld_expected_material_quantity_temp == 0)
                            {
                                ld_expected_material_quantity_temp = 0;
                            }
                            ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity);

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity);

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "pack_list_number", ll_doc_no);
                            // AlexKh - 1.2.46.1 - 30-08-2010 - CR#1138 - populate right indicator
                            //invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "indicator", ll_row_no )

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "indicator", ll_found_dw);
                            // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Set item's state and last_update_datetime
                            //				invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "state", ls_state)
                            //				invuo_doc_item_search.ids_inv_details.SetItem(ll_found, "last_update_datetime", ldt_last_update_datetime)
                            // End
                            // AlexKh - 1.2.44.4 - 2009-05-21 - SPUCM00001251 - copy item details
                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "packing_list_details_details", Convert.ToString(ls_details));
                        }
                        else
                        {
                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_pack_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary);
                        }
                        // End
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {
                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                        {
                            lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                        }
                        // Debug

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }


                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList Details, doc type was changed. Return code: " + ll_last_sql_db_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End
                    }
                }
            }

            IRepository lds_mini_terminal_header_inv_pack;
            lds_mini_terminal_header_inv_pack = new d_mini_terminal_header_inv_packRepository();

            uo_header_dw_body.RowsCopy(0, uo_header_dw_body.RowCount(), ModelBuffer.Primary, lds_mini_terminal_header_inv_pack, 0, ModelBuffer.Primary);
            li_rtn_code = await wf_retrieve_data();
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
            //IF li_rtn_code < 1 THEN RETURN FALSE
            if (li_rtn_code < 1)
            {
                if (lds_mini_terminal_header_inv_pack != null)
                {
                }
                return false;
            }
            // End

            ll_rowCount = lds_mini_terminal_header_inv_pack.RowCount();

            ll_rows_retrieved = uo_header_dw_body.RowCount();
            for (ll_cntr = 0; ll_cntr < ll_rowCount; ll_cntr++)
            {
                //
                d_mini_terminal_header_inv_pack d =
                    lds_mini_terminal_header_inv_pack.GetItem<d_mini_terminal_header_inv_pack>((int)ll_cntr);
                ll_doc_no = d.number;
                ls_find = "number = " + ll_doc_no.ToString();

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_retrieved);
                if (ll_found == -1)
                {
                    lds_mini_terminal_header_inv_pack.RowsCopy(Convert.ToInt32(ll_cntr), Convert.ToInt32(ll_cntr) + 1, ModelBuffer.Primary, uo_header_dw_body.GetDataObject(), uo_header_dw_body.RowCount() + 1, ModelBuffer.Primary);
                    //GalilCS - force update layout
                    uo_header_dw_body.SetRepository(uo_header_dw_body.Repository());

                    // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Check if there is a non b2b doc
                    long ll_item = 0;
                    long ll_doc_no_en = 0;

                    for (ll_item = 0; ll_item < uo_body.dw_inv_pack_details.RowCount(); ll_item++)
                    {
                        ll_doc_no_en = uo_body.dw_inv_pack_details.GetItemValue<Int64>(ll_item, "doc_no_en");
                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(ll_item, "doc_no_en", ll_doc_no_en + 1);
                    }

                    //uf_calculate_declines
                    (uo_body.dw_inv_pack_details).ResetUpdate();
                    // Endw_mini
                }
            }
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
            // End
            if (!await uo_body.uf_is_new_row_exist(uo_body.dw_inv_pack_details))
            {
                //uf_calculate_declines
                await uo_body.uf_new_row(uo_body.dw_inv_pack_details);
            }

            // Initiate instances
            uo_body.is_new_barcode = "";
            uo_body.is_old_barcode = "";
            uo_body.il_old_doc_no = 0;
            // Set summery data
            await wf_retrieve_dw_summary();
            return true;
        }


        public async System.Threading.Tasks.Task<bool> ue_id_password_check() //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			ue_id_password_check
            //*Purpose:							Open the id check window
            //*  
            //*Arguments:									
            //*Return:								
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007   SHARP.						0.2.0 		B2B 			Initiation -same as was done in the old window
            //************************************************************************************************

            s_password pass_t = default(s_password);
            pass_t.action_title = 1;
            pass_t.a_mini_terminal_mode = true;

            await WindowHelper.Open<w_mini_terminal_id_check>("mini_terminal_mini_terminal", "password", pass_t);


            il_employee_numberProperty = (long)WindowHelper.GetParam<double>(this); ;
            masofonAlias.masofon.Instance.gs_vars.active_owner = il_employee_numberProperty;
            if (il_employee_numberProperty > 0)
            {
                //.. update action_control
                await f_update_actionClass.f_update_action(781, il_employee_numberProperty);
            }
            return (il_employee_numberProperty > 0);
        }
        public async System.Threading.Tasks.Task<int> wf_close_window() //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_close_window
            //*Purpose:							close the window.
            //*  
            //*Arguments:						None.
            //*Return:								Integer: 1 - The window sties open.
            //*												  -1 - Error
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

          //  bool lb_save_before_close = false;
            string ls_message = null;
            int li_rtn_code = 0;
            //Write into a log file 
            ls_message = "*************************** Click - cb_close*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "cb_close The Data:  " + "Clicked on close" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);

            if (uo_header_dw_body.RowCount() > 0)
            {
                li_rtn_code = (int)await wf_show_message("�����", "", "OKCancle", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("��� �� ������ ��� ����� -�� �����.��� ��� ����?"));
                if (li_rtn_code == 1)
                {
                    // AlexKh - 1.2.46.2 - 2010-10-12 - SPUCM00002425  - back to entrance window
                    string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_shipment", "boolean", "false", "Supply Merchandise from marlog.", "gds_find");
                    if (temp.ToLower() == "true")
                    {
                        await wf_display("ENTRANCE");
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                // AlexKh - 1.2.46.2 - 2010-10-12 - SPUCM00002425  - back to entrance window
                string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_shipment", "boolean", "false", "Supply Merchandise from marlog.", "gds_find");
                if (temp.ToLower() == "true")
                {
                    await wf_display("ENTRANCE");
                }
                else
                {
                    this.Close();
                }
            }
            return 1;
        }

        public async System.Threading.Tasks.Task<long> wf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_show_message
            //*Purpose:							Display message.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 			string 					as_title 			:window title
            //*										Value 			string 					as_error_text	:error text
            //*										Value 			string 					as_buttons		:buttons to be displayed
            //*										Value 			string 					as_message		:message text
            //*
            //*Return:								LONG: 1  - succeeded.
            //*												   -1 - failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007    SHARP.				   0.2.0			B2B 			Initiation
            //************************************************************************************************

            return await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox(as_title, as_error_text, "", as_buttons, 1, as_message);
        }
        public async System.Threading.Tasks.Task<int> wf_display(string as_show_mode) //**********************************************************************************************
        {
            uo_mini_terminal_entrance uo_entrance = this.GetVisualElementById<uo_mini_terminal_entrance>("uo_entrance");

            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_display
            //*Purpose:							This function will display the window according to the mode (the argument:as_show_mode).
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value			String						as_show_mode
            //*		
            //*Return:								INTEGER: 1 - Success.
            //*													  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*11-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_RtnValue = 0;
            double ll_null = 0;
            GridElement ldwc;
            // AlexKh - 1.1.1.10 - 2013-06-26 - CR#1160 - set instance variables
            ib_return_marlogProperty = false;
            ib_return_supplierProperty = false;
            // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138  - add shipments list and entrance user object
            switch (as_show_mode)
            {
                case "HEADER":
                    {
                        uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();// await get_uo_mini_terminal_header();
                        TextBoxElement supplier_number = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");
                        ComboBoxElement supplier_name = uo_header.GetVisualElementById<ComboBoxElement>("supplier_name");
                        ComboGridElement order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");
                        GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");

                        //GalilCS - Fire DataChanged event
                        uo_header_dw_body.PerformdataChanged(EventArgs.Empty);

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_header.Visible = true;

                        supplier_number.Focus();

                        // AlexKh - 1.1.31.0 - 2015-03-03 - SPUCM00005045 - Check if we need the log_book field
                        if (Mesofon.Common.RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Administration", "log_book_in_purchase_header", "yes", "Show the log_book field in the header") != "yes")
                        {

                            uo_header_dw_body.Modify("log_book", "visible", "0");

                            uo_header_dw_body.Modify("log_book_t", "visible", "0");
                        }
                    }
                    break;
                case "DETAILS":
                    {
                        uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
                        GridElement uo_body_dw_inv_pack_details = uo_body.GetVisualElementById<GridElement>("dw_inv_pack_details");
                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_body.PreventLoadEvent = true;
                        uo_body.Visible = true;
                        uo_body.PreventLoadEvent = false;

                    }
                    break;
                case "REPORTS":
                    //
                    break;
                case "ENTRANCE":
                    //GalilCS - Hide all controls
                    HideAllControlInPnlContent();
                    uo_entrance.Visible = true;

                    break;
                case "SHIPMENT":
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                        uo_shipments_list.il_employee_numberProperty = il_employee_numberProperty;
                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_shipments_list.Visible = true;

                    }
                    break;
                case "SHIPMENT_RESET"://gilad this case is the default of last  50 shipments
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                        TextBoxElement sle_scan_line_dw_pallets_list = uo_shipments_list.GetVisualElementById<TextBoxElement>("sle_scan_line");
                        long temp_il_marlog_branchProperty = uo_shipments_list.il_marlog_branchProperty;
                        if (!((await wf_set_marlog_info(uo_shipments_list.il_marlog_branchProperty, uo_shipments_list.il_marlog_distributor, false)).
                            Retrieve(out temp_il_marlog_branchProperty, out uo_shipments_list.il_marlog_distributor)))
                        {
                            return 0;
                        }

                        uo_shipments_list.il_marlog_branchProperty = temp_il_marlog_branchProperty;
                        IRepository shipmentsList = new dddw_mini_terminal_shipments_listRepository();
                        //gilad
                        //string checkStatus =Convert.ToString( Globals.giladtestCheckBox);
                        //if(!string.IsNullOrEmpty(checkStatus) && checkStatus=="Checked")
                        shipmentsList.Retrieve2(masofonAlias.masofon.Instance.gs_vars.branch_number, uo_shipments_list.il_marlog_branchProperty);

                        //gilad add the 51 select all row
                        //if (shipmentsList.GetPrimaryList().Count == 50)
                        //{
                            shipmentsList.AppendPrimaryList(new Mesofon.Models.dddw_mini_terminal_shipments_list { date_move = "000000", shipment_number = "��� ���" });
                        //}

                        uo_shipments_list_dw_shipments_list.DisplayMember = "shipment_number";
                        uo_shipments_list_dw_shipments_list.ValueMember = "shipment_number";
                        uo_shipments_list_dw_shipments_list.SetRepository(shipmentsList);

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_shipments_list.Visible = true;

                        uo_shipments_list_dw_pallets_list.Reset();
                        uo_shipments_list.il_employee_numberProperty = il_employee_numberProperty;
                        ll_null = 0;


                        uo_shipments_list_dw_shipments_listReset(uo_shipments_list_dw_shipments_list);
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_shipments_list_dw_shipments_listReset(uo_shipments_list_dw_shipments_list);
                        }, delay: 1);

                        //Clear sle_scan_line textbox
                        if (sle_scan_line_dw_pallets_list != null)
                            sle_scan_line_dw_pallets_list.Text = "";

                    }
                    break;
                case "SHIPMENT_RESET_ALL"://gilad this case is the default of all shipments
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                        TextBoxElement sle_scan_line_dw_pallets_list = uo_shipments_list.GetVisualElementById<TextBoxElement>("sle_scan_line");
                        long temp_il_marlog_branchProperty = uo_shipments_list.il_marlog_branchProperty;
                        //if (!((await wf_set_marlog_info(uo_shipments_list.il_marlog_branchProperty, uo_shipments_list.il_marlog_distributor, false)).
                        //    Retrieve(out temp_il_marlog_branchProperty, out uo_shipments_list.il_marlog_distributor)))
                        //{
                        //    return 0;
                        //}

                        uo_shipments_list.il_marlog_branchProperty = temp_il_marlog_branchProperty;

                        IRepository shipmentsList = new dddw_mini_terminal_shipments_listRepository();

                        shipmentsList.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, uo_shipments_list.il_marlog_branchProperty);

                        uo_shipments_list_dw_shipments_list.DisplayMember = "shipment_number";
                        uo_shipments_list_dw_shipments_list.ValueMember = "shipment_number";
                        uo_shipments_list_dw_shipments_list.SetRepository(shipmentsList);

                        //GalilCS - Hide all controls
                       // HideAllControlInPnlContent();
                       uo_shipments_list.Visible = true;

                        uo_shipments_list_dw_pallets_list.Reset();
                        uo_shipments_list.il_employee_numberProperty = il_employee_numberProperty;
                        ll_null = 0;

                        //uo_shipments_list_dw_shipments_list.SetItem(0, "shipment_number", ll_null);
                        uo_shipments_list_dw_shipments_listReset(uo_shipments_list_dw_shipments_list);
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_shipments_list_dw_shipments_listReset(uo_shipments_list_dw_shipments_list);
                        }, delay: 1);

                        //Clear sle_scan_line textbox
                        if (sle_scan_line_dw_pallets_list != null)
                            sle_scan_line_dw_pallets_list.Text = "";

                    }
                    break;
                case "PALLET":
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");

                        uo_mini_terminal_pallet_manual uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
                        // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - set marlog_number and distributor
                        uo_pallet_manual.il_marlog_branchProperty = uo_shipments_list.il_marlog_branchProperty;
                        uo_pallet_manual.il_marlog_distributorProperty = uo_shipments_list.il_marlog_distributor;
                        uo_pallet_manual.il_employee_number = il_employee_numberProperty;

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_pallet_manual.Visible = true;

                        await uo_pallet_manual.uf_get_pallet_invoices();

                    }
                    break;
                case "CROSSDOC_RESET":
                    {
                        uo_mini_terminal_crossdoc_pack_list uo_crossdoc_list = await get_uo_mini_terminal_crossdoc_pack_list();
                        GridElement uo_crossdoc_list_dw_pallets_list = uo_crossdoc_list.GetVisualElementById<GridElement>("crossdoc_pack_list_dw_pallets_list");
                        ComboGridElement uo_crossdoc_list_dw_shipments_list = uo_crossdoc_list.GetVisualElementById<ComboGridElement>("crossdoc_pack_list_dw_shipments_list");
                        // AlexKh - 1.1.1.18 - 2014-02-19 - CR#1171(SPUCM00004718) - set marlog branch and distributor
                        long temp_il_marlog_branchProperty_2 = uo_crossdoc_list.il_marlog_branchProperty;
                        if (!((await wf_set_marlog_info(uo_crossdoc_list.il_marlog_branchProperty,
                                uo_crossdoc_list.il_marlog_distributor, true))
                            .Retrieve(out temp_il_marlog_branchProperty_2,
                                out uo_crossdoc_list.il_marlog_distributor)))
                        {
                            return 0;
                        }
                        uo_crossdoc_list.il_marlog_branchProperty = temp_il_marlog_branchProperty_2;
                        //AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number

                        IRepository packList = new dddw_mini_terminal_crossdoc_listRepository();
                        packList.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, uo_crossdoc_list.il_marlog_branchProperty);
                        uo_crossdoc_list_dw_shipments_list.DisplayMember = "shipment_pallet_number";
                        uo_crossdoc_list_dw_shipments_list.ValueMember = "shipment_pallet_number";
                        uo_crossdoc_list_dw_shipments_list.SetRepository(packList);
                        uo_crossdoc_list.il_employee_numberProperty = il_employee_numberProperty;
                        // AlexKh - 1.1.22.0 - 2014-07-14 - SPUCM00004921  - set focus to the begining
                        uo_crossdoc_list_dw_pallets_list.Reset();

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_crossdoc_list.Visible = true;

                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_crossdoc_list_dw_shipments_list.SelectedText = "";
                            uo_crossdoc_list_dw_shipments_list.DroppedDown = true;
                            uo_crossdoc_list_dw_shipments_list.Focus();

                        }, delay: 1);

                    }
                    break;
                case "SAHAR_RESET":
                    {
                        uo_mini_terminal_trade_site uo_trade_site = await get_uo_mini_terminal_trade_site();
                        GridElement uo_trade_site_dw_orders_list = uo_trade_site.GetVisualElementById<GridElement>("dw_orders_list");



                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_trade_site.Visible = true;

                        //uo_trade_site_dw_orders_list.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number);
                        IRepository _order_list = new d_mini_terminal_trade_order_listRepository();
                        _order_list.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, masofonAlias.masofon.Instance.gs_vars.LaboratoryBranchNum);
                        uo_trade_site_dw_orders_list.SetRepository(_order_list);
                        TextBoxElement sle_scan_line = uo_trade_site.GetVisualElementById<TextBoxElement>("sle_scan_line");
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            sle_scan_line.Focus();
                        }, delay: 1);
                        uo_trade_site.il_employee_number = il_employee_numberProperty;

                    }
                    break;
                case "RETURN_MARLOG":
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                        uo_mini_terminal_returns uo_returns = await get_uo_mini_terminal_returns();
                        GridElement uo_returns_dw_1 = uo_returns.GetVisualElementById<GridElement>("dw_1");
                        GridElement uo_returns_dw_summary = uo_returns.GetVisualElementById<GridElement>("dw_summary_Returns");
                        uo_mini_terminal_returns_details uo_returns_details = await get_uo_mini_terminal_returns_details();
                        GridElement uo_returns_details_dw_summary = uo_returns_details.GetVisualElementById<GridElement>("dw_summary_details");
                        // AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog branch and distributor
                        if (uo_returns_details.il_return_number == 0)
                        {
                            long temp_il_marlog_branchProperty_3 = uo_shipments_list.il_marlog_branchProperty;
                            long temp_il_marlog_distributorProperty = uo_returns.il_marlog_distributorProperty;
                            if (!((await wf_set_marlog_info(uo_returns.il_marlog_branchProperty, uo_returns.il_marlog_distributorProperty, false)).
                                Retrieve(out temp_il_marlog_branchProperty_3, out temp_il_marlog_distributorProperty)))
                            {
                                return 0;
                            }
                            uo_returns.il_marlog_branchProperty = temp_il_marlog_branchProperty_3;
                            uo_returns.il_marlog_distributorProperty = temp_il_marlog_distributorProperty;
                        }
                        // AlexKh - 1.1.26.0 - 2015-01-07 - SPUCM00005132 - prevent return based on parameter and marlog

                        ButtonElement cb_finish = uo_returns.GetVisualElementById<ButtonElement>("cb_finish");
                        string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_return_prevent_" + uo_returns.il_marlog_branchProperty.ToString(), "boolean", "false", "Prevent return for specific marlog.", "gds_find");
                        if (temp.ToLower() == "true")
                        {
                            //Error message
                            await wf_show_message("����� - ����� �����", "", "OK", "�� ���� ������ ����� ����� ����  ��, �� ���� �����");
                            cb_finish.Enabled = false;
                        }
                        else
                        {
                            cb_finish.Enabled = true;
                        }
                        ib_return_marlogProperty = true;
                        uo_returns_dw_summary.Visible = true;

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_returns.Visible = true;


                        uo_returns.uf_create(true, uo_returns_details.il_return_number, uo_returns_details.is_carton_barcode);

                    }
                    break;
                case "RETURN_MARLOG_DETAILS":
                    {
                        uo_mini_terminal_returns uo_returns = await get_uo_mini_terminal_returns();
                        GridElement uo_returns_dw_1 = uo_returns.GetVisualElementById<GridElement>("dw_1");
                        GridElement uo_returns_dw_summary = uo_returns.GetVisualElementById<GridElement>("dw_summary_Returns");
                        uo_mini_terminal_returns_details uo_returns_details = await get_uo_mini_terminal_returns_details();
                        GridElement uo_returns_details_dw_summary = uo_returns_details.GetVisualElementById<GridElement>("dw_summary_details");
                        ib_return_marlogProperty = true;


                        // AlexKh - 1.1.1.17 - 2014-02-06 - SPUCM00004702 - reset dw_summary

                        uo_returns_details_dw_summary.Reset();

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_returns_details.Visible = true;

                        if ((uo_returns_dw_summary != null) && uo_returns_dw_summary.RowCount() > 0)
                        {
                            uo_returns_details_dw_summary.SetRepository(uo_returns_dw_summary.Repository());
                        }

                        await uo_returns_details.uf_create(true, uo_returns.is_carton_barcode, uo_returns.il_return_number, Convert.ToInt64(((ComboGridElement)uo_returns_dw_1).SelectedValue));

                    }
                    break;
                case "RETURN_SUPPLIER":
                    {
                        uo_mini_terminal_returns uo_returns = await get_uo_mini_terminal_returns();
                        GridElement uo_returns_dw_1 = uo_returns.GetVisualElementById<GridElement>("dw_1");
                        GridElement uo_returns_dw_summary = uo_returns.GetVisualElementById<GridElement>("dw_summary_Returns");
                        uo_mini_terminal_returns_details uo_returns_details = await get_uo_mini_terminal_returns_details();
                        GridElement uo_returns_details_dw_summary = uo_returns_details.GetVisualElementById<GridElement>("dw_summary_details");
                        ib_return_supplierProperty = true;
                        uo_returns.uf_create(false, uo_returns_details.il_return_number, uo_returns_details.is_carton_barcode);

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_returns.Visible = true;

                        //uo_crossdoc_list.Visible = false;
                        //uo_shipments_list.Visible = false;
                        //uo_entrance.Visible = false;
                        //uo_header.Visible = false;
                        //uo_body.Visible = false;
                        //uo_pallet_manual.Visible = false;
                        //uo_returns_dw_summary.Visible = false;
                        //uo_trade_site.Visible = false;
                        //uo_returns_details.Visible = false;
                        //uo_crossdoc_return.Visible = false; //RonY@15/01/2015 - SPUCM00005162
                        //uo_crossdoc_returns_details.Visible = false; //RonY@15/01/2015 - SPUCM00005162
                        //uo_trans_to_branch.Visible = false; //RonY@02/02/2015 - SPUCM00005163
                        //uo_trans_from_branch.Visible = false; //RonY@02/02/2015 - SPUCM0000516
                    }
                    break;
                case "CROSSDOC_RETURN":
                    {
                        uo_mini_terminal_pallets_list uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                        ComboGridElement uo_shipments_list_dw_shipments_list = uo_shipments_list.GetVisualElementById<ComboGridElement>("dw_shipments_list");
                        GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                        uo_mini_terminal_crossdoc_returns uo_crossdoc_return = await get_uo_mini_terminal_crossdoc_returns();
                        GridElement uo_crossdoc_return_dw_1 = uo_crossdoc_return.GetVisualElementById<GridElement>("dw_1");
                        GridElement uo_crossdoc_return_dw_summary = uo_crossdoc_return.GetVisualElementById<GridElement>("dw_summary");

                        uo_mini_terminal_crossdoc_returns_details uo_crossdoc_returns_details = await get_uo_mini_terminal_crossdoc_returns_details();
                        GridElement uo_crossdoc_returns_details_dw_summary = uo_crossdoc_returns_details.GetVisualElementById<GridElement>("dw_summary");
                        //RonY@15/01/2015 - SPUCM00005162
                        if (uo_crossdoc_returns_details.il_return_number == 0)
                        {
                            long temp_il_marlog_branchProperty_4 = uo_shipments_list.il_marlog_branchProperty;
                            long temo_il_marlog_distributorProperty = uo_crossdoc_return.il_marlog_distributorProperty;
                            if (!((await wf_set_marlog_info(uo_shipments_list.il_marlog_branchProperty, uo_crossdoc_return.il_marlog_distributorProperty, 42)).
                                Retrieve(out temp_il_marlog_branchProperty_4, out temo_il_marlog_distributorProperty)))
                            {
                                return 0;
                            }
                            uo_shipments_list.il_marlog_branchProperty = temp_il_marlog_branchProperty_4;
                            uo_crossdoc_return.il_marlog_distributorProperty = temo_il_marlog_distributorProperty;

                            long temp_il_marlog_branchProperty_3 = uo_crossdoc_return.il_marlog_branchProperty;
                            long temp_il_marlog_distributorProperty = uo_crossdoc_return.il_marlog_distributorProperty;
                            if (!((await wf_set_marlog_info(uo_crossdoc_return.il_marlog_branchProperty, uo_crossdoc_return.il_marlog_distributorProperty, false)).
                                Retrieve(out temp_il_marlog_branchProperty_3, out temp_il_marlog_distributorProperty)))
                            {
                                return 0;
                            }
                            uo_crossdoc_return.il_marlog_branchProperty = temp_il_marlog_branchProperty_3;
                            uo_crossdoc_return.il_marlog_distributorProperty = temp_il_marlog_distributorProperty;
                        }
                        uo_crossdoc_return.uf_create(false, uo_crossdoc_returns_details.il_return_number, uo_crossdoc_returns_details.is_carton_barcode, uo_crossdoc_returns_details.il_return_son_for_displayProperty);

                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_crossdoc_return.Visible = true;

                    }
                    break;
                case "CROSSDOC_RETURN_DETAILS":
                    {
                        uo_mini_terminal_returns_details uo_returns_details = await get_uo_mini_terminal_returns_details();
                        GridElement uo_returns_details_dw_summary = uo_returns_details.GetVisualElementById<GridElement>("dw_summary_details");
                        uo_mini_terminal_crossdoc_returns uo_crossdoc_return = await get_uo_mini_terminal_crossdoc_returns();
                        GridElement uo_crossdoc_return_dw_1 = uo_crossdoc_return.GetVisualElementById<GridElement>("dw_1");
                        GridElement uo_crossdoc_return_dw_summary = uo_crossdoc_return.GetVisualElementById<GridElement>("dw_summary");
                        uo_mini_terminal_crossdoc_returns_details uo_crossdoc_returns_details = await get_uo_mini_terminal_crossdoc_returns_details();
                        GridElement uo_crossdoc_returns_details_dw_summary = uo_crossdoc_returns_details.GetVisualElementById<GridElement>("dw_summary");
                        //ib_return_marlog = TRUE
                        //GalilCS - Hide all controls
                        HideAllControlInPnlContent();
                        uo_crossdoc_returns_details.Visible = true;

                        // AlexKh - 1.1.1.17 - 2014-02-06 - SPUCM00004702 - reset dw_summary
                        uo_returns_details_dw_summary.Reset();
                        if ((uo_crossdoc_return_dw_summary != null) && uo_crossdoc_return_dw_summary.RowCount() > 0)
                        {
                            uo_crossdoc_returns_details_dw_summary.SetDataRows(uo_crossdoc_return_dw_summary.DataRows());
                        }

                        await uo_crossdoc_returns_details.uf_create(true, uo_crossdoc_return.is_carton_barcode, uo_crossdoc_return.il_return_number, uo_crossdoc_return_dw_1.GetItemValue<long>(0, "supplier"), uo_crossdoc_return.il_marlog_branchProperty);
                    }
                    break;
                case "TRANS_TO_BRANCH":
                    uo_mini_terminal_trans_to_branch uo_trans_to_branch = await get_uo_mini_terminal_trans_to_branch();
                    //RonY@02/02/2015 - SPUCM00005163
                    //GalilCS - Hide all controls
                    HideAllControlInPnlContent();
                    uo_trans_to_branch.Visible = true;

                    break;
                case "TRANS_FROM_BRANCH":
                    uo_mini_terminal_trans_from_branch uo_trans_from_branch = await get_uo_trans_from_branch();

                    //RonY@02/02/2015 - SPUCM00005163
                    //GalilCS - Hide all controls
                    HideAllControlInPnlContent();
                    uo_trans_from_branch.Visible = true;
                    break;
                default:
                    //
                    break;
            }
            return 1;
        }

        private static void uo_shipments_list_dw_shipments_listReset(ComboGridElement uo_shipments_list_dw_shipments_list)
        {
            uo_shipments_list_dw_shipments_list.SelectedText = "";
            uo_shipments_list_dw_shipments_list.DroppedDown = true;
            uo_shipments_list_dw_shipments_list.Focus();
        }


        public async System.Threading.Tasks.Task<int> wf_retrieve_dw_summary() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            ComboGridElement order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_retrieve_dw_summary
            //*Purpose:							Retrieve  dw_summary
            //*  
            //*Arguments:						None
            //*										Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*Return:								Integer : 	1 - Success
            //*													-1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-07-2007    SHARP.			   	0.2.0			B2B 			Initiation
            //************************************************************************************************

            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_retrieved_rows = 0;
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            ll_supplier_number = Convert.ToInt64(supplier_number.Text);
            ll_order_number = Convert.ToInt64(order_number.Text);
            uo_body.dw_summary.SetRepository(new d_summery_inv_packRepository());
            ll_retrieved_rows = uo_body.dw_summary.Retrieve(ll_branch_number, ll_supplier_number, ll_order_number);
            if (ll_retrieved_rows < 0)
            {
                await wf_show_message("Error", "", "OK", "DB error: can not retrieve the invoices or paking list associete with the order you have chossed.");
                return -1;
            }
            //GalilCS - Change width of doc_no column
            uo_body.dw_summary.Columns["doc_no"].PixelWidth = 130;
            uo_body.dw_summary.ResetUpdate();
            return 1;
        }
        public void wf_get_window_position() //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_get_window_position
            //*Purpose:							
            //*  
            //*Arguments:						None.
            //*
            //*Return:								None.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-07-2007    SHARP. 				   0.2.0			B2B 			Initiation - same as in the old window
            //************************************************************************************************

            long ll_xpos = 0;
            long ll_ypos = 0;
            string ls_window_height = null;
            string ls_window_width = null;
            string ls_window_xpos = null;
            string ls_window_ypos = null;
            string ls_lang = null;
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
            {
                ls_lang = "alignment_left";
            }
            else
            {
                ls_lang = "alignment_right";
            }
            if (Convert.ToInt64(ls_window_xpos) > 0)
            {
                ll_xpos = Convert.ToInt64(ls_window_xpos);
            }
            else
            {
                ll_xpos = 0;
            }
            if (Convert.ToInt64(ls_window_ypos) > 0)
            {
                ll_ypos = Convert.ToInt64(ls_window_ypos);
            }
            else
            {
                ll_ypos = 0;
            }
            if (Convert.ToInt64(ls_window_width) > 0)
            {
                PixelWidth = (int)Convert.ToInt64(ls_window_width);
            }
            if (Convert.ToInt64(ls_window_height) > 0)
            {
                PixelHeight = (int)Convert.ToInt64(ls_window_height);
            }

            Visible = true;
        }
        public async Task<bool> wf_save_is_needed() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_save_is_needed
            //*Purpose:							This function will check if changes where done in the window
            //*  
            //*Arguments:						
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										None
            //*		
            //*Return:								INTEGER   -     TRUE - Changes where done 
            //*													  -     FALSE - No Changes where done.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007   SHARP.    				0.2.0 		B2B 			Initiation
            //************************************************************************************************

            bool lb_save_is_needed = false;




            lb_save_is_needed = ((uo_header_dw_body.DeletedCount() + uo_header_dw_body.ModifiedCount() + uo_body.dw_inv_pack_details.DeletedCount() + uo_body.dw_inv_pack_details.ModifiedCount()) > 0);
            return lb_save_is_needed;
        }
        public async Task<bool> wf_item_scanned() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_item_entered
            //*Purpose:							This  function will check if items where scanned
            //*  
            //*Arguments:						
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										None
            //*		
            //*Return:								BOOLEAN   -     TRUE    - Items where scanned
            //*													   -     FALSE  - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007    SHARP.				0.2.0  			B2B 			Initiation
            //************************************************************************************************

            bool lb_item_scanned = false;

            lb_item_scanned = uo_body.dw_inv_pack_details.RowCount() > 0;
            return lb_item_scanned;
        }
        public async Task<int> wf_new_doc() //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_new_doc
            //*Purpose:							This function will reset uo_header
            //*  
            //*Arguments:						
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										None 
            //*		
            //*Return:								INTEGER: 1 - Success
            //*													  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007    SHARP.					   0.2.0			B2B 			Initiation
            //************************************************************************************************

            uo_header.ue_new_doc();
            var res = await this.wf_reset();
            if (res < 1)
            {
                return -1;
            }
            return 1;
        }
        public async System.Threading.Tasks.Task<FuncResults<long, int>> wf_get_selected_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();

            TextBoxElement uo_header_dw_header = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");



            //TextBoxElement uo_header_dw_header = uo_header.GetVisualElementById<TextBoxElement>("dw_header");
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_get_selected_supplier_number
            //*Purpose:							This function will return the supplier number that was chosen.
            //*Arguments:					
            //*										Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Reference 	Long						al_supplier_number
            //*		
            //*Return:								INTEGER: 1 - Sucssess
            //*													  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*03-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_row_count = 0;

            //li_row_count = Convert.ToInt32(uo_header_dw_header.RowCount());
            //if (li_row_count < 0)
            if (uo_header_dw_header.Text == "")
            {
                await wf_show_message("Error", "", "OK", "Supplier was not choosen.");
                return FuncResults.Return(al_supplier_number, -1);
            }
            else
            {
                al_supplier_number = Convert.ToInt64(uo_header_dw_header.Text);
            }
            return FuncResults.Return(al_supplier_number, 1);
        }
        public async System.Threading.Tasks.Task<FuncResults<long, int>> wf_get_selected_order_no(long al_order_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            ComboGridElement uo_header_dw_header = uo_header.GetVisualElementById<ComboGridElement>("order_number");

            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_get_selected_order_no
            //*Purpose:							This function will return the order number that was chosen.
            //*Arguments:					
            //*										Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Reference 	Long						al_order_number
            //*		
            //*Return:								INTEGER: 1 - Sucssess
            //*													  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*05-08-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_row_count = 0;

            li_row_count = Convert.ToInt32(uo_header_dw_header.RowCount());
            if (li_row_count < 0)
            {
                await wf_show_message("Error", "", "OK", "Order was not choosen.");
                return FuncResults.Return(al_order_number, -1);
            }
            else
            {

                // al_order_number = uo_header_dw_header.GetItemValue<long>(0, "order_number");
                if (string.IsNullOrEmpty(Convert.ToString(uo_header_dw_header.SelectedValue)))
                {
                    al_order_number = Convert.ToInt64(uo_header_dw_header.Text);
                }
                else
                {
                    al_order_number = Convert.ToInt64(uo_header_dw_header.SelectedValue);
                }

            }
            return FuncResults.Return(al_order_number, 1);
        }
        public async Task<char> wf_get_doc_type(long? al_doc_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_get_doc_type
            //*Purpose:							Returns the document type (Invoice/PackList) 
            //*										for a given document number.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										value			long						al_doc_number:	Invoice/PackList number
            //*
            //*Return:								character:
            //*													'I' for Invoice, 
            //*													'P' for packList 
            //*													'E' for error 
            //*													''   for not found.
            //*
            //*Date 				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-08-2007		   SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            char lc_doc_type = '\0';
            string ls_find = null;
            long? ll_found_row = 0;
            lc_doc_type = default(char);
            if (isempty_stringClass.isempty_string(al_doc_number.ToString()) || al_doc_number == 0)
            {
                return 'E';
            }
            ls_find = "number == " + al_doc_number.ToString();


            ll_found_row = uo_header_dw_body.Find(ls_find, 0, uo_header_dw_body.RowCount());
            if (ll_found_row < -1)
            {
                return 'E';
            }
            else if (ll_found_row == -1)
            {
                return lc_doc_type;
            }
            else
            {

                lc_doc_type = Convert.ToChar(uo_header_dw_body.GetItemValue<string>(ll_found_row.Value, "doc_type"));
                return lc_doc_type;
            }

        }
        public async System.Threading.Tasks.Task<int> wf_set_focus(string as_uo_name, string as_dw_name, long al_row, string as_column) //**********************************************************************************************
        {
            uo_mini_terminal_returns_details uo_returns_details = null;
            uo_mini_terminal_pallets_list uo_shipments_list = null;
            uo_mini_terminal_header uo_header = null;
            uo_mini_terminal_pallet_manual uo_pallet_manual = null;
            uo_mini_terminal_body uo_body = null;
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_set_focus
            //*Purpose:							Set focus according to the arguments.
            //*										Set focus on the field:as_field_name on row: al_row_no in the dw:as_dw_name
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 			String 				as_uo_name
            //*										Value 			String 				as_dw_name
            //*										Value 			Long	 				al_row
            //*										Value 			String 				as_column
            //*		
            //*Return:								INTEGER :  -1 failed
            //*														1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-08-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            bool lb_visible = false;
            //Check if the UO that contains the field we want to set the focus in - is visible, if not set it to be visible.
            if (as_uo_name == "uo_mini_terminal_header")
            {
                uo_header = await get_uo_mini_terminal_header();
                lb_visible = uo_header.Visible;
                // AlexKh - 1.2.46.0 - 2010-06-20 - CR#1138 - check visibility of right UO	
            }
            else if (as_uo_name == "uo_mini_terminal_body")
            {
                uo_body = await get_uo_mini_terminal_body();
                lb_visible = uo_body.Visible;
            }
            else if (as_uo_name == "uo_mini_terminal_pallet_manual")
            {
                uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
                lb_visible = uo_pallet_manual.Visible;
            }
            else if (as_uo_name == "uo_mini_terminal_pallets_list")
            {
                uo_shipments_list = await get_uo_mini_terminal_pallets_list();
                lb_visible = uo_shipments_list.Visible;
                // AlexKh - 1.1.1.10 - 2013-07-07 - SPUCM00004291 - set focus to the right UO
            }
            else if (as_uo_name == "uo_mini_terminal_returns_details")
            {
                uo_returns_details = await get_uo_mini_terminal_returns_details();
                lb_visible = uo_returns_details.Visible;
            }
            if (!lb_visible)
            {
                if (as_uo_name == "uo_mini_terminal_header")
                {
                    li_rtn_code = await wf_display("HEADER");
                    // AlexKh - 1.2.46.0 - 2010-06-20 - CR#1138 - make visible right UO
                }
                else if (as_uo_name == "uo_mini_terminal_body")
                {
                    li_rtn_code = await wf_display("DETAILS");
                }
                else if (as_uo_name == "uo_mini_terminal_pallet_manual")
                {
                    li_rtn_code = await wf_display("PALLET");
                }
                else if (as_uo_name == "uo_mini_terminal_pallets_list")
                {
                    li_rtn_code = await wf_display("SHIPMENT");
                }
                if (li_rtn_code < 1)
                {
                    return li_rtn_code;
                }
            }
            switch (as_uo_name)
            {
                case "uo_mini_terminal_header":

                    if (as_dw_name == "dw_header")
                    {
                        ComboBoxElement supplier_name = uo_header.GetVisualElementById<ComboBoxElement>("supplier_name");
                        ComboGridElement order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");
                        TextBoxElement supplier_number = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");
                        order_number.Reset();
                        order_number.Text = null;
                        supplier_name.Items.Clear();
                        supplier_name.Text = null;
                        supplier_number.Text = null;
                        supplier_number.Focus();
                    }
                    else
                    {
                        GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
                        uo_header_dw_body.Focus();
                        uo_header_dw_body.SetRow((int)al_row);
                        uo_header_dw_body.SetColumn(as_column);
                        uo_header_dw_body.SelectText(1, 20);
                    }
                    // AlexKh - 1.2.46.0 - 2010-06-20 - CR#1138 - set focus to the right UO
                    break;
                case "uo_mini_terminal_body":
                    if (as_dw_name == "dw_inv_pack_details")
                    {
                        uo_body.dw_inv_pack_details.SetRow((int)al_row);
                        uo_body.dw_inv_pack_details.SetColumn(as_column);
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_body.dw_inv_pack_details.SetFocus((int)al_row, as_column);
                        });
                    }
                    else
                    {
                        uo_body.dw_summary.Focus();
                        uo_body.dw_summary.SetRow((int)al_row);
                        uo_body.dw_summary.SetColumn(as_column);
                    }
                    break;
                case "uo_mini_terminal_pallet_manual":
                    if (as_dw_name == "uo_mini_terminal_pallet_manual_dw_inv_pack_details")
                    {
                        uo_pallet_manual.dw_inv_pack_details.SetRow((int)al_row);
                        uo_pallet_manual.dw_inv_pack_details.SetColumn(as_column);
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_pallet_manual.dw_inv_pack_details.SetFocus((int)al_row, as_column);
                        });
                    }
                    else
                    {
                        uo_pallet_manual.dw_summary.Focus();
                        uo_pallet_manual.dw_summary.SetRow((int)al_row);
                        uo_pallet_manual.dw_summary.SetColumn(as_column);
                        uo_pallet_manual.dw_summary.SelectText(1, 20);
                    }
                    break;
                case "uo_mini_terminal_pallets_list":
                    GridElement uo_shipments_list_dw_pallets_list = uo_shipments_list.GetVisualElementById<GridElement>("dw_pallets_list");
                    uo_shipments_list_dw_pallets_list.Focus();
                    uo_shipments_list_dw_pallets_list.SetRow((int)al_row);
                    uo_shipments_list_dw_pallets_list.SetColumn(as_column);
                    uo_shipments_list_dw_pallets_list.SelectText(1, 20);
                    // AlexKh - 1.1.1.10 - 2013-07-07 - SPUCM00004291 - set focus to the right UO
                    break;
                case "uo_mini_terminal_returns_details":
                    if (as_dw_name == "dw_inv_pack_details")
                    {
                        WidgetGridElement uo_returns_details_dw_inv_pack_details = uo_returns_details.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_returns_details_dw_inv_pack_details");
                        uo_returns_details_dw_inv_pack_details.SetRow((int)al_row);
                        uo_returns_details_dw_inv_pack_details.SetColumn(as_column);
                        Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
                        {
                            uo_returns_details_dw_inv_pack_details.SetFocus((int)al_row, as_column);
                        });
                    }
                    break;
            }
            return 1;
        }
        public async System.Threading.Tasks.Task<int> wf_build_doc_number_list() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_build_doc_number_list
            //*Purpose:							This function will initiate the doc number list box with the invoices numbers and packung list numbers 
            //*										from uo_mini_terminal_header.dw_body
            //*  
            //*Arguments:						None 
            //*Return:								INTEGER 1 - Success
            //*													 -1 - Otherwise.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-08-2007		SHARP.				0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_row_count = 0;
            int li_counter = 0;
            int li_b2b_status = 0;
            string ls_doc_type = null;
            long ll_doc_number = 0;
            long ll_row_no = 0;
            long ll_items = 0;
            long ll_items_count = 0;
            long ll_doc_no_en = 0;
            string lc_state = "\0";
            bool lb_doc_no_enable = false;
            GridColumn ldwc_docs;

            li_row_count = Convert.ToInt32(uo_header_dw_body.RowCount());
            if (li_row_count < 0)
            {
                //Error
                await wf_show_message("Error", "", "OK", "Error while trying to count number of docs in w_mini_terminal.wf_doc_number_list");
                return -1;
            }
            else if (li_row_count == 0)
            {
                // SharonS - 1.2.42.1 - 2009-02-11 - SPUCM00000861
                //		wf_show_message("Error", "", "OK", "In w_mini_terminal.wf_doc_number_list: Cannot enter items while there is no docs")
                // End
                return -1;
            }
            else
            {
                WidgetGridElement dw_inv_pack_details = uo_body.dw_inv_pack_details.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details");
                //uf_calculate_declines
                IRepository repository = new dddw_docsRepository();
                ldwc_docs = uo_body.GetDocNo(dw_inv_pack_details);
                ldwc_docs.SetRepository(repository);
                ldwc_docs.GetRepository().Reset();
                for (li_counter = 0; li_counter < li_row_count; li_counter++)
                {
                    lc_state = uo_header_dw_body.GetItemValue<string>(li_counter, "state");
                    if (lc_state.ToString() != "C" && lc_state.ToString() != "S")
                    {
                        ls_doc_type = uo_header_dw_body.GetItemValue<string>(li_counter, "doc_type");
                        ll_doc_number = uo_header_dw_body.GetItemValue<long>(li_counter, "number");
                        // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Add the b2b_status
                        li_b2b_status = Convert.ToInt32(uo_header_dw_body.GetItemValue<long>(li_counter, "b2b_status"));
                        if (li_b2b_status == 0)
                        {
                            lb_doc_no_enable = true;
                        }
                        // End

                        ll_row_no = ldwc_docs.GetRepository().Insert(0);
                        if (ll_row_no < 0)
                        {
                            await wf_show_message("Error", "", "OK", "Error while building order's doc list");
                            return -1;
                        }


                        ldwc_docs.GetRepository().SetItem(ll_row_no, "doc_type", Convert.ToString(ls_doc_type));

                        ldwc_docs.GetRepository().SetItem(ll_row_no, "doc_number", ll_doc_number);
                        // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Add the b2b_status

                        ldwc_docs.GetRepository().SetItem(ll_row_no, "b2b_status", li_b2b_status);
                        // End


                        var dt = ldwc_docs.GetRepository().GetDataTable();
                        dt.Columns[0].ColumnName = "value";
                        dt.Columns[1].ColumnName = "text";
                        var JsonData = GridMultiComboBoxElement.GetComboBoxData("", dt);
                        for (int i = 0; i < uo_body.dw_inv_pack_details.Rows.Count; i++)
                        {
                            uo_body.dw_inv_pack_details.Rows[i].Cells["invoice_data"].Value = JsonData;
                            uo_body.dw_inv_pack_details.SetItem(i, "invoice_data", JsonData);
                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(i, "invoice_data", JsonData);
                        }
                    }

                }
                invuo_doc_item_searchProperty.ids_inv_details.ResetUpdate();

                repository.ResetUpdate();
            }
            return 1;
        }
        public async Task<int> wf_reset() //**********************************************************************************************
        {
            uo_mini_terminal_pallet_manual uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            ButtonElement uo_header_cb_details = uo_header.GetVisualElementById<ButtonElement>("cb_details");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            // TODO: Member 'uo_header_dw_header' was retyped to dynamic cause it had reflected members. (CODE=30005)
            // Galilcs TODO: uo_header_dw_header.Reset();
            TextBoxElement uo_header_supplier_number = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");
            ComboBoxElement uo_header_supplier_name = uo_header.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement uo_header_order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");

            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_reset
            //*Purpose:							Reset  the dw_body of the header UO and the dw of the items in the body UO
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer: 1-Success , -1 -Failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-08-2007    SHARP. 					0.2.0		B2B 			Initiation - same as in the old window
            //************************************************************************************************

            long ll_null = 0;

            //uo_header_dw_header.Reset();


            uo_header_supplier_name.Items.Clear();
            uo_header_supplier_name.Text = null;

            uo_header_order_number.Text = null;
            uo_header_supplier_number.Text = "";
            uo_header_order_number.Reset();

            // Galilcs TODO: dw_header_ue_new_row
            //uo_header_dw_header.dw_header_ue_new_row();

            uo_header_dw_body.Reset();
            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).Reset();
            uo_header_cb_details.Enabled = false;
            // AlexKh - 1.2.46.0 - 2010-06-20 - CR#1138 - reset UO and DW

            ll_null = 0;

            //uf_calculate_declines
            (uo_pallet_manual.dw_inv_pack_details).Reset();

            uo_pallet_manual.dw_summary.Reset();
            return 1;
        }
        public async System.Threading.Tasks.Task<int> wf_retrieve_data(long? supplier_number = null, long? order_number = null) //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            //ComboGridElement order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_retrieve_data
            //*Purpose:							
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer: 1-Success , -1 -Failed.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*					SHARP							0.2.0
            //************************************************************************************************

            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_rtn_code = 0;
            long ll_SetSort = 0;
            long ll_sort = 0;
            long ll_i = 0;
            long? ll_found = 0;
            long ll_count = 0;
            // AlexKh - 1.2.45.34 - 2010-06-10 - SPUCM00002176 - increase array to 100 invoices.		
            object[,] ls_locs = new object[101, 4];
            if (supplier_number == null)
            {
                (await this.wf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            }
            else
            {
                ll_supplier_number = supplier_number.Value;
            }

            if (order_number == null)
            {
                (await this.wf_get_selected_order_no(ll_order_number)).Retrieve(out ll_order_number);
            }
            else
            {
                ll_order_number = order_number.Value;
            }
            invuo_doc_item_searchProperty.uf_set_supplier_number(ll_supplier_number);
            invuo_doc_item_searchProperty.uf_set_order_number(ll_order_number);
            // AlexKh - 1.2.45.0 - 2009-07-12 - SPUCM00001243 - CR#1119 - save_current locks

            ll_count = uo_header_dw_body.RowCount();
            for (ll_i = 0; ll_i < ll_count; ll_i++)
            {
                ls_locs[ll_i, 0] = uo_header_dw_body.GetItemValue<Int64?>(ll_i, "number")?.ToString();

                ls_locs[ll_i, 1] = uo_header_dw_body.GetItemValue<string>(ll_i, "doc_type");

                ls_locs[ll_i, 2] = uo_header_dw_body.GetItemValue<string>(ll_i, "create_mode");

            }
            // AlexKh - 2009-07-12 - End 

            uo_header_dw_body.Reset();
            ll_rtn_code = uo_body.dw_inv_pack_details.Reset();
            invuo_doc_item_searchProperty.uf_retrieve();
            await this.wf_populate();
            // AlexKh - 1.2.45.0 - 2009-07-12 - SPUCM00001243 - CR#1119 - save_current locks
            for (ll_i = 0; ll_i < ll_count; ll_i++)
            {
                if (ls_locs[ll_i, 2].ToString() == "1")
                {

                    var st1 = SystemHelper.Concat("number == ", ls_locs[ll_i, 0]);
                    var st2 = SystemHelper.Concat(" AND doc_type == \"", ls_locs[ll_i, 1]);
                    var st3 = SystemHelper.Concat(st1, st2);
                    var st4 = SystemHelper.Concat(st3, "\"");
                    ll_found = uo_header_dw_body.Find(st4, 0, uo_header_dw_body.RowCount());

                    if (ll_found >= 0)
                    {

                        uo_header_dw_body.SetItem(ll_found.Value, "create_mode", "1");
                    }
                }
            }
            // AlexKh - 2009-07-12 - End 
            //////////////////////////////////////////////////////
            //ls_sort = uo_body.dw_inv_pack_details.Object.DataWindow.Table.Sort

            ll_SetSort = uo_body.dw_inv_pack_details.SetSort("indicator ASC");

            ll_sort = uo_body.dw_inv_pack_details.Sort();

            //GalilCS - Sort grid
            uo_header_dw_body.SetSort("supply_date ASC");
            uo_header_dw_body.Sort();
            uo_header_dw_body.SetRepository(uo_header_dw_body.Repository(), overwrite: true);
            //ls_sort = uo_body.dw_inv_pack_details.Object.DataWindow.Table.Sort
            /////////////////////////////////////////////////////////////////
            return 1;
        }
        public async System.Threading.Tasks.Task<int> wf_populate() //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            ButtonElement uo_header_cb_details = uo_header.GetVisualElementById<ButtonElement>("cb_details");
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            ComboGridElement order_number = uo_header.GetVisualElementById<ComboGridElement>("order_number");
            //GridElement uo_header_dw_header = uo_header.GetVisualElementById<GridElement>("dw_header");
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_populate
            //*Purpose:							
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										None 
            //*		
            //*Return:								INTEGER:    1 - Success
            //*													 	-1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16/10/2007	    SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            long ll_row_count = 0;
            long ll_rows_copy = 0;
            long ll_docs_count = 0;
            long ll_items_count = 0;
            long ll_cnt = 0;
            long ll_order_number = 0;
            long ll_SetSort = 0;
            long ll_sort = 0;
            long ll_doc = 0;
            long ll_item = 0;
            long ll_doc_no_en = 0;
            long ll_cnt_b2b_docs = 0;
            long ll_cnt_non_b2b_docs = 0;
            int li_rtn_code = 0;
            int li_b2b_status = 0;
            string ls_filter = null;
            string ls_order_number = null;
            string lc_state = "";
            //Set the order no in the title



            ll_order_number = Convert.ToInt64(order_number.Text);
            ls_order_number = Convert.ToString(ll_order_number);
            if (isempty_stringClass.isempty_string(ls_order_number))
            {
                await wf_show_message("Error", "", "OK", "Order number is empty.");
                return -1;
            }

            var st_order_number = uo_body.GetVisualElementById<ControlElement>("st_order_number");
            st_order_number.Text = ls_order_number;
            // ...Invoice Move
            // Get the related invoices and copy into uo_header.dw_body datawindow 

            ll_row_count = invuo_doc_item_searchProperty.ids_inv_move.RowCount();
            if (ll_row_count > 0)
            {
                ll_rows_copy = invuo_doc_item_searchProperty.ids_inv_move.RowsCopy(0, invuo_doc_item_searchProperty.ids_inv_move.RowCount(), ModelBuffer.Primary, uo_header_dw_body.Repository(), 0, ModelBuffer.Primary);
                if (ll_rows_copy == 0 || ll_rows_copy < 0)
                {
                    await wf_show_message("RowsCopy Error", "", "OK", "Error in copying invoice items.");
                    return -1;
                }
            }
            // ...Packlist Move
            // Get the related packlists and copy into uo_header.dw_body datawindow 

            ll_row_count = invuo_doc_item_searchProperty.ids_pack_move.RowCount();
            if (ll_row_count > 0)
            {

                ll_rows_copy = invuo_doc_item_searchProperty.ids_pack_move.RowsCopy(0, invuo_doc_item_searchProperty.ids_pack_move.RowCount(), ModelBuffer.Primary, uo_header_dw_body.Repository(), uo_header_dw_body.RowCount() + 1, ModelBuffer.Primary);
                if (ll_rows_copy == 0 || ll_rows_copy < 0)
                {
                    await wf_show_message("RowsCopy Error", "", "OK", "Error in copying packlist items.");
                    return -1;
                }
            }
            // Set cb_details button to be enable
            if (ll_rows_copy > 0)
            {
                if (uo_header_cb_details.Enabled == false)
                {
                    uo_header_cb_details.Enabled = true;
                }
            }
            // ...Invoice Details
            // Set filter - only not b2b items
            // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Delete items
            //ls_filter = '(IsNull(b2b_status) or b2b_status = 0) or ( b2b_status = 1 and not IsNull( expected_material_quantity ) )'
            // SharonS - 1.2.44.5 - 2009-05-24 - SPUCM00000075 - Update filter items
            ls_filter = "not ( expected_material_quantity = 0 or  expected_material_quantity == null  )";
            // End
            // End

            invuo_doc_item_searchProperty.ids_inv_details.SetFilter(ls_filter);

            invuo_doc_item_searchProperty.ids_inv_details.Filter();
            // Get the related invoices items and copy into dw_inv_pack_details datawindow


            ll_row_count = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
            if (ll_row_count > 0)
            {
                var repository = new dddw_docsRepository();
                var docno = uo_body.GetDocNo(uo_body.dw_inv_pack_details);
                docno.SetRepository(repository);
                ll_rows_copy = invuo_doc_item_searchProperty.ids_inv_details.RowsCopy(0, invuo_doc_item_searchProperty.ids_inv_details.RowCount(), ModelBuffer.Primary, uo_body.dw_inv_pack_details, 0, ModelBuffer.Primary);
                if (ll_rows_copy == 0 || ll_rows_copy < 0)
                {
                    await wf_show_message("RowsCopy Error", "", "OK", "Error in copying invoice items.");
                    return -1;
                }
            }
            // Set filter - Original

            invuo_doc_item_searchProperty.ids_inv_details.SetFilter("");

            invuo_doc_item_searchProperty.ids_inv_details.Filter();
            // ... Packlist Details
            // Set filter - only not b2b items

            invuo_doc_item_searchProperty.ids_pack_details.SetFilter(ls_filter);

            invuo_doc_item_searchProperty.ids_pack_details.Filter();
            // Get the related packlists items and copy into dw_inv_pack_details datawindow 

            ll_row_count = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
            if (ll_row_count > 0)
            {
                ll_rows_copy = invuo_doc_item_searchProperty.ids_pack_details.RowsCopy(0, invuo_doc_item_searchProperty.ids_pack_details.RowCount(), ModelBuffer.Primary, uo_body.dw_inv_pack_details.Repository(), uo_body.dw_inv_pack_details.RowCount() + 1, ModelBuffer.Primary);
                if (ll_rows_copy == 0 || ll_rows_copy < 0)
                {
                    await wf_show_message("RowsCopy Error", "", "OK", "Error in copying packlist items.");
                    return -1;
                }
            }
            // Set filter - Original

            invuo_doc_item_searchProperty.ids_pack_details.SetFilter("");

            invuo_doc_item_searchProperty.ids_pack_details.Filter();
            //Set the row no

            ll_row_count = uo_body.dw_inv_pack_details.RowCount();
            uo_body.il_row_no = ll_row_count + 1;

            ll_SetSort = uo_body.dw_inv_pack_details.SetSort("indicator ASC");

            ll_sort = uo_body.dw_inv_pack_details.Sort();
            for (ll_cnt = 1; ll_cnt <= ll_row_count; ll_cnt++)
            {

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(ll_cnt - 1, "row_no", ll_cnt);
            }
            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-08-07 - Task#10012 - SPUCM00000094 - Update 

            ll_docs_count = uo_header_dw_body.RowCount();
            // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - CR#1119 - set create mode to not locking invoice
            if (ll_docs_count == 1)
            {

                uo_header_dw_body.SetItem(0, "create_mode", "0");
            }
            if (ll_docs_count > 1)
            {
                for (ll_cnt = 0; ll_cnt < ll_docs_count; ll_cnt++)
                {

                    li_b2b_status = (int)uo_header_dw_body.GetItemValue<long>(ll_cnt, "b2b_status");
                    // SharonS - 1.2.37.4 - 2008-11-18 - Task#10012 - SPUCM00000094 - Get State, only in Open state

                    lc_state = uo_header_dw_body.GetItemValue<string>(ll_cnt, "state");
                    if (lc_state.ToString() == "O")
                    {
                        // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - CR#1119 - set create mode to not locking invoice

                        uo_header_dw_body.SetItem(ll_cnt, "create_mode", "0");
                        if (li_b2b_status == 1)
                        {

                            ll_cnt_b2b_docs++;
                        }
                        else
                        {

                            ll_cnt_non_b2b_docs++;
                        }
                    }
                    // End
                }
                if ((ll_cnt_b2b_docs == 0 && ll_cnt_non_b2b_docs > 1) || (ll_cnt_b2b_docs > 0 && ll_cnt_non_b2b_docs > 0))
                {
                    for (ll_cnt = 1; ll_cnt <= ll_row_count; ll_cnt++)
                    {
                        // SharonS - 1.2.37.4 - 2008-11-18 - Task#10012 - SPUCM00000094 - Get State, only in Open state
                        //			THIS.uo_body.dw_inv_pack_details.SetItem(ll_cnt, "doc_no_en", 2)

                        lc_state = uo_body.dw_inv_pack_details.GetItemValue<string>(ll_cnt, "doc_state");
                        if (Convert.ToString(lc_state) == "O")
                        {

                            //uf_calculate_declines
                            (uo_body.dw_inv_pack_details).SetItem(ll_cnt, "doc_no_en", 2);
                        }
                        // End
                    }
                }
            }
            // End
            // Set the update flags off

            uo_header_dw_body.ResetUpdate();

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).ResetUpdate();
            // Set filter to display only items that have quantity difference
            //li_rtn_code = THIS.uo_body.uf_show_items(FALSE)

            li_rtn_code = await uo_body.uf_show_items(true);
            if (li_rtn_code < 1)
            {
                await wf_show_message("Error", "", "OK", "Error while filter items");
                return -1;
            }
            //// Set summery data
            await wf_retrieve_dw_summary();
            return 1;
        }
        public async System.Threading.Tasks.Task<bool> wf_update_doc(long al_row_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_update_doc
            //*Purpose:							
            //*  
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 			long 					al_row_number 			:window title
            //*
            //*Return:								BOOLEAN: TRUE  - succeeded.
            //*												   FALSE - failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-10-2007    SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            char lc_doc_type = '\0';
            string ls_find = null;
            long ll_rows = 0;
            long? ll_doc_number = null;
            long ll_logbook = 0;
            long? ll_found = 0;
            long ll_b2b_status = 0;
            decimal ld_total = default(decimal);
            decimal ldec_discount = default(decimal);
            decimal ldec_descount_percent = default(decimal);
            DateTime? ldt_supply_date = default(DateTime);

            lc_doc_type = Convert.ToChar(uo_header_dw_body.GetItemValue<string>(al_row_number, "doc_type"));

            ll_doc_number = uo_header_dw_body.GetItemValue<Int64?>(al_row_number, "number");

            ldt_supply_date = uo_header_dw_body.GetItemValue<DateTime>((int)al_row_number, "supply_date");

            ld_total = uo_header_dw_body.GetItemValue<decimal>(al_row_number, "total");

            ll_logbook = uo_header_dw_body.GetItemValue<long>(al_row_number, "log_book");
            // AlexKh - 1.1.29.4 - 2015-05-19 - SPUCM00005045 - in case of null value set 0
            if (ll_logbook == 0)
            {
                ll_logbook = 0;
            }
            // SharonS - 1.2.37.6 - 2008-11-26 - Task#10012 - Set the order discount and discount percent

            ll_b2b_status = uo_header_dw_body.GetItemValue<long>(al_row_number, "b2b_status");
            if (ll_b2b_status == 1)
            {
                ldec_descount_percent = uo_header.iuo_order_searchProperty.uf_get_order_discount();
                ldec_discount = ld_total * ldec_descount_percent / 100;
            }
            // End
            if (uo_header.ic_old_doc_typeProperty == uo_header.ic_new_doc_typeProperty)
            {
                if (lc_doc_type.ToString() == "I")
                {
                    // Invoice
                    ls_find = "invoice_number == " + ll_doc_number.ToString();

                    ll_rows = invuo_doc_item_searchProperty.ids_inv_move.RowCount();
                    if (ll_rows < 1)
                    {
                        await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }

                    ll_found = invuo_doc_item_searchProperty.ids_inv_move.Find(ls_find, 0, (int)ll_rows);
                    if (ll_found < 0)
                    {
                        await wf_show_message("wf_update_doc Error", "", "OK", ".����� ����� ���� �����");
                        return false;
                    }

                    invuo_doc_item_searchProperty.ids_inv_move.SetItem(ll_found.Value, "supply_date", ldt_supply_date.ToString());

                    invuo_doc_item_searchProperty.ids_inv_move.SetItem(ll_found.Value, "invoice_total", ld_total.ToString());

                    invuo_doc_item_searchProperty.ids_inv_move.SetItem(ll_found.Value, "log_book", ll_logbook.ToString());
                    // SharonS - 1.2.37.6 - 2008-11-26 - Task#10012 - Set the order discount and discount percent
                    if (ll_b2b_status == 1)
                    {

                        invuo_doc_item_searchProperty.ids_inv_move.SetItem(ll_found.Value, "discount", ldec_discount.ToString());

                        invuo_doc_item_searchProperty.ids_inv_move.SetItem(ll_found.Value, "discount_percent", ldec_descount_percent.ToString());
                    }
                    // End

                    using (UnitOfWork unitOfWork = new UnitOfWork())
                    {
                        invuo_doc_item_searchProperty.ids_inv_move.Update(unitOfWork);
                    }
                }
                else if (lc_doc_type.ToString() == "P")
                {
                    // Packlist
                    ls_find = "pack_list_number == " + ll_doc_number.ToString();

                    ll_rows = invuo_doc_item_searchProperty.ids_pack_move.RowCount();
                    if (ll_rows < 1)
                    {
                        await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }

                    ll_found = invuo_doc_item_searchProperty.ids_pack_move.Find(ls_find, 0, (int)ll_rows);
                    if (ll_found < 0)
                    {
                        await wf_show_message("wf_update_doc Error", "", "OK", ".����� ����� ���� �����");
                        return false;
                    }

                    invuo_doc_item_searchProperty.ids_pack_move.SetItem(ll_found.Value, "supply_date", ldt_supply_date.ToString());
                    // SharonS - 1.2.31.0 - 2008-05-01 - Task#10004 - Bug 
                    //		invuo_doc_item_search.ids_inv_move.SetItem(ll_found, "pack_list_total", ld_total)

                    invuo_doc_item_searchProperty.ids_pack_move.SetItem(ll_found.Value, "pack_list_total", ld_total.ToString());
                    // End

                    invuo_doc_item_searchProperty.ids_pack_move.SetItem(ll_found.Value, "log_book", ll_logbook.ToString());
                    // SharonS - 1.2.37.6 - 2008-11-26 - Task#10012 - Set the order discount percent
                    if (ll_b2b_status == 1)
                    {

                        invuo_doc_item_searchProperty.ids_pack_move.SetItem(ll_found.Value, "discount", ldec_discount.ToString());

                        invuo_doc_item_searchProperty.ids_pack_move.SetItem(ll_found.Value, "discount_percent", ldec_descount_percent.ToString());
                    }
                    // End
                    using (UnitOfWork unitOfWork = new UnitOfWork())
                    {
                        invuo_doc_item_searchProperty.ids_pack_move.Update(unitOfWork);
                    }
                }
            }
            else
            {
                //This is the case in which change was done in the type of the doc
                //In this case we should 
                //		1.	delete the doc and its items of the old type from the 2
                //					 dws of old type:move and items
                //		2.  add new rows for the new doc and its items in the 2 dws
                //			 of the new type:move and details 
                if (uo_header.ic_old_doc_typeProperty.ToString() == "I")
                {
                    //
                }
                else
                {
                    //
                }
            }
            return true;
        }
        public async Task<int> wf_set_order_number(long al_order_number) //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_set_order_number
            //*Purpose:							
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer:  1 - Success
            //*												   -1 - Otherwise.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-10-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            uo_body.iuo_order_searchProperty.uf_set_order_number(al_order_number);
            return 1;
        }
        public async Task<int> wf_set_supplier_number(long al_supplier_number) //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_set_supplier_number
            //*Purpose:							
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer:  1 - Success
            //*												   -1 - Otherwise.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-10-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************
            // Galilcs - TODO uo_body.uf_new_row();
            uo_body.iuo_order_searchProperty.uf_set_supplier_number(al_supplier_number);
            return 1;
        }
        public decimal wf_get_doc_item_quantity(long al_material_number) //**********************************************************************************************
        {
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_get_doc_item_quantity
            //*Purpose:							
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value			LONG					al_material_numbere
            //*		
            //*Return:								DECIMAL: 
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-10-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            decimal ldec_rtn_code = default(decimal);
            ldec_rtn_code = invuo_doc_item_searchProperty.uf_get_doc_item_quantity(al_material_number);
            return ldec_rtn_code;
        }

        protected async System.Threading.Tasks.Task<int> wf_set_material_details(long al_row, string as_barcode, char lc_doc_type, long al_found_row, string as_row_status) //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_set_material_details.
            //*Purpose:							Get material details and set them in the DW if its a new material or in the DS if its an existing material
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										---------------------------------------------------------------------
            //*										Value			Long					al_row				The row number in the DW (for new row)
            //*										Value 		String					as_barcode			The barcode that was insert
            //*										Value 		Char 					lc_doc_type			The document type of the material document 
            //*										Value			Long					al_found_row		The row number in the DS (for exists row)
            //*										Value			String					as_row_status		The row status, NewModified or DataModified
            //*
            //*RETURN:							Long 		1-Success, -1 Failed
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*15-10-2007    SHARP.    				0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            string ls_material_name = null;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_material_serial_number_in_order = 0;
            long ll_material_number = 0;
            long ll_min_valid_month = 0;
            long ll_error_number = 0;
            long ll_order_material_color = 0;
            long ll_ret = 0;
            long ll_ind = 0;
            long ll_rows = 0;
            decimal ldec_sell_price = default(decimal);
            decimal ldec_material_price = default(decimal);
            decimal ldec_material_price_after_discount = default(decimal);
            decimal ldec_material_discount_percent = default(decimal);
            bool lb_is_item_b2b = false;
            DateTime? ldt_curr_expiration_date = default(DateTime);
            li_rtn_code = (await wf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            li_rtn_code = (await wf_get_selected_order_no(ll_order_number)).Retrieve(out ll_order_number);
            // Get material-number, material-name, minimum valid month for material by barcode
            (await uo_body.iuo_material_search.uf_get_material_info(as_barcode, ll_material_number, ls_material_name, ll_min_valid_month)).Retrieve(out ll_material_number, out ls_material_name, out ll_min_valid_month);
            if (ll_material_number == 0)
            {
                await this.wf_show_message("", "", "OK", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("�� ���� ���� �����"));

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", 0);
                return -1;
            }
            // Gets material-prices from material_supplier OR supplier_order_details table
            (await uo_body
                .uf_get_order_material_price(ll_supplier_number, ll_order_number, ll_material_number,
                    ldec_material_price, ldec_material_price_after_discount, ldec_material_discount_percent,
                    ll_error_number)).Retrieve(out ldec_material_price, out ldec_material_price_after_discount, out ldec_material_discount_percent, out ll_error_number);
            // Error occure (for example DB error)
            if (ll_error_number == -1)
            {
                await wf_show_message("�����", "", "OK", ".����� ������ ���� �����");
                return -1;
            }
            // Gets value from branch_item_price table for this branch and material number
            ldec_sell_price = await uo_body.iuo_material_search.uf_get_material_sell_price(masofonAlias.masofon.Instance.gs_vars.branch_number, ll_material_number);
            // If material assigned to order sets its order-color-number, otherwise sets null
            // SharonS - 1.2.38.4 - 2008-12-29 - CR#1104 - Remove the call the the function uf_get_order_material_color.
            //ll_order_material_color = uo_body.iuo_order_search.uf_get_order_material_color(ll_material_number)

            ll_order_material_color = 0;
            // End
            // SharonS - 1.2.37.2 - 2008-11-09 - Task#10012 - SPUCM00000094
            //lb_is_item_b2b = invuo_doc_item_search.uf_is_item_b2b(ll_material_number)
            // End

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_name", ls_material_name);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_number", ll_material_number);
            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Check if there is a non b2b doc
            //IF lb_is_item_b2b THEN
            //	Long		ll_doc_no_en
            //	ll_doc_no_en = uo_body.dw_inv_pack_details.GetItem<int>(al_row, "doc_no_en")
            //END IF

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "barcode", as_barcode);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_price", ldec_material_price);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_price_after_discount", ldec_material_price_after_discount);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_discount_percent", ldec_material_discount_percent);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "expiration_date", ldt_curr_expiration_date);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "sell_price", ldec_sell_price);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "current_catalog_sell_price", ldec_sell_price);

            ll_rows = uo_body.dw_inv_pack_details.RowCount();

            if (!(uo_body.dw_inv_pack_details.GetItemValue<long>(al_row, "indicator") > 0))
            {


                //uf_calculate_declines
                // Galilcs - TODO Describe
                //(uo_body.dw_inv_pack_details).SetItem(al_row, "indicator", Convert.ToInt64(uo_body.dw_inv_pack_details.Describe("Evaluate('MAX(indicator for all)',1)")) + 1);
                (uo_body.dw_inv_pack_details).SetItem(al_row, "indicator", Convert.ToInt64(uo_body.dw_inv_pack_details.Describe<long>("Evaluate('MAX(indicator for all)',1)")) + 1);
            }

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "supplier_number", ll_supplier_number);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "mini_terminal", 1);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "color", ll_order_material_color);
            // End
            // SharonS - 1.2.36.4 -> 1.2.37.8 - 2008-10-22 - Task#10012 - SPUCM00000094
            //IF as_row_status = 'NEW' AND NOT lb_is_item_b2b THEN
            if (as_row_status == "NEW")
            {
                // End

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "barcode", as_barcode);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "material_price", ldec_material_price);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "material_price_after_discount", ldec_material_price_after_discount);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "material_discount_percent", ldec_material_discount_percent);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "expiration_date", ldt_curr_expiration_date);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "sell_price", ldec_sell_price);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "current_catalog_sell_price", ldec_sell_price);

                ll_rows = uo_body.dw_inv_pack_details.RowCount();

                if (!(uo_body.dw_inv_pack_details.GetItemValue<long>(al_row, "indicator") > 0))
                {


                    //uf_calculate_declines
                    // Galilcs - TODO Describe
                    //(uo_body.dw_inv_pack_details).SetItem(al_row, "indicator", Convert.ToInt64(uo_body.dw_inv_pack_details.Describe("Evaluate('MAX(indicator for all)',1)")) + 1);
                }

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "supplier_number", ll_supplier_number);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "mini_terminal", 1);

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "color", ll_order_material_color);
                // SharonS - 1.2.37.9 - 2008-11-07 - Only if al_found_row is valid (<> 0)
                //ELSE
            }
            else if (as_row_status == "OLD" && al_found_row != 0)
            {
                // End 	
                if (lc_doc_type.ToString() == "I")
                {
                    // SharonS - 1.2.29.4 - 2008-04-08 - Set min_valid_months in dw_inv_pack_details also.

                    //uf_calculate_declines
                    (uo_body.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);
                    // End 

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "material_number", ll_material_number.ToString());

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "materials_barcode", Convert.ToString(as_barcode));

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "material_price", ldec_material_price.ToString());

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "material_price_after_discount", ldec_material_price_after_discount.ToString());

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "material_discount_percent", ldec_material_discount_percent.ToString());
                    //invuo_doc_item_search.ids_inv_details.SetItem(al_found_row, "invoice_details_expiration_date", ldt_curr_expiration_date)		

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "sell_price", ldec_sell_price.ToString());

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "current_catalog_sell_price", ldec_sell_price.ToString());

                    ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();

                    if (!(invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<long>((int)al_found_row, "indicator") > 0))
                    {


                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "indicator", Convert.ToString(Convert.ToInt64(invuo_doc_item_searchProperty.ids_inv_details.Describe("Evaluate('MAX(indicator for all)',1)")) + 1));
                    }

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "supplier_number", ll_supplier_number.ToString());

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "mini_terminal", "1");

                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(al_found_row, "color", ll_order_material_color.ToString());
                }
                else if (lc_doc_type.ToString() == "P")
                {
                    // SharonS - 1.2.29.4 - 2008-04-08 - Get min_valid_months  in dw_inv_pack_details also.

                    //uf_calculate_declines
                    (uo_body.dw_inv_pack_details).SetItem(al_row, "min_valid_months", ll_min_valid_month);
                    // End 

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "material_number", ll_material_number.ToString());

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "materials_barcode", Convert.ToString(as_barcode));

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "material_price", ldec_material_price.ToString());

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "material_price_after_discount", ldec_material_price_after_discount.ToString());

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "material_discount_percent", ldec_material_discount_percent.ToString());
                    //invuo_doc_item_search.ids_pack_details.SetItem(al_found_row, "packing_list_details_expiration_date", ldt_curr_expiration_date)		

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "sell_price", ldec_sell_price.ToString());

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "current_catalog_sell_price", ldec_sell_price.ToString());

                    ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();

                    if (!(invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<long>((int)al_found_row, "indicator") > 0))
                    {


                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "indicator", Convert.ToString(Convert.ToInt64(invuo_doc_item_searchProperty.ids_pack_details.Describe("Evaluate('MAX(indicator for all)',1)")) + 1));
                    }

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "supplier_number", ll_supplier_number.ToString());

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "mini_terminal", "1");

                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(al_found_row, "color", ll_order_material_color.ToString());
                }
            }
            return 1;
        }
        public async Task<long> wf_items_row_count() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_items_row_count
            //*Purpose:							This function will return the row count of dw_inv_pack_details 
            //*										in uo_mini_terminal_body.
            //*Arguments:						None 
            //*Return:								Long: the row count
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*12-11-2007		SHARP.				0.2.0  		B2B 			Initiation
            //************************************************************************************************


            return uo_body.dw_inv_pack_details.Rows.Count - 1;//.RowCount() - 1;
        }
        public long wf_get_item_doc_number(long al_material_number, ref long[] al_doc_number) //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_get_item_doc_number
            //*Purpose:						
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_material_number
            //*										Reference	Long					al_doc_number[] - Array
            //*
            //*Return:								Long
            //*
            //*Date 				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-08-2007		   SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Change the function to return a list of document numbers in an array
            //RETURN invuo_doc_item_search.uf_get_item_doc_number(al_material_number)
            return invuo_doc_item_searchProperty.uf_get_item_doc_number(al_material_number, ref al_doc_number);
            // End
        }
        public async Task<int> wf_get_doc_no_en(long al_row_no, long al_material_no) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_get_doc_no_en
            //*Purpose:							Returns the number of potintial documents related to the given item.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long						al_row_no
            //*										Value			Long						al_material_no
            //*
            //*Return:								Integer: number of documents.
            //*
            //*Date 				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*19-08-2008			SharonS					1.2.33.1		10012 			Initiation
            //************************************************************************************************

            long ll_cnt = 0;
            long ll_cnt2 = 0;
            long ll_docs = 0;
            long ll_cnt_docs = 0;
            long ll_cnt_b2b_docs = 0;
            long ll_cnt_non_b2b_docs = 0;
            long? ll_doc_no = null;
            long[] ll_doc_number = new long[2];
            long ll_doc_no_en = 0;
            int li_b2b_status = 0;
          //  bool lb_item_b2b_status = false;
            bool lb_found = false;
            char lc_state = '\0';
            ModelAction ldw_status = default(ModelAction);
            // Check if no rows or first item

            ll_docs = uo_header_dw_body.RowCount();
            if (ll_docs == 0)
            {
                return 2;
            }
            if (al_row_no == 1)
            {

                ldw_status = uo_header_dw_body.GetItemStatus((int)al_row_no, 0, ModelBuffer.Primary);
            }

            if (al_row_no == 1 && ldw_status == ModelAction.Insert)
            {
                return 2;
            }
            // If material number is not 0, check which docs and how many the material is related to.
            if (al_material_no != 0)
            {
                ll_cnt_docs = invuo_doc_item_searchProperty.uf_get_item_doc_number(al_material_no, ref ll_doc_number);
            }
            // If material is not related to any doc 
            if (ll_cnt_docs == 0)
            {
                // Check number of b2b and non b2b documents
                for (ll_cnt = 0; ll_cnt < ll_docs; ll_cnt++)
                {

                    li_b2b_status = (int)uo_header_dw_body.GetItemValue<long>(ll_cnt, "b2b_status");
                    // SharonS - 1.2.37.4 - 2008-11-18 - Task#10012 - SPUCM00000094 - Get State, only in Open state

                    lc_state = Convert.ToChar(uo_header_dw_body.GetItemValue<string>(ll_cnt, "state"));
                    if (lc_state.ToString() == "O")
                    {
                        if (li_b2b_status == 1)
                        {

                            ll_cnt_b2b_docs++;
                        }
                        else
                        {

                            ll_cnt_non_b2b_docs++;
                        }
                    }
                    // End
                }
            }
            else
            {
                for (ll_cnt = 0; ll_cnt < ll_docs; ll_cnt++)
                {

                    ll_doc_no = uo_header_dw_body.GetItemValue<Int64?>(ll_cnt, "number");
                    // SharonS - 1.2.37.12 - 2008-12-16 - Task#10012 - SPUCM00000094 - Get State, only in Open state

                    lc_state = Convert.ToChar(uo_header_dw_body.GetItemValue<string>(ll_cnt, "state"));
                    lb_found = false;
                    if (lc_state.ToString() == "O")
                    {
                        for (ll_cnt2 = 0; ll_cnt2 <= System.Extensions.CollectionExtensions.UBound(ll_doc_number); ll_cnt2++)
                        {
                            if (ll_doc_no == ll_doc_number[ll_cnt2])
                            {

                                li_b2b_status = (int)uo_header_dw_body.GetItemValue<Int64>(ll_cnt, "b2b_status");
                                // SharonS - 1.2.37.4 - 2008-11-18 - Task#10012 - SPUCM00000094 - Get State, only in Open state
                                //					lc_state = THIS.uo_header.dw_body.GetItem<string>(ll_cnt, "state")
                                //					IF lc_state = 'O' THEN
                                if (li_b2b_status == 1)
                                {

                                    ll_cnt_b2b_docs++;
                                }
                                else
                                {

                                    ll_cnt_non_b2b_docs++;
                                }
                                lb_found = true;
                                //					END IF
                                // End
                            }
                        }
                        if (!lb_found)
                        {

                            li_b2b_status = (int)uo_header_dw_body.GetItemValue<long>(ll_cnt, "b2b_status");
                            if (li_b2b_status == 1)
                            {

                                ll_cnt_b2b_docs++;
                            }
                            else
                            {

                                ll_cnt_non_b2b_docs++;
                            }
                        }
                    }
                }
            }
            if ((ll_cnt_b2b_docs + ll_cnt_non_b2b_docs == 1) || (ll_cnt_docs == 1 && ll_cnt_non_b2b_docs == 0))
            {
                ll_doc_no_en = 1;
            }
            else
            {
                ll_doc_no_en = 2;
            }
            return (int)ll_doc_no_en;
        }
        public async System.Threading.Tasks.Task<int> wf_set_items(string as_column_name, string as_data, long al_row) //**********************************************************************************************
        {
            uo_mini_terminal_pallet_manual uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:							w_mini_terminal
            //*Function/Event  Name:		wf_set_items
            //*Purpose:							For each column that is updated, if the row status is NewModified, 
            //*										(new item) set the relevant data. 
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------
            //*										Value 		string 					as_column_name
            //*										Value 		string 					as_data
            //*										Value 		Long	 					al_row
            //*		
            //*Return:								INTEGER :  -1 failed
            //*														1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14/10/2007		SHARP.    				0.2.0 			B2B			Initial version
            //***********************************************************************************************

            int? li_rtn_code = 0;
            int li_cng_doc_count = 0;
            int li_doc_no_en = 0;
            long ll_material_number = 0;
            long ll_old_material_number = 0;
            long ll_order_number = 0;
            long? ll_doc_no = 0;
            long? ll_orig_doc_no = 0;
            long ll_docs_count = 0;
            long[] ll_doc_number = null;
            long ll_material_serial_number_in_order = 0;
            long ll_next_serial_number = 0;
            long ll_rows = 0;
            long? ll_found_row = 0;
            long? ll_found_prev_row = 0;
            long ll_item_b2b_status = 0;
            long ll_min_valid_month = 0;
            long ll_count = 0;
            long ll_null = 0;
            decimal ld_material_quantity = default(decimal);
            string ls_barcode = null;
            string ls_find = null;
            string ls_import_type = null;
            string ls_message = null;
            char lc_doc_type = '\0';
            char? lc_orig_doc_type = null;
            bool lb_material_assigned_for_order = false;
            bool lb_is_item_b2b = false;
            bool lb_is_pre_item_b2b = false;
            bool lb_set_serial_number = false;
            DateTime? ldt_curr_expiration_date = null;
            ModelAction ldws_status = default(ModelAction);
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, Start. Row: " + al_row.ToString() + " Column: " + as_column_name + " Data: " + as_data + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            // Get material number from barcode 
            //ls_barcode = uo_body.dw_inv_pack_details.GetItem<string>(al_row, "barcode", Primary!, True)
            if (as_column_name == "barcode")
            {
                ls_barcode = as_data;
            }
            else
            {

                ls_barcode = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
            }
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, Start. Before uf_get_barcode_main" + " Barcode: " + ls_barcode + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            //////////////Penny 23-12-2007-Start
            // Get full barcode if it was particulaly inserted
            string ls_barcode_main = null;
            //ls_barcode_main = uo_body.iuo_material_search.uf_get_barcode_main(ls_barcode)
            // PninaSG - 1.2.23.0 - 2008-01-02- TASK#10000 - Start
            //ls_barcode_main = uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode)
            ls_barcode_main = await uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode, false);
            //END
            //ll_material_number = uo_body.iuo_material_search.uf_get_material_number(ls_barcode)
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, Start. Before uf_get_material_number" + " Barcode: " + ls_barcode_main + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            ls_barcode = ls_barcode_main;
            ll_material_number = await uo_body.iuo_material_search.uf_get_material_number(ls_barcode_main);
            //////////////Penny 23-12-2007-END 
            // Get row status

            ldws_status = uo_body.dw_inv_pack_details.GetItemStatus((int)al_row, 0, ModelBuffer.Primary);
            // Get current or new documet number 
            // SharonS - 1.2.36.0 -> 1.2.37.8- 2008-09-16 - Task#10012 - SPUCM00000094
            if (as_column_name == "doc_no")
            {
                if (!string.IsNullOrEmpty(as_data))
                {
                    ll_doc_no = Convert.ToInt64(as_data);
                }
                else
                {
                    ll_doc_no = 0;
                    as_data = "0";
                }
            }

            else if (ldws_status != ModelAction.Insert)
            {

                ll_doc_no = uo_body.dw_inv_pack_details.GetItemValue<long?>(al_row, "doc_no");
            }

            else if (ldws_status == ModelAction.Insert)
            {
                ll_docs_count = invuo_doc_item_searchProperty.uf_get_item_doc_number(ll_material_number, ref ll_doc_number);
                if (ll_docs_count >= 0 && ll_doc_number != null)
                {
                    ll_doc_no = ll_doc_number[0];
                    lc_doc_type = await uo_body.uf_get_doc_type(ll_doc_no.Value);
                }
            }
            //ll_doc_no = uo_body.dw_inv_pack_details.GetItem<int>(al_row, 'doc_no')
            if (ll_doc_no != null && ll_doc_no != 0)
            {
                lc_doc_type = await uo_body.uf_get_doc_type(ll_doc_no);
            }
            // Get item b2b status
            if (as_column_name == "barcode")
            {

                //uf_calculate_declines
                (uo_body.dw_inv_pack_details).SetItem(al_row, "barcode", as_data);
                // SharonS - 1.2.37.2 - 2008-11-09 - Task#10012 - SPUCM00000094 - SPUCM00000094
                //	lb_is_item_b2b = invuo_doc_item_search.uf_is_item_b2b(ll_material_number)
                lb_is_item_b2b = invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, Convert.ToInt64(ll_doc_no == null ? 0 : ll_doc_no.Value));
            }
            else if (as_column_name == "doc_no")
            {
                lb_is_item_b2b = invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_doc_no.Value);
                // End
            }
            else
            {

                ll_item_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row, "b2b_status");
                if (ll_item_b2b_status == 1)
                {
                    lb_is_item_b2b = true;
                }
            }
            // Get number of documets and documet type
            //IF (ldws_status <> NewModified! AND as_column_name <> 'barcode') OR lb_is_item_b2b THEN
            //	// SharonS - Change the function to return a list of document numbers in an array
            //	ll_docs_count = invuo_doc_item_search.uf_get_item_doc_number(ll_material_number, ll_doc_number[])
            //	IF ll_docs_count > 0 THEN 
            //		ll_doc_no = ll_doc_number[ll_docs_count]
            //		lc_doc_type = uo_body.uf_get_doc_type(ll_doc_no)
            //	END IF
            //END IF
            // End
            // If the row was updated (not new), find the item in the IRepository
            //ldws_status = uo_body.dw_inv_pack_details.GetItemStatus(al_row, 0, Primary!)
            //IF ldws_status <> NewModified! THEN 
            // SharonS - 1.2.36.0 -> 1.2.37.8- 2008-09-16 - Task#10012 - SPUCM00000094
            //IF ldws_status <> NewModified! OR lb_is_item_b2b THEN 

            if ((ldws_status != ModelAction.Insert && as_column_name != "barcode") || lb_is_item_b2b)
            {
                // End
                // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094
                //	ls_find = "materials_barcode = '" + ls_barcode + "'"
                ls_find = "material_number = " + ll_material_number.ToString();
                // End
                if (lc_doc_type.ToString() == "I")
                {

                    ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                    // SharonS - 1.2.36.0 -> 1.2.37.8- 2008-09-16 - Task#10012 - SPUCM00000094 - Find material by barcode and invoice number
                    ls_find += " and invoice_number == " + ll_doc_no.ToString();
                    // End

                    ll_found_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);
                }
                else if (lc_doc_type.ToString() == "P")
                {

                    ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                    // SharonS - 1.2.36.0 -> 1.2.37.8- 2008-09-16 - Task#10012 - SPUCM00000094 - Find material by barcode and packlist number
                    ls_find += " and pack_list_number == " + ll_doc_no.ToString();
                    // End

                    ll_found_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);
                }
                // SharonS - 1.2.36.0 -> 1.2.37.8- 2008-09-16 - Task#10012 - SPUCM00000094
            }

            else if ((ldws_status != ModelAction.Insert && as_column_name == "barcode") || lb_is_item_b2b)
            {
                if (uo_body.is_new_barcode != uo_body.is_old_barcode)
                {
                    // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094
                    //		ls_find = "materials_barcode = '" + uo_body.is_old_barcode + "'"
                    ll_old_material_number = await uo_body.iuo_material_search.uf_get_material_number(uo_body.is_old_barcode);
                    ls_find = "material_number == " + ll_old_material_number.ToString();
                    // End
                    if (lc_doc_type.ToString() == "I")
                    {

                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                        ls_find += " and invoice_number == " + ll_doc_no.ToString();

                        ll_found_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);
                    }
                    else if (lc_doc_type.ToString() == "P")
                    {

                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                        ls_find += " and pack_list_number == " + ll_doc_no.ToString();

                        ll_found_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);
                    }
                }
                // End
                // SharonS - 1.2.37.7 - 2008-11-30 - Task#10012 - SPUCM00000094
            }
            else
            {
                ls_find = "material_number == " + ll_material_number.ToString();
                if (ll_doc_no.Value != 0 && !(ll_doc_no.Value == null))
                {
                    if (lc_doc_type.ToString() == "I")
                    {

                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                        ls_find += " and invoice_number == " + ll_doc_no.ToString();

                        ll_found_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);
                    }
                    else if (lc_doc_type.ToString() == "P")
                    {

                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                        ls_find += " and pack_list_number == " + ll_doc_no.ToString();

                        ll_found_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);
                    }
                }
                // End	
            }
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, Start. Before CHOOSE CASE." + " Material number: " + ll_material_number.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End
            switch (as_column_name)
            {
                case "barcode":
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, CASE barcode Before uf_material_assigns_to_order." + " Material number: " + ll_material_number.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    ls_barcode = as_data;
                    // Check if item is assigned to the order
                    lb_material_assigned_for_order = uo_body.iuo_order_searchProperty.uf_material_assigns_to_order(ll_material_number);
                    // If yes, get the seleted order number, if no, set the order number to be 0 in the invoice/packlist table
                    if (lb_material_assigned_for_order)
                    {
                        (await uo_body.uf_get_selected_order_number(ll_order_number)).Retrieve(out ll_order_number);
                        ls_import_type = "o";
                    }

                    if (ldws_status == ModelAction.Insert && !lb_is_item_b2b)
                    {

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_number", ll_order_number); //.. order number

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(al_row, "import_type", ls_import_type); //.. order type	
                        await this.wf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");
                    }
                    else
                    {
                        if (lb_is_item_b2b)
                        {

                            //uf_calculate_declines
                            (uo_body.dw_inv_pack_details).SetItem(al_row, "b2b_status", 1);
                        }
                        if (lc_doc_type.ToString() == "I")
                        {

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_row.Value, "packing_list_number", ll_order_number.ToString());

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_row.Value, "import_type", Convert.ToString(ls_import_type));
                        }
                        else if (lc_doc_type.ToString() == "P")
                        {

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_row.Value, "order_number", ll_order_number.ToString());

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_row.Value, "import_type", Convert.ToString(ls_import_type));
                        }
                        // SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - SPUCM00000094 - Set order_number and import_type in the DW also

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_number", ll_order_number); //.. order number

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(al_row, "import_type", ls_import_type); //.. order type	
                        // End
                        // SharonS - 1.2.36.4 -> 1.2.37.8 - 2008-10-22 - Task#10012 - SPUCM00000094
                        if (ll_docs_count > 0)
                        {
                            await this.wf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "NEW");
                        }
                        else
                        {
                            // End
                            await this.wf_set_material_details(al_row, ls_barcode, lc_doc_type, ll_found_row.Value, "OLD");
                        }
                    }
                    break;
                case "expected_material_quantity":
                    // SharonS - 1.2.30.4 - 2008-04-28 - Set material expiration_date in dw_inv_pack_details.
                    //		ldt_curr_expiration_date = uo_body.dw_inv_pack_details.GetItem<DateTime>(al_row, "expiration_date")
                    // SharonS - 1.2.37.6 - 2008-11-23 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    //		IF ldws_status <> NewModified! OR lb_is_item_b2b THEN

                    if ((ldws_status != ModelAction.Insert || lb_is_item_b2b) && ll_found_row != -1)
                    {
                        if (lc_doc_type.ToString() == "I")
                        {

                            ldt_curr_expiration_date = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<DateTime?>((int)ll_found_row, "expiration_date");
                        }
                        else
                        {

                            ldt_curr_expiration_date = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<DateTime?>((int)ll_found_row, "expiration_date");
                        }
                    }
                    else
                    {
                        ldt_curr_expiration_date = uo_body.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "expiration_date");
                    }
                    // End
                    //if (ldt_curr_expiration_date == Convert.ToDateTime("01/01/1900").Add(Convert.ToDateTime("00:00:00").TimeOfDay))
                    if (ldt_curr_expiration_date == default(DateTime))
                    {

                        ldt_curr_expiration_date = null;
                    }

                    ll_min_valid_month = uo_body.dw_inv_pack_details.GetItemValue<long>(al_row, "min_valid_months");
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    string ls_curr_expiration_date = null;
                    ls_curr_expiration_date = Convert.ToString(ldt_curr_expiration_date);
                    if (ls_curr_expiration_date == null)
                    {
                        ls_curr_expiration_date = "";
                    }
                    ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, CASE expected_material_quantity. Before " + " Expiration date: " + ls_curr_expiration_date + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    // Vika 2012-02-23 SPUCM00003469 �� ���� ������ ���� ������� �������
                    bool lb_303_department = false;
                    lb_303_department = await uo_pallet_manual.uf_check_303_department(ll_material_number);
                    //IF NOT(IsNull(ll_min_valid_month) OR ll_min_valid_month = 0) AND IsNull(ldt_curr_expiration_date) THEN
                    if (!(ll_min_valid_month == null || ll_min_valid_month == 0) && (ldt_curr_expiration_date == default(DateTime) || ldt_curr_expiration_date == null || (lb_303_department)))
                    {
                        // End Vika 2012-02-23 SPUCM00003469 �� ���� ������ ���� ������� �������
                        // AlexKh - 1.2.46.0 - 2010-06-16 - CR#1138 - send material name instead row number
                        //IF NOT wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, al_row, FALSE) THEN

                        if (!((await wf_show_expiration_msg(ldt_curr_expiration_date, ll_min_valid_month, uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "material_name"), false)).Retrieve(out ldt_curr_expiration_date)))
                        {
                            //GalilCS - reset expected_material_quantity and raise CellKeyDown
                            uo_body.dw_inv_pack_details.SetItem(al_row, "expected_material_quantity", 0);
                            uo_body.dw_inv_pack_details.PerformCellkeyDown(new GridElementCellEventArgs(System.Web.VisualTree.Keys.Enter, 0, (int)al_row, as_column_name));
                            return -1;
                        }
                        else
                        {

                            if (ldws_status == ModelAction.Insert && !lb_is_item_b2b)
                            {

                                //uf_calculate_declines
                                (uo_body.dw_inv_pack_details).SetItem(al_row, "expiration_date", ldt_curr_expiration_date);
                            }
                            else
                            {
                                if (lc_doc_type.ToString() == "I")
                                {

                                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_row.Value, "expiration_date", ldt_curr_expiration_date.ToString());
                                }
                                else if (lc_doc_type.ToString() == "P")
                                {

                                    invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_row.Value, "expiration_date", ldt_curr_expiration_date.ToString());
                                }
                            }
                        }
                    }
                    // SharonS - 1.2.33.1 - 2008-08-19 - Task#10012 - SPUCM00000094 - Set expiration_date in DW also

                    //uf_calculate_declines
                    uo_body.dw_inv_pack_details.SetItem(al_row, "expiration_date", ldt_curr_expiration_date);
                    // End
                    li_rtn_code = await uo_body.uf_set_quantity_and_doc_no(as_data, al_row);
                    if (li_rtn_code < 1)
                    {
                        return -1;
                    }
                    break;
                case "doc_no":
                    // Get the document number
                    ll_doc_no = Convert.ToInt64(as_data);
                    // Get the document type (I - Invoice or P - Packlist)
                    lc_doc_type = await uo_body.uf_get_doc_type(ll_doc_no.Value);
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, CASE doc_no. Current doc Number: " + ll_doc_no.ToString() + " Doc type: " + lc_doc_type.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    // SharonS - 1.2.37.2 - 2008-11-10 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    if (lc_doc_type.ToString() != "I" && lc_doc_type.ToString() != "P")
                    {
                        return -1;
                    }
                    // End

                    ll_orig_doc_no = uo_body.dw_inv_pack_details.GetItemValue<long?>(al_row, as_column_name);
                    if (ll_orig_doc_no.Equals(null))
                    {
                        ll_orig_doc_no = default(long);
                    }
                    if (!(ll_orig_doc_no == 0))
                    {
                        lc_orig_doc_type = await uo_body.uf_get_doc_type(ll_orig_doc_no.Value);
                    }
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "Object:      " + "wf_set_items Function, CASE doc_no. Original doc Number: " + ll_orig_doc_no.ToString() + " Doc type: " + lc_orig_doc_type.ToString();
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    // SharonS - 1.2.37.2 - 2008-11-10 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    if (lc_orig_doc_type.ToString() == "E")
                    {
                        return -1;
                    }
                    // End
                    // Debug
                    List<ModelBase> lany_inv = null;
                    List<ModelBase> lany_pack = null;
                    List<ModelBase> lany_dis = null;

                    if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                    {
                        lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                    }

                    if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                    {


                        lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                    }

                    if (uo_body.dw_inv_pack_details.RowCount() > 0)
                    {


                        lany_dis = uo_body.dw_inv_pack_details.DataRows();
                    }
                    // Debug
                    // SharonS - 1.2.37.2 - 2008-11-10 - Task#10012 - SPUCM00000094 - SPUCM00000094
                    //		Long	ll_del_cnt
                    //		ll_del_cnt = invuo_doc_item_search.ids_inv_details.DeletedCount()
                    //		IF ll_del_cnt > 0 THEN
                    //			ll_rows = invuo_doc_item_search.ids_inv_details.RowCount()
                    //			invuo_doc_item_search.ids_inv_details.RowsMove(1, ll_del_cnt, Delete!, invuo_doc_item_search.ids_inv_details, ll_rows + 1, Primary!)
                    //		END IF	
                    //		ll_del_cnt = invuo_doc_item_search.ids_pack_details.DeletedCount()
                    //		IF ll_del_cnt > 0 THEN
                    //			ll_rows = invuo_doc_item_search.ids_pack_details.RowCount()
                    //			invuo_doc_item_search.ids_pack_details.RowsMove(1, ll_del_cnt, Delete!, invuo_doc_item_search.ids_pack_details, ll_rows + 1, Primary!)
                    //		END IF	

                    ll_null = 0;
                    ll_material_serial_number_in_order = uo_body.iuo_order_searchProperty.uf_get_order_material_serial_number(ll_material_number);
                    if (ll_doc_no.Value == ll_orig_doc_no || (ll_orig_doc_no == 0 || ll_orig_doc_no == 0))
                    {
                        //		���� ���� �� �����

                        if (ldws_status == ModelAction.Insert)
                        {
                            //			���� ����
                            if (!lb_is_item_b2b)
                            {

                                //uf_calculate_declines
                                (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                lb_set_serial_number = true;
                            }
                        }
                    }
                    else
                    {
                        //		���� ���� �����
                        lb_is_pre_item_b2b = invuo_doc_item_searchProperty.uf_is_item_b2b(ll_material_number, ll_orig_doc_no.Value);
                        if (lc_doc_type == lc_orig_doc_type || (lc_orig_doc_type == default(char) || string.IsNullOrEmpty(lc_orig_doc_type.ToString())))
                        {
                            //			��� ���� �� �����

                            if (ldws_status == ModelAction.Insert)
                            {
                                //				���� ����
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                            else
                            {
                                //				����  �����
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lc_doc_type.ToString() == "I")
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "mini_terminal", "0");
                                    }
                                    else
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and pack_list_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "mini_terminal", "0");
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {


                                    //uf_calculate_declines
                                    (uo_body.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity"));


                                    //uf_calculate_declines
                                    (uo_body.dw_inv_pack_details).SetItem(al_row, "material_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity"));
                                    if (lc_doc_type.ToString() == "I")
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_inv_details.Delete((int)ll_found_prev_row);
                                    }
                                    else
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and pack_list_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_pack_details.Delete((int)ll_found_prev_row);
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //			��� ���� �����

                            if (ldws_status == ModelAction.Insert)
                            {
                                //				���� ����
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                            }
                            else
                            {
                                //				����  �����
                                if (lb_is_pre_item_b2b)
                                {
                                    if (lc_orig_doc_type.ToString() == "I")
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "mini_terminal", "0");
                                    }
                                    else
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and pack_list_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "material_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "bonus_quantity", "0");

                                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found_prev_row.Value, "expected_material_quantity", ll_null.ToString());

                                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found_prev_row.Value, "mini_terminal", "0");
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                        lb_set_serial_number = true;
                                    }
                                }
                                else
                                {
                                    lb_set_serial_number = true;


                                    //uf_calculate_declines
                                    (uo_body.dw_inv_pack_details).SetItem(al_row, "invoice_pack_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity"));


                                    //uf_calculate_declines
                                    (uo_body.dw_inv_pack_details).SetItem(al_row, "material_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "expected_material_quantity"));
                                    if (lc_orig_doc_type.ToString() == "I")
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_inv_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and invoice_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_inv_details.Delete((int)ll_found_prev_row);
                                    }
                                    else
                                    {

                                        ll_rows = invuo_doc_item_searchProperty.ids_pack_details.RowCount();
                                        ls_find = "material_number == " + ll_material_number.ToString() + " and pack_list_number == " + ll_orig_doc_no.ToString();

                                        ll_found_prev_row = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, (int)ll_rows);

                                        invuo_doc_item_searchProperty.ids_pack_details.Delete((int)ll_found_prev_row);
                                    }
                                    if (lb_is_item_b2b)
                                    {
                                    }
                                    else
                                    {

                                        //uf_calculate_declines
                                        (uo_body.dw_inv_pack_details).SetItem(al_row, "order_serial", ll_material_serial_number_in_order);
                                    }
                                }
                            }
                        }
                    }
                    if (lb_set_serial_number)
                    {
                        ll_next_serial_number = await uo_body.uf_get_max_serial_number(ll_doc_no.Value);

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(al_row, "serial_number", ll_next_serial_number);
                    }
                    // End
                    // Debug

                    if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                    {


                        lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                    }

                    if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                    {


                        lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                    }

                    if (uo_body.dw_inv_pack_details.RowCount() > 0)
                    {


                        lany_dis = uo_body.dw_inv_pack_details.DataRows();
                    }
                
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function, CASE doc_no. End of CASE" + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End	
                    break;
                default:
                    break;
            }
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_set_items - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_set_items Function. End of wf_set_items" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End	
            return 1;
        }
        public async System.Threading.Tasks.Task<bool> wf_update_item_new(long al_row_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								wf_mini_terminal
            //*Function/Event  Name:			wf_update_item
            //*Purpose:							
            //*
            //*Arguments:						
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										value 		long						al_row_number
            //*		
            //*Return:								BOOLEAN   - TRUE  - Success
            //*													   FALSE - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-10-2007    SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            bool lb_concatenate = false;
            char lc_doc_type = '\0';
            char lc_old_doc_type = default(char);
            string ls_barcode = null;
            string ls_material_name = null;
            string ls_find = null;
            string ls_old_barcode = null;
            string ls_error_text = null;
            string ls_last_sql_err_text = null;
            string ls_message = null;
            string ls_barcode_main = null;
            decimal ld_expected_material_quantity = default(decimal);
            decimal ld_expected_material_quantity_temp = default(decimal);
            decimal ld_order_quantity = default(decimal);
            decimal ld_invoice_pack_quantity = default(decimal);
            decimal ld_total = default(decimal);
            decimal ld_material_price = default(decimal);
            decimal ld_material_price_after_discount = default(decimal);
            decimal ld_material_discount_percent = default(decimal);
            decimal ld_sell_price = default(decimal);
            decimal ld_bonus_quantity = default(decimal);
            decimal ld_inv_pack_quantity = default(decimal);
            decimal ld_material_quantity = default(decimal);
            long ll_doc_no = 0;
            long ll_rows_count = 0;
            long? ll_found = 0;
            long ll_found2 = 0;
            long ll_rtn_code = 0;
            long ll_logbook = 0;
            long ll_supplier_number = 0;
            long ll_order_num = 0;
            long ll_material_number = 0;
            long ll_result = 0;
            long ll_error_number = 0;
            long ll_update_status = 0;
            long ll_last_sql_db_code = 0;
            long ll_row_no = 0;
            long ll_cntr = 0;
            long ll_rowCount = 0;
            long ll_rows_retrieved = 0;
            long ll_min_valid_month = 0;
            long ll_order_material_color = 0;
            DateTime? ldt_supply_date = default(DateTime);
            DateTime? ldt_curr_expiration_date = default(DateTime);
            int li_b2b_status = 0;
            int li_rtn_code = 0;
            ModelAction ldwis_doc = default(ModelAction);
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
            ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "wf_update_item Function, Start. Row:         " + al_row_number.ToString() + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            // End

            ll_row_no = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "row_no");

            ls_barcode = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "barcode");

            ls_material_name = uo_body.dw_inv_pack_details.GetItemValue<string>(al_row_number, "material_name");

            ld_expected_material_quantity = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "expected_material_quantity");

            ld_order_quantity = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "order_quantity");

            ld_invoice_pack_quantity = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "invoice_pack_quantity");

            ll_doc_no = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "doc_no");

            li_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "b2b_status");

            ll_material_number = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "material_number");

            ld_material_price = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "material_price");
            // SharonS - 1.2.36.4 - 2008-10-22 - Task#10012 - Set material details

            ld_material_price_after_discount = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "material_price_after_discount");

            ld_material_discount_percent = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "material_discount_percent");

            ll_min_valid_month = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "min_valid_months");

            ldt_curr_expiration_date = uo_body.dw_inv_pack_details.GetItemValue<DateTime>(al_row_number, "expiration_date");

            ld_sell_price = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "sell_price");

            ll_order_material_color = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "color");
            // AlexKh - 1.2.48.34 - 2012-12-05 - SPUCM00003402 - check if negative value, then set zero
            if ((ld_expected_material_quantity) < 0)
            {
                ld_expected_material_quantity = 0;
            }
            // Set supplier number and order number in u_material_update non-visual onject
            (await wf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            iuo_material_updateProperty.uf_set_supplier_number(ll_supplier_number);
            (await this.wf_get_selected_order_no(ll_order_num)).Retrieve(out ll_order_num);
            if (ll_order_num == 0)
            {
                ll_order_num = 0;
            }
            iuo_material_updateProperty.uf_set_order_number(ll_order_num);
            // Load material_suppliers_connect data for the material
            await iuo_material_updateProperty.uf_load_material_suppliers_connect_new(ll_material_number);
            // Load material_suppliers data for the material
            await iuo_material_updateProperty.uf_load_material_suppliers_new(ll_material_number, ld_material_price);
            // Update material_suppliers_connect & material_suppliers
            iuo_material_updateProperty.uf_update_new(ref ll_error_number, ref ls_error_text);
            // Update order state in supplier_order_move
            uo_body.iuo_order_searchProperty.uf_set_order_state("P");
            // Update quantity_within_invoice in supplier_order_details
            uo_body.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ld_expected_material_quantity);
            lc_doc_type = await this.wf_get_doc_type(ll_doc_no);
            if (uo_body.il_old_doc_no != 0)
            {
                lc_old_doc_type = await this.wf_get_doc_type(uo_body.il_old_doc_no);
            }
            // SharonS - 1.2.36.0 - 2008-07-27 - Task#10012 - If doc type is 'E' - doc number is not valid
            if (lc_doc_type.ToString() == "E")
            {
                return false;
            }
            // End
            // Debug
            List<ModelBase> lany_inv = null;
            List<ModelBase> lany_pack = null;
            List<ModelBase> lany_dis = null;
            // Debug
            if (lc_doc_type.ToString() == "I")
            {
                // Invoice
                //In case this is the first item for its doc save the doc
                // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                // End
                ls_find = "number == " + ll_doc_no.ToString();

                ll_rows_count = uo_header_dw_body.RowCount();
                if (ll_rows_count < 1)
                {
                    await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                    return false;
                }

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_count);
                if (ll_found >= 0)
                {


                    ldwis_doc = uo_header_dw_body.GetItemStatus((int)ll_found, 0, ModelBuffer.Primary);

                    if (ldwis_doc == ModelAction.Insert)
                    {
                        // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update header. found row: " + ll_found.ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End




                        ll_rtn_code = uo_header_dw_body.RowsCopy((int)ll_found, (int)ll_found, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_move.Repository(), invuo_doc_item_searchProperty.ids_inv_move.RowCount() + 1, ModelBuffer.Primary);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_update_status = invuo_doc_item_searchProperty.ids_inv_move.Update(unitOfWork);
                        }

                        if (ll_update_status < 0)
                        {
                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                            return false;
                        }
                        else
                        {



                            uo_header_dw_body.SetItemStatus((int)ll_found, 0, ModelBuffer.Primary, ModelAction.None);
                        }
                    }
                }
                //Details


                ldwis_doc = uo_body.dw_inv_pack_details.GetItemStatus((int)al_row_number, 0, ModelBuffer.Primary);

                if (ldwis_doc == ModelAction.Insert)
                {
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, row status = NewModified!. Invoice No: " + ll_doc_no.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // End
                    ls_barcode_main = await uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode, false);
                    ls_find = "materials_barcode = '" + ls_barcode_main + "'" + " and invoice_number = " + ll_doc_no.ToString();
                    // Debug

                    if (uo_body.dw_inv_pack_details.RowCount() > 0)
                    {


                        lany_dis = uo_body.dw_inv_pack_details.DataRows();
                    }

                    if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                    {


                        lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                    }
                    // Debug


                    ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                    if (ll_found < -1)
                    {
                        // ����� ������
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    else if (ll_found >= 0)
                    {
                        // ����� �������� �� ����

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());

                        li_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "b2b_status");
                        // Concatenate quantity

                        ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        if (ld_expected_material_quantity_temp == 0)
                        {
                            ld_expected_material_quantity_temp = 0;
                        }
                        ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());

                        ld_bonus_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                        ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                        if (li_b2b_status == 1)
                        {
                            if (ld_expected_material_quantity == ld_inv_pack_quantity)
                            {

                                invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                            }
                        }
                        // Set material details

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "mini_terminal", "1");

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_price", ld_material_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_price_after_discount", ld_material_price_after_discount.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_discount_percent", ld_material_discount_percent.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "materials_min_valid_months", ll_min_valid_month.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "current_catalog_sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "color", ll_order_material_color.ToString());
                        // End

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }

                    }
                    else
                    {




                        ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {


                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_inv_details.RowCount() > 0)
                        {


                            lany_inv = invuo_doc_item_searchProperty.ids_inv_details.GetDataRows();
                        }
                        // Debug
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }
                    }
                    // Check if Update was faild, and update log
                    ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                    ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:      " + "Update Invoice Details, found row: " + ll_found.ToString() + "\r" + "\n" + "Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    if (ll_rtn_code < 0)
                    {
                        return false;
                    }
                }
                else
                {


                    ll_doc_no = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "doc_no");
                    ls_find = "materials_barcode == \"" + uo_body.is_new_barcode + "\"";
                    ls_find += " and invoice_number == " + ll_doc_no.ToString();


                    ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                    if (ll_found < -1)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    else if (ll_found == -1)
                    {




                        ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }

                    }
                    else
                    {
                        if (lc_old_doc_type == lc_doc_type || string.IsNullOrEmpty(lc_old_doc_type.ToString()))
                        {
                            //29-JAN-2008-START

                            ld_bonus_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                            ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_inv_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                            if (li_b2b_status == 1)
                            {
                                if (ld_expected_material_quantity == ld_inv_pack_quantity)
                                {

                                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                    invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                                }
                            }
                            //29-JAN-2008-END




                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary);

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                            }

                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                            ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, row status <> NewModified!, doc type not changed. Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                            SystemHelper.WriteToLog(ls_message);
                        }
                        else
                        {
                            //In this case: lc_old_doc_type <> lc_doc_type THEN
                            //ls_find = "pack_list_number = " + STRING(uo_body.il_old_doc_no) 
                            ls_find = "materials_barcode == \"" + uo_body.is_new_barcode + "\"";
                            ls_find += "AND pack_list_number == " + uo_body.il_old_doc_no.ToString();


                            ll_found = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_pack_details.RowCount());
                            if (ll_found < 0)
                            {
                                await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                                return false;
                            }

                            invuo_doc_item_searchProperty.ids_pack_details.Delete((int)ll_found);

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                            }
                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                            ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList Details, doc type was changed. Return code: " + ll_last_sql_db_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                            SystemHelper.WriteToLog(ls_message);
                            // End

                            invuo_doc_item_searchProperty.ids_inv_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());




                            ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_inv_details.RowCount() + 1, ModelBuffer.Primary);

                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                ll_rtn_code = invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                            }


                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                            ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update Invoice Details, doc type was changed. Return code: " + ll_rtn_code.ToString() + " Error: " + ls_last_sql_err_text + "\r" + "\n";
                            SystemHelper.WriteToLog(ls_message);
                            // End
                        }
                    }
                }
            }
            else if (lc_doc_type.ToString() == "P")
            {
                ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Doc type:  " + lc_doc_type.ToString() + " PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                SystemHelper.WriteToLog(ls_message);
                ls_find = "number == " + ll_doc_no.ToString();

                ll_rows_count = uo_header_dw_body.RowCount();
                if (ll_rows_count < 1)
                {
                    await wf_show_message("wf_update_doc Error", "", "OK", ".�� ���� ����� �� ��������");
                    return false;
                }

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_count);
                if (ll_found >= 0)
                {


                    ldwis_doc = uo_header_dw_body.GetItemStatus((int)ll_found, 0, ModelBuffer.Primary);

                    if (ldwis_doc == ModelAction.Insert)
                    {
                        ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList header. Doc type:  " + "PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                        SystemHelper.WriteToLog(ls_message);
                        // End




                        ll_rtn_code = uo_header_dw_body.RowsCopy((int)ll_found, (int)ll_found + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_pack_move.Repository(), invuo_doc_item_searchProperty.ids_pack_move.RowCount() + 1, ModelBuffer.Primary);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_update_status = invuo_doc_item_searchProperty.ids_pack_move.Update(unitOfWork);
                        }

                        if (ll_update_status < 0)
                        {
                            ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_db_code();
                            ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_move.uf_get_last_sql_err_text();
                            return false;
                        }
                        else
                        {



                            uo_header_dw_body.SetItemStatus((int)ll_found, 0, ModelBuffer.Primary, ModelAction.None);
                        }
                    }
                }
                //Details


                ldwis_doc = uo_body.dw_inv_pack_details.GetItemStatus((int)al_row_number, 0, ModelBuffer.Primary);
                // Debug

                if (uo_body.dw_inv_pack_details.RowCount() > 0)
                {


                    lany_dis = uo_body.dw_inv_pack_details.DataRows();
                }

                if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                {


                    lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                }
                // Debug

                if (ldwis_doc == ModelAction.Insert)
                {
                    //Check if concatenate is needed
                    // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Update log
                    ls_message = "*************************** wf_update_item - w_mini_terminal*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Act:      " + "Update PackList Details, row status = NewModified!, Doc type:  " + "PackList No: " + ll_doc_no.ToString() + "\r" + "\n";
                    SystemHelper.WriteToLog(ls_message);
                    // Get main barcode number
                    ls_barcode_main = await uo_body.iuo_material_search.uf_get_barcode_main(uo_body.is_new_barcode, false);
                    ls_find = "materials_barcode == \"" + ls_barcode_main + "\" and pack_list_number == " + ll_doc_no.ToString();


                    ll_found = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_pack_details.RowCount());
                    if (ll_found < -1)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    else if (ll_found > -1)
                    {
                        // Set indicator

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());
                        //concatenate is needed

                        ld_expected_material_quantity_temp = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        if (ld_expected_material_quantity_temp == 0)
                        {
                            ld_expected_material_quantity_temp = 0;
                        }

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", Convert.ToString(ld_expected_material_quantity.ToString() + ld_expected_material_quantity_temp.ToString()));

                        li_b2b_status = uo_body.dw_inv_pack_details.GetItemValue<int>(al_row_number, "b2b_status");
                        ld_expected_material_quantity = ld_expected_material_quantity + ld_expected_material_quantity_temp;

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", ld_expected_material_quantity.ToString());
                        uo_body.iuo_order_searchProperty.uf_set_item_quantity_within_invoice(ll_material_number, ld_expected_material_quantity);

                        ld_expected_material_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //ld_material_quantity = invuo_doc_item_search.ids_inv_details.GetItem<int>(ll_found, "material_quantity")

                        ld_bonus_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                        ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                        if (li_b2b_status == 1)
                        {
                            if (ld_expected_material_quantity == ld_inv_pack_quantity)
                            {

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                            }
                        }
                        //PninaSG - 2008-01-29 - End
                        // SharonS - 1.2.36.4 - 2008-10-22 - Task#10012 - Set material details

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "mini_terminal", "1");

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_price", ld_material_price.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_price_after_discount", ld_material_price_after_discount.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_discount_percent", ld_material_discount_percent.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "materials_min_valid_months", ll_min_valid_month.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expiration_date", ldt_curr_expiration_date.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "current_catalog_sell_price", ld_sell_price.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "color", ll_order_material_color.ToString());
                        // End

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                    }
                    else
                    {




                        ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                    }
                }
                else
                {
                    ls_find = "materials_barcode == \"" + uo_body.is_new_barcode + "\"";
                    ls_find += " and pack_list_number == " + ll_doc_no.ToString();


                    ll_found = invuo_doc_item_searchProperty.ids_pack_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_pack_details.RowCount());
                    if (ll_found < -1)
                    {
                        await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                        return false;
                    }
                    if (lc_old_doc_type == lc_doc_type || string.IsNullOrEmpty(lc_old_doc_type.ToString()))
                    {

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "materials_barcode", Convert.ToString(ls_barcode));

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", ld_expected_material_quantity.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "order_quantity", ld_order_quantity.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", ld_invoice_pack_quantity.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "pack_list_number", ll_doc_no.ToString());

                        invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "indicator", ll_row_no.ToString());
                        //PninaSG - 2008-01-29 - Start

                        ld_expected_material_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "expected_material_quantity");
                        //ld_material_quantity = invuo_doc_item_search.ids_pack_details.GetItem<int>(ll_found, "material_quantity")

                        ld_bonus_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "bonus_quantity");

                        ld_inv_pack_quantity = invuo_doc_item_searchProperty.ids_pack_details.GetItemValue<decimal>((int)ll_found, "inv_pack_quantity");
                        if (li_b2b_status == 1)
                        {
                            if (ld_expected_material_quantity == ld_inv_pack_quantity)
                            {

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "bonus_quantity", ld_bonus_quantity.ToString());

                                invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "material_quantity", (ld_expected_material_quantity - ld_bonus_quantity).ToString());
                            }
                        }
                        //PninaSG - 2008-01-29 - End
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {


                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                        {


                            lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                        }
                        // Debug
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }

                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                    }
                    else
                    {
                        //In this case: lc_old_doc_type <> lc_doc_type THEN
                        //ls_find = "invoice_number = " + STRING(uo_body.il_old_doc_no) 
                        ls_find = "materials_barcode == \"" + uo_body.is_new_barcode + "\"";
                        ls_find += "AND invoice_number == " + uo_body.il_old_doc_no.ToString();


                        ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                        if (ll_found < 0)
                        {
                            await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                            return false;
                        }
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {


                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                        {


                            lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                        }
                        // Debug

                        invuo_doc_item_searchProperty.ids_inv_details.Delete((int)ll_found);

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            invuo_doc_item_searchProperty.ids_inv_details.Update(unitOfWork);
                        }
                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_inv_details.uf_get_last_sql_err_text();
                        // SharonS - 1.2.36.0 - 2008-07-27 - Task#10012 - Check if the item exists already in the new doc and concatonation is needed.
                        //			ll_rtn_code = uo_body.dw_inv_pack_details.RowsCopy(al_row_number, al_row_number, Primary!, invuo_doc_item_search.ids_pack_details, invuo_doc_item_search.ids_pack_details.RowCount() + 1, Primary!)
                        ls_find = "materials_barcode == \"" + uo_body.is_new_barcode + "\"";
                        ls_find += "AND invoice_number == " + uo_body.il_new_doc_no.ToString();


                        ll_found = invuo_doc_item_searchProperty.ids_inv_details.Find(ls_find, 0, invuo_doc_item_searchProperty.ids_inv_details.RowCount());
                        if (ll_found < -1)
                        {
                            await wf_show_message("wf_update_item Error", "", "OK", ".�� ���� ����� �� ��������");
                            return false;
                        }
                        else if (ll_found == 0)
                        {
                            ll_rtn_code = Convert.ToInt64(uo_body.dw_inv_pack_details.RowsCopy(Convert.ToInt32(al_row_number), Convert.ToInt32(al_row_number) + 1, ModelBuffer.Primary, invuo_doc_item_searchProperty.ids_inv_details, invuo_doc_item_searchProperty.ids_pack_details.RowCount() + 1, ModelBuffer.Primary));
                        }
                        else if (ll_found > 0)
                        {

                            invuo_doc_item_searchProperty.ids_pack_details.SetItem(ll_found.Value, "expected_material_quantity", Convert.ToString(ld_expected_material_quantity.ToString() + ld_expected_material_quantity_temp.ToString()));
                        }
                        // End
                        // Debug

                        if (uo_body.dw_inv_pack_details.RowCount() > 0)
                        {


                            lany_dis = uo_body.dw_inv_pack_details.DataRows();
                        }

                        if (invuo_doc_item_searchProperty.ids_pack_details.RowCount() > 0)
                        {


                            lany_pack = invuo_doc_item_searchProperty.ids_pack_details.GetDataRows();
                        }
                        // Debug

                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            ll_rtn_code = invuo_doc_item_searchProperty.ids_pack_details.Update(unitOfWork);
                        }


                        ll_last_sql_db_code = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_db_code();
                        ls_last_sql_err_text = invuo_doc_item_searchProperty.ids_pack_details.uf_get_last_sql_err_text();
                    }
                }
            }



            IRepository lds_mini_terminal_header_inv_pack;




            lds_mini_terminal_header_inv_pack = new d_mini_terminal_header_inv_packRepository();




            uo_header_dw_body.RowsCopy(1, uo_header_dw_body.RowCount(), ModelBuffer.Primary, lds_mini_terminal_header_inv_pack, 1, ModelBuffer.Primary);
            li_rtn_code = await wf_retrieve_data();
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
            //IF li_rtn_code < 1 THEN RETURN FALSE
            if (li_rtn_code < 1)
            {
                if (lds_mini_terminal_header_inv_pack != null)
                {


                }
                return false;
            }
            // End

            ll_rowCount = lds_mini_terminal_header_inv_pack.RowCount();

            ll_rows_retrieved = uo_header_dw_body.RowCount();
            for (ll_cntr = 1; ll_cntr <= ll_rowCount; ll_cntr++)
            {
                ll_doc_no = Convert.ToInt64(lds_mini_terminal_header_inv_pack.GetItem<d_mini_terminal_header_inv_pack>((int)ll_cntr).number);
                ls_find = "number == " + ll_doc_no.ToString();

                ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_rows_retrieved);
                if (ll_found == -1)
                {




                    lds_mini_terminal_header_inv_pack.RowsCopy(Convert.ToInt32(ll_cntr), Convert.ToInt32(ll_cntr) + 1, ModelBuffer.Primary, uo_header_dw_body.GetDataObject(), uo_header_dw_body.RowCount() + 1, ModelBuffer.Primary);
                    //GalilCS - force update layout
                    uo_header_dw_body.SetRepository(uo_header_dw_body.Repository());

                    // SharonS - 1.2.36.0 - 2008-07-27 - Task#10012 - Check if there is a non b2b doc
                    long ll_item = 0;
                    long ll_doc_no_en = 0;

                    for (ll_item = 0; ll_item < uo_body.dw_inv_pack_details.RowCount(); ll_item++)
                    {

                        ll_doc_no_en = uo_body.dw_inv_pack_details.GetItemValue<Int64>(ll_item, "doc_no_en");

                        //uf_calculate_declines
                        (uo_body.dw_inv_pack_details).SetItem(ll_item, "doc_no_en", ll_doc_no_en + 1);
                    }

                    //uf_calculate_declines
                    (uo_body.dw_inv_pack_details).ResetUpdate();
                    // End
                }
            }
            // SharonS - 1.2.30.19 - 2008-07-02- CR#1096 - Add Destroy
            if (lds_mini_terminal_header_inv_pack != null)
            {


            }
            // End
            if (!await uo_body.uf_is_new_row_exist(uo_body.dw_inv_pack_details))
            {

                //uf_calculate_declines
                await uo_body.uf_new_row(uo_body.dw_inv_pack_details);
            }
            // Initiate instances
            uo_body.is_new_barcode = "";
            uo_body.is_old_barcode = "";
            uo_body.il_old_doc_no = 0;
            // Set summery data
            await wf_retrieve_dw_summary();
            return true;
        }
        public async Task<int> wf_body_new_row() //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_body_new_row
            //*Purpose:							Call the function uf_new_row in the uo_body object.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Integer: 1  - succeeded.
            //*												   -1 - failed.
            //*Date 			Programer		Version		Task#						Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*11-02-2009    SharonS.			1.2.42.1		SPUCM00000861 		Initiation
            //************************************************************************************************

            bool lb_new_row = false;
            lb_new_row = await uo_body.uf_is_new_row_exist(uo_body.dw_inv_pack_details);
            if (lb_new_row)
            {
                return 1;
            }
            else
            {
                var res = await uo_body.uf_new_row(uo_body.dw_inv_pack_details);
                GridElementExtensions.SetDataSourceEx(uo_body.dw_inv_pack_details, uo_body.dw_inv_pack_details.Repository().GetDataTable());
                return res;

            }
        }
        public async Task<int> wf_is_item_in_invoice(long al_doc_number, long al_material_number) // AlexKh - 1.2.43.1 - 2009-04-01 - SPUCM00000950 - check if item exist for specific document
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            long? ll_found = 0;
            long ll_b2b_status = 0;
            string ls_find = null;
            string ls_rows = null;
            decimal ldec_item_pirce = default(decimal);
            ls_find = "number == " + al_doc_number.ToString();
            //Check if invoice is B2B


            ll_found = uo_header_dw_body.Find(ls_find, 0, uo_header_dw_body.RowCount());
            if (ll_found >= 0)
            {

                ll_b2b_status = uo_header_dw_body.GetItemValue<long>(ll_found.Value, "b2b_status");
            }
            //Check if item exist for B2B invoice
            if (ll_b2b_status == 1)
            {
                invuo_doc_item_searchProperty.uf_get_doc_item_price(al_doc_number, al_material_number, ref ldec_item_pirce);
                if (ldec_item_pirce == -1)
                {
                    return -1;
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public async System.Threading.Tasks.Task<bool> wf_copy_item_2_delete_table(long al_row) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:				w_mini_terminal
            //*Function Name:	wf_copy_item_2_delete_table
            //*Purpose: 			Insert a row into the ids_invpack_deleted_items DS.
            //*Arguments: 		Long - al_row - Row number in uo_body.dw_inv_pack_details.
            //*Return:				Boolean - TRUE - Success to Insert the row.
            //*									 FALSE - Failed to Insert the row.
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*12-05-2009		SharonS			1.2.44.1	SPUCM00000075		Initial version
            //************************************************************************************************

            long ll_new_row = 0;
            long? ll_found = 0;
            long ll_supplier = 0;
            long ll_order_number = 0;
            long? ll_doc_number = 0;
            long ll_docs = 0;
            long ll_count = 0;
            long ll_serial_number = 0;
            long ll_material_number = 0;
            long ll_batch_count = 0;
            long ll_batch_row = 0;
            string ls_find = null;
            char lc_doc_type = '\0';

            ll_new_row = ids_invpack_deleted_itemsProperty.Insert(0);
            if (ll_new_row < 0)
            {
                return false;
            }
            //ids_invpack_deleted_items.SetItem(ll_new_row, "branch_number", gs_vars.branch_number)
            //ids_invpack_deleted_items.SetItem(ll_new_row, "station_number", gs_station.station_number)
            //ids_invpack_deleted_items.SetItem(ll_new_row, "delete_point", 2)
            //ids_invpack_deleted_items.SetItem(ll_new_row, "del_emp_number", gs_vars.active_owner)
            //IF ib_return_mode THEN
            //ids_invpack_deleted_items.SetItem(ll_new_row, "doc_type", "r")
            //ELSE
            //ids_invpack_deleted_items.SetItem(ll_new_row, "doc_type", "p")
            //END IF
            // Header

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "delete_option", "1");
            (await wf_get_selected_supplier_number(ll_supplier)).Retrieve(out ll_supplier); ;

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_number", ll_supplier.ToString());
            (await wf_get_selected_order_no(ll_order_number)).Retrieve(out ll_order_number);

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "order_number", ll_order_number.ToString());

            ll_doc_number = uo_body.dw_inv_pack_details.GetItemValue<long?>(al_row, "doc_no");
            lc_doc_type = await wf_get_doc_type(ll_doc_number);
            if (lc_doc_type.ToString() == "I")
            {

                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_name", "Invoice");
            }
            else
            {

                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_name", "PackList");
            }

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_number", ll_doc_number);

            ll_docs = uo_header_dw_body.RowCount();
            ls_find = "number == " + Convert.ToString(Microsoft.VisualBasic.Conversion.Val(ll_doc_number)) + " and doc_type == \"" + lc_doc_type.ToString() + "\"";

            ll_found = uo_header_dw_body.Find(ls_find, 0, (int)ll_docs);


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "employee_number", uo_header_dw_body.GetItemValue<Int64>(ll_found.Value, "employee_number").ToString());


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "move_datetime", uo_header_dw_body.GetItemValue<DateTime>((int)ll_found, "date_move").ToString());


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_datetime", uo_header_dw_body.GetItemValue<DateTime>((int)ll_found, "supply_date").ToString());


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "state", Convert.ToString(uo_header_dw_body.GetItemValue<string>(ll_found.Value, "state")));

            if (uo_header_dw_body.GetItemValue<Int64>(ll_found.Value, "b2b_status") == 1)
            {

                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "b2b_status", "1");
            }
            else
            {

                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "b2b_status", "0");
            }


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "doc_total_amount", uo_header_dw_body.GetItemValue<decimal>(ll_found.Value, "expected_total_amount").ToString());


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "discount_percent", uo_header_dw_body.GetItemValue<decimal>(ll_found.Value, "discount_percent").ToString());
            // Item

            ll_serial_number = uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "serial_number");

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "serial_number", ll_serial_number.ToString());

            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "item_state", uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "state"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "last_update_datetime", uo_body.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "last_update_datetime"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "indicator", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "indicator"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "packing_list_number", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "order_number"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "packing_serial", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "order_serial"));
            if (lc_doc_type.ToString() == "I")
            {


                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "import_type", uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "import_type"));
            }


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "color", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "color"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "mini_terminal", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "mini_terminal"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "expiration_date", uo_body.dw_inv_pack_details.GetItemValue<DateTime?>(al_row, "expiration_date"));
            //ids_invpack_deleted_items.SetItem(ll_new_row, "out_of_stock_datetime", uo_body.dw_inv_pack_details.GetItem<int>(al_row, ""))


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_row", uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "bonus_row"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_bonus_connect", uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_bonus_connect"));
            if (lc_doc_type.ToString() == "I")
            {

                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_within_invoice", "0");
            }
            else
            {


                ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_within_invoice", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "quantity_within_invoice"));
            }


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "expected_material_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal?>(al_row, "expected_material_quantity"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_quantity"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_quantity", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "bonus_quantity"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_price", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_price"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "supplier_discount_percent", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "supplier_discount_percent"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_discount_percent", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_discount_percent"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_discount_amount", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_discount_amount"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "bonus_discount", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "bonus_discount"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "material_price_after_disc", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "material_price_after_discount"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "sell_price", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "sell_price"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "current_catalog_sell_price", uo_body.dw_inv_pack_details.GetItemValue<decimal>(al_row, "current_catalog_sell_price"));


            ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "details", uo_body.dw_inv_pack_details.GetItemValue<string>(al_row, "details"));
            // Batch

            ll_material_number = uo_body.dw_inv_pack_details.GetItemValue<Int64>(al_row, "material_number");
            if (true)
            {
                this.LoadData(ll_serial_number, ll_material_number, ref ll_count);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await wf_show_message("wf_copy_item_2_delete_table", "DB Error", "OK", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                return false;
            }
            if (ll_count > 0)
            {
                if (al_row != 0)
                {
                    if (!(ids_material_batchsProperty != null))
                    {
                        ids_material_batchsProperty = new d_upd_material_batch_argRepository("", "", "", "", "");

                    }

                    ll_batch_count = ids_material_batchsProperty.Retrieve(ll_serial_number, masofonAlias.masofon.Instance.gs_vars.branch_number, ll_material_number);
                    for (ll_batch_row = 1; ll_batch_row <= ll_batch_count; ll_batch_row++)
                    {


                        ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "batch_number", Convert.ToString(ids_material_batchsProperty.GetItemValue<string>(ll_batch_row, "batch_number")));


                        ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "recieved_date", ids_material_batchsProperty.GetItemValue<DateTime>((int)ll_batch_row, "recieved_date").ToString());


                        ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "batch_expiry_date", ids_material_batchsProperty.GetItemValue<DateTime>((int)ll_batch_row, "expiry_date").ToString());


                        ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_in", ids_material_batchsProperty.GetItemValue<int>((int)ll_batch_row, "quantity_in").ToString());


                        ids_invpack_deleted_itemsProperty.SetItem(ll_new_row, "quantity_out", ids_material_batchsProperty.GetItemValue<int>((int)ll_batch_row, "quantity_out").ToString());
                    }

                    ids_material_batchsProperty.Reset();
                }
                this.UpdateData(ll_serial_number, ll_material_number);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await wf_show_message("wf_copy_item_2_delete_table", "DB Error", "OK", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    return false;
                }
            }
            //Long	ll_ret
            //dw_3.SetTransObject(SQLCA)
            //ll_ret = ids_invpack_deleted_items.RowsCopy(1, 1, Primary!	, dw_3, 1, Primary!)
            //ll_ret = dw_3.update()
            return true;
        }
        public async System.Threading.Tasks.Task<bool> wf_delete_item(long al_row) //**********************************************************************************************
        {
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            //*Object:				w_mini_terminal
            //*Function Name:	wf_delete_item
            //*Purpose: 			Call the function wf_copy_item_2_delete_table.
            //*Arguments: 		Long - al_row - Row number in uo_body.dw_inv_pack_details.
            //*Return:				Boolean - TRUE - Success to Insert the row.
            //*									 FALSE - Failed to Insert the row.
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*12-05-2009		SharonS			1.2.44.1	SPUCM00000075		Initial version
            //************************************************************************************************

            long ll_ret = 0;
            long ll_null = 0;
            string ls_batch = null;

            ids_invpack_deleted_itemsProperty.Reset();
            await masofonAlias.f_begin_tranClass.f_begin_tran();
            if (await wf_copy_item_2_delete_table(al_row))
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = ids_invpack_deleted_itemsProperty.Update(unitOfWork);
                }
                if (ll_ret < 0)
                {
                    await wf_show_message("wf_delete_item", "DB Error", "OK", "Failed to update invpack_deleted_items table.");
                    await masofonAlias.f_rollbackClass.f_rollback();
                    return false;
                }
            }

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "material_quantity", 0);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "bonus_quantity", 0);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "expected_material_quantity", 0);

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "state", "d");

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time());
            // SharonS - 1.2.44.5 - 2009-05-24 - SPUCM00000075 - Set mini_terminal indicator to be null

            ll_null = 0;

            //uf_calculate_declines
            (uo_body.dw_inv_pack_details).SetItem(al_row, "mini_terminal", ll_null);
            // End
            if (!await wf_update_item(al_row))
            {
                await wf_show_message("wf_delete_item", "Error", "OK", "Failed to delete item.");
                await masofonAlias.f_rollbackClass.f_rollback();
                return false;
            }
            await masofonAlias.f_commitClass.f_commit();
            return true;
        }
        public async System.Threading.Tasks.Task<bool> wf_invoice_lock(string as_mode, string as_doc_type, long al_doc_no) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            GridElement uo_header_dw_header = uo_header.GetVisualElementById<GridElement>("dw_header");
            TextBoxElement uo_header_ll_supplier_number = uo_header.GetVisualElementById<TextBoxElement>("supplier_number");
            //*Object:				w_mini_terminal
            //*Function Name:	wf_invoice_lock
            //*Purpose: 			Lock the document or prevent changes if document locked
            //*Arguments: 		String 		- as_mode		- type of activity lock/unlock
            //*						String 		- as_doc_type	- type of document
            //*						Long	 		- as_doc_no		- document number
            //*Return:				Boolean 	- TRUE/FALSE
            //*Date				Programer		Version	Task#	 							Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2009		AlexKh			1.2.45.0	SPUCM00001243 - CR#1119		Initial version
            //************************************************************************************************

            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long? ll_found = 0;
            long ll_i = 0;
            string ls_find = null;
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            ll_supplier_number = string.IsNullOrEmpty(uo_header_ll_supplier_number.Text) ? 0 : Convert.ToInt64(uo_header_ll_supplier_number.Text);
            //Choose the working mode
            switch (as_mode)
            {
                case "LOCK":
                    //Lock mode
                    ls_find = "doc_type == \"" + as_doc_type + "\" AND number == " + al_doc_no.ToString();


                    ll_found = uo_header_dw_body.Find(ls_find, 0, uo_header_dw_body.RowCount());
                    if (ll_found <= -1)
                    {
                        return false;
                    }


                    if (uo_header_dw_body.GetItemValue<string>(ll_found.Value, "create_mode") == "1")
                    {
                        return true;
                    }
                    if (!await f_get_set_invoice_lockClass.f_get_set_invoice_lock(ll_branch_number, ll_supplier_number, as_doc_type, al_doc_no, 4))
                    {
                        await wf_show_message("wf_invoice_lock", "Error", "OK", "�� ���� ����� ������� �����");
                        return false;
                    }

                    uo_header_dw_body.SetItem(ll_found.Value, "create_mode", "1");
                    break;
                case "UNLOCK":
                    //Unlock mode

                    for (ll_i = 0; ll_i < uo_header_dw_body.RowCount(); ll_i++)
                    {

                        if (uo_header_dw_body.GetItemValue<string>(ll_i, "create_mode") == "1")
                        {

                            as_doc_type = uo_header_dw_body.GetItemValue<string>(ll_i, "doc_type");

                            al_doc_no = uo_header_dw_body.GetItemValue<long>(ll_i, "number");
                            if (!await f_get_set_invoice_lockClass.f_get_set_invoice_lock(ll_branch_number, ll_supplier_number, as_doc_type, al_doc_no, 3))
                            {
                                await wf_show_message("wf_invoice_lock", "Error", "OK", "����� �������� ����");
                                return false;
                            }

                            uo_header_dw_body.SetItem(ll_i, "create_mode", "0");
                        }
                    }
                    break;
            }
            return true;
        }
        public async System.Threading.Tasks.Task<FuncResults<long, int>> wf_get_distributor_number(long al_distributor_number) //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_header = uo_header.GetVisualElementById<GridElement>("dw_header");
            //*Object:				w_mini_terminal
            //*Function Name:	wf_get_distributor_number
            //*Purpose: 			get distributor number of chosen order
            //*Arguments: 		Long	Ref	al_distributor_number
            //*Return:				integer	1/-1
            //*Date				Programer		Version	Task#	 					Description
            //*------------------------------------------------------------------------------------------------
            //*28-02-2010		AlexKh			1.2.45.19	SPUCM00001981		Initial version
            //************************************************************************************************

            var ldwc_order = uo_header.GetVisualElementById<ComboGridElement>("order_number");
            long ll_row_count = 0;
            long ll_row = 0;

            ll_row_count = ldwc_order.RowCount();
            if (ll_row_count < 0)
            {
                await wf_show_message("Error", "", "OK", "Order was not choosen.");
                return FuncResults.Return(al_distributor_number, -1);
            }
            else
            {
                ll_row = ldwc_order.SelectedIndex;
                if (ll_row >= 0)
                {

                    al_distributor_number = Convert.ToInt64(ldwc_order.GetItemValue<long>((int)ll_row, "distributor_number"));
                }
            }
            return FuncResults.Return(al_distributor_number, 1);
        }
        public async System.Threading.Tasks.Task<FuncResults<DateTime?, bool>> wf_show_expiration_msg(DateTime? adt_curr_expiration_date, long al_min_valid_months, string as_material_name, bool ab_check_only) //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			wf_show_expiration_msg
            //*Purpose:							Ask the user to enter expiration date to the item.
            //*  
            //*Arguments:						Pass By:		Argument Type:		Argument Name:
            //*										---------------------------------------------------------
            //*										Value			DateTime 				adt_curr_expiration_date
            //*										Value			Long						al_min_valid_months
            //*										Value			String					as_material_name
            //*										Value			Boolean 					ab_check_only
            //*
            //*Return:								BOOLEAN: TRUE - Success.
            //*												         FALSE - Failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************
            s_array_arguments lstr_ret = default(s_array_arguments);
            long ll_return = 0;
            string ls_message = null;
            DateTime? ldt_current_date = default(DateTime);
            DateTime? ld_min_valid_date = default(DateTime);
            ldt_current_date = masofonAlias.masofon.Instance.set_machine_time();
            ld_min_valid_date = ldt_current_date.Value.AddDays(al_min_valid_months * 30);
            if (ab_check_only)
            {
                return FuncResults.Return(adt_curr_expiration_date, false);
            }
            ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("�� ��� ����� �� ����").Result + " ";
            // AlexKh - 1.2.46.0 - 2010-06-16 - CR#1138 - send material name instead row number
            //ls_message += nvo_translator.fnv_translate_exp('����� ') + 	uo_body.dw_inv_pack_details.GetItem<string>(al_row, "material_name")
            ls_message += masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("����� ").Result + as_material_name;
            s_array_arguments lstr_arg = new s_array_arguments();
            lstr_arg.a_string[6] = "";
            lstr_arg.a_string[1] = "";
            lstr_arg.a_string[2] = "";
            lstr_arg.a_string[3] = "";
            lstr_arg.a_string[4] = "OKCancel";
            lstr_arg.a_string[5] = "1";
            lstr_arg.a_string[6] = ls_message;
            lstr_arg.a_datetime[0] = adt_curr_expiration_date;
            lstr_arg.a_datetime[1] = ld_min_valid_date.Value.Add(Convert.ToDateTime("00:00").TimeOfDay);

            w_mini_terminal_exiration_date frm = await WindowHelper.Open<w_mini_terminal_exiration_date>("mini_terminal_mini_terminal", "istr_arg", lstr_arg, "type", "w_mini_terminal_exiration_date");


            lstr_ret = (s_array_arguments)WindowHelper.GetParam<s_array_arguments>(frm);
            if (lstr_ret.arg_ok)
            {
                //..user has entered a date
                //	uo_body.dw_inv_pack_details.SetItem(al_row, "expiration_date", lstr_ret.a_datetime[1])		
                adt_curr_expiration_date = lstr_ret.a_datetime[0];
                if (System.Extensions.CollectionExtensions.UBound(lstr_ret.a_boolean) >= 0)
                {
                    if (System.Extensions.CollectionExtensions.UBound(lstr_ret.a_string) >= 0)
                    {
                        if (lstr_ret.a_string[0] == "DateNotValid")
                        {
                            lstr_ret.a_boolean[0] = true;
                        }
                    }
                    return FuncResults.Return(adt_curr_expiration_date, lstr_ret.a_boolean[0]);
                }
            }
            else
            {
                //..user didn't enter a date
                return FuncResults.Return(adt_curr_expiration_date, false);
            }
            return FuncResults.Return(adt_curr_expiration_date, true);
        }
        public void wf_set_parameters(long al_shipment_number, string as_pallet_number) // AlexKh - 1.2.46.0 - 2010-06-17 - CR#1138 - assign variables
        {
            is_pallet_numberProperty = as_pallet_number;
            il_shipment_numberProperty = al_shipment_number;
            //May be to move all retrieve actions to here
        }
        public void wf_get_parameters(ref long al_shipment_number, ref string as_pallet_number, ref bool ab_refresh) // AlexKh - 1.2.46.0 - 2010-06-21 - CR#1138 - assign variables
        {
            as_pallet_number = is_pallet_numberProperty;
            al_shipment_number = il_shipment_numberProperty;
            ab_refresh = true;
        }
        public async Task<bool> wf_check_marlog_distributor(int ai_param, string as_value, long al_distributor_number) //**********************************************************************
        {
            //*Object:				w_mini_terminal
            //*Function Name:	wf_check_marlog_distributor
            //*Purpose: 			Check if distributor requires update in marlog stock
            //*Arguments: 		integer	ai_param
            //*						string	as_value
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version		Task#	 			Description
            //*-----------------------------------------------------------------------
            //*07-10-2010		AlexKh			1.2.46.2		SPUCM00002424	Initial version
            //***********************************************************************

            long ll_distributor_number = 0;
            long? ll_ret = 0;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            string ls_filter = null;
            string ls_value = null;
            IRepository lds_supplier_distributors;




            lds_supplier_distributors = new d_addhock_supplier_distributorsRepository();



            ll_ret = lds_supplier_distributors.Retrieve(ai_param);

            ll_ret = lds_supplier_distributors.SetFilter("");

            ll_ret = lds_supplier_distributors.Filter();
            ll_distributor_number = al_distributor_number;
            if (ll_distributor_number == 0 || ll_distributor_number == 0)
            {
                return false;
            }
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            (await this.wf_get_selected_supplier_number(ll_supplier_number)).Retrieve(out ll_supplier_number);
            ls_filter = "branch_number IN (0, " + ll_branch_number.ToString() + ") AND supplier_number IN (0, " + ll_supplier_number.ToString() + ") AND distributor_number IN (0, " + ll_distributor_number.ToString() + ")";

            ll_ret = lds_supplier_distributors.SetFilter(ls_filter);

            ll_ret = lds_supplier_distributors.Filter();

            ll_ret = lds_supplier_distributors.SetSort("branch_number DESC, supplier_number DESC, distributor_number DESC");

            ll_ret = lds_supplier_distributors.Sort();


            ll_ret = lds_supplier_distributors.Find("distributor_number == " + ll_distributor_number.ToString(), 0, lds_supplier_distributors.RowCount());
            if (ll_ret <= -1)
            {


                ll_ret = lds_supplier_distributors.Find("distributor_number == 0", 0, lds_supplier_distributors.RowCount());
            }
            if (ll_ret >= 0)
            {

                ls_value = lds_supplier_distributors.GetItemValue<string>(ll_ret.Value, "value");



                if (ls_value == as_value)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {


                return false;
            }
        }
       
        public void wf_build_title() //**********************************************************************************************
        {
            //*Object:				w_mini_terminal
            //*Function Name:	wf_build_title
            //*Purpose: 			Build window title
            //*Arguments: 		None
            //*Return:				None
            //*Date				Programer		Version	Task#	 				Description
            //*------------------------------------------------------------------------------------------------
            //*21-02-2013		AlexKh			1.0.0.1	SPUCM#00004018		Initial version
            //************************************************************************************************

            // TODO: Field 'ServerName of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            Text = masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(" ����: ") + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("- ����: ") + masofonAlias.masofon.Instance.gs_station.station_number.ToString() + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("- �����: ") + masofonAlias.masofon.Instance.sqlca.ServerName.ToString() + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("- ����: ").Result + masofonAlias.masofon.Instance.guo_version.uf_get_version_name();
        }
        public async Task<bool> wf_check_303_department(long al_material_number) //********************************************************************
        {
            uo_mini_terminal_pallet_manual uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
            //*Object:				uo_mini_terminal_pallets_list
            //*Function Name:	wf_check_303_department
            //*Purpose: 			Check item belongs to department 303
            //*Arguments: 		Long	al_material_number
            //*Return:				None
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*07-03-2013		AlexKh			1.1.1.3	SPUCM00004018	Initial version
            //********************************************************************


            return await uo_pallet_manual.uf_check_303_department(al_material_number);
        }
        public async System.Threading.Tasks.Task<FuncResults<long, long, bool>> wf_set_marlog_info(long al_marlog_branch, long al_marlog_distributor, bool ab_crossdoc) //**********************************************************************************************
        {
            //*Object:				w_mini_terminal
            //*Function Name:	wf_set_marlog_info
            //*Purpose: 			set appropriate marlog branch and distributor
            //*Arguments: 		Long - al_marlog_branch
            //*						Long - al_marlog_distributor
            //*Return:				Boolean TRUE/FALSE
            //*Date				Programer		Version	Task#	 						Description
            //*------------------------------------------------------------------------------------------------
            //*17-02-2014		AlexKh			1.1.1.18	CR#1171(SPUCM00004718)	Initial version
            //************************************************************************************************

            long ll_count = 0;
            long ll_row = 0;
            if (!(masofonAlias.masofon.Instance.gds_marlog_distributor != null))
            {
                masofonAlias.masofon.Instance.gds_marlog_distributor = new ds_marlog_distributor_translateRepository();


                masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(34);
            }

            ll_count = masofonAlias.masofon.Instance.gds_marlog_distributor.RowCount();
            if (ll_count < 1)
            {
                await wf_show_message("wf_set_marlog_info Error", "", "OK", "������ ������ ����� �����");
                return FuncResults.Return(al_marlog_branch, al_marlog_distributor, false);
            }
            else if (ll_count == 1)
            {
                ll_row = 1;
            }
            else
            {
                // AlexKh - 1.1.28.0 - 2015-02-17 - SPUCM00005224 - crossdoc indication
                var choose_marlog = await WindowHelper.Open<rw_choose_marlog>("global_global", "crossDoc", ab_crossdoc.ToString());
                //Open(rw_choose_marlog)
                ll_row = Convert.ToInt64(WindowHelper.GetParam<int>(choose_marlog));
                if (ll_row == -1)
                {
                    await wf_show_message("wf_set_marlog_info Error", "", "OK", "������ ������ ����� �����");
                    return FuncResults.Return(al_marlog_branch, al_marlog_distributor, false);
                }
            }

            al_marlog_branch = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_row, "branch_number"));

            al_marlog_distributor = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_row, "distributor_number"));
            return FuncResults.Return(al_marlog_branch, al_marlog_distributor, true);
        }
        public async System.Threading.Tasks.Task<FuncResults<long, long, bool>> wf_set_marlog_info(long al_marlog_branch, long al_marlog_distributor, long al_param_code) //********************************************************************
        {
            //*Object:	w_mini_terminal
            //*Function Name:	(wf_set_marlog_info)
            //*Purpose: 	Based on wf_set_marlog_info without al_param_code
            //*Arguments: 	ref long al_marlog_branch, ref long al_marlog_distributor, long al_param_code
            //*Return:	boolean
            //*Programmer	Date   Version	Task#	 Description
            //*---------------------------------------------------------------------
            //*RonY@15/01/2015 -   SPUCM00005162
            //********************************************************************

            long ll_count = 0;
            long ll_row = 0;
            if (!(masofonAlias.masofon.Instance.gds_marlog_distributor != null))
            {



                masofonAlias.masofon.Instance.gds_marlog_distributor = new ds_marlog_distributor_translateRepository();


                masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(al_param_code);
            }
            else
            {

                masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(al_param_code);
            }

            ll_count = masofonAlias.masofon.Instance.gds_marlog_distributor.RowCount();
            if (ll_count < 1)
            {
                await wf_show_message("wf_set_marlog_info Error", "", "OK", "������ ������ ����� �����");

                masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(34); //return to base retrieve for other windows //RonY@18/01/2015 1.12.49.12 - SPUCM00005162
                return FuncResults.Return(al_marlog_branch, al_marlog_distributor, false);
            }
            else if (ll_count == 1)
            {
                ll_row = 1;
            }
            else
            {
                //RonY@11/03/2015  - SPUCM00005247 //Open(rw_choose_marlog)
                var choose_marlog = await WindowHelper.Open<rw_choose_marlog>("global_global", "crossDoc", "CROSSDOC_RETURN");


                // ll_row = (long)WindowHelper.GetParam<int>(choose_marlog); ;
                var dw_list = choose_marlog.GetVisualElementById<GridElement>("dw_list");
                ll_row = dw_list.SelectedRowIndex;
                if (ll_row == -1)
                {
                    await wf_show_message("wf_set_marlog_info Error", "", "OK", "������ ������ ����� �����");

                    masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(34); //return to base retrieve for other windows //RonY@18/01/2015 1.12.49.12 - SPUCM00005162
                    return FuncResults.Return(al_marlog_branch, al_marlog_distributor, false);
                }
            }

            al_marlog_branch = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_row, "branch_number"));

            al_marlog_distributor = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_row, "distributor_number"));

            masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(34); //return to base retrieve for other windows //RonY@18/01/2015 1.12.49.12 - SPUCM00005162
            return FuncResults.Return(al_marlog_branch, al_marlog_distributor, true);
        }
        public async System.Threading.Tasks.Task<bool> wf_check_invoice_state(long al_supplier_number, long al_invoice_number) //********************************************************************
        {
            //*Object:				w_mini_terminal
            //*Function Name:	wf_check_invoice_state
            //*Purpose: 			Check invoice state
            //*Arguments: 		Long	al_supplier_number
            //*						Long	al_invoice_number
            //*Return:				None
            //*Date				Programer		Version	Task#	 			Description
            //*---------------------------------------------------------------------
            //*10-12-2015		AlexKh			1.1.40.0	SPUCM0000		Initial version
            //********************************************************************

            string ls_state = null;
            char lc_doc_type = '\0';
            lc_doc_type = await this.wf_get_doc_type(al_invoice_number);
            if (lc_doc_type.ToString() == "I")
            {
                LoadData1(al_supplier_number, al_invoice_number, ref ls_state);
                //AlexKh - 1.29.7.12 - 2016-07-06 - SPUCM00005717  - in case invoice not exist, proceed with success

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
                {
                    return true;
                }

                if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                {

                    await wf_show_message("wf_check_invoice_state", "DB Error", "OK", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    return false;
                }
                if (ls_state != "O")
                {
                    await wf_show_message("wf_check_invoice_state", al_invoice_number.ToString() + "�����", "OK", " �������� �����. ���� ������� ");
                    return false;
                }
            }
            return true;
        }
        public async Task open() //**********************************************************************************************
        {
            uo_mini_terminal_header uo_header = await get_uo_mini_terminal_header();
            GridElement uo_header_dw_body = uo_header.GetVisualElementById<GridElement>("dw_body");
            GridElement uo_header_dw_header = uo_header.GetVisualElementById<GridElement>("dw_header");
            uo_mini_terminal_body uo_body = await get_uo_mini_terminal_body();
            uo_mini_terminal_pallet_manual uo_pallet_manual = await get_uo_mini_terminal_pallet_manual();
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			Open
            //*Purpose:							Check user identication and display the header UO.
            //*  
            //*Arguments:						None.
            //*Return:								Long.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_RtnCode = 0;
            string ls_message = null;
            string ls_send_decline_msg = null;
            string ls_log_mode = null;
            //// Open the check id window
            //if (!await this.ue_id_password_check())
            //{
            //    this.Close();
            //    return;
            //}
            uo_header.il_employee_numberProperty = il_employee_numberProperty;
            uo_body.il_employee_number = il_employee_numberProperty;
            //.. multi_lingual_is_active = yes + current system language not Hebrew
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(uo_header_dw_header);
                //await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(uo_header_dw_body);
                //await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw((GridElement)uo_body.dw_inv_pack_details);
                //await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(uo_body.dw_summary);
            }
            ls_log_mode = await f_get_parameter_valueClass.f_get_parameter_value("log_modules", "string", "NONE", "MT (MINI_TERMINAL), SF (SUPPLIERS_FILE), SC (STOCK_COUNT),  or None.", "gds_find");
            ib_log_modeProperty = ls_log_mode.IndexOf("NEWMS") > 0;
            // Write to log
            ls_message = "*************************** Open*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "Open" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            //this.wf_get_window_position();
            // AlexKh - 1.2.46.0 - 2010-05-31 - CR#1138 - open entrance uo
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_shipment", "boolean", "false", "Supply Merchandise from marlog.", "gds_find");
            if (temp.ToLower() == "true")
            {
                li_RtnCode = await wf_display("ENTRANCE");
                //IF NOT IsValid(invuo_doc_item_search) THEN invuo_doc_item_search = CREATE nvuo_pallet_item_search
            }
            else
            {
                li_RtnCode = await wf_display("HEADER");
                //IF NOT IsValid(invuo_doc_item_search) THEN invuo_doc_item_search = CREATE nvuo_doc_item_search
            }
            if (!(invuo_doc_item_searchProperty != null))
            {
                invuo_doc_item_searchProperty = new nvuo_doc_item_search();
            }
            invuo_doc_item_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            if (!(iuo_material_updateProperty != null))
            {
                iuo_material_updateProperty = new u_material_update();
            }
            iuo_material_updateProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            ls_send_decline_msg = await f_get_parameter_valueClass.f_get_parameter_value("decline_operation", "string", "NONE", "DB or MSG or DB_MSG or None.", "gds_find");
            uo_body.is_send_decline_msg = ls_send_decline_msg;
            uo_header.is_send_decline_msgProperty = ls_send_decline_msg;
            Globals.IsSendDeclineMsg = ls_send_decline_msg;

            ids_invpack_deleted_itemsProperty = new ds_invpack_deleted_itemsRepository();
            // Initialize value to model
            ids_invpack_deleted_itemsProperty.Modify("Initial,branch_number," + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,station_number," + masofonAlias.masofon.Instance.gs_station.station_number.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,delete_point,1");
            ids_invpack_deleted_itemsProperty.Modify("Initial,del_emp_number," + Globals.EmployeeNumber.ToString());
            ids_invpack_deleted_itemsProperty.Modify("Initial,doc_type,p");
            // End
            //wf_build_title();
            //wf_resize();
        }
        public void close() //**********************************************************************************************
        {
            //*Object:								w_mini_terminal
            //*Function/Event  Name:			close
            //*Purpose:							Write to log.
            //*  
            //*Arguments:						
            //*Return:								
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            string ls_message = null;
            // Write to log
            ls_message = "*************************** Close*************************** DateTime:  " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Command: " + "Close" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            if (invuo_doc_item_searchProperty != null)
            {



            }
            if (iuo_material_updateProperty != null)
            {



            }
            // SharonS - 1.2.44.1 - 2009-05-12 - SPUCM00000075 - Delete items
            if (ids_invpack_deleted_itemsProperty != null)
            {



            }
            // End
        }

        private static w_mini_terminal _w_mini_terminal = null;
        public static w_mini_terminal Instance()
        {
            return _w_mini_terminal = _w_mini_terminal ?? new w_mini_terminal();

        }


        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }

        private void HideAllControlInPnlContent()
        {
            var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
            foreach (ControlElement item in pnl.Controls)
            {
                item.Visible = false;
            }
        }

        internal async Task<uo_mini_terminal_trans_to_branch> get_uo_mini_terminal_trans_to_branch()
        {
            uo_mini_terminal_trans_to_branch returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_trans_to_branch>("uo_trans_to_branch");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_trans_to_branch>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_trans_to_branch";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }

        internal async Task<uo_mini_terminal_trans_from_branch> get_uo_trans_from_branch()
        {
            uo_mini_terminal_trans_from_branch returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_trans_from_branch>("uo_trans_from_branch");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_trans_from_branch>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_trans_from_branch";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }

        internal async Task<uo_mini_terminal_crossdoc_returns_details> get_uo_mini_terminal_crossdoc_returns_details()
        {
            uo_mini_terminal_crossdoc_returns_details returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_crossdoc_returns_details>("uo_crossdoc_returns_details");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_crossdoc_returns_details>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_crossdoc_returns_details";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_crossdoc_returns> get_uo_mini_terminal_crossdoc_returns()
        {
            uo_mini_terminal_crossdoc_returns returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_crossdoc_returns>("uo_crossdoc_return");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_crossdoc_returns>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_crossdoc_return";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }

        internal async Task<uo_mini_terminal_returns_details> get_uo_mini_terminal_returns_details()
        {
            uo_mini_terminal_returns_details returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_returns_details>("uo_returns_details");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_returns_details>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_returns_details";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }

        internal async Task<uo_mini_terminal_returns> get_uo_mini_terminal_returns()
        {
            uo_mini_terminal_returns returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_returns>("uo_returns");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_returns>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_returns";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_trade_site> get_uo_mini_terminal_trade_site()
        {
            uo_mini_terminal_trade_site returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_trade_site>("uo_trade_site");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_trade_site>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_trade_site";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_crossdoc_pack_list> get_uo_mini_terminal_crossdoc_pack_list()
        {
            uo_mini_terminal_crossdoc_pack_list returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_crossdoc_pack_list>("uo_crossdoc_list");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_crossdoc_pack_list>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_crossdoc_list";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_pallet_manual> get_uo_mini_terminal_pallet_manual()
        {
            uo_mini_terminal_pallet_manual returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_pallet_manual>("uo_pallet_manual");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_pallet_manual>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_pallet_manual";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_pallets_list> get_uo_mini_terminal_pallets_list()
        {
            uo_mini_terminal_pallets_list returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_pallets_list>("uo_shipments_list");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_pallets_list>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_shipments_list";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task<uo_mini_terminal_header> get_uo_mini_terminal_header()
        {
            uo_mini_terminal_header returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_header>("uo_header");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_header>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_header";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }

        internal async Task<uo_mini_terminal_body> get_uo_mini_terminal_body()
        {
            uo_mini_terminal_body returnValue = null;
            returnValue = this.GetVisualElementById<uo_mini_terminal_body>("uo_body");
            if (returnValue == null)
            {
                var pnl = this.GetVisualElementById<PanelElement>("pnlContent");
                returnValue = await WindowHelper.Create<uo_mini_terminal_body>("mini_terminal_mini_terminal");
                returnValue.ID = "uo_body";
                pnl.Controls.Add(returnValue);
                returnValue.PerformLoad(EventArgs.Empty);
            }
            return returnValue;
        }


        internal async Task UnlockInvoices()
        {
            //GalilCS - unlock all locked invoices before closing window
            try
            {
                uo_mini_terminal_pallet_manual pallet_manual = await this.get_uo_mini_terminal_pallet_manual();
                if (pallet_manual != null)
                {
                    await pallet_manual.uf_invoice_lock("UNLOCK", "", 0);
                }
            }
            catch { }

            try
            {
                uo_mini_terminal_body body = await this.get_uo_mini_terminal_body();
                if (body != null)
                {
                    await body.uf_invoice_lock("UNLOCK", "", 0);
                }
            }
            catch { }

        }
    }
}
