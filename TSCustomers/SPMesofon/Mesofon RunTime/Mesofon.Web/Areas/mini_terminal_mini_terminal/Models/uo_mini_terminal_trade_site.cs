using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using global;
using Mesofon.Common.Global;
using Mesofon.Common;

namespace mini_terminal
{
    // Creation Time:   08/11/2015 10:21:16 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_trade_site : CompositeElement
    {
        public const string NOT_APPROVED = "s";
        public const string APPROVED = "b";
        public const string PARTIAL = "p";
        public u_order_search iuo_order_search;
        public u_material_update iuo_material_update;
        public long il_employee_number;
        public uo_mini_terminal_trade_site()
        {

        }

        public void constructor() // AlexKh - 1.1.1.10 - 2013-05-01 - CR#1169  - constructor
        {
            GridElement dw_orders_list = this.GetVisualElementById<GridElement>("dw_orders_list");
            long ll_ret = 0;
            this.iuo_order_searchProperty = new u_order_search();
            this.iuo_order_searchProperty.uf_set_mt_message(true);
            this.iuo_order_searchProperty.uf_set_branch_number(masofon.masofon.Instance.gs_vars.branch_number);
            this.iuo_material_updateProperty = new u_material_update();
            this.iuo_material_updateProperty.uf_set_transaction(masofon.masofon.Instance.sqlca);
            this.iuo_material_updateProperty.uf_set_branch_number(masofon.masofon.Instance.gs_vars.branch_number);



            dw_orders_list.Retrieve(masofon.masofon.Instance.gs_vars.branch_number);
            //uf_resize();
        }
        public u_order_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public u_material_update iuo_material_updateProperty
        {
            get { return this.iuo_material_update; }
            set { this.iuo_material_update = value; }
        }
        public void LoadData(Int64 al_site_order_number, ref char lc_site_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_site_order_number }, { "@1", masofon.masofon.Instance.gs_vars.LaboratoryBranchNum } };
                    string sql = "SELECT state FROM site_order_state WHERE branch_number = @1 AND site_order_number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        lc_site_state = UnitOfWork.GetValue(resultRS, 0, Convert.ToChar);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
