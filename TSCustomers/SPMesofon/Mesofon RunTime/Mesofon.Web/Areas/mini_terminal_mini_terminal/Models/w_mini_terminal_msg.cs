using System.Web.VisualTree.Elements;
using global;
using mini_terminalAlias = mini_terminal;
using System;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:21 (Utc)
	// Comments:        
	// 
	public class w_mini_terminal_msg : WindowElement
	{
        
        public string is_window_title;
		//Constant Long				il_max_window_height = 1008
		//Constant Long				il_max_window_width = 1088
		public long il_max_window_height = 640;
		public long il_max_window_width = 480;
        private int il_return_value=1;
        public delegate void ue_key_EventHandler();
	//	public event mini_terminalAlias.w_mini_terminal_msg.ue_key_EventHandler ue_keyEvent;
		public w_mini_terminal_msg(s_array_arguments istr_arg)
		{
             this.istr_arg = istr_arg;
        }
        public s_array_arguments istr_arg { get; set; }


        public s_array_arguments istr_argProperty
		{
			get { return this.istr_arg; }
			set { this.istr_arg = value; }
		}
		public string is_window_titleProperty
		{
			get { return this.is_window_title; }
			set { this.is_window_title = value; }
		}
		public long il_max_window_heightProperty
		{
			get { return this.il_max_window_height; }
			set { this.il_max_window_height = value; }
		}
		public long il_max_window_widthProperty
		{
			get { return this.il_max_window_width; }
			set { this.il_max_window_width = value; }
		}
        public int il_return_valueProperty
        {
            get { return this.il_return_value; }
            set { this.il_return_value = value; }
        }
       

        public async System.Threading.Tasks.Task<string> wf_translate_message(string as_message) //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_msg.wf_translate_message()
            //
            // Returns:         String
            //
            // Parameters:      value String as_message
            // 
            // Copyright  - Stas
            //
            // Date Created: 08/08/2005
            //
            // Description:	
            // 					 translate message :
            //					 1) if message start is # ---> gets message by message number from DB
            //					 2) otherwise return as_message
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (!string.IsNullOrEmpty(as_message) && as_message.Substring(0, 1) == "#")
            {
                long ll_message_number = 0;
                ll_message_number = Convert.ToInt64(System.Extensions.StringsEx.Right(as_message, as_message.Length - 1));
                if (ll_message_number != 0)
                {
                    as_message = await f_get_error_message_numberClass.f_get_error_message_number((int)ll_message_number);
                }
            }
            return as_message;
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
