using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Drawing;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using global;
using Mesofon.Common;
using Mesofon.Repository;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Threading.Tasks;
using MvcSite.Common;
namespace mini_terminal
{
    // Creation Time:   08/11/2015 10:21:09 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_base_body : CompositeElement
    {
        public IRepository ids_details_pack_list_save;
        public IRepository ids_details_invoice_save;
        public IRepository ids_details_pack_list;
        public IRepository ids_details_invoice;
        public IRepository ids_inv_pack_details;
        public u_material_search iuo_material_search;
        public long il_row_no = 1;
        public long il_new_doc_no;
        public long il_old_doc_no;
        public char ic_new_doc_type;
        public char ic_old_doc_type;
        public string is_barcode;
        public string is_new_barcode = "";
        public string is_old_barcode = "";
        public long il_employee_number;
        // SharonS - 1.2.26.8 - 2008-03-18 - Decline - ib_send_decline_msg
        public string is_send_decline_msg;
        // AlexKh - 1.2.44.1 - 2009-05-11 - SPUCM00001251 - timer variable
        public long il_start_time;
        public string is_start_time;
        // AlexKh - 1.2.48.64 - 2013-07-23 - CR#1160 - return number
        public long il_return_number;
        public string is_carton_barcode;
        public const char RETURN_STATE_OPEN = 'n';
        public const char RETURN_STATE_SEND = 'a';
        public const char RETURN_STATE_CLOSE = 'c';
        public const char RETURN_STATE_CANCEL = 'd';
        public delegate void ue_diff_report_EventHandler();
        public event ue_diff_report_EventHandler ue_diff_reportEvent;
        
        public int uf_enable_cb_finish(bool ab_enabled) //**********************************************************************************************
        {
            ButtonElement cb_finish = this.GetVisualElementById<ButtonElement>("cb_finish");
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_enable_cb_finish
            //*Purpose:							Enable/disable the button:cb_finish according to the argument
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 		Boolean 					ab_enabled: TRUE- enabled cb_finish,
            //*																								 	FALSE- disable cb_finish.
            //*Return:								Integer: 1 - Success
            //*												  -1 - Failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.   				0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            if(cb_finish != null)
            {
                cb_finish.Enabled = ab_enabled;
            }
            return 1;
        }
        public async Task<int> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_show_message
            //*Purpose:							Show message.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value 			String 				as_title
            //*										Value 			String 				as_error_text
            //*										Value 			String 				as_buttons
            //*										Value 			String 				as_message
            //*		
            //*Return:								Integer:1 - Success
            //*												  -1 - Otherwise
            //*Date		     	Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*07-08-2007    SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;


            li_rtn_code = Convert.ToInt32(await w_mini_terminal.Instance().wf_show_message(as_title, as_error_text, as_buttons, as_message));
            // AlexKh - 1.2.44.1 - 2009-05-13 - SPUCM00001250 - return function returned value
            //RETURN 1
            return li_rtn_code;
        }
        public virtual async Task<int> uf_new_row(WidgetGridElement dw_inv_pack_details) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_body
            //*Function/Event  Name:			uf_new_row
            //*Purpose:							Add new row in dw_inv_pack_details.
            //*  
            //*Arguments:						None 
            //*		
            //*Return:								Integer: -1 Failed
            //*													1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-08-2007		SHARP.   				0.2.0   		B2B 			Initiation
            //************************************************************************************************

            long ll_row = 0;
            long ll_next_serial_number = 0;
            int li_RtnCode = 0;
            int li_rtn = 0;
            var count = dw_inv_pack_details.RowCount();
            ll_row = dw_inv_pack_details.Insert(0);
            dw_inv_pack_details.SetItem(count, "row_no", count + 1);
            il_row_noProperty += 1;
            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Set row_no_en in the first row
            //iuo_material_search.
            //li_rtn = THIS.uf_get_doc_no_en(ll_row, 0)
            // SharonS - 1.2.42.1 - 2009-02-25 - Fix 
            //THIS.dw_inv_pack_details.SetItem(ll_row, "doc_no_en", li_rtn)

            dw_inv_pack_details.SetItem(count, "doc_no_en", 0);
            // End

            dw_inv_pack_details.SetItem(count, "branch_number", Convert.ToInt64(masofonAlias.masofon.Instance.gs_vars.branch_number));

            //GalilCS
            if (dw_inv_pack_details.Rows.Count > 0)
            {
                Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e) =>
                {
                    FocusLastRow(obj, e);
                }, dw_inv_pack_details,delay:10);
                
                //Get parent element
                var parentElement = dw_inv_pack_details.ParentElement;
                if(parentElement != null)
                {
                    //Get id
                    var id = parentElement.ID;
                    if (!string.IsNullOrEmpty(id))
                    {
                        if (id == "uo_body") //Sapak
                        {
                            //Perform selection change event
                            dw_inv_pack_details.Rows[dw_inv_pack_details.Rows.Count - 1].Selected = true; 
                        }
                        else // Marlog or otherwise
                        {
                            //Prevent raise selection change event
                            dw_inv_pack_details.Rows[dw_inv_pack_details.Rows.Count - 1].SelectedInternal = true; 
                        }
                    }
                    else
                    {
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                    }
                }
                else
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                }
            }

          
             li_RtnCode = this.uf_enable_cb_finish(true);
            if (li_RtnCode < 1)
            {
                await this.uf_show_message("Error", "", "OK", "Error enable cb_finish.");
                return -1;
            }
            dw_inv_pack_details.Modify("NoUserPrompt=Yes");
            //Set the position of Horizonal-Scroll. 
            //dw_inv_pack_details.set_HorizontalScrollPosition(dw_inv_pack_details.get_HorizontalScrollMaximum());
            return 1;
        }
        private void FocusLastRow(object sender,EventArgs e)
        {
            var dw_inv_pack_details = sender as WidgetGridElement;
            if (dw_inv_pack_details != null)
            {
                var rowIndex = dw_inv_pack_details.Rows.Count - 1;
                dw_inv_pack_details.SetFocus(rowIndex, "barcode");
            }
        }

        public uo_mini_terminal_base_body()
        {
            constructor();
        }

        public async System.Threading.Tasks.Task<FuncResults<DateTime?,bool>> uf_show_expiration_msg( DateTime? adt_curr_expiration_date, long al_min_valid_months, long al_row, bool ab_check_only, GridElement dw_inv_pack_details) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_show_expiration_msg
            //*Purpose:							Display expiration message
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										----------------------------------------------------------------------
            //*										Value			DateTime 			adt_curr_expiration_date
            //*										Value			Long					al_min_valid_months
            //*										Value			Long					al_row
            //*										Value			Boolean 				ab_check_only
            //*
            //*Return:								Boolean: TRUE - Success
            //*												     FALSE  - Failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.   				0.2.0   		B2B 			Initiation
            //************************************************************************************************

            bool lb_rtn_code = false;
            // AlexKh - 1.2.46.0 - 2010-06-16 - CR#1138 - send material name instead row number
            //lb_rtn_code =  PARENT.DYNAMIC wf_show_expiration_msg(adt_curr_expiration_date, al_min_valid_months, al_row, ab_check_only)

            var material_name = dw_inv_pack_details.GetItemValue<string>(al_row, "material_name");
            var barcode = dw_inv_pack_details.GetItemValue<string>(al_row, "barcode");
            var s_name_barcode = string.Format("{0} {1}", material_name, string.IsNullOrEmpty(barcode) ? "" : string.Format(", ����� - {0}", barcode));
            lb_rtn_code = (await (this.Parent.Parent as w_mini_terminal).wf_show_expiration_msg(adt_curr_expiration_date, al_min_valid_months, s_name_barcode, ab_check_only)).Retrieve(out adt_curr_expiration_date);
            return FuncResults.Return(adt_curr_expiration_date, lb_rtn_code);
        }

        public virtual async Task<int> uf_set_items(string as_column_name, string as_data, long al_row) //Must be overriden
        {
            return 1;
        }

        public virtual async Task<int> uf_finish_input() //MUST BE OVERRIDEN
        {
            return 1;
        }

        public virtual async Task<FuncResults<decimal, decimal, decimal, long, int>> uf_get_order_material_price(long al_supplier_number, long al_order_number, long al_material_number, decimal adec_material_price, decimal adec_material_price_after_discount, decimal adec_material_discount_percent, long al_error_number)
        {
            return FuncResults.Return(adec_material_price, adec_material_price_after_discount, adec_material_discount_percent, al_error_number, 1);
        }
        public virtual long uf_get_max_serial_number(long al_doc_number) //MUST BE OVERRIDDEN
        {
            return 1;
        }

        public virtual async Task<char> uf_get_doc_type(long? al_doc_number) //MUST BE OVERRIDDEN
        {
            return default(char);
        }
        public virtual async Task<int> uf_build_doc_number_list() //MUST BE OVERRIDDEN
        {
            return 1;
        }
        public void constructor()
        {
            //**********************************************************************************************
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			constructor
            //*Purpose:							Initiate non visual objects and variables.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								LONG: 1 - Success, -1 - Failure.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*06-08-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************
            this.iuo_material_searchProperty = new u_material_search();
            this.ids_inv_pack_detailsProperty = new d_mini_terminal_details_displayRepository();
            this.ids_details_invoiceProperty = new d_mini_terminal_details_invoiceRepository();
            this.ids_details_pack_listProperty = new d_mini_terminal_details_packing_listRepository();
            this.ids_details_invoice_saveProperty = new d_mini_terminal_details_invoice_saveRepository();
            this.ids_details_pack_list_saveProperty = new d_mini_terminal_details_pack_list_saveRepository();

            this.iuo_material_searchProperty.uf_set_mt_message(true);
            this.il_new_doc_noProperty = 0;
            this.il_old_doc_noProperty = 0;
            //AlexKh
          //  uf_resize();
        }


        public async Task<int> uf_set_focus(string as_uo_name, string as_dw_name, long al_row_no, string as_field_name) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_set_focus
            //*Purpose:							Set focus according to the arguments.
            //*										Set focus on the field:as_field_name on row:al_row_no in the dw:as_dw_name
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										-------------------------------------------------------------------------
            //*										Value 			String 				as_uo_name
            //*										Value 			String 				as_dw_name
            //*										Value 			Long	 				al_row_no
            //*										Value 			String 				as_field_name
            //*		
            //*Return:								Integer: -1 Failed
            //*													1 Success 
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-08-2007		SHARP.    				0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;

            //ToDO set focus to element      
            li_rtn_code = Convert.ToInt32(await (Parent.Parent as w_mini_terminal).wf_set_focus(as_uo_name, as_dw_name, al_row_no, as_field_name));
            return li_rtn_code;
        }

        public async Task<int> uf_display(string as_show_mode) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_display
            //*Purpose:							Display uo_mini_terminal_header.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										---------------------------------------------------------------
            //*										Value			String						as_show_mode
            //*		
            //*Return:								Integer: 1 - Success, -1 - Otherwise.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*16-07-2007		SHARP.					0.2.0  		B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = await (Parent.Parent as w_mini_terminal).wf_display(as_show_mode);

             
           // li_rtn_code = Convert.ToInt32((Parent as w_mini_terminal).wf_display(as_show_mode));
            if (li_rtn_code < 1)
            {
                await uf_show_message("�����", "", "1", ".����� ����� �����");
                return -1;
            }
            // AlexKh - 1.2.45.0 - 2009-07-09 - SPUCM00001243 - CR#1119 - Unlock all locked invoices
            if (!await uf_invoice_lock("UNLOCK", "", 0))
            {
                await uf_show_message("�����", "", "1", ".����� ������ �������� ������");
                return 1;
            }
            return 1;
        }

        public virtual async Task<bool> uf_invoice_lock(string as_mode, string as_doc_type, long al_doc_no) //MUST BE OVERRIDEN
        {
            return true;
        }

        public async Task<bool> uf_is_new_row_exist(GridElement dw_inv_pack_details) //**********************************************************************************************
        {

            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_is_new_row_exist
            //*Purpose:							Check if new row was already entered in dw_inv_pack_details.
            //*  
            //*Arguments:						None.
            //*Return:								Boolean: TRUE - New row exist in dw_inv_pack_details
            //*													FALSE - Otherwise
            //*DAte				Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*04-09-2007		SHARP.				0.2.0			B2B 			Initiation
            //************************************************************************************************
           
            long ll_RowCount = 0;
            long ll_cntr = 0;
            string ls_barcode = null;
            string ls_message = null;

            ll_RowCount = dw_inv_pack_details.RowCount();
            uf_prepare_log(GetType().Name, "uf_is_new_row_exist", "RowCount: " + ll_RowCount.ToString(), "", "");
            if (ll_RowCount > 0)
            {
                for (ll_cntr = 0; ll_cntr < ll_RowCount; ll_cntr++)
                {
                    ls_barcode = dw_inv_pack_details.GetItemValue<string>(ll_cntr,"barcode");
                    if (string.IsNullOrEmpty(ls_barcode) || ls_barcode == null)
                    {
                        return true;
                    }
                }
                return false;
            }
            else if (ll_RowCount < 0)
            {
                //error
                await uf_show_message("Error", "", "OK", "Error");
            }
            else if (ll_RowCount == 0)
            {
                //No rows
                return false;
            }

            return true;
        }

        public bool uf_prepare_log(string as_object_name, string as_fucntion_name, string as_function_desc, string as_add_line1, string as_add_line2) //*********************************************************************
        {
            return true;
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_prepare_log
            //*Purpose:							Prepare message for log
            //*Arguments:							String as_object_name
            //*										String as_function_name
            //*										String as_function_desc
            //*										String as_add_line1
            //*										String as_add_line2
            //*Return:								Boolean
            //*Date 			Programer		Version		Task#			Description
            //*---------------------------------------------------------------------
            //*03-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //*********************************************************************

            string ls_message = null;
            ls_message = "*************************** " + as_fucntion_name + " - " + as_object_name + "*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Desc:     " + as_function_desc + "\r" + "\n";
            if (!isempty_stringClass.isempty_string(as_add_line1))
            {
                ls_message += as_add_line1 + "\r" + "\n";
            }
            if (!isempty_stringClass.isempty_string(as_add_line2))
            {
                ls_message += as_add_line2 + "\r" + "\n";
            }
            uf_write_log(ls_message);
            return true;
        }

        public int uf_write_log(string as_message) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_write_log
            //*Purpose:							Write into log file
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										---------------------------------------------------------------------------
            //*										Value 		String 				as_message- the message to write into the file.
            //*
            //*Return:								Integer: 1- Success
            //*												  -1 -Failed.
            //*Date 			Programer				Version		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007    SHARP.   				0.2.0 		B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;

             
            SystemHelper.WriteToLog(as_message);
            if (li_rtn_code < 1)
            {
                return -1;
            }
            return 1;
        }

        public IRepository ids_details_pack_list_saveProperty
        {
            get { return this.ids_details_pack_list_save; }
            set { this.ids_details_pack_list_save = value; }
        }
        public IRepository ids_details_invoice_saveProperty
        {
            get { return this.ids_details_invoice_save; }
            set { this.ids_details_invoice_save = value; }
        }
        public IRepository ids_details_pack_listProperty
        {
            get { return this.ids_details_pack_list; }
            set { this.ids_details_pack_list = value; }
        }
        public IRepository ids_details_invoiceProperty
        {
            get { return this.ids_details_invoice; }
            set { this.ids_details_invoice = value; }
        }
        public IRepository ids_inv_pack_detailsProperty
        {
            get { return this.ids_inv_pack_details; }
            set { this.ids_inv_pack_details = value; }
        }
        public u_material_search iuo_material_searchProperty
        {
            get { return this.iuo_material_search; }
            set { this.iuo_material_search = value; }
        }
        public long il_row_noProperty
        {
            get { return this.il_row_no; }
            set { this.il_row_no = value; }
        }
        public long il_new_doc_noProperty
        {
            get { return this.il_new_doc_no; }
            set { this.il_new_doc_no = value; }
        }
        public long il_old_doc_noProperty
        {
            get { return this.il_old_doc_no; }
            set { this.il_old_doc_no = value; }
        }
        public long il_employee_numberProperty
        {
            get { return this.il_employee_number; }
            set { this.il_employee_number = value; }
        }
        public string is_send_decline_msgProperty
        {
            get { return MvcSite.Common.Globals.IsSendDeclineMsg; }
            set
            {
                this.is_send_decline_msg = value;
                MvcSite.Common.Globals.IsSendDeclineMsg = value;
            }
        }
        public long il_start_timeProperty
        {
            get { return this.il_start_time; }
            set { this.il_start_time = value; }
        }
        public string is_start_timeProperty
        {
            get { return this.is_start_time; }
            set { this.is_start_time = value; }
        }
        public void LoadData(Int64 al_material_number, ref long ll_supplier_number, ref long ll_distributor_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT supplier_number,  IsNull(distributor_number, 0) FROM materials WHERE (materials.number = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ll_distributor_number = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(DateTime? ldt_curr_expiration_date, long ll_branch_number, long ll_supplier_number, long ll_doc_no, long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_curr_expiration_date }, { "@1", ll_branch_number }, { "@2", ll_supplier_number }, { "@3", ll_doc_no }, { "@4", ll_material_number } };
                    string sql = "UPDATE invoice_details SET expiration_date = @0 WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND material_number = @4";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData1(DateTime? ldt_curr_expiration_date, long ll_branch_number, long ll_supplier_number, long ll_doc_no, long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_curr_expiration_date }, { "@1", ll_branch_number }, { "@2", ll_supplier_number }, { "@3", ll_doc_no }, { "@4", ll_material_number } };
                    string sql = "UPDATE packing_list_details SET expiration_date = @0 WHERE branch_number = @1 AND supplier_number = @2 AND pack_list_number = @3 AND material_number = @4";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        internal ControlElement GetControlByID(GridElement dw_inv_pack_details,string id)
        {
            if (dw_inv_pack_details.Columns.Count > 0)
            {
                var col = dw_inv_pack_details.Columns[0] as WidgetColumn;
                if (col != null)
                {
                    foreach (var item in col.Items)
                    {
                        if (item.ID == id)
                            return item;
                    }
                }
            }
            return null;
        }

        internal GridColumn GetDocNo(GridElement dw_inv_pack_details)
        {
            return dw_inv_pack_details.Columns["doc_no"];
        }

        public async Task<bool> uf_validate_doc_change(long al_row, string as_col_name, string as_data, GridElement dw_inv_pack_details) //**********************************************************************************************
        {
             
            //*Object:				uo_mini_terminal_base_body
            //*Function Name:	uf_validate_doc_change
            //*Purpose: 			Lock the document or prevent changes if document locked
            //*Arguments: 		Long	 		- al_row			- row we are trying to change
            //*						String 		- as_col_name	- column we are trying to change
            //*						Long	 		- as_doc_no		- document number
            //*Return:				Boolean 	- TRUE/FALSE
            //*Date				Programer		Version	Task#	 							Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*09-07-2009		AlexKh			1.2.45.0	SPUCM00001243 - CR#1119		Initial version
            //************************************************************************************************

            long? ll_current_doc_no = 0;
            long? ll_found = 0;
            long ll_lock = 0;
            long? ll_null = null ;
            char lc_doc_type = '\0';
          //  bool lb_rtn_state = false;
            GridColumn ldwc_docs;

            ll_current_doc_no = dw_inv_pack_details.GetItemValue<long?>(al_row, "doc_no");

            ldwc_docs = GetDocNo(dw_inv_pack_details);
            if (!(ll_current_doc_no == -1) && ll_current_doc_no >= 0)
            {


                ll_found = ldwc_docs.GetRepository().Find("doc_number == " + ll_current_doc_no.ToString(), 0, ldwc_docs.GetRepository().RowCount());
                if (ll_found >= 0)
                {
                    lc_doc_type = Convert.ToChar(ldwc_docs.GetRepository().GetItemValue<string>((int)ll_found, "doc_type"));
                    if (!await uf_invoice_lock("LOCK", lc_doc_type.ToString(), ll_current_doc_no.Value))
                    {

                        dw_inv_pack_details.SetItem(al_row, "doc_no", ll_null);
                        return false;
                    }
                }
            }
            if (as_col_name == "doc_no" && !isempty_stringClass.isempty_string(as_data))
            {


                ll_found = ldwc_docs.GetRepository().Find("doc_number == " + as_data, 0, ldwc_docs.GetRepository().RowCount());
                if (ll_found >= 0)
                {
                    lc_doc_type = Convert.ToChar(ldwc_docs.GetRepository().GetItemValue<string>((int)ll_found, "doc_type"));
                    if (!await uf_invoice_lock("LOCK", lc_doc_type.ToString(), Convert.ToInt64(as_data)))
                    {
                        //dw_inv_pack_details.SetItem(al_row, "doc_no", ll_current_doc_no.ToString());
                        return false;
                    }
                }
            }
            return true;
        }
        public long uf_check_item_scanned_before(long al_material_number, long al_start_row, long al_current_row, GridElement dw_inv_pack_details) //**********************************************************************************************
        {
            //*Object:				uo_mini_terminal_base_body
            //*Function Name:	uf_check_item_scanned_before
            //*Purpose: 			Check if current item was already scanned earlier and the invoice is opened
            //*Arguments: 		Long	 - al_material_number
            //*						Long	 - al_current_row
            //*Return:				Long	 - ll_row
            //*Date				Programer		Version	Task#	 		Description
            //*----------------------------------------------------------------------------------------------
            //*05-08-2009		AlexKh			1.2.45.0	CR#1138		Initial version
            //************************************************************************************************

            long? ll_found = 0;
            string ls_find = null;
            ls_find = "material_number == " + al_material_number.ToString() + " and doc_state == \"O\"";
            if (al_start_row == -1 )
            {
                al_start_row = 0;
            }


            ll_found = dw_inv_pack_details.Find(ls_find, (int)al_start_row, dw_inv_pack_details.RowCount());
            if (ll_found >= 0)
            {
                if (ll_found != al_current_row)
                {
                    return ll_found.Value;
                }

                else if (ll_found < dw_inv_pack_details.RowCount())
                {


                    ll_found = dw_inv_pack_details.Find(ls_find, (int)(ll_found + 1), dw_inv_pack_details.RowCount());
                    if (ll_found > -1)
                    {
                        return ll_found.Value;
                    }
                }
            }
            return -1;
        }

        public virtual async Task<int> uf_show_items(bool ab_all_items) //**********************************************************************************************
        {
            ButtonElement cb_show_all = this.GetVisualElementById<ButtonElement>("cb_show_all");

            WidgetGridElement dw_inv_pack_details = null;
            if(this is uo_mini_terminal_pallet_manual)
            {
                dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("uo_mini_terminal_pallet_manual_dw_inv_pack_details");

            }
            else
            {
                dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details");

            }

            //*Object:								uo_mini_terminal_base_body
            //*Function/Event  Name:			uf_show_items
            //*Purpose:							show  items in dw_inv_pack_details :all items if ab_all_items = TRUE 
            //*										and diff if ab_all_items is FALSE 
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										------------------------------------------------------------------------
            //*										Value 		Boolean 					ab_all_items
            //*
            //*Return:								Integer: 1 - Success.
            //*												  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*17-07-2007    SHARP.      			0.2.0   			B2B 			Initiation
            //************************************************************************************************

            string ls_filter = null;
            string ls_orig_filter = null;
            long ll_row_count = 0;

            ll_row_count = dw_inv_pack_details.RowCount();
            //Load origenal filter of dw



            ls_orig_filter = dw_inv_pack_details.GetFilter();
            if (ab_all_items)
            {
                //Show all items
                ls_filter = "";
                if (ls_orig_filter != ls_filter)
                {

                    dw_inv_pack_details.SetFilter(ls_filter);
                    //		dw_inv_pack_details.Filter()
                }

                dw_inv_pack_details.Filter();
                if (ll_row_count == 0)
                {
                    if (!await uf_is_new_row_exist(dw_inv_pack_details))
                    {
                        await uf_new_row(dw_inv_pack_details);
                    }
                }
                cb_show_all.Text = "��� ������ �� ������";

                ll_row_count = dw_inv_pack_details.RowCount();
            }
            else
            {
                //Show diff items
                ls_filter = "(((invoice_pack_quantity <> order_quantity) or ";
                ls_filter += "(invoice_pack_quantity <> expected_material_quantity) or ";
                ls_filter += "(order_quantity <> expected_material_quantity)) and ";
                ls_filter += "(barcode <> \"\" OR  barcode <> null)) ";
                //ls_filter += "OR (invoice_pack_quantity == null AND expected_material_quantity == null";
                //ls_filter += " AND order_quantity == null AND barcode == null)";
                if (ls_orig_filter != ls_filter)
                {

                    dw_inv_pack_details.SetFilter(ls_filter);
                }

                dw_inv_pack_details.Filter();
                if (ll_row_count == 0)
                {
                    if (!await uf_is_new_row_exist(dw_inv_pack_details))
                    {
                        await uf_new_row(dw_inv_pack_details);
                    }
                }
                cb_show_all.Text = "��� �� �� �������";

                ll_row_count = dw_inv_pack_details.RowCount();
            }


            dw_inv_pack_details.SetSort("row_no");

            dw_inv_pack_details.Sort();
            if (await uf_is_new_row_exist(dw_inv_pack_details))
            {
                ll_row_count = dw_inv_pack_details.RowCount() - 1;

                Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e) =>
                {
                    dw_inv_pack_details.SetFocus((int)ll_row_count, "barcode");
                });


            }
            else
            {
                //ll_row_count = dw_inv_pack_details.RowCount() - 1;
                //Mesofon.Common.Extensions.ExecuteOnResponeEx((obj, e) =>
                //{
                //    dw_inv_pack_details.SetFocus((int)ll_row_count, "expected_material_quantity");
                //});
            }
            return 1;
        }

        public virtual async Task<FuncResults<long, int>> uf_get_selected_supplier_number(long al_supplier_number) //MUST BE OVERRIDDEN
        {
            return FuncResults.Return(al_supplier_number, 1);
        }

        public virtual async Task<FuncResults<long, int>> uf_get_selected_order_number(long al_order_number) //MUST BE OVERRIDDEN
        {
            return FuncResults.Return(al_order_number, 1);
        }

        public async Task<bool> uf_check_303_department(long al_material_number)
        {
            try
            {
                long ll_count = 0;
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT catalog_entities.entity_number FROM items_entities_connections, catalog_entities WHERE items_entities_connections.item_number = @0 AND items_entities_connections.entity_number = catalog_entities.serial_number AND catalog_entities.entity_type_number  = 20";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
                return ll_count == 303;
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
            return false;
        }

        public virtual async Task<bool> uf_delete_item(object sender, EventArgs e)
        {
            return true;
        }

        public virtual async Task<int> uf_enter(string as_column, long al_row) //MUST BE OVERRIDDEN
        {
            return 1;
        }
    }
}
