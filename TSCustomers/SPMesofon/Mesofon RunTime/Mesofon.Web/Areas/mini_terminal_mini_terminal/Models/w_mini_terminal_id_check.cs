using global;
using System.Web.VisualTree.MVC;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:19 (Utc)
	// Comments:        
	// 
	public class w_mini_terminal_id_check : w_password_id
	{
        public w_mini_terminal_id_check(s_password password)
		{
		//	int iCurrent = 0;
		    Password = password;
			VisualElementHelper.CreateFromView<w_password_id>("w_password_id", "w_password_id", "global_global");
		}

	    public s_password Password{ get; set; }
		
	}
}
