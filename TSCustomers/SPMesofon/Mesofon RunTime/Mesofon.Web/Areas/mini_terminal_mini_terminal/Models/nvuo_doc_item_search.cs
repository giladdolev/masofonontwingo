using System;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using global;
using Mesofon.Repository;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using Mesofon.Common;

namespace  mini_terminal
{
    // Creation Time:   08/11/2015 10:21:08 (Utc)
    // Comments:        
    // 

    public class nvuo_doc_item_search
    {
        public u_datastore_update ids_inv_move;     
        //.. data from invoice_details
        public u_datastore_update ids_inv_details;
        //.. data from invoice_move
        public u_datastore_update ids_pack_move;
        //.. data from packing_list_details
        public u_datastore_update ids_pack_details;
        //.. data from packing_list_move
        //.. connection to DB by default connected to SQLCA
        public SQLError i_transaction;
        //.. show mini terminial messagebox or regular messagebox
        public bool ib_mt_message;
        //.. if some of these parameters was changed then order ds`s will be reretrieved
        public long il_branch_number;
        //.. current branch number
        public long il_supplier_number;
        //.. current supplier number
        public long il_order_number;
        //.. current order number
        public u_datastore_update ids_order_details;
        //.. data from supplier_order_move
        public u_datastore_update ids_order_move;
        //.. data from supplier_order_details
        //.. errors of order ds`s retrieve
        public long il_last_sql_db_code;
        public string is_last_sql_err_text;
        //.. if one branch, order, supplier was modified -> reretrieve data
        public bool ib_retrieve_needed = true;
        public void uf_set_mt_message(bool ab_mt_message) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_set_mt_message
            //*Purpose:							Set the message window type, regular or mini terminal.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			boolean					ab_mt_message
            //*
            //*Return:								None
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	  SHARP.						0.2.0			B2B			
            //************************************************************************************************

            ib_mt_message = ab_mt_message;
        }
        public void uf_set_transaction(SQLError a_transaction) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_set_transaction
            //*Purpose:							Set the current supplier number.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			transaction				a_transaction
            //*
            //*Return:								None
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.					0.2.0			B2B			
            //************************************************************************************************

            i_transaction = a_transaction;
        }
        public void uf_set_branch_number(long al_branch_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_set_branch_number
            //*Purpose:							Set the current branch number.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long						al_branch_number
            //*
            //*Return:								None
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.   					0.2.0			B2B			
            //************************************************************************************************

            il_branch_number = al_branch_number;
            //ib_modified = TRUE
        }
        public void uf_set_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_set_order_number
            //*Purpose:							Set the current order number.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long					  al_order_number
            //*
            //*Return:								None
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.					0.2.0			B2B			
            //************************************************************************************************

            il_order_number = al_order_number;
            ib_retrieve_needed = true;
        }
        public void uf_set_supplier_number(long al_supplier_number)
        {
            //**********************************************************************************************
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_set_supplier_number
            //*Purpose:							Set the current supplier number.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long					  al_supplier_number
            //*
            //*Return:								None
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.					0.2.0			B2B			
            //************************************************************************************************
            il_supplier_number = al_supplier_number;
            ib_retrieve_needed = true;
        }
        public bool uf_item_exist(long al_material_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_item_exist
            //*Purpose:							Check if material exists in the invoce/packlist, 
            //*
            //*Arguments:							None
            //*										Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long						al_material_number-current material number.
            //*
            //*Return:								BOOLEAN - TRUE - The material exists in the invoce/packlist 
            //*													 FALSE - Othervise:-The material does not exists in the invoce/packlist .
            //*
            //*Date							Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007				SHARP.					0.2.0			B2B
            //************************************************************************************************

            long? ll_row = 0;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }


            ll_row = ids_inv_details.Find("material_number == " + al_material_number.ToString(), 0, ids_inv_details.RowCount());
            if (ll_row < 0)
            {


                ll_row = ids_pack_details.Find("material_number == " + al_material_number.ToString(), 0, ids_pack_details.RowCount());
            }
            return (ll_row >= 0);
        }
        public bool uf_get_doc_number_and_quantity(long al_material_number, ref long al_doc_number, ref decimal adec_item_quantity) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_doc_number_and_quantity
            //*Purpose:							If material exists in the invoice or packlist, 
            //*										get the invoice or packlist number material quantity.
            //*
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_material_number - current material number		
            //*										Reference	Long					al_doc_number - invoice/packlist number
            //*										Reference   Decimal				adec_item_quantity - item quantity in the invoice/packlist
            //*
            //*Return:								Boolean: TRUE - Success.
            //*													FALSE - Failure.
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	SHARP.					0.2.0			B2B			Initial verstion
            //************************************************************************************************

            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "material_number == " + al_material_number.ToString();


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < 0)
            {
                return false;
            }
            if (ll_row >= 0)
            {

                al_doc_number = Convert.ToInt64(ids_inv_details.GetItemValue<decimal>((int)ll_row, "invoice_number"));

                adec_item_quantity = ids_inv_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
                return true;
            }
            else
            {


                ll_row = ids_pack_details.Find(ls_find, 0, ids_inv_details.RowCount());
                if (ll_row < 0)
                {
                    return false;
                }
                if (ll_row >= 0)
                {

                    al_doc_number = Convert.ToInt64(ids_pack_details.GetItemValue<decimal>((int)ll_row, "invoice_number"));

                    adec_item_quantity = ids_pack_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
                    return true;
                }
                al_doc_number = 0;
                adec_item_quantity = 0;
                return false;
            }
        }
        public bool uf_invoice_exists(long al_invoice_number, ref long al_error_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_invoice_exists
            //*Purpose:							Checks if entered invoice number exists in DB
            //*										(in invoice_details table).
            //*
            //*Arguments:							None
            //*										Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long						al_invoice_number
            //*										Reference	long						al_error_number
            //*
            //*Return:								BOOLEAN - TRUE - Entered invoice number exists in DB
            //*													 FALSE - Othervise
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.					0.2.0			B2B			
            //************************************************************************************************

            bool lb_return = false;
            lb_return = true;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            if (al_invoice_number == 0 || al_invoice_number == 0)
            {
                return false;
            }
            return lb_return;
        }
        public bool uf_retrieve() //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_retrieve
            //*Purpose:							If (supplier || branch || order numbers) = 0 then do no retrieve DS`s.
            //*										Otherwise retrieve invoice_details,
            //*										invoice_move, packing_list_details and packing_list_move.
            //*										If error occures save error number and message.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*									   None
            //*
            //*Return:								BOOLEAN: TRUE - Retrieve succeeded
            //*													FALSE - Retrieve failed.
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	SHARP.					0.2.0			B2B			
            //************************************************************************************************

            long ll_count = 0;
            long ll_employee_no = 0;
            long ll_inv_move = 0;
            long ll_pack_move = 0;
            long ll_inv_details = 0;
            long ll_pack_details = 0;
            if (il_branch_number == 0)
            {
                return false;
            }
            if (il_supplier_number == 0)
            {
                return false;
            }
            if (il_order_number == 0)
            {
                return false;
            }
            //.. If error occures save error number and message

            if (ids_inv_details.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0)
            {
                il_last_sql_db_code = ids_inv_details.uf_get_last_sql_db_code();
                is_last_sql_err_text = ids_inv_details.uf_get_last_sql_err_text();
                return false;
            }
			//.. If error occures save error number and message

			if (ids_inv_move.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0)
			{
				il_last_sql_db_code = ids_inv_move.uf_get_last_sql_db_code();
				is_last_sql_err_text = ids_inv_move.uf_get_last_sql_err_text();
				return false;
			}
			//.. If error occures save error number and message
			
			if (ids_pack_details.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0)
			{
				il_last_sql_db_code = ids_pack_details.uf_get_last_sql_db_code();
				is_last_sql_err_text = ids_pack_details.uf_get_last_sql_err_text();
				return false;
			}
			//.. If error occures save error number and message
			
			if (ids_pack_move.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0)
			{
				il_last_sql_db_code = ids_pack_move.uf_get_last_sql_db_code();
				is_last_sql_err_text = ids_pack_move.uf_get_last_sql_err_text();
				return false;
			}
            //.. Order data succesfully retrieved
            ib_retrieve_needed = false;
            // SharonS - 1.2.31.0 - 2008-05-07 - Task#10004 - Set employee number if needed
            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-08-07 - Task#10004 - Equal

            ll_inv_move = ids_inv_move.RowCount();
            //FOR ll_count = 1 TO ids_inv_move.RowCount()
            for (ll_count = 0; ll_count < ll_inv_move; ll_count++)
            {
                // End
                ll_employee_no = ids_inv_move.GetItemValue<long>((int)ll_count, "employee_number");
                if (ll_employee_no == 0)
                {
                    ids_inv_move.SetItem(ll_count, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner);
                }
            }
            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-08-07 - Task#10004 - Equal

            ll_pack_move = ids_pack_move.RowCount();
            //FOR ll_count = 1 TO ids_pack_move.RowCount()
            for (ll_count = 0; ll_count < ll_pack_move; ll_count++)
            {
                // End
                ll_employee_no = ids_pack_move.GetItemValue<long>((int)ll_count, "employee_number");
                if (ll_employee_no == 0)
                {
                    ids_pack_move.SetItem(ll_count, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());
                }
            }
            // End
            return true;
        }
        public bool uf_get_doc_vat(long al_doc_number, ref decimal adec_vat_percent) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_doc_vat
            //*Purpose:							Get invoice or packlist vat percent, and return it.
            //*
            //*Arguments:						Pass By		Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long		al_doc_number - invoice or packlist number	
            //*										Reference	Decimal	adec_vat_percent - invoice or packlist vat percent
            //*
            //*Return:								Boolean: TRUE - Success.
            //*												  	FALSE - Failure
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-01-2008		SharonS					1.2.23.0		10001			Initial version
            //************************************************************************************************

            long? ll_row = 0;
            long ll_doc_number = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString();


            ll_row = ids_inv_move.Find(ls_find, 0, ids_inv_move.RowCount());
            if (ll_row < -1)
            {
                return false;
            }
            if (ll_row > -1)
            {

                adec_vat_percent = ids_inv_move.GetItemValue<int>((int)ll_row, "mam");
            }
            else
            {
                ls_find = "pack_list_number == " + al_doc_number.ToString();


                ll_row = ids_pack_move.Find(ls_find, 0, ids_pack_move.RowCount());
                if (ll_row < -1)
                {
                    return false;
                }
                if (ll_row > -1)
                {

                    adec_vat_percent = ids_pack_move.GetItemValue<int>((int)ll_row, "mam");
                }
                else
                {
                    adec_vat_percent = 0;
                }
            }
            return true;
        }
        public bool uf_get_item_serial_number(long al_doc_number, long al_material_number, long al_item_serial_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_item_serial_number
            //*Purpose:							Get the item serial number in invoice_details table.
            //*
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_doc_number - Invoice number
            //*										Value			Long					al_material_number
            //*										Reference	Long					al_item_serial_number
            //*
            //*Return:								Boolean: 
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20/12/2007		SharonS					0.2.0			B2B			Initial version
            //************************************************************************************************

            long? ll_row = 0;
            long ll_item_serial_number = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "material_number == " + al_material_number.ToString() + " and invoice_number == " + al_doc_number.ToString();


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < -1)
            {
                return false;
            }
            if (ll_row > -1)
            {

                ll_item_serial_number = ids_inv_details.GetItemValue<long>((int)ll_row, "invoice_details_serial_number");
            }
            else
            {
                ls_find = "material_number == " + al_material_number.ToString() + " and pack_list_number == " + al_doc_number.ToString();


                ll_row = ids_pack_details.Find(ls_find, 0, ids_pack_details.RowCount());
                if (ll_row < -1)
                {
                    return false;
                }
                if (ll_row > -1)
                {

                    ll_item_serial_number = ids_pack_details.GetItemValue<long>((int)ll_row, "packing_list_details_serial_number");
                }
                else
                {
                    ll_item_serial_number = 0;
                }
            }
            return true;
        }
        public bool uf_get_doc_item_price(long al_doc_number, long al_material_number, ref decimal adec_material_price) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_doc_item_quantity
            //*Purpose:							Get a material price in the invoice or packlist that was set previously,
            //*										of the supplier that was set previously.
            //*
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_material_number - current material number		
            //*
            //*Return:								Decimal: material price of item in invoice or packlist (if exists).
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*28/01/2008		SharonS					1.2.23.0		10001			Initial version		
            //************************************************************************************************

            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            ls_find = "invoice_number == " + al_doc_number.ToString() + " and material_number == " + al_material_number.ToString();


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < -1)
            {
                return false;
            }
            if (ll_row > -1)
            {

                adec_material_price = ids_inv_details.GetItemValue<decimal>((int)ll_row, "material_price");
            }
            else
            {
                ls_find = "pack_list_number == " + al_doc_number.ToString() + " and material_number == " + al_material_number.ToString();


                ll_row = ids_pack_details.Find(ls_find, 0, ids_pack_details.RowCount());
                if (ll_row < -1)
                {
                    return false;
                }
                if (ll_row > -1)
                {

                    adec_material_price = ids_pack_details.GetItemValue<decimal>((int)ll_row, "material_price");
                }
                else
                {
                    adec_material_price = -1;
                }
            }
            return true;
        }
        public decimal uf_get_doc_item_quantity(long al_material_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_doc_item_quantity
            //*Purpose:							Get a material quantity in the invoice or packlist that was set previously,
            //*										of the supplier that was set previously.
            //*
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_material_number - current material number		
            //*
            //*Return:								Decimal: material quantity of item in invoice or packlist (if exists).
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	SHARP.					0.2.0			B2B			
            //************************************************************************************************

            long? ll_row = 0;
            decimal ldec_material_quantity = default(decimal);
            decimal ldec_material_bonus_quantity = default(decimal);
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            ls_find = "material_number == " + al_material_number.ToString();


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < -1)
            {
                return -1;
            }
            if (ll_row > -1)
            {

                ldec_material_quantity = ids_inv_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
                //ldec_material_bonus_quantity = ids_inv_details.GetItem<int>(ll_row, "invoice_details_bonus_quantity")
            }
            else
            {


                ll_row = ids_pack_details.Find(ls_find, 0, ids_pack_details.RowCount());
                if (ll_row < -1)
                {
                    return -1;
                }
                if (ll_row > -1)
                {

                    ldec_material_quantity = ids_pack_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
                    //ldec_material_bonus_quantity = ids_pack_details.GetItem<int>(ll_row, "packing_list_details_bonus_quantity")
                }
                else
                {
                    ldec_material_quantity = 0;
                    //ldec_material_bonus_quantity = 0
                }
            }
            //ldec_material_quantity = ldec_material_quantity + ldec_material_bonus_quantity 
            return ldec_material_quantity;
        }
        public decimal uf_get_doc_item_quantity(long al_doc_number, long al_material_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_doc_item_quantity
            //*Purpose:							Get a material quantity in the invoice or packlist that was set previously,
            //*										of the supplier that was set previously.
            //*
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					al_doc_number - current doc (invoice/packlist) number
            //*										Value			Long					al_material_number - current material number
            //*
            //*Return:								Decimal: material quantity of item in invoice or packlist (if exists).
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	SHARP.					0.2.0			B2B			
            //************************************************************************************************

            long? ll_row = 0;
            decimal ldec_material_quantity = default(decimal);
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            ls_find = "invoice_number = " + al_doc_number.ToString() + " and material_number == " + al_material_number.ToString();


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < -1)
            {
                return -1;
            }
            if (ll_row > -1)
            {

                ldec_material_quantity = ids_inv_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
            }
            else
            {
                ls_find = "pack_list_number == " + al_doc_number.ToString() + " and material_number == " + al_material_number.ToString();


                ll_row = ids_pack_details.Find(ls_find, 0, ids_pack_details.RowCount());
                if (ll_row < -1)
                {
                    return -1;
                }
                if (ll_row > -1)
                {

                    ldec_material_quantity = ids_pack_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
                }
                else
                {
                    ldec_material_quantity = 0;
                }
            }
            return ldec_material_quantity;
        }
        public long uf_get_item_doc_number(long al_material_number, ref long[] al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_get_item_doc_number
            //*Purpose:							If material exists in invoice or packlist,
            //*										return the invoice or packlist number,
            //*										otherwise return 0.
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			Long					   al_material_number - current material number		
            //
            //*Return:								LONG: >0 - If material exists in invoice or packlist,return the invoice or packlist number,
            //*												  0 - otherwise
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007	SHARP.					0.2.0			B2B			
            //************************************************************************************************

            // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-07-27 - Task#10012 - SPUCM00000094 - Change the function to return a list of document numbers in an array
            //LONG		ll_row, &
            //				ll_doc_number
            //STRING		ls_find
            //	
            //SetNull(ll_doc_number)
            //
            //IF uf_retrieve_needed() THEN
            //	IF NOT uf_retrieve() THEN RETURN -1
            //END IF
            //
            //ls_find = "material_number = " + String(al_material_number)
            //
            //ll_row = ids_inv_details.Find(ls_find, 1, ids_inv_details.RowCount())
            //IF ll_row < 0 THEN RETURN -1
            //IF ll_row > 0 THEN
            //	ll_doc_number = ids_inv_details.GetItem<int>(ll_row, "invoice_number")
            //ELSE
            //	ll_row = ids_pack_details.Find(ls_find, 1, ids_pack_details.RowCount())
            //	IF ll_row < 0 THEN RETURN -1
            //	IF ll_row > 0 THEN
            //		ll_doc_number = ids_pack_details.GetItem<int>(ll_row, "pack_list_number")
            //	ELSE
            //		ll_doc_number = 0
            //	END IF
            //END IF
            //
            //RETURN ll_doc_number
            long ll_doc_num = 0;
            long ll_row = 0;
            long ll_count = 0;
            string ls_orig_filter = null;
            string ls_filter = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            //ls_orig_filter = ids_inv_details.
            // SharonS - 1.2.37.4 - 2008-11-18 - Task#10012 - SPUCM00000094
            //ls_filter = "material_number = " + String(al_material_number)
            ls_filter = "material_number = " + al_material_number.ToString() + " and doc_state = \"O\"";
            // End
            // Galilcs - TODO: fix the Filter
            ll_row = ids_inv_details.SetFilter(ls_filter);

            ll_row = ids_inv_details.Filter();

            if (ids_inv_details.RowCount() == 1)
            {
                al_doc_number = new long[1];
                al_doc_number[0] = ids_inv_details.GetItemValue<long>(0, "invoice_number");
            }

            else if (ids_inv_details.RowCount() > 1)
            {
                al_doc_number = new long[ids_inv_details.RowCount()];
                for (ll_row = 0; ll_row < ids_inv_details.RowCount(); ll_row++)
                {

                    ll_doc_num = ids_inv_details.GetItemValue<long>((int)ll_row, "invoice_number");
                    al_doc_number[ll_row] = ll_doc_num;
                }
            }

            ll_row = ids_inv_details.SetFilter("");

            ll_row = ids_inv_details.Filter();

            ll_row = ids_pack_details.SetFilter(ls_filter);

            ll_row = ids_pack_details.Filter();

            if (ids_pack_details.RowCount() == 1)
            {
                al_doc_number = new long[1];
                al_doc_number[0] = ids_pack_details.GetItemValue<long>(0, "pack_list_number");
            }

            else if (ids_pack_details.RowCount() > 1)
            {

                for (ll_row = 0; ll_row <= ids_pack_details.RowCount(); ll_row++)
                {

                    ll_doc_num = ids_pack_details.GetItemValue<long>((int)ll_row, "pack_list_number");
                    al_doc_number[ll_row] = ll_doc_num;
                }
            }

            ll_row = ids_pack_details.SetFilter("");

            ll_row = ids_pack_details.Filter();
            return CollectionExtensions.UBound(al_doc_number) == -1 ? 0: CollectionExtensions.UBound(al_doc_number);
        }
        public bool uf_is_item_b2b(long al_material_number, long al_doc_number) //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_is_item_b2b
            //*Purpose:							Check if material exists in the invoce/packlist, 
            //*
            //*Arguments:							None
            //*										Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			long						al_material_number-current material number.
            //*
            //*Return:								Boolean - TRUE - The material exists in the invoce/packlist 
            //*													 FALSE - Othervise:-The material does not exists in the invoce/packlist .
            //*
            //*Date							Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007				SHARP.					0.2.0			B2B
            //************************************************************************************************

            long? ll_row = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            // SharonS - 1.2.37.2 - 2008-11-09 - Task#10012 - SPUCM00000094
            ls_find = "invoice_number == " + al_doc_number.ToString() + " AND material_number == " + al_material_number.ToString() + " AND b2b_status == 1 ";
            //ls_find = "material_number = " + String(al_material_number) +" AND b2b_status = 1 "
            // End


            ll_row = ids_inv_details.Find(ls_find, 0, ids_inv_details.RowCount());
            if (ll_row < 0)
            {
                // SharonS - 1.2.37.2 - 2008-11-09 - Task#10012 - SPUCM00000094
                ls_find = "pack_list_number == " + al_doc_number.ToString() + " AND material_number == " + al_material_number.ToString() + " AND b2b_status == 1 ";
                // End


                ll_row = ids_pack_details.Find(ls_find, 0, ids_pack_details.RowCount());
            }
            return (ll_row >= 0);
        }
        protected bool uf_retrieve_needed() //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			uf_retrieve_needed
            //*Purpose:							If invoice and packing list datastores not retrived yet
            //*										or (supplier || branch || order) was changed reretrive is needed.
            //*
            //*Arguments:							Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										None
            //*
            //*Return:								BOOLEAN: TRUE  - Retrieve is needed
            //*													FALSE - Retrieve is not needed.
            //*
            //*Date					Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*October 2007		SHARP.					0.2.0			B2B			
            //************************************************************************************************

            //IF (ids_inv_move.RowCount() = 0 AND ids_pack_move.RowCount() = 0 AND &
            //		ids_inv_details.RowCount() = 0 AND ids_pack_details.RowCount() = 0) OR ib_retrieve_needed THEN 
            //	RETURN TRUE
            //END IF
            //
            //RETURN FALSE
            return ib_retrieve_needed;
        }
        public nvuo_doc_item_search()
        {
            constructor();
        }

        public void constructor() //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			constructor
            //*Purpose:							Initiate variables.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								LONG: 1 - Success, -1 - Failure.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*06-08-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            //.. connect to sqlca by default
            //i_transaction = masofonAlias.masofon.Instance.sqlca;
            //ids_inv_move = VisualElementHelper.CreateFromView<u_datastore_update>("d_invoice_move", "d_invoice_move", "mini_terminal_mini_terminal");
            //ids_inv_details = VisualElementHelper.CreateFromView<u_datastore_update>("d_invoices_details", "d_invoices_details", "mini_terminal_mini_terminal");
            //ids_pack_move = VisualElementHelper.CreateFromView<u_datastore_update>("d_packing_list_move", "d_packing_list_move", "mini_terminal_mini_terminal");
            //ids_pack_details = VisualElementHelper.CreateFromView<u_datastore_update>("d_packing_list_details", "d_packing_list_details", "mini_terminal_mini_terminal");


            ids_inv_move = new u_datastore_update();
            ids_inv_move.SetRepository(new d_invoice_moveRepository(), false, true);
            ids_inv_details = new u_datastore_update();
            ids_inv_details.SetRepository(new d_invoices_detailsRepository(), false, true);
            ids_pack_move = new u_datastore_update();
            ids_pack_move.SetRepository(new d_packing_list_moveRepository(), false, true);
            ids_pack_details = new u_datastore_update();
            ids_pack_details.SetRepository(new d_packing_list_detailsRepository(),false,true);
        }
        public void destructor() //**********************************************************************************************
        {
            //*Object:								nvuo_doc_item_search
            //*Function/Event  Name:			destructor
            //*Purpose:							Destroy all non visual objects.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								LONG.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-08-2007		SHARP.					0.2.0			B2B 			Initiation
            //************************************************************************************************

            if (ids_inv_move != null)
            {

           

            }
            if (ids_inv_details != null)
            {


            }
            if (ids_pack_move != null)
            {


            }
            if (ids_pack_details != null)
            {



            }
        }
    }
}
