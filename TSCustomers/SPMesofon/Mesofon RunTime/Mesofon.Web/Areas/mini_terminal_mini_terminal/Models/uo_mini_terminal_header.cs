using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using global;
using Mesofon.Common.Global;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Mesofon.Common;
using Mesofon.Repository;

namespace mini_terminal
{
    // Creation Time:   08/02/2016 08:16:41 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_header : CompositeElement
    {
        public ComboBoxElement idwc_supplier_name;
        //transaction      			SQLCA_REPORTS
        public s_reports_db istr_reports_db;
        public IRepository ids_header_inv_save;
        public IRepository ids_header_pack_save;
        public long il_employee_number;
        public char ic_old_doc_type = default(char);
        public char ic_new_doc_type = default(char);
        // SharonS - 1.2.26.8 - 2008-03-18 - Decline - ib_send_decline_msg
        public string is_send_decline_msg; //Search at supplier_order_move/details
        public u_order_search iuo_order_search;
        // AlexKh - 1.1.26.0 - 2015-01-04 - SPUCM00005131 - crossdoc distributor constant
        public const int CROSSDOC_DISTRIBUTOR = 100;
        public bool ib_sql_error_flag;
        //If true -> show error with sql syntax
        public string is_last_sql_error_text;
        public delegate bool ue_new_doc_EventHandler();
        public event mini_terminalAlias.uo_mini_terminal_header.ue_new_doc_EventHandler ue_new_docEvent;
        public delegate void ue_save_data_EventHandler();
        public event mini_terminalAlias.uo_mini_terminal_header.ue_save_data_EventHandler ue_save_dataEvent;
        public delegate bool ue_validation_check_EventHandler();
        public event mini_terminalAlias.uo_mini_terminal_header.ue_validation_check_EventHandler ue_validation_checkEvent;
        public bool oneSelectedOrderNum = false;

        public IRepository supplier_orderRepository;

        public bool ue_new_doc() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			ue_new_doc
            //*Purpose:							Call the funcion ue_new_row.
            //*  
            //*Arguments:						None.
            //*Return:								Boolean: TRUE - Succeeded.
            //*												 	FALSE - Failed.
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007		SHARP  			0.2.0			B2B 			Initiation
            //************************************************************************************************
            //dw_header_ue_new_row();
            var ls_message = "*************************** ue_new_doc*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:     " + "ue_new_doc The Data:  " + "Event new doc" + "\r" + "\n";
            SystemHelper.WriteToLog(ls_message);
            return true;
        }
        public int uf_write_log(string as_message) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_write_log
            //*Purpose:							Write into log file.
            //*  
            //*Arguments:						Pass By		Argument Type		Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value			String					as_message: the message to write into the file.
            //*
            //*Return:								Integer: 1 for Success
            //*												  -1  for Failure.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007     SHARP.					0.2.0  		B2B 			Initiation
            //***********************************************************************************************************************************

            SystemHelper.WriteToLog(as_message);
            return 1;
        }

        internal void EditCellByPosition(WidgetGridElement dw_body, string columnID, int row)
        {
            dw_body.SetFocus(row, columnID);
        }

        public int uf_set_focus(string as_dw_name, long al_row, string as_column) //**********************************************************************************************
        {
            ComboBoxElement supplier_name = this.GetVisualElementById<ComboBoxElement>("supplier_name");
            ComboGridElement order_number = this.GetVisualElementById<ComboGridElement>("order_number");
            TextBoxElement supplier_number = this.GetVisualElementById<TextBoxElement>("supplier_number");
            WidgetGridElement dw_body = this.GetVisualElementById<WidgetGridElement>("dw_body");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_set_focus
            //*Purpose:							Set focus according to the arguments.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value 		String 					as_dw_name 					
            //*										Value 		Long	 					al_row		
            //*										Value 		String 					as_column	
            //*
            //*Return:								Integer: 1 for Success
            //*												  -1 Otherwise.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*10-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            switch (as_dw_name)
            {
                case "dw_body":
                    dw_body.Focus();
                    dw_body.SetColumn(as_column);
                    dw_body.SetRow((int) al_row);
                    EditCellByPosition(dw_body, as_column, (int)al_row);
                    //if (dw_body.SelectText(1, 20) == -1)
                    //{
                    //    return -1;
                    //}


                    break;
                case "dw_header":
                    supplier_number.Focus();
                     
                    //if (dw_header.SetRow((int)al_row) == -1)
                    //{
                    //    return -1;
                    //}
                    //if (dw_header.SetColumn(as_column) == -1)
                    //{
                    //    return -1;
                    //}
                    //if (dw_header.SelectText(1, 20) == -1)
                    //{
                    //    return -1;
                    //}
                    break;
                default:
                    break;
            }
            return 1;
        }
        public uo_mini_terminal_header()
        {
           
        }

        public async Task<int> uf_build_doc_number_list() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_build_doc_number_list
            //*Purpose:							Initiate the doc number list box with the invoices numbers and packung list numbers 
            //*										from uo_mini_terminal_header.dw_body
            //*  
            //*Arguments:						None.
            //*Return:								Integer: 1 - Success 
            //*												   -1 - Otherwise
            //*Date				Programer		Version					SPUCM		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*30-07-2008		SharonS			1.2.36.0->1.2.37.8 	00000094	10012		Initiation
            //************************************************************************************************

            int li_rtn_code = 0;


            li_rtn_code = await ((w_mini_terminal)this.Parent.Parent).wf_build_doc_number_list();
            if (li_rtn_code < 1)
            {
                return -1;
            }
            return 1;
        }

        public async Task constructor() //**********************************************************************************************
        {
            RadioButtonElement rb_print_pdf = GetVisualElementById<RadioButtonElement>("rb_print_pdf");
            //GridElement dw_header = this.GetVisualElementById<GridElement>("dw_header");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			constructor
            //*Purpose:							Load the drop down data window of the suppliers (supplier_name field)
            //*										and create non visual objects.
            //*  
            //*Arguments:						None.
            //*
            //*Return:								Long.
            //*
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.			0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_RtnCode = 0;
            //GridElement ldwc_order;
            this.iuo_order_searchProperty = new u_order_search();
            this.iuo_order_searchProperty.uf_set_mt_message(true);
            this.iuo_order_searchProperty.uf_set_branch_number(masofonAlias.masofon.Instance.gs_vars.branch_number);
            li_RtnCode = await this.uf_load_suppliers();
            if (li_RtnCode < 0)
            {


                ((WindowElement)this.Parent).Close();
            }
            // AlexKh - 1.1.1.6 - 2013-03-31 - SPUCM00004018 - insert empty row to prevent application crash

            //ldwc_order = dw_header.GetVisualElementById<GridElement>("order_number");


             
            //ldwc_order.Insert(0);
            rb_print_pdf.IsChecked = true; //ron@22/03/2015>>SPUCM00005274
            // Galilcs - TODO: uf_resize();
            // uf_resize();
        }

        public async System.Threading.Tasks.Task<int> uf_load_suppliers() //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_load_suppliers
            //*Purpose:							Load the drop down data window of the suppliers(supplier_name field).
            //*  
            //*Arguments:						None.
            //*Return:								Integer: 1 - succeeded.
            //*												  -1 - failed.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*08-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            long ll_rows = 0;
            // Get DB
            // Galilcs - TODO  switch (this.istr_reports_dbProperty.status)
            //this.istr_reports_dbProperty = await masofonAlias.masofon.Instance.gnvo_reports_db.uf_get_db_transaction("uo_mini_terminal_header", false);
            //switch (this.istr_reports_dbProperty.status)
            //{
            //    case "NON":
            //        this.uf_show_message("Error uo_mini_terminal_header", "", "OK", "Cann't connect to DB");
            //        return 0;
            //    //..Report will run entirely on LIVE DB
            //    case "MAIN":
            //        masofonAlias.masofon.Instance.SQLCA_REPORTS = masofonAlias.masofon.Instance.sqlca;
            //        //..Report will run entirely on Reports DB
            //        break;
            //    case "REP":
            //        masofonAlias.masofon.Instance.SQLCA_REPORTS = this.istr_reports_dbProperty.report_tran;
            //        //..Report will runentirely on LIVE DB
            //        break;
            //    case "SPLIT":
            //        masofonAlias.masofon.Instance.SQLCA_REPORTS = masofonAlias.masofon.Instance.sqlca;
            //        break;
            //    default:
            //        this.uf_show_message("Error uo_mini_terminal_header", "", "OK", "Cann't connect to DB");
            //        return 0;
            //}

            _d_mt_dddw_suppliers_listRepository = new d_mt_dddw_suppliers_listRepository();
            _d_mt_dddw_suppliers_listRepository.Retrieve();
            idwc_supplier_nameProperty = GetVisualElementById<ComboBoxElement>("supplier_name");
            idwc_supplier_nameProperty.DisplayMember = "name";
            idwc_supplier_nameProperty.ValueMember = "name";
            idwc_supplier_nameProperty.SetRepository(_d_mt_dddw_suppliers_listRepository);
            


            ll_rows = idwc_supplier_nameProperty.RowCount();
            if (ll_rows == 0 || ll_rows <= 0)
            {
                await this.uf_show_message("Database Error", "", "OK", "No rows retrieved.");
                return -1;
            }
            return 1;
        }


     

        public void dw_header_ue_new_row() //**********************************************************************************************
        {
            GridElement dw_header = this.GetVisualElementById<GridElement>("dw_header");
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			dw_header.ue_new_row
            //*Purpose:							Insert new row into the DataWindow.
            //*  
            //*Arguments:						None.
            //*Return:								None.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*25-07-2007		SHARP.					0.2.0  			B2B 			Initiation
            //************************************************************************************************

            long ll_row = 0;
            int li_ret_code = 0;
            
            dw_header.Reset();
            
            ll_row = dw_header.Insert(0);
            li_ret_code = this.uf_set_focus("dw_header", ll_row, "supplier_number");
            this.uf_set_focus("dw_header", ll_row, "supplier_number");

            dw_header.Modify("DataWindow", "NoUserPrompt", "Yes");
        }

        private IRepository _d_mt_dddw_suppliers_listRepository;
        public IRepository D_mt_dddw_suppliers_listRepository
        {
            get { return _d_mt_dddw_suppliers_listRepository; }
            set { _d_mt_dddw_suppliers_listRepository = value; }
        }
        public ComboBoxElement idwc_supplier_nameProperty
        {
            get { return this.idwc_supplier_name; }
            set { this.idwc_supplier_name = value; }
        }
        public s_reports_db istr_reports_dbProperty
        {
            get { return this.istr_reports_db; }
            set { this.istr_reports_db = value; }
        }
        public long il_employee_numberProperty
        {
            get { return this.il_employee_number; }
            set { this.il_employee_number = value; }
        }
        public char ic_old_doc_typeProperty
        {
            get { return this.ic_old_doc_type; }
            set { this.ic_old_doc_type = value; }
        }
        public char ic_new_doc_typeProperty
        {
            get { return this.ic_new_doc_type; }
            set { this.ic_new_doc_type = value; }
        }
        public string is_send_decline_msgProperty
        {
            get { return this.is_send_decline_msg; }
            set { this.is_send_decline_msg = value; }
        }
        public u_order_search iuo_order_searchProperty
        {
            get { return this.iuo_order_search; }
            set { this.iuo_order_search = value; }
        }
        public void LoadData(int branch_number ,long ll_supplier_number, long ll_invoice_number, ref string ls_carton_barcode)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_supplier_number }, { "@2", ll_invoice_number } };
                    string sql = "SELECT TOP 1 carton_barcode FROM shipment_crossdoc WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2 AND state = 's'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_carton_barcode = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_supplier_number, long ll_invoice_number, ref string ls_full_path)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ll_supplier_number }, { "@2", ll_invoice_number } };
                    string sql = "SELECT IsNull(digital_invoice_path, '') FROM invoice_move WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2 AND invoice_type = 'p'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_full_path = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_new_log_book, long ll_supplier_number, long ll_doc_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_new_log_book }, { "@2", ll_supplier_number }, { "@3", ll_doc_number } };
                    string sql = "SELECT COUNT(invoice_move.log_book) FROM invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.log_book = @1 AND invoice_move.supplier_number = @2 AND invoice_move.invoice_number <> @3";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 al_new_log_book, long ll_doc_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_new_log_book }, { "@2", ll_doc_number } };
                    string sql = "SELECT COUNT(invoice_move.log_book) FROM invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.log_book = @1 AND invoice_move.invoice_number <> @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(Int64 al_new_log_book, long ll_supplier_number, long ll_doc_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_new_log_book }, { "@2", ll_supplier_number }, { "@3", ll_doc_number } };
                    string sql = "SELECT COUNT(packing_list_move.log_book) FROM packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.log_book = @1 AND packing_list_move.supplier_number = @2 AND packing_list_move.pack_list_number <> @3";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(Int64 al_new_log_book, long ll_doc_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_new_log_book }, { "@2", ll_doc_number } };
                    string sql = "SELECT COUNT(packing_list_move.log_book) FROM packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.log_book = @1 AND packing_list_move.pack_list_number <> @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(Int64 al_doc_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_doc_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM packing_list_move WHERE pack_list_number = @0 AND branch_number = @1 AND supplier_number = @2 AND packing_type = 'p'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(Int64 al_doc_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_doc_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM invoice_move WHERE invoice_number = @0 AND branch_number = @1 AND supplier_number = @2 AND invoice_type = 'p'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(Int64 al_doc_number, long ll_supplier_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_doc_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier_number } };
                    string sql = "SELECT 1 FROM invoice_move WHERE invoice_number = @0 AND branch_number = @1 AND supplier_number = @2 AND invoice_type = 'r'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public async Task<long> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_show_message
            //*Purpose:							Display a messagebox with the given message text.
            //*  
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*										--------------------------------------------------------------------------
            //*										Value 		String 					as_title 					
            //*										Value 		String	 					as_error_text		
            //*										Value 		String 					as_buttons	
            //*										Value 		String 					as_message	
            //*		
            //*Return:								Long: 1 - Success
            //*												-1 -Otherwise
            //*
            //*Date		     	Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*26-07-2007		SHARP.					0.2.0 			B2B 			Initiation
            //************************************************************************************************

            int li_rtn_code = 0;
            //debug -Start
            //Write to log
            var ls_message = "*************************** uo_mini_terminal_header - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "Start of uf_show_message" + "\r" + "\n";
            this.uf_write_log(ls_message);
            //debug -END

            li_rtn_code = Convert.ToInt32(await ((w_mini_terminal)this.Parent.Parent).wf_show_message(as_title, as_error_text, as_buttons, as_message));
            SystemHelper.WriteToLog(as_message);
            //debug -Start
            //Write to log
            ls_message = "*************************** uo_mini_terminal_header - uf_show_message*************************** DateTime: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n" + "Object:        " + "End of uf_show_message" + "\r" + "\n";
            this.uf_write_log(ls_message);
            //debug -END
            return li_rtn_code;
        }

        public async Task<int> uf_retrieve_data(long? ll_supplier_number = null, long? ll_order_number = null) //**********************************************************************************************
        {
            //*Object:								uo_mini_terminal_header
            //*Function/Event  Name:			uf_retrieve_data
            //*Purpose:							Call the function wf_retrieve_data in the window.
            //*  
            //*Arguments:						None.
            //*				
            //*Return:								INTEGER: 1 - Success
            //*													  -1 - Otherwise.
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*					SHARP.					0.2.0 			B2B			Initiation.
            //************************************************************************************************
            await ((w_mini_terminal)this.Parent.Parent).wf_retrieve_data(ll_supplier_number, ll_order_number);
            return 1;
        }

        public w_mini_terminal getMiniTerminal()
        {
            return ((w_mini_terminal)this.Parent.Parent);
        }

        public string checkOneSetOreder = "";
    }
}
