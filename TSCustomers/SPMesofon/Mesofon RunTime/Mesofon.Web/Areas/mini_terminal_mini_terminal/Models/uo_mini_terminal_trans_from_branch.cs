using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using mini_terminalAlias = mini_terminal;
using masofonAlias = masofon;
using Mesofon.Common;

namespace mini_terminal
{
    // Creation Time:   08/20/2015 08:30:16 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_trans_from_branch : uo_mini_terminal_base_body
    {
        public long il_branch_as_supp_num;
        public long il_supplier_num;
        public long il_source_supplier_number;
        public string is_state;
        public bool ib_user_type_invoice_num;
        public bool ib_new_return;
        public bool ib_ignore_err_msg;
        public GridElement idwc_return;
        public IRepository ids_returns_0_list;
        public IRepository ids_source_returns_list;
        public IRepository ids_returns_list;
        public delegate void ue_print_EventHandler();
        public event mini_terminalAlias.uo_mini_terminal_trans_from_branch.ue_print_EventHandler ue_printEvent;
        public uo_mini_terminal_trans_from_branch()
        {
        }

        public void constructor()
        {
            base.constructor();
            uf_create();
        }
        public long il_branch_as_supp_numProperty
        {
            get { return this.il_branch_as_supp_num; }
            set { this.il_branch_as_supp_num = value; }
        }
        public long il_supplier_numProperty
        {
            get { return this.il_supplier_num; }
            set { this.il_supplier_num = value; }
        }
        public long il_source_supplier_numberProperty
        {
            get { return this.il_source_supplier_number; }
            set { this.il_source_supplier_number = value; }
        }
        public string is_stateProperty
        {
            get { return this.is_state; }
            set { this.is_state = value; }
        }
        public bool ib_user_type_invoice_numProperty
        {
            get { return this.ib_user_type_invoice_num; }
            set { this.ib_user_type_invoice_num = value; }
        }
        public bool ib_new_returnProperty
        {
            get { return this.ib_new_return; }
            set { this.ib_new_return = value; }
        }
        public bool ib_ignore_err_msgProperty
        {
            get { return this.ib_ignore_err_msg; }
            set { this.ib_ignore_err_msg = value; }
        }
        public GridElement idwc_returnProperty
        {
            get { return this.idwc_return; }
            set { this.idwc_return = value; }
        }
        public IRepository ids_returns_0_listProperty
        {
            get { return this.ids_returns_0_list; }
            set { this.ids_returns_0_list = value; }
        }
        public IRepository ids_source_returns_listProperty
        {
            get { return this.ids_source_returns_list; }
            set { this.ids_source_returns_list = value; }
        }
        public IRepository ids_returns_listProperty
        {
            get { return this.ids_returns_list; }
            set { this.ids_returns_list = value; }
        }
        public void LoadData1(long supplier_number, long invoice_number, ref int li_mini_terminal)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", supplier_number }, { "@2", invoice_number } };
                    string sql = "SELECT count(*) FROM invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2 AND mini_terminal = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        li_mini_terminal = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(long ll_branch_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_return_number }, { "@1", ll_branch_number }, { "@2", il_supplier_numProperty }, { "@3", ldt_current_datetime }, { "@4", masofonAlias.masofon.Instance.gs_station.station_number } };
                    string sql = "INSERT INTO invoice_move (invoice_number,  branch_number,  supplier_number,  invoice_type,  employee_number,  supply_date,  discount,  mam,  invoice_total,  stock_number,  store_number,  station_num,  state,  date_move,  last_update_datetime) VALUES (@0,  @1,  @2,  r,  0,  @3,  0,  0,  0,  0,  0,  @4,  O,  @3,  @3)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3(long ll_serial_number, long ll_branch_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, decimal ldec_active_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", ll_branch_number }, { "@2", il_return_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", il_supplier_numProperty }, { "@7", ldec_active_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime,  bonus_quantity) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  0,  @7,  0,  @8,  @9,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData4(long ll_material_quantity, long ll_decline_num, DateTime? ldt_current_datetime, decimal ldec_material_price, decimal ldec_active_price, long ll_serial_number, long ll_branch_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_material_quantity }, { "@1", ll_decline_num }, { "@2", ldt_current_datetime }, { "@3", ldec_material_price }, { "@4", ldec_active_price }, { "@5", ll_serial_number }, { "@6", ll_branch_number } };
                    string sql = "UPDATE invoice_details SET material_quantity = @0,  decline_number = @1,  last_update_datetime = @2,  material_price = @3,  material_price_after_discount = @4 WHERE serial_number = @5 AND branch_number = @6";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData5(DateTime? ldt_current_datetime, long ll_orig_serial_number, long ll_branch_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_current_datetime }, { "@1", ll_orig_serial_number }, { "@2", ll_branch_number } };
                    string sql = "UPDATE invoice_details SET last_update_datetime = @0,  material_quantity = 0 WHERE serial_number = @1 AND branch_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData6(long ll_serial_number, long ll_branch_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, decimal ldec_active_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", ll_branch_number }, { "@2", il_return_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", il_supplier_numProperty }, { "@7", ldec_active_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime,  bonus_quantity) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  0,  @7,  0,  @8,  @9,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
     
        public void UpdateData9(Int64 material_number, Int64 store_number, Int64 stock_number, decimal ldec_tmp_avg_value, decimal quantity, decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number }, { "@4", ldec_tmp_avg_value }, { "@5", quantity }, { "@6", ldec_total_stock_purchase } };
                    string sql = "INSERT INTO stock_control (branch_number,  number,  store_number,  buy_stock,  sale_stock,  average_value,  stock_balance,  stock_average_value) VALUES (@0,  @1,  @2,  1,  @3,  @4,  @5,  @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData10(decimal ldec_quantity, decimal ldec_average_value, decimal ldec_total_stock_purchase, Int64 material_number, long ll_stock_a, Int64 store_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_quantity }, { "@1", ldec_average_value }, { "@2", ldec_total_stock_purchase }, { "@3", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@4", material_number }, { "@5", ll_stock_a }, { "@6", store_number } };
                    string sql = "UPDATE stock_control SET stock_balance = (stock_balance + @0),  average_value = @1,  stock_average_value = @2 WHERE (stock_control.branch_number = @3) AND (stock_control.number = @4) AND (stock_control.sale_stock = @5) AND (stock_control.buy_stock = 1) AND (stock_control.store_number = @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 material_number, ref decimal ldec_quantity_in_package)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", material_number } };
                    string sql = "SELECT quantity_in_package FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_quantity_in_package = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(ref decimal ldec_stock_balance, ref decimal ldec_stock_average, ref long ll_stock_a, int p, long material_number, long store_number, long stock_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", p }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "(SELECT stock_control.stock_balance,  stock_control.average_value,  stock_control.sale_stock FROM stock_control WHERE (stock_control.branch_number = @0) AND (stock_control.number = @1) AND (stock_control.store_number = @2) AND (stock_control.sale_stock = @3) AND (stock_control.buy_stock = 1))";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_stock_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_stock_average = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ll_stock_a = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(Int64 material_number, Int64 store_number, Int64 stock_number, ref decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "SELECT IsNull(stock_average_value, 0) FROM stock_control WHERE branch_number = @0 AND number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_total_stock_purchase = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(Int64 material_number, Int64 store_number, Int64 stock_number, ref decimal ldec_curr_balance)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", material_number }, { "@2", store_number }, { "@3", stock_number } };
                    string sql = "SELECT SUM(stock_balance) FROM stock_control WHERE stock_control.branch_number = @0 AND stock_control.number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_curr_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData11(long ll_invoice_number, long ll_supplier_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_invoice_number }, { "@1", il_return_number }, { "@2", il_branch_as_supp_numProperty }, { "@3", ll_supplier_number }, { "@4", ldt_current_datetime }, { "@5", masofonAlias.masofon.Instance.gs_station.station_number } };
                    string sql = "INSERT INTO invoice_move (invoice_number,  order_number,  branch_number,  supplier_number,  invoice_type,  employee_number,  supply_date,  discount,  mam,  invoice_total,  stock_number,  store_number,  station_num,  state,  date_move,  last_update_datetime) VALUES (@0,  @1,  @2,  @3,  p,  0,  @4,  0,  0,  0,  0,  0,  @5,  O,  @4,  @4)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData12(long ll_invoice_number, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_invoice_number }, { "@1", ldt_current_datetime }, { "@2", il_return_number }, { "@3", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@4", il_branch_as_supp_numProperty } };
                    string sql = "UPDATE invoice_move SET order_number = @0,  last_update_datetime = @1 WHERE invoice_number = @2 AND branch_number = @3 AND supplier_number = @4 AND invoice_type = 'r'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData13(long ll_serial_number, long ll_invoice_number, long ll_material_number, long ll_material_quantity, decimal ldec_material_price, long ll_supplier_number, decimal ldec_activate_price, long ll_decline_num, DateTime? ldt_current_datetime)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial_number }, { "@1", il_branch_as_supp_numProperty }, { "@2", ll_invoice_number }, { "@3", ll_material_number }, { "@4", ll_material_quantity }, { "@5", ldec_material_price }, { "@6", ll_supplier_number }, { "@7", ldec_activate_price }, { "@8", ll_decline_num }, { "@9", ldt_current_datetime } };
                    string sql = "INSERT INTO invoice_details (serial_number,  branch_number,  invoice_number,  material_number,  material_quantity,  material_price,  details,  supplier_number,  material_discount_percent,  packing_list_number,  stock_number,  material_price_after_discount,  packing_serial,  decline_number,  last_update_datetime) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  '',  @6,  0,  0,  0,  @7,  0,  @8,  @9)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(ref long ll_supplier_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT number FROM suppliers WHERE supplier_as_branch = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(Int64 al_material_number, ref decimal adec_price_before, ref decimal adec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        adec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        adec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData14(DateTime? ldt_current_datetime, long ll_serial_number, long ll_branch_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_current_datetime }, { "@1", ll_serial_number }, { "@2", ll_branch_number } };
                    string sql = "UPDATE invoice_details SET material_quantity = 0,  last_update_datetime = @0 WHERE serial_number = @1 AND branch_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT number FROM suppliers WHERE supplier_as_branch = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        il_source_supplier_numberProperty = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(string ls_barcode, ref string ls_material_name, ref long ll_material_number, ref string ls_material_status)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT name,  number,  material_status FROM materials WHERE (barcode = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_material_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        ll_material_number = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        ls_material_status = UnitOfWork.GetValue(resultRS, 2, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public int uf_create()
        {
            GridElement dw_returns = this.GetVisualElementById<GridElement>("dw_returns");
            GridElement dw_return_details = this.GetVisualElementById<GridElement>("dw_return_details");
            GridElement dw_branch_list = this.GetVisualElementById<GridElement>("dw_branch_list");

            dw_branch_list.Insert(0);
            GridElement ldwc_branch_num;
            GridElement ldwc_branch_name;
            GridElement ldwc_declines;

            this.ids_returns_listProperty = new d_mini_terminal_return_details_listRepository();


            //Ron@20/07/2015>>SPUCM00005413 - start

            this.ids_source_returns_listProperty = new d_mini_terminal_return_details_listRepository();


            //Ron@20/07/2015>>SPUCM00005413 - end
            //Ron@10/06/2015>>SPUCM00005384 - start

            this.ids_returns_0_listProperty = new d_mini_terminal_return_details_0_listRepository();


            //Ron@10/06/2015>>SPUCM00005384 - end

            ldwc_branch_num = dw_branch_list.GetVisualElementById<GridElement>("branch_num");

            ldwc_branch_name = dw_branch_list.GetVisualElementById<GridElement>("branch_num_1");

            ldwc_declines = dw_return_details.GetVisualElementById<GridElement>("declines");

            dw_returns.Insert(0);

            this.idwc_returnProperty = dw_returns.GetVisualElementById<GridElement>("return_number");

            ldwc_branch_num.Retrieve(default(object));

            ldwc_branch_name.Retrieve(default(object));

            ldwc_declines.Retrieve(3);
            //Ron@20/07/2015>>SPUCM00005413 - start
            //change the display

            var declines = this.GetVisualElementById<ControlElement>("declines");
            declines.Visible = false;

            var declines_t = this.GetVisualElementById<ControlElement>("declines_t");
            declines_t.Visible = false;

            var supplier_number_t = this.GetVisualElementById<ControlElement>("supplier_number_t");
            supplier_number_t.Text = "�";
            //Ron@20/07/2015>>SPUCM00005413 - end
            return 1;
        }
    }
}
