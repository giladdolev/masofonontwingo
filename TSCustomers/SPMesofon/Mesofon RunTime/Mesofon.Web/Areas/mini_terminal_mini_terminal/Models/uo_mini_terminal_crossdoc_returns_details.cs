using System.Web.VisualTree.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Extensions;
using global;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Mesofon.Common;
using System.Threading.Tasks;

namespace mini_terminal
{
    // Creation Time:   12/13/2015 11:20:52 (Utc)
    // Comments:        
    // 
    public class uo_mini_terminal_crossdoc_returns_details : uo_mini_terminal_base_body
    {
        //SPUCM00003614
        public GridElement idwc_distributor_num;
        public long il_return_son;
        public long il_return_son_for_display;
        public long il_supplier;
        public long il_distributor;
        public long il_delete_row;
        public bool ib_marlog_return;
        public bool ib_new_return;
        public IRepository ids_adhoc_update_list;
        //boolean	ib_return_marlog
        // AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_marlog_branch;
        public IRepository ids_supplier_distributors_return;
        public delegate void ue_delete_row_EventHandler();
        public event ue_delete_row_EventHandler ue_delete_rowEvent;

        
        public void constructor()
        {
            this.ids_adhoc_update_listProperty = new d_addhockRepository();
            //Eitan; SPUCM00005483; Build 39.3; 1/12/2015
            this.ids_supplier_distributors_returnProperty = new d_addhock_supplier_distributorsRepository();
            this.ids_supplier_distributors_returnProperty.Retrieve(5);
        }
        public bool uf_reset()
        {
            LabelElement st_carton_number = this.GetVisualElementById<LabelElement>("st_carton_number");
            LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
            GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
            
            dw_inv_pack_details.Reset();
            st_shipment_number.Text = "";
            st_carton_number.Text = "";
            return true;
        }
        public override async Task<int> uf_new_row(WidgetGridElement dw_inv_pack_details)
        {
            //GridElement dw_inv_pack_details = this.GetVisualElementById<GridElement>("dw_inv_pack_details");
            long ll_row = 0;
            long ll_current_row = 0;
            long ll_last_row = 0;
            // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call rowfocuschanging
            
            ll_current_row = dw_inv_pack_details.GetRow();
            
            ll_row = dw_inv_pack_details.Insert(0);

             // avi - need to implement
            //dw_inv_pack_details.rowfocuschanging((int)ll_current_row, (int)ll_row);
            
            
            ll_row = dw_inv_pack_details.RowCount();

            dw_inv_pack_details.SetItem(ll_row, "row_no", ll_row.ToString());

            dw_inv_pack_details.SetItem(ll_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
            // AlexKh - 1.1.1.16 - 22/12/2013 - SPUCM00004291 - call rowfocuschanging
            
            ll_last_row = dw_inv_pack_details.RowCount();
             
            dw_inv_pack_details.ScrollToRow((int)ll_last_row);

            Mesofon.Common.Extensions.ExecuteOnResponeEx((o, e) =>
            {
                dw_inv_pack_details.SetFocus((int)ll_last_row, "barcode");
            });
            //Set the position of Horizonal-Scroll. 





            dw_inv_pack_details.set_HorizontalScrollPosition(dw_inv_pack_details.get_HorizontalScrollMaximum());
            return 0;
        }
        public async Task<int> uf_create(bool ab_marlog_return, string as_carton_barcode, long al_return_number, long al_supplier_number, long al_marlog_branch)
        {
            WidgetGridElement dw_inv_pack_details = this.GetVisualElementById<WidgetGridElement>("dw_inv_pack_details");
            LabelElement st_carton_number = this.GetVisualElementById<LabelElement>("st_carton_number");
            LabelElement st_shipment_number = this.GetVisualElementById<LabelElement>("st_shipment_number");
            GridElement dwc_declines;
            long ll_row = 0;
            long ll_invoice_num = 0;
            long ll_distributor = 0;
            long ll_mat_num = 0;
            long ll_dec_num = 0;
            long ll_supplier = 0;
            long ll_row_serial_number = 0;
            string ls_rc = null;
            string ls_mat_name = null;
            string ls_barcode = null;
            string ls_adhock_key = null;
         //   char lc_state_1 = '\0';
            char lc_state = '\0';
            long li_row = 0;
            decimal ldc_mat_qty = default(decimal);
            //
            uf_reset();
            //
            is_carton_barcode = as_carton_barcode;
            il_return_number = al_return_number;
            ib_marlog_returnProperty = ab_marlog_return;
            //IF ib_marlog_return THEN
            //	il_distributor = al_distributor_number
            //	// AlexKh - 1.1.20.0 - 2014-05-05 - CR#1171(SPUCM00004718) - set marlog branch_number
            //	il_marlog_branch = f_is_marlog_distributor(0, il_distributor)
            //END IF
            il_supplierProperty = al_supplier_number;
            il_marlog_branch = al_marlog_branch;
            st_shipment_number.Text = il_return_number.ToString();
            st_carton_number.Text = is_carton_barcode;
            if (!isempty_stringClass.isempty_string(is_carton_barcode) && il_return_number > 0)
            {
                // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - retrieve row serial number
               
                do
                {
                    LoadData5(ref ll_supplier, ref ll_invoice_num, ref lc_state, ref ls_adhock_key, ref ll_mat_num, ref ldc_mat_qty, ref ll_dec_num, ref ls_barcode, ref ls_mat_name, ref ll_distributor,
                    ref ll_row_serial_number, masofonAlias.masofon.Instance.gs_vars.branch_number, il_return_number, is_carton_barcode, il_marlog_branch);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                    if (!await uf_is_new_row_exist(dw_inv_pack_details))
                    {
                        await uf_new_row(dw_inv_pack_details);
                    }
                    
                    li_row = dw_inv_pack_details.RowCount();

                    dw_inv_pack_details.SetItem(li_row, "barcode", Convert.ToString(ls_barcode));

                    dw_inv_pack_details.SetItem(li_row, "serial_number", il_return_number.ToString());

                    dw_inv_pack_details.SetItem(li_row, "carton_barcode", Convert.ToString(is_carton_barcode));

                    dw_inv_pack_details.SetItem(li_row, "supplier_number", ll_supplier.ToString());

                    dw_inv_pack_details.SetItem(li_row, "distributor_number", ll_distributor.ToString()); //+

                    dw_inv_pack_details.SetItem(li_row, "material_number", ll_mat_num.ToString()); //+

                    dw_inv_pack_details.SetItem(li_row, "material_name", Convert.ToString(ls_mat_name)); //+
                    //dw_inv_pack_details.setitem(li_row, "date_move", ldt_time)

                    dw_inv_pack_details.SetItem(li_row, "material_quantity", ldc_mat_qty.ToString()); //+

                    dw_inv_pack_details.SetItem(li_row, "declines", ll_dec_num.ToString()); //+

                    dw_inv_pack_details.SetItem(li_row, "doc_state", lc_state.ToString()); //+

                    dw_inv_pack_details.SetItem(li_row, "doc_no", ll_invoice_num.ToString()); //invoice_number

                    dw_inv_pack_details.SetItem(li_row, "adhoc_key", Convert.ToString(ls_adhock_key)); //adhoc number used for item return
                    // AlexKh - 1.1.25.0 - 2014-12-21 - SPUCM00005164 - save to datastore row serial number

                    dw_inv_pack_details.SetItem(li_row, "row_serial_number", ll_row_serial_number.ToString()); //save row serial number connecting invoice_details and shipment_marlog_return rows
                    // AlexKh - 1.1.1.17 - 2014-02-06 - SPUCM00004702 - reset dw_summary



                    dw_inv_pack_details.SetItemStatus((int)li_row, 0, ModelBuffer.Primary, ModelAction.None);



                    dw_inv_pack_details.SetItemStatus((int)li_row, 0, ModelBuffer.Primary, ModelAction.None);
                }
                while (masofonAlias.masofon.Instance.sqlca.SqlCode == 0);
                
            }
            if (!await uf_is_new_row_exist(dw_inv_pack_details) && (lc_state == uo_mini_terminal_crossdoc_returns_details.RETURN_STATE_OPEN || isempty_stringClass.isempty_string(lc_state.ToString())))
            {
                await uf_new_row(dw_inv_pack_details);
            }
             
            dwc_declines = dw_inv_pack_details.GetVisualElementById<GridElement>("declines");
            
            
            
            ll_row = dwc_declines.Retrieve(3);
            return 1;
        }

    
        public uo_mini_terminal_crossdoc_returns_details()
        {
        }

        public long il_return_sonProperty
        {
            get { return this.il_return_son; }
            set { this.il_return_son = value; }
        }
        public long il_return_son_for_displayProperty
        {
            get { return this.il_return_son_for_display; }
            set { this.il_return_son_for_display = value; }
        }
        public long il_supplierProperty
        {
            get { return this.il_supplier; }
            set { this.il_supplier = value; }
        }
        public long il_delete_rowProperty
        {
            get { return this.il_delete_row; }
            set { this.il_delete_row = value; }
        }
        public bool ib_marlog_returnProperty
        {
            get { return this.ib_marlog_return; }
            set { this.ib_marlog_return = value; }
        }
        public bool ib_new_returnProperty
        {
            get { return this.ib_new_return; }
            set { this.ib_new_return = value; }
        }
        public IRepository ids_adhoc_update_listProperty
        {
            get { return this.ids_adhoc_update_list; }
            set { this.ids_adhoc_update_list = value; }
        }
        public IRepository ids_supplier_distributors_returnProperty
        {
            get { return this.ids_supplier_distributors_return; }
            set { this.ids_supplier_distributors_return = value; }
        }
        public void LoadData1(long ll_supplier, ref long ll_row_exists, ref decimal ldc_qty)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", il_return_number }, { "@2", ll_supplier } };
                    string sql = "SELECT 1,  invoice_total FROM invoice_move WHERE branch_number = @0 AND invoice_number = @1 AND supplier_number = @2 AND invoice_type = 'r'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_row_exists = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ldc_qty = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT b.price_before_discount,  b.price_after_discount,  b.material_discount,  b.supplier_discount_percent FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier = 1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 al_material_number, ref decimal ldec_price_before, ref decimal ldec_price_after, ref decimal ldec_material_discount, ref decimal ldec_supplier_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT MAX(b.price_before_discount),  MAX(b.price_after_discount),  MAX(b.material_discount),  MAX(b.supplier_discount_percent) FROM material_suppliers AS b WHERE (b.branch_number = @0) AND (b.material_number = @1) AND (b.last_supplier_price = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_price_before = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_price_after = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ldec_material_discount = UnitOfWork.GetValue(resultRS, 2, Convert.ToDecimal);
                        ldec_supplier_discount = UnitOfWork.GetValue(resultRS, 3, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(Int64 al_material_number, ref decimal ldec_sell_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT sell_price FROM branch_item_price WHERE branch_number = @0 AND item_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_sell_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(DateTime? ldt_time, long ll_supplier, long ll_return_son)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldt_time }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@2", ll_supplier }, { "@3", ll_return_son } };
                    string sql = "UPDATE invoice_move SET date_move = @0,  state = D WHERE branch_number = @1 AND supplier_number = @2 AND invoice_number = @3 AND invoice_type = r";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
      
        public void LoadData5(ref long ll_supplier, ref long ll_invoice_num, ref char lc_state, ref string ls_adhock_key, ref long ll_mat_num, ref decimal ldc_mat_qty, ref long ll_dec_num, ref string ls_barcode, ref string ls_mat_name, ref long ll_distributor,
        ref long ll_row_serial_number,int p, long il_return_number, string is_carton_barcode, long il_marlog_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", p }, { "@1", il_return_number }, { "@2", is_carton_barcode }, { "@3", il_marlog_branch } };
                    string sql = "SELECT SMR.supplier_number,  SMR.invoice_number,  SMR.state,  SMR.adhoc_key,  SMR.material_number,  SMR.material_quantity,  ID.decline_number,  M.barcode,  M.name,  M.distributor_number,  SMR.row_serial_number FROM shipment_marlog_return AS SMR,  invoice_details AS ID,  materials AS M WHERE SMR.branch_number = ID.branch_number AND SMR.supplier_number = ID.supplier_number AND SMR.invoice_number = ID.invoice_number AND SMR.material_number = ID.material_number AND SMR.material_quantity = ID.material_quantity AND IsNull(ID.state, '') <> 'd' AND ID.decline_number > 0 AND SMR.branch_number = @0 AND SMR.shipment_number = @1 AND SMR.carton_barcode = @2 AND SMR.state <> 'd' AND SMR.marlog_number = @3 AND SMR.material_number = M.number ORDER BY row_serial_number ";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ll_invoice_num = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        lc_state = UnitOfWork.GetValue(resultRS, 2, Convert.ToChar);
                        ls_adhock_key = UnitOfWork.GetValue(resultRS, 3, Convert.ToString);
                        ll_mat_num = UnitOfWork.GetValue(resultRS, 4, Convert.ToInt64);
                        ldc_mat_qty = UnitOfWork.GetValue(resultRS, 5, Convert.ToDecimal);
                        ll_dec_num = UnitOfWork.GetValue(resultRS, 6, Convert.ToInt64);
                        ls_barcode = UnitOfWork.GetValue(resultRS, 7, Convert.ToString);
                        ls_mat_name = UnitOfWork.GetValue(resultRS, 8, Convert.ToString);
                        ll_distributor = UnitOfWork.GetValue(resultRS, 9, Convert.ToUInt32);
                        ll_row_serial_number = UnitOfWork.GetValue(resultRS, 10, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(string ls_barcode, ref long ll_supplier, ref string ls_material_name, ref long ll_material_number, ref long ll_distributor, ref long ll_return_allowed, ref DateTime? ldt_last_return_date)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name,  number,  distributor_number,  return_allowed,  last_return_date FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        ll_material_number = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        ll_distributor = UnitOfWork.GetValue(resultRS, 3, Convert.ToInt64);
                        ll_return_allowed = UnitOfWork.GetValue(resultRS, 4, Convert.ToInt64);
                        ldt_last_return_date = UnitOfWork.GetValue(resultRS, 5, Convert.ToDateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(string ls_barcode, ref long ll_supplier, ref string ls_material_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_barcode } };
                    string sql = "SELECT supplier_number,  name FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_material_name = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
