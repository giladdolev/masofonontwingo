using System;
using System.Web.VisualTree.MVC;
using global;

namespace  mini_terminal
{
	// Creation Time:   08/11/2015 10:21:19 (Utc)
	// Comments:        
	// 
	public class w_mini_terminal_exiration_date : System.Web.VisualTree.Elements.WindowElement
    {
		//.. minimum expiration date for a material
		public DateTime? idt_min_expiration_date;
        public string is_window_title;
        public delegate void ue_close_with_return_EventHandler(bool ab_variable);
		//public event ue_close_with_return_EventHandler ue_close_with_returnEvent;
        public w_mini_terminal_exiration_date(s_array_arguments istr_arg)
        {
            this.istr_arg = istr_arg;
		}

        public s_array_arguments istr_arg { get; set; }

		public DateTime? idt_min_expiration_dateProperty
		{
			get { return this.idt_min_expiration_date; }
			set { this.idt_min_expiration_date = value; }
		}

        public string is_window_titleProperty
        {
            get { return this.is_window_title; }
            set { this.is_window_title = value; }
        }


        public async System.Threading.Tasks.Task<string> wf_translate_message(string as_message) //--------------------------------------------------------------------
        {
            //Function:			public w_mini_terminal_msg.wf_translate_message()
            //
            // Returns:         String
            //
            // Parameters:      value String as_message
            // 
            // Copyright  - Stas
            //
            // Date Created: 08/08/2005
            //
            // Description:	
            // 					 translate message :
            //					 1) if message start is # ---> gets message by message number from DB
            //					 2) otherwise return as_message
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (!string.IsNullOrEmpty(as_message) && as_message.Substring(0, 1) == "#")
            {
                long ll_message_number = 0;
                ll_message_number = Convert.ToInt64(as_message.Substring(as_message.Length - as_message.Length - 1));
                if (ll_message_number != 0)
                {
                    as_message = await f_get_error_message_numberClass.f_get_error_message_number((int)ll_message_number);
                }
            }
            return as_message;
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
