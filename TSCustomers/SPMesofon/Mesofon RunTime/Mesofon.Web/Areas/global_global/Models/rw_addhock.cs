using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;

namespace global
{
	// Creation Time:   08/11/2015 10:20:56 (Utc)
	// Comments:        
	// 
	public class rw_addhock : WindowElement
	{
		public long il_branch_number;
		public long il_supplier_number;
		public long il_order_number;
		public long il_item_number;
		public long il_adhoc_serial_number;
		public long il_row_serial_number;
		public long il_adhoc_type;
		public const long TEMPORARY_LOCKED = 2;
		public const long FINALY_LOCKED = 3;
		public const long UNLOCKED = 1;
		public string is_adhoc_key;
		public bool ib_return_value;
        public IRepository ids_adhoc_update_list;
		public delegate void ue_close_EventHandler();
		public event ue_close_EventHandler ue_closeEvent;
        public rw_addhock(s_array_arguments args)
        {
            Args = args;
        }

        public s_array_arguments Args { get; set; }

		public long il_branch_numberProperty
		{
			get { return this.il_branch_number; }
			set { this.il_branch_number = value; }
		}
		public long il_supplier_numberProperty
		{
			get { return this.il_supplier_number; }
			set { this.il_supplier_number = value; }
		}
		public long il_order_numberProperty
		{
			get { return this.il_order_number; }
			set { this.il_order_number = value; }
		}
		public long il_item_numberProperty
		{
			get { return this.il_item_number; }
			set { this.il_item_number = value; }
		}
		public long il_adhoc_serial_numberProperty
		{
			get { return this.il_adhoc_serial_number; }
			set { this.il_adhoc_serial_number = value; }
		}
		public long il_row_serial_numberProperty
		{
			get { return this.il_row_serial_number; }
			set { this.il_row_serial_number = value; }
		}
		public long il_adhoc_typeProperty
		{
			get { return this.il_adhoc_type; }
			set { this.il_adhoc_type = value; }
		}
		public string is_adhoc_keyProperty
		{
			get { return this.is_adhoc_key; }
			set { this.is_adhoc_key = value; }
		}
		public bool ib_return_valueProperty
		{
			get { return this.ib_return_value; }
			set { this.ib_return_value = value; }
		}
		public IRepository ids_adhoc_update_listProperty
		{
			get { return this.ids_adhoc_update_list; }
			set { this.ids_adhoc_update_list = value; }
		}

        
    }
}
