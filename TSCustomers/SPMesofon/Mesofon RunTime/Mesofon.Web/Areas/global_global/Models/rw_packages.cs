using System.Web.VisualTree.Elements;
using Mesofon.Repository;

namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public class rw_packages : WindowElement
	{
		public delegate void ue_close_with_return_EventHandler(bool ab_variable);
		public event ue_close_with_return_EventHandler ue_close_with_returnEvent;
		public delegate bool ue_validation_check_EventHandler(long al_quantity);
		public event ue_validation_check_EventHandler ue_validation_checkEvent;
        public rw_packages(d_packageRepository repository)
        {
            Repository = repository;
        }

        public d_packageRepository Repository { get; set; }
         
    }
}
