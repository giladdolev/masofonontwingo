using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using Mesofon.Common;
using Mesofon.Repository;

namespace global
{
    // Creation Time:   08/11/2015 10:21:00 (Utc)
    // Comments:        
    // 

    public class u_material_update
    {
        //.. connection to db. by default connected to sqlca
        public SQLError i_transaction;
        public bool ib_show_error_message;
        public string is_error_message;
        public string is_error_message_title;
        public long il_supplier_number;
        public long il_branch_number;
        public long il_order_number;
        public u_datastore_update ids_material_suppliers_connect;
        public u_datastore_update ids_material_suppliers;
        public u_datastore_update ids_orders;
        public void uf_set_transaction(SQLError al_translation)
        {
            i_transaction = al_translation;
        }
        public bool uf_handle_material_suppliers(long al_material_number, long al_supplier_number, long al_branch_number) //f_handle_material_suppliers(al_material, al_supplier)
        {
            //.. mark table with last supplier
            //UPDATE 	material_suppliers 
            //	SET 	last_supplier 		= 0
            // WHERE	branch_number 		= :al_branch_number					AND
            //			material_number 	= :al_material_number 				;	
            //
            //IF sqlca.sqlcode <> 0 THEN
            //	return False
            //END IF
            //
            ////.. mark table with last price of material
            //
            //UPDATE 	material_suppliers 
            //	SET 	last_supplier_price = 0
            // WHERE	branch_number 		= :al_branch_number					AND
            //			material_number 	= :al_material_number 				AND
            //			supplier_number 	= :al_supplier_number 				;
            //
            //IF sqlca.sqlcode <> 0 THEN
            //	return False
            //END IF
            return true;
        }
        public void uf_set_branch_number(long al_branch_number)
        {
            this.il_branch_number = al_branch_number;
        }
        public void uf_set_supplier_number(long al_supplier_number)
        {
            this.il_supplier_number = al_supplier_number;
        }
        public async Task<FuncResults< long[] ,long>> uf_load_material_suppliers_connect( long[] al_material_number_arrp)
        {
            long ll = 0;
            long ll_return_value = 0;
            bool lb_result = false;
            ll_return_value = 1;
            for (ll = CollectionExtensions.UBound(al_material_number_arrp); ll >= 0; ll += -1)
            {
                lb_result = await uf_material_supplier_connect_exists(al_material_number_arrp[ll]);
                if (!lb_result)
                {
                    long ll_row = 0;

                    ll_row = ids_material_suppliers_connect.Insert(0);

                    ids_material_suppliers_connect.SetItem(ll_row, "material_number", al_material_number_arrp[ll].ToString());

                    ids_material_suppliers_connect.SetItem(ll_row, "supplier_number", il_supplier_number.ToString());
                }
            }
            return FuncResults.Return(al_material_number_arrp, ll_return_value);
        }
        public long uf_update(ref long al_error_number, ref string as_error_text)
        {
            long ll_result = 0;
            long ll_count = 0;
            long[] ll_material_number_arr = null;
            ll_result = 0;
             
             
            if (ids_material_suppliers_connect.DeletedCount() + ids_material_suppliers_connect.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_result = ids_material_suppliers_connect.Update(unitOfWork);
                }

                if (ll_result < 1)
                {
                    al_error_number = ids_material_suppliers_connect.uf_get_last_sql_db_code();
                    as_error_text = ids_material_suppliers_connect.uf_get_last_sql_err_text();
                    goto ERROR;
                }
            }
             
             
            if (ids_material_suppliers.DeletedCount() + ids_material_suppliers.ModifiedCount() > 0)
            {
                
                if (ids_material_suppliers.RowCount() > 0)
                {
                    
                    
                    ll_material_number_arr = ids_material_suppliers.GetItemValues<long>("material_number");
                    for (ll_count = CollectionExtensions.UBound(ll_material_number_arr); ll_count >= 0; ll_count += -1)
                    {
                        uf_handle_material_suppliers(ll_material_number_arr[ll_count], il_supplier_number, il_branch_number);
                    }
                }
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_result = ids_material_suppliers.Update(unitOfWork);
                }
                if (ll_result < 1)
                {
                    al_error_number = ids_material_suppliers.uf_get_last_sql_db_code();
                    as_error_text = ids_material_suppliers.uf_get_last_sql_err_text();
                    goto ERROR;
                }
            }
            //.. if rows in invoice imported from order, update supplier_order_details and 
            //.. supplier_order_move. 
            // SharonS - 0.0.0.1 - 2007-04-12 - CR#1012 - DeletedCount check is not necessary
            //IF ids_orders.RowCount() > 0 AND (ids_orders.DeletedCount() + ids_orders.ModifiedCount() >0) THEN
            // End
            
             
            if (ids_orders.RowCount() > 0 && ids_orders.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_result = ids_orders.Update(unitOfWork);
                }
                if (ll_result < 1)
                {
                    al_error_number = ids_orders.uf_get_last_sql_db_code();
                    as_error_text = ids_orders.uf_get_last_sql_err_text();
                    goto ERROR;
                }
                UpdateData();
            }
            return ll_result;
        ERROR:
            return ll_result;
        }
        public void uf_set_sql_syntax(bool ab_sql_syntax_flag)
        {
            ids_material_suppliers_connect.uf_set_sql_preview_flag(ab_sql_syntax_flag);
            ids_material_suppliers.uf_set_sql_preview_flag(ab_sql_syntax_flag);
        }
        public async Task<bool> uf_material_supplier_exists(long al_material_number, decimal adec_new_material_cost) // 	Checks if supplier exists for current material number.  return TRUE if yes  	
        {

            long ll_material_supplier_count = 0;
            bool lb_return_flag = false;
            decimal ldec_old_material_cost = default(decimal);
            is_error_message = "";
            is_error_message_title = "";
            lb_return_flag = true;
            if (true)
            {
                LoadData(al_material_number, ref ldec_old_material_cost);
            }

            if (i_transaction.SqlCode < 0)
            {
                if (ib_show_error_message)
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("uf_material_supplier_exists", "", "stopsign!",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
                }
                else
                {
                    is_error_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים");
                    is_error_message_title = "uf_material_supplier_connect_exists";
                }
                lb_return_flag = false;
            }

            else if (i_transaction.SqlCode == 100 || ldec_old_material_cost == 0)
            {
                lb_return_flag = false;
            }
            return lb_return_flag;
        }
        public async Task<bool> uf_material_supplier_connect_exists(long al_material_number) // 	Checks if supplier exists for current material number.  return TRUE if yes  	
        {

            long ll_material_supplier_count = 0;
            bool lb_return_flag = false;
            is_error_message = "";
            is_error_message_title = "";
            lb_return_flag = true;
            if (true)
            {
                LoadData1(al_material_number, ref ll_material_supplier_count);
            }

            if (i_transaction.SqlCode < 0)
            {
                if (ib_show_error_message)
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("uf_material_supplier_exists", "", "stopsign!", await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים"));
                }
                else
                {
                    is_error_message =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיסי הנתונים");
                    is_error_message_title = "uf_material_supplier_connect_exists";
                }
                lb_return_flag = false;
            }
            return lb_return_flag;
        }
        public async Task<long> uf_load_material_suppliers(GridElement adw_details)
        {
            long ll = 0;
            long ll_result = 0;
            long ll_return_value = 0;
            long ll_row = 0;
            long[] ll_material_number_arr = null;
            bool lb_result = false;
            decimal[] ldec_material_cost = null;
            DateTime? ldt_current_date = default(DateTime);

            if (adw_details.RowCount() == 0)
            {
                return 0;
            }


            ll_material_number_arr = adw_details.GetItemValues<long>("material_number");


            ldec_material_cost = adw_details.GetItemValues<decimal>("material_price");
            ldt_current_date = masofonAlias.masofon.Instance.set_machine_time();
            ll_return_value = 1;

            for (ll = adw_details.RowCount(); ll >= 1; ll += -1)
            {
                lb_result = await uf_material_supplier_exists(ll_material_number_arr[ll], ldec_material_cost[ll]);
                if (!lb_result)
                {

                    ll_row = ids_material_suppliers.Insert(0);

                    ids_material_suppliers.SetItem(ll_row, "branch_number", il_branch_number.ToString());

                    ids_material_suppliers.SetItem(ll_row, "supplier_number", il_supplier_number.ToString());

                    ids_material_suppliers.SetItem(ll_row, "material_number", ll_material_number_arr[ll].ToString());

                    ids_material_suppliers.SetItem(ll_row, "move_datetime", ldt_current_date.ToString());

                    ids_material_suppliers.SetItem(ll_row, "last_supplier", "0");

                    ids_material_suppliers.SetItem(ll_row, "last_supplier_price", "1");

                    ids_material_suppliers.SetItem(ll_row, "price_before_discount", ldec_material_cost[ll].ToString());

                    ids_material_suppliers.SetItem(ll_row, "price_after_discount", ldec_material_cost[ll].ToString());
                }
            }
            return ll_return_value;
        }
        public void uf_set_order_number(long al_order_number)
        {
            il_order_number = al_order_number;
        }
        public long uf_load_supplier_order_details( GridElement adw_details) //--------------------------------------------------------------------
        {
            //Function:			public u_material_update.uf_load_supplier_order_details()
            //
            // Returns:         Long
            //
            // Parameters:      ref DataWindow adw_details
            // 
            // Copyright  - Stas
            //
            // Date Created: 25/08/2005
            //
            // Description:	
            // 					if order exists at mini-terminal input (invoice/packlist) 
            //					1) gets all appropriate data from supplier_order_details
            //					2) if some material exists in mini-terminal input and in supplier_order_details
            //						sets material qty into supplier_order_details,
            //						if not, sets ordered_not_received = 'y' -> material was ordered but 
            //						not in invoice.
            //
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (il_order_number == 0)
            {
                return 1;
            }
            IRepository lds_temp = null;
            long ll_mt_rows = 0;
            long ll_or_rows = 0;
            long ll_mt = 0;
            long ll_or = 0; //.. counters of mini-terminal and supplier_order_details table
            long ll_material_number_mt = 0; //.. from mini terminal input
            long ll_material_number_or = 0; //.. from supplier_order_details table
            // SharonS - 0.0.0.1 - 2007-04-12 - CR#1012 - Wrong order of the retrieval arguments - Start
            //ll_or_rows = ids_orders.Retrieve(il_branch_number,  il_supplier_number, il_order_number)
            
            ll_or_rows = ids_orders.Retrieve(il_branch_number, il_order_number);
            // End
            lds_temp = adw_details.GetDataObject();
            lds_temp.SetDataRows(adw_details.DataRows());
            
            lds_temp.SetSort("material_number ASC");
            
            lds_temp.Sort();
            
            ids_orders.SetSort("material_number ASC");
            
            ids_orders.Sort();
            
            ll_mt_rows = lds_temp.RowCount();
            ll_mt = 1;
            ll_or = 1;
            while ((ll_mt <= ll_mt_rows) && (ll_or <= ll_or_rows))
            {
                
                ll_material_number_mt = lds_temp.GetItemValue<long>((int)ll_mt, "material_number");
                
                ll_material_number_or = ids_orders.GetItemValue<long>((int)ll_or, "material_number");
                if (ll_material_number_mt < ll_material_number_or)
                {
                    
                    ll_mt++;
                }
                else if (ll_material_number_mt == ll_material_number_or)
                {
                    
                    
                    ids_orders.SetItem(ll_or, "quantity_within_invoice", lds_temp.GetItemValue<decimal>((int)ll_mt, "expected_material_quantity").ToString());
                    
                    ll_mt++;
                    
                    ll_or++;
                }
                else if (ll_material_number_mt > ll_material_number_or)
                {
                    
                    ids_orders.SetItem(ll_or, "quantity_within_invoice", "0");
                    
                    ids_orders.SetItem(ll_or, "ordered_not_received", "y");
                    
                    ll_or++;
                }
            }
            while (ll_or <= ll_or_rows)
            {
                
                ids_orders.SetItem(ll_or, "quantity_within_invoice", "0");
                
                ids_orders.SetItem(ll_or, "ordered_not_received", "y");
                
                ll_or++;
            }
            if (lds_temp != null)
            {


            }
            return 1;
        }
        public async Task<int> uf_load_material_suppliers_connect_new(long al_material_number)
        {
            long ll = 0;
            long ll_return_value = 0;
            bool lb_result = false;
            lb_result = await uf_material_supplier_connect_exists(al_material_number);
            if (!lb_result)
            {
                long ll_row = 0;

                ll_row = ids_material_suppliers_connect.Insert(0);

                ids_material_suppliers_connect.SetItem(ll_row, "material_number", al_material_number.ToString());

                ids_material_suppliers_connect.SetItem(ll_row, "supplier_number", il_supplier_number.ToString());
            }
            return 1;
        }
        public async Task<int> uf_load_material_suppliers_new(long al_material_number, decimal adec_material_price)
        {
            long ll_result = 0;
            long ll_return_value = 0;
            long ll_row = 0;
            bool lb_result = false;
            DateTime? ldt_current_date = default(DateTime);
            ldt_current_date = masofonAlias.masofon.Instance.set_machine_time();
            lb_result = await uf_material_supplier_exists(al_material_number, adec_material_price);
            masofonAlias.masofon.Instance.sqlca = new SQLError(null);
            if (!lb_result)
            {

                ll_row = ids_material_suppliers.Insert(0);

                ids_material_suppliers.SetItem(ll_row, "branch_number", il_branch_number.ToString());

                ids_material_suppliers.SetItem(ll_row, "supplier_number", il_supplier_number.ToString());

                ids_material_suppliers.SetItem(ll_row, "material_number", al_material_number.ToString());

                ids_material_suppliers.SetItem(ll_row, "move_datetime", ldt_current_date.ToString());

                ids_material_suppliers.SetItem(ll_row, "last_supplier", "0");

                ids_material_suppliers.SetItem(ll_row, "last_supplier_price", "1");

                ids_material_suppliers.SetItem(ll_row, "price_before_discount", adec_material_price.ToString());

                ids_material_suppliers.SetItem(ll_row, "price_after_discount", adec_material_price.ToString());
            }
            return 1;
        }
        public int uf_load_supplier_order_details_new(long al_material_number, decimal adec_material_quantity) //--------------------------------------------------------------------
        {
            //Function:			public u_material_update.uf_load_supplier_order_details()
            //
            //Returns:         		Long
            //
            //Parameters:      	ref DataWindow adw_details
            // 
            //Copyright 			SharonS
            //
            //Date Created: 		25/10/2007
            //
            //Description:			if order exists at mini-terminal input (invoice/packlist) 
            //					1) gets all appropriate data from supplier_order_details
            //					2) if some material exists in mini-terminal input and in supplier_order_details
            //						sets material qty into supplier_order_details,
            //						if not, sets ordered_not_received = 'y' -> material was ordered but 
            //						not in invoice.
            //
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_or_rows = 0;
            long? ll_found = 0;
            string ls_find = null;
            if (il_branch_number == 0)
            {
                return 1;
            }
            if (il_supplier_number == 0)
            {
                return 1;
            }
            if (il_order_number == 0)
            {
                return 1;
            }
            
            ll_or_rows = ids_orders.Retrieve(il_branch_number, il_order_number);
            ls_find = "material_number == " + al_material_number.ToString();
            
            ll_found = ids_orders.Find(ls_find, 0, (int)ll_or_rows);
            if (ll_found >= 0)
            {
                
                ids_orders.SetItem(ll_found.Value, "quantity_within_invoice", adec_material_quantity.ToString());
            }
            return 1;
        }
        public int uf_update_new(ref long al_error_number, ref string as_error_text)
        {
            long ll_result = 0;
            long ll_count = 0;
            long[] ll_material_number_arr = null;
            ll_result = 1;
             
             
            if (ids_material_suppliers_connect.DeletedCount() + ids_material_suppliers_connect.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_result = ids_material_suppliers_connect.Update(unitOfWork);
                }

                if (ll_result < 1)
                {
                    al_error_number = ids_material_suppliers_connect.uf_get_last_sql_db_code();
                    as_error_text = ids_material_suppliers_connect.uf_get_last_sql_err_text();
                    return (int)ll_result;
                }
            }
             
             
            if (ids_material_suppliers.DeletedCount() + ids_material_suppliers.ModifiedCount() > 0)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_result = ids_material_suppliers.Update(unitOfWork);
                }
                if (ll_result < 1)
                {
                    al_error_number = ids_material_suppliers.uf_get_last_sql_db_code();
                    as_error_text = ids_material_suppliers.uf_get_last_sql_err_text();
                    return (int)ll_result;
                }
            }
            // Reset DataStores
            //gilad
            ids_material_suppliers_connect.Reset("ids_material_suppliers_connect");
            
            ids_material_suppliers.Reset("ids_material_suppliers");
            return (int)ll_result;
        }
        public u_material_update()
        {
            constructor();
        }

        public void constructor() //.. by the default
        {
            i_transaction = masofonAlias.masofon.Instance.sqlca;
            ids_material_suppliers_connect = new u_datastore_update();
            ids_material_suppliers_connect.SetRepository(new d_sql_material_suppliers_connectRepository(), false);

            ids_material_suppliers = new u_datastore_update();
            ids_material_suppliers.SetRepository(new d_sql_material_suppliersRepository(), false);
            
            ids_orders = new u_datastore_update();
            ids_orders.SetRepository(new d_sql_supplier_order_detailsRepository(), false);
        }
        
        public void UpdateData()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_number }, { "@1", il_order_number }, { "@2", il_supplier_number } };
                    string sql = "UPDATE supplier_order_move SET supplier_order_move.state = 'P' WHERE supplier_order_move.branch_number = @0 AND supplier_order_move.order_number = @1 AND supplier_order_move.supplier_number = @2 AND supplier_order_move.state = 'C'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData(Int64 al_material_number, ref decimal ldec_old_material_cost)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_number }, { "@1", al_material_number }, { "@2", il_supplier_number } };
                    string sql = "SELECT price_before_discount FROM material_suppliers WHERE branch_number = @0 AND material_number = @1 AND supplier_number = @2 AND last_supplier_price = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_old_material_cost = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_material_number, ref long ll_material_supplier_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number }, { "@1", il_supplier_number } };
                    string sql = "SELECT COUNT(*) FROM material_suppliers_connect WHERE material_number = @0 AND supplier_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_material_supplier_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
