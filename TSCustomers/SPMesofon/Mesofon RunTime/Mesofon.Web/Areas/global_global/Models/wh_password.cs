using System.Web.VisualTree.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:21:03 (Utc)
    // Comments:        
    // 
    public class wh_password : WindowElement
    {
        public bool close_window;
        public bool escape_called;
        public s_password pass_t;
        public bool ib_mini_terminal_mode;
        public bool ib_usual_msg;
        public wh_password()
        {
        }

        public bool close_windowProperty
        {
            get { return this.close_window; }
            set { this.close_window = value; }
        }
        public bool escape_calledProperty
        {
            get { return this.escape_called; }
            set { this.escape_called = value; }
        }
        public s_password pass_tProperty
        {
            get { return this.pass_t; }
            set { this.pass_t = value; }
        }
        public bool ib_mini_terminal_modeProperty
        {
            get { return this.ib_mini_terminal_mode; }
            set { this.ib_mini_terminal_mode = value; }
        }
        public bool ib_usual_msgProperty
        {
            get { return this.ib_usual_msg; }
            set { this.ib_usual_msg = value; }
        }
        public void LoadData(Int64 employee_number, ref int i)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", employee_number } };
                    string sql = "SELECT count(pc_user_options_connections.action_number) FROM pc_user_options_connections WHERE (pc_user_options_connections.user_number = @0) AND (pc_user_options_connections.action_number >= 35) AND (pc_user_options_connections.action_number <= 60)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        i = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(string ls_password, ref long ll_employee_number, ref string ls_employee_title)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", ls_password } };
                    string sql = "SELECT employees.number,  employees.title FROM employees WHERE (branch_number = @0) AND (employees.password = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_employee_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_employee_title = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }


        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
