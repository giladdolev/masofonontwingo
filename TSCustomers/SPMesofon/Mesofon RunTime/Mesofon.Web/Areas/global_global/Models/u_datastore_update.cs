
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using Mesofon.Common;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	
    public class u_datastore_update : GridElement
	{
		public bool ib_sql_preview_flag;
		public bool ib_error_message_flag;
		public long il_last_sql_db_code;
		public string is_last_sql_err_text;
		public void uf_set_sql_preview_flag(bool ab_flag)
		{
			ib_sql_preview_flag = ab_flag;
		}
		public long uf_get_last_sql_db_code()
		{
			return il_last_sql_db_code;
		}
		public string uf_get_last_sql_err_text()
		{
			return is_last_sql_err_text;
		}
		public void uf_set_error_message_flag(bool ab_flag)
		{
			ib_error_message_flag = ab_flag;
		}
		public u_datastore_update()
		{

		}

  //      public async Task sqlpreview(SQLPreviewFunctionValue request, SQLPreviewTypeValue sqltype, string sqlsyntax, object buffer, int row)
		//{
		//	string ls_temp = null;
		//	//.. open debug sql window if checked
		//	if (ib_sql_preview_flag)
		//	{
  //              await WindowHelper.Open<w_sql_syntax>("sqlSyntax", sqlsyntax);
			    
				
		//		if (WindowHelper.GetParam<string>(this) == "Cancled")
		//		{
		//			ls_temp = "";
		//		}
		//		else
		//		{
					
		//			ls_temp = WindowHelper.GetParam<string>(this);
		//		}
		//	}
		//}
        public async Task dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row)
		{
			string ls_Temp = null;
			il_last_sql_db_code = sqldbcode;
			is_last_sql_err_text = sqlerrtext;
			if (ib_error_message_flag && sqldbcode != 0)
			{
                await WindowHelper.Open<w_sql_syntax>("global_global", "sqlSyntax", sqlerrtext);
				
				if (WindowHelper.GetParam<string>(this) == "Cancled")
				{
					ls_Temp = "";
				}
				else
				{
					
					ls_Temp = WindowHelper.GetParam<string>(this);
				}
			}
			else if (sqldbcode == -3)
			{
                //.. משתמש אחר מנסה לעדכן את אותה רשומה נא לצאת ולהכנס שוב		
			    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", "", "#5350");
			}
		}
		public void updatestart()
		{
			il_last_sql_db_code = 0;
			is_last_sql_err_text = "";
		}
	}
}
