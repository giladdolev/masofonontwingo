using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Web.VisualTree.Extensions;
using System.Globalization;
using Mesofon.Models;
using Mesofon.Common;

namespace global
{
	// Creation Time:   12/01/2015 08:46:29 (Utc)
	// Comments:        
	// 
	public class rw_declines_reasons : WindowElement
	{
		public IRepository ids_orig_declines_reasons;
		public string is_parent_doc_type;
		public long il_decline_level;
		public long il_supplier_number;
		public long il_order_number;
		public long il_parent_doc_number;
		public long il_material_number;
		public long il_employee_number;
		public long il_max_line_number;
		public long il_orig_decline_exist;
		public long il_serial_number;
		public long il_doc_quantity;
		public long il_actual_quantity;
		// SharonS - 1.2.42.0 - 2009-01-18 - CR#1100 - SPUCM00000115 - Distributor 
		public long il_distributor_number;
		// AlexKh - 1.2.46.5 - 2011-01-06 - CR#1143 - invuo_b2b_decline
		public nvuo_b2b_decline invuo_b2b_decline;
		// AlexKh - 1.1.26.0 - 2015-01-22 - SPUCM00005189 - oversupplied item indicator
		public bool ib_oversupplied;
        public rw_declines_reasons(s_array_arguments args)
        {
            Args = args;
        }


        public long ll_decline_exist_return { get; set; }

        public s_array_arguments Args { get; set; }

		public string is_parent_doc_typeProperty
		{
			get { return this.is_parent_doc_type; }
			set { this.is_parent_doc_type = value; }
		}
		public long il_decline_levelProperty
		{
			get { return this.il_decline_level; }
			set { this.il_decline_level = value; }
		}
		public long il_supplier_numberProperty
		{
			get { return this.il_supplier_number; }
			set { this.il_supplier_number = value; }
		}
		public long il_order_numberProperty
		{
			get { return this.il_order_number; }
			set { this.il_order_number = value; }
		}
		public long il_parent_doc_numberProperty
		{
			get { return this.il_parent_doc_number; }
			set { this.il_parent_doc_number = value; }
		}
		public long il_material_numberProperty
		{
			get { return this.il_material_number; }
			set { this.il_material_number = value; }
		}
		public long il_employee_numberProperty
		{
			get { return this.il_employee_number; }
			set { this.il_employee_number = value; }
		}
		public long il_max_line_numberProperty
		{
			get { return this.il_max_line_number; }
			set { this.il_max_line_number = value; }
		}
		public long il_orig_decline_existProperty
		{
			get { return this.il_orig_decline_exist; }
			set { this.il_orig_decline_exist = value; }
		}
		public long il_serial_numberProperty
		{
			get { return this.il_serial_number; }
			set { this.il_serial_number = value; }
		}
		public long il_doc_quantityProperty
		{
			get { return this.il_doc_quantity; }
			set { this.il_doc_quantity = value; }
		}
		public long il_actual_quantityProperty
		{
			get { return this.il_actual_quantity; }
			set { this.il_actual_quantity = value; }
		}
		public long il_distributor_numberProperty
		{
			get { return this.il_distributor_number; }
			set { this.il_distributor_number = value; }
		}
		public nvuo_b2b_decline invuo_b2b_declineProperty
		{
			get { return this.invuo_b2b_decline; }
			set { this.invuo_b2b_decline = value; }
		}
		public bool ib_oversuppliedProperty
		{
			get { return this.ib_oversupplied; }
			set { this.ib_oversupplied = value; }
		}
	    public void UpdateData()
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@0", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@1", il_supplier_numberProperty},
                        {"@2", il_order_numberProperty},
                        {"@3", is_parent_doc_typeProperty},
                        {"@4", il_parent_doc_numberProperty}
                    };

					string sql = "UPDATE b2b_declines_move SET status = 'a' WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND parent_doc_type = @3 AND parent_doc_number = @4 AND material_number = 0 AND decline_code = '34'";
                    unitOfWork.Execute(sql,parameters);
				}
			}
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
		}
	    public void UpdateData1()
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@0", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@1", il_supplier_numberProperty},
                        {"@2", il_order_numberProperty},
                        {"@3", is_parent_doc_typeProperty},
                        {"@4", il_parent_doc_numberProperty}
                    };

					string sql = "UPDATE b2b_declines_move SET status = 'd' WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND parent_doc_type = @3 AND parent_doc_number = @4 AND material_number = 0 AND decline_code = '34'";
                    unitOfWork.Execute(sql, parameters);
				}
			}
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
		}

        public void UpdateData2()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@0", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@1", il_supplier_numberProperty},
                        {"@2", il_order_numberProperty},
                        {"@3", is_parent_doc_typeProperty},
                        {"@4", il_parent_doc_numberProperty}
                    };

                    string sql = "UPDATE b2b_declines_move SET status = 'a' WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND parent_doc_type = @3 AND parent_doc_number = @4 AND material_number = 0 AND decline_code = '34' AND status = 'd'";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void LoadData(ref long ll_count)
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@0", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@1", il_supplier_numberProperty},
                        {"@2", il_order_numberProperty},
                        {"@3", is_parent_doc_typeProperty},
                        {"@4", il_parent_doc_numberProperty}
                    };

                    string sql = "SELECT COUNT(*) FROM b2b_declines_move WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND parent_doc_type = @3 AND parent_doc_number = @4 AND material_number = 0 AND decline_code = '34'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);

					while (resultRS.Read())
					{
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
					}
				}
			}
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
		}
	    public void LoadData1(ref long ll_count)
		{
			try
			{
                using (System.DataAccess.UnitOfWork unitOfWork = new System.DataAccess.UnitOfWork())
				{
                    System.Collections.Generic.Dictionary<string, object> parameters = new System.Collections.Generic.Dictionary<string, object>
                    {
                        {"@0", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@1", il_supplier_numberProperty},
                        {"@2", il_order_numberProperty},
                        {"@3", is_parent_doc_typeProperty},
                        {"@4", il_parent_doc_numberProperty}
                    };
                    string sql = "SELECT COUNT(*) FROM b2b_declines_move WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND parent_doc_type = @3 AND parent_doc_number = @4 AND material_number <> 0 AND status = 'a'";
                    System.Data.IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);

					while (resultRS.Read())
					{
                        ll_count = System.DataAccess.UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
					}
				}
			}
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
		}

        public async System.Threading.Tasks.Task<long> wf_display(s_array_arguments astr_mess) //**********************************************************************************************
        {
            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");
            LabelElement st_header = this.GetVisualElementById<LabelElement>("st_header");
            //*Object:							rw_reject_reason
            //*Function/Event  Name:		wf_display
            //*Purpose:						Display item/doc declines.
            //*
            //*Arguments:					Pass By		Type						Name
            //*									----------------------------------------------------------------------------------
            //*									Value			s_array_arguments	astr_mess
            //*										
            //*Return:								Long
            //*
            //*Date				Programer		Version		Task#								Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-01-2007		PninaSG    		1.2.23.0 		10003 - B2B - Declines  		Initiation
            //************************************************************************************************

            long ll_branch_number = 0;
            long ll_rtn_code = 0;
            long ll_row_count = 0;
            long? ll_decline_exist = 0;
            long ll_quantity_needed = 0;
            long ll_count = 0;
            string ls_title = null;
            string ls_find = null;
            string ls_filter = null;
            //Set the title of the window
            il_decline_levelProperty = astr_mess.a_long[1];
            il_order_numberProperty = astr_mess.a_long[2];
            il_supplier_numberProperty = astr_mess.a_long[3];
            il_parent_doc_numberProperty = astr_mess.a_long[4];
            is_parent_doc_typeProperty = astr_mess.a_string[1];
            il_employee_numberProperty = astr_mess.a_long[6];
            il_serial_numberProperty = astr_mess.a_long[7];
            if (il_decline_levelProperty == 1) // Only for Item level
            {
                il_doc_quantityProperty = astr_mess.a_long[8];
                il_actual_quantityProperty = astr_mess.a_long[9];
            }
            // SharonS - 1.2.42.0 - 2009-01-18 - CR#1100 - SPUCM00000115 - Get distributor_number 
            il_distributor_numberProperty = astr_mess.a_long[10];
            //End
            // AlexKh - 1.1.26.0 - 2015-01-22 - SPUCM00005189 - oversupplied item indicator
            if (System.Extensions.CollectionExtensions.UBound(astr_mess.a_boolean) >= 0)
            {
                ib_oversuppliedProperty = astr_mess.a_boolean[0];
            }
            if (il_decline_levelProperty == 0)
            {
                //decline for doc
                if (is_parent_doc_typeProperty == "I")
                {
                    ls_title = "�������� �������� ���� " + il_parent_doc_numberProperty.ToString() + ":";
                }
                else
                {
                    ls_title = "�������� ��.�����" + il_parent_doc_numberProperty.ToString() + ":";
                }
            }
            else
            {
                //decline for item
                il_material_numberProperty = astr_mess.a_long[5];
                ls_title = "�������� ����� ���� " + il_material_numberProperty.ToString() + ":";
            }
            st_header.Text = ls_title;
            ll_rtn_code = wf_retrieve_data();
            //SharonS - 1.2.26.8 - 2008-03-17 - If there are no declines 
            if (ll_rtn_code == -1)
            {
                return -1;
            }
            //End
            ls_find = "checkedField == 1";

            ll_row_count = dw_declines.RowCount();

            ll_decline_exist = dw_declines.Find(ls_find, 0, (int)ll_row_count);
            if (ll_decline_exist >= 0)
            {
                il_orig_decline_existProperty = 1;
            }
            // SharonS - 1.2.28.0 - 2008-03-25 - Set declined quantity


            // SharonS - 1.2.42.0 - 2009-01-20 - CR#1100 - SPUCM00000115 - Set distributor_number 

            for (ll_count = 0; ll_count < dw_declines.RowCount(); ll_count++)
            {
                dw_declines.SetItem(ll_count, "distributor_number", il_distributor_numberProperty);
                dw_declines.SetItemStatus((int)ll_count, 0, ModelBuffer.Primary, ModelAction.None);
            }
            //End
            ls_filter = "is_reject_with_qty == 1 and checkedField == 0";

            dw_declines.SetFilter(ls_filter);

            dw_declines.Filter();

            for (ll_count = 0; ll_count < dw_declines.RowCount(); ll_count++)
            {

                dw_declines.SetItem(ll_count, "original_quantity", Convert.ToDecimal(il_doc_quantityProperty));

                dw_declines.SetItem(ll_count, "move_actual_quantity", Convert.ToDecimal(il_actual_quantityProperty));

                dw_declines.SetItem(ll_count, "reject_quantity", Convert.ToDecimal(il_actual_quantityProperty));



                dw_declines.SetItemStatus((int)ll_count, 0, ModelBuffer.Primary, ModelAction.None);
            }

            dw_declines.SetFilter("");

            dw_declines.Filter();
            // AlexKh - 1.2.46.5 - 2010-12-26 - CR#1143 - filter and present only manual declines
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("automatic_declines", "boolean", "false", "Use automatic declines .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "true")
            {

                if (dw_declines.RowCount() > 0)
                {
                    // AlexKh - 1.1.26.0 - 2015-01-22 - SPUCM00005189 - check which decline to present
                    if (ib_oversuppliedProperty)
                    {
                        // Present only decline 102 - not matching quantity (absence)

                        ll_rtn_code = dw_declines.SetFilter("is_automatic_applied == 2");
                    }
                    else
                    {
                        // Present only manual declines

                        ll_rtn_code = dw_declines.SetFilter("is_automatic_applied == 0");
                    }

                    ll_rtn_code = dw_declines.Filter();

                    ll_rtn_code = dw_declines.RowCount();
                }
            }


            //End
            return 1;
        }

        public int wf_retrieve_data() //**********************************************************************************************
        {

            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");
            //*Object:								rw_reject_reason
            //*Function/Event  Name:			wf_retrieve_data
            //*Purpose:							Retrieve item/doc declines.
            //*
            //*Arguments:						None.
            //*										
            //*Return:								Integer.
            //*
            //*Date				Programer		Version		Task#								Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-01-2007		PninaSG    		1.2.23.0 		10003 - B2B - Declines 		Initiation
            //************************************************************************************************

            long ll_ret = 0;
            //Retrieve data



            ll_ret = dw_declines.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number,il_supplier_numberProperty,il_order_numberProperty,is_parent_doc_typeProperty,il_parent_doc_numberProperty,il_material_numberProperty,il_employee_numberProperty, il_decline_levelProperty);
            //SharonS - 1.2.26.8 - 2008-03-17 - Only if there are any declines 
            if (ll_ret > 0)
            {
               il_max_line_numberProperty = Convert.ToInt64(dw_declines.GetItem<d_declines_reasons_display>(0).max_line_number);
                if (il_max_line_numberProperty == 0)
                {
                   il_max_line_numberProperty = 0;
                }
                //SharonS - 1.2.26.8 - 2008-03-17 - Only if there are any declines 	
            }
            else
            {
                return -1;
            }
            //
            return 1;
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
