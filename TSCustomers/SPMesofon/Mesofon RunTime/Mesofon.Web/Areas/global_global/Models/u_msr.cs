using System;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using Mesofon.Common;

namespace global
{
	// Creation Time:   08/11/2015 10:21:01 (Utc)
	// Comments:        Object for IBM 4695 magnetic reader
	// 
	public class u_msr : CompositeElement
	{
		public string magnetic_card_track2_result;
		public delegate void ue_activated_EventHandler();
		public event ue_activated_EventHandler ue_activatedEvent;
		public delegate void ue_destroy_EventHandler();
		public event ue_destroy_EventHandler ue_destroyEvent;
		public void ue_activated()
		{
			// ---------------------------------------------------------------------------------
			// Write the code to deal with the magnetic card string here
			// Use the function "get_card_string()" that return the magnetic card track 2 string
			// after manipulation: adding prefix,suffix and changing the separator sign
			// ---------------------------------------------------------------------------------
		}
		public string get_card_string() //This function is the interface to the card string after manipulation
		{
			return magnetic_card_track2_resultProperty;
		}
		public u_msr()
		{
		    

		}

	    public async Task constructor()
	    {
	        string msr_exist_reg_read = null;
	        if (isempty_stringClass.isempty_string(masofon.masofon.Instance.s_reg_key))
	        {
	            masofon.masofon.Instance.s_reg_key = "HKEY_CURRENT_USER\\Software\\NET-POS\\";
	        }
	        msr_exist_reg_read = RegToDatabase.GetRegistryItem(masofon.masofon.Instance.s_reg_key + "Hardware\\MSR", "4695", "no", "");
	        if (msr_exist_reg_read == "yes")
	        {
	            masofon.masofon.Instance.msr4695_exist = true;
	        }
	        else
	        {
	            masofon.masofon.Instance.msr4695_exist = false;
	        }
	        if (masofon.masofon.Instance.msr4695_exist)
	        {
	            await init_msr();
	        }
	        // --------------------Initializing the MSR Object ------------------------------------
	    }

	    public async Task<int> init_msr()
	    {
	        CompositeElement ole_msr = this.GetVisualElementById<CompositeElement>("ole_msr");

            //Rem by itzik
            //if (Convert.ToBoolean(ole_msr).Open("MSR1"))
	        //{
	        //    await masofon.masofon.Instance.guo_msg.uf_msg(await f_get_error_message_numberClass.f_get_error_message_number(2681), "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number(2682));
	        //    (ole_msr).close();
	        //}
	        //else
	        //{


         //       if (Convert.ToBoolean((ole_msr).Claim(0)))
	        //    {
	        //        await masofon.masofon.Instance.guo_msg.uf_msg(await f_get_error_message_numberClass.f_get_error_message_number(2681), "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number(2683));
	        //    }
	        //    else
	        //    {
	                
	        //        (ole_msr).DeviceEnabled = true;
	        //        (ole_msr).DataEventEnabled = true;
	        //    }
	        //}
	        return 0;
	    }
        public string magnetic_card_track2_resultProperty
		{
			get { return this.magnetic_card_track2_result; }
			set { this.magnetic_card_track2_result = value; }
		}
	}
}
