using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;
using globalAlias = global;

namespace global
{
    // Creation Time:   08/11/2015 10:20:57 (Utc)
    // Comments:        
    // 
    public class rw_not_arrived_inv_report : WindowElement
    {
        public long il_branch_number;
        public const long il_max_window_height = 1008;
        public const long il_max_window_width = 1088;
        public long il_shipment_number;
        public delegate void ue_print_for_shipment_EventHandler();
        public event globalAlias.rw_not_arrived_inv_report.ue_print_for_shipment_EventHandler ue_print_for_shipmentEvent;
        public rw_not_arrived_inv_report(string ls_params)
        {
            Params = ls_params;
        }

        public string Params { get; set; }

        public long il_branch_numberProperty
        {
            get { return this.il_branch_number; }
            set { this.il_branch_number = value; }
        }
        public long il_shipment_numberProperty
        {
            get { return this.il_shipment_number; }
            set { this.il_shipment_number = value; }
        }
        public void LoadData(ref string ls_branch)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_numberProperty } };
                    string sql = "SELECT branch_name FROM global_parameters WHERE serial_number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_branch = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_supplier_number, long ll_invoice_number, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_branch_numberProperty }, { "@1", ll_supplier_number }, { "@2", ll_invoice_number } };
                    string sql = "SELECT Count(*) FROM invoice_details WHERE invoice_details.branch_number = @0 AND invoice_details.supplier_number = @1 AND invoice_details.invoice_number = @2 AND invoice_details.material_quantity > 0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
