using System;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;
using Mesofon.Common;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_cb_ok : u_cb
	{
		public u_cb_ok()
		{
			VisualElementHelper.CreateFromView<u_cb>("u_cb", "u_cb", "global_global");
		    constructor();

		}
		
		public void clicked(object sender, EventArgs e) //--------------------------------------------------------------------
        {
			//Function:			public u_cb_ok.clicked()
			//
			// Returns:         long
			//
			// Copyright  - Stas
			//
			// Date Created: 03/08/2005
			//
			// Description:	
			// 					close parent window with retun or without by the parameter
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			base.u_cb_Click( sender,  e);
			if (ib_return_flag)
			{
				WindowHelper.Close((WindowElement)Parent, il_return_value.ToString());
			}
			else
			{
				Parent.Hide();
			}
		}
		public void constructor() //--------------------------------------------------------------------
		{
			//Function:			public u_cb_ok.constructor()
			//
			// Returns:         long
			//
			// Copyright  - Stas
			//
			// Date Created: 03/08/2005
			//
			// Description:	
			//		 					sets default values. 
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			base.constructor();
			//.. return value is 1 by default
			il_return_value = 1;
			//.. if true --> closewithreturn, otherwise simple close
			ib_return_flag = true;
		}
	}
}
