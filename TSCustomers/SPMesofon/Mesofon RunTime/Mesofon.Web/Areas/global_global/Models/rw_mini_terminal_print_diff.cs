using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:20:57 (Utc)
	// Comments:        
	// 
	public class rw_mini_terminal_print_diff : WindowElement
	{
		public const long il_max_window_height = 1008;
		public const long il_max_window_width = 1088;
		// SharonS - 1.2.37.13 - 2008-12-18- Task#10012 - SPUCM00000094
		public string is_copies;
		public rw_mini_terminal_print_diff()
		{
		}
		
		public string is_copiesProperty
		{
			get { return this.is_copies; }
			set { this.is_copies = value; }
		}

        
    }
}
