﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofon;
using Mesofon.Data;
using Mesofon.Repository;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:20:54 (Utc)
    // Comments:        
    // 
    public class nvuo_b2b_decline : nvuo_b2b_transactions
    {
        public IRepository ids_declines;
        public IRepository ids_decline_reasons;
        public string is_message_type;
        public long il_response_no;
        public DateTime? idt_parent_doc_suply_dt;
        public DateTime? idt_parent_doc_dt;
        public const string B2B_DECLINE_HEADER_34 = "34";
        public const string B2B_REJECT_HEADER_27 = "27";
        public const string B2B_REJECT_DOUBLE_DOC_28 = "28";
        public const string B2B_CONFIRM_HEADER_29 = "29";
        public const string B2B_ITEM_WAS_NOT_ORDERED_114 = "114";
        public const string B2B_ITEM_SUPPLYING_BIGGER_THEN_ORDERED_113 = "113";
        public const string B2B_ITEM_DOES_NOT_EXIST_IN_CATALOGUE_115 = "115";
        public const string B2B_ITEM_LOCKED_FOR_BUYING_116 = "116";
        public const string B2B_ITEM_NOT_IN_VARIETY_104 = "104";
        public const string B2B_ITEM_VALIDITY_DATE_EXPIRED_103 = "103";
        public const string B2B_ITEM_NOT_MATCHING_QUANTITY_102 = "102";
        public const string B2B_INVOICE_DELETE_NOT_PROCESSED_12 = "12";
        public int uf_insert_header(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_insert_header
            //*Purpose:					Insert haeder section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            decimal ld_res_date_time = default(decimal);
            decimal ld_delivery_date_time = default(decimal);
            decimal ld_curr_date_time = default(decimal);
            long ll_response_no = 0;
            long ll_parent_date_time = 0;
            long ll_ref_date = 0;
            long ll_order_no = 0;
            long ll_order_date = 0;
            string ls_invoice_number = null;
            string ls_parent_doc_type = null;
            string ls_supply_doc_type = null;
            DateTime? ldt_date_time = default(DateTime);
            DateTime? ldt_parent_date_time = default(DateTime);
            // ResponseNo
            // SharonS - 1.1.24.1 - 2008-03-05 - Change il_response_no to be String
            //ids_decline_xml_temp.SetItem(al_row, "responseno", il_response_no)
            
            ids_decline_xml_temp.SetItem(al_row, "responseno", il_response_no.ToString());
            //
            // ResponseCode
            
            ids_decline_xml_temp.SetItem(al_row, "responsecode", is_message_type);
            // ResponseDateTime

            ldt_date_time = ids_declines.GetItemValue<DateTime>((int)al_row, "update_datetime");
            ld_res_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
            if (ld_res_date_time == 0)
            {
                return -1;
            }
            
            ids_decline_xml_temp.SetItem(al_row, "responsedatetime", ld_res_date_time);
            // DeliveryDateTime

            ldt_date_time = ids_declines.GetItemValue<DateTime>((int)al_row, "parent_supply_date");
            ld_delivery_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
            
            ids_decline_xml_temp.SetItem(al_row, "deliverydatetime", ld_delivery_date_time);
            // SharonS - 1.2.42.0 - 2009-01-18 - CR#1107 - SPUCM00000777 - Set supplydoctype
            // SupplyDocType

            ls_parent_doc_type = ids_declines.GetItemValue<string>(al_row, "parent_doc_type");
            if (ls_parent_doc_type == "I")
            {
                ls_supply_doc_type = "INV";
            }
            else if (ls_parent_doc_type == "P")
            {
                ls_supply_doc_type = "ASN";
            }
            else
            {
                ls_supply_doc_type = "";
            }
            
            ids_decline_xml_temp.SetItem(al_row, "supplydoctype", ls_supply_doc_type);
            // End
            // InvoiceNo

            ls_invoice_number = ids_declines.GetItemValue<int>((int)al_row, "parent_doc_number").ToString();
            
            ids_decline_xml_temp.SetItem(al_row, "invoiceno", ls_invoice_number);
            // InvoiceDate

            ldt_parent_date_time = ids_declines.GetItemValue<DateTime>((int)al_row, "parent_doc_date");
            ll_parent_date_time = Convert.ToInt64(ldt_parent_date_time.Value.ToString("yyyyMMdd"));
            
            ids_decline_xml_temp.SetItem(al_row, "invoicedate", ll_parent_date_time);
            // AlexKh - 1.2.46.7 - 2011-02-27 - SPUCM00002474 - populate store details
            // StoreNo
            //IF isempty_string(qs_parameters.edi_number) THEN RETURN -1
            //ids_decline_xml_temp.SetItem(al_row, "storeno", qs_parameters.edi_number)
            // StoreName
            //ids_decline_xml_temp.SetItem(al_row, "storename", qs_parameters.company_name)
            if (!uf_set_store_details(al_row))
            {
                return -1;
            }
            // SupplierNo
            if (isempty_stringClass.isempty_string(is_supplier_edi))
            {
                return -1;
            }
            
            ids_decline_xml_temp.SetItem(al_row, "supplierno", is_supplier_edi);
            // RefQual, RefNo & RefDate
            if (is_message_type == "29" || is_message_type == "34")
            {
                //	CONFIRM - Bul or DECLINE
                //uf_calculate_declines
                ldt_date_time =DateTime.Now;
                ld_curr_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
                
                ids_decline_xml_temp.SetItem(al_row, "refqual", "ACE");
                
                ids_decline_xml_temp.SetItem(al_row, "refno", 0);
                
                ids_decline_xml_temp.SetItem(al_row, "refdate", ld_curr_date_time);
            }
            // AlexKh - 1.2.44.36 - 2009-11-04 - SPUCM00001699 - populate order number into RefNo

            ll_order_no = ids_declines.GetItemValue<long>((int)al_row, "order_number");
            
            ids_decline_xml_temp.SetItem(al_row, "refno", ll_order_no.ToString());
            return 1;
        }
        public int uf_insert_details(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_order
            //*Function Name:			uf_insert_details
            //*Purpose:					Insert the Deatails section of the order XML
            //*  
            //*Arguments:				Long	- Row No (al_row)
            //*Return:						Integer - 1 succeed.
            //*											-1 not  succeed.
            //*Date 			Programer		Version	Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*31-05-2007		PninaSG			0.1.0 		B2B 		Initiate Version
            //*18-10-2007		SharonS			1.0.1		1057		Set the quantity to be the bonus quantity.
            //************************************************************************************************

            string ls_item_barcode = null;
            string ls_reason_code = null;
            string ls_yarpa_code = null;
            string ls_material_barcode = null;
            char lc_doc_type = '\0';
            long ll_amend_qty = 0;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_doc_number = 0;
            long ll_item_number = 0;
            long ll_line_number = 0;
            long ll_dprt_no = 0;
            // ItemBarcode

            ls_item_barcode = ids_declines.GetItemValue<string>(al_row, "barcode");
            // SharonS - 1.2.42.11 - 2009-03-24 - SPUCM00001141 - Get YARPA code if needed.

            ll_dprt_no = ids_declines.GetItemValue<long>((int)al_row, "dprt_no");
            if (ll_dprt_no == 8 || ll_dprt_no == 15)
            {

                ll_item_number = ids_declines.GetItemValue<long>((int)al_row, "material_number");
                if (this.uf_get_yarpa_code(ll_item_number, ref ls_yarpa_code))
                {
                    if (!isempty_stringClass.isempty_string(ls_yarpa_code))
                    {
                        ls_item_barcode = ls_yarpa_code;
                    }
                }
            }
            // End
            
            ids_decline_xml.SetItem(al_row - 1, "itembarcode", ls_item_barcode);
            // LineNo
            // SharonS - 1.2.42.1 - 2009-02-24 - SPUCM00000983 - Set 'lineno' element to be the line number from the invoice/packlist message

            ll_branch_number = ids_declines.GetItemValue<long>((int)al_row, "branch_number");

            ll_supplier_number = ids_declines.GetItemValue<long>((int)al_row, "supplier_number");

            ll_order_number = ids_declines.GetItemValue<long>((int)al_row, "order_number");

            ll_doc_number = ids_declines.GetItemValue<long>((int)al_row, "parent_doc_number");

            ll_item_number = ids_declines.GetItemValue<long>((int)al_row, "material_number");

            lc_doc_type = Convert.ToChar(ids_declines.GetItemValue<string>(al_row, "parent_doc_type"));
            // SharonS - 1.2.42.12 - 2009-03-25 - SPUCM00000983 - Fix for bonus and known items

            ls_material_barcode = ids_declines.GetItemValue<string>(al_row, "barcode");
            if (lc_doc_type.ToString() == "I")
            {
                if (ll_item_number == 0)
                {
                    LoadData(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ls_material_barcode, ref ll_line_number);
                }
                else
                {
                    LoadData1(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ref ll_line_number);
                }
            }
            else
            {
                if (ll_item_number == 0)
                {
                    LoadData2(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ls_material_barcode, ref ll_line_number);
                }
                else
                {
                    LoadData3(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ref ll_line_number);
                }
                // SharonS - End
            }


            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1 || masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
            {
                ll_line_number = 0;
            }
            
            ids_decline_xml.SetItem(al_row - 1, "lineno", ll_line_number);
            //ids_decline_xml.SetItem(al_row, "lineno", al_row)           
            // End 
            // AmendQty
            // AlexKh - 1.2.45.9 - 2010-01-06 - SPUCM00001844 - use reject quantity instead actual quantity
            //ll_amend_qty = ids_declines.GetItem<int>(al_row, "move_actual_quantity")

            ll_amend_qty = ids_declines.GetItemValue<long>((int)al_row, "reject_quantity");
            
            ids_decline_xml.SetItem(al_row - 1, "amendqty", ll_amend_qty);
            //ReasonCode

            ls_reason_code = ids_declines.GetItemValue<string>(al_row, "move_decline_code");
            
            ids_decline_xml.SetItem(al_row - 1, "reasoncode", ls_reason_code);
            return 1;
        }
        public bool uf_save_as_xml() //**********************************************************************************************
        {
            //*Object:						nvou_b2b_order
            //*Function Name:			uf_save_as_xml
            //*Purpose:					Save the DW as XML file.
            //*  
            //*Arguments:				None.
            //*Return:						Boolean: 1 - succeeded.
            //*										 	-1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

 
            return ids_decline_xml.SaveToXml(Convert.ToString(is_file_spec)) == 1;
        }
        public async Task<int> uf_write_log(string as_message) //********************************************************************************************************************************
        {
            //*Object:								nvuo_b2b_order
            //*Function/Event  Name:			uf_write_log
            //*Purpose:							Write into log file
            //*
            //*Arguments:					Pass By		Argument Type			Argument Name
            //*									-------------------------------------------------------------------------------------------------------------------
            //*									Value			String						as_message- the message to write into the file.
            //*
            //*Return:							Integer: 1- Success
            //*												  -1 -Failed.
            //*
            //*Date 				Programer				Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
            //*03-12-2007 		Pnina S.G				1.0.2			CR#1060		Initiation
            //**********************************************************************************************************************************

            int li_file = 0;
            int li_file_open = 0;
            string ls_log_file_name = null;
            string ls_message = null;
            string ls_doc_type = null;
            string ls_msg_header = null;
            string ls_log_mode = null;
            bool ib_log_mode = false;
            // SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Add data to log file
            ls_msg_header = "------------------------------------------------------------------------------------------------------------------------" + "\r" + "\n";
            ls_msg_header += "Employee Number: " + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + " Date: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n";
            ls_msg_header += "Response number: " + il_response_no.ToString() + " Response type: " + is_message_type + "\r" + "\n";
            as_message = ls_msg_header + as_message;
            // End
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log
            await masofonAlias.masofon.Instance.guo_logs_in_table_service.uf_write_log_table(as_message, u_logs_in_table_service.SERVICE_CODE_MASOFON_LOG, 1, "nvuo_b2b_decline.uf_write_log", "", "", 3);
            // SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Log on/off
            if (!masofonAlias.masofon.Instance.gb_b2b_log)
            {
                return 1;
            }
            // End
            //// AlexKh - 1.2.48.3 - 2011-05-24 - SPUCM00002871 - change log path 
            ////ls_log_file_name = "c:\" + String(gs_vars.branch_number) + "_DECLINE_MSG.log"
            //ls_log_file_name = "c:\NET-POS\log\" + String(gs_vars.branch_number) + "_DECLINE_MSG.log"
            //
            //li_file_open = FileOpen(ls_log_file_name, LineMode!, Write!, LockWrite!, Append!)
            //IF li_file_open > 0 THEN
            //	li_file = FileWrite(li_file_open, as_message)
            //	IF li_file < 0 OR IsNull(li_file) THEN
            //		guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not write the log file.")
            //		RETURN -1
            //	END IF
            //	li_file = FileClose(li_file_open)
            //	IF li_file < 0 OR IsNull(li_file) THEN
            //		guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not close the log file.")
            //		RETURN -1
            //	END IF
            //	RETURN 1
            //ELSE
            //	guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not open the log file.")
            //	RETURN -1
            //END IF
            //
            //
            return 0;
        }
        public async Task<int> uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_decline
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            int li_index = 0;
            int li_row = 0;
            int li_order_row = 0;
            int li_order_items = 0;
            long ll_find = 0;
            long ll_message_date = 0;
            long ll_supplier_num = 0;
            long ll_distributor_num = 0;
            string ls_find = null;
            string ls_organization_edi = null;
            string ls_supplier_edi = null;
            string ls_message = null;
            string ls_message_time = null;
            ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
            if (isempty_stringClass.isempty_string(ls_organization_edi))
            {
                return -1;
            }
            
            ids_decline_xml_temp.SetItem(al_row, "sender", ls_organization_edi);
            //SharonS - 1.2.37.11 - 20-11-2008 - CR#1094 - SPUCM00000671
            //IF isempty_string(is_supplier_edi) THEN RETURN -1 
            //ids_decline_xml_temp.SetItem(al_row, "receiver", is_supplier_edi)

            ll_supplier_num = ids_declines.GetItemValue<long>(0, "supplier_number");
            if (ll_supplier_num == 0 || ll_supplier_num == 0)
            {
                ls_message = "supplier_number IS NOT VALID.";
                await uf_write_log(ls_message);
                return -1;
            }
            is_parent_supplier_edi = f_get_parent_supplier_ediClass.f_get_parent_supplier_edi(ll_supplier_num);
            // SharonS - 1.2.42.0 - 2009-01-18 - CR#1100 - SPUCM00000115 - Add distributor_number 

            ll_distributor_num = ids_declines.GetItemValue<long>(0, "distributor_number");
            // AlexKh - 1.2.48.26 - 2012-05-23 - SPUCM00003402 - check distributor is crossdoc
            if (ll_distributor_num != 0 && !f_is_crossdoc_distributorClass.f_is_crossdoc_distributor(ll_distributor_num))
            {
                is_supplier_edi = f_get_supplier_distributor_ediClass.f_get_supplier_distributor_edi(ll_supplier_num, ll_distributor_num);
                is_parent_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_distributor_num);
            }
            else if (f_is_crossdoc_distributorClass.f_is_crossdoc_distributor(ll_distributor_num))
            {
                is_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_supplier_num);
            }
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                is_parent_supplier_edi = is_supplier_edi;
            }
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                return -1;
            }
            
            ids_decline_xml_temp.SetItem(al_row, "receiver", is_parent_supplier_edi);
            // End
            
            ids_decline_xml_temp.SetItem(al_row, "doctype", "MMDR01");
            
            ids_decline_xml_temp.SetItem(al_row, "aprf", "MMDR01");
            
            ids_decline_xml_temp.SetItem(al_row, "snrf", "");
            
            ids_decline_xml_temp.SetItem(al_row, "ackn", "1"); // 1 - Required        
            
            ids_decline_xml_temp.SetItem(al_row, "testind", "1"); // 1 - Test   
            ll_message_date = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd"));
            
            ids_decline_xml_temp.SetItem(al_row, "messdate", ll_message_date);
            // SharonS - 1.2.42.0 - 2009-01-29 - SPUCM00000853 - Time format
            //ll_message_time = Long(String(Today(), "hhmmdd"))
            // SharonS - 1.2.44.4 - 2009-05-21 - SPUCM00001339 - Time format
            //ll_message_time = Long(String(Today(), "hhmmss"))
            ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("hhmmss");
            // End
            //ids_decline_xml_temp.SetItem(al_row, "messtime", ll_message_time)
            
            ids_decline_xml_temp.SetItem(al_row, "messtime", ls_message_time);
            // End
            return 1;
        }
        public async Task<FuncResults<int, IRepository[], bool>> uf_send_transaction( int ai_file_status, string as_supplier_edi, IRepository[] ads_data) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_send_transaction
            //*Purpose:					Create the B2B message XML file.
            //*								This function will be coded in the decendance objects.
            //*								In each object a defferent message will be created.
            //*  
            //*Arguments:				Integer - file_status
            //*Return:						BOOLEAN - TRUE - Creation of B2B message XML file succeded.
            //												 FALSE - Creation of B2B message XML file failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Create the B2B message XML file.
            //************************************************************************************************

            int li_decline_row = 0;
            int li_index = 0;
            long li_decline_row_count = 0;
            long ll_row = 0;
            long ll_decline_no = 0;
            long ll_order_items = 0;
            long ll_row_copy = 0;
            long ll_row_no = 0;
            long ll_ret = 0;
            string ls_message = null;
            // Check validity
            if (CollectionExtensions.UBound(ads_data) < 0)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            
            li_decline_row_count = ads_data[0].RowCount();
            if (li_decline_row_count < 1)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_declines = ads_data[0];
            
            ll_ret = ids_declines.RowCount();
            is_supplier_edi = as_supplier_edi;
            // SharonS - 1.2.42.0 - 2009-01-27 - CR#1100 - SPUCM00000115
            is_parent_supplier_edi = is_supplier_edi;
            // End
            // Get Response No
            il_response_no = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(116);
            if (il_response_no == 0)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Create and define IRepository
            
            ids_decline_xml.Reset();
            
            ids_decline_xml_temp.Reset();
            
            ll_row = ids_decline_xml_temp.Insert(0);
            // Set the envalope data in the XML DW
            if ( await this.uf_insert_envelope(ll_row) < 0)
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("כישלון", "", "stopsign!", "שים לב- כישלון בכתיבת מעטפת מסר ההסתיגות - ההיסתיגות לא תישלח במסר דיגיטלי");
                ls_message = "Decline - Failed in writing file envelop.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Set the header data in the XML DW
            if (this.uf_insert_header(ll_row) < 0)
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("כישלון", "", "stopsign!", "שים לב-כישלון בכתיבת כותרת מסר ההסתיגות-ההיסתיגות לא תשלח במסר דיגיטלי");
                ls_message = "Decline No: " + ll_decline_no.ToString() + " - Failed in writing file header.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // AlexKh - 1.2.46.5 - 2010-12-29 - CR#1143 - add also cancel message type
            // Only for Confirm and Reject
            if (is_message_type == B2B_CONFIRM_HEADER_29 || (is_message_type == B2B_REJECT_HEADER_27 || is_message_type == B2B_INVOICE_DELETE_NOT_PROCESSED_12))
            {


                
                ll_row_copy = (long)ids_decline_xml_temp.RowsCopy(1, 1, ModelBuffer.Primary, ids_decline_xml, li_index + 1, ModelBuffer.Primary);
                 
                 
                
                 
                ids_decline_xml.Export("xmlconfirm");
                for (li_index = 1; li_index <= (int)li_decline_row_count; li_index++)
                {
                    
                    ids_declines.SetItem(li_index, "b2b_msg_decline_ref", il_response_no.ToString());
                    
                    ids_declines.SetItem(li_index, "b2b_status", "1");
                }
            }
            else
            {
                // Load the xml DW
                 
                 
                
                 
                ids_decline_xml.Export("XMLExport");
                for (li_index = 2; li_index <= (int)li_decline_row_count; li_index++)
                {
                    
                    ids_declines.SetItem(li_index, "b2b_msg_decline_ref", il_response_no.ToString());
                    
                    ids_declines.SetItem(li_index, "b2b_status", "1");


                    
                    ll_row_copy = (long)ids_decline_xml_temp.RowsCopy(1, 1, ModelBuffer.Primary, ids_decline_xml, li_index, ModelBuffer.Primary);
                    if (this.uf_insert_details(li_index) < 0)
                    {
                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("כישלון", "", "stopsign!", " שים לב -כישלון בכתיבת מסר ההסתיגות - ההסתיגות לא תישלח במסר דיגיטלי");
                        ls_message = "Decline - Failed in writing file details.";
                        await uf_write_log(ls_message);
                        ai_file_status = 0;
                        return FuncResults.Return(ai_file_status, ads_data, false);
                    }
                }
            }


            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_ret = ids_declines.Update(unitOfWork);
            }
            // Create xml file from datawindow
            if (!uf_save_as_xml())
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("כישלון", "", "stopsign!", "שים לב - כישלון ביצירת\\כתיבת מסר ההיסתיגות - ההסתיגות לא תישלח דיגיטלית");
                ls_message = "Decline No: " + ll_decline_no.ToString() + " - Failed to Create/Write to File.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            else
            {
                ai_file_status = 1;
            }
            return FuncResults.Return(ai_file_status, ads_data, true);
        }
        public bool uf_set_message_type(string as_message_type) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_set_message_type
            //*Purpose:					Set message type: Debit, Return or Confirm.
            //*  
            //*Arguments:				String	 - as_message_type
            //*Return:						Boolean - TRUE - Succeed.
            //*										      FALSE - Faild.
            //*Date 			Programer		Version		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*21-01-2008		SharonS			1.2.23.0 		10001		Initiate Version
            //************************************************************************************************

            if (as_message_type == "DECLINE")
            {
                //Accepted with amendment
                is_message_type = B2B_DECLINE_HEADER_34;
            }
            else if (as_message_type == "REJECT")
            {
                //Not accepted
                is_message_type = B2B_REJECT_HEADER_27;
            }
            else if (as_message_type == "CONFIRM")
            {
                //Accepted without amendment
                is_message_type = B2B_CONFIRM_HEADER_29;
            }
            else if (as_message_type == "CANCEL")
            {
                //Invoice canceled
                is_message_type = B2B_INVOICE_DELETE_NOT_PROCESSED_12;
            }
            return true;
        }
        public bool uf_check_item_in_variety(long al_item_number, ref char ac_item_status) //**********************************************************************************************
        {
            LoadData4(al_item_number, ref ac_item_status);

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                return false;
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0) //-1-	//.. not found or Return more then one row
            {
                return false; //.. select item by item_codes table
            }
            return true;
        }
        public bool uf_check_item_in_order(long al_branch_number, long al_supplier_number, long al_order_number, long al_item_number, ref decimal adec_material_quantity) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_decline
            //*Function Name:	uf_check_item_in_order
            //*Purpose: 			For every item in the invoice check if it was ordered
            //*Arguments: 		Long			al_branch_number
            //*						Long			al_supplier_number
            //*						Long			al_order_number
            //*						Long			al_item_number
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version		Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*23-11-2010		AlexKh			1.2.46.4		CR#1143		Initial version
            //************************************************************************************************

            long ll_ret = 0;
            long? ll_found = 0;
            IRepository lds_supplier_order_details;


            lds_supplier_order_details = new d_sql_supplier_order_detailsRepository();



            ll_ret = lds_supplier_order_details.Retrieve(al_branch_number, al_order_number, al_supplier_number);
            if (ll_ret <= 0)
            {



                return false;
            }
            
            ll_found = lds_supplier_order_details.Find("material_number == " + al_item_number.ToString(), 0, (int)ll_ret);
            if (ll_found >= 0)
            {

                adec_material_quantity = lds_supplier_order_details.GetItemValue<decimal>((int)ll_found, "material_quantity");



                return true;
            }


            return false;
        }

        public async Task<FuncResults<IRepository,bool>> uf_populate_decline_details( IRepository ads_declines_reasons_for_doc_for_xml, string as_decline_code, long al_row) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_decline
            //*Function Name:	uf_populate_decline_details
            //*Purpose: 			Populate decline details
            //*Arguments: 		REF Datastore	ads_declines_reasons_for_doc_for_xml
            //*						String 			as_decline_code
            //*						Long				al_row
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version		Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*26-12-2010		AlexKh			1.2.46.5		CR#1143		Initial version
            //************************************************************************************************

            long ll_ret = 0;
            int? ll_found = 0;
            if (!(ids_decline_reasons != null))
            {


                ids_decline_reasons = new dddw_get_return_reasonsRepository();



                ll_ret = ids_decline_reasons.Retrieve(2);
                if (ll_ret <= 0)
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", "משיכת סוגי הסתייגות נכשלה");
                    return FuncResults.Return(ads_declines_reasons_for_doc_for_xml,false);
                }
            }
            
            
            ll_found = ids_decline_reasons.Find("decline_code == '" + as_decline_code + "'", 0, ids_decline_reasons.RowCount());
            if (ll_found <= 0)
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", " סוג הסתייגות " + as_decline_code + " לא נמצא במבנה נתונים ");
                return FuncResults.Return(ads_declines_reasons_for_doc_for_xml, false);
            }

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "move_decline_code", Convert.ToString(ids_decline_reasons.GetItemValue<string>(ll_found.Value, "decline_code")));

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "decline_number", ids_decline_reasons.GetItemValue<int>(ll_found.Value, "decline_number").ToString());

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "is_reject_with_qty", ids_decline_reasons.GetItemValue<int>(ll_found.Value, "is_reject_with_qty").ToString());

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "is_reject_all_invoice", ids_decline_reasons.GetItemValue<int>(ll_found.Value, "is_reject_all_invoice").ToString());

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "is_automatic_applied", ids_decline_reasons.GetItemValue<int>(ll_found.Value, "is_automatic_applied").ToString());

            
            ads_declines_reasons_for_doc_for_xml.SetItem(al_row, "is_red_stamp", ids_decline_reasons.GetItemValue<int>(ll_found.Value, "is_red_stamp").ToString());
            return FuncResults.Return(ads_declines_reasons_for_doc_for_xml, true);
        }
        public async Task<bool> uf_b2b_send_decline(long al_branch_number, long al_supplier_number, long al_distributor_number, string as_parent_doc_type, long al_parent_doc_number, DateTime? adt_parent_doc_dt, DateTime? adt_parent_doc_suply_dt, long al_order_number, bool ab_delete_invoice)
        {
            //**********************************************************************************************
            //*Object:				nvuo_b2b_decline
            //*Function:			uf_b2b_send_decline
            //*Purpose:			Send a digital message of declines for a Invoice/Packlist.(Replace f_b2b_send_decline)
            //*Description:		Send a Reject/Confimation/Decline message, according to the declines related to the document.
            //*			
            //*Arguments:		Pass By		Argument Type		Argument Name
            //*						-------------------------------------------------------------------------
            //*						Value			Long				al_branch_number - Branch number.
            //*						Value			Long				al_supplier_number - Supplier number.
            //*						Value			Long				al_distributor_number - Distributor number.
            //*						Value			String			as_parent_doc_type - I for Invoice / P for Packlist.
            //*						Value			Long				al_parent_doc_number - Invoice/Packlist number. 
            //*						Value			DateTime			adt_parent_doc_dt - Invoice/Packlist move date and time.
            //*						Value			DateTime			adt_parent_doc_suply_dt - Invoice/Packlist supply date and time.
            //*						Value			Long				al_order_numer
            //*										
            //*Return:								Boolean: TRUE - Success
            //*													FALSE - Faliure
            //*
            //*Date				Programer	Version		Task#			Description
            //*-----------------------------------------------------------------------------------------
            //*22-12-2010		AlexKh		1.2.46.5 	CR#1143		Initial
            //******************************************************************************************

            string ls_msg_type = null;
            string ls_msg_sub_type = null;
            string ls_sup_edi_no = null;
            string ls_filter = null;
            int li_decline_sts = 0;
            int li_filter = 0;
            long ll_ret = 0;
            long ll_row_count = 0;
            long ll_row = 0;
            long ll_reject_row = 0;
            long ll_order_no = 0;
            long ll_i = 0;
            bool lb_ret = false;
            IRepository lds_declines_reasons_for_xml;
            IRepository lds_declines_reasons_for_doc_for_xml;
            IRepository[] lds_declines_data = null;
            ls_msg_type = "MMDR01";
            ll_order_no = al_order_number;
            //Call uf_init(), in order to check if the file target directory (outgoing messages) exist and create the file name 
            if (!uf_init(Convert.ToString(ls_msg_type), al_supplier_number, al_parent_doc_number, al_branch_number))
            {
                return false;
            }

            lds_declines_reasons_for_doc_for_xml = new d_declines_reasons_for_doc_for_xmlRepository("", "", "", "", "");

            ll_row_count = lds_declines_reasons_for_doc_for_xml.Retrieve(al_branch_number, al_supplier_number, as_parent_doc_type, al_parent_doc_number);
            // If Retrieve failed
            if (ll_row_count < 0)
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", "משיכת הסתייגויות ידניות נכשלה");
                return false;
            }
            
            lds_declines_reasons_for_doc_for_xml.SetSort("line_number ASC, material_number ASC");
            
            lds_declines_reasons_for_doc_for_xml.Sort();
            idt_parent_doc_suply_dt = adt_parent_doc_suply_dt;
            idt_parent_doc_dt = adt_parent_doc_dt;
            if (ab_delete_invoice)
            {
                ls_msg_sub_type = "CANCEL";
                uf_set_message_type(ls_msg_sub_type);
                ll_ret = uf_cancel_invoice(ref lds_declines_reasons_for_doc_for_xml);
               (await uf_create_new_decline(al_branch_number, al_supplier_number, al_distributor_number, ll_order_no, as_parent_doc_type, al_parent_doc_number, 0, lds_declines_reasons_for_doc_for_xml, B2B_INVOICE_DELETE_NOT_PROCESSED_12, 0)).Retrieve(out lds_declines_reasons_for_doc_for_xml);
                
                lds_declines_reasons_for_doc_for_xml.SetFilter("status = 'a'");
                
                lds_declines_reasons_for_doc_for_xml.Filter();
            }
            else
            {
                //There are no declines for this doc
                if (ll_row_count == 0)
                {
                   (await  uf_create_new_decline(al_branch_number, al_supplier_number, al_distributor_number, ll_order_no, as_parent_doc_type, al_parent_doc_number, 0, lds_declines_reasons_for_doc_for_xml, B2B_CONFIRM_HEADER_29, 0)).Retrieve(out lds_declines_reasons_for_doc_for_xml);
                }
                string temp =await  f_get_parameter_valueClass.f_get_parameter_value("automatic_declines", "boolean", "false", "Use automatic declines .", "gds_find");
                if (temp.ToLower(CultureInfo.InvariantCulture) == "true")
                {
                    lb_ret = uf_calculate_declines(lds_declines_reasons_for_doc_for_xml);
                }
                
                ll_row_count = lds_declines_reasons_for_doc_for_xml.RowCount();
                if (ll_row_count == 1)
                {
                    // Set message type to be a Confirmation message decline code = 29
                    ls_msg_sub_type = "CONFIRM";
                    if (!uf_set_message_type(ls_msg_sub_type))
                    {
                        return false;
                    }
                    if (!(await uf_populate_decline_details( lds_declines_reasons_for_doc_for_xml, B2B_CONFIRM_HEADER_29, 1)).Retrieve(out lds_declines_reasons_for_doc_for_xml))
                    {
                        return false;
                    }
                }
                else
                {
                    // Set message type to be a decline message decline code = 34
                    ls_msg_sub_type = "DECLINE";
                    if (!uf_set_message_type(ls_msg_sub_type))
                    {
                        return false;
                    }
                    if (!(await uf_populate_decline_details( lds_declines_reasons_for_doc_for_xml, B2B_DECLINE_HEADER_34, 1)).Retrieve(out lds_declines_reasons_for_doc_for_xml))
                    {
                        return false;
                    }
                }
            }
            lds_declines_data[1] = lds_declines_reasons_for_doc_for_xml;
            ls_sup_edi_no = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(al_supplier_number);
            if (!(await uf_send_transaction(li_decline_sts, ls_sup_edi_no,lds_declines_data)).Retrieve(out li_decline_sts, out lds_declines_data))
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Failure", "", "stopsign!", "Failure in sending XML file.");
            }
            else
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Success", "", "stopsign!", "XML file was send to the supplier successfully.");
            }
            // Call nvuo_b2b_order.uf_update_outgoing, in order to insert a new row into b2b_outgoing_messages table.
            if (!(await uf_update_outgoing((int)masofonAlias.masofon.Instance.gl_msg_type_decline, li_decline_sts, Convert.ToString(ls_msg_sub_type), al_parent_doc_number, ll_order_no, al_supplier_number, al_branch_number)).Retrieve(out li_decline_sts))
            {
                return false;
            }
            return true;
        }

        // TODO: Missing function. copy from the source
        private bool uf_calculate_declines(IRepository lds_declines_reasons_for_doc_for_xml)
        {
            throw new NotImplementedException();
        }

        public int uf_cancel_invoice(ref IRepository ads_declines_reasons_for_doc_for_xml) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_decline
            //*Function Name:	uf_cancel_invoice
            //*Purpose: 			Handle canceled invoice and send the decline with response code 12
            //*Arguments: 		Ref Datastore			ads_declines_reasons_for_doc_for_xml
            //*Return:				Boolean		TRUE/FALSE
            //*Date				Programer		Version		Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*27-12-2010		AlexKh			1.2.46.5		CR#1143		Initial version
            //************************************************************************************************

            long ll_i = 0;
            long ll_rows = 0;
            
            ll_rows = ads_declines_reasons_for_doc_for_xml.RowCount();
            if (ll_rows > 0)
            {
                for (ll_i = 1; ll_i <= ll_rows; ll_i++)
                {
                    
                    ads_declines_reasons_for_doc_for_xml.SetItem(ll_i, "status", "d");
                    
                    ads_declines_reasons_for_doc_for_xml.SetItem(ll_i, "update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());
                }
            }
            return ((int)(ll_rows + 1));
        }
        public async Task<FuncResults<IRepository, bool>> uf_create_new_decline(long al_branch_number, long al_supplier_number, long al_distributor_number, long al_order_number, string as_parent_doc_type, long al_parent_doc_number, long al_material_number, IRepository ads_declines_reasons_for_doc_for_xml, string as_decline_code, long al_serial_number)
        {
            //**********************************************************************************************
            //*Object:				nvuo_b2b_decline
            //*Function Name:	uf_create_new_decline
            //*Purpose: 			Create new decline
            //*Arguments: 		Long		al_branch_number
            //*						Long		al_supplier_number
            //*						Long		al_distributor_number
            //*						Long		al_order_number
            //*						String	as_parent_doc_type
            //*						Long		al_parent_doc_number
            //*						Long		al_material_number
            //*						Datastore ads_declines_reasons_for_doc_for_xml
            //*						String	as_decline_code
            //*						Long		al_serial_number
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version		Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*17-11-2010		AlexKh			1.2.46.4		CR#1143		Initial version
            //************************************************************************************************

            long ll_row = 0;
            long ll_parent_serial_number = 0;
            long ll_department_number = 0;
            string ls_barcode = null;
            
            ll_row = ads_declines_reasons_for_doc_for_xml.Insert(0);
            if (!(await uf_populate_decline_details( ads_declines_reasons_for_doc_for_xml, as_decline_code, ll_row)).Retrieve(out ads_declines_reasons_for_doc_for_xml))
            {
                
                ads_declines_reasons_for_doc_for_xml.Delete((int)ll_row);
                return FuncResults.Return(ads_declines_reasons_for_doc_for_xml, false);
            }
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "branch_number", al_branch_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "supplier_number", al_supplier_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "distributor_number", al_distributor_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "order_number", al_order_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_doc_type", Convert.ToString(as_parent_doc_type));
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_doc_number", al_parent_doc_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "material_number", al_material_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "line_number", ll_row.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "update_datetime", idt_parent_doc_suply_dt.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_supply_date", idt_parent_doc_suply_dt.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_doc_date", idt_parent_doc_dt.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_serial_number", ll_parent_serial_number.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "status", "a");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "inserted_option", "1");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "move_decline_code", Convert.ToString(as_decline_code));
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "original_quantity", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "move_actual_quantity", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "reject_quantity", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "move_original_price", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "actual_price", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "b2b_msg_decline_ref", "0");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "b2b_status", "1");
            
            ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "parent_serial_number", al_serial_number.ToString());
            if (al_material_number > 0)
            {
                ls_barcode =await f_get_main_barcodeClass.f_get_main_barcode(al_material_number);
                
                ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "barcode", Convert.ToString(ls_barcode));
                uf_get_item_department_number(al_material_number, ref ll_department_number);
                if (ll_department_number > 0)
                {
                    
                    ads_declines_reasons_for_doc_for_xml.SetItem(ll_row, "dprt_no", ll_department_number.ToString());
                }
            }
            return FuncResults.Return(ads_declines_reasons_for_doc_for_xml, true);
        }
        public bool uf_set_store_details(long al_row) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_decline
            //*Function:			uf_set_store_details
            //*Purpose:			Send a digital message of declines for a Invoice/Packlist.(Replace f_b2b_send_decline)
            //*Description:		Send a Reject/Confimation/Decline message, according to the declines related to the document.
            //*			
            //*Arguments:		Pass By		Argument Type		Argument Name
            //*						-------------------------------------------------------------------------
            //*						Value			Long				al_branch_number - Branch number.
            //*						Value			Long				al_supplier_number - Supplier number.
            //*						Value			Long				al_distributor_number - Distributor number.
            //*						Value			String			as_parent_doc_type - I for Invoice / P for Packlist.
            //*						Value			Long				al_parent_doc_number - Invoice/Packlist number. 
            //*						Value			DateTime			adt_parent_doc_dt - Invoice/Packlist move date and time.
            //*						Value			DateTime			adt_parent_doc_suply_dt - Invoice/Packlist supply date and time.
            //*						Value			Long				al_order_numer
            //*										
            //*Return:								Boolean: TRUE - Success
            //*													FALSE - Faliure
            //*
            //*Date				Programer	Version		Task#			Description
            //*-----------------------------------------------------------------------------------------
            //*22-12-2010		AlexKh		1.2.46.5 	CR#1143		Initial
            //******************************************************************************************

            long ll_branch_number = 0;
            string ls_branch_name = null;
            string ls_edi_number = null;

            ll_branch_number = ids_declines.GetItemValue<long>((int)al_row, "branch_number");
            if (ll_branch_number == 0)
            {
                ll_branch_number = 0;
            }
            if (ll_branch_number == masofonAlias.masofon.Instance.gs_vars.branch_number)
            {
                if (isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.qs_parameters.edi_number))
                {
                    return false;
                }
                //StoreNumber
                
                ids_decline_xml_temp.SetItem(al_row, "storeno", masofonAlias.masofon.Instance.qs_parameters.edi_number);
                // StoreName
                
                ids_decline_xml_temp.SetItem(al_row, "storename", masofonAlias.masofon.Instance.qs_parameters.company_name);
            }
            else
            {
                LoadData5(ll_branch_number, ref ls_branch_name, ref ls_edi_number);
                if (isempty_stringClass.isempty_string(ls_edi_number))
                {
                    return false;
                }
                //StoreNumber
                
                ids_decline_xml_temp.SetItem(al_row, "storeno", ls_edi_number);
                // StoreName
                
                ids_decline_xml_temp.SetItem(al_row, "storename", ls_branch_name);
            }
            return true;
        }
        public nvuo_b2b_decline()
            : base()
        {
            constructor();
        }

        public void constructor() // Create and define IRepository
        {

            // Code was removed based on 'nvuo_b2b_transactions.constructor() : Long' mappings.
            // MyBase.constructor()

            ids_decline_xml = new d_decline_xmlRepository("","","","","");
            ids_decline_xml_temp = new d_decline_xmlRepository("", "", "", "", "");
        }

        public void LoadData(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, string ls_material_barcode, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number }, { "@5", ls_material_barcode } };
                    string sql = "SELECT line_number FROM b2b_invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND invoice_number = @3 AND item_number = @4 AND item_barcode = @5 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND bonus_line = 0 AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number } };
                    string sql = "SELECT line_number FROM b2b_invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND invoice_number = @3 AND item_number = @4 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND bonus_line = 0 AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, string ls_material_barcode, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number }, { "@5", ls_material_barcode } };
                    string sql = "SELECT line_number FROM b2b_packing_list_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND pack_list_number = @3 AND item_number = @4 AND item_barcode = @5 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number } };
                    string sql = "SELECT line_number FROM b2b_packing_list_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND pack_list_number = @3 AND item_number = @4 AND status = 'a' AND (item_bonus_code IS NULL OR item_bonus_code = )";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(Int64 al_item_number, ref char ac_item_status)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_item_number } };
                    string sql = "SELECT items.item_status FROM items,  item_codes WHERE item_codes.item_number =* items.number AND item_codes.code_type = 2 AND items.number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ac_item_status = UnitOfWork.GetValue(resultRS, 0, Convert.ToChar);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(long ll_branch_number, ref string ls_branch_name, ref string ls_edi_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number } };
                    string sql = "SELECT global_parameters.branch_name,  global_parameters.edi_number FROM global_parameters WHERE (global_parameters.serial_number = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_branch_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        ls_edi_number = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
