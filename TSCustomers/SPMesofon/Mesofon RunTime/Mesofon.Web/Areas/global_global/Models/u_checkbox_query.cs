using System.Web.VisualTree.MVC;
using Mesofon.Common;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_checkbox_query : u_checkbox
	{
		public u_checkbox_query()
		{
			VisualElementHelper.CreateFromView<u_checkbox>("u_checkbox", "u_checkbox", "global_global");
		    constructor();

		}

	    public void constructor()
	    {
	         
	        //base.constructor();
	        this.Visible = (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Administration", "show_sql_syntax", "no", "") == "yes");
	        if (this.Visible)
	        {
                this.BringToFront(); 
	        }
	    }

    }
}
