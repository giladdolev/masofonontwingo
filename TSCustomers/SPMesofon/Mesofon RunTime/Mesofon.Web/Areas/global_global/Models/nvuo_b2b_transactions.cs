using System.IO;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;

namespace global
{
    // Creation Time:   08/11/2015 10:20:55 (Utc)
    // Comments:        
    // 

    public class nvuo_b2b_transactions
    {
        //DataWindow			idw_data
        public string is_file_spec;
        public string is_file_name;
        public string is_supplier_edi;
        public string is_file_path;
        public long il_file_number;
        public long il_supplier_number;
        public IRepository ids_order_xml;
        public IRepository ids_order_xml_temp;
        public IRepository ids_debit_xml;
        public IRepository ids_debit_xml_temp;
        public IRepository ids_decline_xml;
        public IRepository ids_decline_xml_temp;
        public IRepository ids_return_inv_xml;
        public IRepository ids_return_inv_xml_temp;
        public Mesofon.Repository.d_web_trade_for_b2b_xmlRepository ids_web_trade_xml;
        // SharonS - 1.2.30.16 - 2008-06-04 - CR#1094
        public string is_parent_supplier_edi;
        public async Task<bool> uf_check_directory() //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_check_directory
            //*Purpose:					Check if the file target directory exist.
            //*  
            //*Arguments:				None.
            //*Return:						BOOLEAN - TRUE - Target directory exist or created succesfully.
            //*												 FALSE - Creation of target directory failed .
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            int li_ret = 0;
            DirectoryInfo di = new DirectoryInfo(masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder);
            if (!di.Exists)
            {
                try
                {
                    di.Create();
                }
                catch (Exception e)
                {
                    li_ret = -1;
                }
                
                if (li_ret == -1)
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", "Error Accessing B2B Outgoing Messages Folder");
                    return false;
                }
            }
            return true;
        }
        public bool uf_create_file_name(long al_supplier_number, string as_msg_type)
        {
            is_file_name = masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "_";
            // AlexKh - 1.2.44.32 - 2009-10-04 - SPUCM00001607 - replace station number with supplier number 
            is_file_name += al_supplier_number.ToString() + "_";
            is_file_name += masofonAlias.masofon.Instance.gs_station.station_number.ToString() + "_";
            is_file_name += as_msg_type + "_";
            //Penny - Start-Add Seconds
            //is_file_name += String(Today(), "yyyymmdd_hhmm")
            is_file_name += DateTime.Today.ToString("yyyyMMdd_hhmmss");
            //Penny - Start
            is_file_name += ".xml";
            //Penny - End
            is_file_spec = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder;
            if (is_file_spec.Substring(is_file_spec.Length - 1) != "\\")
            {
                is_file_spec += "\\";
            }
            is_file_spec += is_file_name;
            is_file_path = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder;
            return true;
        }
        public int uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            return 1;
        }
        public bool uf_is_branch_supplier_b2b(int ai)
        {
            return true;
        }
        public bool uf_send_transaction(ref int ai_file_status, string as_supplier_edi, ref IRepository[] ads_data) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_send_transaction
            //*Purpose:					Create the B2B message XML file.
            //*								This function will be coded in the decendance objects.
            //*								In each object a defferent message will be created.
            //*  
            //*Arguments:				Integer - Supplier number
            //*Return:						BOOLEAN - TRUE - Creation of B2B message XML file succeded.
            //												 FALSE - Creation of B2B message XML file failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Create the B2B message XML file.
            //************************************************************************************************

            return true;
        }
        public bool uf_get_yarpa_code(long al_material_number, ref string as_yarpa_code) //**********************************************************************************************
        {
            LoadData(al_material_number, ref as_yarpa_code);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {
                return false;
            }
            return true;
        }
        public bool uf_get_item_department_number(long al_item_number, ref long al_department_number) //**********************************************************************************************
        {
            LoadData1(al_item_number, ref al_department_number);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {
                return false;
            }
            return true;
        }
        public bool uf_init(string as_msg_type, long al_supplier_number, long? al_msg_number, long al_branch_number) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_init
            //*Purpose:					Initiate the object:
            //*										1. Check if the file target directory exist.
            //*										2. Initiate instance variables.
            //*										3. Create the file name.
            //*  
            //*Arguments:				String	- Messag Type (name)
            //*Return:						BOOLEAN - TRUE - Creation of file target directory succeeded.
            //*												 FALSE - Creation of file target directory failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            // AlexKh - 1.2.46.7 - 2011-02-21 - CR#1143 - SPUCM00002474 -use branch number from parameter
            is_file_name = al_branch_number.ToString() + "_";
            //is_file_name = String(gs_vars.branch_number) + "_"
            // AlexKh - 1.2.44.32 - 2009-10-04 - SPUCM00001607 - replace station number with supplier number
            is_file_name += al_supplier_number.ToString() + "_";
            is_file_name += masofonAlias.masofon.Instance.gs_station.station_number.ToString() + "_";
            // AlexKh - 1.2.44.32 - 2009-10-04 - SPUCM00001607 - replace message type and message number
            // AlexKh - 1.2.43.1 - 2009-03-31 - SPUCM00001132 - (CR#1108) add new parameter message number to the file name
            is_file_name += as_msg_type + "_";
            is_file_name += al_msg_number + "_";
            //PennySG - 2007-06-27 - Add seconds in the file bame
            //is_file_name += String(Today(), "yyyymmdd_hhmm")
            // SharonS - 1.2.42.8 - 2009-03-18 - SPUCM00001134 - Get DateTime? from Server machine
            //is_file_name += String(Today(), "yyyymmdd_hhmmss")
            is_file_name += masofonAlias.masofon.Instance.set_machine_time().Value.ToString("yyyyMMdd_hhmmss");
            // SharonS - End
            // PennySG - End
            // PennySG - Start
            is_file_name += ".xml";
            // PennySG - End
            is_file_spec = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder;
            if (is_file_spec.Substring(is_file_spec.Length - 1) != "\\")
            {
                is_file_spec += "\\";
            }
            is_file_spec += is_file_name;
            is_file_path = masofonAlias.masofon.Instance.gs_vars.b2b_outgoing_msg_folder;
            return true;
        }
        public async Task<FuncResults<int,bool>> uf_update_outgoing(int ai_msg_type, int ai_file_status, string as_doc_type, long? al_doc_number, long? al_order_no, long? al_supplier_number, long al_branch_number)
        {
            //**********************************************************************************************
            //*Object:						nvou_b2b_transaction
            //*Function Name:			uf_update_outgoing
            //*Purpose:					Insert a new row into b2b_outgoing_messages table.
            //*
            //*Arguments:				String	- Messag Type (name)
            //*Return:						BOOLEAN - TRUE - Insert succeeded.
            //*												 FALSE - Insert failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*29-05-2007 	SharonS				0.1.0 		B2B 			Insert into b2b_outgoing_messages.
            //*30-07-2007		PninaSG				0.1.0		B2B			Change the sql to insert to colum:file_path insteade of file_spec
            //*01-08-2007		PninaSG				0.1.0		B2B			Change the table name in the sql from b2b_outgoing_message  to b2b_outgoing_messages
            //*																		Change the type and name of the argument from:String as_msg_type to INTEGER ai_msg_type
            //*																		Add to the sqlselect the fields: move_datetime and station_number
            //*02-08-2007		PninaSG				0.1.0		B2B			Add More argument:al_supplier_no
            //****************************************************************************************

            // This function inserts row into b2b_outgoing_messages
            long ll_branch_number = 0;
            long ll_order_number = 0;
            long ll_supplier_number = 0;
            long ll_file_number = 0;
            DateTime? ldt_today = default(DateTime);
            // SharonS - 1.2.42.8 - 2009-03-18 - SPUCM00001134 - Get DateTime? from Server machine
            //ldt_today = DateTime(Today(), Now())
            ldt_today = masofonAlias.masofon.Instance.set_machine_time();
            UpdateData(al_branch_number, ldt_today, al_order_no, as_doc_type, al_doc_number, al_supplier_number, ai_msg_type, ai_file_status);
            // End
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {

                //await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText,"Error",System.Web.VisualTree.MessageBoxButtons.OK,MessageBoxIcon.Error);
                return FuncResults.Return(ai_file_status,false);
            }
            return FuncResults.Return(ai_file_status, true);
        }
        public async Task<bool> uf_update_outgoing_message_status(long al_branch_number, long al_supplier_number, long al_order_number, int ai_msg_type, int ai_file_status, string as_doc_type) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_transactions
            //*Function Name:	uf_update_outgoing_message_status
            //*Purpose: 			This function updates existing row status in b2b_outgoing_messages
            //*Arguments: 		Long 		- al_branch_number - branch number
            //*						Long 		- al_supplier_number - supplier number
            //*						Long 		- al_order_number - order number
            //*						integer 	- ai_msg_type - message type
            //*						integer 	- ai_file_status - file status
            //*						string 	- as_doc_type - document type
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version		Task#	 			Description
            //*------------------------------------------------------------------------------------------------
            //*23-01-2012		AlexKh			1.2.48.14	SPUCM00003413	Initial version
            //************************************************************************************************

            DateTime? ldt_today = default(DateTime);
            UpdateData1(ai_file_status, al_branch_number, al_supplier_number, al_order_number, ai_msg_type, as_doc_type);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {

                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                return false;
            }
            return true;
        }
        public nvuo_b2b_transactions()
        {

        }

        public void LoadData(Int64 al_material_number, ref string as_yarpa_code)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT IsNull(item_codes.code, '') FROM item_codes,  trees,  items_entities_connections AS iec,  catalog_entities AS cat,  materials WHERE code_type = 2 AND item_codes.item_number = trees.item_number AND trees.material_number = materials.number AND iec.item_number = trees.item_number AND iec.entity_number = cat.serial_number AND cat.entity_type_number = 10 AND materials.number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        as_yarpa_code = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_item_number, ref Int64 al_department_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_item_number } };
                    string sql = "SELECT catalog_entities.entity_number FROM items_entities_connections,  catalog_entities WHERE items_entities_connections.item_number = @0 AND items_entities_connections.entity_number = catalog_entities.serial_number AND catalog_entities.entity_type_number = 10";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        al_department_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(Int64 al_branch_number, DateTime? ldt_today, Int64? al_order_no, string as_doc_type, Int64? al_doc_number, long? al_supplier_number, int ai_msg_type, int ai_file_status)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", masofonAlias.masofon.Instance.gs_station.station_number }, { "@2", ldt_today }, { "@3", al_order_no }, { "@4", as_doc_type }, { "@5", al_doc_number }, { "@6", al_supplier_number }, { "@7", ai_msg_type }, { "@8", is_file_name }, { "@9", is_file_path }, { "@10", ai_file_status } };
                    string sql = "INSERT INTO b2b_outgoing_messages (branch_number,  station_number,  move_datetime,  order_number,  doc_type,  doc_number,  supplier_number,  msg_type,  file_name,  file_path,  file_status) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  @6,  @7,  @8,  @9,  @10)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData1(int ai_file_status, Int64 al_branch_number, Int64 al_supplier_number, Int64 al_order_number, int ai_msg_type, string as_doc_type)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ai_file_status }, { "@1", al_branch_number }, { "@2", al_supplier_number }, { "@3", al_order_number }, { "@4", ai_msg_type }, { "@5", as_doc_type } };
                    string sql = "UPDATE b2b_outgoing_messages SET file_status = @0 WHERE branch_number = @1 AND supplier_number = @2 AND doc_number = @3 AND msg_type = @4 AND doc_type = @5";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        
    }
}
