using System;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.MVC;
using Mesofon.Common;
using masofonAlias = masofon;
using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:21:00 (Utc)
	// Comments:        
	// 
	public class u_dw_update : u_dw_many_rows
	{
		//.. if true -> show error with sql syntax
		public bool ib_sql_error_flag;
		public string is_last_sql_error_text;
		public delegate bool ue_check_validation_EventHandler();
		public event ue_check_validation_EventHandler ue_check_validationEvent;
		public delegate void ue_new_row_EventHandler();
		public event ue_new_row_EventHandler ue_new_rowEvent;
		public bool ue_check_validation() // reloaded by child object
		{
			return true;
		}
		public void ue_new_row() // reloaded by child object
		{
		}
		public string uf_get_last_sql_error_text()
		{
			return is_last_sql_error_textProperty;
		}
		public void uf_set_sql_error_flag(bool ab_sql_error_flag) //--------------------------------------------------------------------
		{
			//Function:			public u_dw_update.uf_set_sql_preview_flag()
			//
			// Returns:         Integer
			//
			// Parameters:      value Boolean ab_sql_preview_flag
			// 
			// Copyright  - Stas
			//
			// Date Created: 25/07/2005
			//
			// Description:	
			// 					use this function to set value into ib_sql_preview_flag
			//					only if it`s TRUE -> on error we can see a error message with sql syntax
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			ib_sql_error_flag = ab_sql_error_flag;
		}
		public u_dw_update()
		{
			VisualElementHelper.CreateFromView<u_dw_many_rows>("u_dw_many_rows", "u_dw_many_rows", "global_global");
		}

        public async Task<int> dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row) //--------------------------------------------------------------------
		{
            // Copyright  - Stas
            //
            // Date Created: 25/07/2005
            //
            // Description:	
            // 					
            //					only if ib_sql_error_flag is  TRUE -> 
            //					on error we can see a error message with sql syntax
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------


            // TODO: Method invocation was missing required argument 'sqldbcode' (CODE=30008).
            // TODO: Method invocation was missing required argument 'sqlerrtext' (CODE=30008).
            // TODO: Method invocation was missing required argument 'sqlsyntax' (CODE=30008).
            // TODO: Method invocation was missing required argument 'buffer' (CODE=30008).
		    // TODO: Method invocation was missing required argument 'row' (CODE=30008).
		    GridElement u_dw = this.GetVisualElementById<GridElement>("u_dw");

		    u_dw.dberror(0, default(string), default(string), default(object), 0);
			string ls_Temp = null;
			if (ib_sql_error_flag && sqldbcode != 0)
			{
                await WindowHelper.Open<w_sql_syntax>("global_global", "sqlSyntax", sqlerrtext);
				
				if (WindowHelper.GetParam<string>(this) == "Cancled")
				{
					ls_Temp = "";
				}
				else
				{
					
					ls_Temp = WindowHelper.GetParam<string>(this);
				}
			}
			else if (sqldbcode == -3)
			{
                //.. משתמש אחר מנסה לעדכן את אותה רשומה נא לצאת ולהכנס שוב		
			    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", "", "#5350");
			}
			if (sqldbcode != 0)
			{
				is_last_sql_error_textProperty = sqlerrtext;
			}
			return 1;
		}
		public void ue_enter(object sender, GridElementCellEventArgs e)
		{
			//base.ue_enter();
			object keyflags = null;
			if (Convert.ToInt32(keyflags) == 0)
			{
				// TODO: Field 'Key of type 'Sybase.PowerBuilder.TrigEvent' is unmapped'. (CODE=1004)
				if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
				{
					//.. sets enter click equal to tab click
					uf_dw_enter_key_move();
				}
			}
		}
		public string is_last_sql_error_textProperty
		{
			get { return this.is_last_sql_error_text; }
			set { this.is_last_sql_error_text = value; }
		}

	   
	}
}
