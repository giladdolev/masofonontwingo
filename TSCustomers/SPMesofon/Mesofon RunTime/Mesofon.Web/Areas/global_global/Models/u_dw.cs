using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_dw : GridElement
	{
		//.. pointer to parent object
		public object io_parent_type;
		public WindowElement iw_parent;
		public delegate void ue_enter_EventHandler();
		public event ue_enter_EventHandler ue_enterEvent;
		public u_dw()
		{
		    constructor();

		}
	    public void constructor() // Save parent object
	    {

	        this.io_parent_typeProperty = this.Parent.GetType();
	        if (this.Parent.GetType() == typeof(WindowElement))
	        {

	            this.iw_parentProperty = this.Parent as WindowElement;
	        }
	        //.. connect to LIVE db

	    }

        public object io_parent_typeProperty
		{
			get { return this.io_parent_type; }
			set { this.io_parent_type = value; }
		}
		public WindowElement iw_parentProperty
		{
			get { return this.iw_parent; }
			set { this.iw_parent = value; }
		}

	    public void uf_dw_enter_key_move() //.. sets enter click equal to tab click
	    {



	        //SystemFunctionsExtensions.Send(SystemFunctionsExtensions.Handle(this.ViewModel), 256, 9, SystemFunctionsExtensions.Long(0, 0));
	    }
    }
}
