using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Mesofon.Common;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_dw_many_rows : u_dw
	{
		//.. if true than only one row in dw can be selected
		public bool ib_select_single_row;
		//.. if TRUE -> do not select any row
		public bool ib_no_rows_selection;
		public bool ib_sql_preview_flag;

	    public long il_last_sql_db_code;
		//.. if with include rows checkbox checked
		public bool ib_include_rows;
		public delegate void ue_mousedown_EventHandler();
		public event ue_mousedown_EventHandler ue_mousedownEvent;
		public u_dw_many_rows()
		{
			VisualElementHelper.CreateFromView<u_dw>("u_dw", "u_dw", "global_global");
		    constructor();

		}

        public void constructor()
        {
            uf_set_hscroll_position();
            //.. set ib_no_rows_selection TRUE for no row selection on click event
        }

	    public void uf_set_hscroll_position()
	    {
            if (this.DataSource != null)
	        {
	            if (masofon.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofon.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
	            {
	                //.. position of Horizonal-Scroll. on left direction move it to the left
	                if (masofon.masofon.Instance.nvo_translator.fnv_is_direction_left())
	                {
	                    //this.set_HorizontalScrollPosition("0");
	                }
	                else
	                {
	                    //this.set_HorizontalScrollPosition(this.get_HorizontalScrollMaximum());
	                }
	            }
	            else
	            {
	                //this.set_HorizontalScrollPosition(this.get_HorizontalScrollMaximum());
	            }
	        }
	    }

        public bool ib_select_single_rowProperty
		{
			get { return this.ib_select_single_row; }
			set { this.ib_select_single_row = value; }
		}
		public bool ib_no_rows_selectionProperty
		{
			get { return this.ib_no_rows_selection; }
			set { this.ib_no_rows_selection = value; }
		}
		public bool ib_sql_preview_flagProperty
		{
			get { return this.ib_sql_preview_flag; }
			set { this.ib_sql_preview_flag = value; }
		}
		public bool ib_include_rowsProperty
		{
			get { return this.ib_include_rows; }
			set { this.ib_include_rows = value; }
		}

        public void uf_set_sql_preview_flag(bool ab_sql_preview_flag)
        {
            ib_sql_preview_flagProperty = ab_sql_preview_flag;
        }


    }
}
