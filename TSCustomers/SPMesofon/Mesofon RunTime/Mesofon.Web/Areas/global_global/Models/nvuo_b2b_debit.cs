﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Extensions;
using System.IO;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using System.Web.VisualTree.Extensions;
using Mesofon.Repository;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using SystemIOAlias = System.IO;
using Mesofon.Common;

namespace global
{
    // Creation Time:   09/07/2016 08:34:08 (Utc)
    // Comments:        
    // 
    public class nvuo_b2b_debit : nvuo_b2b_transactions
    {
        public IRepository ids_header;
        public IRepository ids_details;
        public string is_message_type;
        public async Task<int> uf_write_log(string as_message) //********************************************************************************************************************************
        {
            //*Object:								nvuo_b2b_debit
            //*Function/Event  Name:			uf_write_log
            //*Purpose:							Write into log file
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*									-------------------------------------------------------------------------------------------------------------------
            //*									Value			String						as_message- the message to write into the file.
            //*
            //*Return:							Integer: 1- Success
            //*												  -1 -Failed.
            //*
            //*Date 				Programer				Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
            //*19-12-2007 		SharonS					1.0.2			B2B			Initiation
            //**********************************************************************************************************************************

            int li_file = 0;
            StreamWriter li_file_open;
            int ll_debit_invoice_no = 0;
            string ls_log_file_name = null;
            string ls_message = null;
            string ls_doc_type = null;
            string ls_msg_header = null;
            string ls_log_mode = null;
            bool ib_log_mode = false;
            // SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Log on/off
            if (!masofonAlias.masofon.Instance.gb_b2b_log)
            {
                return 1;
            }
            // End
            // SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Add data to log file
            ls_msg_header = "------------------------------------------------------------------------------------------------------------------------" + "\r" + "\n";
            ls_msg_header += "Employee Number: " + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + " Date: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n";
            if (ids_header != null)
            {
                if (is_message_type == "389")
                {

                    ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "invoice_number");
                    ls_msg_header += "Return Invoice number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
                }
                else if (is_message_type == "384")
                {

                    ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "debit_credit_number");
                    ls_msg_header += "Confirm number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
                }
                else
                {

                    ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "debit_credit_number");
                    ls_msg_header += "Debit Invoice number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
                }
            }
            else
            {
                ls_msg_header += "ids_header is not valid." + "\r" + "\n";
            }
            as_message = ls_msg_header + as_message;
            // End
            // AlexKh - 1.2.48.3 - 2011-05-24 - SPUCM00002871 - change log path
            //ls_log_file_name = "c:\" + String(gs_vars.branch_number) + "_DEBIT_MSG.log" 
            ls_log_file_name = "c:\\NET-POS\\log\\" + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "_DEBIT_MSG.log";
            int ret = 1;
            try
            {
                using (var file = new StreamWriter(ls_log_file_name))
                {
                    file.Write(as_message);
                }
            }
            catch (Exception e)
            {
                ret = -1;
            }

            if (ret == -1)
            {
                await MessageBox.Show("Can not open the log file.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }

            return ret;
        }
        public bool uf_save_as_xml() //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_save_as_xml
            //*Purpose:					Save the DW as XML file.
            //*  
            //*Arguments:				None.
            //*Return:						Boolean: 1 - succeeded.
            //*										 	-1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*19-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************


            return ids_debit_xml.SaveToXml(Convert.ToString(is_file_spec)) == 1;
        }
        public int uf_insert_header(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_insert_header
            //*Purpose:					Insert haeder section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*19-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            string ls_reference_no = null;
            string ls_employee_name = null;
            string ls_supplier_invoice_no = null;
            int li_insert_option = 0;
            long ll_reference_date = 0;
            long ll_employee_number = 0;
            long ll_debit_invoice_no = 0;
            decimal ld_date_time = default(decimal);
            decimal ld_vat_percent = default(decimal);
            decimal ld_invoice_date = default(decimal);
            decimal ld_lines_sum = default(decimal);
            decimal ld_inv_sum = default(decimal);
            decimal ld_discount_percent = default(decimal);
            decimal ld_discount_amount = default(decimal);
            DateTime? ldt_date_time = default(DateTime);
            DateTime? ldt_invoice_date = default(DateTime);

            ids_debit_xml_temp.SetItem(al_row, "invoicetype", is_message_type);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ll_debit_invoice_no = ids_header.GetItem<int>(1, "debit_credit_number")
            if (is_message_type == "389")
            {

                ll_debit_invoice_no = ids_header.GetItemValue<long>(0, "invoice_number");
            }
            else
            {

                ll_debit_invoice_no = ids_header.GetItemValue<long>(0, "debit_credit_number");
            }
            // End
            if (ll_debit_invoice_no == 0)
            {
                return -1;
            }
            // SharonS - 2008-03-30 - debitinvoiceno field is not numeric 
            //ids_debit_xml_temp.SetItem(al_row, "debitinvoiceno", ll_debit_invoice_no)  

            ids_debit_xml_temp.SetItem(al_row, "debitinvoiceno", ll_debit_invoice_no.ToString());
            // End

            ids_debit_xml_temp.SetItem(al_row, "invoicefunc", "9");
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ldt_date_time = DateTime(Today(), Now())
            if (is_message_type == "389")
            {

                ldt_date_time = ids_header.GetItemValue<DateTime>(0, "supply_date");
            }
            else
            {

                ldt_date_time = ids_header.GetItemValue<DateTime>(0, "date_move");
            }
            // End
            ld_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
            if (ld_date_time == 0)
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "datetime", ld_date_time);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ls_supplier_invoice_no = String(ids_header.GetItem<int>(1, "parent_doc_number"))
            if (is_message_type == "389")
            {
                ls_supplier_invoice_no = "0";
            }
            else
            {

                ls_supplier_invoice_no = ids_header.GetItemValue<int>(0, "parent_doc_number").ToString();
            }
            // End

            ids_debit_xml_temp.SetItem(al_row, "supplierinvoiceno", ls_supplier_invoice_no);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ldt_invoice_date = ids_header.GetItem<DateTime>(1, "parent_doc_date")
            //ld_invoice_date = Dec(String(ldt_invoice_date, "yyyyMMdd"))
            if (is_message_type == "389")
            {
                ld_invoice_date = 0;
            }
            else
            {

                ldt_invoice_date = ids_header.GetItemValue<DateTime>(0, "parent_doc_date");
                ld_invoice_date = Convert.ToDecimal(ldt_invoice_date.Value.ToString("yyyyMMdd"));
            }
            // End

            ids_debit_xml_temp.SetItem(al_row, "supplierinvoicedate", ld_invoice_date);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ls_reference_no = String(ids_header.GetItem<int>(1, "parent_order_number"))
            //ids_debit_xml_temp.SetItem(al_row, "referenceno", ls_reference_no)  
            if (is_message_type != "389")
            {

                ls_reference_no = ids_header.GetItemValue<int>(0, "parent_order_number").ToString();

                ids_debit_xml_temp.SetItem(al_row, "referenceno", ls_reference_no);
            }
            // End
            //ll_reference_date = Long(ids_header.GetItem<string>(1, "parent_order_date"))
            //ids_debit_xml_temp.SetItem(al_row, "referencedate", ll_reference_date)  

            ids_debit_xml_temp.SetItem(al_row, "pricelistno", "");

            ids_debit_xml_temp.SetItem(al_row, "pricelistdate", 0);
            if (isempty_stringClass.isempty_string(is_supplier_edi))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "supplierno", is_supplier_edi);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ids_debit_xml_temp.SetItem(al_row, "suppliername", ids_header.GetItem<string>(1, "supplier_name"))
            if (is_message_type != "389")
            {


                ids_debit_xml_temp.SetItem(al_row, "suppliername", ids_header.GetItemValue<string>(0, "supplier_name"));
            }
            // End
            if (isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.qs_parameters.edi_number))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "storeno", masofonAlias.masofon.Instance.qs_parameters.edi_number);

            ids_debit_xml_temp.SetItem(al_row, "storename", masofonAlias.masofon.Instance.qs_parameters.company_name);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ids_debit_xml_temp.SetItem(al_row, "discountpercent", 0.00)
            if (is_message_type == "389")
            {

                ld_discount_percent = ids_header.GetItemValue<decimal>(0, "discount_percentage");

                ids_debit_xml_temp.SetItem(al_row, "discountpercent", ld_discount_percent);
            }
            else
            {

                ids_debit_xml_temp.SetItem(al_row, "discountpercent", 0.0);
            }
            // End

            ids_debit_xml_temp.SetItem(al_row, "discountamount", 0.0);
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_vat_percent = ids_header.GetItem<decimal>(1, "vat_percent") 
            if (is_message_type == "389")
            {

                ld_vat_percent = ids_details.GetItemValue<decimal>(0, "vat_percentage");
            }
            else
            {

                ld_vat_percent = ids_header.GetItemValue<decimal>(0, "vat_percent");
            }
            // End
            // Debit/Return total without vat, with out discount
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_lines_sum = ids_header.GetItem<decimal>(1, "debit_credit_total")
            if (is_message_type == "389")
            {

                ld_lines_sum = ids_details.GetItemValue<decimal>(0, "invoice_total_cf");
            }
            else
            {

                ld_lines_sum = ids_header.GetItemValue<decimal>(0, "debit_credit_total");
            }
            // End

            ids_debit_xml_temp.SetItem(al_row, "linessum", ld_lines_sum);
            // SharonS - 2008-06-24 - 1.2.30.18 - CR#1068 - Fix
            // Debit total without vat with discount
            // Since we don't have any discount in Debit invoices, the value will be the same as the value of Debit total without vat
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ids_debit_xml_temp.SetItem(al_row, "docsum", ld_lines_sum)
            if (is_message_type == "389")
            {

                ld_lines_sum = ids_details.GetItemValue<decimal>(0, "total_with_vat");

                ids_debit_xml_temp.SetItem(al_row, "docsum", ld_lines_sum);
            }
            else
            {

                ids_debit_xml_temp.SetItem(al_row, "docsum", ld_lines_sum);
            }
            // End
            // Debit total with vat with discount
            //ld_doc_sum = ld_lines_sum + (ld_lines_sum * ld_vat_percent)
            //ids_debit_xml_temp.SetItem(al_row, "docsum", ld_doc_sum)
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_inv_sum = ld_lines_sum + (ld_lines_sum * ld_vat_percent)
            if (is_message_type == "389")
            {

                ld_inv_sum = ids_header.GetItemValue<decimal>(0, "invoice_total");
            }
            else
            {
                ld_inv_sum = ld_lines_sum + (ld_lines_sum * ld_vat_percent);
            }
            // End

            ids_debit_xml_temp.SetItem(al_row, "invsum", ld_inv_sum);
            // End
            return 1;
        }
        public int uf_insert_details(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_insert_details
            //*Purpose:					Insert the Deatails section of the debit XML
            //*  
            //*Arguments:				Long	- Row No (al_row)
            //*Return:						Integer - 1 succeed.
            //*											-1 not  succeed.
            //*Date 			Programer		Version	Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*19-12-2007		SharonS			0.1.0 		B2B 		Initiate Version
            //************************************************************************************************

            string ls_yarpa_code = null;
            string ls_barcode = null;
            string ls_name = null;
            string ls_temp = null;
            string ls_reason_code = null;
            string ls_material_barcode = null;
            char lc_doc_type = '\0';
            decimal ld_sum = default(decimal);
            decimal ld_price_after_discount = default(decimal);
            decimal ld_vat_percentage = default(decimal);
            decimal ld_quantity_diff = default(decimal);
            decimal ld_tax_price = default(decimal);
            decimal ld_temp = default(decimal);
            decimal ld_price_diff = default(decimal);
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_doc_number = 0;
            long ll_item_number = 0;
            long ll_line_number = 0;
            long ll_dprt_no = 0;
            long ll_decline_number = 0;
            long? ll_find = 0;
            GridElement ldwc_dec_num=null;
            // LineNo
            // SharonS - 1.2.42.3 - 2009-03-08 - SPUCM00000983 - In Retrun message set line number set sequenc
            if (is_message_type == "383")
            {
                // SharonS - 1.2.42.1 - 2009-02-24 - SPUCM00000983 - Set 'lineno' element to be the line number from the invoice/packlist message

                ll_branch_number = ids_details.GetItemValue<long>((int)al_row, "branch_number");

                ll_supplier_number = ids_details.GetItemValue<long>((int)al_row, "supplier_number");

                ll_order_number = ids_header.GetItemValue<long>(0, "parent_order_number");

                ll_doc_number = ids_details.GetItemValue<long>((int)al_row, "parent_doc_number");

                ll_item_number = ids_details.GetItemValue<long>((int)al_row, "material_number");

                lc_doc_type = Convert.ToChar(ids_header.GetItemValue<string>(0, "parent_doc_type"));
                // SharonS - 1.2.42.8 - 2009-03-18 - SPUCM00000983 - Fix for more then 1 item

                ls_material_barcode = ids_details.GetItemValue<string>(al_row, "material_barcode");
                if (lc_doc_type.ToString() == "I")
                {
                    // SharonS - 1.2.42.12 - 2009-03-25 - SPUCM00000983 - Fix for bonus and known items
                    if (ll_item_number == 0)
                    {
                        LoadData(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ls_material_barcode, ref ll_line_number);
                    }
                    else
                    {
                        LoadData1(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ref ll_line_number);
                    }
                }
                else
                {
                    if (ll_item_number == 0)
                    {
                        LoadData2(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ls_material_barcode, ref ll_line_number);
                    }
                    else
                    {
                        LoadData3(ll_branch_number, ll_supplier_number, ll_order_number, ll_doc_number, ll_item_number, ref ll_line_number);
                    }
                    // SharonS - End
                }


                if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1 || masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
                {
                    ll_line_number = 0;
                }
            }
            else
            {
                ll_line_number = al_row;
            }
            // End
            // End

            ids_debit_xml.SetItem(al_row, "lineno", ll_line_number);
            //ids_debit_xml.SetItem(al_row, "lineno", al_row)
            // End
            // ItemBarcode
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ls_barcode = ids_details.GetItem<string>(al_row, "material_barcode")
            if (is_message_type == "389")
            {

                ls_barcode = ids_details.GetItemValue<string>(al_row, "barcode");
            }
            else
            {

                ls_barcode = ids_details.GetItemValue<string>(al_row, "material_barcode");
            }
            // End
            // SharonS - 1.2.42.11 - 2009-03-24 - SPUCM00001141 - Get YARPA code if needed.

            ll_dprt_no = ids_details.GetItemValue<long>((int)al_row, "dprt_no");
            if (ll_dprt_no == 8 || ll_dprt_no == 15)
            {

                ll_item_number = ids_details.GetItemValue<long>((int)al_row, "material_number");
                if (this.uf_get_yarpa_code(ll_item_number, ref ls_yarpa_code))
                {
                    if (!isempty_stringClass.isempty_string(ls_yarpa_code))
                    {
                        ls_barcode = ls_yarpa_code;
                    }
                }
            }
            // End

            ids_debit_xml.SetItem(al_row, "itembarcode", ls_barcode);
            // Product Description
            //ids_debit_xml.Setitem(al_row, "proddesc", "")   
            // Pack No
            // UnitsQty
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_quantity_diff = ids_details.GetItem<decimal>(al_row, "quantity_diff")
            if (is_message_type == "389")
            {


                ld_quantity_diff = ids_details.GetItemValue<decimal>((int)al_row, "material_quantity") + ids_details.GetItemValue<decimal>((int)al_row, "bonus_quantity");
            }
            else
            {

                ld_quantity_diff = ids_details.GetItemValue<decimal>((int)al_row, "quantity_diff");
            }
            // End

            ids_debit_xml.SetItem(al_row, "unitsqty", ld_quantity_diff);
            // UnitsQtyMea
            // Will not be sent   
            // Item Bruto Price
            // Super Pharm Branch
            // SharonS - 2008-06-12 - 1.2.30.18 - CR#1068 - Fix Item Bruto Price
            //ld_temp = ids_details.GetItem<decimal>(al_row, "actual_quantity") * ids_details.GetItem<decimal>(al_row, "actual_price")
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_temp = ids_details.GetItem<decimal>(al_row, "actual_price")
            if (is_message_type == "389")
            {

                ld_temp = ids_details.GetItemValue<decimal>((int)al_row, "material_new_price");
            }
            else
            {

                ld_temp = ids_details.GetItemValue<decimal>((int)al_row, "actual_price");
            }
            // End
            // End

            ids_debit_xml.SetItem(al_row, "itempricenetosph", ld_temp);
            // Supplier
            // SharonS - 2008-06-12 - 1.2.30.18 - CR#1068 - Fix Item Bruto Price
            //ld_temp = ids_details.GetItem<decimal>(al_row, "original_quantity") * ids_details.GetItem<decimal>(al_row, "original_price")
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_temp = ids_details.GetItem<decimal>(al_row, "original_price")
            if (is_message_type == "389")
            {
                ld_temp = 0;
            }
            else
            {

                ld_temp = ids_details.GetItemValue<decimal>((int)al_row, "original_price");
            }
            // End
            // End

            ids_debit_xml.SetItem(al_row, "itempricenetosupplier", ld_temp);
            // Price difference (before discount)
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_price_diff = ids_details.GetItem<decimal>(al_row, "price_diff")
            if (is_message_type == "389")
            {
                ld_price_diff = 0;
            }
            else
            {

                ld_price_diff = ids_details.GetItemValue<decimal>((int)al_row, "price_diff");
            }
            // End	

            ids_debit_xml.SetItem(al_row, "pricedifference", ld_price_diff);
            // Reason Code
            // SharonS - 2008-03-30 - Add Reason Codes
            //ids_debit_xml.SetItem(al_row, "reasoncode", 0)    
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            if (is_message_type != "389")
            {
                if (ld_price_diff == 0 && ld_quantity_diff != 0)
                {
                    ls_reason_code = "100";
                }
                else if (ld_price_diff != 0 && ld_quantity_diff == 0)
                {
                    ls_reason_code = "200";
                }
                else if (ld_price_diff != 0 && ld_quantity_diff != 0)
                {
                    ls_reason_code = "300";
                }

                ids_debit_xml.SetItem(al_row, "reasoncode", ls_reason_code);
                // AlexKh - 1.2.44.0 - 2009-05-04 - SPUCM00001024 - for return message
            }
            else if (is_message_type == "389")
            {
                ls_reason_code = "";

                ll_decline_number = ids_details.GetItemValue<long>((int)al_row, "decline_number");

                //ldwc_dec_num = ids_details.GetVisualElementById<GridElement>("decline_number");
                //ldwc_dec_num.Retrieve(3);
                //ll_find = ldwc_dec_num.Find("decline_number ==" + ll_decline_number.ToString(), 0, ldwc_dec_num.RowCount());
                //if (ll_find >= 0)
                //{
                //    ls_reason_code = ldwc_dec_num.GetItemValue<string>((int)ll_find, "decline_code");
                //}

                //ids_debit_xml.SetItem(al_row, "reasoncode", ls_reason_code);
                // AlexKh - 2009-05-04 - End
            }
            // End
            // End
            // Line Summary (after discount)
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ld_temp = ids_details.GetItem<decimal>(al_row, "item_total")
            if (is_message_type == "389")
            {

                ld_temp = ids_details.GetItemValue<decimal>((int)al_row, "material_new_price") * ld_quantity_diff;
            }
            else
            {

                ld_temp = ids_details.GetItemValue<decimal>((int)al_row, "item_total");
            }
            // End	

            ids_debit_xml.SetItem(al_row, "linesum", ld_temp);
            return 1;
        }
        public async Task<int> uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*24-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            int li_index = 0;
            int li_row = 0;
            long ll_find = 0;
            long ll_message_date = 0;
            long ll_supplier_num = 0;
            long ll_distributor_num = 0;
            string ls_find = null;
            string ls_organization_edi = null;
            string ls_supplier_edi = null;
            string ls_message = null;
            string ls_message_time = null;
            ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
            if (isempty_stringClass.isempty_string(ls_organization_edi))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "sender", ls_organization_edi);
            //SharonS - 1.2.37.11 - 20-11-2008 - CR#1094 - SPUCM00000671
            //IF isempty_string(is_supplier_edi) THEN RETURN -1 
            //ids_decline_xml_temp.SetItem(al_row, "receiver", is_supplier_edi)

            ll_supplier_num = ids_header.GetItemValue<long>(0, "supplier_number");
            if (ll_supplier_num == 0 || ll_supplier_num == 0)
            {
                ls_message = "supplier_number IS NOT VALID.";
                await uf_write_log(ls_message);
                return -1;
            }
            is_parent_supplier_edi = f_get_parent_supplier_ediClass.f_get_parent_supplier_edi(ll_supplier_num);

            ll_distributor_num = ids_header.GetItemValue<long>(0, "distributor_number");
            // AlexKh - 1.2.48.15 - 2012-02-06 - CR#1152 - check distributor is crossdoc
            //IF ll_distributor_num <> 0 THEN
            if (ll_distributor_num != 0 && !f_is_crossdoc_distributorClass.f_is_crossdoc_distributor(ll_distributor_num))
            {
                is_supplier_edi = f_get_supplier_distributor_ediClass.f_get_supplier_distributor_edi(ll_supplier_num, ll_distributor_num);
                is_parent_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_distributor_num);
                // AlexKh - 1.2.48.26 - 2012-05-23 - SPUCM00003402 - check distributor is crossdoc
            }
            else if (f_is_crossdoc_distributorClass.f_is_crossdoc_distributor(ll_distributor_num))
            {
                is_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_supplier_num);
            }
            // End
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                is_parent_supplier_edi = is_supplier_edi;
            }
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "receiver", is_parent_supplier_edi);
            // End

            ids_debit_xml_temp.SetItem(al_row, "doctype", "INVDBT");

            ids_debit_xml_temp.SetItem(al_row, "aprf", "INVDBT");

            ids_debit_xml_temp.SetItem(al_row, "ackn", "1"); // 1 - Required        

            ids_debit_xml_temp.SetItem(al_row, "testind", "1"); // 1 - Test   
            ll_message_date = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd"));

            ids_debit_xml_temp.SetItem(al_row, "messdate", ll_message_date);
            // SharonS - 1.2.42.0 - 2009-01-29 - SPUCM00000853 - Time format
            //ll_message_time = Long(String(Today(), "hhmmdd"))
            // SharonS - 1.2.44.4 - 2009-05-21 - SPUCM00001339 - Time format
            //ll_message_time = Long(String(Today(), "hhmmss"))
            ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("hhmmss");
            // End
            //ids_debit_xml_temp.SetItem(al_row, "messtime", ll_message_time)

            ids_debit_xml_temp.SetItem(al_row, "messtime", ls_message_time);
            // End
            return 1;
        }
        public async Task<FuncResults<int, IRepository[], bool>> uf_send_transaction(int ai_file_status, string as_supplier_edi, IRepository[] ads_data) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_send_transaction
            //*Purpose:					Create the B2B message XML file.
            //*								This function will be coded in the decendance objects.
            //*								In each object a defferent message will be created.
            //*  
            //*Arguments:				Integer - file_status
            //*Return:						Boolean - TRUE - Creation of B2B message XML file succeded.
            //											 FALSE - Creation of B2B message XML file failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*19-12-2007		SharonS				0.1.0 		B2B 			Create the B2B message XML file.
            //************************************************************************************************

            int li_index = 0;
            int li_debit_row = 0;
            long ll_find = 0;
            long ll_row = 0;
            long ll_row_copy = 0;
            long ll_debit_items = 0;
            string ls_find = null;
            string ls_organization_edi = null;
            string ls_branch_edi = null;
            string ls_branch_name = null;
            string ls_supplier_edi = null;
            string ls_debit_no = null;
            string ls_message = null;
            // Check validity
            if (CollectionExtensions.UBound(ads_data) < 0)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }

            li_debit_row = (int)ads_data[0].RowCount();
            if (li_debit_row < 1)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_header = ads_data[0];
            // Only for Debit and Return
            if (is_message_type != "384")
            {

                ll_debit_items = ads_data[1].RowCount();
                if (ll_debit_items < 1)
                {
                    return FuncResults.Return(ai_file_status, ads_data, false);
                }
                ids_details = ads_data[1];
            }
            is_supplier_edi = as_supplier_edi;
            // SharonS - 1.2.42.0 - 2009-01-27 - CR#1100 - SPUCM00000115
            is_parent_supplier_edi = is_supplier_edi;
            // End
            // Create and define DataStore

            ids_debit_xml.Reset();

            ids_debit_xml_temp.Reset();
            // SharonS - 1.2.33.1 - 2008-07-20 - Task#10005 - Send Return Message
            //ls_debit_no = String(ids_header.GetItem<int>(1, "debit_credit_number"))
            if (is_message_type == "383")
            {

                ls_debit_no = ids_header.GetItemValue<int>(0, "debit_credit_number").ToString();
            }
            else if (is_message_type == "389")
            {

                ls_debit_no = ids_header.GetItemValue<int>(0, "invoice_number").ToString();
            }
            // End

            ll_row = ids_debit_xml_temp.Insert(0);
            // Set the envalope data in the XML DW
            if ( await this.uf_insert_envelope(ll_row) < 0)
            {
                // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                await MessageBox.Show("שים לב - כשלון בכתיבת מעטפת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file envelop.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Set the header data in the XML DW
            if (this.uf_insert_header(ll_row) < 0)
            {
                // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                await MessageBox.Show("שים לב - כישלון בכתיבת כותרת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file header.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            //any	lany, lany_temp
            //lany_temp = ids_debit_xml_temp.Object.Data
            // Only for Confirm
            ids_debit_xml.Export("XMLExport");
            if (is_message_type == "384")
            {
                ll_row_copy = (long)ids_debit_xml_temp.RowsCopy(0, 1, ModelBuffer.Primary, ids_debit_xml, li_index + 1, ModelBuffer.Primary);




                ids_debit_xml.Export("xmlconfirm");
            }
            // Load the xml DW
            for (li_index = 1; li_index <= (int)ll_debit_items; li_index++)
            {



                ll_row_copy = (long)ids_debit_xml_temp.RowsCopy(0, 1, ModelBuffer.Primary, ids_debit_xml, li_index + 1, ModelBuffer.Primary);
                //	lany = ids_debit_xml.Object.Data
                //Eitan; SPUCM00004917; Aug/3/2014; Build 57;
                if (is_message_type == "380")
                {
                    if (this.uf_insert_details_okef(li_index) < 0)
                    {
                        // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון בכתיבת פריט במסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file details.";
                        await uf_write_log(ls_message);
                        ai_file_status = 0;
                        return FuncResults.Return(ai_file_status, ads_data, false);
                    }
                }
                else
                {
                    if (this.uf_insert_details(li_index) < 0)
                    {
                        // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון בכתיבת פריט במסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file details.";
                        await uf_write_log(ls_message);
                        ai_file_status = 0;
                        return FuncResults.Return(ai_file_status, ads_data, false);
                    }
                }
            }
            // Create xml file from datawindow
            if (!uf_save_as_xml())
            {
                switch (is_message_type)
                {
                    case "384":
                        // "CONFIRM"
                        // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר אישור חשבונית לספק - המסר הדיגיטאלי לא יישלח", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        ls_message = "Confirm Invoice No: " + ls_debit_no + " - Failed to Create/Write to File.";
                        break;
                    case "383":
                        // "DEBIT" 
                        // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        ls_message = "Debit No: " + ls_debit_no + " - Failed to Create/Write to File.";
                        break;
                    case "389":
                        // "RETURN"
                        // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר תעודת החזרה - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        ls_message = "Return No: " + ls_debit_no + " - Failed to Create/Write to File.";
                        break;
                }
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            else
            {
                ai_file_status = 1;
            }
            return FuncResults.Return(ai_file_status, ads_data, true);
        }
        public bool uf_set_message_type(string as_message_type) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_set_message_type
            //*Purpose:					Set message type: Debit, Return or Confirm.
            //*  
            //*Arguments:				String	 - as_message_type
            //*Return:						Boolean - TRUE - Succeed.
            //*										      FALSE - Faild.
            //*Date 			Programer		Version		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*21-01-2008		SharonS			1.2.23.0 		10001		Initiate Version
            //************************************************************************************************

            //Eitan; SPUCM00004917; Aug/3/2014; Build 57; ADD IF for OKEF
            if (as_message_type == "OKEF") //Okef Debit Invoice
            {
                is_message_type = "380";
            }
            else if (as_message_type == "DEBIT")
            {
                //Debit Note (Responce for supplier Invoice)
                is_message_type = "383";
            }
            else if (as_message_type == "RETURN")
            {
                //Self-Build Invoice (Return Invoice)
                is_message_type = "389";
            }
            else if (as_message_type == "CONFIRM")
            {
                //Corrected Invoice (Confirmation Invoice)
                is_message_type = "384";
            }
            //Eitan; SPUCM00004917; Aug/3/2014; Build 57;
            if (is_message_type == "380")
            {

                //ids_debit_xml = "d_debit_okef_for_b2b_xml";

                //ids_debit_xml_temp = "d_debit_okef_for_b2b_xml";
            }
            return true;
        }
        public int uf_insert_details_okef(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_insert_details_okef
            //*Purpose:					Insert the Deatails section of the debit XML
            //*  
            //*Arguments:				Long	- Row No (al_row)
            //*Return:						Integer - 1 succeed.
            //*											-1 not  succeed.
            //*Date 			Programer		Version	Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*Aug-2014		Eitan					 		B2B			Initial Version
            //************************************************************************************************

            string ls_details = null;
            string ls_adhoc_key = null;
            decimal ldc_actual_price = default(decimal);
            decimal ldc_item_total = default(decimal);
            long ll_reason_number = 0;
            long ll_original_price = 0;
            long ll_original_quantity = 0;
            long ll_res = 0;
            GridElement ldwc_dec_num;

            ll_reason_number = ids_details.GetItemValue<long>((int)al_row, "reason_number");

            ls_details = ids_details.GetItemValue<string>(al_row, "details");

            ll_original_price = ids_details.GetItemValue<long>((int)al_row, "original_price");

            ll_original_quantity = ids_details.GetItemValue<long>((int)al_row, "original_quantity");

            ls_adhoc_key = ids_details.GetItemValue<string>(al_row, "adhoc_key");

            ldc_actual_price = ids_details.GetItemValue<decimal>((int)al_row, "actual_price");

            ldc_item_total = ids_details.GetItemValue<decimal>((int)al_row, "item_total");

            ll_res = (long)ids_debit_xml.SetItem(al_row, "lineno", al_row);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "itembarcode", ll_reason_number.ToString());

            ll_res = (long)ids_debit_xml.SetItem(al_row, "remark", ls_details);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "brandname", ll_original_price);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "deptno", ll_original_quantity);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "confirmation", Convert.ToInt64(ls_adhoc_key));

            ll_res = (long)ids_debit_xml.SetItem(al_row, "packno", 0);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "unitsqty", 0);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "unitsqtymea", "PCE");

            ll_res = (long)ids_debit_xml.SetItem(al_row, "reasoncode", "999");

            ll_res = (long)ids_debit_xml.SetItem(al_row, "itempricenetosph", ldc_actual_price);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "itempricenetosupplier", 0);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "pricedifference", 0);

            ll_res = (long)ids_debit_xml.SetItem(al_row, "linesum", ldc_item_total);
            return 1;
        }
        public nvuo_b2b_debit()
            : base()
        {
            constructor();
        }

        public void constructor() // Create and define DataStore
        {
            ids_debit_xml = new d_debit_for_b2b_xmlRepository();
            ids_debit_xml_temp = new d_debit_for_b2b_xmlRepository();
        }

        public void LoadData(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, string ls_material_barcode, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number }, { "@5", ls_material_barcode } };
                    string sql = "SELECT line_number FROM b2b_invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND invoice_number = @3 AND item_number = @4 AND item_barcode = @5 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND bonus_line = 0 AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number } };
                    string sql = "SELECT line_number FROM b2b_invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND invoice_number = @3 AND item_number = @4 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND bonus_line = 0 AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, string ls_material_barcode, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number }, { "@5", ls_material_barcode } };
                    string sql = "SELECT line_number FROM b2b_packing_list_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND pack_list_number = @3 AND item_number = @4 AND item_barcode = @5 AND (item_bonus_code IS NULL OR item_bonus_code = ) AND status = 'a'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(long ll_branch_number, long ll_supplier_number, long ll_order_number, long ll_doc_number, long ll_item_number, ref long ll_line_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_branch_number }, { "@1", ll_supplier_number }, { "@2", ll_order_number }, { "@3", ll_doc_number }, { "@4", ll_item_number } };
                    string sql = "SELECT line_number FROM b2b_packing_list_details WHERE branch_number = @0 AND supplier_number = @1 AND order_number = @2 AND pack_list_number = @3 AND item_number = @4 AND status = 'a' AND (item_bonus_code IS NULL OR item_bonus_code = )";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_line_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
