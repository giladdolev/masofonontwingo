using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;

namespace global
{
    // Creation Time:   08/11/2015 10:21:01 (Utc)
    // Comments:        
    // 
    public class w_add_translation : WindowElement
    {
        public long il_new_expression_number;
        public bool ib_new;
        public w_add_translation(str_pass pass)
        {
            Pass = pass;
        }

        public str_pass Pass { get; set; }

        public long il_new_expression_numberProperty
        {
            get { return this.il_new_expression_number; }
            set { this.il_new_expression_number = value; }
        }
        public bool ib_newProperty
        {
            get { return this.ib_new; }
            set { this.ib_new = value; }
        }
        public void LoadData()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT isnull(max(number), 0) + 1 FROM expressions";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        il_new_expression_numberProperty = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(string ls_message)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_new_expression_numberProperty }, { "@1", ls_message } };
                    string sql = "INSERT INTO expressions (number,  name) VALUES (@0,  @1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData1(int li_language_number, string ls_translation)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", li_language_number }, { "@1", il_new_expression_numberProperty }, { "@2", ls_translation } };
                    string sql = "INSERT INTO dictionary (application_number,  language_number,  expression_number,  translation) VALUES (1,  @0,  @1,  @2)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(int li_language_number, string ls_translation)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", li_language_number }, { "@1", il_new_expression_numberProperty }, { "@2", ls_translation } };
                    string sql = "INSERT INTO dictionary (application_number,  language_number,  expression_number,  translation) VALUES (1,  @0,  @1,  @2)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3(string ls_translation, int li_language_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_translation }, { "@1", li_language_number }, { "@2", il_new_expression_numberProperty } };
                    string sql = "UPDATE dictionary SET translation = @0 WHERE application_number = 1 AND language_number = @1 AND expression_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
