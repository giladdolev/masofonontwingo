using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace global
{
	// Creation Time:   08/11/2015 10:21:01 (Utc)
	// Comments:        
	// 
	public class w_get_item_from_barcode : WindowElement
	{
		public long wl_Last_Row_Selected;
		public long il_supplier_number;
		public string is_barcode;
        public RepositoryBase<dw_item_code> is_dataobject;
		public s_reports_db istct_reports_db;
		public delegate void ue_exit_EventHandler();
		//public event ue_exit_EventHandler ue_exitEvent;
		public delegate void ue_post_open_EventHandler();
		//public event ue_post_open_EventHandler ue_post_openEvent;
		public delegate void ue_retrieve_EventHandler();
		//public event ue_retrieve_EventHandler ue_retrieveEvent;
        public w_get_item_from_barcode(s_array_arguments parms)
	    {
	        this.Parms = parms;
	    }

	    public s_array_arguments Parms { get; set; }
		
		
		public long wl_Last_Row_SelectedProperty
		{
			get { return this.wl_Last_Row_Selected; }
			set { this.wl_Last_Row_Selected = value; }
		}
		public long il_supplier_numberProperty
		{
			get { return this.il_supplier_number; }
			set { this.il_supplier_number = value; }
		}
		public string is_barcodeProperty
		{
			get { return this.is_barcode; }
			set { this.is_barcode = value; }
		}
		public RepositoryBase<dw_item_code> is_dataobjectProperty
		{
			get { return this.is_dataobject; }
			set { this.is_dataobject = value; }
		}
		public s_reports_db istct_reports_dbProperty
		{
			get { return this.istct_reports_db; }
			set { this.istct_reports_db = value; }
		}

        
    }
}
