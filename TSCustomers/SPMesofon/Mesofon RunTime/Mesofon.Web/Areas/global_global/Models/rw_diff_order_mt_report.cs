using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using globalAlias = global;
using Mesofon.Common;
using Mesofon.Repository;

namespace global
{
	// Creation Time:   08/11/2015 10:20:57 (Utc)
	// Comments:        
	// 
	public class rw_diff_order_mt_report : WindowElement
	{
		public long il_branch_number;
		public long il_supplier_number;
		public long il_order_number;
		public const long il_max_window_height = 1008;
		public const long il_max_window_width = 1088;
		public bool ib_shipment_mode = false;
		public long il_shipment_number;
		public IRepository ids_shipment_orders;
		public delegate void ue_print_EventHandler();
		public event globalAlias.rw_diff_order_mt_report.ue_print_EventHandler ue_printEvent;
		public delegate void ue_print_for_shipment_EventHandler();
		public event globalAlias.rw_diff_order_mt_report.ue_print_for_shipment_EventHandler ue_print_for_shipmentEvent;
        public rw_diff_order_mt_report(string ls_params, string FormType)
	    {
            Params = ls_params;
            this.FormType = FormType;
        }

	    public string Params { get; set; }
        public string FormType { get; set; }

        public long il_branch_numberProperty
		{
			get { return this.il_branch_number; }
			set { this.il_branch_number = value; }
		}
		public long il_supplier_numberProperty
		{
			get { return this.il_supplier_number; }
			set { this.il_supplier_number = value; }
		}
		public long il_order_numberProperty
		{
			get { return this.il_order_number; }
			set { this.il_order_number = value; }
		}
		public bool ib_shipment_modeProperty
		{
			get { return this.ib_shipment_mode; }
			set { this.ib_shipment_mode = value; }
		}
		public long il_shipment_numberProperty
		{
			get { return this.il_shipment_number; }
			set { this.il_shipment_number = value; }
		}
		public IRepository ids_shipment_ordersProperty
		{
			get { return this.ids_shipment_orders; }
			set { this.ids_shipment_orders = value; }
		}

       
    }
}
