using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using globalAlias = global;

namespace global
{
	// Creation Time:   08/11/2015 10:20:57 (Utc)
	// Comments:        
	// 
	public class rw_diff_inv_mt_report : WindowElement
	{
		public long il_branch_number;
		public long il_supplier_number;
		public long il_order_number;
		//Constant Long				il_max_window_height = 1008
		//Constant Long				il_max_window_width = 1088
		//
		public const long il_max_window_height = 1136;
		public const long il_max_window_width = 1079;
		public bool ib_shipment_mode;
		public string is_pallet_number;
		public long il_shipment_number;
		public IRepository ids_pallet_invoices;
		// AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - marlog_number
		public long il_marlog_branch;
		public delegate void ue_print_EventHandler();
		public event globalAlias.rw_diff_inv_mt_report.ue_print_EventHandler ue_printEvent;
        public rw_diff_inv_mt_report(string ls_params, string FormType)
        {
            Params = ls_params;
            this.FormType = FormType;
        }

	    public string Params { get; set; }
        public string FormType { get; set; }
        public long il_branch_numberProperty
		{
			get { return this.il_branch_number; }
			set { this.il_branch_number = value; }
		}
		public long il_supplier_numberProperty
		{
			get { return this.il_supplier_number; }
			set { this.il_supplier_number = value; }
		}
		public long il_order_numberProperty
		{
			get { return this.il_order_number; }
			set { this.il_order_number = value; }
		}
		public bool ib_shipment_modeProperty
		{
			get { return this.ib_shipment_mode; }
			set { this.ib_shipment_mode = value; }
		}
		public string is_pallet_numberProperty
		{
			get { return this.is_pallet_number; }
			set { this.is_pallet_number = value; }
		}
		public long il_shipment_numberProperty
		{
			get { return this.il_shipment_number; }
			set { this.il_shipment_number = value; }
		}
		public IRepository ids_pallet_invoicesProperty
		{
			get { return this.ids_pallet_invoices; }
			set { this.ids_pallet_invoices = value; }
		}
		public long il_marlog_branchProperty
		{
			get { return this.il_marlog_branch; }
			set { this.il_marlog_branch = value; }
		}

       
    }
}
