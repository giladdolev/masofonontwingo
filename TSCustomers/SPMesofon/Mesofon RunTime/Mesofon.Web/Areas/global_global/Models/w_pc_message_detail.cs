using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:21:02 (Utc)
    // Comments:        translate pc_message_detail table (c) stas 07/06/2004
    // 
    public class w_pc_message_detail : WindowElement
    {
        public string gs_message_skey;
        public string gs_language_skey;
        public string gs_message_text;
        public bool ib_new;
        public w_pc_message_detail(int error_number)
        {
            ErrorNumber = error_number;
        }

        public int ErrorNumber { get; set; }

        public string gs_message_skeyProperty
        {
            get { return this.gs_message_skey; }
            set { this.gs_message_skey = value; }
        }
        public string gs_language_skeyProperty
        {
            get { return this.gs_language_skey; }
            set { this.gs_language_skey = value; }
        }
        public string gs_message_textProperty
        {
            get { return this.gs_message_text; }
            set { this.gs_message_text = value; }
        }
        public bool ib_newProperty
        {
            get { return this.ib_new; }
            set { this.ib_new = value; }
        }
        public void LoadData(long ll_skey, ref string ls_message)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@1", ll_skey}
                    };
                    string sql = "SELECT message_text FROM pc_message_detailWHERE pc_message_detail.language_skey = 3 AND message_skey = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_message = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(long ll_skey, ref string ls_message)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_skey } };
                    string sql = "SELECT message_text FROM pc_message_detailWHERE pc_message_detail.language_skey = 1 AND message_skey = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_message = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void LoadData2()
        {

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT message_skey,  language_skey,  message_text FROM pc_message_detail WHERE (pc_message_detail.language_skey = 3 AND message_text LIKE '�� �����%' AND message_skey NOT IN (SELECT message_skey FROM pc_message_detail WHERE pc_message_detail.language_skey = 1 AND message_text LIKE '*********  Add Translation **********')) ";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        gs_message_skeyProperty = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        gs_language_skeyProperty = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        gs_message_textProperty = UnitOfWork.GetValue(resultRS, 2, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(int li_message_skey, int language_skey, string ls_message_title, string sle_1Text, string ls_id, DateTime? ld_date)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>
		            {
		                {"@1", li_message_skey},
                        {"@2", language_skey},
                        {"@3", ls_message_title},
                        {"@4", sle_1Text},
                        {"@5", ls_id},
                        {"@6", ld_date},
		            };
                    string sql = "INSERT INTO pc_message_detail (message_skey, language_skey, message_title, message_text, create_user_id, create_date, maint_user_id, maint_date) VALUES (@0, @1, @2, @3, @4, @5, @4, @5)"; ;
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
        }
        public void UpdateData3(int li_message_skey, int language_skey)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    TextBoxElement sle_1 = this.GetVisualElementById<TextBoxElement>("sle_1");
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", li_message_skey }, { "@1", language_skey }, { "@2", sle_1.Text } };
                    string sql = "INSERT INTO pc_message_detail (message_skey, language_skey, message_text) VALUES (@0, @1, @2)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData4(int li_message_skey)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    TextBoxElement sle_1 = this.GetVisualElementById<TextBoxElement>("sle_1");
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", sle_1.Text }, { "@1", li_message_skey } };
                    string sql = "UPDATE pc_message_detail SET message_text = @0 WHERE pc_message_detail.language_skey = 1 AND message_skey = @1";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Load" /> event.
        /// </summary>
        /// <param name="args">The <see cref="Load" /> instance containing the event data.</param>
        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            // Call the activated event
            OnActivated(args);
        }
    }
}
