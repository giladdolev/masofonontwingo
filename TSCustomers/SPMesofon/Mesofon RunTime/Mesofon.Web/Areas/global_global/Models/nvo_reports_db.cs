using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Data;
using Mesofon.Common;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:20:53 (Utc)
    // Comments:        
    // 
    public class nvo_reports_db : TimerElement
    {
        public bool ib_reports_db_active;
        public bool ib_lock_ds;
        public bool ib_must_repdb;
        public int ii_imm_load;
        public int ii_tran_on_split;
        public long il_db_version;
        public long il_params_count;
        public long il_rep_timer_load_db;
        public long il_max_delay;
        public long il_ins_rep_names;
        public string is_unknown = "UnKnown";
        public string is_message_text;
        public string is_date_format = "dd/MM/yyyy hh:mm:ss";
        public string is_branch_station;
        public string is_title;
        public string is_msg_TRY_LATER;
        public string is_msg_OVER_DELAY;
        public IRepository ids_repots_db;
        private async Task uf_read_reports_db_parms() // uf_read_reports_db_parms()
        {
            bool lb_same_db_parms = false;
            int li_packet_size = 0;
            int li_new_packet_size = 0;
            long ll_start = 0;
            long ll_end = 0;
            string ls_main_profile = null;
            string ls_reports_profile = null;
            string ls_new_reg_entery = null;
            string ls_message_text = null;
            string ls_database_type = null;
            string ls_conn_values = null;
            string ls_new_packet_size = null;
            ls_message_text = "";
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
            {
                ls_message_text = "Going To Read Reports DataBase Connection Configuration." + "\r" + "\n";
            }
            ls_main_profile = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key, "database_profile", "SQLCA", "DB Default Profile");
            ls_reports_profile = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key, "database_reports_profile", "SQLCA_Reports", "Report DB Default Profile");
            if (isempty_stringClass.isempty_string(ls_reports_profile))
            {
                ls_message_text += "Reports DataBase Is Not Set." + "\r" + "\n";
                goto Main_Connected;
            }
            else
            {
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 3)
                {
                    ls_message_text += "MAIN Profile = '" + ls_main_profile.ToString() + "' ,Reports Profile = '" + ls_reports_profile.ToString() + "'." + "\r" + "\n";
                }
                if (ls_main_profile.ToLower(CultureInfo.InvariantCulture) == ls_reports_profile.ToLower(CultureInfo.InvariantCulture))
                {
                    ls_message_text += "Reports And Main Databases Are The Same Entery In Registry DataBase Profile Configuration." + "\r" + "\n";
                    goto Main_Connected;
                }
            }
            //dbms = SYC Adaptive Server Enterprise
            //dbparm = ConnectString='DSN=TARGET_DB;UID=user;PWD=user'
            //dbparm = Release='12.5',Log=0,Locale='enu',CharSet='cp850',Language='us_english'
            ls_new_reg_entery = masofonAlias.masofon.Instance.s_reg_key + ls_reports_profile;
            
            
            masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "servername", masofonAlias.masofon.Instance.sqlca.ServerName.ToString(), "DB Server Name"));
            
            
            masofonAlias.masofon.Instance.SQLCA_REPORTS.Database = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "database", masofonAlias.masofon.Instance.sqlca.Database.ToString(), "DB data file name"));
            
            
            masofonAlias.masofon.Instance.SQLCA_REPORTS.DBMS = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "dbms", masofonAlias.masofon.Instance.sqlca.DBMS.ToString(), "DB Vendor"));
            
            
            masofonAlias.masofon.Instance.SQLCA_REPORTS.LogID = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "logid", masofonAlias.masofon.Instance.sqlca.LogID.ToString(), "Log Login"));
            // TODO: Field 'LogPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'LogPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            masofonAlias.masofon.Instance.SQLCA_REPORTS.LogPass = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "logpass", masofonAlias.masofon.Instance.sqlca.LogPass.ToString(), "Log Password"));
            // TODO: Field 'UserID of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'UserID of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            masofonAlias.masofon.Instance.SQLCA_REPORTS.UserID = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "userid", masofonAlias.masofon.Instance.sqlca.UserID.ToString(), "User Id"));
            // TODO: Field 'DBPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'DBPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            masofonAlias.masofon.Instance.SQLCA_REPORTS.DBPass = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "dbpass", masofonAlias.masofon.Instance.sqlca.DBPass.ToString(), "User Password"));
            
            
            masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "dbparm", masofonAlias.masofon.Instance.sqlca.DBParm.ToString(), "DB Parameters String"));
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.AutoCommit = true;
            
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() == null)
            {
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = "";
            }
            ls_database_type = RegToDatabase.GetRegistryItem(ls_new_reg_entery, "DataBaseType", "ASE", "ASE = Sybase Enterprise, ASA=Sybase Adaptive").ToLower(CultureInfo.InvariantCulture);
            if (ls_database_type != "ase" && (ls_database_type != "asa" && ls_database_type != "sql"))
            {
                ls_message_text += "DataBase Type Not Configured In Program Registry, (ASA, ASE, SQL)." + "\r" + "\n";
                goto Main_Connected;
            }
            
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBMS.ToUpper() == "ODBC")
            {
                
                
                lb_same_db_parms = (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() == masofonAlias.masofon.Instance.sqlca.DBParm.ToString());
                
                if (string.IsNullOrEmpty(masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString()))
                {
                    
                    ll_start = (long)masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.IndexOf("DSN=", 0);
                    if (ll_start > 0)
                    {
                        ll_start += 4;
                        
                        ll_end = (long)masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.IndexOf(";", (int) ll_start + 1, StringComparison.Ordinal);
                        if (ll_end == 0)
                        {
                            
                            ll_end = (long)masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.IndexOf("'", (int) ll_start + 1, StringComparison.Ordinal);
                        }
                        if (ll_end > ll_start)
                        {
                            
                            
                            
                            masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(Convert.ToInt32(ll_start) - 1, masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - Convert.ToInt32(ll_end));
                        }
                    }
                }
            }
            else
            {
                
                
                
                
                lb_same_db_parms = (masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() == masofonAlias.masofon.Instance.sqlca.ServerName.ToString() && masofonAlias.masofon.Instance.SQLCA_REPORTS.Database.ToString() == masofonAlias.masofon.Instance.sqlca.Database.ToString());
            }
            li_packet_size = Convert.ToInt32(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "PacketSize", "1024", "Network Packet Size, Only For ASE or MS SQL"));
            li_new_packet_size = li_packet_size;
            if (li_packet_size % 256 != 0)
            {
                li_new_packet_size = li_packet_size / 256 * 256;
            }
            if (li_new_packet_size < 256)
            {
                li_new_packet_size = 1024;
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_packet_size % 256 != 0)
            {
                masofonAlias.masofon.Instance.gs_vars.reports_packet_size = Convert.ToInt32(masofonAlias.masofon.Instance.gs_vars.reports_packet_size / 256) * 256;
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_packet_size > 0 && masofonAlias.masofon.Instance.gs_vars.reports_packet_size != li_new_packet_size)
            {
                li_new_packet_size = (int)masofonAlias.masofon.Instance.gs_vars.reports_packet_size;
            }
            if (li_new_packet_size != li_packet_size)
            {
                ls_new_packet_size = li_new_packet_size.ToString();
                RegToDatabase.RegistrySet(ls_new_reg_entery, "PacketSize", (RegistryValueType.RegString), ls_new_packet_size);
                ls_message_text += "Packet Size Changed From " + li_packet_size.ToString() + " To " + ls_new_packet_size + "." + "\r" + "\n";
                li_packet_size = li_new_packet_size;
            }
            if (ls_database_type == "asa")
            {
                
                
                if (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - 1, default(int)).ToString() != "'")
                {
                    
                    masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm += "'";
                }
                
                
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(0, masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - 1).ToString() + ";Con='REPORTS_" + is_branch_station + "''");
            }
            else if (ls_database_type == "ase")
            {
                
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() + ",AppName='REPORTS_" + is_branch_station + "',PacketSize=" + li_packet_size.ToString());
            }
            
            
            
            
            
            
            
            
            ls_conn_values = "Main ServerName = '" + masofonAlias.masofon.Instance.sqlca.ServerName.ToString() + "'" + " ,Reports ServerName = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() + "' Main Database = '" + masofonAlias.masofon.Instance.sqlca.Database.ToString() + "'" + " ,Reports Database = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.Database.ToString() + "' Main DbParm = '" + masofonAlias.masofon.Instance.sqlca.DBParm.ToString() + "'" + " ,Reports DbParm = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() + "' Main UserName = '" + masofonAlias.masofon.Instance.sqlca.LogID.ToString() + "'" + " ,Reports UserName = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.LogID.ToString() + "'." + "\r" + "\n";
            if (lb_same_db_parms)
            {
                ls_message_text += "One Of The Connection Parameters Are Same For Reports And Main Databases." + "\r" + "\n" + ls_conn_values;
                goto Main_Connected;
            }
            else if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 1)
            {
                ls_message_text += ls_conn_values;
            }
            ib_reports_db_active = true;
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
            {
                ls_message_text += "Read Connection Configuration To Reports DataBase Successeded." + "\r" + "\n";
            }
            await uf_write_log_file("uf_read_reports_db_parms()", ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_read_reports_db_parms()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return;
        Main_Connected:
            ib_reports_db_active = false;
            ls_message_text += "Error In Connection Configuration To Reports DataBase." + "\r" + "\n";
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_read_reports_db_parms()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS != null)
            {

            }
             await  uf_write_log_file("uf_read_reports_db_parms()", ls_message_text);
             await   masofonAlias.masofon.Instance.guo_msg.uf_msg("uf_read_reports_db_parms()", "", "stopsign!", ls_message_text);
        }
        protected async Task uf_write_log_file(string as_title, string as_message) // uf_write_log_file(as_title, as_message)
        {
            // Global Parameters :
            //	gs_vars.reports_log_path - C:\DIR\HEADER_FILE
            // gs_vars.reports_log_debug_level - 0=No, 1=Err, 2=Msg, 3=Full, 4-Extra
            //	gs_vars.reports_log_bytes - 5,000,000 Bytes To Remove
            //	gs_vars.reports_log_mode - REP=Replace, OtherWise - Delete
            int li_file_number = 0;
            DateTime? ldt_now = default(DateTime);
            string ls_error_log_name = null;
            string ls_new_file_name = null;
            string ls_message_text = null;
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level < 1 || isempty_stringClass.isempty_string(as_message))
            {
                return;
            }
            if (as_title == null)
            {
                as_title = is_unknown;
            }
            if (as_message.Substring(as_message.Length - 2) == "\r" + "\n")
            {
                as_message = as_message.Substring(0, as_message.Length - 2);
            }
            ldt_now = masofonAlias.masofon.Instance.set_machine_time();
            ls_message_text = "--------------------------------------------------------------------------------------------- DateTime?        : " + ldt_now.Value.ToString(is_date_format) + "\r" + "\n" + "Employee        : " + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + " , '" + f_get_employee_nameClass.f_get_employee_name(masofonAlias.masofon.Instance.gs_vars.branch_number, masofonAlias.masofon.Instance.gs_vars.active_owner) + "' Title" + "\t" + "        : " + as_title + "\r" + "\n" + "Message         : " + as_message;
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log
            await masofonAlias.masofon.Instance.guo_logs_in_table_service.uf_write_log_table(ls_message_text, u_logs_in_table_service.SERVICE_CODE_MASOFON_LOG, 1, "nvo_reports_db.uf_write_log_file", "", "", 3);
        }
        private bool uf_is_special_permission() // uf_is_special_permission()
        {
            return masofonAlias.masofon.Instance.gb_im_the_headoffice && (masofonAlias.masofon.Instance.gi_developer_debug_mode > 0);
        }
        private async Task<string> uf_check_reports_db( s_reports_db arstct_reports_db) // uf_check_reports_db(arstct_reports_db)
        {
            int li_return_code = 0;
            string ls_message_text = null;
            arstct_reports_db.run_datetime = masofonAlias.masofon.Instance.set_machine_time();
            arstct_reports_db.update_datetime = arstct_reports_db.run_datetime;
            li_return_code = await uf_connect_reports_db(false);
            if (li_return_code < 0)
            {
                return "Error";
            }
            ls_message_text += is_message_text;
            arstct_reports_db.delay_minutes = await uf_get_reports_db_update_datetime( arstct_reports_db);
            is_message_text = ls_message_text + is_message_text;
            if (arstct_reports_db.delay_minutes < 0)
            {
                return "Error";
            }
            ls_message_text = is_message_text + "Update DateTime? : " + arstct_reports_db.update_datetime.Value.ToString(is_date_format) + "\r" + "\n" + "Actual Delay    : " + arstct_reports_db.delay_minutes.ToString() + "\r" + "\n";
            return ls_message_text;
        }
        public async Task<long> uf_get_main_cpu_usage(bool ab_log_msg) // uf_get_main_cpu_usage(ab_log_msg)
        {
            long ll_cpu_usage = 0;
            is_message_text = "";
            if (!masofonAlias.masofon.Instance.gs_vars.reports_check_main_db_cpu)
            {
                return 0;
            }
            if (masofonAlias.masofon.Instance.gi_max_cpu_usage == 100)
            {
                return 0;
            }
            if (true)
            {
                LoadData(ref ll_cpu_usage);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {


                is_message_text = "Error - Select Cpu Usage From dbrepl..last_mon.Error Code : '" + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString() + "'Error Text : '" + masofonAlias.masofon.Instance.sqlca.SqlErrText + "'." + "\r" + "\n";
                if (ab_log_msg)
                {
                    await uf_write_log_file("uf_get_main_cpu_usage()", is_message_text);
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_get_main_cpu_usage()", masofonAlias.masofon.Instance.sqlca, false, is_message_text);
                }
                ll_cpu_usage = -1;
            }
            return ll_cpu_usage;
        }
        private async Task<int> uf_check_reports_active(bool ab_log_msg) // uf_check_reports_active(ab_log_msg)
        {
            string ls_message_text = null;
            is_message_text = "";
            if (!masofonAlias.masofon.Instance.gs_vars.reports_is_available)
            {
                ls_message_text = "Reports DataBase Available Parameter Is False." + "\r" + "\n";
                if (ab_log_msg)
                {
                    await uf_write_log_file("uf_check_reports_active()", ls_message_text);
                }
                else
                {
                    is_message_text = ls_message_text;
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_check_reports_active()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
                }
                return -1;
            }
            if (!ib_reports_db_active)
            {
                ls_message_text = "Reports DataBase Connection Configuration Problematic." + "\r" + "\n";
                if (ab_log_msg)
                {
                     await  uf_write_log_file("uf_check_reports_active()", ls_message_text);
                }
                else
                {
                    is_message_text = ls_message_text;
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_check_reports_active()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
                }
                return -1;
            }
            // Lyoha 06.06.06 
            if (il_db_version > 153)
            {
                if (masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<int>(0, "report_db_active") != 1)
                {
                    ls_message_text = "Reports DataBase Available Parameter Is False." + "\r" + "\n";
                    if (ab_log_msg)
                    {
                      await  uf_write_log_file("uf_check_reports_active()", ls_message_text);
                    }
                    else
                    {
                        is_message_text = ls_message_text;
                    }
                    if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                    {
                        await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_check_reports_active()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
                    }
                    return -1;
                }
            }
            return 0;
        }
        private async Task<int> uf_connect_reports_db(bool ab_log_msg) // uf_connect_reports_db(ab_log_msg)
        {
            int li_return_code = 0;
            string ls_message_text = null;
            li_return_code =  await uf_check_reports_active(ab_log_msg);
            if (li_return_code < 0)
            {
                return li_return_code;
            }
             
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBHandle() == 0)
            {
                
                ls_message_text = masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() + " - Connect Reports DataBase ..." + "\r" + "\n";

                if (masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlCode != 0)
                {


                    ls_message_text += "Reports DataBase Connect Fail!.Error Code : '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlCode.ToString() + "'Error Text : '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlErrText + "'." + "\r" + "\n";
                    li_return_code = -1;
                }
                if (ab_log_msg)
                {
                    
                  await  uf_write_log_file("uf_connect_reports_db(" + masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() + ")", ls_message_text);
                }
                else
                {
                    is_message_text = ls_message_text;
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_connect_reports_db()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
                }
            }
            return li_return_code;
        }
        public async Task<s_reports_db> uf_get_db_transaction(string as_report_name, bool ab_must_report_db) // uf_get_db_transaction(as_report_name, ab_must_report_db)
        {
            long ll_find = 0;
            string ls_message_text = null;
            string ls_msg = null;
            string ls_title = null;
            s_reports_db lstct_reports_db = default(s_reports_db);
            if (as_report_name == null)
            {
                as_report_name = is_unknown;
            }
            ls_title = "(" + as_report_name + "," + ab_must_report_db.ToString() + ")";
            ll_find =await  uf_get_db_parm(as_report_name);
            if (ll_find == 0)
            {
                uf_insert_report(as_report_name, 1, -1, ab_must_report_db, -1);
            }
            else if (ll_find > 0)
            {
                ab_must_report_db = ib_must_repdb;
                ls_title += " --> (" + ab_must_report_db.ToString() + ")";
            }
            ls_msg =await  uf_check_reports_db( lstct_reports_db);
            ls_message_text += is_message_text;
            if (ls_msg == "Error")
            {
                goto Main_Connect;
            }
            lstct_reports_db.status = "REP";
            lstct_reports_db.report_tran = masofonAlias.masofon.Instance.SQLCA_REPORTS;
            ls_message_text += uf_get_status_returned(lstct_reports_db);
             await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        Main_Connect:
            ls_message_text += (await uf_set_main_db(lstct_reports_db, ab_must_report_db)).Retrieve(out lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        }
        public async Task<s_reports_db> uf_get_db_transaction(string as_report_name, long al_max_delay_allow, bool ab_must_report_db) // uf_get_db_transaction(as_report_name, al_max_delay_allow, ab_must_report_db)
        {
            long ll_find = 0;
            string ls_message_text = null;
            string ls_msg = null;
            string ls_title = null;
            s_reports_db lstct_reports_db = default(s_reports_db);
            if (as_report_name == null)
            {
                as_report_name = is_unknown;
            }
            if (al_max_delay_allow == 0)
            {
                al_max_delay_allow = 0;
            }
            ls_title = "(" + as_report_name + "," + al_max_delay_allow.ToString() + "," + ab_must_report_db.ToString() + ")";
            ll_find =await  uf_get_db_parm(as_report_name);
            if (ll_find == 0)
            {
                uf_insert_report(as_report_name, 2, al_max_delay_allow, ab_must_report_db, -1);
            }
            else if (ll_find > 0)
            {
                al_max_delay_allow = il_max_delay;
                ab_must_report_db = ib_must_repdb;
                ls_title += " --> (" + al_max_delay_allow.ToString() + "," + ab_must_report_db.ToString() + ")";
            }
            ls_msg = await uf_check_reports_db( lstct_reports_db);
            ls_message_text += is_message_text;
            if (ls_msg == "Error")
            {
                goto Main_Connect;
            }
            if (lstct_reports_db.delay_minutes > al_max_delay_allow)
            {
                ls_message_text += "Actual Delay Minutes : " + lstct_reports_db.delay_minutes.ToString() + " Is Greater Than The Maximum Delay Minutes Alowed : " + al_max_delay_allow.ToString() + "." + "\r" + "\n";
                goto Main_Connect;
            }
            lstct_reports_db.status = "REP";
            lstct_reports_db.report_tran = masofonAlias.masofon.Instance.SQLCA_REPORTS;
            ls_message_text += uf_get_status_returned(lstct_reports_db);
            await  uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        Main_Connect:
            ls_message_text += (await uf_set_main_db(lstct_reports_db, ab_must_report_db)).Retrieve(out lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        }
        public async Task<s_reports_db> uf_get_db_transaction(string as_report_name, DateTime? adt_from_datetime, DateTime? adt_to_datetime, long al_max_delay_allow, bool ab_must_report_db) // uf_get_db_transaction(as_report_name, adt_from_datetime, adt_to_datetime, 
        {
            //								 al_max_delay_allow, ab_must_report_db, ai_tran_on_split)
            bool lb_delay_mode = false;
            int ai_tran_on_split = 0;
            long ll_find = 0;
            string ls_message_text = null;
            string ls_msg = null;
            string ls_title = null;
            s_reports_db lstct_reports_db = default(s_reports_db);
            if (as_report_name == null)
            {
                as_report_name = is_unknown;
            }
            if (al_max_delay_allow == 0)
            {
                al_max_delay_allow = 0;
            }
            ls_title = "(" + as_report_name + "," + adt_from_datetime.Value.ToString(is_date_format) + "," + adt_to_datetime.Value.ToString(is_date_format) + "," + al_max_delay_allow.ToString() + "," + ab_must_report_db.ToString() + "," + ai_tran_on_split.ToString() + ")";
            ll_find =await  uf_get_db_parm(as_report_name);
            if (ll_find == 0)
            {
                uf_insert_report(as_report_name, 4, al_max_delay_allow, ab_must_report_db, ai_tran_on_split);
            }
            else if (ll_find > 0)
            {
                al_max_delay_allow = il_max_delay;
                ab_must_report_db = ib_must_repdb;
                ai_tran_on_split = ii_tran_on_split;
                ls_title += " --> (" + al_max_delay_allow.ToString() + "," + ab_must_report_db.ToString() + ")";
            }
            ls_msg =await uf_check_reports_db( lstct_reports_db);
            ls_message_text += is_message_text;
            if (ls_msg == "Error")
            {
                goto Main_Connect;
            }
            if (isempty_datetimeClass.isempty_datetime(adt_from_datetime) && isempty_datetimeClass.isempty_datetime(adt_to_datetime))
            {
                ls_message_text += "Dates Are Both Null, Try Delay Mode." + "\r" + "\n";
                lb_delay_mode = true;
            }
            else if (isempty_datetimeClass.isempty_datetime(adt_from_datetime))
            {
                adt_from_datetime = Convert.ToDateTime("01/01/1980").Add(Convert.ToDateTime("00:00:00").TimeOfDay);
                ls_message_text += "From DateTime? Reset To '" + adt_from_datetime.Value.ToString(is_date_format) + "'" + "\r" + "\n";
            }
            else if (isempty_datetimeClass.isempty_datetime(adt_to_datetime))
            {
                adt_to_datetime = lstct_reports_db.run_datetime;
                ls_message_text += "To DateTime? Reset To '" + adt_to_datetime.Value.ToString(is_date_format) + "'" + "\r" + "\n";
            }
            if (lb_delay_mode)
            {
                if (lstct_reports_db.delay_minutes > al_max_delay_allow)
                {
                    ls_message_text += "Actual Delay Minutes : " + lstct_reports_db.delay_minutes.ToString() + " Is Greater Than The Maximum Delay Minutes Alowed : " + al_max_delay_allow.ToString() + "." + "\r" + "\n";
                    goto Main_Connect;
                }
                // Dates Mode
            }
            else
            {
                if (adt_to_datetime < adt_from_datetime)
                {
                    ls_message_text += "'To DateTime' Is Greather Than 'From DateTime'." + "\r" + "\n";
                    goto Main_Connect;
                }
                if (lstct_reports_db.update_datetime < adt_from_datetime)
                {
                    ls_message_text += "Reports DataBase Is Not Update Yet To Run This Report." + "\r" + "\n";
                    goto Main_Connect;
                }
                else if (lstct_reports_db.update_datetime >= adt_from_datetime && lstct_reports_db.update_datetime <= adt_to_datetime)
                {
                    ls_message_text += "Transaction Type On Split '" + ai_tran_on_split.ToString() + "'." + "\r" + "\n";
                    switch (ai_tran_on_split)
                    {
                        case 1:
                            if (ab_must_report_db)
                            {
                                lstct_reports_db.status = "SPLIT";
                            }
                            else
                            {
                                goto Main_Connect;
                            }
                            break;
                        case 2:
                            lstct_reports_db.status = "REP";
                            break;
                        default:
                            lstct_reports_db.status = "SPLIT";
                            break;
                    }
                }
                else if (lstct_reports_db.update_datetime > adt_to_datetime)
                {
                    lstct_reports_db.status = "REP";
                }
            }
            lstct_reports_db.report_tran = masofonAlias.masofon.Instance.SQLCA_REPORTS;
            ls_message_text += uf_get_status_returned(lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        Main_Connect:
            ls_message_text += (await  uf_set_main_db(lstct_reports_db, ab_must_report_db)).Retrieve( out lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        }
        public async Task<long> uf_get_reports_db_update_datetime( s_reports_db arstrct_reports_db) // uf_get_reports_db_update_datetime(arstrct_reports_db)
        {
            int li_return_code = 0;
            long ll_delay_minutes = 0;
            li_return_code = await uf_check_reports_active(false);
            if (li_return_code < 0)
            {
                return li_return_code;
            }
            if (true)
            {
                LoadData1(arstrct_reports_db, ref ll_delay_minutes);
            }

            if (masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlCode != 0)
            {


                is_message_text = "SELECT cur_date, DateDiff(minute, cur_date, GetDate()) FROM rep_dummy;Error Code : '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlCode.ToString() + "'Error Text : '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.SqlErrText + "'." + "\r" + "\n";
                ll_delay_minutes = -1;
            }
            else
            {
                is_message_text = "Update DateTime? : " + arstrct_reports_db.update_datetime.Value.ToString(is_date_format) + "\r" + "\n" + "Actual Delay    : " + ll_delay_minutes.ToString() + "\r" + "\n";
            }
            return ll_delay_minutes;
        }
        private string uf_set_none_db(ref s_reports_db arstct_reports_db) // uf_set_none_db(arstct_reports_db)
        {
            string ls_message_text = null;
            if (uf_is_special_permission())
            {
                ls_message_text += "Special Permission Allow To Run With MAIN DataBase." + "\r" + "\n";
                arstct_reports_db.status = "MAIN";
                arstct_reports_db.report_tran = masofonAlias.masofon.Instance.sqlca;
            }
            else
            {
                arstct_reports_db.status = "NON";

                arstct_reports_db.report_tran = null;

            }
            return ls_message_text;
        }
        private async Task<FuncResults<s_reports_db,string>> uf_set_main_db(s_reports_db arstct_reports_db, bool ab_only_report_db) // uf_set_main_db(arstct_reports_db, ab_only_report_db)
        {
            string ls_message_text = null;
            if (masofonAlias.masofon.Instance.gs_vars.reports_is_available && ab_only_report_db)
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg(is_title, "", "stopsign!", is_msg_OVER_DELAY);
                ls_message_text += uf_set_none_db(ref arstct_reports_db);
            }
            else
            {
                arstct_reports_db.main_cpu_usage =await  uf_get_main_cpu_usage(false);
                ls_message_text += is_message_text + "Cpu Usage       : " + arstct_reports_db.main_cpu_usage.ToString() + "\r" + "\n" + "Max Cpu Usage   : " + masofonAlias.masofon.Instance.gi_max_cpu_usage.ToString() + "\r" + "\n";
                if (arstct_reports_db.main_cpu_usage > masofonAlias.masofon.Instance.gi_max_cpu_usage)
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg(is_title, "", "stopsign!", is_msg_TRY_LATER);
                    ls_message_text += "MAIN DataBase Is Too Busy To Connect This Report In." + "\r" + "\n";
                    ls_message_text += uf_set_none_db(ref arstct_reports_db);
                }
                else
                {
                    arstct_reports_db.status = "MAIN";
                    arstct_reports_db.report_tran = masofonAlias.masofon.Instance.sqlca;
                    arstct_reports_db.run_datetime = masofonAlias.masofon.Instance.set_machine_time();
                    arstct_reports_db.update_datetime = arstct_reports_db.run_datetime;
                }
            }
            ls_message_text += uf_get_status_returned(arstct_reports_db);
            return FuncResults.Return(arstct_reports_db,ls_message_text);
        }
        private string uf_get_status_returned(s_reports_db astct_reports_db) // uf_get_status_returned(astct_reports_db)
        {
            string ls_server_name = null;
            if (astct_reports_db.report_tran != null)
            {
                if (!(astct_reports_db.report_tran == null))
                {
                    if (astct_reports_db.status == "SPLIT")
                    {
                        
                        
                        ls_server_name = " (" + Convert.ToString(masofonAlias.masofon.Instance.sqlca.ServerName) + "-" + Convert.ToString(astct_reports_db.report_tran.ServerName) + ")";
                    }
                    else
                    {
                        
                         ls_server_name = " (" + Convert.ToString(astct_reports_db.report_tran.ServerName) + ")";
                    }
                }
            }
            return "Status Returned : " + astct_reports_db.status + ls_server_name + "." + "\r" + "\n";
        }
        public async Task uf_read_reports_db_parms_gds() // Lyoha 06.06.06 Read report db connection parameters from db instead of registry
        {
            bool lb_same_db_parms = false;
            int li_packet_size = 0;
            int li_new_packet_size = 0;
            long ll_start = 0;
            long ll_end = 0;
            string ls_main_profile = null;
            string ls_reports_profile = null;
            string ls_new_reg_entery = null;
            string ls_message_text = null;
            string ls_database_type = null;
            string ls_conn_values = null;
            string ls_new_packet_size = null;
            ls_message_text = "";
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
            {
                ls_message_text = "Going To Read Reports DataBase Connection Configuration." + "\r" + "\n";
            }
            ls_main_profile = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key, "database_profile", "SQLCA", "DB Default Profile");
            if (masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<int>(0, "report_db_active") != 1)
            {
                ls_message_text += "Reports DataBase Is Not Set." + "\r" + "\n";
                goto Main_Connected;
            }
            else
            {
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 3)
                {
                    ls_message_text += "MAIN Profile = '" + ls_main_profile.ToString() + "' ,Reports Profile = '" + ls_reports_profile.ToString() + "'." + "\r" + "\n";
                }
            }
            
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = Convert.ToString(masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<string>(0, "report_db_servername"));
            
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.Database = Convert.ToString(masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<string>(0, "report_db_database"));
            
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.DBMS = Convert.ToString(masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<string>(0, "report_db_dbms"));
            
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.LogID = Convert.ToString(masofonAlias.masofon.Instance.gds_station_hardware_parameters.GetItemValue<string>(0, "report_db_userid"));
            // TODO: Field 'LogPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.LogPass = Convert.ToString(masofonAlias.masofon.Instance.gds_station_hardware_parameters.GetItemValue<string>(0, "report_db_password"));
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.AutoCommit = true;
            
             
            masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<string>(0, "report_db_dbparm"));
            
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() == null)
            {
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = "";
            }
             
            ls_database_type = masofonAlias.masofon.Instance.gds_global_branch_parameters.GetItemValue<string>(0, "report_db_type").ToLower(CultureInfo.InvariantCulture);
            if (ls_database_type != "ase" && (ls_database_type != "asa" && ls_database_type != "sql"))
            {
                ls_message_text += "DataBase Type Not Configured In Program Registry, (ASA, ASE, SQL)." + "\r" + "\n";
                goto Main_Connected;
            }
            
            
            if (string.IsNullOrEmpty(masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString()) && masofonAlias.masofon.Instance.SQLCA_REPORTS.DBMS.ToUpper() == "ODBC")
            {
                
                
                lb_same_db_parms = (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() == masofonAlias.masofon.Instance.sqlca.DBParm.ToString());
                if (ll_start > 0)
                {
                    ll_start += 4;
                    
                    ll_end = (long)masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.IndexOf(";", (int)ll_start + 1, StringComparison.Ordinal);
                    if (ll_end == 0)
                    {
                        
                        ll_end = (long)masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.IndexOf("'", (int)ll_start + 1, StringComparison.Ordinal);
                    }
                    if (ll_end > ll_start)
                    {
                        
                        
                        
                        masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName = masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(Convert.ToInt32(ll_start) - 1, masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - Convert.ToInt32(ll_end));
                    }
                }
            }
            else
            {
                
                
                
                
                lb_same_db_parms = (masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() == masofonAlias.masofon.Instance.sqlca.ServerName.ToString() && masofonAlias.masofon.Instance.SQLCA_REPORTS.Database.ToString() == masofonAlias.masofon.Instance.sqlca.Database.ToString());
            }
            li_packet_size = Convert.ToInt32(RegToDatabase.GetRegistryItem(ls_new_reg_entery, "PacketSize", "1024", "Network Packet Size, Only For ASE or MS SQL"));
            li_new_packet_size = li_packet_size;
            if (li_packet_size % 256 != 0)
            {
                li_new_packet_size = li_packet_size / 256 * 256;
            }
            if (li_new_packet_size < 256)
            {
                li_new_packet_size = 1024;
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_packet_size % 256 != 0)
            {
                masofonAlias.masofon.Instance.gs_vars.reports_packet_size = Convert.ToInt32(masofonAlias.masofon.Instance.gs_vars.reports_packet_size / 256) * 256;
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_packet_size > 0 && masofonAlias.masofon.Instance.gs_vars.reports_packet_size != li_new_packet_size)
            {
                li_new_packet_size = (int)masofonAlias.masofon.Instance.gs_vars.reports_packet_size;
            }
            if (li_new_packet_size != li_packet_size)
            {
                ls_new_packet_size = li_new_packet_size.ToString();
                RegToDatabase.RegistrySet(ls_new_reg_entery, "PacketSize", (RegistryValueType.RegString), ls_new_packet_size);
                ls_message_text += "Packet Size Changed From " + li_packet_size.ToString() + " To " + ls_new_packet_size + "." + "\r" + "\n";
                li_packet_size = li_new_packet_size;
            }
            if (ls_database_type == "asa")
            {
                
                
                if (masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - 1, default(int)).ToString() != "'")
                {
                    
                    masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm += "'";
                }
                
                
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Substring(0, masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.Length - 1).ToString() + ";Con='REPORTS_" + is_branch_station + "''");
            }
            else if (ls_database_type == "ase")
            {
                
                
                masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm = Convert.ToString(masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() + ",AppName='REPORTS_" + is_branch_station + "',PacketSize=" + li_packet_size.ToString());
            }
            
            
            
            
            
            
            
            
            ls_conn_values = "Main ServerName = '" + masofonAlias.masofon.Instance.sqlca.ServerName.ToString() + "'" + " ,Reports ServerName = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString() + "' Main Database = '" + masofonAlias.masofon.Instance.sqlca.Database.ToString() + "'" + " ,Reports Database = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.Database.ToString() + "' Main DbParm = '" + masofonAlias.masofon.Instance.sqlca.DBParm.ToString() + "'" + " ,Reports DbParm = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.DBParm.ToString() + "' Main UserName = '" + masofonAlias.masofon.Instance.sqlca.LogID.ToString() + "'" + " ,Reports UserName = '" + masofonAlias.masofon.Instance.SQLCA_REPORTS.LogID.ToString() + "'." + "\r" + "\n";
            if (lb_same_db_parms)
            {
                ls_message_text += "One Of The Connection Parameters Are Same For Reports And Main Databases." + "\r" + "\n" + ls_conn_values;
                goto Main_Connected;
            }
            else if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 1)
            {
                ls_message_text += ls_conn_values;
            }
            ib_reports_db_active = true;
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
            {
                ls_message_text += "Read Connection Configuration To Reports DataBase Successeded." + "\r" + "\n";
            }
            await uf_write_log_file("uf_read_reports_db_parms()", ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_read_reports_db_parms()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return;
        Main_Connected:
            ib_reports_db_active = false;
            ls_message_text += "Error In Connection Configuration To Reports DataBase." + "\r" + "\n";
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "uf_read_reports_db_parms()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            if (masofonAlias.masofon.Instance.SQLCA_REPORTS != null)
            {

            }
            await uf_write_log_file("uf_read_reports_db_parms()", ls_message_text);
            await masofonAlias.masofon.Instance.guo_msg.uf_msg("uf_read_reports_db_parms()", "", "stopsign!", ls_message_text);
        }
        private async Task<long> uf_load_repots_db() // uf_load_repots_db()
        {
            long ll_count_rows = 0;
            long? ll_find = 0;
            long ll_rep_timer_load_db = 0;
            string ls_title = null;
            string ls_msg = null;
            ls_title = "uf_load_repots_db(" + il_rep_timer_load_db.ToString() + ")";
            if (ib_lock_ds)
            {
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
                {
                    await uf_write_log_file(ls_title, "Ds is Lock, Return, Try Next Time.");
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, "Ds is Lock, Return, Try Next Time.");
                }
                return 0;
            }
            this.Stop();
            if (!(ids_repots_db != null))
            {
                return -1;
            }
            if (ids_repots_db == null || il_params_count < 0)
            {
                return -1;
            }
            
            
            il_params_count = ids_repots_db.Retrieve(default(object));
            ls_msg = "Retrieve Reports Ds Rows : " + il_params_count.ToString();
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
            {
                await uf_write_log_file(ls_title, ls_msg);
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
            }
            if (il_params_count < 0)
            {


                return il_params_count;
            }
            if (il_params_count > 0)
            {
                
                ll_find = ids_repots_db.Find("(serial_number == 0) AND (Lower(report_name) == \"timer\")", 0, (int)il_params_count);
                if (ll_find >= 0)
                {
                    
                    ll_rep_timer_load_db = ids_repots_db.GetItemValue<long>((int)ll_find, "max_delay");
                    if (ll_rep_timer_load_db == 0)
                    {
                        ll_rep_timer_load_db = 0;
                    }
                    ls_msg = "Read Reports Timer :" + ll_rep_timer_load_db.ToString();
                    if (ll_rep_timer_load_db == il_rep_timer_load_db)
                    {
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
                        {
                            await uf_write_log_file(ls_title, ls_msg);
                        }
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
                        {
                            await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
                        }
                    }
                    else
                    {
                        il_rep_timer_load_db = ll_rep_timer_load_db;
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 1)
                        {
                            await uf_write_log_file(ls_title, ls_msg);
                        }
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                        {
                            await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
                        }
                    }
                    if (il_rep_timer_load_db > 0)
                    {
                        this.Interval = (int)il_rep_timer_load_db;
                        this.Start();
                    }
                    
                    il_ins_rep_names = ids_repots_db.GetItemValue<long>((int)ll_find, "only_reports_db");
                    if (il_ins_rep_names == 0)
                    {
                        il_ins_rep_names = 0;
                    }
                    
                    ii_imm_load = (int)ids_repots_db.GetItemValue<int>((int)ll_find, "report_type");
                    if (ii_imm_load == 0)
                    {
                        ii_imm_load = 0;
                    }
                }
                else
                {
                    ls_msg = "Timer Row Not Found : " + ll_find.ToString();
                    if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 4)
                    {
                        await uf_write_log_file(ls_title, ls_msg);
                    }
                    if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 4)
                    {
                        await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
                    }
                }
            }
            return il_params_count;
        }
        private async Task<long> uf_get_db_parm(string as_report_name) // uf_get_db_parm(as_report_name)
        {
            long? ll_find = 0;
            long ll_must_repdb = 0;
            string ls_title = null;
            string ls_msg = null;
            if (!(ids_repots_db != null))
            {
                return -1;
            }
            if (ids_repots_db == null || (il_params_count < 0 || il_rep_timer_load_db == -1))
            {
                return -1;
            }
            as_report_name = as_report_name.ToLower(CultureInfo.InvariantCulture);
            ls_title = "uf_get_db_parm(" + as_report_name + ")";
            if (ii_imm_load == 1)
            {
                LoadData2(as_report_name, ref ll_must_repdb);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {
                    if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 1)
                    {

                        await uf_write_log_file(ls_title, masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    }
                    if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                    {

                        await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    }
                    return -1;
                }

                else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
                {
                    ls_msg = as_report_name + " Report Not Found In reports_connections Table.";
                    ll_find = 0;
                }
                else
                {
                    if (il_max_delay == 0)
                    {
                        il_max_delay = 0;
                    }
                    ib_must_repdb = (ll_must_repdb == 1);
                    if (ii_tran_on_split == 0)
                    {
                        ii_tran_on_split = 0;
                    }
                    ls_msg = "Report Table Parm Found : '" + as_report_name + "' (" + il_max_delay.ToString() + "," + ii_tran_on_split.ToString() + "," + ib_must_repdb.ToString() + ")";
                    ll_find = 1;
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 2)
                {
                    await uf_write_log_file(ls_title, ls_msg);
                }
                if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
                {
                    await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
                }
                return ll_find.Value;
            }
            ib_lock_ds = true;
            
            ll_find = ids_repots_db.Find("report_name == \"" + as_report_name + "\"", 1, (int)il_params_count);
            if (ll_find > 0)
            {
                
                il_max_delay = ids_repots_db.GetItemValue<long>((int)ll_find, "max_delay");
                if (il_max_delay == 0)
                {
                    il_max_delay = 0;
                }
                
                ib_must_repdb = (ids_repots_db.GetItemValue<int>((int)ll_find, "only_reports_db") == 1);
                
                ii_tran_on_split = (int)ids_repots_db.GetItemValue<int>((int)ll_find, "tran_ret_on_split");
                if (ii_tran_on_split == 0)
                {
                    ii_tran_on_split = 0;
                }
            }
            ib_lock_ds = false;
            if (ll_find > 0)
            {
                ls_msg = "Report DataStore Parm Found : '" + as_report_name + "' (" + il_max_delay.ToString() + "," + ii_tran_on_split.ToString() + "," + ib_must_repdb.ToString() + ")";
            }
            else
            {
                ls_msg = as_report_name + " Report Not Found In reports_connections DataStore.";
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 2)
            {
                await uf_write_log_file(ls_title, ls_msg);
            }
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.sqlca, false, ls_msg);
            }
            return ll_find.Value;
        }
        public async Task<s_reports_db> uf_get_db_transaction(string as_report_name, DateTime? adt_from_datetime, DateTime? adt_to_datetime, bool ab_must_report_db) // uf_get_db_transaction(as_report_name, adt_from_datetime, adt_to_datetime, ab_must_report_db, ai_tran_on_split)
        {
            int ai_tran_on_split = 0;
            long ll_find = 0;
            string ls_message_text = null;
            string ls_msg = null;
            string ls_title = null;
            s_reports_db lstct_reports_db = default(s_reports_db);
            if (as_report_name == null)
            {
                as_report_name = is_unknown;
            }
            ls_title = "(" + as_report_name + "," + adt_from_datetime.Value.ToString(is_date_format) + "," + adt_to_datetime.Value.ToString(is_date_format) + "," + ab_must_report_db.ToString() + "," + ai_tran_on_split.ToString() + ")";
            ll_find =await uf_get_db_parm(as_report_name);
            if (ll_find == 0)
            {
                uf_insert_report(as_report_name, 3, -1, ab_must_report_db, ai_tran_on_split);
            }
            else if (ll_find > 0)
            {
                ab_must_report_db = ib_must_repdb;
                ai_tran_on_split = ii_tran_on_split;
                ls_title += " --> (" + ab_must_report_db.ToString() + "," + ai_tran_on_split.ToString() + ")";
            }
            ls_msg =await  uf_check_reports_db( lstct_reports_db);
            ls_message_text += is_message_text;
            if (ls_msg == "Error")
            {
                goto Main_Connect;
            }
            if (isempty_datetimeClass.isempty_datetime(adt_from_datetime) && isempty_datetimeClass.isempty_datetime(adt_to_datetime))
            {
                ls_message_text += "Dates Are Both Null." + "\r" + "\n";
                goto Main_Connect;
            }
            else if (isempty_datetimeClass.isempty_datetime(adt_from_datetime))
            {
                adt_from_datetime = Convert.ToDateTime("01/01/1980").Add(Convert.ToDateTime("00:00:00").TimeOfDay);
                ls_message_text += "From DateTime? Reset To '" + adt_from_datetime.Value.ToString(is_date_format) + "'" + "\r" + "\n";
            }
            else if (isempty_datetimeClass.isempty_datetime(adt_to_datetime))
            {
                adt_to_datetime = lstct_reports_db.run_datetime;
                ls_message_text += "To DateTime? Reset To '" + adt_to_datetime.Value.ToString(is_date_format) + "'" + "\r" + "\n";
            }
            if (adt_to_datetime < adt_from_datetime)
            {
                ls_message_text += "'To DateTime' Is Greather Than 'From DateTime'." + "\r" + "\n";
                goto Main_Connect;
            }
            if (lstct_reports_db.update_datetime < adt_from_datetime)
            {
                ls_message_text += "Reports DataBase Is Not Update Yet To Run This Report." + "\r" + "\n";
                goto Main_Connect;
            }
            else if (lstct_reports_db.update_datetime >= adt_from_datetime && lstct_reports_db.update_datetime <= adt_to_datetime)
            {
                ls_message_text += "Transaction Type On Split '" + ai_tran_on_split.ToString() + "'." + "\r" + "\n";
                switch (ai_tran_on_split)
                {
                    case 1:
                        if (ab_must_report_db)
                        {
                            lstct_reports_db.status = "SPLIT";
                        }
                        else
                        {
                            goto Main_Connect;
                        }
                        break;
                    case 2:
                        lstct_reports_db.status = "REP";
                        break;
                    default:
                        lstct_reports_db.status = "SPLIT";
                        break;
                }
            }
            else if (lstct_reports_db.update_datetime > adt_to_datetime)
            {
                lstct_reports_db.status = "REP";
            }
            lstct_reports_db.report_tran = masofonAlias.masofon.Instance.SQLCA_REPORTS;
            ls_message_text += uf_get_status_returned(lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        Main_Connect:
            ls_message_text += (await  uf_set_main_db(lstct_reports_db, ab_must_report_db)).Retrieve(out lstct_reports_db);
            await uf_write_log_file(ls_title, ls_message_text);
            if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 2)
            {
                await f_write_db_errorClass.f_write_db_error("nvo_reports_db", ls_title, masofonAlias.masofon.Instance.SQLCA_REPORTS, false, ls_message_text);
            }
            return lstct_reports_db;
        }
        private void uf_insert_report(string as_report_name, int ai_report_type, long al_max_delay, bool ab_only_repdb, int ai_tran_on_split) // uf_insert_report(as_report_name, ai_report_type, al_max_delay, ab_only_repdb, ai_tran_on_split)
        {
            int li_must_db = 0;
            int li_by_dates = 0;
            if (!(ids_repots_db != null))
            {
                return;
            }
            if (ids_repots_db == null || (il_params_count <= 0 || (il_ins_rep_names == 0 || il_rep_timer_load_db == -1)))
            {
                return;
            }
            if (ab_only_repdb)
            {
                li_must_db = 1;
            }
            else
            {
                li_must_db = 0;
            }
            as_report_name = as_report_name.ToLower(CultureInfo.InvariantCulture);
            UpdateData(as_report_name, ai_report_type, al_max_delay, li_must_db, ai_tran_on_split);
        }
        public void uf_create_rep_con_tab() //create table dbo.reports_connections (serial_number int not null, report_name varchar(50) not null, report_type int not null, max_delay int not null, only_reports_db bit not null, tran_ret_on_split int not null, description varchar(100) null) ;
        {
            //alter table dbo.reports_connections add constraint reports_connections_pk primary key nonclustered (serial_number) ;
            //create unique nonclustered index reports_conn_name on dbo.reports_connections (report_name) ;
            //create nonclustered index reports_conn_type on dbo.reports_connections (report_type) ;
            //
        }
        public nvo_reports_db()
        {
           
        }

        public async Task constructor()
        {
            int li_return_code = 0;
            is_title =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בהרצת הדו'ח");
            is_msg_TRY_LATER = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עקב עומס זמני בשרת לא ניתן להפיק את הדו'ח כעת. אנא נסו מאוחר יותר");
            is_msg_OVER_DELAY =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("דוח זה מוגדר לריצה על שרת דוחות בלבד");
            is_msg_OVER_DELAY += ". " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עקב פיגור יתר או תקלה טכנית בשרת לא ניתן להתחבר לשרת דוחות כעת");
            is_msg_OVER_DELAY += ". " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אנא נסו להתחבר לדוח זה מאוחר יותר") + ".";
            is_branch_station = String.Concat(masofonAlias.masofon.Instance.gs_vars.branch_number, "000") + "_" + String.Concat(masofonAlias.masofon.Instance.gs_station.station_number, "00");
            if (masofonAlias.masofon.Instance.gs_vars.reports_is_available)
            {


                masofonAlias.masofon.Instance.SQLCA_REPORTS = new SQLError();
                il_db_version = f_get_db_versionClass.f_get_db_version();
                if (il_db_version <= 153)
                {
                    await uf_read_reports_db_parms();
                }
                else
                {
                    await uf_read_reports_db_parms_gds();
                }
                if (ib_reports_db_active)
                {
                    li_return_code =await uf_connect_reports_db(true);
                    if (li_return_code == 0)
                    {
                        await uf_get_main_cpu_usage(true);
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level >= 1)
                        {
                            await uf_write_log_file("Constractor()", "Check Cpu Usage : " + masofonAlias.masofon.Instance.gs_vars.reports_check_main_db_cpu.ToString());
                            await uf_write_log_file("Constractor()", "Reports Log Debug Level : " + masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level.ToString());
                            await uf_write_log_file("Constractor()", "Reports DB Debug Level : " + masofonAlias.masofon.Instance.gs_vars.reports_log_db_level.ToString());
                        }
                        if (masofonAlias.masofon.Instance.gs_vars.reports_log_db_level >= 1)
                        {
                            await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "Constractor()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, "Check Cpu Usage : " + masofonAlias.masofon.Instance.gs_vars.reports_check_main_db_cpu.ToString());
                            await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "Constractor()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, "Reports Log Debug Level : " + masofonAlias.masofon.Instance.gs_vars.reports_log_debug_level.ToString());
                            await f_write_db_errorClass.f_write_db_error("nvo_reports_db", "Constractor()", masofonAlias.masofon.Instance.SQLCA_REPORTS, false, "Reports DB Debug Level : " + masofonAlias.masofon.Instance.gs_vars.reports_log_db_level.ToString());
                        }
                    }
                }
                ids_repots_db = new d_reports_connRepository();
                await uf_load_repots_db();
            }
        }

        public async Task timer()
        {
            await uf_load_repots_db();
        }
        public void LoadData(ref long ll_cpu_usage)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT 100 - convert(int, idle AND 100 / (cpu + io + idle)) FROM dbrepl..last_mon WHERE dt = (SELECT Max(dt) FROM dbrepl..last_mon WHERE seconds IS NOT NULL) AND seconds IS NOT NULL";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        ll_cpu_usage = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }

        }
        public void LoadData1(s_reports_db arstrct_reports_db, ref long ll_delay_minutes)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT cur_date,  DateDiff(minute, cur_date, GetDate()),  GetDate() FROM rep_dummy";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        arstrct_reports_db.update_datetime = UnitOfWork.GetValue(resultRS, 0, Convert.ToDateTime);
                        ll_delay_minutes = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        arstrct_reports_db.run_datetime = UnitOfWork.GetValue(resultRS, 2, Convert.ToDateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(string as_report_name, ref long ll_must_repdb)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_report_name } };
                    string sql = "SELECT max_delay,  only_reports_db,  tran_ret_on_split FROM reports_connections WHERE report_name = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        il_max_delay = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ll_must_repdb = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        ii_tran_on_split = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt32);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(string as_report_name, int ai_report_type, Int64 al_max_delay, int li_must_db, int ai_tran_on_split)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_report_name }, { "@1", ai_report_type }, { "@2", al_max_delay }, { "@3", li_must_db }, { "@4", ai_tran_on_split } };
                    string sql = "INSERT INTO reports_connections (serial_number,  report_name,  report_type,  max_delay,  only_reports_db,  tran_ret_on_split,  description) SELECT IsNull(Max(serial_number), 0) + 1,  @0,  @1,  @2,  @3,  @4,  @0 FROM reports_connections GROUP BY reports_connections ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
