using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Web.VisualTree.MVC;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:21:02 (Utc)
    // Comments:        
    // 
    public class w_password_id : wh_password
    {
        public w_password_id()
        {
            int iCurrent = 0;
            VisualElementHelper.CreateFromView<wh_password>("wh_password", "wh_password", "global_global");
        }

        public void LoadData2(long ll_data, ref long ll_employee_number, ref string ls_emp_id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_data }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT number,  id FROM employees WHERE convert(int, id) = @0 AND branch_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_employee_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_emp_id = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
