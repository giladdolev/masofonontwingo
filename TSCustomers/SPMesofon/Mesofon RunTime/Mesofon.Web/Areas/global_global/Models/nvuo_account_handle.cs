﻿using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using masofon;
using Mesofon.Data;
using Mesofon.Models;
using Mesofon.Repository;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using Mesofon.Common;

namespace global
{
    // Creation Time:   08/11/2015 10:20:54 (Utc)
    // Comments:        
    // 

    public class nvuo_account_handle
    {
        public IRepository ids_account_details;
        public IRepository ids_account_header;
        // AlexKh - 1.1.1.18 - 2014-02-19 - CR#1171(SPUCM00004718) - set marlog branch_number
        public long il_account_number;
        public long il_supplier_number;
        public long il_marlog_branch;
        public long ii_closed_status;
        public long il_branch_number;
        public bool ib_account_mode;
        public bool ib_error;
        public string is_account_type;
        public IRepository ids_supplier_distributors;
        public async System.Threading.Tasks.Task<bool> uf_check_material_batch() //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_check_material_batch
            //*Purpose: 			Check if every batch-related item in the document has a batch assigned to it.
            //*Arguments: 		
            //*Return:				Boolean 	- TRUE - Success
            //*									  FALSE - Failed
            //*Date				Programer		Version	Task#	 			Description
            //*-------------------------------------------------------------------------------------------------
            //*28-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            long ll_row = 0;
            long ll_serial = 0;
            long ll_mat = 0;
            long ll_count = 1;
            long ll = 0;
            long ll_rowcount = 0;
            decimal ldec_quantity = default(decimal);
            decimal ldec_batch_count = default(decimal);
            string ls_material_name = null;
            string ls_mess = null;
            string ls_batch = null;
            string[] ls_batch_number = null;
            string ls_material_property = null;
            string ls_no = "n";
            string ls_yes = "y";
            string ls_active = "a";

            for (ll_row = 1; ll_row <= ids_account_details.RowCount(); ll_row++)
            {
                ls_material_property = Convert.ToString(ids_account_details.GetItem<dwh_invoice_details2>((int)ll_row).material_property);
                if ((ls_material_property == "c") || (ls_material_property == "b"))
                {

                    ll_mat = ids_account_details.GetItemValue<long>((int)ll_row, "material_number");

                    ll_serial = ids_account_details.GetItemValue<long>((int)ll_row, "serial_number");


                    ldec_quantity = ids_account_details.GetItemValue<decimal>((int)ll_row, "material_quantity") + ids_account_details.GetItemValue<decimal>((int)ll_row, "bonus_quantity");
                    if (ib_account_mode)
                    {
                        ldec_quantity = ldec_quantity * (-1);
                    }
                    // check if batch is already active
                    //=================================						
                    if (true)
                    {

                    }
                    // Avi Alfassi - 14/09/2005

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_check_material_batch - Sql Error");
                        return false;
                    }
                    //
                    ll = 1;

                    do
                    {
                        LoadData(ref ls_batch, ll_serial, il_marlog_branch, ll_mat);

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
                        {
                            ls_batch_number[ll] = ls_batch;

                            ll++;
                        }
                    }
                    while (masofonAlias.masofon.Instance.sqlca.SqlCode == 0);
                    for (ll = 0; ll <= CollectionExtensions.UBound(ls_batch_number); ll++)
                    {
                        if (!isempty_stringClass.isempty_string(ls_batch_number[ll]))
                        {
                            LoadData1(ls_batch_number, ll_mat, ll, ref ll_count);
                            // Avi Alfassi - 14/09/2005

                            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                            {

                                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_check_material_batch - Sql Error");
                                return false;
                            }
                            //
                            if (ll_count == 0)
                            {
                                ll_count = 0;
                            }
                            if ((ll_count > 0) && (ldec_quantity > 0))
                            {

                                ls_material_name = ids_account_details.GetItemValue<string>(ll_row, "material_name");
                                ls_mess = f_get_error_message_numberClass.f_get_error_message_number(5204) + " - " + ls_material_name;
                                // "Error", "An active batch already exists for the item"
                                await MessageBox.Show(ls_mess, await f_get_error_message_numberClass.f_get_error_message_number(2650));
                                return false;
                            }
                        }
                    }
                    // check quantities
                    //=================
                    if (true)
                    {
                        LoadData2(ll_serial, ll_mat, ref ldec_batch_count);
                    }
                    // Avi Alfassi - 14/09/2005

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "wf_check_material_batch - Sql Error");
                        return false;
                    }
                    //
                    if (ldec_batch_count == 0)
                    {
                        ldec_batch_count = 0;
                    }
                    if ((ldec_batch_count != ldec_quantity) && (ldec_quantity > 0))
                    {

                        ls_material_name = ids_account_details.GetItemValue<string>(ll_row, "material_name");
                        ls_mess = f_get_error_message_numberClass.f_get_error_message_number(5015) + " - " + ls_material_name;
                        // "Notice", "Please insert Batch data for material"
                        await MessageBox.Show(ls_mess, await f_get_error_message_numberClass.f_get_error_message_number(5191));
                        return false;
                    }
                }
            }
            //
            //	updates the material_batch table with materials that have batch numbers.
            //

            ll_rowcount = ids_account_details.RowCount();
            while (ll_count <= ll_rowcount)
            {
                ll_serial = Convert.ToInt64(ids_account_details.GetItem<dwh_invoice_details2>((int)ll_count).serial_number);
                ll_mat = Convert.ToInt64(ids_account_details.GetItem<dwh_invoice_details2>((int)ll_count).material_number);
                UpdateData2(ls_no, ls_active, ll_mat, ll_serial, ls_yes);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {
                    await f_rollbackClass.f_rollback();

                    await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stocks - Sql Error");
                    return false;
                }
                //				

                ll_count++;
            }
            return true;
        }
        public async System.Threading.Tasks.Task<bool> uf_update_stock(long al_stock_number, long al_material_number, decimal adec_quantity, long al_store_number, decimal adec_price) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_stock
            //*Purpose: 			update stock for specific item in account
            //*Arguments: 		Long 		- al_stock_number 	- stock number
            //*						Long 		- al_material_number - material number
            //*						Decimal 	- adec_quantity 		- quantity
            //*						Long 		- al_store_number 	- store number
            //*						Decimal	- adec_price			- price
            //*Return:				Boolean 	- TRUE - Success to update stock.
            //*									  FALSE - Failed to update stock.
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*28-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            decimal ldec_average_value_from_stock = default(decimal);
            decimal ldec_tmp_avg_value = default(decimal);
            decimal ldec_stock_average = default(decimal);
            decimal ldec_average_value = default(decimal);
            decimal ldec_stock_balance = default(decimal);
            decimal ldec_units_amount = default(decimal);
            decimal ldec_quantity_in_package = default(decimal);
            decimal ldec_curr_balance = default(decimal);
            decimal ldec_quantity = default(decimal);
            decimal ldec_price_after_discount = default(decimal);
            decimal ldec_stock_average_value = default(decimal);
            decimal ldec_total_stock_purchase = default(decimal);
            decimal ldec_SAV = default(decimal);
            long ll_stock_a = 0;
            int li_sqlcode = 0;
            ldec_price_after_discount = adec_price;
            //	When we close the stocks - The price needs to be the same as the avg_price
            //	When we close the prices - The price needs to be taken from the invoice
            if (ii_closed_status == 1)
            {
                adec_price = 0;
            }
            ldec_average_value_from_stock = adec_price;
            if (true)
            {
                LoadData3(al_material_number, ref ldec_quantity_in_package);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error ");
                await f_rollbackClass.f_rollback();
                return false;
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0 || ldec_quantity_in_package == 0)
            {
                ldec_quantity_in_package = 1;
            }
            adec_quantity = ldec_quantity_in_package * adec_quantity;
            // Checks if the material exists in this store and stock 


            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {

                await MessageBox.Show(", material_number = " + al_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                await f_rollbackClass.f_rollback();
                return false;
            }
            //	
            if (true)
            {
                LoadData4(ref ldec_stock_balance, ref ldec_stock_average, ref ll_stock_a, il_marlog_branch, al_material_number, al_store_number, al_stock_number);
            }
            if (isempty_stringClass.isempty_string(ldec_stock_balance.ToString()))
            {
                ldec_stock_balance = 0;
            }
            if (isempty_stringClass.isempty_string(ldec_stock_average.ToString()))
            {
                ldec_stock_average = 0;
            }

            li_sqlcode = masofonAlias.masofon.Instance.sqlca.SqlCode;
            if (li_sqlcode == -1)
            {

                await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - SQL - Error");
                await f_rollbackClass.f_rollback();
                return false;
            }
            if (li_sqlcode == 100 && ii_closed_status != 2) // does not exist in stock_control
            {
                //create new stock control row
                if (ldec_average_value_from_stock > 0)
                {
                    ldec_tmp_avg_value = ldec_average_value_from_stock;
                }
                else
                {
                    ldec_tmp_avg_value = 0;
                }
                ldec_stock_average_value = ldec_price_after_discount;
                if (ldec_stock_average_value < 0)
                {
                    ldec_stock_average_value = 0;
                }
                ldec_total_stock_purchase = ldec_stock_average_value * adec_quantity / ldec_quantity_in_package;
                UpdateData5(al_material_number, al_store_number, al_stock_number, ldec_tmp_avg_value, adec_quantity, ldec_total_stock_purchase);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(", material_number = " + al_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
            }
            else
            {
                LoadData5(al_material_number, al_store_number, al_stock_number, ref ldec_total_stock_purchase);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(", material_number = " + al_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
                if (ldec_total_stock_purchase == 0)
                {
                    ldec_total_stock_purchase = 0;
                }
                ldec_total_stock_purchase = ldec_total_stock_purchase / ldec_quantity_in_package;
            }
            // if the quantity is negative, don't update the average value 
            if ((adec_quantity < 0) || (adec_price <= 0))
            {
                ldec_average_value = ldec_stock_average;
            }
            else
            {
                if (ii_closed_status == 2)
                {
                    ldec_units_amount = ldec_stock_balance - adec_quantity;
                }
                else
                {
                    ldec_units_amount = ldec_stock_balance;
                }
                if (ldec_units_amount > 0)
                {
                    ldec_average_value = ((ldec_stock_average * ldec_units_amount) + (adec_quantity * ldec_average_value_from_stock)) / (ldec_units_amount + adec_quantity);
                }
                else
                {
                    ldec_average_value = ldec_average_value_from_stock;
                }
            }
            if (ii_closed_status == 1 || ii_closed_status == 0)
            {
                LoadData6(al_material_number, al_store_number, al_stock_number, ref ldec_curr_balance);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(", material_number = " + al_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }

                else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
                {
                    if (ldec_total_stock_purchase == 0)
                    {
                        ldec_total_stock_purchase = (ldec_curr_balance / ldec_quantity_in_package) * ldec_price_after_discount;
                    }
                }
                ldec_total_stock_purchase += ((adec_quantity / ldec_quantity_in_package) * ldec_price_after_discount);
                if ((ldec_curr_balance + adec_quantity) != 0)
                {
                    ldec_SAV = ldec_total_stock_purchase / ((ldec_curr_balance + adec_quantity) / ldec_quantity_in_package);
                    if (ldec_SAV < 0)
                    {
                        ldec_SAV = 0;
                    }
                }
                else
                {
                    ldec_SAV = 0;
                }
                ldec_quantity = adec_quantity;
                UpdateData6(ldec_quantity, ldec_average_value, ldec_total_stock_purchase, al_material_number, ll_stock_a, al_store_number);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show("material_number = " + al_material_number.ToString() + ", " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
            }
            else
            {
                UpdateData7(ldec_average_value, al_material_number, ll_stock_a, al_store_number);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show(", material_number = " + al_material_number.ToString() + " " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stock - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
            }
            return true;
        }


        public async Task<bool> uf_update_debit(IRepository ads_debit_move, IRepository ads_debit_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_debit
            //*Purpose: 			Insert/Update debit credit for account
            //*Arguments: 		Datastore -	ads_debit_move 	- debit credit header
            //*						Datastore -	ads_debit_details - debit credit details
            //*Return:				Boolean 	- TRUE - Success
            //*									  FALSE - Failed
            //*Date					Programer		Version	Task#	 			Description
            //*-------------------------------------------------------------------------------------------------
            //*28-07-2009			AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            DateTime? ldt_null = default(DateTime);
            long ll_status = 0;
            long ll_debit_number = 0;
            long ll_row = 0;
            long? ll_found = 0;
            long ll_material_number = 0;
            long ll_serial_number = 0;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_doc_number = 0;
            string ls_doc_type = null;
            IRepository lds_debit_move;
            IRepository lds_debit_details;
            // AlexKh - 1.2.45.2 - 2009-11-19 - SPUCM00001670 - parameter dependent don't update stock in marlog branch
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("update_marlog_stock", "boolean", "false", "update stock and prices in marlog .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }

            ldt_null = default(DateTime);

            ll_branch_number = ads_debit_move.GetItemValue<long>(0, "branch_number");

            ll_supplier_number = ads_debit_move.GetItemValue<long>(0, "supplier_number");

            ll_doc_number = ads_debit_move.GetItemValue<long>(0, "parent_doc_number");

            ls_doc_type = ads_debit_move.GetItemValue<string>(0, "parent_doc_type");
            if (!await uf_retrieve_account(ll_branch_number, ll_supplier_number, ll_doc_number, ls_doc_type))
            {
                return false;
            }





            lds_debit_move = new d_debit_credit_moveRepository("", "", "", "", "");

            lds_debit_details = new d_debit_credit_detailsRepository("", "", "", "", "");








            ll_status = ads_debit_move.RowsCopy(1, ads_debit_move.RowCount(), ModelBuffer.Primary, lds_debit_move, 1, ModelBuffer.Primary);




            ll_status = ads_debit_details.RowsCopy(1, ads_debit_details.RowCount(), ModelBuffer.Primary, lds_debit_details, 1, ModelBuffer.Primary);
            //AlexKh comment for masofon
            //ll_debit_number = f_calculate_datatable_rows_by_branch(il_marlog_branch, 115)

            lds_debit_move.SetItem(0, "debit_credit_number", ll_debit_number.ToString());

            lds_debit_move.SetItem(0, "branch_number", il_marlog_branch.ToString());

            lds_debit_move.SetItem(0, "debit_credit_type", "D");

            lds_debit_move.SetItem(0, "parent_doc_type", "A");

            lds_debit_move.SetItem(0, "external_tx_number", "0");

            lds_debit_move.SetItem(0, "b2b_status", "0");


            lds_debit_move.SetItem(0, "debit_credit_total", ((-1) * lds_debit_move.GetItemValue<decimal>(0, "debit_credit_total")).ToString());

            for (ll_row = 1; ll_row <= lds_debit_details.RowCount(); ll_row++)
            {

                lds_debit_details.SetItem(ll_row, "debit_credit_number", ll_debit_number.ToString());

                lds_debit_details.SetItem(ll_row, "branch_number", il_marlog_branch.ToString());

                lds_debit_details.SetItem(ll_row, "debit_credit_type", "D");

                ll_material_number = lds_debit_details.GetItemValue<long>((int)ll_row, "material_number");


                ll_found = ids_account_details.Find("material_number == " + ll_material_number.ToString(), 1, ids_account_details.RowCount());
                if (ll_found > 0)
                {

                    ll_serial_number = ids_account_details.GetItemValue<long>((int)ll_found, "serial_number");

                    lds_debit_details.SetItem(ll_row, "parent_serial_number", ll_serial_number.ToString());
                }
            }
            // Update data into the DataBase
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = lds_debit_move.Update(unitOfWork);
            }
            if (ll_status == -1)
            {
                await MessageBox.Show("Update debit move failed.", "DataBase Error");
                return false;
            }
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = lds_debit_details.Update(unitOfWork);
            }
            if (ll_status == -1)
            {
                await MessageBox.Show("Update debit details failed.", "DataBase Error");
                return false;
            }


            return true;
        }
        public bool uf_update_account_details(GridElement adw_invoice_header, GridElement adw_invoice_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_account_details
            //*Purpose: 			update account details
            //*Arguments: 		Datawindow 		- adw_invoice_header - invoice header
            //*						Datawindow 		- adw_invoice_details - invoice details
            //*Return:				Boolean 	- TRUE - Success to update account details.
            //*									  FALSE - Failed to update account details.
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            long ll_status = 0;
            long ll_material_number = 0;
            long? ll_found = 0;
            long ll_row = 0;
            long ll_serial_number = 0;
            decimal ldec_quantity = default(decimal);
            decimal ldec_bonus_quantity = default(decimal);
            decimal ldec_price = default(decimal);
            decimal ldec_sell = default(decimal);
            decimal ldec_new_price = default(decimal);
            decimal ldec_bonus_discount = default(decimal);
            decimal ldec_discount = default(decimal);
            string ls_state = null;

            ls_state = ids_account_header.GetItemValue<string>(0, "state").ToUpper();
            if (ls_state == "S") //update quantities
            {

                for (ll_row = 1; ll_row <= adw_invoice_details.RowCount(); ll_row++)
                {

                    ll_material_number = adw_invoice_details.GetItemValue<long>(ll_row, "material_number");


                    ll_found = ids_account_details.Find("material_number == " + ll_material_number.ToString(), 1, ids_account_details.RowCount());
                    if (ll_found > 0)
                    {

                        ldec_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_quantity");

                        if (ldec_quantity != ids_account_details.GetItemValue<decimal>((int)ll_found, "material_quantity"))
                        {

                            ldec_bonus_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "bonus_quantity");

                            ids_account_details.SetItem((int)ll_found, "material_quantity", ldec_quantity.ToString());

                            ids_account_details.SetItem((int)ll_found, "bonus_quantity", ldec_bonus_quantity.ToString());

                            ids_account_details.SetItem((int)ll_found, "import_type", Convert.ToString(adw_invoice_details.GetItemValue<string>(ll_row, "import_type")));


                            ids_account_details.SetItem((int)ll_found, "expected_material_quantity", adw_invoice_details.GetItemValue<decimal>(ll_row, "expected_material_quantity").ToString());


                            ids_account_details.SetItem((int)ll_found, "mini_terminal", adw_invoice_details.GetItemValue<decimal>(ll_row, "mini_terminal").ToString());


                            ids_account_details.SetItem((int)ll_found, "packing_serial", adw_invoice_details.GetItemValue<int>(ll_row, "packing_serial").ToString());
                        }
                        // there are additional rows were added to the invoice
                    }
                    else
                    {




                        ll_status = adw_invoice_details.RowsCopy((int)ll_row, (int)ll_row, ModelBuffer.Primary, ids_account_details, ids_account_details.RowCount() + 1, ModelBuffer.Primary);
                        //AlexKh comment for masofon
                        //ll_serial_number = f_calculate_datatable_rows_by_branch(il_marlog_branch, 4) 


                        ids_account_details.SetItem(ids_account_details.RowCount(), "serial_number", ll_serial_number.ToString());


                        ids_account_details.SetItem(ids_account_details.RowCount(), "branch_number", il_marlog_branch.ToString());
                    }
                }
            }
            else if (ls_state == "C") //update prices
            {

                for (ll_row = 1; ll_row <= adw_invoice_details.RowCount(); ll_row++)
                {

                    ll_material_number = adw_invoice_details.GetItemValue<long>(ll_row, "material_number");


                    ll_found = ids_account_details.Find("material_number == " + ll_material_number.ToString(), 1, ids_account_details.RowCount());
                    if (ll_found > 0)
                    {

                        ldec_sell = adw_invoice_details.GetItemValue<decimal>(ll_row, "sell_price"); //..sell price

                        ldec_price = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_price"); //..price before discount

                        ldec_new_price = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_new_price"); //..price after discount

                        ldec_discount = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_discount_percent"); //..material discount

                        ldec_bonus_discount = adw_invoice_details.GetItemValue<decimal>(ll_row, "invoice_details_bonus_discount"); //..bonus discount

                        ldec_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_quantity");

                        ldec_bonus_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "bonus_quantity");
                        if (isempty_stringClass.isempty_string(ldec_bonus_quantity.ToString()))
                        {
                            ldec_bonus_quantity = 0;
                        }
                        if (isempty_stringClass.isempty_string(ldec_quantity.ToString()))
                        {
                            ldec_quantity = 0;
                        }

                        ids_account_details.SetItem((int)ll_found, "sell_price", ldec_sell.ToString());

                        ids_account_details.SetItem((int)ll_found, "material_price", ldec_price.ToString());

                        ids_account_details.SetItem((int)ll_found, "material_new_price", ldec_new_price.ToString());

                        ids_account_details.SetItem((int)ll_found, "material_discount_percent", ldec_discount.ToString());

                        ids_account_details.SetItem((int)ll_found, "invoice_details_bonus_discount", ldec_bonus_discount.ToString());

                        ids_account_details.SetItem((int)ll_found, "branch_sell_price", ldec_sell.ToString());

                        ids_account_details.SetItem((int)ll_found, "current_catalog_sell_price", ldec_sell.ToString());

                        ids_account_details.SetItem((int)ll_found, "material_suppliers_price_before_discount", ldec_price.ToString());
                        if (ldec_bonus_quantity != 0 && ldec_quantity == 0)
                        {

                            ids_account_details.SetItem((int)ll_found, "material_suppliers_price_after_discount", ldec_price.ToString());
                        }
                        else
                        {

                            ids_account_details.SetItem((int)ll_found, "material_suppliers_price_after_discount", ldec_new_price.ToString());
                        }

                        ids_account_details.SetItem((int)ll_found, "material_suppliers_material_discount", ldec_discount.ToString());

                        ids_account_details.SetItem((int)ll_found, "bonus_discount", ldec_bonus_discount.ToString());
                    }
                }
            }

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = ids_account_details.Update(unitOfWork);
            }

            return (ll_status == 1);
        }
        public async Task<bool> uf_update_stocks(GridElement adw_header, GridElement adw_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_stocks
            //*Purpose: 			update stock in marlog branch
            //*Arguments: 		datawindow 		- adw_header - invoice header
            //*						datawindow 		- adw_details - invoice details
            //*Return:				Boolean 	- TRUE - Success to update account stocks.
            //*									  FALSE - Failed to update account stocks.
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            decimal ldec_new_price = default(decimal);
            decimal ldec_quantity = default(decimal);
            decimal ldec_discount = default(decimal);
            decimal ldec_bonus_quantity = default(decimal);
            decimal ldec_all_sons_qty = default(decimal);
            decimal ldec_son_qty_temp = default(decimal);
            decimal[] ldec_son_quan = null;
            decimal[] ldec_son_old_price = null;
            decimal ldec_multiply_percent = default(decimal);
            decimal ldec_new_son_price = default(decimal);
            decimal ldec_parent_old_price = 0;
            long ll_index = 0;
            long ll_material = 0;
            long ll_stock = 0;
            long ll_store = 0;
            long ll_son_number_temp = 0;
            long i = 0;
            long[] ll_son_mat = null;
            bool lb_parent_material = false;
            DateTime? ld_supply_date = default(DateTime);
            // AlexKh - 1.2.45.2 - 2009-11-19 - SPUCM00001670 - parameter dependent don't update stock in marlog branch
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("update_marlog_stock", "boolean", "false", "update stock and prices in marlog .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }
            ib_error = false;


            if (ids_account_details.RowCount() == 0 || ids_account_header.RowCount() == 0)
            {
                return true;
            }

            if (ids_account_header.GetItemValue<string>(0, "state").ToLower(CultureInfo.InvariantCulture) == "s")
            {
                // AlexKh - 1.1.1.18 - 2014-02-18 - CR#1171(SPUCM00004718) - set marlog branch_number
                //MessageBox("nvuo_account_handle.uf_update_stocks - Error", "!סטאטוס חשבון בסניף 999 לא תקין")
                await MessageBox.Show("לא תקין" + il_marlog_branch.ToString() + " סטטוס חשבון בסניף ", "nvuo_account_handle.uf_update_stocks - Error");
                ib_error = true;
                return false;
            }
            // Check if batch was inserted properly (for batch materials only) 
            if (!await uf_check_material_batch())
            {
                // "Notice", "Updating the stock is impossible"
                await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(5202), await f_get_error_message_numberClass.f_get_error_message_number(5191));
                ib_error = true;
                await f_rollbackClass.f_rollback();
                return false;
            }
            if (!uf_update_account_header(adw_header, adw_details))
            {
                // "Notice", "Updating the stock is impossible"
                await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(5202), await f_get_error_message_numberClass.f_get_error_message_number(5191));
                ib_error = true;
                await f_rollbackClass.f_rollback();
                return false;
            }
            if (!uf_update_account_details(adw_header, adw_details))
            {
                // "Notice", "Updating the stock is impossible"
                await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(5202), await f_get_error_message_numberClass.f_get_error_message_number(5191));
                ib_error = true;
                await f_rollbackClass.f_rollback();
                return false;
            }
            // This code updates the quantities of the materials in the stocks

            ll_store = ids_account_header.GetItemValue<long>(0, "store_number");

            ld_supply_date = ids_account_header.GetItemValue<DateTime>(0, "supply_date");

            for (ll_index = 1; ll_index <= ids_account_details.RowCount(); ll_index++)
            {

                ll_material = ids_account_details.GetItemValue<long>((int)ll_index, "material_number");

                ll_stock = ids_account_details.GetItemValue<long>((int)ll_index, "stock_number");

                ldec_new_price = ids_account_details.GetItemValue<decimal>((int)ll_index, "material_new_price");

                ldec_quantity = ids_account_details.GetItemValue<decimal>((int)ll_index, "material_quantity");

                ldec_discount = ids_account_details.GetItemValue<decimal>((int)ll_index, "material_discount_percent");
                lb_parent_material = false;

                ldec_bonus_quantity = ids_account_details.GetItemValue<decimal>((int)ll_index, "bonus_quantity");
                if (isempty_stringClass.isempty_string(ldec_bonus_quantity.ToString()))
                {
                    ldec_bonus_quantity = 0;
                }
                if (isempty_stringClass.isempty_string(ldec_quantity.ToString()))
                {
                    ldec_quantity = 0;
                }
                if (isempty_stringClass.isempty_string(ldec_new_price.ToString()))
                {
                    ldec_new_price = 0;
                }
                // In case we are closing the account => the stock is substructed from account	
                if (ib_account_mode)
                {
                    ldec_quantity = (-1) * ldec_quantity;
                    ldec_bonus_quantity = (-1) * ldec_bonus_quantity;
                }
                // This material is a bonus.
                if ((ldec_bonus_quantity != 0) && (ldec_quantity == 0))
                {

                    ldec_new_price = ids_account_details.GetItemValue<decimal>((int)ll_index, "material_suppliers_price_after_discount");
                    if (isempty_stringClass.isempty_string(ldec_new_price.ToString()))
                    {

                        ldec_new_price = ids_account_details.GetItemValue<decimal>((int)ll_index, "material_new_price");
                    }
                }
                ldec_quantity += ldec_bonus_quantity;

                if (ids_account_details.GetItemValue<string>(ll_index, "import_type").ToLower(CultureInfo.InvariantCulture) == "p")
                {
                    continue;
                }
                //  updates the stocks of the son materials if there are any.  
                if (true)
                {
                    LoadData7(ll_material, ref ldec_all_sons_qty);
                }

                if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                {

                    await MessageBox.Show("materials_tree.number = " + ll_material.ToString() + " " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stocks - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }
                if (ldec_all_sons_qty == 0)
                {
                    ldec_all_sons_qty = 0;
                }
                // if there are sons to this material
                if (ldec_all_sons_qty > 0)
                {
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await MessageBox.Show("materials.number = " + ll_material.ToString() + " " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stocks - Sql Error");
                        await f_rollbackClass.f_rollback();
                        return false;
                    }
                    //
                    lb_parent_material = true;
                    i = 1;

                    do
                    {
                        LoadData8(ref ll_son_number_temp, ref ldec_son_qty_temp, ll_material);

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
                        {
                            ll_son_mat[i] = ll_son_number_temp;
                            ldec_son_quan[i] = ldec_son_qty_temp;

                            i++;
                        }
                    }
                    while (masofonAlias.masofon.Instance.sqlca.SqlCode == 0);
                    for (i = 0; i <= CollectionExtensions.UBound(ll_son_mat); i++)
                    {
                        LoadData9(ll_son_mat, ldec_son_old_price);

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                        {

                            await MessageBox.Show("materials.number = " + ll_son_mat[i].ToString() + " " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_handle.uf_update_stocks - Sql Error");
                            await f_rollbackClass.f_rollback();
                            return false;
                        }
                        //
                        ldec_son_old_price[i] = ldec_son_old_price[i] * ldec_son_quan[i];
                        ldec_parent_old_price += ldec_son_old_price[i];
                    }
                    // Goes over the sons and updates them
                    for (i = 0; i <= CollectionExtensions.UBound(ll_son_mat); i++)
                    {
                        if (ldec_parent_old_price != 0)
                        {
                            ldec_multiply_percent = ldec_son_old_price[i] / ldec_parent_old_price;
                        }
                        else
                        {
                            ldec_multiply_percent = 0;
                        }
                        ldec_new_son_price = (ldec_new_price * ldec_multiply_percent) / ldec_son_quan[i];
                        if ((!(ldec_new_son_price == 0)) || (!(ll_son_mat == null)))
                        {
                            if (!await uf_update_stock(ll_stock, ll_son_mat[i], ldec_son_quan[i] * ldec_quantity, ll_store, ldec_new_son_price))
                            {
                                await MessageBox.Show("!נכשל" + ll_son_mat[i].ToString() + "עדכון מלאי עבור פריט ", "nvuo_account_handle.uf_update_stocks - Error");
                                await f_rollbackClass.f_rollback();
                                return false;
                            }
                        }
                    }
                    ldec_all_sons_qty = 0;
                }
                // If this material doesn't have any sons, update the material itself.
                if (!lb_parent_material)
                {
                    if (!await uf_update_stock(ll_stock, ll_material, ldec_quantity, ll_store, ldec_new_price))
                    {
                        await MessageBox.Show("!נכשל" + ll_son_mat[i].ToString() + "עדכון מלאי עבור פריט ", "nvuo_account_handle.uf_update_stocks - Error");
                        await f_rollbackClass.f_rollback();
                        return false;
                    }
                }
                // Main Loop
            }
            return true;
        }
        public bool uf_update_account_header(GridElement adw_header, GridElement adw_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_account_header
            //*Purpose: 			update account header
            //*Arguments: 		String 		- as_new_state - new invoice state
            //*Return:				Boolean 	- TRUE - Success to update account header.
            //*									  FALSE - Failed to update account header.
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            long ll_status = 0;
            string ls_state = null;
            ls_state = adw_header.GetItemValue<string>(0, "state").ToUpper();
            if (ls_state == "S")
            {

                ids_account_header.SetItem(0, "stock_update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());

                ids_account_header.SetItem(0, "date_move", masofonAlias.masofon.Instance.set_machine_time().ToString());
                ii_closed_status = 1;


                ids_account_header.SetItem(0, "log_book", adw_header.GetItemValue<int>(0, "log_book").ToString());
            }
            if (ls_state == "C")
            {


                ids_account_header.SetItem(0, "discount", adw_header.GetItemValue<decimal>(0, "discount").ToString());


                ids_account_header.SetItem(0, "mam", adw_header.GetItemValue<decimal>(0, "mam").ToString());


                ids_account_header.SetItem(0, "invoice_total", adw_header.GetItemValue<decimal>(0, "invoice_total").ToString());


                ids_account_header.SetItem(0, "discount_percentage", adw_header.GetItemValue<decimal>(0, "discount_percentage").ToString());


                ids_account_header.SetItem(0, "discount_percent", adw_header.GetItemValue<decimal>(0, "discount_percent").ToString());


                ids_account_header.SetItem(0, "supply_date", adw_header.GetItemValue<DateTime>(0, "supply_date").ToString());
            }


            ids_account_header.SetItem(0, "employee_number", adw_header.GetItemValue<int>(0, "employee_number").ToString());

            ids_account_header.SetItem(0, "state", Convert.ToString(ls_state));
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = ids_account_header.Update(unitOfWork);
            }
            return (ll_status == 1);
        }
        public async Task<bool> uf_update_prices(GridElement adw_header, GridElement adw_details) //************************************************************************************************
        {
            //*Function Name:	uf_update_prices
            //*Purpose: 			update prices for account in marlog branch
            //*Arguments: 		datawindow 		- adw_header - invoice header
            //*						datawindow 		- adw_details - invoice details
            //*Return:				Boolean 	- TRUE - Success to update account prices.
            //*									  FALSE - Failed to update account prices.
            //*Date				Programer		Version	Task#	 			Description
            //*-------------------------------------------------------------------------------------------------
            //*02-08-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            // AlexKh - 1.2.45.2 - 2009-11-19 - SPUCM00001670 - parameter dependent don't update stock in marlog branch
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("update_marlog_stock", "boolean", "false", "update stock and prices in marlog .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }
            if (!uf_update_account_header(adw_header, adw_details))
            {
                // "Notice", "Updating the stock is impossible"
                await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(5202), await f_get_error_message_numberClass.f_get_error_message_number(5191));
                ib_error = true;
                await f_rollbackClass.f_rollback();
                return false;
            }
            if (!uf_update_account_details(adw_header, adw_details))
            {
                // "Notice", "Updating the stock is impossible"
                await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(5202), await f_get_error_message_numberClass.f_get_error_message_number(5191));
                ib_error = true;
                await f_rollbackClass.f_rollback();
                return false;
            }
            return true;
        }
        public async Task<bool> uf_close_account(GridElement adw_header, GridElement adw_invoice_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_close_account
            //*Purpose: 			Set status of account to closed
            //*Arguments: 		String 		- as_new_state - new invoice state
            //*Return:				Boolean 	- TRUE - Success to close account.
            //*									  FALSE - Failed to close account.
            //*Date				Programer		Version		Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*04-05-2010		AlexKh			1.2.45.28						Initial version
            //************************************************************************************************

            string ls_state = null;
            long ll_status = 0;
            long ll_material_number = 0;
            long? ll_found = 0;
            long ll_row = 0;
            long ll_serial_number = 0;
            decimal ldec_quantity = default(decimal);
            decimal ldec_bonus_quantity = default(decimal);
            decimal ldec_price = default(decimal);
            decimal ldec_sell = default(decimal);
            decimal ldec_new_price = default(decimal);
            decimal ldec_bonus_discount = default(decimal);
            decimal ldec_discount = default(decimal);

            ids_account_header.SetItem(0, "date_move", masofonAlias.masofon.Instance.set_machine_time().ToString());


            ids_account_header.SetItem(0, "log_book", adw_header.GetItemValue<int>(0, "log_book").ToString());


            ids_account_header.SetItem(0, "discount", adw_header.GetItemValue<decimal>(0, "discount").ToString());


            ids_account_header.SetItem(0, "mam", adw_header.GetItemValue<decimal>(0, "mam").ToString());


            ids_account_header.SetItem(0, "invoice_total", adw_header.GetItemValue<decimal>(0, "invoice_total").ToString());


            ids_account_header.SetItem(0, "discount_percentage", adw_header.GetItemValue<decimal>(0, "discount_percentage").ToString());


            ids_account_header.SetItem(0, "discount_percent", adw_header.GetItemValue<decimal>(0, "discount_percent").ToString());


            ids_account_header.SetItem(0, "supply_date", adw_header.GetItemValue<DateTime>(0, "supply_date").ToString());


            ids_account_header.SetItem(0, "employee_number", adw_header.GetItemValue<int>(0, "employee_number").ToString());

            ids_account_header.SetItem(0, "state", "C");

            for (ll_row = 1; ll_row <= adw_invoice_details.RowCount(); ll_row++)
            {

                ll_material_number = adw_invoice_details.GetItemValue<long>(ll_row, "material_number");


                ll_found = ids_account_details.Find("material_number == " + ll_material_number.ToString(), 1, ids_account_details.RowCount());
                if (ll_found > 0)
                {

                    ldec_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_quantity");

                    if (ldec_quantity != ids_account_details.GetItemValue<decimal>((int)ll_found, "material_quantity"))
                    {

                        ldec_bonus_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "bonus_quantity");

                        ids_account_details.SetItem((int)ll_found, "material_quantity", ldec_quantity.ToString());

                        ids_account_details.SetItem((int)ll_found, "bonus_quantity", ldec_bonus_quantity.ToString());


                        ids_account_details.SetItem((int)ll_found, "import_type", Convert.ToString(adw_invoice_details.GetItemValue<string>(ll_row, "import_type")));


                        ids_account_details.SetItem((int)ll_found, "expected_material_quantity", adw_invoice_details.GetItemValue<decimal>(ll_row, "expected_material_quantity").ToString());


                        ids_account_details.SetItem((int)ll_found, "mini_terminal", adw_invoice_details.GetItemValue<decimal>(ll_row, "mini_terminal").ToString());


                        ids_account_details.SetItem((int)ll_found, "packing_serial", adw_invoice_details.GetItemValue<int>(ll_row, "packing_serial").ToString());
                    }

                    ldec_sell = adw_invoice_details.GetItemValue<decimal>(ll_row, "sell_price"); //..sell price

                    ldec_price = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_price"); //..price before discount

                    ldec_new_price = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_new_price"); //..price after discount

                    ldec_discount = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_discount_percent"); //..material discount

                    ldec_bonus_discount = adw_invoice_details.GetItemValue<decimal>(ll_row, "invoice_details_bonus_discount"); //..bonus discount

                    ldec_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "material_quantity");

                    ldec_bonus_quantity = adw_invoice_details.GetItemValue<decimal>(ll_row, "bonus_quantity");
                    if (isempty_stringClass.isempty_string(ldec_bonus_quantity.ToString()))
                    {
                        ldec_bonus_quantity = 0;
                    }
                    if (isempty_stringClass.isempty_string(ldec_quantity.ToString()))
                    {
                        ldec_quantity = 0;
                    }

                    ids_account_details.SetItem((int)ll_found, "sell_price", ldec_sell.ToString());

                    ids_account_details.SetItem((int)ll_found, "material_price", ldec_price.ToString());

                    ids_account_details.SetItem((int)ll_found, "material_new_price", ldec_new_price.ToString());

                    ids_account_details.SetItem((int)ll_found, "material_discount_percent", ldec_discount.ToString());

                    ids_account_details.SetItem((int)ll_found, "invoice_details_bonus_discount", ldec_bonus_discount.ToString());

                    ids_account_details.SetItem((int)ll_found, "branch_sell_price", ldec_sell.ToString());

                    ids_account_details.SetItem((int)ll_found, "current_catalog_sell_price", ldec_sell.ToString());

                    ids_account_details.SetItem((int)ll_found, "material_suppliers_price_before_discount", ldec_price.ToString());
                    if (ldec_bonus_quantity != 0 && ldec_quantity == 0)
                    {

                        ids_account_details.SetItem((int)ll_found, "material_suppliers_price_after_discount", ldec_price.ToString());
                    }
                    else
                    {

                        ids_account_details.SetItem((int)ll_found, "material_suppliers_price_after_discount", ldec_new_price.ToString());
                    }

                    ids_account_details.SetItem((int)ll_found, "material_suppliers_material_discount", ldec_discount.ToString());

                    ids_account_details.SetItem((int)ll_found, "bonus_discount", ldec_bonus_discount.ToString());
                }
                else
                {
                    // there are additional rows were added to the invoice




                    ll_status = adw_invoice_details.RowsCopy((int)ll_row, (int)ll_row, ModelBuffer.Primary, ids_account_details, ids_account_details.RowCount() + 1, ModelBuffer.Primary);
                    //AlexKh comment for masofon
                    //ll_serial_number = f_calculate_datatable_rows_by_branch(il_marlog_branch, 4) 


                    ids_account_details.SetItem(ids_account_details.RowCount(), "serial_number", ll_serial_number.ToString());


                    ids_account_details.SetItem(ids_account_details.RowCount(), "branch_number", il_marlog_branch.ToString());
                }
            }
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = ids_account_header.Update(unitOfWork);
            }
            if (ll_status == 1)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_status = ids_account_details.Update(unitOfWork);
                }

                if (ll_status != 1)
                {
                    await f_rollbackClass.f_rollback();
                }
            }
            return ll_status == 1;
        }
        public bool uf_check_marlog_distributor(int ai_param, string as_value, long al_supplier_number, long al_distributor_number) //*************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_check_marlog_distributor
            //*Purpose: 			Check if distributor requires update in marlog stock
            //*Arguments: 		integer - al_param
            //*						string  - as_value
            //*						long	  - al_supplier_number
            //*						long	  - al_distributor_number
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version		Task#	 			Description
            //*---------------------------------------------------------------------------
            //*07-10-2010		AlexKh			1.2.46.2	   SPUCM00002208	Initial version
            //***************************************************************************

            long ll_distributor_number = 0;
            long? ll_ret = 0;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            string ls_filter = null;
            string ls_value = null;
            if (!(ids_supplier_distributors != null))
            {


                ids_supplier_distributors = new d_addhock_supplier_distributorsRepository();


            }

            ll_ret = ids_supplier_distributors.Retrieve(ai_param);

            ll_ret = ids_supplier_distributors.SetFilter("");

            ll_ret = ids_supplier_distributors.Filter();
            ll_distributor_number = al_distributor_number;
            // AlexKh - 1.2.48.44 - 2013-03-06 - SPUCM00003402 - handle suppliers without distributors
            //IF IsNull(ll_distributor_number) OR ll_distributor_number = 0 THEN
            if ((ll_distributor_number == 0 || ll_distributor_number == 0) && as_value != "MAAM_NO")
            {
                return false;
            }
            ll_branch_number = masofonAlias.masofon.Instance.gs_vars.branch_number;
            ll_supplier_number = al_supplier_number;
            if (ll_supplier_number == 0 || ll_supplier_number == 0)
            {
                return false;
            }
            ls_filter = "branch_number IN (0, " + ll_branch_number.ToString() + ") AND supplier_number IN (0, " + ll_supplier_number.ToString() + ") AND distributor_number IN (0, " + ll_distributor_number.ToString() + ")";

            ll_ret = ids_supplier_distributors.SetFilter(ls_filter);

            ll_ret = ids_supplier_distributors.Filter();

            ll_ret = ids_supplier_distributors.SetSort("branch_number DESC, supplier_number DESC, distributor_number DESC");

            ll_ret = ids_supplier_distributors.Sort();


            ll_ret = ids_supplier_distributors.Find("distributor_number == " + ll_distributor_number.ToString(), 1, ids_supplier_distributors.RowCount());
            if (ll_ret <= 0)
            {


                ll_ret = ids_supplier_distributors.Find("distributor_number == 0", 1, ids_supplier_distributors.RowCount());
            }
            if (ll_ret > 0)
            {

                ls_value = ids_supplier_distributors.GetItemValue<string>(ll_ret.Value, "value");
                if (ls_value == as_value)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> uf_update_debit_okef(GridElement adw_debit_move, GridElement adw_debit_details) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_debit
            //*Purpose: 			Insert debit credit okef
            //*Arguments: 		datawindow -	ads_debit_move 	- debit credit header
            //*						datawindow -	ads_debit_details - debit credit details
            //*Return:				Boolean 	- TRUE - Success
            //*									  FALSE - Failed
            //*Date					Programer		Version		Task#	 		Description
            //*-------------------------------------------------------------------------------------------------
            //*27-12-2011			AlexKh			1.2.48.13	CR#1153		Initial version
            //************************************************************************************************

            DateTime? ldt_null = default(DateTime);
            long ll_status = 0;
            long ll_debit_number = 0;
            long ll_row = 0;
            long ll_found = 0;
            long ll_material_number = 0;
            long ll_serial_number = 0;
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_doc_number = 0;
            string ls_doc_type = null;
            GridElement lds_debit_move;
            GridElement lds_debit_details;
            // AlexKh - 1.2.45.2 - 2009-11-19 - SPUCM00001670 - parameter dependent don't update stock in marlog branch
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("update_marlog_stock", "boolean", "false", "update stock and prices in marlog .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }

            ldt_null = default(DateTime);

            ll_branch_number = adw_debit_move.GetItemValue<long>(0, "branch_number");

            ll_supplier_number = adw_debit_move.GetItemValue<long>(0, "supplier_number");

            ll_debit_number = adw_debit_move.GetItemValue<long>(0, "debit_credit_number");
            ls_doc_type = adw_debit_move.GetItemValue<string>(0, "parent_doc_type");
            //IF NOT uf_retrieve_account(ll_branch_number, ll_supplier_number, ll_doc_number, ls_doc_type) THEN RETURN FALSE





            lds_debit_move =
                VisualElementHelper.CreateFromView<GridElement>("d_debit_credit_move", "d_debit_credit_move",
                    "global_global");

            lds_debit_details = VisualElementHelper.CreateFromView<GridElement>("d_debit_credit_details_okef", "d_debit_credit_details_okef", "global_global");


            ll_status = adw_debit_move.RowsCopy(1, 1, ModelBuffer.Primary, lds_debit_move, 1, ModelBuffer.Primary);




            ll_status = adw_debit_details.RowsCopy(1, adw_debit_details.RowCount(), ModelBuffer.Primary, lds_debit_details, 1, ModelBuffer.Primary);

            lds_debit_move.SetItem(0, "branch_number", il_marlog_branch.ToString());

            lds_debit_move.SetItem(0, "parent_doc_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());

            lds_debit_move.SetItem(0, "debit_credit_type", "F");

            lds_debit_move.SetItem(0, "parent_doc_type", "");

            lds_debit_move.SetItem(0, "external_tx_number", "0");

            lds_debit_move.SetItem(0, "b2b_status", "0");


            lds_debit_move.SetItem(0, "debit_credit_total", ((-1) * lds_debit_move.GetItemValue<decimal>(0, "debit_credit_total")).ToString());

            for (ll_row = 1; ll_row <= lds_debit_details.RowCount(); ll_row++)
            {

                lds_debit_details.SetItem(ll_row, "branch_number", il_marlog_branch.ToString());

                lds_debit_details.SetItem(ll_row, "debit_credit_type", "F");


                lds_debit_details.SetItem(ll_row, "actual_price", ((-1) * lds_debit_details.GetItemValue<decimal>((int)ll_row, "actual_price")).ToString());
            }
            // Update data into the DataBase

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = lds_debit_move.Update(unitOfWork);
            }


            if (ll_status == -1)
            {
                await MessageBox.Show("Update debit move failed.", "DataBase Error");
                return false;
            }

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                ll_status = lds_debit_details.Update(unitOfWork);
            }

            if (ll_status == -1)
            {
                await MessageBox.Show("Update debit details failed.", "DataBase Error");
                return false;
            }
            // AlexKh - 1.1.1.18 - 2014-02-18 - CR#1171(SPUCM00004718) - set marlog branch_number
            //MessageBox("הודעה", " תעודה עוקפת בסניף 999 נשמרה בהצלחה, מספר התעודה " +String( ll_debit_number))
            await MessageBox.Show(" תעודת חיוב עוקפת נשמרה במרלוג בהצלחה, מספר התעודה " + ll_debit_number.ToString(), "הודעה");


            return true;
        }
        public async Task<int> uf_update_account_delete(long al_invoice_number, long al_branch_number, long al_supplier_number, string as_account_type, int al_action_type)
        {
            // Update invoice_move table 
            // arg al_action_type: 0 - delete account, 1 - open account/invoice
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-05-2012		Vika				1.2.45.0	CR#1161			Initial version
            //*************************************************************************************************/
            // Vika 2012-05-21 SPUCM00003580 ביטול חשבונית מרלוג בסניף
            int li_ret = 0;
            long ll_branch_number = 0;
            IRepository lds_account_header;


            lds_account_header = new dwh_invoiceRepository();



            li_ret = (int)lds_account_header.Retrieve(al_supplier_number, al_invoice_number, al_branch_number, as_account_type);
            if (li_ret == 1)
            {
                switch (al_action_type)
                {
                    case 0:
                        // delete account

                        lds_account_header.SetItem(0, "state", "D");

                        lds_account_header.SetItem(0, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());

                        lds_account_header.SetItem(0, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            li_ret = (int)lds_account_header.Update(unitOfWork);
                        }
                        if (li_ret != 1)
                        {
                            await MessageBox.Show("Error updating Database - table invoice_move", "Error");
                            return -1;
                        }
                        break;
                    case 1:
                        // open deleted account/invoice
                        // open deleted account

                        lds_account_header.SetItem(0, "state", "O");

                        lds_account_header.SetItem(0, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());

                        lds_account_header.SetItem(0, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());
                        using (UnitOfWork unitOfWork = new UnitOfWork())
                        {
                            li_ret = (int)lds_account_header.Update(unitOfWork);
                        }
                        if (li_ret != 1)
                        {
                            await MessageBox.Show("Error updating Database - table invoice_move", "Error");
                            return -1;
                        }
                        // open deleted invoice

                        ll_branch_number = lds_account_header.GetItemValue<long>(0, "price_list_number");

                        lds_account_header.Reset();

                        li_ret = (int)lds_account_header.Retrieve(al_supplier_number, al_invoice_number, ll_branch_number, "p");
                        if (li_ret == 1)
                        {

                            lds_account_header.SetItem(0, "state", "O");

                            lds_account_header.SetItem(0, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());

                            lds_account_header.SetItem(0, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());
                            using (UnitOfWork unitOfWork = new UnitOfWork())
                            {
                                li_ret = (int)lds_account_header.Update(unitOfWork);
                            }
                            if (li_ret != 1)
                            {
                                // TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                                await MessageBox.Show("Error updating Database - table invoice_move", "!פתיחת חשבון נכשלה", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return -1;
                            }
                        }
                        else
                        {
                            await MessageBox.Show("Error updating Database - table invoice_move", "!פתיחת חשבון נכשלה");
                            return -1;
                        }
                        break;
                }
            }
            else
            {
                await MessageBox.Show("Error updating Database - table invoice_move", "Error");
                return -1;
            }
            return 1;
        }
        public async Task<bool> uf_update_return() //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_update_return
            //*Purpose: 			Insert/Update retutn document
            //*Arguments: 		datawindow -	adw_return_header - return document header
            //*						datawindow -	adw_return_details - return document details
            //*Return:				Boolean 	- TRUE - Success
            //*									  FALSE - Failed
            //*Date					Programer		Version	Task#	 			Description
            //*-------------------------------------------------------------------------------------------------
            //*28-07-2009			AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************
            try
            {
                long ll_status = 0;
                long ll_invoice_number = 0;
                long ll_row = 0;
                long ll_found = 0;
                long ll_material_number = 0;
                long ll_serial_number = 0;
                long ll_original_return_number = 0;
                IRepository lds_debit_move;
                IRepository lds_debit_details;
                DateTime ldt_date_null = DateTime.Now;
                string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_return_copy", "boolean", "false", "create return copy in marlog .", "gds_find");
                if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
                {
                    return true;
                }

                il_supplier_number = ids_account_header.GetItemValue<long>(0, "supplier_number");

                il_branch_number = ids_account_header.GetItemValue<long>(0, "branch_number");
                ib_account_mode = false;

                ll_original_return_number = ids_account_header.GetItemValue<long>(0, "invoice_number");
                if (ll_original_return_number == 0)
                {
                    ll_original_return_number = 0;
                }
                ids_account_header.SetItem(0, "price_list_number", Convert.ToInt64(ids_account_header.GetItemValue<Int64>(0, "branch_number").ToString()));
                ids_account_header.SetItem(0, "invoice_number", ll_original_return_number);
                ids_account_header.SetItem(0, "branch_number", il_marlog_branch);
                ids_account_header.SetItem(0, "invoice_type", "B");
                ids_account_header.SetItem(0, "state", "O");
                ids_account_header.SetItem(0, "invoice_total", ((-1) * ids_account_header.GetItemValue<decimal>(0, "invoice_total")).ToString());
                ids_account_header.SetItem(0, "order_number", ll_original_return_number.ToString());
                ids_account_header.SetItem(0, "last_update_datetime", ldt_date_null);
                ids_account_header.SetItemStatus(0, 0, ModelBuffer.Primary, ModelAction.Insert);
                for (ll_row = 0; ll_row < ids_account_details.RowCount(); ll_row++)
                {
                    //AlexKh comment for masofon
                    ll_serial_number = await f_calculate_datatable_rows_by_branchClass.f_calculate_datatable_rows_by_branch(il_marlog_branch, 4);
                    ids_account_details.SetItem(ll_row, "serial_number", ll_serial_number);
                    ids_account_details.SetItem(ll_row, "branch_number", il_marlog_branch);
                    ids_account_details.SetItem(ll_row, "invoice_number", ll_original_return_number);
                    ids_account_details.SetItemStatus((int)ll_row, 0, ModelBuffer.Primary, ModelAction.Insert);
                }
                // Update data into the DataBase
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    try
                    {
                        ll_status = ids_account_header.Update(unitOfWork);
                    }
                    catch (Exception ex2)
                    {

                    }
                }
                if (ll_status == -1)
                {
                    await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_header.uf_update_return - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }

                //GalilCS - update flag to delete then insert like source
                foreach (dwh_invoice_details2 item in ids_account_details.GetPrimaryList())
                {
                    if (item.Action == ModelAction.UpdateByFields || item.Action == ModelAction.UpdateByKeys)
                    {
                        item.Action = ModelAction.DeleteThenInsert;
                    }
                }

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_status = ids_account_details.Update(unitOfWork);
                }

                if (ll_status == -1)
                {

                    await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "nvuo_account_header.uf_update_return - Sql Error");
                    await f_rollbackClass.f_rollback();
                    return false;
                }

                ids_account_header.Reset();

                ids_account_details.Reset();
            }
            catch (Exception ex)
            {
                await MessageBox.Show("uf_create_marlog_return_copy 122 " + ex.StackTrace);
            }
            return true;
        }
        public async Task<bool> uf_retrieve_account(long al_branch_number, long al_supplier_number, long al_account_number, string as_account_type) //**********************************************************************************************
        {
            //*Object:				nvuo_account_handle
            //*Function Name:	uf_retrieve_account
            //*Purpose: 			Retrieve account details
            //*Arguments: 		Long 		- al_branch_number 	- branch number
            //*						Long 		- al_supplier_number - supplier number
            //*						Long 		- al_account_number 	- invoice number
            //*						String 	- as_account_type 	- invoice type
            //*Return:				Boolean 	- TRUE - Success to retrieve account.
            //*									  FALSE - Failed to retrieve account.
            //*Date				Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*20-07-2009		AlexKh			1.2.45.0	CR#1121			Initial version
            //************************************************************************************************

            long ll_count = 0;
            long ll_original_branch_number = 0;
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("marlog_return_copy", "boolean", "false", "Create return copy in marlog .", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "false")
            {
                return true;
            }

            ids_account_header.Reset();

            ids_account_details.Reset();
            is_account_type = "B";
            ib_account_mode = false;
            il_supplier_number = al_supplier_number;
            il_account_number = al_account_number;
            il_branch_number = al_branch_number;

            ll_count = ids_account_header.Retrieve(al_account_number, il_branch_number, al_supplier_number, as_account_type);
            if (ll_count != 1)
            {
                await MessageBox.Show("!משיכת כותרת החזרה נכשלה", "nvuo_account_handle.uf_retrieve_account - Error");
                return false;
            }

            ll_count = ids_account_details.Retrieve(il_branch_number, al_supplier_number, al_account_number, as_account_type);
            if (ll_count <= 0)
            {
                await MessageBox.Show("!משיכת תוכן החזרה נכשלה", "nvuo_account_handle.uf_retrieve_account - Error");
                return false;
            }
            return true;
        }
        public nvuo_account_handle()
        {
            constructor();
        }
        public void constructor() // AlexKh - 1.2.45.0 - 2009-07-20 - CR#1121 - create datastores
        {


            ids_account_header = new dwh_invoiceRepository();




            ids_account_details = new dwh_invoice_details2Repository();




            ids_account_details.SetFilter("");

            ids_account_details.Filter();
        }

        public void destructor() // AlexKh - 1.2.45.0 - 2009-07-20 - CR#1121 - destroy datastores
        {
            if (ids_account_header != null)
            {


            }
            if (ids_account_details != null)
            {


            }
        }


        public void UpdateData2(string ls_no, string ls_active, long ll_mat, long ll_serial, string ls_yes)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_no }, { "@1", ls_active }, { "@2", il_marlog_branch }, { "@3", ll_mat }, { "@4", ll_serial }, { "@5", ls_yes } };
                    string sql = "UPDATE material_batch SET material_batch.temp_row = @0,  material_batch.status = @1 WHERE (material_batch.branch_number = @2) AND (material_batch.material_number = @3) AND ((material_batch.packing_detailes_serial_number = @4) OR (material_batch.invoice_detailes_serial_number = @4)) AND (material_batch.temp_row = @5)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData(ref string ls_batch, long ll_serial, long il_marlog_branch, long ll_mat)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_serial }, { "@1", il_marlog_branch }, { "@2", ll_mat } };
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql =
                        "SELECT material_batch.batch_number FROM material_batch WHERE (material_batch.packing_detailes_serial_number = @0 OR material_batch.invoice_detailes_serial_number = @0) AND (material_batch.branch_number = @1) AND (material_batch.material_number = @2)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_batch = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(string[] ls_batch_number, long ll_mat, long ll, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_batch_number[ll] }, { "@1", ll_mat } };
                    string sql = "SELECT SUM(quantity_in - quantity_out) FROM material_batch WHERE (material_batch.batch_number = @0) AND (material_batch.material_number = @1) AND (lower(material_batch.status) = 'a') AND (lower(material_batch.temp_row) <> 'y')";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(long ll_serial, long ll_mat, ref decimal ldec_batch_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_marlog_branch }, { "@1", ll_serial }, { "@2", ll_mat } };
                    string sql = "SELECT SUM(material_batch.quantity_in) FROM material_batch WHERE (material_batch.branch_number = @0) AND (material_batch.packing_detailes_serial_number = @1 OR material_batch.invoice_detailes_serial_number = @1) AND (material_batch.material_number = @2)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_batch_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void UpdateData5(Int64 al_material_number, Int64 al_store_number, Int64 al_stock_number, decimal ldec_tmp_avg_value, decimal adec_quantity, decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_marlog_branch }, { "@1", al_material_number }, { "@2", al_store_number }, { "@3", al_stock_number }, { "@4", ldec_tmp_avg_value }, { "@5", adec_quantity }, { "@6", ldec_total_stock_purchase } };
                    string sql = "INSERT INTO stock_control (branch_number,  number,  store_number,  buy_stock,  sale_stock,  average_value,  stock_balance,  stock_average_value) VALUES (@0,  @1,  @2,  1,  @3,  @4,  @5,  @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData6(decimal ldec_quantity, decimal ldec_average_value, decimal ldec_total_stock_purchase, Int64 al_material_number, long ll_stock_a, Int64 al_store_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_quantity }, { "@1", ldec_average_value }, { "@2", ldec_total_stock_purchase }, { "@3", il_marlog_branch }, { "@4", al_material_number }, { "@5", ll_stock_a }, { "@6", al_store_number } };
                    string sql = "UPDATE stock_control SET stock_balance = (stock_balance + @0),  average_value = @1,  stock_average_value = @2 WHERE (stock_control.branch_number = @3) AND (stock_control.number = @4) AND (stock_control.sale_stock = @5) AND (stock_control.buy_stock = 1) AND (stock_control.store_number = @6)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData7(decimal ldec_average_value, Int64 al_material_number, long ll_stock_a, Int64 al_store_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ldec_average_value }, { "@1", il_marlog_branch }, { "@2", al_material_number }, { "@3", ll_stock_a }, { "@4", al_store_number } };
                    string sql = "UPDATE stock_control SET average_value = @0 WHERE (stock_control.branch_number = @1) AND (stock_control.number = @2) AND (stock_control.sale_stock = @3) AND (stock_control.buy_stock = 1) AND (stock_control.store_number = @4)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(Int64 al_material_number, ref decimal ldec_quantity_in_package)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT quantity_in_package FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_quantity_in_package = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(ref decimal ldec_stock_balance, ref decimal ldec_stock_average, ref long ll_stock_a, long il_marlog_branch, long al_material_number, long al_store_number, long al_stock_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_marlog_branch }, { "@1", al_material_number }, { "@2", al_store_number }, { "@3", al_stock_number } };
                    string sql = "(SELECT stock_control.stock_balance,  stock_control.average_value,  stock_control.sale_stock FROM stock_control WHERE (stock_control.branch_number = @0) AND (stock_control.number = @1) AND (stock_control.store_number = @2) AND (stock_control.sale_stock = @3) AND (stock_control.buy_stock = 1))";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_stock_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        ldec_stock_average = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                        ll_stock_a = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(Int64 al_material_number, Int64 al_store_number, Int64 al_stock_number, ref decimal ldec_total_stock_purchase)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_marlog_branch }, { "@1", al_material_number }, { "@2", al_store_number }, { "@3", al_stock_number } };
                    string sql = "SELECT IsNull(stock_average_value, 0) FROM stock_control WHERE branch_number = @0 AND number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_total_stock_purchase = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(Int64 al_material_number, Int64 al_store_number, Int64 al_stock_number, ref decimal ldec_curr_balance)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", il_marlog_branch }, { "@1", al_material_number }, { "@2", al_store_number }, { "@3", al_stock_number } };
                    string sql = "SELECT SUM(stock_balance) FROM stock_control WHERE stock_control.branch_number = @0 AND stock_control.number = @1 AND store_number = @2 AND sale_stock = @3 AND buy_stock = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_curr_balance = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public void LoadData7(long ll_material, ref decimal ldec_all_sons_qty)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_material } };
                    string sql = "SELECT SUM(materials_tree.quantity) FROM materials_tree WHERE materials_tree.number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_all_sons_qty = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(ref long ll_son_number_temp, ref decimal ldec_son_qty_temp, long ll_material)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_material } };
                    string sql = "(SELECT materials_tree.material_number,  materials_tree.quantity FROM materials,  materials_tree WHERE (materials.number = materials_tree.number) AND (materials.number = @0))";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_son_number_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ldec_son_qty_temp = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(long[] ll_son_mat, decimal[] ldec_son_old_price)
        {
            int i = 0;
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_son_mat[i] } };
                    string sql = "SELECT materials.active_price FROM materials WHERE materials.number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_son_old_price[i] = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
