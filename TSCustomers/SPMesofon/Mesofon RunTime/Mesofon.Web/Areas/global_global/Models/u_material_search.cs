using System.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using mini_terminal;
using Mesofon.Common;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using MvcSite.Common.Global;

namespace global
{
    // Creation Time:   08/11/2015 10:21:00 (Utc)
    // Comments:        
    // 

    public class u_material_search
    {   

        //.. show miniterminial messagebox or regular messagebox
        public bool ib_mt_message;

        public bool ib_show_error_message;

        public async Task<long> uf_count_external_barcodes(
            string as_external_barcode) // Long wf_count_external_barcodes( String as_external_barcode )
        {
            // gets material number from item_codes
            long ll_count = 0;
            if (isempty_stringClass.isempty_string(as_external_barcode))
            {
                return 0;
            }
            as_external_barcode = StringsEx.StrReverse(as_external_barcode) + "%";
            if (true)
            {
                LoadData(as_external_barcode, ref ll_count);
            }
            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                       await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                ll_count = -1;
            }
            return ll_count;
        }

        public void uf_set_transaction(SQLError a_transaction)
        {
            masofonAlias.masofon.Instance.sqlca = a_transaction;
        }

        public void uf_set_mt_message(bool ab_show_message)
        {
            ib_mt_message = ab_show_message;
        }

        public async Task<string>
            uf_get_barcode(long al_material_number) //.. Long wf_get_barcode( Long al_material_number )
        {
            string ls_material_barcode = null;
            if (al_material_number == 0)
            {
                return "";
            }
            if (true)
            {
                LoadData1(al_material_number, ref ls_material_barcode);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                ls_material_barcode = "-1";
            }
            return ls_material_barcode;
        }

        public async Task<string>
            uf_get_material_name(long al_material_number) //.. Long wf_get_item_name(string as_material_barcode )
        {
            string ls_material_name = null;
            //IF as_item_barcode = "" THEN Return 0
            if (true)
            {
                LoadData2(al_material_number, ref ls_material_name);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                ls_material_name = "";
            }
            return ls_material_name;
        }

        public async Task<long>
            uf_get_material_number(string as_material_barcode) //.. Long wf_get_item_number(string as_material_barcode )
        {
            long ll_material_number = 0;
            if (isempty_stringClass.isempty_string(as_material_barcode))
            {
                return 0;
            }
            if (true)
            {
                LoadData3(as_material_barcode, ref ll_material_number);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                ll_material_number = 0;
            }
            return ll_material_number;
        }

        public bool uf_material_exists(string as_barcode) //.. Boolean wf_material_exists( string as_barcode )
        {
            long ll_material_number = 0;
            if (true)
            {
                LoadData4(as_barcode, ref ll_material_number);
            }

            return (ll_material_number != 0);
        }

        

        public async Task<long> uf_get_material_number_from_external(string as_part_barcode)
        {
            long ll_count = 0;
            long ll_material_number = 0;
            long ll_null = 0;
            string ls_barcode = null;
            string ls_barcode_reverse = null;
            s_array_arguments lstr_parms = new s_array_arguments();
            //.. Check if valid data accepted
            if (string.IsNullOrEmpty(as_part_barcode) || as_part_barcode == "0")
            {
                ll_material_number = 0;
                return ll_material_number;
            }
            ls_barcode_reverse = StringsEx.StrReverse(as_part_barcode);
            ls_barcode_reverse = ls_barcode_reverse + "%";
            //.. IF Exists In Items
            if (true)
            {
                LoadData5(as_part_barcode, ref ll_material_number);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                return -1;
            }
            if (ll_material_number > 0)
            {
                return ll_material_number;
            }
            // If Exists Only One in item_codes
            if (true)
            {
                LoadData6(as_part_barcode, ref ll_count);
            }

            if (masofonAlias.masofon.Instance.sqlca != null &&  masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                return -1;
            }
            if (ll_count == 1)
            {
                LoadData7(as_part_barcode, ref ll_material_number);

                if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                {
                    if (ib_mt_message)
                    {

                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                            "",
                            "Ok", 1,
                           await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                    }
                    else
                    {

                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                            await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                    }
                    return -1;
                }
                return ll_material_number;
            }
            lstr_parms.a_string[1] = ls_barcode_reverse;
            lstr_parms.a_long[1] = ll_null;
            lstr_parms.a_boolean[0] = true;
            //.. Open a Response! window and the user will choose item
            //.. SPR = P0000134

            WindowElement win;
            
            
            if (ib_mt_message)
            {
                win = await WindowHelper.Open<w_mt_materials_codes>("mini_terminal_mini_terminal", "parms", lstr_parms);
            }
            else
            {
                win = await WindowHelper.Open<w_get_item_from_barcode>("mini_terminal_mini_terminal", "parms", lstr_parms);
            }
            //....
             ll_material_number = WindowHelper.GetParam<long>(win);

            if (win != null)
            {
                win.Hide();
                win = null;
            }

            return ll_material_number;
        }

        public async Task<FuncResults<long, string>> uf_get_material_info(string as_material_barcode,
            long al_material_number,
            string as_material_name)
        {
            if (isempty_stringClass.isempty_string(as_material_barcode))
            {
                return FuncResults.Return(al_material_number, as_material_name);
            }
            if (true)
            {
                LoadData8(as_material_barcode, ref al_material_number);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                al_material_number = 0;
                as_material_name = "";
                return FuncResults.Return(al_material_number, as_material_name);
            }
            if (true)
            {
                LoadData9(al_material_number, ref as_material_name);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "",
                        "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                al_material_number = 0;
                as_material_name = "";
                return FuncResults.Return(al_material_number, as_material_name);
            }
            return FuncResults.Return(al_material_number, as_material_name);
        }

        public async Task<FuncResults<long, string, long, object>> uf_get_material_info(string as_material_barcode,
            long al_material_number,
            string as_material_name, long al_min_valid_month)
        {
            if (isempty_stringClass.isempty_string(as_material_barcode))
            {
                return FuncResults.Return(al_material_number, as_material_name, al_min_valid_month, (object)(null));
            }
            if (true)
            {
                LoadData10(as_material_barcode, ref al_material_number);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                    await     masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                al_material_number = 0;
                as_material_name = "";
                return FuncResults.Return(al_material_number, as_material_name, al_min_valid_month, (object)(null));
            }
            if (true)
            {
                LoadData11(al_material_number, ref as_material_name, ref al_min_valid_month);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok",
                        1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,
                        await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                al_material_number = 0;
                as_material_name = "";
                FuncResults.Return(al_material_number, as_material_name, al_min_valid_month);
            }
            return FuncResults.Return(al_material_number, as_material_name, al_min_valid_month, (object)(null));
        }

        public async Task<FuncResults<int[] ,long>> uf_get_items_assigns_to_material(long al_material_number,
            int[] al_item_arr) //--------------------------------------------------------------------
        {
            //Function:			public u_material_search.uf_get_items_assigns_to_material()
            //
            // Returns:         long ---> if error occurce return -1 otherwise 1
            //
            // Parameters:      a) value Long al_material_number ---> search by this material number
            // 					   b) reference Long al_item_arr[] ---> return items assigns to al_material_number
            // 
            // Copyright  - Stas
            //
            // Date Created: 08/08/2005
            //
            // Description:	
            // 						search in trees table for all items assigns to givven material number
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_select = null;

            ls_select = "SELECT item_number FROM trees ";
            ls_select += " WHERE trees.material_number = " + al_material_number.ToString();
            long result = 1;
            string msg = "";
            try
            {
                List<int> list = new List<int>();
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    IDataReader resultRS = unitOfWork.Retrieve(ls_select);
                    while (resultRS.Read())
                    {
                        list.Add(UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32));
                    }
                }
                al_item_arr = list.ToArray();
            }
            catch (Exception ex)
            {
                result = -1;
            }

            if (result == -1)
            {
                if (ib_mt_message)
                {
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("error - Syntax", msg, "", "Ok", 1,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("error - Syntax", msg,                await         masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                
            }
            return FuncResults.Return(al_item_arr, result); ;
        }


        public async Task<decimal> uf_get_material_sell_price(long al_branch_number, long al_material_number) //--------------------------------------------------------------------
        {
            //Function:			public u_material_search.uf_get_material_sell_price()
            //
            // Returns:         decimal		---> return -1 if error occurce otherwise return sell price
            //
            // Parameters:      a) value Long al_branch_number ---> search by current branch number
            // 						b) value Long 	al_material_number---> search by current material number
            // 
            // Copyright  - Stas
            //
            // Date Created: 08/08/2005
            //
            // Description:	
            // 						gets all items assigns to current material number
            //						if items number are more than 1 , search in branch_item_price
            //						only by first one
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            int[] ll_items_numbers_arr = new int[] { };
            long ll_result = 0;
            decimal ldec_sell_price = default(decimal);
            //.. gets all items assigns to this material number
            ll_result = (await uf_get_items_assigns_to_material(al_material_number, ll_items_numbers_arr)).Retrieve(out ll_items_numbers_arr);
            if (ll_result > 0)
            {
                if (CollectionExtensions.UBound(ll_items_numbers_arr) >= 0)
                {
                    LoadData12(al_branch_number, ll_items_numbers_arr, ref ldec_sell_price);

                    if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                    {
                        if (ib_mt_message)
                        {

                            await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                        }
                        else
                        {

                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                        }
                        ldec_sell_price = -1;
                    }
                }
            }
            else
            {
                ldec_sell_price = -1;
            }
            return ldec_sell_price;
        }

        public async Task<FuncResults<int[], long>> uf_get_materials_assigns_to_item(long al_item_number, int[] al_material_arr) //--------------------------------------------------------------------
        {
            //Function:			public u_material_search.uf_get_items_assigns_to_material()
            //
            // Returns:         long ---> if error occurce return -1 otherwise 1
            //
            // Parameters:      a) value Long al_item_number ---> search by this item number
            // 					   b) reference Long al_material_arr[] ---> return materials assigns to al_item_number
            // 
            // Copyright  - Stas
            //
            // Date Created: 11/08/2005
            //
            // Description:	
            // 						search in trees table for all materials assigns to givven item number
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_select = null;

            ls_select = "SELECT material_number FROM trees ";
            ls_select += " WHERE trees.item_number = " + al_item_number.ToString();
            long result = 1;
            string msg = "";
            try
            {
                List<int> list = new List<int>();
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    IDataReader resultRS = unitOfWork.Retrieve(ls_select);
                    while (resultRS.Read())
                    {
                        list.Add(UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32));
                    }
                }
                al_material_arr = list.ToArray();
                return FuncResults.Return(al_material_arr, (long)1);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                result = -1;
            }


            if (result == -1)
            {
                if (ib_mt_message)
                {
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("error - Syntax", msg, "", "Ok", 1,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("error - Syntax", msg,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }

            }
            return FuncResults.Return(al_material_arr, result); ;

        }


        public async Task<FuncResults<long, decimal>> uf_get_material_price(long al_branch_number, long al_supplier_number, long al_material_number, long al_error_number)
        {
            decimal ldec_material_price = default(decimal);
            al_error_number = 0;
            if (true)
            {
                LoadData13(al_branch_number, al_supplier_number, al_material_number, ref ldec_material_price);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofon.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
            }

            al_error_number = masofon.masofon.Instance.sqlca.SqlCode;
            return FuncResults.Return(al_error_number, ldec_material_price);
        }
        public async Task<FuncResults<long, decimal>> uf_get_material_price(long al_branch_number, long al_material_number, long al_error_number) //--------------------------------------------------------------------
        {
            //Function:			public u_material_search.uf_get_material_price()
            //
            // Returns:         Decimal
            //
            // Parameters:      1) value Long al_branch_number ---> search by this branch number
            // 						2) value Long al_material_number -> search by this material number
            //						3) reference Long al_error_number -> if error occurse get -1 value 
            // 
            // Copyright  - Stas
            //
            // Date Created: 11/08/2005
            //
            // Description:	
            // 						gets material price from material_suppliers table.
            //						search by branch number & material number & last supplier
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            al_error_number = 0;
            decimal ldec_material_price = default(decimal);
            string ls_error_syntax = null;
            if (true)
            {
                LoadData14(al_branch_number, al_material_number, ref ldec_material_price);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                ls_error_syntax = " SELECT IsNull(price_before_discount,0.00) INTO :ldec_material_price FROM material_suppliers WHERE branch_number =" + "\t" + al_branch_number.ToString() + " AND material_number" + "\t" + "= " + al_material_number.ToString() + " AND last_supplier" + "\t" + "=1 ";
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText + ls_error_syntax, "", "Ok", 1,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText + ls_error_syntax, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                al_error_number = masofonAlias.masofon.Instance.sqlca.SQLDBCode;
            }
            return FuncResults.Return(al_error_number, ldec_material_price);
        }
        public async Task<string> uf_get_supplier_name(long al_supplier_number)
        {
            string ls_supplier_name = null;
            if (true)
            {
                LoadData15(al_supplier_number, ref ls_supplier_name);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {
                if (ib_mt_message)
                {
                    //.. Error In Db

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1, "^20000");
                }
                else
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText, "^20000");
                }
            }
            return ls_supplier_name;
        }
        public async Task<FuncResults<decimal, decimal, long>> uf_get_material_prices(long al_branch_number, long al_material_number,  decimal adec_price_before_discount,  decimal adec_material_price_after_discount,  long al_error_number) //--------------------------------------------------------------------
        {
            //Function:			public u_material_search.uf_get_material_price()
            //
            // Returns:         Decimal
            //
            // Parameters:      1) value Long al_branch_number ---> search by this branch number
            // 						2) value Long al_material_number -> search by this material number
            //						3) ref 	Decimal	adec_price_before_discount 
            //						4) ref 	Decimal	adec_material_price_after_discount
            //						5) reference Long al_error_number -> if error occurse get -1 value 
            // 
            // Copyright  - Stas
            //
            // Date Created: 21/09/2005
            //
            // Description:	
            // 						gets material price from material_suppliers table.
            //						search by branch number & material number & last supplier
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            al_error_number = 0;
            decimal ldec_material_price = default(decimal);
            string ls_error_syntax = null;
            if (true)
            {
                LoadData16(al_branch_number, al_material_number, ref adec_price_before_discount, ref adec_material_price_after_discount);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                ls_error_syntax = "SELECT IsNull(price_before_discount,0.00)," + "\t" + "IsNull(price_after_discount,0.00) FROM material_suppliers WHERE branch_number" + "\t" + "=" + al_branch_number.ToString() + " AND material_number =" + "\t" + al_material_number.ToString() + "\t" + "AND" + "\t" + "last_supplier" + "\t" + "=1 ";
                if (ib_mt_message)
                {

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofonAlias.masofon.Instance.sqlca.SqlErrText + ls_error_syntax, "", "Ok", 1, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                else if (ib_show_error_message)
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText + ls_error_syntax, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בבסיס נתונים"));
                }
                // TODO: Field 'SQLDBCode of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
                al_error_number = masofonAlias.masofon.Instance.sqlca.SQLDBCode;
            }
            return FuncResults.Return(adec_price_before_discount, adec_material_price_after_discount, al_error_number);
        }
        public void uf_set_show_error_message(bool ab_show_error_message)
        {
            ib_show_error_message = ab_show_error_message;
        }
        public async Task<FuncResults<string,bool>> uf_material_status_allow_purches(long al_material_number,  string as_message)
        {
            char lc_material_status = '\0';
            bool lb_status_allow_purches = false;
            lb_status_allow_purches = true;
            if (true)
            {
                LoadData17(al_material_number, ref lc_material_status);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0 && !isempty_stringClass.isempty_string(lc_material_status.ToString()))
            {
                if (Char.ToLower(lc_material_status, CultureInfo.InvariantCulture) == 'u')
                {
                    as_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("החומר חסום לרכש");
                    lb_status_allow_purches = false;
                }
                else if (Char.ToLower(lc_material_status, CultureInfo.InvariantCulture) == 'd')
                {
                    as_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("החומר מבוטל");
                    lb_status_allow_purches = false;
                }
                else if (Char.ToLower(lc_material_status, CultureInfo.InvariantCulture) == 'e')
                {
                    as_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("החומר שגוי לוגית");
                    lb_status_allow_purches = false;
                }
            }
            return FuncResults.Return (as_message,lb_status_allow_purches);
        }
        public async Task<FuncResults<string,bool>> uf_material_belong_to_current_supplier(long al_branch_number, long al_supplier, long al_material_number, string as_message)
        {
            long ll_material_number = 0;
            long ll_temp_material = 0;
            long ll_last_supplier = 0;
            string ls_last_supplier = null;
            as_message = "";
            // Find out if that material belongs to this supplier 
            if (true)
            {
                LoadData18(al_supplier, al_material_number, ref ll_temp_material);
            }

            if (masofon.masofon.Instance.sqlca.SqlCode == 0)
            {
                return FuncResults.Return(as_message, true);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                LoadData19(al_branch_number, al_material_number, ref ll_last_supplier, ref ls_last_supplier);
                if (al_supplier != ll_last_supplier)
                {
                    as_message = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("החומר נקנה לאחרונה מהספק ") + ls_last_supplier + " (" + ll_last_supplier.ToString() + ")";
                    return FuncResults.Return(as_message, false);
                }
            }
            return FuncResults.Return(as_message, true);
        }
        public async Task<string> uf_get_barcode_main(string as_item_barcode, bool ab_display_message) // wf_get_barcode_main( String as_material_barcode )
        {
            // gets barcode or some baroce part  and returns items barcode if exists
            bool lb_Barcode_Exists = false;
            bool lb_External_Barcode_Exists = false;
            long ll_material_number = 0;
            //.. check if Barcode exists
            lb_Barcode_Exists = uf_material_exists(as_item_barcode);
            //.. if Barcode NOT exists (and not New): check if External Barcode exists (item_codes)
            if (!lb_Barcode_Exists)
            {
                lb_External_Barcode_Exists = (await uf_count_external_barcodes(as_item_barcode) > 0);
            }
            if (!lb_Barcode_Exists && !lb_External_Barcode_Exists)
            {
                //..item not exists
                // PninaSG - 1.2.23.0 - 2008-01-02- TASK#10000 - Start
                //If function was called from the mini terminal window then do not display any message
                //	IF ib_mt_message THEN
                //		f_mini_terminal_messagebox("", "", "" ,"Ok",1, f_get_error_message_number(5092) )
                //	ELSE
                //		guo_msg.uf_msg(f_get_error_message_number(5092))
                //	END IF
                if (ab_display_message)
                {
                    if (ib_mt_message)
                    {
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, await f_get_error_message_numberClass.f_get_error_message_number(5092));
                    }
                    else
                    {
                        await masofonAlias.masofon.Instance.guo_msg.uf_msg(await f_get_error_message_numberClass.f_get_error_message_number(5092));
                    }
                }
                //END 
                return "";
            }
            else if (lb_Barcode_Exists || lb_External_Barcode_Exists)
            {
                //.. if barcode OR external barcode Exists - get item Number
                if (lb_Barcode_Exists)
                {
                    //ll_Item_Number = uf_get_item_number(as_Item_Barcode)
                }
                else
                {
                    ll_material_number =await uf_get_material_number_from_external(as_item_barcode);
                    if (ll_material_number == 0)
                    {
                        return "";
                    }
                    as_item_barcode =await uf_get_barcode(ll_material_number);
                }
            }
            else
            {
                //.. Do Nothing
            }
            return as_item_barcode;
        }
        public long uf_get_material_main_supplier(long al_material_number) //********************************************************************
        {
            //*Object:				u_material_search
            //*Function Name:	uf_get_material_main_supplier
            //*Purpose: 			Get supplier_number
            //*Arguments: 		Long	al_material_number
            //*Return:				Long	supplier_number
            //*Date					Programer		Version	Task#	 			Description
            //*------------------------------------------------------------------------------------------------------------
            //*12-01-2015		AlexKh			1.1.26.0	SPUCM00005189	Initial version
            //********************************************************************

            long ll_supplier_number = 0;
            if (true)
            {
                LoadData20(al_material_number, ref ll_supplier_number);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                return 0;
            }
            return ll_supplier_number;
        }
        public u_material_search()
        {
            constructor();
        }

        public void constructor() //.. connect to sqlca by default
        {
            //masofonAlias.masofon.Instance.sqlca = sqlca
            //masofonAlias.masofon.Instance.sqlca = masofonAlias.masofon.Instance.SQLCA_REPORTS;
            ib_show_error_message = true;
        }
        public void LoadData(string as_external_barcode, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_external_barcode } };
                    string sql = "SELECT COUNT(item_number) FROM item_codes,  items WHERE code_reverse LIKE @0 AND item_codes.item_number = items.number AND items.equal_tree = 'y'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_material_number, ref string ls_material_barcode)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT barcode FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_material_barcode = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(Int64 al_material_number, ref string ls_material_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT name FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_material_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(string as_material_barcode, ref long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_material_barcode } };
                    string sql = "SELECT number FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4(string as_barcode, ref long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_barcode } };
                    string sql = "SELECT number FROM materials WHERE materials.barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5(string as_part_barcode, ref long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_part_barcode } };
                    string sql = "SELECT number FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData6(string as_part_barcode, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_part_barcode } };
                    string sql = "SELECT COUNT(item_number) FROM item_codes,  items WHERE code = @0 AND item_codes.item_number = items.number AND items.equal_tree = 'y'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData7(string as_part_barcode, ref long ll_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_part_barcode } };
                    string sql = "SELECT material_number FROM item_codes,  trees WHERE item_codes.item_number = trees.item_number AND item_codes.code = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData8(string as_material_barcode, ref Int64 al_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_material_barcode } };
                    string sql = "SELECT number FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        al_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData9(Int64 al_material_number, ref string as_material_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT name FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        as_material_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData10(string as_material_barcode, ref Int64 al_material_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_material_barcode } };
                    string sql = "SELECT number FROM materials WHERE barcode = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        al_material_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData11(Int64 al_material_number, ref string as_material_name, ref Int64 al_min_valid_month)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT name,  min_valid_months FROM materials WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        as_material_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        al_min_valid_month = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData12(Int64 al_branch_number, int[] ll_items_numbers_arr, ref decimal ldec_sell_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", ll_items_numbers_arr[0] } };
                    string sql = "SELECT sell_price FROM branch_item_price WHERE branch_number = @0 AND item_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_sell_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData13(Int64 al_branch_number, Int64 al_supplier_number, Int64 al_material_number, ref decimal ldec_material_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_supplier_number }, { "@2", al_material_number } };
                    string sql = "SELECT IsNull(price_before_discount, 0) FROM material_suppliers WHERE branch_number = @0 AND supplier_number = @1 AND material_number = @2 AND last_supplier_price = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_material_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData14(Int64 al_branch_number, Int64 al_material_number, ref decimal ldec_material_price)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT IsNull(price_before_discount, 0) FROM material_suppliers WHERE branch_number = @0 AND material_number = @1 AND last_supplier = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ldec_material_price = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData15(Int64 al_supplier_number, ref string ls_supplier_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_supplier_number } };
                    string sql = "SELECT name FROM suppliers WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_supplier_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData16(Int64 al_branch_number, Int64 al_material_number, ref decimal adec_price_before_discount, ref decimal adec_material_price_after_discount)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT IsNull(price_before_discount, 0),  IsNull(price_after_discount, 0) FROM material_suppliers WHERE branch_number = @0 AND material_number = @1 AND last_supplier = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        adec_price_before_discount = UnitOfWork.GetValue(resultRS, 0, Convert.ToDecimal);
                        adec_material_price_after_discount = UnitOfWork.GetValue(resultRS, 1, Convert.ToDecimal);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData17(Int64 al_material_number, ref char lc_material_status)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT material_status FROM materials WHERE materials.number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        lc_material_status = UnitOfWork.GetValue(resultRS, 0, Convert.ToChar);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData18(Int64 al_supplier, Int64 al_material_number, ref long ll_temp_material)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    bool noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_supplier }, { "@1", al_material_number } };
                    string sql = "SELECT material_suppliers_connect.material_number FROM material_suppliers_connect WHERE (material_suppliers_connect.supplier_number = @0) AND (material_suppliers_connect.material_number = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp_material = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData19(Int64 al_branch_number, Int64 al_material_number, ref long ll_last_supplier, ref string ls_last_supplier)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_material_number } };
                    string sql = "SELECT material_suppliers.supplier_number,  suppliers.name FROM material_suppliers,  suppliers WHERE material_suppliers.supplier_number = suppliers.number AND material_suppliers.branch_number = @0 AND material_suppliers.material_number = @1 AND material_suppliers.last_supplier = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_last_supplier = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_last_supplier = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData20(Int64 al_material_number, ref long ll_supplier_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_material_number } };
                    string sql = "SELECT IsNull(supplier_number, 0) FROM materials WHERE (materials.number = @0)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_supplier_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
