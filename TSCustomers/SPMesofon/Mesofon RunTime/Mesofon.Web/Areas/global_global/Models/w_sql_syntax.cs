using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:21:02 (Utc)
	// Comments:        PFN2
	// 
	public class w_sql_syntax : WindowElement
	{
		public delegate void ue_key_EventHandler();
	//	public event ue_key_EventHandler ue_keyEvent;
        public w_sql_syntax(string sqlSyntax)
		{
            this.SqlSyntax = sqlSyntax;
		}

	    public w_sql_syntax()
	    {
	        
	    }

	    private string SqlSyntax { get; set; }

        
    }
}
