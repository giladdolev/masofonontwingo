using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.MVC;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_dw_report : u_dw_many_rows
	{
		public s_reports_db istct_reports_db;
		public u_dw_report()
		{
			VisualElementHelper.CreateFromView<u_dw_many_rows>("u_dw_many_rows", "u_dw_many_rows", "global_global");
		   

		}

        public async Task constructor() //.. connect to Report DB Server
        {
            base.constructor();
            object lpo = null;
            string ls_win_class = null;
            lpo = this;
            while (lpo.GetType() != typeof(WindowElement))
            {
                lpo = this.Parent;
            }
            ls_win_class = lpo.GetType().Name;
            this.istct_reports_dbProperty = await masofon.masofon.Instance.gnvo_reports_db.uf_get_db_transaction(ls_win_class + "." + this.GetType().Name, false);
            masofon.masofon.Instance.SQLCA_REPORTS = this.istct_reports_dbProperty.report_tran;

        }

        public s_reports_db istct_reports_dbProperty
		{
			get { return this.istct_reports_db; }
			set { this.istct_reports_db = value; }
		}
	}
}
