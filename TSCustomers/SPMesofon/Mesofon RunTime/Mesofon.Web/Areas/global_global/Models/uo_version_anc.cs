namespace global
{
	// Creation Time:   11/30/2015 17:56:12 (Utc)
	// Comments:        
	// 

	public class uo_version_anc 
	{
		// SharonS - 1.0.15.0 - 2007-12-24 - CR#1066 - Add pach number (ii_patch)
		protected int ii_Version;
		protected int ii_patch;
		protected int ii_db_version;
		protected string is_Version_type;
		protected string is_dll_version;
		protected string Major_version = "1";
		protected string Minor_version;
		public int uf_get_version()
		{
			return ii_Version;
		}
		public int uf_get_db_version()
		{
			return ii_db_version;
		}
		public string uf_get_version_name()
		{
			string Full_Version = null;
			// SharonS - 1.0.15.0 - 2007-12-24 - CR#1066 - Add pach number (ii_patch)
			Full_Version = Major_version + "." + Minor_version + "." + ii_Version.ToString() + "." + ii_patch.ToString() + "." + is_dll_version + "." + ii_db_version.ToString();
			return Full_Version;
		}
		public void constructor()
		{
			is_dll_version = "2";
			// Add pach number (ii_patch)
			ii_patch = 2;
			ii_db_version = 131;
			Minor_version = "2";
		}
		public uo_version_anc()
		{
		    constructor();
        }
		
	}
}
