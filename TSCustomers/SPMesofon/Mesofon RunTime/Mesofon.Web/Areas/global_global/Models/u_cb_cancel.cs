using System.Web.VisualTree.MVC;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_cb_cancel : u_cb_ok
	{
		public u_cb_cancel() : base()
		{
			VisualElementHelper.CreateFromView<u_cb_ok>("u_cb_ok", "u_cb_ok", "global_global");
		    constructor();

		}

	    public void constructor() //--------------------------------------------------------------------
	    {
	        //Function:			public u_cb_cancel.constructor()
	        //
	        // Returns:         long
	        //
	        // Copyright  - Stas
	        //
	        // Date Created: 03/08/2005
	        //
	        // Description:	
	        //		 					sets default values. 
	        //--------------------------------------------------------------------------------
	        // Modifications:
	        // Date            Author              Comments
	        //------------------------------------------------------------------------------

 
	        //.. return value is -1 by default
	        il_return_value = -1;
	    }

    }
}
