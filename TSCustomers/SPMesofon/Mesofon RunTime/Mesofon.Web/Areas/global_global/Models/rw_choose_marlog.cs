using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:20:56 (Utc)
	// Comments:        
	// 
	public class rw_choose_marlog : WindowElement
	{
		public const long il_max_window_height = 640;
		public const long il_max_window_width = 480;
        public rw_choose_marlog(string crossDoc)
        {
            CrossDoc = crossDoc;
        }

        public string CrossDoc { get; set; }
       
    }
}
