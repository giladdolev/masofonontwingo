using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:21:01 (Utc)
	// Comments:        
	// 
	public class w_filter : WindowElement
	{
		private GridElement idw_dw;
		public string is_title;
		public int i_current_row;
		public bool i_source_down;
		public bool i_sort_down;
        public w_filter(str_sort sort)
        {
            this.Sort = sort;
        }

	    public w_filter()
	    {
	       
	    }


	    public str_sort Sort { get; set; }
		
		public GridElement idw_dwProperty
		{
			get { return this.idw_dw; }
			set { this.idw_dw = value; }
		}
		public string is_titleProperty
		{
			get { return this.is_title; }
			set { this.is_title = value; }
		}
		public int i_current_rowProperty
		{
			get { return this.i_current_row; }
			set { this.i_current_row = value; }
		}
		public bool i_source_downProperty
		{
			get { return this.i_source_down; }
			set { this.i_source_down = value; }
		}
		public bool i_sort_downProperty
		{
			get { return this.i_sort_down; }
			set { this.i_sort_down = value; }
		}

        
    }
}
