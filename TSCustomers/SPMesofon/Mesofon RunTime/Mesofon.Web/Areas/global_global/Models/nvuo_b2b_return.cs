using System;
using System.Extensions;
using System.IO;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Data;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using SystemIOAlias = System.IO;


namespace global
{
	// Creation Time:   08/11/2015 10:20:55 (Utc)
	// Comments:        
	// 
	public class nvuo_b2b_return : nvuo_b2b_transactions
	{
		public IRepository ids_header;
		public IRepository ids_details;
		public string is_message_type;
		public async Task<int> uf_write_log(string as_message) //********************************************************************************************************************************
		{
			//*Object:								nvuo_b2b_debit
			//*Function/Event  Name:			uf_write_log
			//*Purpose:							Write into log file
			//*
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*									-------------------------------------------------------------------------------------------------------------------
			//*									Value			String						as_message- the message to write into the file.
			//*
			//*Return:							Integer: 1- Success
			//*												  -1 -Failed.
			//*
			//*Date 				Programer				Version		Task#			Description
			//*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
			//*19-12-2007 		SharonS					1.0.2			B2B			Initiation
			//**********************************************************************************************************************************
			
			int li_file = 0;
			int li_file_open = 0;
			int ll_debit_invoice_no = 0;
			string ls_log_file_name = null;
			string ls_message = null;
			string ls_doc_type = null;
			string ls_msg_header = null;
			string ls_log_mode = null;
		//	bool ib_log_mode = false;
			// SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Log on/off
			if (!masofonAlias.masofon.Instance.gb_b2b_log)
			{
				return 1;
			}
			// End
			// SharonS - 1.2.44.6 - 2009-05-26 - SPUCM00001362 - Add data to log file
			ls_msg_header = "------------------------------------------------------------------------------------------------------------------------" + "\r" + "\n";
			ls_msg_header += "Employee Number: " + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + " Date: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n";
			if (ids_header != null)
			{
				if (is_message_type == "389")
				{
					
					ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "invoice_number");
					ls_msg_header += "Return Invoice number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
				}
				else if (is_message_type == "384")
				{
					
					ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "debit_credit_number");
					ls_msg_header += "Confirm number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
				}
				else
				{
					
					ll_debit_invoice_no = (int)ids_header.GetItemValue<int>(0, "debit_credit_number");
					ls_msg_header += "Debit Invoice number: " + ll_debit_invoice_no.ToString() + "\r" + "\n";
				}
			}
			else
			{
				ls_msg_header += "ids_header is not valid." + "\r" + "\n";
			}
			as_message = ls_msg_header + as_message;
			// End
			// AlexKh - 1.2.48.3 - 2011-05-24 - SPUCM00002871 - change log path
			//ls_log_file_name = "c:\" + String(gs_vars.branch_number) + "_DEBIT_MSG.log" 
			ls_log_file_name = "c:\\NET-POS\\log\\" + masofonAlias.masofon.Instance.gs_vars.branch_number.ToString() + "_DEBIT_MSG.log";
		    int result = -1;
		    try
		    {
		        using (var file = new StreamWriter(ls_log_file_name))
		        {
                    file.Write(as_message);
		        }
		        result = 1;
		    }
		    catch (Exception e)
		    {
		        result = -1;

		    }

		    if (result == -1)
		    {
		        await MessageBox.Show("Can not open the log file", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Stop);
		    }
		    return 1;
		}
		public bool uf_save_as_xml() //**********************************************************************************************
		{
			//*Object:						nvuo_b2b_debit
			//*Function Name:			uf_save_as_xml
			//*Purpose:					Save the DW as XML file.
			//*  
			//*Arguments:				None.
			//*Return:						Boolean: 1 - succeeded.
			//*										 	-1 - failed.
			//*Date 			Programer			Version	Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
			//************************************************************************************************


            return ids_return_inv_xml.SaveToXml(Convert.ToString(is_file_spec)) == 1;
		}
		public int uf_insert_header(long al_row) //**********************************************************************************************
		{
			//*Object:						nvuo_b2b_debit
			//*Function Name:			uf_insert_header
			//*Purpose:					Insert haeder section into the XML DW.
			//*  
			//*Arguments:				Long - Row number in the XML DW.
			//*Return:						Integer: 1 - succeeded.
			//*										  -1 - failed.
			//*Date 			Programer			Version	Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
			//************************************************************************************************
			
			string ls_reference_no = null;
			long ll_reference_date = 0;
			long ll_debit_invoice_no = 0;
			decimal ld_date_time = default(decimal);
			DateTime? ldt_date_time = null;
			 
			ids_return_inv_xml_temp.SetItem(al_row, "invoicetype", is_message_type); //1
			
			ll_debit_invoice_no = ids_header.GetItemValue<long>((int)al_row, "invoice_number");
			if (ll_debit_invoice_no == 0)
			{
				return -1;
			}
			 
			ids_return_inv_xml_temp.SetItem(al_row, "debitinvoiceno", ll_debit_invoice_no.ToString()); //2
			 
			ids_return_inv_xml_temp.SetItem(al_row, "invoicefunc", "9"); //3
			
			ldt_date_time = ids_header.GetItemValue<DateTime>((int)al_row, "supply_date");
			ld_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
			if (ld_date_time == 0)
			{
				return -1;
			}
			 
			ids_return_inv_xml_temp.SetItem(al_row, "datetime", ld_date_time); //4
			//	ids_return_inv_xml_temp.SetItem(al_row, "referenceno", ls_reference_no)  			//5
			// ids_return_inv_xml_temp.SetItem(al_row, "referencedate", ll_reference_date)  		//6
			if (isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.qs_parameters.edi_number))
			{
				return -1;
			}
			 
			ids_return_inv_xml_temp.SetItem(al_row, "storeno", masofonAlias.masofon.Instance.qs_parameters.edi_number); //7
			 
			ids_return_inv_xml_temp.SetItem(al_row, "storename", masofonAlias.masofon.Instance.qs_parameters.company_name); //8
			return 1;
		}
		public int uf_insert_details(long al_row) //**********************************************************************************************
		{
			//*Object:						nvou_b2b_debit
			//*Function Name:			uf_insert_details
			//*Purpose:					Insert the Deatails section of the debit XML
			//*  
			//*Arguments:				Long	- Row No (al_row)
			//*Return:						Integer - 1 succeed.
			//*											-1 not  succeed.
			//*Date 			Programer		Version	Task#		Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-12-2007		SharonS			0.1.0 		B2B 		Initiate Version
			//************************************************************************************************
			
			string ls_barcode = null;
			string ls_reason_code = null;
			string ls_cartno = null;
			decimal ld_quantity_diff = default(decimal);
			long ll_line_number = 0;
			long ll_decline_number = 0;
			long ll_find = 0;
			GridElement ldwc_dec_num;
			// LineNo
			ll_line_number = al_row;
			 
			ids_return_inv_xml.SetItem(al_row, "lineno", ll_line_number); //1
			// Barcode
			 
			ls_barcode = ids_details.GetItemValue<string>(al_row, "barcode");
			 
			ids_return_inv_xml.SetItem(al_row, "itembarcode", ls_barcode); //2
			// Product Description
			 
			ids_return_inv_xml.SetItem(al_row, "proddesc", ""); //3
			// Carton No
			 
			ls_cartno = ids_details.GetItemValue<string>(al_row, "carton_barcode");
			 
			ids_return_inv_xml.SetItem(al_row, "cartno", ls_cartno); //4
			// UnitsQty
			
			ld_quantity_diff = ids_details.GetItemValue<decimal>((int)al_row, "material_quantity");
			// End
			 
			ids_return_inv_xml.SetItem(al_row, "unitsqty", ld_quantity_diff); //5       
			// Reason Code
			if (is_message_type == "389")
			{
				ls_reason_code = "";
				
				ll_decline_number = ids_details.GetItemValue<long>((int)al_row, "declines");
				//	ids_details.GetChild("declines", ldwc_dec_num)
				//	ldwc_dec_num.SetTransObject(SQLCA)
				//	ldwc_dec_num.retrieve(3)
				//	ll_find = ldwc_dec_num.Find("declines =" + String(ll_decline_number), 1, ldwc_dec_num.rowcount())
				//	IF ll_find > 0 THEN ls_reason_code = ldwc_dec_num.GetItem<string>(ll_find, "decline_code")
				 
				ids_return_inv_xml.SetItem(al_row, "reasoncode", ll_decline_number.ToString()); //6
			}
			return 1;
		}
        public async Task<int> uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*24-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            long ll_message_date = 0;
            long ll_supplier_num = 0;
            long ll_distributor_num = 0;
            string ls_organization_edi = null;
            string ls_message = null;
            string ls_message_time = null;
            ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
            if (isempty_stringClass.isempty_string(ls_organization_edi))
            {
                return -1;
            }

            ids_return_inv_xml_temp.SetItem(al_row, "sender", ls_organization_edi); //1

            ll_supplier_num = ids_header.GetItemValue<long>((int)al_row, "supplier_number");
            if (ll_supplier_num == 0 || ll_supplier_num == 0)
            {
                ls_message = "supplier_number IS NOT VALID.";
                await uf_write_log(ls_message);
                return -1;
            }
            is_parent_supplier_edi = f_get_parent_supplier_ediClass.f_get_parent_supplier_edi(ll_supplier_num);

            ll_distributor_num = ids_header.GetItemValue<long>((int)al_row, "distributor_number");
            if (ll_distributor_num != 0 && !f_is_crossdoc_distributorClass.f_is_crossdoc_distributor(ll_distributor_num))
            {
                is_supplier_edi = f_get_supplier_distributor_ediClass.f_get_supplier_distributor_edi(ll_supplier_num, ll_distributor_num);
                is_parent_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_distributor_num);
            }
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                is_parent_supplier_edi = is_supplier_edi;
            }
            if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
            {
                return -1;
            }

            ids_return_inv_xml_temp.SetItem(al_row, "receiver", is_parent_supplier_edi); //2

            ids_return_inv_xml_temp.SetItem(al_row, "doctype", "RETURNS2C"); //3

            ids_return_inv_xml_temp.SetItem(al_row, "aprf", "INVDBT"); //4
                                                                       //"SNRF" //5 ???

            ids_return_inv_xml_temp.SetItem(al_row, "ackn", "1"); // 1 - Required	//6

            ids_return_inv_xml_temp.SetItem(al_row, "testind", "1"); // 1 - Test 	//7
            ll_message_date = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd"));

            ids_return_inv_xml_temp.SetItem(al_row, "messdate", ll_message_date); //8
            ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("hhmmss");

            ids_return_inv_xml_temp.SetItem(al_row, "messtime", ls_message_time); //9
                                                                                  // End
            return 1;
        }
        public async Task<FuncResults<int, IRepository[], bool>>  uf_send_transaction( int ai_file_status, string as_supplier_edi,  IRepository[] ads_data) //**********************************************************************************************
		{
			//*Object:						nvuo_b2b_debit
			//*Function Name:			uf_send_transaction
			//*Purpose:					Create the B2B message XML file.
			//*								This function will be coded in the decendance objects.
			//*								In each object a defferent message will be created.
			//*  
			//*Arguments:				Integer - file_status
			//*Return:						Boolean - TRUE - Creation of B2B message XML file succeded.
			//											 FALSE - Creation of B2B message XML file failed.
			//*Date 			Programer			Version	Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*19-12-2007		SharonS				0.1.0 		B2B 			Create the B2B message XML file.
			//************************************************************************************************
			
			int li_index = 0;
			int li_debit_row = 0;
			long ll_find = 0;
			long ll_row = 0;
			long ll_row_copy = 0;
			long ll_debit_items = 0;
			string ls_find = null;
			string ls_organization_edi = null;
			string ls_branch_edi = null;
			string ls_branch_name = null;
			string ls_supplier_edi = null;
			string ls_debit_no = null;
			string ls_message = null;
			// Check validity
			if (CollectionExtensions.UBound(ads_data) < 0)
			{
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			
            li_debit_row = (int)ads_data[0].RowCount();
			if (li_debit_row < 1)
			{
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			ids_header = ads_data[0];
			// Only for Debit and Return
			if (is_message_type != "384")
			{
				
				ll_debit_items = ads_data[1].RowCount();
				if (ll_debit_items < 1)
				{
				    return FuncResults.Return(ai_file_status, ads_data, false);
                }
				ids_details = ads_data[1];
			}
			is_supplier_edi = as_supplier_edi;
			is_parent_supplier_edi = is_supplier_edi;
			// Create and define DataStore
			
			ids_return_inv_xml.Reset();
			
			ids_return_inv_xml.Reset();
			if (is_message_type == "383")
			{
				
				ls_debit_no = ids_header.GetItemValue<int>(0, "debit_credit_number").ToString();
			}
			else if (is_message_type == "389")
			{
				
				ls_debit_no = ids_header.GetItemValue<int>(0, "invoice_number").ToString();
			}
			 
			ll_row = ids_return_inv_xml_temp.Insert(0);
			// Set the envalope data in the XML DW
			if ( await this.uf_insert_envelope(ll_row) < 0)
			{
				// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                await MessageBox.Show("שים לב - כשלון בכתיבת מעטפת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file envelop.";
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			// Set the header data in the XML DW
			if (this.uf_insert_header(ll_row) < 0)
			{
				// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                await MessageBox.Show("שים לב - כישלון בכתיבת כותרת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file header.";
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			//any	lany, lany_temp
			//lany_temp = ids_return_inv_xml_temp.Object.Data
			// Only for Confirm
			 
			 
			 
			 
			ids_return_inv_xml.Export("XMLExport");
			if (is_message_type == "384")
			{
				
				
				
				ll_row_copy = (long)ids_return_inv_xml_temp.RowsCopy(1, 1, ModelBuffer.Primary, ids_return_inv_xml, li_index + 1, ModelBuffer.Primary);
				 
				 
				 
				 
				ids_return_inv_xml.Export("xmlconfirm");
			}
			// Load the xml DW
			for (li_index = 1; li_index <= (int)ll_debit_items; li_index++)
			{
				
				
				
				ll_row_copy = (long)ids_return_inv_xml_temp.RowsCopy(1, 1, ModelBuffer.Primary, ids_return_inv_xml, li_index + 1, ModelBuffer.Primary);
				//	lany = ids_return_inv_xml.Object.Data
				if (this.uf_insert_details(li_index) < 0)
				{
					// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                    await MessageBox.Show("שים לב - כישלון בכתיבת פריט במסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file details.";
				    await uf_write_log(ls_message);
					ai_file_status = 0;
				    return FuncResults.Return(ai_file_status, ads_data, false);
                }
			}
			// Create xml file from datawindow
			if (!uf_save_as_xml())
			{
                switch (is_message_type)
                {
					case "384":
						// "CONFIRM"
						// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר אישור חשבונית לספק - המסר הדיגיטאלי לא יישלח", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
						ls_message = "Confirm Invoice No: " + ls_debit_no + " - Failed to Create/Write to File.";
						break;
					case "383":
						// "DEBIT" 
						// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
						ls_message = "Debit No: " + ls_debit_no + " - Failed to Create/Write to File.";
						break;
					case "389":
						// "RETURN"
						// TODO: Field 'StopSign of type 'Sybase.PowerBuilder.Icon' is unmapped'. (CODE=1004)
                        await MessageBox.Show("שים לב - כישלון ביצירת מסר תעודת החזרה - התעודה לא תשלח במסר דיגיטאלי", "כישלון", MessageBoxButtons.OK, MessageBoxIcon.Stop);
						ls_message = "Return No: " + ls_debit_no + " - Failed to Create/Write to File.";
						break;
				}
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			else
			{
				ai_file_status = 1;
			}
		    return FuncResults.Return(ai_file_status, ads_data, true);
        }
		public bool uf_set_message_type(string as_message_type) //**********************************************************************************************
		{
			//*Object:						nvou_b2b_debit
			//*Function Name:			uf_set_message_type
			//*Purpose:					Set message type: Debit, Return or Confirm.
			//*  
			//*Arguments:				String	 - as_message_type
			//*Return:						Boolean - TRUE - Succeed.
			//*										      FALSE - Faild.
			//*Date 			Programer		Version		Task#		Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*21-01-2008		SharonS			1.2.23.0 		10001		Initiate Version
			//************************************************************************************************
			
			if (as_message_type == "DEBIT")
			{
				//Debit Note (Responce for supplier Invoice)
				is_message_type = "383";
			}
			else if (as_message_type == "RETURN")
			{
				//Self-Build Invoice (Return Invoice)
				is_message_type = "389";
			}
			else if (as_message_type == "CONFIRM")
			{
				//Corrected Invoice (Confirmation Invoice)
				is_message_type = "384";
			}
			return true;
		}

		public nvuo_b2b_return() : base()
		{
		    ids_return_inv_xml = new d_return_inv_for_b2b_xmlRepository();
			ids_return_inv_xml_temp = new d_return_inv_for_b2b_xmlRepository();
		}		
	}
}
