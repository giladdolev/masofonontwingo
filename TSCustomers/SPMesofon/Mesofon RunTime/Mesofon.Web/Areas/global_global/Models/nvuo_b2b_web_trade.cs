﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofon;
using Mesofon.Repository;
using Mesofon.Common;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using SystemIOAlias = System.IO;

namespace global
{
    // Creation Time:   08/11/2015 10:20:55 (Utc)
    // Comments:        
    // 
    public class nvuo_b2b_web_trade : nvuo_b2b_transactions
    {
        public IRepository ids_header;
        public IRepository ids_items;
        public IRepository ids_details;
        public IRepository ids_total;
        public string is_message_type;
        public string is_doc_type;
        public string is_order_org_no;
        public Int64 id_date_time;
        public decimal idc_site_order_number;
        public DateTime? idt_date_time;
        public async Task<int> uf_write_log(string as_message) //********************************************************************************************************************************
        {
            //*Object:								nvuo_b2b_debit
            //*Function/Event  Name:			uf_write_log
            //*Purpose:							Write into log file
            //*
            //*Arguments:						Pass By		Argument Type			Argument Name
            //*									-------------------------------------------------------------------------------------------------------------------
            //*									Value			String						as_message- the message to write into the file.
            //*
            //*Return:							Integer: 1- Success
            //*												  -1 -Failed.
            //*
            //*Date 			Programer		Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
            //*May-1-2013		Eitan				1.2.48.48 	B2B 			Write log
            //**********************************************************************************************************************************

            var lsMsgHeader = "Employee Number: " + masofonAlias.masofon.Instance.gs_vars.active_owner + " Date: " + masofonAlias.masofon.Instance.set_machine_time() + "\r" + "\n";
            lsMsgHeader += "Site order number: " + is_order_org_no + "\r" + "\n";
            as_message = lsMsgHeader + as_message;
            SystemHelper.WriteToLog(as_message, string.Empty);
            return 0;
        }
        public bool uf_save_as_xml() //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_save_as_xml
            //*Purpose:					Save the DW as XML file.
            //*  
            //*Arguments:				None.
            //*Return:						Boolean: 1 - succeeded.
            //*										 	-1 - failed.
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*May-1-2013		Eitan				1.2.48.48 	B2B 			save as xml
            //************************************************************************************************

            return ids_web_trade_xml.SaveToXml(Convert.ToString(is_file_spec)) == 1;
        }
        public int uf_insert_header(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_web_trade
            //*Function Name:			uf_insert_header
            //*Purpose:					Insert haeder section into the XML DW.
            //*  
            //*Arguments:					Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer		Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------
            //*May-1-2013		Eitan				1.2.48.48 	B2B 			Initiate the object
            //************************************************************************************************

            long ll_order_date = 0;
            
            ll_order_date = (long)ids_header.GetItemValue<double>((int)al_row, "orderdate");
            id_date_time = Convert.ToInt64(idt_date_time.Value.ToString("yyyyMMddhhmm"));
            if (id_date_time == 0)
            {
                return -1;
            }
             
            
            ids_web_trade_xml.SetItem(al_row, "statusname", ids_header.GetItemValue<string>(al_row, "statusname")); //1
            
            ids_web_trade_xml.SetItem(al_row, "statuscode", ids_header.GetItemValue<string>(al_row, "statuscode")); //2
            
            ids_web_trade_xml.SetItem(al_row, "statusdatetime", id_date_time); //3
            
            ids_web_trade_xml.SetItem(al_row, "orderorgno", is_order_org_no); //4
            
            ids_web_trade_xml.SetItem(al_row, "orderdate", ll_order_date); //5
            return 1;
        }
        public int uf_insert_details(long al_row)
        {
            return 1;
        }
        public int uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_web_trade
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:					Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*May/1/2013		Eitan					1.2.48.48 	B2B 			Initiate the object
            //************************************************************************************************

            long ll_message_date = 0;
            long ll_supplier_num = 0;
            long ll_distributor_num = 0;
            string ls_organization_edi = null;
            string ls_message = null;
            string ls_message_time = null;
            ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
            if (isempty_stringClass.isempty_string(ls_organization_edi))
            {
                return -1;
            }
            
            ids_web_trade_xml.SetItem(al_row, "sender", ls_organization_edi); //1
            
            ids_web_trade_xml.SetItem(al_row, "receiver", is_supplier_edi); //2-969 EDI
            
            ids_web_trade_xml.SetItem(al_row, "doctype", is_doc_type); //3
            
            ids_web_trade_xml.SetItem(al_row, "aprf", is_doc_type); //4
            //"SNRF" //5
            
            ids_web_trade_xml.SetItem(al_row, "ackn", "1"); // 1 - Required	//6
            
            ids_web_trade_xml.SetItem(al_row, "testind", "1"); // 1 - Test 	//7
            ll_message_date = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd"));
            
            ids_web_trade_xml.SetItem(al_row, "messdate", ll_message_date); //8
            ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("hhmmss");
            
            ids_web_trade_xml.SetItem(al_row, "messtime", ls_message_time); //9
            return 1;
        }
        public async Task<FuncResults<int, IRepository[], bool>> uf_send_transaction( int ai_file_status, string as_supplier_edi, IRepository[] ads_data) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_web_trade
            //*Function Name:			uf_send_transaction
            //*Purpose:					Create the B2B message XML file.
            //*								This function will be coded in the decendance objects.
            //*								In each object a defferent message will be created.
            //*  
            //*Arguments:					Integer - file_status
            //*Return:						Boolean - TRUE - Creation of B2B message XML file succeded.
            //											 FALSE - Creation of B2B message XML file failed.
            //*Date 			Programer		Version		Task#			Description
            //*-----------------------------------------------------------------------------------------------------------------------
            //*May-1-2013		Eitan				1.2.48.48 	B2B 			Create the B2B message XML file.
            //************************************************************************************************

            long ll_row = 0;
            long ll_header_row = 0;
            string ls_message = null;
            string ls_site_state = null;
            string ls_site_state_2 = null;
            string ls_tmp = null;
            if (CollectionExtensions.UBound(ads_data) < 0)
            {
                ls_message = "uf_send_transaction function. UpperBound(ads_data[]) < 1." + "\r" + "\n";
                await uf_write_log(ls_message);
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            else
            {
                ls_message = "uf_send_transaction function. Start." + "\r" + "\n";
                await uf_write_log(ls_message);
            }
            
            ll_header_row = ads_data[1].RowCount();
            if (ll_header_row < 1)
            {
                ls_message = "uf_send_transaction function. ads_data[1].RowCount(): " + ll_header_row + "." + "\r" + "\n";
                await uf_write_log(ls_message);
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_header = ads_data[1];
            is_supplier_edi = as_supplier_edi;
            is_parent_supplier_edi = is_supplier_edi;
            // Create and define DataStore
            
            ids_web_trade_xml.Reset();
             
            ll_row = ids_web_trade_xml.Insert(0);
            // Set the envelope data in the XML DW
            if (this.uf_insert_envelope(ll_row) < 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כשלון ביצירת מסר");
                ls_message = "site_order_number: " + is_order_org_no + " - Failed in writing file envelope.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Set the header data in the XML DW
            if (this.uf_insert_header(ll_row) < 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כשלון ביצירת מסר");
                ls_message = "site_order_number: " + is_order_org_no + " - Failed in writing file header.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
             
             
            
             
            ids_web_trade_xml.Export(ids_web_trade_xml.XMLExport);
            if (true)
            {
                LoadData2(ref ls_site_state);
            }
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                ls_message = "uf_send_transaction function. No Rows #1." + "\r" + "\n";
                await uf_write_log(ls_message);
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            switch (is_doc_type)
            {
                case "STOR01":
                    ls_tmp = (Convert.ToInt64(ls_site_state) + "1");
                    ls_site_state_2 = SystemFunctionsExtensions.Fill("0", 10 - ls_tmp.Length) + ls_tmp; //Eitan; 19/2/2015; Build_29; Change from 5 to 10
                    //Eitan; 12/3/2015; Build 32; Add STOR12
                    break;
                case "STOR02":
                case "STOR12":
                    ls_tmp = (Convert.ToInt64(ls_site_state) + "10");
                    ls_site_state_2 = SystemFunctionsExtensions.Fill("0", 10 - ls_tmp.Length) + ls_tmp; //Eitan; 19/2/2015; Build_29; Change from 5 to 10
                    break;
                case "STOR03":
                    ls_tmp = (Convert.ToInt64(ls_site_state) + "100");
                    ls_site_state_2 = SystemFunctionsExtensions.Fill("0", 10 - ls_tmp.Length) + ls_tmp; //Eitan; 19/2/2015; Build_29; Change from 5 to 10
                    break;
                case "STOR04":
                    ls_tmp = (Convert.ToInt64(ls_site_state) + "1000");
                    ls_site_state_2 = SystemFunctionsExtensions.Fill("0", 10 - ls_tmp.Length) + ls_tmp; //Eitan; 19/2/2015; Build_29; Change from 5 to 10
                    break;
                default:
                    ls_message = "uf_send_transaction function. UnExpected Error." + "\r" + "\n";
                    await uf_write_log(ls_message);
                    return FuncResults.Return(ai_file_status, ads_data, false);
            }
            await f_begin_tranClass.f_begin_tran();
            UpdateData2(ls_site_state_2);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                await f_rollbackClass.f_rollback();
                ls_message = "uf_send_transaction function. No Rows #2." + "\r" + "\n";
                await uf_write_log(ls_message);
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Create xml file from datawindow
            if (!uf_save_as_xml())
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כשלון ביצירת מסר");
                ls_message = "site_order_number: " + is_order_org_no + " - Failed to Create/Write to File.";
                await uf_write_log(ls_message);
                ai_file_status = 0;
                await f_rollbackClass.f_rollback();
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            else
            {
                ai_file_status = 1;
            }
            ls_message = "uf_send_transaction function. Finished Successfully." + "\r" + "\n";
            await uf_write_log(ls_message);
            await f_commitClass.f_commit();
            return FuncResults.Return(ai_file_status, ads_data, true);
        }
        public async Task<bool> uf_init_web_trade(string as_msg_type, decimal adc_site_order_number) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_web_trade
            //*Function Name:			uf_init_web_trade
            //*Purpose:					Initiate the object:
            //*  
            //*Arguments:					String	- Message Type
            //*								String	- site order number Type
            //*Return:						BOOLEAN - TRUE - Creation of file target directory succeeded.
            //*												
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*May-1-2013		Eitan				1.2.48.48	B2B 			Initiate the object
            //************************************************************************************************

            idt_date_time = masofonAlias.masofon.Instance.set_machine_time();
            is_doc_type = as_msg_type;
            idc_site_order_number = adc_site_order_number;
            is_order_org_no = adc_site_order_number.ToString();
            is_file_name = as_msg_type + "_";
            is_file_name += is_order_org_no + "_";
            is_file_name += idt_date_time.Value.ToString("yyyyMMdd_hhmmss");
            is_file_name += ".xml";
            is_file_path = await f_get_parameter_valueClass.f_get_parameter_value("trade_status_path", "String", "C:\\", "path to save trade status messages", "gds_find");
            is_file_spec = is_file_path;
            if (is_file_spec.Substring(is_file_spec.Length - 1) != "\\")
            {
                is_file_spec += "\\";
            }
            is_file_spec += is_file_name;
            return true;
        }
        public nvuo_b2b_web_trade()
            : base()
        {
            ids_web_trade_xml = new  d_web_trade_for_b2b_xmlRepository();
            //ids_web_trade_xml_temp = CREATE DataStore
            //ids_web_trade_xml_temp.DataObject = 'd_web_trade_for_b2b_xml'
        }
        
        public void UpdateData2(string ls_site_state_2)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_site_state_2 }, { "@1", idc_site_order_number } , { "@2", masofonAlias.masofon.Instance.gs_vars.LaboratoryBranchNum } };
                    string sql = "UPDATE site_order_state SET site_state = @0 WHERE site_order_number = @1 AND branch_number = @2";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(ref string ls_site_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", idc_site_order_number }, {"@1", masofonAlias.masofon.Instance.gs_vars.LaboratoryBranchNum } };
                    string sql = "SELECT TOP 1 site_state FROM site_order_state WHERE branch_number = @1 AND site_order_number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_site_state = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
