using System;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using System.Web.VisualTree.MVC;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using Mesofon.Common;

namespace global
{
    // Creation Time:   08/11/2015 10:20:56 (Utc)
    // Comments:        
    // 
    public class nvuo_pallet_orders_search : u_order_search
    {
        public long il_invoice_number;
        public IRepository ids_pallet_invoices;
        public override bool uf_retrieve() //********************************************************************
        {
            //*Object:				nvuo_pallet_orders_search
            //*Function Name:	uf_retrieve
            //*Purpose: 			Retrieve all orders details for the pallet
            //*Arguments: 		None
            //*Return:				Boolean
            //*Date				Programer		Version	Task#	 		Description
            //*---------------------------------------------------------------------
            //*08-07-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //********************************************************************

            long ll_count = 0;
            long ll_employee_no = 0;
            long ll_i = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_prev_order_number = 0;
            u_datastore_update lds_order_details = default(u_datastore_update);
            u_datastore_update lds_order_move = default(u_datastore_update);
            if (il_branch_number == 0)
            {
                return false;
            }

            if (ids_pallet_invoices.RowCount() == 0)
            {
                return false;
            }
            lds_order_details = new u_datastore_update();
            lds_order_move = new u_datastore_update();

            lds_order_details.SetRepository(new d_sql_supplier_order_detailsRepository());


            lds_order_move.SetRepository(new d_sql_supplier_order_moveRepository());
            
            ids_order_move.Reset();
            
            ids_order_details.Reset();

            for (ll_i = 0; ll_i < ids_pallet_invoices.RowCount(); ll_i++)
            {

                ll_supplier_number = ids_pallet_invoices.GetItemValue<long>((int)ll_i, "supplier_number");

                ll_order_number = (long)ids_pallet_invoices.GetItemValue<double>((int)ll_i, "order_number");
                if (ll_supplier_number == 0)
                {
                    return false;
                }
                if (ll_order_number == 0)
                {
                    return false;
                }
                if (ll_prev_order_number == ll_order_number)
                {
                    continue;
                }
                ll_prev_order_number = ll_order_number;
                //.. If error occures save error number and message

                if (lds_order_move.Retrieve(il_branch_number, ll_supplier_number, ll_order_number) < 0)
                {
                    il_last_sql_db_code = lds_order_move.uf_get_last_sql_db_code();
                    is_last_sql_err_text = lds_order_move.uf_get_last_sql_err_text();
                    return false;
                }
                ll_count = lds_order_move.RowsMove(0, lds_order_move.RowCount(), ModelBuffer.Primary, ids_order_move, ids_order_move.RowCount() + 1, ModelBuffer.Primary);
                
                ids_order_move.ResetUpdate();
                //.. If error occures save error number and message

                if (lds_order_details.Retrieve(il_branch_number, ll_order_number, ll_supplier_number) < 0)
                {
                    il_last_sql_db_code = lds_order_details.uf_get_last_sql_db_code();
                    is_last_sql_err_text = lds_order_details.uf_get_last_sql_err_text();
                    return false;
                }




                 
                ll_count = lds_order_details.RowsMove(0, lds_order_details.RowCount(), ModelBuffer.Primary, ids_order_details, ids_order_details.RowCount() + 1, ModelBuffer.Primary);
                
                ids_order_details.ResetUpdate();
            }
            //.. Pallet data succesfully retrieved
            ib_modified = false;



            return true;
        }
        public void uf_set_invoice_number(IRepository ads_invoices_list) //**********************************************************************************************
		{
            //*Object:								nvuo_pallet_orders_search
            //*Function/Event  Name:			uf_set_invoice_number
            //*Purpose:							Set Pallet invoices information
            //*Arguments:							Datstore	ads_invoices_list
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*29-07-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

		    ids_pallet_invoices = ads_invoices_list;
		    ads_invoices_list.SetSort("order_number");
            ids_pallet_invoices.Sort();
			uf_retrieve();
		}
        public override int uf_set_order_state(string as_order_state) //**********************************************************************************************
        {
            //*Object:				nvuo_pallet_orders_search
            //*Function Name:	uf_set_order_state
            //*Purpose: 			Set order state
            //*Arguments: 		String	as_order_state
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*26-08-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //************************************************************************************************

            long ll_rows = 0;
            long? ll_found = 0;
            int li_ret = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }

            ll_rows = ids_order_move.RowCount();
            if (ll_rows < 1)
            {
                return -1;
            }
            ls_find = "order_number ==" + il_order_number.ToString();

            
            ll_found = ids_order_move.Find(ls_find, 0, ids_order_move.RowCount());
            if (ll_found >= 0)
            {

                ids_order_move.SetItem(ll_found.Value, "state", Convert.ToString(as_order_state));
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    li_ret = (int) ids_order_move.Update(unitOfWork);
                }
                if (li_ret < 0)
                {
                    il_last_sql_db_code = ids_order_move.uf_get_last_sql_db_code();
                    is_last_sql_err_text = ids_order_move.uf_get_last_sql_err_text();
                    return -1;
                }
            }
            return 1;
        }
        public override int uf_set_item_quantity_within_invoice(long al_material_number, decimal adec_material_quantity) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_orders_search
            //*Function/Event  Name:			uf_set_item_quantity_within_invoice
            //*Purpose:							set quantity within invoice into order
            //*Arguments:							Long	al_material_number
            //*Return:								RETURN 1
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*26-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            long ll_rows = 0;
            long? ll_found = 0;
            int li_ret = 0;
            string ls_find = null;
            decimal ldec_qurrent_quantity = default(decimal);
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }

            ll_rows = ids_order_details.RowCount();
            ls_find = "material_number == " + al_material_number.ToString() + " and order_number =" + il_order_number.ToString();
            
            ll_found = ids_order_details.Find(ls_find, 0, (int)ll_rows);
            if (ll_found >= 0)
            {

                ldec_qurrent_quantity = ids_order_details.GetItemValue<decimal>((int)ll_found, "quantity_within_invoice");
                if (ldec_qurrent_quantity == 0)
                {
                    ldec_qurrent_quantity = 0;
                }

                ids_order_details.SetItem(ll_found.Value, "quantity_within_invoice", Convert.ToString(adec_material_quantity + ldec_qurrent_quantity));
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    li_ret = (int) ids_order_details.Update(unitOfWork);
                }
                if (li_ret < 1)
                {
                    il_last_sql_db_code = ids_order_details.uf_get_last_sql_db_code();
                    is_last_sql_err_text = ids_order_details.uf_get_last_sql_err_text();
                    return -1;
                }
            }
            return 1;
        }
        public override void uf_set_order_number(long al_order_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_orders_search
            //*Function/Event  Name:			uf_set_order_number
            //*Purpose:							Set order number
            //*Arguments:							Long	al_order_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*29-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            il_order_number = al_order_number;
        }
        public override void uf_set_supplier_number(long? al_supplier_number) //**********************************************************************************************
        {
            //*Object:								nvuo_pallet_orders_search
            //*Function/Event  Name:			uf_set_supplier_number
            //*Purpose:							Set supplier number
            //*Arguments:							Long	al_supplier_number
            //*Return:								None
            //*Date 			Programer		Version		Task#			Description
            //*------------------------------------------------------------------------------------------------
            //*29-08-2010		AlexKh			1.2.46.0  	CR#1138 		Initial Version
            //************************************************************************************************

            il_supplier_number = al_supplier_number;
        }
        public nvuo_pallet_orders_search()
            : base()
        {
            constructor();
        }

        public void constructor() //********************************************************************
        {

            // Code was removed based on 'u_order_search.constructor() : Void' mappings.
            // MyBase.constructor()

            ids_pallet_invoices = new d_mini_terminal_pallet_invoicesRepository();
        }
    }
}
