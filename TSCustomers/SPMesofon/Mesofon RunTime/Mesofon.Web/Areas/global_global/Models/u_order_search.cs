using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using Mesofon.Common;
using Mesofon.Repository;

namespace global
{
    // Creation Time:   08/11/2015 10:21:01 (Utc)
    // Comments:        
    // 

    public class u_order_search
    {
        //.. connection to db. by default connected to sqlca
        public SQLError i_transaction;
        //.. show miniterminial messagebox or regular messagebox
        public bool ib_mt_message;
        //.. if some of these parameters was changed then order ds`s will be reretrieved
        public long il_branch_number;
        //.. current branch number
        public long? il_supplier_number;
        //.. current supplier number
        public long il_order_number;
        //.. current order number
        public u_datastore_update ids_order_details;
        //.. data from supplier_order_move
        public u_datastore_update ids_order_move;
        //.. data from supplier_order_details
        //.. errors of order ds`s retrieve
        public long il_last_sql_db_code;
        public string is_last_sql_err_text;
        //.. if one branch, order, supplier was modified -> reretrieve data
        public bool ib_modified;

        public u_order_search()
        {
            constructor();
        }

        public void uf_set_mt_message(bool ab_mt_message)
        {
            ib_mt_message = ab_mt_message;
        }
        public void uf_set_transaction(SQLError a_transaction)
        {
            i_transaction = a_transaction;
        }
        public async Task<FuncResults<long, bool>> uf_order_exists(long al_branch_number, long al_supplier_number, long al_order_number, long al_error_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.ue_order_exists()
            //
            // Returns:         boolean (TRUE if exists, otherwise FALSE)
            //
            // Parameters:     
            // 						value Long 	al_branch_number
            //						value Long	al_supplier_number	
            // 						value Long 	al_order_number
            // 						ref 	Long	al_error_number							
            // Copyright  - Stas
            //
            // Date Created: 30/08/2005
            //
            // Description:	
            // 					checks if entered order number exists in DB (supplier_order_move)
            //					user can enter invoice without order number, so if al_order_number = 0
            //					or al_order_number is null, return TRUE.
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_temp = 0;
            bool lb_return = false;
            lb_return = true;
            if (al_supplier_number == 0 || al_supplier_number == 0)
            {
                return FuncResults.Return(al_error_number,false);
            }
            if (true)
            {
                LoadData(al_order_number, al_supplier_number, al_branch_number, ref ll_temp);
            }

            if (i_transaction.SqlCode == -1)
            {
                if (ib_mt_message)
                {
                    //.. DB Error 

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", i_transaction.SqlErrText, "", "Ok", 1, "#20000");
                }
                lb_return = false;
            }

            else if (i_transaction.SqlCode == 100 || ll_temp == 0)
            {
                if (ib_mt_message)
                {
                    //.. order not exists
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#20005");
                }
                lb_return = false;
            }

            else if (i_transaction.SqlCode == 0)
            {
                il_branch_number = al_branch_number;
                il_supplier_number = al_supplier_number;
                il_order_number = al_order_number;
            }

            al_error_number = i_transaction.SqlCode;
            return FuncResults.Return(al_error_number, lb_return);
        }
        public async Task<FuncResults<long, bool>> uf_order_for_purchase_exists(long al_branch_number, long? al_supplier_number, long al_order_number, long al_error_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_order_for_purchase_exists()
            //
            // Returns:         boolean (TRUE if exists, otherwise FALSE)
            //
            // Parameters:     
            // 						value Long 	al_branch_number
            //						value Long	al_supplier_number	
            // 						value Long 	al_order_number
            // 												
            // Copyright  - Stas
            //
            // Date Created: 30/08/2005
            //
            // Description:	
            // 					checks if entered order number exists in DB (supplier_order_move)
            //					user can enter invoice without order number, so if al_order_number = 0
            //					or al_order_number is null, return TRUE.
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            string ls_order_state = null;
            masofon.masofon.Instance.sqlca = new SQLError(null);
            bool lb_return = false;
            lb_return = true;
            if (al_supplier_number == 0 || al_supplier_number == 0)
            {
                return FuncResults.Return(al_error_number, false);
            }
            if (true)
            {
                LoadData1(al_order_number, al_supplier_number, al_branch_number, ref ls_order_state);
            }

            if (masofon.masofon.Instance.sqlca.SqlCode == -1)
            {
                if (ib_mt_message)
                {
                    //.. DB Error 

                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", masofon.masofon.Instance.sqlca.SqlErrText, "", "Ok", 1, "#20000");
                }
                lb_return = false;
            }

            else if (masofon.masofon.Instance.sqlca.SqlCode == 100)
            {
                if (ib_mt_message)
                {
                    //.. order not exists		
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#20005");
                }
                lb_return = false;
                //.. SPR = P0000228
            }
            else if (ls_order_state != null && ls_order_state.ToLower(CultureInfo.InvariantCulture) != "c" && ls_order_state.ToLower(CultureInfo.InvariantCulture) != "p")
            {
                if (ib_mt_message)
                {
                    //.. Order exists but already Closed. Please enter new order number
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "Ok", 1, "#20008");
                }
                lb_return = false;
            }
            //....
            return FuncResults.Return(al_error_number, lb_return);
        }
        public void uf_set_branch_number(long al_branch_number)
        {
            il_branch_number = al_branch_number;
            ib_modified = true;
        }
        public virtual void uf_set_order_number(long al_order_number)
        {
            il_order_number = al_order_number;
            ib_modified = true;
        }
        public virtual void uf_set_supplier_number(long? al_supplier_number)
        {
            il_supplier_number = al_supplier_number;
            ib_modified = true;
        }
        public long uf_get_order_material_color(long al_material_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_get_order_material_color()
            //
            // Returns:         Long 	-> color of material from supplier_order_details
            //
            // Parameters:     
            // 						value Long 	al_material_number	---> current material number
            //						ref	Long	al_error_number		---> if error occurce return 1, otherwise 0
            // 												
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            //
            // Description:	
            // 					if some retrieve neeeded (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move
            //					if material exists in order, return color number, otherwise return null
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long? ll_row = -1;
            long ll_order_material_color = 0;

            ll_order_material_color = default(long);

            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return ll_order_material_color;
                }
            }
            
            
            ll_row = ids_order_details.Find("material_number == " + al_material_number.ToString(), 0, ids_order_details.RowCount());
            if (ll_row >= 0)
            {
                
                ll_order_material_color = ids_order_details.GetItemValue<long>((int)ll_row, "color");
            }
            else
            {

                ll_order_material_color = default(long);
            }
            return ll_order_material_color;
        }
        public long uf_get_order_material_serial_number(long al_material_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_get_order_material_serial_number()
            //
            // Returns:         Long		--> serial-number of material in order (if exists)
            //
            // Parameters:     
            // 						value Long 		al_material_number	---> current material number
            //												
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            //
            // Description:	
            // 					if some retrieve neeeded (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move
            //					if material exists in the order, gets info for invoice/pack sort by
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            long? ll_row = 0;
            long ll_sort_number = 0;
            
            
            ll_row = ids_order_details.Find("material_number == " + al_material_number.ToString(), 0, ids_order_details.RowCount());
            if (ll_row >= 0)
            {
                
                ll_sort_number = ids_order_details.GetItemValue<long>((int)ll_row, "sort_number");
                ll_sort_number /= 10;
            }
            else
            {
                ll_sort_number = -1;
            }
            return ll_sort_number;
        }
        public bool uf_material_assigns_to_order(long al_material_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_material_assigns_to_order()
            //
            // Returns:         Integer
            //
            // Parameters:      value Long al_material_number	--> current material
            // 
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            // 
            // Description:	
            //					if some retrieve neeeded (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move
            //					if material exists in the order return TRUE, otherwise FALSE
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            long? ll_row = 0;
            
            
            ll_row = ids_order_details.Find("material_number == " + al_material_number.ToString(), 0, ids_order_details.RowCount());
            return (ll_row >= 0);
        }
        public decimal uf_get_order_discount()
        {
            long? ll_row = 0;
            decimal ldec_discount = default(decimal);
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return 0;
                }
            }
            
            
            ll_row = ids_order_move.Find("order_number == " + il_order_number.ToString(), 0, ids_order_move.RowCount());
            
            ldec_discount = ids_order_move.GetItemValue<decimal>((int)ll_row, "discount_percent");
            if (ldec_discount == 0)
            {
                ldec_discount = 0;
            }
            return ldec_discount;
        }
        public bool uf_get_order_material_price(long al_material_number, ref decimal adec_material_price, ref decimal adec_material_price_after_discount, ref decimal adec_material_discount_percent) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_get_order_material_price()
            //
            // Returns:         None
            //
            // Parameters:     
            // 						value Long 		al_material_number	---> current material number
            //						ref 	dec{2}	adec_material_price	 
            //						ref 	dec{2}	adec_adec_material_price_after_discount
            // 												
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            //
            // Description:	
            // 					if some retrieve neeeded (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move
            //					if material exists in the order, gets 
            //													material price & material_price_after_discount
            //					
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            long? ll_row = 0;
            
            
            ll_row = ids_order_details.Find("material_number == " + al_material_number.ToString(), 0, ids_order_details.RowCount());
            if (ll_row >= 0)
            {
                
                adec_material_price = ids_order_details.GetItemValue<decimal>((int)ll_row, "material_price");
                
                adec_material_price_after_discount = ids_order_details.GetItemValue<decimal>((int)ll_row, "material_price_after_discount");
                
                adec_material_discount_percent = ids_order_details.GetItemValue<decimal>((int)ll_row, "material_discount_percent");
                // SharonS - 2008-04-17 - 1.2.30.3 - CR#1085 - Check if is null, and set 0 instead.
                if (adec_material_price == 0)
                {
                    adec_material_price = 0;
                }
                if (adec_material_price_after_discount == 0)
                {
                    adec_material_price_after_discount = 0;
                }
                if (adec_material_discount_percent == 0)
                {
                    adec_material_discount_percent = 0;
                }
                // End
                return true;
            }
            else
            {
                adec_material_price = 0;
                adec_material_price_after_discount = 0;
                adec_material_discount_percent = 0;
                return false;
            }
        }
        public decimal uf_get_order_material_quantity(long al_material_number) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_get_order_material_quantity()
            //
            // Returns:         		Long		--> material quantity of material in order (if exists)
            //
            // Parameters:     
            // 						value Long 		al_material_number	---> current material number
            //												
            // Copyright 		 - SharonS
            //
            // Date Created: 18/09/2005
            //
            // Description:	if some retrieve neeeded (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move
            //					if material exists in the order, gets material quantity
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long? ll_row = 0;
            decimal ldec_material_quantity = default(decimal);
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            // AlexKh - 1.2.46.0 - 2010-07-08 - CR#1138 - find material by order number
            
            
            ll_row = ids_order_details.Find("material_number == " + al_material_number.ToString() + " and order_number == " + il_order_number.ToString(), 0, ids_order_details.RowCount());
            //ll_row = ids_order_details.Find("material_number = " + String(al_material_number),1,ids_order_details.RowCount())
            if (ll_row >= 0)
            {
                
                ldec_material_quantity = ids_order_details.GetItemValue<decimal>((int)ll_row, "material_quantity");
            }
            else
            {
                ldec_material_quantity = 0;
            }
            return ldec_material_quantity;
        }
        public  int uf_set_order_state(string as_from_state, string as_to_state)
        {
            UpdateData(as_to_state, as_from_state);
            return 1;
        }
        public virtual int uf_set_order_state(string as_order_state)
        {
            long ll_rows = 0;
            int li_ret = 0;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            
            ll_rows = ids_order_move.RowCount();
            if (ll_rows != 1)
            {
                return -1;
            }
            
            ids_order_move.SetItem(0, "state", Convert.ToString(as_order_state));
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                li_ret = (int) ids_order_move.Update(unitOfWork);
            }
            if (li_ret < 0)
            {
                il_last_sql_db_code = ids_order_move.uf_get_last_sql_db_code();
                is_last_sql_err_text = ids_order_move.uf_get_last_sql_err_text();
                return -1;
            }
            return 1;
        }
        public virtual int uf_set_item_quantity_within_invoice(long al_material_number, decimal adec_material_quantity) //--------------------------------------------------------------------
        {
            //Function:			public u_material_update.uf_set_item_quantity_within_invoice()
            //
            //Returns:         		Long
            //
            //Parameters:      	value long 		al_material_number
            //						value decimal	adec_material_quantity
            // 
            //Copyright 			SharonS
            //
            //Date Created: 		25/10/2007
            //
            //Description:			if order exists at mini-terminal input (invoice/packlist) 
            //					1) gets all appropriate data from supplier_order_details
            //					2) if some material exists in mini-terminal input and in supplier_order_details
            //						sets material qty into supplier_order_details,
            //						if not, sets ordered_not_received = 'y' -> material was ordered but 
            //						not in invoice.
            //
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_rows = 0;
            long? ll_found = 0;
            int li_ret = 0;
            string ls_find = null;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            
            ll_rows = ids_order_details.RowCount();
            ls_find = "material_number == " + al_material_number.ToString();
            
            ll_found = ids_order_details.Find(ls_find, 0, (int)ll_rows);
            if (ll_found >= 0)
            {
                
                ids_order_details.SetItem(ll_found.Value, "quantity_within_invoice", adec_material_quantity.ToString());
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    li_ret = (int) ids_order_details.Update(unitOfWork);
                }
                if (li_ret < 1)
                {
                    il_last_sql_db_code = ids_order_details.uf_get_last_sql_db_code();
                    is_last_sql_err_text = ids_order_details.uf_get_last_sql_err_text();
                    return -1;
                }
            }
            return 1;
        }
        public bool uf_get_order_pay_agr(long al_pay_agr, long al_num_of_pays) //--------------------------------------------------------------------
        {
            //Function:			public u_order_search.uf_get_order_pay_agr()
            //
            // Returns:         		boolean		--> True - Success, False - Failure.
            //
            // Parameters:     	By reference - Long - Order payment agreement.
            // 						By reference - Long - Number of  payments.
            //												
            // Description:	If some retrieve needed (supplier || branch || order numbers changed)
            //					retrieve supplier_order_details and supplier_order_move.
            //					Get and return order payment agreement.
            //					
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            	Author          Task/CR    Comments
            // -------------------------------------------------------------------------------
            // 21/04/2008		SharonS		CR#1075		Initial version
            //------------------------------------------------------------------------------

            long? ll_row = 0;
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return false;
                }
            }
            
            
            ll_row = ids_order_move.Find("order_number == " + il_order_number.ToString(), 0, ids_order_move.RowCount());
            
            al_pay_agr = ids_order_move.GetItemValue<long>((int)ll_row, "payment_code_number");
            if (al_pay_agr == 0)
            {
                al_pay_agr = 0;
            }
            
            al_num_of_pays = ids_order_move.GetItemValue<long>((int)ll_row, "payments_number");
            if (al_num_of_pays == 0)
            {
                al_num_of_pays = 0;
            }
            return true;
        }
        public int uf_set_quantity_within_invoice_no_upd(long al_material_number, decimal adec_material_quantity) //**********************************************************************************************
        {
            //*Object:				u_order_search
            //*Function Name:	uf_set_quantity_with_invoice_no_upd
            //*Purpose: 			Set Quantity within invoice without order update. Will be updated later
            //*Arguments: 		value long 		al_material_number
            //*						value decimal	adec_material_quantity
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*08-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //************************************************************************************************

            long ll_rows = 0;
            long? ll_found = 0;
            int li_ret = 0;
            string ls_find = null;
            decimal ldec_qurrent_quantity = default(decimal);
            if (uf_retrieve_needed())
            {
                if (!uf_retrieve())
                {
                    return -1;
                }
            }
            
            ll_rows = ids_order_details.RowCount();
            ls_find = "material_number == " + al_material_number.ToString();
            
            ll_found = ids_order_details.Find(ls_find, 0, (int)ll_rows);
            if (ll_found >= 0)
            {
                
                ldec_qurrent_quantity = ids_order_details.GetItemValue<decimal>((int)ll_found, "quantity_within_invoice");
                if (ldec_qurrent_quantity == 0)
                {
                    ldec_qurrent_quantity = 0;
                }
                
                ids_order_details.SetItem(ll_found.Value, "quantity_within_invoice", (Convert.ToString(adec_material_quantity + ldec_qurrent_quantity)));
            }
            ib_modified = false;
            return 1;
        }
        public int uf_update_order_details() //**********************************************************************************************
        {
            //*Object:				u_order_search
            //*Function Name:	uf_update_order_details
            //*Purpose: 			Update details for order
            //*Arguments: 		None
            //*Return:				None
            //*Date				Programer		Version	Task#	 		Description
            //*------------------------------------------------------------------------------------------------
            //*08-06-2010		AlexKh			1.2.46.0	CR#1138		Initial version
            //************************************************************************************************

            long ll_rows = 0;
            long ll_found = 0;
            long ll_error_number = 0;
            int li_ret = 0;
            string ls_find = null;
            string ls_error_text = null;
            decimal ldec_qurrent_quantity = default(decimal);
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                li_ret = (int) ids_order_details.Update(unitOfWork);
            }
            if (li_ret < 1)
            {
                ll_error_number = ids_order_details.uf_get_last_sql_db_code();
                ls_error_text = ids_order_details.uf_get_last_sql_err_text();
                return -1;
            }
            return 1;
        }
        protected bool uf_retrieve_needed() //--------------------------------------------------------------------
        {
            //Function:			private u_order_search.uf_retrieve_needed()
            //
            // Returns:         Boolean		--> TRUE if retrieve needed
            //
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            // 
            // Description:	
            //					if order datastores not retrived or (supplier || branch || supplier) changed
            //					reretrive ds
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            
            
            if ((ids_order_details.RowCount() == 0 && ids_order_move.RowCount() == 0) || ib_modified)
            {
                return true;
            }
            return false;
        }
        public virtual bool uf_retrieve() //--------------------------------------------------------------------
        {
            //Function:			private u_order_search.uf_retrieve()
            //
            // Returns:         boolean
            //
            // Copyright  - Stas
            //
            // Date Created: 18/09/2005
            // 
            // Description:	
            //					if  (supplier || branch || order numbers ) = 0 then do no retrieve ds`s
            //					otherwise retrieve supplier_order_details and supplier_order_move.
            //					if error occures save error number and message
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            if (il_branch_number == 0)
            {
                return false;
            }
            if (il_supplier_number == 0)
            {
                return false;
            }
            if (il_order_number == 0)
            {
                return false;
            }
            //.. if error occures save error number and message
            // SharonS - 0.0.0.1 - 2007-03-21 - CR#1006 - Change arguments order - Start
            //IF ids_order_details.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0 THEN
            
            if (ids_order_details.Retrieve(il_branch_number, il_order_number, il_supplier_number) < 0)
            {
                // End
                il_last_sql_db_code = ids_order_details.uf_get_last_sql_db_code();
                is_last_sql_err_text = ids_order_details.uf_get_last_sql_err_text();
                return false;
            }
            //.. if error occures save error number and message
            
            if (ids_order_move.Retrieve(il_branch_number, il_supplier_number, il_order_number) < 0)
            {
                il_last_sql_db_code = ids_order_move.uf_get_last_sql_db_code();
                is_last_sql_err_text = ids_order_move.uf_get_last_sql_err_text();
                return false;
            }
            //.. order data succesfully retrieved
            return true;
        }
       

        public void constructor() //.. connect to sqlca by default
        {
            i_transaction = masofonAlias.masofon.Instance.sqlca;
            ids_order_details = new u_datastore_update();
            ids_order_details.SetRepository(new d_sql_supplier_order_detailsRepository(), false);

            //ids_order_details = new d_sql_supplier_order_detailsRepository();
            ids_order_move = new u_datastore_update();
            ids_order_move.SetRepository(new d_sql_supplier_order_moveRepository(), false);
            //ids_order_move = new d_sql_supplier_order_moveRepository();
        }
        
        public void LoadData(Int64 al_order_number, Int64 al_supplier_number, Int64 al_branch_number, ref long ll_temp)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_order_number }, { "@1", al_supplier_number }, { "@2", al_branch_number } };
                    string sql = "SELECT Count(*) FROM supplier_order_move WHERE order_number = @0 AND supplier_number = @1 AND branch_number = @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_temp = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(Int64 al_order_number, Int64? al_supplier_number, Int64 al_branch_number, ref string ls_order_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_order_number }, { "@1", al_supplier_number }, { "@2", al_branch_number } };
                    string sql = "SELECT IsNull(state, '') FROM supplier_order_move WHERE order_number = @0 AND supplier_number = @1 AND branch_number = @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_order_state = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(string as_to_state, string as_from_state)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_to_state }, { "@1", il_branch_number }, { "@2", il_order_number }, { "@3", il_supplier_number }, { "@4", as_from_state } };
                    string sql = "UPDATE supplier_order_move SET supplier_order_move.state = @0 WHERE supplier_order_move.branch_number = @1 AND supplier_order_move.order_number = @2 AND supplier_order_move.supplier_number = @3 AND supplier_order_move.state = @4";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
