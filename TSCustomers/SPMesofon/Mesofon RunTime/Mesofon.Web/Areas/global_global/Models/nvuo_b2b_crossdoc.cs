using System;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using Mesofon.Models;

namespace global
{
	// Creation Time:   08/11/2015 10:20:54 (Utc)
	// Comments:        
	// 
	public class nvuo_b2b_crossdoc : nvuo_b2b_transactions
	{
		public IRepository ids_declines;
		public IRepository ids_decline_reasons;
		public string is_message_type;
		public string is_message_sub_type;
		public long il_response_no;
		public DateTime? idt_parent_doc_suply_dt;
		public DateTime? idt_parent_doc_dt;
		//RonY@15/01/2015 1.12.49.63 - SPUCM00005162	 - add  MALOG_999_EXTRA and MALOG_997_EXTRA
		public const string B2B_DECLINE_HEADER_34 = "34";
		public const string B2B_CONFIRM_HEADER_29 = "29";
		public const string MALOG_999_EXTRA = "729017200113";
		public const string MALOG_997_EXTRA = "7290172900601";
		public const long CROSSDOC_DISTRIBUTOR = 100;
		public string is_error_message;
		// AlexKh - 1.1.1.18 - 2014-02-19 - CR#1171(SPUCM00004718) - set marlog distributor
		public long il_marlog_distributor;
		public long il_marlog_number;
		public int uf_insert_header(long al_row) //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_insert_header
			//*Purpose: 			Insert haeder section into the XML DW.
			//*Arguments: 		Long - Row number in the XML DW.
			//*Return:				Integer: 1  - succeeded.
			//*									-1 - failed.
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			string ls_res_date_time = null;
			long ll_shipment_number = 0;
			long ll_ret = 0;
			DateTime? ldt_date_time = null;
			DateTime? ldt_parent_date_time = null;
			// DocumentNo
			
			if (ids_declines.RowCount() > 0)
			{
				
				ll_shipment_number = (long)ids_declines.GetItemValue<double>(0, "shipment_pallet_number");
				 
				ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "documentno", ll_shipment_number);
			}
			// DeliveryDate
			
			ldt_date_time = ids_declines.GetItemValue<DateTime>((int)al_row, "last_update_datetime");
			ls_res_date_time = ldt_date_time.Value.ToString("yyyyMMddhhmm");
			if (isempty_stringClass.isempty_string(ls_res_date_time))
			{
				return -1;
			}
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "deliverydate", ls_res_date_time);
			// Extra
			//RonY@15/01/2015 1.12.49.63 - SPUCM00005162 - add and remark - start
			if (il_marlog_number == 999)
			{
				 
				ids_decline_xml_temp.SetItem(al_row, "extra", MALOG_999_EXTRA);
			}
			else if (il_marlog_number == 997)
			{
				 
				ids_decline_xml_temp.SetItem(al_row, "extra", MALOG_997_EXTRA);
			}
			//ll_ret = ids_decline_xml_temp.SetItem(al_row, "extra", 0) 
			//RonY@15/01/2015 1.12.49.63 - SPUCM00005162 - add and remark - end
			// RetailNo
			if (isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi))
			{
				return -1;
			}
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "retailno", masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi);
			// CDNo
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "cdno", masofonAlias.masofon.Instance.qs_parameters.edi_number);
			// CDName
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "cdname", masofonAlias.masofon.Instance.qs_parameters.company_name);
			// Rotation
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "rotation", "0");
			// DocConfrType
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "docconfrtype", B2B_CONFIRM_HEADER_29);
			// DocumentType
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "documenttype", "9");
			return 1;
		}
		public int uf_insert_details(long al_row) //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_insert_details
			//*Purpose: 			Insert the Deatails section of the order XML
			//*Arguments: 		Long	al_row
			//*Return:				Integer - 1 succeed.
			//*									-1 not  succeed.
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			string ls_carton_barcode = null;
			string ls_supplier_edi = null;
		//	char lc_doc_type = '\0';
			long ll_sub_supplier_number = 0;
			// SubSupplier
			
			ll_sub_supplier_number = ids_declines.GetItemValue<long>((int)al_row, "sub_supplier_number");
			ls_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(ll_sub_supplier_number);
			 
			ids_decline_xml.SetItem(al_row, "subsupplier", ls_supplier_edi);
			// CartonBarcode
			
			ls_carton_barcode = ids_declines.GetItemValue<string>(al_row, "carton_barcode");
			 
			ids_decline_xml.SetItem(al_row, "cartonbarcode", ls_carton_barcode);
			// DisagrType
			// Extra
			//RonY@15/01/2015 1.12.49.63 - SPUCM00005162 - add and remark - start
			if (il_marlog_number == 999)
			{
				 
				ids_decline_xml.SetItem(al_row, "extradetail", MALOG_999_EXTRA);
			}
			else if (il_marlog_number == 997)
			{
				 
				ids_decline_xml.SetItem(al_row, "extradetail", MALOG_997_EXTRA);
			}
			//ids_decline_xml.SetItem(al_row, "extradetail", 0)
			//RonY@15/01/2015 1.12.49.63 - SPUCM00005162 - add and remark - end
			return 1;
		}
		public bool uf_save_as_xml() //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_save_as_xml
			//*Purpose: 			Save the DW as XML file.
			//*Arguments: 		None
			//*Return:				Integer: 1	-Success
			//*									-1 -Failed.
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			return Convert.ToBoolean(ids_decline_xml.SaveToXml(Convert.ToString(is_file_spec)));
		}
		public async Task<int> uf_write_log(string as_message) //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_write_log
			//*Purpose: 			Write into log file
			//*Arguments: 		String as_message- the message to write into the file.
			//*Return:				Integer: 1	-Success
			//*									-1 -Failed.
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			int li_file = 0;
			int li_file_open = 0;
			string ls_log_file_name = null;
			string ls_message = null;
			string ls_doc_type = null;
			string ls_msg_header = null;
			string ls_log_mode = null;
		//	bool ib_log_mode = false;
			ls_msg_header = "------------------------------------------------------------------------------------------------------------------------" + "\r" + "\n";
			ls_msg_header += "Employee Number: " + masofonAlias.masofon.Instance.gs_vars.active_owner.ToString() + " Date: " + masofonAlias.masofon.Instance.set_machine_time().ToString() + "\r" + "\n";
			ls_msg_header += "Response number: " + il_response_no.ToString() + " Response type: " + is_message_type + "\r" + "\n";
			as_message = ls_msg_header + as_message;
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log
		    await masofonAlias.masofon.Instance.guo_logs_in_table_service.uf_write_log_table(as_message, u_logs_in_table_service.SERVICE_CODE_MASOFON_LOG, 1, "nvuo_b2b_crossdoc.uf_write_log", "", "", 3);
			// Log on/off
			if (!masofonAlias.masofon.Instance.gb_b2b_log)
			{
				return 1;
			}
			//ls_log_file_name = "c:\NET-POS\log\" + String(gs_vars.branch_number) + "_DECLINE_MSG.log"
			//
			//li_file_open = FileOpen(ls_log_file_name, LineMode!, Write!, LockWrite!, Append!)
			//IF li_file_open > 0 THEN
			//	li_file = FileWrite(li_file_open, as_message)
			//	IF li_file < 0 OR IsNull(li_file) THEN
			//		guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not write the log file.")
			//		RETURN -1
			//	END IF
			//	li_file = FileClose(li_file_open)
			//	IF li_file < 0 OR IsNull(li_file) THEN
			//		guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not close the log file.")
			//		RETURN -1
			//	END IF
			//	RETURN 1
			//ELSE
			//	guo_msg.uf_msg("ERROR", "", "stopsign!", "Can not open the log file.")
			//	RETURN -1
			//END IF
			//
			//
		    return 0;
		}
		public int uf_insert_envelope(long al_row) //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_insert_envelope
			//*Purpose: 			Insert envelope section into the XML DW.
			//*Arguments: 		Long	al_row
			//*Return:				Boolean		1/-1
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			long ll_find = 0;
			long ll_ret = 0;
			string ls_organization_edi = null;
			string ls_message = null;
			string ls_message_time = null;
			string ls_message_date = null;
			ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
			if (isempty_stringClass.isempty_string(ls_organization_edi))
			{
				return -1;
			}
			// AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - set marlog distributor	
			//is_parent_supplier_edi = f_is_branch_supplier_b2b(MARLOG_DISTRIBUTOR)
			is_parent_supplier_edi = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(il_marlog_distributor);
			if (isempty_stringClass.isempty_string(is_parent_supplier_edi))
			{
				return -1;
			}
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "sender", ls_organization_edi);
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "receiver", is_parent_supplier_edi);
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "doctype", is_message_type);
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "aprf", is_message_sub_type);
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "snrf", "");
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "ackn", "1"); // 1 - Required        
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "testind", "1"); // 1 - Test   
			ls_message_date = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("yyyyMMdd");
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "messdate", ls_message_date);
			ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("hhmmss");
			 
			ll_ret = (long)ids_decline_xml_temp.SetItem(al_row, "messtime", ls_message_time);
			return 1;
		}
		public async Task<FuncResults<int, IRepository[], bool>> uf_send_transaction( int ai_file_status, string as_supplier_edi, IRepository[] ads_data) //*********************************************************************
		{
			//*Object:				nvuo_b2b_crossdoc
			//*Function Name:	uf_send_transaction
			//*Purpose: 			Create the B2B message XML file.
			//*Arguments: 		Integer - file_status
			//*Return:				BOOLEAN - 	TRUE - Creation of B2B message XML file succeded.
			//										FALSE - Creation of B2B message XML file failed.
			//*Date				Programer		Version		Task#	 		Description
			//*----------------------------------------------------------------------
			//*29-02-2012		AlexKh			1.2.48.15		CR#1152	Initial version
			//*********************************************************************
			
			int li_decline_row = 0;
			int li_index = 0;
			long li_decline_row_count = 0;
			long ll_row = 0;
			long ll_decline_no = 0;
			long ll_row_copy = 0;
			long ll_row_no = 0;
			long ll_ret = 0;
			long ll_i = 0;
			string ls_message = null;
			// Check validity
			if (CollectionExtensions.UBound(ads_data) < 0)
			{
				return FuncResults.Return(ai_file_status, ads_data, false);
			}
			
			li_decline_row_count = ads_data[1].RowCount();
			if (li_decline_row_count < 1)
			{
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			ids_declines = ads_data[1];
			
			ll_ret = ids_declines.RowCount();
			is_supplier_edi = as_supplier_edi;
			is_parent_supplier_edi = is_supplier_edi;
			// End
			// Create and define IRepository
			
			ids_decline_xml.Reset();
			
			ids_decline_xml_temp.Reset();
			
			ll_row = ids_decline_xml_temp.Insert(0);
			// Set the envalope data in the XML DW
			if (this.uf_insert_envelope(ll_row) < 0)
			{
				//MessageBox("כישלון", "שים לב- כישלון בכתיבת מעטפת מסר קרוסדוק - קרוסדוק לא ישלח במסר דיגיטלי",StopSign!)	
				ls_message = "Decline - Failed in writing file envelop crossdoc.";
				//AlexKh
				is_error_message = ls_message;
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			// Set the header data in the XML DW
			if (this.uf_insert_header(ll_row) < 0)
			{
				//MessageBox("כישלון","שים לב-כישלון בכתיבת כותרת מסר קרוסדוק - קרוסדוק לא ישלח במסר דיגיטלי",StopSign!)	
				ls_message = "Crossdoc Shipment - Failed in writing file header crossdoc.";
				//AlexKh
				is_error_message = ls_message;
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			// Load the xml DW
            ids_decline_xml.Export(((d_crossdoc_xmlRepository)ids_decline_xml).XMLExport);
            
			for (li_index = 1; li_index <= (int)li_decline_row_count; li_index++)
			{
				ll_row_copy = (long)ids_decline_xml_temp.RowsCopy(0, 1, ModelBuffer.Primary, ids_decline_xml, (ids_decline_xml.RowCount() + 1), ModelBuffer.Primary);
				
				if (this.uf_insert_details((long)ids_decline_xml.RowCount() - 1) < 0)
				{
					//MessageBox("כישלון"," שים לב -כישלון בכתיבת מסר קרוסדוק - לא ישלח במסר דיגיטלי",StopSign!)	
					ls_message = "Crossdoc Shipment - Failed in writing file details crossdoc.";
					//AlexKh
					is_error_message = ls_message;
				    await uf_write_log(ls_message);
					ai_file_status = 0;
				    return FuncResults.Return(ai_file_status, ads_data, false);
                }
			}
			
			for (ll_i = 0; ll_i < ids_declines.RowCount(); ll_i++)
			{
				 
				ids_declines.SetItem(ll_i, "state", "f");
				 
				ids_declines.SetItem(ll_i, "last_update_datetime", masofonAlias.masofon.Instance.set_machine_time());
			}
		    using (UnitOfWork unitOfWork = new UnitOfWork())
		    {
                ll_ret = ids_declines.Update(unitOfWork);
		    }
		    // Create xml file from datawindow
			if (!uf_save_as_xml())
			{
				//MessageBox("כישלון","שים לב - כישלון ביצירת\כתיבת מסר קרוסדק - לא ישלח דיגיטלית",StopSign!)	
				ls_message = "Crossdoc Shipment - Failed to Create/Write to File crossdoc.";
				//AlexKh
				is_error_message = ls_message;
			    await uf_write_log(ls_message);
				ai_file_status = 0;
			    return FuncResults.Return(ai_file_status, ads_data, false);
            }
			else
			{
				ai_file_status = 1;
			}
		    return FuncResults.Return(ai_file_status, ads_data, true);
        }
        public async Task<string> uf_b2b_send_crossdoc_approve(long al_branch_number, long al_shipment_number, long al_marlog_number) //**********************************************************************************************
        {
            //*Object:				nvuo_b2b_crossdoc
            //*Function:			uf_b2b_send_crossdoc_approve
            //*Purpose:			Send a digital message for crossdoc shipment
            //*			
            //*Arguments:		Pass By		Argument Type		Argument Name
            //*						-------------------------------------------------------------------------
            //*						Value			Long				al_branch_number - Branch number.
            //*						Value			Long				al_shipment_number - shipment number.
            //*										
            //*Return:								Boolean: TRUE - Success
            //*													FALSE - Faliure
            //*
            //*Date				Programer	Version		Task#			Description
            //*-----------------------------------------------------------------------------------------
            //*28-02-2012		AlexKh		1.2.48.15 	CR#1152		Initial
            //******************************************************************************************

            string ls_msg_type = null;
            string ls_msg_sub_type = null;
            string ls_sup_edi_no = null;
            string ls_filter = null;
            int li_decline_sts = 0;
            int li_filter = 0;
            long ll_ret = 0;
            long ll_row_count = 0;
            long ll_row = 0;
            long ll_reject_row = 0;
            long ll_order_no = 0;
            long ll_i = 0;
            bool lb_ret = false;
            IRepository lds_crossdoc_pack_list_for_xml;
            IRepository lds_declines_reasons_for_doc_for_xml;
            IRepository[] lds_declines_data = new IRepository[2];
            is_message_type = "MMCNFXML";
            is_message_sub_type = "CARTP2C";
            is_error_message = "";
            ////Call uf_init(), in order to check if the file target directory (outgoing messages) exist and create the file name 
            if (!uf_init(Convert.ToString(is_message_type), CROSSDOC_DISTRIBUTOR, al_shipment_number, al_branch_number))
            {
                return is_error_message;
            }
            il_marlog_number = al_marlog_number; //RonY@15/01/2015 1.12.49.63 - SPUCM00005162
            // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - set marlog_distributor
            il_marlog_distributor = await f_is_marlog_distributorClass.f_is_marlog_distributor(al_marlog_number, 0);




            lds_crossdoc_pack_list_for_xml = new d_mini_terminal_crossdoc_pack_listRepository();
            // AlexKh - 1.1.20.0 - 2014-05-01 - SPUCM00004718  - retrieve also by marlog_number
            //ll_row_count = lds_crossdoc_pack_list_for_xml.Retrieve(al_branch_number, al_shipment_number)

            ll_row_count = lds_crossdoc_pack_list_for_xml.Retrieve(al_branch_number, al_shipment_number, al_marlog_number);
            // If Retrieve failed
            if (ll_row_count < 0)
            {
                is_error_message = "משיכת קרטונים קרוסדוק נכשלה";
                return is_error_message;
            }

            ll_ret = lds_crossdoc_pack_list_for_xml.SetFilter("state == \"c\"");

            ll_ret = lds_crossdoc_pack_list_for_xml.Filter();

            ll_ret = lds_crossdoc_pack_list_for_xml.SetSort("row_serial_number ASC");

            ll_ret = lds_crossdoc_pack_list_for_xml.Sort();

            ll_row_count = lds_crossdoc_pack_list_for_xml.RowCount();
            if (ll_row_count > 0)
            {
                lds_declines_data[1] = lds_crossdoc_pack_list_for_xml;
                ls_sup_edi_no = f_is_branch_supplier_b2bClass.f_is_branch_supplier_b2b(CROSSDOC_DISTRIBUTOR);
                var result = await uf_send_transaction(li_decline_sts, ls_sup_edi_no, lds_declines_data);
                if (!result.Retrieve(out li_decline_sts,out lds_declines_data))
                {
                    is_error_message = "Failure in sending XML file. - " + is_error_message;
                }
                else
                {
                    is_error_message = "XML file was send to the marlog successfully. - " + is_error_message;
                }
            }
            return is_error_message;
        }
        public nvuo_b2b_crossdoc() : base()
		{
		    constructor();

		}
		
		public void constructor() // Create and define IRepository
		{
			
			// Code was removed based on 'nvuo_b2b_transactions.constructor() : Long' mappings.
			// MyBase.constructor()
			
			ids_decline_xml = new Mesofon.Repository.d_crossdoc_xmlRepository();
			ids_decline_xml_temp = new Mesofon.Repository.d_crossdoc_xmlRepository();
		}
		public void destructor()
		{
			
			// Code was removed based on 'nvuo_b2b_transactions.destructor() : Long' mappings.
			// MyBase.destructor()
			
			if (ids_decline_xml != null)
			{



			}
			if (ids_decline_xml_temp != null)
			{



			}
			if (ids_decline_reasons != null)
			{



			}
		}
	}
}
