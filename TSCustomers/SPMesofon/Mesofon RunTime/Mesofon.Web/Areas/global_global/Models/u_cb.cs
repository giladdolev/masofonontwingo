using System;
using System.Web.VisualTree.Elements;

namespace global
{
	// Creation Time:   08/11/2015 10:20:59 (Utc)
	// Comments:        
	// 
	public class u_cb : ButtonElement
	{
		//.. pointer to parent object
		public object io_parent_type;
		public WindowElement iw_parent;
		//.. value in close statment
		public long il_return_value;
		//.. if true --> closewithreturn, otherwise simple close
		public bool ib_return_flag;
		public u_cb()
		{
		    constructor();

		}
		
		public void constructor() // Save parent.
		{
			 
			io_parent_type = this.Parent.GetType();
			if (this.Parent.GetType() == typeof(WindowElement))
			{
				 
				iw_parent = this.Parent as WindowElement;
			}
			il_return_valueProperty = 0;
			ib_return_flagProperty = false;
		}
		public long il_return_valueProperty
		{
			get { return this.il_return_value; }
			set { this.il_return_value = value; }
		}
		public bool ib_return_flagProperty
		{
			get { return this.ib_return_flag; }
			set { this.ib_return_flag = value; }
		}
        public void u_cb_Click(object sender, EventArgs e)
        {

        }

	    public void uf_set_return_value(long al_return_value)
	    {
	       il_return_valueProperty = al_return_value;
	    }
    }
}
