using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Globalization;
using System.Web.VisualTree;
using Common.Transposition.Extensions;
using Mesofon.Common.Global;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using System.Data;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.Data;


namespace global
{
    // Creation Time:   08/11/2015 10:20:53 (Utc)
    // Comments:        
    // 

    public class nv_translator
    {
        public bool ib_create_expressions;
        public bool ib_display_translate_window;
        public IRepository dw_dic;
        public LabelElement ist_charset;
        public bool multi_lingual_is_active;
        public string direction;
        public string lang_font;
        public string is_keyboard_layout;
        public string is_font_charset;
        public int il_current_lang; // date format from registry
        public int ii_return_to_lang; // date format from registry
        public int ii_font_decrease; // date format from registry
        public int ii_charset_number; // date format from registry
        public string is_date_format;
        public const long il_hebrew_language = 3;
        protected long il_min_dw_language_font;
        public async Task<string> fnv_find_exp_in_dic(string as_search_exp)
        {
            long? ll_row = 0;
            long ll_space_counter = 0;
            long ll_q_pos = 0; //.. position of " in a string
            string ls_result = null;
            string ls_temp = null;
            char lc_start_char = '\0';
            //.. if multi-lingual mode not active or it is hebrew mode -> exit without translation
            if (!fnv_is_multy_lingual() || fnv_is_hebrew_mode())
            {
                ls_result = as_search_exp;
            }
            else
            {
                if (!isempty_stringClass.isempty_string(as_search_exp))
                {

                    if (dw_dic.RowCount() > 0)
                    {
                        ls_temp = as_search_exp.Trim();
                        //******* we can use this code to translate all expressions that
                        //******* includes new row or/and '"' on left and right sides 
                        if (ls_temp.IndexOf("\r" + "\n") > 0)
                        {
                            if (ls_temp.Substring(ls_temp.IndexOf("\r" + "\n") - 1 - 1, 1) == " " || ls_temp.Substring(ls_temp.IndexOf("\r" + "\n") + 2 - 1, 1) == " ")
                            {
                                ls_temp = ls_temp.Substring(0, ls_temp.IndexOf("\r" + "\n")) + "" + ls_temp.Substring(ls_temp.IndexOf("\r" + "\n") + 2);
                            }
                            else
                            {
                                ls_temp = ls_temp.Substring(0, ls_temp.IndexOf("\r" + "\n")) + " " + ls_temp.Substring(ls_temp.IndexOf("\r" + "\n") + 2);
                            }
                        }
                        //.. if there are " on left and right sides of string -> remove it
                        if (ls_temp.Substring(0, 1) == "\"" && ls_temp.Substring(ls_temp.Length - 1) == "\"")
                        {
                            ls_temp = ls_temp.Substring(0, 1) + "" + ls_temp.Substring(1 + 1);
                            ls_temp = ls_temp.Substring(0, ls_temp.Length) + "" + ls_temp.Substring(ls_temp.Length + 1);
                        }
                        //.. if there are ~" between " in string swap to "
                        if (ls_temp.IndexOf("~\"") > 0)
                        {
                            while (ls_temp.IndexOf("~\"") > 0)
                            {
                                ls_temp = ls_temp.Substring(0, ls_temp.IndexOf("~\"")) + "\"" + ls_temp.Substring(ls_temp.IndexOf("~\"") + 2);
                            }
                        }
                        //.. if there are " + ' (in hebrew) in string swap " to double '
                        //.. becouse find in dw return -5 (error) in that case
                        if (ls_temp.IndexOf("'") > 0)
                        {
                            while (ls_temp.IndexOf("\"") > 0)
                            {
                                ls_temp = ls_temp.Substring(0, ls_temp.IndexOf("\"")) + "''" + ls_temp.Substring(ls_temp.IndexOf("\"") + 1);
                            }
                        }
                        //.. if first character is ':' so remove it from string,
                        //.. count spaces if exists, translate expression and concatenate exp + space + :
                        if ((ls_temp.IndexOf(":") == 1 || ((ls_temp.IndexOf(".") == 1 && ls_temp.IndexOf("..") == 0) || ls_temp.IndexOf("%") == 1)) && ls_temp.Length > 1)
                        {
                            lc_start_char = Convert.ToChar(ls_temp.Substring(0, 1));
                            ls_temp = ls_temp.Substring(ls_temp.Length - ls_temp.Length - 1);
                            ll_space_counter = 0;
                            while (ls_temp.Substring(0, 1) == new string(' ', 1) && !isempty_stringClass.isempty_string(ls_temp))
                            {
                                ls_temp = ls_temp.Substring(ls_temp.Length - ls_temp.Length - 1);

                                ll_space_counter++;
                            }
                        }
                        if (as_search_exp.IndexOf("'") > 0)
                        {


                            ll_row = dw_dic.Find("expressions_name == \"" + ls_temp.TrimStart().TrimEnd() + "\"", 0, dw_dic.RowCount());
                        }
                        else
                        {


                            ll_row = dw_dic.Find("expressions_name == \"" + ls_temp.TrimStart().TrimEnd() + "\"", 0, dw_dic.RowCount());
                        }
                        if (ll_row > 0)
                        {

                            ls_result = dw_dic.GetItemValue<string>(ll_row.Value, "1");
                            if (ls_result == "*********  Add Translation **********" && ib_create_expressions)
                            {

                                ls_result = await uf_update_dicionary(dw_dic.GetItemValue<long>((int)ll_row, "expressions_number"), ls_temp);
                            }
                            if (!fnv_is_hebrew_string(ls_result))
                            {
                                ls_result += new string(' ', (int)ll_space_counter) + lc_start_char.ToString();
                            }
                            else
                            {
                                ls_result = lc_start_char.ToString() + new string(' ', (int)ll_space_counter) + ls_result;
                            }
                        }
                        else
                        {
                            if (ib_create_expressions)
                            {
                                ls_result =  await uf_update_dicionary(0, ls_temp);
                                if (!fnv_is_hebrew_string(ls_result))
                                {
                                    ls_result += new string(' ', (int)ll_space_counter) + lc_start_char.ToString();
                                }
                                else
                                {
                                    ls_result = lc_start_char.ToString() + new string(' ', (int)ll_space_counter) + ls_result;
                                }
                            }
                            else
                            {
                                ls_result = as_search_exp;
                            }
                        }
                    }
                    else
                    {
                        ls_result = as_search_exp;
                    }
                }
                else
                {
                    ls_result = as_search_exp.TrimStart();
                }
            }
            return ls_result;
        }
        public void fnv_kbd_layout()
        {
            string ls_current_kbd_layout = null;
            int i = 1;
            //IF GetKeyBoard() = is_keyboard_layout THEN return // stop process if kbd in requested language
            //ActivateKeyboardLayout (1, 8)								// switch to next language
            //ls_current_kbd_layout = GetKeyBoard () 				// read language code to variable
            while ((ls_current_kbd_layout != is_keyboard_layout) && (i < 10))
            {
                
                i++;
                //ActivateKeyboardLayout (1, 9)							// switch to next language
                //ls_current_kbd_layout = GetKeyBoard () 			// read language code to variable
            }
        }
        public void fnv_save_language()
        {
            ii_return_to_lang = il_current_lang;
        }
        public async Task fnv_translate_dwc(GridElement datawindow_arg, int dwc_width)
        {
            string ls_colname = null;
            string ls_dw_Objects = null;
            string ls_dw_Objects_temp = null;
            string ls_ddlb_value = null;
            string ls_ddlb_expr = null;
            int li_ddlb_index = 0;
            // ------------------ Translate datawindow text fields (labels) ----
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }

            ls_dw_Objects = datawindow_arg.Describe("datawindow", "objects");
            ls_dw_Objects_temp = "\t";
            while (ls_dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_dw_Objects, "\t");
                if (ls_dw_Objects_temp.IndexOf("\t" + ls_colname + "\t") > 0)
                {
                    continue;
                }
                else
                {
                    ls_dw_Objects_temp += ls_colname + "\t";
                }

                if (datawindow_arg.Describe(ls_colname, "Coltype").Substring(0, 4) == "char")
                {
                    datawindow_arg.Modify(ls_colname, "righttoleft", "0");
                    datawindow_arg.Modify(ls_colname, "alignment", "1");
                }

                if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "ddlb")
                {
                    // translate dropdownlistbox and radiobuttons in dw
                    li_ddlb_index = 1;
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + "\t" + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                    // stas 13/6/2004 ------
                }

                else if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "radiobuttons")
                {
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, "\t");
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + "\t" + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                    //-- stas 1/7/2004 ----
                }


                else if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "edit" && datawindow_arg.Describe(ls_colname, "Edit.CodeTable") == "yes")
                {
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + Convert.ToChar(9).ToString() + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                }

                if (datawindow_arg.Describe(ls_colname, "type") != "text")
                {
                    continue;
                }

                if (datawindow_arg.Describe(ls_colname, "text").IndexOf("if(") != 0) //stas 07/06/2004
                {
                    continue;
                }
                datawindow_arg.Modify(ls_colname, "text", await fnv_find_exp_in_dic(datawindow_arg.Describe(ls_colname, "text")));
                datawindow_arg.Modify(ls_colname, "font.face", lang_font);
                datawindow_arg.Modify(ls_colname, "Font.CharSet", "1");
            }
            await fnv_rotate_dwc(datawindow_arg, dwc_width);
        }
        public async Task<string> fnv_translate_exp(string as_source_exp) //.. return if multy lingual mode is off
        {
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return as_source_exp;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return as_source_exp;
            }
            return await fnv_find_exp_in_dic(as_source_exp);
        }
        public async Task<string> uf_update_dicionary(long al_expression_number, string as_expression)
        {
            str_pass lstr_pass = new str_pass();
            string ls_translation = null;
            long ll_row = 0;
            long ll_new_expression_number = 0;
            if (!fnv_is_hebrew_string(as_expression))
            {
                return as_expression;
            }
            //.. set 
            lstr_pass.b[1] = (al_expression_number == 0);
            lstr_pass.l[1] = al_expression_number;
            lstr_pass.s[1] = as_expression;
            if (ib_display_translate_window)
            {
                //.. set 
                lstr_pass.b[1] = (al_expression_number == 0);
                lstr_pass.l[1] = al_expression_number;
                lstr_pass.s[1] = as_expression;
                w_add_translation lw_add_translation = await WindowHelper.Open<w_add_translation>("global_global", "pass", lstr_pass);

                ls_translation = WindowHelper.GetParam<string>(lw_add_translation); 
                
                if (lstr_pass.b[1])
                {
                    
                    ll_row = dw_dic.Insert(0);
                    
                    dw_dic.SetItem(ll_row, "expressions_number", "1");
                    
                    dw_dic.SetItem(ll_row, "expressions_name", Convert.ToString(as_expression));
                    
                    dw_dic.SetItem(ll_row, "translation", Convert.ToString(ls_translation));
                }
            }
            else
            {
                if (lstr_pass.b[1])
                {
                    LoadData(ref ll_new_expression_number);
                    //.. insert expression to DB 

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {

                         await masofonAlias.masofon.Instance.guo_msg.uf_msg("uf_update_dictionary()", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        return as_expression;
                    }
                    UpdateData(ll_new_expression_number, as_expression);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {
                        return as_expression;
                    }
                    else
                    {
                        ls_translation = "*********  Add Translation **********";
                        UpdateData1(ll_new_expression_number, ls_translation);
                         
                        dw_dic.Retrieve(il_current_lang);
                    }
                }
                else
                {
                    ls_translation = as_expression;
                }
            }
            return ls_translation;
        }
        public void uf_set_charset()
        {
            switch (is_font_charset)
            {
                case "hebrewcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)177);
                    ii_charset_number = 177;
                    break;
                case "russiancharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)204);
                    ii_charset_number = 204;
                    break;
                case "arabiccharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)178);
                    ii_charset_number = 178;
                    break;
                case "ansi!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)0);
                    ii_charset_number = 0;
                    break;
                case "balticcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)186);
                    ii_charset_number = 186;
                    break;
                case "chinesebig5!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)136);
                    ii_charset_number = 136;
                    break;
                case "defaultcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)1);
                    ii_charset_number = 1;
                    break;
                case "easteuropecharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)238);
                    ii_charset_number = 238;
                    break;
                case "gb2312charset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)134);
                    ii_charset_number = 134;
                    break;
                case "greekcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)161);
                    ii_charset_number = 161;
                    break;
                case "hangeul!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)129);
                    ii_charset_number = 129;
                    break;
                case "johabcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)130);
                    ii_charset_number = 130;
                    break;
                case "maccharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)77);
                    ii_charset_number = 77;
                    break;
                case "oem!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)255);
                    ii_charset_number = 255;
                    break;
                case "thaicharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)222);
                    ii_charset_number = 222;
                    break;
                case "turkishcharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)162);
                    ii_charset_number = 162;
                    break;
                case "vietnamesecharset!":
                    ist_charset.Font = new Font(ist_charset.Font.Name, ist_charset.Font.Size, ist_charset.Font.Style, GraphicsUnit.Point, (byte)163);
                    ii_charset_number = 163;
                    //.. no use charsets
                    //		shift!	number = 128
                    //		symbol!	number = 2
                    break;
            }
        }
        public async Task fnv_change_language(long new_lang)
        {
            if (new_lang != il_hebrew_language)
            {
                multi_lingual_is_active = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "language", "multi_lingual_is_active", "no", "").ToLower(CultureInfo.InvariantCulture) == "yes";
            }
            if (!multi_lingual_is_active)
            {
                return;
            }
            if (true)
            {
                LoadData1(new_lang);
            }

            switch (masofonAlias.masofon.Instance.sqlca.SqlCode)
            {
                case 100:
                    //.. no language defined
                     await   masofonAlias.masofon.Instance.guo_msg.uf_msg("translator.constructor", "", "stopsign!", "No Language Defined In Table languages For Language Number " + new_lang.ToString() + "\r" + "\n" + " Translator Disabled!");
                    multi_lingual_is_active = false;
                    direction = "right";
                    return;
                case -1:
                    //.. db error

                     await masofonAlias.masofon.Instance.guo_msg.uf_msg("translator.constructor", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText + "\r" + "\n" + " Translator Disabled!");
                    multi_lingual_is_active = false;
                    direction = "right";
                    return;
            }
            //.. set direction of windows
            if (isempty_stringClass.isempty_string(direction))
            {
                if (new_lang == il_hebrew_language)
                {
                    //.. hebrew
                    direction = "right";
                }
                else
                {
                    //.. other
                    direction = "left";
                }
            }
            
            dw_dic.Reset();
             
            dw_dic = new dw_window_dicRepository("", "", "", "", "");
            
            
             
            dw_dic.Retrieve(new_lang);
            il_current_lang = (int)new_lang;
            if (new_lang == il_hebrew_language)
            {
                multi_lingual_is_active = false;
            }
            uf_set_charset();
        }
        public int fnv_get_dw_width(GridElement dw) //------- stas ----  09/05/04 ------------------------------------
        {
            string ls_colname = null;
            string ls_dw_Objects = null;
            string ls_col_width = null;
            string ls_col_x = null;
            int li_max_width = 0;
            int li_width_temp = 0;
            if (dw.DataSource == null)
            {
                return 0;
            }
            
            
            ls_dw_Objects = dw.ColumnsNames();
            while (ls_dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_dw_Objects, "\t");
                 
                ls_col_width = dw.Describe(ls_colname, "Width");
                 
                ls_col_x = dw.Describe(ls_colname, "X");
                li_width_temp = Convert.ToInt32(ls_col_width) + Convert.ToInt32(ls_col_x);
                if (li_width_temp > li_max_width)
                {
                    li_max_width = li_width_temp;
                }
            }
            return li_max_width;
            //--------------------------------------------------------------------
        }
        public async Task fnv_translate_menu(MenuElement menu_arg)
        {
            int li_total = 0;
            int i = 0;
            string ls_tmp = null;
            string ls_shortcut_key = null;
            string ls_expretion = null;
            long ll_start_from = 0;
            long ll_lenght = 0;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            li_total = CollectionExtensions.UBound(menu_arg.Items);
            for (i = 1; i <= li_total; i++)
            {
                if (!isempty_stringClass.isempty_string(Convert.ToString(menu_arg.Items[i].Tag)) && Convert.ToString(menu_arg.Items[i].Tag).IndexOf("@") == 0)
                {
                    ls_tmp = Convert.ToString(menu_arg.Items[i].Tag);
                    menu_arg.Items[i].Text = f_get_tokenClass.f_get_token(ref ls_tmp, "@");

                    //menu_arg.Items[i].set_MicroHelp(f_get_tokenClass.f_get_token(ref ls_tmp, "@"));
                    menu_arg.Items[i].Text = f_get_tokenClass.f_get_token(ref ls_tmp, "@");
                    //		menu_arg.item[i].text =left( menu_arg.item[i].tag,(pos(menu_arg.item[i].tag,"@")-1))
                    //		ll_start_from = len(menu_arg.item[i].text) + 2
                    //		menu_arg.item[i].microhelp   = right(menu_arg.item[i].tag,(len(menu_arg.item[i].tag)-pos(menu_arg.item[i].tag,"*")))
                    //		ll_lenght = len(menu_arg.item[i].tag) - len(menu_arg.item[i].microhelp) -len(menu_arg.item[i].text) -2
                    //		menu_arg.item[i].toolbaritemtext = mid(menu_arg.item[i].tag,ll_start_from,ll_lenght)
                }
                else
                {

                    //menu_arg.Items[i].Tag = menu_arg.Items[i].Tag + "@" + menu_arg.Items[i].Text + "@" + menu_arg.Items[i].Text + "@" + menu_arg.Items[i].get_MicroHelp();
                }
                ls_shortcut_key = menu_arg.Items[i].Text;
                ls_expretion = f_get_tokenClass.f_get_token(ref ls_shortcut_key, "\t");
                ls_tmp = f_get_tokenClass.f_get_token(ref ls_expretion, "&");
                ls_expretion = ls_tmp + ls_expretion;
                ls_tmp = await fnv_find_exp_in_dic(ls_expretion);
                //------- stas --- 17/05/04 ----------------------------------------
                if (!(isempty_stringClass.isempty_string(ls_tmp)) && menu_arg.Items[i].Text != "-")
                {
                    menu_arg.Items[i].Text = ls_tmp + "\t" + ls_shortcut_key;
                }
                //-------------------------------------------------------------------
                ls_tmp = await fnv_find_exp_in_dic(menu_arg.Items[i].Text);
                if (!(isempty_stringClass.isempty_string(ls_tmp)))
                {
                    menu_arg.Items[i].Text = ls_tmp;
                }

                //ls_tmp = fnv_find_exp_in_dic(menu_arg.Items[i].get_MicroHelp());
                if (!(isempty_stringClass.isempty_string(ls_tmp)))
                {

                    //menu_arg.Items[i].set_MicroHelp(ls_tmp);
                }
                await fnv_translate_menu(menu_arg.Items[i].Menu);
            }
        }
        public void fnv_translate_dw_fields(GridElement datawindow_arg, string colname)
        {
            if (!multi_lingual_is_active)
            {
                return;
            }
            int li_row = 0;
            int i = 0;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            
            for (li_row = 1; li_row <= datawindow_arg.RowCount(); li_row++)
            {
                

                datawindow_arg.SetItem(li_row, Convert.ToString(colname), Convert.ToString(fnv_find_exp_in_dic(datawindow_arg.GetItemValue<string>(li_row, Convert.ToString(colname)))));
            }
        }
        public void fnv_translate_ddlb(ComboBoxElement ddlb_arg)
        {
            int i = 0;
            int li_total = 0;
            ListItemCollection ddlb_items = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            li_total = CollectionExtensions.UBound(ddlb_arg.Items);
            ddlb_items = ddlb_arg.Items;

            ddlb_arg.ClearItems();

            for (i = 1; i <= li_total; i++)
            {
                ddlb_arg.Add(fnv_translate_exp(ddlb_items[i].Text));
            }
        }
        public void fnv_translate_listbox(ListBoxElement lb_arg)
        {
            int i = 0;
            int li_total = 0;
         //   string[] lb_items = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
             
            li_total = lb_arg.Items.Count;
            //lb_items = lb_arg.Items;
            for (i = 1; i <= li_total; i++)
            {
                lb_arg.Items[i].Value = masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(Convert.ToString(lb_arg.Items[i]));
            }
        }
        public async Task fnv_translate_ds(GridElement dc_arg) //-------- stas 23/05/04 ---------------------
        {
            await fnv_translate_data_store(dc_arg);
            fnv_rotate_data_store(dc_arg);
            //--------------------------------------------
        }
        public async Task fnv_back_to_original_language()
        {
            if (!multi_lingual_is_active || ii_return_to_lang == 0)
            {
                return;
            }
            il_current_lang = ii_return_to_lang;
            await fnv_change_language(il_current_lang);
            ii_return_to_lang = 0;
        }
        public async Task fnv_translate_data_store(GridElement ds_arg) //                        fnv_translate_Data_Store( DataStoreb ds_arg )
        {
            //------------------ stas 23/05/04 -------------------------------------------
            string colname = null;
            string dw_Objects = null;
            string tmp = null;
            string tmp_type = null;
            string ls_ddlb_value = null;
            string ls_ddlb_expr = null;
            long len_translate = 0;
            long ll_need_field = 0;
            string len_field = null;
            string ls_translate = null;
            string ls_new_font = null;
            string font_size = null;
          //  GridElement dwc;
            string ls_display = null;
            long ll_dwc_width = 0;
            int li_ddlb_index = 1;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            // ------------------- Translate datawindow title ------------------
            //tmp = fnv_find_exp_in_dic(ds_arg.title)
            //if not(isempty_string(tmp)) then ds_arg.title = tmp
            // ------------------ Translate datawindow text fields (labels) ----


            dw_Objects = ds_arg.ColumnsNames();
            while (dw_Objects.Length > 0)
            {
                colname = f_get_tokenClass.f_get_token(ref dw_Objects, "\t");

                tmp_type = ds_arg.Describe(colname, "type");

                if (ds_arg.Describe(colname, "dddw.name").Length > 1)
                {
                    //datawindow_arg.getchild(colname,dwc)
                    //		ll_dwc_width = long(datawindow_arg.describe(colname+ ".width"))*long(datawindow_arg.describe(colname+ ".dddw.percentwidth")) /100
                    //		fnv_translate_dwc(dwc,ll_dwc_width)
                }

                if (ds_arg.Describe(colname, "Edit.Style") == "ddlb") // translate dropdownlistbox in dw
                {
                    do
                    {

                        ls_ddlb_value = ds_arg.GetValue(colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                        ls_ddlb_expr =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        ds_arg.SetValue(colname, li_ddlb_index, ls_ddlb_expr + Convert.ToChar(9).ToString() + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                }
                if (tmp_type == "text" || (tmp_type == "CheckBox" || (tmp_type == "button" || tmp_type == "groupbox")))
                {

                    tmp = await fnv_find_exp_in_dic(ds_arg.Describe(colname, "text"));
                    ds_arg.Modify("colname", "text", "tmp");
                    /////////////////////////////////////////////////////////////////////
                    // this code calculate the relative column size and resize the font
                    // of each column
                    ////////////////////////////////////////////////////////////////////// 
                    //len_translate=len(tmp)
                    //			ls_translate=tmp
                    //			len_field=datawindow_arg.Describe(colname, "Width")
                    //			font_size=datawindow_arg.Describe(colname, "Font.Height")
                    //			font_size=Mid(font_size, 2)
                    //			ll_need_field=dec(len_translate * (40) * integer (font_size)/12)
                    //
                    //			IF dec(len_field) < ll_need_field THEN 
                    //   			ls_new_font = string(long ( -.85*(long(font_size) *(long(len_field)/long(ll_need_field)))))
                    //				IF long(ls_new_font) * -1 < 7 THEN ls_new_font = "-7"
                    //				datawindow_arg.Modify(colname + ".Font.Height =" + ls_new_font)
                    //			END IF 
                    ////////////////////////////////////////////////////
                }
                ds_arg.Modify(colname, "font.face", lang_font);
                ds_arg.Modify(colname, "Font.CharSet", "1");
                if (ii_font_decrease > 0)
                {

                    font_size = ds_arg.Describe(colname, "Font.Height");
                    ls_new_font = Convert.ToInt64((Convert.ToInt64(font_size) + ii_font_decrease)).ToString();
                    if (Convert.ToInt64(ls_new_font) * -1 < il_min_dw_language_font)
                    {
                        ls_new_font = (il_min_dw_language_font * -1).ToString();
                    }
                    ds_arg.Modify(colname, "Font.Height", ls_new_font);
                }
            }
            //-------------------------------------------------------------------------
        }
        public void fnv_rotate_data_store(GridElement ds_arg)
        {
            // fnv_rotate_data_store ( IRepository ds_arg )
            //------------------ stas 23/05/04 -------------------------------------------------------
            if (!multi_lingual_is_active)
            {
                return;
            }
            if (direction != "left")
            {
                return;
            }
            string ls_colname = null;
            string ls_dw_Objects = null;
            string ls_col_type = null;
            int li_dw_width = 0;
            int li_col_width = 0;
            int li_col_x = 0;
            int li_alignment = 0;
            int ll_line_x = 0;
            int ll_line_width = 0;
            int li_i = 0;
            IRepository ds;
            string ls_temp = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }


            ls_dw_Objects = ds_arg.ColumnsNames();
            li_dw_width = fnv_get_ds_width(ref ds_arg) + 25;////////////////////////////////////// !!!!
            while (ls_dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_dw_Objects, "\t");
                 
                li_col_width = (int)Convert.ToInt64(ds_arg.Describe(ls_colname, "width"));
                 
                li_col_x = (int)Convert.ToInt64(ds_arg.Describe(ls_colname, "x"));
                 
                li_alignment = (int)Convert.ToInt64(ds_arg.Describe(ls_colname, "alignment"));
                 
                ls_col_type = ds_arg.Describe(ls_colname, "ColType");
                //	//stas 4/7/2004 --------------
                //	IF ds_arg.Describe(ls_colname, "Coltype") = "datetime" OR &
                //								ds_arg.Describe(ls_colname, "Coltype") = "date" THEN	
                //		IF ds_arg.Describe(ls_colname, "EditMask.Mask") <> "?" THEN		
                //			ls_temp = ds_arg.Describe(ls_colname, "EditMask.Mask")
                //			ls_temp = fnv_change_date_format2( ls_temp )
                //			ds_arg.Modify(ls_colname, EditMask.Mask='" + ls_temp + "'") //'mm/dd/yy'")						
                //		ELSE
                //			ls_temp = ds_arg.Describe(ls_colname, "Format")
                //			ls_temp = fnv_change_date_format2( ls_temp )
                //			ds_arg.Modify(ls_colname, format='" + ls_temp + "'") // m/d/yy h:mm'")			
                //		END IF	
                //	END IF
                //	//------------
                 
                if (ds_arg.Describe(ls_colname, "Type") == "line")
                {
                    long ll_line_x2 = 0;
                     
                    ll_line_x = (int)Convert.ToInt64(ds_arg.Describe(ls_colname, "x1"));
                     
                    ll_line_x2 = Convert.ToInt64(ds_arg.Describe(ls_colname, "x2"));
                     
                     
                    ll_line_width = (int)(Convert.ToInt64(ds_arg.Describe(ls_colname, "x2")) - Convert.ToInt64(ds_arg.Describe(ls_colname, "x1")));
                    if (ll_line_x == ll_line_x2)
                    {
                        ds_arg.Modify(ls_colname, "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());
                        ds_arg.Modify(ls_colname, "x2", ds_arg.Describe(ls_colname, "x1") + "'");
                    }
                    else
                    {
                        ds_arg.Modify(ls_colname, "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());
                        ds_arg.Modify(ls_colname, "x2", (Convert.ToInt64(ds_arg.Describe(ls_colname, "x1")).ToString() + ll_line_width.ToString()).ToString() + "'");
                    }
                }
                else
                {

                    ds_arg.Modify(ls_colname, "x", (li_dw_width - li_col_x - li_col_width).ToString());
                    if (direction == "left" && (li_alignment != 2 && (ls_col_type.Substring(0, 7) != "decimal" && (ls_col_type != "long" && (ls_col_type != "integer" && ls_col_type != "number")))))
                    {
                        //IF li_alignment = 1 THEN

                        ds_arg.Modify(ls_colname, "alignment", "0");
                        //ELSE
                        // ds_arg.Modify(ls_colname, alignment = '1'"  )
                        //			END IF
                    }

                    ds_arg.Modify(ls_colname, "righttoleft", "0");
                }
            }
            //-------------------------------------------------------------------------------------
        }
        public async Task fnv_translate_only(WindowElement window_arg)
        {
            string ls_temp = null;
            int i = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            // ------ Translate Window Title --------------------------
            ls_temp =await fnv_find_exp_in_dic(window_arg.Text);
            if (!isempty_stringClass.isempty_string(ls_temp))
            {
                window_arg.Text = ls_temp;
            }
            // ------ Translate Window Menu ---------------------------
            if (window_arg.Menu != null)
            {
                await fnv_translate_menu(window_arg.Menu);
            }
            // ------ Translate All Window Controls -------------------
            for (i = 1; i <= CollectionExtensions.UBound(window_arg.Controls); i++)
            {
                if (!(window_arg.Controls[i] != null))
                {
                    continue;
                }
                if (window_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    sle = window_arg.Controls[i] as TextBoxElement;
                    ls_temp =await fnv_find_exp_in_dic(sle.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        sle.Text = ls_temp;
                    }
                    sle.Font = new Font(lang_font, sle.Font.Size, sle.Font.Style, GraphicsUnit.Point,
                        sle.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(LabelElement))
                {
                    st = window_arg.Controls[i] as LabelElement;
                    ls_temp = await fnv_find_exp_in_dic(st.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        st.Text = ls_temp;
                    }
                    st.Font = new Font(lang_font, st.Font.Size, st.Font.Style, GraphicsUnit.Point, st.Font.GdiCharSet);
                    st.Font = new Font(st.Font.Name, st.Font.Size, st.Font.Style, GraphicsUnit.Point,
                        ist_charset.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    cb = window_arg.Controls[i] as ButtonElement;
                    ls_temp =await fnv_find_exp_in_dic(cb.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        cb.Text = ls_temp;
                    }
                    // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                    //cb.Font.Name = System.Convert.ToChar(lang_font);;
                }

                else if (window_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    uo = window_arg.Controls[i] as CompositeElement;
                    await fnv_translate_uo(uo);
                }
                else if (window_arg.Controls[i].GetType() == typeof(GridElement))
                {
                    dw = window_arg.Controls[i] as GridElement;

                    if (dw.DataSource != null)
                    {
                        await fnv_translate_dw(dw);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                    {
                        pb = window_arg.Controls[i] as ButtonElement;
                        ls_temp =await fnv_find_exp_in_dic(pb.Text);
                        if (!isempty_stringClass.isempty_string(ls_temp))
                        {
                            pb.Text = ls_temp;
                        }
                        // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                        //pb.Font.Name = System.Convert.ToChar(lang_font);;
                        pb.Font = new Font(pb.Font.Name, pb.Font.Size, pb.Font.Style, GraphicsUnit.Point,
                            ist_charset.Font.GdiCharSet);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(CheckBoxElement))
                    {
                        cbx = window_arg.Controls[i] as CheckBoxElement;
                        ls_temp =await  fnv_find_exp_in_dic(cbx.Text);
                        if (!isempty_stringClass.isempty_string(ls_temp))
                        {
                            cbx.Text = ls_temp;
                        }
                        cbx.Font = new Font(lang_font, cbx.Font.Size, cbx.Font.Style, GraphicsUnit.Point,
                            cbx.Font.GdiCharSet);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(RadioButtonElement))
                    {
                        rb = window_arg.Controls[i] as RadioButtonElement;
                        ls_temp =await  fnv_find_exp_in_dic(rb.Text);
                        if (!isempty_stringClass.isempty_string(ls_temp))
                        {
                            rb.Text = ls_temp;
                        }
                        rb.Font = new Font(lang_font, rb.Font.Size, rb.Font.Style, GraphicsUnit.Point,
                            rb.Font.GdiCharSet);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(TabElement))
                    {
                        t = window_arg.Controls[i] as TabElement;
                        await fnv_translate_tab(t);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(RichTextBox))
                    {
                        gb = window_arg.Controls[i] as GroupBoxElement;
                        ls_temp =await  fnv_find_exp_in_dic(gb.Text);
                        if (!isempty_stringClass.isempty_string(ls_temp))
                        {
                            gb.Text = ls_temp;
                        }

                        gb.Font = new Font(lang_font, gb.Font.Size);
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(ListBoxElement))
                    {
                        lb = window_arg.Controls[i] as ListBoxElement;
                        if (CollectionExtensions.UBound(lb.Items) > 0)
                        {
                            fnv_translate_listbox(lb);
                        }
                        lb.Font = new Font(lang_font, lb.Font.Size, lb.Font.Style, GraphicsUnit.Point,
                            lb.Font.GdiCharSet);
                        //----- stas 24/05/04 ----------------------------	
                    }
                    else if (window_arg.Controls[i].GetType() == typeof(LinkLabelElement))
                    {
                        shl = window_arg.Controls[i] as LinkLabelElement;
                        ls_temp = await fnv_find_exp_in_dic(shl.Text);
                        if (!isempty_stringClass.isempty_string(ls_temp))
                        {
                            shl.Text = ls_temp;
                        }
                        shl.Font = new Font(lang_font, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point,
                            shl.Font.GdiCharSet);
                        shl.Font = new Font(shl.Font.Name, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point,
                            ist_charset.Font.GdiCharSet);
                        //------------------------------------------------			
                    }
                }
                //fnv_kbd_layout()	
            }
        }
        public async Task fnv_init(WindowElement window_arg)
        {
            if (!multi_lingual_is_active)
            {
                return;
            }
            await fnv_translate(window_arg);
            fnv_rotate_window(window_arg);
        }
        public void fnv_rotate_uo(CompositeElement userobject_arg)
        {
            int li_win_width = 0;
            int i = 0;
            TextBoxElement sle = default(TextBoxElement);
            LabelElement st = default(LabelElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            ButtonElement pb = default(ButtonElement);
            GridElement dw = default(GridElement);
            VScrollBar vsb = default(VScrollBar);
            HScrollBar hsb = default(HScrollBar);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            GroupBoxElement gb = default(GroupBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            ImageElement p = default(ImageElement);
            TreeElement tv = default(TreeElement);
            LinkLabelElement shl = default(LinkLabelElement);
            Rectangle r = default(Rectangle);
            TabElement t = default(TabElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }
            if (Convert.ToString(userobject_arg.Tag) == "EXIT TRANSLATOR")
            {
                return;
            }
            li_win_width = userobject_arg.PixelWidth;
            for (i = 1; i <= CollectionExtensions.UBound(userobject_arg.Controls); i++)
            {
                if (!(userobject_arg.Controls[i] != null))
                {
                    continue;
                }
                if (userobject_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    sle = userobject_arg.Controls[i] as TextBoxElement;
                    sle.PixelLeft = li_win_width - sle.PixelLeft - sle.PixelWidth - 25;
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(LabelElement))
                {
                    st = userobject_arg.Controls[i] as LabelElement;
                    st.PixelLeft = li_win_width - st.PixelLeft - st.PixelWidth - 25;
                    //------ stas ----  16/05/04 -------------------------
                    st.RightToLeft = st.RightToLeft;
                    
                    if (st.TextAlign != ContentAlignment.MiddleCenter)
                    {
                        
                        st.TextAlign = (ContentAlignment)HorizontalAlignment.Left;
                    }
                    //----------------------------------------------------
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    cb = userobject_arg.Controls[i] as ButtonElement;
                    cb.PixelLeft = li_win_width - cb.PixelLeft - cb.PixelWidth - 25;
                }
                
                else if (userobject_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    uo = userobject_arg.Controls[i] as CompositeElement;
                    uo.PixelLeft = li_win_width - uo.PixelLeft - uo.PixelWidth - 25;
                    fnv_rotate_uo(uo); // stas 23/8/2004
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(GridElement))
                {
                    dw = userobject_arg.Controls[i] as GridElement;
                    dw.PixelLeft = li_win_width - dw.PixelLeft - dw.PixelWidth - 25;
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    pb = userobject_arg.Controls[i] as ButtonElement;
                    pb.PixelLeft = li_win_width - pb.PixelLeft - pb.PixelWidth - 25;
                }
                // TODO: Field 'VScrollBar of type 'Sybase.PowerBuilder.Object' is unmapped'. (CODE=1004)
                //else if (userobject_arg.Controls[i].GetType() == ObjectExtensions.VScrollBar)
                //{
                //    vsb = userobject_arg.Controls[i] as VScrollBar;
                //    vsb.PixelLeft = li_win_width - vsb.PixelLeft - vsb.PixelWidth - 25;
                //}
                //// TODO: Field 'HScrollBar of type 'Sybase.PowerBuilder.Object' is unmapped'. (CODE=1004)
                //else if (userobject_arg.Controls[i].GetType() == ObjectExtensions.HScrollBar)
                //{
                //    hsb = userobject_arg.Controls[i] as HScrollBar;
                //    hsb.PixelLeft = li_win_width - hsb.PixelLeft - hsb.PixelWidth - 25;
                //}
                else if (userobject_arg.Controls[i].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = userobject_arg.Controls[i] as MaskedTextBoxElement;
                    em.PixelLeft = li_win_width - em.PixelLeft - em.PixelWidth - 25;
                    //---- stas 4/7/2004 ---
                     
                    // TODO: Field 'DateTimeMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)
                   // if (em.MaskDataType() == MaskDataType.DateTimeMask)
                   if(Extensions.get_MaskDataType()== MaskDataType.DateTimeMask)
                    {
                        // TODO: Field 'DateTimeMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)

                        //em.SetMask((MaskDataType.DateTimeMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )
                        Extensions.SetMask((MaskDataType.DateTimeMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )
                    }

                    // TODO: Field 'DateMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)
                    //  else if (em.get_MaskDataType() == MaskDataType.DateMask)
                    else if (Extensions.get_MaskDataType() == MaskDataType.DateMask)
                    {
                        // TODO: Field 'DateMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)

                        //  em.SetMask((MaskDataType.DateMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )	
                        Extensions.SetMask((MaskDataType.DateMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )	
                    }
                    //----------------
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(RichTextBox))
                {
                    gb = userobject_arg.Controls[i] as GroupBoxElement;
                    gb.PixelLeft = li_win_width - gb.PixelLeft - gb.PixelWidth - 25;
                    //----- (c) stas 
                    gb.RightToLeft = RightToLeft.No; //NOT gb.righttoleft
                    //---
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    mle = userobject_arg.Controls[i] as TextBoxElement;
                    mle.PixelLeft = li_win_width - mle.PixelLeft - mle.PixelWidth - 25;
                    //--- stas 4/7/2004 ---
                    mle.RightToLeft = RightToLeft.No;
                    
                    mle.TextAlign = (HorizontalAlignment)HorizontalAlignment.Left;
                    //---
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(CheckBoxElement))
                {
                    cbx = userobject_arg.Controls[i] as CheckBoxElement;
                    cbx.PixelLeft = li_win_width - cbx.PixelLeft - cbx.PixelWidth - 25;
                    cbx.RightToLeft = cbx.RightToLeft;
                    //------ stas 29/04/04 ------------------------------
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ImageElement))
                {
                    p = userobject_arg.Controls[i] as ImageElement;
                    p.PixelLeft = li_win_width - p.PixelLeft - p.PixelWidth - 25;
                    //------- stas 12/05/04 -------------------------------	 
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(TreeElement))
                {
                    tv = userobject_arg.Controls[i] as TreeElement;
                    tv.PixelLeft = li_win_width - tv.PixelLeft - tv.PixelWidth - 25;
                    //----- stas 24/05/04 ----------------------------	
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(LinkLabelElement))
                {
                    shl = userobject_arg.Controls[i] as LinkLabelElement;
                    shl.PixelLeft = li_win_width - shl.PixelLeft - shl.PixelWidth - 25;
                    shl.RightToLeft = shl.RightToLeft;
                    //IF shl.Alignment = Left! THEN
                    //	shl.Alignment = right!
                    //			ELSEIF shl.Alignment = right! THEN
                    
                    shl.TextAlign = (ContentAlignment)HorizontalAlignment.Left;
                    //		END IF
                    //----stas ------- 27/05/04 -------------------------------		
                }
                //else if (userobject_arg.Controls[i].GetType() == typeof(Rectangle))
                //{
                //    r = global.Extensions.userobject_arg.Controls[i] as Rectangle;
                //    r.X = li_win_width - r.X - r.Width - 25;
                //    //----  stas  21/8/04 ----------------
                //}
                else if (userobject_arg.Controls[i].GetType() == typeof(TabElement))
                {
                    t = userobject_arg.Controls[i] as TabElement;
                    //		t.x = li_win_width - t.x - t.width - 25  //for test STAS 21/8/2004
                    fnv_rotate_tab(t);
                }
            }
        }
        public async Task fnv_translate(WindowElement window_arg)
        {
            string ls_temp = null;
            int i = 0;
            LabelElement st = default(LabelElement);
            TextBoxElement sle = default(TextBoxElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            TabElement t = default(TabElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            // ------ Translate Window Title --------------------------
            ls_temp = await fnv_find_exp_in_dic(window_arg.Text);
            if (!isempty_stringClass.isempty_string(ls_temp))
            {
                window_arg.Text = ls_temp;
            }
            // ------ Translate Window Menu ---------------------------
            if (window_arg.Menu != null)
            {
                await fnv_translate_menu(window_arg.Menu);
            }
            // ------ Translate All Window Controls -------------------
            for (i = 1; i <= CollectionExtensions.UBound(window_arg.Controls); i++)
            {
                if (!(window_arg.Controls[i] != null))
                {
                    continue;
                }
                if (window_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    sle = window_arg.Controls[i] as TextBoxElement;
                    ls_temp = await fnv_find_exp_in_dic(sle.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        sle.Text = ls_temp;
                    }
                    sle.Font = new Font(lang_font, sle.Font.Size, sle.Font.Style, GraphicsUnit.Point, sle.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(LabelElement))
                {
                    st = window_arg.Controls[i] as LabelElement;
                    ls_temp =await  fnv_find_exp_in_dic(st.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        st.Text = ls_temp;
                    }
                    st.Font = new Font(lang_font, st.Font.Size, st.Font.Style, GraphicsUnit.Point, st.Font.GdiCharSet);
                    st.Font = new Font(st.Font.Name, st.Font.Size, st.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    cb = window_arg.Controls[i] as ButtonElement;
                    ls_temp = await fnv_find_exp_in_dic(cb.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        cb.Text = ls_temp;
                    }
                    // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                    //cb.Font.Name = System.Convert.ToChar(lang_font);;
                }

                else if (window_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    uo = window_arg.Controls[i] as CompositeElement;
                    await fnv_translate_uo(uo);
                    //---
                }
                else if (window_arg.Controls[i].GetType() == typeof(GridElement))
                {
                    dw = window_arg.Controls[i] as GridElement;
                    if (dw.DataSource != null)
                    {
                        await fnv_translate_dw(dw);
                    }
                }
                else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    pb = window_arg.Controls[i] as ButtonElement;
                    ls_temp =await  fnv_find_exp_in_dic(pb.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        pb.Text = ls_temp;
                    }
                    // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                    //pb.Font.Name = System.Convert.ToChar(lang_font);;
                    pb.Font = new Font(pb.Font.Name, pb.Font.Size, pb.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(CheckBoxElement))
                {
                    cbx = window_arg.Controls[i] as CheckBoxElement;
                    ls_temp =await  fnv_find_exp_in_dic(cbx.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        cbx.Text = ls_temp;
                    }
                    cbx.Font = new Font(lang_font, cbx.Font.Size, cbx.Font.Style, GraphicsUnit.Point, cbx.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(RadioButtonElement))
                {
                    rb = window_arg.Controls[i] as RadioButtonElement;
                    ls_temp =await  fnv_find_exp_in_dic(rb.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        rb.Text = ls_temp;
                    }
                    rb.Font = new Font(lang_font, rb.Font.Size, rb.Font.Style, GraphicsUnit.Point, rb.Font.GdiCharSet);
                }
                else if (window_arg.Controls[i].GetType() == typeof(TabElement))
                {
                    t = window_arg.Controls[i] as TabElement;
                    await fnv_translate_tab(t);
                }
                else if (window_arg.Controls[i].GetType() == typeof(RichTextBox))
                {
                    gb = window_arg.Controls[i] as GroupBoxElement;
                    ls_temp = await fnv_find_exp_in_dic(gb.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        gb.Text = ls_temp;
                    }
                    gb.Font = new Font(lang_font, gb.Font.Size);
                }
                else if (window_arg.Controls[i].GetType() == typeof(ListBoxElement))
                {
                    lb = window_arg.Controls[i] as ListBoxElement;
                    if (CollectionExtensions.UBound(lb.Items) > 0)
                    {
                        fnv_translate_listbox(lb);
                    }
                    lb.Font = new Font(lang_font, lb.Font.Size, lb.Font.Style, GraphicsUnit.Point, lb.Font.GdiCharSet);
                    //----- stas 24/05/04 ----------------------------	
                }
                else if (window_arg.Controls[i].GetType() == typeof(LinkLabelElement))
                {
                    shl = window_arg.Controls[i] as LinkLabelElement;
                    ls_temp = await fnv_find_exp_in_dic(shl.Text);
                    if (!isempty_stringClass.isempty_string(ls_temp))
                    {
                        shl.Text = ls_temp;
                    }
                    shl.Font = new Font(lang_font, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point, shl.Font.GdiCharSet);
                    shl.Font = new Font(shl.Font.Name, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                    //------------------------------------------------			
                    //-- stas 7/9/2004
                }
                else if (window_arg.Controls[i].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = window_arg.Controls[i] as ComboBoxElement;
                    fnv_translate_ddlb(ddlb);
                }
            }
            fnv_kbd_layout();
        }
        public async Task fnv_translate_tab(TabElement tab_arg)
        {
            string ls_tmp = null;
            int li_total = 0;
            int i = 0;
            CompositeElement tp = default(CompositeElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            li_total = CollectionExtensions.UBound(tab_arg.Controls);
            tab_arg.Font = new Font(lang_font, tab_arg.Font.Size);

            //     tab_arg.CharSet(ist_charset.Font.GdiCharSet);
            for (i = 1; i <= li_total; i++)
            {
                if (tab_arg.Controls[i] is CompositeElement)
                {
                    tp = (CompositeElement)tab_arg.Controls[i];

                    ls_tmp = await fnv_find_exp_in_dic(tp.Text);
                    if (!(isempty_stringClass.isempty_string(ls_tmp)))
                    {
                        tp.Text = (ls_tmp);
                    }
                    ls_tmp = await fnv_find_exp_in_dic(Convert.ToString(tp.Tag));
                    if (!(isempty_stringClass.isempty_string(ls_tmp)))
                    {
                        tp.Tag = ls_tmp;
                    }

                    ls_tmp = await fnv_find_exp_in_dic(tp.ToolTipText);
                    if (!(isempty_stringClass.isempty_string(ls_tmp)))
                    {

                        tp.ToolTipText = ls_tmp;
                    }
                    await fnv_translate_uo(tp);
                }
            }
        }
        public async Task fnv_translate_uo(CompositeElement userobject_arg)
        {
            string tmp = null;
            int li_total = 0;
            int i = 0;
            TextBoxElement sle = default(TextBoxElement);
            LabelElement st = default(LabelElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            CompositeElement uo_temp = default(CompositeElement);
            ButtonElement pb = default(ButtonElement);
            GridElement dw = default(GridElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            TabElement t = default(TabElement);
            RadioButtonElement rb = default(RadioButtonElement);
            GroupBoxElement gb = default(GroupBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            LinkLabelElement shl = default(LinkLabelElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            li_total = CollectionExtensions.UBound(userobject_arg.Controls);
            if (Convert.ToString(userobject_arg.Tag) == "EXIT TRANSLATOR")
            {
                return;
            }
            for (i = 1; i <= li_total; i++)
            {
                if (!(userobject_arg.Controls[i] != null))
                {
                    continue;
                }
                if (userobject_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    sle = userobject_arg.Controls[i] as TextBoxElement;
                    tmp =await  fnv_find_exp_in_dic(sle.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        sle.Text = tmp;
                    }
                    sle.Font = new Font(lang_font, sle.Font.Size, sle.Font.Style, GraphicsUnit.Point, sle.Font.GdiCharSet);
                    //----- stas 16/05/04 --------------------
                    sle.RightToLeft = sle.RightToLeft;
                    //----------------------------------------
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(LabelElement))
                {
                    st = userobject_arg.Controls[i] as LabelElement;
                    tmp =await  fnv_find_exp_in_dic(st.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        st.Text = tmp;
                    }
                    st.Font = new Font(lang_font, st.Font.Size, st.Font.Style, GraphicsUnit.Point, st.Font.GdiCharSet);
                    st.Font = new Font(st.Font.Name, st.Font.Size, st.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    cb = userobject_arg.Controls[i] as ButtonElement;
                    tmp = await fnv_find_exp_in_dic(cb.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        cb.Text = tmp;
                    }
                    // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                    //cb.Font.Name = System.Convert.ToChar(lang_font);;
                }

                else if (userobject_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    uo = userobject_arg.Controls[i] as CompositeElement;
                    await fnv_translate_uo(uo);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    pb = userobject_arg.Controls[i] as ButtonElement;
                    tmp =await  fnv_find_exp_in_dic(pb.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        pb.Text = tmp;
                    }
                    // TODO: The left-hand side of an assignment must be assignable (CODE=30006).
                    //pb.Font.Name = System.Convert.ToChar(lang_font);;
                    pb.Font = new Font(pb.Font.Name, pb.Font.Size, pb.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(GridElement))
                {
                    dw = userobject_arg.Controls[i] as GridElement;
                    if (dw.DataSource != null)
                    {
                        await fnv_translate_dw(dw);
                        await fnv_rotate_dw(dw);
                    }
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(CheckBoxElement))
                {
                    cbx = userobject_arg.Controls[i] as CheckBoxElement;
                    tmp = await fnv_find_exp_in_dic(cbx.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        cbx.Text = tmp;
                    }
                    cbx.Font = new Font(lang_font, cbx.Font.Size, cbx.Font.Style, GraphicsUnit.Point, cbx.Font.GdiCharSet);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(TabElement))
                {
                    t = userobject_arg.Controls[i] as TabElement;
                    await fnv_translate_tab(t);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(RadioButtonElement))
                {
                    rb = userobject_arg.Controls[i] as RadioButtonElement;
                    tmp =await  fnv_find_exp_in_dic(rb.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        rb.Text = tmp;
                    }
                    rb.Font = new Font(lang_font, rb.Font.Size, rb.Font.Style, GraphicsUnit.Point, rb.Font.GdiCharSet);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(RichTextBox))
                {
                    gb = userobject_arg.Controls[i] as GroupBoxElement;
                    tmp =await  fnv_find_exp_in_dic(gb.Text);
                    if (!(isempty_stringClass.isempty_string(tmp)))
                    {
                        gb.Text = tmp;
                    }
                    gb.Font = new Font(lang_font, gb.Font.Size);
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(ListBoxElement))
                {
                    lb = userobject_arg.Controls[i] as ListBoxElement;
                    if (CollectionExtensions.UBound(lb.Items) > 0)
                    {
                        fnv_translate_listbox(lb);
                    }
                    lb.Font = new Font(lang_font, lb.Font.Size, lb.Font.Style, GraphicsUnit.Point, lb.Font.GdiCharSet);
                    //----- stas 24/05/04 ----------------------------	
                }
                else if (userobject_arg.Controls[i].GetType() == typeof(LinkLabelElement))
                {
                    shl = userobject_arg.Controls[i] as LinkLabelElement;
                    tmp = await fnv_find_exp_in_dic(shl.Text);
                    if (!isempty_stringClass.isempty_string(tmp))
                    {
                        shl.Text = tmp;
                    }
                    shl.Font = new Font(lang_font, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point, shl.Font.GdiCharSet);
                    shl.Font = new Font(shl.Font.Name, shl.Font.Size, shl.Font.Style, GraphicsUnit.Point, ist_charset.Font.GdiCharSet);
                    //------------------------------------------------		
                }
            }
        }
        public void fnv_rotate_tab(TabElement tab_arg) //---------- (c) stas   28/04/04
        {
            string ls_tmp = null;
            int li_total = 0;
            int i = 0;
            CompositeElement tp = default(CompositeElement);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }
            // TODO: Field 'Control of type 'Sybase.PowerBuilder.Win.Tab' is unmapped'. (CODE=1004)
            li_total = CollectionExtensions.UBound(tab_arg.Controls);
            for (i = 1; i <= li_total; i++)
            {
                // TODO: Field 'Control of type 'Sybase.PowerBuilder.Win.Tab' is unmapped'. (CODE=1004)
                if (tab_arg.Controls[i] is CompositeElement)
                {
                    // TODO: Field 'Control of type 'Sybase.PowerBuilder.Win.Tab' is unmapped'. (CODE=1004)
                    tp = (CompositeElement)tab_arg.Controls[i];

                    fnv_rotate_uo(tp);
                }
            }
        }
        public void fnv_rotate_window(WindowElement window_arg)
        {
            int li_win_width = 0;
            int i = 0;
            TextBoxElement sle = default(TextBoxElement);
            LabelElement st = default(LabelElement);
            ButtonElement cb = default(ButtonElement);
            CompositeElement uo = default(CompositeElement);
            GridElement dw = default(GridElement);
            ButtonElement pb = default(ButtonElement);
            VScrollBar vsb = default(VScrollBar);
          //  HScrollBar hsb = default(HScrollBar);
            MaskedTextBoxElement em = default(MaskedTextBoxElement);
            GroupBoxElement gb = default(GroupBoxElement);
            TextBoxElement mle = default(TextBoxElement);
            CheckBoxElement cbx = default(CheckBoxElement);
            RadioButtonElement rb = default(RadioButtonElement);
            ComboBoxElement ddlb = default(ComboBoxElement);
            ListBoxElement lb = default(ListBoxElement);
            CompositeElement ol = default(CompositeElement);
            TabElement t = default(TabElement);
            ImageElement p = default(ImageElement);
            TreeElement tv = default(TreeElement);
            LinkLabelElement shl = default(LinkLabelElement);
            Rectangle r = default(Rectangle);
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }
            li_win_width = window_arg.PixelWidth;
            for (i = 1; i <= CollectionExtensions.UBound(window_arg.Controls); i++)
            {
                if (!(window_arg.Controls[i] != null))
                {
                    continue;
                }
                if (window_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    sle = window_arg.Controls[i] as TextBoxElement;
                    sle.PixelLeft = li_win_width - sle.PixelLeft - sle.PixelWidth - 25;
                    sle.RightToLeft = sle.RightToLeft;
                }
                else if (window_arg.Controls[i].GetType() == typeof(LabelElement))
                {
                    st = window_arg.Controls[i] as LabelElement;
                    st.PixelLeft = li_win_width - st.PixelLeft - st.PixelWidth - 25;
                    
                    if (st.TextAlign != ContentAlignment.MiddleCenter)
                    {
                        
                        st.TextAlign = (ContentAlignment)HorizontalAlignment.Left;
                    }
                    st.RightToLeft = RightToLeft.No;
                }
                else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    cb = window_arg.Controls[i] as ButtonElement;
                    cb.PixelLeft = li_win_width - cb.PixelLeft - cb.PixelWidth - 25;
                }
                
                else if (window_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    uo = window_arg.Controls[i] as CompositeElement;
                    uo.PixelLeft = li_win_width - uo.PixelLeft - uo.PixelWidth - 25;
                    fnv_rotate_uo(uo); // stas 31/05/04
                }
                else if (window_arg.Controls[i].GetType() == typeof(GridElement))
                {
                    dw = window_arg.Controls[i] as GridElement;
                    dw.PixelLeft = li_win_width - dw.PixelLeft - dw.PixelWidth - 25;
                }
                else if (window_arg.Controls[i].GetType() == typeof(ButtonElement))
                {
                    pb = window_arg.Controls[i] as ButtonElement;
                    pb.PixelLeft = li_win_width - pb.PixelLeft - pb.PixelWidth - 25;
                }
                //// TODO: Field 'VScrollBar of type 'Sybase.PowerBuilder.Object' is unmapped'. (CODE=1004)
                //else if (window_arg.Controls[i].GetType() == ObjectExtensions.VScrollBar)
                //{
                //    vsb = window_arg.Controls[i] as VScrollBar;
                //    vsb.PixelLeft = li_win_width - vsb.PixelLeft - vsb.PixelWidth - 25;
                //}
                //// TODO: Field 'HScrollBar of type 'Sybase.PowerBuilder.Object' is unmapped'. (CODE=1004)
                //else if (window_arg.Controls[i].GetType() == ObjectExtensions.HScrollBar)
                //{
                //    hsb = window_arg.Controls[i] as HScrollBar;
                //    hsb.PixelLeft = li_win_width - hsb.PixelLeft - hsb.PixelWidth - 25;
                //}
                else if (window_arg.Controls[i].GetType() == typeof(MaskedTextBoxElement))
                {
                    em = window_arg.Controls[i] as MaskedTextBoxElement;
                    em.PixelLeft = li_win_width - em.PixelLeft - em.PixelWidth - 25;
                    //---- stas 4/7/2004 ---

                    // TODO: Field 'DateTimeMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)
                    // if (em.get_MaskDataType() == MaskDataType.DateTimeMask)
                    if (Extensions.get_MaskDataType() == MaskDataType.DateTimeMask)
                    {
                        // TODO: Field 'DateTimeMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)

                        // em.SetMask((MaskDataType.DateTimeMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )
                        Extensions.SetMask((MaskDataType.DateTimeMask), fnv_change_date_format2(em.Mask)); //fnv_change_date_format( em.Mask ) )
                    }

                    // TODO: Field 'DateMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)
                    //else if (em.get_MaskDataType() == MaskDataType.DateMask)
                    else if (Extensions.get_MaskDataType() == MaskDataType.DateMask)
                    {
                        // TODO: Field 'DateMask of type 'Sybase.PowerBuilder.MaskDataType' is unmapped'. (CODE=1004)

                        //em.SetMask((MaskDataType.DateMask), fnv_change_date_format2(em.Mask)); // fnv_change_date_format( em.Mask ) )	
                        Extensions.SetMask((MaskDataType.DateMask), fnv_change_date_format2(em.Mask)); // fnv_change_date_format( em.Mask ) )			
                    }
                    //----------------
                }
                else if (window_arg.Controls[i].GetType() == typeof(RichTextBox))
                {
                    gb = window_arg.Controls[i] as GroupBoxElement;
                    gb.PixelLeft = li_win_width - gb.PixelLeft - gb.PixelWidth - 25;
                    // --- (c) stas
                    gb.RightToLeft = gb.RightToLeft;
                }
                else if (window_arg.Controls[i].GetType() == typeof(TextBoxElement))
                {
                    mle = window_arg.Controls[i] as TextBoxElement;
                    mle.PixelLeft = li_win_width - mle.PixelLeft - mle.PixelWidth - 25;
                    //--- stas 4/7/2004 ---
                    mle.RightToLeft = RightToLeft.No;
                    
                    mle.TextAlign = (HorizontalAlignment)HorizontalAlignment.Left;
                    //---
                }
                else if (window_arg.Controls[i].GetType() == typeof(CheckBoxElement))
                {
                    cbx = window_arg.Controls[i] as CheckBoxElement;
                    cbx.PixelLeft = li_win_width - cbx.PixelLeft - cbx.PixelWidth - 25;
                    cbx.RightToLeft = cbx.RightToLeft;
                    cbx.RightToLeft = RightToLeft.No;
                }
                else if (window_arg.Controls[i].GetType() == typeof(RadioButtonElement))
                {
                    rb = window_arg.Controls[i] as RadioButtonElement;
                    rb.PixelLeft = li_win_width - rb.PixelLeft - rb.PixelWidth - 25;
                    rb.RightToLeft = rb.RightToLeft;
                    rb.RightToLeft = RightToLeft.No;
                }
                else if (window_arg.Controls[i].GetType() == typeof(ComboBoxElement))
                {
                    ddlb = window_arg.Controls[i] as ComboBoxElement;
                    ddlb.PixelLeft = li_win_width - ddlb.PixelLeft - ddlb.PixelWidth - 25;
                    ddlb.RightToLeft = RightToLeft.No;
                }
                else if (window_arg.Controls[i].GetType() == typeof(ListBoxElement))
                {
                    lb = window_arg.Controls[i] as ListBoxElement;
                    lb.PixelLeft = li_win_width - lb.PixelLeft - lb.PixelWidth - 25;
                }
                else if (window_arg.Controls[i].GetType() == typeof(CompositeElement))
                {
                    ol = window_arg.Controls[i] as CompositeElement;
                    ol.PixelLeft = li_win_width - ol.PixelLeft - ol.PixelWidth;
                    //----  stas  28/04/04 ----------------
                }
                else if (window_arg.Controls[i].GetType() == typeof(TabElement))
                {
                    t = window_arg.Controls[i] as TabElement;
                    t.PixelLeft = li_win_width - t.PixelLeft - t.PixelWidth - 25; //for test STAS 14/6/2004
                    fnv_rotate_tab(t);
                    //------ stas 29/04/04 ------------------------------
                }
                else if (window_arg.Controls[i].GetType() == typeof(ImageElement))
                {
                    p = window_arg.Controls[i] as ImageElement;
                    p.PixelLeft = li_win_width - p.PixelLeft - p.PixelWidth - 25;
                    //------- stas 11/05/04 -------------------------------	 
                }
                else if (window_arg.Controls[i].GetType() == typeof(TreeElement))
                {
                    tv = window_arg.Controls[i] as TreeElement;
                    tv.PixelLeft = li_win_width - tv.PixelLeft - tv.PixelWidth - 25;
                    //----- stas 24/05/04 ----------------------------	
                }
                else if (window_arg.Controls[i].GetType() == typeof(LinkLabelElement))
                {
                    shl = window_arg.Controls[i] as LinkLabelElement;
                    shl.PixelLeft = li_win_width - shl.PixelLeft - shl.PixelWidth - 25;
                    shl.RightToLeft = shl.RightToLeft;
                    //IF shl.Alignment = Left! THEN
                    //	shl.Alignment = right!
                    //			ELSEIF shl.Alignment = right! THEN
                    
                    shl.TextAlign = (ContentAlignment)HorizontalAlignment.Left;
                    //		END IF
                    //----stas ------- 27/05/04 -------------------------------		
                }
                //else if (window_arg.Controls[i].GetType() == typeof(Rectangle))
                //{
                //    r = global.Extensions.window_arg.Controls[i] as Rectangle;
                //    r.X = li_win_width - r.X - r.Width - 25;
                //}
            }
        }
        public async Task fnv_translate_dw(GridElement datawindow_arg)
        {
            string ls_colname = null;
            string dw_Objects = null;
            string ls_tmp = null;
            string ls_type = null;
            string ls_style = null;
            string ls_ddlb_value = null;
            string ls_ddlb_expr = null;
            long len_translate = 0;
            long ll_need_field = 0;
            string len_field = null;
            string ls_translate = null;
            string ls_new_font = null;
            string font_size = null;
            GridElement dwc;
            string ls_display = null;
            long ll_dwc_width = 0;
            int li_ddlb_index = 0;
            string ls_dw_Objects_temp = null;
            li_ddlb_index = 1;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            if (Convert.ToString(datawindow_arg.Tag) == "EXIT TRANSLATOR")
            {
                return;
            }
            // ------------------- Translate datawindow title ------------------

            ls_tmp =await  fnv_find_exp_in_dic(datawindow_arg.Text);
            if (!(isempty_stringClass.isempty_string(ls_tmp)))
            {
                datawindow_arg.Text = ls_tmp;
            }
            // ------------------ Translate datawindow text fields (labels) ----


            dw_Objects = datawindow_arg.ColumnsNames();
            ls_dw_Objects_temp = "\t";
            while (dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref dw_Objects, "\t");
                if (ls_dw_Objects_temp.IndexOf("\t" + ls_colname + "\t") > 0)
                {
                    continue;
                }
                else
                {
                    ls_dw_Objects_temp += ls_colname + "\t";
                }
                //.. we can use object.tag field to no translation

                if (!isempty_stringClass.isempty_string(datawindow_arg.Describe(ls_colname, "tag")))
                {

                    if (datawindow_arg.Describe(ls_colname, "tag") == "EXIT TRANSLATOR")
                    {
                        continue;
                    }
                }

                ls_type = datawindow_arg.Describe(ls_colname, "type");

                if (datawindow_arg.Describe(ls_colname, "dddw.name").Length > 1)
                {
                    //datawindow_arg.getchild(ls_colname,dwc)
                    //		ll_dwc_width = long(datawindow_arg.describe(ls_colname+ ".width"))*long(datawindow_arg.describe(ls_colname+ ".dddw.percentwidth")) /100
                    //		fnv_translate_dwc(dwc,ll_dwc_width)
                }

                if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "ddlb")
                {
                    // translate dropdownlistbox and radiobuttons in dw
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + Convert.ToChar(9).ToString() + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                    // stas 13/6/2004 ------
                }

                else if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "radiobuttons")
                {
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, "\t");
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + "\t" + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                    //-- stas 1/7/2004 ----
                }


                else if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "edit" && datawindow_arg.Describe(ls_colname, "Edit.CodeTable") == "yes")
                {
                    do
                    {

                        ls_ddlb_value = datawindow_arg.GetValue(ls_colname, li_ddlb_index);
                        if (string.IsNullOrEmpty(ls_ddlb_value))
                        {
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                        ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                        ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                        datawindow_arg.SetValue(ls_colname, li_ddlb_index, ls_ddlb_expr + Convert.ToChar(9).ToString() + ls_ddlb_value);

                        li_ddlb_index++;
                    }
                    while (!string.IsNullOrEmpty(ls_ddlb_value));
                    li_ddlb_index = 1;
                }
                //-----
                //	IF ls_type = "text" OR ls_type = "CheckBox" OR ls_type="button" OR ls_type="groupbox" THEN
                // ------ Elena 30/05/06	 --------	
                if (ls_type == "text" || (ls_type == "button" || ls_type == "groupbox"))
                {
                    // ------ stas 17/06/2004	 --------

                    ls_tmp = datawindow_arg.Describe(ls_colname, "text");
                    if (ls_tmp.Trim().IndexOf("if(") == 0) //AND Pos( ls_tmp,"~~" ) = 0 THEN  
                    {
                        ls_tmp = await fnv_find_exp_in_dic(ls_tmp);

                        datawindow_arg.Modify("ls_colname", "text", "ls_tmp");
                    }
                    //--------------------	
                    /////////////////////////////////////////////////////////////////////
                    // this code calculate the relative column size and resize the font
                    // of each column
                    ////////////////////////////////////////////////////////////////////// 
                    //len_translate=len(ls_tmp)
                    //			ls_translate=ls_tmp
                    //			len_field=datawindow_arg.Describe(ls_colname, "Width")
                    //			font_size=datawindow_arg.Describe(ls_colname, "Font.Height")
                    //			font_size=Mid(font_size, 2)
                    //			ll_need_field=dec(len_translate * (40) * integer (font_size)/12)
                    //
                    //			IF dec(len_field) < ll_need_field THEN 
                    //   			ls_new_font = string(long ( -.85*(long(font_size) *(long(len_field)/long(ll_need_field)))))
                    //				IF long(ls_new_font) * -1 < 7 THEN ls_new_font = "-7"
                    //				datawindow_arg.Modify(ls_colname, Font.Height =" + ls_new_font)
                    //			END IF 
                    ////////////////////////////////////////////////////
                }
                // ------ Elena 30/05/06	 --------

                ls_style = datawindow_arg.Describe(ls_colname, "Edit.Style");
                if (ls_style == "checkbox")
                {
                    // ------ stas 17/06/2004	 --------

                    ls_tmp = datawindow_arg.Describe(ls_colname, "CheckBox.Text"); // ".text")
                    if (ls_tmp.Trim().IndexOf("if(") == 0) //AND Pos( ls_tmp,"~~" ) = 0 THEN  
                    {
                        ls_tmp = await fnv_find_exp_in_dic(ls_tmp);

                        datawindow_arg.Modify("ls_colname", "CheckBox.Text", "ls_tmp");
                    }
                }

                datawindow_arg.Modify("ls_colname", "font.face", "lang_font");

                datawindow_arg.Modify("ls_colname", "Font.CharSet", "1");
                if (ii_font_decrease > 0)
                {

                    font_size = datawindow_arg.Describe(ls_colname, "Font.Height");
                    ls_new_font = Convert.ToInt64((Convert.ToInt64(font_size) + ii_font_decrease)).ToString();
                    if (Convert.ToInt64(ls_new_font) * -1 < il_min_dw_language_font)
                    {
                        ls_new_font = (il_min_dw_language_font * -1).ToString();
                    }

                    datawindow_arg.Modify(ls_colname, "Font.Height", ls_new_font);
                }
            }
        }
        public async Task fnv_rotate_dw(GridElement datawindow_arg)
        {
            if (!multi_lingual_is_active)
            {
                return;
            }
            if (direction != "left")
            {
                return;
            }
            string ls_colname = null;
            string ls_dw_Objects = null;
            string ls_col_type = null;
            int li_dw_width = 0;
            int li_col_width = 0;
            int li_col_x = 0;
            int li_alignment = 0;
            int ll_line_x = 0;
            int ll_line_width = 0;
            string ls_temp = null;
            long ll_i = 0;
            long ll_j = 0;
            string ls_ddlb_value_1 = null;
            string ls_ddlb_value_2 = null;
            string ls_ddlb_expr_1 = null;
            string ls_ddlb_expr_2 = null;
            string ls_dw_Objects_temp = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }


            ls_dw_Objects = datawindow_arg.ColumnsNames();
            ls_dw_Objects_temp = "\t";
            //------- stas ---- 09/05/04 ------------------------
            li_dw_width = fnv_get_dw_width(datawindow_arg) + 10; //+ 25 // for test 28/6/2004

            if (SystemFunctionsExtensions.IsNumber(Convert.ToString(datawindow_arg.Tag)))
            {
                li_dw_width += 15; //for test 28/6/2004
                li_dw_width -= Convert.ToInt32(datawindow_arg.Tag);
            }
            else if (Convert.ToString(datawindow_arg.Tag) == "EXIT TRANSLATOR")
            {
                return;
            }
            //------------------------------------------------
            while (ls_dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_dw_Objects, "\t");
                if (ls_dw_Objects_temp.IndexOf("\t" + ls_colname + "\t") > 0)
                {
                    continue;
                }
                else
                {
                    ls_dw_Objects_temp += ls_colname + "\t";
                }

                if (datawindow_arg.Describe(ls_colname, "x") == "?")
                {
                    continue;
                }

                li_col_width = (int)Convert.ToInt64(datawindow_arg.Describe(ls_colname, "width"));

                li_col_x = (int)Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x"));

                li_alignment = (int)Convert.ToInt64(datawindow_arg.Describe(ls_colname, "alignment"));

                ls_col_type = datawindow_arg.Describe(ls_colname, "ColType");
                //stas 4/7/2004 --------------


                if (datawindow_arg.Describe(ls_colname, "Coltype") == "datetime" || datawindow_arg.Describe(ls_colname, "Coltype") == "date")
                {

                    if (datawindow_arg.Describe(ls_colname, "EditMask.Mask").Length > 1)
                    {

                        ls_temp = datawindow_arg.Describe(ls_colname, "EditMask.Mask");
                        ls_temp = await fnv_change_date_format2(ls_temp);

                        datawindow_arg.Modify(ls_colname, "EditMask.Mask", ls_temp); //'mm/dd/yy'")			
                    }

                    if (datawindow_arg.Describe(ls_colname, "Format").Length > 1)
                    {

                        ls_temp = datawindow_arg.Describe(ls_colname, "Format");
                        ls_temp = await fnv_change_date_format2(ls_temp);

                        datawindow_arg.Modify(ls_colname, "format", ls_temp); // m/d/yy h:mm'")			
                    }
                }
                //------------
                //	----- stas 17/05/04 -----------------------------------------------

                if (datawindow_arg.Describe(ls_colname, "Type") == "line")
                {
                    long ll_line_x2 = 0;

                    ll_line_x = (int)Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x1"));

                    ll_line_x2 = Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x2"));


                    ll_line_width = (int)(Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x2")) - Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x1")));
                    if (ll_line_x == ll_line_x2)
                    {

                        datawindow_arg.Modify("ls_colname", "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());


                        datawindow_arg.Modify(ls_colname, "x2", datawindow_arg.Describe(ls_colname, "x1"));
                    }
                    else
                    {

                        datawindow_arg.Modify("ls_colname", "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());


                        datawindow_arg.Modify(ls_colname, "x2", (Convert.ToInt64(datawindow_arg.Describe(ls_colname, "x1")).ToString() + ll_line_width.ToString()).ToString() + "'");
                    }
                }
                else
                {

                    datawindow_arg.Modify(ls_colname, "x", (li_dw_width - li_col_x - li_col_width).ToString());
                    if (direction == "left" && (li_alignment != 2 && (ls_col_type.Substring(0, 7) != "decimal" && (ls_col_type != "long" && (ls_col_type != "integer" && ls_col_type != "number")))))
                    {
                        //IF li_alignment = 1 THEN

                        if (datawindow_arg.Describe(ls_colname, "Edit.Style") != "radiobuttons")
                        {

                            datawindow_arg.Modify(ls_colname, "alignment", "0");
                        }
                        //ELSE
                        // datawindow_arg.Modify(ls_colname, alignment = '1'"  )
                        //END IF
                    }

                    if (datawindow_arg.Describe(ls_colname, "Edit.Style") != "radiobuttons")
                    {

                        datawindow_arg.Modify(ls_colname, "righttoleft", "0");
                    }
                    else
                    {
                        //------------ stas 13/6/2004 ---------------------------------	
                        ll_i = 1;

                        ll_j = Convert.ToInt64(datawindow_arg.Describe(ls_colname, "RadioButtons.Columns"));
                        while (ll_i < ll_j)
                        {

                            ls_ddlb_value_1 = datawindow_arg.GetValue(ls_colname, (int)ll_i);

                            ls_ddlb_value_2 = datawindow_arg.GetValue(ls_colname, (int)ll_j);
                            ls_ddlb_expr_1 = f_get_tokenClass.f_get_token(ref ls_ddlb_value_1, "\t");
                            ls_ddlb_expr_2 = f_get_tokenClass.f_get_token(ref ls_ddlb_value_2, "\t");

                            datawindow_arg.SetValue(ls_colname, (int)ll_i, ls_ddlb_expr_2 + "\t" + ls_ddlb_value_2);

                            datawindow_arg.SetValue(ls_colname, (int)ll_j, ls_ddlb_expr_1 + "\t" + ls_ddlb_value_1);

                            ll_i++;
                            ll_j -= 1;
                        }
                        //----------------------
                    }

                    if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "checkbox")
                    {

                        datawindow_arg.Modify(ls_colname, "CheckBox.LeftText", "Yes");
                    }
                }
                //--------------------------------------------------------------------------------
            }
            //------------- stas ------ 2/05/04 ------- // scrolls for the left side

            //if (datawindow_arg.get_HScrollBar())
            //{

            //    datawindow_arg.Modify("DataWindow", "HorizontalScrollPosition", "0");
            //}
            //-----------------------------------------------------------------------------
        }
        public async Task fnv_rotate_dwc(GridElement dwc, int dwc_width)
        {
            if (!multi_lingual_is_active)
            {
                return;
            }
            string ls_colname = null;
            string ls_dw_Objects = null;
            string ls_col_type = null;
            int li_dw_width = 0;
            int li_col_width = 0;
            int li_col_x = 0;
            int li_alignment = 0;
            string ls_temp = null;
            string ls_dw_Objects_temp = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            //..return if no rotation need
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left())
            {
                return;
            }

            ls_dw_Objects = dwc.Describe("datawindow", "objects");
            ls_dw_Objects_temp = "\t";
            li_dw_width = dwc_width;
            while (ls_dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_dw_Objects, "\t");
                if (ls_dw_Objects_temp.IndexOf("\t" + ls_colname + "\t") > 0)
                {
                    continue;
                }
                else
                {
                    ls_dw_Objects_temp += ls_colname + "\t";
                }

                ls_col_type = dwc.Describe(ls_colname, "ColType");

                li_col_width = (int)Convert.ToInt64(dwc.Describe(ls_colname, "width"));

                li_col_x = (int)Convert.ToInt64(dwc.Describe(ls_colname, "x"));

                li_alignment = (int)Convert.ToInt64(dwc.Describe(ls_colname, "alignment"));
                //stas 4/7/2004 --------------


                if (dwc.Describe(ls_colname, "Coltype") == "datetime" || dwc.Describe(ls_colname, "Coltype") == "date")
                {

                    if (dwc.Describe(ls_colname, "EditMask.Mask").Length > 1)
                    {

                        ls_temp = dwc.Describe(ls_colname, "EditMask.Mask");
                        ls_temp = await fnv_change_date_format2(ls_temp);

                        dwc.Modify(ls_colname, "EditMask.Mask", ls_temp); //'mm/dd/yy'")						
                    }

                    if (dwc.Describe(ls_colname, "Format").Length > 1)
                    {

                        ls_temp = dwc.Describe(ls_colname, "Format");
                        ls_temp =  await fnv_change_date_format2(ls_temp);

                        dwc.Modify(ls_colname, "format", ls_temp); // m/d/yy h:mm'")			
                    }
                }
                //------------
                //----- stas 3/06/04 -----------------------------------------------

                if (dwc.Describe(ls_colname, "Type") == "line")
                {
                    long ll_line_x2 = 0;
                    long ll_line_x = 0;
                    long ll_line_width = 0;

                    ll_line_x = Convert.ToInt64(dwc.Describe(ls_colname, "x1"));

                    ll_line_x2 = Convert.ToInt64(dwc.Describe(ls_colname, "x2"));


                    ll_line_width = Convert.ToInt64(dwc.Describe(ls_colname, "x2")) - Convert.ToInt64(dwc.Describe(ls_colname, "x1"));
                    if (ll_line_x == ll_line_x2)
                    {

                        dwc.Modify(ls_colname, "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());


                        dwc.Modify(ls_colname, "x2", dwc.Describe(ls_colname, "x1"));
                    }
                    else
                    {

                        dwc.Modify(ls_colname, "x1", (li_dw_width - ll_line_x - ll_line_width).ToString());


                        dwc.Modify(ls_colname, "x2", (Convert.ToInt64(dwc.Describe(ls_colname, "x1")).ToString() + ll_line_width.ToString()).ToString());
                    }
                }
                else
                {

                    dwc.Modify(ls_colname, "x", (li_dw_width - li_col_x - li_col_width).ToString());
                }
                //---------
                //----------------- stas ------ 29/04/04 -------- 
                if (direction == "left" && (li_alignment != 2 && (ls_col_type.Substring(0, 7) != "decimal" && (ls_col_type != "long" && (ls_col_type != "integer" && ls_col_type != "number")))))
                {
                    //----			
                    //IF li_alignment = 1 THEN

                    dwc.Modify(ls_colname, "alignment", "0");
                    //ELSE
                    //dwc.Modify(ls_colname, alignment = '1'")
                    //END IF

                    dwc.Modify(ls_colname, "righttoleft", "0"); //stas for testing 8/6/2004
                }
            }
        }
        public async Task fnv_translate_dw_columns(GridElement datawindow_arg) // ------ stas 09/06/2004	 --------
        {
            // translate all collumns in datawindow (in details of DataWindowObject)
            string ls_colname = null;
            string dw_Objects = null;
            string tmp = null;
            string tmp_type = null;
            string ls_ddlb_value = null;
            string ls_ddlb_expr = null;
            long len_translate = 0;
            long ll_need_field = 0;
            string len_field = null;
            string ls_translate = null;
            string ls_new_font = null;
         //   string font_size = null;
          //  GridElement dwc;
            string ls_display = null;
            long ll_dwc_width = 0;
            int li_ddlb_index = 1;
            string ls_dw_Objects_temp = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }


            dw_Objects = datawindow_arg.ColumnsNames();
            ls_dw_Objects_temp = "\t";
            while (dw_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref dw_Objects, "\t");
                if (ls_dw_Objects_temp.IndexOf("\t" + ls_colname + "\t") > 0)
                {
                    continue;
                }
                else
                {
                    ls_dw_Objects_temp += ls_colname + "\t";
                }

                tmp_type = datawindow_arg.Describe(ls_colname, "type");
                if (tmp_type == "column")
                {

                    if (datawindow_arg.Describe(ls_colname, "text").IndexOf("if(") == 0)
                    {

                        tmp = await fnv_find_exp_in_dic(datawindow_arg.Describe(ls_colname, "text"));

                        datawindow_arg.Modify("ls_colname", "text", "tmp");
                    }

                    if (datawindow_arg.Describe(ls_colname, "Edit.Style") == "checkbox")
                    {

                        tmp = await fnv_find_exp_in_dic(datawindow_arg.Describe(ls_colname, "CheckBox.Text"));

                        datawindow_arg.Modify("ls_colname", "CheckBox.Text", "tmp");
                    }
                }
            }
        }
        public async Task fnv_translate_dw_ddlb(GridElement datawindow_arg, string colname)
        {
            //------ stas 10/6/2004 ------------------------
            // translates all ddlb values in DataWindow
            // we can use this function for dynamic DataWindow
            int li_ddlb_index = 0;
            string ls_ddlb_expr = null;
            string ls_ddlb_value = null;
            //.. return if multy lingual mode is off
            if (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual())
            {
                return;
            }
            //.. return if current office language is hebrew - no need translation
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                return;
            }
            li_ddlb_index = 1;


            if (datawindow_arg.Describe(colname, "Edit.Style") == "ddlb" || datawindow_arg.Describe(colname, "Edit.Style") == "radiobuttons")
            {
                // translate dropdownlistbox and radiobuttons in dw
                do
                {

                    ls_ddlb_value = datawindow_arg.GetValue(colname, li_ddlb_index);
                    if (string.IsNullOrEmpty(ls_ddlb_value))
                    {
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                    ls_ddlb_expr = f_get_tokenClass.f_get_token(ref ls_ddlb_value, Convert.ToChar(9).ToString());
                    ls_ddlb_expr = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(ls_ddlb_expr);

                    datawindow_arg.SetValue(colname, li_ddlb_index, ls_ddlb_expr + Convert.ToChar(9).ToString() + ls_ddlb_value);

                    li_ddlb_index++;
                }
                while (!string.IsNullOrEmpty(ls_ddlb_value));
                li_ddlb_index = 1;

                if (datawindow_arg.Describe(colname, "Edit.Style") == "radiobuttons")
                {

                    datawindow_arg.Modify(colname, "RadioButtons.LeftText", "Yes");

                    datawindow_arg.Modify(colname, "righttoleft", "1");
                }
            }
        }
        public string fnv_change_date_format(string as_date)
        {
            string ls_ret = null;
            as_date = as_date.ToLower(CultureInfo.InvariantCulture);
            switch (as_date)
            {
                case "dd/mm/yy hh:mm:ss":
                case "[date] [time]":
                    ls_ret = "mm/dd/yy hh:mm:ss";
                    break;
                case "dd/mm/yyyy hh:mm:ss":
                    ls_ret = "mm/dd/yyyy hh:mm:ss";
                    break;
                case "dd/mmm/yy hh:mm:ss":
                    ls_ret = "mmm/dd/yy hh:mm:ss";
                    break;
                case "dd/mm/yy":
                    ls_ret = "mm/dd/yy";
                    break;
                case "dd/mm/yyyy":
                    ls_ret = "mm/dd/yyyy";
                    break;
                case "dd/mmm/yy":
                    ls_ret = "mmm/dd/yy";
                    break;
                case "[shortdate] [time]":
                    ls_ret = "m/d/yy h:mm:ss";
                    break;
                case "dd/mm/yy hh:mm":
                    ls_ret = "mm/dd/yy hh:mm";
                    break;
                case "dd/mm/yyyy hh:mm":
                    ls_ret = "mm/dd/yyyy hh:mm";
                    break;
            }
            return ls_ret;
        }
        public async Task<string> fnv_change_date_format2(string as_date) // ----- stas 5/7/2004-------------
        {
            //.. this function returns agregate appropriate DateTime? value for the DateTime? format
            if (!(masofonAlias.masofon.Instance.nvo_translator.multi_lingual_is_active && masofonAlias.masofon.Instance.nvo_translator.direction == "left"))
            {
                return as_date;
            }
            string ls_date_format = null;
            string ls_reg_value = null;
            string ls_create_expressions_on_fly = null;
            string ls_day = null;
            string ls_month = null;
            string ls_year = null;
            string ls_time = null;
            string ls_sep = null;
            bool lb_date_place = false;
            as_date = as_date.ToLower(CultureInfo.InvariantCulture);
            if (as_date == "[general]")
            {
                return as_date;
            }
            ls_day = "d";
            ls_month = "m";
            ls_year = "yy";
            ls_sep = "/";
            if (as_date.IndexOf("-") != 0)
            {
                ls_sep = "-";
            }
            as_date = as_date.Trim();
            switch (as_date)
            {
                case "dd/mm/yy hh:mm:ss":
                case "dd-mm-yy hh:mm:ss":
                case "[date] [time]":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yy";
                    ls_time = "hh:mm:ss";
                    break;
                case "dd/mm/yyyy hh:mm:ss":
                case "dd-mm-yyyy hh:mm:ss":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yyyy";
                    ls_time = "hh:mm:ss";
                    break;
                case "dd/mmm/yy hh:mm:ss":
                case "dd-mmm-yy hh:mm:ss":
                    ls_day = "dd";
                    ls_month = "mmm";
                    ls_year = "yy";
                    ls_time = "hh:mm:ss";
                    break;
                case "dd/mm/yy":
                case "yy/mm/dd":
                case "dd-mm-yy":
                case "yy-mm-dd":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yy";
                    break;
                case "dd/mm/yyyy":
                case "dd-mm-yyyy":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yyyy";
                    break;
                case "dd/mmm/yy":
                case "dd-mmm-yy":
                    ls_day = "dd";
                    ls_month = "mmm";
                    ls_year = "yy";
                    break;
                case "[shortdate] [time]":
                case "[time]":
                case "[shortdate]":
                case "[shortdate][time]":
                    return as_date;
                case "dd/mm/yy hh:mm":
                case "dd-mm-yy hh:mm":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yy";
                    ls_time = "hh:mm";
                    break;
                case "dd/mm/yyyy hh:mm":
                case "dd-mm-yyyy hh:mm":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yyyy";
                    ls_time = "hh:mm";
                    break;
                case "mm/dd/yy hh:mm:ss:fff":
                case "mm-dd-yy hh:mm:ss:fff":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yy";
                    ls_time = "hh:mm:ss:fff";
                    break;
                case "mm/dd/yy":
                case "mm-dd-yy":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yy";
                    // tsvika 8/7/04 -------------------------------
                    break;
                case "dd/mm":
                case "dd-mm":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "";
                    //----------------------------------------------
                    break;
                case "hh:mm:ss":
                case "hh:mm":
                    return as_date;
                case "hh:mm dd/mm/yyyy":
                case "hh:mm dd-mm-yyyy":
                    ls_day = "dd";
                    ls_month = "mm";
                    ls_year = "yyyy";
                    ls_time = "hh:mm";
                    //lb_date_place = TRUE
                    break;
                default:
                    ls_create_expressions_on_fly = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Language", "create_expressions_on_fly", "no", "");
                    if (ls_create_expressions_on_fly.ToLower(CultureInfo.InvariantCulture) == "yes")
                    {
                      await  masofonAlias.masofon.Instance.guo_msg.uf_msg("Add DateTime? Format to fnv_change_date_format2", "", "stopsign!", as_date);
                    }
                    return as_date;
            }
            //.. Get Date Format
            switch (is_date_format)
            {
                case "DYM":
                    ls_date_format = ls_day + ls_sep + ls_year + ls_sep + ls_month;
                    break;
                case "MDY":
                    ls_date_format = ls_month + ls_sep + ls_day + ls_sep + ls_year;
                    break;
                case "MYD":
                    ls_date_format = ls_month + ls_sep + ls_year + ls_sep + ls_day;
                    break;
                case "YMD":
                    ls_date_format = ls_year + ls_sep + ls_month + ls_sep + ls_day;
                    break;
                case "YDM":
                    ls_date_format = ls_year + ls_sep + ls_day + ls_sep + ls_month;
                    //.. "DMY"
                    break;
                default:
                    ls_date_format = ls_day + ls_sep + ls_month + ls_sep + ls_year;
                    break;
            }
            // tsvika 8/7/04 
            if (as_date == "dd/mm" || as_date == "dd-mm")
            {
                ls_date_format = ls_date_format.Substring(0, 5);
            }
            //------------------
            if (!lb_date_place)
            {
                if (!string.IsNullOrEmpty(ls_time))
                {
                    ls_date_format = ls_date_format + " " + ls_time;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ls_time))
                {
                    ls_date_format = ls_time + " " + ls_date_format;
                }
            }
            return ls_date_format;
        }
        public string fnv_translate_page_number()
        {
            string ls_temp = null;
            ls_temp = "Page + page() +  of  + pageCount()";
            return ls_temp;
        }
        public void fnv_change_language_menu(ToolBarMenuItem menu_arg)
        {
            int li_total = 0;
            int i = 0;
            string ls_tmp = null;
            string ls_expretion = null;
            if (!fnv_is_multy_lingual())
            {
                return;
            }
            if (fnv_is_hebrew_mode())
            {
                return;
            }
            li_total = CollectionExtensions.UBound(menu_arg.Items);
            for (i = 1; i <= li_total; i++)
            {
                if (!isempty_stringClass.isempty_string(Convert.ToString(menu_arg.Items[i].Tag)) && Convert.ToString(menu_arg.Items[i].Tag).IndexOf("@", StringComparison.Ordinal) > 0)
                {
                    ls_expretion = Convert.ToString(menu_arg.Items[i].Tag);
                    //.. first value can be in use for menu permissions atc.
                    ls_tmp = f_get_tokenClass.f_get_token(ref ls_expretion, "@");
                    menu_arg.Items[i].Text = f_get_tokenClass.f_get_token(ref ls_expretion, "@");
                     
                    //menu_arg.Items[i].set_MicroHelp(f_get_tokenClass.f_get_token(ref ls_expretion, "@"));
                    menu_arg.Items[i].Text = f_get_tokenClass.f_get_token(ref ls_expretion, "@");
                }
                fnv_change_language_menu(menu_arg.Items[i] as ToolBarMenuItem);
            }
        }
        public string fnv_refind_exp_in_dic(string as_search_exp) //fnv_find_exp_in_dic
        {
            long? ll_row = 0;
            string ls_result = null;
            if (!multi_lingual_is_active)
            {
                ls_result = as_search_exp;
            }
            else
            {
                if (!isempty_stringClass.isempty_string(as_search_exp))
                {
                    
                    if (dw_dic.RowCount() > 0)
                    {
                        if (as_search_exp.IndexOf("'") > 0)
                        {
                            
                            
                            ll_row = dw_dic.Find("translation == \"" + as_search_exp.TrimStart().TrimEnd() + "\"", 0, dw_dic.RowCount());
                        }
                        else
                        {
                            
                            
                            ll_row = dw_dic.Find("translation == \"" + as_search_exp.TrimStart().TrimEnd() + "\"", 0, dw_dic.RowCount());
                        }
                        if (ll_row > 0)
                        {
                            
                            ls_result = dw_dic.GetItemValue<string>(ll_row.Value, "expressions_name");
                            if (ls_result == "*********  Add Translation **********" && ib_create_expressions)
                            {
                                //ls_result = uf_update_dicionary(dw_dic.GetItem<int>(ll_row, "expressions_number"), as_search_exp)
                            }
                        }
                        else
                        {
                            if (ib_create_expressions)
                            {
                                //ls_result = uf_update_dicionary(0, as_search_exp)
                            }
                            else
                            {
                                ls_result = as_search_exp;
                            }
                        }
                    }
                    else
                    {
                        ls_result = as_search_exp;
                    }
                }
                else
                {
                    ls_result = as_search_exp.TrimStart();
                }
            }
            return ls_result;
        }
        public bool fnv_is_hebrew_string(string as_expression) //.. if one of characters if hebrew return true
        {
            return (as_expression.IndexOf("א") > 0 || (as_expression.IndexOf("ב") > 0 || (as_expression.IndexOf("ג") > 0 || (as_expression.IndexOf("ד") > 0 || (as_expression.IndexOf("ה") > 0 || (as_expression.IndexOf("ו") > 0 || (as_expression.IndexOf("ז") > 0 || (as_expression.IndexOf("ח") > 0 || (as_expression.IndexOf("ט") > 0 || (as_expression.IndexOf("י") > 0 || (as_expression.IndexOf("ק") > 0 || (as_expression.IndexOf("ר") > 0 || (as_expression.IndexOf("ן") > 0 || (as_expression.IndexOf("ם") > 0 || (as_expression.IndexOf("פ") > 0 || (as_expression.IndexOf("ש") > 0 || (as_expression.IndexOf("כ") > 0 || (as_expression.IndexOf("ע") > 0 || (as_expression.IndexOf("ל") > 0 || (as_expression.IndexOf("ך") > 0 || (as_expression.IndexOf("ף") > 0 || (as_expression.IndexOf("ס") > 0 || (as_expression.IndexOf("נ") > 0 || (as_expression.IndexOf("מ") > 0 || (as_expression.IndexOf("צ") > 0 || (as_expression.IndexOf("ת") > 0 || as_expression.IndexOf("ץ") > 0))))))))))))))))))))))))));
        }
        public bool fnv_is_multy_lingual() // if multy lingual mode then retrun true
        {
            return multi_lingual_is_active;
        }
        public bool fnv_is_hebrew_mode() //.. if hebrew return true
        {
            return (il_current_lang == il_hebrew_language);
        }
        public bool fnv_is_direction_left() // return true if direction left
        {
            return (direction == "left");
        }
        public void fnv_set_min_dw_language_font(long al_min_font) //fnv_set_min_dw_language_font
        {
            il_min_dw_language_font = al_min_font;
        }
        public int fnv_get_ds_width(ref GridElement ads_arg)
        {
            string ls_colname = null;
            string ls_ads_arg_Objects = null;
            string ls_col_width = null;
            string ls_col_x = null;
            int li_max_width = 0;
            int li_width_temp = 0;
             
            if (ads_arg == null)
            {
                return 0;
            }


            ls_ads_arg_Objects = ads_arg.ColumnsNames();
            while (ls_ads_arg_Objects.Length > 0)
            {
                ls_colname = f_get_tokenClass.f_get_token(ref ls_ads_arg_Objects, "\t");
                 
                ls_col_width = ads_arg.Describe(ls_colname + ".Width");
                 
                ls_col_x = ads_arg.Describe(ls_colname + ".X");
                li_width_temp = Convert.ToInt32(ls_col_width) + Convert.ToInt32(ls_col_x);
                if (li_width_temp > li_max_width)
                {
                    li_max_width = li_width_temp;
                }
            }
            return li_max_width;
            //--------------------------------------------------------------------
        }
        public long fnv_get_min_dw_language_font()
        {
            return il_min_dw_language_font;
        }
        public long fnv_get_current_language()
        {
            return il_current_lang;
        }
        public nv_translator()
        {
      
        }

        public async Task constructor()
        {
            int li_current_language = 0;
            multi_lingual_is_active = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "language", "multi_lingual_is_active", "no", "").ToLower(CultureInfo.InvariantCulture) == "yes";
            if (!multi_lingual_is_active)
            {
                direction = "right";
                il_current_lang = 3;
                return;
            }
            ib_create_expressions = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "language", "create_expressions_on_fly", "yes", "").ToLower(CultureInfo.InvariantCulture) == "yes";
            ib_display_translate_window = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "language", "display_translation_window_on_fly", "no", "").ToLower(CultureInfo.InvariantCulture) == "yes";
            li_current_language = Convert.ToInt32(RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "language", "default_language", il_hebrew_language.ToString(), ""));
            if (li_current_language < 1)
            {
                multi_lingual_is_active = false;
                direction = "right";
                return;
            }
            if (true)
            {
                LoadData2(li_current_language);
            }

            switch (masofonAlias.masofon.Instance.sqlca.SqlCode)
            {
                case 100:
                    //.. no language defined
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Translator error", "", "exclamation!", "No languages found! Translator disabled");
                    multi_lingual_is_active = false;
                    direction = "right";
                    return;
                case -1:
                    //.. db error
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Translator error", "", "exclamation!", "Error retrieving languages");
                    multi_lingual_is_active = false;
                    direction = "right";
                    return;
            }
            //.. set direction of windows
            if (isempty_stringClass.isempty_string(direction))
            {
                if (li_current_language == il_hebrew_language)
                {
                    //.. hebrew
                    direction = "right";
                }
                else
                {
                    //.. other
                    direction = "left";
                }
            }
            //.. create dictionary 

             
            dw_dic = new dw_window_dicRepository("", "", "", "", "");
             
            if (dw_dic.Retrieve(li_current_language) < 1)
            {
                 await masofonAlias.masofon.Instance.guo_msg.uf_msg("Translator error", "", "exclamation!", "Dictionary Load Failed! No expressions in DataBase");
                multi_lingual_is_active = false;
                direction = "right";
                return;
            }
            il_current_lang = li_current_language;
            if (li_current_language == il_hebrew_language)
            {
                multi_lingual_is_active = false;
            }
            ist_charset = new LabelElement();
            uf_set_charset();
            string ls_reg_value = null;
            //----------- stas 5/7/2004 ------------------------------------
            //.. gets date format from registry
            ls_reg_value = RegToDatabase.GetRegistryItem("HKEY_CURRENT_USER\\Control Panel\\International",
                "sShortDate", "", ls_reg_value);
            if (string.IsNullOrEmpty(ls_reg_value))
            {
                is_date_format = "DMY";
            }
            else
            {
                ls_reg_value = ls_reg_value.ToLower(CultureInfo.InvariantCulture);
                do
                {
                    switch (ls_reg_value.Substring(0, 1))
                    {
                        case "d":
                            if (is_date_format.IndexOf("D") == 0)
                            {
                                is_date_format += "D";
                            }
                            break;
                        case "m":
                            if (is_date_format.IndexOf("M") == 0)
                            {
                                is_date_format += "M";
                            }
                            break;
                        case "y":
                            if (is_date_format.IndexOf("Y") == 0)
                            {
                                is_date_format += "Y";
                            }
                            break;
                    }
                    ls_reg_value = ls_reg_value.Substring(ls_reg_value.Length - ls_reg_value.Length - 1);
                    if (is_date_format.Length == 3)
                    {
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                }
                while (ls_reg_value.Length > 0);
            }
            fnv_set_min_dw_language_font(7);
        }
        public void destructor()
        {
        }
        public void UpdateData(long ll_new_expression_number, string as_expression)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_new_expression_number }, { "@1", as_expression } };
                    string sql = "INSERT INTO expressions (number,  name) VALUES (@0,  @1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData1(long ll_new_expression_number, string ls_translation)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ll_new_expression_number }, { "@1", ls_translation } };
                    string sql = "INSERT INTO dictionary (application_number,  language_number,  expression_number,  translation) VALUES (1,  1,  @0,  @1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData(ref long ll_new_expression_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT isnull(max(number), 0) + 1 FROM expressions";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        ll_new_expression_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }

        }
        public void LoadData1(Int64 new_lang)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", new_lang } };
                    string sql = "SELECT direction,  language_font,  font_decrease,  keyboard_layout_code,  lower(language_charset) FROM languages WHERE language_number = @0 AND is_active = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        direction = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        lang_font = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        ii_font_decrease = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt32);
                        is_keyboard_layout = UnitOfWork.GetValue(resultRS, 3, Convert.ToString);
                        is_font_charset = UnitOfWork.GetValue(resultRS, 4, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(int li_current_language)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", li_current_language } };
                    string sql = "SELECT direction,  language_font,  font_decrease,  keyboard_layout_code,  lower(language_charset) FROM languages WHERE language_number = @0 AND is_active = 1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        direction = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        lang_font = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        ii_font_decrease = UnitOfWork.GetValue(resultRS, 2, Convert.ToInt32);
                        is_keyboard_layout = UnitOfWork.GetValue(resultRS, 3, Convert.ToString);
                        is_font_charset = UnitOfWork.GetValue(resultRS, 4, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }

    
}
