﻿using System.Extensions;
using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:21:00 (Utc)
    // Comments:        
    // 

    public class u_logs_in_table_service
    {
        //  SERVICE_CODE 	CONSTANTs
        public const long SERVICE_CODE_MESSAGE_BOX = 1;
        public const long SERVICE_CODE_APPLICATION_STATUS = 2;
        public const long SERVICE_CODE_APP_CRASH_EMP_CARD = 3;
        public const long SERVICE_CODE_WRITE_NPFILE = 4;
        public const long SERVICE_CODE_MINUS_2_ERR = 5;
        public const long SERVICE_CODE_MICRO_MARK_LOG = 6;
        public const long SERVICE_CODE_MICRO_COUPONS_LOG = 7;
        public const long SERVICE_CODE_MASOFON_LOG = 9;
        //SERVICE_NAME 	CONSTANTs
        public string[] SERVICE_NAME_C = new string[] {
			"MESSAGE_BOX",
			"APPLICATION_STATUS",
			"APP_CRASH_EMP_CARD",
			"WRITE_NPFILE",
			"MINUS_2_ERR",
			"MICRO_MARK_LOG",
			"MICRO_COUPONS_LOG",
			"UNKNOWN",
			"MASOFON_LOG"
		}; //1
        //2
        //3
        //4
        //5
        //6
        //7
        //8

        //9
        //MSG_CODE 	CONSTANTs
        public const long MSG_CODE_REGULAR = 1;
        //MSG_NAME 	CONSTANTs
        public string[] MSG_NAME_C = new string[] {
			"REGULAR",
			"UNKNOWN"
		}; //1

        //2
        public string is_user_response;
        public async Task<bool> uf_write_log_table(string as_message, long al_service_code, long al_msg_code, string as_title, string as_msg_object, string as_msg_techinfo, long al_log_level)
        {
            //Eduardi 2013-01-21  v_4.1.0.54 SPUCM00003961- Saving logs in the table "error_debug_logs" of the database "LOG_DB".
            //boolean uf_write_log_table (string as_message, long al_service_code, long al_msg_code, string as_title, string as_msg_object, string as_msg_techinfo, long al_log_level)
            //Long  ll_log_level
            bool lb_its_err_msg = false;
            DateTime? ldt_msg_date_time = default(DateTime);
            string ls_error_message = null;
            string ls_log_entry = null;
            string ls_LogLevel = null;
            string ls_msg_date_time = null;
            string ls_transaction_status = null;
            string ls_db_status = null;
            string ls_title = null;
            string ls_msg_object = null;
            string ls_service_name = null;
            string ls_msg_name = null;
            string ls_version_number = null;
            if (al_log_level == 0)
            {
                return true;
            }
            if (al_log_level > masofonAlias.masofon.Instance.gstr_message.write_log_from_level)
            {
                return true;
            }
            if (al_service_code <= SERVICE_NAME_C.UBound())
            {
                ls_service_name = SERVICE_NAME_C[al_service_code].ToLower(CultureInfo.InvariantCulture);
                if (masofonAlias.masofon.Instance.gds_branch_station_params == null)
                {
                    return true;
                }
                if (masofonAlias.masofon.Instance.gds_branch_station_params == null)
                {
                    return true;
                }
                if (await f_get_parameter_valueClass.f_get_parameter_value(ls_service_name, "string", "false", "uf_write_log_table() - Masofon Log", "gds_find") != "true")
                {
                    return true;
                }
            }
            //0 - user response, 1 - warning message
            //2 - parm missed,   3 - database error
            switch (al_log_level)
            {
                case (-4):
                    ls_LogLevel = "ApplicationIdle";
                    break;
                case (-3):
                    ls_LogLevel = "ApplicationError";
                    break;
                case (-2):
                    ls_LogLevel = "ApplicationOpen";
                    break;
                case (-1):
                    ls_LogLevel = "ApplicationShutD";
                    break;
                case 0:
                    ls_LogLevel = "No Log";
                    break;
                case 1:
                    ls_LogLevel = "Error";
                    break;
                case 2:
                    ls_LogLevel = "Warnning";
                    break;
                case 3:
                    ls_LogLevel = "Information";
                    break;
                case 4:
                    ls_LogLevel = "Debug Mode";
                    break;
                default:
                    ls_LogLevel = "UnKnown";
                    break;
            }
            if (al_service_code == SERVICE_CODE_MESSAGE_BOX)
            {
                lb_its_err_msg = (as_message.ToUpper().IndexOf("ERROR") > 0) || ((as_title.ToUpper().IndexOf("ERROR") > 0) || ((as_msg_techinfo.ToUpper().IndexOf("ERROR") > 0) || ((as_message.ToUpper().IndexOf("תקלה") > 0) || ((as_title.ToUpper().IndexOf("תקלה") > 0) || ((as_msg_techinfo.ToUpper().IndexOf("תקלה") > 0) || ((as_message.ToUpper().IndexOf("שגיאה") > 0) || ((as_title.ToUpper().IndexOf("שגיאה") > 0) || (as_msg_techinfo.ToUpper().IndexOf("שגיאה") > 0))))))));
                if (lb_its_err_msg)
                {
                    ls_LogLevel = "Error";
                }
                if (masofonAlias.masofon.Instance.gb_transaction_began)
                {
                    ls_transaction_status = "Open";
                }
                else
                {
                    ls_transaction_status = "Close";
                }
                
                ls_db_status = "User:" + masofonAlias.masofon.Instance.sqlca.UserID + "," + masofonAlias.masofon.Instance.sqlca.LogID + " Transaction:" + ls_transaction_status;
                ls_title = as_title;
                ls_msg_object = as_msg_object;
            }
            if (al_msg_code <= CollectionExtensions.UBound(MSG_NAME_C))
            {
                ls_msg_name = MSG_NAME_C[al_msg_code];
            }
            if (al_log_level < 0)
            {
                al_log_level = 256 + al_log_level;
            }
            ls_title = as_title;
            ls_version_number = masofonAlias.masofon.Instance.guo_version.uf_get_version_name();
            UpdateData(ls_version_number, ls_LogLevel, DateTime.Now, al_log_level, ls_db_status, ls_title, ls_msg_object, al_service_code, ls_service_name, al_msg_code,
            ls_msg_name, as_msg_techinfo, as_message);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                string ls_log_err = null;
                ls_log_err = "  uf_write_log_table():Err INSERT INTO 'error_debug_logs'  " + "  SqlErrText =  " + masofonAlias.masofon.Instance.sqlca.SqlErrText + "  SqlCode =  " + masofonAlias.masofon.Instance.sqlca.SqlCode + "\r" + "\n";
                return false;
            }
            is_user_response = "";
            return true;
        }

        public void UpdateData(string ls_version_number, string ls_LogLevel, DateTime? ldt_msg_date_time,
            Int64 al_log_level, string ls_db_status, string ls_title, string ls_msg_object, Int64 al_service_code,
            string ls_service_name, Int64 al_msg_code,
            string ls_msg_name, string as_msg_techinfo, string as_message)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object>
                    {
                        {"@0", ls_version_number},
                        {"@1", ls_LogLevel},
                        {"@2", masofonAlias.masofon.Instance.gs_vars.branch_number},
                        {"@3", masofonAlias.masofon.Instance.gs_station.station_number},
                        {"@4", masofonAlias.masofon.Instance.sqlca.LogID},
                        {"@5", masofonAlias.masofon.Instance.sqlca.ServerName},
                        {"@6", ldt_msg_date_time},
                        {"@7", al_log_level},
                        {"@8", ls_db_status},
                        {"@9", ls_title},
                        {"@10", ls_msg_object},
                        {"@11", is_user_response},
                        {"@12", al_service_code},
                        {"@13", ls_service_name},
                        {"@14", al_msg_code},
                        {"@15", ls_msg_name},
                        {"@16", as_msg_techinfo},
                        {"@17", as_message}
                    };
                    string sql =
                        "INSERT INTO error_debug_logs (application_number,  application_version,  run_datetime,  event,  branch_number,  station_number,  dbuser_name,  dbserver_name,  msg_date_time,  log_level,  db_status,  msg_title,  msg_object,  user_response,  service_code,  service_name,  msg_code,  msg_name,  msg_techinfo,  msg_text) SELECT application_number,  @0,  GetDate(),  @1,  @2,  @3,  @4,  @5,  @6,  @7,  @8,  @9,  @10,  @11,  @12,  @13,  @14,  @15,  @16,  @17 FROM applications WHERE application_name = 'Masofon' GROUP BY applications ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
