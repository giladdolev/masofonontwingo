
using System.Threading.Tasks;
using Mesofon.Common;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:21:00 (Utc)
	// Comments:        
	// 

	public class u_messagebox 
	{
		public async Task<long> uf_msg(string as_title, string as_error_string, string as_icon, string as_buttons, long al_default_button_focus, string as_msg_string) //--------------------------------------------------------------------
		{
			//Function:			public u_messagebox.uf_msg
			//
			// Returns:         Long ---> message box button clicked
			//
			// Parameters:      a) value String as_title 						---> window title
			//						b) value String as_error_string 				---> error text
			//						c) value String as_icon							---> message icon 
			//						d) value String as_buttons 					---> buttons to be displayed
			//						e) value String as_default_button_focus 	---> sets default button focus
			//						f) value String as_msg_string					---> message text
			// 
			// Copyright  - Stas
			//
			// Date Created: 03/08/2005
			//
			// Description:	
			// 						a)	istr_arg.a_string[1] ---> window title
			//						b) istr_arg.a_string[2] ---> error text
			//						c) istr_arg.a_string[3] ---> message icon 
			//						d) istr_arg.a_string[4] ---> buttons to be displayed
			//						e) istr_arg.a_string[5] ---> sets default button focus
			//						f) istr_arg.a_string[6] ---> message text
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			s_array_arguments lstr_arg = new s_array_arguments();
			long ll_return = 0;
			lstr_arg.a_string[6] = "";
			lstr_arg.a_string[1] = as_title;
			lstr_arg.a_string[2] = as_error_string;
			lstr_arg.a_string[3] = as_icon;
			lstr_arg.a_string[4] = as_buttons;
			lstr_arg.a_string[5] = al_default_button_focus.ToString();
			lstr_arg.a_string[6] = as_msg_string;

            //mini_terminal.w_mini_terminal_msg win = await WindowHelper.Open<mini_terminal.w_mini_terminal_msg>("mini_terminal_mini_terminal", "lstr_arg",lstr_arg);
            
            mini_terminal.w_mini_terminal_msg win = System.Web.VisualTree.MVC.VisualElementHelper.CreateFromView<mini_terminal.w_mini_terminal_msg>("w_mini_terminal_msg", "w_mini_terminal_msg", "mini_terminal_mini_terminal", null, null);
            win.istr_argProperty = lstr_arg;
            win.ID = win.ID + System.DateTime.Now.Ticks;
            win.CssClass = "";
            await win.ShowDialog();


            ll_return = (long)WindowHelper.GetParam<double>(win);
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log
		    await masofonAlias.masofon.Instance.guo_logs_in_table_service.uf_write_log_table(as_msg_string, u_logs_in_table_service.SERVICE_CODE_MESSAGE_BOX, 1, as_title, as_icon, as_buttons, 2);
			return ll_return;
		}
		public async Task<long> uf_msg(string as_title, string as_error_string, string as_icon, string as_buttons, string as_msg_string)
		{
			return await uf_msg(as_title, as_error_string, as_icon, as_buttons, 1, as_msg_string);
		}
		public async Task<long> uf_msg(string as_title, string as_error_string, string as_icon, string as_msg_string)
		{
			return await uf_msg(as_title, as_error_string, as_icon, "OK", 1, as_msg_string);
		}
		public async Task<long> uf_msg(string as_title, string as_error_string, string as_msg_string)
		{
			return await uf_msg(as_title, as_error_string, "", "OK", 1, as_msg_string);
		}
		public async Task<long> uf_msg(string as_title, string as_msg_string)
		{
			return await uf_msg(as_title, "", "", "OK", 1, as_msg_string);
		}
		public async Task<long> uf_msg(string as_msg_string)
		{
			return await uf_msg("", "", "", "OK", 1, as_msg_string);
		}
		public u_messagebox()
		{
		
		}
		
	}
}
