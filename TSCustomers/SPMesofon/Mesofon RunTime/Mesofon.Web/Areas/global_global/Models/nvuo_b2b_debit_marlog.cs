using System;
using System.Extensions;
using System.IO;
using System.Threading.Tasks;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using SystemIOAlias = System.IO;
using System.Text.RegularExpressions;

namespace global
{
    // Creation Time:   08/11/2015 10:20:54 (Utc)
    // Comments:        
    // 
    public class nvuo_b2b_debit_marlog : nvuo_b2b_transactions
    {
        public string is_message_type;
        public IRepository ids_header;
        public IRepository ids_details;
        public bool ib_crossdoc_return; //ron@12/04/2015>>SPUCM00005284 - in corssdoc in the xml should apear the real supplier of the item
        public string is_real_supplier_edi;
        public bool uf_set_message_type(string as_message_type) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_set_message_type
            //*Purpose:					Set message type: Debit, Return or Confirm.
            //*  
            //*Arguments:				String	 - as_message_type
            //*Return:						Boolean - TRUE - Succeed.
            //*										      FALSE - Faild.
            //*Date 			Programer		Version		Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2013		PennyS										Initiate Version
            //************************************************************************************************

            switch (as_message_type)
            {
                case "RETURN":
                    //Self-Build RETURN MARLOG
                    is_message_type = "389";
                    break;
                default:
                    //
                    break;
            }
            return true;
        }
        public async Task<FuncResults<int, IRepository[], bool>> uf_send_transaction(int ai_file_status, string as_supplier_edi, IRepository[] ads_data) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_send_transaction
            //*Purpose:					Create the B2B message XML file.
            //*								This function will be coded in the decendance objects.
            //*								In each object a defferent message will be created.
            //*  
            //*Arguments:				Integer - file_status
            //*Return:						Boolean - TRUE - Creation of B2B message XML file succeded.
            //											 FALSE - Creation of B2B message XML file failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2013		PennyS								Create the B2B message XML file.
            //************************************************************************************************

            int li_index = 0;
            int li_debit_row = 0;
            long ll_find = 0;
            long ll_row = 0;
            long ll_row_copy = 0;
            long ll_debit_items = 0;
            string ls_find = null;
            string ls_organization_edi = null;
            string ls_branch_edi = null;
            string ls_branch_name = null;
            string ls_supplier_edi = null;
            string ls_debit_no = null;
            string ls_message = null;
            // Check validity
            if (CollectionExtensions.UBound(ads_data) < 0)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }

            li_debit_row = (int)ads_data[0].RowCount();
            if (li_debit_row < 1)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_header = ads_data[0];

            ll_debit_items = ads_data[1].RowCount();
            if (ll_debit_items < 1)
            {
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_details = ads_data[1];
            is_supplier_edi = as_supplier_edi;
            is_parent_supplier_edi = is_supplier_edi;
            // Create and define DataStore

            ids_debit_xml.Reset();

            ids_debit_xml_temp.Reset();
            //ls_debit_no = String(ids_header.GetItem<int>(1, "invoice_number"))

            ll_row = ids_debit_xml_temp.Insert(0);
            // Set the envalope data in the XML DW
            if (this.uf_insert_envelope(ll_row) < 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כשלון בכתיבת מעטפת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי");
                ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file envelop.";
                SystemHelper.WriteToLog(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            // Set the header data in the XML DW
            if (this.uf_insert_header(ll_row) < 0)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כישלון בכתיבת כותרת מסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי");
                ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file header.";
                SystemHelper.WriteToLog(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            ids_debit_xml.Export(((dynamic)ids_debit_xml).XMLExport);
            // Load the xml DW
            for (li_index = 0; li_index < (int)ll_debit_items; li_index++)
            {



                ll_row_copy = (long)ids_debit_xml_temp.RowsCopy(0, 1, ModelBuffer.Primary, ids_debit_xml, li_index + 1, ModelBuffer.Primary);
                if (this.uf_insert_details(li_index) < 0)
                {
                    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כישלון בכתיבת פריט במסר תעודת חיוב - התעודה לא תשלח במסר דיגיטאלי");
                    ls_message = "Debit No: " + ls_debit_no + " - Failed in writing file details.";
                    SystemHelper.WriteToLog(ls_message);
                    ai_file_status = 0;
                    return FuncResults.Return(ai_file_status, ads_data, false);
                }
            }
            // Create xml file from datawindow
            if (!uf_save_as_xml())
            {
                switch (is_message_type)
                {
                    case "389":
                        // "RETURN"
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("כישלון", "", "StopSign!", "OK", 1, "שים לב - כישלון ביצירת מסר תעודת החזרה - התעודה לא תשלח במסר דיגיטאלי");
                        ls_message = "Return No: " + ls_debit_no + " - Failed to Create/Write to File.";
                        break;
                }
                SystemHelper.WriteToLog(ls_message);
                ai_file_status = 0;
                return FuncResults.Return(ai_file_status, ads_data, false);
            }
            else
            {
                ai_file_status = 1;
            }
            return FuncResults.Return(ai_file_status, ads_data, true);
        }
        public int uf_insert_envelope(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_insert_envelope
            //*Purpose:					Insert envelope section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*24-12-2007		SharonS				0.1.0 		B2B 			Initiate the object
            //************************************************************************************************

            int li_index = 0;
            int li_row = 0;
            long ll_find = 0;
            long ll_message_date = 0;
            long ll_supplier_num = 0;
            long ll_distributor_num = 0;
            string ls_find = null;
            string ls_organization_edi = null;
            string ls_supplier_edi = null;
            string ls_message = null;
            string ls_message_time = null;
            ls_organization_edi = masofonAlias.masofon.Instance.gs_vars.b2b_organization_edi;
            if (isempty_stringClass.isempty_string(ls_organization_edi))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "sender", ls_organization_edi);

            ids_debit_xml_temp.SetItem(al_row, "receiver", is_parent_supplier_edi);
            if (ib_crossdoc_return) //RonY@01/06/2015>>SPUCM00005375 - add RETURNCDS2C
            {

                ids_debit_xml_temp.SetItem(al_row, "doctype", "RETURNCDS2C");
            }
            else
            {

                ids_debit_xml_temp.SetItem(al_row, "doctype", "RETURNS2C");
            }

            ids_debit_xml_temp.SetItem(al_row, "aprf", "INVDBT");

            ids_debit_xml_temp.SetItem(al_row, "ackn", "1"); // 1 - Required        

            ids_debit_xml_temp.SetItem(al_row, "testind", "1"); // 1 - Test   
            ll_message_date = Convert.ToInt64(DateTime.Today.ToString("yyyyMMdd"));

            ids_debit_xml_temp.SetItem(al_row, "messdate", ll_message_date);
            ls_message_time = masofonAlias.masofon.Instance.set_machine_time().Value.ToString("HHmmss");

            ids_debit_xml_temp.SetItem(al_row, "messtime", ls_message_time);
            return 1;
        }        

        public int uf_insert_header(long al_row) //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_insert_header
            //*Purpose:					Insert haeder section into the XML DW.
            //*  
            //*Arguments:				Long - Row number in the XML DW.
            //*Return:						Integer: 1 - succeeded.
            //*										  -1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2013		PennyS							Initiate the object
            //************************************************************************************************

            string ls_reference_no = null;
            string ls_employee_name = null;
            string ls_supplier_invoice_no = null;
            int li_insert_option = 0;
            long ll_reference_date = 0;
            long ll_employee_number = 0;
            long ll_debit_invoice_no = 0;
            long ll_shipment_number = 0;
            decimal ld_date_time = default(decimal);
            decimal ld_vat_percent = default(decimal);
            decimal ld_invoice_date = default(decimal);
            decimal ld_lines_sum = default(decimal);
            decimal ld_inv_sum = default(decimal);
            decimal ld_discount_percent = default(decimal);
            decimal ld_discount_amount = default(decimal);
            DateTime? ldt_date_time = default(DateTime);
            DateTime? ldt_invoice_date = default(DateTime);

            ids_debit_xml_temp.SetItem(al_row, "invoicetype", is_message_type);

            ll_shipment_number = (long)ids_header.GetItemValue<double>(0, "shipment_number");

            ids_debit_xml_temp.SetItem(al_row, "debitinvoiceno", ll_shipment_number.ToString());

            ids_debit_xml_temp.SetItem(al_row, "invoicefunc", "9");
            if (is_message_type == "389")
            {

                ldt_date_time = ids_header.GetItemValue<DateTime>(0, "move_date");
            }
            else
            {

                ldt_date_time = ids_header.GetItemValue<DateTime>(0, "date_move");
            }
            ld_date_time = Convert.ToDecimal(ldt_date_time.Value.ToString("yyyyMMddhhmm"));
            if (ld_date_time == 0)
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "datetime", ld_date_time);
            //RonY@15/01/2015  SPUCM00005162 - start
            if (ib_crossdoc_return)
            {

                ids_debit_xml_temp.SetItem(al_row, "supplierno", is_real_supplier_edi);
            }
            //RonY@15/01/2015  SPUCM00005162 - end
            if (isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.qs_parameters.edi_number))
            {
                return -1;
            }

            ids_debit_xml_temp.SetItem(al_row, "storeno", masofonAlias.masofon.Instance.qs_parameters.edi_number);

            ids_debit_xml_temp.SetItem(al_row, "storename", masofonAlias.masofon.Instance.qs_parameters.company_name);
            return 1;
        }
        public bool uf_save_as_xml() //**********************************************************************************************
        {
            //*Object:						nvuo_b2b_debit
            //*Function Name:			uf_save_as_xml
            //*Purpose:					Save the DW as XML file.
            //*  
            //*Arguments:				None.
            //*Return:						Boolean: 1 - succeeded.
            //*										 	-1 - failed.
            //*Date 			Programer			Version	Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2013		Pennys											Initiate the object
            //************************************************************************************************


            return ids_debit_xml.SaveToXml(Convert.ToString(is_file_spec)) == 1;
        }
        public int uf_insert_details(long al_row) //**********************************************************************************************
        {
            //*Object:						nvou_b2b_debit
            //*Function Name:			uf_insert_details
            //*Purpose:					Insert the Deatails section of the debit XML
            //*  
            //*Arguments:				Long	- Row No (al_row)
            //*Return:						Integer - 1 succeed.
            //*											-1 not  succeed.
            //*Date 			Programer		Version	Task#		Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*22-07-2013		PennyS									Initiate Version
            //************************************************************************************************

            string ls_yarpa_code = null;
            string ls_barcode = null;
            string ls_name = null;
            string ls_temp = null;
            string ls_reason_code = null;
            string ls_material_barcode = null;
            string ls_carton_barcode = null;
            char lc_doc_type = '\0';
            decimal ld_sum = default(decimal);
            decimal ld_price_after_discount = default(decimal);
            decimal ld_vat_percentage = default(decimal);
            decimal ld_quantity_diff = default(decimal);
            decimal ld_tax_price = default(decimal);
            decimal ld_temp = default(decimal);
            decimal ld_price_diff = default(decimal);
            long ll_branch_number = 0;
            long ll_supplier_number = 0;
            long ll_order_number = 0;
            long ll_doc_number = 0;
            long ll_item_number = 0;
            long ll_line_number = 0;
            long ll_dprt_no = 0;
            long ll_decline_number = 0;
            long? ll_find = 0;
            IRepository ldwc_dec_num= null;
            // LineNo
            ll_line_number = al_row + 1;

            ids_debit_xml.SetItem(al_row, "lineno", ll_line_number);
            // ItemBarcode

            ls_barcode = ids_details.GetItemValue<string>(al_row, "barcode");

            ll_dprt_no = ids_details.GetItemValue<long>((int)al_row, "dprt_no");
            if (ll_dprt_no == 8 || ll_dprt_no == 15)
            {

                ll_item_number = ids_details.GetItemValue<long>((int)al_row, "material_number");
                if (this.uf_get_yarpa_code(ll_item_number, ref ls_yarpa_code))
                {
                    if (!isempty_stringClass.isempty_string(ls_yarpa_code))
                    {
                        ls_barcode = ls_yarpa_code;
                    }
                }
            }

            ids_debit_xml.SetItem(al_row, "itembarcode", ls_barcode);
            // CartNo

            ls_carton_barcode = ids_details.GetItemValue<string>(al_row, "carton_barcode");
            ls_carton_barcode = Regex.Replace(ls_carton_barcode, @"\s", "");
            ids_debit_xml.SetItem(al_row, "cartno", ls_carton_barcode);
            // UnitsQty

            ld_quantity_diff = ids_details.GetItemValue<decimal>((int)al_row, "material_quantity");

            ids_debit_xml.SetItem(al_row, "unitsqty", ld_quantity_diff);
            // Reason Code
            ls_reason_code = "";

            ll_decline_number = ids_details.GetItemValue<long>((int)al_row, "decline_number");

            ldwc_dec_num = new dddw_get_return_reasonsRepository(); //ids_details.GetVisualElementById<GridElement>("decline_number");
            ldwc_dec_num.Retrieve(3);
            ll_find = ldwc_dec_num.Find("decline_number ==" + ll_decline_number.ToString(), 0, ldwc_dec_num.RowCount());
            if (ll_find >= 0)
            {
                ls_reason_code = ldwc_dec_num.GetItemValue<string>((int)ll_find, "decline_code");
            }

            ids_debit_xml.SetItem(al_row, "reasoncode", ls_reason_code);
            return 1;
        }
        public int uf_set_as_crossdoc_return(string as_real_supplier_edi) //********************************************************************
        {
            //*Object:	nvuo_b2b_debit_marlog
            //*Function Name:	(uf_set_as_crossdoc_return)
            //*Purpose: 	 change the object to fit crossdoc_retrun
            //*Arguments: 	(none)
            //*Return:	integer
            //*Programmer	Date   Version	Task#	 Description
            //*---------------------------------------------------------------------
            //*RonY@15/01/2015 -  SPUCM00005162
            //********************************************************************

            ib_crossdoc_return = true;
            is_real_supplier_edi = as_real_supplier_edi; //ron@12/04/2015>>SPUCM00005284 - in corssdoc in the xml should apear the real supplier of the item

            ids_debit_xml = new d_crossdoc_returns_for_b2b_xmlRepository();

            ids_debit_xml_temp = new d_crossdoc_returns_for_b2b_xmlRepository();
            return 1;
        }
        public nvuo_b2b_debit_marlog()
            : base()
        {
            ids_debit_xml = new d_debit_marlog_for_b2b_xmlRepository();
            ids_debit_xml_temp = new d_debit_marlog_for_b2b_xmlRepository();
        }
    }
}
