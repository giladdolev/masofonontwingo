<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_b2b_branches_suppliersRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="105px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Suppliers Edi Number&quot;" TemplateText="&quot;Suppliers Edi Number&quot;" Visible="True" Left="109px" Top="1px" ID="suppliers_edi_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="141px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="105px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="suppliers_edi_number" Visible="True" Left="109px" Top="2px" ID="suppliers_edi_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="141px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

