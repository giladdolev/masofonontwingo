<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.ds_marlog_distributor_translateRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מרלו&quot;ג&quot;" TemplateText="&quot;מרלו&quot;ג&quot;" Visible="True" Left="105px" Top="1px" Alignment="Center" ID="branch_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="108px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר מפיץ&quot;" TemplateText="&quot;מספר מפיץ&quot;" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="distributor_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="101px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewComboField runat="server" Visible="True" Left="105px" Top="1px" Alignment="Center" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="108px"></vt:DataViewComboField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="2px" Top="1px" Alignment="Center" ID="distributor_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="101px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

