<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dw_parent_supplier_ediRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="26px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="20px" Top="1px" Alignment="Right" ID="supplier_number" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="79px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_parent" Visible="True" Left="108px" Top="2px" ID="supplier_parent" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="135px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="parent_supp_edi_number" Visible="True" Left="247px" Top="3px" ID="parent_supp_edi_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="105px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

<vt:ComponentManager runat="server" Visible="False">
	<vt:DataViewLabelField runat="server" Text="&quot;Number&quot;" TemplateText="&quot;Number&quot;" Visible="True" Left="171px" Top="8px" Alignment="Center" ID="number_t" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="61px">
	</vt:DataViewLabelField>
	<vt:DataViewLabelField runat="server" Text="&quot;Name&quot;" TemplateText="&quot;Name&quot;" Visible="True" Left="67px" Top="8px" Alignment="Center" ID="name_t" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="44px">
	</vt:DataViewLabelField>
</vt:ComponentManager>

