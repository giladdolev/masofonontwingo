<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_not_arrived_invoces_printRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="146px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;דוח החשבוניות שלא הגיעו כלל&quot;" TemplateText="&quot;דוח החשבוניות שלא הגיעו כלל&quot;" Visible="True" Left="455px" Top="11px" Alignment="Right" ID="t_title" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="209px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:סניף&quot;" TemplateText="&quot;:סניף&quot;" Visible="True" Left="621px" Top="39px" Alignment="Right" ID="branch_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="42px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:משלוח&quot;" TemplateText="&quot;:משלוח&quot;" Visible="True" Left="611px" Top="67px" Alignment="Right" ID="t_1" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="52px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="537px" Top="67px" ID="t_shipment_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="19px" Width="70px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר פריטים&quot;" TemplateText="&quot;מספר פריטים&quot;" Visible="True" Left="3px" Top="112px" Alignment="Center" ID="items_quantity_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר הזמנה&quot;" TemplateText="&quot;מספר הזמנה&quot;" Visible="True" Left="118px" Top="112px" Alignment="Center" ID="date_move_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר הזמנה&quot;" TemplateText="&quot;מספר הזמנה&quot;" Visible="True" Left="233px" Top="112px" Alignment="Center" ID="order_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר מפיץ&quot;" TemplateText="&quot;מספר מפיץ&quot;" Visible="True" Left="348px" Top="112px" Alignment="Center" ID="distributor_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר חשבונית&quot;" TemplateText="&quot;מספר חשבונית&quot;" Visible="True" Left="463px" Top="112px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="578px" Top="112px" Alignment="Center" ID="supplier_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="32px" Width="43px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="625px" Top="112px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="32px" Width="38px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="559px" Top="39px" Alignment="Center" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="20px" Width="48px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Visible="True" Left="277px" Top="39px" Alignment="Right" ID="branch_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="19px" Width="271px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="items_quantity" Visible="True" Left="3px" Top="1px" Alignment="Center" ID="items_quantity" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="dd/mm/yyyy" DataMember="date_move" Visible="True" Left="118px" Top="1px" Alignment="Center" ID="date_move" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="233px" Top="1px" Alignment="Center" ID="order_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="348px" Top="1px" Alignment="Center" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="463px" Top="1px" Alignment="Center" ID="invoice_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="112px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="578px" Top="1px" Alignment="Center" ID="supplier_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="44px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="625px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="20px" Width="39px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>
</vt:ControlView>
