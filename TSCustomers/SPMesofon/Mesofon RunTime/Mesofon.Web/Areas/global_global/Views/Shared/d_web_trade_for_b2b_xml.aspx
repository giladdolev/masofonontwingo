<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_web_trade_for_b2b_xmlRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="27px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewTextField runat="server" DataMember="testind" Visible="True" Left="451px" Top="8px" ID="testind" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="ackn" Visible="True" Left="373px" Top="8px" ID="ackn" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="snrf" Visible="True" Left="295px" Top="8px" ID="snrf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="aprf" Visible="True" Left="217px" Top="8px" ID="aprf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doctype" Visible="True" Left="138px" Top="8px" ID="doctype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="sender" Visible="True" Left="3px" Top="8px" ID="sender" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="54px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="receiver" Visible="True" Left="60px" Top="8px" ID="receiver" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[General]" DataMember="messdate" Visible="True" Left="530px" Top="8px" ID="messdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[General]" DataMember="messtime" Visible="True" Left="632px" Top="8px" ID="messtime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="statusname" Visible="True" Left="734px" Top="8px" ID="statusname" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="statuscode" Visible="True" Left="836px" Top="8px" ID="statuscode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="statusdatetime" Visible="True" Left="938px" Top="8px" ID="statusdatetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="orderorgno" Visible="True" Left="1040px" Top="8px" ID="orderorgno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="orderdate" Visible="True" Left="1142px" Top="8px" ID="orderdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"></vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

