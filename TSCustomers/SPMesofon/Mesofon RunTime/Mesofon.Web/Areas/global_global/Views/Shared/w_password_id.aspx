<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<global.w_password_id>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Text="סיסמת כניסה" Top="109px" Left="243px" Resizable="True" ID="w_password_id" LoadAction="w_password_id\Form_Load" BackColor="ButtonFace" Height="142px" Width="383px">
		<vt:Label runat="server" TextAlign="TopLeft" RightToLeft="Yes" RightToLeftDecoration="True" Text="ת.ז." Top="53px" Left="293px" ID="st_2" BackColor="ButtonFace" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="17px" Width="85px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" RightToLeft="Yes" RightToLeftDecoration="True" Text="סיסמה" Top="22px" Left="293px" ID="st_1" BackColor="ButtonFace" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="17px" Width="85px">
		</vt:Label>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="51px" Left="17px" ID="sle_id" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="23px" TabIndex="30" Width="274px">
		</vt:TextBox>
		<vt:Button runat="server" Text="ביטול" Top="84px" Left="95px" ID="cb_1" Height="26px" TabIndex="40" Width="110px">
		</vt:Button>
		<vt:PartialControl runat="server" Controller="u_msr" Action="u_msr" Area="global_global" Top="44px" Left="238px" Visible="False" ID="uo_msr_password" BackColor="#FFFFFF" Height="22px" TabIndex="20" Width="21px">
		</vt:PartialControl>
		<vt:TextBox runat="server" UseSystemPasswordChar="True" PasswordChar="" MaxLength="32767" Top="19px" Left="17px" ID="sle_password" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="23px" TabIndex="10" Width="274px">
		</vt:TextBox>
	</vt:WindowView>

</asp:Content>
