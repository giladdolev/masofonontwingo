<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_packageRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="127px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;שם פריט&quot;" TemplateText="&quot;שם פריט&quot;" Visible="True" Left="166px" Top="7px" ID="materials_name_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד פריט&quot;" TemplateText="&quot;ברקוד פריט&quot;" Visible="True" Left="150px" Top="24px" ID="materials_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות קרטונים&quot;" TemplateText="&quot;כמות קרטונים&quot;" Visible="True" Left="140px" Top="89px" ID="actual_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="73px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="actual_quantity" Visible="True" Left="89px" Top="89px" ID="actual_quantity" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="46px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;סה&quot;כ פריטים&quot;" TemplateText="&quot;סה&quot;כ פריטים&quot;" Visible="True" Left="69px" Top="107px" ID="cf_total_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="69px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד קרטון&quot;" TemplateText="&quot;ברקוד קרטון&quot;" Visible="True" Left="147px" Top="50px" ID="packages_pack_barcode_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="66px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות בקרטון&quot;" TemplateText="&quot;כמות בקרטון&quot;" Visible="True" Left="145px" Top="67px" ID="packages_quantity_t" BorderStyle="Inset" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="68px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="materials_name" Visible="True" Left="10px" Top="7px" ID="materials_name" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="126px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="10px" Top="25px" ID="materials_barcode" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="126px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packages_pack_barcode" Visible="True" Left="10px" Top="48px" ID="packages_pack_barcode" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="126px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="packages_quantity" Visible="True" Left="90px" Top="67px" ID="packages_quantity" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="46px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="15px" Top="108px" ID="cf_total_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="13px" Width="46px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

