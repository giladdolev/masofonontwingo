<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.ds_suppliers_distributorsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מפיץ&quot;" TemplateText="&quot;מפיץ&quot;" Visible="True" Left="63px" Top="2px" Alignment="Center" ID="distributor_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="39px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ספק&quot;" TemplateText="&quot;ספק&quot;" Visible="True" Left="5px" Top="2px" Alignment="Center" ID="supplier_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="40px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר EDI&quot;" TemplateText="&quot;מספר EDI&quot;" Visible="True" Left="112px" Top="2px" Alignment="Center" ID="edi_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="54px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="is_active" Left="486px" Top="35px" ID="is_active" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="53px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="18px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Visible="True" Left="64px" Top="1px" Alignment="Center" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="15px" Width="39px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="8px" Top="0px" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="49px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="edi_number" Visible="True" Left="110px" Top="1px" ID="edi_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="89px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Left="224px" Top="0px" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="254px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

