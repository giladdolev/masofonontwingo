<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<global.w_pc_message_detail>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Text="Untitled" Resizable="True" ID="w_pc_message_detail" LoadAction="w_pc_message_detail\Form_Load" BackColor="ButtonFace" Height="245px" Width="433px">
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="36px" Left="8px" ID="mle_1" ForeColor="WindowText" Height="80px" TabIndex="10" Width="411px">
		</vt:TextBox>
		<vt:Label runat="server" TextAlign="TopLeft" Text="Add Translation to pc_message_detail table" Top="6px" Left="38px" ID="st_2" BackColor="ButtonFace" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="23px" Width="371px">
		</vt:Label>
		<vt:Button runat="server" Text="Search" Top="185px" Left="122px" ID="cb_3" ClickAction="w_pc_message_detail\cb_3_clicked" Enabled="False" Height="28px" TabIndex="30" Width="147px">
		</vt:Button>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="120px" Left="8px" ID="sle_1" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="28px" TabIndex="10" Width="411px">
		</vt:TextBox>
		<vt:Button runat="server" Text="Close" Top="153px" Left="203px" ID="cb_2" ClickAction="w_pc_message_detail\cb_2_clicked" Height="28px" TabIndex="20" Width="141px">
		</vt:Button>
		<vt:Button runat="server" Text="Translate" Top="152px" Left="53px" ID="cb_1" ClickAction="w_pc_message_detail\cb_1_clicked" Height="28px" TabIndex="10" Width="147px">
		</vt:Button>
	</vt:WindowView>

</asp:Content>
