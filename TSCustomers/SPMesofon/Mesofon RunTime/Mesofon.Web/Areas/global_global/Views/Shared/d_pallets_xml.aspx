<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_pallets_xmlRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="27px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewTextField runat="server" DataMember="responsedatetime" Visible="True" Left="759px" Top="7px" ID="responsedatetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="refdate" Visible="True" Left="1699px" Top="8px" ID="refdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="75px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="documentno" Visible="True" Left="853px" Top="11px" ID="documentno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="extra" Visible="True" Left="638px" Top="5px" ID="extra" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="retailno" Visible="True" Left="924px" Top="13px" ID="retailno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="89px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="retailname" Visible="True" Left="1021px" Top="10px" ID="retailname" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="177px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="messtime" Visible="True" Left="910px" Top="8px" ID="messtime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="35px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="cdno" Visible="True" Left="1121px" Top="9px" ID="cdno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="89px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="cdname" Visible="True" Left="981px" Top="4px" ID="cdname" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="177px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="rotation" Visible="True" Left="1228px" Top="11px" ID="rotation" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="18px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="docconfrtype" Visible="True" Left="1269px" Top="10px" ID="docconfrtype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="18px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="documenttype" Visible="True" Left="1324px" Top="7px" ID="documenttype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="101px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="deliverydate" Visible="True" Left="699px" Top="10px" ID="deliverydate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="118px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="messdate" Visible="True" Left="565px" Top="5px" ID="messdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="59px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="testind" Visible="True" Left="46px" Top="6px" ID="testind" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="35px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="ackn" Visible="True" Left="105px" Top="11px" ID="ackn" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="43px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="snrf" Visible="True" Left="160px" Top="8px" ID="snrf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="33px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="aprf" Visible="True" Left="202px" Top="8px" ID="aprf" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="29px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="doctype" Visible="True" Left="257px" Top="9px" ID="doctype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="83px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="receiver" Visible="True" Left="349px" Top="7px" ID="receiver" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="52px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="sender" Visible="True" Left="413px" Top="5px" ID="sender" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="57px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="1px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="subsupplier" Visible="True" Left="92px" Top="4px" ID="subsupplier" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="89px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="extradetail" Visible="True" Left="350px" Top="6px" ID="extradetail" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="disagrtype" Visible="True" Left="426px" Top="4px" ID="disagrtype" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="18px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="palletbarcode" Visible="True" Left="476px" Top="5px" ID="palletbarcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="89px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

