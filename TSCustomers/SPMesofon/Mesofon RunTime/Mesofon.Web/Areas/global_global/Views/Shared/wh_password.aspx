<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<global.wh_password>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Text="סיסמת כניסה" Top="109px" Left="243px" Resizable="True" ID="wh_password" BackColor="#0000FF" Height="111px" Width="310px">
		<vt:Button runat="server" Text="ביטול" Top="56px" Left="96px" ID="cb_1" ClickAction="wh_password\cb_1_clicked" Height="26px" TabIndex="21" Width="110px">
		</vt:Button>
		<vt:PartialControl runat="server" Controller="u_msr" Action="u_msr" Area="global_global" Top="44px" Left="238px" Visible="False" ID="uo_msr_password" BackColor="#FFFFFF" Height="22px" TabIndex="11" Width="21px" ue_activatedEventAction="wh_password\uo_msr_password_ue_activated">
		</vt:PartialControl>
		<vt:TextBox runat="server" UseSystemPasswordChar="True" PasswordChar="" MaxLength="32767" Top="19px" Left="17px" ID="sle_password" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="23px" TabIndex="10" Width="274px">
		</vt:TextBox>
	</vt:WindowView>

</asp:Content>
