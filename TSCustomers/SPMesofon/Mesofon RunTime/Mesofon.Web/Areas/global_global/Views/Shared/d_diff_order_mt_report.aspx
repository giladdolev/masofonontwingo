<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_diff_order_mt_reportRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד ושם פריט&quot;" TemplateText="&quot;ברקוד ושם פריט&quot;" Visible="True" Left="48px" Top="2px" Alignment="Center" ID="materials_barcode_name_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="163px" Top="2px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="17px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות שהוזמנה&quot;" TemplateText="&quot;כמות שהוזמנה&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="material_quantity_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="29px" Width="43px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="34px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="163px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="31px" Width="17px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="name" Visible="True" Left="48px" Top="17px" Alignment="Right" ID="name" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="15px" Width="114px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="barcode" Visible="True" Left="48px" Top="1px" Alignment="Right" ID="barcode" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="15px" Width="114px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_quantity" Visible="True" Left="2px" Top="18px" Alignment="Right" ID="order_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="45px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Left="2px" Top="1px" Alignment="Right" ID="order_number" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="15px" Width="43px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

