<%@ Page Language="C#"   Inherits="System.Web.Mvc.ViewPage<global.rw_packages>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
	<vt:WindowView runat="server" Opacity="1" Resizable="True" LoadAction="rw_packages\Form_Load" ID="rw_packages" BackColor="ButtonFace" Height="163px" Width="229px">
		<vt:Button runat="server" Text="ביטול" Top="142px" Left="34px" ID="cb_cancel" ClickAction="rw_packages\cb_cancel_clicked" Height="15px" TabIndex="30" Width="42px">
		</vt:Button>
		<vt:Button runat="server" Text="אישור" Top="142px" Left="132px" ID="cb_ok" ClickAction="rw_packages\cb_ok_clicked" Height="15px"  TabIndex="20" Width="42px">
		</vt:Button>
		<vt:DataView runat="server" Top="2px" Left="2px" Resizable="True" ID="dw_package" Height="136px" TabIndex="10" Width="223px" DataSource="<% Model%>">
		</vt:DataView>
	</vt:WindowView>

