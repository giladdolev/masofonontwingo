<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<global.w_sort>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Text="אפיון שדות מיון" Top="80px" Left="88px" Resizable="True" ID="w_sort" LoadAction="w_sort\Form_Load" BackColor="#D4D0C8" Height="318px" Width="463px">
		<vt:Button runat="server" Text="בחר הכל" Top="249px" Left="102px" ID="pb_select" ClickAction="w_sort\pb_select_clicked" Height="36px" TabIndex="20" Width="89px">
		</vt:Button>
		<vt:Button runat="server" Text="נקה הכל" Top="249px" Left="6px" ID="pb_clear" ClickAction="w_sort\pb_clear_clicked" Height="36px" TabIndex="20" Width="89px">
		</vt:Button>
		<vt:Button runat="server" Text="יציאה" Top="249px" Left="258px" ID="pb_cancel" ClickAction="w_sort\pb_cancel_clicked"  Height="36px" TabIndex="10" Width="89px">
		</vt:Button>
		<vt:Button runat="server" Text="אישור" Top="249px" Left="357px" ID="pb_ok" ClickAction="w_sort\pb_ok_clicked" Height="36px" TabIndex="20" Width="89px">
		</vt:Button>
		<vt:DataView runat="server" Top="7px" Left="250px" ID="dw_source" Height="234px" TabIndex="40" Width="196px" DataSource="<% Model%>">
		</vt:DataView>
		<vt:DataView runat="server" Top="7px" Left="6px" ID="dw_sort" Height="234px" TabIndex="30" Width="238px">
		</vt:DataView>
	</vt:WindowView>

</asp:Content>
