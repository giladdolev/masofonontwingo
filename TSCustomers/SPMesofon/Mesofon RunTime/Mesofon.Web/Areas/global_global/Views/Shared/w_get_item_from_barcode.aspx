<%@ Page Language="C#"   Inherits="System.Web.Mvc.ViewPage<global.w_get_item_from_barcode>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
	<vt:WindowView runat="server" Opacity="1" Text="רשימת ברקודים משניים" Top="58px" Left="181px" Resizable="True" ID="w_get_item_from_barcode" LoadAction="w_get_item_from_barcode\Form_Load" BackColor="#D4D0C8" Height="333px" Width="704px">
		<vt:Label runat="server" TextAlign="TopLeft" Text="00/00/0000 00:00:00" Top="288px" Left="145px" ID="st_datetime_update" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="18px" Width="127px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" Text="Server Name" Top="270px" Left="147px" ID="st_esa_live" BackColor="ButtonFace" Font-Names="Arial" Font-Size="9pt" ForeColor="#000080" Height="16px" Width="128px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" Text="00/00/0000 00:00:00" Top="288px" Left="11px" ID="st_run_datetime" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="16px" Width="127px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" Text="0" Top="261px" Left="617px" ID="st_count" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="17px" Width="75px">
		</vt:Label>
		<vt:Label runat="server" Text="פריטים" Top="261px" Left="572px" ID="st_1" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="17px" Width="40px">
		</vt:Label>
		<vt:CheckBox runat="server" Text="הצג פריטי ספק נוכחי בלבד" Top="279px" Left="539px" Visible="False" ID="cbx_supplier" ClickAction="w_get_item_from_barcode\cbx_supplier_clicked" BackColor="ButtonFace" Font-Names="Arial" Font-Size="10pt" ForeColor="#000080" Height="20px" Width="155px">
		</vt:CheckBox>
		<vt:DataView runat="server" Top="9px" Left="10px" ID="dw_codes" DoubleClickAction="w_get_item_from_barcode\dw_codes_doubleclicked" ClickAction="w_get_item_from_barcode\dw_codes_clicked"  Height="251px" TabIndex="10" Width="682px" DataSource="<% Model%>">
		</vt:DataView>
		<vt:Button runat="server" Text="ביטול" Top="274px" Left="302px" ID="cb_cancel" ClickAction="w_get_item_from_barcode\cb_cancel_clicked" Height="26px" TabIndex="30" Width="76px">
		</vt:Button>
		<vt:Button runat="server" Text="אישור" Top="275px" Left="405px" ID="cb_ok" ClickAction="w_get_item_from_barcode\cb_ok_clicked" Height="26px" TabIndex="20" Width="76px">
		</vt:Button>
		<vt:GroupBox runat="server" Text="תאריך הרצה" Top="256px" Left="8px" ID="gb_1" BackColor="ButtonFace" ForeColor="#000080" Height="55px" TabIndex="110" Width="133px">
		</vt:GroupBox>
		<vt:GroupBox runat="server" Text="שרת ריצה" Top="256px" Left="141px" ID="gb_3" BackColor="ButtonFace" ForeColor="#000080" Height="55px" TabIndex="120" Width="137px">
		</vt:GroupBox>
	</vt:WindowView>

