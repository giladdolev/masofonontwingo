<%@ Page Language="C#"   Inherits="System.Web.Mvc.ViewPage<global.w_filter>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
	<vt:WindowView runat="server" Opacity="1" Text="אפיון שדות מיון" Top="80px" Left="88px" Resizable="True" ID="w_filter" LoadAction="w_filter\Form_Load" BackColor="#D4D0C8" Height="318px" Width="544px">
		<vt:CheckBox runat="server" Text="הצג אוטומתית" Top="277px" Left="235px" ID="cbx" BackColor="ButtonFace" Font-Names="Arial" Font-Size="12pt" ForeColor="WindowText" Height="23px" Width="119px">
		</vt:CheckBox>
		<vt:Button runat="server" Text="נקה הכל" Top="249px" Left="445px" ID="pb_clear" ClickAction="w_filter\pb_clear_clicked" Height="36px" TabIndex="20" Width="89px">
		</vt:Button>
		<vt:Button runat="server" Text="יציאה" Top="249px" Left="6px" ID="pb_cancel" ClickAction="w_filter\pb_cancel_clicked" Height="36px" TabIndex="10" Width="89px">
		</vt:Button>
		<vt:Button runat="server" Text="אישור" Top="249px" Left="106px" ID="pb_ok" ClickAction="w_filter\pb_ok_clicked" Height="36px" TabIndex="20" Width="89px">
		</vt:Button>
		<vt:DataView runat="server" Top="7px" Left="359px" ID="dw_source" Height="234px" TabIndex="40" Width="174px" DataSource="<% Model%>">
		</vt:DataView>
		<vt:DataView runat="server" Top="7px" Left="6px" ID="dw_sort" LostFocusAction="w_filter\dw_sort_losefocus" ClickAction="w_filter\dw_sort_clicked" Height="234px" TabIndex="30" Width="348px" DataSource="<% Model%>">
		</vt:DataView>
	</vt:WindowView>

