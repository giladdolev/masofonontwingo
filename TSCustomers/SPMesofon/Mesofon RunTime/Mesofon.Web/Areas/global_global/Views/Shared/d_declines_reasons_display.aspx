<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_declines_reasons_displayRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="1px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="8px" Top="16px" ID="max_line_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="56px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="display_name" Visible="True" Left="26px" Top="1px" Alignment="Right" ID="display_name" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="163px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#,##0" DataMember="reject_quantity" Left="25px" Top="2px" Alignment="Center" ID="reject_quantity" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="18px" Width="30px"></vt:DataViewTextField>
<vt:DataViewCheckField runat="server" Visible="True" Left="4px" Top="1px" ID="checked" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="19px" Width="18px"></vt:DataViewCheckField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

