<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dddw_param_valuesRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="35px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="param_name" Left="2px" Top="2px" ID="param_name" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="32px" Width="261px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="param_value" Visible="True" Left="265px" Top="2px" ID="param_value" BorderStyle="None" Font-Names="Arial" Font-Size="20pt" ForeColor="WindowText" Height="32px" Width="187px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

