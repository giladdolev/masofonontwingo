<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dw_item_codeRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;שם הפריט&quot;" TemplateText="&quot;שם הפריט&quot;" Visible="True" Left="3px" Top="2px" Alignment="Center" ID="items_name_t" Font-Names="Arial" Font-Size="11pt" ForeColor="#FFFFFF" Height="17px" Width="219px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס פריט&quot;" TemplateText="&quot;מס פריט&quot;" Visible="True" Left="569px" Top="2px" Alignment="Center" ID="item_number_t" Font-Names="Arial" Font-Size="11pt" ForeColor="#FFFFFF" Height="17px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="447px" Top="2px" Alignment="Center" ID="items_barcode_t" Font-Names="Arial" Font-Size="11pt" ForeColor="#FFFFFF" Height="17px" Width="119px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;קוד משני&quot;" TemplateText="&quot;קוד משני&quot;" Visible="True" Left="324px" Top="2px" Alignment="Center" ID="code_t" Font-Names="Arial" Font-Size="11pt" ForeColor="#FFFFFF" Height="17px" Width="119px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;סוג קוד&quot;" TemplateText="&quot;סוג קוד&quot;" Visible="True" Left="226px" Top="2px" Alignment="Center" ID="item_code_name_t" Font-Names="Arial" Font-Size="11pt" ForeColor="#FFFFFF" Height="17px" Width="94px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="items_name" Visible="True" Left="3px" Top="1px" Alignment="Right" ID="items_name" Font-Names="Arial" Font-Size="11pt" ForeColor="#000080" Height="17px" Width="219px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_number" Visible="True" Left="569px" Top="1px" Alignment="Right" ID="item_number" Font-Names="Arial" Font-Size="11pt" ForeColor="#000080" Height="17px" Width="88px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="items_barcode" Visible="True" Left="447px" Top="1px" Alignment="Right" ID="items_barcode" Font-Names="Arial" Font-Size="11pt" ForeColor="#000080" Height="17px" Width="119px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="code" Visible="True" Left="324px" Top="1px" Alignment="Right" ID="code" Font-Names="Arial" Font-Size="11pt" ForeColor="#000080" Height="17px" Width="119px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_code_name" Visible="True" Left="226px" Top="1px" Alignment="Right" ID="item_code_name" Font-Names="Arial" Font-Size="11pt" ForeColor="#000080" Height="17px" Width="94px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

