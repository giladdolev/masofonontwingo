<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_diff_inv_mt_report_printRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  DataSource="<%#Model.PrimaryList%>"Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="165px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;:סניף&quot;" TemplateText="&quot;:סניף&quot;" Visible="True" Left="611px" Top="39px" Alignment="Right" ID="branch_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="42px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;:ספק&quot;" TemplateText="&quot;:ספק&quot;" Visible="True" Left="617px" Top="61px" Alignment="Right" ID="supplier_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="36px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="559px" Top="39px" Alignment="Center" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="48px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="559px" Top="61px" Alignment="Center" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="48px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_name" Visible="True" Left="267px" Top="39px" Alignment="Right" ID="branch_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="280px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="suppliers_name" Visible="True" Left="267px" Top="61px" Alignment="Right" ID="suppliers_name" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="280px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;:הזמנה&quot;" TemplateText="&quot;:הזמנה&quot;" Visible="True" Left="605px" Top="83px" Alignment="Right" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="49px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס מסמך&quot;" TemplateText="&quot;מס מסמך&quot;" Visible="True" Left="0px" Top="130px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="88px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות במסופון&quot;" TemplateText="&quot;כמות במסופון&quot;" Visible="True" Left="159px" Top="130px" Alignment="Center" ID="material_quantity_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם פריט&quot;" TemplateText="&quot;שם פריט&quot;" Visible="True" Left="226px" Top="130px" Alignment="Center" ID="materials_name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="282px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד&quot;" TemplateText="&quot;ברקוד&quot;" Visible="True" Left="512px" Top="130px" Alignment="Center" ID="materials_barcode_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="109px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר&quot;" TemplateText="&quot;מספר&quot;" Visible="True" Left="625px" Top="130px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="488px" Top="83px" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="111px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות בחש ספק&quot;" TemplateText="&quot;כמות בחש ספק&quot;" Visible="True" Left="92px" Top="130px" Alignment="Center" ID="actual_quantity_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="33px" Width="63px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;דוח פריטים שהופיעו בחשבונית/ת. משלוח ספק ולא התקבלו&quot;" TemplateText="&quot;דוח פריטים שהופיעו בחשבונית/ת. משלוח ספק ולא התקבלו&quot;" Visible="True" Left="277px" Top="12px" Alignment="Right" ID="t_title" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="376px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="64px" ForeColor="#000000" Type="Footer" ID="footer"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;                                            :תאריך&quot;" TemplateText="&quot;                                            :תאריך&quot;" Visible="True" Left="419px" Top="36px" Alignment="Right" ID="date_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="234px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;                                      :חתימת נהג&quot;" TemplateText="&quot;                                      :חתימת נהג&quot;" Visible="True" Left="4px" Top="36px" Alignment="Right" ID="signature_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="20px" Width="234px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="doc_number" Visible="True" Left="0px" Top="1px" ID="doc_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="88px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="material_quantity" Visible="True" Left="92px" Top="1px" Alignment="Center" ID="material_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="63px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_name" Visible="True" Left="226px" Top="1px" Alignment="Right" ID="materials_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="282px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="512px" Top="1px" Alignment="Right" ID="materials_barcode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="17px" Width="109px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="625px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="57px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="actual_quantity" Visible="True" Left="159px" Top="1px" Alignment="Center" ID="actual_quantity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="63px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

