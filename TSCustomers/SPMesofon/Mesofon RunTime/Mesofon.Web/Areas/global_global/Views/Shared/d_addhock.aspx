<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_addhockRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window" >
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Serial Number&quot;" TemplateText="&quot;Serial Number&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="serial_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="82px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="86px" Top="2px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Supplier Number&quot;" TemplateText="&quot;Supplier Number&quot;" Visible="True" Left="178px" Top="2px" Alignment="Center" ID="supplier_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="96px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Date Move&quot;" TemplateText="&quot;Date Move&quot;" Visible="True" Left="276px" Top="2px" Alignment="Center" ID="date_move_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="132px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Is Active&quot;" TemplateText="&quot;Is Active&quot;" Visible="True" Left="410px" Top="2px" Alignment="Center" ID="is_active_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="72px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Usage Datetime&quot;" TemplateText="&quot;Usage Datetime&quot;" Visible="True" Left="484px" Top="2px" Alignment="Center" ID="usage_datetime_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="132px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Order Number&quot;" TemplateText="&quot;Order Number&quot;" Visible="True" Left="618px" Top="2px" Alignment="Center" ID="order_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="81px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Item Number&quot;" TemplateText="&quot;Item Number&quot;" Visible="True" Left="701px" Top="2px" Alignment="Center" ID="item_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="74px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Adhoc Key&quot;" TemplateText="&quot;Adhoc Key&quot;" Visible="True" Left="777px" Top="1px" ID="adhoc_key_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Trade Number&quot;" TemplateText="&quot;Trade Number&quot;" Visible="True" Left="869px" Top="1px" ID="trade_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="104px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Row Serial Number&quot;" TemplateText="&quot;Row Serial Number&quot;" Visible="True" Left="975px" Top="1px" ID="row_serial_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="252px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Adhoc Type&quot;" TemplateText="&quot;Adhoc Type&quot;" Visible="True" Left="1229px" Top="1px" ID="adhoc_type_t" BorderStyle="None" Enabled="False" Font-Names="Tahoma" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="466px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="serial_number" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="serial_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="82px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="86px" Top="2px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="178px" Top="2px" Alignment="Right" ID="supplier_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="96px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="date_move" Visible="True" Left="276px" Top="2px" ID="date_move" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="132px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="is_active" Visible="True" Left="410px" Top="2px" Alignment="Right" ID="is_active" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="72px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="[shortdate] [time]" DataMember="usage_datetime" Visible="True" Left="484px" Top="2px" ID="usage_datetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="132px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="618px" Top="2px" Alignment="Right" ID="order_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="81px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="item_number" Visible="True" Left="701px" Top="2px" Alignment="Right" ID="item_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="74px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="adhoc_key" Visible="True" Left="777px" Top="0px" ID="adhoc_key" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="23px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="trade_number" Visible="True" Left="869px" Top="0px" ID="trade_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="23px" Width="104px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="row_serial_number" Visible="True" Left="975px" Top="0px" ID="row_serial_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="252px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="adhoc_type" Visible="True" Left="1230px" Top="0px" ID="adhoc_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="465px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

