<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_diff_pallet_inv_mt_reportRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;מס מסמך&quot;" TemplateText="&quot;מס מסמך&quot;" Visible="True" Left="0px" Top="2px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="29px" Width="42px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות במסופון&quot;" TemplateText="&quot;כמות במסופון&quot;" Visible="True" Left="73px" Top="2px" Alignment="Center" ID="actual_quantity_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="29px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;כמות בחש&quot;" TemplateText="&quot;כמות בחש&quot;" Visible="True" Left="46px" Top="2px" Alignment="Center" ID="material_quantity_t" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="29px" Width="24px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;ברקוד ושם פריט&quot;" TemplateText="&quot;ברקוד ושם פריט&quot;" Visible="True" Left="100px" Top="2px" Alignment="Center" ID="materials_barcode_name_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="194px" Top="2px" Alignment="Center" ID="row_t" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="15px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="193px" Top="1px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="31px" Width="16px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="doc_number" Visible="True" Left="0px" Top="0px" ID="doc_number" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="44px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_barcode" Visible="True" Left="99px" Top="1px" Alignment="Right" ID="materials_barcode" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="15px" Width="92px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="materials_name" Visible="True" Left="99px" Top="17px" Alignment="Right" ID="materials_name" Font-Names="Arial" Font-Size="7pt" ForeColor="WindowText" Height="15px" Width="92px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#####0" DataMember="material_quantity" Visible="True" Left="46px" Top="0px" Alignment="Right" ID="material_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="24px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" Format="#####0" DataMember="actual_quantity" Visible="True" Left="73px" Top="0px" Alignment="Right" ID="actual_quantity" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="33px" Width="24px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

