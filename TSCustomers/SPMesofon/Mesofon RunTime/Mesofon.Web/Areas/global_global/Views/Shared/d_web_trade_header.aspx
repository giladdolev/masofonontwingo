<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_web_trade_headerRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="27px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewTextField runat="server" DataMember="statusname" Visible="True" Left="18px" Top="8px" ID="statusname" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="statuscode" Visible="True" Left="120px" Top="8px" ID="statuscode" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="statusdatetime" Visible="True" Left="222px" Top="8px" ID="statusdatetime" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="orderorgno" Visible="True" Left="324px" Top="8px" ID="orderorgno" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="orderdate" Visible="True" Left="426px" Top="8px" ID="orderdate" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="97px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" Type="Details" ID="detail"></vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

