<%@ Page Language="C#" MasterPageFile="~/Site.Master" Inherits="System.Web.Mvc.ViewPage<global.w_sql_syntax>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<asp:Content ContentPlaceHolderID="Content" runat="server">
	<vt:WindowView runat="server" Opacity="1" Resizable="True" ID="w_sql_syntax" LoadAction="w_sql_syntax\Form_Load" BackColor="ButtonFace" Height="311px" Width="487px">
		<vt:Button runat="server" Text="בטל" Top="257px" Left="256px" ID="cb_cancle"  ClickAction="w_sql_syntax\cb_cancle_clicked" Height="28px" TabIndex="30" Width="88px">
		</vt:Button>
		<vt:Button runat="server" Text="בצע" Top="256px" Left="120px" ID="cb_ok"  ClickAction="w_sql_syntax\cb_ok_clicked" Height="28px" TabIndex="20" Width="88px">
		</vt:Button>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="4px" Left="7px" ID="mle_sql_syntax" ForeColor="WindowText" Height="247px" TabIndex="10" Width="466px">
		</vt:TextBox>
	</vt:WindowView>

</asp:Content>
