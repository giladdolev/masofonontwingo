<%@ Page Language="C#"   Inherits="System.Web.Mvc.ViewPage<global.w_add_translation>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
	<vt:WindowView runat="server" Opacity="1" Resizable="True" ID="w_add_translation" LoadAction="w_add_translation\Form_Load" BackColor="ButtonFace" Height="291px" Width="554px">
		<vt:Button runat="server" Text="Exit" Top="225px" Left="288px" ID="cb_2" ClickAction="w_add_translation\cb_2_clicked" Height="38px" TabIndex="30" Width="100px">
		</vt:Button>
		<vt:Label runat="server" TextAlign="TopLeft" Top="57px" Left="159px" ID="st_number" BackColor="#D4D0C8" Font-Names="Arial" Font-Size="12pt" ForeColor="#000080" Height="18px" Width="100px">
		</vt:Label>
		<vt:Button runat="server" Text="Save And Exit" Top="225px" Left="134px" ID="cb_1" ClickAction="w_add_translation\cb_1_clicked" Height="38px" TabIndex="20" Width="135px">
		</vt:Button>
		<vt:Label runat="server" TextAlign="TopLeft" Text="Translation For Expression" Top="161px" Left="13px" ID="st_4" BackColor="#D4D0C8" Font-Names="Arial" Font-Size="12pt" ForeColor="#000080" Height="21px" Width="215px">
		</vt:Label>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Text="*********  Add Translation **********" Top="184px" Left="13px" ID="sle" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="25px" TabIndex="10" Width="525px">
		</vt:TextBox>
		<vt:Label runat="server" TextAlign="TopLeft" Text="Hebrew Expression " Top="57px" Left="14px" ID="st_3" BackColor="#D4D0C8" Font-Names="Arial" Font-Size="12pt" ForeColor="#000080" Height="21px" Width="143px">
		</vt:Label>
		<vt:Label runat="server" TextAlign="TopLeft" Text="Insert Translation For Expression" Top="20px" Left="16px" ID="st_1" BackColor="#D4D0C8" Font-Names="Arial" Font-Size="14pt" ForeColor="#000080" Height="25px" Width="522px">
		</vt:Label>
		<vt:TextBox runat="server" PasswordChar="" MaxLength="32767" Top="80px" Left="13px" ID="st" BackColor="ButtonFace" ForeColor="WindowText" Height="75px" TabIndex="10" Width="524px">
		</vt:TextBox>
	</vt:WindowView>

