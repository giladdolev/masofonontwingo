<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.dwh_invoiceRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="22px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;שם מפיץ&quot;" TemplateText="&quot;שם מפיץ&quot;" Visible="True" Left="406px" Top="3px" Alignment="Center" ID="distributor_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="125px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם מחסן&quot;" TemplateText="&quot;שם מחסן&quot;" Visible="True" Left="3px" Top="3px" Alignment="Center" ID="stock_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="53px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;שם עסק&quot;" TemplateText="&quot;שם עסק&quot;" Visible="True" Left="60px" Top="3px" Alignment="Center" ID="store_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="51px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;תאריך חשבונית&quot;" TemplateText="&quot;תאריך חשבונית&quot;" Visible="True" Left="206px" Top="3px" Alignment="Center" ID="supply_date_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="83px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;הנחה%&quot;" TemplateText="&quot;הנחה%&quot;" Visible="True" Left="155px" Top="3px" Alignment="Center" ID="t_discount" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;B2B&quot;" TemplateText="&quot;B2B&quot;" Visible="True" Left="115px" Top="3px" Alignment="Center" ID="b2b_status_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="36px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס חשבונית&quot;" TemplateText="&quot;מס חשבונית&quot;" Visible="True" Left="293px" Top="3px" Alignment="Center" ID="invoice_number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="109px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;יצרן לייף&quot;" TemplateText="&quot;יצרן לייף&quot;" Visible="True" Left="60px" Top="3px" Alignment="Center" ID="life_manufacturer_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="51px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="21px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Left="349px" Top="31px" Alignment="Right" ID="original_discount" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="9px"></vt:DataViewLabelField>
<vt:DataViewTextField runat="server" DataMember="log_book" Left="335px" Top="28px" ID="log_book" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="9px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="branch_number" Left="317px" Top="29px" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="16px" Width="9px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="expected_total_amount" Left="300px" Top="28px" ID="expected_total_amount" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="17px" Width="9px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_type" Left="286px" Top="30px" ID="invoice_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="9px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_total" Left="269px" Top="30px" Alignment="Right" ID="invoice_total" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="9px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Left="256px" Top="29px" Alignment="Right" ID="state" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="8px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" Text="&quot;&quot;" TemplateText="&quot;&quot;" Left="230px" Top="30px" Alignment="Right" ID="original_invoice_total" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="14px" Width="21px"></vt:DataViewLabelField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="406px" Top="1px" Alignment="Right" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="18px" Width="125px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="3px" Top="1px" ID="stock_number" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="18px" Width="53px"></vt:DataViewDropDownField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="60px" Top="1px" ID="store_number" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="18px" Width="51px"></vt:DataViewDropDownField>
<vt:DataViewTextField runat="server" DataMember="supply_date" Visible="True" Left="206px" Top="1px" Alignment="Center" ID="supply_date" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="18px" Width="83px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="discount_percentage" Left="155px" Top="1px" ID="discount_percentage" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="18px" Width="47px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="b2b_status" Visible="True" Left="115px" Top="1px" Alignment="Center" ID="b2b_status" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="18px" Width="36px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="155px" Top="1px" ID="discount_percent" BorderStyle="None" Font-Names="Arial" Font-Size="11pt" ForeColor="#000000" Height="18px" Width="47px"></vt:DataViewLabelField>
<vt:DataViewDropDownField runat="server" Visible="True" Left="293px" Top="1px" Alignment="Right" ID="invoice_number" Font-Names="Arial" Font-Size="11pt" ForeColor="WindowText" Height="18px" Width="109px"></vt:DataViewDropDownField>
<vt:DataViewTextField runat="server" DataMember="price_list_number" Left="530px" Top="0px" ID="price_list_number" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="19px" Width="2px"></vt:DataViewTextField>
<vt:DataViewCheckField runat="server" Visible="True" Left="76px" Top="1px" ID="life_manufacturer" BorderStyle="None" Font-Names="Arial" Font-Size="12pt" ForeColor="#000000" Height="18px" Width="21px"></vt:DataViewCheckField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

