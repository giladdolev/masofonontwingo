<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_dddw_suppliersRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="19px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;שם&quot;" TemplateText="&quot;שם&quot;" Visible="True" Left="3px" Top="1px" Alignment="Center" ID="name_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="272px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;קוד&quot;" TemplateText="&quot;קוד&quot;" Visible="True" Left="279px" Top="1px" Alignment="Center" ID="number_t" Font-Names="Arial" Font-Size="10pt" ForeColor="#FFFFFF" Height="16px" Width="78px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="name" Visible="True" Left="3px" Top="1px" Alignment="Right" ID="name" Font-Names="Arial" Font-Size="20pt" Height="30px" Width="272px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="number" Visible="True" Left="279px" Top="1px" Alignment="Right" ID="number" Font-Names="Arial" Font-Size="20pt" Height="30px" Width="78px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

