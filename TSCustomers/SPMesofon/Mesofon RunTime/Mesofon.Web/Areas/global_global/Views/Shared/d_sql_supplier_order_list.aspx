<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_sql_supplier_order_listRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="#D4D0C8">
	<Bands>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" ID="Header"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="order_number" Visible="True" Left="78px" Top="2px" Alignment="Right" ID="order_number" Font-Names="Arial" Font-Size="10pt" Height="16px" Width="69px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="state" Left="152px" Top="2px" ID="state" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="16px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="distributor_number" Left="168px" Top="2px" ID="distributor_number" Font-Names="Arial" Font-Size="10pt" ForeColor="#000000" Height="16px" Width="22px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="supply_date" Visible="True" Left="3px" Top="2px" ID="supply_date" Font-Names="Arial" Font-Size="10pt" Height="16px" Width="71px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

