<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.ds_branch_station_paramsRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="20px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="&quot;Branch Number&quot;" TemplateText="&quot;Branch Number&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="branch_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Station Number&quot;" TemplateText="&quot;Station Number&quot;" Visible="True" Left="94px" Top="2px" Alignment="Center" ID="station_number_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="90px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Parameter Name&quot;" TemplateText="&quot;Parameter Name&quot;" Visible="True" Left="186px" Top="2px" Alignment="Center" ID="parameter_name_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="186px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Parameter Type&quot;" TemplateText="&quot;Parameter Type&quot;" Visible="True" Left="374px" Top="2px" Alignment="Center" ID="parameter_type_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="92px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Parameter Value&quot;" TemplateText="&quot;Parameter Value&quot;" Visible="True" Left="468px" Top="2px" Alignment="Center" ID="parameter_value_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="768px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Default Value&quot;" TemplateText="&quot;Default Value&quot;" Visible="True" Left="1238px" Top="2px" Alignment="Center" ID="default_value_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="768px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Description&quot;" TemplateText="&quot;Description&quot;" Visible="True" Left="2008px" Top="2px" Alignment="Center" ID="description_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="768px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;Parameter Entity&quot;" TemplateText="&quot;Parameter Entity&quot;" Visible="True" Left="2778px" Top="2px" Alignment="Center" ID="parameter_entity_t" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="16px" Width="98px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="23px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="branch_number" Visible="True" Left="2px" Top="2px" Alignment="Right" ID="branch_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="station_number" Visible="True" Left="94px" Top="2px" Alignment="Right" ID="station_number" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="90px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="parameter_name" Visible="True" Left="186px" Top="2px" ID="parameter_name" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="186px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="parameter_type" Visible="True" Left="374px" Top="2px" ID="parameter_type" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="92px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="parameter_value" Visible="True" Left="468px" Top="2px" ID="parameter_value" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="768px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="default_value" Visible="True" Left="1238px" Top="2px" ID="default_value" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="768px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="description" Visible="True" Left="2008px" Top="2px" ID="description" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="768px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="parameter_entity" Visible="True" Left="2778px" Top="2px" Alignment="Right" ID="parameter_entity" BorderStyle="None" Font-Names="Arial" Font-Size="10pt" ForeColor="WindowText" Height="19px" Width="98px"></vt:DataViewTextField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

