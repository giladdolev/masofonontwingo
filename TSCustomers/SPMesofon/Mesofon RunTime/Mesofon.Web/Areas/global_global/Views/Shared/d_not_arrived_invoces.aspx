<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Mesofon.Repository.d_not_arrived_invocesRepository>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%#Model.PrimaryList%>"  Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" ForeColor="Window">
	<Bands>
<vt:DataViewBand runat="server" Height="33px" ForeColor="#000000" ID="Header"><Fields>
<vt:DataViewLabelField runat="server" Text="" TemplateText="" Visible="True" Left="117px" Top="2px" Alignment="Center" ID="t_2" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="29px" Width="43px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מספר חשבונית&quot;" TemplateText="&quot;מספר חשבונית&quot;" Visible="True" Left="2px" Top="2px" Alignment="Center" ID="t_3" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="111px"></vt:DataViewLabelField>
<vt:DataViewLabelField runat="server" Text="&quot;מס&quot;" TemplateText="&quot;מס&quot;" Visible="True" Left="163px" Top="2px" Alignment="Center" ID="t_1" Font-Names="Arial" Font-Size="8pt" ForeColor="#000000" Height="29px" Width="17px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Summary" ID="summary"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="0px" ForeColor="#000000" Type="Footer" ID="footer"></vt:DataViewBand>
<vt:DataViewBand runat="server" Height="16px" ForeColor="#000000" Type="Details" ID="detail"><Fields>
<vt:DataViewTextField runat="server" DataMember="supplier_number" Visible="True" Left="117px" Top="1px" Alignment="Center" ID="supplier_number" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="13px" Width="44px"></vt:DataViewTextField>
<vt:DataViewTextField runat="server" DataMember="invoice_number" Visible="True" Left="2px" Top="1px" Alignment="Right" ID="invoice_number" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="13px" Width="112px"></vt:DataViewTextField>
<vt:DataViewLabelField runat="server" TemplateText="" Visible="True" Left="163px" Top="0px" ID="row" BorderStyle="None" Font-Names="Arial" Font-Size="8pt" ForeColor="WindowText" Height="14px" Width="18px"></vt:DataViewLabelField>
</Fields>
</vt:DataViewBand>
	</Bands>
</vt:DataView>
</vt:ControlView>

