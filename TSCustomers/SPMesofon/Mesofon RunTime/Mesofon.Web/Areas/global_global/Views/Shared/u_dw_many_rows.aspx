<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<global.u_dw_many_rows>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:ControlView runat="server">
<vt:DataView runat="server" AutoSize="True" DataSource="<%Model%>"  ID="u_dw" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="100px" Width="150px">
</vt:DataView>
</vt:ControlView>

