<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<global.u_cb_closewindow>" %>
<%@ Register Assembly="System.Web.VisualTree.WebControls" Namespace="System.Web.VisualTree.WebControls" TagPrefix="vt" %>
<vt:Button runat="server" ImageIndex="-1" TextAlign="MiddleCenter" AutoSize="True" Text="סגירה" ID="u_cb_closewindow" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="25px" Width="61px" ActionClick="Close_MouseClick">
</vt:Button>

