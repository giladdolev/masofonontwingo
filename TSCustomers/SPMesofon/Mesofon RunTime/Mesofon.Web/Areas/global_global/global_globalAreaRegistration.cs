using System.Web.Mvc;

namespace global.Areas.global_global
{
    public class global_globalAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "global_global";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "global_global_default",
                "global_global/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "global" }
            );
        }
    }
}