using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using masofonAlias = masofon;
using System.Web.VisualTree.Extensions;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class u_dw_updateController : u_dw_many_rowsController
	{

		public ActionResult u_dw_update()
		{
			return this.View(new u_dw_update());
		}
		private u_dw_update ViewModel
		{
			get { return this.GetRootVisualElement() as u_dw_update; }
		}
		public void uf_set_column_protection(bool ab_protect_flag, bool ab_protect_color)
		{
			long ll = 0;
			long ll_columns = 0;
			string ls_protect = null;
			
			 
			ll_columns = Convert.ToInt64(this.ViewModel.ColumnCount());
			if (ab_protect_flag)
			{
				ls_protect = "1";
			}
			else
			{
				ls_protect = "0";
			}
			for (ll = 1; ll <= ll_columns; ll++)
			{
				 
				if (this.ViewModel.Describe("#" + ll.ToString() + ".tag") != "-1")
				{
				    this.ViewModel.Modify("#" + ll.ToString(), "Protect", ls_protect);
					if (ab_protect_color)
					{
                        this.ViewModel.Modify("#" + ll.ToString(), "Background.Color", "80269524");
					}
				}
			}
		}
		protected async Task<bool> uf_check_validation_date_to_date(bool ab_allow_empty_values, string as_column_name, long al_from_error_number, long al_to_error_number, long al_between_error_number) // 						wf_check_validation_date_to_date 					
		{
			
			DateTime? ldt_from_date = null;
			DateTime? ldt_to_date = null;
			 
			ldt_from_date = this.ViewModel.GetItemValue<DateTime>(0, "from_" + as_column_name);
			 
			ldt_to_date = this.ViewModel.GetItemValue<DateTime>(0, "to_" + as_column_name);
			if (!ab_allow_empty_values)
			{
				if (ldt_from_date == default(DateTime))
				{
					//.. from_number can not be empty
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_from_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
				else if (ldt_to_date == default(DateTime))
				{
					//.. to_number can not be empty
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_to_error_number));
					 
					this.ViewModel.SetColumn("to_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			if (!(ldt_from_date == default(DateTime)) && !(ldt_to_date == default(DateTime)))
			{
				if (ldt_from_date > ldt_to_date)
				{
					//.. from_number must be <= to_number
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_between_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			return true;
		}
		protected async Task<bool> uf_check_validation_datetime_to_datetime(bool ab_allow_empty_values, string as_column_name, long al_from_error_number, long al_to_error_number, long al_between_error_number) // 						wf_check_validation_datetime_to_datetime 					
		{
			
			DateTime? ldt_from_DateTime = null;
			DateTime? ldt_to_DateTime = null;
			
			ldt_from_DateTime = this.ViewModel.GetItemValue<DateTime>(0, "from_" + as_column_name);
			
			ldt_to_DateTime = this.ViewModel.GetItemValue<DateTime>(0, "to_" + as_column_name);
			if (!ab_allow_empty_values)
			{
				if (ldt_from_DateTime == default(DateTime))
				{
					//.. from_number can not be empty
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_from_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
				else if (ldt_to_DateTime == default(DateTime))
				{
					//.. to_number can not be empty
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_to_error_number));
					 
					this.ViewModel.SetColumn("to_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			if (!(ldt_from_DateTime == default(DateTime)) && !(ldt_to_DateTime == default(DateTime)))
			{
				if (ldt_from_DateTime > ldt_to_DateTime)
				{
					//.. from_number must be <= to_number
				await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_between_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			return true;
		}
		protected async Task<bool> uf_check_validation_n_to_n(bool ab_allow_empty_values, string as_column_name, long al_from_error_number, long al_to_error_number, long al_between_error_number) // 						wf_check_validation_n_to_n 					
		{
			
			decimal ldec_from_number = default(decimal);
			decimal ldec_to_number = default(decimal);
			
			ldec_from_number = this.ViewModel.GetItemValue<decimal>(0, Convert.ToString("from_" + as_column_name));
			
			ldec_to_number = this.ViewModel.GetItemValue<decimal>(0, Convert.ToString("to_" + as_column_name));
			if (!ab_allow_empty_values)
			{
				if (ldec_from_number == 0)
				{
					//.. from_number can not be empty
				await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_from_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
				else if (ldec_to_number == 0)
				{
					//.. to_number can not be empty
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_to_error_number));
					 
					this.ViewModel.SetColumn("to_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			if (!(ldec_from_number == 0) && !(ldec_to_number == 0))
			{
				if (ldec_from_number > ldec_to_number)
				{
					//.. from_number must be <= to_number
					await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number((int)al_between_error_number));
					 
					this.ViewModel.SetColumn("from_" + as_column_name);
					this.ViewModel.Focus();
					 
					this.ViewModel.SelectText(1, 20);
					return false;
				}
			}
			return true;
		}
		public void losefocus(EventArgs e)
		{
			 
		    this.ViewModel.Parent.PerformLostFocus(e);
			 
			this.ViewModel.PerformValidated(e);
		}
		public void updatestart()
		{

		//    this.ViewModel.Parent.UpdateStart();
			this.ViewModel.is_last_sql_error_textProperty = "";
		}
	}
}
