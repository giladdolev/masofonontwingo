using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class dw_parent_supplier_ediController : MvcSite.Common.SPBaseController
	{
		public ActionResult dw_parent_supplier_edi()
		{
			dw_parent_supplier_ediRepository repository = new dw_parent_supplier_ediRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
