using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using masofonAlias = masofon;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class w_sql_syntaxController : MvcSite.Common.SPBaseController
	{

        public ActionResult w_sql_syntax(string sqlSyntax)
		{
            return this.View(new w_sql_syntax(sqlSyntax));
		}
		private w_sql_syntax ViewModel
		{
			get { return this.GetRootVisualElement() as w_sql_syntax; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void ue_key(Keys key)
		{
			TextBoxElement mle_sql_syntax = this.GetVisualElementById<TextBoxElement>("mle_sql_syntax");
			object keyflags = null;
			if (Convert.ToInt32(keyflags) == 2)
			{
                if (key == Keys.A)
				{
					mle_sql_syntax.Select(1, mle_sql_syntax.Text.Length);
				}
                if (key == Keys.D)
				{
					mle_sql_syntax.Select(mle_sql_syntax.Text.Length, 0);
				}
			}
		}
		public void open()
		{
			TextBoxElement mle_sql_syntax = this.GetVisualElementById<TextBoxElement>("mle_sql_syntax");
		    WindowElement window = this.GetVisualElementById<WindowElement>("w_sql_syntax");
            f_center_windowClass.f_center_window(window);

			
			mle_sql_syntax.Text = WindowHelper.GetParam<string>(this.ViewModel);
		}
		
		public void cb_cancle_clicked(object sender, EventArgs e)
		{
			 
			
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, "Cancled");
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			TextBoxElement mle_sql_syntax = this.GetVisualElementById<TextBoxElement>("mle_sql_syntax");
			 
			
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, Convert.ToString(mle_sql_syntax.Text));
		}
	}
}
