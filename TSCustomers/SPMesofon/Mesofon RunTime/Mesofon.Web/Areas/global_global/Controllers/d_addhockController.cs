using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_addhockController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_addhock()
		{
			d_addhockRepository repository = new d_addhockRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
