using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class ds_marlog_distributor_translateController : MvcSite.Common.SPBaseController
	{
		public ActionResult ds_marlog_distributor_translate()
		{
			ds_marlog_distributor_translateRepository repository = new ds_marlog_distributor_translateRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
