using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Threading.Tasks;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;

using global;
using mini_terminal;
using Mesofon.Common;
using System.Web.VisualTree;

namespace Mesofon.Global.Controllers
{
    public class rw_addhockController : MvcSite.Common.SPBaseController
    {

        public ActionResult rw_addhock(s_array_arguments args)
        {
            return this.View(new rw_addhock(args));
        }
        private rw_addhock ViewModel
        {
            get { return this.GetRootVisualElement() as rw_addhock; }
        }

        public static bool enterKeyFlag { get; private set; }

        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }

        public void ue_close() // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - prepare window for close
        {

            s_array_arguments lstr_parms = default(s_array_arguments);
            lstr_parms = this.ViewModel.Args;
            lstr_parms.a_boolean[0] = this.ViewModel.ib_return_valueProperty;
            lstr_parms.a_long[1] = this.ViewModel.il_adhoc_serial_numberProperty;
            lstr_parms.a_string[1] = this.ViewModel.is_adhoc_keyProperty;
            // AlexKh - 1.2.45.21 - 2010-03-04 - SPUCM00002001 - send back adhoc list
            lstr_parms.a_datastore[1] = this.ViewModel.ids_adhoc_update_listProperty;

            WindowHelper.Close(this.ViewModel, lstr_parms.ToString());
        }
        public async Task<bool> wf_handle_adhoc(int ai_state) //**********************************************************************************************
        {
            //*Object:				rw_addhock
            //*Function Name:	wf_handle_adhoc
            //*Purpose: 			handle adhoc number
            //*Arguments: 		Integer	ai_state	0 - trying to unlock the adhoc
            //*												1 - trying to lock the adhoc
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version	Task#	 					Description
            //*-------------------------------------------------------------------------------------------------
            //*29-10-2009		AlexKh			1.2.45.21	SPUCM00002001		Initial version
            //************************************************************************************************

            long ll_rowcount = 0;
            long ll_is_active = 0;
            long ll_row = 0;
            DateTime? ldt_usage_date = null;
            IRepository lds_adhoc;
            if (isempty_stringClass.isempty_string(this.ViewModel.is_adhoc_keyProperty))
            {
                return false;
            }
            // AlexKh - 1.2.45.21 - 2010-03-07 - SPUCM00002001 - check the adhoc in the list of temporary locked
            if (wf_find_adhoc(ai_state))
            {
                return true;
            }
            lds_adhoc = new d_addhockRepository();

            ll_rowcount = lds_adhoc.Retrieve(this.ViewModel.is_adhoc_keyProperty, this.ViewModel.il_branch_numberProperty, this.ViewModel.il_supplier_numberProperty);
            if (ll_rowcount < 1)
            {
                await uf_show_message("שגיאה", "", "OK", "!מספר אישור לא נמצא בבסיס נתונים");
                return false;
            }

            ldt_usage_date = lds_adhoc.GetItemValue<DateTime?>(0, "usage_datetime");

            ll_is_active = lds_adhoc.GetItemValue<long>(0, "is_active");
            if (ll_is_active == 0)
            {
                ll_is_active = 0;
            }
            if (ll_is_active < global.rw_addhock.UNLOCKED || ll_is_active > global.rw_addhock.FINALY_LOCKED) //Check that adhoc is active
            {
                await uf_show_message("שגיאה", "", "OK", "!מספר אישור לא פעיל");
                return false;
            }
            if (ai_state == 1) //trying to lock the adhoc
            {
                // AlexKh - 1.2.45.21 - 2010-03-04 - SPUCM00002001 - check the adhoc is locked
                if (ll_is_active == global.rw_addhock.FINALY_LOCKED)
                {
                    await uf_show_message("שגיאה", "", "OK", "!מספר אישור זה מנוצל");
                    return false;
                }
                if (ll_is_active == global.rw_addhock.TEMPORARY_LOCKED)
                {
                    await uf_show_message("שגיאה", "", "OK", "!מספר אישור זה בסטאטוס נעול זמנית");
                    return false;
                }

                lds_adhoc.SetItem(0, "item_number", this.ViewModel.il_item_numberProperty);

                lds_adhoc.SetItem(0, "row_serial_number", this.ViewModel.il_row_serial_numberProperty);

                this.ViewModel.il_adhoc_serial_numberProperty = lds_adhoc.GetItemValue<long>(0, "serial_number");

                this.ViewModel.is_adhoc_keyProperty = lds_adhoc.GetItemValue<string>(0, "adhoc_key");
            }
            else if (ai_state == 0) //trying to release adhoc
            {
                if (ll_is_active == global.rw_addhock.UNLOCKED)
                {
                    await uf_show_message("שגיאה", "", "OK", "!מספר אישור זה לא נעול");
                    return false;
                }
                this.ViewModel.il_adhoc_serial_numberProperty = 0;
                this.ViewModel.is_adhoc_keyProperty = "";
            }
            // AlexKh - 1.2.45.21 - 2010-03-04 - SPUCM00002001 - Temporary lock the adhoc

            lds_adhoc.SetItem(0, "is_active", global.rw_addhock.TEMPORARY_LOCKED);
            // AlexKh - 1.2.48.50 - 2013-05-16 - SPUCM00004179 - set branch number to current branch

            lds_adhoc.SetItem(0, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number);
            //RonY@01/03/2015 1.12.49.64 - SPUCM00005163 - start
            if (this.ViewModel.il_adhoc_typeProperty == 2)
            {

                lds_adhoc.SetItem(0, "adhoc_type", this.ViewModel.il_adhoc_typeProperty);

                lds_adhoc.SetItem(0, "is_active", global.rw_addhock.FINALY_LOCKED);

                lds_adhoc.SetItem(0, "order_number", this.ViewModel.il_order_numberProperty);
            }
            //RonY@01/03/2015 1.12.49.64 - SPUCM00005163 - end
            long result;
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                result = lds_adhoc.Update(unitOfWork);
            }

            if (result != 1)
            {
                await uf_show_message("שגיאה", "", "OK", "!עדכון סטאטוס מספר אישור נכשל");
                return false;
            }
            else
            {
                await uf_show_message("שגיאה", "", "OK", "!עדכון סטאטוס מספר אישור בוצע בהצלחה");
                // AlexKh - 1.2.45.21 - 2010-03-07 - SPUCM00002001 - copy row




                ll_row = lds_adhoc.RowsCopy(0, 1, ModelBuffer.Primary, this.ViewModel.ids_adhoc_update_listProperty, this.ViewModel.ids_adhoc_update_listProperty.RowCount() + 1, ModelBuffer.Primary);
                if (this.ViewModel.il_adhoc_typeProperty == 2)
                {


                    this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount(), "is_active", global.rw_addhock.FINALY_LOCKED.ToString()); //initial state


                    this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount(), "usage_datetime", masofonAlias.masofon.Instance.set_machine_time().ToString());
                }
                else
                {
                    if (ll_row == 1)
                    {
                        if (ai_state == 1) //Trying to Lock
                        {


                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount() - 1, "is_active", global.rw_addhock.UNLOCKED.ToString()); //initial state


                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount() - 1, "usage_datetime", masofonAlias.masofon.Instance.set_machine_time());
                        }
                        else
                        {
                            //Trying to Unlock
                            //initial state


                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount(), "is_active", global.rw_addhock.FINALY_LOCKED.ToString());

                            ldt_usage_date = default(DateTime);


                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(this.ViewModel.ids_adhoc_update_listProperty.RowCount(), "usage_datetime", ldt_usage_date.ToString());
                        }
                    }
                }
                return true;
            }
        }
        public bool wf_find_adhoc(int ai_state) //**********************************************************************************************
        {
            //*Object:				rw_addhock
            //*Function Name:	wf_find_adhoc
            //*Purpose: 			Find adhoc in the list of adhocs in use
            //*Arguments: 		Integer	ai_state	0 - trying to unlock the adhoc
            //*												1 - trying to lock the adhoc
            //*Return:				Boolean	TRUE/FALSE
            //*Date				Programer		Version	Task#	 					Description
            //*-------------------------------------------------------------------------------------------------
            //*07-03-2010		AlexKh			1.2.45.21	SPUCM00002001		Initial version
            //************************************************************************************************

            long? ll_found = 0;
            long ll_count = 0;
            long ll_item_number = 0;
            long ll_is_active = 0;
            DateTime? ldt_usage_datetime = null;

            ll_count = this.ViewModel.ids_adhoc_update_listProperty.RowCount();
            if (ll_count > 0)
            {

                ll_found = this.ViewModel.ids_adhoc_update_listProperty.Find("adhoc_key == \"" + this.ViewModel.is_adhoc_keyProperty + "\"", 0, (int)ll_count);
                if (ll_found >= 0)
                {

                    ll_item_number = this.ViewModel.ids_adhoc_update_listProperty.GetItemValue<long>((int)ll_found, "item_number");
                    if (ll_item_number == 0)
                    {
                        ll_item_number = 0;
                    }
                    if (ll_item_number == this.ViewModel.il_item_numberProperty)
                    {

                        ldt_usage_datetime = this.ViewModel.ids_adhoc_update_listProperty.GetItemValue<DateTime?>((int)ll_found, "usage_datetime");
                        if (!(isempty_datetimeClass.isempty_datetime(ldt_usage_datetime)) && ai_state == 0)
                        {

                            ldt_usage_datetime = default(DateTime);

                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(ll_found.Value, "usage_datetime", ldt_usage_datetime.ToString());
                            this.ViewModel.il_adhoc_serial_numberProperty = 0;
                            this.ViewModel.is_adhoc_keyProperty = "";
                            return true;
                        }
                        else if (isempty_datetimeClass.isempty_datetime(ldt_usage_datetime) && ai_state == 1)
                        {

                            this.ViewModel.ids_adhoc_update_listProperty.SetItem(ll_found.Value, "usage_datetime", masofonAlias.masofon.Instance.set_machine_time());

                            this.ViewModel.il_adhoc_serial_numberProperty = this.ViewModel.ids_adhoc_update_listProperty.GetItemValue<long>((int)ll_found, "serial_number");

                            this.ViewModel.is_adhoc_keyProperty = this.ViewModel.ids_adhoc_update_listProperty.GetItemValue<string>(ll_found.Value, "adhoc_key");
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        //public int uf_resize()//// AlexKh - function for dynamic resize of the window and all it's elementes
        //{
        //    int ii_WinBolderWidth;
        //    int ii_WinBolderHeight;
        //    double Radio = WindowHelper.GetRadio(this.ViewModel, out ii_WinBolderWidth, out ii_WinBolderHeight);
        //    if (Radio == 1.0) // if the screen is default 800 * 600
        //    {
        //        return 0;
        //    }

        //    this.ViewModel.Hide();
        //    this.ViewModel.PixelWidth = (int)((this.ViewModel.PixelWidth - ii_WinBolderWidth) * Radio + ii_WinBolderWidth);
        //    this.ViewModel.PixelHeight = (int)((this.ViewModel.PixelHeight - ii_WinBolderHeight) * Radio + ii_WinBolderHeight);
        //    int i = 0;
        //    ControlElement temp = default(ControlElement); // used to check a variety of control
        //    for (i = 1; i <= CollectionExtensions.UBound(this.ViewModel.Controls); i++)
        //    {
        //        temp = this.ViewModel.Controls[i]; // adjust the size, location
        //        temp.PixelWidth = (int)(temp.PixelWidth * Radio);
        //        temp.PixelLeft = (int)(temp.PixelLeft * Radio);
        //        temp.PixelTop = (int)(temp.PixelTop * Radio);
        //        temp.PixelHeight = (int)(temp.PixelHeight * Radio);
        //        if (temp.GetType() == typeof(TabElement))
        //        {
        //            TabElement mtab = default(TabElement);
        //            mtab = temp as TabElement;
        //            mtab.Font = new Font(mtab.Font.FontFamily, (float)Math.Abs(mtab.Font.Size * Radio)); // set the font
        //        }
        //        else if (temp.GetType() == typeof(ButtonElement))
        //        {
        //            ButtonElement cb = default(ButtonElement);
        //            cb = temp as ButtonElement;
        //            cb.Font = new Font(cb.Font.FontFamily, (float)Math.Abs(cb.Font.Size * Radio));
        //        }
        //        else if (temp.GetType() == typeof(TextBoxElement))
        //        {
        //            TextBoxElement sle = default(TextBoxElement);
        //            sle = temp as TextBoxElement;
        //            sle.Font = new Font(sle.Font.Name, (float)(sle.Font.SizeInPoints * Radio), sle.Font.Style, GraphicsUnit.Point, sle.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(MaskedTextBoxElement))
        //        {
        //            MaskedTextBoxElement em = default(MaskedTextBoxElement);
        //            em = temp as MaskedTextBoxElement;
        //            em.Font = new Font(em.Font.Name, (float)(em.Font.SizeInPoints * Radio), em.Font.Style, GraphicsUnit.Point, em.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(LabelElement))
        //        {
        //            LabelElement st = default(LabelElement);
        //            st = temp as LabelElement;
        //            st.Font = new Font(st.Font.Name, (float)(st.Font.SizeInPoints * Radio), st.Font.Style, GraphicsUnit.Point, st.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(GridElement))
        //        {
        //            // datawindows get zoomed
        //            GridElement DW = default(GridElement);
        //            DW = temp as GridElement;


        //            //DW.set_Zoom(Convert.ToInt32(Math.Round(Radio * 100)).ToString()); // Note DATAWINDOW different with other controls
        //        }
        //        else if (temp.GetType() == typeof(ButtonElement))
        //        {
        //            ButtonElement pb = default(ButtonElement);
        //            pb = temp as ButtonElement;
        //            pb.Font = new Font(pb.Font.FontFamily, (float)(pb.Font.Size * Radio));
        //        }
        //        else if (temp.GetType() == typeof(CheckBoxElement))
        //        {
        //            CheckBoxElement cbx = default(CheckBoxElement);
        //            cbx = temp as CheckBoxElement;
        //            cbx.Font = new Font(cbx.Font.Name, (float)(cbx.Font.SizeInPoints * Radio), cbx.Font.Style, GraphicsUnit.Point, cbx.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(ComboBoxElement))
        //        {
        //            ComboBoxElement ddlb = default(ComboBoxElement);
        //            ddlb = temp as ComboBoxElement;
        //            ddlb.Font = new Font(ddlb.Font.Name, (float)(ddlb.Font.SizeInPoints * Radio), ddlb.Font.Style, GraphicsUnit.Point, ddlb.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(RichTextBox))
        //        {
        //            GroupBoxElement gb = default(GroupBoxElement);
        //            gb = temp as GroupBoxElement;
        //            gb.Font = new Font(gb.Font.FontFamily, (float)Math.Abs(gb.Font.Size * Radio));
        //        }
        //        else if (temp.GetType() == typeof(ListBoxElement))
        //        {
        //            ListBoxElement lb = default(ListBoxElement);
        //            lb = temp as ListBoxElement;
        //            lb.Font = new Font(lb.Font.Name, (float)(lb.Font.SizeInPoints * Radio), lb.Font.Style, GraphicsUnit.Point, lb.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(TextBoxElement))
        //        {
        //            TextBoxElement mle = default(TextBoxElement);
        //            mle = temp as TextBoxElement;
        //            mle.Font = new Font(mle.Font.Name, (float)(mle.Font.SizeInPoints * Radio), mle.Font.Style, GraphicsUnit.Point, mle.Font.GdiCharSet);
        //        }
        //        else if (temp.GetType() == typeof(RadioButtonElement))
        //        {
        //            RadioButtonElement rb = default(RadioButtonElement);
        //            rb = temp as RadioButtonElement;
        //            rb.Font = new Font(rb.Font.FontFamily, (float)Math.Abs(rb.Font.Size * Radio));
        //        }
        //    }

        //    this.ViewModel.Show();
        //    return 0;
        //}


        public async Task<int> uf_show_message(string as_title, string as_error_text, string as_buttons, string as_message)
        {
            int li_rtn_code = 0;
            if (w_mini_terminal.Instance() != null)
            {
                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox(as_title, as_error_text, "", as_buttons, 1, as_message);
            }
            else
            {
                await MessageBox.Show(as_message, as_title);
            }
            return li_rtn_code;
        }
        public void close() // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - call ue_close
        {
            ue_close();
        }
        public void open() // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - prepare window for open
        {
            ButtonElement cb_release = this.GetVisualElementById<ButtonElement>("cb_release");
            ButtonElement cb_ok = this.GetVisualElementById<ButtonElement>("cb_ok");
            TextBoxElement sle_adhoc = this.GetVisualElementById<TextBoxElement>("sle_adhoc");
            s_array_arguments lstr_parms = default(s_array_arguments);
            //WindowElement root = this.GetRootVisualElement() as WindowElement;
            //root.Location = new Point(600,800);

            lstr_parms = this.ViewModel.Args;
            this.ViewModel.il_branch_numberProperty = lstr_parms.a_long[1];
            this.ViewModel.il_supplier_numberProperty = lstr_parms.a_long[2];
            this.ViewModel.il_order_numberProperty = lstr_parms.a_long[3];
            this.ViewModel.il_item_numberProperty = lstr_parms.a_long[4];
            this.ViewModel.il_adhoc_serial_numberProperty = lstr_parms.a_long[5];
            // AlexKh - 1.2.45.21 - 2010-03-04 - SPUCM00002001 - get adhoc list
            this.ViewModel.ids_adhoc_update_listProperty = lstr_parms.a_datastore[1] as IRepository;
            this.ViewModel.il_row_serial_numberProperty = lstr_parms.a_long[6];
            if (this.ViewModel.il_row_serial_numberProperty == 0)
            {
                this.ViewModel.il_row_serial_numberProperty = 0;
            }
            this.ViewModel.is_adhoc_keyProperty = lstr_parms.a_string[1];
            this.ViewModel.il_adhoc_typeProperty = lstr_parms.a_long[7]; //RonY@01/03/2015 1.12.49.64 - SPUCM00005163
            this.ViewModel.ib_return_valueProperty = false;
            if (isempty_stringClass.isempty_string(this.ViewModel.is_adhoc_keyProperty))
            {
                sle_adhoc.Enabled = true;
                sle_adhoc.BackColor = Color.FromArgb(255, 255, 255);
                cb_ok.Enabled = true;
                cb_release.Enabled = false;
            }
            else
            {
                sle_adhoc.Enabled = false;
                sle_adhoc.BackColor = SystemColors.ButtonFace;
                cb_ok.Enabled = false;
                cb_release.Enabled = true;
                sle_adhoc.Text = this.ViewModel.is_adhoc_keyProperty;
            }

            //GalilCS - Focus sle_adhoc
            sle_adhoc.Focus();

            // AlexKh
            //uf_resize();
        }

        public async Task sle_adhoc_KeyDown(object sender, KeyEventArgs e)
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            if (key == Keys.Enter || key == Keys.Tab)
            {
                enterKeyFlag = true;
                await sle_adhoc_TextChanged(sender, e);
            }
        }
        public async Task sle_adhoc_TextChanged(object sender, EventArgs e)
        {
            TextBoxElement sle_adhoc = this.GetVisualElementById<TextBoxElement>("sle_adhoc");
            if ((e is ValueChangedArgs<string> || e is KeyDownEventArgs) && enterKeyFlag)
            {
                if (sle_adhoc.Text != "")
                {
                    enterKeyFlag = false;
                    await cb_ok_clicked(null, EventArgs.Empty);
                }
            }

        }

        public async Task cb_release_clicked(object sender, EventArgs e) // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - release the adhoc
        {
            if (await wf_handle_adhoc(0))
            {
                this.ViewModel.ib_return_valueProperty = true;
            }
            this.ue_close();
        }

        public void cb_cancel_clicked(object sender, EventArgs e) // AlexKh - 1.2.45.2 - 2009-11-03 - SPUCM00001320 - return without any action
        {
            this.ViewModel.ib_return_valueProperty = false;
            this.ue_close();
        }

        public async Task cb_ok_clicked(object sender, EventArgs e) // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - lock the adhoc
        {
            sle_adhoc_modified(sender, e);
            if (await wf_handle_adhoc(1))
            {
                this.ViewModel.ib_return_valueProperty = true;
            }
            this.ue_close();
        }

        public void sle_adhoc_modified(object sender, EventArgs e) // AlexKh - 1.2.45.2 - 2009-10-29 - SPUCM00001320 - inserted adhoc value
        {
            TextBoxElement sle_adhoc = this.GetVisualElementById<TextBoxElement>("sle_adhoc");
            this.ViewModel.is_adhoc_keyProperty = sle_adhoc.Text;
        }
    }
}
