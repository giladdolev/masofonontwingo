using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_not_arrived_invoces_printController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_not_arrived_invoces_print()
		{
			d_not_arrived_invoces_printRepository repository = new d_not_arrived_invoces_printRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
