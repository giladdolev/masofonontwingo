using System.Web.Mvc;
using System.Web.VisualTree.MVC;

using global;
namespace Mesofon.Global.Controllers
{
	public class u_cb_cancelController : u_cb_okController
	{

		public ActionResult u_cb_cancel()
		{
			return this.View(new u_cb_cancel());
		}
		private u_cb_cancel ViewModel
		{
			get { return this.GetRootVisualElement() as u_cb_cancel; }
		}
		
	}
}
