using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using masofonAlias = masofon;


using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class rw_packagesController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_packages(d_packageRepository repository)
		{
            return this.View(new rw_packages(repository));
		}
		private rw_packages ViewModel
		{
			get { return this.GetRootVisualElement() as rw_packages; }
		}

        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void ue_close_with_return(bool ab_variable) //**********************************************************************************************
		{
			GridElement dw_package = this.GetVisualElementById<GridElement>("dw_package");
			//*Object:								w_packages
			//*Function/Event  Name:			ue_close_with_return
			//*Purpose:							Close the window and return .
			//*
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*										--------------------------------------------------------------------------
			//*										Value			Boolean				ab_variable		
			//*
			//*Return:								Boolean: TRUE - The doc no has been changed 
			//*												 	FALSE - Otherwise
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*31-12-2007		Pnina S.G    			0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			s_array_arguments lstr_mess = default(s_array_arguments);
			long ll_quantity = 0;
			long? ll_found_row = 0;
			long ll_row_count = 0;
			GridElement lds_package;
			string ls_find = null;
			if (ab_variable)
			{
				//OK button was clicked 
				lstr_mess.arg_ok = true;
				
				ll_row_count = dw_package.RowCount();
				if (ll_row_count == 1)
				{
					ll_found_row = 1;
				}
				else
				{
					ls_find = "actual_quantity > 0 ";
					
					ll_found_row = dw_package.Find(ls_find, 0, (int)ll_row_count);
				}
				ll_quantity = dw_package.GetItemValue<long>(ll_found_row.Value, "actual_quantity");
				lstr_mess.a_long[1] = ll_quantity;
				lds_package = VisualElementHelper.CreateFromView<GridElement>("d_package","d_package","global_globa");
				
				
				
				dw_package.RowsCopy((int)ll_found_row, (int)ll_found_row, ModelBuffer.Primary, lds_package,  1, ModelBuffer.Primary);
				lstr_mess.a_obj[1] = lds_package;
			}
			else
			{
				//CANCEL button was clicked
				lstr_mess.arg_ok = false;
			}
			
			WindowHelper.Close(this.ViewModel, lstr_mess.ToString());
		}
        public async Task<bool> ue_validation_check(long al_quantity) //**********************************************************************************************
        {
            //*Object:								w_packages
            //*Function/Event  Name:			ue_validation_check
            //*Purpose:							Perform Validation checks.
            //*
            //*Arguments:							None
            //*
            //*Return:								Boolean: TRUE - Pass Validations 
            //*												 	FALSE - Otherwise
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*01-01-2008		Pnina S.G    			0.2.0 			B2B 			Initiation
            //************************************************************************************************

            //LONG						ll_quantity
            //
            //
            //ll_quantity = THIS.dw_package.Object.actual_quantity[1]
            if (al_quantity < 1 || al_quantity == 0)
            {
                await this.wf_show_message("טעות בכמות  חבילות", "", "OK", "כמות לא יכולה להיות אפס");
                return false;
            }
            else
            {
                return true;
            }
        }
        public async Task<long> wf_show_message(string as_title, string as_error_text, string as_buttons, string as_message) //**********************************************************************************************
		{
			//*Object:								w_packages
			//*Function/Event  Name:			wf_show_message
			//*Purpose:							Display message.
			//*  
			//*Arguments:						Pass By		Argument Type			Argument Name
			//*										---------------------------------------------------------------
			//*										Value 			string 					as_title 			:window title
			//*										Value 			string 					as_error_text	:error text
			//*										Value 			string 					as_buttons		:buttons to be displayed
			//*										Value 			string 					as_message		:message text
			//*
			//*Return:								LONG: 1  - succeeded.
			//*												   -1 - failed.
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*31-12-2007    Pnina S.G.				   0.2.0			B2B 			Initiation
			//************************************************************************************************
			
			return await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox(as_title, as_error_text, "", as_buttons, 1, as_message);
		}
		public async Task<bool> wf_data_ok() //**********************************************************************************************
		{
			GridElement dw_package = this.GetVisualElementById<GridElement>("dw_package");
			//*Object:								w_packages
			//*Function/Event  Name:			wf_data_ok
			//*Purpose:							
			//*
			//*Arguments:						
			//*										None
			//*Return:								BOOLEAN:
			//*													TRUE-Data OK
			//*													FALSE- Othervise
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*31-12-2007		Pnina S.G    			0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			bool lb_pass_validation = false;
			long ll_quantity = 0;
			long ll_row_count = 0;
			long? ll_found_row = 0;
			string ls_find = null;
			
			ll_row_count = dw_package.RowCount();
			if (ll_row_count == 1)
			{
				ll_found_row = 1;
			}
			else
			{
				ls_find = "actual_quantity > 0 ";
				
				ll_found_row = dw_package.Find(ls_find, 0, (int)ll_row_count);
			}
			
			 
			
			ll_quantity = dw_package.GetItemValue<long>(ll_found_row.Value, "actual_quantity");
            lb_pass_validation = await ue_validation_check(ll_quantity);
			if (!lb_pass_validation)
			{
				dw_package.Focus();
				 
				dw_package.SetRow(1);
				 
				dw_package.SetColumn("actual_quantity");
				return false;
			}
			else
			{
				return true;
			}
		}
		public void open() //**********************************************************************************************
		{
			GridElement dw_package = this.GetVisualElementById<GridElement>("dw_package");
			//*Object:								w_packages
			//*Function/Event  Name:			open
			//*Purpose:							
			//*
			//*Arguments:						
			//*										None
			//*Return:								Boolean: TRUE - The doc no has been changed 
			//*												 	FALSE - Otherwise
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*31-12-2007		Pnina S.G    			0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			IRepository lds_package;
			long ll_row_count = 0;
			
			
			
			 
			lds_package = new d_packageRepository();
			
			lds_package = WindowHelper.GetParam<object>(this.ViewModel) as IRepository;
			
			ll_row_count = lds_package.RowCount();



            lds_package.RowsCopy(1, (int)ll_row_count, ModelBuffer.Primary, dw_package.GetDataObject(), 1, ModelBuffer.Primary);
			dw_package.Focus();
			 
			dw_package.SetRow(1);
			 
			dw_package.SetColumn("actual_quantity");
			 
			dw_package.SelectText(1, 20);
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e) //**********************************************************************************************
		{
			//*Object:								w_packages.cb_cancel
			//*Function/Event  Name:			clicked
			//*Purpose:							
			//*
			//*Arguments:						
			//*										None
			//*Return:								Long
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*31-12-2007		Pnina S.G    			0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			this.ue_close_with_return(false);
		}

        public async Task cb_ok_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								w_packages.cb_ok
            //*Function/Event  Name:			clicked
            //*Purpose:							
            //*
            //*Arguments:						
            //*										None
            //*Return:								Long
            //*
            //*Date				Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*31-12-2007		Pnina S.G    			0.2.0 			B2B 			Initiation
            //************************************************************************************************

            bool lb_data_ok = false;
            lb_data_ok = await wf_data_ok();
            if (lb_data_ok)
            {
                this.ue_close_with_return(true);
            }
        }

        public async Task<int> dw_package_itemchanged(int row, VisualElement dwo, string data)
        {
            //**********************************************************************************************
            //*Object:								w_packages
            //*Function/Event  Name:			dw_package.ItemChanged
            //*Purpose:							Check validation 
            //*										
            //*  
            //*Arguments:						Pass By			Argument Type 		Argument Name
            //*										-------------------------------------------------------------------------------
            //*										Value 			Long						row
            //*										Value				ControlElement				dwo
            //*										Value 			String 					data
            //*Return:								Long.
            //*
            //*Date 			Programer				Version		Task#			Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*01-12-2007    Pnina S.G.					0.2.0			B2B 			Initiation
            //************************************************************************************************
            string ls_col_name = null;
            bool lb_pass_validation = false;

            ls_col_name = Convert.ToString(dwo.ID);
            switch (ls_col_name)
            {
                case "actual_quantity":
                    lb_pass_validation = await this.ue_validation_check(Convert.ToInt64(data));
                    if (!lb_pass_validation)
                    {
                        return 2;
                    }
                    break;
            }
            return 0;
        }

        public void dw_package_losefocus(object sender, EventArgs e)
		{
			GridElement dw_package = this.GetVisualElementById<GridElement>("dw_package");
			//**********************************************************************************************
			//*Object:								w_packages
			//*Function/Event  Name:			dw_package.losefocus
			//*Purpose:							Accept text 
			//*										
			//*  
			//*Arguments:						None
			//*
			//*Return:								Long.
			//*
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*01-12-2007    Pnina S.G.					0.2.0			B2B 			Initiation
			//************************************************************************************************
			
			dw_package.PerformValidated(e);
		}
	}
}
