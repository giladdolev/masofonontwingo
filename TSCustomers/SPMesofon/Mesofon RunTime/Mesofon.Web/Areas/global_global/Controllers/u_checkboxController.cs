using System.Web.Mvc;
using System.Web.VisualTree.MVC;

using global;
namespace Mesofon.Global.Controllers
{
	public class u_checkboxController : MvcSite.Common.SPBaseController
	{

		public ActionResult u_checkbox()
		{
			return this.View(new u_checkbox());
		}
		private u_checkbox ViewModel
		{
			get { return this.GetRootVisualElement() as u_checkbox; }
		}
	}
}
