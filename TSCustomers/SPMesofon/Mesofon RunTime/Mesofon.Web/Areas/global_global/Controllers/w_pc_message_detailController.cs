using System;
using System.Extensions;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofonAlias = masofon;
using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class w_pc_message_detailController : MvcSite.Common.SPBaseController
	{

        public ActionResult w_pc_message_detail(int error_number)
		{
            return this.View(new w_pc_message_detail(error_number));
		}
		private w_pc_message_detail ViewModel
		{
			get { return this.GetRootVisualElement() as w_pc_message_detail; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void open()
		{
			TextBoxElement sle_1 = this.GetVisualElementById<TextBoxElement>("sle_1");
			TextBoxElement mle_1 = this.GetVisualElementById<TextBoxElement>("mle_1");
			string ls_message = null;
			long ll_skey = 0;
			//.. Set 
			
			ls_message = WindowHelper.GetParam<string>(this.ViewModel);
			
			if (SystemFunctionsExtensions.IsNumber(ls_message))
			{
				ll_skey = Convert.ToInt64(ls_message);
				if (true)
				{
					ViewModel.LoadData(ll_skey, ref ls_message);
				}
				mle_1.Text = ls_message;
				if (true)
				{
                    ViewModel.LoadData1(ll_skey, ref ls_message);
				}
				this.ViewModel.ib_newProperty = false;
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{
					this.ViewModel.ib_newProperty = true;
				}
				else
				{
					sle_1.Text = ls_message;
				}
				sle_1.Focus();
				sle_1.SelectAll();
				this.ViewModel.gs_message_skeyProperty = ll_skey.ToString();
				//	IF sle_1.text ="" THEN
				//		ib_new = TRUE
				//	ELSE
				//		ib_new  = FALSE
				//	END IF		
				//	
			}
		}
		
		public void cb_3_clicked(object sender, EventArgs e)
		{
			TextBoxElement mle_1 = this.GetVisualElementById<TextBoxElement>("mle_1");
			if (true)
			{
				ViewModel.LoadData2();
			}
			mle_1.Text = this.ViewModel.gs_message_textProperty;
		}
		
		public void cb_2_clicked(object sender, EventArgs e)
		{
			((WindowElement)this.ViewModel.Parent).Close();
		}
		
		public void cb_1_clicked(object sender, EventArgs e)
		{
			TextBoxElement sle_1 = this.GetVisualElementById<TextBoxElement>("sle_1");
			if (string.IsNullOrEmpty(sle_1.Text))
			{
				return;
			}
			int li_message_skey = 0;
			int language_skey = 0;
			string ls_id = null;
			string ls_message_title = null;
			DateTime? ld_date = null;
			ld_date = DateTime.Today;
			ls_id = "pcadmin";
			li_message_skey = Convert.ToInt32(this.ViewModel.gs_message_skeyProperty);
			language_skey = 1;
			ls_message_title = "&frametitle";
			if (this.ViewModel.ib_newProperty)
			{
				if (masofonAlias.masofon.Instance.guo_version.uf_get_db_version() < 155)
				{
					ViewModel.UpdateData2(li_message_skey, language_skey, ls_message_title,sle_1.Text, ls_id, ld_date);
				}
				else
				{
					ViewModel.UpdateData3(li_message_skey, language_skey);
				}
			}
			else
			{
				ViewModel.UpdateData4(li_message_skey);
			}
			 
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, Convert.ToString(sle_1.Text));
			//sle_1.text = ""
		}
	}
}
