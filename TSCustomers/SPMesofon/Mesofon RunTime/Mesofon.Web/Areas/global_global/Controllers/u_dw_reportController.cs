using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using masofonAlias = masofon;

using global;
namespace Mesofon.Global.Controllers
{
	public class u_dw_reportController : u_dw_many_rowsController
	{

		public ActionResult u_dw_report()
		{
			return this.View(new u_dw_report());
		}
		private u_dw_report ViewModel
		{
			get { return this.GetRootVisualElement() as u_dw_report; }
		}
	}
}
