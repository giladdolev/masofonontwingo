using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class ds_invpack_deleted_itemsController : MvcSite.Common.SPBaseController
    {
		public ActionResult ds_invpack_deleted_items()
		{
			ds_invpack_deleted_itemsRepository repository = new ds_invpack_deleted_itemsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
