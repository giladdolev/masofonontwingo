using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using masofonAlias = masofon;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
    public class u_checkbox_queryController : u_checkboxController
    {

        public ActionResult u_checkbox_query()
        {
            //TODO consider move to Model CTOR
            this.ViewModel.Visible = (RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Office\\Administration", "show_sql_syntax", "no", "") == "yes");
            if (this.ViewModel.Visible)
            {
                this.ViewModel.BringToFront();
            }

            return this.View(new u_checkbox_query());
        }
        private u_checkbox_query ViewModel
        {
            get { return this.GetRootVisualElement() as u_checkbox_query; }
        }
        public async Task<string> uf_open_debug_sql_window(string as_all_sql_syntax)
        {
            string ls_return = null;
            //.. open debug sql window if checked
            if (this.ViewModel.IsChecked)
            {

                await WindowHelper.Open<w_sql_syntax>("global_global", "sqlSyntax", as_all_sql_syntax);


                if (WindowHelper.GetParam<string>(this.ViewModel) == "Cancled")
                {
                    ls_return = "";
                }
                else
                {
                    
                    ls_return = WindowHelper.GetParam<string>(this.ViewModel);
                }
            }
            return ls_return;
        }

        

     
    }
}
