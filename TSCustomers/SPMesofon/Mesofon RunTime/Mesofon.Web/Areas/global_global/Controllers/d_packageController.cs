using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_packageController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_package()
		{
			d_packageRepository repository = new d_packageRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
