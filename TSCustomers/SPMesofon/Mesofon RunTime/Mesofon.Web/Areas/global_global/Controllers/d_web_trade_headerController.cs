using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_web_trade_headerController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_web_trade_header()
		{
			d_web_trade_headerRepository repository = new d_web_trade_headerRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
