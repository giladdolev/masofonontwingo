using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class lds_station_hardware_parametersController : MvcSite.Common.SPBaseController
	{
		public ActionResult lds_station_hardware_parameters()
		{
			lds_station_hardware_parametersRepository repository = new lds_station_hardware_parametersRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
