using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using masofonAlias = masofon;


using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class rw_diff_report_choose_typeController : MvcSite.Common.SPBaseController
	{

		public ActionResult rw_diff_report_choose_type()
		{
			return this.View(new rw_diff_report_choose_type());
		}
		private rw_diff_report_choose_type ViewModel
		{
			get { return this.GetRootVisualElement() as rw_diff_report_choose_type; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_report_choose_type
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 0;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 0;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			ll_ypos += (global.rw_diff_report_choose_type.il_max_window_height - this.ViewModel.PixelHeight) / 2;
			ll_xpos += (global.rw_diff_report_choose_type.il_max_window_width - this.ViewModel.PixelWidth) / 2;
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
	
		public void open()
		{
			string ls_doc_type = null;
			
		}
		
		public void cb_order_clicked(object sender, EventArgs e)
		{
			WindowHelper.Close(this.ViewModel as WindowElement, "ORDER");
		}
		
		public void cb_inv_pack_clicked(object sender, EventArgs e)
		{
			 
			
			WindowHelper.Close(this.ViewModel as WindowElement, "DOC");
		}
	}
}
