using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using masofonAlias = masofon;

using global;
using System;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
    public class u_dwController : MvcSite.Common.SPBaseController
    {

        public ActionResult u_dw()
        {
            return this.View(new u_dw());
        }

        private u_dw ViewModel
        {
            get { return this.GetRootVisualElement() as u_dw; }
        }

        public void uf_remove_duplicate_rows(string as_by_color_name) //.. Display only distinct rows
        {
            string ls_old_sort = null;

            ls_old_sort = this.ViewModel.GetSort();

            this.ViewModel.SetSort(as_by_color_name + " ASC");

            this.ViewModel.Sort();

            this.ViewModel.SetFilter("NOT (" + as_by_color_name + "[-1] == null) OR " + as_by_color_name + "[-1] <> " +
                                     as_by_color_name + ")");

            this.ViewModel.Filter();
            this.ViewModel.RowsMove(1, this.ViewModel.RowCount(), ModelBuffer.Primary, this.ViewModel,
                 Convert.ToInt32(this.ViewModel.DeletedCount()) + 1, ModelBuffer.Delete);

            this.ViewModel.SetFilter("");

            this.ViewModel.Filter();

            this.ViewModel.SetSort(ls_old_sort);

            this.ViewModel.Sort();
        }

       

        public long
            uf_remove_duplicate_rows(string as_column_name,
                string as_sort) //--------------------------------------------------------------------
        {
            //Function:			public u_dw.uf_remove_duplicate_rows()
            //
            // Returns:         Long 			--> number of duplicate rows
            //
            // Parameters:      value String as_column_name
            // 						value String as_sort	
            // 
            // Copyright  - Stas
            //
            // Date Created: 22/09/2005
            // 
            // Description:	
            // 							Display only distinct rows
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            //.. 
            string ls_old_sort = null;
            long ll_filtered_rows = 0;

            ls_old_sort = this.ViewModel.GetSort();
            if (isempty_stringClass.isempty_string(as_sort))
            {
                as_sort = as_column_name;
            }

            this.ViewModel.SetSort(as_sort);

            this.ViewModel.Sort();

            this.ViewModel.SetFilter("NOT (" + as_column_name + "[-1] == null) OR " + as_column_name + "[-1] <> " +
                                     as_column_name + ")");

            this.ViewModel.Filter();

            ll_filtered_rows = this.ViewModel.RowCount();
            this.ViewModel.RowsMove(1, (int) ll_filtered_rows, ModelBuffer.Primary, this.ViewModel,
                (int)this.ViewModel.DeletedCount() + 1, ModelBuffer.Delete);

            this.ViewModel.SetFilter("");

            this.ViewModel.Filter();

            this.ViewModel.SetSort(ls_old_sort);

            this.ViewModel.Sort();
            return ll_filtered_rows;
        }

      

        public void ue_enter()
        {
        }

        public void clicked(int xpos, int ypos, int row, object dwo)
        {
        }
    }

}