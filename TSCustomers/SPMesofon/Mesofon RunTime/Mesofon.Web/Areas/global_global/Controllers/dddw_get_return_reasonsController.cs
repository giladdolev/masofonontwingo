using System.Web.Mvc;
using Mesofon.Repository;
using MvcSite.Common;

namespace Mesofon.Global.Controllers
{
	public class dddw_get_return_reasonsController : MvcSite.Common.SPBaseController
	{
		public ActionResult dddw_get_return_reasons()
		{
			dddw_get_return_reasonsRepository repository = new dddw_get_return_reasonsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
