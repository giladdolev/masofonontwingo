using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Extensions;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;

using global;
using Mesofon.Common;
using Mesofon.Models;
using MvcSite.Common;

namespace Mesofon.Global.Controllers
{
	public class rw_diff_order_mt_reportController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_diff_order_mt_report(string ls_params, string FormType)
		{
            return this.View(new rw_diff_order_mt_report(ls_params, FormType));
		}
		private rw_diff_order_mt_report ViewModel
		{
			get { return this.GetRootVisualElement() as rw_diff_order_mt_report; }
		}
        public void Form_Load(object sender, EventArgs e)
        {

            GridElement dw_diff_order_mt_report = this.ViewModel.GetVisualElementById<GridElement>("dw_diff_order_mt_report");
            dw_diff_order_mt_report.SetRepository(new d_diff_order_mt_reportRepository());
            if (this.ViewModel.FormType == "base_body")
            {
                dw_diff_order_mt_report.setVisibleProperty("order_number_order_quantity", false);
                dw_diff_order_mt_report.setVisibleProperty("order_quantity", false);
                dw_diff_order_mt_report.setVisibleProperty("order_quantity_t", true);
            }
            else
            {
                //Marlog
                dw_diff_order_mt_report.setVisibleProperty("order_number_order_quantity", true);
                dw_diff_order_mt_report.setVisibleProperty("order_quantity", false);
                dw_diff_order_mt_report.setVisibleProperty("order_quantity_t", false);

            }
            
            open();
        }
        public void ue_print()
		{
		    IRepository lds_print;
			lds_print = new d_diff_order_mt_report_printRepository();
			
			
			 
			lds_print.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_order_numberProperty);
			
			lds_print.Print(Globals.DeviceID, Globals.UserID, Globals.Password, MvcSite.Common.Globals.PrinterName);
            this.ViewModel.Close();
		}
		public void ue_print_for_shipment() //**********************************************************************************************
		{
			//*Object:				rw_diff_order_mt_report
			//*Function Name:	ue_retrieve_for_shipment
			//*Purpose: 			Print rows
			//*Arguments: 		None
			//*Return:				Integer	0/-1
			//*Date				Programer		Version	Task#	 			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*20-12-2010		AlexKh			1.2.45.5	SPUCM00002425	Initial version
			//************************************************************************************************
			
			long ll_i = 0;
			long ll_supplier_number = 0;
			long ll_order_number = 0;
			long ll_ret = 0;
			string ls_ret = null;
		    IRepository lds_print;
            d_diff_order_shipment_report_printRepository lds_print_all;

            lds_print = new d_diff_order_shipment_report_printRepository();
            lds_print_all = new d_diff_order_shipment_report_printRepository();
			
			
			for (ll_i = 0; ll_i < this.ViewModel.ids_shipment_ordersProperty.RowCount(); ll_i++)
			{
				
				ll_supplier_number = this.ViewModel.ids_shipment_ordersProperty.GetItemValue<long>((int)ll_i, "supplier_number");
				
				ll_order_number = (long)this.ViewModel.ids_shipment_ordersProperty.GetItemValue<double>((int)ll_i, "order_number");
				 
				ll_ret = lds_print.Retrieve(this.ViewModel.il_branch_numberProperty, ll_supplier_number, ll_order_number);
				if (ll_ret > 0)
				{
					ll_ret = lds_print.RowsMove(0, lds_print.RowCount(), ModelBuffer.Primary, (lds_print_all), lds_print_all.RowCount() + 1, ModelBuffer.Primary);
				}
			}

            int row = 1;
            foreach(d_diff_order_shipment_report_print item in lds_print_all.GetPrimaryList())
            {
                item.t_shipment_number = this.ViewModel.il_shipment_numberProperty.ToString();
                item.row = row.ToString();
                row++;
            }
			
			lds_print_all.Print(Globals.DeviceID, Globals.UserID, Globals.Password, MvcSite.Common.Globals.PrinterName);


            this.ViewModel.Close();
		}
		public int wf_get_data()
		{
            string ls_params = null;
			string ls_branch_number = null;
			string ls_supplier_number = null;
			string ls_order_number = null;
			// Get recived message
			
			ls_params = this.ViewModel.Params;
			ls_branch_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_supplier_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_order_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			// Check if empty or Null or 0
			if (isempty_stringClass.isempty_string(ls_branch_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_supplier_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_order_number))
			{
				return -1;
			}
			// Check if not a number
			if (!SystemFunctionsExtensions.IsNumber(ls_branch_number))
			{
				return -1;
			}
			if (!SystemFunctionsExtensions.IsNumber(ls_supplier_number))
			{
				return -1;
			}
			if (!SystemFunctionsExtensions.IsNumber(ls_order_number))
			{
				return -1;
			}
			// Set data into the instance variables
			this.ViewModel.il_branch_numberProperty = Convert.ToInt64(ls_branch_number);
			// AlexKh - 1.2.46.5 - 2010-12-20 - SPUCM00002425 - check if working in shipment mode
			if (ls_order_number == "-1")
			{
				this.ViewModel.ib_shipment_modeProperty = true;
				this.ViewModel.il_shipment_numberProperty = Convert.ToInt64(ls_supplier_number);
			}
			else
			{
				this.ViewModel.il_supplier_numberProperty = Convert.ToInt64(ls_supplier_number);
				this.ViewModel.il_order_numberProperty = Convert.ToInt64(ls_order_number);
			}
			return 1;
		}
		public int wf_retrieve()
		{
            GridElement dw_diff_order_mt_report = this.GetVisualElementById<GridElement>("dw_diff_order_mt_report");
			int li_ret = 0;
			li_ret = (int) dw_diff_order_mt_report.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_order_numberProperty);
			return li_ret;
		}
		public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_order_mt_report
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 0;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 0;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			ll_ypos += (global.rw_diff_order_mt_report.il_max_window_height - this.ViewModel.PixelHeight) / 2;
			ll_xpos += (global.rw_diff_order_mt_report.il_max_window_width - this.ViewModel.PixelWidth) / 2;
			int ll_ret = 0;
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
		public int wf_retrieve_for_shipment() //**********************************************************************************************
		{
			GridElement dw_diff_order_mt_report = this.GetVisualElementById<GridElement>("dw_diff_order_mt_report");
			//*Object:				rw_diff_order_mt_report
			//*Function Name:	wf_retrieve_for_shipment
			//*Purpose: 			Retrieve for every order in shipment the items that where ordered and not supplied
			//*Arguments: 		None
			//*Return:				Integer	1/-1
			//*Date				Programer		Version	Task#	 			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*20-12-2010		AlexKh			1.2.45.5	SPUCM00002425	Initial version
			//************************************************************************************************
			
			int ll_ret = 0;
			int ll_i = 0;
			long ll_order_number = 0;
			long ll_supplier_number = 0;
			string ls_ret = null;
			IRepository lds_shipment_orders = null;
			IRepository lds_diff_order_mt_report;
			this.ViewModel.ids_shipment_ordersProperty = new d_shipment_ordersRepository();
			lds_shipment_orders = new d_shipment_ordersRepository();
			lds_diff_order_mt_report = new d_diff_order_mt_reportRepository();
			ll_ret = (int) lds_shipment_orders.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_shipment_numberProperty);
			if (ll_ret <= 0)
			{
				return -1;
			}
			ll_ret = lds_shipment_orders.RowsCopy(0, lds_shipment_orders.RowCount(), ModelBuffer.Primary, this.ViewModel.ids_shipment_ordersProperty, 1, ModelBuffer.Primary);
            var difforderRepository = new d_diff_order_mt_reportRepository();
            for (ll_i = 0; ll_i < lds_shipment_orders.RowCount(); ll_i++)
			{
				
				ll_supplier_number = lds_shipment_orders.GetItemValue<long>(ll_i, "supplier_number");
				
				ll_order_number = (long)lds_shipment_orders.GetItemValue<double>(ll_i, "order_number");
				 
				ll_ret = (int) lds_diff_order_mt_report.Retrieve(this.ViewModel.il_branch_numberProperty, ll_supplier_number, ll_order_number);
				if (ll_ret > 0)
				{
                    ll_ret = lds_diff_order_mt_report.RowsMove(0, lds_diff_order_mt_report.RowCount(), ModelBuffer.Primary, difforderRepository, difforderRepository.RowCount() + 1, ModelBuffer.Primary);
                }
            }
            dw_diff_order_mt_report.SetRepository(difforderRepository);
            //ls_ret = dw_diff_order_mt_report.Modify("order_number","visible", "TRUE");

            //ls_ret = dw_diff_order_mt_report.Modify("material_quantity_t","text", "הזמנה וכמות");


            return 1;
		}
	
		public void open() // Get message data
		{
			if (this.wf_get_data() == -1)
			{
				
				WindowHelper.Close(this.ViewModel, (-1).ToString());
				return;
			}
			// AlexKh - 1.2.46.5 - 2010-12-20 - SPUCM00002425 - check if working in shipment mode
			if (this.ViewModel.ib_shipment_modeProperty)
			{
				if (wf_retrieve_for_shipment() < 1)
				{
					
					WindowHelper.Close(this.ViewModel, (-1).ToString());
					return;
				}
			}
			else
			{
				// Retrieve data
				if (this.wf_retrieve() < 1)
				{
					
					WindowHelper.Close(this.ViewModel, (-1).ToString());
					return;
				}
			}
			//THIS.wf_set_window_position()
			
		     
		}
		
		public void cb_print_clicked(object sender, EventArgs e)
		{
			if (this.ViewModel.ib_shipment_modeProperty)
			{
				this.ue_print_for_shipment();
			}
			else
			{
				this.ue_print();
			}
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			 
			
			((WindowElement)this.ViewModel).Close();
		}
		
        public int dw_diff_order_mt_report_dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row)
		{
			return 1;
		}
	}
}
