using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_crossdoc_xmlController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_crossdoc_xml()
		{
			d_crossdoc_xmlRepository repository = new d_crossdoc_xmlRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
