using System.Web.Mvc;
using System.Web.VisualTree.MVC;

using global;
using System;

namespace Mesofon.Global.Controllers
{
	public class u_cb_closewindowController : u_cbController
	{

		public ActionResult u_cb_closewindow()
		{
			return this.View(new u_cb_closewindow());
		}
		private u_cb_closewindow ViewModel
		{
			get { return this.GetRootVisualElement() as u_cb_closewindow; }
		}
		public void Close_MouseClick(object sender, EventArgs e)
        {
			base.Close_MouseClick(sender, e);
            ViewModel.Parent.Hide();
		}
	}
}
