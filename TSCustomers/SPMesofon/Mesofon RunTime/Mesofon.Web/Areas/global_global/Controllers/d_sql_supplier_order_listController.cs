using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_sql_supplier_order_listController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_sql_supplier_order_list()
		{
			d_sql_supplier_order_listRepository repository = new d_sql_supplier_order_listRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
