using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_b2b_branches_suppliersController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_b2b_branches_suppliers()
		{
			d_b2b_branches_suppliersRepository repository = new d_b2b_branches_suppliersRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
