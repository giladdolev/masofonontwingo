using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class ds_suppliers_distributorsController : MvcSite.Common.SPBaseController
	{
		public ActionResult ds_suppliers_distributors()
		{
			ds_suppliers_distributorsRepository repository = new ds_suppliers_distributorsRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
