using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Web.VisualTree.Extensions;
using masofonAlias = masofon;
using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;
using global;
using Mesofon.Common;
using System.Collections.Generic;
using MvcSite.Common;

namespace Mesofon.Global.Controllers
{
	public class rw_choose_marlogController : Controller
	{
        
        //gilad test
        public void  select_all()
        {
           
        }
        public ActionResult rw_choose_marlog(string crossDoc)
		{
            return this.View(new rw_choose_marlog(crossDoc));
		}
		private rw_choose_marlog ViewModel
		{
			get { return this.GetRootVisualElement() as rw_choose_marlog; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }

        public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_report_choose_type
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 0;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 0;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			ll_ypos += (global.rw_choose_marlog.il_max_window_height - this.ViewModel.PixelHeight) / 2;
			ll_xpos += (global.rw_choose_marlog.il_max_window_width - this.ViewModel.PixelWidth) / 2;
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
		//gilad removed

		public bool wf_remove_distributor_column() //********************************************************************
		{
            GridElement dw_list = this.GetVisualElementById<GridElement>("dw_list");
			//*Object:				rw_choose_marlog
			//*Function Name:	wf_remove_distributor_column
			//*Purpose: 			remove distributor column from display
			//*Arguments: 		None
			//*Return:				TRUE/FALSE
			//*Date					Programer		Version	Task#	 			Description
			//*---------------------------------------------------------------------
			//*17-02-2015		AlexKh			1.1.28.0	SPUCM00005224	Initial version
			//********************************************************************
			

            dw_list.Columns["distributor_number"].Visible = false;
            //dw_list.Columns["distributor_number_t"].Visible = false;

			return true;
		}
		public void open()
		{
			GridElement dw_list = this.GetVisualElementById<GridElement>("dw_list");
			string ls_doc_type = null;
			string ls_called_from = null;
			//THIS.move(120,350)
			
			ls_called_from = this.ViewModel.CrossDoc.ToUpper(); //RonY@11/03/2015 1.12.49.12 - SPUCM00005247
			// AlexKh - 1.1.28.0 - 2015-02-17 - SPUCM00005224 - remove distributor column from display when crossdoc
			
			if (masofonAlias.masofon.Instance.gds_marlog_distributor.RowCount() > 0)
			{
                

                dw_list.SetRepository(masofonAlias.masofon.Instance.gds_marlog_distributor);
                dw_list.Columns["branch_number"].Width = 140;
                dw_list.Rows[0].Selected = true;
                dw_list.ReadOnly = true;

                if (ls_called_from == "TRUE" || ls_called_from == "CROSSDOC_RETURN")
                {
                    
                    wf_remove_distributor_column();
                }
            }
		}
		
		 
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
		   WindowHelper.Close(this.ViewModel as WindowElement, -1);
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
            var dw_list = this.GetVisualElementById<GridElement>("dw_list");
            //gilad test get the checkbox state
           //  var dw_chk = this.GetVisualElementById<CheckBoxElement>("giladtest");
          
          //  Globals.giladtestCheckBox = dw_chk.CheckState.ToString();

            int li_selected_row = 0;
			int li_ret_row = 0;
			
			li_selected_row = dw_list.SelectedRowIndex;
			if (li_selected_row >= 0)
			{
				li_ret_row = li_selected_row;
			}
			else
			{
				li_ret_row = -1;
			}
			 
			
			WindowHelper.Close(this.ViewModel as WindowElement, li_ret_row);
		}
	}
}
