using System.Web.Mvc;
using System.Web.VisualTree.MVC;

using global;
namespace Mesofon.Global.Controllers
{
	public class u_cb_okController : u_cbController
	{

		public ActionResult u_cb_ok()
		{
			return this.View(new u_cb_ok());
		}
		private u_cb_ok ViewModel
		{
			get { return this.GetRootVisualElement() as u_cb_ok; }
		}
	}
}
