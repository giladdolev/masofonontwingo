using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_diff_pallet_inv_mt_reportController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_diff_pallet_inv_mt_report()
		{
			d_diff_pallet_inv_mt_reportRepository repository = new d_diff_pallet_inv_mt_reportRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
