using System.Web.Mvc;
using Mesofon.Repository;
using MvcSite.Common;

namespace Mesofon.Global.Controllers
{
	public class ds_branch_station_paramsController : MvcSite.Common.SPBaseController
	{
		public ActionResult ds_branch_station_params()
		{
            ds_branch_station_paramsRepository repository = new ds_branch_station_paramsRepository();
            string[] param = { Globals.BranchNumberState, Globals.StationNumberState };
            repository.Retrieve(param);
			return this.View(repository);
		}
	}
}
