using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_diff_inv_mt_report_printController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_diff_inv_mt_report_print()
		{
			d_diff_inv_mt_report_printRepository repository = new d_diff_inv_mt_report_printRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
