using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using masofonAlias = masofon;


using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class rw_mini_terminal_print_diffController : MvcSite.Common.SPBaseController
	{

		public ActionResult rw_mini_terminal_print_diff()
		{
			return this.View(new rw_mini_terminal_print_diff());
		}
		private rw_mini_terminal_print_diff ViewModel
		{
			get { return this.GetRootVisualElement() as rw_mini_terminal_print_diff; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_mini_terminal_print_diff
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 65;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 300;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			//ll_ypos += (il_max_window_height - THIS.Height) / 2
			//ll_xpos += (il_max_window_width - THIS.Width) / 2
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
            root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
	
		public void open()
		{
			NumericUpDownElement em_copies = this.GetVisualElementById<NumericUpDownElement>("em_copies");
			em_copies.Value = 1;
			//wf_resize();
			//this.wf_set_window_position();
		}
        
        public void close() // SharonS - 1.2.36.0 -> 1.2.37.8 - 2008-09-17- Task#10012 - SPUCM00000094
		{
			// SharonS - 1.2.37.13 - 2008-12-18- Task#10012 - SPUCM00000094
			//CloseWithReturn(THIS, "CANCEL")
			if (string.IsNullOrEmpty(this.ViewModel.is_copiesProperty))
			{
				
				WindowHelper.Close(this.ViewModel, "CANCEL");
			}
			// End
			// End
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
            WindowHelper.Close(this.ViewModel, "CANCEL"); 
        }
		
		public void cb_print_clicked(object sender, EventArgs e) // SharonS - 1.2.37.13 - 2008-12-18- Task#10012 - SPUCM00000094
		{
			NumericUpDownElement em_copies = this.GetVisualElementById<NumericUpDownElement>("em_copies");
            //String		ls_copies
            //ls_copies = em_copies.Text
            //CloseWithReturn(PARENT, ls_copies)
            this.ViewModel.is_copiesProperty = em_copies.Value.ToString();


            WindowHelper.Close(this.ViewModel, Convert.ToString(this.ViewModel.is_copiesProperty));
            // End
        }
	}
}
