using System.Web.Mvc;
using System.Web.VisualTree.MVC;

using global;
using System;

namespace Mesofon.Global.Controllers
{
	public class u_cbController : MvcSite.Common.SPBaseController
	{

		public ActionResult u_cb()
		{
			return this.View(new u_cb());
		}
		private u_cb ViewModel
		{
			get { return this.GetRootVisualElement() as u_cb; }
		}
		public void uf_set_close_with_return(bool ab_return_flag)
		{
			this.ViewModel.ib_return_flagProperty = ab_return_flag;
		}
		public bool uf_get_close_with_return()
		{
			return this.ViewModel.ib_return_flagProperty;
		}
		public long uf_get_return_value()
		{
			return this.ViewModel.il_return_valueProperty;
		}
		
        public void Close_MouseClick(object sender, EventArgs e)
        {
        }
    }
}
