using System.Web.Mvc;
using Mesofon.Repository;

namespace Mesofon.Global.Controllers
{
    public class d_addhock_supplier_distributorsController : MvcSite.Common.SPBaseController
    {
        public ActionResult d_addhock_supplier_distributors()
        {
            d_addhock_supplier_distributorsRepository repository = new d_addhock_supplier_distributorsRepository();
            repository.Retrieve();
            return this.View(repository);
        }
    }
}
