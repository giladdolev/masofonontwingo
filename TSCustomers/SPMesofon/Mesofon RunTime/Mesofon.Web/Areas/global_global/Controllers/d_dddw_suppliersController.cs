using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_dddw_suppliersController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_dddw_suppliers()
		{
			d_dddw_suppliersRepository repository = new d_dddw_suppliersRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
