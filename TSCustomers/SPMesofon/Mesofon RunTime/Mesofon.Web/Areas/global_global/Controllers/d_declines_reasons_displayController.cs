using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_declines_reasons_displayController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_declines_reasons_display()
		{
			d_declines_reasons_displayRepository repository = new d_declines_reasons_displayRepository();
			
			return this.View(repository);
		}
	}
}
