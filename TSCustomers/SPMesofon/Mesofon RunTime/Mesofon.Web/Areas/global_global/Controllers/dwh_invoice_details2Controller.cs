using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class dwh_invoice_details2Controller : MvcSite.Common.SPBaseController
	{
		public ActionResult dwh_invoice_details2()
		{
			dwh_invoice_details2Repository repository = new dwh_invoice_details2Repository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
