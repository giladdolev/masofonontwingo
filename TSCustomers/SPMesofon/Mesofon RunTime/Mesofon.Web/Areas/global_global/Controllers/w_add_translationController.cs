using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Extensions;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree;
using masofon;
using masofonAlias = masofon;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class w_add_translationController : MvcSite.Common.SPBaseController
	{

        public ActionResult w_add_translation(str_pass pass)
		{
            return this.View(new w_add_translation(pass));
		}
		private w_add_translation ViewModel
		{
			get { return this.GetRootVisualElement() as w_add_translation; }
		}
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public async Task open()
		{
			TextBoxElement sle = this.GetVisualElementById<TextBoxElement>("sle");
			LabelElement st_number = this.GetVisualElementById<LabelElement>("st_number");
			TextBoxElement st = this.GetVisualElementById<TextBoxElement>("st");
			string ls_message = null;
			str_pass lstr_pass = default(str_pass);
			//..	str_pass:		s[1]	=	expression text
			//..						l[1]	=	expression number
			//..						b[1]	=	true if expression already exists in DB
			
			lstr_pass = (str_pass)WindowHelper.GetParam<object>(this.ViewModel);
			if (CollectionExtensions.UBound(lstr_pass.s) != 1 || (CollectionExtensions.UBound(lstr_pass.l) != 1 || CollectionExtensions.UBound(lstr_pass.b) != 1))
			{
				//.. error
			await	masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.open()", "", "stopsign!", "Message Object Array Error!");
				this.ViewModel.Close();
				return;
			}
			ls_message = lstr_pass.s[1].Trim();
			if (isempty_stringClass.isempty_string(ls_message))
			{
				//.. error
		        await masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.open()", "", "stopsign!", "Expression text is Empty!");
				this.ViewModel.Close();
				return;
			}
			if (lstr_pass.b[1])
			{
				ViewModel.LoadData();
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{
					
				await	masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.open()", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
					this.ViewModel.Close();
					return;
				}
				this.ViewModel.ib_newProperty = true;
			}
			else
			{
				this.ViewModel.il_new_expression_numberProperty = lstr_pass.l[1];
			}
			//.. Set 
			st.Text = ls_message;
			st_number.Text = this.ViewModel.il_new_expression_numberProperty.ToString();
			sle.Font = new Font(sle.Font.Name, sle.Font.Size, sle.Font.Style, GraphicsUnit.Point, masofonAlias.masofon.Instance.nvo_translator.ist_charset.Font.GdiCharSet);
			masofonAlias.masofon.Instance.nvo_translator.fnv_kbd_layout();
			if (masofonAlias.masofon.Instance.nvo_translator.direction == "right")
			{
				sle.RightToLeft = RightToLeft.Yes;
			}
		 	await f_commitClass.f_commit();
			//f_center_window(this)
			sle.Focus();
            sle.SelectAll();
		}
		
		public void cb_2_clicked(object sender, EventArgs e)
		{
			TextBoxElement st = this.GetVisualElementById<TextBoxElement>("st");
			 
			
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, Convert.ToString(st.Text));
		}
		
		public async Task cb_1_clicked(object sender, EventArgs e)
		{
			TextBoxElement st = this.GetVisualElementById<TextBoxElement>("st");
			TextBoxElement sle = this.GetVisualElementById<TextBoxElement>("sle");
			int li_language_number = 0;
			string ls_translation = null;
			string ls_message = null;
			ls_translation = sle.Text;
			if (isempty_stringClass.isempty_string(ls_translation))
			{
				//.. error
	        	await masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.Clicked()", "", "stopsign!", "No Translation Entered!");
				return;
			}
			li_language_number = masofonAlias.masofon.Instance.nvo_translator.il_current_lang;
			if (this.ViewModel.ib_newProperty)
			{
				ls_message = st.Text;
				ViewModel.UpdateData(ls_message);
				
				if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
				{
					
			    await masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.Clicked()", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
					return;
				}
				ViewModel.UpdateData1(li_language_number, ls_translation);
			}
			else
			{
				ViewModel.UpdateData2(li_language_number, ls_translation);
				ViewModel.UpdateData3(ls_translation, li_language_number);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
			{
				
				await MessageBox.Show(masofonAlias.masofon.Instance.sqlca.SqlErrText, "w_add_translation.cb.Clicked()");
				
			    await masofonAlias.masofon.Instance.guo_msg.uf_msg("w_add_translation.Clicked()", "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
			}
			 
			
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, Convert.ToString(ls_translation));
		}
	}
}
