using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.VisualTree;
using System.Web.VisualTree.Extensions;
using masofonAlias = masofon;


using global;
using Mesofon.Common;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Global.Controllers
{
	public class w_get_item_from_barcodeController : MvcSite.Common.SPBaseController
	{

        public ActionResult w_get_item_from_barcode(s_array_arguments parms)
		{
            return this.View(new w_get_item_from_barcode(parms));
		}
		private w_get_item_from_barcode ViewModel
		{
			get { return this.GetRootVisualElement() as w_get_item_from_barcode; }
		}
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public void ue_exit() // ue_exit()
		{
			GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
			long ll_Return_Value = 0;
			long ll_row_count = 0;
			
			ll_row_count = dw_codes.RowCount();
			
			if (dw_codes.RowCount() < 1)
			{
				return;
			}
			if (this.ViewModel.wl_Last_Row_SelectedProperty > ll_row_count)
			{
				
				WindowHelper.Close(this.ViewModel, ll_Return_Value.ToString());
				return;
			}
			if (this.ViewModel.wl_Last_Row_SelectedProperty > 0)
			{
				
				ll_Return_Value = dw_codes.GetItemValue<long>(this.ViewModel.wl_Last_Row_SelectedProperty, "item_number");
			}
			else
			{
				ll_Return_Value = 0;
			}
			
			WindowHelper.Close(this.ViewModel, ll_Return_Value.ToString());
		}
        public async Task ue_post_open()
        {
            CheckBoxElement cbx_supplier = this.GetVisualElementById<CheckBoxElement>("cbx_supplier");
            GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
            WindowElement window = this.GetVisualElementById<WindowElement>("w_get_item_from_barcode");
            f_center_windowClass.f_center_window(window);



            await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel);
            await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_codes);
            this.ViewModel.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(this.ViewModel.Text);


            cbx_supplier.Visible = (!(this.ViewModel.il_supplier_numberProperty == 0)) && (this.ViewModel.il_supplier_numberProperty > 0);
        }
        public void ue_retrieve() // ue_retrieve()
		{
			LabelElement st_count = this.GetVisualElementById<LabelElement>("st_count");
			CheckBoxElement cbx_supplier = this.GetVisualElementById<CheckBoxElement>("cbx_supplier");
			GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
			long ll_rows = 0;
			long ll_Return_Value = 0;
			
			
			dw_codes.Visible = false;
			if (cbx_supplier.IsChecked)
			{
                // TODO: Missing DataWindow "dw_material_code_by_supplier"
				//dw_codes.SetDataObject("dw_material_code_by_supplier");
				ll_rows = dw_codes.Retrieve(this.ViewModel.il_supplier_numberProperty, this.ViewModel.is_barcodeProperty);
			}
			else
			{
				dw_codes.SetDataObject(this.ViewModel.is_dataobjectProperty);
				ll_rows = dw_codes.Retrieve(this.ViewModel.is_barcodeProperty);
			}
			if (ll_rows > 0)
			{
				
				dw_codes.SelectRow(0, false);
				
				dw_codes.SelectRow(1, true);
				 
				dw_codes.ScrollToRow(1);
				this.ViewModel.wl_Last_Row_SelectedProperty = 1;
				st_count.Text = ll_rows.ToString();
			}
			dw_codes.Visible = true;
			
			
		}
		
        public async Task open()
        {
            LabelElement st_count = this.GetVisualElementById<LabelElement>("st_count");
            GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
            LabelElement st_esa_live = this.GetVisualElementById<LabelElement>("st_esa_live");
            LabelElement st_datetime_update = this.GetVisualElementById<LabelElement>("st_datetime_update");
            LabelElement st_run_datetime = this.GetVisualElementById<LabelElement>("st_run_datetime");
            ButtonElement cb_cancel = this.GetVisualElementById<ButtonElement>("cb_cancel");
            long ll_pos = 0;
            long ll_Return_Value = 0;
            long ll_rows = 0;
            s_array_arguments lstr_parms = default(s_array_arguments);

            lstr_parms = (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
            this.ViewModel.is_barcodeProperty = lstr_parms.a_string[1];
            this.ViewModel.il_supplier_numberProperty = lstr_parms.a_long[1];
            ll_pos = this.ViewModel.is_barcodeProperty.IndexOf("@");
            if (ll_pos > 0)
            {
                this.ViewModel.is_barcodeProperty = this.ViewModel.is_barcodeProperty.Substring(1 - 1, (int)(ll_pos - 1));
                // TODO: Missing  DataWindow "dw_material_code"
                //	this.ViewModel.is_dataobjectProperty = "dw_material_code";
            }
            else
            {
                this.ViewModel.is_dataobjectProperty = new dw_item_codeRepository();
            }
            //  Tsvika 11/7/06
            if (isempty_stringClass.isempty_string(this.ViewModel.is_barcodeProperty))
            {

                WindowHelper.Close(this.ViewModel, (-1).ToString());
                return;
            }
            //
            this.ViewModel.istct_reports_dbProperty = await masofonAlias.masofon.Instance.gnvo_reports_db.uf_get_db_transaction("get_item_from_barcode", masofonAlias.masofon.Instance.gs_vars.esa_connect_item_search, false);
            if (this.ViewModel.istct_reports_dbProperty.status == "NON")
            {

                cb_cancel.PerformClick(null);
                return;
            }
            st_run_datetime.Text = this.ViewModel.istct_reports_dbProperty.run_datetime.ToString();
            st_datetime_update.Text = this.ViewModel.istct_reports_dbProperty.update_datetime.ToString();
            masofonAlias.masofon.Instance.SQLCA_REPORTS = this.ViewModel.istct_reports_dbProperty.report_tran;

            st_esa_live.Text = masofonAlias.masofon.Instance.SQLCA_REPORTS.ServerName.ToString();

            dw_codes.SetDataObject(this.ViewModel.is_dataobjectProperty);



            ll_rows = dw_codes.Retrieve(this.ViewModel.is_barcodeProperty);
            if (ll_rows < 0)
            {

                WindowHelper.Close(this.ViewModel, (-1).ToString());
                return;
            }
            else if (ll_rows == 0)
            {

                WindowHelper.Close(this.ViewModel, "0");
                return;
            }
            else if (ll_rows == 1)
            {

                ll_Return_Value = dw_codes.GetItemValue<long>(0, "item_number");

                WindowHelper.Close(this.ViewModel, ll_Return_Value.ToString());
                return;
            }
            if (ll_rows > 0)
            {

                dw_codes.SelectRow(0, false);

                dw_codes.SelectRow(1, true);

                dw_codes.ScrollToRow(1);
                this.ViewModel.wl_Last_Row_SelectedProperty = 1;
                st_count.Text = ll_rows.ToString();
            }
            //wf_resize();
            await this.ue_post_open();
        }
        public async Task activate()
        {
            masofonAlias.masofon.Instance.help_entry = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("בחירת קוד משני");
        }
        public void key(object sender, KeyDownEventArgs e)
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
			long ll_row = 0;
			long ll_row_count = 0;
			
			ll_row_count = dw_codes.RowCount();
			if (ll_row_count > 0)
			{
				 
				ll_row = dw_codes.GetSelectedRow(0);
				if (key == Keys.Down)
				{
					if (ll_row < ll_row_count)
					{
						
						ll_row++;
						
						dw_codes.SelectRow(0, false);
						
						dw_codes.SelectRow((int)ll_row, true);
						 
						dw_codes.ScrollToRow((int)ll_row);
						this.ViewModel.wl_Last_Row_SelectedProperty = ll_row;
					}
				}
				else if (key == Keys.Up)
				{
					if (ll_row > 1)
					{
						 
						ll_row++;
						
						dw_codes.SelectRow(0, false);
						
						dw_codes.SelectRow((int)ll_row, true);
						 
						dw_codes.ScrollToRow((int)ll_row);
						this.ViewModel.wl_Last_Row_SelectedProperty = ll_row;
					}
				}
			}
		}
		
		public void cbx_supplier_clicked(object sender, EventArgs e)
		{
			ue_retrieve();
		}
        public void select_All(object sender, EventArgs e)
        {
           // ue_retrieve();
        }
        public void dw_codes_clicked(int xpos, int ypos, int row, ControlElement dwo)
		{
			GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
			f_dw_set_sortClass.f_dw_set_sort(dw_codes);
			if (row > 0)
			{
				
				dw_codes.SelectRow((int)this.ViewModel.wl_Last_Row_SelectedProperty, false);
				
				dw_codes.SelectRow(row, true);
				this.ViewModel.wl_Last_Row_SelectedProperty = row;
			}
		}
		
		public void dw_codes_doubleclicked(int xpos, int ypos, int row, ControlElement dwo)
		{
			GridElement dw_codes = this.GetVisualElementById<GridElement>("dw_codes");
			if (row > 0)
			{
				
				dw_codes.SelectRow((int)this.ViewModel.wl_Last_Row_SelectedProperty, false);
				
				dw_codes.SelectRow(row, true);
				this.ViewModel.wl_Last_Row_SelectedProperty = row;
				this.ue_exit();
			}
		}
		
		public void cb_cancel_clicked(object sender, EventArgs e)
		{
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, (-4).ToString());
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			this.ue_exit();
		}
	}
}
