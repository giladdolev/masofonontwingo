using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class lds_global_branch_parametersController : MvcSite.Common.SPBaseController
	{
		public ActionResult lds_global_branch_parameters()
		{
			lds_global_branch_parametersRepository repository = new lds_global_branch_parametersRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
