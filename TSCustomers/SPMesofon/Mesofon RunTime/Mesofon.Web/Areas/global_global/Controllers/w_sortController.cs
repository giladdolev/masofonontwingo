using System;
using System.Extensions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree;
using masofonAlias = masofon;
using System.Web.VisualTree.Extensions;


using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class w_sortController : Controller
	{

		public ActionResult w_sort()
		{
			return this.View(new w_sort());
		}
		private w_sort ViewModel
		{
			get { return this.GetRootVisualElement() as w_sort; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
           
        }
        public async Task open()
        {
            string ls_temp = null;
            ButtonElement pb_select = this.GetVisualElementById<ButtonElement>("pb_select");
            ButtonElement pb_clear = this.GetVisualElementById<ButtonElement>("pb_clear");
            ButtonElement pb_ok = this.GetVisualElementById<ButtonElement>("pb_ok");
            ButtonElement pb_cancel = this.GetVisualElementById<ButtonElement>("pb_cancel");
            GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
            GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
            string[] ls_cols = null;
            string ls_label = null;
            string resp = null;
            string col = null;
            string vis = null;
            string ls_col_name = null;
            string ls_import_str = null;
            int li_num_cols = 0;
            int? li_cnt = 0;
            long ll = 0;
            str_sort parm = default(str_sort);


            if (WindowHelper.GetParam<object>(this.ViewModel) == null || !(WindowHelper.GetParam<object>(this.ViewModel) != null))
            {
                this.ViewModel.Close();
                return;
            }

            else if (WindowHelper.GetParam<object>(this.ViewModel).GetType().Name != "str_sort")
            {
                this.ViewModel.Close();
                return;
            }

            parm = (str_sort)WindowHelper.GetParam<object>(this.ViewModel);
            this.ViewModel.idw_dwProperty = parm.dw;
            this.ViewModel.is_titleProperty = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(parm.title);
            this.ViewModel.Text = " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("מיון").Result + " " + masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(parm.title);
            li_num_cols = f_dw_get_objects_attribClass.f_dw_get_objects_attrib(this.ViewModel.idw_dwProperty, ref ls_cols, "column", "*", "visible");
            li_cnt = 1;
            for (ll = 1; ll <= li_num_cols; ll++)
            {
                //.. get the column name
                ls_col_name = f_get_tokenClass.f_get_token(ref ls_cols[ll], "\n");
                //.. is it visible
                vis = f_get_tokenClass.f_get_token(ref ls_cols[ll], "\n");
                //.. if not visible then do not allow sorting on it
                ls_temp = null;
                if (vis == "1")
                {

                    ls_label = this.ViewModel.idw_dwProperty.Describe(ls_col_name + "_t", "text");
                    if (ls_label == "!") // label not found to use the column name
                    {
                        ls_temp = "evaluate ('wordcap (\"" + ls_col_name + "\")', 1)"; // make it pretty

                        ls_label = this.ViewModel.idw_dwProperty.Describe(ls_temp);
                    }
                    else
                    {
                        if (ls_label.IndexOf("\r") > 0)
                        {
                            ls_label = ls_label.Substring(0, ls_label.IndexOf("\r")) + " " + ls_label.Substring(ls_label.IndexOf("\r") + 2);
                        }
                        if (ls_label.IndexOf("\n") > 0)
                        {
                            ls_label = ls_label.Substring(0, ls_label.IndexOf("\n")) + " " + ls_label.Substring(ls_label.IndexOf("\n") + 2);
                        }
                    }

                    dw_source.SetValue("col", li_cnt.Value, ls_label + "\t" + ls_col_name);

                    dw_sort.SetValue("col", li_cnt.Value, ls_label + "\t" + ls_col_name);

                    li_cnt++;
                    ls_import_str = ls_import_str + ls_label + "\r" + "\n";
                }
            }
            if (li_cnt > 0)
            {

                dw_source.ImportString(ls_import_str);
            }
            // size the window to the number of rows


            dw_source.PixelHeight = 100 + li_cnt.Value * Convert.ToInt32(dw_source.Describe("datawindow", "detail.height"));
            // Show the current sort, if there is one

            ls_label = this.ViewModel.idw_dwProperty.Describe("datawindow", "table.sort");
            if (ls_label.Substring(ls_label.Length - 1) == "!")
            {
                ls_label = ls_label.Substring(0, ls_label.Length - 1);
            }
            li_cnt = 0;
            ls_import_str = "";
            while (ls_label.Length > 1)
            {
                ls_temp = f_get_tokenClass.f_get_token(ref ls_label, ",").Trim();
                col = f_get_tokenClass.f_get_token(ref ls_temp, " ");

                li_cnt++;
                ls_import_str = ls_import_str + col + "\t" + ls_temp + "\r" + "\n";
            }
            if (li_cnt > 0)
            {

                dw_sort.ImportString(ls_import_str);
            }

            for (ll = 1; ll <= dw_sort.RowCount(); ll++)
            {
                ls_temp = dw_sort.GetItemValue<string>(ll, "col");


                li_cnt = dw_source.Find("col == \"" + ls_temp + "\"", 0, dw_source.RowCount());
                if (li_cnt >= 0)
                {

                    dw_source.Delete(li_cnt.Value);
                }
            }
            pb_cancel.Image = new UrlReference(masofonAlias.masofon.Instance.pic_lib + "c.bmp");
            pb_ok.Image = new UrlReference(masofonAlias.masofon.Instance.pic_lib + "y.bmp");

            dw_source.DragIcon(masofonAlias.masofon.Instance.pic_lib + "DRAGITEM.ICO");

            dw_sort.DragIcon(masofonAlias.masofon.Instance.pic_lib + "DRAGITEM.ICO");
            pb_ok.PixelTop = dw_source.PixelHeight + 100;
            pb_cancel.PixelTop = pb_ok.PixelTop;
            pb_clear.PixelTop = pb_ok.PixelTop;
            pb_select.PixelTop = pb_ok.PixelTop;
            dw_sort.PixelHeight = dw_source.PixelHeight;
            this.ViewModel.PixelHeight = pb_ok.PixelTop + pb_ok.PixelHeight + 200;
            //.. multi_lingual_is_active = yes + current system language not Hebrew
            if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && !masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode())
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_source);
                await masofonAlias.masofon.Instance.nvo_translator.fnv_rotate_dw(dw_sort);
            }
        }
        public void activate()
		{
			masofonAlias.masofon.Instance.help_entry = "אפיון שדות מיון";
		}

        public void key(object sender, KeyDownEventArgs e)
        {
            Keys key = ((KeyEventArgs)e).KeyCode;
            ButtonElement pb_ok = this.GetVisualElementById<ButtonElement>("pb_ok");
            ButtonElement pb_cancel = this.GetVisualElementById<ButtonElement>("pb_cancel");
            if (key == Keys.Escape)
            {
                 
                pb_cancel.PerformClick(e);
            }
            else if (key == Keys.Enter || key == Keys.Tab)
            {
                 
                pb_ok.PerformClick(e);
            }
        }
        
        public void pb_select_clicked(object sender, EventArgs e)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			long ll = 0;
			long ll_row = 0;
			string ls_temp = null;
			

			

			
			for (ll = dw_source.RowCount(); ll >= 1; ll += -1)
			{
				
				ls_temp = dw_source.GetItemValue<string>(ll, "col");
				
				
				if (dw_sort.Find("col == \"" + ls_temp + "\"", 1, dw_sort.RowCount()) == 0)
				{
					
					ll_row = dw_sort.Insert(0);
					
					dw_sort.SetItem(ll_row, "col", Convert.ToString(ls_temp));
				}
				
				dw_source.Delete((int)ll);
			}
			

			

		}
		
		public void pb_clear_clicked(object sender, EventArgs e)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			long ll = 0;
			long ll_row = 0;
			string ls_temp = null;
			

			

			
			for (ll = dw_sort.RowCount(); ll >= 1; ll += -1)
			{
				ls_temp = dw_sort.GetItemValue<string>(ll, "col");
				
				
				if (dw_source.Find("col == \"" + ls_temp + "\"", 1, dw_source.RowCount()) == 0)
				{
					
					ll_row = dw_source.Insert(0);
					
					dw_source.SetItem(ll_row, "col", Convert.ToString(ls_temp));
				}
				
				dw_sort.Delete((int)ll);
			}
			

			

		}
		
		public void pb_cancel_clicked(object sender, EventArgs e)
		{
			 
			
			((WindowElement)this.ViewModel.Parent).Close();
		}
		
		public async Task pb_ok_clicked(object sender, EventArgs e)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			int cnt = 0;
			long ll = 0;
			string sort_list = null;
			string tmp = null;
			
			cnt = (int) dw_sort.RowCount();
			for (ll = 1; ll <= cnt; ll++)
			{
				tmp = dw_sort.GetItemValue<string>(ll, "col");
				if (tmp.Trim().Length > 0)
				{
					tmp = tmp + " " + dw_sort.GetItemValue<string>(ll, "Direction");
					if (sort_list.Length > 0)
					{
						sort_list = sort_list + ", " + tmp;
					}
					else
					{
						sort_list = tmp;
					}
				}
			}
			if (sort_list.Length > 0)
			{
				
				if (this.ViewModel.idw_dwProperty.SetSort(sort_list) != 1)
				{
				    await masofonAlias.masofon.Instance.guo_msg.uf_msg(this.ViewModel.is_titleProperty, "", "stopsign!", sort_list + f_get_error_message_numberClass.f_get_error_message_number(2615));
					return;
				}
				
				if (this.ViewModel.idw_dwProperty.Sort() != 1)
				{
				await masofonAlias.masofon.Instance.guo_msg.uf_msg(this.ViewModel.is_titleProperty, "", "stopsign!", sort_list + f_get_error_message_numberClass.f_get_error_message_number(2615));
					return;
				}
			}
			else
			{
				
				this.ViewModel.idw_dwProperty.SetSort("");
				
				this.ViewModel.idw_dwProperty.Sort();
			}
			 
			
			((WindowElement)this.ViewModel.Parent).Close();
		}
		
		public void dw_source_ue_lbutton_down(uint flags, int xpos, int ypos)
		{
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			string ls_band = null;
			string col_name = null;
			string band = null;
			string new_text = null;
			int p = 0;
		//	int add_number = 0;
			long row = 0;
			 
			ls_band = dw_source.GetBandAtPointer();
			if (ls_band.Length > 0)
			{
				p = ls_band.IndexOf("\t");
				if (p > 0)
				{
					band = ls_band.Substring(0, p - 1);
					row = Convert.ToInt64(ls_band.Substring(p + 1));
					if (band == "detail" && row > 0)
					{
						this.ViewModel.i_current_rowProperty = (int)row;
						this.ViewModel.i_source_downProperty = true;
					}
				}
			}
		}
		
		public void dw_source_ue_lbutton_up(uint flags, int xpos, int ypos)
		{
			this.ViewModel.i_source_downProperty = false;
		}
		
		public void dw_source_ue_mouse_move(uint flags, int xpos, int ypos)
		{
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			if (this.ViewModel.i_source_downProperty && this.ViewModel.i_current_rowProperty > 0)
			{
				this.ViewModel.i_source_downProperty = false;
				dw_source.Drag((DragModes.Begin));
			}
		}
		
		public void dw_source_dragdrop(ControlElement source, int row, VisualElement dwo)
		{
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			string ls_temp = null;
			long ll_row = 0;

            if (Extensions.DraggedObject() == dw_sort)
			{
				this.ViewModel.i_sort_downProperty = false;
				if (this.ViewModel.i_current_rowProperty > 0)
				{
					ls_temp = dw_sort.GetItemValue<string>(this.ViewModel.i_current_rowProperty, "col");
					
					dw_sort.Delete(this.ViewModel.i_current_rowProperty);
					
					ll_row = dw_source.Insert(0);
					
					dw_source.SetItem(ll_row, "col", Convert.ToString(ls_temp));
				}
			}
		}
		
		public void dw_sort_ue_lbutton_down(uint flags, int xpos, int ypos)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			string ls_band = null;
			string col_name = null;
			string band = null;
			string new_text = null;
			int p = 0;
			//int add_number = 0;
			long row = 0;
			 
			ls_band = dw_sort.GetBandAtPointer();
			if (ls_band.Length > 0)
			{
				p = ls_band.IndexOf("\t");
				if (p > 0)
				{
					band = ls_band.Substring(0, p - 1);
					row = Convert.ToInt64(ls_band.Substring(p + 1));
					if (band == "detail" && row > 0)
					{
						this.ViewModel.i_current_rowProperty = (int)row;
						this.ViewModel.i_sort_downProperty = true;
					}
				}
			}
		}
		
		public void dw_sort_ue_lbutton_up(uint flags, int xpos, int ypos)
		{
			this.ViewModel.i_sort_downProperty = false;
		}
		
		public void dw_sort_ue_mouse_move(uint flags, int xpos, int ypos)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			if (this.ViewModel.i_sort_downProperty && this.ViewModel.i_current_rowProperty > 0)
			{
				this.ViewModel.i_sort_downProperty = false;
				// TODO: Field 'Begin of type 'Sybase.PowerBuilder.DragModes' is unmapped'. (CODE=1004)
				 
				dw_sort.Drag(DragModes.Begin);
			}
		}
		
		public void dw_sort_dragdrop(ControlElement source, int row, VisualElement dwo)
		{
			GridElement dw_sort = this.GetVisualElementById<GridElement>("dw_sort");
			GridElement dw_source = this.GetVisualElementById<GridElement>("dw_source");
			string str = null;

            if (Extensions.DraggedObject() == dw_source)
			{
				str = dw_source.GetItemValue<string>(this.ViewModel.i_current_rowProperty, "col");
				
				dw_source.Delete(this.ViewModel.i_current_rowProperty);
				 
				dw_sort.ImportString(str);
				
				 
				dw_sort.ScrollToRow(dw_sort.RowCount());
				this.ViewModel.i_source_downProperty = false;
			}
		}
	}

    
}
