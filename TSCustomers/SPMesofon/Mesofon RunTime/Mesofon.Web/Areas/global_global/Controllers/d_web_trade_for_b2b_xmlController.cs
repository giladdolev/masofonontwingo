using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_web_trade_for_b2b_xmlController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_web_trade_for_b2b_xml()
		{
			d_web_trade_for_b2b_xmlRepository repository = new d_web_trade_for_b2b_xmlRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
