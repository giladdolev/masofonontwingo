using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class dddw_param_valuesController : MvcSite.Common.SPBaseController
	{
		public ActionResult dddw_param_values()
		{
			dddw_param_valuesRepository repository = new dddw_param_valuesRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
