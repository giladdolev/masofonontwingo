using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class dw_item_codeController : MvcSite.Common.SPBaseController
	{
		public ActionResult dw_item_code()
		{
			dw_item_codeRepository repository = new dw_item_codeRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
