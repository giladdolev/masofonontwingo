using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofonAlias = masofon;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class u_msrController : MvcSite.Common.SPBaseController
	{

		public ActionResult u_msr()
		{
			return this.View(new u_msr());
		}
		private u_msr ViewModel
		{
			get { return this.GetRootVisualElement() as u_msr; }
		}
		
		public int free_msr()
		{
			CompositeElement ole_msr = this.GetVisualElementById<CompositeElement>("ole_msr");
			// ---- Releasing the MSR ------------
			if (masofonAlias.masofon.Instance.msr4695_exist)
			{
				//TODO: Giamox
				//(ole_msr.Object()).Release();
				//(ole_msr.Object()).close();
			}
			return 0;
		}
		public string uf_msr4695_string_fix(string card_string)
		{
			// Get the original ibm msr track2 string in "card_string" and return an "output_string"
			// to the caller. The output_string is formated it for our program
			//---------------------------------------------------------------------
			int i = 0;
			string Prefix = null;
			string Suffix = null;
			string tmp_string = null;
			string output_string = null;
			string separator_old_sign = null;
			string separator_new_sign = null;
			long card_Initial_len = 0;
			Prefix = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Hardware\\MSR", "prefix", "B", "");
			Suffix = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Hardware\\MSR", "suffix", "F?", "");
			separator_old_sign = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Hardware\\MSR", "separator_old_sign", "D", "");
			separator_new_sign = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Hardware\\MSR", "separator_new_sign", "=", "");
			card_Initial_len = card_string.Length;
			output_string = Prefix;
			for (i = 0; i <= (int)card_Initial_len; i++)
			{
				tmp_string = card_string.Substring(0, 1);
				if (tmp_string == separator_old_sign)
				{
					output_string = output_string + separator_new_sign;
				}
				else
				{
					output_string = output_string + tmp_string;
				}
				card_string = card_string.Substring(card_string.Length - card_string.Length - 1);
			}
			output_string = output_string + Suffix;
			return output_string;
		}

		public void ole_msr_dataevent()
		{
			CompositeElement ole_msr = this.GetVisualElementById<CompositeElement>("ole_msr");
			//----  This Evant Happend when the MSR activated by a magnetic card pass
			
			//this.ViewModel.magnetic_card_track2_resultProperty = uf_msr4695_string_fix((ole_msr).Track2Data);

		    this.ViewModel.ue_activated();
			// reinitializing the MSR after use -------------------------------------
			
			//(ole_msr).DeviceEnabled = true;
			
			//(ole_msr).DataEventEnabled = true;
		}
		public void ole_msr_directioevent()
		{
		}
		public void ole_msr_errorevent()
		{
		}
	}
}
