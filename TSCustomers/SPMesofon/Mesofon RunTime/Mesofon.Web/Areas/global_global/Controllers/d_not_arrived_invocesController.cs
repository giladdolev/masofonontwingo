using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_not_arrived_invocesController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_not_arrived_invoces()
		{
			d_not_arrived_invocesRepository repository = new d_not_arrived_invocesRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
