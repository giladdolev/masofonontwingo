﻿using System;
using System.DataAccess;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Globalization;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using masofonAlias = masofon;
using Common.Transposition.Extensions;

using global;
using Mesofon.Common;
using Mesofon.Models;

namespace Mesofon.Global.Controllers
{
    public class rw_declines_reasonsController : MvcSite.Common.SPBaseController
    {

        public ActionResult rw_declines_reasons(s_array_arguments args)
        {
            return this.View(new rw_declines_reasons(args));
        }
        private rw_declines_reasons ViewModel
        {
            get { return this.GetRootVisualElement() as rw_declines_reasons; }
        }

        public async void Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public void wf_close_window(string as_button_name) //**********************************************************************************************
        {
            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");
            dw_declines.SetLockUI(true);
            //*Object:							rw_reject_reason
            //*Function/Event  Name:		wf_close_window
            //*Purpose:						Close declines window.
            //*
            //*Arguments:					Pass By		Type			Name
            //*									----------------------------------------------------------------------------------
            //*									Value			String			as_button_name -
            //*										
            //*Return:								None.
            //*
            //*Date				Programer		Version		Task#								Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*14-01-2007		PninaSG    		1.2.23.0 		10003 - B2B - Declines 		Initiation
            //************************************************************************************************

            string ls_find = null;
            long ll_row_count = 0;
            long? ll_decline_exist = 0;
            if (as_button_name == "OK")
            {
                ls_find = "checkedField == 1";

                ll_row_count = dw_declines.RowCount();

                ll_decline_exist = dw_declines.Find(ls_find, 0, (int)ll_row_count);
                if (ll_decline_exist >= 0)
                {
                    ll_decline_exist = 1;
                }
            }
            else
            {
                ll_decline_exist = this.ViewModel.il_orig_decline_existProperty;
            }

            this.ViewModel.ll_decline_exist_return = (long)ll_decline_exist;
            this.ViewModel.Visible = false;
            //WindowHelper.Close(this.ViewModel, ll_decline_exist);
        }


        public void dw_declines_CellEndEdit(object sender, EventArgs e)
        {
            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");
            var args = e as GridCellEventArgs;
            if (dw_declines != null && args != null)
            {
                var rowIndex = args.RowIndex;
                var colIndex = args.DataIndex;
                if (rowIndex != -1 && !string.IsNullOrEmpty(colIndex))
                    dw_declines.Rows[rowIndex].Cells[colIndex].Value = args.Value;
            }
        }

        public async Task<long> wf_save_data() //**********************************************************************************************
        {

            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");
            //*Object:								rw_reject_reason
            //*Function/Event  Name:			wf_save_data
            //*Purpose:							Save item/doc declines.
            //*
            //*Arguments:						None.
            //*										
            //*Return:								Long.
            //*
            //*Date				Programer		Version		Task#								Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-01-2007		PninaSG    		1.2.23.0 		10003 - B2B - Declines  		Initiation
            //************************************************************************************************
            dw_declines.SetLockUI(true);

            bool lb_exist_row = false;
          bool lb_new_row = false;
            bool lb_decline_exist = false;
            long ll_row_count = 0;
            long ll_cntr = 0;
            long ll_checked = 0;
            long ll_row_no = 0;
            long ll_ret = 0;
            long ll_line_number = 0;
            long? ll_found_reject = 0;
            long ll_material_number = 0;
            long ll_decline_number = 15;
            long ll_count = 0;
            string ls_find = null;
            IRepository lds_new_declines_reasons;
            // AlexKh - 1.1.26.0 - 2015-01-25 - SPUCM00005189 - use nvuo instead datastore
            //	lds_new_declines_reasons = new nvuo_datastore();
            //lds_new_declines_reasons = CREATE IRepository
            lds_new_declines_reasons = new Repository.d_declines_reasons_displayRepository(); // VisualElementHelper.CreateFromView<GridElement>("d_declines_reasons_display","d_declines_reasons_display", "global_global");
            ll_row_count = dw_declines.RowCount();
            for (ll_cntr = 0; ll_cntr < ll_row_count; ll_cntr++)
            {
                //
                //ModelAction ldw_row_status = dw_declines.GetItemStatus((int)ll_cntr, 0, ModelBuffer.Primary);

                //if (ldw_row_status != ModelAction.None)
                {
                    //Check if the row exist
                    ll_line_number = Convert.ToInt64(dw_declines.GetItemValue<long>(ll_cntr, "line_number"));
                    if (ll_line_number > 0)
                    {
                        lb_exist_row = true;
                    }
                    else
                    {
                        lb_exist_row = false;
                    }

                    if (Convert.ToBoolean(dw_declines.Rows[(int)ll_cntr].Cells["checkedField"].Value))
                        dw_declines.SetItem(ll_cntr, "checkedField", 1);
                    else
                        dw_declines.SetItem(ll_cntr, "checkedField", 0);
                    dw_declines.SetItem(ll_cntr, "reject_quantity", Convert.ToInt32(dw_declines.Rows[(int)ll_cntr].Cells["reject_quantity"].Value));


                    ll_checked = dw_declines.GetItemValue<long>(ll_cntr, "checkedField");
                    this.ViewModel.il_max_line_numberProperty = this.ViewModel.il_max_line_numberProperty + 1;
                    if (ll_line_number == 0)
                    {

                        ll_ret = dw_declines.SetItem(ll_cntr, "line_number", this.ViewModel.il_max_line_numberProperty.ToString());
                    }
                    dw_declines.SetItem(ll_cntr, "parent_serial_number", 1);
                    dw_declines.SetItem(ll_cntr, "update_datetime", DateTime.Now);
                    dw_declines.SetItem(ll_cntr, "parent_serial_number", this.ViewModel.il_serial_numberProperty);
                    if (ll_checked == 1)
                    {

                        dw_declines.SetItem(ll_cntr, "status", "a");
                        lb_decline_exist = true;
                    }
                    else
                    {

                        dw_declines.SetItem(ll_cntr, "status", "d");
                    }
                    if (!lb_exist_row)
                    {

                        ll_row_no = lds_new_declines_reasons.RowCount();
                        if (ll_row_no == 0)
                        {
                            ll_row_no = 1;
                        }



                        ll_ret = dw_declines.RowsCopy((int)ll_cntr, (int)ll_cntr + 1, ModelBuffer.Primary, lds_new_declines_reasons, (int)ll_row_no, ModelBuffer.Primary);
                        lb_new_row = true;
                    }
                }
            }

            if (lds_new_declines_reasons.RowCount() > 0)
            {
                // SharonS - 2008-11-25 - 1.2.37.11 - Check if the decline is a reject all Invoice/PackList 
                ls_find = "move_decline_code == \"27\"";


                ll_found_reject = lds_new_declines_reasons.Find(ls_find, 0, lds_new_declines_reasons.RowCount());
                if (ll_found_reject >= 0)
                {
                    // A reject all decline
                    using (UnitOfWork unitOfWork = new UnitOfWork())
                    {
                        ll_ret = lds_new_declines_reasons.Update(unitOfWork);
                    }
                }
                else
                {
                    // End
                    // SharonS - 2008-04-07 - 1.2.29.2 - Decline header line
                    // Check if there is already a decline header row for this doc
                    ll_count = 0;
                    long ll_header_row = 0;
                    if (true)
                    {
                        ViewModel.LoadData(ref ll_count);
                    }

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Select Error", "", "stopsign!", "wf_save_data - Error Text: " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        return -1;
                    }
                    if (ll_count == 0)
                    {
                        // Insert decline header row

                        ll_header_row = lds_new_declines_reasons.Insert(1);

                        lds_new_declines_reasons.SetItem(ll_header_row, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "supplier_number", this.ViewModel.il_supplier_numberProperty.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "order_number", this.ViewModel.il_order_numberProperty.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "parent_doc_type", Convert.ToString(this.ViewModel.is_parent_doc_typeProperty));

                        lds_new_declines_reasons.SetItem(ll_header_row, "parent_doc_number", this.ViewModel.il_parent_doc_numberProperty.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "material_number", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "line_number", "1");

                        lds_new_declines_reasons.SetItem(ll_header_row, "parent_serial_number", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "status", "a");

                        lds_new_declines_reasons.SetItem(ll_header_row, "inserted_option", "1");

                        lds_new_declines_reasons.SetItem(ll_header_row, "update_datetime", DateTime.Now);

                        lds_new_declines_reasons.SetItem(ll_header_row, "employee_number", masofonAlias.masofon.Instance.gs_vars.active_owner.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "decline_number", ll_decline_number.ToString());

                        lds_new_declines_reasons.SetItem(ll_header_row, "move_decline_code", "34");

                        lds_new_declines_reasons.SetItem(ll_header_row, "is_reject_with_qty", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "is_reject_all_invoice", "1");

                        lds_new_declines_reasons.SetItem(ll_header_row, "is_automatic_applied", "1");

                        lds_new_declines_reasons.SetItem(ll_header_row, "is_red_stamp", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "original_quantity", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "move_actual_quantity", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "reject_quantity", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "move_original_price", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "actual_price", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "b2b_msg_decline_ref", "0");

                        lds_new_declines_reasons.SetItem(ll_header_row, "b2b_status", "1");
                        // SharonS - 1.2.42.0 - 2009-01-18 - CR#1100 - SPUCM00000115 - Set distributor_number 

                        lds_new_declines_reasons.SetItem(ll_header_row, "distributor_number", this.ViewModel.il_distributor_numberProperty.ToString());
                        // AlexKh - 1.2.46.5 - 2011-01-06 - CR#1143 - create header decline
                        //	(await this.ViewModel.invuo_b2b_declineProperty.uf_populate_decline_details(lds_new_declines_reasons, "34", ll_header_row)).Retrieve(out lds_new_declines_reasons);
                        // End
                    }
                    else if (ll_count == 1)
                    {
                        ViewModel.UpdateData();

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                        {

                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("Update Error", "", "stopsign!", "wf_save_data - Error Text: " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                            return -1;
                        }
                        // SharonS - 2008-11-25 - 1.2.37.11 - END IF
                    }
                    // End


                    //GalilCS - update flag to delete then insert like source
                    foreach (d_declines_reasons_display item in lds_new_declines_reasons.GetPrimaryList())
                    {
                        if (item.Action == ModelAction.UpdateByFields || item.Action == ModelAction.UpdateByKeys)
                        {
                            item.Action = ModelAction.DeleteThenInsert;
                        }
                    }

                    using (UnitOfWork unitOfWork = new UnitOfWork())
                    {
                        ll_ret = lds_new_declines_reasons.Update(unitOfWork);
                    }
                }
                // End
            }
            if (lb_exist_row)
            {
                //Eitan; SPUCM00005481; Build 39.3; 1/12/2015
                if (this.ViewModel.il_decline_levelProperty == 1)
                {


                    if (dw_declines.GetItemValue<decimal>(0, "reject_quantity") > dw_declines.GetItemValue<decimal>(0, "original_quantity"))
                    {
                        //await masofonAlias.masofon.Instance.guo_msg.uf_msg("Attention", "", "", "הכמות שהוקלדה גדולה מהכמות בחשבונית");
                        await MessageBox.Show("הכמות שהוקלדה גדולה מהכמות בחשבונית", "Attention", System.Web.VisualTree.MessageBoxButtons.OK, System.Web.VisualTree.MessageBoxDirection.Rtl);
                        return -1;
                    }
                }

                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    ll_ret = dw_declines.Update(unitOfWork);
                }

                // SharonS - 2008-04-07 - 1.2.29.2 - Decline header line
                long? ll_found = 0;


                ll_found = dw_declines.Find("checkedField == 1", 0, dw_declines.RowCount());
                if (ll_found == -1)
                {
                    ViewModel.LoadData1(ref ll_count);

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Select Error", "", "stopsign!", "wf_save_data - Error Text: " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        return -1;
                    }
                    if (Convert.ToInt32(ll_count) == 0)
                    {
                        ViewModel.UpdateData1();

                        if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                        {

                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("Update Error", "", "stopsign!", "wf_save_data - Error Text: " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                            return -1;
                        }
                    }
                }
                else if (ll_found == 1) //Update decline header row to be in active status
                {
                    ViewModel.UpdateData2();

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Update Error", "", "stopsign!", "wf_save_data - Error Text: " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        return -1;
                    }
                }
                // End
            }
            ViewModel.wf_retrieve_data();
            if (lb_decline_exist)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        
        public async Task open() //**********************************************************************************************
        {
            //*Object:								rw_reject_reason
            //*Function/Event  Name:			Open
            //*Purpose:							Initiation.  
            //*
            //*Arguments:						None.
            //*										
            //*Return:								Long.
            //*
            //*Date				Programer		Version		Task#									Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-01-2007		PninaSG			0.2.0 			10003 - B2B -declines 			Initiation
            //************************************************************************************************

            GridElement dw_declines = this.GetVisualElementById<GridElement>("dw_declines");

            dw_declines.SetRepository(new Repository.d_declines_reasons_displayRepository());


            long ll_rtn_code = 0;
            s_array_arguments lstr_mess = new s_array_arguments();
            // AlexKh - 1.2.46.5 - 2011-01-06 - CR#1143 - create nvuo_b2b_decline
            this.ViewModel.invuo_b2b_declineProperty = new nvuo_b2b_decline();
            //wf_resize();
            //Get the arguments that was sent to the window 

            lstr_mess = (s_array_arguments)this.ViewModel.Args;// (s_array_arguments)WindowHelper.GetParam<object>(this.ViewModel);
            ll_rtn_code = await ViewModel.wf_display(lstr_mess);
            dw_declines.Columns["display_name"].ReadOnly = true;
            //SharonS - 1.2.26.8 - 2008-03-17 - If there are no declines 
            if (ll_rtn_code == -1)
            {
                wf_close_window("Cancel");
            }
            //
        }
        public void close() // AlexKh - 1.2.46.5 - 2011-01-06 - CR#1143 - destroy nvuo_b2b_decline
        {

        }



        public void cb_cancel_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								rw_reject_reason.cb_cancel
            //*Function/Event  Name:			Clicked
            //*Purpose:							
            //*
            //*Arguments:						
            //*										None
            //*Return:								
            //*
            //*Date				Programer				Version		Task#									Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-01-2007		Pnina S.G    			0.2.0 		10003 - B2B -declines  			Initiation
            //************************************************************************************************

            //close the window
            wf_close_window("Cancel");
        }

        public async Task cb_ok_clicked(object sender, EventArgs e) //**********************************************************************************************
        {
            //*Object:								rw_reject_reason.cb_ok
            //*Function/Event  Name:			Clicked
            //*Purpose:							
            //*
            //*Arguments:						
            //*										None
            //*Return:								
            //*
            //*Date				Programer				Version		Task#									Description
            //*------------------------------------------------------------------------------------------------------------------------
            //*13-01-2007		Pnina S.G    			0.2.0 		10003 - B2B -declines  			Initiation
            //************************************************************************************************

            long ll_rtn_code = 0;
            //Save the data
            ll_rtn_code = await wf_save_data();
            //close the window
            wf_close_window("OK");
        }
    }
}
