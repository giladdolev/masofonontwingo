﻿using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Extensions;
using System.Globalization;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.VisualTree;
using masofonAlias = masofon;
using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
    public class wh_passwordController : MvcSite.Common.SPBaseController
    {

        public ActionResult wh_password()
        {
            return this.View(new wh_password());
        }
        private wh_password ViewModel
        {
            get { return this.GetRootVisualElement() as wh_password; }
        }
        public async Task<int> wf_check_option(long employee_number)
        {
            // find if the employee has the option declared for him/her
            int i = 0;
            if (true)
            {
                ViewModel.LoadData(employee_number, ref i);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                return -1;
            }
            if (i > 0)
            {
                return 1;
            }
            if (i == 0) // no potions
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error", "", "stopsign!",await f_get_error_message_numberClass.f_get_error_message_number(1359));
            }
            return -1;
        }
        public void centerwindow()
        {
            WindowElement window = this.GetVisualElementById<WindowElement>("wh_password");
            window.StartPosition = WindowStartPosition.CenterParent;
        }
        public async Task<int> wf_clearence(long employee_number, string employee_title)
        {
            // check if the employee in question has a system approval for the action
            if (wf_verify_title(1, employee_title)) // if is owner
            {
                return 1;
            }
            else if (wf_verify_title(2, employee_title)) // if is manager
            {
                if (this.ViewModel.pass_tProperty.action_title == 1) // if must be owner
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                if (await wf_check_option(employee_number) == -1)
                {
                    return -1;
                }
                else
                {
                    // if must be owner
                    if (this.ViewModel.pass_tProperty.action_title == 1)
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }
        public bool wf_verify_title(int title_idx, string a_title)
        {
            int i = 0;
            switch (title_idx)
            {
                case 1:
                    // owner arr
                    for (i = 1; i <= CollectionExtensions.UBound(masofonAlias.masofon.Instance.gs_title.owner); i++)
                    {
                        if (a_title.ToLower(CultureInfo.InvariantCulture) == masofonAlias.masofon.Instance.gs_title.owner[i].ToLower(CultureInfo.InvariantCulture))
                        {
                            return true;
                        }
                    }
                    break;
                case 2:
                    // manager arr
                    for (i = 1; i <= CollectionExtensions.UBound(masofonAlias.masofon.Instance.gs_title.manager); i++)
                    {
                        if (a_title.ToLower(CultureInfo.InvariantCulture) == masofonAlias.masofon.Instance.gs_title.manager[i].ToLower(CultureInfo.InvariantCulture))
                        {
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }
        public void wf_set_window_position()
        {
            WindowElement window = this.GetVisualElementById<WindowElement>("wh_password");
            int i_x = 0;
            int i_y = 0;
            if (!this.ViewModel.ib_mini_terminal_modeProperty)
            {

                window.StartPosition = WindowStartPosition.CenterParent;
            }
            else
            {
                i_x = 0;
                i_y = 0;
                window.StartPosition = WindowStartPosition.Manual;
                window.Location = new Point(i_x, i_y);
            }
            this.ViewModel.Visible = true;
        }
        public async Task open()
        {
            var viewModelPassTProperty = this.ViewModel.pass_tProperty;
            TextBoxElement sle_password = this.GetVisualElementById<TextBoxElement>("sle_password");
            
            this.ViewModel.pass_tProperty = (s_password)WindowHelper.GetParam<object>(this.ViewModel);
            if (this.ViewModel.pass_tProperty.action_title == 2)
            {
                this.ViewModel.BackColor = Color.FromArgb(0, 128, 128);
            }
            if (this.ViewModel.pass_tProperty.action_title == 0)
            {
                viewModelPassTProperty.action_title = 3;
            }
            if (this.ViewModel.pass_tProperty.action_number == 0)
            {
                viewModelPassTProperty.action_number = 0;
            }
            this.ViewModel.pass_tProperty = viewModelPassTProperty;
            this.ViewModel.ib_mini_terminal_modeProperty = this.ViewModel.pass_tProperty.a_mini_terminal_mode;
            this.ViewModel.ib_usual_msgProperty = true;
            sle_password.Focus();
            //centerwindow ()
            //centerwindow ()
            wf_set_window_position();
            if (masofonAlias.masofon.Instance.nvo_translator.multi_lingual_is_active && masofonAlias.masofon.Instance.nvo_translator.direction == "left")
            {
                await masofonAlias.masofon.Instance.nvo_translator.fnv_init(this.ViewModel); // *** trans
                sle_password.RightToLeft = RightToLeft.No;
            }
            if (!isempty_stringClass.isempty_string(masofonAlias.masofon.Instance.af_get_password_argument()))
            {
                sle_password.Text = masofonAlias.masofon.Instance.af_get_password_argument();
                await sle_password_modified(null, EventArgs.Empty);
                masofonAlias.masofon.Instance.af_set_password_argument("");
            }
        }
        public void key(Keys key)
        {
            if (key == Keys.Escape)
            {
                this.ViewModel.escape_calledProperty = true;
                this.ViewModel.close_windowProperty = true;
                
                WindowHelper.Close(this.ViewModel, (-1).ToString());
            }
        }
        public void closequery()
        {
            if (!this.ViewModel.close_windowProperty)
            {
                WindowHelper.SetParam<int>(this.ViewModel,-1);
            }
        }
        public async Task activate() //stas 7/6/2004
        {
            masofonAlias.masofon.Instance.help_entry = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("סיסמת כניסה");
        }

        public void cb_1_clicked(object sender, EventArgs e)
        {
             
            
            ((WindowElement)this.ViewModel.Parent).Close();
        }
        public async Task uo_msr_password_ue_activated()
        {
            TextBoxElement sle_password = this.GetVisualElementById<TextBoxElement>("sle_password");
            u_msr uo_msr_password = this.GetVisualElementById<u_msr>("uo_msr_password");
            uo_msr_password.ue_activated();
            sle_password.Text = uo_msr_password.get_card_string();

            await sle_password_modified(this, new EventArgs());
        }

        public async Task sle_password_enchange()
        {
            ButtonElement cb_1 = this.GetVisualElementById<ButtonElement>("cb_1");
            TextBoxElement sle_password = this.GetVisualElementById<TextBoxElement>("sle_password");
            if (sle_password.Text.Length > 0)
            {
                cb_1.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אשור");
            }
            else
            {
                cb_1.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
            }
        }
        public async Task sle_password_modified(object sender, EventArgs e) //	get and verify password - by user title and / or options
        {
            TextBoxElement sle_password = this.GetVisualElementById<TextBoxElement>("sle_password");
            if (this.ViewModel.escape_calledProperty || sle_password.Text.Length == 0)
            {
                return;
            }
            string ls_password = null;
            string ls_employee_title = null;
            string ls_temp = null;
            long ll_employee_number = 0;
            string ls_prefix = null;
            string ls_suffix = null;
            this.ViewModel.close_windowProperty = false;
            //...Upper case
            ls_password = sle_password.Text.ToUpper();
            // if the window has only to return a password string
            // ----------------------------------------------------
            if (this.ViewModel.pass_tProperty.action_title == 0)
            {
                this.ViewModel.close_windowProperty = true;
                 
                
                WindowHelper.Close(this.ViewModel.Parent as WindowElement, Convert.ToString(ls_password));
            }
            else
            {
                // verify the proper work mode - card_only, hand_only, card & hand
                // -----------------------------------------------------------------
                ls_prefix = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.reg_key + "Touch\\Application", "card_only_prefix", "b", "");
                if (f_get_db_versionClass.f_get_db_version() > 153)
                {
                    ls_prefix = masofonAlias.masofon.Instance.gs_prefix;
                }
                ls_prefix = ls_prefix.ToUpper();
                if (ls_prefix == null || string.IsNullOrEmpty(ls_prefix))
                {
                    if (masofonAlias.masofon.Instance.gb_ibm500)
                    {
                        ls_prefix = ";";
                    }
                    else
                    {
                        ls_prefix = "B";
                    }
                }
                ls_suffix = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.reg_key + "Touch\\Application", "card_only_suffix", "f?", "");
                if (f_get_db_versionClass.f_get_db_version() > 153)
                {
                    ls_suffix = masofonAlias.masofon.Instance.gs_suffix;
                }
                ls_suffix = ls_suffix.ToUpper();
                if (ls_suffix == null || string.IsNullOrEmpty(ls_suffix))
                {
                    if (masofonAlias.masofon.Instance.gb_ibm500)
                    {
                        ls_suffix = "?";
                    }
                    else
                    {
                        ls_suffix = "F?";
                    }
                }
                if (ls_suffix == "F?" && (ls_password.Substring(ls_password.Length - 2).Substring(0, 1) == ls_suffix.Substring(0, 1) && (ls_password.Substring(0, ls_prefix.Length) == ls_prefix && !masofonAlias.masofon.Instance.gb_ibm500)))
                {
                    ls_password = ls_password.Substring(0, ls_password.Length - ls_suffix.Length);
                    ls_password = ls_password + ls_suffix;
                }
                ls_temp = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.reg_key + "Touch\\Application", "card_only", "no", "");
                if (masofonAlias.masofon.Instance.gb_ibm500)
                {
                    if (ls_password.Substring(0, 1) == "%" || ls_password.Substring(0, 1) == "+")
                    {
                        sle_password.Text = "";
                        return;
                    }
                }
                //Card only status
                switch (ls_temp.ToLower(CultureInfo.InvariantCulture))
                {
                    case "no":
                        // card & hand
                        // hand only
                        break;
                    case "no1":
                        
                        if (!SystemFunctionsExtensions.IsNumber(ls_password))
                        {
                            if (this.ViewModel.ib_mini_terminal_modeProperty)
                            {
                                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("#2688", "", "", "OK", 1, "#1356");
                            }
                            else
                            {
                                if (this.ViewModel.ib_usual_msgProperty)
                                {
                                    await  masofonAlias.masofon.Instance.guo_msg.uf_msg(await f_get_error_message_numberClass.f_get_error_message_number(2688), "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number(1356));
                                }
                                else
                                {
                                    await  masofonAlias.masofon.Instance.guo_msg.uf_msg("#2688", "#1356");
                                }
                            }
                            this.ViewModel.close_windowProperty = false;
                            sle_password.Select(1, sle_password.Text.Length);
                            return;
                        }
                        break;
                    case "yes":
                        // card only
                        if (ls_password.Substring(0, ls_prefix.Length) != ls_prefix || ls_password.Substring(ls_password.Length - ls_suffix.Length) != ls_suffix)
                        {
                            if (this.ViewModel.ib_mini_terminal_modeProperty)
                            {
                                await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OK", 1, "#1333");
                            }
                            else
                            {
                                if (this.ViewModel.ib_usual_msgProperty)
                                {
                                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number(1333));
                                }
                                else
                                {
                                  await masofonAlias.masofon.Instance.guo_msg.uf_msg("#1333");
                                }
                            }
                            this.ViewModel.close_windowProperty = false;
                             
                            sle_password.Select(1, sle_password.Text.Length);
                            return;
                        }
                        break;
                }
                // get the need password out of the string - in case of a magnetic card
                // -------------------------------------------------------------------
                if (ls_password.Substring(0, ls_prefix.Length) == ls_prefix && ls_password.Substring(ls_password.Length - ls_suffix.Length) == ls_suffix)
                {
                    ls_password = ls_password.Substring(ls_password.Length - ls_password.Length - ls_prefix.Length);
                    ls_password = ls_password.Substring(0, ls_password.Length - ls_suffix.Length);
                    ls_password = f_get_tokenClass.f_get_token(ref ls_password, "=");
                }
                // get the employee who ownes the password and check for his/her clearence
                // -------------------------------------------------------------------
                if (true)
                {
                    ViewModel.LoadData1(ls_password, ref ll_employee_number, ref ls_employee_title);
                }

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
                {
                    if (this.ViewModel.ib_mini_terminal_modeProperty)
                    {

                       await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("",  masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "OK", 1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בסיס נתונים"));
                    }
                    else
                    {
                        if (this.ViewModel.ib_usual_msgProperty)
                        {
                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("", "", "stopsign!",await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בסיס נתונים"));
                        }
                        else
                        {

                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("", masofonAlias.masofon.Instance.sqlca.SqlErrText, await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בסיס נתונים"));
                        }
                    }
                }

                else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
                {
                    this.ViewModel.close_windowProperty = false;
                    //.. Invalid Password
                    if (this.ViewModel.ib_mini_terminal_modeProperty)
                    {
                        await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OK", 1, "#1357");
                    }
                    else
                    {
                        if (this.ViewModel.ib_usual_msgProperty)
                        {
                             await masofonAlias.masofon.Instance.guo_msg.uf_msg("", "", "stopsign!", await f_get_error_message_numberClass.f_get_error_message_number(1357));
                        }
                        else
                        {
                             await masofonAlias.masofon.Instance.guo_msg.uf_msg("#1357");
                        }
                    }
                     
                    sle_password.Select(1, sle_password.Text.Length);
                }
                else
                {
                    // any existing employee
                    if (this.ViewModel.pass_tProperty.action_title == 3)
                    {
                        this.ViewModel.close_windowProperty = true;
                         
                        
                        WindowHelper.Close(this.ViewModel.Parent as WindowElement, ll_employee_number.ToString());
                    }
                    else
                    {
                        this.ViewModel.close_windowProperty = true;
                         
                        
                        WindowHelper.Close(this.ViewModel.Parent as WindowElement, ll_employee_number.ToString());
                    }
                }
            }
        }
    }
}
