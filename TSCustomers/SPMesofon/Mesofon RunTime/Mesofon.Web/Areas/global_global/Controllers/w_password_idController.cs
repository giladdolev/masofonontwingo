﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using masofonAlias = masofon;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class w_password_idController : wh_passwordController
	{

		public ActionResult w_password_id()
		{
			return this.View(new w_password_id());
		}
		private w_password_id ViewModel
		{
			get { return this.GetRootVisualElement() as w_password_id; }
		}
        public async Task Form_Load(object sender, EventArgs e)
        {
            await open();
        }
        public async Task open()
		{
			await base.open();
		    TextBoxElement sle_password = this.GetVisualElementById<TextBoxElement>("sle_password");
            sle_password.Focus();
			this.ViewModel.ib_usual_msg = false;
		}

        public async Task sle_id_ue_enchange()
        {
            TextBoxElement sle_id = this.GetVisualElementById<TextBoxElement>("sle_id");
            ButtonElement cb_1 = this.GetVisualElementById<ButtonElement>("cb_1");
            if (sle_id.Text.Length > 0)
            {
                cb_1.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("אשור");
            }
            else
            {
                cb_1.Text = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("ביטול");
            }
        }

        public async Task sle_id_modified(object sender, EventArgs e)
		{
			TextBoxElement sle_id = this.GetVisualElementById<TextBoxElement>("sle_id");
			long ll_data = 0;
			long ll_employee_number = 0;
			string ls_emp_id = null;
			if (this.ViewModel.escape_called || sle_id.Text.Length == 0)
			{
				return;
			}
			ll_data = Convert.ToInt64(sle_id.Text);
			if (true)
			{
				ViewModel.LoadData2(ll_data, ref ll_employee_number, ref ls_emp_id);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{
				//MessageBox("Sql Error", SQLCA.SqlErrText)
				if (this.ViewModel.ib_mini_terminal_mode)
				{
					
					await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("Sql Error",  masofonAlias.masofon.Instance.sqlca.SqlErrText, "", "OK", 1,await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בסיס נתונים"));
				}
				else
				{
					
			    	await masofonAlias.masofon.Instance.guo_msg.uf_msg("Sql Error", masofonAlias.masofon.Instance.sqlca.SqlErrText,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("תקלה בסיס נתונים"));
				}
				return;
			}
			
			else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
			{
				//MessageBox("", "עובד עם ת.ז. כזה אינו קיים")
				if (this.ViewModel.ib_mini_terminal_mode)
				{
				    await f_mini_terminal_messageboxClass.f_mini_terminal_messagebox("", "", "", "OK", 1,await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עובד עם ת.ז. כזה אינו קיים"));
				}
				else
				{
				    await masofonAlias.masofon.Instance.guo_msg.uf_msg( await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("עובד עם ת.ז. כזה אינו קיים"));
				}
				return;
			}
			this.ViewModel.close_window = true;
			 
			
			WindowHelper.Close(this.ViewModel.Parent as WindowElement, ll_employee_number.ToString());
		}
	}
}
