using System.Web.Mvc;
using Mesofon.Repository;
 
namespace Mesofon.Global.Controllers
{
	public class d_pallets_xmlController : MvcSite.Common.SPBaseController
	{
		public ActionResult d_pallets_xml()
		{
			d_pallets_xmlRepository repository = new d_pallets_xmlRepository();
			repository.Retrieve();
			return this.View(repository);
		}
	}
}
