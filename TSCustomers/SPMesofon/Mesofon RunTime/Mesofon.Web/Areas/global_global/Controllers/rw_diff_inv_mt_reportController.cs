using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Extensions;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;

using global;
using Mesofon.Common;
using MvcSite.Common;
using System.Xml;
using Mesofon.Data.ServicePrintBOS;
using System.Collections.Generic;
using System.Collections;
using Mesofon.Models;
using System.Linq;

namespace Mesofon.Global.Controllers
{
	public class rw_diff_inv_mt_reportController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_diff_inv_mt_report(string ls_params, string FormType)
		{
            return this.View(new rw_diff_inv_mt_report(ls_params, FormType));
		}
		private rw_diff_inv_mt_report ViewModel
		{
			get { return this.GetRootVisualElement() as rw_diff_inv_mt_report; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            GridElement dw_diff_inv_mt_report = this.GetVisualElementById<GridElement>("dw_diff_inv_mt_report");
            dw_diff_inv_mt_report.SetRepository(new d_diff_inv_mt_reportRepository());
            open();
            if (this.ViewModel.FormType == "base_body")
            {
                //Sapak
                dw_diff_inv_mt_report.setVisibleProperty("actual_quantity", true);
            }
            else
            {
                //Marlog
                dw_diff_inv_mt_report.setVisibleProperty("actual_quantity", false);
            }
        }
        public void ue_print()
		{
		    IRepository lds_print;
			lds_print = new d_diff_inv_mt_report_printRepository();
			lds_print.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_order_numberProperty);
            lds_print.SetSort("order_number Desc");
            lds_print.Sort();
            if (lds_print.RowCount() > 2)
            {
                lds_print.RowsMove(0, 1, ModelBuffer.Primary, lds_print, lds_print.RowCount(), ModelBuffer.Primary);
            }

            XmlDocument docHeader;
            string HeaderName;
            lds_print.GetXML(out docHeader, out HeaderName);

            lds_print.Print(Globals.DeviceID , Globals.UserID,Globals.Password, MvcSite.Common.Globals.PrinterName);

            this.ViewModel.Close();
		}
		public int wf_get_data()
		{
			ButtonElement cb_print = this.GetVisualElementById<ButtonElement>("cb_print");
			string ls_params = null;
			string ls_branch_number = null;
			string ls_supplier_number = null;
			string ls_order_number = null;
			// Get recived message
			
			ls_params = this.ViewModel.Params;
			ls_branch_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_supplier_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_order_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			// Check if empty or Null or 0
			if (isempty_stringClass.isempty_string(ls_branch_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_supplier_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_order_number))
			{
				return -1;
			}
			// Check if not a number
			
			if (!SystemFunctionsExtensions.IsNumber(ls_branch_number))
			{
				return -1;
			}
			
			if (!SystemFunctionsExtensions.IsNumber(ls_supplier_number))
			{
				return -1;
			}
			
			if (!SystemFunctionsExtensions.IsNumber(ls_order_number))
			{
				return -1;
			}
			// Set data into the instance variables
			this.ViewModel.il_branch_numberProperty = Convert.ToInt64(ls_branch_number);
			// AlexKh - 1.2.46.6 - 2011-01-24 - SPUCM00002425 - check if working in shipment mode
			if (ls_order_number == "-1")
			{
				this.ViewModel.ib_shipment_modeProperty = true;
				this.ViewModel.il_shipment_numberProperty = Convert.ToInt64(ls_supplier_number);
				this.ViewModel.is_pallet_numberProperty = f_get_tokenClass.f_get_token(ref ls_params, "@");
				// AlexKh - 1.1.1.20 - 2014-05-04 - CR#1171(SPUCM00004718) - set marlog branch_number
				this.ViewModel.il_marlog_branchProperty = Convert.ToInt64(f_get_tokenClass.f_get_token(ref ls_params, "@"));
				cb_print.Enabled = false;
			}
			else
			{
				this.ViewModel.il_supplier_numberProperty = Convert.ToInt64(ls_supplier_number);
				this.ViewModel.il_order_numberProperty = Convert.ToInt64(ls_order_number);
			}
			return 1;
		}
		public int wf_retrieve()
		{
			GridElement dw_diff_inv_mt_report = this.GetVisualElementById<GridElement>("dw_diff_inv_mt_report");
			int li_ret = 0;
			
			
			
			li_ret = (int) dw_diff_inv_mt_report.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_supplier_numberProperty, this.ViewModel.il_order_numberProperty);
			return li_ret;
		}
		public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_inv_mt_report
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			//IF nvo_translator.fnv_is_multy_lingual() AND NOT nvo_translator.fnv_is_hebrew_mode() AND &
			//	 															nvo_translator.fnv_is_direction_left() THEN
			//	ls_lang = "alignment_left"
			//ELSE
			//	ls_lang = "alignment_right"
			//END IF
			//
			//IF Long(ls_window_xpos) > 0 THEN
			//	ll_xpos = Long(ls_window_xpos)
			//ELSE
			//	ll_xpos = 0
			//END IF
			//IF Long(ls_window_ypos) > 0 THEN
			//	ll_ypos = Long(ls_window_ypos)
			//ELSE
			//	ll_ypos = 0
			//END IF
			//IF Long(ls_window_width) > 0 THEN
			//	THIS.Width = Long(ls_window_width)
			//END IF
			//IF Long(ls_window_height) > 0 THEN
			//	THIS.Height = Long(ls_window_height)
			//END IF
			this.ViewModel.PixelLeft = (int)((global.rw_diff_inv_mt_report.il_max_window_width - this.ViewModel.PixelWidth) / 2);
			this.ViewModel.PixelTop = (int)((global.rw_diff_inv_mt_report.il_max_window_height - this.ViewModel.PixelHeight) / 2);
			return;
            //int ll_ret = 0;
            // 
            //ll_ret = this.ViewModel.Move((int)ll_xpos, (int)ll_ypos);
			//THIS.Visible = TRUE
		}
		public int wf_retrieve_for_shipment() //**********************************************************************************************
		{
			GridElement dw_diff_inv_mt_report = this.GetVisualElementById<GridElement>("dw_diff_inv_mt_report");
			//*Object:				rw_diff_order_mt_report
			//*Function Name:	wf_retrieve_for_shipment
			//*Purpose: 			Retrieve for every invoice in pallet the items that where not scanned
			//*Arguments: 		None
			//*Return:				Integer	1/-1
			//*Date				Programer		Version	Task#	 			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*24-01-2011		AlexKh			1.2.45.6	SPUCM00002425	Initial version
			//************************************************************************************************
			
			int ll_ret = 0;
			int ll_i = 0;
			long ll_invoice_number = 0;
			long ll_supplier_number = 0;
			string ls_ret = null;
			IRepository lds_diff_inv_mt_report;
			
			
			 
			this.ViewModel.ids_pallet_invoicesProperty = new d_mini_terminal_pallet_invoicesRepository();
			
			lds_diff_inv_mt_report = new d_diff_pallet_inv_mt_reportRepository();

            if (dw_diff_inv_mt_report.Repository() == null)
                dw_diff_inv_mt_report.SetRepository(new d_diff_pallet_inv_mt_reportRepository(), false);

            // AlexKh - 1.1.20.0 - 2014-05-04 - SPUCM00004718  - retrieve also by marlog_number
            //ll_ret = ids_pallet_invoices.Retrieve(il_branch_number, il_shipment_number, is_pallet_number)

            ll_ret = (int) this.ViewModel.ids_pallet_invoicesProperty.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_shipment_numberProperty, this.ViewModel.is_pallet_numberProperty, this.ViewModel.il_marlog_branchProperty);
			if (ll_ret <= 0)
			{
				return -1;
			}
			
			for (ll_i = 0; ll_i < this.ViewModel.ids_pallet_invoicesProperty.RowCount(); ll_i++)
			{
				
				ll_supplier_number = this.ViewModel.ids_pallet_invoicesProperty.GetItemValue<long>(ll_i, "supplier_number");
				
				ll_invoice_number = (long)this.ViewModel.ids_pallet_invoicesProperty.GetItemValue<double>(ll_i, "invoice_number");
				 
				ll_ret = (int) lds_diff_inv_mt_report.Retrieve(this.ViewModel.il_branch_numberProperty, ll_supplier_number, ll_invoice_number, this.ViewModel.is_pallet_numberProperty);
				if (ll_ret > 0)
				{
                    ll_ret = lds_diff_inv_mt_report.RowsMove(0, lds_diff_inv_mt_report.RowCount(), ModelBuffer.Primary, dw_diff_inv_mt_report.Repository(), dw_diff_inv_mt_report.RowCount() + 1, ModelBuffer.Primary);
				}
			}
            dw_diff_inv_mt_report.SetRepository(dw_diff_inv_mt_report.Repository());
			return 1;
		}
		
		public void open() // Get message data
		{
			if (this.wf_get_data() == -1)
			{
				
				WindowHelper.Close(this.ViewModel, (-1).ToString());
				return;
			}
			// AlexKh - 1.2.46.6 - 2011-01-24 - SPUCM00002425 - check if working in shipment mode
			if (this.ViewModel.ib_shipment_modeProperty)
			{
				if (wf_retrieve_for_shipment() < 1)
				{
					
					WindowHelper.Close(this.ViewModel, (-1).ToString());
					return;
				}
			}
			else
			{
				// Retrieve data
				if (this.wf_retrieve() < 1)
				{
					
					WindowHelper.Close(this.ViewModel, (-1).ToString());
					return;
				}
			}
			//THIS.wf_set_window_position()
		 //   WindowElement root = this.GetRootVisualElement() as WindowElement;
		 //   root.Location = new Point((int)((global.rw_diff_inv_mt_report.il_max_window_width - this.ViewModel.PixelWidth) / 2), (int)((global.rw_diff_inv_mt_report.il_max_window_height - this.ViewModel.PixelHeight) / 2));
			//wf_resize();
		}
		
		public void cb_print_clicked(object sender, EventArgs e)
		{
			this.ue_print();
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			 
			
			((WindowElement)this.ViewModel).Close();
		}
		
        public int dw_diff_inv_mt_report_dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row)
		{
			return 1;
		}
	}
}
