using System;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree.Elements;
using System.Drawing;
using System.Extensions;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;

using global;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using MvcSite.Common;
using Mesofon.Models;

namespace Mesofon.Global.Controllers
{
	public class rw_not_arrived_inv_reportController : MvcSite.Common.SPBaseController
	{

        public ActionResult rw_not_arrived_inv_report(string ls_params)
		{
            return this.View(new rw_not_arrived_inv_report(ls_params));
		}
		private rw_not_arrived_inv_report ViewModel
		{
			get { return this.GetRootVisualElement() as rw_not_arrived_inv_report; }
		}
        public void Form_Load(object sender, EventArgs e)
        {
            open();
        }
        public void ue_print_for_shipment() //**********************************************************************************************
		{
            GridElement dw_not_arrived_invoces = this.GetVisualElementById<GridElement>("dw_not_arrived_invoces");
			//*Object:				rw_diff_order_mt_report
			//*Function Name:	ue_retrieve_for_shipment
			//*Purpose: 			Print rows
			//*Arguments: 		None
			//*Return:				Integer	0/-1
			//*Date				Programer		Version	Task#	 			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*30-05-2012		Vika			  1.2.48.26	SPUCM00003580	Initial version
			//************************************************************************************************
			
			long ll_i = 0;
			long ll_supplier_number = 0;
			long ll_invoice_number = 0;
			long ll_ret = 0;
			long ll_rowcount = 0;
			string ls_branch = null;

            d_not_arrived_invoces_printRepository lds_print_all = new d_not_arrived_invoces_printRepository();
            IRepository lds_print = new d_not_arrived_invoces_printRepository();
			ll_rowcount = dw_not_arrived_invoces.RowCount();
			for (ll_i = 0; ll_i < ll_rowcount; ll_i++)
			{
				ll_supplier_number = (long)dw_not_arrived_invoces.GetItemValue<double>(ll_i, "supplier_number");
				ll_invoice_number = (long)dw_not_arrived_invoces.GetItemValue<double>(ll_i, "invoice_number");
				ll_ret = lds_print.Retrieve(this.ViewModel.il_branch_numberProperty, ll_supplier_number, ll_invoice_number);
				if (ll_ret > 0)
				{
					lds_print.SetItem(0, "items_quantity", ll_ret.ToString());
					ll_ret = lds_print.RowsMove(0, 1, ModelBuffer.Primary, (lds_print_all), lds_print_all.RowCount() + 1, ModelBuffer.Primary);
				}
			}
			if (true)
			{
				ViewModel.LoadData(ref ls_branch);
			}
            for (int i = 0; i < lds_print_all.RowCount(); i++)
            {
                d_not_arrived_invoces_print item = lds_print_all.GetItem<d_not_arrived_invoces_print>(i);
                item.t_shipment_number = this.ViewModel.il_shipment_numberProperty.ToString();
                item.branch_name_t = ls_branch;
                item.row = (i + 1).ToString();
            }
            
			lds_print_all.Print(Globals.DeviceID, Globals.UserID, Globals.Password, MvcSite.Common.Globals.PrinterName);


            this.ViewModel.Close();
		}
		public int wf_get_data()
		{
			// Vika 2012-05-30 SPUCM00003580 ביטול חשבונית מרלוג בסניף
			string ls_params = null;
			string ls_branch_number = null;
			string ls_supplier_number = null;
			string ls_order_number = null;
			// Get recived message
			
			ls_params = this.ViewModel.Params;
			ls_branch_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_supplier_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			ls_order_number = f_get_tokenClass.f_get_token(ref ls_params, "@");
			// Check if empty or Null or 0
			if (isempty_stringClass.isempty_string(ls_branch_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_supplier_number))
			{
				return -1;
			}
			if (isempty_stringClass.isempty_string(ls_order_number))
			{
				return -1;
			}
			// Check if not a number
			
			if (!SystemFunctionsExtensions.IsNumber(ls_branch_number))
			{
				return -1;
			}
			
			if (!SystemFunctionsExtensions.IsNumber(ls_supplier_number))
			{
				return -1;
			}
			
			if (!SystemFunctionsExtensions.IsNumber(ls_order_number))
			{
				return -1;
			}
			// Set data into the instance variables
			this.ViewModel.il_branch_numberProperty = Convert.ToInt64(ls_branch_number);
			this.ViewModel.il_shipment_numberProperty = Convert.ToInt64(ls_supplier_number);
			return 1;
		}
		public void wf_set_window_position() //**********************************************************************************************
		{
			//*Object:								rw_diff_order_mt_report
			//*Function/Event  Name:			wf_set_window_position
			//*Purpose:							This function sets the window in the currect position 
			//*										for the mini terminal srceen.
			//*  
			//*Arguments:						None.
			//*Return:								None.  
			//*Date 			Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*22-08-2007		SharonS					0.1.1  		B2B 			Initiation
			//************************************************************************************************
			
			long ll_xpos = 0;
			long ll_ypos = 0;
			string ls_window_height = null;
			string ls_window_width = null;
			string ls_window_xpos = null;
			string ls_window_ypos = null;
			string ls_lang = null;
			if (masofonAlias.masofon.Instance.nvo_translator.fnv_is_multy_lingual() && (!masofonAlias.masofon.Instance.nvo_translator.fnv_is_hebrew_mode() && masofonAlias.masofon.Instance.nvo_translator.fnv_is_direction_left()))
			{
				ls_lang = "alignment_left";
			}
			else
			{
				ls_lang = "alignment_right";
			}
			if (Convert.ToInt64(ls_window_xpos) > 0)
			{
				ll_xpos = Convert.ToInt64(ls_window_xpos);
			}
			else
			{
				ll_xpos = 0;
			}
			if (Convert.ToInt64(ls_window_ypos) > 0)
			{
				ll_ypos = Convert.ToInt64(ls_window_ypos);
			}
			else
			{
				ll_ypos = 0;
			}
			if (Convert.ToInt64(ls_window_width) > 0)
			{
				this.ViewModel.PixelWidth = (int)Convert.ToInt64(ls_window_width);
			}
			if (Convert.ToInt64(ls_window_height) > 0)
			{
				this.ViewModel.PixelHeight = (int)Convert.ToInt64(ls_window_height);
			}
			ll_ypos += (global.rw_not_arrived_inv_report.il_max_window_height - this.ViewModel.PixelHeight) / 2;
			ll_xpos += (global.rw_not_arrived_inv_report.il_max_window_width - this.ViewModel.PixelWidth) / 2;
			int ll_ret = 0;
		
		    WindowElement root = this.GetRootVisualElement() as WindowElement;
		    root.Location = new Point((int)ll_xpos, (int)ll_ypos);
			this.ViewModel.Visible = true;
		}
		public int wf_retrieve_for_shipment() //**********************************************************************************************
		{
			GridElement dw_not_arrived_invoces = this.GetVisualElementById<GridElement>("dw_not_arrived_invoces");
			//*Object:				rw_diff_order_mt_report
			//*Function Name:	wf_retrieve_for_shipment
			//*Purpose: 			Retrieve for every order in shipment the items that where ordered and not supplied
			//*Arguments: 		None
			//*Return:				Integer	1/-1
			//*Date				Programer		Version	Task#	 			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*30-05-2012		Vika			1.2.48.26	SPUCM00003580	Initial version
			//************************************************************************************************
			
			int ll_ret = 0;
			int ll_i = 0;
			long ll_invoice_number = 0;
			long ll_supplier_number = 0;
			long ll_rowcount = 0;
			long ll_count = 0;
			long ll_new_row = 0;
			string ls_ret = null;
			IRepository lds_shipment_orders;
			IRepository lds_not_arrived_invoces;
			 
			lds_shipment_orders = new d_shipment_ordersRepository();
			lds_not_arrived_invoces = new d_not_arrived_invocesRepository();
			ll_ret = (int) lds_shipment_orders.Retrieve(this.ViewModel.il_branch_numberProperty, this.ViewModel.il_shipment_numberProperty);
			if (ll_ret <= 0)
			{
				return -1;
			}
			
			ll_rowcount = lds_shipment_orders.RowCount();
			for (ll_i = 0; ll_i < (int)ll_rowcount; ll_i++)
			{
				ll_supplier_number = lds_shipment_orders.GetItemValue<long>(ll_i, "supplier_number");
				
				ll_invoice_number = (long)lds_shipment_orders.GetItemValue<double>(ll_i, "invoice_number");
				ll_count = 0;
				if (true)
				{
					ViewModel.LoadData1(ll_supplier_number, ll_invoice_number, ref ll_count);
				}
				if (ll_count == 0) // חשבוניות שלא הגיעו כלל
				{
					
					ll_new_row = lds_not_arrived_invoces.Insert(0);
					 
					lds_not_arrived_invoces.SetItem(ll_new_row, "supplier_number", Convert.ToDouble(ll_supplier_number));
					 
					lds_not_arrived_invoces.SetItem(ll_new_row, "invoice_number", Convert.ToDouble(ll_invoice_number));
				}
			}
			// AlexKh - 1.1.1.2 - 2013-03-05 - SPUCM00004018 - fix to present not scanned invoices
			//IF Not lb_not_close THEN Return -1
			
			if (lds_not_arrived_invoces.RowCount() == 0)
			{
				return -1;
			}




            var not_arrived_invocesRepo = new d_not_arrived_invocesRepository();
            ll_ret = lds_not_arrived_invoces.RowsMove(0, lds_not_arrived_invoces.RowCount(), ModelBuffer.Primary, not_arrived_invocesRepo, dw_not_arrived_invoces.RowCount() + 1, ModelBuffer.Primary);
            dw_not_arrived_invoces.SetRepository(not_arrived_invocesRepo);



            return 1;
		}
		
		public void open()
		{
			// Vika 2012-05-30 SPUCM00003580 ביטול חשבונית מרלוג בסניף
			// Get message data
			if (this.wf_get_data() == -1)
			{
				
				WindowHelper.Close(this.ViewModel, (-1).ToString());
				return;
			}
			if (wf_retrieve_for_shipment() < 1)
			{
				
				WindowHelper.Close(this.ViewModel, (-1).ToString());
				return;
			}
			
		
		}
		
		public void cb_print_clicked(object sender, EventArgs e)
		{
			// Vika 2012-05-30 SPUCM00003580 ביטול חשבונית מרלוג בסניף
			this.ue_print_for_shipment();
		}
		
		public void cb_ok_clicked(object sender, EventArgs e)
		{
			 
			
			((WindowElement)this.ViewModel).Close();
		}
		
        public int dw_not_arrived_invoces_dberror(int sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, int row)
		{
			return 1;
		}
	}
}
