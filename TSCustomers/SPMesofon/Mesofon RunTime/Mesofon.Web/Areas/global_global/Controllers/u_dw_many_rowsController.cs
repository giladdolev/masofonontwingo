using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.VisualTree.MVC;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using masofonAlias = masofon;
using System.Web.VisualTree.Extensions;

using global;
using Mesofon.Common;

namespace Mesofon.Global.Controllers
{
	public class u_dw_many_rowsController : u_dwController
	{

		public ActionResult u_dw_many_rows()
		{
			return this.View(new u_dw_many_rows());
		}
		private u_dw_many_rows ViewModel
		{
			get { return this.GetRootVisualElement() as u_dw_many_rows; }
		}
		public async Task ue_mousedown()
		{
			str_sort lstr_dw_sort = default(str_sort);
			object flags = null;
			//.. open filter window for this dw
			if (Convert.ToInt32(flags) == 16)
			{
				lstr_dw_sort.dw = this.ViewModel;
				if (this.ViewModel.iw_parent != null)
				{
					lstr_dw_sort.title = await masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(this.ViewModel.iw_parent.Text);
				}
				else
				{
					lstr_dw_sort.title = "";
				}

                await WindowHelper.Open<w_filter>("global_global", "sort", lstr_dw_sort);


            }
		}

	    public long uf_get_selected_rows()
		{
			long ll_SelectedCount = 0;
			 
			ll_SelectedCount = Convert.ToInt64(this.ViewModel.Describe("Evaluate('sum(if (IsSelected(),1, 0) for all)',1)"));
			return ll_SelectedCount;
		}
		
		public void uf_set_select_single_row(bool ab_select_single_row)
		{
			this.ViewModel.ib_select_single_rowProperty = ab_select_single_row;
		}
		public bool uf_get_include_rows()
		{
			return this.ViewModel.ib_include_rowsProperty;
		}
		public void uf_set_include_rows(bool ab_include_rows)
		{
			this.ViewModel.ib_include_rowsProperty = ab_include_rows;
		}
		protected void uf_select_with_include_rows(long al_row) //--------------------------------------------------------------------
        {
            KeyEventArgs e = new KeyEventArgs();
            //Function:			protected u_dw_report.uf_select_with_include_rows()
            //
            // Returns:         None
            //
            // Parameters:      value Long al_row	---> current clicked row at dw
            // 
            // Copyright  - Stas
            //
            // Date Created: 11/08/2005
            //
            // Description:	
            // 						select rows depend to shiftclicked or ib_include_rows checkbox clicked
            //--------------------------------------------------------------------------------
            // Modifications:
            // Date            Author              Comments
            //------------------------------------------------------------------------------

            long ll_rowcount = 0;
			long ll_target_row = 0;
			long ll_count = 0;
			if (!(this.ViewModel != null))
			{
				return;
			}
			
			ll_rowcount = this.ViewModel.RowCount();
			if (ll_rowcount == 0 || ll_rowcount <= 0)
			{
				return;
			}
			if (al_row > 0 && al_row <= ll_rowcount)
			{
                ll_target_row = this.ViewModel.GetSelectedRow(0);
				 
				if (this.ViewModel.ib_include_rowsProperty || e.KeyCode == Keys.Shift)
				{
					if (ll_target_row > 0)
					{
						
						if (!this.ViewModel.IsSelected((int)al_row))
						{
							if (ll_target_row > al_row)
							{
								for (ll_count = al_row; ll_count <= ll_target_row; ll_count += 1)
								{
									
									this.ViewModel.SelectRow((int)ll_count, true);
								}
							}
							else if (ll_target_row < al_row)
							{
								for (ll_count = al_row; ll_count >= ll_target_row; ll_count += -1)
								{
									
									this.ViewModel.SelectRow((int)ll_count, true);
								}
							}
							else if (ll_target_row == al_row)
							{
								
								this.ViewModel.SelectRow((int)al_row, false);
							}
						}
						else
						{
							
							this.ViewModel.SelectRow((int)al_row, false);
						}
					}
					else
					{
						for (ll_count = 1; ll_count <= al_row; ll_count += 1)
						{
							
							this.ViewModel.SelectRow((int)ll_count, true);
						}
						//IF ll_target_row > 0
					}
				}
				else
				{
					
					if (this.ViewModel.IsSelected((int)al_row))
					{
						
						this.ViewModel.SelectRow((int)al_row, false);
					}
					else
					{
						
						this.ViewModel.SelectRow((int)al_row, true);
					}
				} //IF cbx_1.Checked OR KeyDown(KeyShift!)
			} //IF row > 0 AND row <=  ll_rowcount
		}
		
		public void clicked(int xpos, int ypos, int row, ControlElement dwo)
        {
			 
			base.clicked(0, 0, 0, default(ControlElement));
			f_dw_set_sortClass.f_dw_set_sort(this.ViewModel);
			if (row > 0 && !this.ViewModel.ib_no_rows_selectionProperty)
			{
				//.. select single row only
				if (this.ViewModel.ib_select_single_rowProperty)
				{
					
					if (ViewModel.IsSelected(row))
					{
						
						this.ViewModel.SelectRow(row, false);
					}
					else
					{
						
						this.ViewModel.SelectRow(0, false);
						
						this.ViewModel.SelectRow(row, true);
					}
				}
				else
				{
					uf_select_with_include_rows(row);
				}
			}
		}
		public async Task rbuttondown(int xpos, int ypos, int row, ControlElement dwo)
		{
			//base.rbuttondown(0, 0, 0, default(ControlElement));
			str_sort lstr_dw_sort = default(str_sort);
			//.. header of dw activated -> open sort window
			 
			if (this.ViewModel.GetBandAtPointer().Substring(0, 7) == "header" + "\t")
			{
				lstr_dw_sort.dw = this.ViewModel;
				if (this.ViewModel.iw_parent != null)
				{
					lstr_dw_sort.title =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp(this.ViewModel.iw_parent.Text);
				}
				else
				{
					lstr_dw_sort.title =await  masofonAlias.masofon.Instance.nvo_translator.fnv_translate_exp("");
				}
                await WindowHelper.Open<w_filter>("global_global", "sort", lstr_dw_sort);

            }
		}
		
		public void retrieveend(int l)
		{
			this.ViewModel.retrieveend(0);
		    this.ViewModel.uf_set_hscroll_position();
		}
        public async Task sqlpreview(string sqlsyntax, object buffer, int row)
		{
         //   base.sqlpreview(default(SQLPreviewFunctionValue), default(SQLPreviewTypeValue), default(string), default(object), 0);
			string ls_temp = null;
			//.. open debug sql window if checked
			if (this.ViewModel.ib_sql_preview_flagProperty)
			{
                await WindowHelper.Open<w_sql_syntax>("global_global", "sqlSyntax", sqlsyntax);
			    
				
				if (WindowHelper.GetParam<string>(this.ViewModel) == "Cancled")
				{
					ls_temp = "";
				}
				else
				{
					
					ls_temp = WindowHelper.GetParam<string>(this.ViewModel);
				}
			}
		}
	}
}
