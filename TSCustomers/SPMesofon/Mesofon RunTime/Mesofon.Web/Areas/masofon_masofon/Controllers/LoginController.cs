﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mesofon.Common;
using System.Threading.Tasks;
using MvcSite.ServiceSecurity;
using MvcSite.Common;
using System.DataAccess;
using System.Data;
using System.Web.Security;
using System.Net;
using log4net;
using System.Configuration;

namespace masofon
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        public ActionResult Login()
        {
            Login model = new Login();
            
            //GalilCS - Callback this page as a get method.
            this.Response.AddHeader("loginpage", "loginpage");
            try
            {
                bool isTimeout = Convert.ToBoolean(Request.QueryString["timeout"]);
                if (isTimeout)
                {
                    model.Message = "Your session has been expired";
                    FormsAuthentication.SignOut();
                    return View(model);
                }
            }
            catch { }
            FormsAuthentication.SignOut();
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(Login model)
        {
            try
            {
                string deviceId = "";

                var data = new SecurityServiceClient();
                //Masofon authentication 
                var MasofonAuthentication = data.Authentication(deviceId);
                if (MasofonAuthentication.Item3)
                {
                    //get branch number station number
                    Globals.BranchNumberState = model.BranchNumber; // MasofonAuthentication.Item1;
                    Globals.StationNumberState = model.StationNumber;

                    //Get empolyee name
                    long? empNumber = null;
                    string empTitle = "";
                    
                    if (model.ValidationType == "password")//by password 
                    {
                        LoadData1(Convert.ToInt32(Globals.BranchNumberState), model.Password, ref empNumber, ref empTitle);
                        if (empNumber == null)
                        {
                            model.Message = string.Format("סיסמה לא מוכרת");
                            return View(model);
                        }
                    }
                    else if (model.ValidationType == "UserId")//by password 
                    {
                        LoadData2(Convert.ToInt32(Globals.BranchNumberState), model.UserId, ref empNumber, ref empTitle);
                        if (empNumber == null)
                        {
                            model.Message = string.Format("עובד עם ת.ז כזה אינו קיים");
                            return View(model);
                        }
                    }

                    //User authentication 
                    var userAuthentication = data.Authentication2(model.UserId, model.Password, deviceId);
                   

                    if (userAuthentication.Item2)
                    {
                        Globals.UserRegistryItemsState = userAuthentication.Item1.ToList();
                        Globals.UserIDState = model.UserId;
                        Globals.PasswordState = model.Password;
                        Globals.DeviceIDState = deviceId;
                        Globals.EmployeeNumberState = Convert.ToString(empNumber);

                        //Kill previous ticket
                        if (model.ValidationType == "password")//by password 
                        {
                            FormsAuthentication.SetAuthCookie(model.Password, false);
                        }
                        else if (model.ValidationType == "UserId")//by password 
                        {
                            FormsAuthentication.SetAuthCookie(model.UserId, false);
                        }
                        return RedirectToRoute("Terminal");
                    }
                }

                return View(model);

            }
            catch (Exception e)
            {
                model.Message = string.Format("חיבור נכשל, לנסות שנית");
                var msg = string.Format("Login Form {0}Branch: {1}{0}Station: {2}{0}UserId:{3}{0}Message: {4}{0}Trace: {5}{0}", Environment.NewLine, model.BranchNumber, model.StationNumber, model.UserId, e.Message, e.StackTrace);
                System.Diagnostics.EventLog.WriteEntry("Mesofon", msg, System.Diagnostics.EventLogEntryType.Error);
            }
            finally
            {
                Globals.QueryStringState = Request.QueryString.ToString();
                if (string.IsNullOrEmpty(Globals.PrinterNameState))
                {
                    Globals.PrinterNameState = ConfigurationManager.AppSettings["PrinterName"];
                }

                if (string.IsNullOrEmpty(Globals.BarcodePrinterNameState))
                {
                    Globals.BarcodePrinterNameState = ConfigurationManager.AppSettings["BarcodePrinterName"];
                }
            }

            return View(model);
        }


        public void LoadData1(int branch_number, string ls_password, ref long? ll_employee_number, ref string ls_employee_title)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", branch_number }, { "@1", ls_password } };
                    string sql = "SELECT employees.number,  employees.title FROM employees WHERE (branch_number = @0) AND (employees.password = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_employee_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_employee_title = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
        }

        public void LoadData2(int branch_number, string ls_id, ref long? ll_employee_number, ref string ls_employee_title)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", branch_number }, { "@1", ls_id } };
                    //string sql = "SELECT employees.number,  employees.title FROM employees WHERE (branch_number = @0) AND (employees.password = @1)";
                    string sql = "SELECT employees.number, employees.title FROM employees WHERE employees.id = @1 AND branch_number = @0";
                    
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_employee_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        ls_employee_title = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
        }
    }
}