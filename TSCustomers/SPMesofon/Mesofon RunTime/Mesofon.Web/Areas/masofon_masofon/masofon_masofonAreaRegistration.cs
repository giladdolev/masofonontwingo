using System.Web.Mvc;

namespace masofon.Areas.masofon_masofon
{
    public class masofon_masofonAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "masofon_masofon";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "masofon_masofon_default",
                "masofon_masofon/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "masofon" }
            );
        }
    }
}