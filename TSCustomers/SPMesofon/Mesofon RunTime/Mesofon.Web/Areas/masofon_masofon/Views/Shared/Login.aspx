﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<masofon.Login>" %>

<%@ Import Namespace="System.Web.Mvc.Html" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, user-scalable=0" />
    <title>Masofon</title>
    <link rel="shortcut icon" href="#" />
    <style>
        body{font-family:'Roboto',sans-serif}*{box-sizing:border-box}.container h1{margin-top:0;text-align:center;font-size:3.3em}label{font-size:22px;font-weight:700}.InputListBox{font-size:22px;font-weight:500;text-align:right;width:100%;display:inline-block;background:#f1f1f1}input[type=text]:focus{background-color:#ddd;outline:none}input[type=text],input[type=number],input[type=password]{text-align:center;width:100%;padding:5px;margin:5px 0;display:inline-block;background:#f1f1f1;font-size:22px;font-weight:500;border:none}.inputbig{padding:15px!important}input[type=text]:focus,input[type=password]:focus,input[type=number]:focus{background-color:#ddd;outline:none}hr{border:1px solid #f1f1f1;margin-bottom:25px}.Loginbtn{float:left;color:#fff;padding:14px 20px;margin:8px 0;border:none;cursor:pointer;width:100%;opacity:.9;font-size:22px;text-shadow:0 1px rgba(0,0,0,.1);background-color:#4d90fe}.Loginbtn:disabled{background-color:ButtonFace}.Loginbtn:hover{opacity:1}.container table{width:100%}.container{text-align:center;direction:rtl}.clearfix::after{content:"";clear:both;display:table}.list{text-align:left;direction:ltr}@media screen and(max-width:300px){.Loginbtn{width:100%}}.pass{-webkit-text-security:disc}.validation1{color:red;font-size:30px}#loader{position:absolute;left:53%;top:50%;z-index:1;width:150px;height:150px;margin:-75px 0 0-75px;border:16px solid #f3f3f3;border-radius:50%;border-top:16px solid #1e90ff;width:120px;height:120px;-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}.animate-bottom{position:relative;-webkit-animation-name:animatebottom;-webkit-animation-duration:1s;animation-name:animatebottom;animation-duration:1s}@-webkit-keyframes animatebottom{from{bottom:-100px;opacity:0}to{bottom:0;opacity:1}}@keyframes animatebottom{from{bottom:-100px;opacity:0}to{bottom:0;opacity:1}}
    </style>
    <script type="text/javascript">
        var urlParams = new URLSearchParams(window.location.search), savequeryParam = urlParams.get("savequery"), disableLoginButton = !1; if ("yes" == savequeryParam) localStorage.setItem("QueryStringSession", location.search), disableLoginButton = !1; else try { var queryString = localStorage.getItem("QueryStringSession"); disableLoginButton = null != queryString ? (window.location.href = "/Login" + queryString, !1) : (alert("נא לסגור את האפליקציה ולהיכנס מחדש"), !0) } catch (e) { }
    </script>
</head>
<body>

    <form id="form1" runat="server" onsubmit="return validateForm()">
        <%
            string branchQueryString = Request.QueryString["Branch"];
            if (!string.IsNullOrEmpty(branchQueryString))
            {
                BranchNumber.Text = branchQueryString;

                //disable branch combo
               BranchNumber.Attributes.Add("readonly", "true");
            }

            string StationQueryString = Request.QueryString["Station"];
            if (!string.IsNullOrEmpty(StationQueryString))
            {
                StationNumber.Text = StationQueryString;

                //disable station combo
                StationNumber.Attributes.Add("readonly", "true");
            }

            string PrinterNameQueryString = Request.QueryString["PrinterName"];
            if (!string.IsNullOrEmpty(PrinterNameQueryString))
            {
                lstInfo.Text = string.Format("Printer: {0}{1}", PrinterNameQueryString, Environment.NewLine);
                MvcSite.Common.Globals.PrinterNameState = PrinterNameQueryString;
            }
            string BarcodePrinterNameQueryString = Request.QueryString["BarcodePrinterName"];
            if (!string.IsNullOrEmpty(BarcodePrinterNameQueryString))
            {
                lstInfo.Text += string.Format("Barcode Printer: {0}", BarcodePrinterNameQueryString);
                MvcSite.Common.Globals.BarcodePrinterNameState = BarcodePrinterNameQueryString;
            }
            if (string.IsNullOrEmpty(lstInfo.Text))
            {
                lstInfo.Visible = false;
            }
        %>
        <div class="container">

            <h1>כניסה למסופון</h1>
            <hr />
            <table>
                <tr>
                    <td style="width: 8%">
                        <label>סניף</label>
                    </td>

                    <td>
<%--
                        <asp:TextBox ID="BranchNumber" name="BranchNumber" onfocusout="focusInputOnblur(this);" onfocus="lastFocused=this;" runat="server" TextMode="Number" CssClass="InputListBox" placeholder="סניף" AutoCompleteType="Disabled"></asp:TextBox>
--%>
			<asp:DropDownList runat="server" ID="BranchNumber" ReadOnly="true" CssClass="InputListBox">
             
				<asp:ListItem Value="1">1</asp:ListItem>
				<asp:ListItem Value="2">2</asp:ListItem>
				<asp:ListItem Value="3">3</asp:ListItem>
				<asp:ListItem Value="4">4</asp:ListItem>
				<asp:ListItem Value="5">5</asp:ListItem>
		                <asp:ListItem Value="6">6</asp:ListItem>
		                <asp:ListItem Value="7">7</asp:ListItem>
				<asp:ListItem Value="8">8</asp:ListItem>
				<asp:ListItem Value="9">9</asp:ListItem>
				<asp:ListItem Value="10">10</asp:ListItem>
				<asp:ListItem Value="11">11</asp:ListItem>
				<asp:ListItem Value="12">12</asp:ListItem>
				<asp:ListItem Value="13">13</asp:ListItem>
				<asp:ListItem Value="14">14</asp:ListItem>
				<asp:ListItem Value="15">15</asp:ListItem>
		                <asp:ListItem Selected="True"  ReadOnly="true" Value="15">15</asp:ListItem>
	            </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                    <td>
                        <label>תחנה</label>
                    </td>
                    <td>
                        <asp:TextBox ID="StationNumber" name="StationNumber" onfocusout="focusInputOnblur(this);" onfocus="lastFocused=this;" TextMode="Number" runat="server" CssClass="InputListBox" placeholder="תחנה" AutoCompleteType="Disabled"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width: 50%">
                        <label>ת.ז</label>
                        <input type="radio" onclick="javascript: validateRadio();" name="radio1" id="chooseId" checked="checked" />

                    </td>
                    <td style="width: 50%">
                        <label>סיסמה</label>
                        <input type="radio" onclick="javascript: validateRadio();" name="radio1" id="choosePassword" />
                    </td>
                </tr>
            </table>
            <div id="inputValidation" style="color: red; text-align: right;"></div>
            <div style="color: red; text-align: right;"><%: this.Model.Message %></div>

            <input type="text" id="ValidationType" name="ValidationType" value="UserId" style="display: none;" />
            <asp:TextBox ID="UserId" name="UserId" onfocusout="focusInputOnblur(this);" runat="server" onfocus="lastFocused=this;" TextMode="Number" CssClass="input inputbig" placeholder="ת.ז" AutoCompleteType="Disabled"></asp:TextBox>
            <asp:TextBox ID="password" name="password" TextMode="Number" onfocus="lastFocused=this;" onfocusout="focusInputOnblur(this);" runat="server" CssClass="input pass inputbig" Style="display: none;" AutoCompleteType="Disabled" placeholder="סיסמה"></asp:TextBox>

            <div class="clearfix">
                <asp:Button ID="Loginbtn" runat="server" class="Loginbtn" Text="כניסה" />
                <div id="loader"></div>
            </div>
            <asp:TextBox ID="lstInfo" runat="server" Rows="4" TextMode="MultiLine" Width="100%" CssClass="list"></asp:TextBox>
        </div>
    </form>

    <script type="text/javascript">
        function validateRadio() { document.getElementById("chooseId").checked ? (document.getElementById("UserId").style.display = "inline-block", document.getElementById("password").style.display = "none", document.getElementById("ValidationType").value = "UserId", document.getElementById("UserId").focus()) : document.getElementById("choosePassword").checked && (document.getElementById("UserId").style.display = "none", document.getElementById("password").style.display = "inline-block", document.getElementById("ValidationType").value = "password", document.getElementById("password").focus()) } function validateForm() { var e = document.getElementById("chooseId").checked ? "Id" : "Password", d = document.getElementById("inputValidation"); if ("Id" == e && "" == document.forms.form1.UserId.value) return !(d.innerHTML = "*נא למלא ת.ז"); if ("Password" == e && "" == document.forms.form1.password.value) return !(d.innerHTML = "*נא למלא סיסמה"); return document.getElementById("loader").style.display = "block", !0 } function focusInputOnblur(e) { var d = document.getElementById("chooseId").checked ? "Id" : "Password", t = document.getElementById("UserId"), n = document.getElementById("password"), o = document.getElementById("BranchNumber"), l = document.getElementById("StationNumber"); "" == o.value && "" == l.value ? o.focus() : "" != o.value && "" != l.value ? "Id" == d ? t.focus() : "Password" == d && n.focus() : "" == l.value && l.focus() } window.onload = function () { focusInputOnblur(), document.getElementById("<%=Loginbtn.ClientID %>").disabled = disableLoginButton }, document.getElementById("loader").style.display = "none";
    </script>
</body>
</html>


