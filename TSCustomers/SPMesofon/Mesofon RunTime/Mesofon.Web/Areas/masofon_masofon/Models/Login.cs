﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace masofon
{
    public class Login 
    {
        public string UserId { get; set; }
        public string Password { get; set; }

        public string BranchNumber { get; set; }
        public string StationNumber { get; set; }
        public string Message { get; internal set; }


        public string ValidationType { get; set; }

        public Login()
        {

        }

        
    }
}