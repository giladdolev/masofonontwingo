using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Extensions;
using System.Globalization;
using System.Web.VisualTree.Elements;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.VisualTree.Extensions;
using Common.Transposition.Extensions;
using global;
using mini_terminal;
using Mesofon.Repository;
using Mesofon.Common;
using Mesofon.Common.Global;
using MvcSite.ServiceSecurity;
using CollectionExtensions = System.Extensions.CollectionExtensions;
using masofonAlias = masofon;
using MvcSite.Common;

namespace masofon
{
    // Creation Time:   09/07/2016 10:59:22 (Utc)
    // Comments:        Generated Application Object
    // 
    //--
    public class masofon
    {
        public SQLError sqlca = new SQLError();
        public Exception error;
        public EventArgs message;
        public bool gb_transaction_began;
        public string gs_database_type = "ase";
        public string gs_reg_key;
        public string gs_b2b_incoming_msg_folder;
        public bool gb_ibolt;
        public bool gb_manual_mode;
        public long gl_file_err_level_log;
        public long gl_db_err_level_log;
        public DateTime? gdt_null_date_time;
        public DateTime? gdt_prev;
        public SQLError SQLCA_REPORTS;
        // AlexKh - for masofon
        public u_messagebox guo_msg;
        public nv_translator nvo_translator;
        public string s_reg_key;
        public s_global_variables gs_vars;
        public s_station gs_station;
        public s_global_parameters qs_parameters;
        public IRepository gds_b2b_branch_suppliers;
        public bool gb_b2b_log;
        public IRepository gds_branch_station_params;
        public nvo_reports_db gnvo_reports_db;
        public IRepository gds_parent_supplier_edi;
        public IRepository gds_suppliers_distributors_edi;
        public long gl_msg_type_order = 1;
        public long gl_msg_type_debit = 2;
        public long gl_msg_type_decline = 3;
        public bool gb_im_the_headoffice;
        public int gi_developer_debug_mode;
        public int gi_max_cpu_usage;
        public IRepository gds_global_branch_parameters;
        public IRepository gds_station_hardware_parameters;
        public string gs_user_id;
        public uo_version guo_version;
        public bool gb_b2b_enable = true;
        public bool gb_mt_log;
        public string pic_lib;
        public bool msr4695_exist;
        public decimal i_scr;
        public s_titles gs_title;
        public string help_entry = "";
        public string reg_key;
        public string gs_prefix; // ibm sure pos 500
        public string gs_suffix; // ibm sure pos 500
        public bool gb_ibm500 = false;
        public u_logs_in_table_service guo_logs_in_table_service;
        public s_message gstr_message;
        // AlexKh - 1.1.1.18 - 2014-02-17 - CR#1171(SPUCM00004718) - marlog branch structure
        public IRepository gds_marlog_distributor;
        //Eduardi 2015-09-24 v.1.1.39.2 SPUCM00005427-מדפסת החזרות מרלוג CITIZEN
        public const long BTP_PRINTER_TYPE = 1;
        public const long CITIZEN_PRINTER_TYPE = 2;
        public bool gb_printer_is_citizen;
        private string is_password_argument;

        public DateTime? set_machine_time()
        {
            DateTime? db_time = default(DateTime);
            DateTime? local_time = default(DateTime);
            bool return_local = false;
            int li_file_number = 0;
            // get dB date & time
            // -------------------------------
            if (true)
            {
                LoadData(ref db_time);
            }

            if (masofonAlias.masofon.Instance.sqlca != null && masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {
                //uf_calculate_declines
                return DateTime.Now;
            }
            if (db_time == default(DateTime))
            {
                //uf_calculate_declines
                return DateTime.Now;
            }
            return db_time;
        }
        public bool af_get_reg_db_prifile() // af_get_reg_db_prifile()
        {
            int li_packet_size = 0;
            string ls_temp = null;
            string ls_db_user = null;
            string ls_db_profile = null;
            string ls_reg_db_profile = null;
            string ls_Server = null;
            ls_db_profile = RegToDatabase.GetRegistryItem(gs_reg_key, "database_profile", "SQLCA", "DB Default Profile");
            ls_reg_db_profile = gs_reg_key + ls_db_profile;

            sqlca.DBMS = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "dbms", "ODBC", "DB Vendor"));

            sqlca.Database = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "database", "", "DB data file name"));
            sqlca.ServerName = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "servername", "", "DB Server Name"));
            sqlca.LogID = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "logid", "", "Log Login"));
            sqlca.LogPass = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "logpass", "", "Log Password"));
            sqlca.UserID = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "userid", "", "User Id"));
            sqlca.DBPass = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "dbpass", "", "User Password"));
            li_packet_size = Convert.ToInt32(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "PacketSize", "512", "Network packet size, only for ASE or MS SQL"));
            if (li_packet_size % 256 != 0)
            {
                li_packet_size = li_packet_size / 256 * 256;
            }
            if (li_packet_size < 256)
            {
                li_packet_size = 512;
            }

            sqlca.AutoCommit = true;

            sqlca.DBParm = Convert.ToString(RegToDatabase.GetRegistryItem(ls_reg_db_profile, "dbparm", "ConnectString='DSN=sp_dsn'", "DB Parameters String"));
            ls_temp = RegToDatabase.GetRegistryItem(ls_reg_db_profile, "DataBaseType", "ASE", "ASE = Sybase Enterprise, ASA=Sybase Adaptive").ToLower(CultureInfo.InvariantCulture);
            if (ls_temp != "ase" && ls_temp != "asa") //.. configure as ASE 
            {
                // TODO: Field 'RegString of type 'Sybase.PowerBuilder.RegistryValueType' is unmapped'. (CODE=1004)

                RegToDatabase.RegistrySet(ls_reg_db_profile, "DataBaseType", (RegistryValueType.RegString), "ASE");
                ls_temp = "ase";
            }
            gs_database_type = ls_temp;
            if (gs_database_type == "asa")
            {


                if (sqlca.DBParm.Substring(sqlca.DBParm.Length - 1, default(int)).ToString() != "'")
                {

                    sqlca.DBParm += "'";
                }



                sqlca.DBParm = Convert.ToString(sqlca.DBParm.Substring(0, sqlca.DBParm.Length - 1).ToString() + ";connectionname=MASOFON_" + String.Concat(gs_vars.branch_number, "000") + "_" + String.Concat(gs_station.station_number, "00") + "'");
            }
            else if (gs_database_type == "ase")
            {


                sqlca.DBParm = Convert.ToString(sqlca.DBParm.ToString() + ",AppName='MASOFON_" + String.Concat(gs_vars.branch_number, "000") + "_" + String.Concat(gs_station.station_number, "00") + "',PacketSize=" + li_packet_size.ToString());
            }

            if (sqlca.SqlCode < 0)
            {
                //invo_automatic_reports.uf_write_log ("Connect DataBase! " + "Error connecting to database To Server " +  SQLCA.ServerName + " And DataBase " + &
                //									SQLCA.DataBase + " With User " + SQLCA.Logid + " Error : " + SQLCA.SqlErrText + "~r~n")
                return false;
            }
            else
            {



                ls_Server = "Connect Successfully To Server " + sqlca.ServerName.ToString() + " And DataBase " + sqlca.Database.ToString() + " With User " + sqlca.LogID.ToString() + "\r" + "\n";
                //invo_automatic_reports.uf_write_log ("Connect DataBase! " + ls_Server)
            }
            return true;
        }
        public bool af_get_report_db_profile() //	SQLCA_REPORTS - report DB global transaction object
        {
            bool lb_same_db_parms = false;
            string ls_reg = null;
            string ls_main = null;
            string ls_Server = null;
            int li_minutes = 0;
            int li_packet_size = 0;


            SQLCA_REPORTS = new SQLError();
            //.. Check if Database_Reports_Profile Valid
            ls_reg = RegToDatabase.GetRegistryItem(gs_reg_key, "database_reports_profile", "SQLCA_Reports", "Report DB Default Profile");
            ls_main = RegToDatabase.GetRegistryItem(gs_reg_key, "database_profile", "SQLCA", "DB Default Profile");
            if (ls_main.ToLower(CultureInfo.InvariantCulture) == ls_reg.ToLower(CultureInfo.InvariantCulture) || (string.IsNullOrEmpty(ls_reg) || ls_reg == null))
            {
                //.. same as main db or not setted
                SQLCA_REPORTS = sqlca;
                return false;
            }
            //.. Get Report DB settings..................................................................................


            SQLCA_REPORTS.DBMS = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "dbms", sqlca.DBMS.ToString(), "DB Vendor"));


            SQLCA_REPORTS.Database = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "database", sqlca.Database.ToString(), "DB data file name"));


            SQLCA_REPORTS.ServerName = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "servername", sqlca.ServerName.ToString(), "DB Server Name"));


            SQLCA_REPORTS.LogID = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "logid", sqlca.LogID.ToString(), "Log Login"));
            // TODO: Field 'LogPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'LogPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            SQLCA_REPORTS.LogPass = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "logpass", sqlca.LogPass.ToString(), "Log Password"));
            // TODO: Field 'UserID of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'UserID of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            SQLCA_REPORTS.UserID = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "userid", sqlca.UserID.ToString(), "User Id"));
            // TODO: Field 'DBPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            // TODO: Field 'DBPass of type 'Sybase.PowerBuilder.Transaction' is unmapped'. (CODE=1004)
            SQLCA_REPORTS.DBPass = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "dbpass", sqlca.DBPass.ToString(), "User Password"));

            SQLCA_REPORTS.AutoCommit = true;
            li_packet_size = Convert.ToInt32(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "PacketSize", "512", "Network packet size, only for ASE or MS SQL"));
            if (li_packet_size % 256 != 0)
            {
                li_packet_size = li_packet_size / 256 * 256;
            }
            if (li_packet_size < 256)
            {
                li_packet_size = 512;
            }


            SQLCA_REPORTS.DBParm = Convert.ToString(RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "dbparm", sqlca.DBParm.ToString(), "DB Parameters String"));
            ls_main = RegToDatabase.GetRegistryItem(gs_reg_key + ls_reg, "DataBaseType", "ASE", "ASE = Sybase Enterprise, ASA=Sybase Adaptive").ToLower(CultureInfo.InvariantCulture);
            if (ls_main != "ase" && ls_main != "asa")
            {
                //.. configure ASE 
                // TODO: Field 'RegString of type 'Sybase.PowerBuilder.RegistryValueType' is unmapped'. (CODE=1004)

                RegToDatabase.RegistrySet(gs_reg_key + ls_reg, "DataBaseType", (RegistryValueType.RegString), "ASE");
                ls_main = "ase";
            }


            SQLCA_REPORTS.DBParm = Convert.ToString(SQLCA_REPORTS.DBParm.ToString() + ",AppName='MASOFON_" + String.Concat(gs_vars.branch_number, "000") + "_" + String.Concat(gs_station.station_number, "00") + "SNC',PacketSize=" + li_packet_size.ToString());
            //.. Connect To Reports DB

            if (SQLCA_REPORTS.DBMS.ToLower(CultureInfo.InvariantCulture) == "ODBC")
            {


                lb_same_db_parms = (SQLCA_REPORTS.DBParm.ToString() == sqlca.DBParm.ToString());
            }
            else
            {




                lb_same_db_parms = (SQLCA_REPORTS.ServerName.ToString() == sqlca.ServerName.ToString() && SQLCA_REPORTS.Database.ToString() == sqlca.Database.ToString());
            }
            //invo_automatic_reports.uf_write_log(	" SQLCA_REPORTS.dbms:~t~t" + SQLCA_REPORTS.dbms + "~r~n" + &
            //												" SQLCA_REPORTS.database:~t~t" + SQLCA_REPORTS.database + "~r~n" + &
            //												" SQLCA_REPORTS.servername:~t~t" + SQLCA_REPORTS.servername + "~r~n" + &
            //												" SQLCA_REPORTS.logid:~t~t" + SQLCA_REPORTS.logid + "~r~n" + &
            //												" SQLCA_REPORTS.logpass:~t~t" + SQLCA_REPORTS.logpass + "~r~n" + &
            //												" SQLCA_REPORTS.userid:~t~t" + SQLCA_REPORTS.userid + "~r~n" + &
            //												" SQLCA_REPORTS.dbpass:~t~t" + SQLCA_REPORTS.dbpass + "~r~n" + &
            //												" SQLCA_REPORTS.dbparm:~t~t" + SQLCA_REPORTS.dbparm + "~r~n" + &
            //												" packet_size: " + String(li_packet_size) + "~r~n")
            if (lb_same_db_parms)
            {
                //.. sqlca reports DB equal to main DB
                SQLCA_REPORTS = sqlca;
                ls_Server = "SQLCA REPORTS Server - SQLCA_REPORTS = SQLCA " + "\r" + "\n";
                //invo_automatic_reports.uf_write_log ("Connect DataBase! " + ls_Server)
                return true;
            }
            else
            {

                if (SQLCA_REPORTS.SqlCode != 0)
                {
                    //.. Connect Error
                    //invo_automatic_reports.uf_write_log("Connet DataBase! " + "Error connecting to database To Server " +  SQLCA_REPORTS.ServerName + " And DataBase " + &
                    //				SQLCA_REPORTS.DataBase + " With User " + SQLCA_REPORTS.Logid + " Error : " + SQLCA_REPORTS.SqlErrText)
                    SQLCA_REPORTS = sqlca;
                    ls_Server = "SQLCA REPORTS Server - SQLCA_REPORTS = SQLCA " + "\r" + "\n";
                    //	invo_automatic_reports.uf_write_log ("Connect DataBase! " + ls_Server)
                    return false;
                }
            }



            ls_Server = "Connect Successfully To Server " + SQLCA_REPORTS.ServerName.ToString() + " And DataBase " + SQLCA_REPORTS.Database.ToString() + " With User " + SQLCA_REPORTS.LogID.ToString() + "\r" + "\n";
            //invo_automatic_reports.uf_write_log ("Connect DataBase! " + ls_Server)
            return true;
        }
        public async Task shut_down()
        {
            string ls_ShowText = null;
            string ls_UserControl = null;
            string ls_sql_syntax = null;
            DateTime? ldt = default(DateTime);
            //u_print		luo_print
            long ll_db_version = 0;

            //if (this.get_ToolbarText())
            //{
            //    ls_ShowText = "yes";
            //}
            //else
            //{
            //    ls_ShowText = "no";
            //}
            // TODO: Field 'ToolbarUserControl of type 'masofon.masofon' is unmapped'. (CODE=1004)
            //if (this.get_ToolbarUserControl())
            //{
            //    ls_UserControl = "yes";
            //}
            //else
            //{
            //    ls_UserControl = "no";
            //}
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log Close application
            await guo_logs_in_table_service.uf_write_log_table("Application ShutDown", u_logs_in_table_service.SERVICE_CODE_APPLICATION_STATUS, 1, "Application ShutDown", "", "", -1);
            // AlexKh - 1.1.22.0 - 2014-07-16 - SPUCM00004921 - add log close application
            f_application_run_logClass.f_application_run_log("ShutD", "Dissconnect and ShutDown Application");
            // Check connection to db and update action
            //ls_sql_syntax = "SELECT 1";
            //UpdateData(ls_sql_syntax);

            //if (sqlca.SqlCode == 0)
            //{
            //}
            //if (sqlca != null)
            //{

            //    if (sqlca.DBHandle() > 0)
            //    {
            //    }
            //}
        }
        public string af_get_password_argument()
        {
            return is_password_argument;
        }
        public void af_set_password_argument(string as_arg)
        {
            is_password_argument = as_arg;
        }
        public async Task af_set_globals()
        {
            gs_station.station_number = Convert.ToInt32(Globals.StationNumber);
            if (gs_station.station_number <= 0 || gs_station.station_number == 0)
            {
                await guo_msg.uf_msg("Wrong station parameters", "", "stopsign!", "Incorrect station number");
                await this.shut_down();


            }
            gs_vars.branch_number = Convert.ToInt32(Globals.BranchNumber);
            if (gs_vars.branch_number <= 0 || gs_vars.branch_number == 0)
            {
                await guo_msg.uf_msg("Wrong branch parameters", "", "stopsign!", "Incorrect branch number");
                await this.shut_down();
            }

            gs_vars.LaboratoryBranchNum = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["LaboratoryBranchNum"]);

        }
        public async Task<FuncResults<object, bool>> af_get_global_db_parm_value(string as_paramater_name, string as_parameter_type, string as_default_value, object aref_parameter)
        {
            long ll_count = 0;
            string ls_value = null;
            string ls_error_text = null;
            if (true)
            {
                LoadData1(as_paramater_name, as_parameter_type, ref ls_value);
            }

            if (sqlca.SqlCode != 0)
            {
                //.. not exist in database - insert with default value
                await f_begin_tranClass.f_begin_tran();
                //.. Check if many 
                if (true)
                {
                    LoadData2(as_paramater_name, as_parameter_type, ref ll_count);
                }
                if (ll_count > 1)
                {
                    UpdateData1(as_paramater_name, as_parameter_type);
                }
                UpdateData2(as_paramater_name, as_parameter_type, as_default_value);

                if (sqlca.SqlCode != 0)
                {

                    ls_error_text = sqlca.SqlErrText;
                    await f_rollbackClass.f_rollback();
                    await guo_msg.uf_msg("DB Error", "u_init.uf_get_global_db_parm_value() INSERT global_database_parameters FAIL", "stopsign!", ls_error_text);
                    return FuncResults.Return(aref_parameter, false);
                }
                await f_commitClass.f_commit();
                if (true)
                {
                    LoadData3(as_paramater_name, as_parameter_type, ref ls_value);
                }

                if (sqlca.SqlCode != 0)
                {
                    await guo_msg.uf_msg("DB Error", "u_init.uf_get_global_db_parm_value() RETRIEVE of global_database_parameters FAIL", "stopsign!", "Parameter: " + as_paramater_name + " of type: " + as_parameter_type + "Can Not Be Found in global_database_parameters Table! Check Database!");
                    return FuncResults.Return(aref_parameter, false);
                }
            }
            //.. get value
            switch (as_parameter_type.ToLower(CultureInfo.InvariantCulture))
            {
                case "long":
                    aref_parameter = Convert.ToInt64(ls_value);
                    break;
                case "integer":
                    aref_parameter = Convert.ToInt32(ls_value);
                    break;
                case "string":
                    aref_parameter = ls_value;
                    break;
                case "char":
                    aref_parameter = Convert.ToChar(ls_value);
                    break;
                case "boolean":
                    aref_parameter = (ls_value.ToLower(CultureInfo.InvariantCulture) == "true" || (ls_value.ToLower(CultureInfo.InvariantCulture) == "yes" || ls_value.ToLower(CultureInfo.InvariantCulture) == "y"));
                    break;
                default:
                    if (as_parameter_type.Substring(0, 7) == "decimal")
                    {
                        aref_parameter = Convert.ToDecimal(ls_value);
                    }
                    else
                    {
                        await guo_msg.uf_msg("DB Error", "u_init.uf_get_global_db_parm_value() Parameter Type Invalid", "stopsign!", "Parameter: " + as_paramater_name + " of type: " + as_parameter_type + " Type Invalid!");
                        return FuncResults.Return(aref_parameter, false);

                    }
                    break;
            }
            return FuncResults.Return(aref_parameter, true);
        }
        public async Task af_load_global_db_parameters()
        {
            object ll_any = null;
            if (!(await af_get_global_db_parm_value("b2b_organization_edi", "string", "7290172900007", ll_any)).Retrieve(out ll_any))
            {
                goto shutdown;
            }
            gs_vars.b2b_organization_edi = Convert.ToString(ll_any);
            if (!(await af_get_global_db_parm_value("log_book_defined_by_supplier", "string", "yes", ll_any)).Retrieve(out ll_any))
            {
                goto shutdown;
            }
            gs_vars.log_book_defined_by_supplier = Convert.ToString(ll_any);
            if (!(await af_get_global_db_parm_value("b2b_outgoing_msg_folder", "string", "C:\\Temp\\", ll_any)).Retrieve(out ll_any))
            {
                goto shutdown;
            }
            gs_vars.b2b_outgoing_msg_folder = Convert.ToString(ll_any);
            //Ron@20/07/2015>>SPUCM00005412
            if (!(await af_get_global_db_parm_value("net_price_on_purchase_return", "string", "yes", ll_any)).Retrieve(out ll_any))
            {
                goto shutdown;
            }
            gs_vars.net_price_on_purchase_return = Convert.ToString(ll_any);
            //Eitan; SPUCM00005489; 7/9/2016; 1.39.6;
            if (!(await af_get_global_db_parm_value("b2b_outgoing_msg_folder_temp", "string", "C:\\Temp\\", ll_any)).Retrieve(out ll_any))
            {
                goto shutdown;
            }
            gs_vars.b2b_outgoing_msg_folder_temp = Convert.ToString(ll_any);
            return;
            shutdown:
            await guo_msg.uf_msg("MASOFON Error loading parameter", "", "stopsign!", "Error loading global database parameters!");
            await this.shut_down();


        }
        public async Task<bool> af_get_global_parametets()
        {
            bool lb_employee_updated = false;

            // Code was removed based on 'SystemFunctions.SetPointer(PointerValue) : PointerValue' mappings.
            // Sybase.PowerBuilder.Win.SystemFunctions.SetPointer(System.Web.VisualTree.Elements.CursorsElement.WaitCursor)

            start:
            // AlexkH - 1.2.45.0 - 09-08-2009 - CR#1121 - add new parameter branch_type
            //SharonS - 1.0.0.0 - 2007-05-21 - B2B - Add 2 new parameters: b2b_supported and edi_number
            if (true)
            {
                LoadData4();
            }

            switch (sqlca.SqlCode)
            {
                case -1:

                    await guo_msg.uf_msg("Global Parameters Error", sqlca.SqlErrText, "stopsign!", "Error Getting Global Parameters");
                    await MessageBox.Show(await f_get_error_message_numberClass.f_get_error_message_number(1037), "");
                    return false;
                case 100:
                    if (!lb_employee_updated)
                    {
                        UpdateData3();
                        lb_employee_updated = true;
                        goto start;
                    }

                    await guo_msg.uf_msg("Global Parameters Error", sqlca.SqlErrText, "stopsign!", "Error Getting Global Parameters");

                    // Code was removed based on 'SystemFunctions.SetPointer(PointerValue) : PointerValue' mappings.
                    // Sybase.PowerBuilder.Win.SystemFunctions.SetPointer(System.Web.VisualTree.Elements.CursorsElement.Arrow)

                    return false;
            }
            if (qs_parameters.vat_percent == 0)
            {
                qs_parameters.vat_percent = 0;
            }
            if (f_get_db_versionClass.f_get_db_version() > 153)
            {

                qs_parameters.macabi_tran_code = gds_global_branch_parameters.GetItemValue<long>(0, "macabi_transaction_code");

                qs_parameters.leumi_tran_code = gds_global_branch_parameters.GetItemValue<long>(0, "leumit_transaction_code");
            }
            else
            {
                LoadData5();
            }
            //SharonS - 1.0.0.0 - 2007-05-21 - B2B - Define if the branch is b2b enable - Start
            gb_b2b_enable = ((qs_parameters.b2b_supported == 1) && ((!(qs_parameters.edi_number == null)) && (qs_parameters.edi_number.Trim().Length == 13)));

            // Code was removed based on 'SystemFunctions.SetPointer(PointerValue) : PointerValue' mappings.
            // Sybase.PowerBuilder.Win.SystemFunctions.SetPointer(System.Web.VisualTree.Elements.CursorsElement.Arrow)

            return true;
        }
        public masofon()
        {
            message = new EventArgs();
            sqlca = new SQLError();
            error = new Exception();
            gs_reg_key = "HKEY_CURRENT_USER\\Software\\NET-POS\\";
            gdt_null_date_time = Convert.ToDateTime("01/01/1900").Add(Convert.ToDateTime("00:00:00").TimeOfDay);

            nvo_translator = new nv_translator();
            gnvo_reports_db = new nvo_reports_db();
            guo_msg = new u_messagebox();
            guo_version = new uo_version();
            guo_logs_in_table_service = new u_logs_in_table_service();
            gds_b2b_branch_suppliers = new d_b2b_branches_suppliersRepository();
            gds_branch_station_params = new ds_branch_station_paramsRepository();
            gds_suppliers_distributors_edi = new ds_suppliers_distributorsRepository();
            gds_global_branch_parameters = new lds_global_branch_parametersRepository();
            gds_station_hardware_parameters = new lds_station_hardware_parametersRepository();
            gds_parent_supplier_edi = new dw_parent_supplier_ediRepository();
            gds_b2b_branch_suppliers.Retrieve(int.Parse(Globals.BranchNumber));
            gds_branch_station_params.Retrieve(Convert.ToInt64(Globals.BranchNumber), Convert.ToInt64(Globals.StationNumber));
            gds_parent_supplier_edi.Retrieve();
            gds_suppliers_distributors_edi.Retrieve(int.Parse(Globals.BranchNumber));
            gds_global_branch_parameters.Retrieve(Convert.ToInt64(Globals.BranchNumber));

        }

        public async void open()
        {
            await af_set_globals();
            await af_load_global_db_parameters();
            await af_get_global_parametets();
            s_reg_key = reg_key = gs_reg_key;
            
            // AlexKh - 1.1.24.0 - 2014-09-18 - SPUCM00004921 - add log close application


        }
        public void close()
        {
            if (nvo_translator != null)
            {

 

            }
            if (gnvo_reports_db != null)
            {



            }
            if (guo_msg != null)
            {



            }
            if (guo_version != null)
            {


            }
            if (guo_logs_in_table_service != null)
            {


            }
        }
        public async Task systemerror()
        {
            string ls_message = null;
            // AlexKh - 1.1.1.4 - 2013-03-12 - SPUCM00004018 - add log Error application
            //ls_message = "OBJECT: " + error.GetObject() + "\r" + "\n" + "EVENT: " + error.get_ObjectEvent() + "\r" + "\n" + "LINE: " + error.get_Line().ToString() + "\r" + "\n" + "ERROR: " + error.ToString() + " = " + error.Message;
            ls_message = error.Message;
            await guo_logs_in_table_service.uf_write_log_table(ls_message, u_logs_in_table_service.SERVICE_CODE_APPLICATION_STATUS, 1, "Application Error", "", "", -3);
            // AlexKh - 1.1.22.0 - 2014-07-16 - SPUCM00004921 - add log system error application
            f_application_run_logClass.f_application_run_log("System Error", ls_message);
        }
        public void idle()
        {
            // AlexKh - 1.1.24.0 - 2014-09-18 - SPUCM00004921 - add log close application
            f_application_run_logClass.f_application_run_log("Idle", "Idle for last 30 minutes");
        }
       
        public static masofonAlias.masofon Instance
        {
            get
            {
                return (ApplicationElement.Current["masofon"] ??
                        (ApplicationElement.Current["masofon"] = new masofonAlias.masofon())) as masofon;
            }
        }
        public void LoadData(ref DateTime? db_time)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    string sql = "SELECT getdate() FROM rep_dummy";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        db_time = UnitOfWork.GetValue(resultRS, 0, Convert.ToDateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData(string ls_sql_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_sql_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING SQLCA ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData1(string as_paramater_name, string as_parameter_type)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_paramater_name }, { "@1", as_parameter_type } };
                    string sql = " DELETE global_database_parameters WHERE parameter_name = @0 AND parameter_type = @1";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData2(string as_paramater_name, string as_parameter_type, string as_default_value)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_paramater_name }, { "@1", as_parameter_type }, { "@2", as_default_value } };
                    string sql = "INSERT INTO global_database_parameters (parameter_number,  parameter_name,  parameter_type,  parameter_value) SELECT isnull(max(parameter_number), 0) + 1,  @0,  @1,  @2 FROM global_database_parameters GROUP BY global_database_parameters ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData1(string as_paramater_name, string as_parameter_type, ref string ls_value)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_paramater_name }, { "@1", as_parameter_type } };
                    string sql = "SELECT parameter_value FROM global_database_parameters WHERE parameter_name = @0 AND parameter_type = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_value = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData2(string as_paramater_name, string as_parameter_type, ref long ll_count)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_paramater_name }, { "@1", as_parameter_type } };
                    string sql = "SELECT count(*) FROM global_database_parameters WHERE parameter_name = @0 AND parameter_type = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_count = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData3(string as_paramater_name, string as_parameter_type, ref string ls_value)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", as_paramater_name }, { "@1", as_parameter_type } };
                    string sql = "SELECT parameter_value FROM global_database_parameters WHERE parameter_name = @0 AND parameter_type = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_value = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void UpdateData3()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", gs_vars.branch_number } };
                    string sql = "UPDATE global_parameters SET manager_number = (SELECT min(number) FROM employees WHERE branch_number = @0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData4()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", gs_vars.branch_number } };
                    string sql = "SELECT global_parameters.branch_name,  global_parameters.vat_number,  global_parameters.invoice_header,  global_parameters.source_header,  global_parameters.copy_header,  global_parameters.temp_invoice_header,  global_parameters.vat_percent,  global_parameters.invoice_recipt_header,  global_parameters.recipt_header,  global_parameters.manager_number,  global_parameters.employee_message1,  global_parameters.employee_message2,  global_parameters.employee_message3,  global_parameters.zikuy_header,  global_parameters.data_line1,  global_parameters.data_line2,  global_parameters.auctions_global_client_number,  employees.name,  global_parameters.email,  global_parameters.pharm_number,  global_parameters.b2b_supported,  global_parameters.edi_number,  global_parameters.branch_type FROM global_parameters,  employees WHERE (global_parameters.serial_number = @0) AND (employees.branch_number = @0) AND global_parameters.manager_number = employees.number";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        qs_parameters.company_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        qs_parameters.vat_number = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                        qs_parameters.invoice_header = UnitOfWork.GetValue(resultRS, 2, Convert.ToString);
                        qs_parameters.source_header = UnitOfWork.GetValue(resultRS, 3, Convert.ToString);
                        qs_parameters.copy_header = UnitOfWork.GetValue(resultRS, 4, Convert.ToString);
                        qs_parameters.temp_invoice_header = UnitOfWork.GetValue(resultRS, 5, Convert.ToString);
                        qs_parameters.vat_percent = UnitOfWork.GetValue(resultRS, 6, Convert.ToDecimal);
                        qs_parameters.invoice_receipt_header = UnitOfWork.GetValue(resultRS, 7, Convert.ToString);
                        qs_parameters.receipt_header = UnitOfWork.GetValue(resultRS, 8, Convert.ToString);
                        qs_parameters.manager_number = UnitOfWork.GetValue(resultRS, 9, Convert.ToUInt32);
                        qs_parameters.employee_message1 = UnitOfWork.GetValue(resultRS, 10, Convert.ToString);
                        qs_parameters.employee_message2 = UnitOfWork.GetValue(resultRS, 11, Convert.ToString);
                        qs_parameters.employee_message3 = UnitOfWork.GetValue(resultRS, 12, Convert.ToString);
                        qs_parameters.zikuy_header = UnitOfWork.GetValue(resultRS, 13, Convert.ToString);
                        qs_parameters.r_data_line1 = UnitOfWork.GetValue(resultRS, 14, Convert.ToString);
                        qs_parameters.r_data_line2 = UnitOfWork.GetValue(resultRS, 15, Convert.ToString);
                        qs_parameters.auctions_global_client_number = UnitOfWork.GetValue(resultRS, 16, Convert.ToInt64);
                        qs_parameters.manager_name = UnitOfWork.GetValue(resultRS, 17, Convert.ToString);
                        qs_parameters.email = UnitOfWork.GetValue(resultRS, 18, Convert.ToString);
                        qs_parameters.pharm_number = UnitOfWork.GetValue(resultRS, 19, Convert.ToInt64);
                        qs_parameters.b2b_supported = UnitOfWork.GetValue(resultRS, 20, Convert.ToInt32);
                        qs_parameters.edi_number = UnitOfWork.GetValue(resultRS, 21, Convert.ToString);
                        qs_parameters.branch_type = UnitOfWork.GetValue(resultRS, 22, Convert.ToInt32);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public void LoadData5()
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", gs_vars.branch_number } };
                    string sql = "SELECT macabi_tran_code,  leumi_tran_code FROM global_parameters WHERE global_parameters.serial_number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        qs_parameters.macabi_tran_code = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        qs_parameters.leumi_tran_code = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
