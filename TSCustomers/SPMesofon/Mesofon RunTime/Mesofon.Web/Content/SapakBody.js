﻿Ext.define("VT.ux.SPViewModel", {
    extend: "Ext.app.ViewModel",
    alias: "viewmodel.vm-spviewmodelBody",
    data: {
        row_no: "",
        barcode: "",
        material_name: "",
        expected_material_quantity: "",
        order_quantity: "",
        invoice_pack_quantity: "",
        doc_no_en: "",
        doc_no: "",
        invoice_data: "",
        record: null
    }
});

function VT_WidgetColumnAttachBody(i, a, h, f) {
    var e = "";
    displayRef = Ext.getCmp(h.items.keys[h.items.length - 1]);
    var grid = displayRef.up('grid');
    var rowIndex = parseInt(grid.getView().indexOf(displayRef.el.up('table')));
    var d = h.getViewModel();
    if (i) {
        i.ownerGrid.viewModelContrlos[rowIndex] = d;
    }


    d.set("record", f);
    d.set("row_no", f.data.row_no);
    d.set("barcode", f.data.barcode);
    d.set("material_name", f.data.material_name);
    d.set("order_quantity", f.data.order_quantity);
    d.set("expected_material_quantity", f.data.expected_material_quantity);
    d.set("invoice_pack_quantity", f.data.invoice_pack_quantity);
    d.set("doc_no_en", f.data.doc_no_en);
    var j = f.data.doc_no;
    d.set("doc_no", j);
    e = f.data.invoice_data;
    if (displayRef.xtype == "gridMultiComboBox") {
        var pickerElement = displayRef.picker;
        displayRef.setStore(null);
        displayRef.setStore(e);
        if (j == null || j == "0" || j == "") {
            d.set("doc_no", "");
            displayRef.setselectedText("")

            if (pickerElement) {
                pickerElement.getSelectionModel().deselectAll();
            }
        } else {

            displayRef.setselectedText(j)
            if (pickerElement) {
                var model = pickerElement.getSelectionModel();
                var recs = [];
                pickerElement.store.each(function (rec) {
                    if (rec.data.text == j)
                        recs.push(rec);
                }, pickerElement);
                model.select(recs, false, true);
            }

        }

    }
    displayRef.NotEditableEx = true;
    var g = f.data.doc_no_en;
    if (g > 1) {
        displayRef.setDisabled(false)
    } else {
        displayRef.setDisabled(true)
    }
    var k = Ext.getCmp(h.items.keys[8]);
    var c = f.data.b2b_status;
    if (c == 1) {
        k.setReadOnly(true)
    } else {
        k.setReadOnly(false)
    }
    var b = f.data.declines;
    if (b > 0) {
        h.addCls("TrueFontWeightExpressionDeclineAttribute")
    } else {
        h.removeCls("TrueFontWeightExpressionDeclineAttribute")
    }
    for (var index = 0; index < h.items.length; index++) {
        if (h.items.items[index].resetOriginalValue && h.items.items[index].dataIndex) {
            if (h.items.items[index].xtype == "gridMultiComboBox") {
                var doc_noFromRecord = d.get('record').get(h.items.items[index].dataIndex);
                d.set("doc_no", doc_noFromRecord);
                displayRef.setselectedText(doc_noFromRecord);
            }
            h.items.items[index].setValue(d.get(h.items.items[index].dataIndex));
            h.items.items[index].resetOriginalValue();
        }
    }
}
Ext.define("VT.ux.SPGridComponentBody", {
    extend: "Ext.Container",
    alias: "widget.spgridcomponentBody",
    defaultBindProperty: "value",
    ownerWidgetGrid: "",
    viewModel: "vm-spviewmodelBody",
    setValue: function () {
        this.getViewModel().set("record", this.getWidgetRecord())
    },
    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (a) {
        var b = a;
        b = b.ownerCmp;
        if (b) {
            b = b.ownerGrid;
            if (b) {
                this.ownerWidgetGrid = b.ownerGrid
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid
    },
    height: 80,
    width : 350,
    items: [{
        xtype: "label",
        x: 0,
        y: 0,
        docked: "right",
        style: "background:lightgrey",
        width: "30px",
        cls: "WidgetColumnLabelRowNoCssClass2",
        height: "36px",
        bind: {
            text: "{row_no}"
        },
        dataIndex: "row_no",
    }, {
        xtype: "sptextfield",
        x: 31,
        y: 0,
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "155px",
        height: "36px",
        bind: {
            value: "{barcode}"
        },
        dataIndex: "barcode",
    }, {
        xtype: "textfield",
        x: 0,
        y: 38,
        fieldStyle: "background:lightgrey",
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "180px",
        height: "34px",
        cls: "WidgetColumnDisableCssClass",
        readOnly: true,
        bind: {
            value: "{material_name}"
        },
        dataIndex: "material_name",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }, {
        xtype: "label",
        x: 187,
        y: 0,
        style: "background: lightblue",
        width: "20px",
        height: "36px",
        text: "מ",
        cls: "WidgetColumnLabelCssClass1"
    }, {
        xtype: "sptextfield",
        x: 208,
        y: 0,
        width: "45px",
        height: "36px",
        cls: "WidgetColumnDisableCssClass",
        bind: {
            value: "{expected_material_quantity}",
        },
        dataIndex: "expected_material_quantity",
        listeners: {
            afterrender: function (view, eOpts) {
                var element = view.getEl();
                if (element) {
                    var dom1 = element.dom;
                    if (dom1) {
                        dom1.oninput = function (a, b) {
                            var target = a.target;
                            if (target) {
                                if (target.value == '0')
                                    target.value = '';
                                target.value = target.value.replace(/(\..*)\./g, '$1');
                            }
                        };
                    }
                }
            },
        }
    }, {
        xtype: "label",
        x: 92,
        y: 25,
        hidden: true,
        width: "23px",
        height: "24px",
        cls: "WidgetColumnLabelCssClass",
        text: "ה"
    }, {
        xtype: "sptextfield",
        x: 114,
        y: 25,
        hidden: true,
        width: "34px",
        height: "22px",
        readOnly: true,
        cls: "WidgetColumnDisableCssClass",
        bind: {
            value: "{order_quantity}",
        },
        dataIndex: "order_quantity",
    }, {
        xtype: "label",
        x: 254,
        y: 0,
        style: "background: lightblue",
        cls: "WidgetColumnLabelCssClass1",
        width: "20px",
        height: "36px",
        text: "ח"
    }, {
        xtype: "sptextfield",
        x: 275,
        y: 0,
        width: "45px",
        height: "36px",
        readOnly: true,
        dataIndex: "invoice_pack_quantity",
        bind: {
            value: "{invoice_pack_quantity}",
        },
    }, {
        xtype: "label",
        x: 181,
        y: 38,
        cls: "WidgetColumnLabelCssClass1",
        width: "26px",
        height: "34px",
        text: "מס"
    }, {
        xtype: "sptextfield",
        x: 230,
        y: 25,
        width: "98px",
        height: "22px",
        readOnly: true,
        hidden: true,
        bind: {
            value: "{doc_no_en}",
        },
        dataIndex: "doc_no_en",
    }, {
        xtype: "gridMultiComboBox",
        x: 208,
        y: 38,
        fieldStyle: "background:lightgrey",
        style: "background: lightgrey",
        width: "112px",
        height: "34px",
        bind: {
            value: "{doc_no}",
        },
        dataIndex: "doc_no",
        cls: "WidgetColumnDisableCssClass",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }],
    listeners: {
        afterrender: function (a, b) {
            this.setOwnerGrid(a)
        },
    }
});