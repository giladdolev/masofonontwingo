﻿Ext.define("VT.ux.SPViewModel", {
    extend: "Ext.app.ViewModel",
    alias: "viewmodel.vm-spviewmodelPalletManual",
    data: {
        row_no: "",
        barcode: "",
        material_name: "",
        expected_material_quantity: "",
        order_quantity: "",
        invoice_pack_quantity: "",
        doc_no_en: "",
        doc_no: "",
        declines: "",
        invoice_data: "",
        record: null
    }
});

function VT_WidgetColumnAttachPalletManual(h, a, g, e) {
    var d = "";
    var c = g.getViewModel();
    var j = parseInt(e.data.row_no) - 1;
    if (h) {
        h.ownerGrid.viewModelContrlos[j] = c;
    }

    displayRef = Ext.getCmp(g.items.keys[g.items.length - 1]);
    c.set("record", e);
    c.set("row_no", e.data.row_no);
    c.set("barcode", e.data.barcode);
    c.set("material_name", e.data.material_name);
    c.set("order_quantity", e.data.order_quantity);
    c.set("expected_material_quantity", e.data.expected_material_quantity);
    c.set("invoice_pack_quantity", e.data.invoice_pack_quantity);
    c.set("doc_no_en", e.data.doc_no_en);
    var i = e.data.doc_no;
    c.set("doc_no", i);
    d = e.data.invoice_data;
    if (displayRef.xtype == "gridMultiComboBox") {
        if (i == null || i == "0") {
            c.set("doc_no", "");
            displayRef.setselectedText("")
        }
        displayRef.setStore(d)
    }
    var f = e.data.doc_no_en;
    if (f > 1) {
        displayRef.setDisabled(false)
    } else {
        displayRef.setDisabled(true)
    }
    var b = e.data.declines;
    if (b > 0) {
        g.addCls("TrueFontWeightExpressionDeclineAttribute")
    } else {
        g.removeCls("TrueFontWeightExpressionDeclineAttribute")
    }

    for (var i = 0; i < g.items.length; i++) {
        if (g.items.items[i].resetOriginalValue)
            g.items.items[i].resetOriginalValue();
    }
}
Ext.define("VT.ux.SPGridComponentPalletManual", {
    extend: "Ext.Container",
    alias: "widget.spgridcomponentPalletManual",
    defaultBindProperty: "value",
    ownerWidgetGrid: "",
    viewModel: "vm-spviewmodelPalletManual",
    setValue: function () {
        this.getViewModel().set("record", this.getWidgetRecord())
    },
    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (a) {
        var b = a;
        b = b.ownerCmp;
        if (b) {
            b = b.ownerGrid;
            if (b) {
                this.ownerWidgetGrid = b.ownerGrid
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid
    },
    height: 80,
    items: [{
        xtype: "label",
        x: 0,
        y: 0,
        docked: "right",
        style: "background:lightgrey",
        width: "30px",
        cls: "WidgetColumnLabelRowNoCssClass2",
        height: "36px",
        bind: {
            text: "{row_no}"
        },
        dataIndex: "row_no",
    }, {
        xtype: "sptextfield",
        x: 31,
        y: 0,
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "155px",
        height: "36px",
        bind: {
            value: "{barcode}"
        },
        dataIndex: "barcode",
    }, {
        xtype: "textfield",
        x: 0,
        y: 38,
        fieldStyle: "background:lightgrey",
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "180px",
        height: "34px",
        cls: "WidgetColumnDisableCssClass",
        readOnly: true,
        bind: {
            value: "{material_name}"
        },
        dataIndex: "material_name",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }, {
        xtype: "label",
        x: 187,
        y: 0,
        style: "background: lightblue",
        width: "20px",
        height: "36px",
        text: "מ",
        cls: "WidgetColumnLabelCssClass1"
    }, {
        xtype: "sptextfield",
        x: 208,
        y: 0,
        width: "45px",
        height: "36px",
        cls: "WidgetColumnDisableCssClass",
        bind: {
            value: "{expected_material_quantity}",
        },
        dataIndex: "expected_material_quantity",
        listeners: {
            afterrender: function (view, eOpts) {
                var element = view.getEl();
                if (element) {
                    var dom1 = element.dom;
                    if (dom1) {
                        dom1.oninput = function (a, b) {
                            var target = a.target;
                            if (target) {
                                if (target.value == '0')
                                    target.value = '';
                                target.value = target.value.replace(/(\..*)\./g, '$1');
                            }
                        };
                    }
                }
            },
        }
    }, {
        xtype: "label",
        x: 254,
        y: 0,
        style: "background: lightblue",
        cls: "WidgetColumnLabelCssClass1",
        width: "20px",
        height: "36px",
        text: "ח"
    }, {
        xtype: "sptextfield",
        x: 275,
        y: 0,
        width: "45px",
        height: "36px",
        readOnly: true,
        dataIndex: "invoice_pack_quantity",
        bind: {
            value: "{invoice_pack_quantity}",
        },
    }, {
        xtype: "label",
        x: 181,
        y: 38,
        cls: "WidgetColumnLabelCssClass1",
        width: "26px",
        height: "34px",
        text: "מס"
    }, {
        xtype: "sptextfield",
        x: 230,
        y: 38,
        width: "98px",
        height: "22px",
        readOnly: true,
        hidden: true,
        dataIndex: "doc_no_en",
        bind: {
            value: "{doc_no_en}",
        },
        dataIndex: "doc_no_en",
    }, {
        xtype: "gridMultiComboBox",
        x: 208,
        y: 38,
        fieldStyle: "background:lightgrey",
        dataIndex: "invoice_number",
        style: "background: lightgrey",
        width: "112px",
        height: "34px",
        bind: {
            value: "{doc_no}",
        },
        dataIndex: "doc_no",
        cls: "WidgetColumnDisableCssClass",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }],
    listeners: {
        afterrender: function (a, b) {
            this.setOwnerGrid(a)
        },
    }
});