﻿Ext.define("VT.ux.SPViewModel", {
    extend: "Ext.app.ViewModel",
    alias: "viewmodel.vm-spviewmodelReturnsDetails",
    data: {
        row_no: "",
        barcode: "",
        material_name: "",
        material_quantity: "",
        supplier_number: "",
        declines: "",
        invoice_data: "",
        record: null
    }
});

function VT_WidgetColumnAttachReturnsDetails(j, b, i, g) {
    var e = "";
    displayRef = Ext.getCmp(i.items.keys[i.items.length - 1]);
    var grid = displayRef.up('grid');
    var rowIndex = parseInt(grid.getView().indexOf(displayRef.el.up('table')));
    var d = i.getViewModel();

    if (j) {
        j.ownerGrid.viewModelContrlos[rowIndex] = d;
    }

    d.set("record", g);
    d.set("row_no", g.data.row_no);
    d.set("barcode", g.data.barcode);
    d.set("material_name", g.data.material_name);
    d.set("material_quantity", g.data.material_quantity);
    d.set("supplier_number", g.data.supplier_number);
    var c = g.data.declines;
    d.set("declines", c);
    if (displayRef.xtype == "gridMultiComboBox") {
        if (c == null || c == "0" || c == "") {
            d.set("declines", "");
            g.declines = '';
            d.set("record", g);
            displayRef.setselectedText("")
            var pickerElement = displayRef.picker;
            if (pickerElement) {
                pickerElement.getSelectionModel().deselectAll(true);
            }
        }
        else {
            var pickerElement = displayRef.picker;
            if (pickerElement) {
                var model = pickerElement.getSelectionModel();
                var recs = [];
                pickerElement.store.each(function (rec) {
                    if (rec.data.text == c)
                        recs.push(rec);
                }, pickerElement);
                model.select(recs, false, true);
            }
        }
        e = g.data.invoice_data;
        displayRef.setStore(e);
        displayRef.getPicker().getColumns()[0].setHidden(true);
        displayRef.getPicker().getColumns()[1].setWidth(120);
        displayRef.setEditable(false);
    }
    var h = Ext.getCmp(i.items.keys[1]);
    var a = Ext.getCmp(i.items.keys[4]);
    var f = g.data.doc_state;
    if (f == "" || g.data.barcode == "" || g.data.barcode == null) {
        h.setReadOnly(false);
        a.setReadOnly(false);
        displayRef.setDisabled(false)
    }
    for (var index = 0; index < i.items.length; index++) {
        if (i.items.items[index].resetOriginalValue && i.items.items[index].dataIndex) {
            if (i.items.items[index].xtype == "gridMultiComboBox") {
                var doc_noFromRecord = d.get('record').get(i.items.items[index].dataIndex);
                if (doc_noFromRecord == '0')
                    doc_noFromRecord = '';
                i.items.items[index].setValue(doc_noFromRecord);
                d.set("declines", doc_noFromRecord);
                displayRef.setselectedText(doc_noFromRecord);
            } else {
                i.items.items[index].setValue(d.get(i.items.items[index].dataIndex));
            }

            i.items.items[index].resetOriginalValue();
        }
    }
}
Ext.define("VT.ux.SPGridComponentReturnsDetails", {
    extend: "Ext.Container",
    alias: "widget.spgridcomponentReturnsDetails",
    defaultBindProperty: "value",
    ownerWidgetGrid: "",
    viewModel: "vm-spviewmodelReturnsDetails",
    setValue: function () {
        this.getViewModel().set("record", this.getWidgetRecord())
    },
    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (a) {
        var b = a;
        b = b.ownerCmp;
        if (b) {
            b = b.ownerGrid;
            if (b) {
                this.ownerWidgetGrid = b.ownerGrid
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid
    },
    height: 80,
    items: [{
        xtype: "label",
        x: 0,
        y: 0,
        docked: "right",
        style: "background:lightgrey",
        width: "30px",
        cls: "WidgetColumnLabelRowNoCssClass2",
        height: "36px",
        bind: {
            text: "{row_no}"
        },
        dataIndex: "row_no",
    }, {
        xtype: "sptextfield",
        x: 31,
        y: 0,
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "155px",
        height: "36px",
        bind: {
            value: "{barcode}"
        },
        dataIndex: "barcode",
    }, {
        xtype: "textfield",
        x: 0,
        y: 38,
        fieldStyle: "background:lightgrey",
        style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
        width: "172px",
        height: "34px",
        cls: "WidgetColumnDisableCssClass",
        readOnly: true,

        bind: {
            value: "{material_name}"
        },
        dataIndex: "material_name",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }, {
        xtype: "label",
        x: 187,
        y: 0,
        style: "background: lightblue",
        width: "20px",
        height: "36px",
        text: "כ",
        cls: "WidgetColumnLabelCssClass1"
    }, {
        xtype: "sptextfield",
        x: 208,
        y: 0,
        width: "45px",
        height: "36px",
        cls: "WidgetColumnDisableCssClass",
        bind: {
            value: "{material_quantity}",
        },
        dataIndex: "material_quantity",
    }, , {
        xtype: "label",
        x: 254,
        y: 0,
        style: "background: lightblue",
        cls: "WidgetColumnLabelCssClass1",
        width: "20px",
        height: "36px",
        text: "ס"
    }, {
        xtype: "sptextfield",
        x: 275,
        y: 0,
        width: "45px",
        height: "36px",
        readOnly: true,
        dataIndex: "supplier_number",
        bind: {
            value: "{supplier_number}",
        },
    }, {
        xtype: "label",
        x: 173,
        y: 38,
        cls: "WidgetColumnLabelCssClass1",
        width: "26px",
        height: "34px",
        text: "ס"
    }, , {
        xtype: "gridMultiComboBox",
        x: 200,
        y: 38,
        dataIndex: "invoice_number",
        width: "120px",
        height: "34px",
        bind: {
            value: "{declines}",
        },
        dataIndex: "declines",
        cls: "WidgetColumnDisableCssClass",
        listeners: {
            beforerender: function () {
                this.inputType = 'text';
            }
        }
    }],
    listeners: {
        afterrender: function (a, b) {
            this.setOwnerGrid(a)
        },
    }
});