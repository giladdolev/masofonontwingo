﻿//Custom.js 

// Variables
var cnp = Ext.util.Format.numberRenderer('0.00');
var lastActiveElement = document.activeElement;
window.lastInputElement = null;
// End

// Functions
function SP_RowTemplate(t, e, i, n) {
    try { var d = Ext.id(); return Ext.defer(function () { Ext.create("Ext.panel.Panel", { renderTo: d, layout: { type: "absolute" }, height: 50, width: 326, x: 0, y: 0, items: [{ xtype: "textfield", x: 34, y: 0, fieldStyle: "background:lightgrey", style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;", width: "150px", height: "24px", value: i.get("materials_material_name"), readOnly: !0 }, { xtype: "textfield", x: 187, y: 0, width: "105px", height: "22px", readOnly: !0, value: i.get("materials_barcode"), listeners: { render: function (t, e) { window.Ext.getCmp(this.el.up("div.x-grid").id).ownerGrid; t.getEl().on("dblclick", function (t, e, i) { var n = window.Ext.getCmp(this.el.up("div.x-grid").id); n.fireEvent("clientEvent", n.ownGrid, "dblclick,barcode") }) } } }, { xtype: "label", x: 295, y: 0, docked: "right", style: "background:lightgrey", width: "30px", height: "50px", text: n + 1 }, { xtype: "Grid-Combo", x: 34, y: 25, fieldStyle: "background:lightgrey", dataIndex: "invoice_number", style: "background: lightgrey", width: "126px", height: "22px", readOnly: !0, value: i.get("invoice_number") }, { xtype: "label", x: 163, y: 25, style: "background: lightblue", width: "20px", height: "24px", text: "מס" }, { xtype: "textfield", x: 186, y: 25, fieldStyle: "background:lightgrey", dataIndex: "inv_pack_quantity", width: "30px", height: "22px", readOnly: !0, value: i.get("inv_pack_quantity") }, { xtype: "label", x: 219, y: 25, style: "background: lightblue", width: "20px", height: "24px", text: "ח" }, { xtype: "textfield", x: 240, y: 25, width: "29px", height: "22px", dataIndex: "expected_material_quantity", readOnly: !0, value: i.get("expected_material_quantity"), listeners: { render: function (t, e) { window.Ext.getCmp(this.el.up("div.x-grid").id).ownerGrid; t.getEl().on("dblclick", function (t, e, i) { var n = window.Ext.getCmp(this.el.up("div.x-grid").id); n.fireEvent("clientEvent", n.ownGrid, "dblclick,expected_material_quantity") }) } } }, { xtype: "label", x: 272, y: 25, style: "background: lightblue", width: "20px", height: "24px", text: "מ" }], listeners: { render: function (t, e) { var i = window.Ext.getCmp(this.el.up("div.x-grid").id).ownerGrid; i.getView().all.count > 0 && i.getView().getCell(0, i.getColumns()[0]) && (i.getView().getCell(0, i.getColumns()[0]).dom.height = 50) } } }) }, 50), Ext.String.format('<div id="{0}"></div>', d) } catch (t) { }
}
function SP_ButtonButton(t, e, i, n, d, r, l, a) {
    try {
        var x = Ext.id();
        return Ext.defer(function () {
            Ext.create("Ext.panel.Panel", {
                renderTo: Ext.query("#" + x)[0],
                layout: {
                    type: "absolute"
                },
                bodyStyle: "background:transparent;",
                height: 40,
                width: 115,
                cls: 'ButtonButtonPadingPanel',
                x: 0,
                y: 0,
                items: [{
                    x: 0,
                    xtype: "button",
                    text: "ידני",
                    cls: 'ButtonButtonPading',
                    hidden: l.ownerGrid.get1State(i),
                    listeners: {
                        click: function (t) {
                            var e = window.Ext.getCmp(this.container.el.up("div.x-grid").id);
                            e.fireEvent("clientEvent", e.ownGrid, "click,b_manual")
                        }
                    }
                }, {
                    xtype: "button",
                    text: "אוט",
                    cls: 'ButtonButtonPading',
                    hidden: l.ownerGrid.get2State(i),
                    listeners: {
                        click: function (t) {
                            var e = window.Ext.getCmp(this.container.el.up("div.x-grid").id);
                            e.fireEvent("clientEvent", e.ownGrid, "click,b_automatic")
                        }
                    }
                }
                ]
            })
        }, 50),
                Ext.String.format('<div id="{0}"></div>', x)
    } catch (t) { }
}
function hideimage() {
    document.getElementById("pdfloading").style.visibility = "hidden";
}
function detectBlur() {
    //console.info('detectBlur', arguments);
}
function detectFocus() {
    // Add logic to detect focus and to see if it has changed or not from the lastActiveElement. 

    if (document.activeElement.nodeName.toLowerCase() == 'input')
        window.lastInputElement = document.activeElement;

    //console.info('document.activeElement changed', document.activeElement);
    //console.info('window.lastInputElement changed', window.lastInputElement);
    if (window.lastInputElement && document.activeElement.contains(window.lastInputElement) && window.lastInputElement.id.includes('sptextfield')) {
        window.lastInputElement.focus();
    }

}
function attachEvents() {
    window.addEventListener ? window.addEventListener('focus', detectFocus, true) : window.attachEvent('onfocusout', detectFocus);
    window.addEventListener ? window.addEventListener('blur', detectBlur, true) : window.attachEvent('onblur', detectBlur);
}


function VT_getItemIdPathEx(record) {

    if (record) {
        // Get parent record
        var parentRecord = record.findParentByType('panel');
        if (typeof parentRecord == 'undefined') {
            parentRecord = record.findParentByType('viewport');
        }

        // Get full path
        if (parentRecord)
            return VT_getItemIdPathEx(parentRecord) + "/" + record.getItemId();
        else
            return record.getItemId();

    } else {
        return "";
    }

}
function getParent(cmp) {
    var parent = cmp.up('panel[xtype="panel"]');
    return parent;
}
Ext.util.Format.decimal2places = function (v) {
    return cnp(v);
};
// End

// Function Calls
attachEvents();
// End

// Events
window.onbeforeunload = function (e) {
    try {
        if (window.isUnloaded == true) {
            return false;
        }

        if (!window.NotValidAuthentication) {
            var e = e || window.event;
            var msg = "Do you really want to leave this page?Your work will be lost."

            // For IE and Firefox
            if (e) {
                e.returnValue = msg;
            }

            window.isUnloaded = true;

            // For Safari / chrome
            return msg;
        }
    } catch (err2) {

    }
};

window.onhashchange = function () {
    debugger;
}

window.onunload = function (e) {
    WindowClosing();
}

function WindowClosing() {
    try {
        //VT_raiseEvent("w_mini_terminal", "WindowClosing", {});

        // Create the event object
        eventObject = {
            id: "w_mini_terminal", name: "WindowClosing"
        };

        // Push new event to queue
        var events = [];
        events.push(eventObject);

        // Call the server
        Ext.Ajax.request({
            url: "/VisualTree/ProcessEvents",
            method: 'POST',
            params: {
                appId: VT_APPID
            },
            jsonData: events,
            success: function (response, opts) {

            },
            failure: function (response, opts) {
            }
        });
    } catch (err) {

    }
}
// End

// Overrides
Ext.override(Ext.panel.Panel, {
    scrollable: true,
    focusable: false,

    onScrollEnd: function (x, y) {
        //alert('onScrollEnd ' + x+',' + y);

    }
});
Ext.override(Ext.form.field.Base, {
    isDirtyValue: false,
    isOnInit: true,
    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        me.on("focusenter", me.focusenterHandler, me);
        me.on("focusleave", me.focusleaveHandler, me);
        me.on("change", me.changeHandler, me);
        me.on("afterrender", me.afterrenderHandler, me);

        //if contain css class "inputtypetext" then change the input type to text
        if (this.cls == "inputtypetext") {
            this.inputType = "text"
        }

    },
    afterrenderHandler: function () {
        this.isOnInit = false;
    },
    focusElement: function (e, timeout) {
        var fromCMP = e.fromComponent;
        if (fromCMP && fromCMP.el && fromCMP.el.dom) {
            var className = fromCMP.el.dom.className;
            if (className) {
                if (!className.includes('focus')) {
                    //console.log("from    " + e.fromComponent.xtype + "    , to " + e.toComponent.xtype);
                    if (timeout) {
                        setTimeout(function () {
                            fromCMP.preventRaisewidgetControlFocusEnter = true;
                            var cmp1 = fromCMP.focus(false);
                            fromCMP.preventRaisewidgetControlFocusEnter = false;
                        }, 80);
                    } else {
                        //if the element focused , no need to focus again
                        if (fromCMP.inputEl && fromCMP.inputEl.dom && fromCMP.inputEl.dom == window.lastInputElement) {
                            return;
                        }
                        fromCMP.preventRaisewidgetControlFocusEnter = true;
                        var cmp1 = fromCMP.focus(false);
                        if (cmp1 && cmp1.xtype == 'Grid-Combo') {
                            cmp1.collapse();
                        }
                        fromCMP.preventRaisewidgetControlFocusEnter = false;
                    }
                }
            }
        }
    },
    onFocusLeave: function (e) {
        // Start - Sencha Code
        var me = this;

        if (me.destroying || me.destroyed) {
            return;
        }


        //prevent focus on non-editable component
        if (!e.toComponent.editable && e.toComponent.xtype != 'Grid-Combo' && e.toComponent.xtype != 'container') {

            //prevent selection range when input is number
            if (e.fromComponent.inputType == 'number') {
                e.fromComponent.inputEl.dom.setSelectionRange = null;
            }

            var pnl = e.fromComponent.up('panel');
            if (pnl && pnl.isVisible && pnl.isVisible() == false) {
                return;
            }
            var grid = e.fromComponent.up('grid');
            //console.log("from    " + e.fromComponent.xtype + "    , to " + e.toComponent.xtype)
            //Focus from component
            if (e.fromComponent.xtype == "sptextfield" && grid && grid.xtype == "VTWidgetGrid" && grid.bufferedRenderer && grid.bufferedRenderer.refreshing) {
                return;
            } else if (grid && grid.getView() && grid.getView().refreshing) {
                return;
            }
            else if (e.toComponent.xtype == 'messagebox') {
            }
            else if (e.fromComponent.xtype == 'sptextfield' && e.toComponent.xtype == 'loadmask') {

            }
            else if (e.fromComponent.xtype == 'gridMultiComboBox' && e.toComponent.xtype == 'viewport') {
                this.focusElement(e, false);
            }
            else if (e.fromComponent.xtype == 'gridMultiComboBox' && e.toComponent.xtype == 'tableview') {
                try {
                    this.focusElement(e, false);
                } catch (err3) {
                }

            }
            else if (e.fromComponent.xtype == 'sptextfield' && e.toComponent.xtype == 'viewport'
                || e.fromComponent.xtype == 'sptextfield' && e.toComponent.xtype == 'tableview') {

                //if toComponent is GridMultiComboBox so exit
                if ((e.toComponent.ownerGrid && e.toComponent.ownerGrid.PickerField)) {
                    return;
                }
                this.focusElement(e, false);
            }
            else if (e.toComponent.xtype == 'tableview') {
                var toPicker;
                var toGrid = e.toComponent.up('grid');
                if (toGrid) {
                    toPicker = toGrid.pickerField;
                }
                var fromPicker;
                var fromGrid = e.fromComponent.up('grid');
                if (fromGrid) {
                    fromPicker = fromGrid.pickerField;
                }
                if (toPicker || fromPicker || toGrid.xtype == 'VTWidgetGrid') {
                    //ComboGrid
                } else if (e.fromComponent.xtype != 'Grid-Combo') {
                    if (toGrid && toGrid.xtype != 'VTgridpanel') {
                        this.focusElement(e, true);
                    }
                }
            }
            else {

                var tableview = e.fromComponent.up('tableview');
                if (tableview && e.target) {
                    var record = tableview.getRecords(e.target);
                    if (record && record.length > 0) {
                        this.focusElement(e, false);
                    }
                } else {
                    var cmp1 = this.focusElement(e, true);
                }

            }
            return;
        } else if (e.fromComponent.xtype == 'textfield' && e.toComponent.xtype == 'container' && String(e.toComponent.getItemId()).startsWith('rw_pallet')) {
            this.focusElement(e, false);
        }

        me.focusEnterEvent = null;
        me.containsFocus = false;
        me.fireEvent('focusleave', me, e);
        // End - Sencha Code
    },
    validatorHandler: function (val) {
        if (val == '') {
            var grid = this.findParentByType('grid');
            if (grid) {
                var plugin = grid.getPlugin('cellEditing');
                if (plugin) {
                    setTimeout(function (plugin, rowIndex, columnIndex) {
                        plugin.startEditByPosition({
                            row: rowIndex, column: columnIndex
                        });
                    }, 1, plugin, this.ownerCt.context.rowIdx, this.ownerCt.context.colIdx);
                }
            }
        } else {

        }

        return true;
    },
    focusenterHandler: function (view, e) {
        try {
            var toCmp = e.toComponent;
            if (toCmp) {
                var toOwner = toCmp.ownerCt;
                if (toOwner && toOwner.componentCls == 'x-editor') {
                    // if (typeof toCmp.validator == 'undefined')
                    // toCmp.validator = this.validatorHandler;
                }
                var parent = getParent(toCmp);
                if (parent) {
                    toCmp.SpecificWindowHeight = 626;
                    if (typeof toCmp.windowExHeight == 'undefined') {
                        toCmp.windowExHeight = window.innerHeight;
                    }
                    if (typeof toCmp.parentEx == 'undefined') {
                        toCmp.parentEx = parent;
                        toCmp.parentExHeight = parent.height;
                        parent.on('hide', this.hideparentHandler, parent);
                    }
                    var containerEx = VT_GetCmp('pnlContent');
                    if (containerEx) {
                        if (typeof toCmp.containerEx == 'undefined') {
                            toCmp.containerEx = containerEx;
                            toCmp.containerExHeight = containerEx.height;
                        }
                    }

                    if (toCmp.parentEx.getItemId() == 'uo_shipments_list' ||
                        toCmp.parentEx.getItemId() == 'uo_trade_site' ||
                        toCmp.parentEx.getItemId() == 'uo_crossdoc_list') {
                        toCmp.parentEx.setHeight(403);
                        toCmp.containerEx.setHeight(403);
                    }

                    setTimeout(function (toCmp) {
                        var newHeight = window.innerHeight;
                        var keyboardHeight = (toCmp.windowExHeight - newHeight);
                        //Set heights
                        if (toCmp.containerEx.body.el.dom) {
                            toCmp.containerEx.body.el.dom.style.removeProperty('overflow');
                            toCmp.containerEx.body.el.dom.style.setProperty('height', '100%');
                            toCmp.containerEx.body.el.dom.style.setProperty('width', '100%');
                        }
                        if (toCmp.parentEx.body.el.dom) {
                            toCmp.parentEx.body.el.dom.style.setProperty('height', '100%');
                            toCmp.parentEx.body.el.dom.style.setProperty('width', '100%');
                        }


                        //Get top of component
                        if (toCmp.el.dom) {
                            var offsetTop = toCmp.el.dom.offsetTop;
                            if (offsetTop) {
                                toCmp.parentEx.scrollTo(0, offsetTop);
                            }
                        }
                    }, 300, toCmp);
                }
            }
        } catch (err) {
            console.log(err.message);
        }
    },
    hideparentHandler: function (view) {
        try {
            var containerEx = VT_GetCmp('pnlContent');
            if (containerEx) {
                containerEx.setHeight(626);
            }
            var entrance = VT_GetCmp('uo_entrance');
            if (entrance) {
                entrance.setHeight(625);
            }
        } catch (err) {
        }
    },
    focusleaveHandler: function (view, e) {
        var cmp = e.fromComponent;
        if (cmp.parentEx && cmp.containerEx) {
            cmp.parentEx.setHeight(cmp.parentExHeight);
            cmp.containerEx.setHeight(cmp.containerExHeight);
        }
    },
    changeHandler: function (view, newValue, oldValue, eOpts) {
        if (this.ownerCt && this.ownerCt.completeEdit && this.ownerCt.componentCls == 'x-editor') {
            var grid = view.up('grid');
            if (grid != null && grid.xtype == 'VTgridpanel') {
                grid.dirtyCellValue = newValue;
                grid.hasDirtyCell = true;
                VT_GridPanelID = VT_getItemIdPathEx(grid);
            }
        }
    }
});
Ext.override(Ext.form.field.Text, {
    selectText: function (start, end) {
        var me = this,
          el = me.inputEl.dom,
          v = el.value,
          len = v.length,
      range;
        if (el.type == "number") {
            if (me.id && me.id != '') {
                var inputElement = document.getElementById(me.id + "-inputEl");
                if (inputElement && inputElement.value != '' && inputElement.select && !inputElement.readOnly) {
                    inputElement.select();
                }
            }

        } else {
            me.callParent(arguments);
        }
    }
});
Ext.override(Ext.grid.Panel, {
    bufferedRenderer: false,
    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        this.store.parent = me;
        this.store && this.store.on({
            load: {
                fn: function () {
                    //Reset flags 
                    var grid = this.parent;
                    if (grid) {
                        //clear selection
                        grid.selectedRowIndex = -1;
                        grid.getSelectionModel().deselectAll(true);

                        //reset flags for widgetitemchange
                        grid.lastDataIndex = null;
                        grid.lastValue = null;
                        grid.obj = null;
                    }
                }
            },
            single: true
        })
        this.store && this.store.on({
            remove: {
                fn: function () {
                    //Reset flags for widgetitemchange event
                    var grid = this.parent;
                    if (grid) {
                        grid.lastDataIndex = null;
                        grid.lastValue = null;
                        grid.obj = null;
                    }
                }
            },
            single: true
        })
    },
    reconfigure: function () {
        var grd = this;

        grd.completeCellEditing();
        grd.callParent(arguments);
        grd.store && grd.store.on({
            load: {
                fn: function () {
                    var gridcombo = grd.findParentByType('Grid-Combo');
                    if (gridcombo) {
                        gridcombo.selectedRowIndex = -1;
                    }

                    clearTimeout(grd.syncStartWithEditCellTimer);
                    grd.syncStartWithEditCellTimer = setTimeout(function () {
                        try {
                            if (grd.getItemId() == 'dw_body') {
                                var rowIndex = 0;
                                var columnIndex = 1;
                                if (rowIndex >= 0 && columnIndex >= 0) {
                                    var cell = grd.getView().getCell(rowIndex, grd.getColumns()[columnIndex]);
                                    if (cell && !cell.Protect) {
                                        var plugin = grd.getPlugin('cellEditing');
                                        if (plugin) {

                                            if (plugin) {
                                                plugin.startEditByPosition({ row: rowIndex, column: columnIndex });
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (err2) {

                        }
                    }, 100);
                }
            },
            single: true
        })
        grd.store && grd.store.on({
            remove: {
                fn: function () {
                    //Reset flags for widgetitemchange event
                    var grid = this.parent;
                    if (grid) {
                        grid.lastDataIndex = null;
                        grid.lastValue = null;
                        grid.obj = null;
                    }
                }
            },
            single: true
        })
    },
    completeCellEditing: function () {
        this.plugins && Ext.Array.forEach(this.plugins, function (item) {
            if (Ext.getClassName(item) === 'Ext.grid.plugin.CellEditing') {
                item.completeEdit();
            }
        });
    },

});


Ext.override(Ext.Button, {
    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        me.on("afterrender", me.afterRenderHandler, me);
    },
    afterRenderHandler: function (view, e) {
        try {
            var pr = getParent(view);
            if (typeof pr != 'undefined') {
                var parent = pr.itemId;
                if (view.itemId == "cb_finish" && parent == "uo_entrance") {
                    view.on('click', view.ClickHandler, view)
                } 
                } 
        } catch (err) {

        }
    },
    ClickHandler: function (view, e) {
        try {
            var queryString = localStorage.getItem("QueryStringSession");
            window.NotValidAuthentication = true;
            window.location.href = "/Login" + queryString;
        } catch (err) {

        }
    }
});



// End

