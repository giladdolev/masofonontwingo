﻿Ext.define("VT.ux.SPViewModel", {
    extend: "Ext.app.ViewModel",
    alias: "viewmodel.vm-spviewmodelSapak_dw_body",
    data: {
        record: null,

        //Need cmps
        doc_type: "",
        number: "",
        supply_date: "",
        invoice_total: "",
        b2b_reference_number: "",

        //No need
        state: "",
        create_mode: "",
        b2b_status: "",
        payment_code_number: "",
        payments_number: "",
        distributor_number: "",
        row_saved: "",
        row_no: ""
    }
});



function VT_WidgetColumnAttachSapak_dw_body(h, a, g, e) {
    var d = "";
    var c = g.getViewModel();

    c.set("record", e);
    c.set("row_no", e.data.row_no);
    c.set("doc_type", e.data.doc_type);
    c.set("number", e.data.number);
    c.set("supply_date", e.data.supply_date);
    c.set("invoice_total", e.data.invoice_total);
    c.set("b2b_reference_number", e.data.b2b_reference_number);
    c.set("state", e.data.state);
    c.set("create_mode", e.data.create_mode);
    c.set("b2b_status", e.data.b2b_status);
    c.set("payment_code_number", e.data.payment_code_number);
    c.set("payments_number", e.data.payments_number);
    c.set("distributor_number", e.data.distributor_number);
    c.set("row_saved", e.data.row_saved);

    var doc_typeElement = g.items.items[0];
    var numberElement = g.items.items[1];
    var supply_dateElement = g.items.items[2];
    var invoice_totalElement = g.items.items[3];
    var b2b_reference_numberElement = g.items.items[4];

    var grid = doc_typeElement.up('grid');
    var rowIndex = parseInt(grid.getView().indexOf(doc_typeElement.el.up('table')));
    h.ownerGrid.viewModelContrlos[rowIndex] = c;
    c.set("row_no", rowIndex + 1);

    var b2b_status = e.data.b2b_status;
    var state = e.data.state;
    var row_saved = e.data.row_saved;
    var toFocus = false;
    if (b2b_status == 1 || state != 'O' || row_saved == 1) {
        doc_typeElement.inputEl.addCls('sapak_dw_body');
        numberElement.inputEl.addCls('sapak_dw_body');
        supply_dateElement.inputEl.addCls('sapak_dw_body');
        invoice_totalElement.inputEl.addCls('sapak_dw_body');


        doc_typeElement.setReadOnly(true);
        numberElement.setReadOnly(true);
        supply_dateElement.setReadOnly(true);
        invoice_totalElement.setReadOnly(true);
    }
    else {
        doc_typeElement.inputEl.removeCls('sapak_dw_body');
        numberElement.inputEl.removeCls('sapak_dw_body');
        supply_dateElement.inputEl.removeCls('sapak_dw_body');
        invoice_totalElement.inputEl.removeCls('sapak_dw_body');

        doc_typeElement.setReadOnly(false);
        numberElement.setReadOnly(false);
        supply_dateElement.setReadOnly(false);
        invoice_totalElement.setReadOnly(false);
        toFocus = true;

    }
    b2b_reference_numberElement.setReadOnly(true);


    var cnt = (rowIndex * 10);
    doc_typeElement.setTabIndex(9000 + cnt);
    numberElement.setTabIndex(9001 + cnt);
    supply_dateElement.setTabIndex(9002 + cnt);
    invoice_totalElement.setTabIndex(9003 + cnt);
    b2b_reference_numberElement.setTabIndex(9004 + cnt);

    for (var i = 0; i < g.items.length; i++) {
        if (g.items.items[i].resetOriginalValue && g.items.items[i].dataIndex) {
            g.items.items[i].setValue(c.get(g.items.items[i].dataIndex));
            g.items.items[i].resetOriginalValue();
        }
    }

    if (toFocus) {
        try {
            var grid = numberElement.up('grid');
            if (numberElement.getValue() == '') {
                setTimeout(function (grid, rowIndex, numberElement) {
                    try {
                        grid.getView().getNode(rowIndex).PreventHighlight = true;
                        grid.getSelectionModel().select(rowIndex);
                        numberElement.inputEl.dom.focus();
                    } catch (err2) {
                    }
                }, 80, grid, rowIndex, numberElement);
            }
        }
        catch (err) {
        }
    } else if (grid.store.data.items[grid.store.data.items.length - 1].id == '_id') {
    }
    else {
        setTimeout(function (numberElement) { try { numberElement.inputEl.dom.focus(); } catch (err2) { } }, 80, numberElement);
    }
}

Ext.define("VT.ux.SPGridComponentSapak_dw_body", {
    extend: "Ext.Container",
    alias: "widget.spgridcomponentSapak_dw_body",
    defaultBindProperty: "value",
    ownerWidgetGrid: "",
    viewModel: "vm-spviewmodelSapak_dw_body",
    cls: "SPGridComponentSapak_dw_body",
    setValue: function () {
        this.getViewModel().set("record", this.getWidgetRecord())
    },
    layout: {
        type: "absolute"
    },
    setOwnerGrid: function (a) {
        var b = a;
        b = b.ownerCmp;
        if (b) {
            b = b.ownerGrid;
            if (b) {
                this.ownerWidgetGrid = b.ownerGrid
            }
        }
    },
    getOwnerGrid: function () {
        return this.ownerWidgetGrid
    },
    height: 80,
    items: [
        {
            xtype: "combo",
            x: 31,
            y: 0,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "80px",
            height: "36px",
            store: {
                fields: ['comboName', 'comboValue'],
                data: [
					{
					    comboName: "חשבונית",
					    comboValue: "I"
					},
					{
					    comboName: "ת.משלוח",
					    comboValue: "P"
					}
                ]
            },
            displayField: "comboName",
            valueField: "comboValue",
            autoSelect:false,
            typeAhead: true,
            queryMode: "local",
            editable: false,
            bind: {
                value: "{doc_type}"
            },
            dataIndex: "doc_type",
            listeners: {
                afterrender: function (view, eOpts) {
                    if (view.inputEl && view.inputEl.dom) {
                        view.inputEl.dom.style.fontSize = "14px"
                        view.inputEl.dom.style.paddingRight = "1px";
                        view.inputEl.dom.style.marginTop = "-3px";
                    }

                },
                specialkey: function (view, event, eOpts) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid) {
                            if (event.keyCode == 13 || event.keyCode == 9) {
                                if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue() && view.preventRaisewidgetControlFocusEnter != true) {
                                    //Raise  widgetControlItemChanged
                                    try {
                                        view.preventRaisewidgetControlItemChange = true;
                                        var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                        var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                        // Listen for the event.
                                        elem = grid.el.dom;
                                        elem.addEventListener('widgetControlItemChanged', function (e) {
                                            e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                            this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                        }, false);

                                        // Dispatch the event.
                                        elem.dispatchEvent(event);

                                    } catch (err) {
                                        console.log("error :- specialkey - Raise widgetControlItemChanged : " + err.message)
                                    }
                                    view.preventRaisewidgetControlItemChange = false;
                                    view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                    view.resetOriginalValue();
                                }
                                else {
                                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                    if (rowIndex >= 0) {
                                        var controls = getWidgetColumnControls(grid, rowIndex);
                                        if (controls && controls[1])
                                            controls[1].focus();
                                    }
                                }

                            }
                        }
                    } catch (err) {
                        console.log("error :- text box keydown event : " + err.message)
                    }
                },
                blur: function (view, e, o) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid && !grid.bufferedRenderer.refreshing && !grid.FocusingRow && view.preventRaisewidgetControlFocusEnter != true && !grid.lockbyuser) {
                            view.preventRaisewidgetControlItemChange = true;
                            if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue()) {
                                var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                // Listen for the event.
                                elem = grid.el.dom;
                                elem.addEventListener('widgetControlItemChanged', function (e) {
                                    e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                    this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                }, false);
                                // Dispatch the event.
                                elem.dispatchEvent(event);

                                view.preventRaisewidgetControlItemChange = false;
                                view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                view.resetOriginalValue();
                            }
                        }
                    } catch (err) {
                        console.log("error :- text box blur event : " + err.message)
                    }
                },
                expand: function (view, eOpts) {
                    try{
                        var matchedItem = this.getStore().findRecord(this.displayField, this.getValue(), 0, false, false, true);
                        if (matchedItem) {
                            var indxMatch = this.getStore().indexOf(matchedItem);
                            var picker = view.getPicker();
                            var items = picker.getNodes();
                            var item = items[indxMatch];
                            if (item) {
                                picker.highlightItem(item);
                            }
                        }
                    } catch (err) {

                    }
                },
            }
        },
        {
            xtype: "textfield",
            x: 143,
            y: 0,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "106px",
            height: "36px",
            bind: {
                value: "{number}"
            },
            dataIndex: "number",
            value: '',
            enableKeyEvents: true,
            isInit: true,
            // Remove spinner buttons, and arrow key and mouse wheel listeners
            hideTrigger: true,
            keyNavEnabled: false,
            mouseWheelEnabled: false,
            inputType: 'number',
            listeners: {
                blur: function (view, e, o) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid && !grid.bufferedRenderer.refreshing && !grid.FocusingRow && view.preventRaisewidgetControlFocusEnter != true) {
                            view.preventRaisewidgetControlItemChange = true;
                            if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue()) {
                                var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                // Listen for the event.
                                elem = grid.el.dom;
                                elem.addEventListener('widgetControlItemChanged', function (e) {
                                    e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                    this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                }, false);
                                // Dispatch the event.
                                elem.dispatchEvent(event);

                                view.preventRaisewidgetControlItemChange = false;
                                view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                view.resetOriginalValue();
                            }
                        }

                    } catch (err) {
                        console.log("error :- text box blur event : " + err.message)
                    }
                },
                specialkey: function (view, event, eOpts) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid) {
                            if (event.keyCode == 13 || event.keyCode == 9) {
                                if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue() && view.preventRaisewidgetControlFocusEnter != true) {
                                    //Raise  widgetControlItemChanged
                                    try {
                                        view.preventRaisewidgetControlItemChange = true;
                                        var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                        var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                        // Listen for the event.
                                        elem = grid.el.dom;
                                        elem.addEventListener('widgetControlItemChanged', function (e) {
                                            e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                            this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                        }, false);

                                        // Dispatch the event.
                                        elem.dispatchEvent(event);

                                    } catch (err) {
                                        console.log("error :- specialkey - Raise widgetControlItemChanged : " + err.message)
                                    }
                                    view.preventRaisewidgetControlItemChange = false;
                                    view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                    view.resetOriginalValue();
                                }
                                else {
                                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                    if (rowIndex >= 0) {
                                        var controls = getWidgetColumnControls(grid, rowIndex);
                                        if (controls && controls[2])
                                            controls[2].focus();
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        console.log("error :- text box keydown event : " + err.message)
                    }
                }

            },
        },
        {
            xtype: "datefield",
            format: "d/m/y",
            submitFormat: "d/m/y",
            allowBlank: false,
            x: 250,
            y: 0,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "95px",
            height: "35px",
            selectOnFocus:true,
            bind: {
                value: "{supply_date}"
            },
            dataIndex: "supply_date",
            listeners: {
                afterrender: function (view, eOpts) {
                    if (view.inputEl && view.inputEl.dom) {
                        view.inputEl.dom.style.fontSize = "14px"
                        view.inputEl.dom.style.paddingRight = "1px";
                    }

                },
                specialkey: function (view, event, eOpts) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid) {
                            if (event.keyCode == 13 || event.keyCode == 9) {
                                var storedValue = Ext.Date.parse(view.ownerCt.getWidgetRecord().get(view.dataIndex), 'Y-m-d');
                                var newDate = view.getValue();
                                var sameDate = (storedValue.getFullYear() == newDate.getFullYear() && storedValue.getDate() == newDate.getDate() && storedValue.getMonth() == newDate.getMonth());
                                if (!sameDate && view.preventRaisewidgetControlFocusEnter != true) {
                                    //Raise  widgetControlItemChanged
                                    try {
                                        view.preventRaisewidgetControlItemChange = true;
                                        var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                        var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                        // Listen for the event.
                                        elem = grid.el.dom;
                                        elem.addEventListener('widgetControlItemChanged', function (e) {
                                            e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                            this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                        }, false);

                                        // Dispatch the event.
                                        elem.dispatchEvent(event);

                                    } catch (err) {
                                        console.log("error :- specialkey - Raise widgetControlItemChanged : " + err.message)
                                    }
                                    view.preventRaisewidgetControlItemChange = false;
                                    view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                    view.resetOriginalValue();
                                }
                                else {
                                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                    if (rowIndex >= 0) {
                                        var controls = getWidgetColumnControls(grid, rowIndex);
                                        if (controls && controls[3])
                                            controls[3].focus();
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        console.log("error :- text box keydown event : " + err.message)
                    }
                },
                blur: function (view, e, o) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid && !grid.bufferedRenderer.refreshing && !grid.FocusingRow && view.preventRaisewidgetControlFocusEnter != true) {
                            view.preventRaisewidgetControlItemChange = true;
                            var storedValue = Ext.Date.parse(view.ownerCt.getWidgetRecord().get(view.dataIndex), 'Y-m-d');
                            var newDate = view.getValue();
                            var sameDate = (storedValue.getFullYear() == newDate.getFullYear() && storedValue.getDate() == newDate.getDate() && storedValue.getMonth() == newDate.getMonth());
                            if (!sameDate) {
                                var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                var event = new CustomEvent('widgetControlItemChanged', { detail: { view: grid, rowIndex: rowIndex, grid: grid, txtfield: view } });
                                // Listen for the event.
                                elem = grid.el.dom;
                                elem.addEventListener('widgetControlItemChanged', function (e) {
                                    e.detail.grid.fireEvent('widgetControlItemChanged', e.detail.grid, e.detail.rowIndex, e.detail.txtfield.getValue(), e.detail.txtfield.dataIndex);
                                    this.removeEventListener('widgetControlItemChanged', arguments.callee)
                                }, false);
                                // Dispatch the event.
                                elem.dispatchEvent(event);

                                view.preventRaisewidgetControlItemChange = false;
                                view.ownerCt.getWidgetRecord().set(view.dataIndex, view.getValue());
                                view.resetOriginalValue();
                            }
                        }

                    } catch (err) {
                        console.log("error :- text box blur event : " + err.message)
                    }
                },
            }
        },
        {
            xtype: "sptextfield",
            x: 72,
            y: 38,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "98px",
            height: "34px",
            listeners: {
                blur: function (object) {
                    var newValue = object.value;
                    var oldValue = object.value;
                    newValue = Ext.util.Format.decimal2places(newValue.replace(/[^0-9\.]/g, ''));
                    if (newValue == '$NaN.00') object.setValue(oldValue); else object.setValue(newValue);
                },
                change: function (object, newValue, oldValue) {
                    if (object.wasDirty != undefined)
                        return;
                    newValue = Ext.util.Format.decimal2places(newValue.replace(/[^0-9\.]/g, ''));
                    if (newValue == '$NaN.00') object.setValue(oldValue); else object.setValue(newValue);
                },
                specialkey: function (view, event, eOpts) {
                    var grid = this.ownerCt.getOwnerGrid();
                    try {
                        if (grid) {
                            if (event.keyCode == 13 || event.keyCode == 9) {
                                if (view.ownerCt.getWidgetRecord().get(view.dataIndex) != view.getValue() && view.preventRaisewidgetControlFocusEnter != true) {

                                }
                                else {
                                    var rowIndex = grid.getView().indexOf(this.el.up('table'));
                                    if (rowIndex >= 0) {
                                        var controls = getWidgetColumnControls(grid, rowIndex);
                                        if (controls && controls[0])
                                            controls[0].focus();
                                    }
                                }

                            }
                        }
                    } catch (err) {
                        console.log("error :- text box keydown event : " + err.message)
                    }
                },
            },
            bind: {
                value: "{invoice_total}"
            },
            dataIndex: "invoice_total",

        },
        {
            xtype: "sptextfield",
            x: 259,
            y: 38,
            style: "background: lightgrey;white-space: nowrap !important;text-align: right !important;word-break: break-all !important;direction: rtl !important;",
            width: "87px",
            height: "34px",
            bind: {
                value: "{b2b_reference_number}"
            },
            dataIndex: "b2b_reference_number",
        },
        {
            xtype: "label",
            x: 112,
            y: 0,
            cls: "WidgetColumnLabelCssClass1",
            width: "30px !important",
            height: "36px !important",
            text: "מס"
        },
        {
            xtype: "label",
            x: 171,
            y: 38,
            style: "background: lightblue",
            width: "87px",
            height: "34px",
            text: "מס משלוח",
            cls: "WidgetColumnLabelCssClass1"
        },
        {
            xtype: "label",
            x: 31,
            y: 38,
            style: "background: lightblue",
            width: "40px",
            height: "34px",
            text: "סה\"כ",
            cls: "WidgetColumnLabelCssClass1"
        },
        {
            xtype: "label",
            x: 0,
            y: 0,
            docked: "right",
            style: "background:lightgrey",
            width: "30px",
            cls: "WidgetColumnLabelRowNoCssClass1",
            height: "72px",
            bind: {
                text: "{row_no}"
            },
            dataIndex: "row_no",
        },

    ],
    listeners: {
        afterrender: function (a, b) {
            this.setOwnerGrid(a)
        },
    }
});

