﻿using MvcSite.ServiceSecurity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;
using System.Web.SessionState;
using log4net;

namespace MvcSite.Common
{
    public static class Globals
    {

        //Define a static logger variable so that it references the Logger instance named "Program"
        public static readonly ILog log = LogManager.GetLogger(typeof(Globals));

        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private static HttpSessionState State
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }
        static public List<RegistryItem> UserRegistryItemsState
        {
            get { return (List<RegistryItem>)State["Common.Globals.UserRegistryItems"]; }
            set { State["Common.Globals.UserRegistryItems"] = value; }
        }
        static public string BranchNumberState
        {
            get { return (string)State["Common.Globals.BranchNumber"]; }
            set { State["Common.Globals.BranchNumber"] = value; }
        }
        
        static public string StationNumberState
        {
            get { return (string)State["Common.Globals.StationNumber"]; }
            set { State["Common.Globals.StationNumber"] = value; }
        }
        static public string UserIDState
        {
            get { return (string)State["Common.Globals.UserID"]; }
            set { State["Common.Globals.UserID"] = value; }
        }
        //gilad test
        static public string giladtestCheckBox
        {
            //get { return (string)State["Common.Globals.giladtestCheckBox"]; }
            //set { State["Common.Globals.giladtestCheckBox"] = value; }

            get { return State["giladtestCheckBox"] as string; }
            set { State["giladtestCheckBox"] = value; }
        }
        static public string PasswordState
        {
            get { return (string)State["Common.Globals.Password"]; }
            set { State["Common.Globals.Password"] = value; }
        }
        
        static public string DeviceIDState
        {
            get { return (string)State["Common.Globals.DeviceID"]; }
            set { State["Common.Globals.DeviceID"] = value; }
        }

        static public string EmployeeNumberState
        {
            get { return State["Common.Globals.EmployeeNumber"] as string; }
            set { State["Common.Globals.EmployeeNumber"] = value; }
        }


        public static string QueryStringState
        {
            get { return State["QueryString"] as string; }
            set { State["QueryString"] = value; }
        }

        public static string PrinterNameState
        {
            get { return State["PrinterName"] as string; }
            set { State["PrinterName"] = value; }
        }

        public static string BarcodePrinterNameState
        {
            get { return State["BarcodePrinterName"] as string; }
            set { State["BarcodePrinterName"] = value; }
        }


        public static List<RegistryItem> UserRegistryItems
        {
            get { return ApplicationElement.Current["RegistryItems"] as List<RegistryItem>; }
            set { ApplicationElement.Current["RegistryItems"] = value; }
        }

        public static string BranchNumber
        {
            get { return ApplicationElement.Current["BranchNumber"] as string; }
            set { ApplicationElement.Current["BranchNumber"] = value; }
        }
        public static string StationNumber
        {
            get { return ApplicationElement.Current["StationNumber"] as string; }
            set { ApplicationElement.Current["StationNumber"] = value; }
        }

        public static string UserID
        {
            get { return ApplicationElement.Current["UserID"] as string; }
            set { ApplicationElement.Current["UserID"] = value; }
        }

        public static string Password
        {
            get { return ApplicationElement.Current["Password"] as string; }
            set { ApplicationElement.Current["Password"] = value; }
        }

        public static string DeviceID
        {
            get { return ApplicationElement.Current["DeviceID"] as string; }
            set { ApplicationElement.Current["DeviceID"] = value; }
        }

        public static string EmployeeNumber
        {
            get { return ApplicationElement.Current["EmployeeNumber"] as string; }
            set { ApplicationElement.Current["EmployeeNumber"] = value; }
        }
        public static string IsSendDeclineMsg
        {
            get { return ApplicationElement.Current["IsSendDeclineMsg"] as string; }
            set { ApplicationElement.Current["IsSendDeclineMsg"] = value; }
        }


        public static string QueryString
        {
            get { return ApplicationElement.Current["QueryString"] as string; }
            set { ApplicationElement.Current["QueryString"] = value; }
        }

        public static string PrinterName
        {
            get { return ApplicationElement.Current["PrinterName"] as string; }
            set { ApplicationElement.Current["PrinterName"] = value; }
        }

        public static string BarcodePrinterName
        {
            get { return ApplicationElement.Current["BarcodePrinterName"] as string; }
            set { ApplicationElement.Current["BarcodePrinterName"] = value; }
        }


    }
}