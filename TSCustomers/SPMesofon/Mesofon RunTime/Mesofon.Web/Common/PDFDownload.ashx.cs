﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Extensions.Printing;
using System.IO;
using System.Linq;
using System.Web;

namespace MvcSite.Common
{
    /// <summary>
    /// Summary description for PDFDownload
    /// </summary>
    public class PDFDownload : System.Web.UI.Page, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string fileLocation = Convert.ToString(context.Request["location"]);
            if (!string.IsNullOrEmpty(fileLocation))
            {
                //Get Images of given pdf
                List<Image> lstImages = PdfToImage.Convert2(fileLocation);

                //Merge all images
                using (Image image = PdfToImage.MergeFiles(lstImages))
                {
                    if (lstImages.Count > 0)
                    {
                        //Show merged image.
                        context.Response.ContentType = "image/png";
                        image.Save(context.Response.OutputStream, ImageFormat.Png);
                    }
                    context.Response.Flush();
                }
                
                //Clean up lstImages
                foreach (Image img in lstImages)
                {
                    img.Dispose();
                }
                lstImages = null;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}