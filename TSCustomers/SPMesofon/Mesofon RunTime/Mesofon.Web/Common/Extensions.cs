﻿using mini_terminal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Utilities;

namespace Mesofon.Common
{
    public enum DragModes
    {
        Begin
    }

    public enum MaskDataType
    {
        DateTimeMask,
        DateMask
    }


    public static class Extensions
    {


        /// <summary>
        /// Sets the current column in a GridElement
        /// </summary>
        /// <param name="column">The column you want to make current. Column can be a column number or a column name.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs</returns>
        /// <exception>If column is less than 1 or greater than the number of columns, SetColumn fails</exception>
        public static int SetColumn(this GridElement element, object column)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtains the number of the current column. The current column is the column that has focus.
        /// </summary>
        /// <returns>Returns the number of the current column in GridElement. Returns 0 if no column is current (because all the columns have a tab value of 0, making all of them uneditable), and –1 if an error occurs.</returns>
        public static object GetColumn(this GridElement element)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtains the name of the current column
        /// </summary>
        /// <returns>Returns the name of the current column in GridElement. Returns the empty string (“”) if no column is current or if an error occurs.</returns>
        public static string GetColumnName(this GridElement element)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Highlights or removes highlights from rows in a DataWindow control or DataStore. You can select all rows or a single row. SelectRow does not affect which row is current
        /// </summary>
        /// <param name="row">A value identifying the row you want to select or deselect. Specify 0 to select or deselect all rows.</param>
        /// <param name="select">A boolean value that determines whether the row is selected or not selected</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs</returns>
        public static int SelectRow(this GridElement element, int row, bool select)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        /// Sets the value of an item in a value list. SetValue does not affect the data stored in the column.
        /// </summary>
        /// <param name="column">The column that contains the value list or code table. Column can be a column number or a column name. </param>
        /// <param name="index">The number of the item in the value list or code table for which you want to set the value.</param>
        /// <param name="value">A string whose value is the new value for the item. For a code table, use a tab (~t in PowerBuilder) to separate the display value from the data value ("Texas~tTX"). The data value must be a string that can be converted to the datatype of the column.</param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs</returns>
        public static int SetValue(this GridElement element, object column, int index, string value)
        {
            throw new NotImplementedException();
        }





        public static void Clear(this GridElement element)
        {
            // Stub for Super Pharm.
            // Deletes selected text in the edit control over the current row and column, but does not store it in the clipboard.
            throw new NotImplementedException();
        }


        public static void itemchanged(this GridElement element, int v1, ControlElement controlElement, string v2)
        {
            // Stub for Super Pharm.
            // Occurs when a field in a DataWindow control has been modified and loses focus (for example, the user presses Enter, the Tab key, or an arrow key, or clicks the mouse on another field within the DataWindow).
            throw new NotImplementedException();
        }

        public static void rowfocuschanging(this GridElement element, int v1, int v2)
        {
            // Stub for Super Pharm.
            // Occurs when the current row changes in the DataWindow.
            throw new NotImplementedException();
        }

        /// <summary>
        /// ItemFocusChangedAction
        /// http://infocenter-archive.sybase.com/help/index.jsp?topic=/com.sybase.dc37783_1150/html/dwref/CAICBAFA.htm
        /// Occurs when the current item in the control changes.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="row">Long by value. The number of the row containing the item that just gained focus</param>
        /// <param name="item">DWObject by value. A reference to the column containing the item.</param>
        public static void ItemFocusChanged(this GridElement element, int row, ControlElement item)
        {
            // Stub for Super Pharm.
            // Occurs when the current row changes in the DataWindow.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Occurs when a field has been modified, the field loses focus (for example, the user presses Enter, Tab, or an arrow key or clicks the mouse on another field in the DataWindow), and the data in the field does not pass the validation rules for its column.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="row"></param>
        /// <param name="item"></param>
        public static void ItemError(this GridElement element, int row, VisualElement item, object newValue)
        {
            // Stub for Super Pharm.
            // Occurs when the current row changes in the DataWindow.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Occurs when the current row changes in the DataWindow.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="newRow">Number. The number of the row that has just become current</param>
        public static void rowfocuschanged(this GridElement element, int newRow)
        {
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }

        public static void dberror(this GridElement element, long sqldbcode, string sqlerrtext, string sqlsyntax, object buffer, long row)
        {
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }
        /// <summary>
        /// Occurs when the user double-clicks in a DataWindow control.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="xpos">Integer by value. The distance of the pointer from the left side of the DataWindow’s workspace. The distance is given in pixels.</param>
        /// <param name="ypos">Integer by value. The distance of the pointer from the top of the DataWindow’s workspace. The distance is given in pixels.</param>
        /// <param name="row">Long by value. The number of the row the user double-clicked.</param>
        /// <param name="dwo">DWObject by value. A reference to the control within the DataWindow the user double-clicked.</param>
        public static void doubleclicked(this GridElement element, int xpos, int ypos, long row, object dwo)
        {
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }

        /// <summary>
        /// Occurs when the user clicks anywhere in a DataWindow control.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="xpos">Integer by value. The distance of the pointer from the left side of the DataWindow’s workspace. The distance is given in pixels.</param>
        /// <param name="ypos">Integer by value. The distance of the pointer from the top of the DataWindow’s workspace. The distance is given in pixels.</param>
        /// <param name="row">Long by value. The number of the row the user clicked.</param>
        /// <param name="dwo">DWObject by value. A reference to the control within the DataWindow the user clicked.</param>
        public static void clicked(this GridElement element, int xpos, int ypos, long row, object dwo)
        {
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }






        public static string GetBandAtPointer(this GridElement element)
        {
            // Stub for Super Pharm.
            // Reports the band in which the pointer is currently located, as well as the row number associated with the band. The bands are the headers, trailers, and detail areas of the DataWindow and correspond to the horizontal areas of the DataWindow painter.
            throw new System.NotImplementedException();
        }
        /// <summary>
        /// The position of the scroll box in the horizontal scroll bar. Use HorizontalScrollMaximum with HorizontalScrollPosition to synchronize horizontal scrolling in multiple DataWindow objects.
        /// </summary>
        /// <param name="v"></param>
        public static void set_HorizontalScrollPosition(this GridElement element, string v)
        {
            // avi - dont need in web
            return;
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }

        /// <summary>
        /// The maximum width of the scroll box of the DataWindow’s horizontal scroll bar. This value is set by PowerBuilder based on the layout of the DataWindow object and the size of the DataWindow control. Use HorizontalScrollMaximum with HorizontalScrollPosition to synchronize horizontal scrolling in multiple DataWindow objects.
        /// </summary>
        /// <returns></returns>
        public static string get_HorizontalScrollMaximum(this GridElement element)
        {
            // avi - dont need in web
            return "";
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }
        /// <summary>
        /// Occurs when the retrieval for the DataWindow or DataStore is complete.
        /// </summary>
        /// <param name="rowCount">The number of rows that were retrieved.</param>
        public static void retrieveend(this GridElement element, long rowCount)
        {
            // Stub for Super Pharm.
            throw new NotImplementedException();
        }


        /// <summary>
        /// Determines whether the row is selected.
        /// </summary>
        /// <param name="row">Row number</param>
        /// <returns>Boolean. Returns true if the row is selected and false if it is not selected.</returns>
        public static bool IsSelected(this GridElement element, int row)
        {
            throw new NotImplementedException();
        }


        public static string GetObjectAtPointer(this GridElement element)
        {
            throw new NotImplementedException();
        }

        #region Drag
        public static void DragIcon(this GridElement element, string s)
        {
            throw new NotImplementedException();
        }

        public static void Drag(this GridElement element, DragModes dragModes)
        {
            throw new NotImplementedException();
        }

        public static object DraggedObject()
        {
            throw new NotImplementedException();
        }
        #endregion Drag

        public static void SetMask(MaskDataType value, Task<string> value2)
        {
            throw new NotImplementedException();
        }

        public static MaskDataType get_MaskDataType()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Download PDF
        /// </summary>
        /// <param name="fileFullPathSupplierPDF">Full path</param>
        /// <param name="nameFileTarget">Target file name</param>
        public static async Task DownloadPDF(string fileFullPathSupplierPDF, string nameFileTarget = null)
        {
            if (System.IO.File.Exists(fileFullPathSupplierPDF))
            {
                w_pdf_viewer frm = await WindowHelper.Open<w_pdf_viewer>("mini_terminal_mini_terminal", "pdf", fileFullPathSupplierPDF);
                await frm.ShowDialog();
            }
            else
            {

            }

        }



        public static bool CompareEx(this object obj, object another)
        {
            if (ReferenceEquals(obj, another))
                return true;

            if (obj.GetType() != another.GetType())
                return false;

            //properties: int, double, DateTime, etc, not class
            if (!obj.GetType().IsClass)
                return obj.Equals(another);

            var result = true;
            foreach (var property in obj.GetType().GetProperties())
            {
                var objValue = property.GetValue(obj);
                var anotherValue = property.GetValue(another);
                //Recursion
                //if (!objValue.DeepCompare(anotherValue)) result = false;

                if ((objValue != null) && (anotherValue != null))
                {
                    if ((objValue == null) && (anotherValue != null) || (objValue != null) && (anotherValue == null))
                        return false;

                    if (!objValue.Equals(anotherValue))
                        result = false;
                }
            }
            return result;
        }

        public static bool DataColumnsEquals(IList<GridColumn> columns1, IList<GridColumn> columns2)
        {
            if (columns1 == null || columns2 == null)
                return false;

            if (columns1.Count != columns2.Count)
                return false;


            for (int i = 0; i < columns1.Count; i++)
            {
                var col1 = columns1[i];
                var col2 = columns2[i];

                // TODO: improve comparer
                if (col1.GetType() != col2.GetType() ||
                    !String.Equals(col1.ID, col2.ID) ||
                     !String.Equals(col1.HeaderText, col2.HeaderText))
                {
                    return false;
                }
            }


            return true;
        }

        public static bool DataColumnsEquals(DataTable table1, DataTable table2)
        {
            if (table1 == null || table2 == null)
                return false;

            if (table1.Columns.Count != table2.Columns.Count)
                return false;

            for (int i = 0; i < table1.Columns.Count; i++)
            {
                var colname1 = table1.Columns[i].ColumnName;
                var colname2 = table2.Columns[i].ColumnName;

                if (!colname1.Equals(colname2))
                {
                    return false;
                }
            }


            return true;
        }

        public static bool DataTablesEquals(DataTable table1, DataTable table2)
        {
            if (table1 == null || table2 == null)
                return false;

            if (table1.Rows.Count != table2.Rows.Count)
                return false;

            for (int i = 0; i < table1.Rows.Count; i++)
            {
                for (int j = i; j < table2.Rows.Count; j++)
                {
                    var array1 = table1.Rows[i].ItemArray;
                    var array2 = table2.Rows[j].ItemArray;

                    if (!array1.SequenceEqual(array2))
                    {
                        return false;
                    }
                    else
                    {
                        break;
                    }
                }

            }

            return true;
        }


        public static bool GridRowDataRowEquals(GridRow row1, DataRow row2)
        {
            if (row1 == null || row2 == null)
                return false;

            if (row1.Cells.Count != row2.ItemArray.Length)
                return false;

            for (int i = 0; i < row1.Cells.Count; i++)
            {
                var cell1 = row1.Cells[i].Value;
                var cell2 = row2[i];

                if (Extensions.IsNumeric(cell1) && Extensions.IsNumeric(cell2))
                {
                    if (!Convert.ToInt64(cell1).Equals(Convert.ToInt64(cell2)))
                    {
                        return false;
                    }
                }
                else if (!cell1.Equals(cell2))
                {
                    return false;
                }
            }
            return true;
        }


        public static bool IsNumeric(object expression)
        {
            if (expression == null)
                return false;

            double number;
            return Double.TryParse(Convert.ToString(expression
                                                    , System.Globalization.CultureInfo.InvariantCulture)
                                  , System.Globalization.NumberStyles.Any
                                  , System.Globalization.NumberFormatInfo.InvariantInfo
                                  , out number);
        }


        /// <summary>
        /// Executes the callback on response
        /// </summary>
        public static void ExecuteOnResponeEx(EventHandler callback, object sender = null, EventArgs args = null, int delay = 10)
        {
            //Call ExecuteOnRespone method
            ApplicationElement.ExecuteOnRespone(callback, sender, args, delay);
        }
       
    }
}