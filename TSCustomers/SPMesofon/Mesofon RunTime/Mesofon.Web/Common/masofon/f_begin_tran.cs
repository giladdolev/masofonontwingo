using System.Globalization;
using System.Threading.Tasks;
using masofonAlias = masofon;
namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_begin_tranClass
	{
		public static async Task f_begin_tran()
		{
            //return;
			string syntax = "begin tran";
			if (masofonAlias.masofon.Instance.gb_transaction_began)
			{
			    await f_commitClass.f_commit();
			}
			switch (masofonAlias.masofon.Instance.gs_database_type.ToLower(CultureInfo.InvariantCulture)) {
				case "ase":
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
					{
					    await f_rollbackClass.f_rollback();


					    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Begin Tran Failed! - Error Code - " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
						return;
					}
					break;
				case "asa":
					//.. database options must:		Chained = Off
					//..										Isolation_Level = 1
					 
					masofonAlias.masofon.Instance.sqlca.AutoCommit = false; //.. will return to true in f_commit()
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
					{
					    await f_rollbackClass.f_rollback();
						
						
						await masofonAlias.masofon.Instance.guo_msg.uf_msg("Begin Tran Failed! - Error Code - " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
						return;
					}
					break;
				case "sql":
					
					if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
					{
					    await f_rollbackClass.f_rollback();


					    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Begin Tran Failed! - Error Code - " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
						return;
					}
					break;
			}
			masofonAlias.masofon.Instance.gb_transaction_began = true;
		}
	}
}
