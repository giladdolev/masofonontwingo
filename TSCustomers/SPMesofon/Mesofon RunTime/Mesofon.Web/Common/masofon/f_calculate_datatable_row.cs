using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using global;

namespace masofon
{
    // Creation Time:   08/11/2015 10:21:03 (Utc)
    // Comments:        
    // 
    public static class f_calculate_datatable_rowClass
    {
        public static async Task<long> f_calculate_datatable_row(int table_number, long al_branch_number, SQLError al_transaction)
        {
            long serial_number = 0;
            long next_serial_number = 0;
            string ls_message = null;
            string ls_table_name = null;
            string ls_db_error = null;
            string ls_syntax = "begin tran";
            // AlexKh - 1.2.43.0 - 2009-03-29 - SPUCM00000931 - replace function to support different DBs and to lock transactions
            switch (masofonAlias.masofon.Instance.gs_database_type.ToUpper())
            {
                case "ASE":
                case "SQL":
                    //..		Adaptive Server Enterprise and Miscrosoft SQL Server
                    //.. Begin Tran

                    if (al_transaction.SqlCode != 0)
                    {

                        await MessageBox.Show("תקלה בבסיס הנתונים  SQLCA ERROR TEXT " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "Begin Tran Failed! => f_calculate_datatable_row()");
                        return -1;
                    }
                    ls_syntax = "rollback";
                    f_calculate_datatable_rows_by_branchClass.UpdateData(al_branch_number, table_number);
                    f_calculate_datatable_rowClass.LoadData(table_number, al_branch_number, ref serial_number);
                    

                    if (al_transaction.SqlCode != 0)
                    {

                        if (al_transaction.SqlCode == 100)
                        {
                            f_calculate_datatable_rowClass.UpdateData1(al_branch_number, table_number, ls_table_name);

                            if (al_transaction.SqlCode != 0)
                            {

                                ls_db_error = al_transaction.SqlErrText;
                                f_calculate_datatable_rowClass.UpdateData2(ls_syntax);
                                await MessageBox.Show("תקלה בבסיס הנתונים SQLCA ERROR TEXT " + ls_db_error, "Insert Failed Table Number - " + table_number.ToString() + " => f_calculate_datatable_row()");
                                return -1;
                            }
                            ls_syntax = "commit";
                            f_calculate_datatable_rowClass.UpdateData3(ls_syntax);
                            return 1;
                        }

                        ls_db_error = al_transaction.SqlErrText;
                        f_calculate_datatable_rowClass.UpdateData4(ls_syntax);
                        await MessageBox.Show("תקלה בבסיס הנתונים SQLCA ERROR TEXT " + ls_db_error, "NOT FOUND Table Number - " + table_number.ToString() + " => f_calculate_datatable_row()");
                        return -1;
                    }
                    f_calculate_datatable_rowClass.LoadData(table_number, al_branch_number, ref serial_number);
                    ls_syntax = "commit";
                    //.. Commit
                    break;
                case "ASA":
                    //..		Adaptive Server Anywhere
                    if (!masofonAlias.masofon.Instance.gb_transaction_began)
                    {
                        await f_begin_tranClass.f_begin_tran();
                    }

                    if (al_transaction.SqlCode != 0)
                    {

                        ls_db_error = al_transaction.SqlErrText;
                        await f_rollbackClass.f_rollback();
                        await MessageBox.Show("תקלה בבסיס הנתונים SQLCA ERROR TEXT " + ls_db_error, "NOT FOUND Table Number - " + table_number.ToString() + " => f_calculate_datatable_row()");
                        return -1;
                    }
                    if (true)
                    {
                        f_calculate_datatable_rowClass.LoadData1(table_number, al_branch_number, ref serial_number);
                    }

                    if (al_transaction.SqlCode != 0)
                    {

                        if (al_transaction.SqlCode == 100)
                        {
                            f_calculate_datatable_rowClass.UpdateData5(al_branch_number, table_number, ls_table_name);

                            if (al_transaction.SqlCode != 0)
                            {

                                ls_db_error = al_transaction.SqlErrText;
                                await f_rollbackClass.f_rollback();
                                await MessageBox.Show("תקלה בבסיס הנתונים SQLCA ERROR TEXT " + ls_db_error, "Insert Failed Table Number - " + table_number.ToString() + " => f_calculate_datatable_row()");
                                return -1;
                            }
                            await f_commitClass.f_commit();
                            return 1;
                        }
                        else
                        {

                            ls_db_error = al_transaction.SqlErrText;
                            await f_rollbackClass.f_rollback();
                            await MessageBox.Show("תקלה בבסיס הנתונים SQLCA ERROR TEXT " + ls_db_error, "NOT FOUND Table Number - " + table_number.ToString() + " => f_calculate_datatable_row()");
                            return -1;
                        }
                    }
                    await f_commitClass.f_commit();
                    break;
            }
            if (serial_number == 0)
            {
                serial_number = 0;
            }
            return serial_number;
         
        }
        public static void UpdateData(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING al_transaction ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData1(Int64 al_branch_number, int table_number, string ls_table_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", table_number }, { "@2", ls_table_name } };
                    string sql = "INSERT INTO serial_number_control (branch_number,  table_number,  table_name,  next_serial_number) VALUES (@0,  @1,  @2,  1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData2(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING al_transaction ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData3(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING al_transaction ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData4(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING al_transaction ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData5(Int64 al_branch_number, int table_number, string ls_table_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", table_number }, { "@2", ls_table_name } };
                    string sql = "INSERT INTO serial_number_control (branch_number,  table_number,  table_name,  next_serial_number) VALUES (@0,  @1,  @2,  1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData(int table_number, Int64 al_branch_number, ref long serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", table_number }, { "@1", al_branch_number } };
                    string sql = "SELECT isnull(next_serial_number, 0) FROM serial_number_control WHERE table_number = @0 AND branch_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        serial_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData1(int table_number, Int64 al_branch_number, ref long serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", table_number }, { "@1", al_branch_number } };
                    string sql = "SELECT isnull(next_serial_number, 0) FROM serial_number_control WHERE table_number = @0 AND branch_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        serial_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
