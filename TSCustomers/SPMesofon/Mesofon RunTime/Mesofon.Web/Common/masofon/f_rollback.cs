using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using Mesofon.Common.Global;
using masofonAlias = masofon;
namespace masofon
{
    // Creation Time:   08/11/2015 10:21:04 (Utc)
    // Comments:        
    // 
    public static class f_rollbackClass
    {
        public static async Task f_rollback()
        {
            return;
            string syntax = "rollback";
            switch (masofonAlias.masofon.Instance.gs_database_type.ToLower(CultureInfo.InvariantCulture))
            {
                case "ase":
                    //..		Adaptive Server Enterprise
                    
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {


                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Rollback Failed! - Error Code - " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    }
                    break;
                case "asa":
                    //..		Adaptive Server Anywhere
                     
                    masofonAlias.masofon.Instance.sqlca.AutoCommit = true;
                    break;
                case "sql":
                    //..		Microsoft SQL Server
                    if (masofonAlias.masofon.Instance.gb_transaction_began)
                    {
                        f_rollbackClass.UpdateData(syntax);
                        
                        if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                        {


                            await masofonAlias.masofon.Instance.guo_msg.uf_msg("Rollback Failed! - Error Code - " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        }
                    }
                    break;
            }
            masofonAlias.masofon.Instance.gb_transaction_began = false;
        }
        public static void UpdateData(string syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
