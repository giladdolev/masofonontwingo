using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using Mesofon.Common.Global;
using masofonAlias = masofon;
namespace masofon
{
    // Creation Time:   08/11/2015 10:21:03 (Utc)
    // Comments:        
    // 
    public static class f_calculate_datatable_rowsClass
    {
        public static long f_calculate_datatable_rows(long al_table_number)
        {
            long ll_serial_number = 0;
            string ls_syntax = "begin tran";
            string ls_table_name = null;
            string ls_db_error = null;
            //	begin tran in this function 
            //	this section must be separate transaction becouse it has been called from an another transaction 
            //	and commit must affect only on this table
            switch (masofonAlias.masofon.Instance.gs_database_type.ToUpper())
            {
                case "ASE":
                case "SQL":
                    //.. Adaptive Server Enterprise and Miscrosoft SQL Server
                    //.. Begin Tran

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {
                        return -1;
                    }
                    ls_syntax = "rollback";

                    UpdateData2(al_table_number);
                    if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                    {

                        ls_db_error = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                        f_calculate_datatable_rowsClass.UpdateData(ls_syntax);
                        return -1;
                    }
                    if (true)
                    {
                        f_calculate_datatable_rowsClass.LoadData(al_table_number, ref ll_serial_number);
                    }

                    switch (masofonAlias.masofon.Instance.sqlca.SqlCode)
                    {
                        case 100:
                            ls_table_name = "invoice_details";

                            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                            {

                                ls_db_error = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                                f_calculate_datatable_rowsClass.UpdateData1(ls_syntax);
                                return -1;
                            }
                            ls_syntax = "commit";
                            //.. Commit
                            return 1;
                        case -1:

                            ls_db_error = masofonAlias.masofon.Instance.sqlca.SqlErrText;
                            //.. RollBack		
                            return -1;
                        default:
                            ls_syntax = "commit";
                            //.. Commit
                            break;
                    }
                    break;
            }
            if (ll_serial_number == 0)
            {
                ll_serial_number = 0;
            }
            return ll_serial_number;
        }
        public static void UpdateData(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING SQLCA ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData1(string ls_syntax)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_syntax } };
                    string sql = "EXECUTE IMMEDIATE @0 USING SQLCA ";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }

        public static void UpdateData2(long table_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", table_number }, { "@1", Convert.ToInt32(MvcSite.Common.Globals.BranchNumber) } };
                    string sql = "UPDATE serial_number_control SET next_serial_number = next_serial_number + 1 WHERE serial_number_control.table_number = @0 AND serial_number_control.branch_number = @1";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData(Int64 al_table_number, ref long ll_serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_table_number }, { "@1", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT isnull(next_serial_number, 0) FROM serial_number_control WHERE table_number = @0 AND branch_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_serial_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
