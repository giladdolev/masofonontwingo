namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_check_english_charClass
	{
		public static bool f_check_english_char(string next_char)
		{
		    char ch = char.Parse(next_char);
			if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
			{
				return (true);
			}
			return (false);
		}
	}
}
