using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;
using Mesofon.Common.Global;
using Mesofon.Repository;
using Common.Transposition.Extensions;

namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_get_param_valuesClass
	{
		public static bool f_get_param_values(long al_param_code, ref ComboGridElement adw_object, string as_name, bool ab_set_default_value, SQLError at_transaction) //******************************************************************************
		{
			//*Purpose: 			Get parameter values for specific parameter code
			//*Arguments: 		Long 			- param_code 	- parameter code
			//*						Datawindow	- adw_object 	- structure to populate with values
			//*						String		- as_name		- field name
			//*						Boolean		- ab_set_default_value - check if default value must be set
			//*						transaction - at_transaction
			//*Return:				Boolean 	- TRUE/FALSE
			//*Date				Programer		Version	Task#	 	Description
			//*--------------------------------------------------------------------------------
			//*06-02-2011		AlexKh			1.2.47.0	CR#1146	Initial version
			//********************************************************************************
			
			long ll_ret = 0;
			long? ll_found = 0;
			string ls_find = null;
			//DataViewElement ldwc;
			//ldwc = adw_object.GetVisualElementById<DataViewElement>(as_name);
            //ll_ret = ldwc.Retrieve(al_param_code);

            IRepository dw_1List = new dddw_param_valuesRepository();
            ll_ret = dw_1List.Retrieve(al_param_code);
            adw_object.DisplayMember = "param_value";
            adw_object.ValueMember = "param_value";
            adw_object.SetRepository(dw_1List);

            if (ll_ret > 0 && ab_set_default_value)
			{
				ls_find = "default_value == 1";
                ll_found = dw_1List.Find(ls_find, 0, (int)ll_ret);
                if (ll_found >= 0)
				{
                    //ldwc.ScrollToRow((int)ll_found);
                    //ldwc.SetRow((int)ll_found);
                }
            }
			return (ll_ret > 0);
		}
	}
}
