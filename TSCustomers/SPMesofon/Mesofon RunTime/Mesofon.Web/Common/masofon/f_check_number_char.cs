namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_check_number_charClass
	{
		public static bool f_check_number_char(string check_char)
		{
            char ch = char.Parse(check_char);
            if (((ch >= '0' && ch <= '9')) || check_char == ".")
			{
				return true;
			}
			return false;
		}
	}
}
