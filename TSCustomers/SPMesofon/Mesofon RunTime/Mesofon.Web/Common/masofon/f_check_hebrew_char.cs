namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_check_hebrew_charClass
	{
		public static bool f_check_hebrew_char(string check_char)
		{
            char ch = char.Parse(check_char);
            if (ch >= 'א' && ch <= 'ת')
			{
				return (true);
			}
			return (false);
		}
	}
}
