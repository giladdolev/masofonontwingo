using System;

namespace masofon
{
	// Creation Time:   08/11/2015 10:21:03 (Utc)
	// Comments:        
	// 
	public static class f_language_translatorClass
	{
		public static string f_language_translator(string source_str) //						LANGUAGE COMPILER (ENGLISH, HEBREW) FOR PRINTING 
		{
			int i = 0;
			int magic_num = 96;
			string current_char = null;
			string number_word = "";
			string heb_word = "";
			//string current_word = "";
			string target_str = "";
			string tmpr_str = "";
			string temp_str = "";
			bool hebrew = false;
			magic_num = 96;
			i = 1;
			current_char = source_str.Substring(i - 1, 1);
			while ((i <= source_str.Length))
			{
				// --- IF CURRENT CHARACTER IS HEBREW CHARECTER ---
				if (f_check_hebrew_charClass.f_check_hebrew_char(current_char))
				{
					hebrew = true;
				}
				else
				{
					hebrew = false;
				}
				// --- WHILE IN HEBREW FONT ---
				while (hebrew)
				{
					// --- WHILE GET HEBEREW CHARECTERS ---
					while ((f_check_hebrew_charClass.f_check_hebrew_char(current_char) && i <= source_str.Length))
					{
						// --- PREPARE CHARACTER FOR PRINT ON NOT LASER PRINTER ---
                        current_char = Convert.ToChar(string.IsNullOrEmpty(current_char) ? 0 : Convert.ToInt32(current_char[0]) - magic_num).ToString();
						// --- PREPARE HEBREW WORD ---
						heb_word = current_char + heb_word;
						i = i + 1;
						// --- GET CURRENT CHAR ---
						current_char = source_str.Substring(i - 1, 1);
					} // HEBREW CHARACTERS ALREADY 
					// --- WHILE SERVICE CHARACTERS ---
					while ((!f_check_hebrew_charClass.f_check_hebrew_char(current_char) && (!f_check_number_charClass.f_check_number_char(current_char) && (!f_check_english_charClass.f_check_english_char(current_char) && i <= source_str.Length))))
					{
						// --- PREPARE TEMPORARY STRING OF SERVICES CHARACTERS ---
						tmpr_str = current_char + tmpr_str;
						i = i + 1;
						current_char = source_str.Substring(i - 1, 1);
					} // SERVICE CHARACTER 
					// --- CURRENT CHAR IS NUMBER ---
					if (f_check_number_charClass.f_check_number_char(current_char))
					{
						// --- WHILE CURRENT CHAR IS NUMBER ---
						while ((f_check_number_charClass.f_check_number_char(current_char) && i <= source_str.Length))
						{
							// --- PREPARE NUMBER WORD TEMPORARY ---
							number_word = number_word + current_char;
							i = i + 1;
							current_char = source_str.Substring(i - 1, 1);
						}
						// --- PREPARE HEBREW WORD ---
						heb_word = number_word + f_str_transporationClass.f_str_transporation(tmpr_str) + heb_word;
						// --- SET EMPTY ROW --
						number_word = "";
						tmpr_str = "";
						// --- IF CURRENT CHARACTER IS NOT ENGLISH 
						if (!f_check_english_charClass.f_check_english_char(current_char))
						{
							hebrew = true;
						}
						else
						{
							hebrew = false;
						}
					}
					else
					{
						// CURRENT CHAR IS NOT NUMBER ---
						// --- IF NOT ENGLISH ---
						if (!f_check_english_charClass.f_check_english_char(current_char) && i <= source_str.Length)
						{
							hebrew = true;
							// --- PREPARE HEBREW WORD WITH  SIRVICE STRING---
							heb_word = tmpr_str + heb_word;
							tmpr_str = "";
						}
						else
						{
							// --- ENGLISH ---
							heb_word = heb_word + tmpr_str;
							tmpr_str = "";
							hebrew = false;
						}
					}
				}
				// --- PREPARE TARGET STRING AFTER HEBREW FONT ---	
				target_str = target_str + heb_word;
				heb_word = "";
				// --- WHILE ENGLISH FONT ---		
				while ((!f_check_hebrew_charClass.f_check_hebrew_char(current_char) && i <= source_str.Length))
				{
					target_str = target_str + current_char;
					i = i + 1;
					current_char = source_str.Substring(i - 1, 1);
				}
			}
			return (target_str);
		}
	}
}
