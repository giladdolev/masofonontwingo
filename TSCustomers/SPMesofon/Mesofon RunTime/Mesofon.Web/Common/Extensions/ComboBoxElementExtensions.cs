﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;

namespace Mesofon.Common
{
    public static class ComboBoxElementExtensions
    {
        public static void SetRepository(this ComboBoxElement ComboBoxElement, IRepository repository)
        {
            System.Web.VisualTree.Engine.DataExtender<IRepository> extender = VisualElementExtender.GetOrCreateExtender(ComboBoxElement,
               arg => new System.Web.VisualTree.Engine.DataExtender<IRepository>());
            extender.Value = repository;
            ComboBoxElement.Columns.Clear();
            ComboBoxElement.DataSource = repository.GetDataTable();
        }

        public static long Retrieve(this ComboBoxElement ComboBoxElement, params object[] parameters)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Retrieve(parameters);
            }
            return -1;
        }

        public static int RowCount(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            return repository != null ? repository.RowCount() : 0;
        }

        public static int FilteredCount(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            return repository != null ? repository.FilteredCount() : 0;
        }

        private static IRepository Repository(ComboBoxElement ComboBoxElement)
        {
            if (ComboBoxElement == null)
            {
                return null;
            }
            IRepositoryContainer repositoryContainer = ComboBoxElement.DataSource as IRepositoryContainer;
            if (repositoryContainer != null)
            {
                return repositoryContainer.Repository;
            }
            IRepository repository = ComboBoxElement.DataSource as IRepository;
            if (repository != null)
            {
                return repository;
            }
            System.Web.VisualTree.Engine.DataExtender<IRepository> extender = VisualElementExtender.GetExtender<System.Web.VisualTree.Engine.DataExtender<IRepository>>(ComboBoxElement);
            if (extender != null)
            {
                return extender.Value;
            }

            return null;
        }

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing the update status of different tables in the DataWindow so that you can update more than one table
        /// Modifying the WHERE clause of the DataWindow object’s SQL SELECT statement
        /// Turning on Query mode or Prompt For Criteria so users can specify the data they want
        /// Changing the status of Retrieve Only As Needed
        /// Changing the data source of the DataWindow object
        /// </summary>
        /// <param name="ComboBoxElement">Name of the control.</param>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// Returns the empty string (“”) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument’s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        public static string Modify(this ComboBoxElement ComboBoxElement, string expression)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Modify(expression);
            }
            return "";
        }

        public static string Modify(this ComboBoxElement GridElement, string controlName, string propertyName, string value)
        {
            string result = null;
            try
            {
                result = "";
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        /// <summary>
        //// Evaluate expressions involving values of a particular row and column. When you include Describe’s Evaluate function in the property list, the value of the evaluated expression is included in the reported information.
        /// </summary>
        /// <param name="ComboBoxElement">Name of the control.</param>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        public static string Describe(this ComboBoxElement ComboBoxElement, string expression)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Describe(expression);
            }
            return "";
        }



        public static T1 GetItemValue<T1>(this ComboBoxElement ComboBoxElement, long row, string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetItemValue<T1>(row, columnName, buffer);
            }
            return default(T1);
        }

        public static T1[] GetItemValues<T1>(this ComboBoxElement ComboBoxElement, string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetItemValues<T1>(columnName, buffer);
            }
            return default(T1[]);
        }

        public static T GetItem<T>(this ComboBoxElement ComboBoxElement, int row, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetItem<T>(row, buffer);
            }
            return default(T);
        }

        public static long DeletedCount(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            return repository != null ? repository.DeletedCount() : 0;
        }

        public static long ModifiedCount(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            return repository != null ? repository.ModifiedCount() : 0;
        }

        public static long SetItem(this ComboBoxElement ComboBoxElement, long row, string columnName, object value)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.SetItem(row, columnName, value);
            }
            return -1;
        }

        /// <summary>
        /// Deletes the specified row.
        /// </summary>
        /// <param name="row">A value identifying the row you want to delete. To delete the current row, specify 0 for row.</param>

        public static void Delete(this ComboBoxElement ComboBoxElement, int row)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                repository.Delete(row);
            }
        }

        /// <summary>
        /// A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.
        /// </summary>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <param name="row">A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.</param>
        /// <returns>Returns the number of the row that was added if it succeeds and –1 if an error occurs.</returns>
        public static int Insert(this ComboBoxElement ComboBoxElement, int row)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Insert(row);
            }
            return -1;
        }

        /// <summary>
        /// Clears a range of rows from one repository and inserts them in another. Alternatively, RowsMove moves rows from one buffer to another within a single repository
        /// </summary>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <param name="startRow">The number of the first row you want to move.</param>
        /// <param name="endRow">The number of the last row you want to move.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to move the rows.</param>
        /// <param name="target">The name of the repository to which you want to move the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the moved rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to move the rows..</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsMove(this ComboBoxElement ComboBoxElement, int startRow, int endRow, ModelBuffer sourceBuffer, ComboBoxElement target, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                IRepository targetRepository = Repository(target);
                return repository.RowsMove(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
            }
            return -1;
        }

        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsCopy(this ComboBoxElement ComboBoxElement, int startRow, int endRow, ModelBuffer sourceBuffer, ComboBoxElement target, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                IRepository targetRepository = Repository(target);
                return repository.RowsCopy(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
            }
            return -1;
        }

        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsCopy(this ComboBoxElement ComboBoxElement, int startRow, int endRow, ModelBuffer sourceBuffer, IRepository targetRepository, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.RowsCopy(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
            }
            return -1;
        }

        /// <summary>
        /// Displays rows in a repository that pass the current filter criteria. Rows that do not meet the filter criteria are moved to the filter buffer.
        /// The Filter method retrieves all rows before applying the filter.
        /// To change the filter criteria, use the SetFilter method. 
        /// </summary>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public static int Filter(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Filter();
            }
            return -1;
        }

        /// <summary>
        /// Specifies filter criteria for a repository.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the filter criteria. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetFilter(this ComboBoxElement ComboBoxElement, string expression)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.SetFilter(expression);
            }
            return -1;
        }

        public static string GetFilter(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetFilter();
            }
            return "";
        }
        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the search criterion. The expression includes column names.</param>
        /// <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// –1 General error
        /// –5 Bad argument</returns>
        public static int? Find(this ComboBoxElement ComboBoxElement, string expression, int start, int end)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Find(expression,start,end);
            }
            return -1;
        }

        /// <summary>
        /// Specifies sort criteria for a repository.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="expression">
        /// A string whose value is valid sort criteria for the repository. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).
        /// A repository can have sort criteria specified as part of its definition. SetSort overrides the definition, providing new sort criteria for the repository. However, it does not actually sort the rows. Call the Sort method to perform the actual sorting.
        /// </param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetSort(this ComboBoxElement ComboBoxElement, string expression)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.SetSort(expression);
            }
            return -1;
        }

        public static string GetSort(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetSort();
            }
            return "";
        }

        /// <summary>
        /// Sort criteria for a newly created repository. To specify sorting for existing repository.
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="expression">
        /// A string whose value represents valid sort criteria.
        /// </param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public static int Sort(this ComboBoxElement ComboBoxElement, string expression = "")
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Sort(expression);
            }
            return -1;
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <returns>The return value identifies the status of the item at row, column in the specified buffer.</returns>
        public static ModelAction GetItemStatus(this ComboBoxElement ComboBoxElement, int row, object column, ModelBuffer buffer)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetItemStatus(row, column, buffer);
            }
            return ModelAction.None;
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="ComboBoxElement"></param>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <param name="status">A value that specifying the new status.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetItemStatus(this ComboBoxElement ComboBoxElement, int row, object column, ModelBuffer buffer, ModelAction status)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.SetItemStatus(row, column, buffer, status);
            }
            return -1;
        }

        /// <summary>
        /// Get objects that have knowledge about the data they are retrieving.
        /// </summary>
        /// <returns>Returns a string that contains names of columns.</returns>
        public static string ColumnsNames(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.ColumnsNames();
            }
            return "";
        }

        /// <summary>
        /// Obtains the value of an item in a value list or code table associated with a column in a repository.
        /// </summary>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <param name="row">The number of the item in the value list or the code table for the edit style.</param>
        /// <param name="column">The column for which you want the item. Column can be a column number (integer) or a column name (string)</param>
        /// <returns>
        /// Returns the item identified by index in the value list or the code table associated with column of repository. Returns the empty string (“ ”) if the index is not valid or the column does not have a value list or code table.
        /// </returns>
        public static string GetValue(this ComboBoxElement ComboBoxElement, object column, int row)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetValue(column, row);
            }
            return "";
        }

        /// <summary>
        /// Clears all the data from a repository
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs. </returns>
        public static int Reset(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Reset();
            }
            return -1;
        }

        /// <summary>
        /// Inserts data into a repository control tab-separated, comma-separated, or XML data in a string.
        /// </summary>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <param name="importedString">A string from which you want to copy the data. The string should contain tab-separated or comma-separated columns or XML with one row per line</param>
        /// <returns>
        /// Returns the number of rows that were imported if it succeeds and one of the following negative integers if an error occurs:
        /// -1 No rows or startrow value supplied is greater than the number of rows in the string
        /// -3 Invalid argument
        /// -4 Invalid input
        /// -11 XML Parsing Error; XML parser libraries not found or XML not well formed
        /// -12 XML Template does not exist or does not match the DataWindow
        /// -13 Unsupported DataWindow style for import
        /// -14 Error resolving DataWindow nesting
        /// </returns>
        public static int ImportString(this ComboBoxElement ComboBoxElement, string importedString)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.ImportString(importedString);
            }
            return -1;
        }


        /// <summary>
        /// Updates the database with the changes made
        /// </summary>
        /// <param name="ComboBoxElement">DataView element</param>
        /// <param name="uow"></param>
        /// <returns></returns>
        public static long Update(this ComboBoxElement ComboBoxElement, IUnitOfWork uow)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.Update(uow);
            }
            return -1;
        }


        /// <summary>
        /// Saves the data in XML format.
        /// </summary>
        /// <param name="ComboBoxElement">DataView element</param>
        /// <param name="filename">File name</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs</returns>
        public static int SaveToXml(this ComboBoxElement ComboBoxElement, string filename)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.SaveToXml(filename);
            }
            return -1;
        }


        /// <summary>
        /// Clears the update flags in the primary and filter buffers and empties the delete buffer
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int ResetUpdate(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.ResetUpdate();
            }
            return -1;
        }


     


        /// <summary>
        /// Reports the SQL SELECT statement
        /// </summary>
        /// <returns>Returns the current SQL SELECT statement</returns>
        public static string GetSQLSelect(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetSQLSelect();
            }
            return "";
        }

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns></returns>
        public static List<ModelBase> DataRows(this ComboBoxElement ComboBoxElement, int row = -1, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetDataRows(row, dataSource, buffer);
            }
            return null;
        }

        public static void SetDataRows(this ComboBoxElement ComboBoxElement, List<ModelBase> list)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                repository.SetDataRows(list);
            }
        }

        /// <summary>
        /// Gets the repository of the ComboBoxElement.
        /// </summary>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <returns></returns>
        public static IRepository GetDataObject(this ComboBoxElement ComboBoxElement)
        {
            return Repository(ComboBoxElement);
        }

        /// <summary>
        /// Sets the repository of the ComboBoxElement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ComboBoxElement">The data view element.</param>
        /// <param name="repository">The repository.</param>
        public static void SetDataObject<T>(this ComboBoxElement ComboBoxElement, IRepository<T> repository) where T : ModelBase, new()
        {
            repository.Retrieve();
            ComboBoxElement.DataSource = repository.PrimaryList;
        }

        /// <summary>
        /// Get the number of columns in the repository.
        /// </summary>
        /// <returns></returns>
        public static int ColumnCount(this ComboBoxElement ComboBoxElement)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.ColumnCount();
            }
            return 0;
        }

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns></returns>
        public static List<ModelBase> GetDataRows(this ComboBoxElement ComboBoxElement, int row = -1,
            ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(ComboBoxElement);
            if (repository != null)
            {
                return repository.GetDataRows(row, dataSource, buffer);
            }
            return null;
        }
    }
}