﻿using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;

namespace MvcSite.Common
{
    public static class GridColumnExtensions
    {
        public static IRepository GetRepository(this GridColumn col)
        {
            return col.Repository();
        }
        public static void SetRepository(this GridColumn col, IRepository repository)
        {
            DataExtender<IRepository> extender = VisualElementExtender.GetOrCreateExtender(col,
               arg => new DataExtender<IRepository>());
            extender.Value = repository;
        }

        /// <summary>
        /// Gets the repository of GridElement
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns null if an error occurs </returns>
        private static IRepository Repository(this GridColumn col)
        {
            if (col == null)
            {
                return null;
            }
            DataExtender<IRepository> extender = VisualElementExtender.GetExtender<DataExtender<IRepository>>(col);
            if (extender != null)
            {
                return extender.Value;
            }
            return null;
        }
    }
}