﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;
using Common.Transposition.Extensions;
using Common.Transposition.Extensions.Data;
using System.ComponentModel;
using System.Data;
using System.Extensions;
using System.Web.VisualTree.Engine;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Collections;
using System.Reflection;
using MvcSite.Common;

namespace Mesofon.Common
{
    public static class GridElementExtensions
    {
        public static void SetLockUI(this GridElement GridElement, bool value)
        {
            DataExtender<bool> extender = VisualElementExtender.GetOrCreateExtender(GridElement, arg => new DataExtender<bool>());
            extender.Value = value;
        }

        public static bool GetLockUI(this GridElement GridElement)
        {
            DataExtender<bool> extender = VisualElementExtender.GetExtender<DataExtender<bool>>(GridElement);
            if (extender != null)
            {
                return extender.Value;
            }
            return false;
        }

        public static void SetPreventClickOnDetails(this GridElement GridElement, bool value)
        {
            DataExtender<bool> extender = VisualElementExtender.GetOrCreateExtender(GridElement, arg => new DataExtender<bool>());
            extender.Value = value;
        }

        public static bool GetPreventClickOnDetails(this GridElement GridElement)
        {
            DataExtender<bool> extender = VisualElementExtender.GetExtender<DataExtender<bool>>(GridElement);
            if (extender != null)
            {
                return extender.Value;
            }
            return false;
        }


        private static void FixGridDateTimePickerColumns(GridElement GridElement)
        {
            foreach (var item in GridElement.Columns)
            {
                var col = item as GridDateTimePickerColumn;
                if (col != null)
                {
                    col.Format = System.Web.VisualTree.DateTimePickerFormat.Short;
                }

            }
        }


        /// <summary>
        /// Sets repository to Element
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="repository"></param>
        /// <param name="LoadData"></param>
        public static void SetRepository(this GridElement GridElement, IRepository repository, bool LoadData = true, bool InitializeColumns = false, bool overwrite = false)
        {
            //Check if UI Lock 
            if (GridElement.GetLockUI())
            {
                return;
            }


            DataExtender<IRepository> extender = VisualElementExtender.GetOrCreateExtender(GridElement,
               arg => new DataExtender<IRepository>());

            var isModelChanged = extender.Value == null || extender.Value.GetType() != repository.GetType();


            // Update repository
            if (extender != null)
            {
                extender.Value = repository;
            }

            // check if model is changed
            if (isModelChanged)
            {
                InitializeColumns = true;
            }

            if (GridElement is global.u_datastore_update)
            {
                return;
            }

            if (GridElement is ComboGridElement || GridElement is WidgetGridElement)
            {
                // Next line cause WidgetGrid refresh
                SetDataSourceEx(GridElement, repository.GetDataTable(), overwrite);
                return;
            }


            //Fill Data 
            if (InitializeColumns)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(repository.GetTypeOfModel());
                DataTable table = new DataTable(repository.GetTypeOfModel().Name);
                SortedList<int, GridColumn> cols = new SortedList<int, GridColumn>();
                foreach (PropertyDescriptor prop in properties)
                {
                    var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                    if (att != null)
                    {
                        GridColumn col = null;
                        var NumberField = SystemFunctionsExtensions.GetNumberFieldAttribute(prop);
                        switch (Type.GetTypeCode(att.Type))
                        {
                            case TypeCode.Int32:
                            case TypeCode.Int16:
                            case TypeCode.Int64:
                            case TypeCode.Decimal:
                            case TypeCode.Double:
                            case TypeCode.String:
                                col = new GridTextBoxColumn(prop.Name, att.Caption);

                                if (NumberField != null)
                                {
                                    col.NumberField = true;
                                }
                                break;
                            case TypeCode.Boolean:
                                col = new GridCheckBoxColumn(prop.Name, att.Caption);
                                break;
                            case TypeCode.DateTime:
                                col = new GridDateTimePickerColumn(prop.Name, att.Caption);
                                var formatAttribue = SystemFunctionsExtensions.GetFormatAttribute(prop);
                                if (formatAttribue != null)
                                {
                                    ((GridColumn)col).Format = formatAttribue.CustomFormat;
                                }
                                else { ((GridDateTimePickerColumn)col).Format = System.Web.VisualTree.DateTimePickerFormat.Short; }

                                break;
                            case TypeCode.Object:
                                if (att.Type.Name == "GridCheckBoxColumn")
                                {
                                    col = new GridCheckBoxColumn(prop.Name, att.Caption);
                                    ((GridCheckBoxColumn)col).ThreeState = true;
                                }
                                else if (att.Type.Name == "GridHtmlContentColumn")
                                {
                                    col = new GridHtmlContentColumn(prop.Name, att.Caption);
                                    var htmlContentAtt = SystemFunctionsExtensions.GetHTMLContentAttribute(prop);
                                    if (htmlContentAtt != null)
                                    {
                                        ((GridHtmlContentColumn)col).HtmlContent = htmlContentAtt.HtmlContent;
                                        ((GridHtmlContentColumn)col).DataMember = htmlContentAtt.DataMembers;
                                    }
                                }
                                else if (att.Type.Name == "GridCustomColumn")
                                {
                                    col = new GridCustomColumn(prop.Name, att.Caption);
                                    var customColAtt = SystemFunctionsExtensions.GetCustomColumnAttribute(prop);
                                    if (customColAtt != null)
                                    {
                                        ((GridCustomColumn)col).Renderer = customColAtt.RendererMethod;
                                    }

                                }
                                else if (att.Type.Name == "GridComboBoxColumn")
                                {
                                    col = new GridComboBoxColumn(prop.Name, att.Caption, typeof(GridComboBoxColumn));
                                    Dictionary<string, object> codeTable;
                                    var isExist = repository.LookupTables.TryGetValue(prop.Name, out codeTable);
                                    if (codeTable != null && codeTable.Count > 0)
                                    {
                                        string str = "";
                                        foreach (var item in codeTable)
                                        {
                                            str += Convert.ToString(item.Value) + "|" + Convert.ToString(item.Key) + ",";
                                        }
                                        GridElement.AddToComboBoxList(col.ID, str);
                                    }
                                }
                                else if (att.Type.Name == "WidgetColumn")
                                {
                                    col = new WidgetColumn();
                                    col.ID = prop.Name;
                                    col.HeaderText = att.Caption;
                                    col.Height = 1;
                                    var WidgetColumnAtt = SystemFunctionsExtensions.GetWidgetColumnAttribute(prop);
                                    if (WidgetColumnAtt != null)
                                    {
                                        foreach (var WG in WidgetColumnAtt)
                                        {
                                            ControlElement control = Activator.CreateInstance(WG.ElementType) as ControlElement;
                                            if (control == null)
                                            {
                                                throw new Exception("GalilCS - Illegal type");
                                            }
                                            foreach (var pair in WG.Properties)
                                            {
                                                PropertyInfo property = control.GetType().GetProperty(pair.Key, BindingFlags.Public | BindingFlags.Instance);
                                                if (property != null)
                                                {
                                                    if (property.CanWrite)
                                                    {
                                                        property.SetValue(control, Convert.ChangeType(pair.Value, property.PropertyType), null);
                                                    }
                                                }
                                                else if (pair.Key == "SetEnabledExpression")
                                                {
                                                    control.SetEnabledExpression(Convert.ToString(pair.Value));
                                                }
                                            }




                                            ((WidgetColumn)col).Items.Add(control);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("GalilCS - TODO : Need to implement");
                                }
                                break;
                            default:
                                throw new Exception("GalilCS - TODO : Need to implement");
                        }
                        if (att.Width > 0)
                        {
                            col.Width = att.Width;
                        }
                        col.Visible = att.Visible;
                        cols.Add(att.DisplayIndex, col);
                    }

                }
                IList<GridColumn> newColumns = cols.Select(x => x.Value).ToArray();
                if (!Extensions.DataColumnsEquals(GridElement.Columns, newColumns))
                {
                    //Clear Columns
                    GridElement.Columns.Clear();

                    GridElement.Columns.AddRange(newColumns);
                }
            }
            if (LoadData && repository != null)
            {
                //Clear Rows
                GridElement.Rows.Clear();

                for (int i = 0; i < repository.GetPrimaryList().Count; i++)
                {
                    GridRow r = new GridRow();
                    foreach (var col in GridElement.Columns)
                    {
                        r.Cells.Add(new GridContentCell());
                    }
                    GridElement.Rows.Add(r);
                }
                FillData(GridElement, repository);

            }

            CheckIfDataSync(GridElement);
        }



        //gilad 
        public static bool IsRepoAligned(GridElement GridElement, object datasource, bool overwrite = false)
        {
            //Check if two datasource is equals then exit
            DataTable CurrentDatasource = (DataTable)GridElement.DataSource;
            DataTable newDatasource = (DataTable)datasource;

            var resCols = Extensions.DataColumnsEquals(CurrentDatasource, newDatasource);
            var resRows = Extensions.DataTablesEquals(CurrentDatasource, newDatasource);

            if (resCols && resRows)//two objects are identical
            {
                return true;
            }

            return false;
        }



        public static void SetDataSourceEx(GridElement GridElement, object datasource, bool overwrite = false)
        {
            //Check if two datasource is equals then exit
            DataTable CurrentDatasource = (DataTable)GridElement.DataSource;
            DataTable newDatasource = (DataTable)datasource;

            var resCols = Extensions.DataColumnsEquals(CurrentDatasource, newDatasource);
            var resRows = Extensions.DataTablesEquals(CurrentDatasource, newDatasource);

            if (resCols && resRows && !overwrite)//two objects are identical
            {
                return;
            }
            else if (!resCols) // datacolumns not identical , render all data
            {
                ChangeDataSource(GridElement, datasource);
            }
            else if (!resRows || overwrite)//Columns identical but rows not identical , need to add/update/delete rows depend on new table
            {
                int i = 0;

                for (i = 0; i < newDatasource.Rows.Count; i++)
                {
                    var sourceRow = newDatasource.Rows[i];
                    if (CurrentDatasource.Rows.Count > 0 && i < CurrentDatasource.Rows.Count)
                    {
                        var destRow = CurrentDatasource.Rows[i];

                        InsertOrUpdateRow(GridElement, i, sourceRow);

                        if (sourceRow.ItemArray.SequenceEqual(destRow.ItemArray))
                        {
                            continue;
                        }
                        //Replace data in destination row 
                        destRow.ItemArray = sourceRow.ItemArray;
                    }
                    else
                    {
                        //Adding new row
                        var dr = CurrentDatasource.NewRow();
                        dr.ItemArray = sourceRow.ItemArray;
                        CurrentDatasource.Rows.Add(dr);
                        InsertOrUpdateRow(GridElement, i, sourceRow);
                    }
                }

                //Delete unnecessary rows from datasource
                for (int j = CurrentDatasource.Rows.Count - 1; j >= i; j--)
                {
                    CurrentDatasource.Rows.RemoveAt(j);
                    if (j >= 0 && j < GridElement.Rows.Count)
                        GridElement.Rows.RemoveAt(j);
                }
            }

            CheckIfDataSync(GridElement);
        }

        private static void InsertOrUpdateRow(GridElement GridElement, int index, DataRow row)
        {
            // Update -  if index is in range of gridelement rows 
            if (index >= 0 && index < GridElement.Rows.Count)
            {
                var destRow = GridElement.Rows[index];

                if (!Extensions.GridRowDataRowEquals(destRow, row))
                {
                    //Update Data in GridRow
                    foreach (var cell in destRow.Cells)
                    {
                        cell.Value = row[cell.ColumnIndex];
                    }
                }
            }
            else //Insert row
            {
                //Create GridRow
                GridRow r = new GridRow();
                foreach (var col in GridElement.Columns)
                {
                    r.Cells.Add(new GridContentCell());
                }

                //Update Data in GridRow
                foreach (var cell in r.Cells)
                {
                    cell.Value = row[cell.ColumnIndex];
                }

                //Insert row to GridElement
                if (index == 0 || index >= GridElement.Rows.Count)
                {
                    //Add row to end of rows
                    GridElement.Rows.Add(r);
                }
                else
                {
                    //Add row to specific index
                    GridElement.Rows.Insert(index - 1, r);
                }
            }
        }

        private static void ChangeDataSource(GridElement GridElement, object datasource)
        {
            //Save repositories
            Dictionary<string, IRepository> lstRepository = new Dictionary<string, IRepository>();
            Dictionary<string, string> lstFormat = new Dictionary<string, string>();
            foreach (var col in GridElement.Columns)
            {
                var repo = col.GetRepository();
                if (repo != null)
                {
                    lstRepository.Add(col.ID, repo);
                }
                if (!string.IsNullOrEmpty(col.Format))
                {
                    lstFormat.Add(col.ID, col.Format);
                }
            }

            GridElement.DataSource = datasource;

            //Restore repositories after modify data source
            foreach (var pair in lstRepository)
            {
                var col = GridElement.Columns[pair.Key];
                if (col != null)
                {
                    col.SetRepository(pair.Value);
                }
            }


            foreach (var pair in lstFormat)
            {
                var col = GridElement.Columns[pair.Key];
                if (col != null)
                {
                    col.Format = pair.Value;
                }
            }
        }


        //gilad 
        private static void FillData(GridElement GridElement, IRepository repository, int? specificrow = null)
        {

            int row = -1;


            foreach (var item in repository.GetPrimaryList())
            {
                row++;
                if (specificrow.HasValue && specificrow != row)
                    continue;

                FillGridRow(GridElement, repository, row, item);

                if (GridElement.Columns.Count > 0)
                {
                    GridElement.Rows.SetWidgetControls(row);
                }

            }

            if (GridElement.ID != null && !GridElement.HasdataChangedListeners)
            {
                GridElement.dataChanged += GridElement_dataChanged;
            }
        }

        private static void FillGridRow(GridElement gridelement, IRepository repository, int row, object item)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(repository.GetTypeOfModel());

            foreach (PropertyDescriptor prop in properties)
            {
                var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                if (att != null)
                {
                    Dictionary<string, object> codeTable;
                    var isExist = repository.LookupTables.TryGetValue(prop.Name, out codeTable);
                    // Replace the codeTable in source
                    if (isExist)
                    {
                        object value;
                        if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(item))))
                        {
                            if (codeTable.TryGetValue(Convert.ToString(prop.GetValue(item)), out value))
                            {
                                gridelement.Rows[row].Cells[prop.Name].Value = value;
                            }
                        }
                        else
                        {
                            gridelement.Rows[row].Cells[prop.Name].Value = DBNull.Value;
                        }
                    }
                    else
                    {

                        if (att.Type == typeof(DateTime))
                        {
                            DateTime dt;
                            if (DateTime.TryParseExact(Convert.ToString(prop.GetValue(item)), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out dt))
                            {
                                gridelement.Rows[row].Cells[prop.Name].Value = dt;
                            }
                            else if (prop.GetValue(item) is DateTime)
                            {
                                gridelement.Rows[row].Cells[prop.Name].Value = prop.GetValue(item);
                            }
                        }
                        else if (att.Type == typeof(Boolean) || att.Type == typeof(GridCheckBoxColumn))
                        {
                            var threeState = SystemFunctionsExtensions.GetThreeStateAttribute(prop);
                            if (threeState != null)
                            {
                                if (threeState.On == Convert.ToString(prop.GetValue(item)))
                                {
                                    gridelement.Rows[row].Cells[prop.Name].Value = true;
                                }
                                else if (threeState.Off == Convert.ToString(prop.GetValue(item)))
                                {
                                    gridelement.Rows[row].Cells[prop.Name].Value = false;
                                }
                                else
                                {
                                    gridelement.Rows[row].Cells[prop.Name].Value = null;
                                }
                            }
                            else
                            {
                                gridelement.Rows[row].Cells[prop.Name].Value = Convert.ToBoolean(ConversionEx.Val(prop.GetValue(item)));
                            }
                        }

                        else if (att.Type == typeof(GridHtmlContentColumn))
                        {
                            gridelement.Rows[row].Cells[prop.Name].Value = prop.GetValue(item) ?? DBNull.Value;
                            //Get GetHTMLContentAttribute
                            var HTMLContentAtt = SystemFunctionsExtensions.GetHTMLContentAttribute(prop);

                        }
                        else if (att.Type == typeof(GridCustomColumn))
                        {
                            gridelement.Rows[row].Cells[prop.Name].Value = prop.GetValue(item) ?? DBNull.Value;

                            //Get ButtonButtonAttribute
                            var atts = SystemFunctionsExtensions.GetButtonButtonColumnAttribute(prop);
                            if (atts != null && atts.Count() == 2)
                            {
                                string state = "";
                                bool visible1 = false;
                                bool visible2 = false;
                                var lst = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType()));
                                lst.Add(item);
                                var match1 = lst.Where(atts[0].VisibleExp);

                                if (lst.Count > 0 && match1.Count() > 0)
                                {
                                    //True
                                    visible1 = true;
                                }

                                var match2 = lst.Where(atts[1].VisibleExp);
                                if (lst.Count > 0 && match2.Count() > 0)
                                {
                                    //True
                                    visible2 = true;
                                }

                                if (visible1 && visible2)
                                {
                                    state = "both";
                                }
                                else if (visible1 && !visible2)
                                {
                                    state = "right";
                                }
                                else if (!visible1 && visible2)
                                {
                                    state = "left";
                                }
                                else if (!visible1 && !visible2)
                                {
                                    state = "none";
                                }
                                gridelement.Rows[row].Cells[prop.Name].Value = state;
                            }


                        }
                        else if (att.Type == typeof(WidgetColumn))
                        {
                        }
                        else
                        {
                            gridelement.Rows[row].Cells[prop.Name].Value = prop.GetValue(item) ?? DBNull.Value;

                        }

                    }
                    //Resolve value of cell if there is an text expression
                    ResolveValues(gridelement, row, prop, item);
                }
            }
        }

        /// <summary>
        /// Reformat values in text expression available on specific property in model
        /// </summary>
        /// <param name="GridElement">Grid element</param>
        /// <param name="row">number of row</param>
        /// <param name="prop">property</param>
        /// <param name="item">model</param>
        private static void ResolveValues(GridElement GridElement, int row, PropertyDescriptor prop, object item)
        {
            var textExpAttribute = SystemFunctionsExtensions.GetTextExpressionAttribute(prop);
            //Check if expression exist
            if (textExpAttribute != null)
            {
                if (textExpAttribute.Exp == "getrow()")
                {
                    int num;
                    var IsParsed = int.TryParse(Convert.ToString(GridElement.Rows[row].Cells[prop.Name].Value), out num);
                    if (!IsParsed)
                    {
                        GridElement.Rows[row].Cells[prop.Name].Value = Convert.ToString(row + 1);
                    }
                }
                else if (textExpAttribute.Exp == "FormatTwoDecimalPlaces")
                {
                    GridElement.Columns[prop.Name].Format = "Ext.util.Format.decimal2places";
                }
                else
                {
                    var lst = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType()));
                    lst.Add(item);
                    var match = lst.Where(textExpAttribute.Exp);

                    if (match.Count() == 0 && lst.Count > 0) //False
                    {
                        GridElement.Rows[row].Cells[prop.Name].Value = textExpAttribute.FalseExp;
                    }
                    else //True
                    {
                        GridElement.Rows[row].Cells[prop.Name].Value = textExpAttribute.TrueExp;
                    }
                }
            }
        }

        /// <summary>
        /// Responsible for updating the UI after data changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void GridElement_dataChanged(object sender, EventArgs e)
        {
            if (sender is WidgetGridElement)
                return;
            GridElement GridElement = sender as GridElement;
            IRepository repository = GridElement.Repository();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(repository.GetTypeOfModel());
            int row = 0;


            foreach (ModelBase item in repository.GetPrimaryList())
            {
                foreach (PropertyDescriptor prop in properties)
                {
                    var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                    if (att != null)
                    {
                        Dictionary<string, object> codeTable;
                        var isExist = repository.LookupTables.TryGetValue(prop.Name, out codeTable);
                        var colIndex = GridElement.Columns.IndexOf(prop.Name);
                        ResolveExpressionProperties(GridElement, row, item, prop, att, colIndex);
                    }
                }
                row++;
            }
        }

        /// <summary>
        /// Resolve expression properties for specific cell
        /// </summary>
        /// <param name="GridElement">Grid element</param>
        /// <param name="row">Number of row</param>
        /// <param name="item">Model</param>
        /// <param name="prop">Property</param>
        /// <param name="att">Attribute of model</param>
        private static void ResolveExpressionProperties(GridElement GridElement, int row, object item, PropertyDescriptor prop, PropertiesAttribute att, int colIndex)
        {
            if (GridElement.Rows.Count <= row)
                return;

            //Set BackColor from model
            var BackColorAtt = SystemFunctionsExtensions.GetBackColorAttribute(prop);
            if (BackColorAtt != null)
            {
                var lst = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType()));
                lst.Add(item);
                var match = lst.Where(BackColorAtt.Exp);

                if (match.Count() > 0 && lst.Count > 0)
                {
                    GridElement.SetCellStyle(row, colIndex, BackColorAtt.CSSClass);
                }
            }

            //Set ProtectExpression from model
            var ProtectExpressionAtt = SystemFunctionsExtensions.GetProtectExpressionAttribute(prop);
            if (ProtectExpressionAtt != null)
            {
                var lst = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType()));
                lst.Add(item);
                var match = lst.Where(ProtectExpressionAtt.Exp);

                if (match.Count() > 0 && lst.Count > 0)
                {
                    //true
                    GridElement.Rows[row].Cells[prop.Name].Protect = false;
                    GridElement.Rows[row].Cells[prop.Name].Protect = true;
                }
                else
                {
                    //false
                    GridElement.Rows[row].Cells[prop.Name].Protect = false;
                }
            }
        }

        /// <summary>
        /// Retrieves data from the database
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="parameters"></param>
        /// <returns>Returns the number of rows displayed (that is, rows in the primary buffer) if it succeeds and –1 if it fails</returns>
        public static long Retrieve(this GridElement GridElement, params object[] parameters)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                long result = repository.Retrieve(parameters);
                GridElement.SetRepository(repository);
                return result;
            }
            return -1;
        }

        /// <summary>
        /// Obtains the number of rows that are currently available in the primary buffer.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns the number of rows that are currently available, 0 if no rows are currently available </returns>
        public static int RowCount(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            return repository != null ? repository.RowCount() : 0;
        }


        /// <summary>
        /// Reports the number of rows that are not displayed in the DataWindow because of the current filter criteria.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns the number of rows in repository that are not displayed because they do not meet the current filter criteria. 
        /// Returns 0 if all rows are displayed.</returns>
        public static int FilteredCount(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            return repository != null ? repository.FilteredCount() : 0;
        }

        /// <summary>
        /// Gets the repository of GridElement
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns null if an error occurs </returns>
        public static IRepository Repository(this GridElement GridElement)
        {
            if (GridElement == null)
            {
                return null;
            }
            IRepositoryContainer repositoryContainer = GridElement.DataSource as IRepositoryContainer;
            if (repositoryContainer != null)
            {
                return repositoryContainer.Repository;
            }
            IRepository repository = GridElement.DataSource as IRepository;
            if (repository != null)
            {
                return repository;
            }

            DataExtender<IRepository> extender = VisualElementExtender.GetExtender<DataExtender<IRepository>>(GridElement);
            if (extender != null)
            {
                return extender.Value;
            }


            return null;
        }

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing the update status of different tables in the DataWindow so that you can update more than one table
        /// Modifying the WHERE clause of the DataWindow object’s SQL SELECT statement
        /// Turning on Query mode or Prompt For Criteria so users can specify the data they want
        /// Changing the status of Retrieve Only As Needed
        /// Changing the data source of the DataWindow object
        /// </summary>
        /// <param name="GridElement">Name of the control.</param>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// Returns the empty string (“”) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument’s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        public static string Modify(this GridElement GridElement, string expression)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Modify(expression);
            }
            return "";
        }

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing colors, text settings, and other appearance settings of controls
        /// Controlling the Print Preview display
        /// Deleting and adding controls (such as lines or bitmaps) in the DataViewElement
        /// </summary>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// Returns the empty string (“”) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument’s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        public static string Modify(this GridElement GridElement, string controlName, string propertyName, string value)
        {
            string result = null;
            try
            {
                VisualElement control = GridElement.Columns[controlName];
                if (control != null)
                {
                    PropertyInfo propertyInfo = control.GetType().GetProperty(propertyName);
                    if (propertyInfo != null)
                    {
                        Type t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
                        object safeValue = (value == null) ? null : Convert.ChangeType(value, t);
                        propertyInfo.SetValue(control, safeValue, null);
                    }
                }
                result = "";
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        public static string Describe(this GridElement GridElement, string expression)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Describe(expression);
            }
            return "";
        }

        /// <summary>
        /// Evaluate expressions involving values of a particular row and column. When you include Describe’s Evaluate function in the property list, the value of the evaluated expression is included in the reported information.
        /// </summary>
        /// <param name="GridElement">Name of the control.</param>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        public static string Describe<T>(this GridElement GridElement, string expression)
        {
            string result = null;
            string functionString;
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                if (expression.Contains("Evaluate("))
                {
                    functionString = expression.Split('(')[1];
                    if (functionString.Equals("'MAX"))
                    {
                        result = expression.Split('(')[2];
                        if (expression.Contains("for all"))
                        {
                            string column = result.Split(' ')[0]; ;
                            string row = result.Split(',')[1];
                            row = row.Substring(0, 1);
                            List<T> ListValues = GetListValuesColumn<T>(GridElement, row, column);
                            T max = ListValues.Max<T>();
                            return max.ToString();
                        }
                    }
                }
                //return repository.Describe(expression);
            }
            return "!";
        }

        private static List<T> GetListValuesColumn<T>(GridElement GridElement, string row, string columnName)
        {
            List<T> result = new List<T>();
            {
                for (long i = Convert.ToInt64(row) - 1; i < GridElement.Repository().GetPrimaryList().Count; i++)
                {
                    result.Add(GridElement.GetItemValue<T>(i, columnName, ModelBuffer.Primary));
                }
            }
            return result;
        }

        /// <summary>
        /// Reports the values of properties of a DataViewElement object and controls within the DataViewElement object.
        /// </summary>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        public static string Describe(this GridElement GridElement, string controlName, string propertyName)
        {
            string result = null;
            try
            {
                VisualElement control = GridElement.Columns[(controlName)];
                if (control != null)
                {
                    PropertyInfo propertyInfo = control.GetType().GetProperty(propertyName);
                    if (propertyInfo != null)
                    {
                        Type t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
                        return propertyInfo.GetValue(control).ToString();
                    }
                }
                result = @"""?""";
            }
            catch (Exception e)
            {
                result = @"""!""";
            }

            return result;
        }

        /// <summary>
        /// Gets the value of an item for the specified row and column
        /// </summary>
        /// <typeparam name="T1">The type of column and return value</typeparam>
        /// <param name="row">A value identifying the row location of the data.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="buffer">The buffer from which you want to get item.</param>
        /// <returns>Value of an item for the specified row and column. Returns null or 0 if an error occurs</returns>
        public static T1 GetItemValue<T1>(this GridElement GridElement, long row, string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetItemValue<T1>(row, columnName, buffer);
            }
            return default(T1);
        }

        /// <summary>
        /// Gets the values of an item for the specified column
        /// </summary>
        /// <typeparam name="T1">The type of column and return value</typeparam>
        /// <param name="GridElement"></param>
        /// <param name="columnName"></param>
        /// <param name="buffer"></param>
        /// <returns>Values of an items for the specified columns. Returns null or 0 if an error occurs</returns>
        public static T1[] GetItemValues<T1>(this GridElement GridElement, string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetItemValues<T1>(columnName, buffer);
            }
            return default(T1[]);
        }

        /// <summary>
        /// Gets an item from buffer for specific row
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="GridElement"></param>
        /// <param name="row"></param>
        /// <param name="buffer"></param>
        /// <returns>Returns the item in the specified row, and null or 0 if an error occurs</returns>
        public static T GetItem<T>(this GridElement GridElement, int row, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetItem<T>(row, buffer);
            }
            return default(T);
        }

        /// <summary>
        /// Get number of the rows that have been deleted.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns 0 if no rows have been deleted or if all the deleted rows have been updated in the database.</returns>
        public static long DeletedCount(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            return repository != null ? repository.DeletedCount() : 0;
        }

        /// <summary>
        /// Get number of the rows that have been modified.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns the number of rows that have been modified</returns>
        public static long ModifiedCount(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            return repository != null ? repository.ModifiedCount() : 0;
        }

        private static PropertyDescriptor GetPropertyDescriptor(IRepository repository, string columnName)
        {
            PropertyDescriptor returnValue = null;
            if (repository != null)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(repository.GetTypeOfModel());
                foreach (PropertyDescriptor item in properties)
                {
                    if (item.Name == columnName)
                        return item;
                }
            }


            return returnValue;
        }

        /// <summary>
        /// Sets the value of a row and column in a Repository to the specified value.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row">The row location of the data.</param>
        /// <param name="columnName">The column location of the data.</param>
        /// <param name="value">The value to which you want to set the data at the row and column location. The datatype of the value must be the same datatype as the column.</param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs</returns>
        public static long SetItem(this GridElement GridElement, long row, string columnName, object value)
        {
            bool ToResolveExpProperties = true;
            IRepository repository = Repository(GridElement);
            var prop = GetPropertyDescriptor(repository, columnName);
            if (repository != null)
            {

                if (repository.SetItem(row, columnName, value) == 1)
                {
                    if (GridElement.Columns[columnName] != null)
                    {
                        if (GridElement.Columns[columnName] is GridCheckBoxColumn)
                        {
                            if (((GridCheckBoxColumn)GridElement.Columns[columnName]).ThreeState)
                            {
                                var threeState = SystemFunctionsExtensions.GetThreeStateAttribute(prop);
                                if (threeState != null)
                                {
                                    if (threeState.On == Convert.ToString(value))
                                    {
                                        if (GridElement.Rows[(int)row].Cells[columnName].Value == null ||
                                            (GridElement.Rows[(int)row].Cells[columnName].Value != null && !GridElement.Rows[(int)row].Cells[columnName].Value.Equals(true)))
                                        {
                                            GridElement.Rows[(int)row].Cells[columnName].Value = true;
                                        }
                                    }
                                    else if (threeState.Off == Convert.ToString(value))
                                    {
                                        if (GridElement.Rows[(int)row].Cells[columnName].Value == null ||
                                            (GridElement.Rows[(int)row].Cells[columnName].Value != null && !GridElement.Rows[(int)row].Cells[columnName].Value.Equals(false)))
                                        {
                                            GridElement.Rows[(int)row].Cells[columnName].Value = false;
                                        }
                                    }
                                    else
                                    {
                                        if (GridElement.Rows[(int)row].Cells[columnName].Value != null)
                                        {
                                            GridElement.Rows[(int)row].Cells[columnName].Value = null;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                GridElement.Rows[(int)row].Cells[columnName].Value = Convert.ToBoolean(ConversionEx.Val(value));
                            }
                        }
                        else
                        {
                            if (GridElement.Rows.Count > (int)row)
                            {
                                SetLookupTablesValue(GridElement, row, columnName, value);
                            }
                            else
                            {
                                if (GridElement is WidgetGridElement)
                                    return 1;
                                var end = repository.RowCount() - GridElement.Rows.Count;
                                for (int i = 0; i < end; i++)
                                {
                                    GridRow r = new GridRow();
                                    foreach (var col in GridElement.Columns)
                                    {
                                        r.Cells.Add(new GridContentCell());
                                    }
                                    GridElement.Rows.Add(r);
                                }
                                //Refill all data
                                FillData(GridElement, repository);
                                ToResolveExpProperties = false;
                            }
                        }
                    }
                    //if (ToResolveExpProperties)
                    //ResolveExpressionProperties(GridElement, (int)row, columnName, repository);
                }
                return 1;
            }
            return -1;
        }

        /// <summary>
        /// Deletes the specified row.
        /// </summary>
        /// <param name="row">A value identifying the row you want to delete. To delete the current row, specify 0 for row.</param>

        public static void Delete(this GridElement GridElement, int row)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                repository.Delete(row);
                GridElement.Rows.RemoveAt(row);
            }
        }


        /// <summary>
        /// A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.
        /// </summary>
        /// <param name="GridElement">The data view element.</param>
        /// <param name="row">A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.</param>
        /// <returns>Returns the number of the row that was added if it succeeds and –1 if an error occurs.</returns>
        public static int Insert(this GridElement GridElement, int row)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                //Update in repository
                var result = repository.Insert(row);
                if (repository.RowCount() > GridElement.Rows.Count)
                {
                    GridRow r = new GridRow();
                    foreach (var col in GridElement.Columns)
                    {
                        r.Cells.Add(new GridContentCell());
                    }


                    //Update in GridElement

                    var updateRow = row;
                    // end of list
                    if (row == 0 || row >= GridElement.Rows.Count)
                    {
                        //Add to GridElement
                        updateRow = GridElement.Rows.Add(r);

                        //Add to DataSource
                        DataTable dt = (DataTable)GridElement.DataSource;
                        if (dt != null)
                        {
                            var dr = dt.NewRow();
                            dt.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        updateRow = updateRow - 1;
                        GridElement.Rows.Insert(updateRow, r);

                        //Add to DataSource
                        DataTable dt = (DataTable)GridElement.DataSource;
                        if (dt != null)
                        {
                            var dr = dt.NewRow();
                            dt.Rows.InsertAt(dr, updateRow);
                        }
                    }

                    //SetDataSourceEx(GridElement, repository.GetDataTable());
                    FillData(GridElement, repository, updateRow);


                    CheckIfDataSync(GridElement);

                }
                return result;
            }
            return -1;
        }

        /// <summary>
        /// Clears a range of rows from one repository and inserts them in another. Alternatively, RowsMove moves rows from one buffer to another within a single repository
        /// </summary>
        /// <param name="GridElement">The data view element.</param>
        /// <param name="startRow">The number of the first row you want to move.</param>
        /// <param name="endRow">The number of the last row you want to move.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to move the rows.</param>
        /// <param name="target">The name of the repository to which you want to move the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the moved rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to move the rows..</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsMove(this GridElement GridElement, int startRow, int endRow, ModelBuffer sourceBuffer, GridElement target, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                IRepository targetRepository = Repository(target);
                return repository.RowsMove(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
            }
            return -1;
        }

        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsCopy(this GridElement GridElement, int startRow, int endRow, ModelBuffer sourceBuffer, GridElement target, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(GridElement);
            if (startRow == endRow)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    throw new Exception("the arguments startRow and endRow are equle");
                }
            }

            if (repository != null)
            {
                IRepository targetRepository = Repository(target);
                var result = repository.RowsCopy(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
                if (!(GridElement is WidgetGridElement))
                    GridElement.SetRepository(repository);
                target.SetRepository(targetRepository);
                return result;
            }
            return -1;
        }

        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int RowsCopy(this GridElement GridElement, int startRow, int endRow, ModelBuffer sourceBuffer, IRepository targetRepository, int beforeRow, ModelBuffer targetBuffer)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                var result = repository.RowsCopy(startRow, endRow, sourceBuffer, targetRepository, beforeRow, targetBuffer);
                GridElement.SetRepository(repository);
                return result;
            }
            return -1;
        }

        /// <summary>
        /// Displays rows in a repository that pass the current filter criteria. Rows that do not meet the filter criteria are moved to the filter buffer.
        /// The Filter method retrieves all rows before applying the filter.
        /// To change the filter criteria, use the SetFilter method. 
        /// </summary>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public static int Filter(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                var res = repository.Filter();
                GridElement.SetRepository(repository);
                return res;
            }
            return -1;
        }

        /// <summary>
        /// Specifies filter criteria for a repository.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the filter criteria. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetFilter(this GridElement GridElement, string expression)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.SetFilter(expression);
            }
            return -1;
        }

        /// <summary>
        /// Gets the filter expression
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns filter expression, and empty string if an error occurs.</returns>
        public static string GetFilter(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetFilter();
            }
            return "";
        }
        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the search criterion. The expression includes column names.</param>
        /// <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// –1 General error
        /// –5 Bad argument</returns>
        public static int? Find(this GridElement GridElement, string expression, int start, int end)
        {
            IRepository repository = Repository(GridElement);
            if (string.IsNullOrEmpty(expression))
            {
                return null;
            }

            if (repository != null && expression != null)
            {
                return repository.Find(expression, start, end);
            }

            return -1;
        }

        /// <summary>
        /// Specifies sort criteria for a repository.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="expression">
        /// A string whose value is valid sort criteria for the repository. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).
        /// A repository can have sort criteria specified as part of its definition. SetSort overrides the definition, providing new sort criteria for the repository. However, it does not actually sort the rows. Call the Sort method to perform the actual sorting.
        /// </param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetSort(this GridElement GridElement, string expression)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.SetSort(expression);
            }
            return -1;
        }

        /// <summary>
        /// Gets sort expression
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns>Returns string of sort expression, and empty string if an error occurs</returns>
        public static string GetSort(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetSort();
            }
            return "";
        }

        /// <summary>
        /// Sort criteria for a newly created repository. To specify sorting for existing repository.
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="expression">
        /// A string whose value represents valid sort criteria.
        /// </param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public static int Sort(this GridElement GridElement, string expression = "")
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Sort(expression);
            }
            return -1;
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <returns>The return value identifies the status of the item at row, column in the specified buffer.</returns>
        public static ModelAction GetItemStatus(this GridElement GridElement, int row, object column, ModelBuffer buffer)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetItemStatus(row, column, buffer);
            }
            return ModelAction.None;
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <param name="status">A value that specifying the new status.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int SetItemStatus(this GridElement GridElement, int row, object column, ModelBuffer buffer, ModelAction status)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.SetItemStatus(row, column, buffer, status);
            }
            return -1;
        }

        /// <summary>
        /// Get objects that have knowledge about the data they are retrieving.
        /// </summary>
        /// <returns>Returns a string that contains names of columns.</returns>
        public static string ColumnsNames(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.ColumnsNames();
            }
            return "";
        }

        /// <summary>
        /// Obtains the value of an item in a value list or code table associated with a column in a repository.
        /// </summary>
        /// <param name="GridElement">The data view element.</param>
        /// <param name="row">The number of the item in the value list or the code table for the edit style.</param>
        /// <param name="column">The column for which you want the item. Column can be a column number (integer) or a column name (string)</param>
        /// <returns>
        /// Returns the item identified by index in the value list or the code table associated with column of repository. Returns the empty string (“ ”) if the index is not valid or the column does not have a value list or code table.
        /// </returns>
        public static string GetValue(this GridElement GridElement, object column, int row)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetValue(column, row);
            }
            return "";
        }

        /// <summary>
        /// Clears all the data from a repository
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs. </returns>

        public static int Reset(this GridElement GridElement, string msg = null)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                GridElement.Rows.ClearEX(msg);
                return repository.Reset();
            }
            return -1;
        }

        public static int ResetWithoutClearGrid(this GridElement GridElement, string msg = null)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Reset();
            }
            return -1;
        }

        /// <summary>
        /// Inserts data into a repository control tab-separated, comma-separated, or XML data in a string.
        /// </summary>
        /// <param name="GridElement">The data view element.</param>
        /// <param name="importedString">A string from which you want to copy the data. The string should contain tab-separated or comma-separated columns or XML with one row per line</param>
        /// <returns>
        /// Returns the number of rows that were imported if it succeeds and one of the following negative integers if an error occurs:
        /// -1 No rows or startrow value supplied is greater than the number of rows in the string
        /// -3 Invalid argument
        /// -4 Invalid input
        /// -11 XML Parsing Error; XML parser libraries not found or XML not well formed
        /// -12 XML Template does not exist or does not match the DataWindow
        /// -13 Unsupported DataWindow style for import
        /// -14 Error resolving DataWindow nesting
        /// </returns>
        public static int ImportString(this GridElement GridElement, string importedString)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.ImportString(importedString);
            }
            return -1;
        }


        /// <summary>
        /// Updates the database with the changes made
        /// </summary>
        /// <param name="GridElement">DataView element</param>
        /// <param name="uow"></param>
        /// <returns></returns>
        public static long Update(this GridElement GridElement, IUnitOfWork uow)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Update(uow);
            }
            return -1;
        }

        /// <summary>
        /// you do not want an update to affect the rows in the database
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static void RowsDiscard(this GridElement GridElement, int row)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                repository.RowsDiscard(row);
            }
        }



        /// <summary>
        /// Saves the data in XML format.
        /// </summary>
        /// <param name="GridElement">DataView element</param>
        /// <param name="filename">File name</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs</returns>
        public static int SaveToXml(this GridElement GridElement, string filename)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.SaveToXml(filename);
            }
            return -1;
        }


        /// <summary>
        /// Clears the update flags in the primary and filter buffers and empties the delete buffer
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        public static int ResetUpdate(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.ResetUpdate();
            }
            return -1;
        }


        /// <summary>
        /// Printing to PrintBOS API
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="DeviceID"> ID of device </param>
        /// <param name="UserID"> ID of user </param>
        /// <param name="Password"> Password </param>
        /// <returns>Returns 0 if it succeeds and -1 if an error occurs</returns>
        public static int Print(this GridElement GridElement, string DeviceID, string UserID, string Password, string templateName)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.Print(DeviceID, UserID, Password, templateName);
            }
            return -1;
        }


        /// <summary>
        /// Reports the SQL SELECT statement
        /// </summary>
        /// <returns>Returns the current SQL SELECT statement, and empty string if an error occurs</returns>
        public static string GetSQLSelect(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetSQLSelect();
            }
            return "";
        }

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns></returns>
        public static List<ModelBase> DataRows(this GridElement GridElement, int row = -1, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetDataRows(row, dataSource, buffer);
            }
            return null;
        }

        /// <summary>
        /// Sets data to GridElement
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="list">List of data</param>
        public static void SetDataRows(this GridElement GridElement, List<ModelBase> list)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                repository.SetDataRows(list);
            }
        }

        /// <summary>
        /// Gets the repository of the GridElement.
        /// </summary>
        /// <param name="GridElement">The data view element.</param>
        /// <returns></returns>
        public static IRepository GetDataObject(this GridElement GridElement)
        {
            return Repository(GridElement);
        }

        /// <summary>
        /// Sets the repository of the GridElement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="GridElement">The data view element.</param>
        /// <param name="repository">The repository.</param>
        public static void SetDataObject<T>(this GridElement GridElement, IRepository<T> repository) where T : ModelBase, new()
        {
            repository.Retrieve();
            SetDataSourceEx(GridElement, repository.PrimaryList);
        }

        /// <summary>
        /// Get the number of columns in the repository.
        /// </summary>
        /// <returns>Returns the number of columns in the repository</returns>
        public static int ColumnCount(this GridElement GridElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.ColumnCount();
            }
            return 0;
        }

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns>Returns null if an error occurs</returns>
        public static List<ModelBase> GetDataRows(this GridElement GridElement, int row = -1,
            ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                return repository.GetDataRows(row, dataSource, buffer);
            }
            return null;
        }

        /// <summary>
        /// Sets the current row in repository
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="index">index the row to which you want to set current</param>
        /// <returns>Returns 1 if it succeeds and 0 if an error occurs</returns>
        public static int SetRow(this GridElement GridElement, int index)
        {
            if (GridElement != null && index < GridElement.Rows.Count)
            {
                GridElement.CurrentRow = GridElement.Rows[index];
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Sets the current column cell in repository
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="index">name of column to which you want to set</param>
        /// <returns>Returns 1 if it succeeds and 0 if an error occurs</returns>
        public static int SetColumn(this GridElement GridElement, string index)
        {
            if (GridElement != null)
            {
                GridElement.SelectedColumnIndex = GridElement.Columns[index].Index;
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static long row_saved(this GridElement GridElement, int index)
        {
            // TODO: Galilcs 
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static long log_book(this GridElement GridElement, int index)
        {
            // Galilcs - TODO
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static int SelectText(this GridElement GridElement, int start, int end)
        {
            // TODO: Galilcs 
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GridElement"></param>
        /// <returns></returns>
        public static long GetRow(this GridElement GridElement)
        {
            if (GridElement.CurrentRow != null)
                return GridElement.CurrentRow.Index;

            return 0;
        }

        /// <summary>
        /// Scrolls a GridElement to the specified row
        /// </summary>
        /// <param name="row">A value identifying the row to which you want to scroll. If row is 0, ScrollToRow scrolls to the first row. If row is greater than the last row number, it scrolls to the last row. If row is visible without scrolling, the GridElement does not scroll.</param>
        public static int ScrollToRow(this GridElement GridElement, int index)
        {
            GridElement.ScrollToRow(index);
            return 0;
        }

        /// <summary>
        /// Gets the current row index
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="index"></param>
        /// <returns>Returns selected row index if it succeeds and -1 if an error occurs</returns>
        public static int GetSelectedRow(this GridElement GridElement, int index)
        {
            if (GridElement != null && index < GridElement.Rows.Count)
            {
                if (index <= GridElement.SelectedRowIndex)
                {
                    return GridElement.SelectedRowIndex;
                }
            }
            return -1;
        }
        /// <summary>
        /// Set Visible property
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="displayIndex"></param>
        /// <param name="visible"></param>
        public static void setVisibleProperty(this GridElement GridElement, string name, bool visible)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(GridElement.Repository().GetTypeOfModel());
            foreach (PropertyDescriptor prop in properties)
            {
                var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                if (att != null)
                {
                    if (att.Name == name)
                    {
                        att.Visible = visible;
                        if (GridElement.Columns[att.Name] != null)
                        {
                            GridElement.Columns[att.Name].Visible = visible;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Replace the codeTable in source
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row"></param>
        /// <param name="columnName"></param>
        /// <param name="value"></param>
        private static void SetLookupTablesValue(GridElement GridElement, long row, string columnName, object value)
        {
            IRepository repository = Repository(GridElement);
            Dictionary<string, object> codeTable;
            var isExist = repository.LookupTables.TryGetValue(columnName, out codeTable);
            if (isExist)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(value)))
                {
                    if (codeTable.TryGetValue(Convert.ToString(value), out value))
                    {
                        if (value == null || value != null && !value.Equals(GridElement.Rows[(int)row].Cells[columnName].Value))
                        {
                            GridElement.Rows[(int)row].Cells[columnName].Value = value;
                        }
                    }
                }
                else
                {
                    if (GridElement.Rows[(int)row].Cells[columnName].Value != DBNull.Value)
                    {
                        GridElement.Rows[(int)row].Cells[columnName].Value = DBNull.Value;
                    }
                }
            }
            else
            {
                if (value == null || value != null && !value.Equals(GridElement.Rows[(int)row].Cells[columnName].Value))
                {
                    GridElement.Rows[(int)row].Cells[columnName].Value = value;
                }
            }
        }



        private static void CheckIfDataSync(GridElement GridElement)
        {
            bool returnValue = false;
            try
            {
                IRepository repository = Repository(GridElement);
                DataTable dt = (DataTable)GridElement.DataSource;

                if ((repository != null && repository.GetPrimaryList().Count != GridElement.Rows.Count) ||
                    (dt != null && dt.Rows.Count != GridElement.Rows.Count))
                {
                    returnValue = false;

                }
                else
                {
                    returnValue = true;
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (!returnValue)
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                }
            }
        }

        /// <summary>
        /// Check if grid is synced with repo
        /// </summary>
        /// <param name="GridElement"></param>
        /// <param name="row"></param>
        /// <param name="syncElement"></param>
        /// <returns></returns>
        /// //gilad 
        public static bool IsRowSynced(this GridElement GridElement, int row, string syncElement)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null && GridElement.Rows.Count > row)
            {
                DataTable gridDT = GridElement.DataSource as DataTable;
                DataTable repoDT = repository.GetDataTable();

                var gridVal = gridDT.Rows[row][syncElement];
                var rowRepo = repoDT.Rows.Cast<DataRow>().FirstOrDefault(y => y[syncElement].ToString() == gridVal.ToString());

                object[] gridArr = gridDT.Rows[row].ItemArray;
                object[] repoArr = rowRepo.ItemArray;
                if (gridArr.SequenceEqual(repoArr))
                    return true;
            }

            return false;
        }


        public static bool UpdateRow(this GridElement GridElement, int rowIndex, IRepository GridRepository, string compareKey)
        {
            IRepository repository = Repository(GridElement);
            if (repository != null)
            {
                int countErrors = 0;//fix 2 empthy rows problem
                int errorToRemove = 0;
                var result = false;
                int repoIndexToCopy = 0;
                int rouwCount = repository.RowCount();
                string GridDoc_no = GridRepository.GetValue("doc_no", rowIndex);
                string GridMaterial_number = GridRepository.GetValue("material_number", rowIndex);

                //check for more than 1 empthy item - some bag fix
                for (int i = 0; i < repository.RowCount(); i++)
                {
                    if (GridRepository.GetValue("barcode", i) == null)
                    {
                        countErrors++;
                        if (errorToRemove == 0)
                            errorToRemove = i;
                    }
                }
                if (countErrors > 1)
                    GridRepository.Delete(errorToRemove);//remove the first empthy found


                //check if there are more than 1 invoice for current item
                int itemsAtRepo = 0;
                if (rouwCount > 0)
                {
                    for (int i = 0; i < repository.RowCount(); i++)
                    {
                        string repoMaterial_number = repository.GetValue("material_number", i);
                        string repoInvoice_number = repository.GetValue("invoice_number", i);

                        if (repoMaterial_number == GridMaterial_number)
                        {
                            itemsAtRepo++;
                            if (repoInvoice_number == GridDoc_no)
                                repoIndexToCopy = i;//current item at repo to copy
                        }
                    }
                }
                //handle 
                if (itemsAtRepo == 0)//no such item at repo
                    return false;
                else if (itemsAtRepo == 1)//only 1 invoice with current item
                {
                    //find item to update and remove other
                    bool update = false;
                    for (int i = 0; i <= GridRepository.RowCount() - 1; i++)// we dont tuch the last empty
                    {
                        if (GridRepository.GetValue("doc_no", i) == GridDoc_no && GridRepository.GetValue("material_number", i) == GridMaterial_number)
                        {

                            if (!update)
                            {
                                result = repository.UpdateRow(i, ModelBuffer.Primary, GridRepository, repoIndexToCopy);
                                update = true;
                            }
                            else
                                GridRepository.Delete(i);
                        }
                    }


                    GridElement.SetRepository(repository);
                    return true;
                }
                else//more than 1 invoice with current item
                {
                    //find item to update and remove other
                    bool update = false;
                    for (int i = 0; i <= GridRepository.RowCount() - 1; i++)// we dont tuch the last empty
                    {
                        if (GridRepository.GetValue("doc_no", i) == GridDoc_no && GridRepository.GetValue("material_number", i) == GridMaterial_number)
                        {

                            if (!update)
                            {
                                result = repository.UpdateRow(i, ModelBuffer.Primary, GridRepository, repoIndexToCopy);
                                update = true;
                            }
                            else
                                GridRepository.Delete(i);
                        }
                    }

                    GridElement.SetRepository(repository);
                    return true;

                }
            }
            return false;
        }

    }
}
