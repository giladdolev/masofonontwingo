﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.VisualTree.Elements;

namespace Mesofon.Common
{
    public static class ControlElementExtensions
    {
        public static void SetEnabledExpression(this ControlElement control, string exp)
        {
            System.Web.VisualTree.Engine.DataExtender<string> extender = VisualElementExtender.GetOrCreateExtender(control,
                           arg => new System.Web.VisualTree.Engine.DataExtender<string>());
            extender.Value = exp;
        }

        public static string GetEnabledExpression(this ControlElement control)
        {
            System.Web.VisualTree.Engine.DataExtender<string> extender = VisualElementExtender.GetExtender<System.Web.VisualTree.Engine.DataExtender<string>>(control);
            if (extender != null)
            {
                return extender.Value;
            }
            return null;
        }
    }
}