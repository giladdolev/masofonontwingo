﻿using System;
using System.Web.VisualTree.Elements;
using MvcSite.ServiceSecurity;
using System.Text;
using System.Xml;
using System.IO;
using MvcSite.ServicePrintBOS;
using MvcSite.Common;
using System.Xml.Linq;
using System.Linq;
using System.Extensions.Printing;

namespace Mesofon.Common
{
    public class Utf8StringWriter : StringWriter
    {
        public Utf8StringWriter(StringBuilder sb) : base(sb)
        {
        }
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }

    public static class SystemHelper
    {
        public static string Concat(string source, object dest)
        {
            string result = source;
            if (dest == null || source == null)
                result = null;
            else
                result += Convert.ToString(dest);
            return result;
        }

        public static long ShellExecuteA(object p, string v1, string v2, string ls_null1, string ls_null2, int v3)
        {
            throw new NotImplementedException();
        }

        public static long CalculateCheckSumNum(string lsBarcode)
        {
            Random random = new Random();
            long randomNumber = random.Next(0, 9);
            return randomNumber;
        }

        public static bool Print_ItemReturn(long ll_printer_type, string ls_PrnName, long ll_RetunType, string ls_Barcode, long branchNum,
            string ls_BranchName, long ll_SupplierNum, string ls_SupplierName,string ls_Subject, string templateid = "Reprt00010")
        {
            try
            {
                XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", null),new XElement("root"
                                                , new XElement("ReturnType", Convert.ToString(ll_RetunType))
                                                , new XElement("BarCode", Convert.ToString(ls_Barcode))
                                                , new XElement("BranchNumber", Convert.ToString(branchNum))
                                                , new XElement("BranchName", Convert.ToString(ls_BranchName))
                                                , new XElement("SupplierNumber", Convert.ToString(ll_SupplierNum))
                                                , new XElement("SupplierName", Convert.ToString(ls_SupplierName))
                                                , new XElement("metadata", Convert.ToString(ls_SupplierName)
                                                , new XElement("TemplateId", templateid)
                                                , new XElement("PrinterType", Convert.ToString(ll_printer_type))
                                                , new XElement("PrinterName", Convert.ToString(ls_PrnName)))
                                                , new XElement("Subject", ls_Subject)
                                                ));


                StringBuilder builder = new StringBuilder();
                Utf8StringWriter writer = new Utf8StringWriter(builder);
                doc.Save(writer);
                writer.Flush();
                

                //Call PrintBOS API
                var printBOS = new PrintBosServiceClient();
                printBOS.Print(Globals.DeviceID, Globals.UserID, Globals.Password, "BarCode", builder.ToString());


            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool Print_Pdf(string ls_PrnName, string ls_FileName,string pdfBase64, string templateid = "PrintPdf", PdfToImage.Layout layout = PdfToImage.Layout.Portrait)
        {
            try
            {
                XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", null), new XElement("Root"
                                                , new XElement("metadata"
                                                , new XElement("TemplateId", templateid)
                                                , new XElement("PrinterName", ls_PrnName.ToString()))
                                                , new XElement("Body"
                                                , new XElement("FileName", ls_FileName)
                                                , new XElement("Orientation", Convert.ToString(layout))
                                                , new XElement("File", pdfBase64))
                                                ));

                StringBuilder builder = new StringBuilder();
                Utf8StringWriter writer = new Utf8StringWriter(builder);
                doc.Save(writer);
                writer.Flush();


                //Call PrintBOS API
                var printBOS = new PrintBosServiceClient();
                printBOS.Print(Globals.DeviceID, Globals.UserID, Globals.Password, "TemplatePDF", builder.ToString());


            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        internal static void WriteToLog(string message)
        {
            WriteToLog(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.BranchNumber, MvcSite.Common.Globals.StationNumber, message, string.Empty);
        }

        internal static void WriteToLog(string message, string description)
        {
            WriteToLog(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.BranchNumber, MvcSite.Common.Globals.StationNumber, message, description);
        }

        internal static void WriteToLog(string userId, string password, string branchNumber, string stationNumber, string message, string description)
        {
            //try
            //{
            //    var client = new SecurityServiceClient();
            //    client.WriteToLog(userId, password, branchNumber, stationNumber, message, description);
            //}
            //catch { }
        }


        public static void WriteToLogFile(string source, string message)
        {
            //Globals.log.DebugFormat("{0} - {1}", source, message);
            //var tempFolder = @"C:\\Superpharm-Temp";

            //if (!Directory.Exists(tempFolder))
            //{
            //    Directory.CreateDirectory(tempFolder);
            //}

            //var file = tempFolder + "\\logger.txt";
            //using (StreamWriter st = new StreamWriter(file, true))
            //{
            //    var line = string.Format("{0}, {1}, {2}", DateTime.Now.ToLongTimeString(), source, message);
            //    st.WriteLine(line);
            //}
        }
    }
}