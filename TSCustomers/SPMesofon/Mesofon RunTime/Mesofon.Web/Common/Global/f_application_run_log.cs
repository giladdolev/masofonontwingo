using System;
using System.Collections.Generic;
using System.DataAccess;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Data;

namespace global
{
    // Creation Time:   08/11/2015 10:20:49 (Utc)
    // Comments:        
    // 
    public static class f_application_run_logClass
    {
        public static bool f_application_run_log(string as_event, string as_message)
        {
            string ls_version = null;
            //ls_version = 	'1.49.44.1'
            ls_version = masofonAlias.masofon.Instance.guo_version.uf_get_version_name();
            f_application_run_logClass.UpdateData(ls_version, as_event, as_message);

            return (masofonAlias.masofon.Instance.sqlca.SqlCode == 0);
        }
        public static void UpdateData(string ls_version, string as_event, string as_message)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {


                    int application_number = default(int);

                    string sql = "SELECT application_number FROM applications WHERE application_name = 'Masofon'";
                    IDataReader resultRS = unitOfWork.Retrieve(sql);
                    while (resultRS.Read())
                    {
                        application_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt32);
                    }


                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ls_version }, { "@1", as_event }, { "@2", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@3", masofonAlias.masofon.Instance.gs_station.station_number }, { "@4", MvcSite.Common.Globals.UserID }, { "@5", as_message }, { "@6", application_number } };
                    sql = "INSERT INTO application_run_log(application_number, application_version, run_datetime, ipaddress,  event,  branch_number,  station_number,  dbuser_name,  message_text) VALUES(@6 ,  @0,  GetDate(),  'AppSrvIP',  @1,  @2,  @3,  @4,  @5)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
