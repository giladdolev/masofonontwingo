using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using Mesofon.Common;
using Mesofon.Common.Global;
using masofonAlias = masofon;


namespace global
{
    // Creation Time:   08/11/2015 10:20:50 (Utc)
    // Comments:        
    // 
    public static class f_get_error_message_numberClass
    {
        public static async Task<string> f_get_error_message_number(int ai_error_number)
        {
            string ls_lang_number_in_reg = null;
            string message_text = null;
            string multi_lingual_is_active = null;
            string ls_create_expressions_on_fly = null;
            long ll_lang_number = 0;
            long li_search = 0;
            multi_lingual_is_active = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Language", "multi_lingual_is_active", "no", "");
            ls_create_expressions_on_fly = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Language", "create_expressions_on_fly", "no", "");
            if (multi_lingual_is_active == "no")
            {
                ll_lang_number = 3; //hebrew
            }
            else
            {
                if (masofonAlias.masofon.Instance.nvo_translator != null)
                {
                    ll_lang_number = masofonAlias.masofon.Instance.nvo_translator.fnv_get_current_language();
                }
                else
                {
                    ls_lang_number_in_reg = RegToDatabase.GetRegistryItem(masofonAlias.masofon.Instance.s_reg_key + "Language", "default_language", "3", "");
                    if (string.IsNullOrEmpty(ls_lang_number_in_reg))
                    {
                        ll_lang_number = 1; //english
                    }
                    else
                    {
                        ll_lang_number = Convert.ToInt64(ls_lang_number_in_reg);
                    }
                }
            }
            if (true)
            {
                f_get_error_message_numberClass.LoadData(ai_error_number, ll_lang_number, ref message_text, ref li_search);
            }
            if (message_text == null || string.IsNullOrEmpty(message_text.Trim()))
            {
                f_get_error_message_numberClass.LoadData1(ai_error_number, ref message_text, ref li_search);
                //----- stas 30/06/2004 ------------------------

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100 || (message_text == null || string.IsNullOrEmpty(message_text.Trim())))
                {
                    if (masofonAlias.masofon.Instance.nvo_translator != null)
                    {
                        if (masofonAlias.masofon.Instance.nvo_translator.multi_lingual_is_active && (masofonAlias.masofon.Instance.nvo_translator.direction == "left" && ls_create_expressions_on_fly == "yes"))
                        {
                            w_pc_message_detail win = await WindowHelper.Open<w_pc_message_detail>("global_global", "error_number", ai_error_number);

                            message_text = WindowHelper.GetParam<string>(win); 
                        }
                    }
                }
                //------------------------------------------------

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100 || (message_text == null || string.IsNullOrEmpty(message_text.Trim())))
                {
                    message_text = "Error message number" + " " + ai_error_number.ToString() + " " + "not found";
                }
            }
            //----- stas 07/06/2004 ------------------------
            if (message_text == "*********  Add Translation **********")
            {
                if (masofonAlias.masofon.Instance.nvo_translator != null)
                {
                    if (masofonAlias.masofon.Instance.nvo_translator.multi_lingual_is_active && (masofonAlias.masofon.Instance.nvo_translator.direction == "left" && ls_create_expressions_on_fly == "yes"))
                    {
                        w_pc_message_detail win = await WindowHelper.Open<w_pc_message_detail>("global_global", "error_number", ai_error_number);
                        message_text = WindowHelper.GetParam<string>(win); 
                    }
                }
            }
            //------------------------------------------------
            return message_text;
        }
        public static void LoadData(int ai_error_number, long ll_lang_number, ref string message_text, ref long li_search)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ai_error_number }, { "@1", ll_lang_number } };
                    string sql = "SELECT pc_message_detail.message_text,  pc_message_detail.message_skey FROM pc_message_detail WHERE (pc_message_detail.message_skey = @0) AND (pc_message_detail.language_skey = @1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        message_text = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        li_search = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData1(int ai_error_number, ref string message_text, ref long li_search)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", ai_error_number } };
                    string sql = "SELECT pc_message_detail.message_text,  pc_message_detail.message_skey FROM pc_message_detail WHERE (pc_message_detail.message_skey = @0) AND (pc_message_detail.language_skey = 1)";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        message_text = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        li_search = UnitOfWork.GetValue(resultRS, 1, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
