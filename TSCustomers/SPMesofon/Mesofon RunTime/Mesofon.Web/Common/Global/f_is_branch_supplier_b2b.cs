using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_is_branch_supplier_b2bClass
	{
		public static string f_is_branch_supplier_b2b(long al_supplier_number) //**********************************************************************************************
		{
			//*Object:						f_is_branch_supplier_b2b
			//*Purpose:					Check if the supplier is B2B enable for the current branch.
			//*										1. Check if the supplier can work in B2B mode with the current branch.
			//*  
			//*Arguments:				Long - Supplier number
			//*Return:						BOOLEAN - String - EDI Number, if suppleir doesn't exist in gds_b2b_branch_suppliers,
			//*														    meaning the supplier does not work in B2B with this branch or that it
			//*														    does not has an EDI number, the function will return "" (empty string).
			//*Date 			Programer			Version	Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*29-05-2007		SharonS				0.1.0 		B2B 			Check branch-supplier B2B enable.
			//************************************************************************************************
			
			string ls_find = null;
			string ls_edi_number = null;
			long? ll_found = 0;
			// Validation Checks
			if (!(masofonAlias.masofon.Instance.gds_b2b_branch_suppliers != null))
			{
				return "";
			}
			ls_find = "supplier_number == " + al_supplier_number.ToString();
			
			
			ll_found = masofonAlias.masofon.Instance.gds_b2b_branch_suppliers.Find(ls_find, 0, masofonAlias.masofon.Instance.gds_b2b_branch_suppliers.RowCount());
			if (ll_found == -1)
			{
				ls_edi_number = "";
			}
			else
			{
				
				ls_edi_number = masofonAlias.masofon.Instance.gds_b2b_branch_suppliers.GetItemValue<string>(ll_found.Value, "suppliers_edi_number");
			}
			return ls_edi_number;
		}
	}
}
