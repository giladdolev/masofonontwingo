namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public struct s_global_parameters
	{
		public int row_nbr;
		public string company_name;
		public string vat_number;
		public string invoice_header;
		public string source_header;
		public string copy_header;
		public string data_line1;
		public string data_line2;
		public string temp_invoice_header;
		public decimal vat_percent;
		public string invoice_receipt_header;
		public string receipt_header;
		public string zikuy_header;
		public ulong manager_number;
		public string manager_name;
		public string employee_message1;
		public string employee_message2;
		public string employee_message3;
		public string automemoryrestart;
		public string r_data_line1;
		public string r_data_line2;
		public long auctions_global_client_number;
		public bool internet_server_active;
		public string email;
		public long pharm_number;
		public long macabi_tran_code;
		public long leumi_tran_code;
		public long menu_window_counter;
		public int b2b_supported;
		public string edi_number;
		public int branch_type;
	}
}
