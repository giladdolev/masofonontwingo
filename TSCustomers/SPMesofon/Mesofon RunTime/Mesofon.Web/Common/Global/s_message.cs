namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public struct s_message
	{
		public string button_picture_ok;
		public string button_picture_yes;
		public string button_picture_no;
		public string button_picture_cancel;
		public bool picture_mode;
		public int write_log_from_level;
		public string error_log_file_name;
		public long background_color;
		public long message_text_color;
		public long message_background_color;
		public string support_contact;
		public string log_file_name;
		public bool message_border;
		public bool transmit_errors;
		public bool transmit_via_service;
		public long max_log_size;
	}
}
