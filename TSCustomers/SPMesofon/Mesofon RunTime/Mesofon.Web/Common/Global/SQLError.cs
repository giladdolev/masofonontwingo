﻿using System;

namespace Mesofon.Common.Global
{
    public class SQLError
    {
        private readonly Exception _exception;
        public long SQLDBCode;

        public SQLError()
        {
            
        }
        public SQLError(Exception ex)
        {
            _exception = ex;
        }

        public int DBHandle()
        {
            return 1;
        }
        public int SqlCode
        {
            get {
                if(_exception == null)
                    return 0;
                return _exception.HResult;

            }
        }

        public string SqlErrText
        {
            get { return _exception.Message; }
        }


        public int SqldbCode
        {
            get { return 0; }
        }

        public long SqlnRows
        {
            get { return 0; }
        }

        public string ServerName { get; set; }

        public string Database { get; set; }

        public string DBMS { get; set; }

        public string LogID { get; set; }

        public string LogPass { get; set; }

        public string UserID { get; set; }

        public string DBPass { get; set; }

        public string DBParm { get; set; }

        public bool AutoCommit { get; set; }
    }
}