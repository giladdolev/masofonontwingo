using System.Threading.Tasks;
using mini_terminal;
using Mesofon.Common;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:52 (Utc)
	// Comments:        
	// 
	public static class f_mini_terminal_messageboxClass
	{
		public static async Task<long> f_mini_terminal_messagebox(string as_title, string as_error_string, string as_icon, string as_buttons, long al_default_button_focus, string as_msg_string) //--------------------------------------------------------------------
		{
			//Function:			public f_mini_terminal_messagebox
			//
			// Returns:         Long
			//
			// Parameters:      a) value String as_title 						---> window title
			//						b) value String as_error_string 				---> error text
			//						c) value String as_icon							---> message icon 
			//						d) value String as_buttons 					---> buttons to be displayed
			//						e) value String as_default_button_focus 	---> sets default button focus
			//						f) value String as_msg_string					---> message text
			// 
			// Copyright  - Stas
			//
			// Date Created: 01/08/2005
			//
			// Description:	
			// 						a)	istr_arg.a_string[1] ---> window title
			//						b) istr_arg.a_string[2] ---> error text
			//						c) istr_arg.a_string[3] ---> message icon 
			//						d) istr_arg.a_string[4] ---> buttons to be displayed
			//						e) istr_arg.a_string[5] ---> sets default button focus
			//						f) istr_arg.a_string[6] ---> message text
			//--------------------------------------------------------------------------------
			// Modifications:
			// Date            Author              Comments
			//------------------------------------------------------------------------------
			
			s_array_arguments lstr_arg = new s_array_arguments();
			long ll_return = 0;
			lstr_arg.a_string[6] = "";
			lstr_arg.a_string[1] = as_title;
			lstr_arg.a_string[2] = as_error_string;
			lstr_arg.a_string[3] = as_icon;
			lstr_arg.a_string[4] = as_buttons;
			lstr_arg.a_string[5] = al_default_button_focus.ToString();
			lstr_arg.a_string[6] = as_msg_string;

            w_mini_terminal_msg win = await WindowHelper.Open<w_mini_terminal_msg>("mini_terminal_mini_terminal", "istr_arg", lstr_arg);
            ll_return = win.il_return_valueProperty;
            return ll_return;
		}
	}
}
