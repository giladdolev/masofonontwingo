using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_get_supplier_distributor_ediClass
	{
		public static string f_get_supplier_distributor_edi(long al_supplier_number, long al_distributor_number) //***************************************************************************************************************
		{
			//*Object:						f_get_supplier_distributor_edi
			//*Purpose:					Get the supplier distributor EDI number.
			//*  
			//*Arguments:				Long - Distributor number
			//*Return:						String - Distributor EDI Number, if distributor doesn't exist in gds_supplier_distributors_edi,
			//*														    meaning the supplier is not a distributor, the function will return "" (empty string).
			//*Date 			Programer		Version		Task#			SPUCM		Description
			//*---------------------------------------------------------------------------------------------------------------------------------------------
			//*27-01-2009		SharonS			1.2.42.0		CR#1107		00000671 	Get supplier distributor EDI.
			//*****************************************************************************************************************
			
			string ls_find = null;
			string ls_distributor_edi = null;
			long? ll_found = 0;
			long ll_status = 0;
			// Validation Checks
			if (!(masofonAlias.masofon.Instance.gds_suppliers_distributors_edi != null))
			{
				return "";
			}
			ls_find = "supplier_number == " + al_supplier_number.ToString() + " and distributor_number == " + al_distributor_number.ToString();
			
			
			ll_found = masofonAlias.masofon.Instance.gds_suppliers_distributors_edi.Find(ls_find, 1, masofonAlias.masofon.Instance.gds_suppliers_distributors_edi.RowCount());
			if (ll_found == 0)
			{
				ls_distributor_edi = "";
			}
			else
			{
				// AlexKh - 1.2.44.0 - 2009-05-05 - SPUCM00001159 - check status of record for specific distributor
				
				ll_status = masofonAlias.masofon.Instance.gds_suppliers_distributors_edi.GetItemValue<long>((int)ll_found, "is_active");
				if (ll_status == 0)
				{
					ls_distributor_edi = "";
				}
				else
				{
					
					ls_distributor_edi = masofonAlias.masofon.Instance.gds_suppliers_distributors_edi.GetItemValue<string>(ll_found.Value, "edi_number");
				}
			}
			return ls_distributor_edi;
		}
	}
}
