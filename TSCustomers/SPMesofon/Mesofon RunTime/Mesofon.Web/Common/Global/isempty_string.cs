namespace global
{
	// Creation Time:   08/11/2015 10:20:52 (Utc)
	// Comments:        
	// 
	public static class isempty_stringClass
	{
		public static bool isempty_string(string i_string_value)
		{
			bool return_value = false;
			if ((string.IsNullOrEmpty(i_string_value)) || ((i_string_value == null) || (i_string_value.Trim().Length == 0)))
			{
				return_value = true;
			}
			return (return_value);
		}
	}
}
