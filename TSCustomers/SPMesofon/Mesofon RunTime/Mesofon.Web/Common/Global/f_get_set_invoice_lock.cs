using System;
using System.DataAccess;
using System.Globalization;
using System.Threading.Tasks;
using Common.Transposition.Extensions;
using Mesofon.Repository;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_get_set_invoice_lockClass
	{
		public static async Task<bool> f_get_set_invoice_lock(long al_branch_number, long al_supplier_number, string as_doc_type, long al_doc_number, int ai_state) //**********************************************************************************************
		{
			//*Object:				f_get_set_invoice_lock
			//*Purpose: 			Lock or Unlock relevant invoice/packing list
			//*Arguments: 		Long 		- al_branch_number 		- branch number
			//*						Long		- al_supplier_number 	- supplier number
			//*						String	- as_doc_type				- type of document invoice or packing list
			//*						Long		- al_doc_number			- document number
			//*						Integer	- ai_state					- working mode Unlock/Lock/Select from mini terminal or w_pack_ord_inv_from_suppliers
			//*Return:				Boolean 	- TRUE/FALSE
			//*Date				Programer		Version	Task#	 					Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*25-06-2009		AlexKh			1.2.45.0	SPUCM00001243		Initial version
			//************************************************************************************************
			
			long ll_ret = 0;
			long ll_i = 0;
			long ll_start = 0;
			long ll_length = 0;
			string ls_lock_state = null;
			string ls_type = null;
			string ls_old_query = null;
			string ls_new_query = null;
			string ls_ret = null;
			DateTime? ldt_today = null;
			IRepository lds_docs_list = null;
            // AlexKh - 1.2.45.12 - 2009-02-01 - SPUCM00001900 - check if lock needed
            string temp =await  f_get_parameter_valueClass.f_get_parameter_value("lock_invoices", "boolean", "true", "Lock invoices to prevent parallel usage.", "gds_find");

            if( temp.ToLower(CultureInfo.InvariantCulture) == "false")
			{
				return true;
			}
			// AlexKh - 1.2.45.12 - 2009-02-01 - SPUCM00001900 - handle return documents lock
			if (as_doc_type.ToUpper() == "R")
			{
			    lds_docs_list = new d_invoices_lockRepository();

				ls_type = "r";
			}
			else if (as_doc_type.ToUpper() == "I")
			{
			    lds_docs_list = new d_invoices_lockRepository();
				ls_type = "p";
			}
			else if (as_doc_type.ToUpper() == "P")
			{
			    lds_docs_list = new d_packing_list_lockRepository();
				ls_type = "p";
			}
			// AlexKh - 1.2.46.2 - 2010-08-18 - SPUCM00002357 - improve query
			ls_old_query = lds_docs_list.GetSQLSelect();
			if (al_supplier_number > 0)
			{
				ll_start = ls_old_query.IndexOf("OR @1");
				ll_length = ls_old_query.IndexOf(")", (int)(ll_start - 1));
				if (ll_start > 0 && ll_length > 0)
				{
					ll_length = ll_length - ll_start;
					ls_new_query = ls_old_query.Substring(0, (int)ll_start) + "" + ls_old_query.Substring((int)(ll_start + ll_length));
					ls_old_query = ls_new_query.Trim();
				}
			}
			if (al_doc_number > 0)
			{
				ll_start = ls_old_query.IndexOf("OR @3");
				ll_length = ls_old_query.IndexOf(")", (int)(ll_start - 1));
				if (ll_start > 0 && ll_length > 0)
				{
					ll_length = ll_length - ll_start;
					ls_new_query = ls_old_query.Substring(0, (int)ll_start) + "" + ls_old_query.Substring((int)(ll_start + ll_length));
					ls_old_query = ls_new_query.Trim();
				}
			}

            ls_ret = lds_docs_list.Modify("DataWindow.Table.Select ="+ ls_old_query );

            //Choose the working mode
            switch (ai_state) {
				case 0:
				case 3:
					//Unlock mode
					
					ll_ret = lds_docs_list.Retrieve(al_branch_number, al_supplier_number, ls_type, al_doc_number, "1");
					if (ll_ret > 0)
					{
						for (ll_i = 0; ll_i < ll_ret; ll_i++)
						{
							 
							lds_docs_list.SetItem(ll_i, "create_mode", "0");
						}
					    using (UnitOfWork unitOfWork = new UnitOfWork())
					    {
                            ll_ret = lds_docs_list.Update(unitOfWork);
					    }
					    if (ll_ret != 1 && ai_state == 0)
						{
						    await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", "שחרור נעילה עבור אחד המסמכים נכשל");
						}
						
						 
						
						return ll_ret == 1;
						// AlexKh - 1.2.45.28 - 2010-04-21 - SPUCM00002014 - if no invoice exist yet
					}
					else if (ai_state == 3 && ll_ret == 0)
					{
						
						 
						
						return true;
					}
					break;
				case 1:
				case 4:
					//Lock mode
					
					ll_ret = lds_docs_list.Retrieve(al_branch_number, al_supplier_number, ls_type, al_doc_number, "-1");
					if (ll_ret == 1)
					{
						
						ls_lock_state = lds_docs_list.GetItemValue<string>(ll_ret - 1, "create_mode");
						if (ls_lock_state == null)
						{
							ls_lock_state = "0";
						}
						if (ls_lock_state == "1")
						{
							if (ai_state == 1)
							{
							    await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", "משתמש אחר נועל מסמך זה");
							}
							 
							
							return false;
						}
						else
						{
							ldt_today = masofonAlias.masofon.Instance.set_machine_time();
							 
							lds_docs_list.SetItem(0, "last_update_datetime", ldt_today);
							 
							lds_docs_list.SetItem(0, "create_mode", "1");

						    using (UnitOfWork unitOfWork = new UnitOfWork())
						    {
                                ll_ret = lds_docs_list.Update(unitOfWork);
						    }

							if (ll_ret != 1 && ai_state == 1)
							{
							    await masofonAlias.masofon.Instance.guo_msg.uf_msg("שגיאה", "", "stopsign!", "נעילת מסמך נכשלה");
							}
							
							
							return ll_ret == 1;
						}
					}
					break;
				case 2:
				case 5:
					//Select mode
					
					ll_ret = lds_docs_list.Retrieve(al_branch_number, al_supplier_number, ls_type, al_doc_number, "-1");
					if (ll_ret == 1)
					{
						
						ls_lock_state = lds_docs_list.GetItemValue<string>(ll_ret, "create_mode");
					}
					
					
					return ls_lock_state == "1";
			}
			
			
			return false;
		}
	}
}
