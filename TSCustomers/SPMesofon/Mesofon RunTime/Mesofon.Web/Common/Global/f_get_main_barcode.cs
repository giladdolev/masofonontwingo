using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:50 (Utc)
	// Comments:        
	// 
	public static class f_get_main_barcodeClass
	{
		public static async Task<string> f_get_main_barcode(long al_item_number) // f_get_main_barcode(al_item_number)
		{
			string ls_barcode = null;
			if (true)
			{
				f_get_main_barcodeClass.LoadData(al_item_number, ref ls_barcode);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
			{


			    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Sql Error " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
				return "";
			}
			return ls_barcode;
		}
		public static void LoadData(Int64 al_item_number, ref string ls_barcode)
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_item_number } };
                    string sql = "SELECT barcode FROM items WHERE number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
					while (resultRS.Read())
					{
                        ls_barcode = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
					}
				}
			}
            catch (Exception ex)
			{
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
			}
		}
	}
}
