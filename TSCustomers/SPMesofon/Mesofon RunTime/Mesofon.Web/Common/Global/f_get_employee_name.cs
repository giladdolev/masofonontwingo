using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:20:50 (Utc)
    // Comments:        
    // 
    public static class f_get_employee_nameClass
    {
        public static async Task<string> f_get_employee_name(long al_branch_number, long al_emp_number) // f_get_employee_name(al_branch_number, al_emp_number)
        {
            string ls_name = null;
            if (true)
            {
                f_get_employee_nameClass.LoadData(al_branch_number, al_emp_number, ref ls_name);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
            {


                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Sql Error " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                return "";
            }
            return ls_name;
        }
        public static void LoadData(Int64 al_branch_number, Int64 al_emp_number, ref string ls_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_emp_number } };
                    string sql = "SELECT IsNull(name, '') FROM employees WHERE branch_number = @0 AND number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_name = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
