using System.Web.VisualTree.Elements;
using Mesofon.Common;

namespace global
{
	// Creation Time:   08/11/2015 10:20:50 (Utc)
	// Comments:        
	// 
	public static class f_dw_get_attributesClass
	{
		public static string f_dw_get_attributes(GridElement dw_arg, string @object, string attributes)////////////////////////////////////////////////////////////////////////
		{
			//
			//		Function:  F_Dw_Get_Attributes
			//		 Purpose:  The following function will return the values of datawindow attributes 
			//					  by passing a comma separated list of attribute names. 
			//					  Returned attributes are separated by "~n' characters
			//					   
			//
			//	     ARGUMENTS:
			//						dw_arg	 datawindow control passed by value
			//						object  string passed by value
			//						attributes a string passed by value
			//
			//      Log:
			//
			//			Who				When				What
			//-----------------------------------------------------------------------
			//			Powersoft 							initial version
			//
			//////////////////////////////////////////////////////////////////////////
			string command = null;
			// make up the command string template
			// first remove any extraneous spaces
			attributes = f_global_replaceClass.f_global_replace(attributes, " ", "");
			// add a comma to the front of the list as we will do our 
			//replacement process using comma's as a guide
			attributes = "," + attributes;
			// now replace all of the commas with the name of the objects we 
			// are getting info for
			command = f_global_replaceClass.f_global_replace(attributes, ",", " " + @object + ".");
			// Now ask for the value and return them

            return dw_arg.Describe(command,attributes);
		}
	}
}
