using System;
using System.Globalization;
using Mesofon.Repository;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_is_marlog_distributorClass
	{
        public static async System.Threading.Tasks.Task<long> f_is_marlog_distributor(long al_branch_number, long al_distributor_number) //**********************************************************************
        {
            //*Object:				f_is_marlog_distributor
            //*Function Name:	f_is_marlog_distributor
            //*Purpose: 			Check if branch or distributor are from marlog
            //*Arguments: 		Long al_branch_number
            //						Long al_distributor_number
            //*Return:				long
            //*Date				Programer		Version		Task#	 		Description
            //*-----------------------------------------------------------------------
            //*17-02-2014		AlexKh			1.1.1.18	CR#1171		Initial version
            //***********************************************************************

            string ls_find = null;
            long? ll_found = 0;
            long ll_marlog_number = 0;
            long ll_marlog_distributor = 0;
            if (al_distributor_number == 0)
            {
                al_distributor_number = 0;
            }
            if (al_branch_number == 0)
            {
                al_branch_number = 0;
            }
            if (al_branch_number == 0 && al_distributor_number == 0)
            {
                return 0;
            }
            string temp = await f_get_parameter_valueClass.f_get_parameter_value("multi_marlog_on", "boolean", "false", "Turn on multiple marlogs", "gds_find");
            if (temp.ToLower(CultureInfo.InvariantCulture) == "true")
            {
                if (!(masofonAlias.masofon.Instance.gds_marlog_distributor != null))
                {
                    masofonAlias.masofon.Instance.gds_marlog_distributor = new ds_marlog_distributor_translateRepository();


                    masofonAlias.masofon.Instance.gds_marlog_distributor.Retrieve(34);
                }

                if (masofonAlias.masofon.Instance.gds_marlog_distributor.RowCount() > 0)
                {
                    if (al_branch_number > 0)
                    {
                        ls_find = "branch_number == \"" + al_branch_number.ToString() + "\"";
                    }
                    if (al_distributor_number > 0)
                    {
                        if (!isempty_stringClass.isempty_string(ls_find))
                        {
                            ls_find += " or ";
                        }
                        ls_find += "distributor_number == \"" + al_distributor_number.ToString() + "\"";
                    }


                    ll_found = masofonAlias.masofon.Instance.gds_marlog_distributor.Find(ls_find, 0, masofonAlias.masofon.Instance.gds_marlog_distributor.RowCount());
                    if (ll_found >= 0)
                    {

                        ll_marlog_number = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_found.Value, "branch_number"));

                        ll_marlog_distributor = Convert.ToInt64(masofonAlias.masofon.Instance.gds_marlog_distributor.GetItemValue<string>(ll_found.Value, "distributor_number"));
                        if (al_branch_number > 0)
                        {
                            return ll_marlog_distributor;
                        }
                        else
                        {
                            return ll_marlog_number;
                        }
                    }
                }
            }
            else
            {
                if (al_branch_number > 0)
                {
                    return 99; //return default distributor
                }
                else
                {
                    //return default marlog number
                    return 999;
                }
            }
            return 0;
        }
    }
}
