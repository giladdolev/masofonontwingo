using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Threading.Tasks;
using masofon;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:52 (Utc)
	// Comments:        
	// 
	public static class f_write_db_errorClass
	{
		public static async Task<bool> f_write_db_error(string as_object_name, string as_function_name, SQLError a_transaction, bool ab_make_rollback, string as_usertext) // f_write_db_error(as_object_name, as_function_name, a_transaction, 
		{
			// 					 ab_make_rollback, as_usertext)
			long ll_sqlnrows = 0;
			int li_app_number = 0;
			int li_tran_began = 0;
			int li_sql_code = 0;
			int li_db_err_code = 0;
			string ls_errtext = null;
			string ls_user_text = null;
			DateTime? ldt_now_datetime = null;
			if ((!(a_transaction != null)) || a_transaction == null)
			{
				return false;
			}
			li_app_number = 2; // 1= touch , 2= office , 3= stnsrv ,  ...
			ldt_now_datetime = masofonAlias.masofon.Instance.set_machine_time();
			ls_user_text = as_usertext;
            li_sql_code = a_transaction.SqlCode;
			li_db_err_code = a_transaction.SqldbCode;
			ls_errtext = a_transaction.SqlErrText;
			ll_sqlnrows = a_transaction.SqlnRows;
			if (masofonAlias.masofon.Instance.gb_transaction_began)
			{
				li_tran_began = 1;
			}
			if (li_sql_code == 100 && isempty_stringClass.isempty_string(ls_errtext))
			{
				ls_errtext = ls_user_text;
				ls_user_text = "No rows found";
			}
			else if (isempty_stringClass.isempty_string(ls_errtext))
			{
				ls_errtext = ls_user_text;
				ls_user_text = "Rows Affected : " + ll_sqlnrows.ToString();
			}
			if (ab_make_rollback)
			{
			    await f_rollbackClass.f_rollback();
			}
			f_write_db_errorClass.UpdateData(ldt_now_datetime, li_app_number, as_object_name, as_function_name, li_sql_code, li_db_err_code, ls_errtext, ls_user_text, li_tran_began);
			
			return (masofonAlias.masofon.Instance.sqlca.SqlCode == 0);
		}
		public static void UpdateData(DateTime? ldt_now_datetime, int li_app_number, string as_object_name, string as_function_name, int li_sql_code, int li_db_err_code, string ls_errtext, string ls_user_text, int li_tran_began)
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", masofonAlias.masofon.Instance.gs_station.station_number }, { "@2", ldt_now_datetime }, { "@3", li_app_number }, { "@4", as_object_name }, { "@5", as_function_name }, { "@6", li_sql_code }, { "@7", li_db_err_code }, { "@8", ls_errtext }, { "@9", ls_user_text }, { "@10", li_tran_began } };
                    string sql = "INSERT INTO db_error_log (branch_number,  station_number,  error_datetime,  application_number,  object_name,  function_name,  sql_error_code,  sql_db_error_code,  sql_error_text,  user_text,  transaction_open) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  @6,  @7,  @8,  @9,  @10)";
                    unitOfWork.Execute(sql, parameters);
				}
			}
            catch (Exception ex)
			{
                masofon.masofon.Instance.sqlca = new SQLError(ex);
			}
		}
	}
}
