using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:50 (Utc)
	// Comments:        
	// 
	public static class f_get_db_versionClass
	{
		public static long f_get_db_version() //**************************************************************************************
		{
			//f_get_db_version: Andy 07-09-2005
			//<DESC>	Function to return the DB versione </DESC>
			//<RETURN>	DB version in string </RETURN>			
			//<ACCESS> Public </ACCESS>
			//<ARGS>	no arguments </ARGS>
			//<USAGE> 	We can use this function to show DB version </USAGE>
			//***************************************************************************************
			
			long ll_db_version = 0;
			long ll_version_to_return = 0;
			// Get DB version number
			if (true)
			{
				f_get_db_versionClass.LoadData(ref ll_db_version);
			}
			
			if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0 && !isempty_stringClass.isempty_string(ll_db_version.ToString()))
			{
				ll_version_to_return = ll_db_version;
			}
			else
			{
				ll_version_to_return = -1;
			}
			return ll_version_to_return;
		}
		public static void LoadData(ref long ll_db_version)
		{
			try
			{
                using (UnitOfWork unitOfWork = new UnitOfWork())
				{
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number } };
                    string sql = "SELECT last_sql_file FROM global_parameters WHERE serial_number = @0";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
					while (resultRS.Read())
					{
                        ll_db_version = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
					}
				}
			}
            catch (Exception ex)
			{
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
			}
		}
	}
}
