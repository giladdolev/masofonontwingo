using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_get_parent_supplier_ediClass
	{
		public static string f_get_parent_supplier_edi(long al_supplier_number) //***************************************************************************************************************
		{
			//*Object:						f_get_parent_supplier_edi
			//*Purpose:					Get the parent supplier EDI number, if the supplier has a parent.
			//*  
			//*Arguments:				Long - Supplier number
			//*Return:						String - EDI Number, if supplier doesn't exist in gds_b2b_branch_suppliers,
			//*														    meaning the supplier does not has a parent, or if supplier 
			//*														    does not has an EDI number, the function will return "" (empty string).
			//*Date 			Programer		Version		Task#			SPUCM		Description
			//*---------------------------------------------------------------------------------------------------------------------------------------------
			//*20-11-2008		SharonS			1.2.37.11	CR#1094		00000671 	Get parent supplier EDI number.
			//*****************************************************************************************************************
			
			string ls_find = null;
			string ls_parent_edi_number = null;
			long? ll_found = 0;
			// Validation Checks
			if (!(masofonAlias.masofon.Instance.gds_parent_supplier_edi != null))
			{
				return "";
			}
			ls_find = "supplier_number == " + al_supplier_number.ToString();
			
			
			ll_found = masofonAlias.masofon.Instance.gds_parent_supplier_edi.Find(ls_find, 1, masofonAlias.masofon.Instance.gds_parent_supplier_edi.RowCount());
			if (ll_found == 0)
			{
				ls_parent_edi_number = "";
			}
			else
			{
				
				ls_parent_edi_number = masofonAlias.masofon.Instance.gds_parent_supplier_edi.GetItemValue<string>(ll_found.Value, "parent_supp_edi_number");
			}
			return ls_parent_edi_number;
		}
	}
}
