namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public struct s_titles
	{
		public string[] owner;
		public string[] manager;
		public string[] waiter;
		public string[] barmen;
		public string[] cashier;
		public string[] in_charge;
	}
}
