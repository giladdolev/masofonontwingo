using System;
using System.Collections.Generic;
using System.Data;
using System.DataAccess;
using System.Threading.Tasks;
using System.Web.VisualTree.Elements;
using masofon;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:20:49 (Utc)
    // Comments:        
    // 
    public static class f_calculate_datatable_rows_by_branchClass
    {
        public static async Task<long> f_calculate_datatable_rows_by_branch(long al_branch_number, long al_table_number) // f_calculate_datatable_rows_by_branch(al_branch_number, al_table_number)
        {
            long ll_next_serial_number = 0;
            string ls_table_name = null;
            string ls_database_type = null;
            // Adaptive Server Enterprise , Miscrosoft SQL Server, Adaptive Server Anywhere
            ls_database_type = masofonAlias.masofon.Instance.gs_database_type.ToUpper();
            if (ls_database_type == null)
            {
                ls_database_type = "";
            }
            if (ls_database_type != "ASE" && (ls_database_type != "SQL" && ls_database_type != "ASA"))
            {
                await MessageBox.Show("DataBase Type Must Be One Of (ASE, SQL, ASA), Get : " + ls_database_type, "f_calculate_datatable_rows_by_branch(" + al_branch_number.ToString() + " ," + al_table_number.ToString() + ")");
                return 0;
            }
            await f_begin_tranClass.f_begin_tran();
            f_calculate_datatable_rows_by_branchClass.UpdateData(al_branch_number, al_table_number);

            if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
            {
                await f_rollbackClass.f_rollback();

                await MessageBox.Show("UPDATE serial_number_control Failed : " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "f_calculate_datatable_rows_by_branch(" + al_branch_number.ToString() + " ," + al_table_number.ToString() + ")");
                return -1;
            }
            if (true)
            {
                f_calculate_datatable_rows_by_branchClass.LoadData(al_branch_number, al_table_number, ref ll_next_serial_number);
            }

            if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
            {
                await f_commitClass.f_commit();
                return ll_next_serial_number;
            }

            else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 100)
            {
                f_calculate_datatable_rows_by_branchClass.UpdateData1(al_branch_number, al_table_number, ls_table_name);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
                {
                    await f_commitClass.f_commit();
                    return 1;
                }
                else
                {
                    await f_rollbackClass.f_rollback();

                    await MessageBox.Show("INSERT serial_number_control Failed : " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "f_calculate_datatable_rows_by_branch(" + al_branch_number.ToString() + " ," + al_table_number.ToString() + ")");
                    return -1;
                }
            }
            else
            {
                await f_rollbackClass.f_rollback();

                await MessageBox.Show("SELECT serial_number_control Failed : " + masofonAlias.masofon.Instance.sqlca.SqlErrText, "f_calculate_datatable_rows_by_branch(" + al_branch_number.ToString() + " ," + al_table_number.ToString() + ")");
                return -1;
            }
            return 0;
        }
        public static void UpdateData(Int64 al_branch_number, Int64 al_table_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_table_number } };
                    string sql = "UPDATE serial_number_control SET next_serial_number = next_serial_number + 1 WHERE branch_number = @0 AND table_number = @1";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofonAlias.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void UpdateData1(Int64 al_branch_number, Int64 al_table_number, string ls_table_name)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_table_number }, { "@2", ls_table_name } };
                    string sql = "INSERT INTO serial_number_control (branch_number,  table_number,  table_name,  next_serial_number) VALUES (@0,  @1,  @2,  1)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData(Int64 al_branch_number, Int64 al_table_number, ref long ll_next_serial_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    var noData = true;
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", al_branch_number }, { "@1", al_table_number } };
                    string sql = "SELECT IsNull(next_serial_number, 0) FROM serial_number_control WHERE branch_number = @0 AND table_number = @1";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ll_next_serial_number = UnitOfWork.GetValue(resultRS, 0, Convert.ToInt64);
                        noData = false;
                    }
                    if (noData)
                    {
                        throw new MvcSite.Common.Global.NoDataException(100);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
