using System;

namespace global
{
	// Creation Time:   08/11/2015 10:20:52 (Utc)
	// Comments:        
	// 
	public static class isempty_datetimeClass
	{
		public static bool isempty_datetime(DateTime? adt_datetime) // isempty_datetime(adt_datetime)
		{
			return (adt_datetime == null || adt_datetime == default(DateTime) || (adt_datetime == Convert.ToDateTime("01/01/1900").Add(Convert.ToDateTime("00:00:00").TimeOfDay)));
		}
	}
}
