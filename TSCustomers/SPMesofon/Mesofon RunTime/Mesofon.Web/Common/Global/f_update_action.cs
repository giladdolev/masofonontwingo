using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Threading.Tasks;
using masofon;
using Mesofon.Common.Global;
using masofonAlias = masofon;

namespace global
{
    // Creation Time:   08/11/2015 10:20:52 (Utc)
    // Comments:        
    // 
    public static class f_update_actionClass
    {
        public static async Task<int> f_update_action(int action_number, long manager_number)
        {
            long serial_number = 0;
            serial_number = f_calculate_datatable_rowsClass.f_calculate_datatable_rows(21);
            if (serial_number == -1)
            {
                return -1;
            }
            DateTime? now_date_time = default(DateTime);
            now_date_time = masofonAlias.masofon.Instance.set_machine_time();
            f_update_actionClass.UpdateData(serial_number, now_date_time, manager_number, action_number);
            
            if (masofonAlias.masofon.Instance.sqlca.SqlCode == -1)
            {


                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Sql Error " + masofonAlias.masofon.Instance.sqlca.SqlCode.ToString(), "", "stopsign!", masofonAlias.masofon.Instance.sqlca.SqlErrText);
                return -1;
            }
            return 1;
        }
        public static void UpdateData(long serial_number, DateTime? now_date_time, Int64 manager_number, int action_number)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", serial_number }, { "@2", now_date_time }, { "@3", masofonAlias.masofon.Instance.gs_station.store_number }, { "@4", masofonAlias.masofon.Instance.gs_station.station_number }, { "@5", masofonAlias.masofon.Instance.gs_vars.active_owner }, { "@6", manager_number }, { "@7", action_number }, { "@8", masofonAlias.masofon.Instance.gs_user_id } };
                    string sql = "INSERT INTO action_control (branch_number,  serial_number,  action_datetime,  store_number,  station_number,  manager_number,  employee_number,  action_number,  user_id) VALUES (@0,  @1,  @2,  @3,  @4,  @5,  @6,  @7,  @8)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
