using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;

namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        Generic Window parameter passing structure
	// 
	public struct str_pass
	{
		public object po_caller;
		public string s_win_title;
		public string s_dataobject;
		public string s_win_parms;
		public string s_open_style;
        public SQLError tr_trans;
		public WindowElement w_frame;
		public int i_idx;
		public string s_action;
		public string[] s;
		public double[] d;
		public bool[] b;
		public System.DateTime[] dt;
		public System.DateTime[] dates;
		public object[] po;
		public long[] l;
	}
}
