using System.Threading.Tasks;
using Common.Transposition.Extensions;
using Mesofon.Common;
using Mesofon.Repository;
using masofonAlias = masofon;


namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_is_pack_barcodeClass
	{
		public static async Task<s_array_arguments> f_is_pack_barcode(bool ab_open_window, string as_barcode, long al_supplier_number) //**********************************************************************************************
		{
			//*Object:								f_is_pack_barcode
			//*Function/Event  Name:			
			//*Purpose:						For a given Barcode Check if the barcode is of a package 	
			//*									If so open the window:rw_packages
			//*
			//*Arguments:				Pass By			Argument Type					Argument Name
			//*							-----------------------------------------------------------
			//*							Value				Boolean							ab_open_window
			//*							Value				String							as_barcode
			//*										
			//*Return:								s_array_arguments:
			//*													
			//*
			//*Date				Programer				Version		Task#			Description
			//*------------------------------------------------------------------------------------------------------------------------
			//*01-01-2008		Pnina S.G    			0.2.0 			B2B 			Initiation
			//************************************************************************************************
			
			long ll_row_count = 0;
		    IRepository lds_package;
			s_reports_db lstr_reports_db = default(s_reports_db);
			s_array_arguments lstr_mess = new s_array_arguments();
			long ll_item_qantity = 0;
			// Get DB 
			lstr_reports_db =await masofonAlias.masofon.Instance.gnvo_reports_db.uf_get_db_transaction("uo_mini_terminal_header", false);
			switch (lstr_reports_db.status) {
				case "NON":
				    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error uo_mini_terminal_header", "", "stopsign!", "Can not connect to DB");
					lstr_mess.a_boolean[0] = false;
					return lstr_mess;
				//..Report will run entirely on LIVE DB
				case "MAIN":
					masofonAlias.masofon.Instance.SQLCA_REPORTS = masofonAlias.masofon.Instance.sqlca;
					//..Report will run entirely on Reports DB
					break;
				case "REP":
					masofonAlias.masofon.Instance.SQLCA_REPORTS = lstr_reports_db.report_tran;
					//..Report will runentirely on LIVE DB
					break;
				case "SPLIT":
					masofonAlias.masofon.Instance.SQLCA_REPORTS = masofonAlias.masofon.Instance.sqlca;
					break;
				default:
				    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error uo_mini_terminal_header", "", "stopsign!", "Can not connect to DB");
					lstr_mess.a_boolean[0] = false;
					return lstr_mess;
			}
			lds_package = new d_packageRepository();
			
			
			
			ll_row_count = lds_package.Retrieve(al_supplier_number, as_barcode);
			if (ll_row_count > 0)
			{
				if (ab_open_window)
				{
				    rw_packages win = await WindowHelper.Open<rw_packages>("global_global", "repository", lds_package);

                    lstr_mess = (s_array_arguments)WindowHelper.GetParam<s_array_arguments>(win); 
				}
			}
			else
			{
				lstr_mess.a_boolean[0] = false;
			}
			if (lds_package != null)
			{


			}
			return lstr_mess;
		}
	}
}
