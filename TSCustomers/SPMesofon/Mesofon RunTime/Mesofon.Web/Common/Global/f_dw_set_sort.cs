using System;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Extensions;
using Mesofon.Common;

namespace global
{
    // Creation Time:   08/11/2015 10:20:50 (Utc)
    // Comments:        
    // 
    public static class f_dw_set_sortClass
    {
        public static void f_dw_set_sort(GridElement dw) // Add the column whose label was clicked on (as identified by
        {
            // <column name> + _t) to the sort criteria and sort.
            string s_object = null;
            string s_col_name = null;
            string ls = null;
            //ls = dw.GetBandAtPointer ( )
            //MessageBox("", ls)
            if (dw.GetBandAtPointer().Substring(0, 7) == "header" + "\t")
            {
                s_object = dw.GetObjectAtPointer();
                s_object = s_object.Substring(0, s_object.IndexOf("\t") - 1);
                 
                if (dw.Describe(s_object, "Tag") == "-1")
                {
                    return;
                }
                s_col_name = s_object.Substring(0, s_object.Length - 2);
                // 3d lowered border

                dw.Modify(s_object, "border", "5");
                if (Convert.ToString(dw.Tag) == s_col_name + "A")
                {
                    
                    dw.SetSort(s_col_name + " DESC");
                    dw.Tag = s_col_name + "D";
                }
                else
                {
                    
                    dw.SetSort(s_col_name + " ASC");
                    dw.Tag = s_col_name + "A";
                }
                
                dw.Sort();
                // 3d raised border

                dw.Modify(s_object, "border", "6");
            }
        }
    }
}
