using System.Web.VisualTree.Elements;
using System;
using System.DataAccess;
using System.Threading.Tasks;
using Common.Transposition.Extensions;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:52 (Utc)
	// Comments:        
	// 
	public static class f_update_adhocClass
	{
		public static async Task<bool> f_update_adhoc(int ai_state, long? al_order_number, IRepository ads_adhoc_update_list) //**********************************************************************************************
		{
			//*Object:				f_update_adhoc
			//*Function Name:	f_update_adhoc
			//*Purpose: 			update adhocs in the database
			//*Arguments: 		Integer	ai_state	0 - proceed with update
			//*												1 - rollback locked adhocs
			//*						Long					al_order_number
			//*Return:				Boolean	TRUE/FALSE
			//*Date				Programer		Version	Task#	 		Description
			//*-------------------------------------------------------------------------------------------------
			//*08-07-2013		AlexKh			1.1.1.10	CR#1169		Initial version
			//************************************************************************************************
			
			long ll_i = 0;
			long ll_count = 0;
			long ll_ret = 0;
			long ll_active = 0;
			DateTime? ldt_usage_date = null;
			DateTime? ldt_current_datetime = null;
			long TEMPORARY_LOCKED = 2;
			long FINALY_LOCKED = 3;
			long UNLOCKED = 1;
			
			ll_count = ads_adhoc_update_list.RowCount();
			if (ll_count > 0)
			{
				ldt_current_datetime = masofonAlias.masofon.Instance.set_machine_time();
                ldt_usage_date = default(DateTime); 
				switch (ai_state) {
					case 0:
						//Finalize Update
						for (ll_i = 0; ll_i < ll_count; ll_i++)
						{



                            ads_adhoc_update_list.SetItemStatus((int)ll_i, 0, ModelBuffer.Primary, ModelAction.UpdateByKeys);
							
							ldt_usage_date = ads_adhoc_update_list.GetItemValue<DateTime>((int)ll_i, "usage_datetime");
							if (!isempty_datetimeClass.isempty_datetime(ldt_usage_date)) //Trying to lock ad-hoc
							{
								 
								ads_adhoc_update_list.SetItem(ll_i, "usage_datetime", ldt_current_datetime);
								 
								ads_adhoc_update_list.SetItem(ll_i, "order_number", al_order_number.ToString());
								
								 
								ads_adhoc_update_list.SetItem(ll_i, "item_number", ads_adhoc_update_list.GetItemValue<Int64>((int)ll_i, "item_number"));
								
								 
								ads_adhoc_update_list.SetItem(ll_i, "row_serial_number", ads_adhoc_update_list.GetItemValue<Int64>((int)ll_i, "row_serial_number"));
								 
								ads_adhoc_update_list.SetItem(ll_i, "is_active", FINALY_LOCKED.ToString());
								//Trying to Unlock ad-hoc
							}
							else if (isempty_datetimeClass.isempty_datetime(ldt_usage_date))
							{
								 
								ads_adhoc_update_list.SetItem(ll_i, "usage_datetime", ldt_usage_date.ToString());
								 
								ads_adhoc_update_list.SetItem(ll_i, "order_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "item_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "row_serial_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "is_active", UNLOCKED.ToString());
							}
						}
						//Rollback
						break;
					case 1:
						for (ll_i = 0; ll_i < ll_count; ll_i++)
						{
                            ads_adhoc_update_list.SetItemStatus((int)ll_i, 0, ModelBuffer.Primary, ModelAction.None);
							
							ll_active = ads_adhoc_update_list.GetItemValue<long>((int)ll_i, "is_active");
							if (ll_active == UNLOCKED) //Rollback to unlocked	
							{
								 
								ads_adhoc_update_list.SetItem(ll_i, "is_active", UNLOCKED.ToString());
								 
								ads_adhoc_update_list.SetItem(ll_i, "item_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "row_serial_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "order_number", "0");
								 
								ads_adhoc_update_list.SetItem(ll_i, "usage_datetime", ldt_usage_date.ToString());
								// AlexKh - 1.2.48.50 - 2013-05-16 - SPUCM00004179 - set branch number to 0
								 
								ads_adhoc_update_list.SetItem(ll_i, "branch_number", "0");
								//Rollback to locked
							}
							else if (ll_active == FINALY_LOCKED)
							{
								 
								ads_adhoc_update_list.SetItem(ll_i, "is_active", FINALY_LOCKED.ToString());
								
								 
								ads_adhoc_update_list.SetItem(ll_i, "order_number", ads_adhoc_update_list.GetItemValue<int>((int)ll_i, "order_number").ToString());
								
								 
								ads_adhoc_update_list.SetItem(ll_i, "item_number", ads_adhoc_update_list.GetItemValue<int>((int)ll_i, "item_number").ToString());
								
								 
								ads_adhoc_update_list.SetItem(ll_i, "row_serial_number", ads_adhoc_update_list.GetItemValue<int>((int)ll_i, "row_serial_number").ToString());
								 
								ads_adhoc_update_list.SetItem(ll_i, "usage_datetime", ldt_current_datetime.ToString());
								// AlexKh - 1.2.48.50 - 2013-05-16 - SPUCM00004179 - set branch number to current branch
								 
								ads_adhoc_update_list.SetItem(ll_i, "branch_number", masofonAlias.masofon.Instance.gs_vars.branch_number.ToString());
							}
						}
						break;
				}
			    using (UnitOfWork unitOfWork = new UnitOfWork())
			    {
                    ll_ret = ads_adhoc_update_list.Update(unitOfWork);
			    }
			    if (ll_ret != 1)
				{
					await MessageBox.Show("עדכון סטטוס מספר אישור נכשל", "שגיאה ");
					return false;
				}
				
				ads_adhoc_update_list.Reset();
			}
			return true;
		}
	}
}
