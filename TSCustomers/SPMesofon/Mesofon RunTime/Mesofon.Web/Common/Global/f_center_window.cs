using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using masofonAlias = masofon;

namespace global
{
	// Creation Time:   08/11/2015 10:20:49 (Utc)
	// Comments:        
	// 
	public static class f_center_windowClass
	{
		public static void f_center_window(WindowElement i_window)
		{
		    i_window.StartPosition = WindowStartPosition.CenterParent;
		}
	}
}
