using Common.Transposition.Extensions;
using Mesofon.Repository;

namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public class s_array_arguments
	{
        
        public string[] a_string ;
		public long[] a_long;
		public bool[] a_boolean;
		public int[] a_integer;
		public object[] a_obj;
		public bool arg_ok;
		public decimal[] a_dec;
		public object[] a_datastore;
		public System.DateTime?[] a_date;
		public System.DateTime?[] a_datetime;
		public object[] a_datawindow;
		public double[] a_double;
		public System.DateTime?[] a_time_start;
		public System.DateTime?[] a_time_finish;
		public RepositoryBase<ModelBase>[] a_transaction;
		public object[] a_sql_obj;

        public s_array_arguments()
        {
            a_string = new string[7];
            a_datetime = new System.DateTime?[3];
            a_long = new long[11];
            a_boolean = new bool[1];
            a_datastore = new object[2];
        }

    }
}
