namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_global_replaceClass
	{
		public static string f_global_replace(string source, string look_for, string replace_with) //
		{
			//A String Occurrence Search and Replace Routine
			//
			//The following code demonstrates a string occurrence search and replace routine.
			//
			//This routine works generically for any string. For example, 
			//if old_str = "red" and new_str = "green", all occurrences of 
			// "red" inside of mystring will be replaced with "green".
			//
			
			int start_pos = 1;
			int len_look_for = 0;
			len_look_for = look_for.Length;
			//find the first occurrence of look_for ...
			start_pos = source.IndexOf(look_for, start_pos - 1);
			//only enter the loop if you find whats in look_for
			while (start_pos > 0)
			{
				//replace look_for with replace_with ...
				source = source.Substring(0, start_pos) + replace_with + source.Substring(start_pos + len_look_for);
				//find the next occurrence of look_for
				start_pos = source.IndexOf(look_for, start_pos + replace_with.Length);
			}
			return source;
		}
	}
}
