using System;
using Mesofon.Common.Global;

namespace global
{
	// Creation Time:   08/11/2015 10:20:58 (Utc)
	// Comments:        
	// 
	public struct s_reports_db
	{
		public string status;
        public SQLError report_tran;
		public DateTime? update_datetime;
		public long delay_minutes;
		public long main_cpu_usage;
		public DateTime? run_datetime;
	}
}
