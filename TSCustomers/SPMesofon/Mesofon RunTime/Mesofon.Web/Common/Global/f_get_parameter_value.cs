using System;
using System.Collections.Generic;
using System.DataAccess;
using System.Globalization;
using System.Web.VisualTree.Elements;
using Mesofon.Common.Global;
using masofonAlias = masofon;
using System.Data;
using System.Extensions;
using System.Threading.Tasks;

namespace global
{
    // Creation Time:   08/11/2015 10:20:50 (Utc)
    // Comments:        
    // 
    public static class f_get_parameter_valueClass
    {
        public static async Task<string> f_get_parameter_value(string as_parameter_name, string as_parameter_type, string as_default_value, string as_description, string as_action_type) // Avia - 1.0.0.0 - 2007-11-05 - CR#999 -  f_get_parameter_value(as_parameter_name, as_parameter_type, as_default_value, as_description, as_action_type)
        {
            long? ll_row = 0;
            long ll_count = 0;
            string ls_parameter_type = null;
            string ls_parameter_value = null;
            DateTime? ldt = default(DateTime);
            as_parameter_name = as_parameter_name.ToLower(CultureInfo.InvariantCulture);
            as_parameter_type = as_parameter_type.ToLower(CultureInfo.InvariantCulture);
            switch (as_action_type)
            {
                case "gds_find":
                    
                    ll_count = masofonAlias.masofon.Instance.gds_branch_station_params.RowCount();
                    
                    ll_row = masofonAlias.masofon.Instance.gds_branch_station_params.Find("parameter_name == \"" + as_parameter_name + "\"", 0, (int)ll_count);
                    break;
                case "gds_refresh":
                    
                    ll_count = masofonAlias.masofon.Instance.gds_branch_station_params.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, masofonAlias.masofon.Instance.gs_station.station_number);
                    if (ll_count < 0)
                    {
                        await MessageBox.Show("Retrieve gds_branch_station_params FAIL", "Error! - f_get_parameter_value(...)");
                        return "";
                    }
                    else if (ll_count > 0)
                    {
                        
                        ll_row = masofonAlias.masofon.Instance.gds_branch_station_params.Find("parameter_name == \"" + as_parameter_name + "\"", 0, (int)ll_count);
                    }
                    break;
                case "bsp_sql_imm":
                    if (true)
                    {
                        f_get_parameter_valueClass.LoadData(as_parameter_name, ref ls_parameter_value, ref ls_parameter_type);
                    }

                    if (masofonAlias.masofon.Instance.sqlca.SqlCode < 0)
                    {

                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "SELECT FROM station_parameters Fail : " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                        return "";
                    }

                    else if (masofonAlias.masofon.Instance.sqlca.SqlCode == 0)
                    {
                        ll_row = -1;
                    }
                    break;
                default:
                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "UnKnow Type Action " + as_action_type);
                    return "";
            }
            if (ll_row == -1)
            {
                // Avia - 0.1.11.8 - 2007-11-04 - CR#1039 - as_description = as_action_type + ' :  ' + as_description
                as_description = as_action_type + " :  " + as_description;
                f_get_parameter_valueClass.UpdateData(as_parameter_name, as_parameter_type, as_default_value, as_description);

                if (masofonAlias.masofon.Instance.sqlca.SqlCode != 0)
                {

                    await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "INSERT station_parameters Fail : " + masofonAlias.masofon.Instance.sqlca.SqlErrText);
                    return "";
                }
                if (as_action_type != "sql_imm")
                {
                    //.. Retrieve
                    
                    ll_count = masofonAlias.masofon.Instance.gds_branch_station_params.Retrieve(masofonAlias.masofon.Instance.gs_vars.branch_number, masofonAlias.masofon.Instance.gs_station.station_number);
                    if (ll_count < 1)
                    {
                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "Retrieve gds_branch_station_params FAIL");
                        return "";
                    }
                    // Find
                    
                    ll_row = masofonAlias.masofon.Instance.gds_branch_station_params.Find("parameter_name == \"" + as_parameter_name + "\"", 0, (int)ll_count);
                    if (ll_row <= 0)
                    {
                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "Find Parameter FAIL");
                        return "";
                    }
                }
            }
            if (as_action_type != "sql_imm")
            {
                
                ls_parameter_type = masofonAlias.masofon.Instance.gds_branch_station_params.GetItemValue<string>(ll_row.Value, "parameter_type");
                
                ls_parameter_value = masofonAlias.masofon.Instance.gds_branch_station_params.GetItemValue<string>(ll_row.Value, "parameter_value");
            }

            if (ls_parameter_type ==null || as_parameter_type != ls_parameter_type.ToLower(CultureInfo.InvariantCulture))
            {
                await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "Type Of  Parameter Not Find");
                return "";
            }
            switch (as_parameter_type)
            {
                case "datetime":
                    if (ldt == Convert.ToDateTime(ls_parameter_value))
                    {
                        return "";
                    }
                    else
                    {
                        return ls_parameter_value;
                    }
                    break;
                case "date":
                    
                    var isDate = SystemFunctionsExtensions.IsDate(ls_parameter_value);
                    if (isDate != null && isDate.Value)
                    {
                        return ls_parameter_value;
                    }
                    else
                    {
                        return "";
                    }
                    break;
                case "time":
                    
                    if (SystemFunctionsExtensions.IsTime(ls_parameter_value))
                    {
                        return ls_parameter_value;
                    }
                    else
                    {
                        return "";
                    }
                    break;
                case "long":
                case "int":
                case "integer":
                case "decimal":
                    
                    if (SystemFunctionsExtensions.IsNumber(ls_parameter_value))
                    {
                        return ls_parameter_value;
                    }
                    else
                    {
                        return "";
                    }
                    break;
                case "char":
                    if (isempty_stringClass.isempty_string(ls_parameter_value))
                    {
                        return "";
                    }
                    else
                    {
                        return ls_parameter_value.Substring(0, 1);
                    }
                    break;
                case "string":
                    if (ls_parameter_value == null)
                    {
                        return "";
                    }
                    else
                    {
                        return ls_parameter_value;
                    }
                    break;
                case "boolean":
                    switch (ls_parameter_value.ToLower(CultureInfo.InvariantCulture))
                        {
                            case "1":
                            case "yes":
                            case "y":
                            case "true":
                                return "true";
                            case "0":
                            case "no":
                            case "not":
                            case "n":
                            case "false":
                                return "false";
                            default:
                                return "";
                        }
                    break;
                default:
                    if (as_parameter_type.Substring(0, 7) == "decimal")
                    {
                        
                        if (SystemFunctionsExtensions.IsNumber(ls_parameter_value))
                        {
                            return ls_parameter_value;
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else
                    {
                        await masofonAlias.masofon.Instance.guo_msg.uf_msg("Error! - f_get_parameter_value(...)", "", "stopsign!", "Parameter Type Invalid");
                        return "";
                    }
                    break;
            }
            return "";
        }
        public static void UpdateData(string as_parameter_name, string as_parameter_type, string as_default_value, string as_description)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", masofonAlias.masofon.Instance.gs_station.station_number }, { "@2", as_parameter_name }, { "@3", as_parameter_type }, { "@4", as_default_value }, { "@5", as_description } };
                    string sql = "INSERT INTO station_parameters (branch_number,  station_number,  parameter_name,  parameter_type,  parameter_value,  default_value,  description,  parameter_entity) VALUES (@0,  @1,  @2,  @3,  @4,  @4,  @5,  0)";
                    unitOfWork.Execute(sql, parameters);
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
        public static void LoadData(string as_parameter_name, ref string ls_parameter_value, ref string ls_parameter_type)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Dictionary<string, object> parameters = new Dictionary<string, object> { { "@0", masofonAlias.masofon.Instance.gs_vars.branch_number }, { "@1", masofonAlias.masofon.Instance.gs_station.station_number }, { "@2", as_parameter_name } };
                    string sql = "SELECT parameter_value,  parameter_type FROM station_parameters WHERE branch_number = @0 AND station_number = @1 AND parameter_name = @2";
                    IDataReader resultRS = unitOfWork.Retrieve(sql, parameters);
                    while (resultRS.Read())
                    {
                        ls_parameter_value = UnitOfWork.GetValue(resultRS, 0, Convert.ToString);
                        ls_parameter_type = UnitOfWork.GetValue(resultRS, 1, Convert.ToString);
                    }
                }
            }
            catch (Exception ex)
            {
                masofon.masofon.Instance.sqlca = new SQLError(ex);
            }
        }
    }
}
