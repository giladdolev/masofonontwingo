namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_is_crossdoc_distributorClass
	{
		public static bool f_is_crossdoc_distributor(long al_distributor) //**********************************************************************
		{
			//*Object:				f_is_crossdoc_distributor
			//*Function Name:	f_is_crossdoc_distributor
			//*Purpose: 			Check if distributor is crossdoc distributor
			//*Arguments: 		Long al_distributor
			//*Return:				Boolean
			//*Date				Programer		Version		Task#	 		Description
			//*-----------------------------------------------------------------------
			//*05-02-2012		AlexKh			1.2.48.15	CR#1152		Initial version
			//***********************************************************************
			
			return (al_distributor == 100);
		}
	}
}
