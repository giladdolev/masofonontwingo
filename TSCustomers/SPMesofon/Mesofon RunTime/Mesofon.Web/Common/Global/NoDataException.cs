﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcSite.Common.Global
{
    public class NoDataException : Exception
    {
        private int _sqlCode;
      //  private Exception _exception;
        public NoDataException(int sqlCode)
        {
            _sqlCode = sqlCode;
            HResult = sqlCode;
        }

        public int SqlCode
        {
            get
            {
                return _sqlCode;
            }
            set
            {
                _sqlCode = value;
                HResult = value;
            }
        }
    }
}