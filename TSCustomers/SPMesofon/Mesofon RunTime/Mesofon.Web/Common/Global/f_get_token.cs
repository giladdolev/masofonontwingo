namespace global
{
	// Creation Time:   08/11/2015 10:20:51 (Utc)
	// Comments:        
	// 
	public static class f_get_tokenClass
	{
		public static string f_get_token(ref string source, string separator) // String Function f_GET_TOKEN (ref string Source, string Separator)
		{
			// The function Get_Token receive, as arguments, the string from which
			// the token is to be stripped off, from the left, and the separator
			// character.  If the separator character does not appear in the string,
			// it returns the entire string.  Otherwise, it returns the token, not
			// including the separator character.  In either case, the source string
			// is truncated on the left, by the length of the token and separator
			// character, if any.
			int p = 0;
			string ret = "";
            if (string.IsNullOrEmpty(source))
            {
                ret = source; // return the whole source string and
                source = ""; // make the original source of zero length
            }
            else
            {
                p = source.IndexOf(separator); // Get the position of the separator
                if (p == 0) // if no separator, 
                {
                    ret = source; // return the whole source string and
                    source = ""; // make the original source of zero length
                }
                else
                {
                    // otherwise, return just the token and
                    ret = source.Substring(0, p);
                    source = source.Substring(p + 1,source.Length -( p + 1)); // strip it & the separator
                }
            }
			return ret;
		}
	}
}
