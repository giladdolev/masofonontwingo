﻿using System;
using System.Collections.Generic;
using System.Web.VisualTree;
using System.Web.VisualTree.Elements;
using System.Web.VisualTree.Engine;
using System.Web.VisualTree.MVC;
using System.Threading.Tasks;

namespace Mesofon.Common
{
    public class WindowHelper
    {
        public static async Task<T> Open<T>(string areaName, params object[] args) where T : WindowElement
        {
            IDictionary<string, object> arguments = GetArguments(args);
            string controllerName = GetFriendlyName(typeof(T));
            T w = VisualElementHelper.CreateFromView<T>(controllerName, controllerName, areaName, arguments, null);
            w.ID = w.ID + DateTime.Now.Ticks;
            await w.ShowDialog();
            return w;

        }

        public static async Task<T> Create<T>(string areaName, params object[] args) where T : CompositeElement
        {
            IDictionary<string, object> arguments = GetArguments(args);
            string controllerName = GetFriendlyName(typeof(T));
            T w = VisualElementHelper.CreateFromView<T>(controllerName, controllerName, areaName, arguments, null);
            w.Height = 625;
            w.Visible = false;
            return w;
        }

        private static IDictionary<string, object> GetArguments(object[] args)
        {
            IDictionary<string, object> arguments = null;

            if (args != null)
            {
                arguments = new Dictionary<string, object>();
                for (int i = 0; i < args.Length; i++)
                {
                    arguments.Add(args[i].ToString(), args[i+1]);
                    i++;
                }
            }
            return arguments;
        }


        public static string GetFriendlyName(Type type)
        {
            string friendlyName = type.Name;
            if (type.IsGenericType)
            {
                int iBacktick = friendlyName.IndexOf('`');
                if (iBacktick > 0)
                {
                    friendlyName = friendlyName.Remove(iBacktick);
                }
                friendlyName += "<";
                Type[] typeParameters = type.GetGenericArguments();
                for (int i = 0; i < typeParameters.Length; ++i)
                {
                    string typeParamName = typeParameters[i].Name;
                    friendlyName += (i == 0 ? typeParamName : "," + typeParamName);
                }
                friendlyName += ">";
            }

            return friendlyName;
        }

        public static int Close<T>(WindowElement closedWindow, T returnValue)
        {
            try
            {
                SetParam<T>(closedWindow, returnValue);
                closedWindow.PerformWindowClosing(new WindowClosingEventArgs(CloseReason.UserClosing, false));
                //closedWindow.Close();
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public static T GetParam<T>(ControlElement element)
        {
            
            DataExtender<T> extender = VisualElementExtender.GetExtender<DataExtender<T>>(element);
            if (extender != null)
            {
                return extender.Value;
            }
            return default(T);
        }

        public static void SetParam<T>(ControlElement element, T returnValue)
        {
            DataExtender<T> extender = VisualElementExtender.GetOrCreateExtender(element,
                arg => new DataExtender<T>());
            extender.Value = returnValue;
        }

        public static double GetRadio(ControlElement window, out int ii_WinBolderWidth, out int ii_WinBolderHeight)
        {
            int ii_ScreenWidth = 0;
            int ii_ScreenHeight = 0;
            double WRadio = 0;
            double HRadio = 0;
            double Radio = 0;

            ii_WinBolderWidth = window.PixelWidth;

            ii_WinBolderHeight = window.PixelHeight;

            ii_ScreenWidth = ScreenElement.PrimaryScreen.PixelWidth;

            ii_ScreenHeight = ScreenElement.PrimaryScreen.PixelHeight;
            // compute the radio that need be resize
            WRadio = ii_ScreenWidth / 240; // standard that the screen resolution of 800 * 600
            HRadio = ii_ScreenHeight / 320; // calculate the relative 800 * 600 screen resolution change
            Radio = Math.Min(WRadio, HRadio);
            //AlexKh - decrease zoom to 90%
            Radio = Convert.ToInt64(Radio * 0.9);
            return Radio;
        }

    }
}