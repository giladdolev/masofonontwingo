using Mesofon.Models;
namespace  mini_terminal.Extensions
{
	static internal class BundleResolutions
	{
		public static string get_employee_name(object _instance)
		{
			if (_instance is d_mini_terminal_report)
				return ((d_mini_terminal_report)_instance).employee_name;
			if (_instance is d_mini_terminal_b2b_report)
				return ((d_mini_terminal_b2b_report)_instance).employee_name;
			return default(string);
		}
		public static long get_order_number(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_head)
				return ((d_sql_mt_input_invoice_head)_instance).order_number;
			if (_instance is d_sql_mt_input_pack_head)
				return ((d_sql_mt_input_pack_head)_instance).order_number;
			return default(long);
		}
		public static long get_log_book(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_head)
				return ((d_sql_mt_input_invoice_head)_instance).log_book;
			if (_instance is d_sql_mt_input_pack_head)
				return ((d_sql_mt_input_pack_head)_instance).log_book;
			return default(long);
		}
		public static string get_barcode(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_body)
				return ((d_sql_mt_input_invoice_body)_instance).barcode;
			if (_instance is d_sql_mt_input_pack_body)
				return ((d_sql_mt_input_pack_body)_instance).barcode;
			return default(string);
		}
		public static decimal/*DataViewTextField */get_expected_material_quantity(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_body)
				return ((d_sql_mt_input_invoice_body)_instance).expected_material_quantity;
			if (_instance is d_sql_mt_input_pack_body)
				return ((d_sql_mt_input_pack_body)_instance).expected_material_quantity;
			return default(decimal/*DataViewTextField*/);
		}
		public static decimal/*DataViewTextField */get_material_quantity(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_body)
				return ((d_sql_mt_input_invoice_body)_instance).material_quantity;
			if (_instance is d_sql_mt_input_pack_body)
				return ((d_sql_mt_input_pack_body)_instance).material_quantity;
			return default(decimal/*DataViewTextField*/);
		}
		public static string get_supplier_name(object _instance)
		{
			if (_instance is d_sql_mt_input_invoice_head)
				return ((d_sql_mt_input_invoice_head)_instance).supplier_name;
			if (_instance is d_sql_mt_input_pack_head)
				return ((d_sql_mt_input_pack_head)_instance).supplier_name;
			return default(string);
		}
	}
}
