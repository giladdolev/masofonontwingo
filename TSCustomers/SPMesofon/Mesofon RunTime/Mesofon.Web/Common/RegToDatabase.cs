﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.VisualTree.Elements;
using MvcSite.ServiceSecurity;

namespace Mesofon.Common
{
    public enum RegistryValueType
    {
        RegString
    }
    public class RegToDatabase
    {
        #region Current Key

        private string _key;
        public string Value { get; private set; }
        public RegistryItem Item;

        public bool IsNew { get; private set; }

        #endregion Current Key

        #region service

        private readonly SecurityServiceClient _client = new SecurityServiceClient();

        #endregion service

   
       
        public string Key
        {
            get { return _key; }
            set
            {
                _key = value;
                Item = GetRegistryItem();
                Value = Item.RegValue;
            }
        }

        public static string GetRegistryItem(string key, string value, string def, string des)
        {
            var reg = new RegToDatabase { Key = value };

            if (reg.IsNew)
            {
                reg.SetValue(value);
                var sb = new StringBuilder();
                sb.Append("Key - " + key);
                sb.Append("Value - \"" + value + "\"" + " = " + string.Empty);
                sb.Append(reg.Item.RegDescription);
                SystemHelper.WriteToLog(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.BranchNumber, MvcSite.Common.Globals.StationNumber, "New Key", sb.ToString());

            }
            return reg.Value;
        }

        internal static void RegistrySet(string key, string value, object regString, string ls_new_packet_size)
        {
            var reg = new RegToDatabase { Key = key };

            if (reg.IsNew)
            {
                reg.SetValue(value);
                
                var sb = new StringBuilder();
                sb.Append("Key - " + key);
                sb.Append("Value - \"" + value + "\"" + " = " + string.Empty);
                sb.Append(reg.Item.RegDescription);
                SystemHelper.WriteToLog(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, MvcSite.Common.Globals.BranchNumber, MvcSite.Common.Globals.StationNumber, "New Key", sb.ToString());
            }
        }
        private RegistryItem GetRegistryItem()
        {
            //get from cache
            var items = (List<RegistryItem>)MvcSite.Common.Globals.UserRegistryItems;
            var item = items.Find(i => i.RegParam.ToLower() == _key.ToLower());

            //if not exists add Item to cache
            if (item == null)
            {
                var id = _client.GetSybaseNewId();

                IsNew = true;
                item = AddItemsToCache(id, _key, string.Empty, string.Empty, items[0]);

                //update new item to db async
                _client.InsertRegistryItem(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, item);
                
            }

            return item;
        }

        private RegistryItem AddItemsToCache(string id, string key, string value, string description,
            RegistryItem parent)
        {
            var item = new RegistryItem
            {
                Id = Guid.Parse(id),
                BranchNumber = parent.BranchNumber,
                StationNumber = parent.StationNumber,
                AppName = parent.AppName,
                DateMove = parent.DateMove,
                LastUpdateDatetime = DateTime.Now,
                RegParam = key,
                RegValue = value,
                RegDescription = description,
                RegOldValue = string.Empty,
            };
            ((List<RegistryItem>)MvcSite.Common.Globals.UserRegistryItems).Add(item);
            return item;
        }

        public void SetValue(string value)
        {
            SetItemValueInCache(value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        private void SetItemValueInCache(string value)
        {
            var item =
                ((List<RegistryItem>)MvcSite.Common.Globals.UserRegistryItems).Find(i => i.RegParam.ToLower() == _key.ToLower());
            if (item != null)
            {
                item.RegValue = value;
                //update item to db async
                _client.UpdateRegistryItem(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, item);
            }
        }

        public void DeleteKey(string key)
        {
            var item =
                ((List<RegistryItem>)MvcSite.Common.Globals.UserRegistryItems).Find(i => i.RegParam.ToLower() == key.ToLower());
            if (item != null)
            {
                ((List<RegistryItem>)MvcSite.Common.Globals.UserRegistryItems).RemoveAll(i => i.RegParam.Equals(key));
                //delete item to db async
                _client.DeleteRegistryItem(MvcSite.Common.Globals.UserID, MvcSite.Common.Globals.Password, item);
            }
        }
    }
}