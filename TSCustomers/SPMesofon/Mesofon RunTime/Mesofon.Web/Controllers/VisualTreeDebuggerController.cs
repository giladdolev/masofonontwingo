﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MvcSite.Controllers
{
    public class VisualTreeDebuggerController : Controller
    {
        /// <summary>
        /// Debugs the views.
        /// </summary>
        /// <returns></returns>
        public ActionResult DebugViews()
        {
            return DebugViewsInDirectoryInternal(Server.MapPath("/"));
        }

        /// <summary>
        /// Debugs the views in a given directory.
        /// </summary>
        /// <returns></returns>
        public ActionResult DebugViewsInDirectory(string directory)
        {
            return DebugViewsInDirectoryInternal(directory);
        }

        /// <summary>
        /// Debugs the views in directory.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private ActionResult DebugViewsInDirectoryInternal(string root)
        {
            StringBuilder content = new StringBuilder();
            content.AppendLine("<html>");
            content.AppendLine("<head><title>Views</title></head>");
            content.AppendLine("<body>");
            content.AppendLine("<ul>");
            // Loop all views
            foreach (string view in GetViewsToDebug(root))
            {
                content.AppendLine("<li>");
                content.AppendLine(string.Format("<a href=\"/VisualTreeDebugger/DebugView?viewName={0}\">{1}</a>", HttpUtility.UrlEncode(view), view));
                content.AppendLine("</li>");
            }
            content.AppendLine("</ul>");
            content.AppendLine("</body>");
            content.AppendLine("</html>");
            return this.Content(content.ToString(), "text/html");
        }

        /// <summary>
        /// Debugs the view.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns></returns>
        public ActionResult DebugView(string viewName)
        {
            // If there is a valid view name
            if (viewName != null)
            {
                // Get view parts
                string[] viewParts = viewName.Split('/');

                // If there are more then one part
                if (viewParts.Length > 1)
                {
                    // Create controller context
                    ControllerContext context = new ControllerContext(this.Request.RequestContext, this);

                    // Add area
                    context.RouteData.DataTokens["area"] = viewParts[0];

                    // Loop all engines
                    foreach (IViewEngine viewEngine in ViewEngines.Engines)
                    {
                        // If there is a valid view engine
                        if (viewEngine != null)
                        {

                            // Find view
                            ViewEngineResult result = viewEngine.FindView(context, viewParts[1], "", false);

                            // If there is a valid result
                            if (result != null)
                            {
                                // Get view
                                IView view = result.View;

                                // If there is a valid view
                                if (view != null)
                                {
                                    // Return view
                                    return this.View(view);
                                }
                            }
                        }
                    }
                }
            }

            return this.View(viewName);
        }

        /// <summary>
        /// Gets the views to debug.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> GetViewsToDebug(string root)
        {


            // If there is a valid root
            if (Directory.Exists(root))
            {
                // Loop all views
                foreach (string view in GetViewsToDebugFromDirectory(Path.Combine(root, "Views")))
                {
                    // Return content URL
                    yield return view;
                }

                string areasRoot = Path.Combine(root, "Areas");

                // If there is a valid areas root
                if (Directory.Exists(areasRoot))
                {
                    // Loop all areas
                    foreach (string area in Directory.GetDirectories(areasRoot))
                    {
                        // Get area name
                        string areaName = Path.GetFileName(area);

                        // Loop all views
                        foreach (string view in GetViewsToDebugFromDirectory(Path.Combine(area, "Views")))
                        {
                            // Return content URL
                            yield return string.Concat(areaName, "/", view);
                        }
                    }
                }
            }

            yield break;
        }

        /// <summary>
        /// Gets the views to debug.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <returns></returns>
        private IEnumerable<string> GetViewsToDebugFromDirectory(string directory)
        {
            // If there is a valid directory
            if (Directory.Exists(directory))
            {
                // Loop all files
                foreach (string file in Directory.GetFiles(directory, "*.aspx"))
                {
                    yield return Path.GetFileNameWithoutExtension(file);
                }

                // Loop all sub directories
                foreach (string subDirectory in Directory.GetDirectories(directory))
                {
                    // Loop all sub files
                    foreach (string file in GetViewsToDebugFromDirectory(subDirectory))
                    {
                        yield return file;
                    }
                }
            }
        }

    }
}