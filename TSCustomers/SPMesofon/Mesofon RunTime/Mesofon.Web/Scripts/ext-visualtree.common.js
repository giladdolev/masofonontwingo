//Ext.Loader.setPath('Ext.ux', 'Scripts/extjs/examples/ux');
//Ext.require([
//   'Ext.ux.form.MultiSelect'
//]);

/// <summary>
/// Future support for multiple apps in a single session
/// </summary>
var VT_APPID = 1;

/// <summary>
/// A flag indicating that during updating of the UI we don't want to process events.
/// </summary>
var VT_SuspendEvents = false;

/// <summary>
/// The client event queue used to acumalate client events.
/// </summary>
var VT_EventQueue = [];

/// <summary>
/// The client event hash used to prevent enqueuing duplicate events.
/// </summary>
var VT_EventQueueHash = {};

/// <summary>
/// The mask used during loading.
/// </summary>
var VT_LoadingMask = null;

/// <summary>
/// The used to delay loading mask.
/// </summary>
var VT_LoadingMaskHandle = 0;

/// <summary>
/// The used to delay raising events.
/// </summary>
var VT_raiseDelayedEventsHandle = 0;

/// <summary>
/// The application is running in design mode.
/// </summary>
var VT_DesignMode = false;

/// <summary>
/// The application is running in mobile mode.
/// </summary>
var VT_MobileMode = false;

/// <summary>
/// The process element id to enable sending events to the process.
/// </summary>
var VT_ProcessElementID = "__process";

/// <summary>
/// The callback hash.
/// </summary>
var VT_callbacks = {};

/// <summary>
/// The timer pointers hash.
/// </summary>
var VT_timers = {};

function VT_beep() {
    var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    snd.play();
}

function VT_raiseEvent(id, name, params, loadingId) {
    /// <signature>
    ///   <summary>Generates the event request.</summary>
    ///   <param name="id">The element id.</param>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    ///   <param name="loadingId">The loading target.</params>
    /// </signature>

    // If we are in suspend mode
    if (VT_SuspendEvents || VT_DesignMode) {
        return;
    }

    // Raise mouse position
    VT_raiseMousePositionEvent();

    // Enqueue the event object
    VT_queueEventObject(id, name, params, false);

    // Raise events
    VT_raiseEvents(loadingId ? loadingId : id);
};

function VT_raiseDelayedEvents(delay) {
    /// <signature>
    ///   <summary>Raises delayed events request.</summary>
    ///   <param name="delay">The delay time.</param>
    /// </signature>

    // Cancel the execution of the timed event
    clearTimeout(VT_raiseDelayedEventsHandle);

    // Set raise event timeout
    VT_raiseDelayedEventsHandle = setTimeout(function () {
        VT_raiseEvents();
    }, delay)
}

function VT_raiseEvents(loadingId) {
    /// <signature>
    ///   <summary>Raises events request.</summary>
    ///   <param name="loadingId">The loading element id.</param>
    /// </signature>

    // Cancel the execution of the timed event
    clearTimeout(VT_raiseDelayedEventsHandle);

    // Clear timeout handle
    VT_raiseDelayedEventsHandle = 0;

    // Get the current queue
    var currentQueue = VT_EventQueue;

    // Clear the global event queue
    VT_EventQueue = [];
    VT_EventQueueHash = {};

    // If there is a valid loading id
    if (loadingId != null) {
        // Show the loading mask for the given component id
        VT_ShowLoadingMask(loadingId);
    }

    // Call action
    VT_callAction(currentQueue);
}

function VT_raiseMousePositionEvent() {
    /// <signature>
    ///   <summary>Raise the mouse position event.</summary>
    /// </signature>

    // Get the event object
    var eventObject = Ext.EventObject;

    // If there is a valid event object
    if (eventObject) {

        // If we have a get XY method
        if (eventObject.getXY) {

            // Get XY mouse position
            var eventXY = eventObject.getXY();

            // If there is a valid position
            if (eventXY) {

                // Enqueue the mouse position event
                VT_queueEventObject(VT_ProcessElementID, "MousePosition", { x: eventXY[0], y: eventXY[1] }, true);
            }
        }
    }
}

function VT_raiseDelayedEvent(id, name, interval, params) {
    /// <signature>
    ///   <summary>Generates the timedout event request.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>
    if (interval > 0) {
        return setTimeout(function () { VT_raiseEvent(id, name, params); }, interval);
    }
}

function VT_raiseDoubleFilteredEvent(id, name, params) {
    /// <signature>
    ///   <summary>Generates the timedout event request but filters out duplicate calls.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // Set the owner for closure support
    var owner = this;

    // Get the current double filter handle
    var handle = owner._VT_DoubleFilteredHandle;

    // If there is allready a registered handler
    if (!isNaN(handle) && handle > 0) {

        // Cancel the execution of the timed event
        clearTimeout(handle);

        // Clear the filtered event handle
        owner._VT_DoubleFilteredHandle = 0;

    } else {

        // Set delayed event raising
        owner._VT_DoubleFilteredHandle = setTimeout(function () {

            // Raise the event
            VT_raiseEvent(id, name, params);

            // Clear the timeout handle
            owner._VT_DoubleFilteredHandle = 0;

        }, 500);
    }
}


function VT_queueEvent(id, name, params) {
    /// <signature>
    ///   <summary>Generates an event in the event queue but it is not sent to the server.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // If we are in suspend mode
    if (VT_SuspendEvents || VT_DesignMode) {
        return;
    }

    // Enqueue the event object
    VT_queueEventObject(id, name, params, true);
}

function VT_queueEventObject(id, name, params, isUnique) {
    /// <signature>
    ///   <summary>Generates an event in the event queue but it is not sent to the server.</summary>
    ///   <param name="name">The event name.</param>
    ///   <param name="params">The event params.</params>
    /// </signature>

    // Create the event object
    var eventObject = null;

    // Create the event key
    var eventKey = null;

    // If is a unique event
    if (isUnique) {

        // Create the event key
        eventKey = id + "::" + name;

        // Try to get existing event
        eventObject = VT_EventQueueHash[eventKey];
    }

    // If there is no valid event object
    if (!eventObject) {

        // Create the event object
        eventObject = { id: id, name: name };

        // Push new event to queue
        VT_EventQueue.push(eventObject);

        // If is a unique event
        if (isUnique) {

            // Try to get existing event
            VT_EventQueueHash[eventKey] = eventObject;
        }
    }

    // If there is a valid params action
    if (params) {

        // Loop all paramss
        for (var param in params) {

            // Set action object param
            eventObject[param] = params[param];
        }
    }


}

function VT_invokeCallbackById(id) {
    /// <signature>
    ///   <summary>Invokes the callback.</summary>
    ///   <param name="id">The callback id.</param>
    ///   <param name="arguments">The callback arguments.</param>
    /// </signature>

    // Get callback
    var callbackFn = VT_callbacks[id];

    // If there is a valid callback
    if (callbackFn != null) {

        // Invoke callback
        callbackFn.call(this, arguments[1], arguments[2], arguments[3], arguments[4]);

        // Remove callback
        delete VT_callbacks[id];
    }
}

function VT_getCallbackId(callbackFn, id) {
    /// <signature>
    ///   <summary>Stores the callback and returns an id.</summary>
    ///   <param name="callbackFn">The callback function.</param>
    /// </signature>

    // If there is a valid callback
    if (callbackFn != null) {
        // Set default id
        if (id == null) id = "_callback";

        // The index
        var index = 0;

        // The unique id
        var uniqueId = id;

        // While not unique
        while (VT_callbacks[uniqueId] != null) {
            // Next index
            index++;

            // Create unique id
            uniqueId = id + index;
        }

        // Set the callback function
        VT_callbacks[uniqueId] = callbackFn;

        // Return unique id
        return uniqueId;
    }
}

function VT_onAfterInitialize() {
    /// <signature>
    ///   <summary>Provides support for processing actions after initialization of the app environment.</summary>
    /// </signature>
}

function VT_onInitialize() {

    // Handle the document key down
    Ext.getBody().addListener('keydown', function (e, t) {

        var VT_FocusedComponent;
        try
        {
            VT_FocusedComponent = Ext.get(Ext.Element.getActiveElement());
        }
        catch (ex) {}
        // If there is a valid focus component
        if (VT_FocusedComponent) {

            // Get the key code
            var keyCode = e.keyCode;

            // TODO: add special key mask

            // Raise the event
            VT_FocusedComponent.fireEvent("vt_keypress", keyCode);

            // Get the focused root
            var focusedRoot = VT_GetRootCmp(VT_FocusedComponent);

            // If there is a valid focused component
            if (focusedRoot) {


                // Raise the event
                focusedRoot.fireEvent("vt_keypress", keyCode);
            }
        }
    });

    // Set the ajax timeout
    Ext.Ajax.setTimeout(60000000);
}

function VT_ShowLoadingMask(id, msg) {
    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    ///   <param name="id">The id of the component to use to show mask.</param>
    ///   <param name="id">The global id or the full path id.</param>
    /// </signature>

    // Hide existing loading mask if there is one
    VT_HideLoadingMask();

    // Schedule showing the mask
    VT_LoadingMaskHandle = setTimeout(function () {

        // Get root component from component id
        var cmp = VT_GetCmp(id, true);

        // If there is a valid component
        if (cmp) {

            // If we need to create mask
            if (!msg) {
                msg = "Loading...";
            }

            // Set the loading mask
            VT_LoadingMask = cmp.setLoading({ msg: msg, maskCls: 'vt-mask' });
        }
    }, 600);
}

function VT_HideLoadingMask() {
    /// <signature>
    ///   <summary>Hide loading mask.</summary>
    /// </signature>

    // Clear timeout
    clearTimeout(VT_LoadingMaskHandle);

    // Clear handle
    VT_LoadingMaskHandle = 0;

    // If there is a valid mask hide it
    if (VT_LoadingMask) {
        VT_LoadingMask.hide();
        VT_LoadingMask = null;
    }
}

function VT_GetRootCmp(cmp) {
    /// <signature>
    ///   <summary>Gets the root component from from current.</summary>
    ///   <param name="cmp">The component to get it's root.</param>
    /// </signature>

    if (cmp) {
        var ownerCt = cmp.ownerCt;

        if (ownerCt) {
            return VT_GetRootCmp(ownerCt);
        } else {
            return cmp;
        }
    }
}

function VT_GetCmp(id, getRoot) {
    /// <signature>
    ///   <summary>Gets a component from a full path id.</summary>
    ///   <param name="id">The global id or the full path id.</param>
    ///   <param name="id">(optional) Indicates if to get the root component.</param>
    /// </signature>

    // If there is a valid id
    if (id) {
        // Get id parts from path
        var idParts = String(id).split("/");

        // If should get root only
        if (getRoot) {

            // If there are valid id parts
            if (idParts != null && idParts.length > 0) {

                // Get first id part as an array
                idParts = [idParts[0]];
            }
        }

        // The current component
        var cmp = null;

        // Get components by query
        var cmps = Ext.ComponentQuery.query("#" + idParts.join(" #"));

        // If there are valid results
        if (cmps != null && cmps.length > 0) {

            // Get first result
            cmp = cmps[0];
        }

    }

    // If should get the root component
    if (!cmp && getRoot) {

        // Try to get the viewport
        cmps = Ext.ComponentQuery.query('viewport');

        // If there are valid results
        if (cmps != null && cmps.length > 0) {

            // Get first result
            cmp = cmps[0];
        }
    }
	
    if (!cmp && idParts.length > 1) {
        //try to get tree view item

        var itemId = idParts.pop(),
            treeViewCmp, store;
        cmps = Ext.ComponentQuery.query("#" + idParts.join(" #"));
        treeViewCmp = cmps && cmps[0];

        if (treeViewCmp && treeViewCmp instanceof Ext.tree.Panel) {
            store = treeViewCmp && treeViewCmp.getStore && treeViewCmp.getStore();
            cmp = store.findNode('itemId', itemId);
        }
    }
	
    if (cmp == null) {
        console.warn('Component with ID: %s not found', id);
    }

    // Return result
    return cmp;
}

function VT_callAction(events) {
    /// <signature>
    ///   <summary>Generates the action request.</summary>
    ///   <param name="action">The action url.</param>
    ///   <param name="events">The action events.</param>
    /// </signature>

    // If we need to create events
    if (!events) {
        events = [];
    }


    // Call the server
    Ext.Ajax.request({
        url: "/VisualTree/ProcessEvents",
        method: 'POST',
        params: {
            appId: VT_APPID
        },
        jsonData: events,
        success: function (response, opts) {

            // Hide existing loading mask if there is one
            VT_HideLoadingMask();

            VT_callback(response.responseText);
        },
        failure: function (response, opts) {

            // Hide existing loading mask if there is one
            VT_HideLoadingMask();

            // If we could not connect the server
            if (response.status == 0) {

                // Could not connect to the server
                VT_Alert('server-side failure', "Could not connect to server. Retry?", Ext.Msg.YESNO, function (btn) {

                    // If user required a retry
                    if (btn == 'yes') {

                        // Show the loading mask
                        VT_ShowLoadingMask();

                        // Try to send events again
                        VT_callAction(events);

                    }
                });
            } else {
                VT_Alert('server-side failure', response.status + "-" + response.responseText);
            }
        }
    });
};

function VT_callback(responseText) {
    /// <signature>
    ///   <summary>Processes the visual tree callback.</summary>
    ///   <param name="responseText">The response text from the server.</param>
    /// </signature>

    try {
        // Enter suspend mode
        VT_SuspendEvents = true;

        // Evaluate the response
        VT_callbackApplyItems(eval(responseText));

        // Resume actions
        VT_SuspendEvents = false;
    }
    catch (e) {

        // Resume actions
        VT_SuspendEvents = false;
        VT_Alert('server-side callback error.', e.message);
    }
}


function VT_callbackApplyItems(response) {
    /// <signature>
    ///   <summary>Processes the visual tree callbacks.</summary>
    ///   <param name="response">The response items to apply.</param>
    /// </signature>
    // If there is a valid respose
    if (response && response.length) {
        // Loop all callbacks
        for (index = 0; index < response.length; ++index) {

            // Apply callback
            VT_callbackApply(response[index]);
        }
    }
}

function VT_callbackApply(responseItem) {
    /// <signature>
    ///   <summary>Processes the visual tree callback.</summary>
    ///   <param name="responseItem">The response item evaluated from the response.</param>
    /// </signature>

    // If response item is a function
    if (Ext.isFunction(responseItem)) {
        try {
            responseItem();
        } catch (e) {
            console.error(e.message, e);
            VT_Alert('Operation error.', e.message);
        }
    }
    else {
        VT_Alert('Unknown response item.', responseItem);
    }
}

function VT_Alert(title, message, buttons, callback) {
    /// <signature>
    ///   <summary>Shows an alert box.</summary>
    ///   <param name="title">The title.</param>
    ///   <param name="message">The message.</param>
    /// </signature>

    // If there is no buttons
    if (!buttons) {

        // Set default buttons
        buttons = Ext.Msg.OK;
    }

    Ext.Msg.show({
        title: title,
        msg: message,
        buttons: buttons,
        fn: callback
    });
}

function VT_ShowWindow(win) {
    /// <signature>
    ///   <summary>Opens a window.</summary>
    ///   <param name="win">The window to open.</param>
    /// </signature>
    if (win) {
        win.center();
        win.show();

        // If the application is running in mobile mode
        if (VT_MobileMode) {
            win.maximize();
        }
        else {
            // Ensure form is not hidden beyond top
            win.setY(Math.max(0, win.getY()));
        }
    }
}

function VT_GetCssClassForBackgroundImage(imageSrc, width, height) {
    width = typeof width !== 'undefined' ? width : 20;
    height = typeof height !== 'undefined' ? height : 20;
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.dynClass' + VT_CssUniqueId + ' { background-image: url(' + imageSrc + '); background-size: contain; width: ' + width + 'px; height: ' + height + 'px;}';
    document.getElementsByTagName('head')[0].appendChild(style);

    var cls = 'dynClass' + VT_CssUniqueId;
    VT_CssUniqueId++;
    return cls;
}

function VT_ShowToolTip(cmp)
{ Ext.create('Ext.tip.ToolTip', { target: cmp.getEl(), html: cmp.tip }); }


Ext.override(Ext.layout.BorderLayout, {
    calculateChildAxis: function (childContext, axis) {
        var collapseTarget = childContext.collapseTarget,
            setSizeMethod = 'set' + axis.sizePropCap,
            sizeProp = axis.sizeProp,
            childMarginSize = childContext.getMarginInfo()[sizeProp],
            region, isBegin, flex, pos, size;
        if (collapseTarget) {

            region = collapseTarget.region;
        } else {
            region = childContext.region;
            flex = childContext.flex;
        }
        isBegin = region === axis.borderBegin;
        if (!isBegin && region !== axis.borderEnd) {

            childContext[setSizeMethod](axis.end - axis.begin - childMarginSize - (this.owner.scrollFlags[axis.posProp] ? Ext.getScrollbarSize()[axis.sizeProp] : 0));
            pos = axis.begin;
        } else {
            if (flex) {
                size = Math.ceil(axis.flexSpace * (flex / axis.totalFlex));
                size = childContext[setSizeMethod](size);
            } else if (childContext.percentage) {

                size = childContext.peek(sizeProp);
            } else {
                size = childContext.getProp(sizeProp);
            }
            size += childMarginSize;
            if (isBegin) {
                pos = axis.begin;
                axis.begin += size;
            } else {
                axis.end = pos = axis.end - size;
            }
        }
        childContext.layoutPos[axis.posProp] = pos;
    }
});

Ext.override(Ext.container.Viewport, {
    setTitle: function (title) {
        document.title = title;
    }
});

Ext.onReady(function () {
    Ext.tip.QuickTipManager.init();
});

function VT_Column_Style_renderer(value, metaData, record, row, col, store, gridView) {
    try {
        var columnStyle = gridView.grid.getColumns()[col].style
        metaData.tdStyle = metaData.tdStyle || '';
        for (var name in columnStyle) {
            if (!columnStyle.hasOwnProperty(name)) {
                continue;
            }

            name = name.replace(/[A-Z]/g, function (match) { return '-' + match.toLowerCase() });
            metaData.tdStyle += '; ' + name +': ' + columnStyle[name];
        }
    } catch (err) {
    }
}


Ext.define('Vt.ux.ListView', {
    extend: 'Ext.grid.Panel',
    xtype: 'vtlistview',
    uses: ['Ext.selection.CheckboxModel'],
    config: {
        selModel: {
            selType: 'checkboxmodel',
            mode: 'MULTI'
        }
    },
    setMultiSelect: function(isMulty){
        var selModel = this.getSelectionModel();
        selModel && selModel.setSelectionMode && selModel.setSelectionMode(isMulty ? 'MULTI' : 'SINGLE');
    },

    setCheckboxies: function(visible){
        var columns = this.getColumns(),
            selectionColumn = columns && columns[0];

        selectionColumn && selectionColumn[visible? 'show' : 'hide']();
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        me.setMultiSelect(me.multiSelect);
        me.setCheckboxies(me.showCheckboxes);
    },

    getSelectedIndexes: function () {
        var result = [],
            me = this,
            selModel = me.getSelectionModel(),
            selection = selModel && selModel.getSelection(),
            store = me.getStore();
        if (selection && store) {
            Ext.each(selection, function (item) {
                var index = store.indexOf(item);
                if (index >= 0) {
                    result.push(index);
                }
            });
        }

        return result;
    }
});

function VT_RemoveListener(elem, eventName) {
    var hasListeners = elem && elem.hasListeners,
        eventListeners = hasListeners && hasListeners[eventName],
        vtFilter = function (item) {
            return item.isVtListener;
        },
        platformListener = eventListeners && Ext.Array.filter(eventListeners.listeners, vtFilter)[0];

    platformListener && elem.removeListener(eventName, platformListener);
}

function VT_CreateMarkedListener(eventHandler) {
    eventHandler.isVtListener = true;
    return eventHandler;
}
