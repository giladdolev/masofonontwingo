Ext.define('VT.Ext.MultiLineTabs', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.multiline-tabs',
    cls: 'vt-tabpanel-multiline',
    tabBar: {
        layout: {
            type: 'hbox',
            align: 'left',
            overflowHandler: 'none'
        },
        afterLayout: function () {
            this.updateHeight();
        },
        updateHeight: function () {
            if (!this.isUpdatingHeight) {
                var targetEl = Ext.getElementById(this.id + "-targetEl");

                if (targetEl) {
                    var tabBarHeight = this.el.dom.clientHeight;
                    var targetElHeight = targetEl.clientHeight;

                    if (tabBarHeight != targetElHeight + 3) {
                        this.isUpdatingHeight = true;
                        this.setHeight(targetElHeight + 3);
                        this.isUpdatingHeight = false;
                    }
                }
            }
        }
    },
    afterLayout: function () {
        this.callParent();

        this.updateHeight();
    },

    updateHeight: function () {
        if (!this.isUpdatingHeight) {
            var tabBar = this.getTabBar();

            if (tabBar) {
                var targetEl = Ext.getElementById(tabBar.id + "-targetEl");
                var innerCt = tabBar.layout.innerCt;

                if (targetEl && innerCt) {
                    var targetElHeight = targetEl.clientHeight;
                    var innerCtHeight = innerCt.el.dom.clientHeight;

                    if (innerCtHeight != targetElHeight) {
                        this.isUpdatingHeight = true;
                        innerCt.setHeight(targetElHeight);
                        this.isUpdatingHeight = false;
                    }
                }
            }
        }
    }

});

/*
Example to run in fiddle
*/

/*
Ext.application({
    name: 'Fiddle',

    launch: function () {
        Ext.create('VT.Ext.MultiLineTabs', {
            renderTo: document.body,
            width: 250,
            height: 400,
            items: [{
                title: 'Active Tab',
                html: 'asdfa'
            }, {
                title: 'Inactive Tab',
                html: 'qwerqer'
            }, {
                title: 'Disabled Tab',
                html: '1234135'
            }],
        })
    }
});
*/