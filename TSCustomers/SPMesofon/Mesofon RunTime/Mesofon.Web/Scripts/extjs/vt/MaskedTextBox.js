/**
 * InputTextMask script used for mask/regexp operations.
 * Mask Individual Character Usage:
 * 9 - designates only numeric values
 * L - designates only uppercase letter values
 * l - designates only lowercase letter values
 * A - designates only alphanumeric values
 * X - denotes that a custom client script regular expression is specified</li>
 * All other characters are assumed to be "special" characters used to mask the input component.
 * Example 1:
 * (999)999-9999 only numeric values can be entered where the the character
 * position value is 9. Parenthesis and dash are non-editable/mask characters.
 * Example 2:
 * 99L-ll-X[^A-C]X only numeric values for the first two characters,
 * uppercase values for the third character, lowercase letters for the
 * fifth/sixth characters, and the last character X[^A-C]X together counts
 * as the eighth character regular expression that would allow all characters
 * but "A", "B", and "C". Dashes outside the regular expression are non-editable/mask characters.
 * @constructor
 * @param (String) mask The InputTextMask
 * @param (boolean) clearWhenInvalid True to clear the mask when the field blurs and the text is invalid. Optional, default is true.
 */

Ext.define('Ux.InputTextMask', {
    extend: 'Ext.plugin.Abstract',
    constructor: function (mask, clearWhenInvalid) {
        var me = this;

        me.clearWhenInvalid = Ext.isDefined(clearWhenInvalid) ? clearWhenInvalid : false;
        me.setMask(mask || '');

        return me;
    },
    alias: 'plugin.maskedinputplugin',
    init: function (field) {
        var me = this;

        me.field = field;

        if (field.rendered) {
            me.assignEl();
        } else {
            field.on('render', me.assignEl, me);
        }

        field.on('blur', me.removeValueWhenInvalid, me);
        field.on('focus', me.processMaskFocus, me);

    },

    setMask: function (mask) {
        var me = this,
			mai = 0,
			regexp = '',
			PROMPT_CHAR = (me.field && me.field.promptChar) || '_';

        me.rawMask = mask;
        me.viewMask = '';
        me.maskArray = [];


        for (var i = 0; i < mask.length; i++) {
            if (regexp) {
                if (regexp == 'X') {
                    regexp = '';
                }
                if (mask.charAt(i) == 'X') {
                    me.maskArray[mai] = regexp;
                    mai++;
                    regexp = '';
                } else {
                    regexp += mask.charAt(i);
                }
            } else if (mask.charAt(i) == 'X') {
                regexp += 'X';
                me.viewMask += PROMPT_CHAR;
            } else if (mask.charAt(i) == '9' || mask.charAt(i) == 'L' || mask.charAt(i) == 'l' || mask.charAt(i) == 'A') {
                me.viewMask += PROMPT_CHAR;
                me.maskArray[mai] = mask.charAt(i);
                mai++;
            } else {
                me.viewMask += mask.charAt(i);
                me.maskArray[mai] = RegExp.escape(mask.charAt(i));
                mai++;
            }
        }

        me.specialChars = me.viewMask.replace(new RegExp('(L|l|9|A|#|' + PROMPT_CHAR + '|X)', 'g'), '');

        me.inputTextElement && me.processMaskFocus();
    },

    assignEl: function () {
        var me = this,
            fieldInputEl = me.field.inputEl;

        me.inputTextElement = fieldInputEl.dom;
        fieldInputEl.on('keypress', me.processKeyPress, me);
        fieldInputEl.on('keydown', me.processKeyDown, me);

        if (Ext.isSafari || Ext.isIE) {
            fieldInputEl.on('paste', me.startTask, me);
            fieldInputEl.on('cut', me.startTask, me);
        }
        if (Ext.isGecko || Ext.isOpera) {
            fieldInputEl.on('mousedown', me.setPreviousValue, me);
        }
        if (Ext.isGecko) {
            fieldInputEl.on('input', me.onInput, me);
        }
        if (Ext.isOpera) {
            fieldInputEl.on('input', me.onInputOpera, me);
        }

        fieldInputEl.on('click', me.processClick, me);
    },

    onInput: function () {
        this.startTask(false);
    },

    onInputOpera: function () {
        var me = this;
        if (!me.prevValueOpera) {
            me.startTask(false);
        } else {
            me.manageBackspaceAndDeleteOpera();
        }
    },

    manageBackspaceAndDeleteOpera: function () {
        var me = this;
        me.inputTextElement.value = me.prevValueOpera.cursorPos.previousValue;
        me.manageTheText(me.prevValueOpera.keycode, me.prevValueOpera.cursorPos);
        me.prevValueOpera = null;
    },

    setPreviousValue: function (event) {
        this.oldCursorPos = this.getCursorPosition();
    },

    getValidatedKey: function (keycode, cursorPosition) {
        var pressedKey = keycode.pressedKey,
			maskKey = this.maskArray[cursorPosition.start];
        switch (maskKey) {
            case '0':
                return pressedKey.match(/[0-9]/);
            case '9':
                return pressedKey.match(/[0-9\s]/);
            case '#':
                return pressedKey.match(/[0-9\s]/);
            case 'L':
                return (pressedKey.match(/[A-Za-z]/)) ? pressedKey.toUpperCase() : null;
            case 'l':
                return (pressedKey.match(/[A-Za-z]/)) ? pressedKey.toLowerCase() : null;
            case 'A':
                return pressedKey.match(/[A-Za-z0-9]/);
            default:
                if (maskKey) {
                    return (keycode.pressedKey.match(new RegExp(maskKey)));
                }

        }

        return (null);
    },

    removeValueWhenInvalid: function () {
        var me = this,
            PROMPT_CHAR = (me.field && me.field.promptChar) || '_';

        if (me.clearWhenInvalid && me.inputTextElement.value.indexOf(PROMPT_CHAR) > -1) {
            me.inputTextElement.value = '';
        }
    },

    managePaste: function () {

        if (this.oldCursorPos == null) {
            return;
        }
        var valuePasted = this.inputTextElement.value.substring(this.oldCursorPos.start, this.inputTextElement.value.length - (this.oldCursorPos.previousValue.length - this.oldCursorPos.end));
        if (this.oldCursorPos.start < this.oldCursorPos.end) {
            this.oldCursorPos.previousValue =
			this.oldCursorPos.previousValue.substring(0, this.oldCursorPos.start) +
			this.viewMask.substring(this.oldCursorPos.start, this.oldCursorPos.end) +
			this.oldCursorPos.previousValue.substring(this.oldCursorPos.end, this.oldCursorPos.previousValue.length);
            valuePasted = valuePasted.substr(0, this.oldCursorPos.end - this.oldCursorPos.start);
        }
        this.inputTextElement.value = this.oldCursorPos.previousValue;
        var keycode = {
            unicode: '',
            isShiftPressed: false,
            isTab: false,
            isBackspace: false,
            isLeftOrRightArrow: false,
            isDelete: false,
            pressedKey: ''
        }
        var charOk = false;
        for (var i = 0; i < valuePasted.length; i++) {
            keycode.pressedKey = valuePasted.substr(i, 1);
            keycode.unicode = valuePasted.charCodeAt(i);
            this.oldCursorPos = this.skipMaskCharacters(keycode, this.oldCursorPos);
            if (this.oldCursorPos === false) {
                break;
            }
            if (this.injectValue(keycode, this.oldCursorPos)) {
                charOk = true;
                this.moveCursorToPosition(keycode, this.oldCursorPos);
                this.oldCursorPos.previousValue = this.inputTextElement.value;
                this.oldCursorPos.start = this.oldCursorPos.start + 1;
            }
        }
        if (!charOk && this.oldCursorPos !== false) {
            this.moveCursorToPosition(null, this.oldCursorPos);
        }
        this.oldCursorPos = null;
    },

    processKeyDown: function (e) {
        this.processMaskFormatting(e, 'keydown');
    },

    processKeyPress: function (e) {
        this.processMaskFormatting(e, 'keypress');
    },

    startTask: function (setOldCursor) {
        var me = this;

        if (me.task == undefined) {
            me.task = new Ext.util.DelayedTask(me.managePaste, me);
        }
        if (setOldCursor !== false) {
            me.oldCursorPos = me.getCursorPosition();
        }
        me.task.delay(0);
    },

    skipMaskCharacters: function (keycode, cursorPos) {
        if (cursorPos.start != cursorPos.end && (keycode.isDelete || keycode.isBackspace)) {
            return (cursorPos);
        }

        while (this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace) ? cursorPos.start - 1 : cursorPos.start))))) {
            if (keycode.isBackspace) {
                cursorPos.dec();
            } else {
                cursorPos.inc();
            }
            if (cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0) {
                return false;
            }
        }
        return (cursorPos);
    },

    isManagedByKeyDown: function (keycode) {
        if (keycode.isDelete || keycode.isBackspace) {
            return (true);
        }
        return (false);
    },

    processMaskFormatting: function (e, type) {
        this.oldCursorPos = null;
        var cursorPos = this.getCursorPosition(),
			keycode = this.getKeyCode(e, type),
			keyUnicode = keycode.unicode;

        if (keyUnicode == 0) {//?? sometimes on Safari
            return;
        }
        if ((keyUnicode == 67 || keyUnicode == 99) && e.ctrlKey) {//Ctrl+c, let's the browser manage it!
            return;
        }
        if ((keycode.unicode == 88 || keyUnicode == 120) && e.ctrlKey) {//Ctrl+x, manage paste
            this.startTask();
            return;
        }
        if ((keyUnicode == 86 || keyUnicode == 118) && e.ctrlKey) {//Ctrl+v, manage paste....
            this.startTask();
            return;
        }
        if ((keycode.isBackspace || keycode.isDelete) && Ext.isOpera) {
            this.prevValueOpera = { cursorPos: cursorPos, keycode: keycode };
            return;
        }
        if (type == 'keydown' && !this.isManagedByKeyDown(keycode)) {
            return true;
        }
        if (type == 'keypress' && this.isManagedByKeyDown(keycode)) {
            return true;
        }
        if (this.handleEventBubble(e, keycode, type)) {
            return true;
        }
        return (this.manageTheText(keycode, cursorPos));
    },

    manageTheText: function (keycode, cursorPos) {
        var me = this;
        if (me.inputTextElement.value.length === 0) {
            me.inputTextElement.value = me.viewMask;
        }
        cursorPos = me.skipMaskCharacters(keycode, cursorPos);
        if (cursorPos === false) {
            return false;
        }
        if (me.injectValue(keycode, cursorPos)) {
            me.moveCursorToPosition(keycode, cursorPos);
        }
        return (false);
    },

    processMaskFocus: function () {
        var me = this;
        if (me.inputTextElement.value.length == 0) {
            var cursorPos = me.getCursorPosition();
            me.inputTextElement.value = me.viewMask;
            me.moveCursorToPosition(null, cursorPos);
        }
    },

    isManagedByBrowser: function (keyEvent, keycode, type) {
        var eventObject = Ext.EventObject,
			keyList = [
				eventObject.TAB,
				eventObject.RETURN,
				eventObject.ENTER,
				eventObject.SHIFT,
				eventObject.CONTROL,
				eventObject.ESC,
				eventObject.PAGEUP,
				eventObject.PAGEDOWN,
				eventObject.END,
				eventObject.HOME,
				eventObject.LEFT,
				eventObject.UP,
				eventObject.RIGHT,
				eventObject.DOWN
			];

        if (((type == 'keypress' && keyEvent.charCode === 0) || type == 'keydown') && Ext.Array.contains(keyList, keycode.unicode)) {
            return (true);
        }
        return (false);
    },

    handleEventBubble: function (keyEvent, keycode, type) {
        try {
            if (keycode && this.isManagedByBrowser(keyEvent, keycode, type)) {
                return true;
            }
            keyEvent.stopEvent();
            return false;
        } catch (e) {
            alert(e.message);
        }
    },

    getCursorPosition: function () {
        var s, e, r,
            me = this,
            inputTextElement = me.inputTextElement;

        if (inputTextElement.createTextRange && document.selection) {
            if (document.selection) {
                r = document.selection.createRange().duplicate();
                r = me.getRange();
                r.moveEnd('character', this.inputTextElement.value.length);
                if (r.text === '') {
                    s = inputTextElement.value.length;
                } else {
                    s = inputTextElement.value.lastIndexOf(r.text);
                }
                r = me.getRange();
                r.moveStart('character', -inputTextElement.value.length);
                e = r.text.length;
            }
            
        } else {
            s = inputTextElement.selectionStart;
            e = inputTextElement.selectionEnd;
        }
        return me.CursorPosition(s, e, r, inputTextElement.value);
    },

    moveCursorToPosition: function (keycode, cursorPosition) {
        var p = (!keycode || (keycode && keycode.isBackspace)) ? cursorPosition.start : cursorPosition.start + 1;
        if (this.inputTextElement.createTextRange && document.selection) {
            cursorPosition.range.move('character', p);
            cursorPosition.range.select();
        } else {
            this.inputTextElement.selectionStart = p;
            this.inputTextElement.selectionEnd = p;
        }
    },

    injectValue: function (keycode, cursorPosition) {
        if (!keycode.isDelete && keycode.unicode == cursorPosition.previousValue.charCodeAt(cursorPosition.start)) {
            return true;
        }

        var key;
        if (!keycode.isDelete && !keycode.isBackspace) {
            key = this.getValidatedKey(keycode, cursorPosition);
        } else {
            if (cursorPosition.start == cursorPosition.end) {
                key = '_';
                if (keycode.isBackspace) {
                    cursorPosition.dec();
                }
            } else {
                key = this.viewMask.substring(cursorPosition.start, cursorPosition.end);
            }
        }
        if (key) {
            this.inputTextElement.value = cursorPosition.previousValue.substring(0, cursorPosition.start)
			+ key +
			cursorPosition.previousValue.substring(cursorPosition.start + key.length, cursorPosition.previousValue.length);
            return true;
        }
        return false;
    },

    getKeyCode: function (onKeyDownEvent, type) {
        var keycode = {},
			DELETE_CODE = Ext.EventObject.DELETE;
        keycode.unicode = onKeyDownEvent.getKey();
        keycode.isShiftPressed = onKeyDownEvent.shiftKey;

        keycode.isDelete = ((onKeyDownEvent.getKey() == DELETE_CODE && type == 'keydown') || (type == 'keypress' && onKeyDownEvent.charCode === 0 && onKeyDownEvent.keyCode == DELETE_CODE));
        keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB);
        keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE);
        keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT);
        keycode.pressedKey = String.fromCharCode(keycode.unicode);
        return (keycode);
    },

    CursorPosition: function (start, end, range, previousValue) {
        var cursorPosition = {};
        cursorPosition.start = isNaN(start) ? 0 : start;
        cursorPosition.end = isNaN(end) ? 0 : end;
        cursorPosition.range = range;
        cursorPosition.previousValue = previousValue;
        cursorPosition.inc = function () { cursorPosition.start++; cursorPosition.end++; };
        cursorPosition.dec = function () { cursorPosition.start--; cursorPosition.end--; };
        return (cursorPosition);
    },

    processClick: function (snd) {
        if (this.isMaskValue()) {
            this.moveCursorToStart();
        }
    },

    isMaskValue: function () {
        return this.inputTextElement.value === this.viewMask;
    },

    moveCursorToStart: function () {
        var me = this;

        if (me.inputTextElement.createTextRange && document.selection) {
            cursorPosition.range.move('character', 0);
            cursorPosition.range.select();
        } else {
            me.inputTextElement.selectionStart = 0;
            me.inputTextElement.selectionEnd = 0;
        }

        me.inputTextElement.focus();
    },

    destroy: function(){
        var me = this,
            field = me.field,
            fieldInputEl = field.inputEl;

        me.inputTextElement = undefined;
        me.field = undefined;

        fieldInputEl.un('keypress', me.processKeyPress, me);
        fieldInputEl.un('keydown', me.processKeyDown, me);

        if (Ext.isSafari || Ext.isIE) {
            fieldInputEl.un('paste', me.startTask, me);
            fieldInputEl.un('cut', me.startTask, me);
        }
        if (Ext.isGecko || Ext.isOpera) {
            fieldInputEl.un('mousedown', me.setPreviousValue, me);
        }
        if (Ext.isGecko) {
            fieldInputEl.un('input', me.onInput, me);
        }
        if (Ext.isOpera) {
            fieldInputEl.un('input', me.onInputOpera, me);
        }

        fieldInputEl.un('click', me.processClick, me);

        field.un('render', me.assignEl, me);
        field.un('blur', me.removeValueWhenInvalid, me);
        field.un('focus', me.processMaskFocus, me);
    }
});

Ext.applyIf(RegExp, {
    escape: function (str) {
        return new String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
    }
});


Ext.define('VT.ux.MaskedTextBox', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.maskedinput',
    plugins: [{
        ptype: 'maskedinputplugin'
    }],
    setMask: function(mask){
        var me = this,
            plugin;
        
        plugin = me.findPlugin('maskedinputplugin');
        plugin && plugin.setMask(mask);
        me.inputMask = mask;
    },

    afterRender: function () {
        var me = this;

        me.callParent();
        me.setMask(me.inputMask);
    }
    
})