Ext.Loader.setConfig({
    enabled: true,
    paths: {
        'VT.Ext': 'Scripts/extjs/vt',
        'Ext.ux': 'http://cdn.sencha.com/ext/commercial/5.1.1/examples/ux'
    }
});

Ext.require([
        'Ext.ux.form.MultiSelect',
        'VT.Ext.MultiLineTabs'
]);
