using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_supplier_per_distributor_dsRepository : RepositoryBase<d_supplier_per_distributor_ds>
    {
		private static readonly string _selectQuery = "SELECT supplier_number FROM dbo.supplier_distributors WHERE distributor_number = @0";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_supplier_per_distributor_dsService"/> class.
        /// </summary>
		public d_supplier_per_distributor_dsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_supplier_per_distributor_ds dataRow = null;
		dataRow = new d_supplier_per_distributor_ds();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
		dataRow = new d_supplier_per_distributor_ds();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
		dataRow = new d_supplier_per_distributor_ds();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_supplier_per_distributor_ds model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
		}
    }
}
