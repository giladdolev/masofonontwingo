using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_mt_material_codeRepository : RepositoryBase<d_sql_mt_material_code>
    {
		private static readonly string _selectQuery = "SELECT number, '' AS code, barcode, name FROM materials WHERE barcode_reverse LIKE @0";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_mt_material_codeService"/> class.
        /// </summary>
		public d_sql_mt_material_codeRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_mt_material_code dataRow = null;
		dataRow = new d_sql_mt_material_code();
		data[0] = dataRow;
			dataRow.number = 0;
			dataRow.code = "code_0";
			dataRow.barcode = "barcode_0";
			dataRow.name = "name_0";
		dataRow = new d_sql_mt_material_code();
		data[1] = dataRow;
			dataRow.number = 1;
			dataRow.code = "code_1";
			dataRow.barcode = "barcode_1";
			dataRow.name = "name_1";
		dataRow = new d_sql_mt_material_code();
		data[2] = dataRow;
			dataRow.number = 2;
			dataRow.code = "code_2";
			dataRow.barcode = "barcode_2";
			dataRow.name = "name_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_mt_material_code model)
        {
            if (model == null)
            {
                return;
            }

            model.number = GetValue(record, "number", Convert.ToInt64);
            model.code = GetValue(record, "code", Convert.ToString);
            model.barcode = GetValue(record, "barcode", Convert.ToString);
            model.name = GetValue(record, "name", Convert.ToString);
		}
    }
}
