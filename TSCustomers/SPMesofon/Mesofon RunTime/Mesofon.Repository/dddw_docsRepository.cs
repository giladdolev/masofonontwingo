using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class dddw_docsRepository : RepositoryBase<dddw_docs>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dddw_docsService"/> class.
        /// </summary>
		public dddw_docsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("P", "��");
            codeTable.Add("I", "��");
            LookupTables.Add("doc_type", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dddw_docs dataRow = null;
		dataRow = new dddw_docs();
		data[0] = dataRow;
			dataRow.doc_type = "doc_type_0";
			dataRow.doc_number = 0;
			dataRow.b2b_status = 0;
		dataRow = new dddw_docs();
		data[1] = dataRow;
			dataRow.doc_type = "doc_type_1";
			dataRow.doc_number = 1;
			dataRow.b2b_status = 1;
		dataRow = new dddw_docs();
		data[2] = dataRow;
			dataRow.doc_type = "doc_type_2";
			dataRow.doc_number = 2;
			dataRow.b2b_status = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dddw_docs model)
        {
            if (model == null)
            {
                return;
            }

            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.doc_number = GetValue(record, "doc_number", Convert.ToInt64);
            model.b2b_status = GetValue(record, "name_1", Convert.ToDouble);
		}
    }
}
