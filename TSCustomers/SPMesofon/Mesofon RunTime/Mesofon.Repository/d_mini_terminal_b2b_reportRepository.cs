using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_mini_terminal_b2b_reportRepository : RepositoryBase<d_mini_terminal_b2b_report>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.branch_number, global_parameters.branch_name, invoice_details.supplier_number, suppliers.name AS suppliers_name, invoice_move.order_number, 'I' AS doc_type, invoice_details.invoice_number AS doc_no, '' AS employee_name, materials.barcode, materials.name, DateAdd(mm, materials.min_valid_months, GetDate()) AS min_valid_months, invoice_details.expiration_date, '' AS ok_valid_months, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) AS order_quantity_1, IsNull(invoice_details.expected_material_quantity, 0) AS mt_quantity_1, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) - IsNull(invoice_details.expected_material_quantity, 0) AS diff_1, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) AS order_quantity_2, IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE b2b_invoice_details.branch_number = invoice_move.branch_number AND b2b_invoice_details.supplier_number = invoice_move.supplier_number AND b2b_invoice_details.order_number = invoice_move.order_number AND b2b_invoice_details.invoice_number = invoice_move.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.status = 'a'), 0) AS inv_quantity_2, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) - IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE b2b_invoice_details.branch_number = invoice_move.branch_number AND b2b_invoice_details.supplier_number = invoice_move.supplier_number AND b2b_invoice_details.order_number = invoice_move.order_number AND b2b_invoice_details.invoice_number = invoice_move.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.status = 'a'), 0) AS diff_2, IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE b2b_invoice_details.branch_number = invoice_move.branch_number AND b2b_invoice_details.supplier_number = invoice_move.supplier_number AND b2b_invoice_details.order_number = invoice_move.order_number AND b2b_invoice_details.invoice_number = invoice_move.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.status = 'a'), 0) AS inv_quantity_3, IsNull(invoice_details.expected_material_quantity, 0) AS mt_quantity_3, IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE b2b_invoice_details.branch_number = invoice_move.branch_number AND b2b_invoice_details.supplier_number = invoice_move.supplier_number AND b2b_invoice_details.order_number = invoice_move.order_number AND b2b_invoice_details.invoice_number = invoice_move.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.status = 'a'), 0) - IsNull(invoice_details.expected_material_quantity, 0) AS diff_3 FROM invoice_move, invoice_details, supplier_order_details, global_parameters, suppliers, materials WHERE (invoice_move.branch_number = @0) AND (invoice_move.supplier_number = @1) AND (invoice_move.order_number = @2) AND (Lower(invoice_move.state) <> 'd') AND (invoice_details.branch_number = invoice_move.branch_number) AND (invoice_details.supplier_number = invoice_move.supplier_number) AND (invoice_details.invoice_number = invoice_move.invoice_number) AND (invoice_details.mini_terminal = 1) AND (supplier_order_details.branch_number =* invoice_details.branch_number) AND (supplier_order_details.supplier_number =* invoice_details.supplier_number) AND (supplier_order_details.order_number =* invoice_details.packing_list_number) AND (supplier_order_details.material_number =* invoice_details.material_number) AND (global_parameters.serial_number = invoice_details.branch_number) AND (suppliers.number = invoice_details.supplier_number) AND (materials.number = invoice_details.material_number) UNION ALL SELECT packing_list_details.branch_number, global_parameters.branch_name, packing_list_details.supplier_number, suppliers.name AS suppliers_name, packing_list_move.order_number, 'P' AS doc_type, IsNull(packing_list_details.pack_list_number, 0) AS doc_no, '' AS employee_name, materials.barcode, materials.name, DateAdd(mm, materials.min_valid_months, GetDate()) AS min_valid_months, packing_list_details.expiration_date, '' AS ok_valid_months, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) AS order_quantity_1, IsNull(packing_list_details.expected_material_quantity, 0) AS mt_quantity_1, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) - IsNull(packing_list_details.expected_material_quantity, 0) AS diff_1, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) AS order_quantity_2, IsNull((SELECT SUM(units_qty) FROM b2b_packing_list_details WHERE b2b_packing_list_details.branch_number = packing_list_move.branch_number AND b2b_packing_list_details.supplier_number = packing_list_move.supplier_number AND b2b_packing_list_details.order_number = packing_list_move.order_number AND b2b_packing_list_details.pack_list_number = packing_list_move.pack_list_number AND b2b_packing_list_details.item_number = packing_list_details.material_number AND b2b_packing_list_details.status = 'a'), 0) AS inv_quantity_2, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) - IsNull((SELECT SUM(units_qty) FROM b2b_packing_list_details WHERE b2b_packing_list_details.branch_number = packing_list_move.branch_number AND b2b_packing_list_details.supplier_number = packing_list_move.supplier_number AND b2b_packing_list_details.order_number = packing_list_move.order_number AND b2b_packing_list_details.pack_list_number = packing_list_move.pack_list_number AND b2b_packing_list_details.item_number = packing_list_details.material_number AND b2b_packing_list_details.status = 'a'), 0) AS diff_2, IsNull((SELECT SUM(units_qty) FROM b2b_packing_list_details WHERE b2b_packing_list_details.branch_number = packing_list_move.branch_number AND b2b_packing_list_details.supplier_number = packing_list_move.supplier_number AND b2b_packing_list_details.order_number = packing_list_move.order_number AND b2b_packing_list_details.pack_list_number = packing_list_move.pack_list_number AND b2b_packing_list_details.item_number = packing_list_details.material_number AND b2b_packing_list_details.status = 'a'), 0) AS inv_quantity_3, IsNull(packing_list_details.expected_material_quantity, 0) AS mt_quantity_3, IsNull((SELECT SUM(units_qty) FROM b2b_packing_list_details WHERE b2b_packing_list_details.branch_number = packing_list_move.branch_number AND b2b_packing_list_details.supplier_number = packing_list_move.supplier_number AND b2b_packing_list_details.order_number = packing_list_move.order_number AND b2b_packing_list_details.pack_list_number = packing_list_move.pack_list_number AND b2b_packing_list_details.item_number = packing_list_details.material_number AND b2b_packing_list_details.status = 'a'), 0) - IsNull(packing_list_details.expected_material_quantity, 0) AS diff_3 FROM packing_list_move, packing_list_details, supplier_order_details, global_parameters, suppliers, materials WHERE (packing_list_move.branch_number = @0) AND (packing_list_move.supplier_number = @1) AND (packing_list_move.order_number = @2) AND (Lower(packing_list_move.state) <> 'd') AND (packing_list_details.branch_number = packing_list_move.branch_number) AND (packing_list_details.supplier_number = packing_list_move.supplier_number) AND (packing_list_details.pack_list_number = packing_list_move.pack_list_number) AND (packing_list_details.mini_terminal = 1) AND (supplier_order_details.branch_number =* packing_list_details.branch_number) AND (supplier_order_details.supplier_number =* packing_list_details.supplier_number) AND (supplier_order_details.order_number =* packing_list_details.order_number) AND (supplier_order_details.material_number =* packing_list_details.material_number) AND (global_parameters.serial_number = packing_list_details.branch_number) AND (suppliers.number = packing_list_details.supplier_number) AND (materials.number = packing_list_details.material_number) ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_b2b_reportService"/> class.
        /// </summary>
		public d_mini_terminal_b2b_reportRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("I", ":�������");
            codeTable.Add("P", ":�.�����");
            LookupTables.Add("state", codeTable);
        }
    

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_b2b_report dataRow = null;
		dataRow = new d_mini_terminal_b2b_report();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.branch_name = "branch_name_0";
			dataRow.supplier_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.order_number = 0;
			dataRow.doc_type = "doc_type_0";
			dataRow.doc_no = 0;
			dataRow.employee_name = "employee_name_0";
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.materials_name = "materials_name_0";
			dataRow.min_valid_months = new DateTime(1900,1,1);
			dataRow.invoice_details_expiration_date = new DateTime(1900,1,1);
			dataRow.ok_valid_months = "ok_valid_months_0";
			dataRow.order_quantity_1 = 0;
			dataRow.mt_quantity_1 = 0;
			dataRow.diff_1 = 0;
			dataRow.order_quantity_2 = 0;
			dataRow.inv_quantity_2 = 0;
			dataRow.diff_2 = 0;
			dataRow.inv_quantity_3 = 0;
			dataRow.mt_quantity_3 = 0;
			dataRow.diff_3 = 0;
			dataRow.date = "date_0";
			dataRow.compute = "compute_0";
			dataRow.row = "row_0";
			dataRow.sum_inv_quantity_3 = "sum_inv_quantity_3_0";
			dataRow.sum_mt_quantity_3 = "sum_mt_quantity_3_0";
			dataRow.sum_diff_3 = "sum_diff_3_0";
			dataRow.sum_diff_1 = "sum_diff_1_0";
			dataRow.sum_order_quantity_2 = "sum_order_quantity_2_0";
			dataRow.sum_inv_quantity_2 = "sum_inv_quantity_2_0";
			dataRow.sum_diff_2 = "sum_diff_2_0";
			dataRow.sum_order_quantity_1 = "sum_order_quantity_1_0";
			dataRow.sum_mt_quantity_1 = "sum_mt_quantity_1_0";
		dataRow = new d_mini_terminal_b2b_report();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.branch_name = "branch_name_1";
			dataRow.supplier_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.order_number = 1;
			dataRow.doc_type = "doc_type_1";
			dataRow.doc_no = 1;
			dataRow.employee_name = "employee_name_1";
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.materials_name = "materials_name_1";
			dataRow.min_valid_months = new DateTime(1901,2,2);
			dataRow.invoice_details_expiration_date = new DateTime(1901,2,2);
			dataRow.ok_valid_months = "ok_valid_months_1";
			dataRow.order_quantity_1 = 1;
			dataRow.mt_quantity_1 = 1;
			dataRow.diff_1 = 1;
			dataRow.order_quantity_2 = 1;
			dataRow.inv_quantity_2 = 1;
			dataRow.diff_2 = 1;
			dataRow.inv_quantity_3 = 1;
			dataRow.mt_quantity_3 = 1;
			dataRow.diff_3 = 1;
			dataRow.date = "date_1";
			dataRow.compute = "compute_1";
			dataRow.row = "row_1";
			dataRow.sum_inv_quantity_3 = "sum_inv_quantity_3_1";
			dataRow.sum_mt_quantity_3 = "sum_mt_quantity_3_1";
			dataRow.sum_diff_3 = "sum_diff_3_1";
			dataRow.sum_diff_1 = "sum_diff_1_1";
			dataRow.sum_order_quantity_2 = "sum_order_quantity_2_1";
			dataRow.sum_inv_quantity_2 = "sum_inv_quantity_2_1";
			dataRow.sum_diff_2 = "sum_diff_2_1";
			dataRow.sum_order_quantity_1 = "sum_order_quantity_1_1";
			dataRow.sum_mt_quantity_1 = "sum_mt_quantity_1_1";
		dataRow = new d_mini_terminal_b2b_report();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.branch_name = "branch_name_2";
			dataRow.supplier_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.order_number = 2;
			dataRow.doc_type = "doc_type_2";
			dataRow.doc_no = 2;
			dataRow.employee_name = "employee_name_2";
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.materials_name = "materials_name_2";
			dataRow.min_valid_months = new DateTime(1902,3,3);
			dataRow.invoice_details_expiration_date = new DateTime(1902,3,3);
			dataRow.ok_valid_months = "ok_valid_months_2";
			dataRow.order_quantity_1 = 2;
			dataRow.mt_quantity_1 = 2;
			dataRow.diff_1 = 2;
			dataRow.order_quantity_2 = 2;
			dataRow.inv_quantity_2 = 2;
			dataRow.diff_2 = 2;
			dataRow.inv_quantity_3 = 2;
			dataRow.mt_quantity_3 = 2;
			dataRow.diff_3 = 2;
			dataRow.date = "date_2";
			dataRow.compute = "compute_2";
			dataRow.row = "row_2";
			dataRow.sum_inv_quantity_3 = "sum_inv_quantity_3_2";
			dataRow.sum_mt_quantity_3 = "sum_mt_quantity_3_2";
			dataRow.sum_diff_3 = "sum_diff_3_2";
			dataRow.sum_diff_1 = "sum_diff_1_2";
			dataRow.sum_order_quantity_2 = "sum_order_quantity_2_2";
			dataRow.sum_inv_quantity_2 = "sum_inv_quantity_2_2";
			dataRow.sum_diff_2 = "sum_diff_2_2";
			dataRow.sum_order_quantity_1 = "sum_order_quantity_1_2";
			dataRow.sum_mt_quantity_1 = "sum_mt_quantity_1_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_b2b_report model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "suppliers_name", Convert.ToString);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_name = GetValue(record, "name", Convert.ToString);
            model.min_valid_months = GetDateTime(record, "min_valid_months");
            model.invoice_details_expiration_date = GetDateTime(record, "expiration_date");
            model.ok_valid_months = GetValue(record, "ok_valid_months", Convert.ToString);
            model.order_quantity_1 = GetValue(record, "order_quantity_1", Convert.ToDecimal);
            model.mt_quantity_1 = GetValue(record, "mt_quantity_1", Convert.ToDecimal);
            model.diff_1 = GetValue(record, "diff_1", Convert.ToDecimal);
            model.order_quantity_2 = GetValue(record, "order_quantity_2", Convert.ToDecimal);
            model.inv_quantity_2 = GetValue(record, "inv_quantity_2", Convert.ToDecimal);
            model.diff_2 = GetValue(record, "diff_2", Convert.ToDecimal);
            model.inv_quantity_3 = GetValue(record, "inv_quantity_3", Convert.ToDecimal);
            model.mt_quantity_3 = GetValue(record, "mt_quantity_3", Convert.ToDecimal);
            model.diff_3 = GetValue(record, "diff_3", Convert.ToDecimal);
            //model.date = GetValue(record, "", Convert.ToString);
            //model.compute = GetValue(record, "", Convert.ToString);
            //model.row = GetValue(record, "", Convert.ToString);
            //model.sum_inv_quantity_3 = GetValue(record, "", Convert.ToString);
            //model.sum_mt_quantity_3 = GetValue(record, "", Convert.ToString);
            //model.sum_diff_3 = GetValue(record, "", Convert.ToString);
            //model.sum_diff_1 = GetValue(record, "", Convert.ToString);
            //model.sum_order_quantity_2 = GetValue(record, "", Convert.ToString);
            //model.sum_inv_quantity_2 = GetValue(record, "", Convert.ToString);
            //model.sum_diff_2 = GetValue(record, "", Convert.ToString);
            //model.sum_order_quantity_1 = GetValue(record, "", Convert.ToString);
            //model.sum_mt_quantity_1 = GetValue(record, "", Convert.ToString);
		}
    }
}
