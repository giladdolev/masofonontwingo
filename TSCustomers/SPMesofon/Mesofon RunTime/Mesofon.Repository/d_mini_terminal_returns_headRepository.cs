using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_returns_headRepository : RepositoryBase<d_mini_terminal_returns_head>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_returns_headService"/> class.
        /// </summary>
		public d_mini_terminal_returns_headRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_returns_head dataRow = null;
		dataRow = new d_mini_terminal_returns_head();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
			dataRow.distributor_number = 0;
			dataRow.invoice_number = 0;
			dataRow.serial_number = 0;
			dataRow.debit_credit_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
		dataRow = new d_mini_terminal_returns_head();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
			dataRow.distributor_number = 1;
			dataRow.invoice_number = 1;
			dataRow.serial_number = 1;
			dataRow.debit_credit_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
		dataRow = new d_mini_terminal_returns_head();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			dataRow.distributor_number = 2;
			dataRow.invoice_number = 2;
			dataRow.serial_number = 2;
			dataRow.debit_credit_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_returns_head model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "supplier_number", Convert.ToDouble);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToDouble);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.serial_number = GetValue(record, "serial_number", Convert.ToDouble);
            model.debit_credit_number = GetValue(record, "debit_credit_number", Convert.ToDouble);
            model.supply_date = GetDateTime(record, "supply_date");
		}
    }
}
