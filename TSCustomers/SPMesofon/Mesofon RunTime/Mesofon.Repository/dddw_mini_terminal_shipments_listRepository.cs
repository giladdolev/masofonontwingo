using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dddw_mini_terminal_shipments_listRepository : RepositoryBase<dddw_mini_terminal_shipments_list>
    {
        //gilad
		private static readonly string _selectQuery = "SELECT DISTINCT convert(varchar, shipment_pallets.date_move, 103) AS date_move, shipment_pallets.shipment_number FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.marlog_number = @1) ORDER BY shipment_pallets.shipment_number DESC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dddw_mini_terminal_shipments_listService"/> class.
        /// </summary>
		public dddw_mini_terminal_shipments_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dddw_mini_terminal_shipments_list dataRow = null;
		dataRow = new dddw_mini_terminal_shipments_list();
		data[0] = dataRow;
			dataRow.date_move = "date_move_0";
			dataRow.shipment_number = "0";
		dataRow = new dddw_mini_terminal_shipments_list();
		data[1] = dataRow;
			dataRow.date_move = "date_move_1";
			dataRow.shipment_number = "1";
		dataRow = new dddw_mini_terminal_shipments_list();
		data[2] = dataRow;
			dataRow.date_move = "date_move_2";
			dataRow.shipment_number = "2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dddw_mini_terminal_shipments_list model)
        {
            if (model == null)
            {
                return;
            }
            //gilad change
            model.date_move = GetValue(record, "date_move", Convert.ToString);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToString);
		}
    }
}
