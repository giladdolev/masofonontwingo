using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_return_number_by_branch_exRepository : RepositoryBase<d_return_number_by_branch_ex>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_return_number_by_branch_exService"/> class.
        /// </summary>
		public d_return_number_by_branch_exRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_return_number_by_branch_ex dataRow = null;
		dataRow = new d_return_number_by_branch_ex();
		data[0] = dataRow;
			dataRow.return_number = 0;
		dataRow = new d_return_number_by_branch_ex();
		data[1] = dataRow;
			dataRow.return_number = 1;
		dataRow = new d_return_number_by_branch_ex();
		data[2] = dataRow;
			dataRow.return_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_return_number_by_branch_ex model)
        {
            if (model == null)
            {
                return;
            }

            model.return_number = GetValue(record, "return_number", Convert.ToDouble);
		}
    }
}
