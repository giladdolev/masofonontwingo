using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_branches_listRepository : RepositoryBase<d_branches_list>
    {
		private static readonly string _selectQuery = "SELECT global_parameters.serial_number, global_parameters.branch_name, 0, sort = 0 FROM global_parameters";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[global_parameters] ([serial_number], [branch_name]) VALUES (@p1, @p2)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[global_parameters] SET [serial_number] = @p1, [branch_name] = @p2 WHERE (([serial_number] = @p3))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[global_parameters] SET [serial_number] = @p1, [branch_name] = @p2 WHERE (([serial_number] = @p3) AND ([branch_name] = @p4))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[global_parameters] WHERE (([serial_number] = @p1))";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_branches_listService"/> class.
        /// </summary>
		public d_branches_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_branches_list dataRow = null;
		dataRow = new d_branches_list();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.branch_name = "branch_name_0";
			dataRow.flag = 0;
			dataRow.sort = 0;
		dataRow = new d_branches_list();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.branch_name = "branch_name_1";
			dataRow.flag = 1;
			dataRow.sort = 1;
		dataRow = new d_branches_list();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.branch_name = "branch_name_2";
			dataRow.flag = 2;
			dataRow.sort = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_branches_list model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.flag = GetValue(record, "compute_0003", Convert.ToInt64);
            model.sort = GetValue(record, "sort", Convert.ToInt64);
		}
    }
}
