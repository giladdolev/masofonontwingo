using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dwh_invoice_print2_sum_newRepository : RepositoryBase<dwh_invoice_print2_sum_new>
    {
		private static readonly string _selectQuery = "SELECT materials.barcode, materials.name, invoice_details.material_price, invoice_details.material_quantity, invoice_move.discount, invoice_move.mam, invoice_details.material_discount_percent, invoice_details.material_price_after_discount, invoice_move.invoice_number, invoice_move.supplier_number, invoice_move.employee_number, invoice_move.supply_date, invoice_move.state, employees.name, suppliers.name, vat_types.percentage, invoice_details.serial_number, invoice_details.color, invoice_details.bonus_quantity, invoice_details.material_bonus_connect, invoice_details.import_type, invoice_details.sell_price, invoice_move.discount_percent, stores.store_name, Convert(varchar(255), @0), invoice_move.log_book, (SELECT catalog_entities.entity_number FROM catalog_entities, items_entities_connections WHERE catalog_entities.serial_number = items_entities_connections.entity_number AND items_entities_connections.item_number = items.number AND catalog_entities.entity_type_number = 10) AS entity_number, (SELECT catalog_entities.name FROM catalog_entities, items_entities_connections WHERE catalog_entities.serial_number = items_entities_connections.entity_number AND items_entities_connections.item_number = items.number AND catalog_entities.entity_type_number = 10) AS entity_name, invoice_details.bonus_discount, (SELECT campaign_items.campaign_new_price FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaign_items.campaign_number = (SELECT MAX(campaigns.serial_number) FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaigns.campaign_status = 'a') AND (campaign_items.campaign_new_price IS NOT NULL) AND (campaigns.branches_numbers = '0' OR campaigns.branches_numbers LIKE ',' + convert(varchar(3), @1) + ',')))) AS campaign_new_price FROM invoice_details, materials, invoice_move, employees, suppliers, vat_types, stores, items, trees WHERE (invoice_details.branch_number = @1) AND (invoice_move.branch_number = @1) AND (employees.branch_number = @1) AND (stores.branch_number = @1) AND (materials.number = invoice_details.material_number) AND (invoice_details.invoice_number = invoice_move.invoice_number) AND (invoice_move.supplier_number = invoice_details.supplier_number) AND (employees.number = invoice_move.employee_number) AND (suppliers.number = invoice_move.supplier_number) AND (materials.vat_group_number = vat_types.serial_number) AND (vat_types.branch_number = @1) AND (invoice_move.store_number = stores.store_number) AND (invoice_details.material_number = materials.number) AND (trees.material_number = materials.number) AND (items.number = trees.item_number) AND ((invoice_details.supplier_number = @2) AND (invoice_details.invoice_number = @3)) AND (invoice_details.material_quantity + invoice_details.bonus_quantity <> 0) ORDER BY entity_number ASC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dwh_invoice_print2_sum_newService"/> class.
        /// </summary>
		public dwh_invoice_print2_sum_newRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dwh_invoice_print2_sum_new dataRow = null;
		dataRow = new dwh_invoice_print2_sum_new();
		data[0] = dataRow;
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.material_name = "material_name_0";
			dataRow.material_price = 0;
			dataRow.material_quantity = 0;
			dataRow.invoice_move_discount = 0;
			dataRow.mam = 0;
			dataRow.material_discount_percent = 0;
			dataRow.material_new_price = 0;
			dataRow.invoice_number = 0;
			dataRow.supplier_number = 0;
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.state = "state_0";
			dataRow.employee_name = "employee_name_0";
			dataRow.supplier_name = "supplier_name_0";
			dataRow.vat_types_percentage = 0;
			dataRow.serial_number = 0;
			dataRow.color = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_bonus_connect = 0;
			dataRow.import_type = "import_type_0";
			dataRow.sell_price = 0;
			dataRow.discount_percent = 0;
			dataRow.store_name = "store_name_0";
			dataRow.logo = "logo_0";
			dataRow.log_book = 0;
			dataRow.catalog_entities_entity_number = 0;
			dataRow.entity_name = "entity_name_0";
			dataRow.bonus_discount = 0;
			dataRow.campaign_new_price = 0;
			dataRow.compute_1 = "compute_1_0";
			dataRow.material_value = "material_value_0";
			dataRow.line_number = "line_number_0";
			dataRow.compute_gp = "compute_gp_0";
			dataRow.compute_6 = "compute_6_0";
			dataRow.compute_7 = "compute_7_0";
			dataRow.compute_8 = "compute_8_0";
			dataRow.compute_13 = "compute_13_0";
			dataRow.compute_11 = "compute_11_0";
			dataRow.compute_10 = "compute_10_0";
			dataRow.compute_gp_1 = "compute_gp_1_0";
			dataRow.invoice_total_cf = "invoice_total_cf_0";
			dataRow.total_vat = "total_vat_0";
			dataRow.compute_5 = "compute_5_0";
			dataRow.compute_vat = "compute_vat_0";
			dataRow.compute_total = "compute_total_0";
			dataRow.mam_total_cf = "mam_total_cf_0";
			dataRow.total_cf = "total_cf_0";
			dataRow.compute_4 = "compute_4_0";
			dataRow.total_quantity = "total_quantity_0";
			dataRow.compute_2 = "compute_2_0";
			dataRow.compute_14 = "compute_14_0";
			dataRow.compute_12 = "compute_12_0";
			dataRow.compute_3 = "compute_3_0";
			dataRow.sum_gp = "sum_gp_0";
			dataRow.sum_gp_1 = "sum_gp_1_0";
		dataRow = new dwh_invoice_print2_sum_new();
		data[1] = dataRow;
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.material_name = "material_name_1";
			dataRow.material_price = 1;
			dataRow.material_quantity = 1;
			dataRow.invoice_move_discount = 1;
			dataRow.mam = 1;
			dataRow.material_discount_percent = 1;
			dataRow.material_new_price = 1;
			dataRow.invoice_number = 1;
			dataRow.supplier_number = 1;
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.state = "state_1";
			dataRow.employee_name = "employee_name_1";
			dataRow.supplier_name = "supplier_name_1";
			dataRow.vat_types_percentage = 1;
			dataRow.serial_number = 1;
			dataRow.color = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_bonus_connect = 1;
			dataRow.import_type = "import_type_1";
			dataRow.sell_price = 1;
			dataRow.discount_percent = 1;
			dataRow.store_name = "store_name_1";
			dataRow.logo = "logo_1";
			dataRow.log_book = 1;
			dataRow.catalog_entities_entity_number = 1;
			dataRow.entity_name = "entity_name_1";
			dataRow.bonus_discount = 1;
			dataRow.campaign_new_price = 1;
			dataRow.compute_1 = "compute_1_1";
			dataRow.material_value = "material_value_1";
			dataRow.line_number = "line_number_1";
			dataRow.compute_gp = "compute_gp_1";
			dataRow.compute_6 = "compute_6_1";
			dataRow.compute_7 = "compute_7_1";
			dataRow.compute_8 = "compute_8_1";
			dataRow.compute_13 = "compute_13_1";
			dataRow.compute_11 = "compute_11_1";
			dataRow.compute_10 = "compute_10_1";
			dataRow.compute_gp_1 = "compute_gp_1_1";
			dataRow.invoice_total_cf = "invoice_total_cf_1";
			dataRow.total_vat = "total_vat_1";
			dataRow.compute_5 = "compute_5_1";
			dataRow.compute_vat = "compute_vat_1";
			dataRow.compute_total = "compute_total_1";
			dataRow.mam_total_cf = "mam_total_cf_1";
			dataRow.total_cf = "total_cf_1";
			dataRow.compute_4 = "compute_4_1";
			dataRow.total_quantity = "total_quantity_1";
			dataRow.compute_2 = "compute_2_1";
			dataRow.compute_14 = "compute_14_1";
			dataRow.compute_12 = "compute_12_1";
			dataRow.compute_3 = "compute_3_1";
			dataRow.sum_gp = "sum_gp_1";
			dataRow.sum_gp_1 = "sum_gp_1_1";
		dataRow = new dwh_invoice_print2_sum_new();
		data[2] = dataRow;
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.material_name = "material_name_2";
			dataRow.material_price = 2;
			dataRow.material_quantity = 2;
			dataRow.invoice_move_discount = 2;
			dataRow.mam = 2;
			dataRow.material_discount_percent = 2;
			dataRow.material_new_price = 2;
			dataRow.invoice_number = 2;
			dataRow.supplier_number = 2;
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			dataRow.employee_name = "employee_name_2";
			dataRow.supplier_name = "supplier_name_2";
			dataRow.vat_types_percentage = 2;
			dataRow.serial_number = 2;
			dataRow.color = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_bonus_connect = 2;
			dataRow.import_type = "import_type_2";
			dataRow.sell_price = 2;
			dataRow.discount_percent = 2;
			dataRow.store_name = "store_name_2";
			dataRow.logo = "logo_2";
			dataRow.log_book = 2;
			dataRow.catalog_entities_entity_number = 2;
			dataRow.entity_name = "entity_name_2";
			dataRow.bonus_discount = 2;
			dataRow.campaign_new_price = 2;
			dataRow.compute_1 = "compute_1_2";
			dataRow.material_value = "material_value_2";
			dataRow.line_number = "line_number_2";
			dataRow.compute_gp = "compute_gp_2";
			dataRow.compute_6 = "compute_6_2";
			dataRow.compute_7 = "compute_7_2";
			dataRow.compute_8 = "compute_8_2";
			dataRow.compute_13 = "compute_13_2";
			dataRow.compute_11 = "compute_11_2";
			dataRow.compute_10 = "compute_10_2";
			dataRow.compute_gp_1 = "compute_gp_1_2";
			dataRow.invoice_total_cf = "invoice_total_cf_2";
			dataRow.total_vat = "total_vat_2";
			dataRow.compute_5 = "compute_5_2";
			dataRow.compute_vat = "compute_vat_2";
			dataRow.compute_total = "compute_total_2";
			dataRow.mam_total_cf = "mam_total_cf_2";
			dataRow.total_cf = "total_cf_2";
			dataRow.compute_4 = "compute_4_2";
			dataRow.total_quantity = "total_quantity_2";
			dataRow.compute_2 = "compute_2_2";
			dataRow.compute_14 = "compute_14_2";
			dataRow.compute_12 = "compute_12_2";
			dataRow.compute_3 = "compute_3_2";
			dataRow.sum_gp = "sum_gp_2";
			dataRow.sum_gp_1 = "sum_gp_1_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dwh_invoice_print2_sum_new model)
        {
            if (model == null)
            {
                return;
            }

            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.material_name = GetValue(record, "name", Convert.ToString);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.invoice_move_discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_new_price = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.state = GetValue(record, "state", Convert.ToString);
            model.employee_name = GetValue(record, "name", Convert.ToString);
            model.supplier_name = GetValue(record, "name", Convert.ToString);
            model.vat_types_percentage = GetValue(record, "percentage", Convert.ToDecimal);
            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.store_name = GetValue(record, "store_name", Convert.ToString);
            model.logo = GetValue(record, "compute_0025", Convert.ToString);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.catalog_entities_entity_number = GetValue(record, "entity_number", Convert.ToInt64);
            model.entity_name = GetValue(record, "entity_name", Convert.ToString);
            model.bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.campaign_new_price = GetValue(record, "campaign_new_price", Convert.ToDecimal);
            model.compute_1 = GetValue(record, "", Convert.ToString);
            model.material_value = GetValue(record, "", Convert.ToString);
            model.line_number = GetValue(record, "", Convert.ToString);
            model.compute_gp = GetValue(record, "", Convert.ToString);
            model.compute_6 = GetValue(record, "", Convert.ToString);
            model.compute_7 = GetValue(record, "", Convert.ToString);
            model.compute_8 = GetValue(record, "", Convert.ToString);
            model.compute_13 = GetValue(record, "", Convert.ToString);
            model.compute_11 = GetValue(record, "", Convert.ToString);
            model.compute_10 = GetValue(record, "", Convert.ToString);
            model.compute_gp_1 = GetValue(record, "", Convert.ToString);
            model.invoice_total_cf = GetValue(record, "", Convert.ToString);
            model.total_vat = GetValue(record, "", Convert.ToString);
            model.compute_5 = GetValue(record, "", Convert.ToString);
            model.compute_vat = GetValue(record, "", Convert.ToString);
            model.compute_total = GetValue(record, "", Convert.ToString);
            model.mam_total_cf = GetValue(record, "", Convert.ToString);
            model.total_cf = GetValue(record, "", Convert.ToString);
            model.compute_4 = GetValue(record, "", Convert.ToString);
            model.total_quantity = GetValue(record, "", Convert.ToString);
            model.compute_2 = GetValue(record, "", Convert.ToString);
            model.compute_14 = GetValue(record, "", Convert.ToString);
            model.compute_12 = GetValue(record, "", Convert.ToString);
            model.compute_3 = GetValue(record, "", Convert.ToString);
            model.sum_gp = GetValue(record, "", Convert.ToString);
            model.sum_gp_1 = GetValue(record, "", Convert.ToString);
		}
    }
}
