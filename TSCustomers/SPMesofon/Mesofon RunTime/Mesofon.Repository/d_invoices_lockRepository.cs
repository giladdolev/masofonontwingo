using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_invoices_lockRepository : RepositoryBase<d_invoices_lock>
    {
		private static readonly string _selectQuery = "SELECT invoice_move.invoice_number, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_move.create_mode, invoice_move.last_update_datetime FROM invoice_move WHERE branch_number = @0 AND (supplier_number = @1 OR @1 = -1) AND invoice_type = @2 AND (invoice_number = @3 OR @3 = -1) AND (create_mode = @4 OR @4 = '-1')";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "update invoice_move Set last_update_datetime = @last_update_datetime,create_mode = @create_mode WHERE branch_number = @branch_number AND supplier_number = @supplier_number AND invoice_type = @invoice_type AND invoice_number = @invoice_number";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_invoices_lockService"/> class.
        /// </summary>
		public d_invoices_lockRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_invoices_lock dataRow = null;
		dataRow = new d_invoices_lock();
		data[0] = dataRow;
			dataRow.invoice_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.invoice_type = "invoice_type_0";
			dataRow.create_mode = "create_mode_0";
			dataRow.last_update_datetime = new DateTime(1900,1,1);
		dataRow = new d_invoices_lock();
		data[1] = dataRow;
			dataRow.invoice_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.invoice_type = "invoice_type_1";
			dataRow.create_mode = "create_mode_1";
			dataRow.last_update_datetime = new DateTime(1901,2,2);
		dataRow = new d_invoices_lock();
		data[2] = dataRow;
			dataRow.invoice_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.invoice_type = "invoice_type_2";
			dataRow.create_mode = "create_mode_2";
			dataRow.last_update_datetime = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_invoices_lock model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
		}
    }
}
