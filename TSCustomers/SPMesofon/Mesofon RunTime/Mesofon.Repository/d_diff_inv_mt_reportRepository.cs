using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_diff_inv_mt_reportRepository : RepositoryBase<d_diff_inv_mt_report>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.branch_number, invoice_details.invoice_number AS doc_number, invoice_details.material_number, IsNull(SUM(b2b_invoice_details.units_qty), 0) AS material_quantity, (IsNull(invoice_details.material_quantity, 0) + IsNull(invoice_details.bonus_quantity, 0)) AS actual_quantity, invoice_details.supplier_number, materials.name, materials.barcode FROM invoice_move, invoice_details, b2b_invoice_details, materials WHERE invoice_move.branch_number = @0 AND invoice_move.supplier_number = @1 AND invoice_move.order_number = @2 AND invoice_move.branch_number = invoice_details.branch_number AND invoice_move.supplier_number = invoice_details.supplier_number AND invoice_move.invoice_number = invoice_details.invoice_number AND (invoice_details.packing_list_number = invoice_move.order_number OR invoice_details.packing_list_number = 0) AND invoice_details.material_number = materials.number AND b2b_invoice_details.branch_number = invoice_details.branch_number AND b2b_invoice_details.supplier_number = invoice_details.supplier_number AND b2b_invoice_details.invoice_number = invoice_details.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.status = 'a' GROUP BY invoice_details.branch_number, invoice_details.supplier_number, invoice_details.invoice_number, invoice_details.material_number, materials.name, materials.barcode HAVING SUM(IsNull(b2b_invoice_details.units_qty, 0)) <> (IsNull(invoice_details.material_quantity, 0) + IsNull(invoice_details.bonus_quantity, 0)) UNION ALL SELECT b2b_invoice_details.branch_number, b2b_invoice_details.invoice_number AS doc_number, b2b_invoice_details.item_number, IsNull(SUM(b2b_invoice_details.units_qty), 0) AS material_quantity, 0 AS actual_quantity, b2b_invoice_details.supplier_number, \"לא ידוע\" AS name, b2b_invoice_details.item_barcode FROM b2b_invoice_details WHERE b2b_invoice_details.branch_number = @0 AND b2b_invoice_details.supplier_number = @1 AND b2b_invoice_details.order_number = @2 AND b2b_invoice_details.item_number = 0 GROUP BY b2b_invoice_details.branch_number, b2b_invoice_details.supplier_number, b2b_invoice_details.invoice_number, b2b_invoice_details.item_number, b2b_invoice_details.item_barcode UNION ALL SELECT packing_list_details.branch_number, packing_list_details.pack_list_number AS doc_number, packing_list_details.material_number, IsNull(SUM(b2b_packing_list_details.units_qty), 0) AS material_quantity, (IsNull(packing_list_details.material_quantity, 0) + IsNull(packing_list_details.bonus_quantity, 0)) AS actual_quantity, packing_list_details.supplier_number, materials.name, materials.barcode FROM packing_list_move, packing_list_details, b2b_packing_list_details, materials WHERE packing_list_move.branch_number = @0 AND packing_list_move.supplier_number = @1 AND packing_list_move.order_number = @2 AND packing_list_move.branch_number = packing_list_details.branch_number AND packing_list_move.supplier_number = packing_list_details.supplier_number AND packing_list_move.pack_list_number = packing_list_details.pack_list_number AND (packing_list_details.order_number = packing_list_move.order_number OR packing_list_details.order_number = 0) AND packing_list_details.material_number = materials.number AND b2b_packing_list_details.branch_number = packing_list_details.branch_number AND b2b_packing_list_details.supplier_number = packing_list_details.supplier_number AND b2b_packing_list_details.pack_list_number = packing_list_details.pack_list_number AND b2b_packing_list_details.item_number = packing_list_details.material_number AND b2b_packing_list_details.status = 'a' GROUP BY packing_list_details.branch_number, packing_list_details.supplier_number, packing_list_details.pack_list_number, packing_list_details.material_number, materials.name, materials.barcode HAVING SUM(IsNull(b2b_packing_list_details.units_qty, 0)) <> (IsNull(packing_list_details.material_quantity, 0) + IsNull(packing_list_details.bonus_quantity, 0)) UNION ALL SELECT b2b_packing_list_details.branch_number, b2b_packing_list_details.pack_list_number AS doc_number, b2b_packing_list_details.item_number, IsNull(SUM(b2b_packing_list_details.units_qty), 0) AS material_quantity, 0 AS actual_quantity, b2b_packing_list_details.supplier_number, \"לא ידוע\" AS name, b2b_packing_list_details.item_barcode FROM b2b_packing_list_details WHERE b2b_packing_list_details.branch_number = @0 AND b2b_packing_list_details.supplier_number = @1 AND b2b_packing_list_details.order_number = @2 AND b2b_packing_list_details.item_number = 0 GROUP BY b2b_packing_list_details.branch_number, b2b_packing_list_details.supplier_number, b2b_packing_list_details.pack_list_number, b2b_packing_list_details.item_number, b2b_packing_list_details.item_barcode ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_diff_inv_mt_reportService"/> class.
        /// </summary>
		public d_diff_inv_mt_reportRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_diff_inv_mt_report dataRow = null;
		dataRow = new d_diff_inv_mt_report();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.doc_number = 0;
			dataRow.material_number = 0;
			dataRow.material_quantity = 0;
			dataRow.actual_quantity = 0;
			dataRow.supplier_number = 0;
			dataRow.materials_name = "materials_name_0";
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.row = "row_0";
		dataRow = new d_diff_inv_mt_report();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.doc_number = 1;
			dataRow.material_number = 1;
			dataRow.material_quantity = 1;
			dataRow.actual_quantity = 1;
			dataRow.supplier_number = 1;
			dataRow.materials_name = "materials_name_1";
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.row = "row_1";
		dataRow = new d_diff_inv_mt_report();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.doc_number = 2;
			dataRow.material_number = 2;
			dataRow.material_quantity = 2;
			dataRow.actual_quantity = 2;
			dataRow.supplier_number = 2;
			dataRow.materials_name = "materials_name_2";
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.row = "row_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_diff_inv_mt_report model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_number = GetValue(record, "doc_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.materials_name = GetValue(record, "name", Convert.ToString);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            //model.row = GetValue(record, "", Convert.ToString);
		}
    }
}
