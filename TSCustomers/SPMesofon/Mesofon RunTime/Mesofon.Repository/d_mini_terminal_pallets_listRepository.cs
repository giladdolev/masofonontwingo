using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_pallets_listRepository : RepositoryBase<d_mini_terminal_pallets_list>
    {
        private static readonly string _selectQuery = "SELECT shipment_pallets.shipment_number, " +
       "MIN(shipment_pallets.auto_scan_status) AS auto_scan_status, " +
       "shipment_pallets.branch_number, " +
       "UPPER(shipment_pallets.pallet_number) AS pallet_number, " +
       "SUM(shipment_pallets.pack_quantity)AS pack_quantity, " +
       "MAX(shipment_pallets.arrive_datetime) AS arrive_datetime, " +
       "MAX(shipment_pallets.approve_employee) AS approve_employee, " +
       "MAX(CASE " +
          "     WHEN shipment_pallets.approve_employee > 0 THEN 1 " +
         "      ELSE 0 " +
        "   END) AS approve_ind, " +
       "right(MAX(shipment_pallets.pallet_number), 4) AS short_pallet_number, " +
      " MIN(shipment_pallets.state) AS state, " +
     "  MAX(shipment_pallets.document_type) AS document_type, " +
    "   marlog_number, " +
   "    MAX(isnull(value_indicator, 0)) AS value_indicator, " +
  "     message_state, " +
 "      0 AS new_scan " +
"FROM shipment_pallets " +
"WHERE(shipment_pallets.branch_number = @0) " +
  "AND(shipment_pallets.shipment_number = @1) " +
 " AND(shipment_pallets.marlog_number = @2) " +
"GROUP BY shipment_pallets.branch_number, " +
         "shipment_pallets.shipment_number, " +
         "shipment_pallets.pallet_number, " +
         "shipment_pallets.marlog_number, " +
         "message_state";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "UPDATE [shipment_pallets] SET [auto_scan_status] = @auto_scan_status , [arrive_datetime] = @arrive_datetime , [approve_employee] = @approve_employee , [state] = @state   , [value_indicator] = @value_indicator , [message_state] = @message_state WHERE (branch_number = @branch_number) AND (shipment_number = @shipment_number) AND (marlog_number = @marlog_number) AND (pallet_number = @pallet_number) ";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_pallets_listService"/> class.
        /// </summary>
		public d_mini_terminal_pallets_listRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_mini_terminal_pallets_list dataRow = null;
            dataRow = new d_mini_terminal_pallets_list();
            data[0] = dataRow;
            dataRow.shipment_number = 0;
            dataRow.auto_scan_status = 0;
            dataRow.branch_number = 0;
            dataRow.pallet_number = "pallet_number_0";
            dataRow.pack_quantity = 0;
            dataRow.arrive_datetime = new DateTime(1900, 1, 1);
            dataRow.approve_employee = 0;
            dataRow.approve_ind = 0;
            dataRow.short_pallet_number = "short_pallet_number_0";
            dataRow.state = "state_0";
            dataRow.document_type = 0;
            dataRow.marlog_number = 0;
            dataRow.value_indicator = 0;
            dataRow.message_state = "message_state_0";
            dataRow.new_scan = 0;
            dataRow.compute_1 = "compute_1_0";
            dataRow = new d_mini_terminal_pallets_list();
            data[1] = dataRow;
            dataRow.shipment_number = 1;
            dataRow.auto_scan_status = 1;
            dataRow.branch_number = 1;
            dataRow.pallet_number = "pallet_number_1";
            dataRow.pack_quantity = 1;
            dataRow.arrive_datetime = new DateTime(1901, 2, 2);
            dataRow.approve_employee = 1;
            dataRow.approve_ind = 1;
            dataRow.short_pallet_number = "short_pallet_number_1";
            dataRow.state = "state_1";
            dataRow.document_type = 1;
            dataRow.marlog_number = 1;
            dataRow.value_indicator = 1;
            dataRow.message_state = "message_state_1";
            dataRow.new_scan = 1;
            dataRow.compute_1 = "compute_1_1";
            dataRow = new d_mini_terminal_pallets_list();
            data[2] = dataRow;
            dataRow.shipment_number = 2;
            dataRow.auto_scan_status = 2;
            dataRow.branch_number = 2;
            dataRow.pallet_number = "pallet_number_2";
            dataRow.pack_quantity = 2;
            dataRow.arrive_datetime = new DateTime(1902, 3, 3);
            dataRow.approve_employee = 2;
            dataRow.approve_ind = 2;
            dataRow.short_pallet_number = "short_pallet_number_2";
            dataRow.state = "state_2";
            dataRow.document_type = 2;
            dataRow.marlog_number = 2;
            dataRow.value_indicator = 2;
            dataRow.message_state = "message_state_2";
            dataRow.new_scan = 2;
            dataRow.compute_1 = "compute_1_2";
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_pallets_list model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.auto_scan_status = GetValue(record, "auto_scan_status", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.pallet_number = GetValue(record, "pallet_number", Convert.ToString);
            model.pack_quantity = GetValue(record, "pack_quantity", Convert.ToInt64);
            model.arrive_datetime = GetDateTime(record, "arrive_datetime");
            model.approve_employee = GetValue(record, "approve_employee", Convert.ToInt64);
            model.approve_ind = GetValue(record, "approve_ind", Convert.ToInt64);
            model.short_pallet_number = GetValue(record, "short_pallet_number", Convert.ToString);
            model.state = GetValue(record, "state", Convert.ToString);
            model.document_type = GetValue(record, "document_type", Convert.ToInt64);
            model.marlog_number = GetValue(record, "marlog_number", Convert.ToInt64);
            model.value_indicator = GetValue(record, "value_indicator", Convert.ToInt64);
            model.message_state = GetValue(record, "message_state", Convert.ToString);
            model.new_scan = GetValue(record, "new_scan", Convert.ToInt64);
            // model.compute_1 = GetValue(record, "", Convert.ToString);
        }
    }
}
