using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class ds_marlog_distributor_translateRepository : RepositoryBase<ds_marlog_distributor_translate>
    {
        private static readonly string _selectQuery = "SELECT param_values.param_value AS branch_number, param_values.param_name AS distributor_number, param_values.sort_order, param_values.default_value, param_values.description FROM param_values WHERE param_code = @0";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="ds_marlog_distributor_translateService"/> class.
        /// </summary>
        public ds_marlog_distributor_translateRepository()
            : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("999", "����\"� ����");
            codeTable.Add("997", "����\"� ��� �����");
            LookupTables.Add("branch_number", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
        protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            ds_marlog_distributor_translate dataRow = null;
            dataRow = new ds_marlog_distributor_translate();
            data[0] = dataRow;
            dataRow.branch_number = "branch_number_0";
            dataRow.distributor_number = "distributor_number_0";
            dataRow.sort_order = 0;
            dataRow.default_value = 0;
            dataRow.description = "description_0";
            dataRow = new ds_marlog_distributor_translate();
            data[1] = dataRow;
            dataRow.branch_number = "branch_number_1";
            dataRow.distributor_number = "distributor_number_1";
            dataRow.sort_order = 1;
            dataRow.default_value = 1;
            dataRow.description = "description_1";
            dataRow = new ds_marlog_distributor_translate();
            data[2] = dataRow;
            dataRow.branch_number = "branch_number_2";
            dataRow.distributor_number = "distributor_number_2";
            dataRow.sort_order = 2;
            dataRow.default_value = 2;
            dataRow.description = "description_2";
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, ds_marlog_distributor_translate model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToString);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToString);
            model.sort_order = GetValue(record, "sort_order", Convert.ToDouble);
            model.default_value = GetValue(record, "default_value", Convert.ToDouble);
            model.description = GetValue(record, "description", Convert.ToString);
        }
    }
}
