using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_diff_pallet_inv_mt_reportRepository : RepositoryBase<d_diff_pallet_inv_mt_report>
    {
		private static readonly string _selectQuery = "SELECT b2b_invoice_details.branch_number, convert(decimal(12, 0), b2b_invoice_details.invoice_number) AS doc_number, b2b_invoice_details.item_number AS material_number, IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE branch_number = @0 AND supplier_number = @1 AND invoice_number = @2 AND item_number = materials.number AND status = 'a' AND pallet_number = @3 GROUP BY branch_number, supplier_number, invoice_number, item_number), 0) AS material_quantity, IsNull(b2b_invoice_details.scanned_quantity, 0) AS actual_quantity, b2b_invoice_details.supplier_number, materials.name, materials.barcode FROM materials, b2b_invoice_details WHERE b2b_invoice_details.item_number = materials.number AND b2b_invoice_details.branch_number = @0 AND b2b_invoice_details.supplier_number = @1 AND b2b_invoice_details.invoice_number = @2 AND b2b_invoice_details.pallet_number = @3 AND b2b_invoice_details.status = 'a' GROUP BY b2b_invoice_details.branch_number, b2b_invoice_details.supplier_number, b2b_invoice_details.invoice_number, b2b_invoice_details.item_number, materials.name, materials.barcode, b2b_invoice_details.scanned_quantity HAVING (IsNull(b2b_invoice_details.scanned_quantity, 0) = 0)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_diff_pallet_inv_mt_reportService"/> class.
        /// </summary>
		public d_diff_pallet_inv_mt_reportRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_diff_pallet_inv_mt_report dataRow = null;
		dataRow = new d_diff_pallet_inv_mt_report();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.doc_number = 0;
			dataRow.b2b_invoice_details_material_number = 0;
			dataRow.material_quantity = 0;
			dataRow.actual_quantity = 0;
			dataRow.supplier_number = 0;
			dataRow.materials_name = "materials_name_0";
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.row = "row_0";
		dataRow = new d_diff_pallet_inv_mt_report();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.doc_number = 1;
			dataRow.b2b_invoice_details_material_number = 1;
			dataRow.material_quantity = 1;
			dataRow.actual_quantity = 1;
			dataRow.supplier_number = 1;
			dataRow.materials_name = "materials_name_1";
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.row = "row_1";
		dataRow = new d_diff_pallet_inv_mt_report();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.doc_number = 2;
			dataRow.b2b_invoice_details_material_number = 2;
			dataRow.material_quantity = 2;
			dataRow.actual_quantity = 2;
			dataRow.supplier_number = 2;
			dataRow.materials_name = "materials_name_2";
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.row = "row_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_diff_pallet_inv_mt_report model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_number = GetValue(record, "doc_number", Convert.ToInt64);
            model.b2b_invoice_details_material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.materials_name = GetValue(record, "name", Convert.ToString);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
           // model.row = GetValue(record, "", Convert.ToString);
		}
    }
}
