using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_summary_inv_palletRepository : RepositoryBase<d_summary_inv_pallet>
    {
		private static readonly string _selectQuery = "SELECT b2b_invoice_details.branch_number, b2b_invoice_move.invoice_number AS doc_no, b2b_invoice_move.supplier_number, SUM(b2b_invoice_details.scanned_quantity) AS TOTAL FROM b2b_invoice_details, b2b_invoice_move WHERE b2b_invoice_move.branch_number = b2b_invoice_details.branch_number AND b2b_invoice_move.supplier_number = b2b_invoice_details.supplier_number AND b2b_invoice_details.branch_number = @0 AND b2b_invoice_details.supplier_number = @1 AND b2b_invoice_details.invoice_number = @2 AND Lower(b2b_invoice_move.status) = 'a' AND b2b_invoice_move.invoice_number = b2b_invoice_details.invoice_number AND b2b_invoice_details.pallet_number = @3 GROUP BY b2b_invoice_details.branch_number, b2b_invoice_move.supplier_number, b2b_invoice_move.invoice_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_summary_inv_palletService"/> class.
        /// </summary>
		public d_summary_inv_palletRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_summary_inv_pallet dataRow = null;
		dataRow = new d_summary_inv_pallet();
		data[0] = dataRow;
			dataRow.invoice_details_branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.invoice_move_supplier_number = 0;
			dataRow.total = 0;
		dataRow = new d_summary_inv_pallet();
		data[1] = dataRow;
			dataRow.invoice_details_branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.invoice_move_supplier_number = 1;
			dataRow.total = 1;
		dataRow = new d_summary_inv_pallet();
		data[2] = dataRow;
			dataRow.invoice_details_branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.invoice_move_supplier_number = 2;
			dataRow.total = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_summary_inv_pallet model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.invoice_move_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.total = GetValue(record, "TOTAL", Convert.ToDecimal);
		}
    }
}
