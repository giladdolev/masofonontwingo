using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_details_pack_list_saveRepository : RepositoryBase<d_mini_terminal_details_pack_list_save>
    {
		private static readonly string _selectQuery = "SELECT packing_list_details.serial_number, packing_list_details.branch_number, packing_list_details.pack_list_number, packing_list_details.material_number, packing_list_details.material_quantity, packing_list_details.material_price, packing_list_details.details, packing_list_details.supplier_number, packing_list_details.material_discount_percent, packing_list_details.material_price_after_discount, packing_list_details.quantity_within_invoice, packing_list_details.order_number, packing_list_details.order_serial, packing_list_details.stock_number, packing_list_details.sell_price, packing_list_details.bonus_quantity, packing_list_details.material_bonus_connect, packing_list_details.max_quantity_allowed, packing_list_details.last_order_change, '' AS import_type, packing_list_details.material_discount_amount, packing_list_details.bonus_row, packing_list_details.color, packing_list_details.pay_shape, packing_list_details.indicator, packing_list_details.bonus_discount, packing_list_details.supplier_discount_percent, packing_list_details.current_catalog_sell_price, packing_list_details.expiration_date, packing_list_details.expected_material_quantity, packing_list_details.mini_terminal, materials.barcode AS barcode, materials.name AS material_name, materials.min_valid_months, supplier_order_details.material_quantity AS order_quantity, packing_list_move.b2b_status AS b2b_status, 0 AS row_no, 1 AS doc_no_en FROM packing_list_details, materials, supplier_order_details, packing_list_move WHERE packing_list_details.order_number = @0 AND packing_list_details.supplier_number = @1 AND packing_list_details.branch_number = @2 AND packing_list_details.material_number = materials.number AND supplier_order_details.order_number = @0 AND supplier_order_details.supplier_number = @1 AND supplier_order_details.branch_number = @2 AND packing_list_move.branch_number = @2 AND packing_list_move.supplier_number = @1 AND packing_list_move.pack_list_number = packing_list_details.pack_list_number AND supplier_order_details.material_number =* packing_list_details.material_number AND packing_list_details.material_number = @3";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_details_pack_list_saveService"/> class.
        /// </summary>
		public d_mini_terminal_details_pack_list_saveRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_details_pack_list_save dataRow = null;
		dataRow = new d_mini_terminal_details_pack_list_save();
		data[0] = dataRow;
			dataRow.packing_list_details_serial_number = 0;
			dataRow.packing_list_details_branch_number = 0;
			dataRow.pack_list_number = 0;
			dataRow.packing_list_details_material_number = 0;
			dataRow.material_quantity = 0;
			dataRow.packing_list_details_material_price = 0;
			dataRow.packing_list_details_details = "packing_list_details_details_0";
			dataRow.packing_list_details_supplier_number = 0;
			dataRow.packing_list_details_material_discount_percent = 0;
			dataRow.packing_list_details_material_price_after_discount = 0;
			dataRow.packing_list_details_quantity_within_invoice = 0;
			dataRow.packing_list_details_order_number = 0;
			dataRow.packing_list_details_order_serial = 0;
			dataRow.packing_list_details_stock_number = 0;
			dataRow.packing_list_details_sell_price = 0;
			dataRow.packing_list_details_bonus_quantity = 0;
			dataRow.packing_list_details_material_bonus_connect = 0;
			dataRow.packing_list_details_max_quantity_allowed = 0;
			dataRow.packing_list_details_last_order_change = 0;
			dataRow.import_type = "import_type_0";
			dataRow.packing_list_details_material_discount_amount = 0;
			dataRow.packing_list_details_bonus_row = "packing_list_details_bonus_row_0";
			dataRow.packing_list_details_color = 0;
			dataRow.packing_list_details_pay_shape = 0;
			dataRow.packing_list_details_indicator = 0;
			dataRow.packing_list_details_bonus_discount = 0;
			dataRow.packing_list_details_supplier_discount_percent = 0;
			dataRow.packing_list_details_current_catalog_sell_price = 0;
			dataRow.packing_list_details_expiration_date = new DateTime(1900,1,1);
			dataRow.expected_material_quantity = 0;
			dataRow.packing_list_details_mini_terminal = 0;
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.materials_material_name = "materials_material_name_0";
			dataRow.materials_min_valid_months = 0;
			dataRow.order_quantity = 0;
			dataRow.b2b_status = 0;
			dataRow.row_no = 0;
			dataRow.doc_no_en = 0;
			dataRow.compute_1 = "compute_1_0";
		dataRow = new d_mini_terminal_details_pack_list_save();
		data[1] = dataRow;
			dataRow.packing_list_details_serial_number = 1;
			dataRow.packing_list_details_branch_number = 1;
			dataRow.pack_list_number = 1;
			dataRow.packing_list_details_material_number = 1;
			dataRow.material_quantity = 1;
			dataRow.packing_list_details_material_price = 1;
			dataRow.packing_list_details_details = "packing_list_details_details_1";
			dataRow.packing_list_details_supplier_number = 1;
			dataRow.packing_list_details_material_discount_percent = 1;
			dataRow.packing_list_details_material_price_after_discount = 1;
			dataRow.packing_list_details_quantity_within_invoice = 1;
			dataRow.packing_list_details_order_number = 1;
			dataRow.packing_list_details_order_serial = 1;
			dataRow.packing_list_details_stock_number = 1;
			dataRow.packing_list_details_sell_price = 1;
			dataRow.packing_list_details_bonus_quantity = 1;
			dataRow.packing_list_details_material_bonus_connect = 1;
			dataRow.packing_list_details_max_quantity_allowed = 1;
			dataRow.packing_list_details_last_order_change = 1;
			dataRow.import_type = "import_type_1";
			dataRow.packing_list_details_material_discount_amount = 1;
			dataRow.packing_list_details_bonus_row = "packing_list_details_bonus_row_1";
			dataRow.packing_list_details_color = 1;
			dataRow.packing_list_details_pay_shape = 1;
			dataRow.packing_list_details_indicator = 1;
			dataRow.packing_list_details_bonus_discount = 1;
			dataRow.packing_list_details_supplier_discount_percent = 1;
			dataRow.packing_list_details_current_catalog_sell_price = 1;
			dataRow.packing_list_details_expiration_date = new DateTime(1901,2,2);
			dataRow.expected_material_quantity = 1;
			dataRow.packing_list_details_mini_terminal = 1;
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.materials_material_name = "materials_material_name_1";
			dataRow.materials_min_valid_months = 1;
			dataRow.order_quantity = 1;
			dataRow.b2b_status = 1;
			dataRow.row_no = 1;
			dataRow.doc_no_en = 1;
			dataRow.compute_1 = "compute_1_1";
		dataRow = new d_mini_terminal_details_pack_list_save();
		data[2] = dataRow;
			dataRow.packing_list_details_serial_number = 2;
			dataRow.packing_list_details_branch_number = 2;
			dataRow.pack_list_number = 2;
			dataRow.packing_list_details_material_number = 2;
			dataRow.material_quantity = 2;
			dataRow.packing_list_details_material_price = 2;
			dataRow.packing_list_details_details = "packing_list_details_details_2";
			dataRow.packing_list_details_supplier_number = 2;
			dataRow.packing_list_details_material_discount_percent = 2;
			dataRow.packing_list_details_material_price_after_discount = 2;
			dataRow.packing_list_details_quantity_within_invoice = 2;
			dataRow.packing_list_details_order_number = 2;
			dataRow.packing_list_details_order_serial = 2;
			dataRow.packing_list_details_stock_number = 2;
			dataRow.packing_list_details_sell_price = 2;
			dataRow.packing_list_details_bonus_quantity = 2;
			dataRow.packing_list_details_material_bonus_connect = 2;
			dataRow.packing_list_details_max_quantity_allowed = 2;
			dataRow.packing_list_details_last_order_change = 2;
			dataRow.import_type = "import_type_2";
			dataRow.packing_list_details_material_discount_amount = 2;
			dataRow.packing_list_details_bonus_row = "packing_list_details_bonus_row_2";
			dataRow.packing_list_details_color = 2;
			dataRow.packing_list_details_pay_shape = 2;
			dataRow.packing_list_details_indicator = 2;
			dataRow.packing_list_details_bonus_discount = 2;
			dataRow.packing_list_details_supplier_discount_percent = 2;
			dataRow.packing_list_details_current_catalog_sell_price = 2;
			dataRow.packing_list_details_expiration_date = new DateTime(1902,3,3);
			dataRow.expected_material_quantity = 2;
			dataRow.packing_list_details_mini_terminal = 2;
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.materials_material_name = "materials_material_name_2";
			dataRow.materials_min_valid_months = 2;
			dataRow.order_quantity = 2;
			dataRow.b2b_status = 2;
			dataRow.row_no = 2;
			dataRow.doc_no_en = 2;
			dataRow.compute_1 = "compute_1_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_details_pack_list_save model)
        {
            if (model == null)
            {
                return;
            }

            model.packing_list_details_serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.packing_list_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.pack_list_number = GetValue(record, "pack_list_number", Convert.ToInt64);
            model.packing_list_details_material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.packing_list_details_material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.packing_list_details_details = GetValue(record, "details", Convert.ToString);
            model.packing_list_details_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.packing_list_details_material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.packing_list_details_material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.packing_list_details_quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.packing_list_details_order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.packing_list_details_order_serial = GetValue(record, "order_serial", Convert.ToInt64);
            model.packing_list_details_stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.packing_list_details_sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.packing_list_details_bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.packing_list_details_material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.packing_list_details_max_quantity_allowed = GetValue(record, "max_quantity_allowed", Convert.ToDecimal);
            model.packing_list_details_last_order_change = GetValue(record, "last_order_change", Convert.ToDecimal);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.packing_list_details_material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.packing_list_details_bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.packing_list_details_color = GetValue(record, "color", Convert.ToInt64);
            model.packing_list_details_pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.packing_list_details_indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.packing_list_details_bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.packing_list_details_supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.packing_list_details_current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.packing_list_details_expiration_date = GetDateTime(record, "expiration_date");
            model.expected_material_quantity = GetValue(record, "expected_material_quantity", Convert.ToDecimal);
            model.packing_list_details_mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_material_name = GetValue(record, "material_name", Convert.ToString);
            model.materials_min_valid_months = GetValue(record, "min_valid_months", Convert.ToInt64);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.row_no = GetValue(record, "row_no", Convert.ToInt64);
            model.doc_no_en = GetValue(record, "doc_no_en", Convert.ToInt64);
            model.compute_1 = GetValue(record, "", Convert.ToString);
		}
    }
}
