using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_addhockRepository : RepositoryBase<d_addhock>
    {
		private static readonly string _selectQuery = "SELECT supplier_order_trades.serial_number, supplier_order_trades.branch_number, supplier_order_trades.supplier_number, supplier_order_trades.adhoc_key, supplier_order_trades.date_move, supplier_order_trades.is_active, supplier_order_trades.usage_datetime, supplier_order_trades.order_number, supplier_order_trades.item_number, supplier_order_trades.trade_number, supplier_order_trades.row_serial_number, supplier_order_trades.adhoc_type FROM supplier_order_trades WHERE (supplier_order_trades.adhoc_key = @0) AND (supplier_order_trades.branch_number IN (0,@1)) AND (supplier_order_trades.supplier_number IN (0,@2)) ORDER BY supplier_order_trades.branch_number DESC, supplier_order_trades.supplier_number DESC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "UPDATE supplier_order_trades SET branch_number = @branch_number, is_active = @is_active, usage_datetime = @usage_datetime, order_number = @order_number, item_number = @item_number, trade_number = @trade_number, row_serial_number = @row_serial_number WHERE serial_number = @serial_number";

        private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_addhockService"/> class.
        /// </summary>
		public d_addhockRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_addhock dataRow = null;
		dataRow = new d_addhock();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.adhoc_key = "adhoc_key_0";
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.is_active = 0;
			dataRow.usage_datetime = new DateTime(1900,1,1);
			dataRow.order_number = 0;
			dataRow.item_number = 0;
			dataRow.trade_number = 0;
			dataRow.row_serial_number = 0;
			dataRow.adhoc_type = 0;
		dataRow = new d_addhock();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.adhoc_key = "adhoc_key_1";
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.is_active = 1;
			dataRow.usage_datetime = new DateTime(1901,2,2);
			dataRow.order_number = 1;
			dataRow.item_number = 1;
			dataRow.trade_number = 1;
			dataRow.row_serial_number = 1;
			dataRow.adhoc_type = 1;
		dataRow = new d_addhock();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.adhoc_key = "adhoc_key_2";
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.is_active = 2;
			dataRow.usage_datetime = new DateTime(1902,3,3);
			dataRow.order_number = 2;
			dataRow.item_number = 2;
			dataRow.trade_number = 2;
			dataRow.row_serial_number = 2;
			dataRow.adhoc_type = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_addhock model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.adhoc_key = GetValue(record, "adhoc_key", Convert.ToString);
            model.date_move = GetDateTime(record, "date_move");
            model.is_active = GetValue(record, "is_active", Convert.ToInt64);
            model.usage_datetime = GetDateTime(record, "usage_datetime");
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.item_number = GetValue(record, "item_number", Convert.ToInt64);
            model.trade_number = GetValue(record, "trade_number", Convert.ToInt64);
            model.row_serial_number = GetValue(record, "row_serial_number", Convert.ToInt64);
            model.adhoc_type = GetValue(record, "adhoc_type", Convert.ToInt64);
		}
    }
}
