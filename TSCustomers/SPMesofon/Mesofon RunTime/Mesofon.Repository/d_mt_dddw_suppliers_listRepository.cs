using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mt_dddw_suppliers_listRepository : RepositoryBase<d_mt_dddw_suppliers_list>
    {
		private static readonly string _selectQuery = "SELECT suppliers.number, suppliers.name FROM suppliers ORDER BY suppliers.name ASC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mt_dddw_suppliers_listService"/> class.
        /// </summary>
		public d_mt_dddw_suppliers_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mt_dddw_suppliers_list dataRow = null;
		dataRow = new d_mt_dddw_suppliers_list();
		data[0] = dataRow;
			dataRow.number = 0;
			dataRow.name = "name_0";
		dataRow = new d_mt_dddw_suppliers_list();
		data[1] = dataRow;
			dataRow.number = 1;
			dataRow.name = "name_1";
		dataRow = new d_mt_dddw_suppliers_list();
		data[2] = dataRow;
			dataRow.number = 2;
			dataRow.name = "name_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mt_dddw_suppliers_list model)
        {
            if (model == null)
            {
                return;
            }

            model.number = GetValue(record, "number", Convert.ToInt64);
            model.name = GetValue(record, "name", Convert.ToString);
		}
    }
}
