using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class lds_station_hardware_parametersRepository : RepositoryBase<lds_station_hardware_parameters>
    {
		private static readonly string _selectQuery = "SELECT station_hardware_parameters.report_db_userid, station_hardware_parameters.report_db_password, station_hardware_parameters.target_db_userid, station_hardware_parameters.target_db_password, station_hardware_parameters.printer_1, station_hardware_parameters.printer_2, station_hardware_parameters.printer_3, station_hardware_parameters.printer_4, station_hardware_parameters.printer_5, station_hardware_parameters.printer_6, station_hardware_parameters.printer_7, station_hardware_parameters.printer_8, station_hardware_parameters.printer_9, station_hardware_parameters.printer_10, station_hardware_parameters.printer_11, station_hardware_parameters.printer_12, station_hardware_parameters.printer_13, station_hardware_parameters.printer_14, station_hardware_parameters.printer_15, station_hardware_parameters.printer_16, station_hardware_parameters.printer_1_redirection, station_hardware_parameters.printer_2_redirection, station_hardware_parameters.printer_3_redirection, station_hardware_parameters.printer_4_redirection, station_hardware_parameters.printer_5_redirection, station_hardware_parameters.printer_6_redirection, station_hardware_parameters.printer_7_redirection, station_hardware_parameters.printer_8_redirection, station_hardware_parameters.printer_9_redirection, station_hardware_parameters.printer_10_redirection, station_hardware_parameters.printer_11_redirection, station_hardware_parameters.printer_12_redirection, station_hardware_parameters.printer_13_redirection, station_hardware_parameters.printer_14_redirection, station_hardware_parameters.printer_15_redirection, station_hardware_parameters.printer_16_redirection, station_hardware_parameters.drawer_first_open_active, station_hardware_parameters.drawer_second_open_active, station_hardware_parameters.drawer_check_status_active, station_hardware_parameters.drawer_status_return_logic_not, station_hardware_parameters.drawer_on_printer, station_hardware_parameters.drawer_hardware_device_type, station_hardware_parameters.customer_display_active, station_hardware_parameters.barcode_reader_active, station_hardware_parameters.msr_active, station_hardware_parameters.msr_device_type, station_hardware_parameters.msr_second_track_only, station_hardware_parameters.msr_first_track_prefix, station_hardware_parameters.msr_first_track_suffix, station_hardware_parameters.msr_second_track_prefix, station_hardware_parameters.msr_second_track_suffix, station_hardware_parameters.msr_third_track_prefix, station_hardware_parameters.msr_third_track_suffix, station_hardware_parameters.msr_third_track_delay, station_hardware_parameters.msr_com_port, station_hardware_parameters.queue_db_userid, station_hardware_parameters.queue_db_password FROM station_hardware_parameters WHERE branch_number = @0 AND station_number = @1";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="lds_station_hardware_parametersService"/> class.
        /// </summary>
		public lds_station_hardware_parametersRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			lds_station_hardware_parameters dataRow = null;
		dataRow = new lds_station_hardware_parameters();
		data[0] = dataRow;
			dataRow.report_db_userid = "report_db_userid_0";
			dataRow.report_db_password = "report_db_password_0";
			dataRow.target_db_userid = "target_db_userid_0";
			dataRow.target_db_password = "target_db_password_0";
			dataRow.printer_1 = 0;
			dataRow.printer_2 = 0;
			dataRow.printer_3 = 0;
			dataRow.printer_4 = 0;
			dataRow.printer_5 = 0;
			dataRow.printer_6 = 0;
			dataRow.printer_7 = 0;
			dataRow.printer_8 = 0;
			dataRow.printer_9 = 0;
			dataRow.printer_10 = 0;
			dataRow.printer_11 = 0;
			dataRow.printer_12 = 0;
			dataRow.printer_13 = 0;
			dataRow.printer_14 = 0;
			dataRow.printer_15 = 0;
			dataRow.printer_16 = 0;
			dataRow.printer_1_redirection = 0;
			dataRow.printer_2_redirection = 0;
			dataRow.printer_3_redirection = 0;
			dataRow.printer_4_redirection = 0;
			dataRow.printer_5_redirection = 0;
			dataRow.printer_6_redirection = 0;
			dataRow.printer_7_redirection = 0;
			dataRow.printer_8_redirection = 0;
			dataRow.printer_9_redirection = 0;
			dataRow.printer_10_redirection = 0;
			dataRow.printer_11_redirection = 0;
			dataRow.printer_12_redirection = 0;
			dataRow.printer_13_redirection = 0;
			dataRow.printer_14_redirection = 0;
			dataRow.printer_15_redirection = 0;
			dataRow.printer_16_redirection = 0;
			dataRow.drawer_first_open_active = 0;
			dataRow.drawer_second_open_active = 0;
			dataRow.drawer_check_status_active = 0;
			dataRow.drawer_status_return_logic_not = 0;
			dataRow.drawer_on_printer = 0;
			dataRow.drawer_hardware_device_type = 0;
			dataRow.customer_display_active = 0;
			dataRow.barcode_reader_active = 0;
			dataRow.msr_active = 0;
			dataRow.msr_device_type = 0;
			dataRow.msr_second_track_only = 0;
			dataRow.msr_first_track_prefix = "msr_first_track_prefix_0";
			dataRow.msr_first_track_suffix = "msr_first_track_suffix_0";
			dataRow.msr_second_track_prefix = "msr_second_track_prefix_0";
			dataRow.msr_second_track_suffix = "msr_second_track_suffix_0";
			dataRow.msr_third_track_prefix = "msr_third_track_prefix_0";
			dataRow.msr_third_track_suffix = "msr_third_track_suffix_0";
			dataRow.msr_third_track_delay = 0;
			dataRow.msr_com_port = 0;
			dataRow.queue_db_userid = "queue_db_userid_0";
			dataRow.queue_db_password = "queue_db_password_0";
		dataRow = new lds_station_hardware_parameters();
		data[1] = dataRow;
			dataRow.report_db_userid = "report_db_userid_1";
			dataRow.report_db_password = "report_db_password_1";
			dataRow.target_db_userid = "target_db_userid_1";
			dataRow.target_db_password = "target_db_password_1";
			dataRow.printer_1 = 1;
			dataRow.printer_2 = 1;
			dataRow.printer_3 = 1;
			dataRow.printer_4 = 1;
			dataRow.printer_5 = 1;
			dataRow.printer_6 = 1;
			dataRow.printer_7 = 1;
			dataRow.printer_8 = 1;
			dataRow.printer_9 = 1;
			dataRow.printer_10 = 1;
			dataRow.printer_11 = 1;
			dataRow.printer_12 = 1;
			dataRow.printer_13 = 1;
			dataRow.printer_14 = 1;
			dataRow.printer_15 = 1;
			dataRow.printer_16 = 1;
			dataRow.printer_1_redirection = 1;
			dataRow.printer_2_redirection = 1;
			dataRow.printer_3_redirection = 1;
			dataRow.printer_4_redirection = 1;
			dataRow.printer_5_redirection = 1;
			dataRow.printer_6_redirection = 1;
			dataRow.printer_7_redirection = 1;
			dataRow.printer_8_redirection = 1;
			dataRow.printer_9_redirection = 1;
			dataRow.printer_10_redirection = 1;
			dataRow.printer_11_redirection = 1;
			dataRow.printer_12_redirection = 1;
			dataRow.printer_13_redirection = 1;
			dataRow.printer_14_redirection = 1;
			dataRow.printer_15_redirection = 1;
			dataRow.printer_16_redirection = 1;
			dataRow.drawer_first_open_active = 1;
			dataRow.drawer_second_open_active = 1;
			dataRow.drawer_check_status_active = 1;
			dataRow.drawer_status_return_logic_not = 1;
			dataRow.drawer_on_printer = 1;
			dataRow.drawer_hardware_device_type = 1;
			dataRow.customer_display_active = 1;
			dataRow.barcode_reader_active = 1;
			dataRow.msr_active = 1;
			dataRow.msr_device_type = 1;
			dataRow.msr_second_track_only = 1;
			dataRow.msr_first_track_prefix = "msr_first_track_prefix_1";
			dataRow.msr_first_track_suffix = "msr_first_track_suffix_1";
			dataRow.msr_second_track_prefix = "msr_second_track_prefix_1";
			dataRow.msr_second_track_suffix = "msr_second_track_suffix_1";
			dataRow.msr_third_track_prefix = "msr_third_track_prefix_1";
			dataRow.msr_third_track_suffix = "msr_third_track_suffix_1";
			dataRow.msr_third_track_delay = 1;
			dataRow.msr_com_port = 1;
			dataRow.queue_db_userid = "queue_db_userid_1";
			dataRow.queue_db_password = "queue_db_password_1";
		dataRow = new lds_station_hardware_parameters();
		data[2] = dataRow;
			dataRow.report_db_userid = "report_db_userid_2";
			dataRow.report_db_password = "report_db_password_2";
			dataRow.target_db_userid = "target_db_userid_2";
			dataRow.target_db_password = "target_db_password_2";
			dataRow.printer_1 = 2;
			dataRow.printer_2 = 2;
			dataRow.printer_3 = 2;
			dataRow.printer_4 = 2;
			dataRow.printer_5 = 2;
			dataRow.printer_6 = 2;
			dataRow.printer_7 = 2;
			dataRow.printer_8 = 2;
			dataRow.printer_9 = 2;
			dataRow.printer_10 = 2;
			dataRow.printer_11 = 2;
			dataRow.printer_12 = 2;
			dataRow.printer_13 = 2;
			dataRow.printer_14 = 2;
			dataRow.printer_15 = 2;
			dataRow.printer_16 = 2;
			dataRow.printer_1_redirection = 2;
			dataRow.printer_2_redirection = 2;
			dataRow.printer_3_redirection = 2;
			dataRow.printer_4_redirection = 2;
			dataRow.printer_5_redirection = 2;
			dataRow.printer_6_redirection = 2;
			dataRow.printer_7_redirection = 2;
			dataRow.printer_8_redirection = 2;
			dataRow.printer_9_redirection = 2;
			dataRow.printer_10_redirection = 2;
			dataRow.printer_11_redirection = 2;
			dataRow.printer_12_redirection = 2;
			dataRow.printer_13_redirection = 2;
			dataRow.printer_14_redirection = 2;
			dataRow.printer_15_redirection = 2;
			dataRow.printer_16_redirection = 2;
			dataRow.drawer_first_open_active = 2;
			dataRow.drawer_second_open_active = 2;
			dataRow.drawer_check_status_active = 2;
			dataRow.drawer_status_return_logic_not = 2;
			dataRow.drawer_on_printer = 2;
			dataRow.drawer_hardware_device_type = 2;
			dataRow.customer_display_active = 2;
			dataRow.barcode_reader_active = 2;
			dataRow.msr_active = 2;
			dataRow.msr_device_type = 2;
			dataRow.msr_second_track_only = 2;
			dataRow.msr_first_track_prefix = "msr_first_track_prefix_2";
			dataRow.msr_first_track_suffix = "msr_first_track_suffix_2";
			dataRow.msr_second_track_prefix = "msr_second_track_prefix_2";
			dataRow.msr_second_track_suffix = "msr_second_track_suffix_2";
			dataRow.msr_third_track_prefix = "msr_third_track_prefix_2";
			dataRow.msr_third_track_suffix = "msr_third_track_suffix_2";
			dataRow.msr_third_track_delay = 2;
			dataRow.msr_com_port = 2;
			dataRow.queue_db_userid = "queue_db_userid_2";
			dataRow.queue_db_password = "queue_db_password_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, lds_station_hardware_parameters model)
        {
            if (model == null)
            {
                return;
            }

            model.report_db_userid = GetValue(record, "report_db_userid", Convert.ToString);
            model.report_db_password = GetValue(record, "report_db_password", Convert.ToString);
            model.target_db_userid = GetValue(record, "target_db_userid", Convert.ToString);
            model.target_db_password = GetValue(record, "target_db_password", Convert.ToString);
            model.printer_1 = GetValue(record, "printer_1", Convert.ToInt64);
            model.printer_2 = GetValue(record, "printer_2", Convert.ToInt64);
            model.printer_3 = GetValue(record, "printer_3", Convert.ToInt64);
            model.printer_4 = GetValue(record, "printer_4", Convert.ToInt64);
            model.printer_5 = GetValue(record, "printer_5", Convert.ToInt64);
            model.printer_6 = GetValue(record, "printer_6", Convert.ToInt64);
            model.printer_7 = GetValue(record, "printer_7", Convert.ToInt64);
            model.printer_8 = GetValue(record, "printer_8", Convert.ToInt64);
            model.printer_9 = GetValue(record, "printer_9", Convert.ToInt64);
            model.printer_10 = GetValue(record, "printer_10", Convert.ToInt64);
            model.printer_11 = GetValue(record, "printer_11", Convert.ToInt64);
            model.printer_12 = GetValue(record, "printer_12", Convert.ToInt64);
            model.printer_13 = GetValue(record, "printer_13", Convert.ToInt64);
            model.printer_14 = GetValue(record, "printer_14", Convert.ToInt64);
            model.printer_15 = GetValue(record, "printer_15", Convert.ToInt64);
            model.printer_16 = GetValue(record, "printer_16", Convert.ToInt64);
            model.printer_1_redirection = GetValue(record, "printer_1_redirection", Convert.ToInt64);
            model.printer_2_redirection = GetValue(record, "printer_2_redirection", Convert.ToInt64);
            model.printer_3_redirection = GetValue(record, "printer_3_redirection", Convert.ToInt64);
            model.printer_4_redirection = GetValue(record, "printer_4_redirection", Convert.ToInt64);
            model.printer_5_redirection = GetValue(record, "printer_5_redirection", Convert.ToInt64);
            model.printer_6_redirection = GetValue(record, "printer_6_redirection", Convert.ToInt64);
            model.printer_7_redirection = GetValue(record, "printer_7_redirection", Convert.ToInt64);
            model.printer_8_redirection = GetValue(record, "printer_8_redirection", Convert.ToInt64);
            model.printer_9_redirection = GetValue(record, "printer_9_redirection", Convert.ToInt64);
            model.printer_10_redirection = GetValue(record, "printer_10_redirection", Convert.ToInt64);
            model.printer_11_redirection = GetValue(record, "printer_11_redirection", Convert.ToInt64);
            model.printer_12_redirection = GetValue(record, "printer_12_redirection", Convert.ToInt64);
            model.printer_13_redirection = GetValue(record, "printer_13_redirection", Convert.ToInt64);
            model.printer_14_redirection = GetValue(record, "printer_14_redirection", Convert.ToInt64);
            model.printer_15_redirection = GetValue(record, "printer_15_redirection", Convert.ToInt64);
            model.printer_16_redirection = GetValue(record, "printer_16_redirection", Convert.ToInt64);
            model.drawer_first_open_active = GetValue(record, "drawer_first_open_active", Convert.ToDouble);
            model.drawer_second_open_active = GetValue(record, "drawer_second_open_active", Convert.ToDouble);
            model.drawer_check_status_active = GetValue(record, "drawer_check_status_active", Convert.ToDouble);
            model.drawer_status_return_logic_not = GetValue(record, "drawer_status_return_logic_not", Convert.ToDouble);
            model.drawer_on_printer = GetValue(record, "drawer_on_printer", Convert.ToDouble);
            model.drawer_hardware_device_type = GetValue(record, "drawer_hardware_device_type", Convert.ToInt64);
            model.customer_display_active = GetValue(record, "customer_display_active", Convert.ToDouble);
            model.barcode_reader_active = GetValue(record, "barcode_reader_active", Convert.ToDouble);
            model.msr_active = GetValue(record, "msr_active", Convert.ToDouble);
            model.msr_device_type = GetValue(record, "msr_device_type", Convert.ToInt64);
            model.msr_second_track_only = GetValue(record, "msr_second_track_only", Convert.ToDouble);
            model.msr_first_track_prefix = GetValue(record, "msr_first_track_prefix", Convert.ToString);
            model.msr_first_track_suffix = GetValue(record, "msr_first_track_suffix", Convert.ToString);
            model.msr_second_track_prefix = GetValue(record, "msr_second_track_prefix", Convert.ToString);
            model.msr_second_track_suffix = GetValue(record, "msr_second_track_suffix", Convert.ToString);
            model.msr_third_track_prefix = GetValue(record, "msr_third_track_prefix", Convert.ToString);
            model.msr_third_track_suffix = GetValue(record, "msr_third_track_suffix", Convert.ToString);
            model.msr_third_track_delay = GetValue(record, "msr_third_track_delay", Convert.ToDecimal);
            model.msr_com_port = GetValue(record, "msr_com_port", Convert.ToInt64);
            model.queue_db_userid = GetValue(record, "queue_db_userid", Convert.ToString);
            model.queue_db_password = GetValue(record, "queue_db_password", Convert.ToString);
		}
    }
}
