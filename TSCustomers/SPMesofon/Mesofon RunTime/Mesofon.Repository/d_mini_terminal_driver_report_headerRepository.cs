using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_driver_report_headerRepository : RepositoryBase<d_mini_terminal_driver_report_header>
    {
		private static readonly string _selectQuery = "SELECT MAX(shipment_pallets.date_move) AS date_move, shipment_pallets.branch_number, shipment_pallets.shipment_number, shipment_pallets.pallet_number, SUM(shipment_pallets.pack_quantity) AS pack_quantity, MAX(shipment_pallets.arrive_datetime) AS arrive_datetime, MAX(convert(varchar, shipment_pallets.arrive_datetime, 18)) AS arrive_time, MIN(shipment_pallets.auto_scan_status) AS auto_scan_status, (SELECT global_parameters.branch_name FROM global_parameters WHERE serial_number = shipment_pallets.branch_number) AS branch_name, convert(varchar, getdate(), 18) AS scan_time, convert(varchar, getdate(), 103) AS print_date, (SELECT IsNull(SUM(SP.pack_quantity), 0) FROM shipment_pallets AS SP WHERE SP.branch_number = shipment_pallets.branch_number AND SP.shipment_number = shipment_pallets.shipment_number AND SP.pallet_number = shipment_pallets.pallet_number AND SP.document_type = 1) AS crossdoc_quantity, (SELECT IsNull(name, '') FROM employees WHERE branch_number = @0 AND number = @1) AS employee_name FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.shipment_number = @2) AND (shipment_pallets.marlog_number = @3) GROUP BY shipment_pallets.branch_number, shipment_pallets.shipment_number, shipment_pallets.pallet_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_driver_report_headerService"/> class.
        /// </summary>
		public d_mini_terminal_driver_report_headerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_driver_report_header dataRow = null;
		dataRow = new d_mini_terminal_driver_report_header();
		data[0] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1900,1,1);
			dataRow.shipment_pallets_branch_number = 0;
			dataRow.shipment_pallets_shipment_number = 0;
			dataRow.shipment_pallets_pallet_number = "shipment_pallets_pallet_number_0";
			dataRow.shipment_pallets_pack_quantity = 0;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1900,1,1).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_0";
			dataRow.shipment_pallets_auto_scan_status = 0;
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_0";
			dataRow.scan_time = "scan_time_0";
			dataRow.print_date = "print_date_0";
			dataRow.crossdoc_quantity = 0;
			dataRow.employee_name = "employee_name_0";
			dataRow.compute_pallets_number = "compute_pallets_number_0";
			dataRow.compute_pack_quantity = "compute_pack_quantity_0";
		dataRow = new d_mini_terminal_driver_report_header();
		data[1] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1901,2,2);
			dataRow.shipment_pallets_branch_number = 1;
			dataRow.shipment_pallets_shipment_number = 1;
			dataRow.shipment_pallets_pallet_number = "shipment_pallets_pallet_number_1";
			dataRow.shipment_pallets_pack_quantity = 1;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1901,2,2).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_1";
			dataRow.shipment_pallets_auto_scan_status = 1;
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_1";
			dataRow.scan_time = "scan_time_1";
			dataRow.print_date = "print_date_1";
			dataRow.crossdoc_quantity = 1;
			dataRow.employee_name = "employee_name_1";
			dataRow.compute_pallets_number = "compute_pallets_number_1";
			dataRow.compute_pack_quantity = "compute_pack_quantity_1";
		dataRow = new d_mini_terminal_driver_report_header();
		data[2] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1902,3,3);
			dataRow.shipment_pallets_branch_number = 2;
			dataRow.shipment_pallets_shipment_number = 2;
			dataRow.shipment_pallets_pallet_number = "shipment_pallets_pallet_number_2";
			dataRow.shipment_pallets_pack_quantity = 2;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1902,3,3).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_2";
			dataRow.shipment_pallets_auto_scan_status = 2;
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_2";
			dataRow.scan_time = "scan_time_2";
			dataRow.print_date = "print_date_2";
			dataRow.crossdoc_quantity = 2;
			dataRow.employee_name = "employee_name_2";
			dataRow.compute_pallets_number = "compute_pallets_number_2";
			dataRow.compute_pack_quantity = "compute_pack_quantity_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_driver_report_header model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_pallets_date_move = GetDateTime(record, "date_move");
            model.shipment_pallets_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.shipment_pallets_shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.shipment_pallets_pallet_number = GetValue(record, "pallet_number", Convert.ToString);
            model.shipment_pallets_pack_quantity = GetValue(record, "pack_quantity", Convert.ToInt64);
            model.shipment_pallets_arrive_datetime = GetDateTime(record, "arrive_datetime")?.ToString("dd/MM/yyyy");
            model.shipment_pallets_arrive_time = GetValue(record, "arrive_time", Convert.ToString);
            model.shipment_pallets_auto_scan_status = GetValue(record, "auto_scan_status", Convert.ToInt64);
            model.global_parameters_branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.scan_time = GetValue(record, "scan_time", Convert.ToString);
            model.print_date = GetValue(record, "print_date", Convert.ToString);
            model.crossdoc_quantity = GetValue(record, "crossdoc_quantity", Convert.ToInt64);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            //model.compute_pallets_number = GetValue(record, "", Convert.ToString);
            //model.compute_pack_quantity = GetValue(record, "", Convert.ToString);
		}
    }
}
