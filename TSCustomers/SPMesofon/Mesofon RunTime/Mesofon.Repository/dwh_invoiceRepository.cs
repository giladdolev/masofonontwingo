using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class dwh_invoiceRepository : RepositoryBase<dwh_invoice>
    {
        private static readonly string _selectQuery = "SELECT invoice_move.invoice_number, invoice_move.supply_date, invoice_move.supplier_number,  invoice_move.employee_number, invoice_move.date_move, invoice_move.discount,  invoice_move.state, invoice_move.mam, invoice_move.station_num,  invoice_move.invoice_total, invoice_move.store_number,  invoice_move.stock_number, invoice_move.branch_number, invoice_move.discount_percent, invoice_move.expected_total_amount, invoice_move.log_book, invoice_move.invoice_type, invoice_move.b2b_status, invoice_move.order_number, invoice_move.payment_code_number,  invoice_move.payments_number, invoice_move.stock_update_datetime,  invoice_move.distributor_number, invoice_move.price_list_number, IsNull(invoice_move.life_manufacturer, 0) AS life_manufacturer, invoice_move.last_update_datetime FROM invoice_move WHERE (invoice_move.invoice_number = @0) AND (invoice_move.branch_number = @1) AND (invoice_move.supplier_number = @2) AND (invoice_move.invoice_type = @3)";
        private static readonly string _insertQuery = "INSERT INTO [invoice_move] ([invoice_number] ,[branch_number] ,[supplier_number] ,[invoice_type] ,[employee_number] ,[supply_date] ,[date_move] ,[discount] ,[mam] ,[invoice_total] ,[stock_number] ,[store_number] ,[station_num] ,[log_book] ,[expected_total_amount] ,[discount_percent] ,[state]  ,[price_list_number]  ,[b2b_status] ,[order_number] ,[payment_code_number] ,[payments_number] ,[distributor_number] ,[stock_update_datetime] ,[last_update_datetime] ,[life_manufacturer])VALUES(@invoice_number ,@branch_number ,@supplier_number ,@invoice_type ,@employee_number ,@supply_date ,@date_move ,@discount ,@mam ,@invoice_total ,@stock_number ,@store_number ,@station_num ,@log_book ,@expected_total_amount ,@discount_percent ,@state ,@price_list_number ,@b2b_status ,@order_number ,@payment_code_number ,@payments_number ,@distributor_number ,@stock_update_datetime ,@last_update_datetime ,@life_manufacturer)";
        private static readonly string _updateKeyOnlyQuery = "update invoice_move " +
            "set  " +
            "supply_date = @supply_date, " +
            "employee_number = @employee_number, " +
            "date_move = @date_move, " +
            "discount = @discount, " +
            "state = @state, " +
            "mam = @mam, " +
            "station_num = @station_num, " +
            "invoice_total = @invoice_total, " +
            "store_number = @store_number, " +
            "stock_number = @stock_number, " +
            "discount_percent = @discount_percent, " +
            "expected_total_amount = @expected_total_amount, " +
            "log_book = @log_book, " +
            "order_number = @order_number, " +
            "payment_code_number = @payment_code_number, " +
            "payments_number = @payments_number, " +
            "stock_update_datetime = @stock_update_datetime, " +
            "distributor_number = @distributor_number, " +
            "price_list_number = @price_list_number, " +
            "life_manufacturer = @life_manufacturer, " +
            "last_update_datetime = @last_update_datetime " +
            "WHERE branch_number = @branch_number AND supplier_number = @supplier_number AND invoice_type = @invoice_type AND invoice_number = @invoice_number";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dwh_invoiceService"/> class.
        /// </summary>
		public dwh_invoiceRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("C", "������ ������");
            codeTable.Add("S", "���� ����");
            codeTable.Add("D", "�����");
            LookupTables.Add("state", codeTable);

            codeTable = new Dictionary<string, object>();
            codeTable.Add("1", "Yes");
            codeTable.Add("0", "No");
            LookupTables.Add("b2b_status", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            dwh_invoice dataRow = null;
            dataRow = new dwh_invoice();
            data[0] = dataRow;
            dataRow.invoice_number = 0;
            dataRow.supply_date = new DateTime(1900, 1, 1);
            dataRow.supplier_number = 0;
            dataRow.employee_number = 0;
            dataRow.date_move = new DateTime(1900, 1, 1);
            dataRow.discount = 0;
            dataRow.state = "state_0";
            dataRow.mam = 0;
            dataRow.station_num = 0;
            dataRow.invoice_total = 0;
            dataRow.store_number = 0;
            dataRow.stock_number = 0;
            dataRow.branch_number = 0;
            dataRow.discount_percentage = 0;
            dataRow.expected_total_amount = 0;
            dataRow.log_book = 0;
            dataRow.invoice_type = "invoice_type_0";
            dataRow.b2b_status = 0;
            dataRow.order_number = 0;
            dataRow.payment_code_number = 0;
            dataRow.payments_number = 0;
            dataRow.stock_update_datetime = new DateTime(1900, 1, 1);
            dataRow.distributor_number = 0;
            dataRow.price_list_number = 0;
            dataRow.life_manufacturer = 0;
            dataRow.last_update_datetime = new DateTime(1900, 1, 1);
            dataRow.discount_percent = 0;
            dataRow = new dwh_invoice();
            data[1] = dataRow;
            dataRow.invoice_number = 1;
            dataRow.supply_date = new DateTime(1901, 2, 2);
            dataRow.supplier_number = 1;
            dataRow.employee_number = 1;
            dataRow.date_move = new DateTime(1901, 2, 2);
            dataRow.discount = 1;
            dataRow.state = "state_1";
            dataRow.mam = 1;
            dataRow.station_num = 1;
            dataRow.invoice_total = 1;
            dataRow.store_number = 1;
            dataRow.stock_number = 1;
            dataRow.branch_number = 1;
            dataRow.discount_percentage = 1;
            dataRow.expected_total_amount = 1;
            dataRow.log_book = 1;
            dataRow.invoice_type = "invoice_type_1";
            dataRow.b2b_status = 1;
            dataRow.order_number = 1;
            dataRow.payment_code_number = 1;
            dataRow.payments_number = 1;
            dataRow.stock_update_datetime = new DateTime(1901, 2, 2);
            dataRow.distributor_number = 1;
            dataRow.price_list_number = 1;
            dataRow.life_manufacturer = 1;
            dataRow.last_update_datetime = new DateTime(1901, 2, 2);
            dataRow.discount_percent = 0;
            dataRow = new dwh_invoice();
            data[2] = dataRow;
            dataRow.invoice_number = 2;
            dataRow.supply_date = new DateTime(1902, 3, 3);
            dataRow.supplier_number = 2;
            dataRow.employee_number = 2;
            dataRow.date_move = new DateTime(1902, 3, 3);
            dataRow.discount = 2;
            dataRow.state = "state_2";
            dataRow.mam = 2;
            dataRow.station_num = 2;
            dataRow.invoice_total = 2;
            dataRow.store_number = 2;
            dataRow.stock_number = 2;
            dataRow.branch_number = 2;
            dataRow.discount_percentage = 2;
            dataRow.expected_total_amount = 2;
            dataRow.log_book = 2;
            dataRow.invoice_type = "invoice_type_2";
            dataRow.b2b_status = 2;
            dataRow.order_number = 2;
            dataRow.payment_code_number = 2;
            dataRow.payments_number = 2;
            dataRow.stock_update_datetime = new DateTime(1902, 3, 3);
            dataRow.distributor_number = 2;
            dataRow.price_list_number = 2;
            dataRow.life_manufacturer = 2;
            dataRow.last_update_datetime = new DateTime(1902, 3, 3);
            dataRow.discount_percent = 0;
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dwh_invoice model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.date_move = GetDateTime(record, "date_move");
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.state = GetValue(record, "state", Convert.ToString);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.station_num = GetValue(record, "station_num", Convert.ToInt64);
            model.invoice_total = GetValue(record, "invoice_total", Convert.ToDecimal);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.discount_percentage = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.payment_code_number = GetValue(record, "payment_code_number", Convert.ToInt64);
            model.payments_number = GetValue(record, "payments_number", Convert.ToInt64);
            model.stock_update_datetime = GetDateTime(record, "stock_update_datetime");
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.price_list_number = GetValue(record, "price_list_number", Convert.ToInt64);
            model.life_manufacturer = GetValue(record, "life_manufacturer", Convert.ToInt64);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            model.discount_percent = Math.Round((model.discount / (model.invoice_total / (1 + model.mam) + model.discount)) * 100, 2);


        }
    }
}
