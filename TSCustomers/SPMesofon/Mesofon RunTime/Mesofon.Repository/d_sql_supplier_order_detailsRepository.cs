using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_supplier_order_detailsRepository : RepositoryBase<d_sql_supplier_order_details>
    {
		private static readonly string _selectQuery = "SELECT supplier_order_details.serial_number, supplier_order_details.branch_number, supplier_order_details.order_number, supplier_order_details.material_number, supplier_order_details.material_price_after_discount, supplier_order_details.quantity_within_invoice, supplier_order_details.material_discount_amount, supplier_order_details.color, supplier_order_details.sell_price, supplier_order_details.supplier_number, supplier_order_details.material_price, supplier_order_details.material_quantity, supplier_order_details.stock_number, supplier_order_details.details, supplier_order_details.material_discount_percent, supplier_order_details.pay_shape, supplier_order_details.sort_number, supplier_order_details.bonus_quantity, supplier_order_details.material_bonus_connect, supplier_order_details.bonus_row, supplier_order_details.stock_balance, supplier_order_details.this_month_sales, supplier_order_details.last_month_sales, supplier_order_details.two_last_months_sales, supplier_order_details.min_week_sales, supplier_order_details.max_week_sales, supplier_order_details.recommended_quantity, supplier_order_details.confirmed_orders, supplier_order_details.row_in_order, supplier_order_details.ordered_not_received, supplier_order_details.row_found, supplier_order_details.avg_week_sales FROM supplier_order_details WHERE supplier_order_details.branch_number = @0 AND supplier_order_details.order_number = @1 AND supplier_order_details.supplier_number = @2";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[supplier_order_details] ([serial_number], [branch_number], [order_number], [material_number], [material_price_after_discount], [quantity_within_invoice], [material_discount_amount], [color], [sell_price], [supplier_number], [material_price], [material_quantity], [stock_number], [details], [material_discount_percent], [pay_shape], [sort_number], [bonus_quantity], [material_bonus_connect], [bonus_row], [stock_balance], [this_month_sales], [last_month_sales], [two_last_months_sales], [min_week_sales], [max_week_sales], [recommended_quantity], [confirmed_orders], [row_in_order], [ordered_not_received], [row_found], [avg_week_sales]) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19, @p20, @p21, @p22, @p23, @p24, @p25, @p26, @p27, @p28, @p29, @p30, @p31, @p32)";
        private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[supplier_order_details] SET [serial_number] = @serial_number, [branch_number] = @branch_number, [order_number] = @order_number, [material_number] = @material_number, [material_price_after_discount] = @material_price_after_discount " +
            ", [quantity_within_invoice] = @quantity_within_invoice"+
            " , [material_discount_amount] = @material_discount_amount, [color] = @color, [sell_price] = @sell_price, [supplier_number] = @supplier_number, [material_price] = @material_price, [material_quantity] = @material_quantity, [stock_number] = @stock_number, [details] = @details, [material_discount_percent] = @material_discount_percent, [pay_shape] = @pay_shape, [sort_number] = @sort_number, [bonus_quantity] = @bonus_quantity, [material_bonus_connect] = @material_bonus_connect, [bonus_row] = @bonus_row, [stock_balance] = @stock_balance, [this_month_sales] = @this_month_sales, [last_month_sales] = @last_month_sales, [two_last_months_sales] = @two_last_months_sales, [min_week_sales] = @min_week_sales, [max_week_sales] = @max_week_sales, [recommended_quantity] = @recommended_quantity, [confirmed_orders] = @confirmed_orders, [row_in_order] = @row_in_order, [ordered_not_received] = @ordered_not_received, [row_found] = @row_found, [avg_week_sales] = @avg_week_sales WHERE (([serial_number] = @serial_number) AND ([branch_number] = @branch_number))";
        private static readonly string _updateColumnsQuery = "";// UPDATE [dbo].[supplier_order_details] SET [serial_number] = @p1, [branch_number] = @p2, [order_number] = @p3, [material_number] = @p4, [material_price_after_discount] = @p5, [quantity_within_invoice] = @p6, [material_discount_amount] = @p7, [color] = @p8, [sell_price] = @p9, [supplier_number] = @p10, [material_price] = @p11, [material_quantity] = @p12, [stock_number] = @p13, [details] = @p14, [material_discount_percent] = @p15, [pay_shape] = @p16, [sort_number] = @p17, [bonus_quantity] = @p18, [material_bonus_connect] = @p19, [bonus_row] = @p20, [stock_balance] = @p21, [this_month_sales] = @p22, [last_month_sales] = @p23, [two_last_months_sales] = @p24, [min_week_sales] = @p25, [max_week_sales] = @p26, [recommended_quantity] = @p27, [confirmed_orders] = @p28, [row_in_order] = @p29, [ordered_not_received] = @p30, [row_found] = @p31, [avg_week_sales] = @p32 WHERE (([serial_number] = @p33) AND ([branch_number] = @p34) AND ([order_number] = @p35) AND ([material_number] = @p36) AND ((@p37 = 1 AND [material_price_after_discount] IS NULL) OR ([material_price_after_discount] = @p38)) AND ((@p39 = 1 AND [quantity_within_invoice] IS NULL) OR ([quantity_within_invoice] = @p40)) AND ((@p41 = 1 AND [material_discount_amount] IS NULL) OR ([material_discount_amount] = @p42)) AND ((@p43 = 1 AND [color] IS NULL) OR ([color] = @p44)) AND ((@p45 = 1 AND [sell_price] IS NULL) OR ([sell_price] = @p46)) AND ([supplier_number] = @p47) AND ([material_price] = @p48) AND ([material_quantity] = @p49) AND ([stock_number] = @p50) AND ((@p51 = 1 AND [details] IS NULL) OR ([details] = @p52)) AND ((@p53 = 1 AND [material_discount_percent] IS NULL) OR ([material_discount_percent] = @p54)) AND ((@p55 = 1 AND [pay_shape] IS NULL) OR ([pay_shape] = @p56)) AND ((@p57 = 1 AND [sort_number] IS NULL) OR ([sort_number] = @p58)) AND ((@p59 = 1 AND [bonus_quantity] IS NULL) OR ([bonus_quantity] = @p60)) AND ((@p61 = 1 AND [material_bonus_connect] IS NULL) OR ([material_bonus_connect] = @p62)) AND ((@p63 = 1 AND [bonus_row] IS NULL) OR ([bonus_row] = @p64)) AND ((@p65 = 1 AND [stock_balance] IS NULL) OR ([stock_balance] = @p66)) AND ((@p67 = 1 AND [this_month_sales] IS NULL) OR ([this_month_sales] = @p68)) AND ((@p69 = 1 AND [last_month_sales] IS NULL) OR ([last_month_sales] = @p70)) AND ((@p71 = 1 AND [two_last_months_sales] IS NULL) OR ([two_last_months_sales] = @p72)) AND ((@p73 = 1 AND [min_week_sales] IS NULL) OR ([min_week_sales] = @p74)) AND ((@p75 = 1 AND [max_week_sales] IS NULL) OR ([max_week_sales] = @p76)) AND ((@p77 = 1 AND [recommended_quantity] IS NULL) OR ([recommended_quantity] = @p78)) AND ((@p79 = 1 AND [confirmed_orders] IS NULL) OR ([confirmed_orders] = @p80)) AND ((@p81 = 1 AND [row_in_order] IS NULL) OR ([row_in_order] = @p82)) AND ((@p83 = 1 AND [ordered_not_received] IS NULL) OR ([ordered_not_received] = @p84)) AND ((@p85 = 1 AND [row_found] IS NULL) OR ([row_found] = @p86)) AND ((@p87 = 1 AND [avg_week_sales] IS NULL) OR ([avg_week_sales] = @p88)))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[supplier_order_details] WHERE (([serial_number] = @p1) AND ([branch_number] = @p2))";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_supplier_order_detailsService"/> class.
        /// </summary>
		public d_sql_supplier_order_detailsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_supplier_order_details dataRow = null;
		dataRow = new d_sql_supplier_order_details();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.branch_number = 0;
			dataRow.order_number = 0;
			dataRow.material_number = 0;
			dataRow.material_price_after_discount = 0;
			dataRow.quantity_within_invoice = 0;
			dataRow.material_discount_amount = 0;
			dataRow.color = 0;
			dataRow.sell_price = 0;
			dataRow.supplier_number = 0;
			dataRow.material_price = 0;
			dataRow.material_quantity = 0;
			dataRow.stock_number = 0;
			dataRow.details = "details_0";
			dataRow.material_discount_percent = 0;
			dataRow.pay_shape = 0;
			dataRow.sort_number = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_bonus_connect = 0;
			dataRow.bonus_row = "bonus_row_0";
			dataRow.stock_balance = 0;
			dataRow.this_month_sales = 0;
			dataRow.last_month_sales = 0;
			dataRow.two_last_months_sales = 0;
			dataRow.min_week_sales = 0;
			dataRow.max_week_sales = 0;
			dataRow.recommended_quantity = 0;
			dataRow.confirmed_orders = 0;
			dataRow.row_in_order = 0;
			dataRow.ordered_not_received = "ordered_not_received_0";
			dataRow.row_found = 0;
			dataRow.avg_week_sales = 0;
		dataRow = new d_sql_supplier_order_details();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.branch_number = 1;
			dataRow.order_number = 1;
			dataRow.material_number = 1;
			dataRow.material_price_after_discount = 1;
			dataRow.quantity_within_invoice = 1;
			dataRow.material_discount_amount = 1;
			dataRow.color = 1;
			dataRow.sell_price = 1;
			dataRow.supplier_number = 1;
			dataRow.material_price = 1;
			dataRow.material_quantity = 1;
			dataRow.stock_number = 1;
			dataRow.details = "details_1";
			dataRow.material_discount_percent = 1;
			dataRow.pay_shape = 1;
			dataRow.sort_number = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_bonus_connect = 1;
			dataRow.bonus_row = "bonus_row_1";
			dataRow.stock_balance = 1;
			dataRow.this_month_sales = 1;
			dataRow.last_month_sales = 1;
			dataRow.two_last_months_sales = 1;
			dataRow.min_week_sales = 1;
			dataRow.max_week_sales = 1;
			dataRow.recommended_quantity = 1;
			dataRow.confirmed_orders = 1;
			dataRow.row_in_order = 1;
			dataRow.ordered_not_received = "ordered_not_received_1";
			dataRow.row_found = 1;
			dataRow.avg_week_sales = 1;
		dataRow = new d_sql_supplier_order_details();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.branch_number = 2;
			dataRow.order_number = 2;
			dataRow.material_number = 2;
			dataRow.material_price_after_discount = 2;
			dataRow.quantity_within_invoice = 2;
			dataRow.material_discount_amount = 2;
			dataRow.color = 2;
			dataRow.sell_price = 2;
			dataRow.supplier_number = 2;
			dataRow.material_price = 2;
			dataRow.material_quantity = 2;
			dataRow.stock_number = 2;
			dataRow.details = "details_2";
			dataRow.material_discount_percent = 2;
			dataRow.pay_shape = 2;
			dataRow.sort_number = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_bonus_connect = 2;
			dataRow.bonus_row = "bonus_row_2";
			dataRow.stock_balance = 2;
			dataRow.this_month_sales = 2;
			dataRow.last_month_sales = 2;
			dataRow.two_last_months_sales = 2;
			dataRow.min_week_sales = 2;
			dataRow.max_week_sales = 2;
			dataRow.recommended_quantity = 2;
			dataRow.confirmed_orders = 2;
			dataRow.row_in_order = 2;
			dataRow.ordered_not_received = "ordered_not_received_2";
			dataRow.row_found = 2;
			dataRow.avg_week_sales = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_supplier_order_details model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.details = GetValue(record, "details", Convert.ToString);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.sort_number = GetValue(record, "sort_number", Convert.ToInt64);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.stock_balance = GetValue(record, "stock_balance", Convert.ToDecimal);
            model.this_month_sales = GetValue(record, "this_month_sales", Convert.ToDecimal);
            model.last_month_sales = GetValue(record, "last_month_sales", Convert.ToDecimal);
            model.two_last_months_sales = GetValue(record, "two_last_months_sales", Convert.ToDecimal);
            model.min_week_sales = GetValue(record, "min_week_sales", Convert.ToDecimal);
            model.max_week_sales = GetValue(record, "max_week_sales", Convert.ToDecimal);
            model.recommended_quantity = GetValue(record, "recommended_quantity", Convert.ToDecimal);
            model.confirmed_orders = GetValue(record, "confirmed_orders", Convert.ToDecimal);
            model.row_in_order = GetValue(record, "row_in_order", Convert.ToInt64);
            model.ordered_not_received = GetValue(record, "ordered_not_received", Convert.ToString);
            model.row_found = GetValue(record, "row_found", Convert.ToInt64);
            model.avg_week_sales = GetValue(record, "avg_week_sales", Convert.ToDecimal);
		}
    }
}
