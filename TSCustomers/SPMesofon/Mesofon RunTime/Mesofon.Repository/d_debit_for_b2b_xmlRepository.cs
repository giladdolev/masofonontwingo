using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_debit_for_b2b_xmlRepository : RepositoryBase<d_debit_for_b2b_xml>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_debit_for_b2b_xmlService"/> class.
        /// </summary>
		public d_debit_for_b2b_xmlRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_debit_for_b2b_xml dataRow = null;
		dataRow = new d_debit_for_b2b_xml();
		data[0] = dataRow;
			dataRow.sender = "sender_0";
			dataRow.receiver = "receiver_0";
			dataRow.doctype = "doctype_0";
			dataRow.aprf = "aprf_0";
			dataRow.snrf = "snrf_0";
			dataRow.ackn = "ackn_0";
			dataRow.testind = "testind_0";
			dataRow.messdate = 0;
			dataRow.messtime = "messtime_0";
			dataRow.invoicetype = "invoicetype_0";
			dataRow.debitinvoiceno = "debitinvoiceno_0";
			dataRow.invoicefunc = "invoicefunc_0";
			dataRow.datetime = 0;
			dataRow.supplierinvoiceno = "supplierinvoiceno_0";
			dataRow.supplierinvoicedate = 0;
			dataRow.referenceno = "referenceno_0";
			dataRow.referencedate = 0;
			dataRow.pricelistno = "pricelistno_0";
			dataRow.pricelistdate = 0;
			dataRow.supplierno = "supplierno_0";
			dataRow.suppliername = "suppliername_0";
			dataRow.storeno = "storeno_0";
			dataRow.storename = "storename_0";
			dataRow.discountpercent = 0;
			dataRow.discountamount = 0;
			dataRow.linessum = 0;
			dataRow.docsum = 0;
			dataRow.invsum = 0;
			dataRow.lineno = 0;
			dataRow.itembarcode = "itembarcode_0";
			dataRow.proddesc = "proddesc_0";
			dataRow.packno = 0;
			dataRow.unitsqty = 0;
			dataRow.unitsqtymea = "unitsqtymea_0";
			dataRow.reasoncode = "reasoncode_0";
			dataRow.itempricenetosph = 0;
			dataRow.itempricenetosupplier = 0;
			dataRow.pricedifference = 0;
			dataRow.linesum = 0;
		dataRow = new d_debit_for_b2b_xml();
		data[1] = dataRow;
			dataRow.sender = "sender_1";
			dataRow.receiver = "receiver_1";
			dataRow.doctype = "doctype_1";
			dataRow.aprf = "aprf_1";
			dataRow.snrf = "snrf_1";
			dataRow.ackn = "ackn_1";
			dataRow.testind = "testind_1";
			dataRow.messdate = 1;
			dataRow.messtime = "messtime_1";
			dataRow.invoicetype = "invoicetype_1";
			dataRow.debitinvoiceno = "debitinvoiceno_1";
			dataRow.invoicefunc = "invoicefunc_1";
			dataRow.datetime = 1;
			dataRow.supplierinvoiceno = "supplierinvoiceno_1";
			dataRow.supplierinvoicedate = 1;
			dataRow.referenceno = "referenceno_1";
			dataRow.referencedate = 1;
			dataRow.pricelistno = "pricelistno_1";
			dataRow.pricelistdate = 1;
			dataRow.supplierno = "supplierno_1";
			dataRow.suppliername = "suppliername_1";
			dataRow.storeno = "storeno_1";
			dataRow.storename = "storename_1";
			dataRow.discountpercent = 1;
			dataRow.discountamount = 1;
			dataRow.linessum = 1;
			dataRow.docsum = 1;
			dataRow.invsum = 1;
			dataRow.lineno = 1;
			dataRow.itembarcode = "itembarcode_1";
			dataRow.proddesc = "proddesc_1";
			dataRow.packno = 1;
			dataRow.unitsqty = 1;
			dataRow.unitsqtymea = "unitsqtymea_1";
			dataRow.reasoncode = "reasoncode_1";
			dataRow.itempricenetosph = 1;
			dataRow.itempricenetosupplier = 1;
			dataRow.pricedifference = 1;
			dataRow.linesum = 1;
		dataRow = new d_debit_for_b2b_xml();
		data[2] = dataRow;
			dataRow.sender = "sender_2";
			dataRow.receiver = "receiver_2";
			dataRow.doctype = "doctype_2";
			dataRow.aprf = "aprf_2";
			dataRow.snrf = "snrf_2";
			dataRow.ackn = "ackn_2";
			dataRow.testind = "testind_2";
			dataRow.messdate = 2;
			dataRow.messtime = "messtime_2";
			dataRow.invoicetype = "invoicetype_2";
			dataRow.debitinvoiceno = "debitinvoiceno_2";
			dataRow.invoicefunc = "invoicefunc_2";
			dataRow.datetime = 2;
			dataRow.supplierinvoiceno = "supplierinvoiceno_2";
			dataRow.supplierinvoicedate = 2;
			dataRow.referenceno = "referenceno_2";
			dataRow.referencedate = 2;
			dataRow.pricelistno = "pricelistno_2";
			dataRow.pricelistdate = 2;
			dataRow.supplierno = "supplierno_2";
			dataRow.suppliername = "suppliername_2";
			dataRow.storeno = "storeno_2";
			dataRow.storename = "storename_2";
			dataRow.discountpercent = 2;
			dataRow.discountamount = 2;
			dataRow.linessum = 2;
			dataRow.docsum = 2;
			dataRow.invsum = 2;
			dataRow.lineno = 2;
			dataRow.itembarcode = "itembarcode_2";
			dataRow.proddesc = "proddesc_2";
			dataRow.packno = 2;
			dataRow.unitsqty = 2;
			dataRow.unitsqtymea = "unitsqtymea_2";
			dataRow.reasoncode = "reasoncode_2";
			dataRow.itempricenetosph = 2;
			dataRow.itempricenetosupplier = 2;
			dataRow.pricedifference = 2;
			dataRow.linesum = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_debit_for_b2b_xml model)
        {
            if (model == null)
            {
                return;
            }

            model.sender = GetValue(record, "Sender", Convert.ToString);
            model.receiver = GetValue(record, "Receiver", Convert.ToString);
            model.doctype = GetValue(record, "DocType", Convert.ToString);
            model.aprf = GetValue(record, "APRF", Convert.ToString);
            model.snrf = GetValue(record, "SNRF", Convert.ToString);
            model.ackn = GetValue(record, "Ackn", Convert.ToString);
            model.testind = GetValue(record, "TestInd", Convert.ToString);
            model.messdate = GetValue(record, "MessDate", Convert.ToDouble);
            model.messtime = GetValue(record, "MessTime", Convert.ToString);
            model.invoicetype = GetValue(record, "InvoiceType", Convert.ToString);
            model.debitinvoiceno = GetValue(record, "DebitInvoiceNo", Convert.ToString);
            model.invoicefunc = GetValue(record, "InvoiceFunc", Convert.ToString);
            model.datetime = GetValue(record, "DateTime", Convert.ToInt64);
            model.supplierinvoiceno = GetValue(record, "SupplierInvoiceNo", Convert.ToString);
            model.supplierinvoicedate = GetValue(record, "SupplierInvoiceDate", Convert.ToDouble);
            model.referenceno = GetValue(record, "ReferenceNo", Convert.ToString);
            model.referencedate = GetValue(record, "ReferenceDate", Convert.ToDouble);
            model.pricelistno = GetValue(record, "PriceListNo", Convert.ToString);
            model.pricelistdate = GetValue(record, "PriceListDate", Convert.ToDouble);
            model.supplierno = GetValue(record, "SupplierNo", Convert.ToString);
            model.suppliername = GetValue(record, "SupplierName", Convert.ToString);
            model.storeno = GetValue(record, "StoreNo", Convert.ToString);
            model.storename = GetValue(record, "StoreName", Convert.ToString);
            model.discountpercent = GetValue(record, "DiscountPercent", Convert.ToDecimal);
            model.discountamount = GetValue(record, "DiscountAmount", Convert.ToDecimal);
            model.linessum = GetValue(record, "LinesSum", Convert.ToDecimal);
            model.docsum = GetValue(record, "DocSum", Convert.ToDecimal);
            model.invsum = GetValue(record, "InvSum", Convert.ToDecimal);
            model.lineno = GetValue(record, "LineNo", Convert.ToDouble);
            model.itembarcode = GetValue(record, "ItemBarcode", Convert.ToString);
            model.proddesc = GetValue(record, "ProdDesc", Convert.ToString);
            model.packno = GetValue(record, "PackNo", Convert.ToDouble);
            model.unitsqty = GetValue(record, "UnitsQty", Convert.ToDouble);
            model.unitsqtymea = GetValue(record, "UnitsQtyMea", Convert.ToString);
            model.reasoncode = GetValue(record, "ReasonCode", Convert.ToString);
            model.itempricenetosph = GetValue(record, "ItemPriceBrutoSPH", Convert.ToDecimal);
            model.itempricenetosupplier = GetValue(record, "ItemPriceBrutoSupplier", Convert.ToDecimal);
            model.pricedifference = GetValue(record, "PriceDifference", Convert.ToDecimal);
            model.linesum = GetValue(record, "LineSum", Convert.ToDecimal);
		}

       
    }
}
