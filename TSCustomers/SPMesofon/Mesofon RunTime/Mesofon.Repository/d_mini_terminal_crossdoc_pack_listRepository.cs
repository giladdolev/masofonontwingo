using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_mini_terminal_crossdoc_pack_listRepository : RepositoryBase<d_mini_terminal_crossdoc_pack_list>
    {
        private static readonly string _selectQuery = "SELECT shipment_crossdoc.state, shipment_crossdoc.branch_number AS branch_number, MAX(shipment_crossdoc.last_update_datetime) AS last_update_datetime, MAX(shipment_crossdoc.sub_supplier_number) AS sub_supplier_number, shipment_crossdoc.carton_barcode, MAX(shipment_crossdoc.row_serial_number) AS row_serial_number, shipment_crossdoc.shipment_pallet_number, shipment_crossdoc.marlog_number,MAX(shipment_crossdoc.branch_accept_datetime) AS branch_accept_datetime FROM shipment_crossdoc WHERE (shipment_crossdoc.branch_number = @0) AND (shipment_crossdoc.shipment_pallet_number = @1) AND (shipment_crossdoc.marlog_number = @2) AND (shipment_crossdoc.state IN ('s', 'c', 'f')) GROUP BY branch_number, shipment_pallet_number, carton_barcode, state, marlog_number";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "UPDATE [shipment_crossdoc] SET [state] = @state, [last_update_datetime] = @last_update_datetime WHERE (([branch_number] = @branch_number)) AND (([shipment_pallet_number] = @shipment_pallet_number)) AND (([marlog_number] = @marlog_number)) AND (([carton_barcode] = @carton_barcode))";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_crossdoc_pack_listService"/> class.
        /// </summary>
		public d_mini_terminal_crossdoc_pack_listRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {

        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_mini_terminal_crossdoc_pack_list dataRow = null;
            dataRow = new d_mini_terminal_crossdoc_pack_list();
            data[0] = dataRow;
            dataRow.state = "state_0";
            dataRow.branch_number = 0;
            dataRow.last_update_datetime = new DateTime(1900, 1, 1);
            dataRow.sub_supplier_number = 0;
            dataRow.carton_barcode = "carton_barcode_0";
            dataRow.row_serial_number = 0;
            dataRow.shipment_pallet_number = 0;
            dataRow.marlog_number = 0;
            dataRow.branch_accept_datetime = new DateTime(1900, 1, 1);
            dataRow = new d_mini_terminal_crossdoc_pack_list();
            data[1] = dataRow;
            dataRow.state = "state_1";
            dataRow.branch_number = 1;
            dataRow.last_update_datetime = new DateTime(1901, 2, 2);
            dataRow.sub_supplier_number = 1;
            dataRow.carton_barcode = "carton_barcode_1";
            dataRow.row_serial_number = 1;
            dataRow.shipment_pallet_number = 1;
            dataRow.marlog_number = 1;
            dataRow.branch_accept_datetime = new DateTime(1901, 2, 2);
            dataRow = new d_mini_terminal_crossdoc_pack_list();
            data[2] = dataRow;
            dataRow.state = "state_2";
            dataRow.branch_number = 2;
            dataRow.last_update_datetime = new DateTime(1902, 3, 3);
            dataRow.sub_supplier_number = 2;
            dataRow.carton_barcode = "carton_barcode_2";
            dataRow.row_serial_number = 2;
            dataRow.shipment_pallet_number = 2;
            dataRow.marlog_number = 2;
            dataRow.branch_accept_datetime = new DateTime(1902, 3, 3);
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_crossdoc_pack_list model)
        {
            if (model == null)
            {
                return;
            }

            model.state = GetValue(record, "state", Convert.ToString);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            model.sub_supplier_number = GetValue(record, "sub_supplier_number", Convert.ToInt64);
            model.carton_barcode = GetValue(record, "carton_barcode", Convert.ToString);
            model.row_serial_number = GetValue(record, "row_serial_number", Convert.ToInt64);
            model.shipment_pallet_number = GetValue(record, "shipment_pallet_number", Convert.ToDouble);
            model.marlog_number = GetValue(record, "marlog_number", Convert.ToInt64);
            model.branch_accept_datetime = GetDateTime(record, "branch_accept_datetime");
        }
    }
}
