using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_pallet_invoicesRepository : RepositoryBase<d_mini_terminal_pallet_invoices>
    {
		private static readonly string _selectQuery = "SELECT shipment_pallets.serial_number, shipment_pallets.date_move, shipment_pallets.state, shipment_pallets.branch_number, shipment_pallets.supplier_number, shipment_pallets.invoice_number, shipment_pallets.order_number, shipment_pallets.row_serial_number, shipment_pallets.shipment_number, shipment_pallets.pallet_number, shipment_pallets.pack_quantity, shipment_pallets.arrive_datetime, shipment_pallets.approve_employee, shipment_pallets.auto_scan_status, shipment_pallets.document_type, 0 AS create_mode FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.shipment_number = @1) AND (shipment_pallets.pallet_number = @2 OR @2 = '') AND (shipment_pallets.marlog_number = @3) AND (shipment_pallets.state = 'a')";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_pallet_invoicesService"/> class.
        /// </summary>
		public d_mini_terminal_pallet_invoicesRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_pallet_invoices dataRow = null;
		dataRow = new d_mini_terminal_pallet_invoices();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.state = "state_0";
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.invoice_number = 0;
			dataRow.order_number = 0;
			dataRow.row_serial_number = 0;
			dataRow.shipment_number = 0;
			dataRow.pallet_number = "pallet_number_0";
			dataRow.pack_quantity = 0;
			dataRow.arrive_datetime = new DateTime(1900,1,1);
			dataRow.approve_employee = 0;
			dataRow.auto_scan_status = 0;
			dataRow.document_type = 0;
			dataRow.create_mode = "create_mode_0";
		dataRow = new d_mini_terminal_pallet_invoices();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.state = "state_1";
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.invoice_number = 1;
			dataRow.order_number = 1;
			dataRow.row_serial_number = 1;
			dataRow.shipment_number = 1;
			dataRow.pallet_number = "pallet_number_1";
			dataRow.pack_quantity = 1;
			dataRow.arrive_datetime = new DateTime(1901,2,2);
			dataRow.approve_employee = 1;
			dataRow.auto_scan_status = 1;
			dataRow.document_type = 1;
			dataRow.create_mode = "create_mode_1";
		dataRow = new d_mini_terminal_pallet_invoices();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.invoice_number = 2;
			dataRow.order_number = 2;
			dataRow.row_serial_number = 2;
			dataRow.shipment_number = 2;
			dataRow.pallet_number = "pallet_number_2";
			dataRow.pack_quantity = 2;
			dataRow.arrive_datetime = new DateTime(1902,3,3);
			dataRow.approve_employee = 2;
			dataRow.auto_scan_status = 2;
			dataRow.document_type = 2;
			dataRow.create_mode = "create_mode_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_pallet_invoices model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToDouble);
            model.date_move = GetDateTime(record, "date_move");
            model.state = GetValue(record, "state", Convert.ToString);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.order_number = GetValue(record, "order_number", Convert.ToDouble);
            model.row_serial_number = GetValue(record, "row_serial_number", Convert.ToInt64);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.pallet_number = GetValue(record, "pallet_number", Convert.ToString);
            model.pack_quantity = GetValue(record, "pack_quantity", Convert.ToInt64);
            model.arrive_datetime = GetDateTime(record, "arrive_datetime");
            model.approve_employee = GetValue(record, "approve_employee", Convert.ToInt64);
            model.auto_scan_status = GetValue(record, "auto_scan_status", Convert.ToInt64);
            model.document_type = GetValue(record, "document_type", Convert.ToInt64);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
		}
    }
}
