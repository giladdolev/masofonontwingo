using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_mini_terminal_header_invRepository : RepositoryBase<d_mini_terminal_header_inv>
    {
		private static readonly string _selectQuery = "SELECT 'I' AS doc_type, invoice_move.invoice_number, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_move.employee_number, invoice_move.supply_date, invoice_move.date_move, invoice_move.discount, invoice_move.mam, invoice_move.station_num, invoice_move.store_number, invoice_move.stock_number, invoice_move.invoice_total, invoice_move.log_book, invoice_move.state, invoice_move.external_account_tx, invoice_move.external_tx_number, invoice_move.expected_total_amount, invoice_move.discount_percent, invoice_move.price_list_number, invoice_move.create_mode, invoice_move.van_sale, invoice_move.decline_reason, invoice_move.b2b_status FROM invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.supplier_number = @1 AND invoice_move.invoice_type = 'p'";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_header_invService"/> class.
        /// </summary>
		public d_mini_terminal_header_invRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("I", "�");
            codeTable.Add("P", "�");
            LookupTables.Add("doc_type", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_header_inv dataRow = null;
		dataRow = new d_mini_terminal_header_inv();
		data[0] = dataRow;
			dataRow.doc_type = "doc_type_0";
			dataRow.invoice_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.invoice_type = "invoice_type_0";
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.discount = 0;
			dataRow.mam = 0;
			dataRow.station_num = 0;
			dataRow.store_number = 0;
			dataRow.stock_number = 0;
			dataRow.invoice_total = 0;
			dataRow.log_book = 0;
			dataRow.state = "state_0";
			dataRow.external_account_tx = 0;
			dataRow.external_tx_number = 0;
			dataRow.expected_total_amount = 0;
			dataRow.discount_percent = 0;
			dataRow.price_list_number = 0;
			dataRow.create_mode = "create_mode_0";
			dataRow.van_sale = 0;
			dataRow.decline_reason = 0;
			dataRow.b2b_status = 0;
		dataRow = new d_mini_terminal_header_inv();
		data[1] = dataRow;
			dataRow.doc_type = "doc_type_1";
			dataRow.invoice_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.invoice_type = "invoice_type_1";
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.discount = 1;
			dataRow.mam = 1;
			dataRow.station_num = 1;
			dataRow.store_number = 1;
			dataRow.stock_number = 1;
			dataRow.invoice_total = 1;
			dataRow.log_book = 1;
			dataRow.state = "state_1";
			dataRow.external_account_tx = 1;
			dataRow.external_tx_number = 1;
			dataRow.expected_total_amount = 1;
			dataRow.discount_percent = 1;
			dataRow.price_list_number = 1;
			dataRow.create_mode = "create_mode_1";
			dataRow.van_sale = 1;
			dataRow.decline_reason = 1;
			dataRow.b2b_status = 1;
		dataRow = new d_mini_terminal_header_inv();
		data[2] = dataRow;
			dataRow.doc_type = "doc_type_2";
			dataRow.invoice_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.invoice_type = "invoice_type_2";
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.discount = 2;
			dataRow.mam = 2;
			dataRow.station_num = 2;
			dataRow.store_number = 2;
			dataRow.stock_number = 2;
			dataRow.invoice_total = 2;
			dataRow.log_book = 2;
			dataRow.state = "state_2";
			dataRow.external_account_tx = 2;
			dataRow.external_tx_number = 2;
			dataRow.expected_total_amount = 2;
			dataRow.discount_percent = 2;
			dataRow.price_list_number = 2;
			dataRow.create_mode = "create_mode_2";
			dataRow.van_sale = 2;
			dataRow.decline_reason = 2;
			dataRow.b2b_status = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_header_inv model)
        {
            if (model == null)
            {
                return;
            }

            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.date_move = GetDateTime(record, "date_move");
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.station_num = GetValue(record, "station_num", Convert.ToInt64);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.invoice_total = GetValue(record, "invoice_total", Convert.ToDecimal);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.external_account_tx = GetValue(record, "external_account_tx", Convert.ToInt64);
            model.external_tx_number = GetValue(record, "external_tx_number", Convert.ToInt64);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.price_list_number = GetValue(record, "price_list_number", Convert.ToInt64);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
            model.van_sale = GetValue(record, "van_sale", Convert.ToInt64);
            model.decline_reason = GetValue(record, "decline_reason", Convert.ToInt64);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
		}
    }
}
