using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_summary_returnsRepository : RepositoryBase<d_summary_returns>
    {
		private static readonly string _selectQuery = "SELECT shipment_marlog_return.branch_number, shipment_marlog_return.invoice_number AS doc_no, shipment_marlog_return.supplier_number, SUM(shipment_marlog_return.material_quantity) AS TOTAL FROM shipment_marlog_return WHERE shipment_marlog_return.branch_number = @0 AND shipment_marlog_return.shipment_number = @1 AND shipment_marlog_return.marlog_number = @2 AND shipment_marlog_return.state <> 'd' GROUP BY shipment_marlog_return.branch_number, shipment_marlog_return.supplier_number, shipment_marlog_return.invoice_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_summary_returnsService"/> class.
        /// </summary>
		public d_summary_returnsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_summary_returns dataRow = null;
		dataRow = new d_summary_returns();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.supplier_number = 0;
			dataRow.total = 0;
		dataRow = new d_summary_returns();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.supplier_number = 1;
			dataRow.total = 1;
		dataRow = new d_summary_returns();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.supplier_number = 2;
			dataRow.total = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_summary_returns model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToDouble);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.total = GetValue(record, "TOTAL", Convert.ToInt64);
		}
    }
}
