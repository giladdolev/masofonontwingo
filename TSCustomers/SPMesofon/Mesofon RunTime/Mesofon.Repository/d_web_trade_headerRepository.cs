using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_web_trade_headerRepository : RepositoryBase<d_web_trade_header>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_web_trade_headerService"/> class.
        /// </summary>
		public d_web_trade_headerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_web_trade_header dataRow = null;
		dataRow = new d_web_trade_header();
		data[0] = dataRow;
			dataRow.statusname = "statusname_0";
			dataRow.statuscode = "statuscode_0";
			dataRow.statusdatetime = 0;
			dataRow.orderorgno = "orderorgno_0";
			dataRow.orderdate = 0;
		dataRow = new d_web_trade_header();
		data[1] = dataRow;
			dataRow.statusname = "statusname_1";
			dataRow.statuscode = "statuscode_1";
			dataRow.statusdatetime = 1;
			dataRow.orderorgno = "orderorgno_1";
			dataRow.orderdate = 1;
		dataRow = new d_web_trade_header();
		data[2] = dataRow;
			dataRow.statusname = "statusname_2";
			dataRow.statuscode = "statuscode_2";
			dataRow.statusdatetime = 2;
			dataRow.orderorgno = "orderorgno_2";
			dataRow.orderdate = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_web_trade_header model)
        {
            if (model == null)
            {
                return;
            }

            model.statusname = GetValue(record, "StatusName", Convert.ToString);
            model.statuscode = GetValue(record, "StatusCode", Convert.ToString);
            model.statusdatetime = GetValue(record, "StatusDateTime", Convert.ToInt64);
            model.orderorgno = GetValue(record, "OrderOrgNo", Convert.ToString);
            model.orderdate = GetValue(record, "OrderDate", Convert.ToDouble);
		}
    }
}
