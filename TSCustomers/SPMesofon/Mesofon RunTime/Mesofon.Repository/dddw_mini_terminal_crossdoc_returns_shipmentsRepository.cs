using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dddw_mini_terminal_crossdoc_returns_shipmentsRepository : RepositoryBase<dddw_mini_terminal_crossdoc_returns_shipments>
    {
		private static readonly string _selectQuery = "SELECT shipment_marlog_return.shipment_number, invoice_number, Max(convert(varchar, shipment_marlog_return.date_move, 103)) AS date_move FROM shipment_marlog_return WHERE (shipment_marlog_return.branch_number = @0) AND (shipment_marlog_return.state <> 'd') AND (shipment_marlog_return.marlog_number = @1) AND (shipment_marlog_return.supplier_number = @2) GROUP BY shipment_marlog_return.shipment_number, invoice_number ORDER BY shipment_marlog_return.shipment_number DESC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dddw_mini_terminal_crossdoc_returns_shipmentsService"/> class.
        /// </summary>
		public dddw_mini_terminal_crossdoc_returns_shipmentsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dddw_mini_terminal_crossdoc_returns_shipments dataRow = null;
		dataRow = new dddw_mini_terminal_crossdoc_returns_shipments();
		data[0] = dataRow;
			dataRow.shipment_number = 0;
			dataRow.invoice_number = 0;
			dataRow.date_move = "date_move_0";
		dataRow = new dddw_mini_terminal_crossdoc_returns_shipments();
		data[1] = dataRow;
			dataRow.shipment_number = 1;
			dataRow.invoice_number = 1;
			dataRow.date_move = "date_move_1";
		dataRow = new dddw_mini_terminal_crossdoc_returns_shipments();
		data[2] = dataRow;
			dataRow.shipment_number = 2;
			dataRow.invoice_number = 2;
			dataRow.date_move = "date_move_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dddw_mini_terminal_crossdoc_returns_shipments model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.date_move = GetValue(record, "date_move", Convert.ToString);
		}
    }
}
