using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_supplier_order_moveRepository : RepositoryBase<d_sql_supplier_order_move>
    {
        private static readonly string _selectQuery = "SELECT supplier_order_move.order_number, supplier_order_move.branch_number, supplier_order_move.supplier_number, supplier_order_move.employee_number, supplier_order_move.supply_date, supplier_order_move.date_move, supplier_order_move.station_num, supplier_order_move.store_number, supplier_order_move.stock_number, supplier_order_move.state, supplier_order_move.expected_total_amount, supplier_order_move.discount, supplier_order_move.discount_percent, supplier_order_move.mam, supplier_order_move.order_total, supplier_order_move.web_number, supplier_order_move.last_month_period, supplier_order_move.last_month_time_back, supplier_order_move.two_months_period, supplier_order_move.two_months_time_back, supplier_order_move.time_period_of_sales, supplier_order_move.order_type, supplier_order_move.calc_avg_period, supplier_order_move.cancel_datetime, supplier_order_move.payment_code_number, supplier_order_move.payments_number FROM supplier_order_move WHERE supplier_order_move.branch_number = @0 AND supplier_order_move.supplier_number = @1 AND supplier_order_move.order_number = @2";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "UPDATE supplier_order_move" +
                    " SET " +
                    "employee_number = @employee_number, " +
                    "supply_date = @supply_date, " +
                    "date_move = @date_move, " +
            "station_num = @station_num, " +
            "store_number = @store_number, " +
            "stock_number = @stock_number, " +
            "state = @state, " +
            "expected_total_amount = @expected_total_amount, " +
            "discount = @discount, " +
            "discount_percent = @discount_percent, " +
            "mam = @mam, " +
            "order_total = @order_total, " +
            "web_number = @web_number, " +
            "last_month_period = @last_month_period, " +
            "last_month_time_back = @last_month_time_back, " +
            "two_months_period = @two_months_period, " +
            "two_months_time_back = @two_months_time_back, " +
            "time_period_of_sales = @time_period_of_sales, " +
            "order_type = @order_type, " +
            "calc_avg_period = @calc_avg_period, " +
            "cancel_datetime = @cancel_datetime " +
                    "WHERE branch_number = @branch_number " +
                    "AND supplier_number = @supplier_number " +
                    "AND order_number = @order_number ";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_supplier_order_moveService"/> class.
        /// </summary>
		public d_sql_supplier_order_moveRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_sql_supplier_order_move dataRow = null;
            dataRow = new d_sql_supplier_order_move();
            data[0] = dataRow;
            dataRow.order_number = 0;
            dataRow.branch_number = 0;
            dataRow.supplier_number = 0;
            dataRow.employee_number = 0;
            dataRow.supply_date = new DateTime(1900, 1, 1);
            dataRow.date_move = new DateTime(1900, 1, 1);
            dataRow.station_num = 0;
            dataRow.store_number = 0;
            dataRow.stock_number = 0;
            dataRow.state = "state_0";
            dataRow.expected_total_amount = 0;
            dataRow.discount = 0;
            dataRow.discount_percent = 0;
            dataRow.mam = 0;
            dataRow.order_total = 0;
            dataRow.web_number = 0;
            dataRow.last_month_period = 0;
            dataRow.last_month_time_back = 0;
            dataRow.two_months_period = 0;
            dataRow.two_months_time_back = 0;
            dataRow.time_period_of_sales = 0;
            dataRow.order_type = 0;
            dataRow.calc_avg_period = 0;
            dataRow.cancel_datetime = new DateTime(1900, 1, 1);
            dataRow.payment_code_number = 0;
            dataRow.payments_number = 0;
            dataRow = new d_sql_supplier_order_move();
            data[1] = dataRow;
            dataRow.order_number = 1;
            dataRow.branch_number = 1;
            dataRow.supplier_number = 1;
            dataRow.employee_number = 1;
            dataRow.supply_date = new DateTime(1901, 2, 2);
            dataRow.date_move = new DateTime(1901, 2, 2);
            dataRow.station_num = 1;
            dataRow.store_number = 1;
            dataRow.stock_number = 1;
            dataRow.state = "state_1";
            dataRow.expected_total_amount = 1;
            dataRow.discount = 1;
            dataRow.discount_percent = 1;
            dataRow.mam = 1;
            dataRow.order_total = 1;
            dataRow.web_number = 1;
            dataRow.last_month_period = 1;
            dataRow.last_month_time_back = 1;
            dataRow.two_months_period = 1;
            dataRow.two_months_time_back = 1;
            dataRow.time_period_of_sales = 1;
            dataRow.order_type = 1;
            dataRow.calc_avg_period = 1;
            dataRow.cancel_datetime = new DateTime(1901, 2, 2);
            dataRow.payment_code_number = 1;
            dataRow.payments_number = 1;
            dataRow = new d_sql_supplier_order_move();
            data[2] = dataRow;
            dataRow.order_number = 2;
            dataRow.branch_number = 2;
            dataRow.supplier_number = 2;
            dataRow.employee_number = 2;
            dataRow.supply_date = new DateTime(1902, 3, 3);
            dataRow.date_move = new DateTime(1902, 3, 3);
            dataRow.station_num = 2;
            dataRow.store_number = 2;
            dataRow.stock_number = 2;
            dataRow.state = "state_2";
            dataRow.expected_total_amount = 2;
            dataRow.discount = 2;
            dataRow.discount_percent = 2;
            dataRow.mam = 2;
            dataRow.order_total = 2;
            dataRow.web_number = 2;
            dataRow.last_month_period = 2;
            dataRow.last_month_time_back = 2;
            dataRow.two_months_period = 2;
            dataRow.two_months_time_back = 2;
            dataRow.time_period_of_sales = 2;
            dataRow.order_type = 2;
            dataRow.calc_avg_period = 2;
            dataRow.cancel_datetime = new DateTime(1902, 3, 3);
            dataRow.payment_code_number = 2;
            dataRow.payments_number = 2;
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_supplier_order_move model)
        {
            if (model == null)
            {
                return;
            }

            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.date_move = GetDateTime(record, "date_move");
            model.station_num = GetValue(record, "station_num", Convert.ToInt64);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.order_total = GetValue(record, "order_total", Convert.ToDecimal);
            model.web_number = GetValue(record, "web_number", Convert.ToInt64);
            model.last_month_period = GetValue(record, "last_month_period", Convert.ToInt64);
            model.last_month_time_back = GetValue(record, "last_month_time_back", Convert.ToInt64);
            model.two_months_period = GetValue(record, "two_months_period", Convert.ToInt64);
            model.two_months_time_back = GetValue(record, "two_months_time_back", Convert.ToInt64);
            model.time_period_of_sales = GetValue(record, "time_period_of_sales", Convert.ToInt64);
            model.order_type = GetValue(record, "order_type", Convert.ToInt64);
            model.calc_avg_period = GetValue(record, "calc_avg_period", Convert.ToInt64);
            model.cancel_datetime = GetDateTime(record, "cancel_datetime");
            model.payment_code_number = GetValue(record, "payment_code_number", Convert.ToInt64);
            model.payments_number = GetValue(record, "payments_number", Convert.ToInt64);
        }
    }
}
