using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_driver_report_footer_tempRepository : RepositoryBase<d_driver_report_footer_temp>
    {
		private static readonly string _selectQuery = "SELECT materials.barcode AS item_barcode, IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND status = 'a' AND pallet_number = @0 GROUP BY branch_number, supplier_number, invoice_number, item_number), 0) AS original_quantity, CASE WHEN IsNull(b2b_invoice_details.invoice_number, 0) > 0 THEN IsNull(b2b_invoice_details.scanned_quantity, 0) ELSE IsNull(invoice_details.expected_material_quantity, 0) END AS actual_quantity, convert(varchar, @0, 20) AS display_name, materials.name AS description, invoice_details.invoice_number AS parent_doc_number FROM invoice_move, invoice_details, materials, b2b_invoice_details WHERE invoice_details.branch_number = invoice_move.branch_number AND invoice_details.supplier_number = invoice_move.supplier_number AND invoice_details.invoice_number = invoice_move.invoice_number AND invoice_details.material_number = materials.number AND invoice_move.branch_number = @1 AND invoice_move.supplier_number = @2 AND invoice_move.invoice_number = @3 AND invoice_move.invoice_type = 'p' AND Lower(invoice_move.state) <> 'd' AND b2b_invoice_details.branch_number = invoice_move.branch_number AND b2b_invoice_details.supplier_number = invoice_move.supplier_number AND b2b_invoice_details.invoice_number = invoice_move.invoice_number AND b2b_invoice_details.item_number = invoice_details.material_number AND b2b_invoice_details.order_number = invoice_details.packing_list_number AND b2b_invoice_details.pallet_number = @0 AND b2b_invoice_details.status = 'a' UNION SELECT materials.barcode AS item_barcode, invoice_details.expected_material_quantity AS original_quantity, invoice_details.material_quantity + invoice_details.bonus_quantity AS actual_quantity, convert(varchar, @0, 20) AS display_name, materials.name AS description, invoice_details.invoice_number AS parent_doc_number FROM invoice_move, invoice_details, materials WHERE invoice_details.branch_number = invoice_move.branch_number AND invoice_details.supplier_number = invoice_move.supplier_number AND invoice_details.invoice_number = invoice_move.invoice_number AND invoice_details.material_number = materials.number AND invoice_move.branch_number = @1 AND invoice_move.supplier_number = @2 AND invoice_move.invoice_number = @3 AND invoice_move.invoice_type = 'p' AND Lower(invoice_move.state) <> 'd' AND invoice_details.material_number NOT IN (SELECT item_number FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND order_number = invoice_details.packing_list_number AND status = 'a') ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_driver_report_footer_tempService"/> class.
        /// </summary>
		public d_driver_report_footer_tempRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_driver_report_footer_temp dataRow = null;
		dataRow = new d_driver_report_footer_temp();
		data[0] = dataRow;
			dataRow.item_barcode = "item_barcode_0";
			dataRow.original_quantity = 0;
			dataRow.actual_quantity = 0;
			dataRow.display_name = "display_name_0";
			dataRow.description = "description_0";
			dataRow.parent_doc_number = 0;
			dataRow.rejected_quantity = "rejected_quantity_0";
		dataRow = new d_driver_report_footer_temp();
		data[1] = dataRow;
			dataRow.item_barcode = "item_barcode_1";
			dataRow.original_quantity = 1;
			dataRow.actual_quantity = 1;
			dataRow.display_name = "display_name_1";
			dataRow.description = "description_1";
			dataRow.parent_doc_number = 1;
			dataRow.rejected_quantity = "rejected_quantity_1";
		dataRow = new d_driver_report_footer_temp();
		data[2] = dataRow;
			dataRow.item_barcode = "item_barcode_2";
			dataRow.original_quantity = 2;
			dataRow.actual_quantity = 2;
			dataRow.display_name = "display_name_2";
			dataRow.description = "description_2";
			dataRow.parent_doc_number = 2;
			dataRow.rejected_quantity = "rejected_quantity_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_driver_report_footer_temp model)
        {
            if (model == null)
            {
                return;
            }

            model.item_barcode = GetValue(record, "item_barcode", Convert.ToString);
            model.original_quantity = GetValue(record, "original_quantity", Convert.ToDecimal);
            model.actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.display_name = GetValue(record, "display_name", Convert.ToString);
            model.description = GetValue(record, "description", Convert.ToString);
            model.parent_doc_number = GetValue(record, "parent_doc_number", Convert.ToInt64);
            //model.rejected_quantity = GetValue(record, "", Convert.ToString);
		}
    }
}
