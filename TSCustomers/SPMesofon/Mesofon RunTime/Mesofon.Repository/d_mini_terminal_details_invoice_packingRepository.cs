using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_details_invoice_packingRepository : RepositoryBase<d_mini_terminal_details_invoice_packing>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.serial_number, invoice_details.branch_number, invoice_details.invoice_number AS doc_no, invoice_details.material_number, invoice_details.material_quantity AS invoice_pack_quantity, invoice_details.material_price, invoice_details.details, invoice_details.supplier_number, invoice_details.material_discount_percent, invoice_details.material_price_after_discount, invoice_details.packing_list_number, invoice_details.packing_serial, invoice_details.stock_number, invoice_details.sell_price, invoice_details.bonus_quantity, invoice_details.material_bonus_connect, invoice_details.max_quantity_allowed, invoice_details.last_packing_change, invoice_details.import_type, invoice_details.material_discount_amount, invoice_details.color, invoice_details.bonus_row, invoice_details.pay_shape, invoice_details.indicator, invoice_details.bonus_discount, invoice_details.supplier_discount_percent, invoice_details.current_catalog_sell_price, invoice_details.expiration_date, invoice_details.expected_material_quantity AS actual_quantity, invoice_details.mini_terminal, materials.barcode AS barcode, materials.name AS material_name, materials.min_valid_months, supplier_order_details.material_quantity AS order_quantity, invoice_move.b2b_status AS b2b_status FROM invoice_details, materials, supplier_order_details, invoice_move WHERE invoice_details.packing_list_number = @0 AND invoice_details.supplier_number = @1 AND invoice_details.branch_number = @2 AND invoice_details.material_number = materials.number AND supplier_order_details.order_number = @0 AND supplier_order_details.supplier_number = @1 AND supplier_order_details.branch_number = @2 AND invoice_move.branch_number = @2 AND invoice_move.supplier_number = @1 AND invoice_move.invoice_number = invoice_details.invoice_number AND supplier_order_details.material_number = invoice_details.material_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_details_invoice_packingService"/> class.
        /// </summary>
		public d_mini_terminal_details_invoice_packingRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_details_invoice_packing dataRow = null;
		dataRow = new d_mini_terminal_details_invoice_packing();
		data[0] = dataRow;
			dataRow.invoice_details_serial_number = 0;
			dataRow.invoice_details_branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.invoice_details_material_number = 0;
			dataRow.invoice_pack_quantity = 0;
			dataRow.invoice_details_material_price = 0;
			dataRow.invoice_details_details = "invoice_details_details_0";
			dataRow.invoice_details_supplier_number = 0;
			dataRow.invoice_details_material_discount_percent = 0;
			dataRow.invoice_details_material_price_after_discount = 0;
			dataRow.invoice_details_packing_list_number = 0;
			dataRow.invoice_details_packing_serial = 0;
			dataRow.invoice_details_stock_number = 0;
			dataRow.invoice_details_sell_price = 0;
			dataRow.invoice_details_bonus_quantity = 0;
			dataRow.invoice_details_material_bonus_connect = 0;
			dataRow.invoice_details_max_quantity_allowed = 0;
			dataRow.invoice_details_last_packing_change = 0;
			dataRow.invoice_details_import_type = "invoice_details_import_type_0";
			dataRow.invoice_details_material_discount_amount = 0;
			dataRow.invoice_details_color = 0;
			dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_0";
			dataRow.invoice_details_pay_shape = 0;
			dataRow.invoice_details_indicator = 0;
			dataRow.invoice_details_bonus_discount = 0;
			dataRow.invoice_details_supplier_discount_percent = 0;
			dataRow.invoice_details_current_catalog_sell_price = 0;
			dataRow.invoice_details_expiration_date = new DateTime(1900,1,1);
			dataRow.actual_quantity = 0;
			dataRow.invoice_details_mini_terminal = 0;
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.materials_material_name = "materials_material_name_0";
			dataRow.materials_min_valid_months = 0;
			dataRow.order_quantity = 0;
			dataRow.b2b_status = 0;
			dataRow.compute_1 = "compute_1_0";
		dataRow = new d_mini_terminal_details_invoice_packing();
		data[1] = dataRow;
			dataRow.invoice_details_serial_number = 1;
			dataRow.invoice_details_branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.invoice_details_material_number = 1;
			dataRow.invoice_pack_quantity = 1;
			dataRow.invoice_details_material_price = 1;
			dataRow.invoice_details_details = "invoice_details_details_1";
			dataRow.invoice_details_supplier_number = 1;
			dataRow.invoice_details_material_discount_percent = 1;
			dataRow.invoice_details_material_price_after_discount = 1;
			dataRow.invoice_details_packing_list_number = 1;
			dataRow.invoice_details_packing_serial = 1;
			dataRow.invoice_details_stock_number = 1;
			dataRow.invoice_details_sell_price = 1;
			dataRow.invoice_details_bonus_quantity = 1;
			dataRow.invoice_details_material_bonus_connect = 1;
			dataRow.invoice_details_max_quantity_allowed = 1;
			dataRow.invoice_details_last_packing_change = 1;
			dataRow.invoice_details_import_type = "invoice_details_import_type_1";
			dataRow.invoice_details_material_discount_amount = 1;
			dataRow.invoice_details_color = 1;
			dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_1";
			dataRow.invoice_details_pay_shape = 1;
			dataRow.invoice_details_indicator = 1;
			dataRow.invoice_details_bonus_discount = 1;
			dataRow.invoice_details_supplier_discount_percent = 1;
			dataRow.invoice_details_current_catalog_sell_price = 1;
			dataRow.invoice_details_expiration_date = new DateTime(1901,2,2);
			dataRow.actual_quantity = 1;
			dataRow.invoice_details_mini_terminal = 1;
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.materials_material_name = "materials_material_name_1";
			dataRow.materials_min_valid_months = 1;
			dataRow.order_quantity = 1;
			dataRow.b2b_status = 1;
			dataRow.compute_1 = "compute_1_1";
		dataRow = new d_mini_terminal_details_invoice_packing();
		data[2] = dataRow;
			dataRow.invoice_details_serial_number = 2;
			dataRow.invoice_details_branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.invoice_details_material_number = 2;
			dataRow.invoice_pack_quantity = 2;
			dataRow.invoice_details_material_price = 2;
			dataRow.invoice_details_details = "invoice_details_details_2";
			dataRow.invoice_details_supplier_number = 2;
			dataRow.invoice_details_material_discount_percent = 2;
			dataRow.invoice_details_material_price_after_discount = 2;
			dataRow.invoice_details_packing_list_number = 2;
			dataRow.invoice_details_packing_serial = 2;
			dataRow.invoice_details_stock_number = 2;
			dataRow.invoice_details_sell_price = 2;
			dataRow.invoice_details_bonus_quantity = 2;
			dataRow.invoice_details_material_bonus_connect = 2;
			dataRow.invoice_details_max_quantity_allowed = 2;
			dataRow.invoice_details_last_packing_change = 2;
			dataRow.invoice_details_import_type = "invoice_details_import_type_2";
			dataRow.invoice_details_material_discount_amount = 2;
			dataRow.invoice_details_color = 2;
			dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_2";
			dataRow.invoice_details_pay_shape = 2;
			dataRow.invoice_details_indicator = 2;
			dataRow.invoice_details_bonus_discount = 2;
			dataRow.invoice_details_supplier_discount_percent = 2;
			dataRow.invoice_details_current_catalog_sell_price = 2;
			dataRow.invoice_details_expiration_date = new DateTime(1902,3,3);
			dataRow.actual_quantity = 2;
			dataRow.invoice_details_mini_terminal = 2;
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.materials_material_name = "materials_material_name_2";
			dataRow.materials_min_valid_months = 2;
			dataRow.order_quantity = 2;
			dataRow.b2b_status = 2;
			dataRow.compute_1 = "compute_1_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_details_invoice_packing model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.invoice_details_material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.invoice_pack_quantity = GetValue(record, "invoice_pack_quantity", Convert.ToDecimal);
            model.invoice_details_material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.invoice_details_details = GetValue(record, "details", Convert.ToString);
            model.invoice_details_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_details_material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.invoice_details_material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.invoice_details_packing_list_number = GetValue(record, "packing_list_number", Convert.ToInt64);
            model.invoice_details_packing_serial = GetValue(record, "packing_serial", Convert.ToInt64);
            model.invoice_details_stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.invoice_details_sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.invoice_details_bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.invoice_details_material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.invoice_details_max_quantity_allowed = GetValue(record, "max_quantity_allowed", Convert.ToDecimal);
            model.invoice_details_last_packing_change = GetValue(record, "last_packing_change", Convert.ToDecimal);
            model.invoice_details_import_type = GetValue(record, "import_type", Convert.ToString);
            model.invoice_details_material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.invoice_details_color = GetValue(record, "color", Convert.ToInt64);
            model.invoice_details_bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.invoice_details_pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.invoice_details_indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.invoice_details_bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.invoice_details_supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.invoice_details_current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.invoice_details_expiration_date = GetDateTime(record, "expiration_date");
            model.actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.invoice_details_mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_material_name = GetValue(record, "material_name", Convert.ToString);
            model.materials_min_valid_months = GetValue(record, "min_valid_months", Convert.ToInt64);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.compute_1 = GetValue(record, "", Convert.ToString);
		}
    }
}
