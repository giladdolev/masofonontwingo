using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_summery_invRepository : RepositoryBase<d_summery_inv>
    {
		private static readonly string _selectQuery = "SELECT DISTINCT invoice_details.branch_number, invoice_move.invoice_number AS doc_no, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_details.packing_list_number, SUM(invoice_details.material_quantity) AS TOTAL FROM invoice_details, invoice_move WHERE invoice_move.branch_number = @0 AND invoice_move.supplier_number = @1 AND invoice_move.invoice_type = 'p' AND invoice_details.branch_number = @0 AND invoice_details.supplier_number = @1 AND invoice_details.packing_list_number = @2 AND invoice_move.invoice_number = invoice_details.invoice_number GROUP BY invoice_details.branch_number, invoice_move.invoice_number, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_details.packing_list_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_summery_invService"/> class.
        /// </summary>
		public d_summery_invRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_summery_inv dataRow = null;
		dataRow = new d_summery_inv();
		data[0] = dataRow;
			dataRow.invoice_details_branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.invoice_move_branch_number = 0;
			dataRow.invoice_move_supplier_number = 0;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_0";
			dataRow.invoice_details_packing_list_number = 0;
			dataRow.total = 0;
		dataRow = new d_summery_inv();
		data[1] = dataRow;
			dataRow.invoice_details_branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.invoice_move_branch_number = 1;
			dataRow.invoice_move_supplier_number = 1;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_1";
			dataRow.invoice_details_packing_list_number = 1;
			dataRow.total = 1;
		dataRow = new d_summery_inv();
		data[2] = dataRow;
			dataRow.invoice_details_branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.invoice_move_branch_number = 2;
			dataRow.invoice_move_supplier_number = 2;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_2";
			dataRow.invoice_details_packing_list_number = 2;
			dataRow.total = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_summery_inv model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.invoice_move_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_move_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_move_invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.invoice_details_packing_list_number = GetValue(record, "packing_list_number", Convert.ToInt64);
            model.total = GetValue(record, "TOTAL", Convert.ToDecimal);
		}
    }
}
