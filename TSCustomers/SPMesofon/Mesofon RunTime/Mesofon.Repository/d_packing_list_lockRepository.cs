using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_packing_list_lockRepository : RepositoryBase<d_packing_list_lock>
    {
		private static readonly string _selectQuery = "SELECT packing_list_move.pack_list_number, packing_list_move.branch_number, packing_list_move.supplier_number, packing_list_move.packing_type, packing_list_move.create_mode, packing_list_move.last_update_datetime FROM packing_list_move WHERE branch_number = @0 AND (supplier_number = @1 OR @1 = -1) AND packing_type = @2 AND (pack_list_number = @3 OR @3 = -1) AND (create_mode = @4 OR @4 = '-1')";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[packing_list_move] SET [pack_list_number] = @pack_list_number, [branch_number] = @branch_number, [supplier_number] = @supplier_number, [packing_type] = @packing_type,[create_mode] = @create_mode ,[last_update_datetime] = @last_update_datetime  WHERE ([pack_list_number] = @pack_list_number AND [branch_number] = @branch_number AND [supplier_number] = @supplier_number)";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[packing_list_move] SET [pack_list_number] = @pack_list_number, [branch_number] = @branch_number, [supplier_number] = @supplier_number, [packing_type] = @packing_type,[create_mode] = @create_mode ,[last_update_datetime] = @last_update_datetime  WHERE ([pack_list_number] = @pack_list_number AND [branch_number] = @branch_number AND [supplier_number] = @supplier_number)";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_packing_list_lockService"/> class.
        /// </summary>
		public d_packing_list_lockRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_packing_list_lock dataRow = null;
		dataRow = new d_packing_list_lock();
		data[0] = dataRow;
			dataRow.pack_list_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.packing_type = "packing_type_0";
			dataRow.create_mode = "create_mode_0";
			dataRow.last_update_datetime = new DateTime(1900,1,1);
		dataRow = new d_packing_list_lock();
		data[1] = dataRow;
			dataRow.pack_list_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.packing_type = "packing_type_1";
			dataRow.create_mode = "create_mode_1";
			dataRow.last_update_datetime = new DateTime(1901,2,2);
		dataRow = new d_packing_list_lock();
		data[2] = dataRow;
			dataRow.pack_list_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.packing_type = "packing_type_2";
			dataRow.create_mode = "create_mode_2";
			dataRow.last_update_datetime = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_packing_list_lock model)
        {
            if (model == null)
            {
                return;
            }

            model.pack_list_number = GetValue(record, "pack_list_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.packing_type = GetValue(record, "packing_type", Convert.ToString);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
		}
    }
}
