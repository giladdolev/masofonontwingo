using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_supplier_order_listRepository : RepositoryBase<d_sql_supplier_order_list>
    {
		private static readonly string _selectQuery = "SELECT supplier_order_move.order_number, supplier_order_move.supply_date, supplier_order_move.state, supplier_order_move.distributor_number FROM supplier_order_move WHERE (supplier_order_move.branch_number = @0) AND (supplier_order_move.supplier_number = @1) AND (supplier_order_move.cancel_datetime IS NULL) AND (Lower(state) IN ('c', 'p')) order by supplier_order_move.supply_date DESC, supplier_order_move.order_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_supplier_order_listService"/> class.
        /// </summary>
		public d_sql_supplier_order_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_supplier_order_list dataRow = null;
		dataRow = new d_sql_supplier_order_list();
		data[0] = dataRow;
			dataRow.order_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.state = "state_0";
			dataRow.distributor_number = 0;
		dataRow = new d_sql_supplier_order_list();
		data[1] = dataRow;
			dataRow.order_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.state = "state_1";
			dataRow.distributor_number = 1;
		dataRow = new d_sql_supplier_order_list();
		data[2] = dataRow;
			dataRow.order_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			dataRow.distributor_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_supplier_order_list model)
        {
            if (model == null)
            {
                return;
            }

            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.state = GetValue(record, "state", Convert.ToString);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
		}
    }
}
