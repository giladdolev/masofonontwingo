using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_return_details_listRepository : RepositoryBase<d_mini_terminal_return_details_list>
    {
		private static readonly string _selectQuery = "SELECT invd.serial_number, invd.branch_number, invd.invoice_number, invd.material_number, invd.material_quantity, invd.supplier_number, invd.decline_number, invd.material_price, invd.material_price_after_discount, mt.name, mt.barcode, invm.date_move FROM invoice_details AS invd, invoice_move AS invm, materials AS mt WHERE invm.branch_number = invd.branch_number AND invm.invoice_number = invd.invoice_number AND invm.supplier_number = invd.supplier_number AND invd.branch_number = @0 AND invd.invoice_number = @1 AND invd.supplier_number = @2 AND mt.number = invd.material_number AND invm.invoice_type = @3 AND (invd.material_quantity <> 0 AND invd.material_quantity <> NULL)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_return_details_listService"/> class.
        /// </summary>
		public d_mini_terminal_return_details_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_return_details_list dataRow = null;
		dataRow = new d_mini_terminal_return_details_list();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.branch_number = 0;
			dataRow.invoice_number = 0;
			dataRow.material_number = 0;
			dataRow.material_quantity = 0;
			dataRow.supplier_number = 0;
			dataRow.decline_number = 0;
			dataRow.material_price = 0;
			dataRow.material_price_after_discount = 0;
			dataRow.name = "name_0";
			dataRow.barcode = "barcode_0";
			dataRow.date_move = new DateTime(1900,1,1);
		dataRow = new d_mini_terminal_return_details_list();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.branch_number = 1;
			dataRow.invoice_number = 1;
			dataRow.material_number = 1;
			dataRow.material_quantity = 1;
			dataRow.supplier_number = 1;
			dataRow.decline_number = 1;
			dataRow.material_price = 1;
			dataRow.material_price_after_discount = 1;
			dataRow.name = "name_1";
			dataRow.barcode = "barcode_1";
			dataRow.date_move = new DateTime(1901,2,2);
		dataRow = new d_mini_terminal_return_details_list();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.branch_number = 2;
			dataRow.invoice_number = 2;
			dataRow.material_number = 2;
			dataRow.material_quantity = 2;
			dataRow.supplier_number = 2;
			dataRow.decline_number = 2;
			dataRow.material_price = 2;
			dataRow.material_price_after_discount = 2;
			dataRow.name = "name_2";
			dataRow.barcode = "barcode_2";
			dataRow.date_move = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_return_details_list model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.decline_number = GetValue(record, "decline_number", Convert.ToInt64);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.name = GetValue(record, "name", Convert.ToString);
            model.barcode = GetValue(record, "barcode", Convert.ToString);
            model.date_move = GetDateTime(record, "date_move");
		}
    }
}
