using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_crossdoc_returns_headerRepository : RepositoryBase<d_mini_terminal_crossdoc_returns_header>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_crossdoc_returns_headerService"/> class.
        /// </summary>
		public d_mini_terminal_crossdoc_returns_headerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_crossdoc_returns_header dataRow = null;
		dataRow = new d_mini_terminal_crossdoc_returns_header();
		data[0] = dataRow;
			dataRow.supplier = 0;
			dataRow.distributor = 0;
		dataRow = new d_mini_terminal_crossdoc_returns_header();
		data[1] = dataRow;
			dataRow.supplier = 1;
			dataRow.distributor = 1;
		dataRow = new d_mini_terminal_crossdoc_returns_header();
		data[2] = dataRow;
			dataRow.supplier = 2;
			dataRow.distributor = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_crossdoc_returns_header model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier = GetValue(record, "supplier", Convert.ToDouble);
            model.distributor = GetValue(record, "distributor", Convert.ToDouble);
		}
    }
}
