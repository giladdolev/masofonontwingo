﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Repository
{
    public class d_sql_material_suppliers_connectRepository:RepositoryBase<d_sql_material_suppliers_connect>
    {
        private static readonly string _selectQuery = "";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_material_suppliers_connectRepository"/> class.
        /// </summary>
		public d_sql_material_suppliers_connectRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }
      

        public override void Map(IDataRecord record, d_sql_material_suppliers_connect model)
        {
        }

        protected override ModelBase[] GetMockData()
        {
            return null;
        }
    }
}
