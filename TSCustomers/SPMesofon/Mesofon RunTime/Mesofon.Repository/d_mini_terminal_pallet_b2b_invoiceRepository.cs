using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_pallet_b2b_invoiceRepository : RepositoryBase<d_mini_terminal_pallet_b2b_invoice>
    {
        private static readonly string _selectQuery = "SELECT b2b_invoice_details.branch_number, b2b_invoice_details.supplier_number, b2b_invoice_details.order_number, b2b_invoice_details.invoice_number, b2b_invoice_details.serial_number, b2b_invoice_details.line_number, b2b_invoice_details.upd_date_time, b2b_invoice_details.status, b2b_invoice_details.item_barcode, b2b_invoice_details.item_number, b2b_invoice_details.units_qty, b2b_invoice_details.item_bonus_code, b2b_invoice_details.bonus_line, b2b_invoice_details.bonus_description, b2b_invoice_details.packages_quantity, b2b_invoice_details.validity_date, b2b_invoice_details.pack_quantity, b2b_invoice_details.pallet_number, b2b_invoice_move.invoice_type, IsNull(b2b_invoice_details.scanned_quantity, 0) AS scanned_quantity FROM b2b_invoice_details, b2b_invoice_move WHERE (b2b_invoice_details.branch_number = b2b_invoice_move.branch_number) AND (b2b_invoice_details.supplier_number = b2b_invoice_move.supplier_number) AND (b2b_invoice_details.invoice_number = b2b_invoice_move.invoice_number) AND (b2b_invoice_details.status = 'a') AND (b2b_invoice_move.branch_number = @0) AND (b2b_invoice_move.supplier_number = @1) AND (b2b_invoice_move.invoice_number = @2) AND (b2b_invoice_details.pallet_number = @3) AND (b2b_invoice_move.shipment_number = @4)";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "UPDATE b2b_invoice_details" +
                    " SET scanned_quantity = @b2b_invoice_details_scanned_quantity " +
                    " WHERE   branch_number = @b2b_invoice_details_branch_number AND " +
                    "supplier_number = @b2b_invoice_details_supplier_number AND " +
                    "order_number = @b2b_invoice_details_order_number AND " +
                    "invoice_number = @b2b_invoice_details_invoice_number AND " +
                    "serial_number = @b2b_invoice_details_serial_number AND " +
                    "line_number = @b2b_invoice_details_line_number AND " +
                    "item_number = @b2b_invoice_details_item_number AND " +
                    "pallet_number = @b2b_invoice_details_pallet_number ";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_pallet_b2b_invoiceService"/> class.
        /// </summary>
		public d_mini_terminal_pallet_b2b_invoiceRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_mini_terminal_pallet_b2b_invoice dataRow = null;
            dataRow = new d_mini_terminal_pallet_b2b_invoice();
            data[0] = dataRow;
            dataRow.b2b_invoice_details_branch_number = 0;
            dataRow.b2b_invoice_details_supplier_number = 0;
            dataRow.b2b_invoice_details_order_number = 0;
            dataRow.b2b_invoice_details_invoice_number = 0;
            dataRow.b2b_invoice_details_serial_number = 0;
            dataRow.b2b_invoice_details_line_number = 0;
            dataRow.b2b_invoice_details_upd_date_time = new DateTime(1900, 1, 1);
            dataRow.b2b_invoice_details_status = "b2b_invoice_details_status_0";
            dataRow.b2b_invoice_details_item_barcode = "b2b_invoice_details_item_barcode_0";
            dataRow.b2b_invoice_details_item_number = 0;
            dataRow.b2b_invoice_details_units_qty = 0;
            dataRow.b2b_invoice_details_item_bonus_code = "b2b_invoice_details_item_bonus_code_0";
            dataRow.b2b_invoice_details_bonus_line = 0;
            dataRow.b2b_invoice_details_bonus_description = "b2b_invoice_details_bonus_description_0";
            dataRow.b2b_invoice_details_packages_quantity = 0;
            dataRow.b2b_invoice_details_validity_date = new DateTime(1900, 1, 1);
            dataRow.b2b_invoice_details_pack_quantity = 0;
            dataRow.b2b_invoice_details_pallet_number = "b2b_invoice_details_pallet_number_0";
            dataRow.b2b_invoice_move_invoice_type = "b2b_invoice_move_invoice_type_0";
            dataRow.b2b_invoice_details_scanned_quantity = 0;
            dataRow = new d_mini_terminal_pallet_b2b_invoice();
            data[1] = dataRow;
            dataRow.b2b_invoice_details_branch_number = 1;
            dataRow.b2b_invoice_details_supplier_number = 1;
            dataRow.b2b_invoice_details_order_number = 1;
            dataRow.b2b_invoice_details_invoice_number = 1;
            dataRow.b2b_invoice_details_serial_number = 1;
            dataRow.b2b_invoice_details_line_number = 1;
            dataRow.b2b_invoice_details_upd_date_time = new DateTime(1901, 2, 2);
            dataRow.b2b_invoice_details_status = "b2b_invoice_details_status_1";
            dataRow.b2b_invoice_details_item_barcode = "b2b_invoice_details_item_barcode_1";
            dataRow.b2b_invoice_details_item_number = 1;
            dataRow.b2b_invoice_details_units_qty = 1;
            dataRow.b2b_invoice_details_item_bonus_code = "b2b_invoice_details_item_bonus_code_1";
            dataRow.b2b_invoice_details_bonus_line = 1;
            dataRow.b2b_invoice_details_bonus_description = "b2b_invoice_details_bonus_description_1";
            dataRow.b2b_invoice_details_packages_quantity = 1;
            dataRow.b2b_invoice_details_validity_date = new DateTime(1901, 2, 2);
            dataRow.b2b_invoice_details_pack_quantity = 1;
            dataRow.b2b_invoice_details_pallet_number = "b2b_invoice_details_pallet_number_1";
            dataRow.b2b_invoice_move_invoice_type = "b2b_invoice_move_invoice_type_1";
            dataRow.b2b_invoice_details_scanned_quantity = 1;
            dataRow = new d_mini_terminal_pallet_b2b_invoice();
            data[2] = dataRow;
            dataRow.b2b_invoice_details_branch_number = 2;
            dataRow.b2b_invoice_details_supplier_number = 2;
            dataRow.b2b_invoice_details_order_number = 2;
            dataRow.b2b_invoice_details_invoice_number = 2;
            dataRow.b2b_invoice_details_serial_number = 2;
            dataRow.b2b_invoice_details_line_number = 2;
            dataRow.b2b_invoice_details_upd_date_time = new DateTime(1902, 3, 3);
            dataRow.b2b_invoice_details_status = "b2b_invoice_details_status_2";
            dataRow.b2b_invoice_details_item_barcode = "b2b_invoice_details_item_barcode_2";
            dataRow.b2b_invoice_details_item_number = 2;
            dataRow.b2b_invoice_details_units_qty = 2;
            dataRow.b2b_invoice_details_item_bonus_code = "b2b_invoice_details_item_bonus_code_2";
            dataRow.b2b_invoice_details_bonus_line = 2;
            dataRow.b2b_invoice_details_bonus_description = "b2b_invoice_details_bonus_description_2";
            dataRow.b2b_invoice_details_packages_quantity = 2;
            dataRow.b2b_invoice_details_validity_date = new DateTime(1902, 3, 3);
            dataRow.b2b_invoice_details_pack_quantity = 2;
            dataRow.b2b_invoice_details_pallet_number = "b2b_invoice_details_pallet_number_2";
            dataRow.b2b_invoice_move_invoice_type = "b2b_invoice_move_invoice_type_2";
            dataRow.b2b_invoice_details_scanned_quantity = 2;
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_pallet_b2b_invoice model)
        {
            if (model == null)
            {
                return;
            }

            model.b2b_invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.b2b_invoice_details_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.b2b_invoice_details_order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.b2b_invoice_details_invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.b2b_invoice_details_serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.b2b_invoice_details_line_number = GetValue(record, "line_number", Convert.ToInt64);
            model.b2b_invoice_details_upd_date_time = GetDateTime(record, "upd_date_time");
            model.b2b_invoice_details_status = GetValue(record, "status", Convert.ToString);
            model.b2b_invoice_details_item_barcode = GetValue(record, "item_barcode", Convert.ToString);
            model.b2b_invoice_details_item_number = GetValue(record, "item_number", Convert.ToInt64);
            model.b2b_invoice_details_units_qty = GetValue(record, "units_qty", Convert.ToDecimal);
            model.b2b_invoice_details_item_bonus_code = GetValue(record, "item_bonus_code", Convert.ToString);
            model.b2b_invoice_details_bonus_line = GetValue(record, "bonus_line", Convert.ToInt64);
            model.b2b_invoice_details_bonus_description = GetValue(record, "bonus_description", Convert.ToString);
            model.b2b_invoice_details_packages_quantity = GetValue(record, "packages_quantity", Convert.ToDecimal);
            model.b2b_invoice_details_validity_date = GetDateTime(record, "validity_date");
            model.b2b_invoice_details_pack_quantity = GetValue(record, "pack_quantity", Convert.ToDouble);
            model.b2b_invoice_details_pallet_number = GetValue(record, "pallet_number", Convert.ToString);
            model.b2b_invoice_move_invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.b2b_invoice_details_scanned_quantity = GetValue(record, "scanned_quantity", Convert.ToDecimal);
        }
    }
}
