using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dwh_invoice_print2_oldRepository : RepositoryBase<dwh_invoice_print2_old>
    {
		private static readonly string _selectQuery = "SELECT DISTINCT materials.barcode, materials.name, invoice_details.material_price, invoice_details.material_quantity, invoice_move.discount, invoice_move.mam, invoice_details.material_discount_percent, invoice_details.material_price_after_discount, invoice_move.invoice_number, invoice_move.supplier_number, invoice_move.employee_number, invoice_move.supply_date, IsNull(invoice_move.state, 'o'), (SELECT employees.name FROM employees WHERE employees.branch_number = @0 AND employees.number = invoice_move.employee_number) AS employee_name, suppliers.name, vat_types.percentage, invoice_details.serial_number, invoice_details.color, invoice_details.bonus_quantity, invoice_details.material_bonus_connect, invoice_details.import_type, invoice_details.sell_price, invoice_move.discount_percent, global_parameters.branch_name, convert(varchar(255), @1), invoice_move.log_book, (SELECT catalog_entities.entity_number FROM catalog_entities, items_entities_connections WHERE catalog_entities.serial_number = items_entities_connections.entity_number AND items_entities_connections.item_number = items.number AND catalog_entities.entity_type_number = 10) AS entity_number, invoice_move.date_move, (SELECT MAX(packing_list_number) FROM invoice_details WHERE branch_number = @0 AND supplier_number = @2 AND invoice_number = @3) AS order_number, (SELECT date_move FROM supplier_order_move WHERE branch_number = @0 AND supplier_number = @2 AND order_number = (SELECT MAX(packing_list_number) FROM invoice_details WHERE branch_number = @0 AND supplier_number = @2 AND invoice_number = @3)) AS order_date, IsNull(invoice_details.bonus_discount, 0), invoice_move.invoice_type, (SELECT campaign_items.campaign_new_price FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaign_items.campaign_number = (SELECT MAX(campaigns.serial_number) FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaigns.campaign_status = 'a') AND (campaign_items.campaign_new_price IS NOT NULL) AND (campaigns.branches_numbers = '0' OR campaigns.branches_numbers LIKE ',' + convert(varchar(3), @0) + ',')))) AS campaign_new_price, (SELECT campaigns.campaign_name FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaign_items.campaign_number = (SELECT MAX(campaigns.serial_number) FROM campaign_items, campaigns WHERE (campaign_items.item_number = items.number) AND (campaign_items.campaign_number = campaigns.serial_number) AND (campaigns.campaign_status = 'a') AND (campaign_items.campaign_new_price IS NOT NULL) AND (campaigns.branches_numbers = '0' OR campaigns.branches_numbers LIKE ',' + convert(varchar(3), @0) + ',')))) AS campaign_name, invoice_details.indicator, 0 AS flag_color, IsNull((SELECT 1 FROM branch_item_price AS b WHERE (b.sell_price <> invoice_details.sell_price) AND (b.branch_number = @0) AND (b.item_number = items.number)), 0) AS flag_color_1, IsNull((SELECT 1 FROM material_suppliers AS b WHERE (b.price_before_discount <> invoice_details.material_price) AND (b.branch_number = @0) AND (b.supplier_number = @2) AND (b.material_number = invoice_details.material_number) AND (b.last_supplier_price = 1)), 0) AS flag_color_2, IsNull((SELECT 1 FROM material_suppliers AS b WHERE (b.price_after_discount <> invoice_details.material_price_after_discount) AND (b.branch_number = @0) AND (b.supplier_number = @2) AND (b.material_number = invoice_details.material_number) AND (b.last_supplier_price = 1)), 0) AS flag_color_3, global_parameters.vat_number, global_parameters.phone_number, global_parameters.fax_number, invoice_move.expected_total_amount, invoice_details.supplier_discount_percent, 0 AS long_barcode_ind, 0 AS introduction_campaign, invoice_move.distributor_number, (SELECT suppliers.name FROM suppliers WHERE number = invoice_move.distributor_number) AS distributor_name FROM invoice_details, materials, invoice_move, suppliers, vat_types, global_parameters, items, trees WHERE (invoice_details.branch_number = @0) AND (invoice_move.branch_number = @0) AND (global_parameters.serial_number = @0) AND (materials.number = invoice_details.material_number) AND (invoice_details.invoice_number = invoice_move.invoice_number) AND (invoice_move.supplier_number = invoice_details.supplier_number) AND (suppliers.number = invoice_move.supplier_number) AND (materials.vat_group_number = vat_types.serial_number) AND (vat_types.branch_number = @0) AND (invoice_details.material_number = materials.number) AND (trees.material_number = materials.number) AND (items.number = trees.item_number) AND ((invoice_details.supplier_number = @2) AND (invoice_details.invoice_number = @3)) AND (invoice_details.material_quantity + invoice_details.bonus_quantity <> 0) ORDER BY invoice_details.indicator ASC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dwh_invoice_print2_oldService"/> class.
        /// </summary>
		public dwh_invoice_print2_oldRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dwh_invoice_print2_old dataRow = null;
		dataRow = new dwh_invoice_print2_old();
		data[0] = dataRow;
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.material_name = "material_name_0";
			dataRow.material_price = 0;
			dataRow.material_quantity = 0;
			dataRow.invoice_move_discount = 0;
			dataRow.mam = 0;
			dataRow.material_discount_percent = 0;
			dataRow.material_new_price = 0;
			dataRow.invoice_number = 0;
			dataRow.supplier_number = 0;
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.compute_0013 = "compute_0013_0";
			dataRow.employee_name = "employee_name_0";
			dataRow.supplier_name = "supplier_name_0";
			dataRow.vat_types_percentage = 0;
			dataRow.serial_number = 0;
			dataRow.color = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_bonus_connect = 0;
			dataRow.import_type = "import_type_0";
			dataRow.sell_price = 0;
			dataRow.discount_percent = 0;
			dataRow.branch_name = "branch_name_0";
			dataRow.logo = "logo_0";
			dataRow.log_book = 0;
			dataRow.catalog_entities_entity_number = 0;
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.order_number = 0;
			dataRow.order_date = new DateTime(1900,1,1);
			dataRow.bonus_discount = 0;
			dataRow.invoice_type = "invoice_type_0";
			dataRow.campaign_new_price = 0;
			dataRow.campaign_name = "campaign_name_0";
			dataRow.invoice_details_indicator = 0;
			dataRow.flag_color = 0;
			dataRow.flag_color_1 = 0;
			dataRow.flag_color_2 = 0;
			dataRow.flag_color_3 = 0;
			dataRow.vat_number = "vat_number_0";
			dataRow.phone_number = "phone_number_0";
			dataRow.fax_number = "fax_number_0";
			dataRow.expected_total_amount = 0;
			dataRow.invoice_details_supplier_discount_percent = 0;
			dataRow.long_barcode_ind = 0;
			dataRow.introduction_campaign = 0;
			dataRow.invoice_move_distributor_number = 0;
			dataRow.distributor_name = "distributor_name_0";
			dataRow.state = "state_0";
			dataRow.compute_gp_1 = "compute_gp_1_0";
			dataRow.compute_gp = "compute_gp_0";
			dataRow.materials_name_1 = "materials_name_1_0";
			dataRow.material_value = "material_value_0";
			dataRow.comp_new_price = "comp_new_price_0";
			dataRow.compute_6 = "compute_6_0";
			dataRow.material_new_price1 = "material_new_price1_0";
			dataRow.long_barcode = "long_barcode_0";
			dataRow.line_number = "line_number_0";
			dataRow.compute_3 = "compute_3_0";
			dataRow.total_quantity = "total_quantity_0";
			dataRow.compute_2 = "compute_2_0";
			dataRow.compute_7 = "compute_7_0";
			dataRow.total_cf = "total_cf_0";
			dataRow.invoice_total_cf = "invoice_total_cf_0";
			dataRow.compute_vat = "compute_vat_0";
			dataRow.mam_total_cf = "mam_total_cf_0";
			dataRow.compute_total = "compute_total_0";
			dataRow.compute_4 = "compute_4_0";
			dataRow.total_vat = "total_vat_0";
			dataRow.compute_5 = "compute_5_0";
			dataRow.compute_1 = "compute_1_0";
			dataRow.compute_8 = "compute_8_0";
		dataRow = new dwh_invoice_print2_old();
		data[1] = dataRow;
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.material_name = "material_name_1";
			dataRow.material_price = 1;
			dataRow.material_quantity = 1;
			dataRow.invoice_move_discount = 1;
			dataRow.mam = 1;
			dataRow.material_discount_percent = 1;
			dataRow.material_new_price = 1;
			dataRow.invoice_number = 1;
			dataRow.supplier_number = 1;
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.compute_0013 = "compute_0013_1";
			dataRow.employee_name = "employee_name_1";
			dataRow.supplier_name = "supplier_name_1";
			dataRow.vat_types_percentage = 1;
			dataRow.serial_number = 1;
			dataRow.color = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_bonus_connect = 1;
			dataRow.import_type = "import_type_1";
			dataRow.sell_price = 1;
			dataRow.discount_percent = 1;
			dataRow.branch_name = "branch_name_1";
			dataRow.logo = "logo_1";
			dataRow.log_book = 1;
			dataRow.catalog_entities_entity_number = 1;
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.order_number = 1;
			dataRow.order_date = new DateTime(1901,2,2);
			dataRow.bonus_discount = 1;
			dataRow.invoice_type = "invoice_type_1";
			dataRow.campaign_new_price = 1;
			dataRow.campaign_name = "campaign_name_1";
			dataRow.invoice_details_indicator = 1;
			dataRow.flag_color = 1;
			dataRow.flag_color_1 = 1;
			dataRow.flag_color_2 = 1;
			dataRow.flag_color_3 = 1;
			dataRow.vat_number = "vat_number_1";
			dataRow.phone_number = "phone_number_1";
			dataRow.fax_number = "fax_number_1";
			dataRow.expected_total_amount = 1;
			dataRow.invoice_details_supplier_discount_percent = 1;
			dataRow.long_barcode_ind = 1;
			dataRow.introduction_campaign = 1;
			dataRow.invoice_move_distributor_number = 1;
			dataRow.distributor_name = "distributor_name_1";
			dataRow.state = "state_1";
			dataRow.compute_gp_1 = "compute_gp_1_1";
			dataRow.compute_gp = "compute_gp_1";
			dataRow.materials_name_1 = "materials_name_1_1";
			dataRow.material_value = "material_value_1";
			dataRow.comp_new_price = "comp_new_price_1";
			dataRow.compute_6 = "compute_6_1";
			dataRow.material_new_price1 = "material_new_price1_1";
			dataRow.long_barcode = "long_barcode_1";
			dataRow.line_number = "line_number_1";
			dataRow.compute_3 = "compute_3_1";
			dataRow.total_quantity = "total_quantity_1";
			dataRow.compute_2 = "compute_2_1";
			dataRow.compute_7 = "compute_7_1";
			dataRow.total_cf = "total_cf_1";
			dataRow.invoice_total_cf = "invoice_total_cf_1";
			dataRow.compute_vat = "compute_vat_1";
			dataRow.mam_total_cf = "mam_total_cf_1";
			dataRow.compute_total = "compute_total_1";
			dataRow.compute_4 = "compute_4_1";
			dataRow.total_vat = "total_vat_1";
			dataRow.compute_5 = "compute_5_1";
			dataRow.compute_1 = "compute_1_1";
			dataRow.compute_8 = "compute_8_1";
		dataRow = new dwh_invoice_print2_old();
		data[2] = dataRow;
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.material_name = "material_name_2";
			dataRow.material_price = 2;
			dataRow.material_quantity = 2;
			dataRow.invoice_move_discount = 2;
			dataRow.mam = 2;
			dataRow.material_discount_percent = 2;
			dataRow.material_new_price = 2;
			dataRow.invoice_number = 2;
			dataRow.supplier_number = 2;
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.compute_0013 = "compute_0013_2";
			dataRow.employee_name = "employee_name_2";
			dataRow.supplier_name = "supplier_name_2";
			dataRow.vat_types_percentage = 2;
			dataRow.serial_number = 2;
			dataRow.color = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_bonus_connect = 2;
			dataRow.import_type = "import_type_2";
			dataRow.sell_price = 2;
			dataRow.discount_percent = 2;
			dataRow.branch_name = "branch_name_2";
			dataRow.logo = "logo_2";
			dataRow.log_book = 2;
			dataRow.catalog_entities_entity_number = 2;
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.order_number = 2;
			dataRow.order_date = new DateTime(1902,3,3);
			dataRow.bonus_discount = 2;
			dataRow.invoice_type = "invoice_type_2";
			dataRow.campaign_new_price = 2;
			dataRow.campaign_name = "campaign_name_2";
			dataRow.invoice_details_indicator = 2;
			dataRow.flag_color = 2;
			dataRow.flag_color_1 = 2;
			dataRow.flag_color_2 = 2;
			dataRow.flag_color_3 = 2;
			dataRow.vat_number = "vat_number_2";
			dataRow.phone_number = "phone_number_2";
			dataRow.fax_number = "fax_number_2";
			dataRow.expected_total_amount = 2;
			dataRow.invoice_details_supplier_discount_percent = 2;
			dataRow.long_barcode_ind = 2;
			dataRow.introduction_campaign = 2;
			dataRow.invoice_move_distributor_number = 2;
			dataRow.distributor_name = "distributor_name_2";
			dataRow.state = "state_2";
			dataRow.compute_gp_1 = "compute_gp_1_2";
			dataRow.compute_gp = "compute_gp_2";
			dataRow.materials_name_1 = "materials_name_1_2";
			dataRow.material_value = "material_value_2";
			dataRow.comp_new_price = "comp_new_price_2";
			dataRow.compute_6 = "compute_6_2";
			dataRow.material_new_price1 = "material_new_price1_2";
			dataRow.long_barcode = "long_barcode_2";
			dataRow.line_number = "line_number_2";
			dataRow.compute_3 = "compute_3_2";
			dataRow.total_quantity = "total_quantity_2";
			dataRow.compute_2 = "compute_2_2";
			dataRow.compute_7 = "compute_7_2";
			dataRow.total_cf = "total_cf_2";
			dataRow.invoice_total_cf = "invoice_total_cf_2";
			dataRow.compute_vat = "compute_vat_2";
			dataRow.mam_total_cf = "mam_total_cf_2";
			dataRow.compute_total = "compute_total_2";
			dataRow.compute_4 = "compute_4_2";
			dataRow.total_vat = "total_vat_2";
			dataRow.compute_5 = "compute_5_2";
			dataRow.compute_1 = "compute_1_2";
			dataRow.compute_8 = "compute_8_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dwh_invoice_print2_old model)
        {
            if (model == null)
            {
                return;
            }

            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.material_name = GetValue(record, "name", Convert.ToString);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.invoice_move_discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_new_price = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.compute_0013 = GetValue(record, "compute_0013", Convert.ToString);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            model.supplier_name = GetValue(record, "suppliers.name", Convert.ToString);
            model.vat_types_percentage = GetValue(record, "percentage", Convert.ToDecimal);
            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.logo = GetValue(record, "compute_0025", Convert.ToString);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.catalog_entities_entity_number = GetValue(record, "entity_number", Convert.ToInt64);
            model.date_move = GetDateTime(record, "date_move");
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.order_date = GetDateTime(record, "order_date");
            model.bonus_discount = GetValue(record, "compute_0031", Convert.ToDecimal);
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.campaign_new_price = GetValue(record, "campaign_new_price", Convert.ToDecimal);
            model.campaign_name = GetValue(record, "campaign_name", Convert.ToString);
            model.invoice_details_indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.flag_color = GetValue(record, "flag_color", Convert.ToInt64);
            model.flag_color_1 = GetValue(record, "flag_color_1", Convert.ToInt64);
            model.flag_color_2 = GetValue(record, "flag_color_2", Convert.ToInt64);
            model.flag_color_3 = GetValue(record, "flag_color_3", Convert.ToInt64);
            model.vat_number = GetValue(record, "vat_number", Convert.ToString);
            model.phone_number = GetValue(record, "phone_number", Convert.ToString);
            model.fax_number = GetValue(record, "fax_number", Convert.ToString);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.invoice_details_supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.long_barcode_ind = GetValue(record, "long_barcode_ind", Convert.ToInt64);
            model.introduction_campaign = GetValue(record, "introduction_campaign", Convert.ToInt64);
            model.invoice_move_distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.distributor_name = GetValue(record, "distributor_name", Convert.ToString);
            model.state = GetValue(record, "", Convert.ToString);
            model.compute_gp_1 = GetValue(record, "", Convert.ToString);
            model.compute_gp = GetValue(record, "", Convert.ToString);
            model.materials_name_1 = GetValue(record, "", Convert.ToString);
            model.material_value = GetValue(record, "", Convert.ToString);
            model.comp_new_price = GetValue(record, "", Convert.ToString);
            model.compute_6 = GetValue(record, "", Convert.ToString);
            model.material_new_price1 = GetValue(record, "", Convert.ToString);
            model.long_barcode = GetValue(record, "", Convert.ToString);
            model.line_number = GetValue(record, "", Convert.ToString);
            model.compute_3 = GetValue(record, "", Convert.ToString);
            model.total_quantity = GetValue(record, "", Convert.ToString);
            model.compute_2 = GetValue(record, "", Convert.ToString);
            model.compute_7 = GetValue(record, "", Convert.ToString);
            model.total_cf = GetValue(record, "", Convert.ToString);
            model.invoice_total_cf = GetValue(record, "", Convert.ToString);
            model.compute_vat = GetValue(record, "", Convert.ToString);
            model.mam_total_cf = GetValue(record, "", Convert.ToString);
            model.compute_total = GetValue(record, "", Convert.ToString);
            model.compute_4 = GetValue(record, "", Convert.ToString);
            model.total_vat = GetValue(record, "", Convert.ToString);
            model.compute_5 = GetValue(record, "", Convert.ToString);
            model.compute_1 = GetValue(record, "", Convert.ToString);
            model.compute_8 = GetValue(record, "", Convert.ToString);
		}
    }
}
