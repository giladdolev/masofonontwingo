using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_invoices_detailsRepository : RepositoryBase<d_invoices_details>
    {
        private static readonly string _selectQuery = "SELECT invoice_details.serial_number, invoice_details.branch_number, invoice_details.invoice_number, invoice_details.material_number AS material_number, CASE WHEN IsNull(invoice_move.b2b_status, 0) = 0 THEN invoice_details.material_quantity + invoice_details.bonus_quantity ELSE IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND order_number = invoice_details.packing_list_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND status = 'a' GROUP BY branch_number, supplier_number, invoice_number, item_number), 0) END AS inv_pack_quantity, CASE WHEN IsNull(invoice_move.b2b_status, 0) = 0 THEN IsNull(invoice_details.material_quantity, 0) ELSE IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND order_number = invoice_details.packing_list_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND status = 'a' AND item_bonus_code IS NULL GROUP BY branch_number, supplier_number, invoice_number, item_number), 0) END AS material_quantity, invoice_details.material_price, invoice_details.details, invoice_details.supplier_number, invoice_details.material_discount_percent, invoice_details.material_price_after_discount, 0 AS quantity_within_invoice, invoice_details.packing_list_number, invoice_details.packing_serial, invoice_details.stock_number, invoice_details.sell_price, CASE WHEN IsNull(invoice_move.b2b_status, 0) = 0 THEN invoice_details.bonus_quantity ELSE IsNull((SELECT SUM(units_qty) FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND order_number = invoice_details.packing_list_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND status = 'a' AND item_bonus_code = '6' GROUP BY branch_number, supplier_number, invoice_number, item_number, item_bonus_code), 0) END AS bonus_quantity, invoice_details.material_bonus_connect, invoice_details.max_quantity_allowed, invoice_details.last_packing_change, invoice_details.import_type, invoice_details.material_discount_amount, invoice_details.bonus_row, invoice_details.color, invoice_details.pay_shape, invoice_details.indicator, invoice_details.bonus_discount, invoice_details.supplier_discount_percent, invoice_details.current_catalog_sell_price, invoice_details.expiration_date, invoice_details.expected_material_quantity, invoice_details.mini_terminal, materials.barcode AS barcode, materials.name AS material_name, materials.min_valid_months, IsNull(supplier_order_details.material_quantity, 0) + IsNull(supplier_order_details.bonus_quantity, 0) AS order_quantity, CASE WHEN EXISTS (SELECT b2b_invoice_details.item_number FROM b2b_invoice_details WHERE branch_number = invoice_details.branch_number AND supplier_number = invoice_details.supplier_number AND order_number = invoice_details.packing_list_number AND invoice_number = invoice_details.invoice_number AND item_number = invoice_details.material_number AND status = 'a') THEN 1 ELSE 0 END AS b2b_status, invoice_move.state AS doc_state, invoice_move.distributor_number, 0 AS row_no, (SELECT count(*) FROM invoice_details AS INV WHERE INV.branch_number = invoice_move.branch_number AND INV.packing_list_number = invoice_move.order_number AND INV.supplier_number = invoice_move.supplier_number AND INV.material_number = invoice_details.material_number AND invoice_move.state = 'O') AS doc_no_en, cng_doc_count, (SELECT count(*) FROM b2b_decline_types, b2b_declines_move WHERE b2b_decline_types.decline_number = b2b_declines_move.decline_number AND b2b_decline_types.decline_level = 1 AND b2b_decline_types.is_active IN (1, 2) AND b2b_decline_types.user_interface = 1 AND b2b_declines_move.branch_number = @0 AND b2b_declines_move.supplier_number = @1 AND b2b_declines_move.order_number = @2 AND b2b_declines_move.parent_doc_type = 'I' AND b2b_declines_move.material_number = invoice_details.material_number AND b2b_declines_move.status = 'a' AND b2b_declines_move.parent_doc_number = invoice_move.invoice_number) AS declines, invoice_details.state, invoice_details.last_update_datetime FROM invoice_move, invoice_details, supplier_order_details, materials WHERE invoice_details.branch_number = invoice_move.branch_number AND invoice_details.supplier_number = invoice_move.supplier_number AND invoice_details.invoice_number = invoice_move.invoice_number AND supplier_order_details.branch_number =* invoice_move.branch_number AND supplier_order_details.supplier_number =* invoice_move.supplier_number AND supplier_order_details.order_number =* invoice_move.order_number AND supplier_order_details.material_number =* invoice_details.material_number AND invoice_details.material_number = materials.number AND invoice_move.branch_number = @0 AND invoice_move.supplier_number = @1 AND invoice_move.order_number = @2 AND invoice_move.invoice_type = 'p' AND Lower(invoice_move.state) <> 'd'";
        private static readonly string _insertQuery = "INSERT INTO [dbo].[invoice_details] (serial_number,branch_number,invoice_number, material_number, material_quantity, material_price,details,supplier_number,material_discount_percent,material_price_after_discount,packing_list_number,packing_serial,stock_number,sell_price,bonus_quantity,material_bonus_connect,max_quantity_allowed,last_packing_change,import_type,material_discount_amount,bonus_row,color,pay_shape,indicator,bonus_discount,supplier_discount_percent,current_catalog_sell_price,expiration_date,expected_material_quantity,mini_terminal,cng_doc_count,state,last_update_datetime) VALUES (@invoice_details_serial_number,@invoice_details_branch_number,@invoice_number,@material_number,@material_quantity,@material_price,@invoice_details_details,@supplier_number,@material_discount_percent,@material_price_after_discount,@packing_list_number,@order_serial,@invoice_details_stock_number,@sell_price,@bonus_quantity,@invoice_details_material_bonus_connect,@invoice_details_max_quantity_allowed,@invoice_details_last_packing_change,@import_type,@invoice_details_material_discount_amount,@invoice_details_bonus_row,@color,@invoice_details_pay_shape,@indicator,@invoice_details_bonus_discount,@invoice_details_supplier_discount_percent,@current_catalog_sell_price, @expiration_date,@expected_material_quantity,@mini_terminal,@cng_doc_count,@state,@last_update_datetime)";
        private static readonly string _updateKeyOnlyQuery = 
            "UPDATE[dbo].[invoice_details] " +
            "SET  " +
            "[invoice_number] = @invoice_number, [material_number] = @material_number, [material_quantity] = @material_quantity, " +
            "[material_price] = @material_price, [details] = @invoice_details_details, " +
            "[supplier_number] = @supplier_number, [material_discount_percent] = @material_discount_percent, " +
            "[material_price_after_discount] = @material_price_after_discount, [packing_list_number] = @packing_list_number,  " +
            "[packing_serial] = @order_serial, [stock_number] = @invoice_details_stock_number,  " +
            "[sell_price] = @sell_price, [bonus_quantity] = @bonus_quantity, [material_bonus_connect] = @invoice_details_material_bonus_connect, " +
            "[max_quantity_allowed] = @invoice_details_max_quantity_allowed, [last_packing_change] = @invoice_details_last_packing_change, [import_type] = @import_type, [material_discount_amount] = @invoice_details_material_discount_amount, [bonus_row] = @invoice_details_bonus_row, [color] = @color, [pay_shape] = @invoice_details_pay_shape, [indicator] = @indicator, [bonus_discount] = @invoice_details_bonus_discount, [supplier_discount_percent] = @invoice_details_supplier_discount_percent, [current_catalog_sell_price] = @current_catalog_sell_price, [expiration_date] = @expiration_date, [expected_material_quantity] = @expected_material_quantity, [mini_terminal] = @mini_terminal, [cng_doc_count] = @cng_doc_count, " +
            "[state] = @state, [last_update_datetime] = @last_update_datetime  " +
            "WHERE(([serial_number] = @invoice_details_serial_number) AND  " +
            "([branch_number] = @invoice_details_branch_number))"; 
        private static readonly string _updateColumnsQuery = "";

        private static readonly string _deleteQuery = "delete from [dbo].[invoice_details] where (([serial_number] = @invoice_details_serial_number) AND  " +
            "([branch_number] = @invoice_details_branch_number))";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_invoices_detailsService"/> class.
        /// </summary>
		public d_invoices_detailsRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_invoices_details dataRow = null;
            dataRow = new d_invoices_details();
            data[0] = dataRow;
            dataRow.invoice_details_serial_number = 0;
            dataRow.invoice_details_branch_number = 0;
            dataRow.invoice_number = 0;
            dataRow.material_number = 0;
            dataRow.inv_pack_quantity = 0;
            dataRow.material_quantity = 0;
            dataRow.material_price = 0;
            dataRow.invoice_details_details = "invoice_details_details_0";
            dataRow.supplier_number = 0;
            dataRow.material_discount_percent = 0;
            dataRow.material_price_after_discount = 0;
            dataRow.quantity_within_invoice = 0;
            dataRow.packing_list_number = 0;
            dataRow.order_serial = 0;
            dataRow.invoice_details_stock_number = 0;
            dataRow.sell_price = 0;
            dataRow.bonus_quantity = 0;
            dataRow.invoice_details_material_bonus_connect = 0;
            dataRow.invoice_details_max_quantity_allowed = 0;
            dataRow.invoice_details_last_packing_change = 0;
            dataRow.import_type = "import_type_0";
            dataRow.invoice_details_material_discount_amount = 0;
            dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_0";
            dataRow.color = 0;
            dataRow.invoice_details_pay_shape = 0;
            dataRow.indicator = 0;
            dataRow.invoice_details_bonus_discount = 0;
            dataRow.invoice_details_supplier_discount_percent = 0;
            dataRow.current_catalog_sell_price = 0;
            dataRow.expiration_date = new DateTime(1900, 1, 1);
            dataRow.expected_material_quantity = 0;
            dataRow.mini_terminal = 0;
            dataRow.materials_barcode = "materials_barcode_0";
            dataRow.materials_material_name = "materials_material_name_0";
            dataRow.materials_min_valid_months = 0;
            dataRow.order_quantity = 0;
            dataRow.b2b_status = 0;
            dataRow.doc_state = "doc_state_0";
            dataRow.distributor_number = 0;
            dataRow.row_no = 0;
            dataRow.doc_no_en = 0;
            dataRow.cng_doc_count = 0;
            dataRow.declines = 0;
            dataRow.state = "state_0";
            dataRow.last_update_datetime = new DateTime(1900, 1, 1);
            dataRow = new d_invoices_details();
            data[1] = dataRow;
            dataRow.invoice_details_serial_number = 1;
            dataRow.invoice_details_branch_number = 1;
            dataRow.invoice_number = 1;
            dataRow.material_number = 1;
            dataRow.inv_pack_quantity = 1;
            dataRow.material_quantity = 1;
            dataRow.material_price = 1;
            dataRow.invoice_details_details = "invoice_details_details_1";
            dataRow.supplier_number = 1;
            dataRow.material_discount_percent = 1;
            dataRow.material_price_after_discount = 1;
            dataRow.quantity_within_invoice = 1;
            dataRow.packing_list_number = 1;
            dataRow.order_serial = 1;
            dataRow.invoice_details_stock_number = 1;
            dataRow.sell_price = 1;
            dataRow.bonus_quantity = 1;
            dataRow.invoice_details_material_bonus_connect = 1;
            dataRow.invoice_details_max_quantity_allowed = 1;
            dataRow.invoice_details_last_packing_change = 1;
            dataRow.import_type = "import_type_1";
            dataRow.invoice_details_material_discount_amount = 1;
            dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_1";
            dataRow.color = 1;
            dataRow.invoice_details_pay_shape = 1;
            dataRow.indicator = 1;
            dataRow.invoice_details_bonus_discount = 1;
            dataRow.invoice_details_supplier_discount_percent = 1;
            dataRow.current_catalog_sell_price = 1;
            dataRow.expiration_date = new DateTime(1901, 2, 2);
            dataRow.expected_material_quantity = 1;
            dataRow.mini_terminal = 1;
            dataRow.materials_barcode = "materials_barcode_1";
            dataRow.materials_material_name = "materials_material_name_1";
            dataRow.materials_min_valid_months = 1;
            dataRow.order_quantity = 1;
            dataRow.b2b_status = 1;
            dataRow.doc_state = "doc_state_1";
            dataRow.distributor_number = 1;
            dataRow.row_no = 1;
            dataRow.doc_no_en = 1;
            dataRow.cng_doc_count = 1;
            dataRow.declines = 1;
            dataRow.state = "state_1";
            dataRow.last_update_datetime = new DateTime(1901, 2, 2);
            dataRow = new d_invoices_details();
            data[2] = dataRow;
            dataRow.invoice_details_serial_number = 2;
            dataRow.invoice_details_branch_number = 2;
            dataRow.invoice_number = 2;
            dataRow.material_number = 2;
            dataRow.inv_pack_quantity = 2;
            dataRow.material_quantity = 2;
            dataRow.material_price = 2;
            dataRow.invoice_details_details = "invoice_details_details_2";
            dataRow.supplier_number = 2;
            dataRow.material_discount_percent = 2;
            dataRow.material_price_after_discount = 2;
            dataRow.quantity_within_invoice = 2;
            dataRow.packing_list_number = 2;
            dataRow.order_serial = 2;
            dataRow.invoice_details_stock_number = 2;
            dataRow.sell_price = 2;
            dataRow.bonus_quantity = 2;
            dataRow.invoice_details_material_bonus_connect = 2;
            dataRow.invoice_details_max_quantity_allowed = 2;
            dataRow.invoice_details_last_packing_change = 2;
            dataRow.import_type = "import_type_2";
            dataRow.invoice_details_material_discount_amount = 2;
            dataRow.invoice_details_bonus_row = "invoice_details_bonus_row_2";
            dataRow.color = 2;
            dataRow.invoice_details_pay_shape = 2;
            dataRow.indicator = 2;
            dataRow.invoice_details_bonus_discount = 2;
            dataRow.invoice_details_supplier_discount_percent = 2;
            dataRow.current_catalog_sell_price = 2;
            dataRow.expiration_date = new DateTime(1902, 3, 3);
            dataRow.expected_material_quantity = 2;
            dataRow.mini_terminal = 2;
            dataRow.materials_barcode = "materials_barcode_2";
            dataRow.materials_material_name = "materials_material_name_2";
            dataRow.materials_min_valid_months = 2;
            dataRow.order_quantity = 2;
            dataRow.b2b_status = 2;
            dataRow.doc_state = "doc_state_2";
            dataRow.distributor_number = 2;
            dataRow.row_no = 2;
            dataRow.doc_no_en = 2;
            dataRow.cng_doc_count = 2;
            dataRow.declines = 2;
            dataRow.state = "state_2";
            dataRow.last_update_datetime = new DateTime(1902, 3, 3);
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_invoices_details model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.inv_pack_quantity = GetValue(record, "inv_pack_quantity", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.invoice_details_details = GetValue(record, "details", Convert.ToString);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.packing_list_number = GetValue(record, "packing_list_number", Convert.ToInt64);
            model.order_serial = GetValue(record, "packing_serial", Convert.ToInt64);
            model.invoice_details_stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.invoice_details_material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.invoice_details_max_quantity_allowed = GetValue(record, "max_quantity_allowed", Convert.ToDecimal);
            model.invoice_details_last_packing_change = GetValue(record, "last_packing_change", Convert.ToDecimal);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.invoice_details_material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.invoice_details_bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.invoice_details_pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.invoice_details_bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.invoice_details_supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.expiration_date = GetDateTime(record, "expiration_date");
            model.expected_material_quantity = GetValue(record, "expected_material_quantity", Convert.ToDecimal);
            model.mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_material_name = GetValue(record, "material_name", Convert.ToString);
            model.materials_min_valid_months = GetValue(record, "min_valid_months", Convert.ToInt64);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.doc_state = GetValue(record, "doc_state", Convert.ToString);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.row_no = GetValue(record, "row_no", Convert.ToInt64);
            model.doc_no_en = GetValue(record, "doc_no_en", Convert.ToInt64);
            model.cng_doc_count = GetValue(record, "cng_doc_count", Convert.ToInt64);
            model.declines = GetValue(record, "declines", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            //model.compute_1 = GetValue(record, "", Convert.ToString);
        }
    }
}
