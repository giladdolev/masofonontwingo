using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_return_detailsRepository : RepositoryBase<d_mini_terminal_return_details>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_return_detailsService"/> class.
        /// </summary>
		public d_mini_terminal_return_detailsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_return_details dataRow = null;
		dataRow = new d_mini_terminal_return_details();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.material_number = 0;
			dataRow.invoice_pack_quantity = 0;
			dataRow.material_quantity = 0;
			dataRow.material_price = 0;
			dataRow.details = "details_0";
			dataRow.supplier_number = 0;
			dataRow.material_discount_percent = 0;
			dataRow.material_price_after_discount = 0;
			dataRow.quantity_within_invoice = 0;
			dataRow.order_number = 0;
			dataRow.order_serial = 0;
			dataRow.stock_number = 0;
			dataRow.sell_price = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_bonus_connect = 0;
			dataRow.max_quantity_allowed = 0;
			dataRow.last_order_change = 0;
			dataRow.import_type = "import_type_0";
			dataRow.material_discount_amount = 0;
			dataRow.bonus_row = "bonus_row_0";
			dataRow.color = 0;
			dataRow.pay_shape = 0;
			dataRow.indicator = 0;
			dataRow.bonus_discount = 0;
			dataRow.supplier_discount_percent = 0;
			dataRow.current_catalog_sell_price = 0;
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.expected_material_quantity = 0;
			dataRow.mini_terminal = 0;
			dataRow.barcode = "barcode_0";
			dataRow.material_name = "material_name_0";
			dataRow.min_valid_months = 0;
			dataRow.order_quantity = 0;
			dataRow.b2b_status = 0;
			dataRow.doc_state = "doc_state_0";
			dataRow.distributor_number = 0;
			dataRow.row_no = 0;
			dataRow.doc_no_en = 0;
			dataRow.cng_doc_count = 0;
			dataRow.declines = 0;
			dataRow.state = "state_0";
			dataRow.last_update_datetime = new DateTime(1900,1,1);
			dataRow.carton_barcode = "carton_barcode_0";
			dataRow.adhoc_serial_number = 0;
			dataRow.adhoc_key = "adhoc_key_0";
			dataRow.row_serial_number = 0;
			dataRow.orig_serial_number = 0;
		dataRow = new d_mini_terminal_return_details();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.material_number = 1;
			dataRow.invoice_pack_quantity = 1;
			dataRow.material_quantity = 1;
			dataRow.material_price = 1;
			dataRow.details = "details_1";
			dataRow.supplier_number = 1;
			dataRow.material_discount_percent = 1;
			dataRow.material_price_after_discount = 1;
			dataRow.quantity_within_invoice = 1;
			dataRow.order_number = 1;
			dataRow.order_serial = 1;
			dataRow.stock_number = 1;
			dataRow.sell_price = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_bonus_connect = 1;
			dataRow.max_quantity_allowed = 1;
			dataRow.last_order_change = 1;
			dataRow.import_type = "import_type_1";
			dataRow.material_discount_amount = 1;
			dataRow.bonus_row = "bonus_row_1";
			dataRow.color = 1;
			dataRow.pay_shape = 1;
			dataRow.indicator = 1;
			dataRow.bonus_discount = 1;
			dataRow.supplier_discount_percent = 1;
			dataRow.current_catalog_sell_price = 1;
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.expected_material_quantity = 1;
			dataRow.mini_terminal = 1;
			dataRow.barcode = "barcode_1";
			dataRow.material_name = "material_name_1";
			dataRow.min_valid_months = 1;
			dataRow.order_quantity = 1;
			dataRow.b2b_status = 1;
			dataRow.doc_state = "doc_state_1";
			dataRow.distributor_number = 1;
			dataRow.row_no = 1;
			dataRow.doc_no_en = 1;
			dataRow.cng_doc_count = 1;
			dataRow.declines = 1;
			dataRow.state = "state_1";
			dataRow.last_update_datetime = new DateTime(1901,2,2);
			dataRow.carton_barcode = "carton_barcode_1";
			dataRow.adhoc_serial_number = 1;
			dataRow.adhoc_key = "adhoc_key_1";
			dataRow.row_serial_number = 1;
			dataRow.orig_serial_number = 1;
		dataRow = new d_mini_terminal_return_details();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.material_number = 2;
			dataRow.invoice_pack_quantity = 2;
			dataRow.material_quantity = 2;
			dataRow.material_price = 2;
			dataRow.details = "details_2";
			dataRow.supplier_number = 2;
			dataRow.material_discount_percent = 2;
			dataRow.material_price_after_discount = 2;
			dataRow.quantity_within_invoice = 2;
			dataRow.order_number = 2;
			dataRow.order_serial = 2;
			dataRow.stock_number = 2;
			dataRow.sell_price = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_bonus_connect = 2;
			dataRow.max_quantity_allowed = 2;
			dataRow.last_order_change = 2;
			dataRow.import_type = "import_type_2";
			dataRow.material_discount_amount = 2;
			dataRow.bonus_row = "bonus_row_2";
			dataRow.color = 2;
			dataRow.pay_shape = 2;
			dataRow.indicator = 2;
			dataRow.bonus_discount = 2;
			dataRow.supplier_discount_percent = 2;
			dataRow.current_catalog_sell_price = 2;
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.expected_material_quantity = 2;
			dataRow.mini_terminal = 2;
			dataRow.barcode = "barcode_2";
			dataRow.material_name = "material_name_2";
			dataRow.min_valid_months = 2;
			dataRow.order_quantity = 2;
			dataRow.b2b_status = 2;
			dataRow.doc_state = "doc_state_2";
			dataRow.distributor_number = 2;
			dataRow.row_no = 2;
			dataRow.doc_no_en = 2;
			dataRow.cng_doc_count = 2;
			dataRow.declines = 2;
			dataRow.state = "state_2";
			dataRow.last_update_datetime = new DateTime(1902,3,3);
			dataRow.carton_barcode = "carton_barcode_2";
			dataRow.adhoc_serial_number = 2;
			dataRow.adhoc_key = "adhoc_key_2";
			dataRow.row_serial_number = 2;
			dataRow.orig_serial_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_return_details model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.invoice_pack_quantity = GetValue(record, "invoice_pack_quantity", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.details = GetValue(record, "details", Convert.ToString);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.order_serial = GetValue(record, "order_serial", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.max_quantity_allowed = GetValue(record, "max_quantity_allowed", Convert.ToDecimal);
            model.last_order_change = GetValue(record, "last_order_change", Convert.ToDecimal);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.date_move = GetDateTime(record, "date_move");
            model.expected_material_quantity = GetValue(record, "expected_material_quantity", Convert.ToDecimal);
            model.mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.barcode = GetValue(record, "barcode", Convert.ToString);
            model.material_name = GetValue(record, "material_name", Convert.ToString);
            model.min_valid_months = GetValue(record, "min_valid_months", Convert.ToInt64);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.doc_state = GetValue(record, "doc_state", Convert.ToString);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.row_no = GetValue(record, "row_no", Convert.ToInt64);
            model.doc_no_en = GetValue(record, "doc_no_en", Convert.ToInt64);
            model.cng_doc_count = GetValue(record, "cng_doc_count", Convert.ToInt64);
            model.declines = GetValue(record, "declines", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            model.carton_barcode = GetValue(record, "carton_barcode", Convert.ToString);
            model.adhoc_serial_number = GetValue(record, "name_1", Convert.ToInt64);
            model.adhoc_key = GetValue(record, "name_2", Convert.ToString);
            model.row_serial_number = GetValue(record, "row_serial_number", Convert.ToInt64);
            model.orig_serial_number = GetValue(record, "orig_serial_number", Convert.ToInt64);
		}
    }
}
