using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_pallet_detailsRepository : RepositoryBase<d_pallet_details>
    {
		private static readonly string _selectQuery = "SELECT b.item_barcode, b.invoice_number, b.units_qty, items.name AS items_name FROM b2b_invoice_details AS b, items WHERE pallet_number = @0 AND branch_number = @1 AND b.item_barcode = items.barcode";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_pallet_detailsService"/> class.
        /// </summary>
		public d_pallet_detailsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_pallet_details dataRow = null;
		dataRow = new d_pallet_details();
		data[0] = dataRow;
			dataRow.item_barcode = "item_barcode_0";
			dataRow.invoice_number = 0;
			dataRow.units_qty = 0;
			dataRow.items_name = "items_name_0";
		dataRow = new d_pallet_details();
		data[1] = dataRow;
			dataRow.item_barcode = "item_barcode_1";
			dataRow.invoice_number = 1;
			dataRow.units_qty = 1;
			dataRow.items_name = "items_name_1";
		dataRow = new d_pallet_details();
		data[2] = dataRow;
			dataRow.item_barcode = "item_barcode_2";
			dataRow.invoice_number = 2;
			dataRow.units_qty = 2;
			dataRow.items_name = "items_name_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_pallet_details model)
        {
            if (model == null)
            {
                return;
            }

            model.item_barcode = GetValue(record, "item_barcode", Convert.ToString);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.units_qty = GetValue(record, "units_qty", Convert.ToDecimal);
            model.items_name = GetValue(record, "items_name", Convert.ToString);
		}
    }
}
