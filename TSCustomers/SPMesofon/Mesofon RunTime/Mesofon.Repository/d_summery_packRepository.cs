using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_summery_packRepository : RepositoryBase<d_summery_pack>
    {
		private static readonly string _selectQuery = "SELECT DISTINCT packing_list_details.branch_number, packing_list_details.pack_list_number AS doc_no, packing_list_move.branch_number, packing_list_move.supplier_number, packing_list_move.packing_type, packing_list_details.order_number, SUM(packing_list_details.material_quantity) AS TOTAL FROM packing_list_details, packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.supplier_number = @1 AND packing_list_move.packing_type = 'p' AND packing_list_details.order_number = @2 AND packing_list_details.branch_number = @0 AND packing_list_details.supplier_number = @1 AND packing_list_details.pack_list_number = packing_list_move.pack_list_number GROUP BY packing_list_details.branch_number, packing_list_details.pack_list_number, packing_list_move.branch_number, packing_list_move.supplier_number, packing_list_move.packing_type, packing_list_details.order_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_summery_packService"/> class.
        /// </summary>
		public d_summery_packRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_summery_pack dataRow = null;
		dataRow = new d_summery_pack();
		data[0] = dataRow;
			dataRow.packing_list_details_branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.packing_list_move_branch_number = 0;
			dataRow.packing_list_move_supplier_number = 0;
			dataRow.packing_list_move_packing_type = "packing_list_move_packing_type_0";
			dataRow.packing_list_details_order_number = 0;
			dataRow.total = 0;
		dataRow = new d_summery_pack();
		data[1] = dataRow;
			dataRow.packing_list_details_branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.packing_list_move_branch_number = 1;
			dataRow.packing_list_move_supplier_number = 1;
			dataRow.packing_list_move_packing_type = "packing_list_move_packing_type_1";
			dataRow.packing_list_details_order_number = 1;
			dataRow.total = 1;
		dataRow = new d_summery_pack();
		data[2] = dataRow;
			dataRow.packing_list_details_branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.packing_list_move_branch_number = 2;
			dataRow.packing_list_move_supplier_number = 2;
			dataRow.packing_list_move_packing_type = "packing_list_move_packing_type_2";
			dataRow.packing_list_details_order_number = 2;
			dataRow.total = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_summery_pack model)
        {
            if (model == null)
            {
                return;
            }

            model.packing_list_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.packing_list_move_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.packing_list_move_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.packing_list_move_packing_type = GetValue(record, "packing_type", Convert.ToString);
            model.packing_list_details_order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.total = GetValue(record, "TOTAL", Convert.ToDecimal);
		}
    }
}
