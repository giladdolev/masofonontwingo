using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_mini_terminal_input_headRepository : RepositoryBase<d_sql_mini_terminal_input_head>
    {
		private static readonly string _selectQuery = "SELECT invoice_move.invoice_number, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_move.employee_number, invoice_move.supply_date, invoice_move.date_move, invoice_move.discount, invoice_move.mam, invoice_move.invoice_total, invoice_move.stock_number, invoice_move.store_number, invoice_move.station_num, invoice_move.log_book, invoice_move.expected_total_amount, invoice_move.discount_percent, invoice_move.state, invoice_move.external_account_tx, invoice_move.external_tx_number, 0 AS order_number, '' AS supplier_name, invoice_move.create_mode, invoice_move.distributor_number FROM invoice_move WHERE invoice_move.invoice_number = @0 AND invoice_move.supplier_number = @1 AND invoice_move.branch_number = @2 AND invoice_move.invoice_type = 'p'";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_mini_terminal_input_headService"/> class.
        /// </summary>
		public d_sql_mini_terminal_input_headRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_mini_terminal_input_head dataRow = null;
		dataRow = new d_sql_mini_terminal_input_head();
		data[0] = dataRow;
			dataRow.invoice_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.invoice_type = "invoice_type_0";
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.discount = 0;
			dataRow.mam = 0;
			dataRow.total = 0;
			dataRow.stock_number = 0;
			dataRow.store_number = 0;
			dataRow.station_number = 0;
			dataRow.log_book = 0;
			dataRow.expected_total_amount = 0;
			dataRow.discount_percent = 0;
			dataRow.state = "state_0";
			dataRow.external_account_tx = 0;
			dataRow.external_tx_number = 0;
			dataRow.order_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.create_mode = "create_mode_0";
			dataRow.distributor_number = 0;
		dataRow = new d_sql_mini_terminal_input_head();
		data[1] = dataRow;
			dataRow.invoice_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.invoice_type = "invoice_type_1";
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.discount = 1;
			dataRow.mam = 1;
			dataRow.total = 1;
			dataRow.stock_number = 1;
			dataRow.store_number = 1;
			dataRow.station_number = 1;
			dataRow.log_book = 1;
			dataRow.expected_total_amount = 1;
			dataRow.discount_percent = 1;
			dataRow.state = "state_1";
			dataRow.external_account_tx = 1;
			dataRow.external_tx_number = 1;
			dataRow.order_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.create_mode = "create_mode_1";
			dataRow.distributor_number = 1;
		dataRow = new d_sql_mini_terminal_input_head();
		data[2] = dataRow;
			dataRow.invoice_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.invoice_type = "invoice_type_2";
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.discount = 2;
			dataRow.mam = 2;
			dataRow.total = 2;
			dataRow.stock_number = 2;
			dataRow.store_number = 2;
			dataRow.station_number = 2;
			dataRow.log_book = 2;
			dataRow.expected_total_amount = 2;
			dataRow.discount_percent = 2;
			dataRow.state = "state_2";
			dataRow.external_account_tx = 2;
			dataRow.external_tx_number = 2;
			dataRow.order_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.create_mode = "create_mode_2";
			dataRow.distributor_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_mini_terminal_input_head model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.date_move = GetDateTime(record, "date_move");
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.total = GetValue(record, "invoice_total", Convert.ToDecimal);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.station_number = GetValue(record, "station_num", Convert.ToInt64);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.state = GetValue(record, "state", Convert.ToString);
            model.external_account_tx = GetValue(record, "external_account_tx", Convert.ToInt64);
            model.external_tx_number = GetValue(record, "external_tx_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "supplier_name", Convert.ToString);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
		}
    }
}
