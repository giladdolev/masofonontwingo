using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dwh_invoice_marlogRepository : RepositoryBase<dwh_invoice_marlog>
    {
		private static readonly string _selectQuery = "SELECT 99 AS distributer_number, shipment_number, MAX(date_move) AS move_date, state FROM shipment_marlog_return WHERE branch_number = @0 AND shipment_number = @1 GROUP BY shipment_number, state";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dwh_invoice_marlogService"/> class.
        /// </summary>
		public dwh_invoice_marlogRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dwh_invoice_marlog dataRow = null;
		dataRow = new dwh_invoice_marlog();
		data[0] = dataRow;
			dataRow.distributor_number = 0;
			dataRow.shipment_number = 0;
			dataRow.move_date = new DateTime(1900,1,1);
			dataRow.state = "state_0";
		dataRow = new dwh_invoice_marlog();
		data[1] = dataRow;
			dataRow.distributor_number = 1;
			dataRow.shipment_number = 1;
			dataRow.move_date = new DateTime(1901,2,2);
			dataRow.state = "state_1";
		dataRow = new dwh_invoice_marlog();
		data[2] = dataRow;
			dataRow.distributor_number = 2;
			dataRow.shipment_number = 2;
			dataRow.move_date = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dwh_invoice_marlog model)
        {
            if (model == null)
            {
                return;
            }

            model.distributor_number = GetValue(record, "distributer_number", Convert.ToInt64);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.move_date = GetDateTime(record, "move_date");
            model.state = GetValue(record, "state", Convert.ToString);
		}
    }
}
