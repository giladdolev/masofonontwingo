using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_web_trade_for_b2b_xmlRepository : RepositoryBase<d_web_trade_for_b2b_xml>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_web_trade_for_b2b_xmlService"/> class.
        /// </summary>
		public d_web_trade_for_b2b_xmlRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_web_trade_for_b2b_xml dataRow = null;
		dataRow = new d_web_trade_for_b2b_xml();
		data[0] = dataRow;
			dataRow.sender = "sender_0";
			dataRow.receiver = "receiver_0";
			dataRow.doctype = "doctype_0";
			dataRow.aprf = "aprf_0";
			dataRow.snrf = "snrf_0";
			dataRow.ackn = "ackn_0";
			dataRow.testind = "testind_0";
			dataRow.messdate = 0;
			dataRow.messtime = "messtime_0";
			dataRow.statusname = "statusname_0";
			dataRow.statuscode = "statuscode_0";
			dataRow.statusdatetime = 0;
			dataRow.orderorgno = "orderorgno_0";
			dataRow.orderdate = 0;
		dataRow = new d_web_trade_for_b2b_xml();
		data[1] = dataRow;
			dataRow.sender = "sender_1";
			dataRow.receiver = "receiver_1";
			dataRow.doctype = "doctype_1";
			dataRow.aprf = "aprf_1";
			dataRow.snrf = "snrf_1";
			dataRow.ackn = "ackn_1";
			dataRow.testind = "testind_1";
			dataRow.messdate = 1;
			dataRow.messtime = "messtime_1";
			dataRow.statusname = "statusname_1";
			dataRow.statuscode = "statuscode_1";
			dataRow.statusdatetime = 1;
			dataRow.orderorgno = "orderorgno_1";
			dataRow.orderdate = 1;
		dataRow = new d_web_trade_for_b2b_xml();
		data[2] = dataRow;
			dataRow.sender = "sender_2";
			dataRow.receiver = "receiver_2";
			dataRow.doctype = "doctype_2";
			dataRow.aprf = "aprf_2";
			dataRow.snrf = "snrf_2";
			dataRow.ackn = "ackn_2";
			dataRow.testind = "testind_2";
			dataRow.messdate = 2;
			dataRow.messtime = "messtime_2";
			dataRow.statusname = "statusname_2";
			dataRow.statuscode = "statuscode_2";
			dataRow.statusdatetime = 2;
			dataRow.orderorgno = "orderorgno_2";
			dataRow.orderdate = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_web_trade_for_b2b_xml model)
        {
            if (model == null)
            {
                return;
            }

            model.sender = GetValue(record, "Sender", Convert.ToString);
            model.receiver = GetValue(record, "Receiver", Convert.ToString);
            model.doctype = GetValue(record, "DocType", Convert.ToString);
            model.aprf = GetValue(record, "APRF", Convert.ToString);
            model.snrf = GetValue(record, "SNRF", Convert.ToString);
            model.ackn = GetValue(record, "Ackn", Convert.ToString);
            model.testind = GetValue(record, "TestInd", Convert.ToString);
            model.messdate = GetValue(record, "MessDate", Convert.ToDouble);
            model.messtime = GetValue(record, "MessTime", Convert.ToString);
            model.statusname = GetValue(record, "StatusName", Convert.ToString);
            model.statuscode = GetValue(record, "StatusCode", Convert.ToString);
            model.statusdatetime = GetValue(record, "StatusDateTime", Convert.ToInt64);
            model.orderorgno = GetValue(record, "OrderOrgNo", Convert.ToString);
            model.orderdate = GetValue(record, "OrderDate", Convert.ToDouble);
		}

        public string XMLExport
        {
            get
            {
                return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><OrderXml><Envelope __pbband=\"detail\"><Sender>sender</Sender><Receiver>receiver</Receiver><DocType>doctype</DocType><APRF>aprf</APRF><SNRF>snrf</SNRF><Ackn>ackn</Ackn><TestInd>testind</TestInd><MessDate>messdate</MessDate><MessTime>messtime</MessTime><Header><StatusName>statusname</StatusName><StatusCode>statuscode</StatusCode><StatusDateTime>statusdatetime</StatusDateTime><OrderOrgNo>orderorgno</OrderOrgNo><OrderDate>orderdate</OrderDate></Header></Envelope></OrderXml>";
            }
        }
    }
}
