using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_marlog_return_cartonsRepository : RepositoryBase<d_marlog_return_cartons>
    {
		private static readonly string _selectQuery = "SELECT shipment_marlog_return.branch_number, shipment_marlog_return.shipment_number, shipment_marlog_return.carton_barcode, SUM(shipment_marlog_return.material_quantity) AS material_quantity, shipment_marlog_return.state, MAX(shipment_marlog_return.last_update_datetime) AS last_update_datetime, shipment_marlog_return.marlog_number FROM shipment_marlog_return WHERE (shipment_marlog_return.branch_number = @0) AND (shipment_marlog_return.shipment_number = @1) AND (shipment_marlog_return.marlog_number = @2) AND (shipment_marlog_return.state <> 'd') GROUP BY shipment_marlog_return.branch_number, shipment_marlog_return.shipment_number, shipment_marlog_return.marlog_number, shipment_marlog_return.carton_barcode, shipment_marlog_return.state";
		private static readonly string _insertQuery = "insert into shipment_marlog_return (branch_number,shipment_number,carton_barcode,material_quantity,state,marlog_number) values(@branch_number,@shipment_number,@carton_barcode,@material_quantity,@state,@marlog_number)";
        private static readonly string _updateKeyOnlyQuery = "UPDATE[shipment_marlog_return] SET[state] = @state, [last_update_datetime] = @last_update_datetime WHERE([branch_number] = @branch_number) AND([shipment_number] = @shipment_number)  AND([marlog_number] = @marlog_number) AND state <> 'd'";
        private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";
        
        /// <summary>
        /// Initializes a new instance of the <see cref="d_marlog_return_cartonsService"/> class.
        /// </summary>
		public d_marlog_return_cartonsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("n", "����");
            codeTable.Add("a", "����");
            codeTable.Add("c", "����");
            LookupTables.Add("state", codeTable);
        }
    

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_marlog_return_cartons dataRow = null;
		dataRow = new d_marlog_return_cartons();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.shipment_number = 0;
			dataRow.carton_barcode = "carton_barcode_0";
			dataRow.material_quantity = 0;
			dataRow.state = "state_0";
			dataRow.last_update_datetime = new DateTime(1900,1,1);
			dataRow.marlog_number = 0;
		dataRow = new d_marlog_return_cartons();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.shipment_number = 1;
			dataRow.carton_barcode = "carton_barcode_1";
			dataRow.material_quantity = 1;
			dataRow.state = "state_1";
			dataRow.last_update_datetime = new DateTime(1901,2,2);
			dataRow.marlog_number = 1;
		dataRow = new d_marlog_return_cartons();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.shipment_number = 2;
			dataRow.carton_barcode = "carton_barcode_2";
			dataRow.material_quantity = 2;
			dataRow.state = "state_2";
			dataRow.last_update_datetime = new DateTime(1902,3,3);
			dataRow.marlog_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_marlog_return_cartons model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.carton_barcode = GetValue(record, "carton_barcode", Convert.ToString);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            model.marlog_number = GetValue(record, "marlog_number", Convert.ToInt64);
		}
    }
}
