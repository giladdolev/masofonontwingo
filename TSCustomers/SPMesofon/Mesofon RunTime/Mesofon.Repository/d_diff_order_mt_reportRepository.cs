using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_diff_order_mt_reportRepository : RepositoryBase<d_diff_order_mt_report>
    {
		private static readonly string _selectQuery = "SELECT supplier_order_details.branch_number, supplier_order_details.supplier_number, supplier_order_details.order_number, supplier_order_details.material_number, IsNull(supplier_order_details.material_quantity, 0) AS order_quantity, supplier_order_details.quantity_within_invoice, materials.name, materials.barcode FROM supplier_order_details, materials WHERE (supplier_order_details.branch_number = @0) AND (supplier_order_details.supplier_number = @1) AND (supplier_order_details.order_number = @2) AND (supplier_order_details.material_number = materials.number) AND (IsNull(supplier_order_details.quantity_within_invoice, 0) = 0)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_diff_order_mt_reportService"/> class.
        /// </summary>
		public d_diff_order_mt_reportRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_diff_order_mt_report dataRow = null;
		dataRow = new d_diff_order_mt_report();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.order_number = 0;
			dataRow.material_number = 0;
			dataRow.order_quantity = 0;
			dataRow.quantity_within_invoice = 0;
			dataRow.name = "name_0";
			dataRow.barcode = "barcode_0";
			dataRow.row = "row_0";
		dataRow = new d_diff_order_mt_report();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.order_number = 1;
			dataRow.material_number = 1;
			dataRow.order_quantity = 1;
			dataRow.quantity_within_invoice = 1;
			dataRow.name = "name_1";
			dataRow.barcode = "barcode_1";
			dataRow.row = "row_1";
		dataRow = new d_diff_order_mt_report();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.order_number = 2;
			dataRow.material_number = 2;
			dataRow.order_quantity = 2;
			dataRow.quantity_within_invoice = 2;
			dataRow.name = "name_2";
			dataRow.barcode = "barcode_2";
			dataRow.row = "row_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_diff_order_mt_report model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.order_quantity_t = string.Format("{0:#.000}", GetValue(record, "order_quantity", Convert.ToDecimal));
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.name = GetValue(record, "name", Convert.ToString);
            model.barcode = GetValue(record, "barcode", Convert.ToString);
            //model.row = GetValue(record, "", Convert.ToString);
		}
    }
}
