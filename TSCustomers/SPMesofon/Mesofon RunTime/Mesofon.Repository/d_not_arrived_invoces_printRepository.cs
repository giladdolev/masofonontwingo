using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_not_arrived_invoces_printRepository : RepositoryBase<d_not_arrived_invoces_print>
    {
		private static readonly string _selectQuery = "SELECT invoice_move.branch_number, invoice_move.invoice_number, invoice_move.supplier_number, invoice_move.date_move, invoice_move.order_number, invoice_move.distributor_number, invoice_details.material_quantity, 0 AS items_quantity FROM invoice_move, invoice_details WHERE (invoice_move.branch_number = invoice_details.branch_number) AND (invoice_move.invoice_number = invoice_details.invoice_number) AND (invoice_move.supplier_number = invoice_details.supplier_number) AND (invoice_move.branch_number = @0) AND (invoice_move.supplier_number = @1) AND (invoice_move.invoice_number = @2)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_not_arrived_invoces_printService"/> class.
        /// </summary>
		public d_not_arrived_invoces_printRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_not_arrived_invoces_print dataRow = null;
		dataRow = new d_not_arrived_invoces_print();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.invoice_number = 0;
			dataRow.supplier_number = 0;
			dataRow.date_move = new DateTime(1900,1,1).ToShortDateString();
			dataRow.order_number = 0;
			dataRow.distributor_number = 0;
			dataRow.material_quantity = 0;
			dataRow.items_quantity = 0;
			dataRow.row = "row_0";
		dataRow = new d_not_arrived_invoces_print();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.invoice_number = 1;
			dataRow.supplier_number = 1;
			dataRow.date_move = new DateTime(1901,2,2).ToShortDateString();
			dataRow.order_number = 1;
			dataRow.distributor_number = 1;
			dataRow.material_quantity = 1;
			dataRow.items_quantity = 1;
			dataRow.row = "row_1";
		dataRow = new d_not_arrived_invoces_print();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.invoice_number = 2;
			dataRow.supplier_number = 2;
			dataRow.date_move = new DateTime(1902,3,3).ToShortDateString();
			dataRow.order_number = 2;
			dataRow.distributor_number = 2;
			dataRow.material_quantity = 2;
			dataRow.items_quantity = 2;
			dataRow.row = "row_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_not_arrived_invoces_print model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.date_move = GetDateTime(record, "date_move")?.ToString("dd/MM/yyyy");
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.items_quantity = GetValue(record, "items_quantity", Convert.ToInt64);
            //model.row = GetValue(record, "", Convert.ToString);
		}
    }
}
