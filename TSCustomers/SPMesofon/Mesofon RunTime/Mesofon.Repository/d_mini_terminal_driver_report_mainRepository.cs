using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_driver_report_mainRepository : RepositoryBase<d_mini_terminal_driver_report_main>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_driver_report_mainService"/> class.
        /// </summary>
		public d_mini_terminal_driver_report_mainRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_driver_report_main dataRow = null;
		dataRow = new d_mini_terminal_driver_report_main();
		data[0] = dataRow;
			dataRow.a = "a_0";
			dataRow.page_1 = "page_1_0";
			dataRow.page_2 = "page_2_0";
		dataRow = new d_mini_terminal_driver_report_main();
		data[1] = dataRow;
			dataRow.a = "a_1";
			dataRow.page_1 = "page_1_1";
			dataRow.page_2 = "page_2_1";
		dataRow = new d_mini_terminal_driver_report_main();
		data[2] = dataRow;
			dataRow.a = "a_2";
			dataRow.page_1 = "page_1_2";
			dataRow.page_2 = "page_2_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_driver_report_main model)
        {
            if (model == null)
            {
                return;
            }

            model.a = GetValue(record, "a", Convert.ToString);
            model.page_1 = GetValue(record, "", Convert.ToString);
            model.page_2 = GetValue(record, "", Convert.ToString);
		}
    }
}
