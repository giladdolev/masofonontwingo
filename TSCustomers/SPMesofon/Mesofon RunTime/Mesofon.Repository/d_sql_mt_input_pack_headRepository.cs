using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_mt_input_pack_headRepository : RepositoryBase<d_sql_mt_input_pack_head>
    {
		private static readonly string _selectQuery = "SELECT packing_list_move.pack_list_number, packing_list_move.branch_number, packing_list_move.supplier_number, packing_list_move.packing_type, packing_list_move.employee_number, packing_list_move.supply_date, packing_list_move.date_move, packing_list_move.discount, packing_list_move.mam, packing_list_move.station_num, packing_list_move.pack_list_total, packing_list_move.store_number, packing_list_move.stock_number, packing_list_move.log_book, packing_list_move.state, packing_list_move.expected_total_amount, packing_list_move.discount_percent, 0 AS order_number, 0 AS packing_details_serial_number, space(100) AS supplier_name, packing_list_move.payment_code_number, packing_list_move.payments_number FROM packing_list_move WHERE packing_list_move.pack_list_number = @0 AND packing_list_move.supplier_number = @1 AND packing_list_move.branch_number = @2 AND packing_list_move.packing_type = 'p'";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[packing_list_move] ([pack_list_number], [branch_number], [supplier_number], [packing_type], [employee_number], [supply_date], [date_move], [discount], [mam], [station_num], [pack_list_total], [store_number], [stock_number], [log_book], [state], [expected_total_amount], [discount_percent], [payment_code_number], [payments_number]) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18, @p19)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[packing_list_move] SET [pack_list_number] = @p1, [branch_number] = @p2, [supplier_number] = @p3, [packing_type] = @p4, [employee_number] = @p5, [supply_date] = @p6, [date_move] = @p7, [discount] = @p8, [mam] = @p9, [station_num] = @p10, [pack_list_total] = @p11, [store_number] = @p12, [stock_number] = @p13, [log_book] = @p14, [state] = @p15, [expected_total_amount] = @p16, [discount_percent] = @p17, [payment_code_number] = @p18, [payments_number] = @p19 WHERE (([pack_list_number] = @p20) AND ([branch_number] = @p21) AND ([supplier_number] = @p22) AND ([packing_type] = @p23))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[packing_list_move] SET [pack_list_number] = @p1, [branch_number] = @p2, [supplier_number] = @p3, [packing_type] = @p4, [employee_number] = @p5, [supply_date] = @p6, [date_move] = @p7, [discount] = @p8, [mam] = @p9, [station_num] = @p10, [pack_list_total] = @p11, [store_number] = @p12, [stock_number] = @p13, [log_book] = @p14, [state] = @p15, [expected_total_amount] = @p16, [discount_percent] = @p17, [payment_code_number] = @p18, [payments_number] = @p19 WHERE (([pack_list_number] = @p20) AND ([branch_number] = @p21) AND ([supplier_number] = @p22) AND ([packing_type] = @p23) AND ([employee_number] = @p24) AND ([supply_date] = @p25) AND ([date_move] = @p26) AND ([discount] = @p27) AND ([mam] = @p28) AND ([station_num] = @p29) AND ([pack_list_total] = @p30) AND ([store_number] = @p31) AND ([stock_number] = @p32) AND ((@p33 = 1 AND [log_book] IS NULL) OR ([log_book] = @p34)) AND ((@p35 = 1 AND [state] IS NULL) OR ([state] = @p36)) AND ((@p37 = 1 AND [expected_total_amount] IS NULL) OR ([expected_total_amount] = @p38)) AND ((@p39 = 1 AND [discount_percent] IS NULL) OR ([discount_percent] = @p40)) AND ((@p41 = 1 AND [payment_code_number] IS NULL) OR ([payment_code_number] = @p42)) AND ((@p43 = 1 AND [payments_number] IS NULL) OR ([payments_number] = @p44)))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[packing_list_move] WHERE (([pack_list_number] = @p1) AND ([branch_number] = @p2) AND ([supplier_number] = @p3) AND ([packing_type] = @p4))";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_mt_input_pack_headService"/> class.
        /// </summary>
		public d_sql_mt_input_pack_headRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_mt_input_pack_head dataRow = null;
		dataRow = new d_sql_mt_input_pack_head();
		data[0] = dataRow;
			dataRow.pack_list_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.packing_type = "packing_type_0";
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.discount = 0;
			dataRow.mam = 0;
			dataRow.station_number = 0;
			dataRow.total = 0;
			dataRow.store_number = 0;
			dataRow.stock_number = 0;
			dataRow.log_book = 0;
			dataRow.state = "state_0";
			dataRow.expected_total_amount = 0;
			dataRow.discount_percent = 0;
			dataRow.order_number = 0;
			dataRow.packing_details_serial_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.payment_code_number = 0;
			dataRow.payments_number = 0;
		dataRow = new d_sql_mt_input_pack_head();
		data[1] = dataRow;
			dataRow.pack_list_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.packing_type = "packing_type_1";
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.discount = 1;
			dataRow.mam = 1;
			dataRow.station_number = 1;
			dataRow.total = 1;
			dataRow.store_number = 1;
			dataRow.stock_number = 1;
			dataRow.log_book = 1;
			dataRow.state = "state_1";
			dataRow.expected_total_amount = 1;
			dataRow.discount_percent = 1;
			dataRow.order_number = 1;
			dataRow.packing_details_serial_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.payment_code_number = 1;
			dataRow.payments_number = 1;
		dataRow = new d_sql_mt_input_pack_head();
		data[2] = dataRow;
			dataRow.pack_list_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.packing_type = "packing_type_2";
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.discount = 2;
			dataRow.mam = 2;
			dataRow.station_number = 2;
			dataRow.total = 2;
			dataRow.store_number = 2;
			dataRow.stock_number = 2;
			dataRow.log_book = 2;
			dataRow.state = "state_2";
			dataRow.expected_total_amount = 2;
			dataRow.discount_percent = 2;
			dataRow.order_number = 2;
			dataRow.packing_details_serial_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.payment_code_number = 2;
			dataRow.payments_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_mt_input_pack_head model)
        {
            if (model == null)
            {
                return;
            }

            model.pack_list_number = GetValue(record, "pack_list_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.packing_type = GetValue(record, "packing_type", Convert.ToString);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.date_move = GetDateTime(record, "date_move");
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.station_number = GetValue(record, "station_num", Convert.ToInt64);
            model.total = GetValue(record, "pack_list_total", Convert.ToDecimal);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.packing_details_serial_number = GetValue(record, "packing_details_serial_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "supplier_name", Convert.ToString);
            model.payment_code_number = GetValue(record, "payment_code_number", Convert.ToInt64);
            model.payments_number = GetValue(record, "payments_number", Convert.ToInt64);
		}
    }
}
