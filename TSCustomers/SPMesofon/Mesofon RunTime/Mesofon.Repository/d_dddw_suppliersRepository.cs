using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_dddw_suppliersRepository : RepositoryBase<d_dddw_suppliers>
    {
		private static readonly string _selectQuery = "SELECT suppliers.number, suppliers.name, 0, 0 AS sort, suppliers.canceled_date FROM suppliers ORDER BY suppliers.number ASC";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_dddw_suppliersService"/> class.
        /// </summary>
		public d_dddw_suppliersRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_dddw_suppliers dataRow = null;
		dataRow = new d_dddw_suppliers();
		data[0] = dataRow;
			dataRow.number = 0;
			dataRow.name = "name_0";
			dataRow.flag_select = 0;
			dataRow.sort = 0;
			dataRow.canceled_date = new DateTime(1900,1,1);
		dataRow = new d_dddw_suppliers();
		data[1] = dataRow;
			dataRow.number = 1;
			dataRow.name = "name_1";
			dataRow.flag_select = 1;
			dataRow.sort = 1;
			dataRow.canceled_date = new DateTime(1901,2,2);
		dataRow = new d_dddw_suppliers();
		data[2] = dataRow;
			dataRow.number = 2;
			dataRow.name = "name_2";
			dataRow.flag_select = 2;
			dataRow.sort = 2;
			dataRow.canceled_date = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_dddw_suppliers model)
        {
            if (model == null)
            {
                return;
            }

            model.number = GetValue(record, "number", Convert.ToInt64);
            model.name = GetValue(record, "name", Convert.ToString);
            model.flag_select = GetValue(record, "compute_0003", Convert.ToInt64);
            model.sort = GetValue(record, "sort", Convert.ToInt64);
            model.canceled_date = GetDateTime(record, "canceled_date");
		}
    }
}
