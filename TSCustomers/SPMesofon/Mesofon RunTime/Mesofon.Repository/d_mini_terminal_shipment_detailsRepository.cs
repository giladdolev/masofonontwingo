using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_shipment_detailsRepository : RepositoryBase<d_mini_terminal_shipment_details>
    {
        private static readonly string _selectQuery =   "SELECT SP.branch_number, SP.supplier_number, SP.order_number, SP.state, SP.invoice_number, SP.shipment_number, SP.pallet_number,  ID.item_number, ID.item_barcode " +
                                                        "FROM shipment_pallets SP, b2b_invoice_details ID, invoice_move IM " +
                                                        "WHERE   SP.branch_number 	= ID.branch_number " +
                                                        "AND     SP.supplier_number 	= ID.supplier_number " +
                                                        "AND     SP.invoice_number	= ID.invoice_number " +
                                                        "AND     SP.pallet_number 		= ID.pallet_number " +
                                                        "AND         SP.branch_number 	= IM.branch_number " +
                                                        "AND     SP.supplier_number 	= IM.supplier_number " +
                                                        "AND     SP.invoice_number	= IM.invoice_number " +
                                                        "AND     IM.state = 'O' " +
                                                        "AND IM.invoice_type = 'p' " +
                                                        "AND SP.branch_number    = @0 " +
                                                        "AND SP.shipment_number  = @1 " +
                                                        "AND SP.marlog_number        = @2 ";

        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_shipment_detailsService"/> class.
        /// </summary>
		public d_mini_terminal_shipment_detailsRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
        {
            ModelBase[] data = new ModelBase[3];
            d_mini_terminal_shipment_details dataRow = null;
            dataRow = new d_mini_terminal_shipment_details();
            data[0] = dataRow;
            dataRow.branch_number = 0;
            dataRow.supplier_number = 0;
            dataRow.order_number = 0;
            dataRow.state = "state_0";
            dataRow.invoice_number = 0;
            dataRow.shipment_number = 0;
            dataRow.pallet_number = "pallet_number_0";
            dataRow.item_number = 0;
            dataRow.item_barcode = "item_barcode_0";
            dataRow = new d_mini_terminal_shipment_details();
            data[1] = dataRow;
            dataRow.branch_number = 1;
            dataRow.supplier_number = 1;
            dataRow.order_number = 1;
            dataRow.state = "state_1";
            dataRow.invoice_number = 1;
            dataRow.shipment_number = 1;
            dataRow.pallet_number = "pallet_number_1";
            dataRow.item_number = 1;
            dataRow.item_barcode = "item_barcode_1";
            dataRow = new d_mini_terminal_shipment_details();
            data[2] = dataRow;
            dataRow.branch_number = 2;
            dataRow.supplier_number = 2;
            dataRow.order_number = 2;
            dataRow.state = "state_2";
            dataRow.invoice_number = 2;
            dataRow.shipment_number = 2;
            dataRow.pallet_number = "pallet_number_2";
            dataRow.item_number = 2;
            dataRow.item_barcode = "item_barcode_2";
            return data;
        }

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_shipment_details model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToDouble);
            model.state = GetValue(record, "state", Convert.ToString);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.pallet_number = GetValue(record, "pallet_number", Convert.ToString);
            model.item_number = GetValue(record, "item_number", Convert.ToInt64);
            model.item_barcode = GetValue(record, "item_barcode", Convert.ToString);
        }
    }
}
