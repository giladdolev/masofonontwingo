using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class lds_global_branch_parametersRepository : RepositoryBase<lds_global_branch_parameters>
    {
		private static readonly string _selectQuery = "SELECT global_branch_parameters.report_db_active, global_branch_parameters.report_db_type, global_branch_parameters.report_db_dbms, global_branch_parameters.report_db_dbparm, global_branch_parameters.report_db_database, global_branch_parameters.report_db_servername, global_branch_parameters.target_db_active, global_branch_parameters.target_db_type, global_branch_parameters.target_db_dbms, global_branch_parameters.target_db_dbparm, global_branch_parameters.target_db_database, global_branch_parameters.target_db_servername, global_branch_parameters.macabi_transaction_code, global_branch_parameters.leumit_transaction_code, global_branch_parameters.card_terminal_proxy_active, global_branch_parameters.card_terminal_path_from_client, global_branch_parameters.card_terminal_path_from_proxy, global_branch_parameters.card_terminal_number, global_branch_parameters.card_terminal_version, global_branch_parameters.queue_db_active, global_branch_parameters.queue_db_type, global_branch_parameters.queue_db_dbms, global_branch_parameters.queue_db_dbparm, global_branch_parameters.queue_db_database, global_branch_parameters.queue_db_servername, global_branch_parameters.macabi_delete_after_tran, global_branch_parameters.leumit_delete_after_tran FROM global_branch_parameters WHERE branch_number = @0";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="lds_global_branch_parametersService"/> class.
        /// </summary>
		public lds_global_branch_parametersRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			lds_global_branch_parameters dataRow = null;
		dataRow = new lds_global_branch_parameters();
		data[0] = dataRow;
			dataRow.report_db_active = 0;
			dataRow.report_db_type = "report_db_type_0";
			dataRow.report_db_dbms = "report_db_dbms_0";
			dataRow.report_db_dbparm = "report_db_dbparm_0";
			dataRow.report_db_database = "report_db_database_0";
			dataRow.report_db_servername = "report_db_servername_0";
			dataRow.target_db_active = 0;
			dataRow.target_db_type = "target_db_type_0";
			dataRow.target_db_dbms = "target_db_dbms_0";
			dataRow.target_db_dbparm = "target_db_dbparm_0";
			dataRow.target_db_database = "target_db_database_0";
			dataRow.target_db_servername = "target_db_servername_0";
			dataRow.macabi_transaction_code = 0;
			dataRow.leumit_transaction_code = 0;
			dataRow.card_terminal_proxy_active = 0;
			dataRow.card_terminal_path_from_client = "card_terminal_path_from_client_0";
			dataRow.card_terminal_path_from_proxy = "card_terminal_path_from_proxy_0";
			dataRow.card_terminal_number = "card_terminal_number_0";
			dataRow.card_terminal_version = "card_terminal_version_0";
			dataRow.queue_db_active = 0;
			dataRow.queue_db_type = "queue_db_type_0";
			dataRow.queue_db_dbms = "queue_db_dbms_0";
			dataRow.queue_db_dbparm = "queue_db_dbparm_0";
			dataRow.queue_db_database = "queue_db_database_0";
			dataRow.queue_db_servername = "queue_db_servername_0";
			dataRow.macabi_delete_after_tran = 0;
			dataRow.leumit_delete_after_tran = 0;
		dataRow = new lds_global_branch_parameters();
		data[1] = dataRow;
			dataRow.report_db_active = 1;
			dataRow.report_db_type = "report_db_type_1";
			dataRow.report_db_dbms = "report_db_dbms_1";
			dataRow.report_db_dbparm = "report_db_dbparm_1";
			dataRow.report_db_database = "report_db_database_1";
			dataRow.report_db_servername = "report_db_servername_1";
			dataRow.target_db_active = 1;
			dataRow.target_db_type = "target_db_type_1";
			dataRow.target_db_dbms = "target_db_dbms_1";
			dataRow.target_db_dbparm = "target_db_dbparm_1";
			dataRow.target_db_database = "target_db_database_1";
			dataRow.target_db_servername = "target_db_servername_1";
			dataRow.macabi_transaction_code = 1;
			dataRow.leumit_transaction_code = 1;
			dataRow.card_terminal_proxy_active = 1;
			dataRow.card_terminal_path_from_client = "card_terminal_path_from_client_1";
			dataRow.card_terminal_path_from_proxy = "card_terminal_path_from_proxy_1";
			dataRow.card_terminal_number = "card_terminal_number_1";
			dataRow.card_terminal_version = "card_terminal_version_1";
			dataRow.queue_db_active = 1;
			dataRow.queue_db_type = "queue_db_type_1";
			dataRow.queue_db_dbms = "queue_db_dbms_1";
			dataRow.queue_db_dbparm = "queue_db_dbparm_1";
			dataRow.queue_db_database = "queue_db_database_1";
			dataRow.queue_db_servername = "queue_db_servername_1";
			dataRow.macabi_delete_after_tran = 1;
			dataRow.leumit_delete_after_tran = 1;
		dataRow = new lds_global_branch_parameters();
		data[2] = dataRow;
			dataRow.report_db_active = 2;
			dataRow.report_db_type = "report_db_type_2";
			dataRow.report_db_dbms = "report_db_dbms_2";
			dataRow.report_db_dbparm = "report_db_dbparm_2";
			dataRow.report_db_database = "report_db_database_2";
			dataRow.report_db_servername = "report_db_servername_2";
			dataRow.target_db_active = 2;
			dataRow.target_db_type = "target_db_type_2";
			dataRow.target_db_dbms = "target_db_dbms_2";
			dataRow.target_db_dbparm = "target_db_dbparm_2";
			dataRow.target_db_database = "target_db_database_2";
			dataRow.target_db_servername = "target_db_servername_2";
			dataRow.macabi_transaction_code = 2;
			dataRow.leumit_transaction_code = 2;
			dataRow.card_terminal_proxy_active = 2;
			dataRow.card_terminal_path_from_client = "card_terminal_path_from_client_2";
			dataRow.card_terminal_path_from_proxy = "card_terminal_path_from_proxy_2";
			dataRow.card_terminal_number = "card_terminal_number_2";
			dataRow.card_terminal_version = "card_terminal_version_2";
			dataRow.queue_db_active = 2;
			dataRow.queue_db_type = "queue_db_type_2";
			dataRow.queue_db_dbms = "queue_db_dbms_2";
			dataRow.queue_db_dbparm = "queue_db_dbparm_2";
			dataRow.queue_db_database = "queue_db_database_2";
			dataRow.queue_db_servername = "queue_db_servername_2";
			dataRow.macabi_delete_after_tran = 2;
			dataRow.leumit_delete_after_tran = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, lds_global_branch_parameters model)
        {
            if (model == null)
            {
                return;
            }

            model.report_db_active = GetValue(record, "report_db_active", Convert.ToDouble);
            model.report_db_type = GetValue(record, "report_db_type", Convert.ToString);
            model.report_db_dbms = GetValue(record, "report_db_dbms", Convert.ToString);
            model.report_db_dbparm = GetValue(record, "report_db_dbparm", Convert.ToString);
            model.report_db_database = GetValue(record, "report_db_database", Convert.ToString);
            model.report_db_servername = GetValue(record, "report_db_servername", Convert.ToString);
            model.target_db_active = GetValue(record, "target_db_active", Convert.ToDouble);
            model.target_db_type = GetValue(record, "target_db_type", Convert.ToString);
            model.target_db_dbms = GetValue(record, "target_db_dbms", Convert.ToString);
            model.target_db_dbparm = GetValue(record, "target_db_dbparm", Convert.ToString);
            model.target_db_database = GetValue(record, "target_db_database", Convert.ToString);
            model.target_db_servername = GetValue(record, "target_db_servername", Convert.ToString);
            model.macabi_transaction_code = GetValue(record, "macabi_transaction_code", Convert.ToInt64);
            model.leumit_transaction_code = GetValue(record, "leumit_transaction_code", Convert.ToInt64);
            model.card_terminal_proxy_active = GetValue(record, "card_terminal_proxy_active", Convert.ToDouble);
            model.card_terminal_path_from_client = GetValue(record, "card_terminal_path_from_client", Convert.ToString);
            model.card_terminal_path_from_proxy = GetValue(record, "card_terminal_path_from_proxy", Convert.ToString);
            model.card_terminal_number = GetValue(record, "card_terminal_number", Convert.ToString);
            model.card_terminal_version = GetValue(record, "card_terminal_version", Convert.ToString);
            model.queue_db_active = GetValue(record, "queue_db_active", Convert.ToDouble);
            model.queue_db_type = GetValue(record, "queue_db_type", Convert.ToString);
            model.queue_db_dbms = GetValue(record, "queue_db_dbms", Convert.ToString);
            model.queue_db_dbparm = GetValue(record, "queue_db_dbparm", Convert.ToString);
            model.queue_db_database = GetValue(record, "queue_db_database", Convert.ToString);
            model.queue_db_servername = GetValue(record, "queue_db_servername", Convert.ToString);
            model.macabi_delete_after_tran = GetValue(record, "macabi_delete_after_tran", Convert.ToDouble);
            model.leumit_delete_after_tran = GetValue(record, "leumit_delete_after_tran", Convert.ToDouble);
		}
    }
}
