using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dw_item_codeRepository : RepositoryBase<dw_item_code>
    {
		private static readonly string _selectQuery = "SELECT item_codes.item_number, item_codes.code, items.barcode, items.name, item_code_name FROM item_codes, items, item_codes_type WHERE item_codes.item_number = items.number AND item_codes.code_reverse LIKE @0 AND items.barcode_reverse NOT LIKE @0 AND item_code_number = code_type UNION SELECT number AS item_number, '' AS code, barcode, name, 'Main' AS item_code_name FROM items WHERE items.barcode_reverse LIKE @0 ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dw_item_codeService"/> class.
        /// </summary>
		public dw_item_codeRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dw_item_code dataRow = null;
		dataRow = new dw_item_code();
		data[0] = dataRow;
			dataRow.item_number = 0;
			dataRow.code = "code_0";
			dataRow.items_barcode = "items_barcode_0";
			dataRow.items_name = "items_name_0";
			dataRow.item_code_name = "item_code_name_0";
		dataRow = new dw_item_code();
		data[1] = dataRow;
			dataRow.item_number = 1;
			dataRow.code = "code_1";
			dataRow.items_barcode = "items_barcode_1";
			dataRow.items_name = "items_name_1";
			dataRow.item_code_name = "item_code_name_1";
		dataRow = new dw_item_code();
		data[2] = dataRow;
			dataRow.item_number = 2;
			dataRow.code = "code_2";
			dataRow.items_barcode = "items_barcode_2";
			dataRow.items_name = "items_name_2";
			dataRow.item_code_name = "item_code_name_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dw_item_code model)
        {
            if (model == null)
            {
                return;
            }

            model.item_number = GetValue(record, "item_number", Convert.ToInt64);
            model.code = GetValue(record, "code", Convert.ToString);
            model.items_barcode = GetValue(record, "barcode", Convert.ToString);
            model.items_name = GetValue(record, "name", Convert.ToString);
            model.item_code_name = GetValue(record, "item_code_name", Convert.ToString);
		}
    }
}
