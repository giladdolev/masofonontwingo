using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_tc_campaign_definiteRepository : RepositoryBase<d_tc_campaign_definite>
    {
		private static readonly string _selectQuery = "SELECT C.campaign_grade, C.campaign_status, C.campaign_description, C.campaign_name, C.serial_number, C.container_number, C.branches_numbers, C.start_datetime, C.end_datetime, C.campaign_property, C.club_number, C.all_days, C.without_days, getdate(), getdate(), '', '', '', ' ', C.consider_time_in_daily_calc, C.automatic_campaign, C.external_campaign_number, C.allow_cancel_from_branch, C.campaign_type, C.clubs, C.stars, C.campaign_period, C.adhoc_serial_number, (SELECT adhoc_key FROM supplier_order_trades WHERE serial_number = C.adhoc_serial_number AND supplier_order_trades.trade_number = @0) AS adhoc_key FROM tc_campaigns AS C WHERE (C.serial_number = @0)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_tc_campaign_definiteService"/> class.
        /// </summary>
		public d_tc_campaign_definiteRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("i", "����� �����");
            codeTable.Add("b", "���� �����");
            codeTable.Add("v", "�� �� �������");
            LookupTables.Add("campaign_property", codeTable);

            codeTable = new Dictionary<string, object>();
            codeTable.Add("p", "������");
            codeTable.Add("d", "�����");
            codeTable.Add("t", "������");
            codeTable.Add("c", "����");
            codeTable.Add("a", "����");
            codeTable.Add("e", "���� ����");
            LookupTables.Add("campaign_status", codeTable);

            codeTable = new Dictionary<string, object>();
            codeTable.Add("p", "������");
            codeTable.Add("d", "�����");
            codeTable.Add("t", "������");
            codeTable.Add("c", "����");
            LookupTables.Add("status_flag", codeTable);

            codeTable = new Dictionary<string, object>();
            codeTable.Add("0", "���� ��� 999");
            codeTable.Add(null, "����� ������");
            LookupTables.Add("compute_branch", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_tc_campaign_definite dataRow = null;
		dataRow = new d_tc_campaign_definite();
		data[0] = dataRow;
			dataRow.campaign_grade = 0;
			dataRow.campaign_status = "campaign_status_0";
			dataRow.campaign_description = "campaign_description_0";
			dataRow.campaign_name = "campaign_name_0";
			dataRow.serial_number = 0;
			dataRow.container_number = 0;
			dataRow.branches_numbers = "branches_numbers_0";
			dataRow.start_datetime = new DateTime(1900,1,1);
			dataRow.end_datetime = new DateTime(1900,1,1);
			dataRow.campaign_property = "campaign_property_0";
			dataRow.club_number = 0;
			dataRow.all_days = "all_days_0";
			dataRow.without_days = "without_days_0";
			dataRow.compute_start = new DateTime(1900,1,1);
			dataRow.compute_end = new DateTime(1900,1,1);
			dataRow.flag = "flag_0";
			dataRow.status_flag = "status_flag_0";
			dataRow.compute_branch = "compute_branch_0";
			dataRow.more_year = "more_year_0";
			dataRow.daily_calc = "daily_calc_0";
			dataRow.automatic_campaign = 0;
			dataRow.external_campaign_number = 0;
			dataRow.allow_cancel_from_branch = "allow_cancel_from_branch_0";
			dataRow.campaign_type = 0;
			dataRow.clubs = "clubs_0";
			dataRow.stars = 0;
			dataRow.campaign_period = 0;
			dataRow.adhoc_serial_number = 0;
			dataRow.adhoc_key = "adhoc_key_0";
		dataRow = new d_tc_campaign_definite();
		data[1] = dataRow;
			dataRow.campaign_grade = 1;
			dataRow.campaign_status = "campaign_status_1";
			dataRow.campaign_description = "campaign_description_1";
			dataRow.campaign_name = "campaign_name_1";
			dataRow.serial_number = 1;
			dataRow.container_number = 1;
			dataRow.branches_numbers = "branches_numbers_1";
			dataRow.start_datetime = new DateTime(1901,2,2);
			dataRow.end_datetime = new DateTime(1901,2,2);
			dataRow.campaign_property = "campaign_property_1";
			dataRow.club_number = 1;
			dataRow.all_days = "all_days_1";
			dataRow.without_days = "without_days_1";
			dataRow.compute_start = new DateTime(1901,2,2);
			dataRow.compute_end = new DateTime(1901,2,2);
			dataRow.flag = "flag_1";
			dataRow.status_flag = "status_flag_1";
			dataRow.compute_branch = "compute_branch_1";
			dataRow.more_year = "more_year_1";
			dataRow.daily_calc = "daily_calc_1";
			dataRow.automatic_campaign = 1;
			dataRow.external_campaign_number = 1;
			dataRow.allow_cancel_from_branch = "allow_cancel_from_branch_1";
			dataRow.campaign_type = 1;
			dataRow.clubs = "clubs_1";
			dataRow.stars = 1;
			dataRow.campaign_period = 1;
			dataRow.adhoc_serial_number = 1;
			dataRow.adhoc_key = "adhoc_key_1";
		dataRow = new d_tc_campaign_definite();
		data[2] = dataRow;
			dataRow.campaign_grade = 2;
			dataRow.campaign_status = "campaign_status_2";
			dataRow.campaign_description = "campaign_description_2";
			dataRow.campaign_name = "campaign_name_2";
			dataRow.serial_number = 2;
			dataRow.container_number = 2;
			dataRow.branches_numbers = "branches_numbers_2";
			dataRow.start_datetime = new DateTime(1902,3,3);
			dataRow.end_datetime = new DateTime(1902,3,3);
			dataRow.campaign_property = "campaign_property_2";
			dataRow.club_number = 2;
			dataRow.all_days = "all_days_2";
			dataRow.without_days = "without_days_2";
			dataRow.compute_start = new DateTime(1902,3,3);
			dataRow.compute_end = new DateTime(1902,3,3);
			dataRow.flag = "flag_2";
			dataRow.status_flag = "status_flag_2";
			dataRow.compute_branch = "compute_branch_2";
			dataRow.more_year = "more_year_2";
			dataRow.daily_calc = "daily_calc_2";
			dataRow.automatic_campaign = 2;
			dataRow.external_campaign_number = 2;
			dataRow.allow_cancel_from_branch = "allow_cancel_from_branch_2";
			dataRow.campaign_type = 2;
			dataRow.clubs = "clubs_2";
			dataRow.stars = 2;
			dataRow.campaign_period = 2;
			dataRow.adhoc_serial_number = 2;
			dataRow.adhoc_key = "adhoc_key_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_tc_campaign_definite model)
        {
            if (model == null)
            {
                return;
            }

            model.campaign_grade = GetValue(record, "campaign_grade", Convert.ToInt64);
            model.campaign_status = GetValue(record, "campaign_status", Convert.ToString);
            model.campaign_description = GetValue(record, "campaign_description", Convert.ToString);
            model.campaign_name = GetValue(record, "campaign_name", Convert.ToString);
            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.container_number = GetValue(record, "container_number", Convert.ToInt64);
            model.branches_numbers = GetValue(record, "branches_numbers", Convert.ToString);
            model.start_datetime = GetDateTime(record, "start_datetime");
            model.end_datetime = GetDateTime(record, "end_datetime");
            model.campaign_property = GetValue(record, "campaign_property", Convert.ToString);
            model.club_number = GetValue(record, "club_number", Convert.ToInt64);
            model.all_days = GetValue(record, "all_days", Convert.ToString);
            model.without_days = GetValue(record, "without_days", Convert.ToString);
            model.compute_start = GetDateTime(record, "compute_0014");
            model.compute_end = GetDateTime(record, "compute_0015");
            model.flag = GetValue(record, "compute_0016", Convert.ToString);
            model.status_flag = GetValue(record, "compute_0017", Convert.ToString);
            model.compute_branch = GetValue(record, "compute_0018", Convert.ToString);
            model.more_year = GetValue(record, "compute_0019", Convert.ToString);
            model.daily_calc = GetValue(record, "consider_time_in_daily_calc", Convert.ToString);
            model.automatic_campaign = GetValue(record, "automatic_campaign", Convert.ToInt64);
            model.external_campaign_number = GetValue(record, "external_campaign_number", Convert.ToInt64);
            model.allow_cancel_from_branch = GetValue(record, "allow_cancel_from_branch", Convert.ToString);
            model.campaign_type = GetValue(record, "campaign_type", Convert.ToInt64);
            model.clubs = GetValue(record, "clubs", Convert.ToString);
            model.stars = GetValue(record, "stars", Convert.ToInt64);
            model.campaign_period = GetValue(record, "campaign_period", Convert.ToInt64);
            model.adhoc_serial_number = GetValue(record, "adhoc_serial_number", Convert.ToInt64);
            model.adhoc_key = GetValue(record, "adhoc_key", Convert.ToString);
		}
    }
}
