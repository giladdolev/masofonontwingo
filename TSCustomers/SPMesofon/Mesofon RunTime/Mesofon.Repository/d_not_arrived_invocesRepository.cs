using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_not_arrived_invocesRepository : RepositoryBase<d_not_arrived_invoces>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_not_arrived_invocesService"/> class.
        /// </summary>
		public d_not_arrived_invocesRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_not_arrived_invoces dataRow = null;
		dataRow = new d_not_arrived_invoces();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
			dataRow.invoice_number = 0;
			dataRow.row = "row_0";
		dataRow = new d_not_arrived_invoces();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
			dataRow.invoice_number = 1;
			dataRow.row = "row_1";
		dataRow = new d_not_arrived_invoces();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			dataRow.invoice_number = 2;
			dataRow.row = "row_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_not_arrived_invoces model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "supplier_number", Convert.ToDouble);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.row = GetValue(record, "", Convert.ToString);
		}
    }
}
