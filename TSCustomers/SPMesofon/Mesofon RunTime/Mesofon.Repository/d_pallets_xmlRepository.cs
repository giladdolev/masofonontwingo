using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_pallets_xmlRepository : RepositoryBase<d_pallets_xml>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_pallets_xmlService"/> class.
        /// </summary>
		public d_pallets_xmlRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_pallets_xml dataRow = null;
		dataRow = new d_pallets_xml();
		data[0] = dataRow;
			dataRow.sender = "sender_0";
			dataRow.receiver = "receiver_0";
			dataRow.doctype = "doctype_0";
			dataRow.aprf = "aprf_0";
			dataRow.snrf = "snrf_0";
			dataRow.ackn = "ackn_0";
			dataRow.testind = "testind_0";
			dataRow.messdate = "messdate_0";
			dataRow.messtime = "messtime_0";
			dataRow.deliverydate = "deliverydate_0";
			dataRow.documenttype = "documenttype_0";
			dataRow.palletbarcode = "palletbarcode_0";
			dataRow.retailno = "retailno_0";
			dataRow.retailname = "retailname_0";
			dataRow.cdno = "cdno_0";
			dataRow.cdname = "cdname_0";
			dataRow.documentno = 0;
			dataRow.extra = 0;
			dataRow.rotation = "rotation_0";
			dataRow.docconfrtype = "docconfrtype_0";
			dataRow.subsupplier = "subsupplier_0";
			dataRow.disagrtype = "disagrtype_0";
			dataRow.extradetail = 0;
		dataRow = new d_pallets_xml();
		data[1] = dataRow;
			dataRow.sender = "sender_1";
			dataRow.receiver = "receiver_1";
			dataRow.doctype = "doctype_1";
			dataRow.aprf = "aprf_1";
			dataRow.snrf = "snrf_1";
			dataRow.ackn = "ackn_1";
			dataRow.testind = "testind_1";
			dataRow.messdate = "messdate_1";
			dataRow.messtime = "messtime_1";
			dataRow.deliverydate = "deliverydate_1";
			dataRow.documenttype = "documenttype_1";
			dataRow.palletbarcode = "palletbarcode_1";
			dataRow.retailno = "retailno_1";
			dataRow.retailname = "retailname_1";
			dataRow.cdno = "cdno_1";
			dataRow.cdname = "cdname_1";
			dataRow.documentno = 1;
			dataRow.extra = 1;
			dataRow.rotation = "rotation_1";
			dataRow.docconfrtype = "docconfrtype_1";
			dataRow.subsupplier = "subsupplier_1";
			dataRow.disagrtype = "disagrtype_1";
			dataRow.extradetail = 1;
		dataRow = new d_pallets_xml();
		data[2] = dataRow;
			dataRow.sender = "sender_2";
			dataRow.receiver = "receiver_2";
			dataRow.doctype = "doctype_2";
			dataRow.aprf = "aprf_2";
			dataRow.snrf = "snrf_2";
			dataRow.ackn = "ackn_2";
			dataRow.testind = "testind_2";
			dataRow.messdate = "messdate_2";
			dataRow.messtime = "messtime_2";
			dataRow.deliverydate = "deliverydate_2";
			dataRow.documenttype = "documenttype_2";
			dataRow.palletbarcode = "palletbarcode_2";
			dataRow.retailno = "retailno_2";
			dataRow.retailname = "retailname_2";
			dataRow.cdno = "cdno_2";
			dataRow.cdname = "cdname_2";
			dataRow.documentno = 2;
			dataRow.extra = 2;
			dataRow.rotation = "rotation_2";
			dataRow.docconfrtype = "docconfrtype_2";
			dataRow.subsupplier = "subsupplier_2";
			dataRow.disagrtype = "disagrtype_2";
			dataRow.extradetail = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_pallets_xml model)
        {
            if (model == null)
            {
                return;
            }

            model.sender = GetValue(record, "Sender", Convert.ToString);
            model.receiver = GetValue(record, "Receiver", Convert.ToString);
            model.doctype = GetValue(record, "DocType", Convert.ToString);
            model.aprf = GetValue(record, "APRF", Convert.ToString);
            model.snrf = GetValue(record, "SNRF", Convert.ToString);
            model.ackn = GetValue(record, "Ackn", Convert.ToString);
            model.testind = GetValue(record, "TestInd", Convert.ToString);
            model.messdate = GetValue(record, "MessDate", Convert.ToString);
            model.messtime = GetValue(record, "MessTime", Convert.ToString);
            model.deliverydate = GetValue(record, "DeliveryDate", Convert.ToString);
            model.documenttype = GetValue(record, "DocumentType", Convert.ToString);
            model.palletbarcode = GetValue(record, "PalletBarcode", Convert.ToString);
            model.retailno = GetValue(record, "RetailNo", Convert.ToString);
            model.retailname = GetValue(record, "RetailName", Convert.ToString);
            model.cdno = GetValue(record, "CDNo", Convert.ToString);
            model.cdname = GetValue(record, "CDName", Convert.ToString);
            model.documentno = GetValue(record, "DocumentNo", Convert.ToDouble);
            model.extra = GetValue(record, "Extra", Convert.ToDouble);
            model.rotation = GetValue(record, "Rotation", Convert.ToString);
            model.docconfrtype = GetValue(record, "DocConfrType", Convert.ToString);
            model.subsupplier = GetValue(record, "SubSupplier", Convert.ToString);
            model.disagrtype = GetValue(record, "DisAgrType", Convert.ToString);
            model.extradetail = GetValue(record, "ExtraDetail", Convert.ToDouble);
        }




        public string XMLExport
        {
            get
            {
                return "<?xml version=\"1.0\" encoding=\"UTF-16LE\" standalone=\"no\"?><ConfirmDeliveryXml><Envelope><Sender>sender</Sender><Receiver>receiver</Receiver><DocType>doctype</DocType><APRF>aprf</APRF><SNRF>snrf</SNRF><Ackn>ackn</Ackn><TestInd>testind</TestInd><MessDate>messdate</MessDate><MessTime>messtime</MessTime><Header><DocumentNo>documentno</DocumentNo><DeliveryDate>deliverydate</DeliveryDate><Extra>extra</Extra><RetailNo>retailno</RetailNo><RetailName>retailname</RetailName><CDNo>cdno</CDNo><CDName>cdname</CDName><Rotation>rotation</Rotation><DocConfrType>docconfrtype</DocConfrType><DocumentType>documenttype</DocumentType><Details><Run __pbband=\"detail\"><PalletBarcode>palletbarcode</PalletBarcode><DisagrType>disagrtype</DisagrType><Extra>extradetail</Extra></Run></Details></Header></Envelope></ConfirmDeliveryXml>";
            }
        }
        
    }
}
