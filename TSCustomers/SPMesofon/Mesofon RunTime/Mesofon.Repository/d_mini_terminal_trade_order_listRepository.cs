using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_trade_order_listRepository : RepositoryBase<d_mini_terminal_trade_order_list>
    {
		private static readonly string _selectQuery = "SELECT site_order_state.branch_number, site_order_state.site_order_number, MAX(convert(varchar, site_order_state.date_move, 103)) AS date_move, MAX(site_order_state.target_branch_datetime) AS target_branch_datetime, MAX(CASE WHEN site_order_state.state = 's' THEN 0 ELSE 1 END) AS approve_ind, MIN(site_order_state.state) AS state, MIN(site_order_state.site_state) AS site_state, MAX(convert(date, site_order_state.date_move, 103)) AS date_move_short FROM site_order_state WHERE (site_order_state.branch_number = @1) AND (site_order_state.target_branch_number = @0) AND site_order_state.state IN ('b', 's', 'f', 'p') GROUP BY site_order_state.branch_number, site_order_state.site_order_number ORDER BY site_order_state.site_order_number DESC";
		private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "UPDATE [site_order_state] SET [target_branch_datetime] = @target_branch_datetime, [state] = @state, [site_state] = @site_state WHERE (([branch_number] = @branch_number)) AND (([site_order_number] = @site_order_number)) ";
        private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_trade_order_listService"/> class.
        /// </summary>
		public d_mini_terminal_trade_order_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_trade_order_list dataRow = null;
		dataRow = new d_mini_terminal_trade_order_list();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.site_order_number = 0;
			dataRow.date_move = "date_move_0";
			dataRow.target_branch_datetime = new DateTime(1900,1,1);
			dataRow.approve_ind = 0;
			dataRow.state = "state_0";
			dataRow.site_state = "site_state_0";
			dataRow.date_move_short = new DateTime(1900,1,1);
		dataRow = new d_mini_terminal_trade_order_list();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.site_order_number = 1;
			dataRow.date_move = "date_move_1";
			dataRow.target_branch_datetime = new DateTime(1901,2,2);
			dataRow.approve_ind = 1;
			dataRow.state = "state_1";
			dataRow.site_state = "site_state_1";
			dataRow.date_move_short = new DateTime(1901,2,2);
		dataRow = new d_mini_terminal_trade_order_list();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.site_order_number = 2;
			dataRow.date_move = "date_move_2";
			dataRow.target_branch_datetime = new DateTime(1902,3,3);
			dataRow.approve_ind = 2;
			dataRow.state = "state_2";
			dataRow.site_state = "site_state_2";
			dataRow.date_move_short = new DateTime(1902,3,3);
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_trade_order_list model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.site_order_number = GetValue(record, "site_order_number", Convert.ToDouble);
            model.date_move = GetValue(record, "date_move", Convert.ToString);
            model.target_branch_datetime = GetDateTime(record, "target_branch_datetime");
            model.approve_ind = GetValue(record, "approve_ind", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.site_state = GetValue(record, "site_state", Convert.ToString);
            model.date_move_short = GetDateTime(record, "date_move_short");
		}
    }
}
