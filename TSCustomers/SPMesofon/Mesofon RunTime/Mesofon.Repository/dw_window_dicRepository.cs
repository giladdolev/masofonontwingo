﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class dw_window_dicRepository : RepositoryBase<dw_window_dic>
    {
        public dw_window_dicRepository(string selectQuery, string updateQueryByKey, string updateQueryByFields, string deleteQuery, string insertQuery) : base(selectQuery, updateQueryByKey, updateQueryByFields, deleteQuery, insertQuery)
        {
        }

        public override void Map(IDataRecord record, dw_window_dic model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
