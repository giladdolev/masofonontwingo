using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_return_marlog_print_footerRepository : RepositoryBase<d_return_marlog_print_footer>
    {
		private static readonly string _selectQuery = "SELECT SMR.branch_number, SMR.supplier_number, SMR.invoice_number, MIN(S.name) AS supplier_name, SUM(SMR.material_quantity) AS total_quantity FROM shipment_marlog_return AS SMR, suppliers AS S WHERE SMR.supplier_number = S.number AND SMR.branch_number = @0 AND SMR.shipment_number = @1 AND SMR.marlog_number = @2 AND SMR.state <> 'd' GROUP BY SMR.branch_number, SMR.supplier_number, SMR.invoice_number ORDER BY SMR.invoice_number ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_return_marlog_print_footerService"/> class.
        /// </summary>
		public d_return_marlog_print_footerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_return_marlog_print_footer dataRow = null;
		dataRow = new d_return_marlog_print_footer();
		data[0] = dataRow;
			dataRow.shipment_marlog_return_branch_number = 0;
			dataRow.shipment_marlog_return_supplier_number = 0;
			dataRow.shipment_marlog_return_invoice_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.total_quantity = 0;
		dataRow = new d_return_marlog_print_footer();
		data[1] = dataRow;
			dataRow.shipment_marlog_return_branch_number = 1;
			dataRow.shipment_marlog_return_supplier_number = 1;
			dataRow.shipment_marlog_return_invoice_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.total_quantity = 1;
		dataRow = new d_return_marlog_print_footer();
		data[2] = dataRow;
			dataRow.shipment_marlog_return_branch_number = 2;
			dataRow.shipment_marlog_return_supplier_number = 2;
			dataRow.shipment_marlog_return_invoice_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.total_quantity = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_return_marlog_print_footer model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_marlog_return_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.shipment_marlog_return_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.shipment_marlog_return_invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.supplier_name = GetValue(record, "supplier_name", Convert.ToString);
            model.total_quantity = GetValue(record, "total_quantity", Convert.ToInt64);
		}
    }
}
