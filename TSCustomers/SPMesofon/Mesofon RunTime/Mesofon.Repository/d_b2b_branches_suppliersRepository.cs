using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_b2b_branches_suppliersRepository : RepositoryBase<d_b2b_branches_suppliers>
    {
		private static readonly string _selectQuery = "SELECT b2b_branches_suppliers.supplier_number, suppliers.edi_number FROM b2b_branches_suppliers, suppliers WHERE b2b_branches_suppliers.branch_number IN (@0) AND b2b_branches_suppliers.supplier_number = suppliers.number AND b2b_branches_suppliers.b2b_enabled = 1 AND suppliers.b2b_supported = 1";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_b2b_branches_suppliersService"/> class.
        /// </summary>
		public d_b2b_branches_suppliersRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_b2b_branches_suppliers dataRow = null;
		dataRow = new d_b2b_branches_suppliers();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
			dataRow.suppliers_edi_number = "suppliers_edi_number_0";
		dataRow = new d_b2b_branches_suppliers();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
			dataRow.suppliers_edi_number = "suppliers_edi_number_1";
		dataRow = new d_b2b_branches_suppliers();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			dataRow.suppliers_edi_number = "suppliers_edi_number_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_b2b_branches_suppliers model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.suppliers_edi_number = GetValue(record, "edi_number", Convert.ToString);
		}
    }
}
