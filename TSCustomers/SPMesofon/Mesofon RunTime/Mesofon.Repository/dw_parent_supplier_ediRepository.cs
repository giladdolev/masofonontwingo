using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dw_parent_supplier_ediRepository : RepositoryBase<dw_parent_supplier_edi>
    {
		private static readonly string _selectQuery = "SELECT suppliers.number, suppliers.supplier_parent, PSEN.edi_number FROM suppliers, suppliers AS PSEN WHERE PSEN.number = suppliers.supplier_parent AND suppliers.number <> suppliers.supplier_parent";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[suppliers] ([number], [supplier_parent], [edi_number]) VALUES (@p1, @p2, @p3)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[suppliers] SET [number] = @p1, [supplier_parent] = @p2, [edi_number] = @p3 WHERE (([number] = @p4))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[suppliers] SET [number] = @p1, [supplier_parent] = @p2, [edi_number] = @p3 WHERE (([number] = @p4) AND ([supplier_parent] = @p5) AND ((@p6 = 1 AND [edi_number] IS NULL) OR ([edi_number] = @p7)))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[suppliers] WHERE (([number] = @p1))";

        /// <summary>
        /// Initializes a new instance of the <see cref="dw_parent_supplier_ediService"/> class.
        /// </summary>
		public dw_parent_supplier_ediRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dw_parent_supplier_edi dataRow = null;
		dataRow = new dw_parent_supplier_edi();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
			dataRow.supplier_parent = 0;
			dataRow.parent_supp_edi_number = "parent_supp_edi_number_0";
		dataRow = new dw_parent_supplier_edi();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
			dataRow.supplier_parent = 1;
			dataRow.parent_supp_edi_number = "parent_supp_edi_number_1";
		dataRow = new dw_parent_supplier_edi();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			dataRow.supplier_parent = 2;
			dataRow.parent_supp_edi_number = "parent_supp_edi_number_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dw_parent_supplier_edi model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "number", Convert.ToInt64);
            model.supplier_parent = GetValue(record, "supplier_parent", Convert.ToInt64);
            model.parent_supp_edi_number = GetValue(record, "edi_number", Convert.ToString);
		}
    }
}
