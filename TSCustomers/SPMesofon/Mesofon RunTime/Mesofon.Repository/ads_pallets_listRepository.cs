﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class ads_pallets_listRepository:RepositoryBase<ads_pallets_list>
    {
        private static readonly string _selectQuery = "";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";
        public ads_pallets_listRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        public override void Map(IDataRecord record, ads_pallets_list model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
