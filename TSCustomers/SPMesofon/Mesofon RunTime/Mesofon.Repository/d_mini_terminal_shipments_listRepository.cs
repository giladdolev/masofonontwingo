using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_shipments_listRepository : RepositoryBase<d_mini_terminal_shipments_list>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_shipments_listService"/> class.
        /// </summary>
		public d_mini_terminal_shipments_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_shipments_list dataRow = null;
		dataRow = new d_mini_terminal_shipments_list();
		data[0] = dataRow;
			dataRow.shipment_number = 0;
		dataRow = new d_mini_terminal_shipments_list();
		data[1] = dataRow;
			dataRow.shipment_number = 1;
		dataRow = new d_mini_terminal_shipments_list();
		data[2] = dataRow;
			dataRow.shipment_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_shipments_list model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
		}
    }
}
