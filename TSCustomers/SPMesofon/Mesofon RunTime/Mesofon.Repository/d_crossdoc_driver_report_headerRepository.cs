using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_crossdoc_driver_report_headerRepository : RepositoryBase<d_crossdoc_driver_report_header>
    {
        private static readonly string _selectQuery = "SELECT MAX(shipment_crossdoc.date_move) AS date_move, shipment_crossdoc.branch_number, shipment_crossdoc.shipment_pallet_number AS shipment_number, shipment_crossdoc.carton_barcode, 1 AS pack_quantity, MAX(shipment_crossdoc.marlog_send_datetime) AS arrive_datetime,MAX(convert(varchar, shipment_crossdoc.marlog_send_datetime, 18)) AS arrive_time, (SELECT global_parameters.branch_name FROM global_parameters WHERE serial_number = shipment_crossdoc.branch_number) AS branch_name, convert(varchar, getdate(), 18) AS scan_time, convert(varchar, getdate(), 103) AS print_date, (SELECT IsNull(name, '') FROM employees WHERE branch_number = @0 AND number = @1) AS employee_name, MAX(shipment_crossdoc.state) AS state, shipment_crossdoc.supplier_number, (SELECT name FROM suppliers WHERE number = shipment_crossdoc.supplier_number) AS supplier_name FROM shipment_crossdoc WHERE (shipment_crossdoc.branch_number = @0) AND (shipment_crossdoc.shipment_pallet_number = @2) AND (shipment_crossdoc.marlog_number = @3) GROUP BY branch_number, shipment_pallet_number, carton_barcode, supplier_number ORDER BY shipment_crossdoc.supplier_number ";
        private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_crossdoc_driver_report_headerService"/> class.
        /// </summary>
		public d_crossdoc_driver_report_headerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("a", "����\"�");
            codeTable.Add("s", "��");
            codeTable.Add("n", "���");
            codeTable.Add("c", "��");
            codeTable.Add("f", "���� ���");
            LookupTables.Add("state", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_crossdoc_driver_report_header dataRow = null;
		dataRow = new d_crossdoc_driver_report_header();
		data[0] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1900,1,1);
			dataRow.branch_number = 0;
			dataRow.shipment_number = 0;
			dataRow.carton_barcode = "carton_barcode_0";
			dataRow.shipment_pallets_pack_quantity = 0;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1900,1,1).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_0";
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_0";
			dataRow.scan_time = "scan_time_0";
			dataRow.print_date = "print_date_0";
			dataRow.employee_name = "employee_name_0";
			dataRow.state = "state_0";
			dataRow.supplier_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.compute_pack_quantity = "compute_pack_quantity_0";
			dataRow.compute_pallets_number = "compute_pallets_number_0";
		dataRow = new d_crossdoc_driver_report_header();
		data[1] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1901,2,2);
			dataRow.branch_number = 1;
			dataRow.shipment_number = 1;
			dataRow.carton_barcode = "carton_barcode_1";
			dataRow.shipment_pallets_pack_quantity = 1;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1901,2,2).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_1";
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_1";
			dataRow.scan_time = "scan_time_1";
			dataRow.print_date = "print_date_1";
			dataRow.employee_name = "employee_name_1";
			dataRow.state = "state_1";
			dataRow.supplier_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.compute_pack_quantity = "compute_pack_quantity_1";
			dataRow.compute_pallets_number = "compute_pallets_number_1";
		dataRow = new d_crossdoc_driver_report_header();
		data[2] = dataRow;
			dataRow.shipment_pallets_date_move = new DateTime(1902,3,3);
			dataRow.branch_number = 2;
			dataRow.shipment_number = 2;
			dataRow.carton_barcode = "carton_barcode_2";
			dataRow.shipment_pallets_pack_quantity = 2;
			dataRow.shipment_pallets_arrive_datetime = new DateTime(1902,3,3).ToShortDateString();
			dataRow.shipment_pallets_arrive_time = "shipment_pallets_arrive_time_2";
			dataRow.global_parameters_branch_name = "global_parameters_branch_name_2";
			dataRow.scan_time = "scan_time_2";
			dataRow.print_date = "print_date_2";
			dataRow.employee_name = "employee_name_2";
			dataRow.state = "state_2";
			dataRow.supplier_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.compute_pack_quantity = "compute_pack_quantity_2";
			dataRow.compute_pallets_number = "compute_pallets_number_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_crossdoc_driver_report_header model)
        {
            if (model == null)
            {
                return;
            }

            model.shipment_pallets_date_move = GetDateTime(record, "date_move");
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.shipment_number = GetValue(record, "shipment_number", Convert.ToDouble);
            model.carton_barcode = GetValue(record, "carton_barcode", Convert.ToString);
            model.shipment_pallets_pack_quantity = GetValue(record, "pack_quantity", Convert.ToInt64);
            model.shipment_pallets_arrive_datetime = GetDateTime(record, "arrive_datetime")?.ToString("dd/MM/yyyy");
            model.shipment_pallets_arrive_time = GetValue(record, "arrive_time", Convert.ToString);
            model.global_parameters_branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.scan_time = GetValue(record, "scan_time", Convert.ToString);
            model.print_date = GetValue(record, "print_date", Convert.ToString);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            model.state = GetValue(record, "state", Convert.ToString);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "supplier_name", Convert.ToString);
            
		}
    }
}
