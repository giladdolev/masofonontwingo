using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_return_details_0_listRepository : RepositoryBase<d_mini_terminal_return_details_0_list>
    {
		private static readonly string _selectQuery = "SELECT invd.serial_number, invd.material_number FROM invoice_details AS invd, invoice_move AS invm WHERE invm.branch_number = invd.branch_number AND invm.invoice_number = invd.invoice_number AND invm.supplier_number = invd.supplier_number AND invd.branch_number = @0 AND invd.invoice_number = @1 AND invd.supplier_number = @2 AND invm.invoice_type = @3 AND invd.material_quantity = 0";
		private static readonly string _insertQuery = "INSERT INTO [invoice_details] ([serial_number], [material_number]) VALUES (@p1, @p2)";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_return_details_0_listService"/> class.
        /// </summary>
		public d_mini_terminal_return_details_0_listRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_return_details_0_list dataRow = null;
		dataRow = new d_mini_terminal_return_details_0_list();
		data[0] = dataRow;
			dataRow.serial_number = 0;
			dataRow.material_number = 0;
		dataRow = new d_mini_terminal_return_details_0_list();
		data[1] = dataRow;
			dataRow.serial_number = 1;
			dataRow.material_number = 1;
		dataRow = new d_mini_terminal_return_details_0_list();
		data[2] = dataRow;
			dataRow.serial_number = 2;
			dataRow.material_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_return_details_0_list model)
        {
            if (model == null)
            {
                return;
            }

            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
		}
    }
}
