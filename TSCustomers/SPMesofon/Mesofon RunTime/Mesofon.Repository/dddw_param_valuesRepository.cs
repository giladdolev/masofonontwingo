using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dddw_param_valuesRepository : RepositoryBase<dddw_param_values>
    {
		private static readonly string _selectQuery = "SELECT param_values.param_name, param_values.param_value FROM param_values WHERE param_values.param_code = @0 ORDER BY param_values.sort_order ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dddw_param_valuesService"/> class.
        /// </summary>
		public dddw_param_valuesRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dddw_param_values dataRow = null;
		dataRow = new dddw_param_values();
		data[0] = dataRow;
			dataRow.param_name = "param_name_0";
			dataRow.param_value = "param_value_0";
		dataRow = new dddw_param_values();
		data[1] = dataRow;
			dataRow.param_name = "param_name_1";
			dataRow.param_value = "param_value_1";
		dataRow = new dddw_param_values();
		data[2] = dataRow;
			dataRow.param_name = "param_name_2";
			dataRow.param_value = "param_value_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dddw_param_values model)
        {
            if (model == null)
            {
                return;
            }

            model.param_name = GetValue(record, "param_name", Convert.ToString);
            model.param_value = GetValue(record, "param_value", Convert.ToString);
		}
    }
}
