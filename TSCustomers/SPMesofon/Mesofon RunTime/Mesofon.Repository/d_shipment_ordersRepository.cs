using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_shipment_ordersRepository : RepositoryBase<d_shipment_orders>
    {
		private static readonly string _selectQuery = "SELECT DISTINCT shipment_pallets.branch_number, shipment_pallets.supplier_number, shipment_pallets.order_number, shipment_pallets.invoice_number FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.shipment_number = @1)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_shipment_ordersService"/> class.
        /// </summary>
		public d_shipment_ordersRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_shipment_orders dataRow = null;
		dataRow = new d_shipment_orders();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.order_number = 0;
			dataRow.invoice_number = 0;
		dataRow = new d_shipment_orders();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.order_number = 1;
			dataRow.invoice_number = 1;
		dataRow = new d_shipment_orders();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.order_number = 2;
			dataRow.invoice_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_shipment_orders model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToDouble);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
		}
    }
}
