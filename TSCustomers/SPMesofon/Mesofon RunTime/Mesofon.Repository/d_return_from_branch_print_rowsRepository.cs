using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_return_from_branch_print_rowsRepository : RepositoryBase<d_return_from_branch_print_rows>
    {
		private static readonly string _selectQuery = "SELECT materials.barcode, materials.name, invoice_details.material_price, invoice_details.material_quantity, invoice_move.mam, invoice_move.invoice_number, invoice_move.supplier_number, invoice_move.employee_number, invoice_move.invoice_number, invoice_move.state, (SELECT employees.name FROM employees WHERE employees.branch_number = @0 AND employees.number = invoice_move.employee_number) AS employee_name, suppliers.name, invoice_details.sell_price, global_parameters.branch_name, invoice_move.log_book, invoice_move.date_move, invoice_move.invoice_type, global_parameters.vat_number, global_parameters.phone_number, global_parameters.fax_number, invoice_details.decline_number FROM invoice_details, materials, invoice_move, suppliers, global_parameters WHERE (invoice_details.branch_number = @0) AND (invoice_move.branch_number = @0) AND (global_parameters.serial_number = @0) AND (materials.number = invoice_details.material_number) AND (invoice_details.invoice_number = invoice_move.invoice_number) AND (invoice_move.supplier_number = invoice_details.supplier_number) AND (suppliers.number = invoice_move.supplier_number) AND (invoice_details.material_number = materials.number) AND suppliers.number = @1 AND invoice_move.invoice_number = @2";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_return_from_branch_print_rowsService"/> class.
        /// </summary>
		public d_return_from_branch_print_rowsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_return_from_branch_print_rows dataRow = null;
		dataRow = new d_return_from_branch_print_rows();
		data[0] = dataRow;
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.material_name = "material_name_0";
			dataRow.material_price = 0;
			dataRow.material_quantity = 0;
			dataRow.mam = 0;
			dataRow.invoice_number = 0;
			dataRow.supplier_number = 0;
			dataRow.employee_number = 0;
			dataRow.shipment_marlog_return_invoice_number = 0;
			dataRow.shipment_marlog_return_state = "shipment_marlog_return_state_0";
			dataRow.employee_name = "employee_name_0";
			dataRow.supplier_name = "supplier_name_0";
			dataRow.sell_price = 0;
			dataRow.branch_name = "branch_name_0";
			dataRow.log_book = 0;
			dataRow.shipment_marlog_return_date_move = new DateTime(1900,1,1);
			dataRow.invoice_type = "invoice_type_0";
			dataRow.vat_number = "vat_number_0";
			dataRow.phone_number = "phone_number_0";
			dataRow.fax_number = "fax_number_0";
			dataRow.decline_number = 0;
			dataRow.cf_quantity_sum = "cf_quantity_sum_0";
			dataRow.line_number = "line_number_0";
		dataRow = new d_return_from_branch_print_rows();
		data[1] = dataRow;
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.material_name = "material_name_1";
			dataRow.material_price = 1;
			dataRow.material_quantity = 1;
			dataRow.mam = 1;
			dataRow.invoice_number = 1;
			dataRow.supplier_number = 1;
			dataRow.employee_number = 1;
			dataRow.shipment_marlog_return_invoice_number = 1;
			dataRow.shipment_marlog_return_state = "shipment_marlog_return_state_1";
			dataRow.employee_name = "employee_name_1";
			dataRow.supplier_name = "supplier_name_1";
			dataRow.sell_price = 1;
			dataRow.branch_name = "branch_name_1";
			dataRow.log_book = 1;
			dataRow.shipment_marlog_return_date_move = new DateTime(1901,2,2);
			dataRow.invoice_type = "invoice_type_1";
			dataRow.vat_number = "vat_number_1";
			dataRow.phone_number = "phone_number_1";
			dataRow.fax_number = "fax_number_1";
			dataRow.decline_number = 1;
			dataRow.cf_quantity_sum = "cf_quantity_sum_1";
			dataRow.line_number = "line_number_1";
		dataRow = new d_return_from_branch_print_rows();
		data[2] = dataRow;
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.material_name = "material_name_2";
			dataRow.material_price = 2;
			dataRow.material_quantity = 2;
			dataRow.mam = 2;
			dataRow.invoice_number = 2;
			dataRow.supplier_number = 2;
			dataRow.employee_number = 2;
			dataRow.shipment_marlog_return_invoice_number = 2;
			dataRow.shipment_marlog_return_state = "shipment_marlog_return_state_2";
			dataRow.employee_name = "employee_name_2";
			dataRow.supplier_name = "supplier_name_2";
			dataRow.sell_price = 2;
			dataRow.branch_name = "branch_name_2";
			dataRow.log_book = 2;
			dataRow.shipment_marlog_return_date_move = new DateTime(1902,3,3);
			dataRow.invoice_type = "invoice_type_2";
			dataRow.vat_number = "vat_number_2";
			dataRow.phone_number = "phone_number_2";
			dataRow.fax_number = "fax_number_2";
			dataRow.decline_number = 2;
			dataRow.cf_quantity_sum = "cf_quantity_sum_2";
			dataRow.line_number = "line_number_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_return_from_branch_print_rows model)
        {
            if (model == null)
            {
                return;
            }

            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.material_name = GetValue(record, "name", Convert.ToString);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.shipment_marlog_return_invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.shipment_marlog_return_state = GetValue(record, "state", Convert.ToString);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            model.supplier_name = GetValue(record, "name", Convert.ToString);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.shipment_marlog_return_date_move = GetDateTime(record, "date_move");
            model.invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.vat_number = GetValue(record, "vat_number", Convert.ToString);
            model.phone_number = GetValue(record, "phone_number", Convert.ToString);
            model.fax_number = GetValue(record, "fax_number", Convert.ToString);
            model.decline_number = GetValue(record, "decline_number", Convert.ToInt64);
            model.cf_quantity_sum = GetValue(record, "", Convert.ToString);
            model.line_number = GetValue(record, "", Convert.ToString);
		}
    }
}
