﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class d_invoice_numberRepository : RepositoryBase<d_invoice_number>
    {
        private static readonly string _selectQuery = "";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";
        public d_invoice_numberRepository() : base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        public override void Map(IDataRecord record, d_invoice_number model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
