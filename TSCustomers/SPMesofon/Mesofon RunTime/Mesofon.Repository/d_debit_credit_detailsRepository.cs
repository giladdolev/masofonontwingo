﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class d_debit_credit_detailsRepository : RepositoryBase<d_debit_credit_details>
    {
        public d_debit_credit_detailsRepository(string selectQuery, string updateQueryByKey, string updateQueryByFields, string deleteQuery, string insertQuery) : base(selectQuery, updateQueryByKey, updateQueryByFields, deleteQuery, insertQuery)
        {
        }

        public override void Map(IDataRecord record, d_debit_credit_details model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
