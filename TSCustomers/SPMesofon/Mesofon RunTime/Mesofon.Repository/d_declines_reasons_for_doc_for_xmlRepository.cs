﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class d_declines_reasons_for_doc_for_xmlRepository : RepositoryBase<d_declines_reasons_for_doc_for_xml>
    {
        public d_declines_reasons_for_doc_for_xmlRepository(string selectQuery, string updateQueryByKey, string updateQueryByFields, string deleteQuery, string insertQuery) : base(selectQuery, updateQueryByKey, updateQueryByFields, deleteQuery, insertQuery)
        {
        }

        public override void Map(IDataRecord record, d_declines_reasons_for_doc_for_xml model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
