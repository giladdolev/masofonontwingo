using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class ds_invpack_deleted_itemsRepository : RepositoryBase<ds_invpack_deleted_items>
    {
		private static readonly string _selectQuery = "SELECT invpack_deleted_items.branch_number, invpack_deleted_items.station_number, invpack_deleted_items.delete_point, invpack_deleted_items.serial_number, invpack_deleted_items.update_datetime, invpack_deleted_items.del_emp_number, invpack_deleted_items.delete_option, invpack_deleted_items.supplier_number, invpack_deleted_items.order_number, invpack_deleted_items.doc_name, invpack_deleted_items.doc_type, invpack_deleted_items.doc_number, invpack_deleted_items.employee_number, invpack_deleted_items.move_datetime, invpack_deleted_items.supplier_datetime, invpack_deleted_items.state, invpack_deleted_items.b2b_status, invpack_deleted_items.doc_total_amount, invpack_deleted_items.discount_percent, invpack_deleted_items.item_state, invpack_deleted_items.last_update_datetime, invpack_deleted_items.indicator, invpack_deleted_items.packing_list_number, invpack_deleted_items.packing_serial, invpack_deleted_items.import_type, invpack_deleted_items.color, invpack_deleted_items.decline_number, invpack_deleted_items.mini_terminal, invpack_deleted_items.expiration_date, invpack_deleted_items.out_of_stock_datetime, invpack_deleted_items.bonus_row, invpack_deleted_items.material_bonus_connect, invpack_deleted_items.quantity_within_invoice, invpack_deleted_items.expected_material_quantity, invpack_deleted_items.material_quantity, invpack_deleted_items.bonus_quantity, invpack_deleted_items.material_price, invpack_deleted_items.supplier_discount_percent, invpack_deleted_items.material_discount_percent, invpack_deleted_items.material_discount_amount, invpack_deleted_items.bonus_discount, invpack_deleted_items.material_price_after_disc, invpack_deleted_items.sell_price, invpack_deleted_items.current_catalog_sell_price, invpack_deleted_items.details, invpack_deleted_items.batch_number, invpack_deleted_items.recieved_date, invpack_deleted_items.batch_expiry_date, invpack_deleted_items.quantity_in, invpack_deleted_items.quantity_out FROM invpack_deleted_items";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[invpack_deleted_items] ([branch_number], [station_number], [delete_point],[serial_number], [update_datetime], [del_emp_number], [delete_option], [supplier_number], [order_number], [doc_name], [doc_type], [doc_number], [employee_number], [move_datetime], [supplier_datetime], [state], [b2b_status], [doc_total_amount], [discount_percent], [item_state], [last_update_datetime], [indicator], [packing_list_number], [packing_serial], [import_type], [color], [decline_number], [mini_terminal], [expiration_date], [out_of_stock_datetime], [bonus_row], [material_bonus_connect], [quantity_within_invoice],  [expected_material_quantity], [material_quantity], [bonus_quantity], [material_price],  [supplier_discount_percent], [material_discount_percent], [material_discount_amount], [bonus_discount], [material_price_after_disc], [sell_price], [current_catalog_sell_price], [details], [batch_number], [recieved_date], [batch_expiry_date], [quantity_in], [quantity_out]) VALUES (0, @station_number, @delete_point, @serial_number, @update_datetime, @del_emp_number, @delete_option, @supplier_number, @order_number, @doc_name, @doc_type, @doc_number, @employee_number, @move_datetime, @supplier_datetime,@state, @b2b_status, @doc_total_amount, @discount_percent, @item_state, @last_update_datetime,@indicator, @packing_list_number, @packing_serial, @import_type, @color, @decline_number, @mini_terminal, @expiration_date, @out_of_stock_datetime, @bonus_row, @material_bonus_connect, @quantity_within_invoice, @expected_material_quantity, @material_quantity, @bonus_quantity, @material_price, @supplier_discount_percent, @material_discount_percent, @material_discount_amount, @bonus_discount, @material_price_after_disc, @sell_price, @current_catalog_sell_price, @details, @batch_number, @recieved_date, @batch_expiry_date, @quantity_in, @quantity_out)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[invpack_deleted_items] SET [branch_number] = @p1, [station_number] = @p2, [delete_point] = @p3, [serial_number] = @p4, [update_datetime] = @p5, [del_emp_number] = @p6, [delete_option] = @p7, [supplier_number] = @p8, [order_number] = @p9, [doc_name] = @p10, [doc_type] = @p11, [doc_number] = @p12, [employee_number] = @p13, [move_datetime] = @p14, [supplier_datetime] = @p15, [state] = @p16, [b2b_status] = @p17, [doc_total_amount] = @p18, [discount_percent] = @p19, [item_state] = @p20, [last_update_datetime] = @p21, [indicator] = @p22, [packing_list_number] = @p23, [packing_serial] = @p24, [import_type] = @p25, [color] = @p26, [decline_number] = @p27, [mini_terminal] = @p28, [expiration_date] = @p29, [out_of_stock_datetime] = @p30, [bonus_row] = @p31, [material_bonus_connect] = @p32, [quantity_within_invoice] = @p33, [expected_material_quantity] = @p34, [material_quantity] = @p35, [bonus_quantity] = @p36, [material_price] = @p37, [supplier_discount_percent] = @p38, [material_discount_percent] = @p39, [material_discount_amount] = @p40, [bonus_discount] = @p41, [material_price_after_disc] = @p42, [sell_price] = @p43, [current_catalog_sell_price] = @p44, [details] = @p45, [batch_number] = @p46, [recieved_date] = @p47, [batch_expiry_date] = @p48, [quantity_in] = @p49, [quantity_out] = @p50 WHERE (([branch_number] = @p51) AND ([station_number] = @p52) AND ([delete_point] = @p53) AND ([serial_number] = @p54) AND ([update_datetime] = @p55))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[invpack_deleted_items] SET [branch_number] = @p1, [station_number] = @p2, [delete_point] = @p3, [serial_number] = @p4, [update_datetime] = @p5, [del_emp_number] = @p6, [delete_option] = @p7, [supplier_number] = @p8, [order_number] = @p9, [doc_name] = @p10, [doc_type] = @p11, [doc_number] = @p12, [employee_number] = @p13, [move_datetime] = @p14, [supplier_datetime] = @p15, [state] = @p16, [b2b_status] = @p17, [doc_total_amount] = @p18, [discount_percent] = @p19, [item_state] = @p20, [last_update_datetime] = @p21, [indicator] = @p22, [packing_list_number] = @p23, [packing_serial] = @p24, [import_type] = @p25, [color] = @p26, [decline_number] = @p27, [mini_terminal] = @p28, [expiration_date] = @p29, [out_of_stock_datetime] = @p30, [bonus_row] = @p31, [material_bonus_connect] = @p32, [quantity_within_invoice] = @p33, [expected_material_quantity] = @p34, [material_quantity] = @p35, [bonus_quantity] = @p36, [material_price] = @p37, [supplier_discount_percent] = @p38, [material_discount_percent] = @p39, [material_discount_amount] = @p40, [bonus_discount] = @p41, [material_price_after_disc] = @p42, [sell_price] = @p43, [current_catalog_sell_price] = @p44, [details] = @p45, [batch_number] = @p46, [recieved_date] = @p47, [batch_expiry_date] = @p48, [quantity_in] = @p49, [quantity_out] = @p50 WHERE (([branch_number] = @p51) AND ([station_number] = @p52) AND ([delete_point] = @p53) AND ([serial_number] = @p54) AND ([update_datetime] = @p55) AND ([del_emp_number] = @p56) AND ([delete_option] = @p57) AND ([supplier_number] = @p58) AND ([order_number] = @p59) AND ([doc_name] = @p60) AND ([doc_type] = @p61) AND ([doc_number] = @p62) AND ([employee_number] = @p63) AND ([move_datetime] = @p64) AND ([supplier_datetime] = @p65) AND ([state] = @p66) AND ([b2b_status] = @p67) AND ([doc_total_amount] = @p68) AND ([discount_percent] = @p69) AND ((@p70 = 1 AND [item_state] IS NULL) OR ([item_state] = @p71)) AND ((@p72 = 1 AND [last_update_datetime] IS NULL) OR ([last_update_datetime] = @p73)) AND ((@p74 = 1 AND [indicator] IS NULL) OR ([indicator] = @p75)) AND ([packing_list_number] = @p76) AND ([packing_serial] = @p77) AND ((@p78 = 1 AND [import_type] IS NULL) OR ([import_type] = @p79)) AND ((@p80 = 1 AND [color] IS NULL) OR ([color] = @p81)) AND ((@p82 = 1 AND [decline_number] IS NULL) OR ([decline_number] = @p83)) AND ((@p84 = 1 AND [mini_terminal] IS NULL) OR ([mini_terminal] = @p85)) AND ((@p86 = 1 AND [expiration_date] IS NULL) OR ([expiration_date] = @p87)) AND ((@p88 = 1 AND [out_of_stock_datetime] IS NULL) OR ([out_of_stock_datetime] = @p89)) AND ((@p90 = 1 AND [bonus_row] IS NULL) OR ([bonus_row] = @p91)) AND ((@p92 = 1 AND [material_bonus_connect] IS NULL) OR ([material_bonus_connect] = @p93)) AND ([quantity_within_invoice] = @p94) AND ((@p95 = 1 AND [expected_material_quantity] IS NULL) OR ([expected_material_quantity] = @p96)) AND ([material_quantity] = @p97) AND ((@p98 = 1 AND [bonus_quantity] IS NULL) OR ([bonus_quantity] = @p99)) AND ([material_price] = @p100) AND ((@p101 = 1 AND [supplier_discount_percent] IS NULL) OR ([supplier_discount_percent] = @p102)) AND ([material_discount_percent] = @p103) AND ((@p104 = 1 AND [material_discount_amount] IS NULL) OR ([material_discount_amount] = @p105)) AND ((@p106 = 1 AND [bonus_discount] IS NULL) OR ([bonus_discount] = @p107)) AND ([material_price_after_disc] = @p108) AND ((@p109 = 1 AND [sell_price] IS NULL) OR ([sell_price] = @p110)) AND ((@p111 = 1 AND [current_catalog_sell_price] IS NULL) OR ([current_catalog_sell_price] = @p112)) AND ([details] = @p113) AND ((@p114 = 1 AND [batch_number] IS NULL) OR ([batch_number] = @p115)) AND ((@p116 = 1 AND [recieved_date] IS NULL) OR ([recieved_date] = @p117)) AND ((@p118 = 1 AND [batch_expiry_date] IS NULL) OR ([batch_expiry_date] = @p119)) AND ((@p120 = 1 AND [quantity_in] IS NULL) OR ([quantity_in] = @p121)) AND ((@p122 = 1 AND [quantity_out] IS NULL) OR ([quantity_out] = @p123)))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[invpack_deleted_items] WHERE (([branch_number] = @p1) AND ([station_number] = @p2) AND ([delete_point] = @p3) AND ([serial_number] = @p4) AND ([update_datetime] = @p5))";

        /// <summary>
        /// Initializes a new instance of the <see cref="ds_invpack_deleted_itemsService"/> class.
        /// </summary>
		public ds_invpack_deleted_itemsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			ds_invpack_deleted_items dataRow = null;
		dataRow = new ds_invpack_deleted_items();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.station_number = 0;
			dataRow.delete_point = 0;
			dataRow.serial_number = 0;
			dataRow.update_datetime = new DateTime(1900,1,1);
			dataRow.del_emp_number = 0;
			dataRow.delete_option = 0;
			dataRow.supplier_number = 0;
			dataRow.order_number = 0;
			dataRow.doc_name = "doc_name_0";
			dataRow.doc_type = "doc_type_0";
			dataRow.doc_number = 0;
			dataRow.employee_number = 0;
			dataRow.move_datetime = new DateTime(1900,1,1);
			dataRow.supplier_datetime = new DateTime(1900,1,1);
			dataRow.state = "state_0";
			dataRow.b2b_status = 0;
			dataRow.doc_total_amount = 0;
			dataRow.discount_percent = 0;
			dataRow.item_state = "item_state_0";
			dataRow.last_update_datetime = new DateTime(1900,1,1);
			dataRow.indicator = 0;
			dataRow.packing_list_number = 0;
			dataRow.packing_serial = 0;
			dataRow.import_type = "import_type_0";
			dataRow.color = 0;
			dataRow.decline_number = 0;
			dataRow.mini_terminal = 0;
			dataRow.expiration_date = new DateTime(1900,1,1);
			dataRow.out_of_stock_datetime = new DateTime(1900,1,1);
			dataRow.bonus_row = "bonus_row_0";
			dataRow.material_bonus_connect = 0;
			dataRow.quantity_within_invoice = 0;
			dataRow.expected_material_quantity = 0;
			dataRow.material_quantity = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_price = 0;
			dataRow.supplier_discount_percent = 0;
			dataRow.material_discount_percent = 0;
			dataRow.material_discount_amount = 0;
			dataRow.bonus_discount = 0;
			dataRow.material_price_after_disc = 0;
			dataRow.sell_price = 0;
			dataRow.current_catalog_sell_price = 0;
			dataRow.details = "details_0";
			dataRow.batch_number = "batch_number_0";
			dataRow.recieved_date = new DateTime(1900,1,1);
			dataRow.batch_expiry_date = new DateTime(1900,1,1);
			dataRow.quantity_in = 0;
			dataRow.quantity_out = 0;
		dataRow = new ds_invpack_deleted_items();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.station_number = 1;
			dataRow.delete_point = 1;
			dataRow.serial_number = 1;
			dataRow.update_datetime = new DateTime(1901,2,2);
			dataRow.del_emp_number = 1;
			dataRow.delete_option = 1;
			dataRow.supplier_number = 1;
			dataRow.order_number = 1;
			dataRow.doc_name = "doc_name_1";
			dataRow.doc_type = "doc_type_1";
			dataRow.doc_number = 1;
			dataRow.employee_number = 1;
			dataRow.move_datetime = new DateTime(1901,2,2);
			dataRow.supplier_datetime = new DateTime(1901,2,2);
			dataRow.state = "state_1";
			dataRow.b2b_status = 1;
			dataRow.doc_total_amount = 1;
			dataRow.discount_percent = 1;
			dataRow.item_state = "item_state_1";
			dataRow.last_update_datetime = new DateTime(1901,2,2);
			dataRow.indicator = 1;
			dataRow.packing_list_number = 1;
			dataRow.packing_serial = 1;
			dataRow.import_type = "import_type_1";
			dataRow.color = 1;
			dataRow.decline_number = 1;
			dataRow.mini_terminal = 1;
			dataRow.expiration_date = new DateTime(1901,2,2);
			dataRow.out_of_stock_datetime = new DateTime(1901,2,2);
			dataRow.bonus_row = "bonus_row_1";
			dataRow.material_bonus_connect = 1;
			dataRow.quantity_within_invoice = 1;
			dataRow.expected_material_quantity = 1;
			dataRow.material_quantity = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_price = 1;
			dataRow.supplier_discount_percent = 1;
			dataRow.material_discount_percent = 1;
			dataRow.material_discount_amount = 1;
			dataRow.bonus_discount = 1;
			dataRow.material_price_after_disc = 1;
			dataRow.sell_price = 1;
			dataRow.current_catalog_sell_price = 1;
			dataRow.details = "details_1";
			dataRow.batch_number = "batch_number_1";
			dataRow.recieved_date = new DateTime(1901,2,2);
			dataRow.batch_expiry_date = new DateTime(1901,2,2);
			dataRow.quantity_in = 1;
			dataRow.quantity_out = 1;
		dataRow = new ds_invpack_deleted_items();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.station_number = 2;
			dataRow.delete_point = 2;
			dataRow.serial_number = 2;
			dataRow.update_datetime = new DateTime(1902,3,3);
			dataRow.del_emp_number = 2;
			dataRow.delete_option = 2;
			dataRow.supplier_number = 2;
			dataRow.order_number = 2;
			dataRow.doc_name = "doc_name_2";
			dataRow.doc_type = "doc_type_2";
			dataRow.doc_number = 2;
			dataRow.employee_number = 2;
			dataRow.move_datetime = new DateTime(1902,3,3);
			dataRow.supplier_datetime = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			dataRow.b2b_status = 2;
			dataRow.doc_total_amount = 2;
			dataRow.discount_percent = 2;
			dataRow.item_state = "item_state_2";
			dataRow.last_update_datetime = new DateTime(1902,3,3);
			dataRow.indicator = 2;
			dataRow.packing_list_number = 2;
			dataRow.packing_serial = 2;
			dataRow.import_type = "import_type_2";
			dataRow.color = 2;
			dataRow.decline_number = 2;
			dataRow.mini_terminal = 2;
			dataRow.expiration_date = new DateTime(1902,3,3);
			dataRow.out_of_stock_datetime = new DateTime(1902,3,3);
			dataRow.bonus_row = "bonus_row_2";
			dataRow.material_bonus_connect = 2;
			dataRow.quantity_within_invoice = 2;
			dataRow.expected_material_quantity = 2;
			dataRow.material_quantity = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_price = 2;
			dataRow.supplier_discount_percent = 2;
			dataRow.material_discount_percent = 2;
			dataRow.material_discount_amount = 2;
			dataRow.bonus_discount = 2;
			dataRow.material_price_after_disc = 2;
			dataRow.sell_price = 2;
			dataRow.current_catalog_sell_price = 2;
			dataRow.details = "details_2";
			dataRow.batch_number = "batch_number_2";
			dataRow.recieved_date = new DateTime(1902,3,3);
			dataRow.batch_expiry_date = new DateTime(1902,3,3);
			dataRow.quantity_in = 2;
			dataRow.quantity_out = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, ds_invpack_deleted_items model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.station_number = GetValue(record, "station_number", Convert.ToInt64);
            model.delete_point = GetValue(record, "delete_point", Convert.ToInt64);
            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.update_datetime = GetDateTime(record, "update_datetime");
            model.del_emp_number = GetValue(record, "del_emp_number", Convert.ToInt64);
            model.delete_option = GetValue(record, "delete_option", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.doc_name = GetValue(record, "doc_name", Convert.ToString);
            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.doc_number = GetValue(record, "doc_number", Convert.ToInt64);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.move_datetime = GetDateTime(record, "move_datetime");
            model.supplier_datetime = GetDateTime(record, "supplier_datetime");
            model.state = GetValue(record, "state", Convert.ToString);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.doc_total_amount = GetValue(record, "doc_total_amount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.item_state = GetValue(record, "item_state", Convert.ToString);
            model.last_update_datetime = GetDateTime(record, "last_update_datetime");
            model.indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.packing_list_number = GetValue(record, "packing_list_number", Convert.ToInt64);
            model.packing_serial = GetValue(record, "packing_serial", Convert.ToInt64);
            model.import_type = GetValue(record, "import_type", Convert.ToString);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.decline_number = GetValue(record, "decline_number", Convert.ToInt64);
            model.mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.expiration_date = GetDateTime(record, "expiration_date");
            model.out_of_stock_datetime = GetDateTime(record, "out_of_stock_datetime");
            model.bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.expected_material_quantity = GetValue(record, "expected_material_quantity", Convert.ToDecimal);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.material_price_after_disc = GetValue(record, "material_price_after_disc", Convert.ToDecimal);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.details = GetValue(record, "details", Convert.ToString);
            model.batch_number = GetValue(record, "batch_number", Convert.ToString);
            model.recieved_date = GetDateTime(record, "recieved_date");
            model.batch_expiry_date = GetDateTime(record, "batch_expiry_date");
            model.quantity_in = GetValue(record, "quantity_in", Convert.ToDecimal);
            model.quantity_out = GetValue(record, "quantity_out", Convert.ToDecimal);
		}
    }
}
