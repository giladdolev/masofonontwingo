using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_summery_inv_packRepository : RepositoryBase<d_summery_inv_pack>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.branch_number, invoice_move.invoice_number AS doc_no, invoice_move.supplier_number, SUM(invoice_details.expected_material_quantity) AS TOTAL FROM invoice_details, invoice_move WHERE invoice_move.branch_number = invoice_details.branch_number AND invoice_move.supplier_number = invoice_details.supplier_number AND invoice_move.invoice_type = 'p' AND invoice_details.branch_number = @0 AND invoice_details.supplier_number = @1 AND invoice_move.order_number = @2 AND Lower(invoice_move.state) <> 'd' AND invoice_move.invoice_number = invoice_details.invoice_number GROUP BY invoice_details.branch_number, invoice_move.supplier_number, invoice_move.invoice_number UNION ALL SELECT packing_list_details.branch_number, packing_list_details.pack_list_number AS doc_no, packing_list_move.supplier_number, SUM(packing_list_details.expected_material_quantity) AS TOTAL FROM packing_list_details, packing_list_move WHERE packing_list_move.branch_number = packing_list_details.branch_number AND packing_list_move.supplier_number = packing_list_details.supplier_number AND packing_list_move.packing_type = 'p' AND packing_list_details.branch_number = @0 AND packing_list_details.supplier_number = @1 AND packing_list_move.order_number = @2 AND Lower(packing_list_move.state) <> 'd' AND packing_list_details.pack_list_number = packing_list_move.pack_list_number GROUP BY packing_list_details.branch_number, packing_list_move.supplier_number, packing_list_details.pack_list_number ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_summery_inv_packService"/> class.
        /// </summary>
		public d_summery_inv_packRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_summery_inv_pack dataRow = null;
		dataRow = new d_summery_inv_pack();
		data[0] = dataRow;
			dataRow.invoice_details_branch_number = 0;
			dataRow.doc_no = 0;
			dataRow.invoice_move_supplier_number = 0;
			dataRow.total = 0;
		dataRow = new d_summery_inv_pack();
		data[1] = dataRow;
			dataRow.invoice_details_branch_number = 1;
			dataRow.doc_no = 1;
			dataRow.invoice_move_supplier_number = 1;
			dataRow.total = 1;
		dataRow = new d_summery_inv_pack();
		data[2] = dataRow;
			dataRow.invoice_details_branch_number = 2;
			dataRow.doc_no = 2;
			dataRow.invoice_move_supplier_number = 2;
			dataRow.total = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_summery_inv_pack model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.invoice_move_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.total = GetValue(record, "TOTAL", Convert.ToDecimal);
		}
    }
}
