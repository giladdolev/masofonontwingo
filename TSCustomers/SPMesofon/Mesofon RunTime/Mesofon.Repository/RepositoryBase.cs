using System;
using System.Collections;
using Common.Transposition.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.DataAccess;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Common.Transposition.Extensions.Data;
using System.Linq.Dynamic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Extensions;
using System.Xml.Linq;
using System.Text;

using Mesofon.Data.ServicePrintBOS;

namespace Mesofon.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : ModelBase, new()
    {
        private object[] _parameters;
        private Dictionary<string, Dictionary<string, object>> _lookupTables;
        private string _selectQuery;
        private string _updateQueryByKey;
        private string _updateQueryByFields;
        private string _deleteQuery;
        private string _insertQuery;
        private bool _useMock;
        private Lazy<List<T>> _fullList = new Lazy<List<T>>();
        private string _filterExpression;
        private string _XMLGroupExpression;
        private string _XMLGroupMovePropertiesToAttribute;
        private readonly Lazy<List<PropertyInfo>> _columnsPropertyInfo = new Lazy<List<PropertyInfo>>();
        private string _sortExpression;
        private string _sortExpressionInit;
        private CommandType _commandType = CommandType.Text;
        private string _export;
        //store all initial values of model
        private Dictionary<string, object> _initialValues;

        //For print
        private Dictionary<string, string> template = new Dictionary<string, string>()
            {
                {"d_diff_order_mt_report_print","0001"},
                {"d_diff_inv_mt_report_print","0002"},
                {"d_mini_terminal_driver_report_header","0003"},
                {"d_return_marlog_print_rows2","0004"},
                {"d_return_marlog_print_rows","0005"},
                {"d_not_arrived_invoces_print","0006"},
                {"d_mini_terminal_b2b_report" ,"0007"},
                {"d_diff_order_shipment_report_print","0008"},
                {"d_crossdoc_driver_report_header" ,"0009"}
            };

        /// <summary>
        /// Constractor of RepositoryBase class
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="updateQueryByKey"></param>
        /// <param name="updateQueryByFields"></param>
        /// <param name="deleteQuery"></param>
        /// <param name="insertQuery"></param>
        protected RepositoryBase(string selectQuery, string updateQueryByKey, string updateQueryByFields, string deleteQuery, string insertQuery)
        {
            _selectQuery = selectQuery;
            _updateQueryByFields = updateQueryByFields;
            _updateQueryByKey = updateQueryByKey;
            _deleteQuery = deleteQuery;
            _insertQuery = insertQuery;
            _useMock = Convert.ToBoolean(ConfigurationManager.AppSettings["UseMock"]);
            _initialValues = new Dictionary<string, object>();
            FillColumnsInfo();
        }

        /// <summary>
        /// Implementation of CommandType property
        /// </summary>
        protected CommandType CommandType
        {
            get { return _commandType; }
            set { _commandType = value; }
        }
        /// <summary>
        /// Implementation of LookupTables property
        /// </summary>
        public Dictionary<string, Dictionary<string, object>> LookupTables
        {
            get
            {
                if (_lookupTables == null)
                    _lookupTables = new Dictionary<string, Dictionary<string, object>>();
                return _lookupTables;
            }
            set
            {
                _lookupTables = value;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="TU">Field data type</typeparam>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="convert">The conversion method.</param>
        /// <returns></returns>
        protected TU GetValue<TU>(IDataRecord reader, string fieldName, Func<object, TU> convert)
        {
            return UnitOfWork.GetValue(reader, fieldName, convert);
        }



        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        protected DateTime? GetDateTime(IDataRecord reader, string fieldName)
        {
            return UnitOfWork.GetDateTime(reader, fieldName);
        }

        public List<T> PrimaryList
        {
            set
            {
                _fullList = new Lazy<List<T>>(() => value);
            }
            get
            {
                return _fullList != null ? new ModelList<T>(this, _fullList.Value.Where(model => model.Buffer == ModelBuffer.Primary).ToList()) : null;
            }
        }

        public List<T> DeletedList
        {
            get
            {
                return _fullList.Value.Where(model => model.Buffer == ModelBuffer.Delete).ToList();
            }
        }

        public List<T> FilteredList
        {
            get
            {
                return _fullList.Value.Where(model => model.Buffer == ModelBuffer.Filter).ToList();
            }
        }
        /// <summary>
        /// Obtains the number of rows that are currently available in the primary buffer.
        /// </summary>
        /// <returns>Returns the number of rows that are currently available, 0 if no rows are currently available</returns>
        public int RowCount()
        {
            return _fullList.Value.Where(model => model.Buffer == ModelBuffer.Primary).ToList().Count;
        }

        public abstract void Map(IDataRecord record, T model);

        protected abstract ModelBase[] GetMockData();

        /// <summary>
        /// Retrieves data from the database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Returns the number of rows displayed (that is, rows in the primary buffer)</returns>
        public long Retrieve(params object[] parameters)
        {
            _parameters = parameters;
            _fullList.Value.Clear();


            if (_useMock)
            {
                ModelBase[] list = GetMockData();
                _fullList = new Lazy<List<T>>(() => list.Cast<T>().ToList());

            }
            else
            {

                using (UnitOfWork uof = new UnitOfWork())
                {
                    List<T> models = uof.Retrieve<T>(_selectQuery, Map, _commandType, parameters);
                    _fullList = new Lazy<List<T>>(() => models);

                    //Check if there is a compute fields
                    var computeAttributes = this.GetTypeOfModel().GetCustomAttributes<ComputeAttribute>();
                    if (computeAttributes != null && computeAttributes.Count() > 0)
                    {
                        foreach (ComputeAttribute att in computeAttributes)
                        {
                            var property = att.propertyName;
                            var expArr = att.computeExpression.Split(' ');
                            if (expArr.Length == 2)
                            {
                                var action = expArr[0];
                                var actionON = expArr[1];
                                object value = "";
                                if (action.ToUpper() == "SUM")
                                {
                                    int sum = 0;
                                    foreach (var item in models)
                                    {
                                        var valueofmodel = this.GetTypeOfModel().GetProperty(actionON).GetValue(item, null);
                                        sum += Convert.ToInt32(valueofmodel);
                                    }
                                    value = sum;
                                }
                                else if (action.ToUpper() == "COUNT")
                                {
                                    value = models.Count;
                                }
                                else if (action.ToUpper() == "ROWCOUNT()")
                                {
                                    value = models.Count;
                                }

                                //set value
                                foreach (var item in models)
                                {
                                    this.GetTypeOfModel().GetProperty(property).SetValue(item, value);
                                }
                            }
                            else
                            {
                                throw new Exception("GalilCS - Not supported");
                            }
                        }
                    }

                }
            }
            return _fullList.Value.Count;
        }
        //gilad test to get short list of shipments
        public long Retrieve2(params object[] parameters)
        {
            _parameters = parameters;
            _fullList.Value.Clear();

                //    private static readonly string _selectQuery = "SELECT DISTINCT convert(varchar, shipment_pallets.date_move, 103) AS date_move, shipment_pallets.shipment_number FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.marlog_number = @1) ORDER BY shipment_pallets.shipment_number DESC";


            if (_useMock)
            {
                ModelBase[] list = GetMockData();
                _fullList = new Lazy<List<T>>(() => list.Cast<T>().ToList());

            }
            else
            {
                _selectQuery = "SELECT DISTINCT TOP 50 convert(varchar, shipment_pallets.date_move, 103) AS date_move, shipment_pallets.shipment_number FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.marlog_number = @1) ORDER BY shipment_pallets.shipment_number DESC"; 
                using (UnitOfWork uof = new UnitOfWork())
                {
                    List<T> models = uof.Retrieve<T>(_selectQuery, Map, _commandType, parameters);
                    _fullList = new Lazy<List<T>>(() => models);

                    //Check if there is a compute fields
                    var computeAttributes = this.GetTypeOfModel().GetCustomAttributes<ComputeAttribute>();
                    if (computeAttributes != null && computeAttributes.Count() > 0)
                    {
                        foreach (ComputeAttribute att in computeAttributes)
                        {
                            var property = att.propertyName;
                            var expArr = att.computeExpression.Split(' ');
                            if (expArr.Length == 2)
                            {
                                var action = expArr[0];
                                var actionON = expArr[1];
                                object value = "";
                                if (action.ToUpper() == "SUM")
                                {
                                    int sum = 0;
                                    foreach (var item in models)
                                    {
                                        var valueofmodel = this.GetTypeOfModel().GetProperty(actionON).GetValue(item, null);
                                        sum += Convert.ToInt32(valueofmodel);
                                    }
                                    value = sum;
                                }
                                else if (action.ToUpper() == "COUNT")
                                {
                                    value = models.Count;
                                }
                                else if (action.ToUpper() == "ROWCOUNT()")
                                {
                                    value = models.Count;
                                }

                                //set value
                                foreach (var item in models)
                                {
                                    this.GetTypeOfModel().GetProperty(property).SetValue(item, value);
                                }
                            }
                            else
                            {
                                throw new Exception("GalilCS - Not supported");
                            }
                        }
                    }

                }
            }
            return _fullList.Value.Count;
        }

        private void FillColumnsInfo()
        {
            _columnsPropertyInfo.Value.Clear();
            T model = CreateNewModel();
            _columnsPropertyInfo.Value.AddRange(model.GetType().GetProperties());
        }

        /// <summary>
        /// Displays rows in a repository that pass the current filter criteria. Rows that do not meet the filter criteria are moved to the filter buffer.
        /// The Filter method retrieves all rows before applying the filter.
        /// To change the filter criteria, use the SetFilter method. 
        /// </summary>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public int Filter()
        {
            try
            {
                if (_filterExpression == "")
                {
                    foreach (T model in _fullList.Value)
                    {
                        if (model.Buffer == ModelBuffer.Filter)
                        {
                            model.Buffer = ModelBuffer.Primary;
                        }
                    }
                    return 1;
                }
                // Filter rows that do not meet the filter criteria
                IEnumerable<T> filteredList = _fullList.Value.Where(string.Format("not ({0})", _filterExpression));

                // Change filter to filter buffer
                foreach (T model in filteredList)
                {
                    model.Buffer = ModelBuffer.Filter;
                }
                return 1;
            }
            catch (Exception)
            {

                return -1;
            }
        }

        /// <summary>
        /// Gets the filter expression
        /// </summary>
        /// <returns>Returns filter expression</returns>
        public string GetFilter()
        {
            return _filterExpression;
        }

        /// <summary>
        /// Specifies filter criteria for a repository.
        /// </summary>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the filter criteria. The expression includes column names or numbers. A column number must be preceded by a pound sign (#)</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int SetFilter(string expression)
        {
            try
            {
                expression = GetExpressionWithColumnsNames(expression ?? "");
            }
            catch (Exception e)
            {
                return -1;
            }

            _filterExpression = expression;
            return 1;
        }

        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the search criterion. The expression includes column names.</param>
        /// <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// �1 General error
        /// �5 Bad argument</returns>
        public int? Find(string expression, int start, int end)
        {
            try
            {
                if (string.IsNullOrEmpty(expression))
                {
                    return null;
                }

                List<T> primaryList = PrimaryList;

                // Search forward or backward
                bool toSearchBackward = start > end;

                // Calculate index to last item
                end = Math.Min(primaryList.Count, end);

                // Calculate total items
                int count = Math.Abs(end - start);

                // Normalized the expression 
                expression = GetExpressionWithColumnsNames(expression);

                // Take sub list
                IEnumerable<T> findList = PrimaryList.Skip(start).Take(count).Where(expression);

                // Find the first item that matches the criteria
                var findItem = toSearchBackward ? findList.LastOrDefault() : findList.FirstOrDefault();

                // Return the row number
                return primaryList.FindIndex(model => model == findItem);

            }
            catch (Exception e)
            {
                return -1;
            }
            _sortExpression = expression;


            return 1;
        }

        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="columnName">A string of column names for The expression.</param>
        /// <param name="searchFor">A string whose value is a boolean expression that you want to use as the search criterion. The expression.</param>
        ///  <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// �1 General error
        /// �5 Bad argument</returns>
        public int? Find(string columnName, string searchFor, int start, int end)
        {
            try
            {
                if (string.IsNullOrEmpty(searchFor))
                {
                    return null;
                }

                List<T> primaryList = PrimaryList;

                // Search forward or backward
                bool toSearchBackward = start > end;

                // Calculate index to last item
                end = Math.Min(primaryList.Count, end);

                // Calculate total items
                int count = Math.Abs(end - start);

                // Normalized the expression 
                searchFor = GetExpressionWithColumnsNames(searchFor);

                // Take sub list
                //IEnumerable<T> findList = PrimaryList.Skip(start).Take(count).Where(expression);
                IEnumerable<T> findList = PrimaryList.Skip(start).Take(count).Where(_model =>
                Convert.ToString(_columnsPropertyInfo.Value.FirstOrDefault(property => property.Name == columnName).GetValue(_model, null)) == searchFor
                );

                // Find the first item that matches the criteria
                var findItem = toSearchBackward ? findList.LastOrDefault() : findList.FirstOrDefault();

                // Return the row number
                return primaryList.FindIndex(model => model == findItem);

            }
            catch (Exception e)
            {
                return -1;
            }




            return 1;
        }

        /// <summary>
        /// Specifies sort criteria for a repository.
        /// </summary>
        /// <param name="expression">
        /// A string whose value is valid sort criteria for the repository. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).
        /// A repository can have sort criteria specified as part of its definition. SetSort overrides the definition, providing new sort criteria for the repository. However, it does not actually sort the rows. Call the Sort method to perform the actual sorting.
        /// </param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int SetSort(string expression)
        {
            try
            {
                expression = GetExpressionWithColumnsNames(expression);
                _sortExpression = expression;
            }
            catch (Exception e)
            {
                return -1;
            }
            return 1;
        }

        /// <summary>
        /// Gets sort expression
        /// </summary>
        /// <returns>Returns string of sort expression</returns>
        public string GetSort()
        {
            return _sortExpression;
        }



        /// <summary>
        /// Sort criteria for a newly created repository. To specify sorting for existing repository.
        /// </summary>
        /// <param name="expression">
        /// A string whose value represents valid sort criteria.
        /// </param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        public int Sort(string expression = "")
        {
            try
            {
                expression = GetExpressionWithColumnsNames(expression);
                _sortExpressionInit = expression;

                // New sort
                if (!string.IsNullOrEmpty(_sortExpression))
                {
                    var orderedList = _fullList.Value.OrderBy(_sortExpression);
                    _fullList = new Lazy<List<T>>(() => orderedList.Cast<T>().ToList());

                }
                else
                {
                    // Reset to sort initialized sort
                    if (!string.IsNullOrEmpty(_sortExpressionInit))
                    {
                        var orderedList = _fullList.Value.OrderBy(_sortExpressionInit);
                        _fullList = new Lazy<List<T>>(() => orderedList.Cast<T>().ToList());
                    }
                }


            }
            catch (Exception e)
            {
                return -1;
            }
            return 1;
        }

        public virtual void Insert(T model)
        {
            model.Action = ModelAction.Insert;
            _fullList.Value.Add(model);
        }

        private string GetExpressionWithColumnsNames(string expression)
        {
            // Find columns number (start with #)
            Regex regex = new Regex(@"#\d+", RegexOptions.None);

            foreach (Match match in regex.Matches(expression))
            {
                if (match.Success)
                {
                    // Get column name by removing # prefix
                    int columnNumber = Convert.ToInt32(match.Value.Remove(0, 1)) - 1;

                    // replace #colId with column name
                    expression = Regex.Replace(expression, match.Value + @"\b", _columnsPropertyInfo.Value[columnNumber].Name);
                }
            }

            return expression;
        }

        /// <summary>
        /// Insert a new item to repository
        /// </summary>
        /// <param name="row">A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0..</param>
        /// <returns>Returns the number of the row that was added if it succeeds and �1 if an error occurs.</returns>
        public virtual int Insert(int row)
        {
            try
            {
                // Add new item to end of list
                if (row == 0 || row >= _fullList.Value.Count)
                {
                    T model = CreateNewModel();
                    Insert(model);
                    return RowCount() - 1;
                }

                // Add new item to list before the row which you want to insert
                _fullList.Value.Insert(row - 1, CreateNewModel());
                return row;

            }
            catch (Exception)
            {

                return -1;
            }
        }
        public virtual void Delete(T model)
        {
            if (model != null)
            {
                model.Action = ModelAction.Delete;
                model.Buffer = ModelBuffer.Delete;
            }
        }

        /// <summary>
        /// Deletes the specified row.
        /// </summary>
        /// <param name="row">A value identifying the row you want to delete. To delete the current row, specify 0 for row.</param>

        public virtual void Delete(int row)
        {
            if (row <= PrimaryList.Count)
            {
                Delete(PrimaryList[row]);
            }
        }

        /// <summary>
        /// Use RowsDiscard when your application is finished with some of the rows in a DataWindow control and you do not want an update to affect the rows in the database
        /// </summary>
        /// <param name="row">A value identifying the row you want to discard.</param>
        public virtual void RowsDiscard(int row)
        {
            if (row <= PrimaryList.Count)
            {
                T model = PrimaryList[row];
                model.Action = ModelAction.Discard;
            }
        }



        public virtual void UpdateByKeys(T model)
        {
            Update(model, model, ModelAction.UpdateByKeys);
        }

        public virtual void UpdateByFields(T model, T originalModel)
        {
            Update(model, originalModel, ModelAction.UpdateByFields);
        }

        /// <summary>
        /// Updates the database with the changes made
        /// </summary>
        /// <param name="uow"></param>
        /// <returns></returns>
        public long Update(IUnitOfWork uow)
        {
            long result = 1;

            // Get all changed items
            var changedItems = GetAllChangedItems();
            if (changedItems.Count() > 0)
            {
                foreach (T item in changedItems)
                {
                    Dictionary<string, object> parameters = GetParameters(item);
                    Dictionary<string, object> pkParameters = GetParameters(item, true);

                    switch (item.Action)
                    {
                        case ModelAction.Delete:
                            uow.Execute(_deleteQuery, pkParameters);
                            break;
                        case ModelAction.Insert:
                            uow.Execute(_insertQuery, parameters);
                            break;
                        case ModelAction.UpdateByFields:
                            uow.Execute(_updateQueryByFields, parameters);
                            break;
                        case ModelAction.UpdateByKeys:
                            uow.Execute(_updateQueryByKey, parameters);
                            break;
                        case ModelAction.DeleteThenInsert:
                            uow.Execute(_deleteQuery, pkParameters);
                            uow.Execute(_insertQuery, parameters);
                            break;
                        case ModelAction.Discard:
                            //Skip (Nothing to do)
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    item.Action = ModelAction.None;
                }

            }


            return result;
        }


        private Dictionary<string, object> GetParameters(T model, bool onlyKey = false)
        {
            Dictionary<string, object> primaryKeys = new Dictionary<string, object>();
            foreach (PropertyInfo property in _columnsPropertyInfo.Value)
            {
                Attribute attribute = GetColumnAttribute(property, onlyKey);
                if (attribute != null)
                {
                    primaryKeys.Add(property.Name, property.GetValue(model));
                }
            }
            return primaryKeys;

        }


        private void Update(T model, T originalModel, ModelAction action)
        {
            bool onlyKey = action == ModelAction.UpdateByKeys;

            // Searching for original model in the primary list
            T foundModel = FindModel(originalModel, onlyKey);


            if (foundModel == null)
            {
                return;
            }

            // Assigning updated model
            foundModel.Action = action;
            foreach (PropertyInfo listItemProperty in _columnsPropertyInfo.Value)
            {
                // Gets all properties
                var attribute = GetColumnAttribute(listItemProperty);
                if (attribute != null)
                {
                    PropertyInfo propertyInfo = model.GetType().GetProperty(listItemProperty.Name);
                    if (propertyInfo != null)
                    {
                        object value = propertyInfo.GetValue(foundModel, null);

                        listItemProperty.SetValue(foundModel, value, null);
                    }
                }
            }
        }

        private T FindModel(T originalModel, bool onlyKey)
        {
            // Searching for original model in the primary list
            foreach (T currentModel in PrimaryList)
            {
                bool isModelsMatch = true;
                foreach (PropertyInfo listItemProperty in _columnsPropertyInfo.Value)
                {
                    // Gets only required properties
                    var attribute = GetColumnAttribute(listItemProperty, onlyKey);
                    if (attribute != null)
                    {
                        var propertyInfo = originalModel.GetType().GetProperty(listItemProperty.Name);
                        if (propertyInfo != null)
                        {
                            object value = propertyInfo.GetValue(currentModel, null);
                            if (!object.Equals(value, listItemProperty.GetValue(currentModel, null)))
                            {
                                isModelsMatch = false;
                                break;
                            }
                        }
                    }
                }

                // Assigning updated model
                if (isModelsMatch)
                {
                    return currentModel;
                }
            }
            return default(T);
        }

        private Attribute GetColumnAttribute(PropertyInfo property, bool onlyKey = false)
        {
            if (onlyKey)
            {
                return property.GetCustomAttributes(false).OfType<KeyAttribute>().FirstOrDefault();
            }
            return property.GetCustomAttributes(false).OfType<ColumnAttribute>().FirstOrDefault();
        }

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing the update status of different tables in the DataWindow so that you can update more than one table
        /// Modifying the WHERE clause of the DataWindow object�s SQL SELECT statement
        /// Turning on Query mode or Prompt For Criteria so users can specify the data they want
        /// Changing the status of Retrieve Only As Needed
        /// Changing the data source of the DataWindow object
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// Returns the empty string (��) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument�s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        public string Modify(string expression)
        {
            string result = null;
            try
            {
                if (expression.StartsWith("DataWindow.Table.Select ="))
                {
                    var sql = expression.Replace("DataWindow.Table.Select =", "");
                    _selectQuery = sql;
                }
                else if (expression.StartsWith("Initial,"))
                {
                    //Initialiaze value when creating new model
                    //"Initial,ColumnName,Value"
                    var arrSplit = expression.Split(',');
                    if (arrSplit.Length == 3)
                    {
                        InitialValues.Add(arrSplit[1], arrSplit[2]);
                    }
                }
                //TODO: expression.Invoke();
                result = "";
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return result;
        }

        /// <summary>
        //// Evaluate expressions involving values of a particular row and column. When you include Describe�s Evaluate function in the property list, the value of the evaluated expression is included in the reported information.
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        public string Describe(string expression)
        {
            string result = null;
            try
            {
                //TODO: expression.Invoke();
                result = "expression.Invoke()";

                if (expression.Equals("Evaluate('MAX(indicator for all)',1)"))
                {
                    if (PrimaryList.Count > 0)
                    {
                        long maxIndicator = this.GetItemValue<Int64>(0, "indicator", ModelBuffer.Primary);
                        for (int i = 1; i < PrimaryList.Count; i++)
                        {
                            if (this.GetItemValue<Int64>(i, "indicator", ModelBuffer.Primary) > maxIndicator)
                            {
                                maxIndicator = this.GetItemValue<Int64>(i, "indicator", ModelBuffer.Primary);
                            }
                        }
                        result = Convert.ToString(maxIndicator);
                    }
                }
            }
            catch (Exception e)
            {
                result = @"""!""";
            }

            return result;
        }

        /// <summary>
        /// Gets the value of an item for the specified row and column
        /// </summary>
        /// <typeparam name="T1">The type of column as a return value</typeparam>
        /// <param name="row">A value identifying the row location of the data.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="buffer">The buffer.</param>
        /// <returns>Value of an item for the specified row and column</returns>
        public T1 GetItemValue<T1>(long row, string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            T model;
            PropertyInfo property = GetProperty((int)row, columnName, buffer, out model);
            return property == null ? default(T1) : (T1)property.GetValue(model, null);
        }


        /// <summary>
        /// Gets the values of an item for the specified column
        /// </summary>
        /// <typeparam name="T1">The type of column as a return value</typeparam>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="buffer">The buffer.</param>
        /// <returns>Values of an items for the specified columns</returns>
        public T1[] GetItemValues<T1>(string columnName, ModelBuffer buffer = ModelBuffer.Primary)
        {
            List<T> bufferList = GetBufferList<T>(buffer).Cast<T>().ToList();
            List<T1> list = new List<T1>();
            foreach (T model in bufferList)
            {
                PropertyInfo property = _columnsPropertyInfo.Value.FirstOrDefault(xproperty => xproperty.Name == columnName);
                if (property != null)
                {
                    list.Add((T1)property.GetValue(model, null));
                }
            }
            return list.ToArray();
        }

        /// <summary>
        /// Gets an item from buffer for specific row
        /// </summary>
        /// <typeparam name="T"></typeparam> 
        /// <param name="row"></param>
        /// <param name="buffer"></param>
        /// <returns>Returns the item in the specified row, and null or 0 if an error occurs</returns>
        public T GetItem<T>(int row, ModelBuffer buffer = ModelBuffer.Primary)
        {
            List<T> list = GetBufferList<T>(buffer).Cast<T>().ToList();
            var model = list[row];
            return model;
        }


        private PropertyInfo GetProperty(int row, string columnName, ModelBuffer buffer, out T model)
        {
            List<T> list = GetBufferList<T>(buffer).Cast<T>().ToList();
            if (list.Count > row && row >= 0)
            {
                model = list[row];
            }
            else
            {
                model = null;
            }
            // If model exists return the property info that has the same filed name as the column name
            return model == null ? null : _columnsPropertyInfo.Value.FirstOrDefault(property => property.Name == columnName);
        }

        /// <summary>
        /// Get number of the rows that have been deleted.
        /// </summary>
        /// <returns>Returns 0 if no rows have been deleted or if all the deleted rows have been updated in the database.</returns>
        public long DeletedCount()
        {
            return DeletedList != null ? DeletedList.Count : 0;
        }


        /// <summary>
        /// Get number of the rows that have been modified.
        /// </summary>
        /// <returns>Returns the number of rows that have been modified</returns>
        public long ModifiedCount()
        {
            // Get all modified items
            List<T> modifiedItems = PrimaryList.Where(model => model.Action == ModelAction.UpdateByKeys || model.Action == ModelAction.UpdateByFields).ToList();

            return modifiedItems.Count;
        }

        /// <summary>
        /// Sets the value of a row and column in a Repository to the specified value.
        /// </summary>
        /// <param name="row">The row location of the data.</param>
        /// <param name="columnName">The column location of the data.</param>
        /// <param name="value">The value to which you want to set the data at the row and column location. The datatype of the value must be the same datatype as the column.</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs. If any argument�s value is null, in PowerBuilder and JavaScript the method returns null.</returns>
        public long SetItem(long row, object columnName, object value)
        {
            T model = null;

            // Get model from list
            if (PrimaryList.Count > row)
            {
                model = PrimaryList[(int)row];
            }

            if (model != null)
            {
                foreach (PropertyInfo property in _columnsPropertyInfo.Value)
                {
                    if (property.Name == Convert.ToString(columnName))
                    {
                        if (value == null)
                        {
                            property.SetValue(model, value);
                        }
                        else if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            var propertyType = property.PropertyType.GetGenericArguments()[0];
                            property.SetValue(model, Convert.ChangeType(value, propertyType));
                        }
                        else
                        {
                            property.SetValue(model, Convert.ChangeType(value, property.PropertyType));
                        }
                        var preventFlagChange = property.GetCustomAttribute<PreventFlagChangeAttribute>();
                        if (preventFlagChange == null)
                        {
                            if (model.Action == ModelAction.None)
                            {
                                model.Action = ModelAction.UpdateByKeys;
                            }
                        }
                        return 1;
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Clears a range of rows from one repository and inserts them in another. Alternatively, RowsMove moves rows from one buffer to another within a single repository
        /// </summary>
        /// <param name="startRow">The number of the first row you want to move.</param>
        /// <param name="endRow">The number of the last row you want to move.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to move the rows.</param>
        /// <param name="target">The name of the repository to which you want to move the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the moved rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to move the rows.</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int RowsMove(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow, ModelBuffer targetBuffer)
        {
            try
            {
                if (startRow == endRow)
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        throw new Exception("the arguments startRow and endRow are equle");
                    }
                }
                if (this.GetTypeOfModel() == target.GetTypeOfModel())
                {
                    RestructureRowsList(startRow, endRow, sourceBuffer, target, beforeRow, targetBuffer, true);
                }
                else
                {
                    return CopyRowsList(startRow, endRow, sourceBuffer, target, beforeRow, targetBuffer, true);
                }
                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }


        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int RowsCopy(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow,
            ModelBuffer targetBuffer)
        {
            try
            {
                if (startRow == endRow)
                {
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        throw new Exception("the arguments startRow and endRow are equle");
                    }
                }

                if (this.GetTypeOfModel() == target.GetTypeOfModel())
                {
                    RestructureRowsList(startRow, endRow, sourceBuffer, target, beforeRow, targetBuffer, false);
                }
                else
                {
                    return CopyRowsList(startRow, endRow, sourceBuffer, target, beforeRow, targetBuffer, false);
                }
                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        private void UpdateForType(Type type, T source, T destination)
        {
            FieldInfo[] myObjectFields = type.GetFields(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            foreach (FieldInfo fi in myObjectFields)
            {
                fi.SetValue(destination, fi.GetValue(source));
            }
        }

        private int CopyRowsList(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow,
          ModelBuffer targetBuffer, bool toRemoveRows)
        {
            int iteration = 0;
            PropertyDescriptorCollection propertiesSource = TypeDescriptor.GetProperties(this.GetTypeOfModel());
            PropertyDescriptorCollection propertiesTarget = TypeDescriptor.GetProperties(target.GetTypeOfModel());
            List<T> sourceList = this.GetBufferList<T>(sourceBuffer);
            int rowsCount = endRow - startRow;

            if (propertiesSource.Count != propertiesTarget.Count)
            {
                return -1;
            }
            for (int j = startRow; j < endRow; j++)
            {
                //Add row in specific index
                var rowIndex = target.Insert(beforeRow + iteration++);
                for (int i = 0; i < propertiesSource.Count; i++)
                {
                    switch (propertiesTarget[i].PropertyType.Name)
                    {
                        case "String":
                            target.SetItem(rowIndex, propertiesTarget[i].Name, this.GetItemValue<string>(j, propertiesSource[i].Name, sourceBuffer));
                            break;
                        case "Int64":
                            target.SetItem(rowIndex, propertiesTarget[i].Name, this.GetItemValue<long>(j, propertiesSource[i].Name, sourceBuffer));
                            break;
                        case "Decimal":
                            target.SetItem(rowIndex, propertiesTarget[i].Name, this.GetItemValue<Decimal?>(j, propertiesSource[i].Name, sourceBuffer));
                            break;
                        case "DateTime":
                            target.SetItem(rowIndex, propertiesTarget[i].Name, this.GetItemValue<DateTime>(j, propertiesSource[i].Name, sourceBuffer));
                            break;
                        case "Double":
                            target.SetItem(rowIndex, propertiesTarget[i].Name, this.GetItemValue<Double>(j, propertiesSource[i].Name, sourceBuffer));
                            break;
                        case "Nullable`1":
                            if (propertiesTarget[i].PropertyType.FullName.Contains("DateTime"))
                            {
                                DateTime? val = this.GetItemValue<DateTime?>(j, propertiesSource[i].Name, sourceBuffer);
                                target.SetItem(rowIndex, propertiesTarget[i].Name, val);
                            }
                            else if (propertiesTarget[i].PropertyType.FullName.Contains("Int64"))
                            {
                                Int64? val = this.GetItemValue<Int64?>(j, propertiesSource[i].Name, sourceBuffer);
                                target.SetItem(rowIndex, propertiesTarget[i].Name, val);
                            }
                            else if (propertiesTarget[i].PropertyType.FullName.Contains("Int32"))
                            {
                                Int32? val = this.GetItemValue<Int32?>(j, propertiesSource[i].Name, sourceBuffer);
                                target.SetItem(rowIndex, propertiesTarget[i].Name, val);
                            }
                            else if (propertiesTarget[i].PropertyType.FullName.Contains("Decimal"))
                            {
                                decimal? val = this.GetItemValue<decimal?>(j, propertiesSource[i].Name, sourceBuffer);
                                target.SetItem(rowIndex, propertiesTarget[i].Name, val);
                            }
                            else
                            {
                                throw new Exception("GalilCS - TODO : Not supported");
                            }
                            break;
                        case "ModelAction":
                            {
                                object val = this.GetItemValue<object>(j, propertiesSource[i].Name, sourceBuffer);
                                target.SetItem(rowIndex, propertiesTarget[i].Name, val);
                            }
                            break;
                        case "ModelBase":
                        case "ModelBuffer":
                            continue;
                        default:
                            throw new Exception("GalilCS - TODO : Not supported");
                    }

                }


            }
            if (toRemoveRows)
            {
                sourceList.RemoveRange(startRow, rowsCount);
            }
            return 1;
        }

        private void RestructureRowsList(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow,
            ModelBuffer targetBuffer, bool toRemoveRows)
        {
            List<T> sourceList = this.GetBufferList<T>(sourceBuffer);
            List<T> targetList = target.GetBufferList<T>(targetBuffer);

            endRow = Math.Min(endRow, sourceList.Count);
            beforeRow = Math.Min(beforeRow, targetList.Count);

            int rowsCount = endRow - startRow;

            var toAdd = sourceList.GetRange(startRow, rowsCount);
            foreach (var item in toAdd)
            {
                T dest = CreateNewModel();
                UpdateForType(item.GetType(), item, dest);
                dest.Action = item.Action;
                targetList.Insert(beforeRow, dest);
                beforeRow++;
            }

            if (object.ReferenceEquals(this, target))
            {
                targetList.RemoveRange(startRow, rowsCount);
            }
            ((IRepository<T>)target).PrimaryList = targetList;
            if (toRemoveRows)
            {
                sourceList.RemoveRange(startRow, rowsCount);
            }

        }

        //gilad test
        public void AppendPrimeryList(ModelBase obj)
        {
            Insert(obj as T);
        }

        /// <summary>
        /// Gets data from buffer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="buffer">The name of the buffer from which you want to get data</param>
        /// <returns>Return buffer list</returns>
        public List<T> GetBufferList<T>(ModelBuffer buffer)
        {
            switch (buffer)
            {
                case ModelBuffer.Primary:
                    return PrimaryList.Cast<T>().ToList();
                case ModelBuffer.Delete:
                    return DeletedList.Cast<T>().ToList();
                case ModelBuffer.Filter:
                    return FilteredList.Cast<T>().ToList();
                default:
                    throw new ArgumentOutOfRangeException("buffer", buffer, null);
            }
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <returns>The return value identifies the status of the item at row, column in the specified buffer.</returns>
        public ModelAction GetItemStatus(int row, object column, ModelBuffer buffer)
        {
            int columnNumber;
            T model;

            // Get column name
            string columnName = GetColumnName(column);

            PropertyInfo property = GetProperty(row, columnName, buffer, out model);
            // TODO: Find status of the specified column
            if (property != null)
            {
                return model.Action;
            }
            return ModelAction.None;
        }

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <param name="status">A value that specifying the new status.</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int SetItemStatus(int row, object column, ModelBuffer buffer, ModelAction status)
        {
            try
            {

          //      int columnNumber;
                T model;

                string columnName = GetColumnName(column);
                // TODO: Find status of the specified column
                PropertyInfo property = GetProperty(row, "Action", buffer, out model);

                if (property != null)
                {
                    property.SetValue(model, status, null);
                }
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        private string GetColumnName(object column)
        {
            int columnNumber;
            // Get column name
            string columnName = int.TryParse(column.ToString(), out columnNumber)
                ? _columnsPropertyInfo.Value[columnNumber].Name
                : column.ToString();
            return columnName;
        }

        /// <summary>
        /// Get objects that have knowledge about the data they are retrieving.
        /// </summary>
        /// <returns>Returns a string that contains names of columns.</returns>
        public string ColumnsNames()
        {
            string returnValue = "";

            foreach (var prop in _columnsPropertyInfo.Value)
            {
                returnValue += prop.Name + "\t";
            }

            return returnValue;
        }

        public string GetValue(object column, int row, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            return GetValue(column, row);
        }

        /// <summary>
        /// Obtains the value of an item in a value list or code table associated with a column in a repository.
        /// </summary>
        /// <param name="row">The number of the item in the value list or the code table for the edit style.</param>
        /// <param name="column">The column for which you want the item. Column can be a column number (integer) or a column name (string)</param>
        /// <returns>
        /// Returns the item identified by index in the value list or the code table associated with column of repository. Returns the empty string (� �) if the index is not valid or the column does not have a value list or code table. 
        /// </returns>
        public string GetValue(object column, int row)
        {
            T model;
            string columnName = GetColumnName(column);
            PropertyInfo property = GetProperty(row, columnName, ModelBuffer.Primary, out model);

            if (property != null)
            {
                return Convert.ToString(property.GetValue(model, null));
            }
            return "";
        }

        /// <summary>
        /// Clears all the data from a repository
        /// </summary>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs. </returns>
        public int Reset()
        {
            try
            {
                _fullList.Value.Clear();
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }

        }


        /// <summary>
        /// Saves the data in XML format.
        /// </summary>
        /// <param name="filename">File name</param>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs</returns>
        public int SaveToXml(string filename)
        {
            try
            {
                if (!string.IsNullOrEmpty(_export))
                {
                    var document = new XmlDocument();
                    document.LoadXml(_export);
                    DataTable table = GetDataTable();

                    //if (table.Rows.Count == 1)
                    //{
                    //    fill data
                    //    foreach (DataColumn col in table.Columns)
                    //    {
                    //        var node = document.SelectSingleNode("//" + col.Caption);
                    //        if (node != null)
                    //        {
                    //            node.InnerText = Convert.ToString(table.Rows[0][col]);
                    //        }
                    //    }

                    //    write to file
                    //    using (StreamWriter fs = new StreamWriter(filename))
                    //    {
                    //        document.Save(fs);
                    //    }
                    //}
                    //else
                    {

                        //init list of details
                        XmlNode[] listOfDetail = new XmlNode[table.Rows.Count];
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            listOfDetail[i] = document.SelectSingleNode("//*[@__pbband]").CloneNode(true);

                            //remove __pbband attribute
                            XmlAttribute attribute = null;
                            foreach (XmlAttribute attri in listOfDetail[i].Attributes)
                            {
                                if (attri.Name == "__pbband")
                                    attribute = attri;
                            }
                            if (attribute != null)
                                listOfDetail[i].Attributes.Remove(attribute);
                        }

                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow row = table.Rows[i];

                            foreach (DataColumn col in table.Columns)
                            {
                                var node = document.SelectSingleNode("//*[text()='" + col.Caption + "']");
                                if (node != null)
                                {
                                    node.InnerText = Convert.ToString(row[col]);
                                }

                                var nodeofDetail = listOfDetail[i].SelectSingleNode("//*[text()='" + col.Caption + "']");
                                if (nodeofDetail != null)
                                {
                                    nodeofDetail.InnerText = Convert.ToString(row[col]);
                                }

                            }
                        }

                        var toRemove = document.SelectSingleNode("//*[@__pbband]");
                        var parentNodeOfPBBand = toRemove.ParentNode;
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            parentNodeOfPBBand.InsertBefore(listOfDetail[i], toRemove);
                        }
                        parentNodeOfPBBand.RemoveChild(toRemove);

                        //write to file 
                        using (StreamWriter fs = new StreamWriter(filename))
                        {
                            document.Save(fs);
                        }

                    }
                }
                else
                {
                    //GalilCS - TODO : Need to implement
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                }
            }
            catch
            {
                return -1;
            }
            return 1;
        }

        /// <summary>
        /// Creates DataTable of PrimaryList and returns it
        /// </summary>
        /// <returns>Returns DataTable</returns>
        public DataTable GetDataTable()
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable(typeof(T).Name);
            SortedList<int, DataColumn> cols = new SortedList<int, DataColumn>();
            foreach (PropertyDescriptor prop in properties)
            {
                var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                if (att != null && att.Visible)
                {
                    var col = new DataColumn(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    col.Caption = att.Caption;
                    col.DataType = att.Type;
                    cols.Add(att.DisplayIndex, col);
                }

            }
            table.Columns.AddRange(cols.Select(x => x.Value).ToArray());

            int rowIndex = 0;
            foreach (T item in PrimaryList)
            {
                DataRow row = table.NewRow();
                rowIndex++;
                foreach (PropertyDescriptor prop in properties)
                {
                    var att = SystemFunctionsExtensions.GetPropertiesAttribute(prop);
                    if (att != null && att.Visible)
                    {
                        Dictionary<string, object> codeTable;
                        var isExist = LookupTables.TryGetValue(prop.Name, out codeTable);
                        // Replace the codeTable in source
                        if (isExist)
                        {
                            object value;
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(item))))
                            {
                                if (codeTable.TryGetValue(Convert.ToString(prop.GetValue(item)), out value))
                                {
                                    row[prop.Name] = value;
                                }
                            }
                            else
                            {
                                row[prop.Name] = DBNull.Value;
                            }
                        }
                        else
                        {
                            if (att.Type == typeof(DateTime))
                            {
                                DateTime dt;
                                if (DateTime.TryParseExact(Convert.ToString(prop.GetValue(item)), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out dt))
                                {
                                    row[prop.Name] = dt;
                                }
                                else if (prop.GetValue(item) is DateTime)
                                {
                                    row[prop.Name] = prop.GetValue(item);
                                }
                            }
                            else if (att.Type == typeof(Boolean))
                            {
                                row[prop.Name] = Convert.ToBoolean(ConversionEx.Val(prop.GetValue(item)));
                            }
                            else
                            {
                                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                            }

                            var textExpAttribute = SystemFunctionsExtensions.GetTextExpressionAttribute(prop);
                            //Check if expression exist
                            if (textExpAttribute != null)
                            {
                                if (textExpAttribute.Exp == "getrow()")
                                {
                                    row[prop.Name] = Convert.ToString(rowIndex);
                                }
                                else if (textExpAttribute.Exp == "FormatTwoDecimalPlaces")
                                {
                                    row[prop.Name] = string.Format("{0:0.00}", row[prop.Name]);
                                }
                                else
                                {
                                    var lst = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType()));
                                    lst.Add(item);
                                    var match = lst.Where(textExpAttribute.Exp);

                                    if (match.Count() == 0 && lst.Count > 0) //False
                                    {
                                        row[prop.Name] = textExpAttribute.FalseExp;
                                    }
                                    else //True
                                    {
                                        row[prop.Name] = textExpAttribute.TrueExp;
                                    }
                                }
                            }

                            var computeAtts = SystemFunctionsExtensions.GetComputeAttribute(prop);
                            if (computeAtts != null && computeAtts.Length > 0)
                            {
                                foreach (ComputeAttribute _att in computeAtts)
                                {
                                    var property = _att.propertyName;
                                    var expArr = _att.computeExpression.Split(' ');
                                    if (expArr.Length == 3)
                                    {
                                        var action = expArr[0];
                                        var param1 = expArr[1];
                                        var param2 = expArr[2];
                                        object value = "";
                                        if (action.ToUpper() == "SUB")
                                        {
                                            int val1 = Convert.ToInt32(row[param1]);
                                            int val2 = Convert.ToInt32(row[param2]);
                                            row[property] = val1 - val2;
                                        }
                                        else if (action.ToUpper() == "SUMGROUP")
                                        {
                                            int sum = 0;
                                            foreach (T model in PrimaryList)
                                            {
                                                var groupKey = this.GetTypeOfModel().GetProperty(param1).GetValue(model, null);
                                                if (Convert.ToString(row[param1]).Trim() == Convert.ToString(groupKey).Trim())
                                                {
                                                    var valueofmodel = this.GetTypeOfModel().GetProperty(param2).GetValue(model, null);
                                                    sum += Convert.ToInt32(valueofmodel);
                                                }
                                            }
                                            row[property] = sum;
                                        }
                                        else
                                        {
                                            throw new Exception("GalilCS - Not supported");
                                        }

                                    }
                                    else if (expArr.Length == 7)
                                    {
                                        var action = expArr[0];
                                        var param1 = expArr[1];
                                        var param2 = expArr[3];
                                        var param3 = expArr[5];
                                        var param4 = expArr[6];
                                        object value = "";
                                        if (action.ToUpper() == "TRANSLATE")
                                        {
                                            Type type = Type.GetType(param3);
                                            if (type != null)
                                            {
                                                IRepository ldwc_dec_num = (IRepository)Activator.CreateInstance(type);
                                                if (ldwc_dec_num != null)
                                                {
                                                    ldwc_dec_num.Retrieve(Convert.ToInt32(param4));
                                                    var found = ldwc_dec_num.Find("decline_number ==" + properties[param1].GetValue(item), 0, ldwc_dec_num.RowCount());
                                                    if (found >= 0)
                                                    {
                                                        var translatedValue = ldwc_dec_num.GetItemValue<string>(found.Value, param2);
                                                        prop.SetValue(item, translatedValue);
                                                        row[property] = translatedValue;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("GalilCS - Not supported");
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("GalilCS - Not supported");
                                    }
                                }
                            }
                        }
                    }
                }
                table.Rows.Add(row);
            }

            return table;
        }


        /// <summary>
        /// Clears the update flags in the primary and filter buffers and empties the delete buffer
        /// </summary>
        /// <returns>Returns 1 if it succeeds and �1 if an error occurs.</returns>
        public int ResetUpdate()
        {
            try
            {
                // Get all changed items
                var changedItems = GetAllChangedItems();
                foreach (var item in changedItems)
                {
                    item.Action = ModelAction.None;
                    item.Buffer = ModelBuffer.Primary;
                }

            }
            catch
            {
                return -1;
            }
            return 1;
        }


        /// <summary>
        /// Get all changed items
        /// </summary>
        /// <returns></returns>
        private IEnumerable<T> GetAllChangedItems()
        {
            return _fullList.Value.Where(model => model.Action != ModelAction.None);
        }

        /// <summary>
        /// Inserts data into a repository control tab-separated, comma-separated, or XML data in a string.
        /// </summary>
        /// <param name="importedString">A string from which you want to copy the data. The string should contain tab-separated or comma-separated columns or XML with one row per line </param>
        /// <returns></returns>
        public int ImportString(string importedString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Printing to PrintBOS API
        /// </summary>
        /// <param name="DeviceID"> ID of device </param>
        /// <param name="UserID"> ID of user </param>
        /// <param name="Password"> Password </param>
        /// <returns>Returns 0 if it succeeds and -1 if an error occurs</returns>
        public int Print(string DeviceID, string UserID, string Password, string templateName, string PrinterName)
        {
            try
            {
                XmlDocument docHeader;
                string HeaderName;

                //Get xml document and name
                GetXML(out docHeader, out HeaderName, template, PrinterName);
                var data = docHeader.InnerXml;
                //Call PrintBOS API
                var printBOS = new PrintBosServiceClient();
                printBOS.Print(DeviceID, UserID, Password, templateName, data);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        public int Print(string deviceID, string userID, string password, string PrinterName, params IRepository[] repository)
        {
            try
            {
                XmlDocument docHeader;
                string HeaderName;
                List<XmlDocument> repositoryDocument = new List<XmlDocument>(); ;

                this.GetXML(out docHeader, out HeaderName, template, PrinterName);
                foreach (var item in repository)
                {
                    string name;
                    XmlDocument doc = null;
                    item.GetXML(out doc, out name);
                    repositoryDocument.Add(doc);
                }
                var data = MergeXml(docHeader, repositoryDocument.ToArray());
                var printBOS = new PrintBosServiceClient();
                printBOS.Print(deviceID, userID, password, HeaderName, data);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        private string MergeXml(XmlDocument first, XmlDocument[] xml)
        {
            string returnValue = "";
            if (first == null)
                return returnValue;
            else if (first != null && xml.Length == 0)
                return first.InnerXml;
            else
            {
                var headerFirst = first.SelectSingleNode("//Header");
                var DetailsFirst = first.SelectSingleNode("//Details");
                var SummaryFirst = first.SelectSingleNode("//Summary");
                var FooterFirst = first.SelectSingleNode("//Footer");

                //All nodes in details moved to table
                var detailsNodes = first.ImportNode(DetailsFirst, true);
                detailsNodes = RenameElement((XmlElement)detailsNodes, string.Format("Table1"));
                DetailsFirst.RemoveAll();
                DetailsFirst.AppendChild(detailsNodes);
                var tableNo = 2;
                for (int i = 0; i < xml.Length; i++)
                {
                    var doc = xml[i];
                    foreach (XmlNode childEl in doc.DocumentElement.ChildNodes)
                    {
                        if (childEl.ChildNodes.Count > 0)
                        {
                            var newNode = first.ImportNode(childEl, true);
                            switch (childEl.Name)
                            {
                                case "Header":
                                    for (int index = 0; index < newNode.ChildNodes.Count; index++)
                                    {
                                        var node = newNode.ChildNodes[index].Clone();
                                        headerFirst.AppendChild(node);
                                    }
                                    break;
                                case "Details":
                                    newNode = RenameElement((XmlElement)newNode, string.Format("Table{0}", tableNo));
                                    DetailsFirst.AppendChild(newNode);
                                    tableNo++;
                                    break;
                                case "Summary":
                                    for (int index = 0; index < newNode.ChildNodes.Count; index++)
                                    {
                                        var node = newNode.ChildNodes[index].Clone();
                                        SummaryFirst.AppendChild(node);
                                    }
                                    break;
                                case "Footer":
                                    for (int index = 0; index < newNode.ChildNodes.Count; index++)
                                    {
                                        var node = newNode.ChildNodes[index].Clone();
                                        FooterFirst.AppendChild(node);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                return first.InnerXml;
            }


            return returnValue;
        }

        private XmlElement RenameElement(XmlElement e, string newName)
        {
            XmlDocument doc = e.OwnerDocument;
            XmlElement newElement = doc.CreateElement(newName);
            while (e.HasChildNodes)
            {
                newElement.AppendChild(e.FirstChild);
            }
            XmlAttributeCollection ac = e.Attributes;
            while (ac.Count > 0)
            {
                newElement.Attributes.Append(ac[0]);
            }

            return newElement;
        }

        public void GetXML(out XmlDocument document, out string name)
        {
            GetXML(out document, out name, null, null);
        }
        public void GetXML(out XmlDocument document, out string name, Dictionary<string, string> templates = null, string printerName = null)
        {
            document = new XmlDocument();
            var table = GetDataTable();
            string xml;
            var lstHeader = new List<XmlNode>();
            var lstSummary = new List<XmlNode>();
            var lstFooter = new List<XmlNode>();
            using (TextWriter writer = new StringWriter())
            {
                table.WriteXml(writer);
                xml = writer.ToString();
            }

            document.LoadXml(xml);
            try
            {
                document = RemoveAllNamespaces(document);
            }
            catch (Exception ex)
            {
                document.LoadXml(xml);
            }
            name = this.GetType().Name.Replace("Repository", "");

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor prop in properties)
            {
                var att = SystemFunctionsExtensions.GetXMLOutputAttribute(prop);
                if (att == null) continue;
                var nodeName = prop.Name;
                switch (att.XMLLocation)
                {
                    case XMLOutputLocation.Header:
                        {
                            var nodes = document.SelectNodes(string.Format("//{0}", nodeName));
                            if (nodes != null && nodes.Count > 0)
                            {
                                lstHeader.Add(nodes[0].Clone());
                                foreach (XmlNode item in nodes)
                                {
                                    item.ParentNode.RemoveChild(item);
                                }
                            }
                        }
                        break;
                    case XMLOutputLocation.Summary:
                        {
                            var nodes = document.SelectNodes(string.Format("//{0}", nodeName));
                            if (nodes != null && nodes.Count > 0)
                            {
                                lstSummary.Add(nodes[0].Clone());
                                foreach (XmlNode item in nodes)
                                {
                                    item.ParentNode.RemoveChild(item);
                                }
                            }
                        }
                        break;
                    case XMLOutputLocation.Footer:
                        {
                            var nodes = document.SelectNodes(string.Format("//{0}", nodeName));
                            if (nodes != null && nodes.Count > 0)
                            {
                                lstFooter.Add(nodes[0].Clone());
                                foreach (XmlNode item in nodes)
                                {
                                    item.ParentNode.RemoveChild(item);
                                }
                            }
                        }
                        break;
                }
            }

            var metadata = document.CreateElement("metadata");
            var header = document.CreateElement("Header");
            var details = document.CreateElement("Details");
            var summary = document.CreateElement("Summary");
            var footer = document.CreateElement("Footer");
            if (document.DocumentElement != null)
            {
                //Prepare Details section 
                details.InnerXml = document.DocumentElement.Clone().InnerXml;
                //Fixed details from empty nodes
                var length = details.ChildNodes.Count;
                List<XmlNode> toRemove = new List<XmlNode>();
                for (int i = 0; i < length; i++)
                {
                    if (string.IsNullOrEmpty(details.ChildNodes[i].InnerXml))
                        toRemove.Add(details.ChildNodes[i]);
                }

                foreach (var item in toRemove)
                {
                    details.RemoveChild(item);
                }

            }
            else
            {
                throw new Exception("Exception : GetXML method");
            }

            if (templates != null && printerName != null)
            {
                //Prepare metadata section
                var templateID = document.CreateNode(XmlNodeType.Element, "TemplateId", "");
                templateID.InnerText = templates[name];
                metadata.AppendChild(templateID);
                var printer = document.CreateNode(XmlNodeType.Element, "PrinterName", "");
                printer.InnerText = printerName;
                metadata.AppendChild(printer);
            }


            //Prepare Header section
            foreach (var item in lstHeader)
            {
                var n = document.CreateNode(XmlNodeType.Element, item.Name, "");
                n.InnerText = item.InnerText;
                header.AppendChild(n);
            }

            //Prepare Summary section
            foreach (var item in lstSummary)
            {
                var n = document.CreateNode(XmlNodeType.Element, item.Name, "");
                n.InnerText = item.InnerText;
                summary.AppendChild(n);
            }

            //Prepare Footer section
            foreach (var item in lstFooter)
            {
                var n = document.CreateNode(XmlNodeType.Element, item.Name, "");
                n.InnerText = item.InnerText;
                footer.AppendChild(n);
            }



            //Grouping
            if (!string.IsNullOrEmpty(_XMLGroupExpression))
            {
                XElement _details = XElement.Parse(details.OuterXml);

                //Create collection of group by _XMLGroupExpression
                var groups = _details.Descendants(name)
               .GroupBy(x => Convert.ToString(x.Element(_XMLGroupExpression).Value).Trim())
               .ToList();

                //remove all nodes from details section
                details.RemoveAll();
                foreach (var group in groups)
                {
                    //Create new group
                    var subGroup = document.CreateElement("groups");
                    //Add attribute to group
                    var attGroup = document.CreateAttribute("name");
                    attGroup.Value = group.Key;
                    subGroup.Attributes.Append(attGroup);
                    if (!string.IsNullOrEmpty(_XMLGroupMovePropertiesToAttribute))
                    {
                        string[] arrProperties = _XMLGroupMovePropertiesToAttribute.Split(' ', ',');
                        foreach (string propertyToMove in arrProperties)
                        {
                            //Add attribute to group
                            var att = document.CreateAttribute(propertyToMove);


                            //Update all child that will removed them to attribute
                            foreach (var childGroup in group)
                            {
                                XmlElement element = document.ReadNode(childGroup.CreateReader()) as XmlElement;

                                var property = element.SelectSingleNode(propertyToMove);
                                if (property != null)
                                {
                                    att.Value = property.InnerText;
                                    //Remove all child that will removed them to attribute
                                    element.RemoveChild(property);
                                }
                                subGroup.AppendChild(element);
                            }
                            //Add attribute to group
                            subGroup.Attributes.Append(att);
                        }
                    }else
                    {
                        //Add all related nodes to group
                        foreach (var childGroup in group)
                        {
                            XmlElement element = document.ReadNode(childGroup.CreateReader()) as XmlElement;
                            subGroup.AppendChild(element);
                        }
                    }
                    
                    //Add sub group to details section.
                    details.AppendChild(subGroup);
                }
            }

            //Remove all nodes
            document.RemoveAll();

            var doc = document.CreateElement(name);

            var declaration = document.CreateXmlDeclaration("1.0", "UTF-8", "");
            //add sections to document
            if (templates != null && printerName != null)
            {
                doc.AppendChild(metadata);
            }
            doc.AppendChild(header);
            doc.AppendChild(details);
            doc.AppendChild(summary);
            doc.AppendChild(footer);
            document.AppendChild(declaration);
            document.AppendChild(doc);
        }
        private XmlDocument RemoveAllNamespaces(XmlDocument oldDom)
        {
            // Remove all xmlns:* instances from the passed XmlDocument
            // to simplify our xpath expressions.
            XmlDocument newDom = new XmlDocument();
            string s = System.Text.RegularExpressions.Regex.Replace(
                oldDom.OuterXml, @"(xmlns:?[^=]*=[""][^""]*[""])", "",
                System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                System.Text.RegularExpressions.RegexOptions.Multiline);

            s = System.Text.RegularExpressions.Regex.Replace(
                            s, @"(xsi:?[^=]*=[""][^""]*[""])", "",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase |
                            System.Text.RegularExpressions.RegexOptions.Multiline);

            newDom.LoadXml(s);
            return newDom;


        }

        /// <summary>
        /// Reports the SQL SELECT statement
        /// </summary>
        /// <returns>Returns the current SQL SELECT statement</returns>
        public string GetSQLSelect()
        {
            return _selectQuery;
        }

        /// <summary>
        /// Reports the number of rows that are not displayed in the DataWindow because of the current filter criteria.
        /// </summary>
        /// <returns>Returns the number of rows in repository that are not displayed because they do not meet the current filter criteria. Returns 0 if all rows are displayed</returns>
        public int FilteredCount()
        {
            return _fullList.Value.Where(model => model.Buffer == ModelBuffer.Filter).ToList().Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        public void Export(string xml)
        {
            _export = xml;
        }

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns></returns>
        public List<ModelBase> GetDataRows(int row = -1, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary)
        {
            List<ModelBase> bufferList = GetBufferList<T>(buffer).Cast<ModelBase>().ToList();

            if (row < 0)
            {
                return bufferList;
            }
            return new List<ModelBase> { bufferList[row] };

        }

        /// <summary>
        /// Sets data to Element
        /// </summary>
        /// <param name="list">List of data</param>
        public void SetDataRows(List<ModelBase> list)
        {
            //TODO: Simple implementation of merging lists w/o duplicates
            if (!_fullList.IsValueCreated)
            {
                _fullList = new Lazy<List<T>>(() => list.Cast<T>().ToList());
            }
            else
            {
                _fullList = new Lazy<List<T>>(() => list.Cast<T>().ToList().Union(_fullList.Value).ToList());
            }

        }

        public int ColumnCount()
        {
            return _columnsPropertyInfo.Value.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="v3"></param>
        public string Modify(string v1, string v2, string v3)
        {
            string result = null;
            try
            {
                //TODO: expression.Invoke();
                result = "";
            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        /// <summary>
        /// Gets PrimaryList of repository
        /// </summary>
        /// <returns>Returns PrimaryList</returns>
        public IList GetPrimaryList()
        {
            return PrimaryList;
        }

        public int RowsDiscard(int startRow, int endRow, ModelBuffer buffer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets type of repository model
        /// </summary>
        /// <returns>Returns type of model</returns>
        public Type GetTypeOfModel()
        {
            return (new T()).GetType();
        }


        /// <summary>
        /// Gets the value of an item for the specified row and column
        /// </summary>
        /// <typeparam name="T1">The type of column as a return value</typeparam>
        /// <param name="row">A value identifying the row location of the data.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="buffer">The buffer from which you want to get item</param>
        /// <param name="isOriginalValue"></param>
        /// <returns>Value of an item for the specified row and column</returns>
        public T1 GetItemValue<T1>(long row, string columnName, ModelBuffer buffer = ModelBuffer.Primary, bool isOriginalValue = false)
        {
            return GetItemValue<T1>(row, columnName, buffer);
        }


        public int AcceptText()
        {
            throw new NotImplementedException();
        }

        int IRepository.FirstRowOnPage()
        {
            throw new NotImplementedException();
        }

        public void SetDataRows(ModelBase[] array)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets column name
        /// </summary>
        /// <returns>Returns name of column</returns>
        public string GetColumnName()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns an integer containing the number of the current row in the DataStore.
        /// </summary>
        /// <returns></returns>
        public long GetRow()
        {
            throw new NotImplementedException();
        }

        public bool ReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public void FirstRowOnPage()
        {
            throw new NotImplementedException();
        }

        public int Print()
        {
            throw new NotImplementedException();
        }

        public long SetData(params object[] data)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Group nodes when create xml using column name
        /// </summary>
        /// <param name="expression">Column Name</param>
        public void SetXMLGroup(string expression)
        {
            _XMLGroupExpression = expression;
        }

        /// <summary>
        /// Get group string.
        /// </summary>
        public string GetXMLGroup()
        {
            return _XMLGroupExpression;
        }

        public void SetXMLGroupMovePropertiesToAttribute(string expression)
        {
            _XMLGroupMovePropertiesToAttribute = expression;
        }

        /// <summary>
        /// Get group string.
        /// </summary>
        public string GetXMLGroupMovePropertiesToAttribute()
        {
            return _XMLGroupMovePropertiesToAttribute;
        }



        private T CreateNewModel()
        {
            var model = new T();
            if (InitialValues.Count > 0)
            {
                foreach (var item in InitialValues)
                {
                    var property = _columnsPropertyInfo.Value.FirstOrDefault(x => x.Name == item.Key);
                    if (property != null)
                    {
                        property.SetValue(model, Convert.ChangeType(item.Value, property.PropertyType));
                    }

                }

            }
            return model;
        }
        public Dictionary<string, object> InitialValues
        {
            get
            {
                return _initialValues;
            }
            set
            {
                _initialValues = value;
            }
        }

        public bool UpdateRow(int targetIndex, ModelBuffer sourceBuffer, IRepository target, int repoIndex)
        {

            PropertyDescriptorCollection propertiesSource = TypeDescriptor.GetProperties(this.GetTypeOfModel());
            PropertyDescriptorCollection propertiesTarget = TypeDescriptor.GetProperties(target.GetTypeOfModel());
            List<T> sourceList = this.GetBufferList<T>(sourceBuffer);

            if (propertiesSource.Count != propertiesTarget.Count)
            {
                return false;
            }

            for (int i = 0; i < propertiesSource.Count; i++)
            {
                switch (propertiesTarget[i].PropertyType.Name)
                {
                    case "String":
                        target.SetItem(targetIndex, propertiesTarget[i].Name, this.GetItemValue<string>(repoIndex, propertiesSource[i].Name, sourceBuffer));
                        break;
                    case "Int64":
                        target.SetItem(targetIndex, propertiesTarget[i].Name, this.GetItemValue<long>(repoIndex, propertiesSource[i].Name, sourceBuffer));
                        break;
                    case "Decimal":
                        target.SetItem(targetIndex, propertiesTarget[i].Name, this.GetItemValue<Decimal?>(repoIndex, propertiesSource[i].Name, sourceBuffer));
                        break;
                    case "DateTime":
                        target.SetItem(targetIndex, propertiesTarget[i].Name, this.GetItemValue<DateTime>(repoIndex, propertiesSource[i].Name, sourceBuffer));
                        break;
                    case "Double":
                        target.SetItem(targetIndex, propertiesTarget[i].Name, this.GetItemValue<Double>(repoIndex, propertiesSource[i].Name, sourceBuffer));
                        break;
                    case "Nullable`1":
                        if (propertiesTarget[i].PropertyType.FullName.Contains("DateTime"))
                        {
                            DateTime? val = this.GetItemValue<DateTime?>(repoIndex, propertiesSource[i].Name, sourceBuffer);
                            target.SetItem(targetIndex, propertiesTarget[i].Name, val);
                        }
                        else if (propertiesTarget[i].PropertyType.FullName.Contains("Int64"))
                        {
                            Int64? val = this.GetItemValue<Int64?>(repoIndex, propertiesSource[i].Name, sourceBuffer);
                            target.SetItem(targetIndex, propertiesTarget[i].Name, val);
                        }
                        else if (propertiesTarget[i].PropertyType.FullName.Contains("Int32"))
                        {
                            Int32? val = this.GetItemValue<Int32?>(repoIndex, propertiesSource[i].Name, sourceBuffer);
                            target.SetItem(targetIndex, propertiesTarget[i].Name, val);
                        }
                        else if (propertiesTarget[i].PropertyType.FullName.Contains("Decimal"))
                        {
                            decimal? val = this.GetItemValue<decimal?>(repoIndex, propertiesSource[i].Name, sourceBuffer);
                            target.SetItem(targetIndex, propertiesTarget[i].Name, val);
                        }
                        else
                        {
                            throw new Exception("GalilCS - TODO : Not supported");
                        }
                        break;
                    case "ModelAction":
                        {
                            object val = this.GetItemValue<object>(repoIndex, propertiesSource[i].Name, sourceBuffer);
                            target.SetItem(targetIndex, propertiesTarget[i].Name, val);
                        }
                        break;
                    case "ModelBase":
                    case "ModelBuffer":
                        continue;
                    default:
                        throw new Exception("GalilCS - TODO : Not supported");
                }
            }
            return true;

        }
        //gilad test
        public void AppendPrimaryList(ModelBase model)
        {
            Insert(model as T);
        }
    }
}
