using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_inv_pack_summeryRepository : RepositoryBase<d_inv_pack_summery>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.serial_number, invoice_details.branch_number, invoice_move.invoice_number, invoice_move.branch_number, invoice_move.supplier_number, invoice_move.invoice_type, invoice_move.order_number, invoice_details.material_quantity FROM invoice_details, invoice_move WHERE invoice_details.branch_number = invoice_move.branch_number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_inv_pack_summeryService"/> class.
        /// </summary>
		public d_inv_pack_summeryRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_inv_pack_summery dataRow = null;
		dataRow = new d_inv_pack_summery();
		data[0] = dataRow;
			dataRow.invoice_details_serial_number = 0;
			dataRow.invoice_details_branch_number = 0;
			dataRow.invoice_move_invoice_number = 0;
			dataRow.invoice_move_branch_number = 0;
			dataRow.invoice_move_supplier_number = 0;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_0";
			dataRow.invoice_move_order_number = 0;
			dataRow.invoice_details_material_quantity = 0;
		dataRow = new d_inv_pack_summery();
		data[1] = dataRow;
			dataRow.invoice_details_serial_number = 1;
			dataRow.invoice_details_branch_number = 1;
			dataRow.invoice_move_invoice_number = 1;
			dataRow.invoice_move_branch_number = 1;
			dataRow.invoice_move_supplier_number = 1;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_1";
			dataRow.invoice_move_order_number = 1;
			dataRow.invoice_details_material_quantity = 1;
		dataRow = new d_inv_pack_summery();
		data[2] = dataRow;
			dataRow.invoice_details_serial_number = 2;
			dataRow.invoice_details_branch_number = 2;
			dataRow.invoice_move_invoice_number = 2;
			dataRow.invoice_move_branch_number = 2;
			dataRow.invoice_move_supplier_number = 2;
			dataRow.invoice_move_invoice_type = "invoice_move_invoice_type_2";
			dataRow.invoice_move_order_number = 2;
			dataRow.invoice_details_material_quantity = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_inv_pack_summery model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_details_serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.invoice_details_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_move_invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.invoice_move_branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.invoice_move_supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_move_invoice_type = GetValue(record, "invoice_type", Convert.ToString);
            model.invoice_move_order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.invoice_details_material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
		}
    }
}
