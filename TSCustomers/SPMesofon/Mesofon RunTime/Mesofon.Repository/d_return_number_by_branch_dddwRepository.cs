using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_return_number_by_branch_dddwRepository : RepositoryBase<d_return_number_by_branch_dddw>
    {
		private static readonly string _selectQuery = "SELECT invoice_number, supplier_number, branch_number, date_move, state FROM invoice_move WHERE (invoice_move.invoice_type = @0) AND (invoice_move.branch_number = @1) AND (invoice_move.supplier_number = (SELECT number FROM suppliers WHERE supplier_as_branch = @2)) AND (invoice_move.state <> 'D')";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_return_number_by_branch_dddwService"/> class.
        /// </summary>
		public d_return_number_by_branch_dddwRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_return_number_by_branch_dddw dataRow = null;
		dataRow = new d_return_number_by_branch_dddw();
		data[0] = dataRow;
			dataRow.invoice_number = 0;
			dataRow.supplier_number = 0;
			dataRow.branch_number = 0;
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.state = "state_0";
		dataRow = new d_return_number_by_branch_dddw();
		data[1] = dataRow;
			dataRow.invoice_number = 1;
			dataRow.supplier_number = 1;
			dataRow.branch_number = 1;
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.state = "state_1";
		dataRow = new d_return_number_by_branch_dddw();
		data[2] = dataRow;
			dataRow.invoice_number = 2;
			dataRow.supplier_number = 2;
			dataRow.branch_number = 2;
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.state = "state_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_return_number_by_branch_dddw model)
        {
            if (model == null)
            {
                return;
            }

            model.invoice_number = GetValue(record, "invoice_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.date_move = GetDateTime(record, "date_move");
            model.state = GetValue(record, "state", Convert.ToString);
		}
    }
}
