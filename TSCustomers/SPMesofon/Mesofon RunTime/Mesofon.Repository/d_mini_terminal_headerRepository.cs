using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_mini_terminal_headerRepository : RepositoryBase<d_mini_terminal_header>
    {
		private static readonly string _selectQuery = "";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_headerService"/> class.
        /// </summary>
		public d_mini_terminal_headerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("P", "�.�����");
            codeTable.Add("I", "�������");
            LookupTables.Add("doc_type", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_header dataRow = null;
		dataRow = new d_mini_terminal_header();
		data[0] = dataRow;
			dataRow.supplier_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.order_number = 0;
			dataRow.doc_type = "doc_type_0";
		dataRow = new d_mini_terminal_header();
		data[1] = dataRow;
			dataRow.supplier_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.order_number = 1;
			dataRow.doc_type = "doc_type_1";
		dataRow = new d_mini_terminal_header();
		data[2] = dataRow;
			dataRow.supplier_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.order_number = 2;
			dataRow.doc_type = "doc_type_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_header model)
        {
            if (model == null)
            {
                return;
            }

            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "supplier_name", Convert.ToString);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.doc_type = GetValue(record, "name_1", Convert.ToString);
		}
    }
}
