using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class dddw_get_return_reasonsRepository : RepositoryBase<dddw_get_return_reasons>
    {
		private static readonly string _selectQuery = "SELECT b2b_decline_types.decline_number, b2b_decline_types.decline_code, b2b_decline_types.display_name, b2b_decline_types.is_reject_with_qty, b2b_decline_types.is_reject_all_invoice, b2b_decline_types.is_automatic_applied, b2b_decline_types.is_red_stamp FROM b2b_decline_types WHERE b2b_decline_types.is_active = @0";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="dddw_get_return_reasonsService"/> class.
        /// </summary>
		public dddw_get_return_reasonsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			dddw_get_return_reasons dataRow = null;
		dataRow = new dddw_get_return_reasons();
		data[0] = dataRow;
			dataRow.decline_number = 0;
			dataRow.decline_code = "decline_code_0";
			dataRow.display_name = "display_name_0";
			dataRow.is_reject_with_qty = 0;
			dataRow.is_reject_all_invoice = 0;
			dataRow.is_automatic_applied = 0;
			dataRow.is_red_stamp = 0;
		dataRow = new dddw_get_return_reasons();
		data[1] = dataRow;
			dataRow.decline_number = 1;
			dataRow.decline_code = "decline_code_1";
			dataRow.display_name = "display_name_1";
			dataRow.is_reject_with_qty = 1;
			dataRow.is_reject_all_invoice = 1;
			dataRow.is_automatic_applied = 1;
			dataRow.is_red_stamp = 1;
		dataRow = new dddw_get_return_reasons();
		data[2] = dataRow;
			dataRow.decline_number = 2;
			dataRow.decline_code = "decline_code_2";
			dataRow.display_name = "display_name_2";
			dataRow.is_reject_with_qty = 2;
			dataRow.is_reject_all_invoice = 2;
			dataRow.is_automatic_applied = 2;
			dataRow.is_red_stamp = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, dddw_get_return_reasons model)
        {
            if (model == null)
            {
                return;
            }

            model.decline_number = GetValue(record, "decline_number", Convert.ToInt64);
            model.decline_code = GetValue(record, "decline_code", Convert.ToString);
            model.display_name = GetValue(record, "display_name", Convert.ToString);
            model.is_reject_with_qty = GetValue(record, "is_reject_with_qty", Convert.ToInt64);
            model.is_reject_all_invoice = GetValue(record, "is_reject_all_invoice", Convert.ToInt64);
            model.is_automatic_applied = GetValue(record, "is_automatic_applied", Convert.ToInt64);
            model.is_red_stamp = GetValue(record, "is_red_stamp", Convert.ToInt64);
		}
    }
}
