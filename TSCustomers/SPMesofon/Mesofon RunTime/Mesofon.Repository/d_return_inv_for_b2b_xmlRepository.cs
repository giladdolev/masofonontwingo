﻿using System;
using System.Data;
using Common.Transposition.Extensions;
using Mesofon.Models;
using Mesofon.Repository;

namespace Mesofon.Data
{
    public class d_return_inv_for_b2b_xmlRepository : RepositoryBase<d_return_inv_for_b2b_xml>
    {
        private static readonly string _selectQuery = "";
        private static readonly string _insertQuery = "";
        private static readonly string _updateKeyOnlyQuery = "";
        private static readonly string _updateColumnsQuery = "";
        private static readonly string _deleteQuery = "";

        public d_return_inv_for_b2b_xmlRepository():base(_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery, _insertQuery)
        {
        }

        public override void Map(IDataRecord record, d_return_inv_for_b2b_xml model)
        {
            throw new NotImplementedException();
        }

        protected override ModelBase[] GetMockData()
        {
            throw new NotImplementedException();
        }
    }
}
