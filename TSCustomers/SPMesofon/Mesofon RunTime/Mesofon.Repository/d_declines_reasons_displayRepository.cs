using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_declines_reasons_displayRepository : RepositoryBase<d_declines_reasons_display>
    {
		private static readonly string _selectQuery = "SELECT b2b_decline_types.display_name, IsNull(b2b_declines_move.branch_number, @0) AS branch_number, IsNull(b2b_declines_move.supplier_number, @1) AS supplier_number, IsNull(b2b_declines_move.order_number, @2) AS order_number, IsNull(b2b_declines_move.parent_doc_type, @3) AS parent_doc_type, IsNull(b2b_declines_move.parent_doc_number, @4) AS parent_doc_number, IsNull(b2b_declines_move.material_number, @5) AS material_number, IsNull(b2b_declines_move.line_number, 0) AS line_number, b2b_declines_move.parent_serial_number, IsNull(b2b_declines_move.status, 'a') AS status, IsNull(b2b_declines_move.inserted_option, 1) AS inserted_option, IsNull(b2b_declines_move.update_datetime, GetDate()) AS update_datetime, IsNull(b2b_declines_move.employee_number, @6) AS employee_number, IsNull(b2b_declines_move.decline_number, b2b_decline_types.decline_number) AS decline_number, IsNull(b2b_declines_move.decline_code, b2b_decline_types.decline_code) AS decline_code, IsNull(b2b_declines_move.is_reject_with_qty, b2b_decline_types.is_reject_with_qty) AS is_reject_with_qty, IsNull(b2b_declines_move.is_reject_all_invoice, b2b_decline_types.is_reject_all_invoice) AS is_reject_all_invoice, IsNull(b2b_declines_move.is_automatic_applied, b2b_decline_types.is_automatic_applied) AS is_automatic_applied, IsNull(b2b_declines_move.is_red_stamp, b2b_decline_types.is_red_stamp) AS is_red_stamp, IsNull(b2b_declines_move.original_quantity, 0) AS original_quantity, IsNull(b2b_declines_move.actual_quantity, 0) AS actual_quantity, IsNull(b2b_declines_move.reject_quantity, 0) AS reject_quantity, IsNull(b2b_declines_move.original_price, 0) AS original_price, IsNull(b2b_declines_move.actual_price, 0) AS actual_price, IsNull(b2b_declines_move.b2b_msg_decline_ref, 0) AS b2b_msg_decline_ref, IsNull(b2b_declines_move.b2b_status, 0) AS b2b_status, IsNull(b2b_declines_move.distributor_number, 0) AS distributor_number, b2b_declines_move.details, b2b_decline_types.is_active,CASE WHEN b2b_declines_move.status = 'a' THEN 1 ELSE 0 END AS checked FROM b2b_decline_types, b2b_declines_move WHERE (b2b_decline_types.decline_number *= b2b_declines_move.decline_number) AND (b2b_decline_types.decline_level = @7) AND (b2b_decline_types.is_active IN (1, 2)) AND (b2b_decline_types.user_interface = 1) AND (b2b_declines_move.branch_number = @0) AND (b2b_declines_move.supplier_number = @1) AND (b2b_declines_move.order_number = @2) AND (b2b_declines_move.parent_doc_type = @3) AND (b2b_declines_move.material_number = @5) AND (b2b_declines_move.parent_doc_number = @4)";
		private static readonly string _insertQuery = "insert into b2b_declines_move (parent_serial_number ,status ,inserted_option  ,update_datetime ,employee_number, decline_number  , decline_code  ,is_reject_with_qty    , is_reject_all_invoice  ,is_automatic_applied  , is_red_stamp , original_quantity ,actual_quantity   , reject_quantity ,original_price ,actual_price   , b2b_msg_decline_ref,b2b_status ,distributor_number, details , branch_number, supplier_number ,  order_number , parent_doc_number, parent_doc_type ,material_number, line_number)VALUES(@parent_serial_number ,@status ,@inserted_option,@update_datetime ,@employee_number ,@decline_number ,@move_decline_code ,@is_reject_with_qty,@is_reject_all_invoice,@is_automatic_applied , @is_red_stamp,@original_quantity,@move_actual_quantity,@reject_quantity,@move_original_price, @actual_price,@b2b_msg_decline_ref,@b2b_status,@distributor_number ,@details,@branch_number,@supplier_number, @order_number,@parent_doc_number,@parent_doc_type,@material_number,@line_number)";
        private static readonly string _updateKeyOnlyQuery = "update b2b_declines_move set parent_serial_number = @parent_serial_number,status=@status,inserted_option = @inserted_option ,update_datetime = @update_datetime,employee_number = @employee_number, decline_number = @decline_number , decline_code = @move_decline_code,is_reject_with_qty = @is_reject_with_qty  , is_reject_all_invoice = @is_reject_all_invoice,is_automatic_applied = @is_automatic_applied , is_red_stamp = @is_red_stamp , original_quantity = @original_quantity,actual_quantity = @move_actual_quantity , reject_quantity = @reject_quantity,original_price = @move_original_price , actual_price = @actual_price , b2b_msg_decline_ref = @b2b_msg_decline_ref,b2b_status = @b2b_status,distributor_number = @distributor_number , details = @details where  branch_number= @branch_number and supplier_number = @supplier_number and order_number = @order_number and parent_doc_number= @parent_doc_number and parent_doc_type = @parent_doc_type and material_number = @material_number and line_number = @line_number";
        private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "delete from b2b_declines_move where  branch_number= @branch_number and supplier_number = @supplier_number and order_number = @order_number and parent_doc_number= @parent_doc_number and parent_doc_type = @parent_doc_type and material_number = @material_number and line_number = @line_number";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_declines_reasons_displayService"/> class.
        /// </summary>
		public d_declines_reasons_displayRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_declines_reasons_display dataRow = null;
		dataRow = new d_declines_reasons_display();
		data[0] = dataRow;
			dataRow.display_name = "display_name_0";
			dataRow.checkedField = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.order_number = 0;
			dataRow.parent_doc_type = "parent_doc_type_0";
			dataRow.parent_doc_number = 0;
			dataRow.material_number = 0;
			dataRow.line_number = 0;
			dataRow.parent_serial_number = 0;
			dataRow.status = "status_0";
			dataRow.inserted_option = 0;
			dataRow.update_datetime = new DateTime(1900,1,1);
			dataRow.employee_number = 0;
			dataRow.decline_number = 0;
			dataRow.move_decline_code = "move_decline_code_0";
			dataRow.is_reject_with_qty = 0;
			dataRow.is_reject_all_invoice = 0;
			dataRow.is_automatic_applied = 0;
			dataRow.is_red_stamp = 0;
			dataRow.original_quantity = 0;
			dataRow.move_actual_quantity = 0;
			dataRow.reject_quantity = 0;
			dataRow.move_original_price = 0;
			dataRow.actual_price = 0;
			dataRow.b2b_msg_decline_ref = 0;
			dataRow.b2b_status = 0;
			dataRow.distributor_number = 0;
			dataRow.details = "details_0";
			dataRow.b2b_decline_types_is_active = 0;
			dataRow.max_line_number = Convert.ToString(0);
		dataRow = new d_declines_reasons_display();
		data[1] = dataRow;
			dataRow.display_name = "display_name_1";
			dataRow.checkedField = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.order_number = 1;
			dataRow.parent_doc_type = "parent_doc_type_1";
			dataRow.parent_doc_number = 1;
			dataRow.material_number = 1;
			dataRow.line_number = 1;
			dataRow.parent_serial_number = 1;
			dataRow.status = "status_1";
			dataRow.inserted_option = 1;
			dataRow.update_datetime = new DateTime(1901,2,2);
			dataRow.employee_number = 1;
			dataRow.decline_number = 1;
			dataRow.move_decline_code = "move_decline_code_1";
			dataRow.is_reject_with_qty = 1;
			dataRow.is_reject_all_invoice = 1;
			dataRow.is_automatic_applied = 1;
			dataRow.is_red_stamp = 1;
			dataRow.original_quantity = 1;
			dataRow.move_actual_quantity = 1;
			dataRow.reject_quantity = 1;
			dataRow.move_original_price = 1;
			dataRow.actual_price = 1;
			dataRow.b2b_msg_decline_ref = 1;
			dataRow.b2b_status = 1;
			dataRow.distributor_number = 1;
			dataRow.details = "details_1";
			dataRow.b2b_decline_types_is_active = 1;
			dataRow.max_line_number = Convert.ToString(1);
            dataRow = new d_declines_reasons_display();
		data[2] = dataRow;
			dataRow.display_name = "display_name_2";
			dataRow.checkedField = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.order_number = 2;
			dataRow.parent_doc_type = "parent_doc_type_2";
			dataRow.parent_doc_number = 2;
			dataRow.material_number = 2;
			dataRow.line_number = 2;
			dataRow.parent_serial_number = 2;
			dataRow.status = "status_2";
			dataRow.inserted_option = 2;
			dataRow.update_datetime = new DateTime(1902,3,3);
			dataRow.employee_number = 2;
			dataRow.decline_number = 2;
			dataRow.move_decline_code = "move_decline_code_2";
			dataRow.is_reject_with_qty = 2;
			dataRow.is_reject_all_invoice = 2;
			dataRow.is_automatic_applied = 2;
			dataRow.is_red_stamp = 2;
			dataRow.original_quantity = 2;
			dataRow.move_actual_quantity = 2;
			dataRow.reject_quantity = 2;
			dataRow.move_original_price = 2;
			dataRow.actual_price = 2;
			dataRow.b2b_msg_decline_ref = 2;
			dataRow.b2b_status = 2;
			dataRow.distributor_number = 2;
			dataRow.details = "details_2";
			dataRow.b2b_decline_types_is_active = 2;
			dataRow.max_line_number = Convert.ToString(2);
            return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_declines_reasons_display model)
        {
            if (model == null)
            {
                return;
            }

            model.display_name = GetValue(record, "display_name", Convert.ToString);
            model.checkedField = GetValue(record, "checked", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.parent_doc_type = GetValue(record, "parent_doc_type", Convert.ToString);
            model.parent_doc_number = GetValue(record, "parent_doc_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.line_number = GetValue(record, "line_number", Convert.ToInt64);
            model.parent_serial_number = GetValue(record, "parent_serial_number", Convert.ToInt64);
            model.status = GetValue(record, "status", Convert.ToString);
            model.inserted_option = GetValue(record, "inserted_option", Convert.ToInt64);
            model.update_datetime = GetDateTime(record, "update_datetime");
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.decline_number = GetValue(record, "decline_number", Convert.ToInt64);
            model.move_decline_code = GetValue(record, "decline_code", Convert.ToString);
            model.is_reject_with_qty = GetValue(record, "is_reject_with_qty", Convert.ToInt64);
            model.is_reject_all_invoice = GetValue(record, "is_reject_all_invoice", Convert.ToInt64);
            model.is_automatic_applied = GetValue(record, "is_automatic_applied", Convert.ToInt64);
            model.is_red_stamp = GetValue(record, "is_red_stamp", Convert.ToInt64);
            model.original_quantity = GetValue(record, "original_quantity", Convert.ToDecimal);
            model.move_actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.reject_quantity = GetValue(record, "reject_quantity", Convert.ToDecimal);
            model.move_original_price = GetValue(record, "original_price", Convert.ToDecimal);
            model.actual_price = GetValue(record, "actual_price", Convert.ToDecimal);
            model.b2b_msg_decline_ref = GetValue(record, "b2b_msg_decline_ref", Convert.ToInt64);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.details = GetValue(record, "details", Convert.ToString);
            model.b2b_decline_types_is_active = GetValue(record, "is_active", Convert.ToInt64);
		}
    }
}
