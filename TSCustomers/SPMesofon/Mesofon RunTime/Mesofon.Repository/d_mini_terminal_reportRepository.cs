using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_mini_terminal_reportRepository : RepositoryBase<d_mini_terminal_report>
    {
		private static readonly string _selectQuery = "SELECT invoice_details.branch_number, global_parameters.branch_name, invoice_details.supplier_number, suppliers.name AS suppliers_name, invoice_move.order_number, AS employee_name, materials.barcode, materials.name, dateadd(mm, materials.min_valid_months, getdate()) AS min_valid_months, invoice_details.expiration_date, AS ok_valid_months, IsNull(supplier_order_details.material_quantity, 0) AS order_quantity, IsNull(invoice_details.expected_material_quantity, 0) AS mt_quantity, invoice_details.invoice_number AS doc_no, I AS doc_type, IsNull(supplier_order_details.material_quantity, 0) - IsNull(invoice_details.expected_material_quantity, 0) AS diff FROM invoice_move, invoice_details, supplier_order_details, global_parameters, suppliers, materials WHERE (invoice_move.branch_number = @0) AND (invoice_move.supplier_number = @1) AND (invoice_move.order_number = @2) AND (Lower(invoice_move.state) <> 'd') AND (invoice_details.branch_number = @0) AND (supplier_order_details.branch_number = @0) AND (invoice_details.supplier_number = @1) AND (supplier_order_details.supplier_number = @1) AND (invoice_move.invoice_number = invoice_details.invoice_number) AND (supplier_order_details.order_number = @2) AND (global_parameters.serial_number = @0) AND (suppliers.number = @1) AND (invoice_details.material_number *= supplier_order_details.material_number) AND (invoice_details.material_number = materials.number) AND (invoice_details.mini_terminal = 1) UNION ALL SELECT packing_list_details.branch_number, global_parameters.branch_name, packing_list_details.supplier_number, suppliers.name AS suppliers_name, packing_list_move.order_number, AS employee_name, materials.barcode, materials.name, dateadd(mm, materials.min_valid_months, getdate()) AS min_valid_months, packing_list_details.expiration_date, AS ok_valid_months, supplier_order_details.material_quantity AS order_quantity, packing_list_details.expected_material_quantity AS mt_quantity, packing_list_details.pack_list_number AS doc_no, P AS doc_type, supplier_order_details.material_quantity - packing_list_details.expected_material_quantity AS diff FROM packing_list_move, packing_list_details, supplier_order_details, global_parameters, suppliers, materials WHERE (packing_list_move.branch_number = @0) AND (packing_list_move.supplier_number = @1) AND (packing_list_move.order_number = @2) AND (Lower(packing_list_move.state) <> 'd') AND (packing_list_details.branch_number = @0) AND (supplier_order_details.branch_number = @0) AND (packing_list_details.supplier_number = @1) AND (supplier_order_details.supplier_number = @1) AND (packing_list_move.pack_list_number = packing_list_details.pack_list_number) AND (supplier_order_details.order_number = @2) AND (global_parameters.serial_number = @0) AND (suppliers.number = @1) AND (packing_list_details.material_number *= supplier_order_details.material_number) AND (packing_list_details.material_number = materials.number) AND (packing_list_details.mini_terminal = 1) ";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_reportService"/> class.
        /// </summary>
		public d_mini_terminal_reportRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("I", ":�������");
            codeTable.Add("P", ":�.�����");
            LookupTables.Add("doc_type", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_report dataRow = null;
		dataRow = new d_mini_terminal_report();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.branch_name = "branch_name_0";
			dataRow.supplier_number = 0;
			dataRow.supplier_name = "supplier_name_0";
			dataRow.order_number = 0;
			dataRow.employee_name = "employee_name_0";
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.materials_name = "materials_name_0";
			dataRow.materials_min_valid_months = new DateTime(1900,1,1);
			dataRow.invoice_details_expiration_date = new DateTime(1900,1,1);
			dataRow.ok_valid_months = "ok_valid_months_0";
			dataRow.order_quantity = 0;
			dataRow.mt_quantity = 0;
			dataRow.doc_no = 0;
			dataRow.doc_type = "doc_type_0";
			dataRow.diff = 0;
			dataRow.date = "date_0";
			dataRow.row = "row_0";
			dataRow.compute = "compute_0";
		dataRow = new d_mini_terminal_report();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.branch_name = "branch_name_1";
			dataRow.supplier_number = 1;
			dataRow.supplier_name = "supplier_name_1";
			dataRow.order_number = 1;
			dataRow.employee_name = "employee_name_1";
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.materials_name = "materials_name_1";
			dataRow.materials_min_valid_months = new DateTime(1901,2,2);
			dataRow.invoice_details_expiration_date = new DateTime(1901,2,2);
			dataRow.ok_valid_months = "ok_valid_months_1";
			dataRow.order_quantity = 1;
			dataRow.mt_quantity = 1;
			dataRow.doc_no = 1;
			dataRow.doc_type = "doc_type_1";
			dataRow.diff = 1;
			dataRow.date = "date_1";
			dataRow.row = "row_1";
			dataRow.compute = "compute_1";
		dataRow = new d_mini_terminal_report();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.branch_name = "branch_name_2";
			dataRow.supplier_number = 2;
			dataRow.supplier_name = "supplier_name_2";
			dataRow.order_number = 2;
			dataRow.employee_name = "employee_name_2";
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.materials_name = "materials_name_2";
			dataRow.materials_min_valid_months = new DateTime(1902,3,3);
			dataRow.invoice_details_expiration_date = new DateTime(1902,3,3);
			dataRow.ok_valid_months = "ok_valid_months_2";
			dataRow.order_quantity = 2;
			dataRow.mt_quantity = 2;
			dataRow.doc_no = 2;
			dataRow.doc_type = "doc_type_2";
			dataRow.diff = 2;
			dataRow.date = "date_2";
			dataRow.row = "row_2";
			dataRow.compute = "compute_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_report model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.branch_name = GetValue(record, "branch_name", Convert.ToString);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.supplier_name = GetValue(record, "suppliers_name", Convert.ToString);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.employee_name = GetValue(record, "employee_name", Convert.ToString);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_name = GetValue(record, "name", Convert.ToString);
            model.materials_min_valid_months = GetDateTime(record, "min_valid_months");
            model.invoice_details_expiration_date = GetDateTime(record, "expiration_date");
            model.ok_valid_months = GetValue(record, "ok_valid_months", Convert.ToString);
            model.order_quantity = GetValue(record, "order_quantity", Convert.ToDecimal);
            model.mt_quantity = GetValue(record, "mt_quantity", Convert.ToDecimal);
            model.doc_no = GetValue(record, "doc_no", Convert.ToInt64);
            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.diff = GetValue(record, "diff", Convert.ToDecimal);
            model.date = GetValue(record, "", Convert.ToString);
            model.row = GetValue(record, "", Convert.ToString);
            model.compute = GetValue(record, "", Convert.ToString);
		}
    }
}
