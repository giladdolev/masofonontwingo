using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;
using System.Collections.Generic;

namespace Mesofon.Repository
{
    public class d_packing_list_moveRepository : RepositoryBase<d_packing_list_move>
    {
		private static readonly string _selectQuery = "SELECT 'P' AS doc_type, packing_list_move.pack_list_number, packing_list_move.branch_number, packing_list_move.supplier_number, packing_list_move.packing_type, packing_list_move.employee_number, packing_list_move.supply_date, packing_list_move.date_move, packing_list_move.discount, packing_list_move.mam, packing_list_move.station_num, packing_list_move.store_number, packing_list_move.stock_number, packing_list_move.pack_list_total, packing_list_move.log_book, packing_list_move.state, Convert(Decimal(3, 0), 0) AS external_account_tx, 0 AS external_tx_number, packing_list_move.expected_total_amount, packing_list_move.discount_percent, packing_list_move.price_list_number, packing_list_move.create_mode, packing_list_move.van_sale, packing_list_move.decline_reason, packing_list_move.order_number, packing_list_move.b2b_status, packing_list_move.payment_code_number, packing_list_move.payments_number, packing_list_move.distributor_number, 1 AS row_saved, (SELECT count(*) FROM b2b_decline_types, b2b_declines_move WHERE (b2b_decline_types.decline_number = b2b_declines_move.decline_number) AND (b2b_decline_types.decline_level = 0) AND (b2b_decline_types.is_active IN (1, 2)) AND (b2b_decline_types.user_interface = 1) AND (b2b_declines_move.branch_number = @0) AND (b2b_declines_move.supplier_number = @1) AND (b2b_declines_move.order_number = @2) AND (b2b_declines_move.parent_doc_type = 'P') AND (b2b_declines_move.material_number = 0) AND (b2b_declines_move.status = 'a') AND (b2b_declines_move.parent_doc_number = packing_list_move.pack_list_number)) AS declines, 0 AS b2b_reference_number FROM packing_list_move WHERE packing_list_move.branch_number = @0 AND packing_list_move.supplier_number = @1 AND packing_list_move.order_number = @2 AND packing_list_move.packing_type = 'p' AND Lower(packing_list_move.state) <> 'd'";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[packing_list_move] ([pack_list_number], [branch_number], [supplier_number], [packing_type] , [employee_number] , [supply_date],[date_move],[discount],[mam],[station_num],[store_number], [stock_number], [pack_list_total],[log_book],[state],[expected_total_amount],[discount_percent],[price_list_number],[create_mode],[van_sale],[decline_reason], [order_number], [b2b_status], [payment_code_number], [payments_number], [distributor_number]) VALUES(@pack_list_number , @branch_number , @supplier_number, @packing_type , @employee_number , @supply_date, @date_move, @discount, @mam, @station_num, @store_number, @stock_number , @pack_list_total, @log_book, @state, @expected_total_amount, @discount_percent , @price_list_number, @create_mode,@van_sale , @decline_reason , @order_number , @b2b_status , @payment_code_number , @payments_number , @distributor_number )";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_packing_list_moveService"/> class.
        /// </summary>
		public d_packing_list_moveRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
            Dictionary<string, object> codeTable = new Dictionary<string, object>();
            codeTable.Add("P", "�");
            codeTable.Add("I", "�");
            LookupTables.Add("doc_type", codeTable);
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_packing_list_move dataRow = null;
		dataRow = new d_packing_list_move();
		data[0] = dataRow;
			dataRow.doc_type = "doc_type_0";
			dataRow.pack_list_number = 0;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.packing_type = "packing_type_0";
			dataRow.employee_number = 0;
			dataRow.supply_date = new DateTime(1900,1,1);
			dataRow.date_move = new DateTime(1900,1,1);
			dataRow.discount = 0;
			dataRow.mam = 0;
			dataRow.station_num = 0;
			dataRow.store_number = 0;
			dataRow.stock_number = 0;
			dataRow.pack_list_total = 0;
			dataRow.log_book = 0;
			dataRow.state = "state_0";
			dataRow.external_account_tx = 0;
			dataRow.external_tx_number = 0;
			dataRow.expected_total_amount = 0;
			dataRow.discount_percent = 0;
			dataRow.price_list_number = 0;
			dataRow.create_mode = "create_mode_0";
			dataRow.van_sale = 0;
			dataRow.decline_reason = 0;
			dataRow.order_number = 0;
			dataRow.b2b_status = 0;
			dataRow.payment_code_number = 0;
			dataRow.payments_number = 0;
			dataRow.distributor_number = 0;
			dataRow.row_saved = 0;
			dataRow.declines = 0;
			dataRow.b2b_reference_number = 0;
		dataRow = new d_packing_list_move();
		data[1] = dataRow;
			dataRow.doc_type = "doc_type_1";
			dataRow.pack_list_number = 1;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.packing_type = "packing_type_1";
			dataRow.employee_number = 1;
			dataRow.supply_date = new DateTime(1901,2,2);
			dataRow.date_move = new DateTime(1901,2,2);
			dataRow.discount = 1;
			dataRow.mam = 1;
			dataRow.station_num = 1;
			dataRow.store_number = 1;
			dataRow.stock_number = 1;
			dataRow.pack_list_total = 1;
			dataRow.log_book = 1;
			dataRow.state = "state_1";
			dataRow.external_account_tx = 1;
			dataRow.external_tx_number = 1;
			dataRow.expected_total_amount = 1;
			dataRow.discount_percent = 1;
			dataRow.price_list_number = 1;
			dataRow.create_mode = "create_mode_1";
			dataRow.van_sale = 1;
			dataRow.decline_reason = 1;
			dataRow.order_number = 1;
			dataRow.b2b_status = 1;
			dataRow.payment_code_number = 1;
			dataRow.payments_number = 1;
			dataRow.distributor_number = 1;
			dataRow.row_saved = 1;
			dataRow.declines = 1;
			dataRow.b2b_reference_number = 1;
		dataRow = new d_packing_list_move();
		data[2] = dataRow;
			dataRow.doc_type = "doc_type_2";
			dataRow.pack_list_number = 2;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.packing_type = "packing_type_2";
			dataRow.employee_number = 2;
			dataRow.supply_date = new DateTime(1902,3,3);
			dataRow.date_move = new DateTime(1902,3,3);
			dataRow.discount = 2;
			dataRow.mam = 2;
			dataRow.station_num = 2;
			dataRow.store_number = 2;
			dataRow.stock_number = 2;
			dataRow.pack_list_total = 2;
			dataRow.log_book = 2;
			dataRow.state = "state_2";
			dataRow.external_account_tx = 2;
			dataRow.external_tx_number = 2;
			dataRow.expected_total_amount = 2;
			dataRow.discount_percent = 2;
			dataRow.price_list_number = 2;
			dataRow.create_mode = "create_mode_2";
			dataRow.van_sale = 2;
			dataRow.decline_reason = 2;
			dataRow.order_number = 2;
			dataRow.b2b_status = 2;
			dataRow.payment_code_number = 2;
			dataRow.payments_number = 2;
			dataRow.distributor_number = 2;
			dataRow.row_saved = 2;
			dataRow.declines = 2;
			dataRow.b2b_reference_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_packing_list_move model)
        {
            if (model == null)
            {
                return;
            }

            model.doc_type = GetValue(record, "doc_type", Convert.ToString);
            model.pack_list_number = GetValue(record, "pack_list_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.packing_type = GetValue(record, "packing_type", Convert.ToString);
            model.employee_number = GetValue(record, "employee_number", Convert.ToInt64);
            model.supply_date = GetDateTime(record, "supply_date");
            model.date_move = GetDateTime(record, "date_move");
            model.discount = GetValue(record, "discount", Convert.ToDecimal);
            model.mam = GetValue(record, "mam", Convert.ToDecimal);
            model.station_num = GetValue(record, "station_num", Convert.ToInt64);
            model.store_number = GetValue(record, "store_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.pack_list_total = GetValue(record, "pack_list_total", Convert.ToDecimal);
            model.log_book = GetValue(record, "log_book", Convert.ToInt64);
            model.state = GetValue(record, "state", Convert.ToString);
            model.external_account_tx = GetValue(record, "external_account_tx", Convert.ToInt64);
            model.external_tx_number = GetValue(record, "external_tx_number", Convert.ToInt64);
            model.expected_total_amount = GetValue(record, "expected_total_amount", Convert.ToDecimal);
            model.discount_percent = GetValue(record, "discount_percent", Convert.ToDecimal);
            model.price_list_number = GetValue(record, "price_list_number", Convert.ToInt64);
            model.create_mode = GetValue(record, "create_mode", Convert.ToString);
            model.van_sale = GetValue(record, "van_sale", Convert.ToInt64);
            model.decline_reason = GetValue(record, "decline_reason", Convert.ToInt64);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.b2b_status = GetValue(record, "b2b_status", Convert.ToInt64);
            model.payment_code_number = GetValue(record, "payment_code_number", Convert.ToInt64);
            model.payments_number = GetValue(record, "payments_number", Convert.ToInt64);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.row_saved = GetValue(record, "row_saved", Convert.ToInt64);
            model.declines = GetValue(record, "declines", Convert.ToInt64);
            model.b2b_reference_number = GetValue(record, "b2b_reference_number", Convert.ToInt64);
		}
    }
}
