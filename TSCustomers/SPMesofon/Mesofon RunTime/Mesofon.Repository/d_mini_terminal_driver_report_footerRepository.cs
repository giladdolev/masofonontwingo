using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_mini_terminal_driver_report_footerRepository : RepositoryBase<d_mini_terminal_driver_report_footer>
    {
		private static readonly string _selectQuery = "SELECT materials.barcode AS item_barcode, b2b_decline_types.decline_description, b2b_declines_move.original_quantity, b2b_declines_move.actual_quantity, b2b_declines_move.reject_quantity, b2b_decline_types.display_name, materials.name AS description, b2b_declines_move.parent_doc_number FROM b2b_decline_types, b2b_declines_move, materials WHERE b2b_declines_move.branch_number = @0 AND b2b_declines_move.supplier_number = @1 AND b2b_declines_move.parent_doc_number = @2 AND b2b_decline_types.decline_number = b2b_declines_move.decline_number AND b2b_decline_types.decline_level = 1 AND b2b_decline_types.is_active IN (1, 2) AND b2b_decline_types.user_interface = 1 AND b2b_declines_move.parent_doc_type = 'I' AND b2b_declines_move.status = 'a' AND (b2b_declines_move.material_number = materials.number) AND ((b2b_decline_types.is_automatic_applied <> 2) OR (b2b_decline_types.is_automatic_applied = 2) AND (b2b_declines_move.original_quantity <> 0 OR b2b_declines_move.actual_quantity <> 0))";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_mini_terminal_driver_report_footerService"/> class.
        /// </summary>
		public d_mini_terminal_driver_report_footerRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_mini_terminal_driver_report_footer dataRow = null;
		dataRow = new d_mini_terminal_driver_report_footer();
		data[0] = dataRow;
			dataRow.materials_item_barcode = "materials_item_barcode_0";
			dataRow.b2b_decline_types_decline_description = "b2b_decline_types_decline_description_0";
			dataRow.b2b_declines_move_original_quantity = 0;
			dataRow.b2b_declines_move_actual_quantity = 0;
			dataRow.b2b_declines_move_reject_quantity = 0;
			dataRow.b2b_decline_types_display_name = "b2b_decline_types_display_name_0";
			dataRow.description = "description_0";
			dataRow.b2b_declines_move_parent_doc_number = 0;
		dataRow = new d_mini_terminal_driver_report_footer();
		data[1] = dataRow;
			dataRow.materials_item_barcode = "materials_item_barcode_1";
			dataRow.b2b_decline_types_decline_description = "b2b_decline_types_decline_description_1";
			dataRow.b2b_declines_move_original_quantity = 1;
			dataRow.b2b_declines_move_actual_quantity = 1;
			dataRow.b2b_declines_move_reject_quantity = 1;
			dataRow.b2b_decline_types_display_name = "b2b_decline_types_display_name_1";
			dataRow.description = "description_1";
			dataRow.b2b_declines_move_parent_doc_number = 1;
		dataRow = new d_mini_terminal_driver_report_footer();
		data[2] = dataRow;
			dataRow.materials_item_barcode = "materials_item_barcode_2";
			dataRow.b2b_decline_types_decline_description = "b2b_decline_types_decline_description_2";
			dataRow.b2b_declines_move_original_quantity = 2;
			dataRow.b2b_declines_move_actual_quantity = 2;
			dataRow.b2b_declines_move_reject_quantity = 2;
			dataRow.b2b_decline_types_display_name = "b2b_decline_types_display_name_2";
			dataRow.description = "description_2";
			dataRow.b2b_declines_move_parent_doc_number = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_mini_terminal_driver_report_footer model)
        {
            if (model == null)
            {
                return;
            }

            model.materials_item_barcode = GetValue(record, "item_barcode", Convert.ToString);
            model.b2b_decline_types_decline_description = GetValue(record, "decline_description", Convert.ToString);
            model.b2b_declines_move_original_quantity = GetValue(record, "original_quantity", Convert.ToDecimal);
            model.b2b_declines_move_actual_quantity = GetValue(record, "actual_quantity", Convert.ToDecimal);
            model.b2b_declines_move_reject_quantity = GetValue(record, "reject_quantity", Convert.ToDecimal);
            model.b2b_decline_types_display_name = GetValue(record, "display_name", Convert.ToString);
            model.description = GetValue(record, "description", Convert.ToString);
            model.b2b_declines_move_parent_doc_number = GetValue(record, "parent_doc_number", Convert.ToInt64);
		}
    }
}
