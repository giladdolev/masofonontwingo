using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_sql_mt_input_pack_bodyRepository : RepositoryBase<d_sql_mt_input_pack_body>
    {
		private static readonly string _selectQuery = "SELECT materials.barcode AS barcode, materials.name AS material_name, materials.min_valid_months, packing_list_details.serial_number, packing_list_details.branch_number, packing_list_details.pack_list_number, packing_list_details.material_number, packing_list_details.material_quantity, packing_list_details.material_price, packing_list_details.supplier_number, packing_list_details.material_discount_percent, packing_list_details.material_price_after_discount, packing_list_details.quantity_within_invoice, packing_list_details.order_number, packing_list_details.stock_number, packing_list_details.sell_price, packing_list_details.bonus_quantity, packing_list_details.material_bonus_connect, packing_list_details.details, packing_list_details.max_quantity_allowed, packing_list_details.last_order_change, packing_list_details.order_serial, packing_list_details.material_discount_amount, packing_list_details.bonus_row, packing_list_details.color, packing_list_details.pay_shape, packing_list_details.indicator, packing_list_details.bonus_discount, packing_list_details.supplier_discount_percent, packing_list_details.current_catalog_sell_price, packing_list_details.expiration_date, packing_list_details.expected_material_quantity, packing_list_details.mini_terminal FROM packing_list_details, materials WHERE packing_list_details.pack_list_number = @0 AND packing_list_details.supplier_number = @1 AND packing_list_details.branch_number = @2 AND packing_list_details.material_number = materials.number";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_sql_mt_input_pack_bodyService"/> class.
        /// </summary>
		public d_sql_mt_input_pack_bodyRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_sql_mt_input_pack_body dataRow = null;
		dataRow = new d_sql_mt_input_pack_body();
		data[0] = dataRow;
			dataRow.barcode = "barcode_0";
			dataRow.material_name = "material_name_0";
			dataRow.min_valid_month = 0;
			dataRow.serial_number = 0;
			dataRow.branch_number = 0;
			dataRow.pack_list_number = 0;
			dataRow.material_number = 0;
			dataRow.material_quantity = 0;
			dataRow.material_price = 0;
			dataRow.supplier_number = 0;
			dataRow.material_discount_percent = 0;
			dataRow.material_price_after_discount = 0;
			dataRow.quantity_within_invoice = 0;
			dataRow.order_number = 0;
			dataRow.stock_number = 0;
			dataRow.sell_price = 0;
			dataRow.bonus_quantity = 0;
			dataRow.material_bonus_connect = 0;
			dataRow.details = "details_0";
			dataRow.max_quantity_allowed = 0;
			dataRow.last_order_change = 0;
			dataRow.order_serial = 0;
			dataRow.material_discount_amount = 0;
			dataRow.bonus_row = "bonus_row_0";
			dataRow.color = 0;
			dataRow.pay_shape = 0;
			dataRow.indicator = 0;
			dataRow.bonus_discount = 0;
			dataRow.supplier_discount_percent = 0;
			dataRow.current_catalog_sell_price = 0;
			dataRow.expiration_date = new DateTime(1900,1,1);
			dataRow.expected_material_quantity = 0;
			dataRow.mini_terminal = 0;
			dataRow.compute_1 = "compute_1_0";
			dataRow.material_name_com = "material_name_com_0";
		dataRow = new d_sql_mt_input_pack_body();
		data[1] = dataRow;
			dataRow.barcode = "barcode_1";
			dataRow.material_name = "material_name_1";
			dataRow.min_valid_month = 1;
			dataRow.serial_number = 1;
			dataRow.branch_number = 1;
			dataRow.pack_list_number = 1;
			dataRow.material_number = 1;
			dataRow.material_quantity = 1;
			dataRow.material_price = 1;
			dataRow.supplier_number = 1;
			dataRow.material_discount_percent = 1;
			dataRow.material_price_after_discount = 1;
			dataRow.quantity_within_invoice = 1;
			dataRow.order_number = 1;
			dataRow.stock_number = 1;
			dataRow.sell_price = 1;
			dataRow.bonus_quantity = 1;
			dataRow.material_bonus_connect = 1;
			dataRow.details = "details_1";
			dataRow.max_quantity_allowed = 1;
			dataRow.last_order_change = 1;
			dataRow.order_serial = 1;
			dataRow.material_discount_amount = 1;
			dataRow.bonus_row = "bonus_row_1";
			dataRow.color = 1;
			dataRow.pay_shape = 1;
			dataRow.indicator = 1;
			dataRow.bonus_discount = 1;
			dataRow.supplier_discount_percent = 1;
			dataRow.current_catalog_sell_price = 1;
			dataRow.expiration_date = new DateTime(1901,2,2);
			dataRow.expected_material_quantity = 1;
			dataRow.mini_terminal = 1;
			dataRow.compute_1 = "compute_1_1";
			dataRow.material_name_com = "material_name_com_1";
		dataRow = new d_sql_mt_input_pack_body();
		data[2] = dataRow;
			dataRow.barcode = "barcode_2";
			dataRow.material_name = "material_name_2";
			dataRow.min_valid_month = 2;
			dataRow.serial_number = 2;
			dataRow.branch_number = 2;
			dataRow.pack_list_number = 2;
			dataRow.material_number = 2;
			dataRow.material_quantity = 2;
			dataRow.material_price = 2;
			dataRow.supplier_number = 2;
			dataRow.material_discount_percent = 2;
			dataRow.material_price_after_discount = 2;
			dataRow.quantity_within_invoice = 2;
			dataRow.order_number = 2;
			dataRow.stock_number = 2;
			dataRow.sell_price = 2;
			dataRow.bonus_quantity = 2;
			dataRow.material_bonus_connect = 2;
			dataRow.details = "details_2";
			dataRow.max_quantity_allowed = 2;
			dataRow.last_order_change = 2;
			dataRow.order_serial = 2;
			dataRow.material_discount_amount = 2;
			dataRow.bonus_row = "bonus_row_2";
			dataRow.color = 2;
			dataRow.pay_shape = 2;
			dataRow.indicator = 2;
			dataRow.bonus_discount = 2;
			dataRow.supplier_discount_percent = 2;
			dataRow.current_catalog_sell_price = 2;
			dataRow.expiration_date = new DateTime(1902,3,3);
			dataRow.expected_material_quantity = 2;
			dataRow.mini_terminal = 2;
			dataRow.compute_1 = "compute_1_2";
			dataRow.material_name_com = "material_name_com_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_sql_mt_input_pack_body model)
        {
            if (model == null)
            {
                return;
            }

            model.barcode = GetValue(record, "barcode", Convert.ToString);
            model.material_name = GetValue(record, "material_name", Convert.ToString);
            model.min_valid_month = GetValue(record, "min_valid_months", Convert.ToInt64);
            model.serial_number = GetValue(record, "serial_number", Convert.ToInt64);
            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.pack_list_number = GetValue(record, "pack_list_number", Convert.ToInt64);
            model.material_number = GetValue(record, "material_number", Convert.ToInt64);
            model.material_quantity = GetValue(record, "material_quantity", Convert.ToDecimal);
            model.material_price = GetValue(record, "material_price", Convert.ToDecimal);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.material_discount_percent = GetValue(record, "material_discount_percent", Convert.ToDecimal);
            model.material_price_after_discount = GetValue(record, "material_price_after_discount", Convert.ToDecimal);
            model.quantity_within_invoice = GetValue(record, "quantity_within_invoice", Convert.ToDecimal);
            model.order_number = GetValue(record, "order_number", Convert.ToInt64);
            model.stock_number = GetValue(record, "stock_number", Convert.ToInt64);
            model.sell_price = GetValue(record, "sell_price", Convert.ToDecimal);
            model.bonus_quantity = GetValue(record, "bonus_quantity", Convert.ToDecimal);
            model.material_bonus_connect = GetValue(record, "material_bonus_connect", Convert.ToInt64);
            model.details = GetValue(record, "details", Convert.ToString);
            model.max_quantity_allowed = GetValue(record, "max_quantity_allowed", Convert.ToDecimal);
            model.last_order_change = GetValue(record, "last_order_change", Convert.ToDecimal);
            model.order_serial = GetValue(record, "order_serial", Convert.ToInt64);
            model.material_discount_amount = GetValue(record, "material_discount_amount", Convert.ToDecimal);
            model.bonus_row = GetValue(record, "bonus_row", Convert.ToString);
            model.color = GetValue(record, "color", Convert.ToInt64);
            model.pay_shape = GetValue(record, "pay_shape", Convert.ToInt64);
            model.indicator = GetValue(record, "indicator", Convert.ToInt64);
            model.bonus_discount = GetValue(record, "bonus_discount", Convert.ToDecimal);
            model.supplier_discount_percent = GetValue(record, "supplier_discount_percent", Convert.ToDecimal);
            model.current_catalog_sell_price = GetValue(record, "current_catalog_sell_price", Convert.ToDecimal);
            model.expiration_date = GetDateTime(record, "expiration_date");
            model.expected_material_quantity = GetValue(record, "expected_material_quantity", Convert.ToDecimal);
            model.mini_terminal = GetValue(record, "mini_terminal", Convert.ToInt64);
            model.compute_1 = GetValue(record, "", Convert.ToString);
            model.material_name_com = GetValue(record, "", Convert.ToString);
		}
    }
}
