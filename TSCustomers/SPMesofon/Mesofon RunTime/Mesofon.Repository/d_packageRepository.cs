using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_packageRepository : RepositoryBase<d_package>
    {
		private static readonly string _selectQuery = "SELECT materials.name, materials.barcode, materials.number, packages.pack_barcode, packages.quantity, 0 AS actual_quantity FROM packages, materials WHERE packages.item_number = materials.number AND packages.supplier_number = @0 AND packages.pack_barcode = @1 AND packages.status = 'a' AND packages.packing_level = 1";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_packageService"/> class.
        /// </summary>
		public d_packageRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_package dataRow = null;
		dataRow = new d_package();
		data[0] = dataRow;
			dataRow.materials_name = "materials_name_0";
			dataRow.materials_barcode = "materials_barcode_0";
			dataRow.materials_number = 0;
			dataRow.packages_pack_barcode = "packages_pack_barcode_0";
			dataRow.packages_quantity = 0;
			dataRow.actual_quantity = 0;
			dataRow.cf_total_quantity = "cf_total_quantity_0";
		dataRow = new d_package();
		data[1] = dataRow;
			dataRow.materials_name = "materials_name_1";
			dataRow.materials_barcode = "materials_barcode_1";
			dataRow.materials_number = 1;
			dataRow.packages_pack_barcode = "packages_pack_barcode_1";
			dataRow.packages_quantity = 1;
			dataRow.actual_quantity = 1;
			dataRow.cf_total_quantity = "cf_total_quantity_1";
		dataRow = new d_package();
		data[2] = dataRow;
			dataRow.materials_name = "materials_name_2";
			dataRow.materials_barcode = "materials_barcode_2";
			dataRow.materials_number = 2;
			dataRow.packages_pack_barcode = "packages_pack_barcode_2";
			dataRow.packages_quantity = 2;
			dataRow.actual_quantity = 2;
			dataRow.cf_total_quantity = "cf_total_quantity_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_package model)
        {
            if (model == null)
            {
                return;
            }

            model.materials_name = GetValue(record, "name", Convert.ToString);
            model.materials_barcode = GetValue(record, "barcode", Convert.ToString);
            model.materials_number = GetValue(record, "number", Convert.ToInt64);
            model.packages_pack_barcode = GetValue(record, "pack_barcode", Convert.ToString);
            model.packages_quantity = GetValue(record, "quantity", Convert.ToDecimal);
            model.actual_quantity = GetValue(record, "actual_quantity", Convert.ToInt64);
            model.cf_total_quantity = GetValue(record, "", Convert.ToString);
		}
    }
}
