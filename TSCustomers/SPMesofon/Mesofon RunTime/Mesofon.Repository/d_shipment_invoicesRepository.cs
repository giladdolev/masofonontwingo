using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_shipment_invoicesRepository : RepositoryBase<d_shipment_invoices>
    {
		private static readonly string _selectQuery = "SELECT DISTINCT shipment_pallets.branch_number, shipment_pallets.supplier_number, shipment_pallets.invoice_number, shipment_pallets.order_number, shipment_pallets.pallet_number FROM shipment_pallets WHERE (shipment_pallets.branch_number = @0) AND (shipment_pallets.shipment_number = @1) AND (shipment_pallets.marlog_number = @2)";
		private static readonly string _insertQuery = "";
		private static readonly string _updateKeyOnlyQuery = "";
		private static readonly string _updateColumnsQuery = "";
		private static readonly string _deleteQuery = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_shipment_invoicesService"/> class.
        /// </summary>
		public d_shipment_invoicesRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_shipment_invoices dataRow = null;
		dataRow = new d_shipment_invoices();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.invoice_number = 0;
			dataRow.order_number = 0;
			dataRow.pallet_number = "pallet_number_0";
		dataRow = new d_shipment_invoices();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.invoice_number = 1;
			dataRow.order_number = 1;
			dataRow.pallet_number = "pallet_number_1";
		dataRow = new d_shipment_invoices();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.invoice_number = 2;
			dataRow.order_number = 2;
			dataRow.pallet_number = "pallet_number_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_shipment_invoices model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.invoice_number = GetValue(record, "invoice_number", Convert.ToDouble);
            model.order_number = GetValue(record, "order_number", Convert.ToDouble);
            model.pallet_number = GetValue(record, "pallet_number", Convert.ToString);
		}
    }
}
