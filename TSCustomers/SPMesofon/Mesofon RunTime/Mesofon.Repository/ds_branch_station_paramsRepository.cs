using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class ds_branch_station_paramsRepository : RepositoryBase<ds_branch_station_params>
    {
		private static readonly string _selectQuery = "SELECT station_parameters.branch_number, station_parameters.station_number, station_parameters.parameter_name, station_parameters.parameter_type, station_parameters.parameter_value, station_parameters.default_value, station_parameters.description, station_parameters.parameter_entity FROM station_parameters WHERE station_parameters.branch_number IN (0,@0) AND station_parameters.station_number IN (0,@1) order by station_parameters.parameter_name Asc, station_parameters.branch_number Desc, station_parameters.station_number Desc";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[station_parameters] ([branch_number], [station_number], [parameter_name], [parameter_type], [parameter_value], [default_value], [description], [parameter_entity]) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[station_parameters] SET [branch_number] = @p1, [station_number] = @p2, [parameter_name] = @p3, [parameter_type] = @p4, [parameter_value] = @p5, [default_value] = @p6, [description] = @p7, [parameter_entity] = @p8 WHERE (([branch_number] = @p9) AND ([station_number] = @p10) AND ([parameter_name] = @p11))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[station_parameters] SET [branch_number] = @p1, [station_number] = @p2, [parameter_name] = @p3, [parameter_type] = @p4, [parameter_value] = @p5, [default_value] = @p6, [description] = @p7, [parameter_entity] = @p8 WHERE (([branch_number] = @p9) AND ([station_number] = @p10) AND ([parameter_name] = @p11) AND ([parameter_type] = @p12) AND ([parameter_value] = @p13) AND ([default_value] = @p14) AND ([description] = @p15) AND ([parameter_entity] = @p16))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[station_parameters] WHERE (([branch_number] = @p1) AND ([station_number] = @p2) AND ([parameter_name] = @p3))";

        /// <summary>
        /// Initializes a new instance of the <see cref="ds_branch_station_paramsService"/> class.
        /// </summary>
		public ds_branch_station_paramsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			ds_branch_station_params dataRow = null;
		dataRow = new ds_branch_station_params();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.station_number = 0;
			dataRow.parameter_name = "parameter_name_0";
			dataRow.parameter_type = "parameter_type_0";
			dataRow.parameter_value = "parameter_value_0";
			dataRow.default_value = "default_value_0";
			dataRow.description = "description_0";
			dataRow.parameter_entity = 0;
		dataRow = new ds_branch_station_params();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.station_number = 1;
			dataRow.parameter_name = "parameter_name_1";
			dataRow.parameter_type = "parameter_type_1";
			dataRow.parameter_value = "parameter_value_1";
			dataRow.default_value = "default_value_1";
			dataRow.description = "description_1";
			dataRow.parameter_entity = 1;
		dataRow = new ds_branch_station_params();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.station_number = 2;
			dataRow.parameter_name = "parameter_name_2";
			dataRow.parameter_type = "parameter_type_2";
			dataRow.parameter_value = "parameter_value_2";
			dataRow.default_value = "default_value_2";
			dataRow.description = "description_2";
			dataRow.parameter_entity = 2;
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, ds_branch_station_params model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.station_number = GetValue(record, "station_number", Convert.ToInt64);
            model.parameter_name = GetValue(record, "parameter_name", Convert.ToString);
            model.parameter_type = GetValue(record, "parameter_type", Convert.ToString);
            model.parameter_value = GetValue(record, "parameter_value", Convert.ToString);
            model.default_value = GetValue(record, "default_value", Convert.ToString);
            model.description = GetValue(record, "description", Convert.ToString);
            model.parameter_entity = GetValue(record, "parameter_entity", Convert.ToInt64);
		}
    }
}
