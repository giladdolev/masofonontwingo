using System;
using System.Data;
using Mesofon.Models;
using Common.Transposition.Extensions;

namespace Mesofon.Repository
{
    public class d_addhock_supplier_distributorsRepository : RepositoryBase<d_addhock_supplier_distributors>
    {
		private static readonly string _selectQuery = "SELECT dbo.supplier_distributors.branch_number, dbo.supplier_distributors.supplier_number, dbo.supplier_distributors.distributor_number, dbo.supplier_distributors.is_active, dbo.supplier_distributors.edi_number AS value FROM dbo.supplier_distributors WHERE dbo.supplier_distributors.is_active = @0";
		private static readonly string _insertQuery = "INSERT INTO [dbo].[supplier_distributors] ([branch_number], [supplier_number], [distributor_number], [is_active], [edi_number]) VALUES (@p1, @p2, @p3, @p4, @p5)";
		private static readonly string _updateKeyOnlyQuery = "UPDATE [dbo].[supplier_distributors] SET [branch_number] = @p1, [supplier_number] = @p2, [distributor_number] = @p3, [is_active] = @p4, [edi_number] = @p5 WHERE (([branch_number] = @p6) AND ([supplier_number] = @p7) AND ([distributor_number] = @p8) AND ([is_active] = @p9))";
		private static readonly string _updateColumnsQuery = "UPDATE [dbo].[supplier_distributors] SET [branch_number] = @p1, [supplier_number] = @p2, [distributor_number] = @p3, [is_active] = @p4, [edi_number] = @p5 WHERE (([branch_number] = @p6) AND ([supplier_number] = @p7) AND ([distributor_number] = @p8) AND ([is_active] = @p9) AND ((@p10 = 1 AND [edi_number] IS NULL) OR ([edi_number] = @p11)))";
		private static readonly string _deleteQuery = "DELETE FROM [dbo].[supplier_distributors] WHERE (([branch_number] = @p1) AND ([supplier_number] = @p2) AND ([distributor_number] = @p3) AND ([is_active] = @p4))";

        /// <summary>
        /// Initializes a new instance of the <see cref="d_addhock_supplier_distributorsService"/> class.
        /// </summary>
		public d_addhock_supplier_distributorsRepository() : base (_selectQuery, _updateKeyOnlyQuery, _updateColumnsQuery, _deleteQuery,_insertQuery)
        {
        }

        /// <summary>
        /// Get repository filled with mock data.
        /// </summary>
		protected override ModelBase[] GetMockData()
		{
			ModelBase[] data = new ModelBase[3];
			d_addhock_supplier_distributors dataRow = null;
		dataRow = new d_addhock_supplier_distributors();
		data[0] = dataRow;
			dataRow.branch_number = 0;
			dataRow.supplier_number = 0;
			dataRow.distributor_number = 0;
			dataRow.is_active = 0;
			dataRow.value = "value_0";
		dataRow = new d_addhock_supplier_distributors();
		data[1] = dataRow;
			dataRow.branch_number = 1;
			dataRow.supplier_number = 1;
			dataRow.distributor_number = 1;
			dataRow.is_active = 1;
			dataRow.value = "value_1";
		dataRow = new d_addhock_supplier_distributors();
		data[2] = dataRow;
			dataRow.branch_number = 2;
			dataRow.supplier_number = 2;
			dataRow.distributor_number = 2;
			dataRow.is_active = 2;
			dataRow.value = "value_2";
			return data;
		}

        /// <summary>
        /// Maps the specified record to model.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="model">The model.</param>
        public override void Map(IDataRecord record, d_addhock_supplier_distributors model)
        {
            if (model == null)
            {
                return;
            }

            model.branch_number = GetValue(record, "branch_number", Convert.ToInt64);
            model.supplier_number = GetValue(record, "supplier_number", Convert.ToInt64);
            model.distributor_number = GetValue(record, "distributor_number", Convert.ToInt64);
            model.is_active = GetValue(record, "is_active", Convert.ToInt64);
            model.value = GetValue(record, "value", Convert.ToString);
		}
    }
}
