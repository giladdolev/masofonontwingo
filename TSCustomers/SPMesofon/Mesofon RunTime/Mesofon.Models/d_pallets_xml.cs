using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_pallets_xml : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_pallets_xml()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="receiver">The receiver.</param>
		/// <param name="doctype">The doctype.</param>
		/// <param name="aprf">The aprf.</param>
		/// <param name="snrf">The snrf.</param>
		/// <param name="ackn">The ackn.</param>
		/// <param name="testind">The testind.</param>
		/// <param name="messdate">The messdate.</param>
		/// <param name="messtime">The messtime.</param>
		/// <param name="deliverydate">The deliverydate.</param>
		/// <param name="documenttype">The documenttype.</param>
		/// <param name="palletbarcode">The palletbarcode.</param>
		/// <param name="retailno">The retailno.</param>
		/// <param name="retailname">The retailname.</param>
		/// <param name="cdno">The cdno.</param>
		/// <param name="cdname">The cdname.</param>
		/// <param name="documentno">The documentno.</param>
		/// <param name="extra">The extra.</param>
		/// <param name="rotation">The rotation.</param>
		/// <param name="docconfrtype">The docconfrtype.</param>
		/// <param name="subsupplier">The subsupplier.</param>
		/// <param name="disagrtype">The disagrtype.</param>
		/// <param name="extradetail">The extradetail.</param>
        public d_pallets_xml(string sender, string receiver, string doctype, string aprf, string snrf, string ackn, string testind, string messdate, string messtime, string deliverydate, string documenttype, string palletbarcode, string retailno, string retailname, string cdno, string cdname, double documentno, double extra, string rotation, string docconfrtype, string subsupplier, string disagrtype, double extradetail)
        {
			this._sender = sender;
			this._receiver = receiver;
			this._doctype = doctype;
			this._aprf = aprf;
			this._snrf = snrf;
			this._ackn = ackn;
			this._testind = testind;
			this._messdate = messdate;
			this._messtime = messtime;
			this._deliverydate = deliverydate;
			this._documenttype = documenttype;
			this._palletbarcode = palletbarcode;
			this._retailno = retailno;
			this._retailname = retailname;
			this._cdno = cdno;
			this._cdname = cdname;
			this._documentno = documentno;
			this._extra = extra;
			this._rotation = rotation;
			this._docconfrtype = docconfrtype;
			this._subsupplier = subsupplier;
			this._disagrtype = disagrtype;
			this._extradetail = extradetail;
		}

	
        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
        private string _sender;

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
	    [Editable(true)]
        [Column("sender")]
        [Properties("sender", "sender", true, 0, typeof(string))]
        public string sender
        {
            get { return _sender; }
            set
            {
                if (_sender != value)
                {
                    PropertyChanged("sender");
					_sender = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
        private string _receiver;

        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
	    [Editable(true)]
        [Column("receiver")]
        [Properties("receiver", "receiver", true, 1, typeof(string))]
        public string receiver
        {
            get { return _receiver; }
            set
            {
                if (_receiver != value)
                {
                    PropertyChanged("receiver");
					_receiver = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
        private string _doctype;

        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
	    [Editable(true)]
        [Column("doctype")]
        [Properties("doctype", "doctype", true, 2, typeof(string))]
        public string doctype
        {
            get { return _doctype; }
            set
            {
                if (_doctype != value)
                {
                    PropertyChanged("doctype");
					_doctype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
        private string _aprf;

        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
	    [Editable(true)]
        [Column("aprf")]
        [Properties("aprf", "aprf", true, 3, typeof(string))]
        public string aprf
        {
            get { return _aprf; }
            set
            {
                if (_aprf != value)
                {
                    PropertyChanged("aprf");
					_aprf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
        private string _snrf;

        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
	    [Editable(true)]
        [Column("snrf")]
        [Properties("snrf", "snrf", true, 4, typeof(string))]
        public string snrf
        {
            get { return _snrf; }
            set
            {
                if (_snrf != value)
                {
                    PropertyChanged("snrf");
					_snrf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
        private string _ackn;

        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
	    [Editable(true)]
        [Column("ackn")]
        [Properties("ackn", "ackn", true, 5, typeof(string))]
        public string ackn
        {
            get { return _ackn; }
            set
            {
                if (_ackn != value)
                {
                    PropertyChanged("ackn");
					_ackn = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
        private string _testind;

        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
	    [Editable(true)]
        [Column("testind")]
        [Properties("testind", "testind", true, 6, typeof(string))]
        public string testind
        {
            get { return _testind; }
            set
            {
                if (_testind != value)
                {
                    PropertyChanged("testind");
					_testind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
        private string _messdate;

        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
	    [Editable(true)]
        [Column("messdate")]
        [Properties("messdate", "messdate", true, 7, typeof(string))]
        public string messdate
        {
            get { return _messdate; }
            set
            {
                if (_messdate != value)
                {
                    PropertyChanged("messdate");
					_messdate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
        private string _messtime;

        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
	    [Editable(true)]
        [Column("messtime")]
        [Properties("messtime", "messtime", true, 8, typeof(string))]
        public string messtime
        {
            get { return _messtime; }
            set
            {
                if (_messtime != value)
                {
                    PropertyChanged("messtime");
					_messtime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the deliverydate.
        /// </summary>
        /// <value>
        /// The deliverydate.
        /// </value>
        private string _deliverydate;

        /// <summary>
        /// Gets or sets the deliverydate.
        /// </summary>
        /// <value>
        /// The deliverydate.
        /// </value>
	    [Editable(true)]
        [Column("deliverydate")]
        [Properties("deliverydate", "deliverydate", true, 9, typeof(string))]
        public string deliverydate
        {
            get { return _deliverydate; }
            set
            {
                if (_deliverydate != value)
                {
                    PropertyChanged("deliverydate");
					_deliverydate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the documenttype.
        /// </summary>
        /// <value>
        /// The documenttype.
        /// </value>
        private string _documenttype;

        /// <summary>
        /// Gets or sets the documenttype.
        /// </summary>
        /// <value>
        /// The documenttype.
        /// </value>
	    [Editable(true)]
        [Column("documenttype")]
        [Properties("documenttype", "documenttype", true, 17, typeof(string))]
        public string documenttype
        {
            get { return _documenttype; }
            set
            {
                if (_documenttype != value)
                {
                    PropertyChanged("documenttype");
					_documenttype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the palletbarcode.
        /// </summary>
        /// <value>
        /// The palletbarcode.
        /// </value>
        private string _palletbarcode;

        /// <summary>
        /// Gets or sets the palletbarcode.
        /// </summary>
        /// <value>
        /// The palletbarcode.
        /// </value>
	    [Editable(true)]
        [Column("palletbarcode")]
        [Properties("palletbarcode", "palletbarcode", true, 18, typeof(string))]
        public string palletbarcode
        {
            get { return _palletbarcode; }
            set
            {
                if (_palletbarcode != value)
                {
                    PropertyChanged("palletbarcode");
					_palletbarcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the retailno.
        /// </summary>
        /// <value>
        /// The retailno.
        /// </value>
        private string _retailno;

        /// <summary>
        /// Gets or sets the retailno.
        /// </summary>
        /// <value>
        /// The retailno.
        /// </value>
	    [Editable(true)]
        [Column("retailno")]
        [Properties("retailno", "retailno", true, 11, typeof(string))]
        public string retailno
        {
            get { return _retailno; }
            set
            {
                if (_retailno != value)
                {
                    PropertyChanged("retailno");
					_retailno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the retailname.
        /// </summary>
        /// <value>
        /// The retailname.
        /// </value>
        private string _retailname;

        /// <summary>
        /// Gets or sets the retailname.
        /// </summary>
        /// <value>
        /// The retailname.
        /// </value>
	    [Editable(true)]
        [Column("retailname")]
        [Properties("retailname", "retailname", true, 12, typeof(string))]
        public string retailname
        {
            get { return _retailname; }
            set
            {
                if (_retailname != value)
                {
                    PropertyChanged("retailname");
					_retailname = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cdno.
        /// </summary>
        /// <value>
        /// The cdno.
        /// </value>
        private string _cdno;

        /// <summary>
        /// Gets or sets the cdno.
        /// </summary>
        /// <value>
        /// The cdno.
        /// </value>
	    [Editable(true)]
        [Column("cdno")]
        [Properties("cdno", "cdno", true, 13, typeof(string))]
        public string cdno
        {
            get { return _cdno; }
            set
            {
                if (_cdno != value)
                {
                    PropertyChanged("cdno");
					_cdno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cdname.
        /// </summary>
        /// <value>
        /// The cdname.
        /// </value>
        private string _cdname;

        /// <summary>
        /// Gets or sets the cdname.
        /// </summary>
        /// <value>
        /// The cdname.
        /// </value>
	    [Editable(true)]
        [Column("cdname")]
        [Properties("cdname", "cdname", true, 14, typeof(string))]
        public string cdname
        {
            get { return _cdname; }
            set
            {
                if (_cdname != value)
                {
                    PropertyChanged("cdname");
					_cdname = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the documentno.
        /// </summary>
        /// <value>
        /// The documentno.
        /// </value>
        private double _documentno;

        /// <summary>
        /// Gets or sets the documentno.
        /// </summary>
        /// <value>
        /// The documentno.
        /// </value>
	    [Editable(true)]
        [Column("documentno")]
        [Properties("documentno", "documentno", true, 21, typeof(double))]
        public double documentno
        {
            get { return _documentno; }
            set
            {
                if (_documentno != value)
                {
                    PropertyChanged("documentno");
					_documentno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the extra.
        /// </summary>
        /// <value>
        /// The extra.
        /// </value>
        private double _extra;

        /// <summary>
        /// Gets or sets the extra.
        /// </summary>
        /// <value>
        /// The extra.
        /// </value>
	    [Editable(true)]
        [Column("extra")]
        [Properties("extra", "extra", true, 10, typeof(double))]
        public double extra
        {
            get { return _extra; }
            set
            {
                if (_extra != value)
                {
                    PropertyChanged("extra");
					_extra = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        private string _rotation;

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
	    [Editable(true)]
        [Column("rotation")]
        [Properties("rotation", "rotation", true, 15, typeof(string))]
        public string rotation
        {
            get { return _rotation; }
            set
            {
                if (_rotation != value)
                {
                    PropertyChanged("rotation");
					_rotation = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the docconfrtype.
        /// </summary>
        /// <value>
        /// The docconfrtype.
        /// </value>
        private string _docconfrtype;

        /// <summary>
        /// Gets or sets the docconfrtype.
        /// </summary>
        /// <value>
        /// The docconfrtype.
        /// </value>
	    [Editable(true)]
        [Column("docconfrtype")]
        [Properties("docconfrtype", "docconfrtype", true, 16, typeof(string))]
        public string docconfrtype
        {
            get { return _docconfrtype; }
            set
            {
                if (_docconfrtype != value)
                {
                    PropertyChanged("docconfrtype");
					_docconfrtype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the subsupplier.
        /// </summary>
        /// <value>
        /// The subsupplier.
        /// </value>
        private string _subsupplier;

        /// <summary>
        /// Gets or sets the subsupplier.
        /// </summary>
        /// <value>
        /// The subsupplier.
        /// </value>
	    [Editable(true)]
        [Column("subsupplier")]
		public string subsupplier
        {
            get { return _subsupplier; }
            set
            {
                if (_subsupplier != value)
                {
                    PropertyChanged("subsupplier");
					_subsupplier = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the disagrtype.
        /// </summary>
        /// <value>
        /// The disagrtype.
        /// </value>
        private string _disagrtype;

        /// <summary>
        /// Gets or sets the disagrtype.
        /// </summary>
        /// <value>
        /// The disagrtype.
        /// </value>
	    [Editable(true)]
        [Column("disagrtype")]
        [Properties("disagrtype", "disagrtype", true, 19, typeof(string))]
        public string disagrtype
        {
            get { return _disagrtype; }
            set
            {
                if (_disagrtype != value)
                {
                    PropertyChanged("disagrtype");
					_disagrtype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the extradetail.
        /// </summary>
        /// <value>
        /// The extradetail.
        /// </value>
        private double _extradetail;

        /// <summary>
        /// Gets or sets the extradetail.
        /// </summary>
        /// <value>
        /// The extradetail.
        /// </value>
	    [Editable(true)]
        [Column("extradetail")]
        [Properties("extradetail", "extradetail", true, 20, typeof(double))]
        public double extradetail
        {
            get { return _extradetail; }
            set
            {
                if (_extradetail != value)
                {
                    PropertyChanged("extradetail");
					_extradetail = value;
                    
                }
            }
        }
	}
}
