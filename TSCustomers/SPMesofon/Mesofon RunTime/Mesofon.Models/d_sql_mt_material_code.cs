using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_sql_mt_material_code : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_sql_mt_material_code()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="number">The number.</param>
		/// <param name="code">The code.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="name">The name.</param>
        public d_sql_mt_material_code(long number, string code, string barcode, string name)
        {
			this._number = number;
			this._code = code;
			this._barcode = barcode;
			this._name = name;
		}

	
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        private long _number;

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("number")]
        [Properties("number", "number", false, 2, typeof(long))]
        public long number
        {
            get { return _number; }
            set
            {
                if (_number != value)
                {
                    PropertyChanged("number");
					_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        private string _code;

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
	    [Editable(true)]
        [Column("code")]
        [Properties("code", "code", false, 3, typeof(string))]
        public string code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    PropertyChanged("code");
					_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
        [Properties("barcode", "�����", true, 0, typeof(string), width: 149)]
        public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string _name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
	    [Editable(true)]
        [Column("name")]
        [Properties("name", "�� �����", true, 1, typeof(string), width:149)]
        public string name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    PropertyChanged("name");
					_name = value;
                    
                }
            }
        }
	}
}
