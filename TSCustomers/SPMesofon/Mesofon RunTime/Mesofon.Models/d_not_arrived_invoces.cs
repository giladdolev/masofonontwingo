using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_not_arrived_invoces : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_not_arrived_invoces()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="row">The row.</param>
        public d_not_arrived_invoces(double supplier_number, double invoice_number, string row)
        {
			this._supplier_number = supplier_number;
			this._invoice_number = invoice_number;
			this._row = row;
		}

	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private double _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [System.Extensions.Properties("supplier_number", "���� ���", true, 1, typeof(double))]
        public double supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private double _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
        [System.Extensions.Properties("invoice_number", "���� �������", true, 2, typeof(long))]
        public double invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
        [System.Extensions.Properties("row", "��", true, 0, typeof(string),70)]
        [TextExpressionAttribute("getrow()", "", "")]
        public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	}
}
