using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_sql_mini_terminal_input_body : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_sql_mini_terminal_input_body()
        {
            this._details = "";
            this._stock_number = 1;
            this._bonus_row = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="details">The details.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="material_price_after_discount">The material_price_after_discount.</param>
		/// <param name="packing_list_number">The packing_list_number.</param>
		/// <param name="packing_serial">The packing_serial.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="material_bonus_connect">The material_bonus_connect.</param>
		/// <param name="max_quantity_allowed">The max_quantity_allowed.</param>
		/// <param name="last_packing_change">The last_packing_change.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="material_discount_amount">The material_discount_amount.</param>
		/// <param name="color">The color.</param>
		/// <param name="bonus_row">The bonus_row.</param>
		/// <param name="pay_shape">The pay_shape.</param>
		/// <param name="indicator">The indicator.</param>
		/// <param name="bonus_discount">The bonus_discount.</param>
		/// <param name="supplier_discount_percent">The supplier_discount_percent.</param>
		/// <param name="current_catalog_sell_price">The current_catalog_sell_price.</param>
		/// <param name="expiration_date">The expiration_date.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="mini_terminal">The mini_terminal.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="material_name">The material_name.</param>
		/// <param name="min_valid_month">The min_valid_month.</param>
        public d_sql_mini_terminal_input_body(long serial_number, long branch_number, long invoice_number, long material_number, decimal material_quantity, decimal material_price, string details, long supplier_number, decimal material_discount_percent, decimal material_price_after_discount, long packing_list_number, long packing_serial, long stock_number, decimal sell_price, decimal bonus_quantity, long material_bonus_connect, decimal max_quantity_allowed, decimal last_packing_change, string import_type, decimal material_discount_amount, long color, string bonus_row, long pay_shape, long indicator, decimal bonus_discount, decimal supplier_discount_percent, decimal current_catalog_sell_price, DateTime? expiration_date, decimal expected_material_quantity, long mini_terminal, string barcode, string material_name, long min_valid_month)
        {
			this._serial_number = serial_number;
			this._branch_number = branch_number;
			this._invoice_number = invoice_number;
			this._material_number = material_number;
			this._material_quantity = material_quantity;
			this._material_price = material_price;
			this._details = details;
			this._supplier_number = supplier_number;
			this._material_discount_percent = material_discount_percent;
			this._material_price_after_discount = material_price_after_discount;
			this._packing_list_number = packing_list_number;
			this._packing_serial = packing_serial;
			this._stock_number = stock_number;
			this._sell_price = sell_price;
			this._bonus_quantity = bonus_quantity;
			this._material_bonus_connect = material_bonus_connect;
			this._max_quantity_allowed = max_quantity_allowed;
			this._last_packing_change = last_packing_change;
			this._import_type = import_type;
			this._material_discount_amount = material_discount_amount;
			this._color = color;
			this._bonus_row = bonus_row;
			this._pay_shape = pay_shape;
			this._indicator = indicator;
			this._bonus_discount = bonus_discount;
			this._supplier_discount_percent = supplier_discount_percent;
			this._current_catalog_sell_price = current_catalog_sell_price;
			this._expiration_date = expiration_date;
			this._expected_material_quantity = expected_material_quantity;
			this._mini_terminal = mini_terminal;
			this._barcode = barcode;
			this._material_name = material_name;
			this._min_valid_month = min_valid_month;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
		public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
		public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
        private decimal _material_price_after_discount;

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_discount")]
		public decimal material_price_after_discount
        {
            get { return _material_price_after_discount; }
            set
            {
                if (_material_price_after_discount != value)
                {
                    PropertyChanged("material_price_after_discount");
					_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
        private long _packing_list_number;

        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_number")]
		public long packing_list_number
        {
            get { return _packing_list_number; }
            set
            {
                if (_packing_list_number != value)
                {
                    PropertyChanged("packing_list_number");
					_packing_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
        private long _packing_serial;

        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
	    [Editable(true)]
        [Column("packing_serial")]
		public long packing_serial
        {
            get { return _packing_serial; }
            set
            {
                if (_packing_serial != value)
                {
                    PropertyChanged("packing_serial");
					_packing_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
		public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
		public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The max_quantity_allowed.
        /// </value>
        private decimal _max_quantity_allowed;

        /// <summary>
        /// Gets or sets the max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The max_quantity_allowed.
        /// </value>
	    [Editable(true)]
        [Column("max_quantity_allowed")]
		public decimal max_quantity_allowed
        {
            get { return _max_quantity_allowed; }
            set
            {
                if (_max_quantity_allowed != value)
                {
                    PropertyChanged("max_quantity_allowed");
					_max_quantity_allowed = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_packing_change.
        /// </summary>
        /// <value>
        /// The last_packing_change.
        /// </value>
        private decimal _last_packing_change;

        /// <summary>
        /// Gets or sets the last_packing_change.
        /// </summary>
        /// <value>
        /// The last_packing_change.
        /// </value>
	    [Editable(true)]
        [Column("last_packing_change")]
		public decimal last_packing_change
        {
            get { return _last_packing_change; }
            set
            {
                if (_last_packing_change != value)
                {
                    PropertyChanged("last_packing_change");
					_last_packing_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
        private decimal _material_discount_amount;

        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_amount")]
		public decimal material_discount_amount
        {
            get { return _material_discount_amount; }
            set
            {
                if (_material_discount_amount != value)
                {
                    PropertyChanged("material_discount_amount");
					_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
		public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
        private string _bonus_row;

        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("bonus_row")]
		public string bonus_row
        {
            get { return _bonus_row; }
            set
            {
                if (_bonus_row != value)
                {
                    PropertyChanged("bonus_row");
					_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
        private long _pay_shape;

        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("pay_shape")]
		public long pay_shape
        {
            get { return _pay_shape; }
            set
            {
                if (_pay_shape != value)
                {
                    PropertyChanged("pay_shape");
					_pay_shape = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        private long _indicator;

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
	    [Editable(true)]
        [Column("indicator")]
		public long indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    PropertyChanged("indicator");
					_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
        private decimal _bonus_discount;

        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("bonus_discount")]
		public decimal bonus_discount
        {
            get { return _bonus_discount; }
            set
            {
                if (_bonus_discount != value)
                {
                    PropertyChanged("bonus_discount");
					_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
        private decimal _supplier_discount_percent;

        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("supplier_discount_percent")]
		public decimal supplier_discount_percent
        {
            get { return _supplier_discount_percent; }
            set
            {
                if (_supplier_discount_percent != value)
                {
                    PropertyChanged("supplier_discount_percent");
					_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
        private decimal _current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("current_catalog_sell_price")]
		public decimal current_catalog_sell_price
        {
            get { return _current_catalog_sell_price; }
            set
            {
                if (_current_catalog_sell_price != value)
                {
                    PropertyChanged("current_catalog_sell_price");
					_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
        private DateTime? _expiration_date;

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("expiration_date")]
		public DateTime? expiration_date
        {
            get { return _expiration_date; }
            set
            {
                if (_expiration_date != value)
                {
                    PropertyChanged("expiration_date");
					_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
        private long _mini_terminal;

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("mini_terminal")]
		public long mini_terminal
        {
            get { return _mini_terminal; }
            set
            {
                if (_mini_terminal != value)
                {
                    PropertyChanged("mini_terminal");
					_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
		public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
		public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the min_valid_month.
        /// </summary>
        /// <value>
        /// The min_valid_month.
        /// </value>
        private long _min_valid_month;

        /// <summary>
        /// Gets or sets the min_valid_month.
        /// </summary>
        /// <value>
        /// The min_valid_month.
        /// </value>
	    [Editable(true)]
        [Column("min_valid_month")]
		public long min_valid_month
        {
            get { return _min_valid_month; }
            set
            {
                if (_min_valid_month != value)
                {
                    PropertyChanged("min_valid_month");
					_min_valid_month = value;
                    
                }
            }
        }
	}
}
