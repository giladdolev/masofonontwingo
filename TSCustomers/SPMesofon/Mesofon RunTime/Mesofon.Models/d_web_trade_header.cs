using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_web_trade_header : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_web_trade_header()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="statusname">The statusname.</param>
		/// <param name="statuscode">The statuscode.</param>
		/// <param name="statusdatetime">The statusdatetime.</param>
		/// <param name="orderorgno">The orderorgno.</param>
		/// <param name="orderdate">The orderdate.</param>
        public d_web_trade_header(string statusname, string statuscode, long statusdatetime, string orderorgno, double orderdate)
        {
			this._statusname = statusname;
			this._statuscode = statuscode;
			this._statusdatetime = statusdatetime;
			this._orderorgno = orderorgno;
			this._orderdate = orderdate;
		}

	
        /// <summary>
        /// Gets or sets the statusname.
        /// </summary>
        /// <value>
        /// The statusname.
        /// </value>
        private string _statusname;

        /// <summary>
        /// Gets or sets the statusname.
        /// </summary>
        /// <value>
        /// The statusname.
        /// </value>
	    [Editable(true)]
        [Column("statusname")]
		public string statusname
        {
            get { return _statusname; }
            set
            {
                if (_statusname != value)
                {
                    PropertyChanged("statusname");
					_statusname = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the statuscode.
        /// </summary>
        /// <value>
        /// The statuscode.
        /// </value>
        private string _statuscode;

        /// <summary>
        /// Gets or sets the statuscode.
        /// </summary>
        /// <value>
        /// The statuscode.
        /// </value>
	    [Editable(true)]
        [Column("statuscode")]
		public string statuscode
        {
            get { return _statuscode; }
            set
            {
                if (_statuscode != value)
                {
                    PropertyChanged("statuscode");
					_statuscode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the statusdatetime.
        /// </summary>
        /// <value>
        /// The statusdatetime.
        /// </value>
        private long _statusdatetime;

        /// <summary>
        /// Gets or sets the statusdatetime.
        /// </summary>
        /// <value>
        /// The statusdatetime.
        /// </value>
	    [Editable(true)]
        [Column("statusdatetime")]
		public long statusdatetime
        {
            get { return _statusdatetime; }
            set
            {
                if (_statusdatetime != value)
                {
                    PropertyChanged("statusdatetime");
					_statusdatetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the orderorgno.
        /// </summary>
        /// <value>
        /// The orderorgno.
        /// </value>
        private string _orderorgno;

        /// <summary>
        /// Gets or sets the orderorgno.
        /// </summary>
        /// <value>
        /// The orderorgno.
        /// </value>
	    [Editable(true)]
        [Column("orderorgno")]
		public string orderorgno
        {
            get { return _orderorgno; }
            set
            {
                if (_orderorgno != value)
                {
                    PropertyChanged("orderorgno");
					_orderorgno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the orderdate.
        /// </summary>
        /// <value>
        /// The orderdate.
        /// </value>
        private double _orderdate;

        /// <summary>
        /// Gets or sets the orderdate.
        /// </summary>
        /// <value>
        /// The orderdate.
        /// </value>
	    [Editable(true)]
        [Column("orderdate")]
		public double orderdate
        {
            get { return _orderdate; }
            set
            {
                if (_orderdate != value)
                {
                    PropertyChanged("orderdate");
					_orderdate = value;
                    
                }
            }
        }
	}
}
