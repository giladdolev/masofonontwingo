using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_inv_pack_summery : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_inv_pack_summery()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_details_serial_number">The invoice_details_serial_number.</param>
		/// <param name="invoice_details_branch_number">The invoice_details_branch_number.</param>
		/// <param name="invoice_move_invoice_number">The invoice_move_invoice_number.</param>
		/// <param name="invoice_move_branch_number">The invoice_move_branch_number.</param>
		/// <param name="invoice_move_supplier_number">The invoice_move_supplier_number.</param>
		/// <param name="invoice_move_invoice_type">The invoice_move_invoice_type.</param>
		/// <param name="invoice_move_order_number">The invoice_move_order_number.</param>
		/// <param name="invoice_details_material_quantity">The invoice_details_material_quantity.</param>
        public d_inv_pack_summery(long invoice_details_serial_number, long invoice_details_branch_number, long invoice_move_invoice_number, long invoice_move_branch_number, long invoice_move_supplier_number, string invoice_move_invoice_type, long invoice_move_order_number, decimal invoice_details_material_quantity)
        {
			this._invoice_details_serial_number = invoice_details_serial_number;
			this._invoice_details_branch_number = invoice_details_branch_number;
			this._invoice_move_invoice_number = invoice_move_invoice_number;
			this._invoice_move_branch_number = invoice_move_branch_number;
			this._invoice_move_supplier_number = invoice_move_supplier_number;
			this._invoice_move_invoice_type = invoice_move_invoice_type;
			this._invoice_move_order_number = invoice_move_order_number;
			this._invoice_details_material_quantity = invoice_details_material_quantity;
		}

	
        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
        private long _invoice_details_serial_number;

        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_serial_number")]
		public long invoice_details_serial_number
        {
            get { return _invoice_details_serial_number; }
            set
            {
                if (_invoice_details_serial_number != value)
                {
                    PropertyChanged("invoice_details_serial_number");
					_invoice_details_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
        private long _invoice_details_branch_number;

        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_branch_number")]
		public long invoice_details_branch_number
        {
            get { return _invoice_details_branch_number; }
            set
            {
                if (_invoice_details_branch_number != value)
                {
                    PropertyChanged("invoice_details_branch_number");
					_invoice_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_number.
        /// </value>
        private long _invoice_move_invoice_number;

        /// <summary>
        /// Gets or sets the invoice_move_invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_invoice_number")]
		public long invoice_move_invoice_number
        {
            get { return _invoice_move_invoice_number; }
            set
            {
                if (_invoice_move_invoice_number != value)
                {
                    PropertyChanged("invoice_move_invoice_number");
					_invoice_move_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_move_branch_number.
        /// </value>
        private long _invoice_move_branch_number;

        /// <summary>
        /// Gets or sets the invoice_move_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_move_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_branch_number")]
		public long invoice_move_branch_number
        {
            get { return _invoice_move_branch_number; }
            set
            {
                if (_invoice_move_branch_number != value)
                {
                    PropertyChanged("invoice_move_branch_number");
					_invoice_move_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_move_supplier_number.
        /// </value>
        private long _invoice_move_supplier_number;

        /// <summary>
        /// Gets or sets the invoice_move_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_move_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_supplier_number")]
		public long invoice_move_supplier_number
        {
            get { return _invoice_move_supplier_number; }
            set
            {
                if (_invoice_move_supplier_number != value)
                {
                    PropertyChanged("invoice_move_supplier_number");
					_invoice_move_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_type.
        /// </value>
        private string _invoice_move_invoice_type;

        /// <summary>
        /// Gets or sets the invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_type.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_invoice_type")]
		public string invoice_move_invoice_type
        {
            get { return _invoice_move_invoice_type; }
            set
            {
                if (_invoice_move_invoice_type != value)
                {
                    PropertyChanged("invoice_move_invoice_type");
					_invoice_move_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_order_number.
        /// </summary>
        /// <value>
        /// The invoice_move_order_number.
        /// </value>
        private long _invoice_move_order_number;

        /// <summary>
        /// Gets or sets the invoice_move_order_number.
        /// </summary>
        /// <value>
        /// The invoice_move_order_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_order_number")]
		public long invoice_move_order_number
        {
            get { return _invoice_move_order_number; }
            set
            {
                if (_invoice_move_order_number != value)
                {
                    PropertyChanged("invoice_move_order_number");
					_invoice_move_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_quantity.
        /// </summary>
        /// <value>
        /// The invoice_details_material_quantity.
        /// </value>
        private decimal _invoice_details_material_quantity;

        /// <summary>
        /// Gets or sets the invoice_details_material_quantity.
        /// </summary>
        /// <value>
        /// The invoice_details_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_quantity")]
		public decimal invoice_details_material_quantity
        {
            get { return _invoice_details_material_quantity; }
            set
            {
                if (_invoice_details_material_quantity != value)
                {
                    PropertyChanged("invoice_details_material_quantity");
					_invoice_details_material_quantity = value;
                    
                }
            }
        }
	}
}
