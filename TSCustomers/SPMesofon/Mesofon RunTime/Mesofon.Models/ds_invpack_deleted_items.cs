using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class ds_invpack_deleted_items : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public ds_invpack_deleted_items()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="station_number">The station_number.</param>
		/// <param name="delete_point">The delete_point.</param>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="update_datetime">The update_datetime.</param>
		/// <param name="del_emp_number">The del_emp_number.</param>
		/// <param name="delete_option">The delete_option.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="doc_name">The doc_name.</param>
		/// <param name="doc_type">The doc_type.</param>
		/// <param name="doc_number">The doc_number.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="move_datetime">The move_datetime.</param>
		/// <param name="supplier_datetime">The supplier_datetime.</param>
		/// <param name="state">The state.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="doc_total_amount">The doc_total_amount.</param>
		/// <param name="discount_percent">The discount_percent.</param>
		/// <param name="item_state">The item_state.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="indicator">The indicator.</param>
		/// <param name="packing_list_number">The packing_list_number.</param>
		/// <param name="packing_serial">The packing_serial.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="color">The color.</param>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="mini_terminal">The mini_terminal.</param>
		/// <param name="expiration_date">The expiration_date.</param>
		/// <param name="out_of_stock_datetime">The out_of_stock_datetime.</param>
		/// <param name="bonus_row">The bonus_row.</param>
		/// <param name="material_bonus_connect">The material_bonus_connect.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="supplier_discount_percent">The supplier_discount_percent.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="material_discount_amount">The material_discount_amount.</param>
		/// <param name="bonus_discount">The bonus_discount.</param>
		/// <param name="material_price_after_disc">The material_price_after_disc.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="current_catalog_sell_price">The current_catalog_sell_price.</param>
		/// <param name="details">The details.</param>
		/// <param name="batch_number">The batch_number.</param>
		/// <param name="recieved_date">The recieved_date.</param>
		/// <param name="batch_expiry_date">The batch_expiry_date.</param>
		/// <param name="quantity_in">The quantity_in.</param>
		/// <param name="quantity_out">The quantity_out.</param>
        public ds_invpack_deleted_items(long branch_number, long station_number, long delete_point, long serial_number, DateTime? update_datetime, long del_emp_number, long delete_option, long supplier_number, long order_number, string doc_name, string doc_type, long doc_number, long employee_number, DateTime? move_datetime, DateTime? supplier_datetime, string state, long b2b_status, decimal doc_total_amount, decimal discount_percent, string item_state, DateTime? last_update_datetime, long indicator, long packing_list_number, long packing_serial, string import_type, long color, long decline_number, long mini_terminal, DateTime? expiration_date, DateTime? out_of_stock_datetime, string bonus_row, long material_bonus_connect, decimal quantity_within_invoice, decimal expected_material_quantity, decimal material_quantity, decimal bonus_quantity, decimal material_price, decimal supplier_discount_percent, decimal material_discount_percent, decimal material_discount_amount, decimal bonus_discount, decimal material_price_after_disc, decimal sell_price, decimal current_catalog_sell_price, string details, string batch_number, DateTime? recieved_date, DateTime? batch_expiry_date, decimal quantity_in, decimal quantity_out)
        {
			this._branch_number = branch_number;
			this._station_number = station_number;
			this._delete_point = delete_point;
			this._serial_number = serial_number;
			this._update_datetime = update_datetime;
			this._del_emp_number = del_emp_number;
			this._delete_option = delete_option;
			this._supplier_number = supplier_number;
			this._order_number = order_number;
			this._doc_name = doc_name;
			this._doc_type = doc_type;
			this._doc_number = doc_number;
			this._employee_number = employee_number;
			this._move_datetime = move_datetime;
			this._supplier_datetime = supplier_datetime;
			this._state = state;
			this._b2b_status = b2b_status;
			this._doc_total_amount = doc_total_amount;
			this._discount_percent = discount_percent;
			this._item_state = item_state;
			this._last_update_datetime = last_update_datetime;
			this._indicator = indicator;
			this._packing_list_number = packing_list_number;
			this._packing_serial = packing_serial;
			this._import_type = import_type;
			this._color = color;
			this._decline_number = decline_number;
			this._mini_terminal = mini_terminal;
			this._expiration_date = expiration_date;
			this._out_of_stock_datetime = out_of_stock_datetime;
			this._bonus_row = bonus_row;
			this._material_bonus_connect = material_bonus_connect;
			this._quantity_within_invoice = quantity_within_invoice;
			this._expected_material_quantity = expected_material_quantity;
			this._material_quantity = material_quantity;
			this._bonus_quantity = bonus_quantity;
			this._material_price = material_price;
			this._supplier_discount_percent = supplier_discount_percent;
			this._material_discount_percent = material_discount_percent;
			this._material_discount_amount = material_discount_amount;
			this._bonus_discount = bonus_discount;
			this._material_price_after_disc = material_price_after_disc;
			this._sell_price = sell_price;
			this._current_catalog_sell_price = current_catalog_sell_price;
			this._details = details;
			this._batch_number = batch_number;
			this._recieved_date = recieved_date;
			this._batch_expiry_date = batch_expiry_date;
			this._quantity_in = quantity_in;
			this._quantity_out = quantity_out;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the station_number.
        /// </summary>
        /// <value>
        /// The station_number.
        /// </value>
        private long _station_number;

        /// <summary>
        /// Gets or sets the station_number.
        /// </summary>
        /// <value>
        /// The station_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("station_number")]
		public long station_number
        {
            get { return _station_number; }
            set
            {
                if (_station_number != value)
                {
                    PropertyChanged("station_number");
					_station_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the delete_point.
        /// </summary>
        /// <value>
        /// The delete_point.
        /// </value>
        private long _delete_point;

        /// <summary>
        /// Gets or sets the delete_point.
        /// </summary>
        /// <value>
        /// The delete_point.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("delete_point")]
		public long delete_point
        {
            get { return _delete_point; }
            set
            {
                if (_delete_point != value)
                {
                    PropertyChanged("delete_point");
					_delete_point = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the update_datetime.
        /// </summary>
        /// <value>
        /// The update_datetime.
        /// </value>
        private DateTime? _update_datetime;

        /// <summary>
        /// Gets or sets the update_datetime.
        /// </summary>
        /// <value>
        /// The update_datetime.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("update_datetime")]
		public DateTime? update_datetime
        {
            get { return _update_datetime; }
            set
            {
                if (_update_datetime != value)
                {
                    PropertyChanged("update_datetime");
					_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the del_emp_number.
        /// </summary>
        /// <value>
        /// The del_emp_number.
        /// </value>
        private long _del_emp_number;

        /// <summary>
        /// Gets or sets the del_emp_number.
        /// </summary>
        /// <value>
        /// The del_emp_number.
        /// </value>
	    [Editable(true)]
        [Column("del_emp_number")]
		public long del_emp_number
        {
            get { return _del_emp_number; }
            set
            {
                if (_del_emp_number != value)
                {
                    PropertyChanged("del_emp_number");
					_del_emp_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the delete_option.
        /// </summary>
        /// <value>
        /// The delete_option.
        /// </value>
        private long _delete_option;

        /// <summary>
        /// Gets or sets the delete_option.
        /// </summary>
        /// <value>
        /// The delete_option.
        /// </value>
	    [Editable(true)]
        [Column("delete_option")]
		public long delete_option
        {
            get { return _delete_option; }
            set
            {
                if (_delete_option != value)
                {
                    PropertyChanged("delete_option");
					_delete_option = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_name.
        /// </summary>
        /// <value>
        /// The doc_name.
        /// </value>
        private string _doc_name;

        /// <summary>
        /// Gets or sets the doc_name.
        /// </summary>
        /// <value>
        /// The doc_name.
        /// </value>
	    [Editable(true)]
        [Column("doc_name")]
		public string doc_name
        {
            get { return _doc_name; }
            set
            {
                if (_doc_name != value)
                {
                    PropertyChanged("doc_name");
					_doc_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
	    [Editable(true)]
        [Column("doc_type")]
		public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
					_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
        private long _doc_number;

        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
	    [Editable(true)]
        [Column("doc_number")]
		public long doc_number
        {
            get { return _doc_number; }
            set
            {
                if (_doc_number != value)
                {
                    PropertyChanged("doc_number");
					_doc_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the move_datetime.
        /// </summary>
        /// <value>
        /// The move_datetime.
        /// </value>
        private DateTime? _move_datetime;

        /// <summary>
        /// Gets or sets the move_datetime.
        /// </summary>
        /// <value>
        /// The move_datetime.
        /// </value>
	    [Editable(true)]
        [Column("move_datetime")]
		public DateTime? move_datetime
        {
            get { return _move_datetime; }
            set
            {
                if (_move_datetime != value)
                {
                    PropertyChanged("move_datetime");
					_move_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_datetime.
        /// </summary>
        /// <value>
        /// The supplier_datetime.
        /// </value>
        private DateTime? _supplier_datetime;

        /// <summary>
        /// Gets or sets the supplier_datetime.
        /// </summary>
        /// <value>
        /// The supplier_datetime.
        /// </value>
	    [Editable(true)]
        [Column("supplier_datetime")]
		public DateTime? supplier_datetime
        {
            get { return _supplier_datetime; }
            set
            {
                if (_supplier_datetime != value)
                {
                    PropertyChanged("supplier_datetime");
					_supplier_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_total_amount.
        /// </summary>
        /// <value>
        /// The doc_total_amount.
        /// </value>
        private decimal _doc_total_amount;

        /// <summary>
        /// Gets or sets the doc_total_amount.
        /// </summary>
        /// <value>
        /// The doc_total_amount.
        /// </value>
	    [Editable(true)]
        [Column("doc_total_amount")]
		public decimal doc_total_amount
        {
            get { return _doc_total_amount; }
            set
            {
                if (_doc_total_amount != value)
                {
                    PropertyChanged("doc_total_amount");
					_doc_total_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
        private decimal _discount_percent;

        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("discount_percent")]
		public decimal discount_percent
        {
            get { return _discount_percent; }
            set
            {
                if (_discount_percent != value)
                {
                    PropertyChanged("discount_percent");
					_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the item_state.
        /// </summary>
        /// <value>
        /// The item_state.
        /// </value>
        private string _item_state;

        /// <summary>
        /// Gets or sets the item_state.
        /// </summary>
        /// <value>
        /// The item_state.
        /// </value>
	    [Editable(true)]
        [Column("item_state")]
		public string item_state
        {
            get { return _item_state; }
            set
            {
                if (_item_state != value)
                {
                    PropertyChanged("item_state");
					_item_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        private long _indicator;

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
	    [Editable(true)]
        [Column("indicator")]
		public long indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    PropertyChanged("indicator");
					_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
        private long _packing_list_number;

        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_number")]
		public long packing_list_number
        {
            get { return _packing_list_number; }
            set
            {
                if (_packing_list_number != value)
                {
                    PropertyChanged("packing_list_number");
					_packing_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
        private long _packing_serial;

        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
	    [Editable(true)]
        [Column("packing_serial")]
		public long packing_serial
        {
            get { return _packing_serial; }
            set
            {
                if (_packing_serial != value)
                {
                    PropertyChanged("packing_serial");
					_packing_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
		public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
	    [Editable(true)]
        [Column("decline_number")]
		public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
        private long _mini_terminal;

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("mini_terminal")]
		public long mini_terminal
        {
            get { return _mini_terminal; }
            set
            {
                if (_mini_terminal != value)
                {
                    PropertyChanged("mini_terminal");
					_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
        private DateTime? _expiration_date;

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("expiration_date")]
		public DateTime? expiration_date
        {
            get { return _expiration_date; }
            set
            {
                if (_expiration_date != value)
                {
                    PropertyChanged("expiration_date");
					_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the out_of_stock_datetime.
        /// </summary>
        /// <value>
        /// The out_of_stock_datetime.
        /// </value>
        private DateTime? _out_of_stock_datetime;

        /// <summary>
        /// Gets or sets the out_of_stock_datetime.
        /// </summary>
        /// <value>
        /// The out_of_stock_datetime.
        /// </value>
	    [Editable(true)]
        [Column("out_of_stock_datetime")]
		public DateTime? out_of_stock_datetime
        {
            get { return _out_of_stock_datetime; }
            set
            {
                if (_out_of_stock_datetime != value)
                {
                    PropertyChanged("out_of_stock_datetime");
					_out_of_stock_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
        private string _bonus_row;

        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("bonus_row")]
		public string bonus_row
        {
            get { return _bonus_row; }
            set
            {
                if (_bonus_row != value)
                {
                    PropertyChanged("bonus_row");
					_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
		public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
		public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
		public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
        private decimal _supplier_discount_percent;

        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("supplier_discount_percent")]
		public decimal supplier_discount_percent
        {
            get { return _supplier_discount_percent; }
            set
            {
                if (_supplier_discount_percent != value)
                {
                    PropertyChanged("supplier_discount_percent");
					_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
		public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
        private decimal _material_discount_amount;

        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_amount")]
		public decimal material_discount_amount
        {
            get { return _material_discount_amount; }
            set
            {
                if (_material_discount_amount != value)
                {
                    PropertyChanged("material_discount_amount");
					_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
        private decimal _bonus_discount;

        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("bonus_discount")]
		public decimal bonus_discount
        {
            get { return _bonus_discount; }
            set
            {
                if (_bonus_discount != value)
                {
                    PropertyChanged("bonus_discount");
					_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price_after_disc.
        /// </summary>
        /// <value>
        /// The material_price_after_disc.
        /// </value>
        private decimal _material_price_after_disc;

        /// <summary>
        /// Gets or sets the material_price_after_disc.
        /// </summary>
        /// <value>
        /// The material_price_after_disc.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_disc")]
		public decimal material_price_after_disc
        {
            get { return _material_price_after_disc; }
            set
            {
                if (_material_price_after_disc != value)
                {
                    PropertyChanged("material_price_after_disc");
					_material_price_after_disc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
        private decimal _current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("current_catalog_sell_price")]
		public decimal current_catalog_sell_price
        {
            get { return _current_catalog_sell_price; }
            set
            {
                if (_current_catalog_sell_price != value)
                {
                    PropertyChanged("current_catalog_sell_price");
					_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
		public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the batch_number.
        /// </summary>
        /// <value>
        /// The batch_number.
        /// </value>
        private string _batch_number;

        /// <summary>
        /// Gets or sets the batch_number.
        /// </summary>
        /// <value>
        /// The batch_number.
        /// </value>
	    [Editable(true)]
        [Column("batch_number")]
		public string batch_number
        {
            get { return _batch_number; }
            set
            {
                if (_batch_number != value)
                {
                    PropertyChanged("batch_number");
					_batch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the recieved_date.
        /// </summary>
        /// <value>
        /// The recieved_date.
        /// </value>
        private DateTime? _recieved_date;

        /// <summary>
        /// Gets or sets the recieved_date.
        /// </summary>
        /// <value>
        /// The recieved_date.
        /// </value>
	    [Editable(true)]
        [Column("recieved_date")]
		public DateTime? recieved_date
        {
            get { return _recieved_date; }
            set
            {
                if (_recieved_date != value)
                {
                    PropertyChanged("recieved_date");
					_recieved_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the batch_expiry_date.
        /// </summary>
        /// <value>
        /// The batch_expiry_date.
        /// </value>
        private DateTime? _batch_expiry_date;

        /// <summary>
        /// Gets or sets the batch_expiry_date.
        /// </summary>
        /// <value>
        /// The batch_expiry_date.
        /// </value>
	    [Editable(true)]
        [Column("batch_expiry_date")]
		public DateTime? batch_expiry_date
        {
            get { return _batch_expiry_date; }
            set
            {
                if (_batch_expiry_date != value)
                {
                    PropertyChanged("batch_expiry_date");
					_batch_expiry_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_in.
        /// </summary>
        /// <value>
        /// The quantity_in.
        /// </value>
        private decimal _quantity_in;

        /// <summary>
        /// Gets or sets the quantity_in.
        /// </summary>
        /// <value>
        /// The quantity_in.
        /// </value>
	    [Editable(true)]
        [Column("quantity_in")]
		public decimal quantity_in
        {
            get { return _quantity_in; }
            set
            {
                if (_quantity_in != value)
                {
                    PropertyChanged("quantity_in");
					_quantity_in = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_out.
        /// </summary>
        /// <value>
        /// The quantity_out.
        /// </value>
        private decimal _quantity_out;

        /// <summary>
        /// Gets or sets the quantity_out.
        /// </summary>
        /// <value>
        /// The quantity_out.
        /// </value>
	    [Editable(true)]
        [Column("quantity_out")]
		public decimal quantity_out
        {
            get { return _quantity_out; }
            set
            {
                if (_quantity_out != value)
                {
                    PropertyChanged("quantity_out");
					_quantity_out = value;
                    
                }
            }
        }
	}
}
