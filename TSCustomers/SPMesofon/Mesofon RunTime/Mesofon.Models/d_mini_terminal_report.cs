using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_report : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_report()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_name">The materials_name.</param>
		/// <param name="materials_min_valid_months">The materials_min_valid_months.</param>
		/// <param name="invoice_details_expiration_date">The invoice_details_expiration_date.</param>
		/// <param name="ok_valid_months">The ok_valid_months.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="mt_quantity">The mt_quantity.</param>
		/// <param name="doc_no">The doc_no.</param>
		/// <param name="doc_type">The doc_type.</param>
		/// <param name="diff">The diff.</param>
		/// <param name="date">The date.</param>
		/// <param name="row">The row.</param>
		/// <param name="compute">The compute.</param>
        public d_mini_terminal_report(long branch_number, string branch_name, long supplier_number, string supplier_name, long order_number, string employee_name, string materials_barcode, string materials_name, DateTime? materials_min_valid_months, DateTime? invoice_details_expiration_date, string ok_valid_months, decimal order_quantity, decimal mt_quantity, long doc_no, string doc_type, decimal diff, string date, string row, string compute)
        {
			this._branch_number = branch_number;
			this._branch_name = branch_name;
			this._supplier_number = supplier_number;
			this._supplier_name = supplier_name;
			this._order_number = order_number;
			this._employee_name = employee_name;
			this._materials_barcode = materials_barcode;
			this._materials_name = materials_name;
			this._materials_min_valid_months = materials_min_valid_months;
			this._invoice_details_expiration_date = invoice_details_expiration_date;
			this._ok_valid_months = ok_valid_months;
			this._order_quantity = order_quantity;
			this._mt_quantity = mt_quantity;
			this._doc_no = doc_no;
			this._doc_type = doc_type;
			this._diff = diff;
			this._date = date;
			this._row = row;
			this._compute = compute;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
		public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
		public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
		public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
        private string _materials_name;

        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_name")]
		public string materials_name
        {
            get { return _materials_name; }
            set
            {
                if (_materials_name != value)
                {
                    PropertyChanged("materials_name");
					_materials_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
        private DateTime? _materials_min_valid_months;

        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("materials_min_valid_months")]
		public DateTime? materials_min_valid_months
        {
            get { return _materials_min_valid_months; }
            set
            {
                if (_materials_min_valid_months != value)
                {
                    PropertyChanged("materials_min_valid_months");
					_materials_min_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
        private DateTime? _invoice_details_expiration_date;

        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_expiration_date")]
		public DateTime? invoice_details_expiration_date
        {
            get { return _invoice_details_expiration_date; }
            set
            {
                if (_invoice_details_expiration_date != value)
                {
                    PropertyChanged("invoice_details_expiration_date");
					_invoice_details_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
        private string _ok_valid_months;

        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("ok_valid_months")]
		public string ok_valid_months
        {
            get { return _ok_valid_months; }
            set
            {
                if (_ok_valid_months != value)
                {
                    PropertyChanged("ok_valid_months");
					_ok_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
		public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mt_quantity.
        /// </summary>
        /// <value>
        /// The mt_quantity.
        /// </value>
        private decimal _mt_quantity;

        /// <summary>
        /// Gets or sets the mt_quantity.
        /// </summary>
        /// <value>
        /// The mt_quantity.
        /// </value>
	    [Editable(true)]
        [Column("mt_quantity")]
		public decimal mt_quantity
        {
            get { return _mt_quantity; }
            set
            {
                if (_mt_quantity != value)
                {
                    PropertyChanged("mt_quantity");
					_mt_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
        private long _doc_no;

        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
	    [Editable(true)]
        [Column("doc_no")]
		public long doc_no
        {
            get { return _doc_no; }
            set
            {
                if (_doc_no != value)
                {
                    PropertyChanged("doc_no");
					_doc_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
	    [Editable(true)]
        [Column("doc_type")]
		public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
					_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the diff.
        /// </summary>
        /// <value>
        /// The diff.
        /// </value>
        private decimal _diff;

        /// <summary>
        /// Gets or sets the diff.
        /// </summary>
        /// <value>
        /// The diff.
        /// </value>
	    [Editable(true)]
        [Column("diff")]
		public decimal diff
        {
            get { return _diff; }
            set
            {
                if (_diff != value)
                {
                    PropertyChanged("diff");
					_diff = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        private string _date;

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
	    [Editable(true)]
        [Column("date")]
		public string date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    PropertyChanged("date");
					_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
		public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute.
        /// </summary>
        /// <value>
        /// The compute.
        /// </value>
        private string _compute;

        /// <summary>
        /// Gets or sets the compute.
        /// </summary>
        /// <value>
        /// The compute.
        /// </value>
	    [Editable(true)]
        [Column("compute")]
		public string compute
        {
            get { return _compute; }
            set
            {
                if (_compute != value)
                {
                    PropertyChanged("compute");
					_compute = value;
                    
                }
            }
        }
	}
}
