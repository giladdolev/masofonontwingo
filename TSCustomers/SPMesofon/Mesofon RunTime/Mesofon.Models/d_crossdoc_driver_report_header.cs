using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
    [Compute("compute_pack_quantity", "SUM shipment_pallets_pack_quantity")]
    [Compute("compute_pallets_number", "COUNT carton_barcode")]
    public class d_crossdoc_driver_report_header : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_crossdoc_driver_report_header()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="shipment_pallets_date_move">The shipment_pallets_date_move.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="carton_barcode">The carton_barcode.</param>
		/// <param name="shipment_pallets_pack_quantity">The shipment_pallets_pack_quantity.</param>
		/// <param name="shipment_pallets_arrive_datetime">The shipment_pallets_arrive_datetime.</param>
		/// <param name="shipment_pallets_arrive_time">The shipment_pallets_arrive_time.</param>
		/// <param name="global_parameters_branch_name">The global_parameters_branch_name.</param>
		/// <param name="scan_time">The scan_time.</param>
		/// <param name="print_date">The print_date.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="state">The state.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="compute_pack_quantity">The compute_pack_quantity.</param>
		/// <param name="compute_pallets_number">The compute_pallets_number.</param>
        public d_crossdoc_driver_report_header(DateTime shipment_pallets_date_move, long branch_number, double shipment_number, string carton_barcode, long shipment_pallets_pack_quantity, string shipment_pallets_arrive_datetime, string shipment_pallets_arrive_time, string global_parameters_branch_name, string scan_time, string print_date, string employee_name, string state, long supplier_number, string supplier_name, string compute_pack_quantity, string compute_pallets_number)
        {
			this._shipment_pallets_date_move = shipment_pallets_date_move;
			this._branch_number = branch_number;
			this._shipment_number = shipment_number;
			this._carton_barcode = carton_barcode;
			this._shipment_pallets_pack_quantity = shipment_pallets_pack_quantity;
			this._shipment_pallets_arrive_datetime = shipment_pallets_arrive_datetime;
			this._shipment_pallets_arrive_time = shipment_pallets_arrive_time;
			this._global_parameters_branch_name = global_parameters_branch_name;
			this._scan_time = scan_time;
			this._print_date = print_date;
			this._employee_name = employee_name;
			this._state = state;
			this._supplier_number = supplier_number;
			this._supplier_name = supplier_name;
			this._compute_pack_quantity = compute_pack_quantity;
			this._compute_pallets_number = compute_pallets_number;
		}

	
        /// <summary>
        /// Gets or sets the shipment_pallets_date_move.
        /// </summary>
        /// <value>
        /// The shipment_pallets_date_move.
        /// </value>
        private DateTime? _shipment_pallets_date_move;

        /// <summary>
        /// Gets or sets the shipment_pallets_date_move.
        /// </summary>
        /// <value>
        /// The shipment_pallets_date_move.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_date_move")]
        [Properties("shipment_pallets_date_move", "shipment_pallets_date_move",false,0,typeof(DateTime))]
        public DateTime? shipment_pallets_date_move
        {
            get { return _shipment_pallets_date_move; }
            set
            {
                if (_shipment_pallets_date_move != value)
                {
                    PropertyChanged("shipment_pallets_date_move");
					_shipment_pallets_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
        [Properties("branch_number", "branch_number", true, 1, typeof(long))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_number")]
        [Properties("shipment_number", "shipment_number", true, 2, typeof(double))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
        private string _carton_barcode;

        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
	    [Editable(true)]
        [Column("carton_barcode")]
        [Properties("carton_barcode", "carton_barcode", true, 3, typeof(string))]
        public string carton_barcode
        {
            get { return _carton_barcode; }
            set
            {
                if (_carton_barcode != value)
                {
                    PropertyChanged("carton_barcode");
					_carton_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_pack_quantity.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pack_quantity.
        /// </value>
        private long _shipment_pallets_pack_quantity;

        /// <summary>
        /// Gets or sets the shipment_pallets_pack_quantity.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_pack_quantity")]
        [Properties("shipment_pallets_pack_quantity", "shipment_pallets_pack_quantity", true, 4, typeof(long))]
        public long shipment_pallets_pack_quantity
        {
            get { return _shipment_pallets_pack_quantity; }
            set
            {
                if (_shipment_pallets_pack_quantity != value)
                {
                    PropertyChanged("shipment_pallets_pack_quantity");
					_shipment_pallets_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_datetime.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_datetime.
        /// </value>
        private string _shipment_pallets_arrive_datetime;

        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_datetime.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_datetime.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_arrive_datetime")]
        [Properties("shipment_pallets_arrive_datetime", "shipment_pallets_arrive_datetime", true, 5, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string shipment_pallets_arrive_datetime
        {
            get { return _shipment_pallets_arrive_datetime; }
            set
            {
                if (_shipment_pallets_arrive_datetime != value)
                {
                    PropertyChanged("shipment_pallets_arrive_datetime");
					_shipment_pallets_arrive_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_time.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_time.
        /// </value>
        private string _shipment_pallets_arrive_time;

        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_time.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_time.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_arrive_time")]
        [Properties("shipment_pallets_arrive_time", "shipment_pallets_arrive_time", true, 6, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string shipment_pallets_arrive_time
        {
            get { return _shipment_pallets_arrive_time; }
            set
            {
                if (_shipment_pallets_arrive_time != value)
                {
                    PropertyChanged("shipment_pallets_arrive_time");
					_shipment_pallets_arrive_time = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the global_parameters_branch_name.
        /// </summary>
        /// <value>
        /// The global_parameters_branch_name.
        /// </value>
        private string _global_parameters_branch_name;

        /// <summary>
        /// Gets or sets the global_parameters_branch_name.
        /// </summary>
        /// <value>
        /// The global_parameters_branch_name.
        /// </value>
	    [Editable(true)]
        [Column("global_parameters_branch_name")]
        [Properties("global_parameters_branch_name", "global_parameters_branch_name", true, 7, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string global_parameters_branch_name
        {
            get { return _global_parameters_branch_name; }
            set
            {
                if (_global_parameters_branch_name != value)
                {
                    PropertyChanged("global_parameters_branch_name");
					_global_parameters_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the scan_time.
        /// </summary>
        /// <value>
        /// The scan_time.
        /// </value>
        private string _scan_time;

        /// <summary>
        /// Gets or sets the scan_time.
        /// </summary>
        /// <value>
        /// The scan_time.
        /// </value>
	    [Editable(true)]
        [Column("scan_time")]
        [Properties("scan_time", "scan_time", true, 8, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string scan_time
        {
            get { return _scan_time; }
            set
            {
                if (_scan_time != value)
                {
                    PropertyChanged("scan_time");
					_scan_time = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the print_date.
        /// </summary>
        /// <value>
        /// The print_date.
        /// </value>
        private string _print_date;

        /// <summary>
        /// Gets or sets the print_date.
        /// </summary>
        /// <value>
        /// The print_date.
        /// </value>
	    [Editable(true)]
        [Column("print_date")]
        [Properties("print_date", "print_date", true, 9, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string print_date
        {
            get { return _print_date; }
            set
            {
                if (_print_date != value)
                {
                    PropertyChanged("print_date");
					_print_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
        [Properties("employee_name", "employee_name", true, 10, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
        [Properties("state", "state", true, 11, typeof(string))]
        public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [Properties("supplier_number", "supplier_number", true, 12, typeof(long))]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
        [Properties("supplier_name", "supplier_name", true, 13, typeof(string))]
        public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_pack_quantity.
        /// </summary>
        /// <value>
        /// The compute_pack_quantity.
        /// </value>
        private object _compute_pack_quantity;

        /// <summary>
        /// Gets or sets the compute_pack_quantity.
        /// </summary>
        /// <value>
        /// The compute_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("compute_pack_quantity")]
        [Properties("compute_pack_quantity", "compute_pack_quantity", true, 14, typeof(object))]
        [XMLOutput(XMLOutputLocation.Summary)]
        public object compute_pack_quantity
        {
            get { return _compute_pack_quantity; }
            set
            {
                if (_compute_pack_quantity != value)
                {
					_compute_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_pallets_number.
        /// </summary>
        /// <value>
        /// The compute_pallets_number.
        /// </value>
        private object _compute_pallets_number;

        /// <summary>
        /// Gets or sets the compute_pallets_number.
        /// </summary>
        /// <value>
        /// The compute_pallets_number.
        /// </value>
	    [Editable(true)]
        [Column("compute_pallets_number")]
        [Properties("compute_pallets_number", "compute_pallets_number", true, 15, typeof(object))]
        [XMLOutput(XMLOutputLocation.Summary)]
        public object compute_pallets_number
        {
            get { return _compute_pallets_number; }
            set
            {
                if (_compute_pallets_number != value)
                {
					_compute_pallets_number = value;
                    
                }
            }
        }
	}
}
