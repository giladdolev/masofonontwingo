using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class lds_global_branch_parameters : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public lds_global_branch_parameters()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="report_db_active">The report_db_active.</param>
		/// <param name="report_db_type">The report_db_type.</param>
		/// <param name="report_db_dbms">The report_db_dbms.</param>
		/// <param name="report_db_dbparm">The report_db_dbparm.</param>
		/// <param name="report_db_database">The report_db_database.</param>
		/// <param name="report_db_servername">The report_db_servername.</param>
		/// <param name="target_db_active">The target_db_active.</param>
		/// <param name="target_db_type">The target_db_type.</param>
		/// <param name="target_db_dbms">The target_db_dbms.</param>
		/// <param name="target_db_dbparm">The target_db_dbparm.</param>
		/// <param name="target_db_database">The target_db_database.</param>
		/// <param name="target_db_servername">The target_db_servername.</param>
		/// <param name="macabi_transaction_code">The macabi_transaction_code.</param>
		/// <param name="leumit_transaction_code">The leumit_transaction_code.</param>
		/// <param name="card_terminal_proxy_active">The card_terminal_proxy_active.</param>
		/// <param name="card_terminal_path_from_client">The card_terminal_path_from_client.</param>
		/// <param name="card_terminal_path_from_proxy">The card_terminal_path_from_proxy.</param>
		/// <param name="card_terminal_number">The card_terminal_number.</param>
		/// <param name="card_terminal_version">The card_terminal_version.</param>
		/// <param name="queue_db_active">The queue_db_active.</param>
		/// <param name="queue_db_type">The queue_db_type.</param>
		/// <param name="queue_db_dbms">The queue_db_dbms.</param>
		/// <param name="queue_db_dbparm">The queue_db_dbparm.</param>
		/// <param name="queue_db_database">The queue_db_database.</param>
		/// <param name="queue_db_servername">The queue_db_servername.</param>
		/// <param name="macabi_delete_after_tran">The macabi_delete_after_tran.</param>
		/// <param name="leumit_delete_after_tran">The leumit_delete_after_tran.</param>
        public lds_global_branch_parameters(double report_db_active, string report_db_type, string report_db_dbms, string report_db_dbparm, string report_db_database, string report_db_servername, double target_db_active, string target_db_type, string target_db_dbms, string target_db_dbparm, string target_db_database, string target_db_servername, long macabi_transaction_code, long leumit_transaction_code, double card_terminal_proxy_active, string card_terminal_path_from_client, string card_terminal_path_from_proxy, string card_terminal_number, string card_terminal_version, double queue_db_active, string queue_db_type, string queue_db_dbms, string queue_db_dbparm, string queue_db_database, string queue_db_servername, double macabi_delete_after_tran, double leumit_delete_after_tran)
        {
			this._report_db_active = report_db_active;
			this._report_db_type = report_db_type;
			this._report_db_dbms = report_db_dbms;
			this._report_db_dbparm = report_db_dbparm;
			this._report_db_database = report_db_database;
			this._report_db_servername = report_db_servername;
			this._target_db_active = target_db_active;
			this._target_db_type = target_db_type;
			this._target_db_dbms = target_db_dbms;
			this._target_db_dbparm = target_db_dbparm;
			this._target_db_database = target_db_database;
			this._target_db_servername = target_db_servername;
			this._macabi_transaction_code = macabi_transaction_code;
			this._leumit_transaction_code = leumit_transaction_code;
			this._card_terminal_proxy_active = card_terminal_proxy_active;
			this._card_terminal_path_from_client = card_terminal_path_from_client;
			this._card_terminal_path_from_proxy = card_terminal_path_from_proxy;
			this._card_terminal_number = card_terminal_number;
			this._card_terminal_version = card_terminal_version;
			this._queue_db_active = queue_db_active;
			this._queue_db_type = queue_db_type;
			this._queue_db_dbms = queue_db_dbms;
			this._queue_db_dbparm = queue_db_dbparm;
			this._queue_db_database = queue_db_database;
			this._queue_db_servername = queue_db_servername;
			this._macabi_delete_after_tran = macabi_delete_after_tran;
			this._leumit_delete_after_tran = leumit_delete_after_tran;
		}

	
        /// <summary>
        /// Gets or sets the report_db_active.
        /// </summary>
        /// <value>
        /// The report_db_active.
        /// </value>
        private double _report_db_active;

        /// <summary>
        /// Gets or sets the report_db_active.
        /// </summary>
        /// <value>
        /// The report_db_active.
        /// </value>
	    [Editable(true)]
        [Column("report_db_active")]
		public double report_db_active
        {
            get { return _report_db_active; }
            set
            {
                if (_report_db_active != value)
                {
                    PropertyChanged("report_db_active");
					_report_db_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_type.
        /// </summary>
        /// <value>
        /// The report_db_type.
        /// </value>
        private string _report_db_type;

        /// <summary>
        /// Gets or sets the report_db_type.
        /// </summary>
        /// <value>
        /// The report_db_type.
        /// </value>
	    [Editable(true)]
        [Column("report_db_type")]
		public string report_db_type
        {
            get { return _report_db_type; }
            set
            {
                if (_report_db_type != value)
                {
                    PropertyChanged("report_db_type");
					_report_db_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_dbms.
        /// </summary>
        /// <value>
        /// The report_db_dbms.
        /// </value>
        private string _report_db_dbms;

        /// <summary>
        /// Gets or sets the report_db_dbms.
        /// </summary>
        /// <value>
        /// The report_db_dbms.
        /// </value>
	    [Editable(true)]
        [Column("report_db_dbms")]
		public string report_db_dbms
        {
            get { return _report_db_dbms; }
            set
            {
                if (_report_db_dbms != value)
                {
                    PropertyChanged("report_db_dbms");
					_report_db_dbms = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_dbparm.
        /// </summary>
        /// <value>
        /// The report_db_dbparm.
        /// </value>
        private string _report_db_dbparm;

        /// <summary>
        /// Gets or sets the report_db_dbparm.
        /// </summary>
        /// <value>
        /// The report_db_dbparm.
        /// </value>
	    [Editable(true)]
        [Column("report_db_dbparm")]
		public string report_db_dbparm
        {
            get { return _report_db_dbparm; }
            set
            {
                if (_report_db_dbparm != value)
                {
                    PropertyChanged("report_db_dbparm");
					_report_db_dbparm = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_database.
        /// </summary>
        /// <value>
        /// The report_db_database.
        /// </value>
        private string _report_db_database;

        /// <summary>
        /// Gets or sets the report_db_database.
        /// </summary>
        /// <value>
        /// The report_db_database.
        /// </value>
	    [Editable(true)]
        [Column("report_db_database")]
		public string report_db_database
        {
            get { return _report_db_database; }
            set
            {
                if (_report_db_database != value)
                {
                    PropertyChanged("report_db_database");
					_report_db_database = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_servername.
        /// </summary>
        /// <value>
        /// The report_db_servername.
        /// </value>
        private string _report_db_servername;

        /// <summary>
        /// Gets or sets the report_db_servername.
        /// </summary>
        /// <value>
        /// The report_db_servername.
        /// </value>
	    [Editable(true)]
        [Column("report_db_servername")]
		public string report_db_servername
        {
            get { return _report_db_servername; }
            set
            {
                if (_report_db_servername != value)
                {
                    PropertyChanged("report_db_servername");
					_report_db_servername = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_active.
        /// </summary>
        /// <value>
        /// The target_db_active.
        /// </value>
        private double _target_db_active;

        /// <summary>
        /// Gets or sets the target_db_active.
        /// </summary>
        /// <value>
        /// The target_db_active.
        /// </value>
	    [Editable(true)]
        [Column("target_db_active")]
		public double target_db_active
        {
            get { return _target_db_active; }
            set
            {
                if (_target_db_active != value)
                {
                    PropertyChanged("target_db_active");
					_target_db_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_type.
        /// </summary>
        /// <value>
        /// The target_db_type.
        /// </value>
        private string _target_db_type;

        /// <summary>
        /// Gets or sets the target_db_type.
        /// </summary>
        /// <value>
        /// The target_db_type.
        /// </value>
	    [Editable(true)]
        [Column("target_db_type")]
		public string target_db_type
        {
            get { return _target_db_type; }
            set
            {
                if (_target_db_type != value)
                {
                    PropertyChanged("target_db_type");
					_target_db_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_dbms.
        /// </summary>
        /// <value>
        /// The target_db_dbms.
        /// </value>
        private string _target_db_dbms;

        /// <summary>
        /// Gets or sets the target_db_dbms.
        /// </summary>
        /// <value>
        /// The target_db_dbms.
        /// </value>
	    [Editable(true)]
        [Column("target_db_dbms")]
		public string target_db_dbms
        {
            get { return _target_db_dbms; }
            set
            {
                if (_target_db_dbms != value)
                {
                    PropertyChanged("target_db_dbms");
					_target_db_dbms = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_dbparm.
        /// </summary>
        /// <value>
        /// The target_db_dbparm.
        /// </value>
        private string _target_db_dbparm;

        /// <summary>
        /// Gets or sets the target_db_dbparm.
        /// </summary>
        /// <value>
        /// The target_db_dbparm.
        /// </value>
	    [Editable(true)]
        [Column("target_db_dbparm")]
		public string target_db_dbparm
        {
            get { return _target_db_dbparm; }
            set
            {
                if (_target_db_dbparm != value)
                {
                    PropertyChanged("target_db_dbparm");
					_target_db_dbparm = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_database.
        /// </summary>
        /// <value>
        /// The target_db_database.
        /// </value>
        private string _target_db_database;

        /// <summary>
        /// Gets or sets the target_db_database.
        /// </summary>
        /// <value>
        /// The target_db_database.
        /// </value>
	    [Editable(true)]
        [Column("target_db_database")]
		public string target_db_database
        {
            get { return _target_db_database; }
            set
            {
                if (_target_db_database != value)
                {
                    PropertyChanged("target_db_database");
					_target_db_database = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_servername.
        /// </summary>
        /// <value>
        /// The target_db_servername.
        /// </value>
        private string _target_db_servername;

        /// <summary>
        /// Gets or sets the target_db_servername.
        /// </summary>
        /// <value>
        /// The target_db_servername.
        /// </value>
	    [Editable(true)]
        [Column("target_db_servername")]
		public string target_db_servername
        {
            get { return _target_db_servername; }
            set
            {
                if (_target_db_servername != value)
                {
                    PropertyChanged("target_db_servername");
					_target_db_servername = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the macabi_transaction_code.
        /// </summary>
        /// <value>
        /// The macabi_transaction_code.
        /// </value>
        private long _macabi_transaction_code;

        /// <summary>
        /// Gets or sets the macabi_transaction_code.
        /// </summary>
        /// <value>
        /// The macabi_transaction_code.
        /// </value>
	    [Editable(true)]
        [Column("macabi_transaction_code")]
		public long macabi_transaction_code
        {
            get { return _macabi_transaction_code; }
            set
            {
                if (_macabi_transaction_code != value)
                {
                    PropertyChanged("macabi_transaction_code");
					_macabi_transaction_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the leumit_transaction_code.
        /// </summary>
        /// <value>
        /// The leumit_transaction_code.
        /// </value>
        private long _leumit_transaction_code;

        /// <summary>
        /// Gets or sets the leumit_transaction_code.
        /// </summary>
        /// <value>
        /// The leumit_transaction_code.
        /// </value>
	    [Editable(true)]
        [Column("leumit_transaction_code")]
		public long leumit_transaction_code
        {
            get { return _leumit_transaction_code; }
            set
            {
                if (_leumit_transaction_code != value)
                {
                    PropertyChanged("leumit_transaction_code");
					_leumit_transaction_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the card_terminal_proxy_active.
        /// </summary>
        /// <value>
        /// The card_terminal_proxy_active.
        /// </value>
        private double _card_terminal_proxy_active;

        /// <summary>
        /// Gets or sets the card_terminal_proxy_active.
        /// </summary>
        /// <value>
        /// The card_terminal_proxy_active.
        /// </value>
	    [Editable(true)]
        [Column("card_terminal_proxy_active")]
		public double card_terminal_proxy_active
        {
            get { return _card_terminal_proxy_active; }
            set
            {
                if (_card_terminal_proxy_active != value)
                {
                    PropertyChanged("card_terminal_proxy_active");
					_card_terminal_proxy_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the card_terminal_path_from_client.
        /// </summary>
        /// <value>
        /// The card_terminal_path_from_client.
        /// </value>
        private string _card_terminal_path_from_client;

        /// <summary>
        /// Gets or sets the card_terminal_path_from_client.
        /// </summary>
        /// <value>
        /// The card_terminal_path_from_client.
        /// </value>
	    [Editable(true)]
        [Column("card_terminal_path_from_client")]
		public string card_terminal_path_from_client
        {
            get { return _card_terminal_path_from_client; }
            set
            {
                if (_card_terminal_path_from_client != value)
                {
                    PropertyChanged("card_terminal_path_from_client");
					_card_terminal_path_from_client = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the card_terminal_path_from_proxy.
        /// </summary>
        /// <value>
        /// The card_terminal_path_from_proxy.
        /// </value>
        private string _card_terminal_path_from_proxy;

        /// <summary>
        /// Gets or sets the card_terminal_path_from_proxy.
        /// </summary>
        /// <value>
        /// The card_terminal_path_from_proxy.
        /// </value>
	    [Editable(true)]
        [Column("card_terminal_path_from_proxy")]
		public string card_terminal_path_from_proxy
        {
            get { return _card_terminal_path_from_proxy; }
            set
            {
                if (_card_terminal_path_from_proxy != value)
                {
                    PropertyChanged("card_terminal_path_from_proxy");
					_card_terminal_path_from_proxy = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the card_terminal_number.
        /// </summary>
        /// <value>
        /// The card_terminal_number.
        /// </value>
        private string _card_terminal_number;

        /// <summary>
        /// Gets or sets the card_terminal_number.
        /// </summary>
        /// <value>
        /// The card_terminal_number.
        /// </value>
	    [Editable(true)]
        [Column("card_terminal_number")]
		public string card_terminal_number
        {
            get { return _card_terminal_number; }
            set
            {
                if (_card_terminal_number != value)
                {
                    PropertyChanged("card_terminal_number");
					_card_terminal_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the card_terminal_version.
        /// </summary>
        /// <value>
        /// The card_terminal_version.
        /// </value>
        private string _card_terminal_version;

        /// <summary>
        /// Gets or sets the card_terminal_version.
        /// </summary>
        /// <value>
        /// The card_terminal_version.
        /// </value>
	    [Editable(true)]
        [Column("card_terminal_version")]
		public string card_terminal_version
        {
            get { return _card_terminal_version; }
            set
            {
                if (_card_terminal_version != value)
                {
                    PropertyChanged("card_terminal_version");
					_card_terminal_version = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_active.
        /// </summary>
        /// <value>
        /// The queue_db_active.
        /// </value>
        private double _queue_db_active;

        /// <summary>
        /// Gets or sets the queue_db_active.
        /// </summary>
        /// <value>
        /// The queue_db_active.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_active")]
		public double queue_db_active
        {
            get { return _queue_db_active; }
            set
            {
                if (_queue_db_active != value)
                {
                    PropertyChanged("queue_db_active");
					_queue_db_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_type.
        /// </summary>
        /// <value>
        /// The queue_db_type.
        /// </value>
        private string _queue_db_type;

        /// <summary>
        /// Gets or sets the queue_db_type.
        /// </summary>
        /// <value>
        /// The queue_db_type.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_type")]
		public string queue_db_type
        {
            get { return _queue_db_type; }
            set
            {
                if (_queue_db_type != value)
                {
                    PropertyChanged("queue_db_type");
					_queue_db_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_dbms.
        /// </summary>
        /// <value>
        /// The queue_db_dbms.
        /// </value>
        private string _queue_db_dbms;

        /// <summary>
        /// Gets or sets the queue_db_dbms.
        /// </summary>
        /// <value>
        /// The queue_db_dbms.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_dbms")]
		public string queue_db_dbms
        {
            get { return _queue_db_dbms; }
            set
            {
                if (_queue_db_dbms != value)
                {
                    PropertyChanged("queue_db_dbms");
					_queue_db_dbms = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_dbparm.
        /// </summary>
        /// <value>
        /// The queue_db_dbparm.
        /// </value>
        private string _queue_db_dbparm;

        /// <summary>
        /// Gets or sets the queue_db_dbparm.
        /// </summary>
        /// <value>
        /// The queue_db_dbparm.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_dbparm")]
		public string queue_db_dbparm
        {
            get { return _queue_db_dbparm; }
            set
            {
                if (_queue_db_dbparm != value)
                {
                    PropertyChanged("queue_db_dbparm");
					_queue_db_dbparm = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_database.
        /// </summary>
        /// <value>
        /// The queue_db_database.
        /// </value>
        private string _queue_db_database;

        /// <summary>
        /// Gets or sets the queue_db_database.
        /// </summary>
        /// <value>
        /// The queue_db_database.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_database")]
		public string queue_db_database
        {
            get { return _queue_db_database; }
            set
            {
                if (_queue_db_database != value)
                {
                    PropertyChanged("queue_db_database");
					_queue_db_database = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_servername.
        /// </summary>
        /// <value>
        /// The queue_db_servername.
        /// </value>
        private string _queue_db_servername;

        /// <summary>
        /// Gets or sets the queue_db_servername.
        /// </summary>
        /// <value>
        /// The queue_db_servername.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_servername")]
		public string queue_db_servername
        {
            get { return _queue_db_servername; }
            set
            {
                if (_queue_db_servername != value)
                {
                    PropertyChanged("queue_db_servername");
					_queue_db_servername = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the macabi_delete_after_tran.
        /// </summary>
        /// <value>
        /// The macabi_delete_after_tran.
        /// </value>
        private double _macabi_delete_after_tran;

        /// <summary>
        /// Gets or sets the macabi_delete_after_tran.
        /// </summary>
        /// <value>
        /// The macabi_delete_after_tran.
        /// </value>
	    [Editable(true)]
        [Column("macabi_delete_after_tran")]
		public double macabi_delete_after_tran
        {
            get { return _macabi_delete_after_tran; }
            set
            {
                if (_macabi_delete_after_tran != value)
                {
                    PropertyChanged("macabi_delete_after_tran");
					_macabi_delete_after_tran = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the leumit_delete_after_tran.
        /// </summary>
        /// <value>
        /// The leumit_delete_after_tran.
        /// </value>
        private double _leumit_delete_after_tran;

        /// <summary>
        /// Gets or sets the leumit_delete_after_tran.
        /// </summary>
        /// <value>
        /// The leumit_delete_after_tran.
        /// </value>
	    [Editable(true)]
        [Column("leumit_delete_after_tran")]
		public double leumit_delete_after_tran
        {
            get { return _leumit_delete_after_tran; }
            set
            {
                if (_leumit_delete_after_tran != value)
                {
                    PropertyChanged("leumit_delete_after_tran");
					_leumit_delete_after_tran = value;
                    
                }
            }
        }
	}
}
