using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_dddw_suppliers : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_dddw_suppliers()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="number">The number.</param>
		/// <param name="name">The name.</param>
		/// <param name="flag_select">The flag_select.</param>
		/// <param name="sort">The sort.</param>
		/// <param name="canceled_date">The canceled_date.</param>
        public d_dddw_suppliers(long number, string name, long flag_select, long sort, DateTime? canceled_date)
        {
			this._number = number;
			this._name = name;
			this._flag_select = flag_select;
			this._sort = sort;
			this._canceled_date = canceled_date;
		}

	
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        private long _number;

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
	    [Editable(true)]
        [Column("number")]
		public long number
        {
            get { return _number; }
            set
            {
                if (_number != value)
                {
                    PropertyChanged("number");
					_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string _name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
	    [Editable(true)]
        [Column("name")]
		public string name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    PropertyChanged("name");
					_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_select.
        /// </summary>
        /// <value>
        /// The flag_select.
        /// </value>
        private long _flag_select;

        /// <summary>
        /// Gets or sets the flag_select.
        /// </summary>
        /// <value>
        /// The flag_select.
        /// </value>
	    [Editable(true)]
        [Column("flag_select")]
		public long flag_select
        {
            get { return _flag_select; }
            set
            {
                if (_flag_select != value)
                {
                    PropertyChanged("flag_select");
					_flag_select = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        /// <value>
        /// The sort.
        /// </value>
        private long _sort;

        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        /// <value>
        /// The sort.
        /// </value>
	    [Editable(true)]
        [Column("sort")]
		public long sort
        {
            get { return _sort; }
            set
            {
                if (_sort != value)
                {
                    PropertyChanged("sort");
					_sort = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the canceled_date.
        /// </summary>
        /// <value>
        /// The canceled_date.
        /// </value>
        private DateTime? _canceled_date;

        /// <summary>
        /// Gets or sets the canceled_date.
        /// </summary>
        /// <value>
        /// The canceled_date.
        /// </value>
	    [Editable(true)]
        [Column("canceled_date")]
		public DateTime? canceled_date
        {
            get { return _canceled_date; }
            set
            {
                if (_canceled_date != value)
                {
                    PropertyChanged("canceled_date");
					_canceled_date = value;
                    
                }
            }
        }
	}
}
