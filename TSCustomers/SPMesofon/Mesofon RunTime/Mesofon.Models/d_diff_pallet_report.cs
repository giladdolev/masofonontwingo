using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_diff_pallet_report : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_diff_pallet_report()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="doc_number">The doc_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="actual_quantity">The actual_quantity.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="materials_name">The materials_name.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="row">The row.</param>
        public d_diff_pallet_report(long branch_number, long doc_number, long material_number, decimal material_quantity, decimal actual_quantity, long supplier_number, string materials_name, string materials_barcode, string row)
        {
			this._branch_number = branch_number;
			this._doc_number = doc_number;
			this._material_number = material_number;
			this._material_quantity = material_quantity;
			this._actual_quantity = actual_quantity;
			this._supplier_number = supplier_number;
			this._materials_name = materials_name;
			this._materials_barcode = materials_barcode;
			this._row = row;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
        private long _doc_number;

        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
	    [Editable(true)]
        [Column("doc_number")]
		public long doc_number
        {
            get { return _doc_number; }
            set
            {
                if (_doc_number != value)
                {
                    PropertyChanged("doc_number");
					_doc_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
        private decimal _actual_quantity;

        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
	    [Editable(true)]
        [Column("actual_quantity")]
		public decimal actual_quantity
        {
            get { return _actual_quantity; }
            set
            {
                if (_actual_quantity != value)
                {
                    PropertyChanged("actual_quantity");
					_actual_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
        private string _materials_name;

        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_name")]
		public string materials_name
        {
            get { return _materials_name; }
            set
            {
                if (_materials_name != value)
                {
                    PropertyChanged("materials_name");
					_materials_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
		public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	}
}
