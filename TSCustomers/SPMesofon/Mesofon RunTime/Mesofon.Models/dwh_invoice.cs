using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dwh_invoice : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dwh_invoice()
        {
            this._invoice_number = null;
            this._supply_date = DateTime.Now;
            this._date_move = DateTime.Now;
            this._state = "O";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="supply_date">The supply_date.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="discount">The discount.</param>
		/// <param name="state">The state.</param>
		/// <param name="mam">The mam.</param>
		/// <param name="station_num">The station_num.</param>
		/// <param name="invoice_total">The invoice_total.</param>
		/// <param name="store_number">The store_number.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="discount_percentage">The discount_percentage.</param>
		/// <param name="expected_total_amount">The expected_total_amount.</param>
		/// <param name="log_book">The log_book.</param>
		/// <param name="invoice_type">The invoice_type.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="payment_code_number">The payment_code_number.</param>
		/// <param name="payments_number">The payments_number.</param>
		/// <param name="stock_update_datetime">The stock_update_datetime.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="price_list_number">The price_list_number.</param>
		/// <param name="life_manufacturer">The life_manufacturer.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="discount_percent">The discount_percent.</param>
        public dwh_invoice(long invoice_number, DateTime? supply_date, long supplier_number, long employee_number, DateTime? date_move, decimal discount, string state, decimal mam, long station_num, decimal invoice_total, long store_number, long stock_number, long branch_number, decimal discount_percentage, decimal expected_total_amount, long log_book, string invoice_type, long b2b_status, long order_number, long payment_code_number, long payments_number, DateTime? stock_update_datetime, long distributor_number, long price_list_number, long life_manufacturer, DateTime? last_update_datetime, decimal discount_percent)
        {
			this._invoice_number = invoice_number;
			this._supply_date = supply_date;
			this._supplier_number = supplier_number;
			this._employee_number = employee_number;
			this._date_move = date_move;
			this._discount = discount;
			this._state = state;
			this._mam = mam;
			this._station_num = station_num;
			this._invoice_total = invoice_total;
			this._store_number = store_number;
			this._stock_number = stock_number;
			this._branch_number = branch_number;
			this._discount_percentage = discount_percentage;
			this._expected_total_amount = expected_total_amount;
			this._log_book = log_book;
			this._invoice_type = invoice_type;
			this._b2b_status = b2b_status;
			this._order_number = order_number;
			this._payment_code_number = payment_code_number;
			this._payments_number = payments_number;
			this._stock_update_datetime = stock_update_datetime;
			this._distributor_number = distributor_number;
			this._price_list_number = price_list_number;
			this._life_manufacturer = life_manufacturer;
			this._last_update_datetime = last_update_datetime;
			this._discount_percent = discount_percent;
		}

	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long? _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_number")]
		public long? invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
        private DateTime? _supply_date;

        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
	    [Editable(true)]
        [Column("supply_date")]
		public DateTime? supply_date
        {
            get { return _supply_date; }
            set
            {
                if (_supply_date != value)
                {
                    PropertyChanged("supply_date");
					_supply_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
        private decimal _discount;

        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
	    [Editable(true)]
        [Column("discount")]
		public decimal discount
        {
            get { return _discount; }
            set
            {
                if (_discount != value)
                {
                    PropertyChanged("discount");
					_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
        private decimal _mam;

        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
	    [Editable(true)]
        [Column("mam")]
		public decimal mam
        {
            get { return _mam; }
            set
            {
                if (_mam != value)
                {
                    PropertyChanged("mam");
					_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
        private long _station_num;

        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
	    [Editable(true)]
        [Column("station_num")]
		public long station_num
        {
            get { return _station_num; }
            set
            {
                if (_station_num != value)
                {
                    PropertyChanged("station_num");
					_station_num = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_total.
        /// </summary>
        /// <value>
        /// The invoice_total.
        /// </value>
        private decimal _invoice_total;

        /// <summary>
        /// Gets or sets the invoice_total.
        /// </summary>
        /// <value>
        /// The invoice_total.
        /// </value>
	    [Editable(true)]
        [Column("invoice_total")]
		public decimal invoice_total
        {
            get { return _invoice_total; }
            set
            {
                if (_invoice_total != value)
                {
                    PropertyChanged("invoice_total");
					_invoice_total = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
        private long _store_number;

        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
	    [Editable(true)]
        [Column("store_number")]
		public long store_number
        {
            get { return _store_number; }
            set
            {
                if (_store_number != value)
                {
                    PropertyChanged("store_number");
					_store_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percentage.
        /// </summary>
        /// <value>
        /// The discount_percentage.
        /// </value>
        private decimal _discount_percentage;

        /// <summary>
        /// Gets or sets the discount_percentage.
        /// </summary>
        /// <value>
        /// The discount_percentage.
        /// </value>
	    [Editable(true)]
        [Column("discount_percentage")]
		public decimal discount_percentage
        {
            get { return _discount_percentage; }
            set
            {
                if (_discount_percentage != value)
                {
                    PropertyChanged("discount_percentage");
					_discount_percentage = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
        private decimal _expected_total_amount;

        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
	    [Editable(true)]
        [Column("expected_total_amount")]
		public decimal expected_total_amount
        {
            get { return _expected_total_amount; }
            set
            {
                if (_expected_total_amount != value)
                {
                    PropertyChanged("expected_total_amount");
					_expected_total_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
        private long _log_book;

        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
	    [Editable(true)]
        [Column("log_book")]
		public long log_book
        {
            get { return _log_book; }
            set
            {
                if (_log_book != value)
                {
                    PropertyChanged("log_book");
					_log_book = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
        private string _invoice_type;

        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_type")]
		public string invoice_type
        {
            get { return _invoice_type; }
            set
            {
                if (_invoice_type != value)
                {
                    PropertyChanged("invoice_type");
					_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
        private long _payment_code_number;

        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
	    [Editable(true)]
        [Column("payment_code_number")]
		public long payment_code_number
        {
            get { return _payment_code_number; }
            set
            {
                if (_payment_code_number != value)
                {
                    PropertyChanged("payment_code_number");
					_payment_code_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
        private long _payments_number;

        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
	    [Editable(true)]
        [Column("payments_number")]
		public long payments_number
        {
            get { return _payments_number; }
            set
            {
                if (_payments_number != value)
                {
                    PropertyChanged("payments_number");
					_payments_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_update_datetime.
        /// </summary>
        /// <value>
        /// The stock_update_datetime.
        /// </value>
        private DateTime? _stock_update_datetime;

        /// <summary>
        /// Gets or sets the stock_update_datetime.
        /// </summary>
        /// <value>
        /// The stock_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("stock_update_datetime")]
		public DateTime? stock_update_datetime
        {
            get { return _stock_update_datetime; }
            set
            {
                if (_stock_update_datetime != value)
                {
                    PropertyChanged("stock_update_datetime");
					_stock_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the price_list_number.
        /// </summary>
        /// <value>
        /// The price_list_number.
        /// </value>
        private long _price_list_number;

        /// <summary>
        /// Gets or sets the price_list_number.
        /// </summary>
        /// <value>
        /// The price_list_number.
        /// </value>
	    [Editable(true)]
        [Column("price_list_number")]
		public long price_list_number
        {
            get { return _price_list_number; }
            set
            {
                if (_price_list_number != value)
                {
                    PropertyChanged("price_list_number");
					_price_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the life_manufacturer.
        /// </summary>
        /// <value>
        /// The life_manufacturer.
        /// </value>
        private long _life_manufacturer;

        /// <summary>
        /// Gets or sets the life_manufacturer.
        /// </summary>
        /// <value>
        /// The life_manufacturer.
        /// </value>
	    [Editable(true)]
        [Column("life_manufacturer")]
		public long life_manufacturer
        {
            get { return _life_manufacturer; }
            set
            {
                if (_life_manufacturer != value)
                {
                    PropertyChanged("life_manufacturer");
					_life_manufacturer = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
        private decimal _discount_percent;

        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("discount_percent")]
		public decimal discount_percent
        {
            get { return _discount_percent; }
            set
            {
                if (_discount_percent != value)
                {
                    PropertyChanged("discount_percent");
					_discount_percent = value;
                    
                }
            }
        }
	}
}
