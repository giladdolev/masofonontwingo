using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class dddw_get_return_reasons : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dddw_get_return_reasons()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="decline_code">The decline_code.</param>
		/// <param name="display_name">The display_name.</param>
		/// <param name="is_reject_with_qty">The is_reject_with_qty.</param>
		/// <param name="is_reject_all_invoice">The is_reject_all_invoice.</param>
		/// <param name="is_automatic_applied">The is_automatic_applied.</param>
		/// <param name="is_red_stamp">The is_red_stamp.</param>
        public dddw_get_return_reasons(long decline_number, string decline_code, string display_name, long is_reject_with_qty, long is_reject_all_invoice, long is_automatic_applied, long is_red_stamp)
        {
			this._decline_number = decline_number;
			this._decline_code = decline_code;
			this._display_name = display_name;
			this._is_reject_with_qty = is_reject_with_qty;
			this._is_reject_all_invoice = is_reject_all_invoice;
			this._is_automatic_applied = is_automatic_applied;
			this._is_red_stamp = is_red_stamp;
		}

	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("decline_number")]
        [PropertiesAttribute("decline_number", "decline_number", true, 0, typeof(long), 50)]
        public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_code.
        /// </summary>
        /// <value>
        /// The decline_code.
        /// </value>
        private string _decline_code;

        /// <summary>
        /// Gets or sets the decline_code.
        /// </summary>
        /// <value>
        /// The decline_code.
        /// </value>
	    [Editable(true)]
        [Column("decline_code")]
		public string decline_code
        {
            get { return _decline_code; }
            set
            {
                if (_decline_code != value)
                {
                    PropertyChanged("decline_code");
					_decline_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
        private string _display_name;

        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
	    [Editable(true)]
        [Column("display_name")]
        [PropertiesAttribute("display_name", "display_name", true, 1, typeof(string), 50)]
        public string display_name
        {
            get { return _display_name; }
            set
            {
                if (_display_name != value)
                {
                    PropertyChanged("display_name");
					_display_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_reject_with_qty.
        /// </summary>
        /// <value>
        /// The is_reject_with_qty.
        /// </value>
        private long _is_reject_with_qty;

        /// <summary>
        /// Gets or sets the is_reject_with_qty.
        /// </summary>
        /// <value>
        /// The is_reject_with_qty.
        /// </value>
	    [Editable(true)]
        [Column("is_reject_with_qty")]
		public long is_reject_with_qty
        {
            get { return _is_reject_with_qty; }
            set
            {
                if (_is_reject_with_qty != value)
                {
                    PropertyChanged("is_reject_with_qty");
					_is_reject_with_qty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_reject_all_invoice.
        /// </summary>
        /// <value>
        /// The is_reject_all_invoice.
        /// </value>
        private long _is_reject_all_invoice;

        /// <summary>
        /// Gets or sets the is_reject_all_invoice.
        /// </summary>
        /// <value>
        /// The is_reject_all_invoice.
        /// </value>
	    [Editable(true)]
        [Column("is_reject_all_invoice")]
		public long is_reject_all_invoice
        {
            get { return _is_reject_all_invoice; }
            set
            {
                if (_is_reject_all_invoice != value)
                {
                    PropertyChanged("is_reject_all_invoice");
					_is_reject_all_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_automatic_applied.
        /// </summary>
        /// <value>
        /// The is_automatic_applied.
        /// </value>
        private long _is_automatic_applied;

        /// <summary>
        /// Gets or sets the is_automatic_applied.
        /// </summary>
        /// <value>
        /// The is_automatic_applied.
        /// </value>
	    [Editable(true)]
        [Column("is_automatic_applied")]
		public long is_automatic_applied
        {
            get { return _is_automatic_applied; }
            set
            {
                if (_is_automatic_applied != value)
                {
                    PropertyChanged("is_automatic_applied");
					_is_automatic_applied = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_red_stamp.
        /// </summary>
        /// <value>
        /// The is_red_stamp.
        /// </value>
        private long _is_red_stamp;

        /// <summary>
        /// Gets or sets the is_red_stamp.
        /// </summary>
        /// <value>
        /// The is_red_stamp.
        /// </value>
	    [Editable(true)]
        [Column("is_red_stamp")]
		public long is_red_stamp
        {
            get { return _is_red_stamp; }
            set
            {
                if (_is_red_stamp != value)
                {
                    PropertyChanged("is_red_stamp");
					_is_red_stamp = value;
                    
                }
            }
        }
	}
}
