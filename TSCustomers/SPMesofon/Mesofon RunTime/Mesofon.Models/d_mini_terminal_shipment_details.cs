using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_shipment_details : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_shipment_details()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="state">The state.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="pallet_number">The pallet_number.</param>
		/// <param name="item_number">The item_number.</param>
		/// <param name="item_barcode">The item_barcode.</param>
        public d_mini_terminal_shipment_details(long branch_number, long supplier_number, double order_number, string state, double invoice_number, double shipment_number, string pallet_number, long item_number, string item_barcode)
        {
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._order_number = order_number;
			this._state = state;
			this._invoice_number = invoice_number;
			this._shipment_number = shipment_number;
			this._pallet_number = pallet_number;
			this._item_number = item_number;
			this._item_barcode = item_barcode;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private double _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public double order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private double _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public double invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_number")]
		public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
        private string _pallet_number;

        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
	    [Editable(true)]
        [Column("pallet_number")]
		public string pallet_number
        {
            get { return _pallet_number; }
            set
            {
                if (_pallet_number != value)
                {
                    PropertyChanged("pallet_number");
					_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
        private long _item_number;

        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
	    [Editable(true)]
        [Column("item_number")]
		public long item_number
        {
            get { return _item_number; }
            set
            {
                if (_item_number != value)
                {
                    PropertyChanged("item_number");
					_item_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
        private string _item_barcode;

        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
	    [Editable(true)]
        [Column("item_barcode")]
		public string item_barcode
        {
            get { return _item_barcode; }
            set
            {
                if (_item_barcode != value)
                {
                    PropertyChanged("item_barcode");
					_item_barcode = value;
                    
                }
            }
        }
	}
}
