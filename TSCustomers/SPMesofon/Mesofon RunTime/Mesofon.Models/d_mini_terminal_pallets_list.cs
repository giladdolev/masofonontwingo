using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;
using System.Web.VisualTree.Elements;
using System.Drawing;

namespace Mesofon.Models
{
	public class d_mini_terminal_pallets_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_pallets_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="auto_scan_status">The auto_scan_status.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="pallet_number">The pallet_number.</param>
		/// <param name="pack_quantity">The pack_quantity.</param>
		/// <param name="arrive_datetime">The arrive_datetime.</param>
		/// <param name="approve_employee">The approve_employee.</param>
		/// <param name="approve_ind">The approve_ind.</param>
		/// <param name="short_pallet_number">The short_pallet_number.</param>
		/// <param name="state">The state.</param>
		/// <param name="document_type">The document_type.</param>
		/// <param name="marlog_number">The marlog_number.</param>
		/// <param name="value_indicator">The value_indicator.</param>
		/// <param name="message_state">The message_state.</param>
		/// <param name="new_scan">The new_scan.</param>
		/// <param name="compute_1">The compute_1.</param>
        public d_mini_terminal_pallets_list(double shipment_number, long auto_scan_status, long branch_number, string pallet_number, long pack_quantity, DateTime? arrive_datetime, long approve_employee, long approve_ind, string short_pallet_number, string state, long document_type, long marlog_number, long value_indicator, string message_state, long new_scan, string compute_1)
        {
			this._shipment_number = shipment_number;
			this._auto_scan_status = auto_scan_status;
			this._branch_number = branch_number;
			this._pallet_number = pallet_number;
			this._pack_quantity = pack_quantity;
			this._arrive_datetime = arrive_datetime;
			this._approve_employee = approve_employee;
			this._approve_ind = approve_ind;
			this._short_pallet_number = short_pallet_number;
			this._state = state;
			this._document_type = document_type;
			this._marlog_number = marlog_number;
			this._value_indicator = value_indicator;
			this._message_state = message_state;
			this._new_scan = new_scan;
			this._compute_1 = compute_1;
		}

	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("shipment_number")]
        public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }

        private string _stateauto_scan_status;

        [Editable(true)]
        [Column("_stateauto_scan_status")]
        [PropertiesAttribute("state", "�����", true, 3, typeof(GridCustomColumn), 115)]
        [CustomColumn("SP_ButtonButton")]
        [ButtonButtonColumn(Buttons.Button1 , "b_manual", "����", "auto_scan_status > 0")]
        [ButtonButtonColumn(Buttons.Button2 , "b_automatic", "���", "auto_scan_status = 1")]
        public string state_auto_scan_status
        {
            get { return _stateauto_scan_status; }
            set
            {
                if (_stateauto_scan_status != value)
                {
                    _stateauto_scan_status = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the auto_scan_status.
        /// </summary>
        /// <value>
        /// The auto_scan_status.
        /// </value>
        private long _auto_scan_status;


        /// <summary>
        /// Gets or sets the auto_scan_status.
        /// </summary>
        /// <value>
        /// The auto_scan_status.
        /// </value>
	    [Editable(true)]
        [Column("auto_scan_status")]
        [PropertiesAttribute("auto_scan_status", "�����", false, 4,typeof(long),90)]
        public long auto_scan_status
        {
            get { return _auto_scan_status; }
            set
            {
                if (_auto_scan_status != value)
                {
                    PropertyChanged("auto_scan_status");
					_auto_scan_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
        private string _pallet_number;

        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("pallet_number")]
        [PropertiesAttribute("shipment_number", "�� ����", true, 0,typeof(string),125)]
        [BackColor("auto_scan_status == 3", "pallet_manual_backColor")]
        public string pallet_number
        {
            get { return _pallet_number; }
            set
            {
                if (_pallet_number != value)
                {
                    PropertyChanged("pallet_number");
					_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pack_quantity.
        /// </summary>
        /// <value>
        /// The pack_quantity.
        /// </value>
        private long _pack_quantity;

        /// <summary>
        /// Gets or sets the pack_quantity.
        /// </summary>
        /// <value>
        /// The pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("pack_quantity")]
        [PropertiesAttribute("pack_quantity", "���", true, 1,typeof(long),47)]
        [BackColor("auto_scan_status == 3", "pallet_manual_backColor")]
        public long pack_quantity
        {
            get { return _pack_quantity; }
            set
            {
                if (_pack_quantity != value)
                {
                    PropertyChanged("pack_quantity");
					_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the arrive_datetime.
        /// </summary>
        /// <value>
        /// The arrive_datetime.
        /// </value>
        private DateTime? _arrive_datetime;

        /// <summary>
        /// Gets or sets the arrive_datetime.
        /// </summary>
        /// <value>
        /// The arrive_datetime.
        /// </value>
	    [Editable(true)]
        [Column("arrive_datetime")]
		public DateTime? arrive_datetime
        {
            get { return _arrive_datetime; }
            set
            {
                if (_arrive_datetime != value)
                {
                    PropertyChanged("arrive_datetime");
					_arrive_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the approve_employee.
        /// </summary>
        /// <value>
        /// The approve_employee.
        /// </value>
        private long _approve_employee;

        /// <summary>
        /// Gets or sets the approve_employee.
        /// </summary>
        /// <value>
        /// The approve_employee.
        /// </value>
	    [Editable(true)]
        [Column("approve_employee")]
		public long approve_employee
        {
            get { return _approve_employee; }
            set
            {
                if (_approve_employee != value)
                {
                    PropertyChanged("approve_employee");
					_approve_employee = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the approve_ind.
        /// </summary>
        /// <value>
        /// The approve_ind.
        /// </value>
        private long _approve_ind;

        /// <summary>
        /// Gets or sets the approve_ind.
        /// </summary>
        /// <value>
        /// The approve_ind.
        /// </value>
	    [Editable(true)]
        [Column("approve_ind")]
        [PropertiesAttribute("approve_ind", "�����", true, 2, typeof(Boolean),53)]
        [BackColor("auto_scan_status == 3", "pallet_manual_backColor")]
        public long approve_ind
        {
            get { return _approve_ind; }
            set
            {
                if (_approve_ind != value)
                {
                    PropertyChanged("approve_ind");
					_approve_ind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the short_pallet_number.
        /// </summary>
        /// <value>
        /// The short_pallet_number.
        /// </value>
        private string _short_pallet_number;

        /// <summary>
        /// Gets or sets the short_pallet_number.
        /// </summary>
        /// <value>
        /// The short_pallet_number.
        /// </value>
	    [Editable(true)]
        [Column("short_pallet_number")]        
        public string short_pallet_number
        {
            get { return _short_pallet_number; }
            set
            {
                if (_short_pallet_number != value)
                {
                    PropertyChanged("short_pallet_number");
					_short_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]  
        public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the document_type.
        /// </summary>
        /// <value>
        /// The document_type.
        /// </value>
        private long _document_type;

        /// <summary>
        /// Gets or sets the document_type.
        /// </summary>
        /// <value>
        /// The document_type.
        /// </value>
	    [Editable(true)]
        [Column("document_type")]
		public long document_type
        {
            get { return _document_type; }
            set
            {
                if (_document_type != value)
                {
                    PropertyChanged("document_type");
					_document_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
        private long _marlog_number;

        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("marlog_number")]
		public long marlog_number
        {
            get { return _marlog_number; }
            set
            {
                if (_marlog_number != value)
                {
                    PropertyChanged("marlog_number");
					_marlog_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the value_indicator.
        /// </summary>
        /// <value>
        /// The value_indicator.
        /// </value>
        private long _value_indicator;

        /// <summary>
        /// Gets or sets the value_indicator.
        /// </summary>
        /// <value>
        /// The value_indicator.
        /// </value>
	    [Editable(true)]
        [Column("value_indicator")]
		public long value_indicator
        {
            get { return _value_indicator; }
            set
            {
                if (_value_indicator != value)
                {
                    PropertyChanged("value_indicator");
					_value_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the message_state.
        /// </summary>
        /// <value>
        /// The message_state.
        /// </value>
        private string _message_state;

        /// <summary>
        /// Gets or sets the message_state.
        /// </summary>
        /// <value>
        /// The message_state.
        /// </value>
	    [Editable(true)]
        [Column("message_state")]
		public string message_state
        {
            get { return _message_state; }
            set
            {
                if (_message_state != value)
                {
                    PropertyChanged("message_state");
					_message_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the new_scan.
        /// </summary>
        /// <value>
        /// The new_scan.
        /// </value>
        private long _new_scan;

        /// <summary>
        /// Gets or sets the new_scan.
        /// </summary>
        /// <value>
        /// The new_scan.
        /// </value>
	    [Editable(true)]
        [Column("new_scan")]
        public long new_scan
        {
            get { return _new_scan; }
            set
            {
                if (_new_scan != value)
                {
                    PropertyChanged("new_scan");
					_new_scan = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
        private string _compute_1;

        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_1")]
		public string compute_1
        {
            get { return _compute_1; }
            set
            {
                if (_compute_1 != value)
                {
                    PropertyChanged("compute_1");
					_compute_1 = value;
                    
                }
            }
        }
	}
}
