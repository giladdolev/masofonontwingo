using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_addhock_supplier_distributors : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_addhock_supplier_distributors()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="is_active">The is_active.</param>
		/// <param name="value">The value.</param>
        public d_addhock_supplier_distributors(long branch_number, long supplier_number, long distributor_number, long is_active, string value)
        {
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._distributor_number = distributor_number;
			this._is_active = is_active;
			this._value = value;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_active.
        /// </summary>
        /// <value>
        /// The is_active.
        /// </value>
        private long _is_active;

        /// <summary>
        /// Gets or sets the is_active.
        /// </summary>
        /// <value>
        /// The is_active.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("is_active")]
		public long is_active
        {
            get { return _is_active; }
            set
            {
                if (_is_active != value)
                {
                    PropertyChanged("is_active");
					_is_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        private string _value;

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
	    [Editable(true)]
        [Column("value")]
		public string value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    PropertyChanged("value");
					_value = value;
                    
                }
            }
        }
	}
}
