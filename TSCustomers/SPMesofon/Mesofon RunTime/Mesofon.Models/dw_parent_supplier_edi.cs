using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dw_parent_supplier_edi : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dw_parent_supplier_edi()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="supplier_parent">The supplier_parent.</param>
		/// <param name="parent_supp_edi_number">The parent_supp_edi_number.</param>
        public dw_parent_supplier_edi(long supplier_number, long supplier_parent, string parent_supp_edi_number)
        {
			this._supplier_number = supplier_number;
			this._supplier_parent = supplier_parent;
			this._parent_supp_edi_number = parent_supp_edi_number;
		}

	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_parent.
        /// </summary>
        /// <value>
        /// The supplier_parent.
        /// </value>
        private long _supplier_parent;

        /// <summary>
        /// Gets or sets the supplier_parent.
        /// </summary>
        /// <value>
        /// The supplier_parent.
        /// </value>
	    [Editable(true)]
        [Column("supplier_parent")]
		public long supplier_parent
        {
            get { return _supplier_parent; }
            set
            {
                if (_supplier_parent != value)
                {
                    PropertyChanged("supplier_parent");
					_supplier_parent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parent_supp_edi_number.
        /// </summary>
        /// <value>
        /// The parent_supp_edi_number.
        /// </value>
        private string _parent_supp_edi_number;

        /// <summary>
        /// Gets or sets the parent_supp_edi_number.
        /// </summary>
        /// <value>
        /// The parent_supp_edi_number.
        /// </value>
	    [Editable(true)]
        [Column("parent_supp_edi_number")]
		public string parent_supp_edi_number
        {
            get { return _parent_supp_edi_number; }
            set
            {
                if (_parent_supp_edi_number != value)
                {
                    PropertyChanged("parent_supp_edi_number");
					_parent_supp_edi_number = value;
                    
                }
            }
        }
	}
}
