using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_not_arrived_invoces_print : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_not_arrived_invoces_print()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="items_quantity">The items_quantity.</param>
		/// <param name="row">The row.</param>
        public d_not_arrived_invoces_print(long branch_number, long invoice_number, long supplier_number, string date_move, long order_number, long distributor_number, decimal material_quantity, long items_quantity, string row)
        {
			this._branch_number = branch_number;
			this._invoice_number = invoice_number;
			this._supplier_number = supplier_number;
			this._date_move = date_move;
			this._order_number = order_number;
			this._distributor_number = distributor_number;
			this._material_quantity = material_quantity;
			this._items_quantity = items_quantity;
			this._row = row;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
        [Properties("branch_number", "branch_number",true,0,typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]

        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the t_shipment_number.
        /// </summary>
        /// <value>
        /// The t_shipment_number.
        /// </value>
        private string _t_shipment_number;

        /// <summary>
        /// Gets or sets the t_shipment_number.
        /// </summary>
        /// <value>
        /// The t_shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("t_shipment_number")]
        [Properties("t_shipment_number", "t_shipment_number", true, 1, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]

        public string t_shipment_number
        {
            get { return _t_shipment_number; }
            set
            {
                if (_t_shipment_number != value)
                {
                    _t_shipment_number = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the branch_name_t.
        /// </summary>
        /// <value>
        /// The branch_name_t.
        /// </value>
        private string _branch_name_t;

        /// <summary>
        /// Gets or sets the branch_name_t.
        /// </summary>
        /// <value>
        /// The branch_name_t.
        /// </value>
	    [Editable(true)]
        [Column("branch_name_t")]
        [Properties("branch_name_t", "branch_name_t", true, 2, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]

        public string branch_name_t
        {
            get { return _branch_name_t; }
            set
            {
                if (_branch_name_t != value)
                {
                    _branch_name_t = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
        [System.Extensions.Properties("invoice_number", "���� �������", true, 5, typeof(long))]
        public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [System.Extensions.Properties("supplier_number", "���� ���",true,4,typeof(long))]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private string _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
        [Properties("date_move", "date_move", true, 8, typeof(string))]
        public string date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
        [Properties("order_number", "order_number", true, 7, typeof(long))]
        public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
        [Properties("distributor_number", "distributor_number", true, 6, typeof(long))]
        public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the items_quantity.
        /// </summary>
        /// <value>
        /// The items_quantity.
        /// </value>
        private long _items_quantity;

        /// <summary>
        /// Gets or sets the items_quantity.
        /// </summary>
        /// <value>
        /// The items_quantity.
        /// </value>
	    [Editable(true)]
        [Column("items_quantity")]
        [Properties("items_quantity", "items_quantity", true, 9, typeof(long))]
        public long items_quantity
        {
            get { return _items_quantity; }
            set
            {
                if (_items_quantity != value)
                {
                    PropertyChanged("items_quantity");
					_items_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
        [Properties("row", "row", true, 3, typeof(string))]
        public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	}
}
