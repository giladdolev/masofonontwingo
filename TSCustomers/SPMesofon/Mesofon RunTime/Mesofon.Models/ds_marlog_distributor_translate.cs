using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class ds_marlog_distributor_translate : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public ds_marlog_distributor_translate()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="sort_order">The sort_order.</param>
		/// <param name="default_value">The default_value.</param>
		/// <param name="description">The description.</param>
        public ds_marlog_distributor_translate(string branch_number, string distributor_number, double sort_order, double default_value, string description)
        {
			this._branch_number = branch_number;
			this._distributor_number = distributor_number;
			this._sort_order = sort_order;
			this._default_value = default_value;
			this._description = description;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private string _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
        [PropertiesAttribute("branch_number", "����\"�",true,0,typeof(string))]
		public string branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private string _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
        [PropertiesAttribute("distributor_number", "���� ����", true,1)]
        public string distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sort_order.
        /// </summary>
        /// <value>
        /// The sort_order.
        /// </value>
        private double _sort_order;

        /// <summary>
        /// Gets or sets the sort_order.
        /// </summary>
        /// <value>
        /// The sort_order.
        /// </value>
	    [Editable(true)]
        [Column("sort_order")]
		public double sort_order
        {
            get { return _sort_order; }
            set
            {
                if (_sort_order != value)
                {
                    PropertyChanged("sort_order");
					_sort_order = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the default_value.
        /// </summary>
        /// <value>
        /// The default_value.
        /// </value>
        private double _default_value;

        /// <summary>
        /// Gets or sets the default_value.
        /// </summary>
        /// <value>
        /// The default_value.
        /// </value>
	    [Editable(true)]
        [Column("default_value")]
		public double default_value
        {
            get { return _default_value; }
            set
            {
                if (_default_value != value)
                {
                    PropertyChanged("default_value");
					_default_value = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        private string _description;

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
	    [Editable(true)]
        [Column("description")]
		public string description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    PropertyChanged("description");
					_description = value;
                    
                }
            }
        }
	}
}
