using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_return_from_branch_print_rows : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_return_from_branch_print_rows()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="material_name">The material_name.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="mam">The mam.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="shipment_marlog_return_invoice_number">The shipment_marlog_return_invoice_number.</param>
		/// <param name="shipment_marlog_return_state">The shipment_marlog_return_state.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="log_book">The log_book.</param>
		/// <param name="shipment_marlog_return_date_move">The shipment_marlog_return_date_move.</param>
		/// <param name="invoice_type">The invoice_type.</param>
		/// <param name="vat_number">The vat_number.</param>
		/// <param name="phone_number">The phone_number.</param>
		/// <param name="fax_number">The fax_number.</param>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="cf_quantity_sum">The cf_quantity_sum.</param>
		/// <param name="line_number">The line_number.</param>
        public d_return_from_branch_print_rows(string materials_barcode, string material_name, decimal material_price, decimal material_quantity, decimal mam, long invoice_number, long supplier_number, long employee_number, long shipment_marlog_return_invoice_number, string shipment_marlog_return_state, string employee_name, string supplier_name, decimal sell_price, string branch_name, long log_book, DateTime? shipment_marlog_return_date_move, string invoice_type, string vat_number, string phone_number, string fax_number, long decline_number, string cf_quantity_sum, string line_number)
        {
			this._materials_barcode = materials_barcode;
			this._material_name = material_name;
			this._material_price = material_price;
			this._material_quantity = material_quantity;
			this._mam = mam;
			this._invoice_number = invoice_number;
			this._supplier_number = supplier_number;
			this._employee_number = employee_number;
			this._shipment_marlog_return_invoice_number = shipment_marlog_return_invoice_number;
			this._shipment_marlog_return_state = shipment_marlog_return_state;
			this._employee_name = employee_name;
			this._supplier_name = supplier_name;
			this._sell_price = sell_price;
			this._branch_name = branch_name;
			this._log_book = log_book;
			this._shipment_marlog_return_date_move = shipment_marlog_return_date_move;
			this._invoice_type = invoice_type;
			this._vat_number = vat_number;
			this._phone_number = phone_number;
			this._fax_number = fax_number;
			this._decline_number = decline_number;
			this._cf_quantity_sum = cf_quantity_sum;
			this._line_number = line_number;
		}

	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
		public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
        private decimal _mam;

        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
	    [Editable(true)]
        [Column("mam")]
		public decimal mam
        {
            get { return _mam; }
            set
            {
                if (_mam != value)
                {
                    PropertyChanged("mam");
					_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_invoice_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_invoice_number.
        /// </value>
        private long _shipment_marlog_return_invoice_number;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_invoice_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_invoice_number")]
		public long shipment_marlog_return_invoice_number
        {
            get { return _shipment_marlog_return_invoice_number; }
            set
            {
                if (_shipment_marlog_return_invoice_number != value)
                {
                    PropertyChanged("shipment_marlog_return_invoice_number");
					_shipment_marlog_return_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_state.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_state.
        /// </value>
        private string _shipment_marlog_return_state;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_state.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_state.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_state")]
		public string shipment_marlog_return_state
        {
            get { return _shipment_marlog_return_state; }
            set
            {
                if (_shipment_marlog_return_state != value)
                {
                    PropertyChanged("shipment_marlog_return_state");
					_shipment_marlog_return_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
		public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
		public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
		public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
        private long _log_book;

        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
	    [Editable(true)]
        [Column("log_book")]
		public long log_book
        {
            get { return _log_book; }
            set
            {
                if (_log_book != value)
                {
                    PropertyChanged("log_book");
					_log_book = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_date_move.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_date_move.
        /// </value>
        private DateTime? _shipment_marlog_return_date_move;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_date_move.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_date_move.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_date_move")]
		public DateTime? shipment_marlog_return_date_move
        {
            get { return _shipment_marlog_return_date_move; }
            set
            {
                if (_shipment_marlog_return_date_move != value)
                {
                    PropertyChanged("shipment_marlog_return_date_move");
					_shipment_marlog_return_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
        private string _invoice_type;

        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
	    [Editable(true)]
        [Column("invoice_type")]
		public string invoice_type
        {
            get { return _invoice_type; }
            set
            {
                if (_invoice_type != value)
                {
                    PropertyChanged("invoice_type");
					_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
        private string _vat_number;

        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
	    [Editable(true)]
        [Column("vat_number")]
		public string vat_number
        {
            get { return _vat_number; }
            set
            {
                if (_vat_number != value)
                {
                    PropertyChanged("vat_number");
					_vat_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the phone_number.
        /// </summary>
        /// <value>
        /// The phone_number.
        /// </value>
        private string _phone_number;

        /// <summary>
        /// Gets or sets the phone_number.
        /// </summary>
        /// <value>
        /// The phone_number.
        /// </value>
	    [Editable(true)]
        [Column("phone_number")]
		public string phone_number
        {
            get { return _phone_number; }
            set
            {
                if (_phone_number != value)
                {
                    PropertyChanged("phone_number");
					_phone_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the fax_number.
        /// </summary>
        /// <value>
        /// The fax_number.
        /// </value>
        private string _fax_number;

        /// <summary>
        /// Gets or sets the fax_number.
        /// </summary>
        /// <value>
        /// The fax_number.
        /// </value>
	    [Editable(true)]
        [Column("fax_number")]
		public string fax_number
        {
            get { return _fax_number; }
            set
            {
                if (_fax_number != value)
                {
                    PropertyChanged("fax_number");
					_fax_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
	    [Editable(true)]
        [Column("decline_number")]
		public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cf_quantity_sum.
        /// </summary>
        /// <value>
        /// The cf_quantity_sum.
        /// </value>
        private string _cf_quantity_sum;

        /// <summary>
        /// Gets or sets the cf_quantity_sum.
        /// </summary>
        /// <value>
        /// The cf_quantity_sum.
        /// </value>
	    [Editable(true)]
        [Column("cf_quantity_sum")]
		public string cf_quantity_sum
        {
            get { return _cf_quantity_sum; }
            set
            {
                if (_cf_quantity_sum != value)
                {
                    PropertyChanged("cf_quantity_sum");
					_cf_quantity_sum = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
        private string _line_number;

        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
	    [Editable(true)]
        [Column("line_number")]
		public string line_number
        {
            get { return _line_number; }
            set
            {
                if (_line_number != value)
                {
                    PropertyChanged("line_number");
					_line_number = value;
                    
                }
            }
        }
	}
}
