using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_return_marlog_print_footer : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_return_marlog_print_footer()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="shipment_marlog_return_branch_number">The shipment_marlog_return_branch_number.</param>
		/// <param name="shipment_marlog_return_supplier_number">The shipment_marlog_return_supplier_number.</param>
		/// <param name="shipment_marlog_return_invoice_number">The shipment_marlog_return_invoice_number.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="total_quantity">The total_quantity.</param>
        public d_return_marlog_print_footer(long shipment_marlog_return_branch_number, long shipment_marlog_return_supplier_number, double shipment_marlog_return_invoice_number, string supplier_name, long total_quantity)
        {
			this._shipment_marlog_return_branch_number = shipment_marlog_return_branch_number;
			this._shipment_marlog_return_supplier_number = shipment_marlog_return_supplier_number;
			this._shipment_marlog_return_invoice_number = shipment_marlog_return_invoice_number;
			this._supplier_name = supplier_name;
			this._total_quantity = total_quantity;
		}

	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_branch_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_branch_number.
        /// </value>
        private long _shipment_marlog_return_branch_number;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_branch_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_branch_number")]
        [Properties("shipment_marlog_return_branch_number", "shipment_marlog_return_branch_number", true, 0, typeof(long))]
        public long shipment_marlog_return_branch_number
        {
            get { return _shipment_marlog_return_branch_number; }
            set
            {
                if (_shipment_marlog_return_branch_number != value)
                {
                    PropertyChanged("shipment_marlog_return_branch_number");
					_shipment_marlog_return_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_supplier_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_supplier_number.
        /// </value>
        private long _shipment_marlog_return_supplier_number;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_supplier_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_supplier_number")]
        [Properties("shipment_marlog_return_supplier_number", "shipment_marlog_return_supplier_number", true, 1, typeof(long))]
        public long shipment_marlog_return_supplier_number
        {
            get { return _shipment_marlog_return_supplier_number; }
            set
            {
                if (_shipment_marlog_return_supplier_number != value)
                {
                    PropertyChanged("shipment_marlog_return_supplier_number");
					_shipment_marlog_return_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_marlog_return_invoice_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_invoice_number.
        /// </value>
        private double _shipment_marlog_return_invoice_number;

        /// <summary>
        /// Gets or sets the shipment_marlog_return_invoice_number.
        /// </summary>
        /// <value>
        /// The shipment_marlog_return_invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_marlog_return_invoice_number")]
        [Properties("shipment_marlog_return_invoice_number", "shipment_marlog_return_invoice_number", true, 2, typeof(double))]
        public double shipment_marlog_return_invoice_number
        {
            get { return _shipment_marlog_return_invoice_number; }
            set
            {
                if (_shipment_marlog_return_invoice_number != value)
                {
                    PropertyChanged("shipment_marlog_return_invoice_number");
					_shipment_marlog_return_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
        [Properties("supplier_name", "supplier_name", true, 3, typeof(string))]
        public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_quantity.
        /// </summary>
        /// <value>
        /// The total_quantity.
        /// </value>
        private long _total_quantity;

        /// <summary>
        /// Gets or sets the total_quantity.
        /// </summary>
        /// <value>
        /// The total_quantity.
        /// </value>
	    [Editable(true)]
        [Column("total_quantity")]
        [Properties("total_quantity", "total_quantity", true, 4, typeof(long))]
        public long total_quantity
        {
            get { return _total_quantity; }
            set
            {
                if (_total_quantity != value)
                {
                    PropertyChanged("total_quantity");
					_total_quantity = value;
                    
                }
            }
        }
	}
}
