using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_summery_pack : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_summery_pack()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="packing_list_details_branch_number">The packing_list_details_branch_number.</param>
		/// <param name="doc_no">The doc_no.</param>
		/// <param name="packing_list_move_branch_number">The packing_list_move_branch_number.</param>
		/// <param name="packing_list_move_supplier_number">The packing_list_move_supplier_number.</param>
		/// <param name="packing_list_move_packing_type">The packing_list_move_packing_type.</param>
		/// <param name="packing_list_details_order_number">The packing_list_details_order_number.</param>
		/// <param name="total">The total.</param>
        public d_summery_pack(long packing_list_details_branch_number, long doc_no, long packing_list_move_branch_number, long packing_list_move_supplier_number, string packing_list_move_packing_type, long packing_list_details_order_number, decimal total)
        {
			this._packing_list_details_branch_number = packing_list_details_branch_number;
			this._doc_no = doc_no;
			this._packing_list_move_branch_number = packing_list_move_branch_number;
			this._packing_list_move_supplier_number = packing_list_move_supplier_number;
			this._packing_list_move_packing_type = packing_list_move_packing_type;
			this._packing_list_details_order_number = packing_list_details_order_number;
			this._total = total;
		}

	
        /// <summary>
        /// Gets or sets the packing_list_details_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_branch_number.
        /// </value>
        private long _packing_list_details_branch_number;

        /// <summary>
        /// Gets or sets the packing_list_details_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_branch_number")]
		public long packing_list_details_branch_number
        {
            get { return _packing_list_details_branch_number; }
            set
            {
                if (_packing_list_details_branch_number != value)
                {
                    PropertyChanged("packing_list_details_branch_number");
					_packing_list_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
        private long _doc_no;

        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
	    [Editable(true)]
        [Column("doc_no")]
		public long doc_no
        {
            get { return _doc_no; }
            set
            {
                if (_doc_no != value)
                {
                    PropertyChanged("doc_no");
					_doc_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_move_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_move_branch_number.
        /// </value>
        private long _packing_list_move_branch_number;

        /// <summary>
        /// Gets or sets the packing_list_move_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_move_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_move_branch_number")]
		public long packing_list_move_branch_number
        {
            get { return _packing_list_move_branch_number; }
            set
            {
                if (_packing_list_move_branch_number != value)
                {
                    PropertyChanged("packing_list_move_branch_number");
					_packing_list_move_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_move_supplier_number.
        /// </summary>
        /// <value>
        /// The packing_list_move_supplier_number.
        /// </value>
        private long _packing_list_move_supplier_number;

        /// <summary>
        /// Gets or sets the packing_list_move_supplier_number.
        /// </summary>
        /// <value>
        /// The packing_list_move_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_move_supplier_number")]
		public long packing_list_move_supplier_number
        {
            get { return _packing_list_move_supplier_number; }
            set
            {
                if (_packing_list_move_supplier_number != value)
                {
                    PropertyChanged("packing_list_move_supplier_number");
					_packing_list_move_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_move_packing_type.
        /// </summary>
        /// <value>
        /// The packing_list_move_packing_type.
        /// </value>
        private string _packing_list_move_packing_type;

        /// <summary>
        /// Gets or sets the packing_list_move_packing_type.
        /// </summary>
        /// <value>
        /// The packing_list_move_packing_type.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_move_packing_type")]
		public string packing_list_move_packing_type
        {
            get { return _packing_list_move_packing_type; }
            set
            {
                if (_packing_list_move_packing_type != value)
                {
                    PropertyChanged("packing_list_move_packing_type");
					_packing_list_move_packing_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_order_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_number.
        /// </value>
        private long _packing_list_details_order_number;

        /// <summary>
        /// Gets or sets the packing_list_details_order_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_order_number")]
		public long packing_list_details_order_number
        {
            get { return _packing_list_details_order_number; }
            set
            {
                if (_packing_list_details_order_number != value)
                {
                    PropertyChanged("packing_list_details_order_number");
					_packing_list_details_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
        private decimal _total;

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
	    [Editable(true)]
        [Column("total")]
		public decimal total
        {
            get { return _total; }
            set
            {
                if (_total != value)
                {
                    PropertyChanged("total");
					_total = value;
                    
                }
            }
        }
	}
}
