using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_diff_order_shipment_report_print : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_diff_order_shipment_report_print()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="material_name">The material_name.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="row">The row.</param>
        public d_diff_order_shipment_report_print(long branch_number, string branch_name, long supplier_number, long order_number, long material_number, decimal order_quantity, decimal quantity_within_invoice, string material_name, string barcode, string row)
        {
			this._branch_number = branch_number;
			this._branch_name = branch_name;
			this._supplier_number = supplier_number;
			this._order_number = order_number;
			this._material_number = material_number;
			this._order_quantity = order_quantity;
			this._quantity_within_invoice = quantity_within_invoice;
			this._material_name = material_name;
			this._barcode = barcode;
			this._row = row;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
        [Properties("branch_number", "branch_number", true, 0, typeof(long))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the t_shipment_number.
        /// </summary>
        /// <value>
        /// The t_shipment_number.
        /// </value>
        private string _t_shipment_number;

        /// <summary>
        /// Gets or sets the t_shipment_number.
        /// </summary>
        /// <value>
        /// The t_shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("t_shipment_number")]
        [Properties("t_shipment_number", "t_shipment_number", true, 2, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string t_shipment_number
        {
            get { return _t_shipment_number; }
            set
            {
                if (_t_shipment_number != value)
                {
                    _t_shipment_number = value;
                }
            }
        }
        

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
        [Properties("branch_name", "branch_name", true, 1, typeof(string))]
        [XMLOutputAttribute(XMLOutputLocation.Header)]
        public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [Properties("supplier_number", "supplier_number", true, 7, typeof(long))]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
        [Properties("order_number", "order_number", true, 8, typeof(long))]
        public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the _order_quantity_t.
        /// </summary>
        /// <value>
        /// The _order_quantity_t.
        /// </value>
        private string _order_quantity_t;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity_t")]
        [Properties("order_quantity_t", "order_quantity_t", true, 6, typeof(string))]
        public string order_quantity_t
        {
            get { return _order_quantity_t; }
            set
            {
                if (_order_quantity_t != value)
                {
                    _order_quantity_t = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
        [Properties("order_quantity", "order_quantity", false, 16, typeof(decimal))]
        public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
		public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
        [Properties("material_name", "material_name", true, 5, typeof(string))]
        public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
        [Properties("barcode", "barcode", true, 4, typeof(string))]
        
        public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
        [Properties("row", "row", true, 3, typeof(string))]
        
        public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	}
}
