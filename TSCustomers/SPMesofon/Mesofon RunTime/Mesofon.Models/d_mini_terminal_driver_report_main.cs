using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_driver_report_main : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_driver_report_main()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="a">The a.</param>
		/// <param name="page_1">The page_1.</param>
		/// <param name="page_2">The page_2.</param>
        public d_mini_terminal_driver_report_main(string a, string page_1, string page_2)
        {
			this._a = a;
			this._page_1 = page_1;
			this._page_2 = page_2;
		}

	
        /// <summary>
        /// Gets or sets the a.
        /// </summary>
        /// <value>
        /// The a.
        /// </value>
        private string _a;

        /// <summary>
        /// Gets or sets the a.
        /// </summary>
        /// <value>
        /// The a.
        /// </value>
	    [Editable(true)]
        [Column("a")]
		public string a
        {
            get { return _a; }
            set
            {
                if (_a != value)
                {
                    PropertyChanged("a");
					_a = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the page_1.
        /// </summary>
        /// <value>
        /// The page_1.
        /// </value>
        private string _page_1;

        /// <summary>
        /// Gets or sets the page_1.
        /// </summary>
        /// <value>
        /// The page_1.
        /// </value>
	    [Editable(true)]
        [Column("page_1")]
		public string page_1
        {
            get { return _page_1; }
            set
            {
                if (_page_1 != value)
                {
                    PropertyChanged("page_1");
					_page_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the page_2.
        /// </summary>
        /// <value>
        /// The page_2.
        /// </value>
        private string _page_2;

        /// <summary>
        /// Gets or sets the page_2.
        /// </summary>
        /// <value>
        /// The page_2.
        /// </value>
	    [Editable(true)]
        [Column("page_2")]
		public string page_2
        {
            get { return _page_2; }
            set
            {
                if (_page_2 != value)
                {
                    PropertyChanged("page_2");
					_page_2 = value;
                    
                }
            }
        }
	}
}
