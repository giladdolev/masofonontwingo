using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_return_details_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_return_details_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="material_price_after_discount">The material_price_after_discount.</param>
		/// <param name="name">The name.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="date_move">The date_move.</param>
        public d_mini_terminal_return_details_list(long serial_number, long branch_number, long invoice_number, long material_number, decimal material_quantity, long supplier_number, long decline_number, decimal material_price, decimal material_price_after_discount, string name, string barcode, DateTime? date_move)
        {
			this._serial_number = serial_number;
			this._branch_number = branch_number;
			this._invoice_number = invoice_number;
			this._material_number = material_number;
			this._material_quantity = material_quantity;
			this._supplier_number = supplier_number;
			this._decline_number = decline_number;
			this._material_price = material_price;
			this._material_price_after_discount = material_price_after_discount;
			this._name = name;
			this._barcode = barcode;
			this._date_move = date_move;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
	    [Editable(true)]
        [Column("decline_number")]
		public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
        private decimal _material_price_after_discount;

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_discount")]
		public decimal material_price_after_discount
        {
            get { return _material_price_after_discount; }
            set
            {
                if (_material_price_after_discount != value)
                {
                    PropertyChanged("material_price_after_discount");
					_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string _name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
	    [Editable(true)]
        [Column("name")]
		public string name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    PropertyChanged("name");
					_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
		public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	}
}
