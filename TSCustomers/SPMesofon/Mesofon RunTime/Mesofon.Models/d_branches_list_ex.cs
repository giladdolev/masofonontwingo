using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_branches_list_ex : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_branches_list_ex()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_num">The branch_num.</param>
        public d_branches_list_ex(double branch_num)
        {
			this._branch_num = branch_num;
		}

	
        /// <summary>
        /// Gets or sets the branch_num.
        /// </summary>
        /// <value>
        /// The branch_num.
        /// </value>
        private double _branch_num;

        /// <summary>
        /// Gets or sets the branch_num.
        /// </summary>
        /// <value>
        /// The branch_num.
        /// </value>
	    [Editable(true)]
        [Column("branch_num")]
		public double branch_num
        {
            get { return _branch_num; }
            set
            {
                if (_branch_num != value)
                {
                    PropertyChanged("branch_num");
					_branch_num = value;
                    
                }
            }
        }
	}
}
