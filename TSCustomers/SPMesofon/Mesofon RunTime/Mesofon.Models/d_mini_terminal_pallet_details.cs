using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;
using System.Web.VisualTree.Elements;

namespace Mesofon.Models
{
    public class d_mini_terminal_pallet_details : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_pallet_details()
        {
            this._doc_no_en = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_details_serial_number">The invoice_details_serial_number.</param>
		/// <param name="invoice_details_branch_number">The invoice_details_branch_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="inv_pack_quantity">The inv_pack_quantity.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="invoice_details_details">The invoice_details_details.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="material_price_after_discount">The material_price_after_discount.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="packing_list_number">The packing_list_number.</param>
		/// <param name="order_serial">The order_serial.</param>
		/// <param name="invoice_details_stock_number">The invoice_details_stock_number.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="invoice_details_material_bonus_connect">The invoice_details_material_bonus_connect.</param>
		/// <param name="invoice_details_max_quantity_allowed">The invoice_details_max_quantity_allowed.</param>
		/// <param name="invoice_details_last_packing_change">The invoice_details_last_packing_change.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="invoice_details_material_discount_amount">The invoice_details_material_discount_amount.</param>
		/// <param name="invoice_details_bonus_row">The invoice_details_bonus_row.</param>
		/// <param name="color">The color.</param>
		/// <param name="invoice_details_pay_shape">The invoice_details_pay_shape.</param>
		/// <param name="indicator">The indicator.</param>
		/// <param name="invoice_details_bonus_discount">The invoice_details_bonus_discount.</param>
		/// <param name="invoice_details_supplier_discount_percent">The invoice_details_supplier_discount_percent.</param>
		/// <param name="current_catalog_sell_price">The current_catalog_sell_price.</param>
		/// <param name="expiration_date">The expiration_date.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="mini_terminal">The mini_terminal.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_material_name">The materials_material_name.</param>
		/// <param name="materials_min_valid_months">The materials_min_valid_months.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="doc_state">The doc_state.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="row_no">The row_no.</param>
		/// <param name="doc_no_en">The doc_no_en.</param>
		/// <param name="cng_doc_count">The cng_doc_count.</param>
		/// <param name="declines">The declines.</param>
		/// <param name="state">The state.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="compute_1">The compute_1.</param>
        public d_mini_terminal_pallet_details(long invoice_details_serial_number, long invoice_details_branch_number, long invoice_number, long material_number, decimal inv_pack_quantity, decimal material_quantity, decimal material_price, string invoice_details_details, long supplier_number, decimal material_discount_percent, decimal material_price_after_discount, decimal quantity_within_invoice, long packing_list_number, long order_serial, long invoice_details_stock_number, decimal sell_price, decimal bonus_quantity, long invoice_details_material_bonus_connect, decimal invoice_details_max_quantity_allowed, decimal invoice_details_last_packing_change, string import_type, decimal invoice_details_material_discount_amount, string invoice_details_bonus_row, long color, long invoice_details_pay_shape, long indicator, decimal invoice_details_bonus_discount, decimal invoice_details_supplier_discount_percent, decimal current_catalog_sell_price, DateTime? expiration_date, decimal expected_material_quantity, long mini_terminal, string materials_barcode, string materials_material_name, long materials_min_valid_months, decimal order_quantity, long b2b_status, string doc_state, long distributor_number, long row_no, long doc_no_en, long cng_doc_count, long declines, string state, DateTime? last_update_datetime, string compute_1)
        {
            this._invoice_details_serial_number = invoice_details_serial_number;
            this._invoice_details_branch_number = invoice_details_branch_number;
            this._invoice_number = invoice_number;
            this._material_number = material_number;
            this._inv_pack_quantity = inv_pack_quantity;
            this._material_quantity = material_quantity;
            this._material_price = material_price;
            this._invoice_details_details = invoice_details_details;
            this._supplier_number = supplier_number;
            this._material_discount_percent = material_discount_percent;
            this._material_price_after_discount = material_price_after_discount;
            this._quantity_within_invoice = quantity_within_invoice;
            this._packing_list_number = packing_list_number;
            this._order_serial = order_serial;
            this._invoice_details_stock_number = invoice_details_stock_number;
            this._sell_price = sell_price;
            this._bonus_quantity = bonus_quantity;
            this._invoice_details_material_bonus_connect = invoice_details_material_bonus_connect;
            this._invoice_details_max_quantity_allowed = invoice_details_max_quantity_allowed;
            this._invoice_details_last_packing_change = invoice_details_last_packing_change;
            this._import_type = import_type;
            this._invoice_details_material_discount_amount = invoice_details_material_discount_amount;
            this._invoice_details_bonus_row = invoice_details_bonus_row;
            this._color = color;
            this._invoice_details_pay_shape = invoice_details_pay_shape;
            this._indicator = indicator;
            this._invoice_details_bonus_discount = invoice_details_bonus_discount;
            this._invoice_details_supplier_discount_percent = invoice_details_supplier_discount_percent;
            this._current_catalog_sell_price = current_catalog_sell_price;
            this._expiration_date = expiration_date;
            this._expected_material_quantity = expected_material_quantity;
            this._mini_terminal = mini_terminal;
            this._materials_barcode = materials_barcode;
            this._materials_material_name = materials_material_name;
            this._materials_min_valid_months = materials_min_valid_months;
            this._order_quantity = order_quantity;
            this._b2b_status = b2b_status;
            this._doc_state = doc_state;
            this._distributor_number = distributor_number;
            this._row_no = row_no;
            this._doc_no_en = doc_no_en;
            this._cng_doc_count = cng_doc_count;
            this._declines = declines;
            this._state = state;
            this._last_update_datetime = last_update_datetime;
        }


        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
        private long _invoice_details_serial_number;

        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
		[Key()]
        [Editable(true)]
        [Column("invoice_details_serial_number")]
        public long invoice_details_serial_number
        {
            get { return _invoice_details_serial_number; }
            set
            {
                if (_invoice_details_serial_number != value)
                {
                    PropertyChanged("invoice_details_serial_number");
                    _invoice_details_serial_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
        private long _invoice_details_branch_number;

        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
		[Key()]
        [Editable(true)]
        [Column("invoice_details_branch_number")]
        public long invoice_details_branch_number
        {
            get { return _invoice_details_branch_number; }
            set
            {
                if (_invoice_details_branch_number != value)
                {
                    PropertyChanged("invoice_details_branch_number");
                    _invoice_details_branch_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
		[Key()]
        [Editable(true)]
        [Column("invoice_number")]
        [Properties("invoice_number", "invoice_number", false, 5)]
        public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
                    _invoice_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
		[Key()]
        [Editable(true)]
        [Column("material_number")]
        public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
                    _material_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the inv_pack_quantity.
        /// </summary>
        /// <value>
        /// The inv_pack_quantity.
        /// </value>
        private decimal? _inv_pack_quantity;

        /// <summary>
        /// Gets or sets the inv_pack_quantity.
        /// </summary>
        /// <value>
        /// The inv_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("inv_pack_quantity")]
        [Properties("inv_pack_quantity", "inv_pack_quantity", false, 4)]
        public decimal? inv_pack_quantity
        {
            get { return _inv_pack_quantity; }
            set
            {
                if (_inv_pack_quantity != value)
                {
                    PropertyChanged("inv_pack_quantity");
                    _inv_pack_quantity = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
        public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
                    _material_quantity = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
        public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
                    _material_price = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_details.
        /// </summary>
        /// <value>
        /// The invoice_details_details.
        /// </value>
        private string _invoice_details_details;

        /// <summary>
        /// Gets or sets the invoice_details_details.
        /// </summary>
        /// <value>
        /// The invoice_details_details.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_details")]
        public string invoice_details_details
        {
            get { return _invoice_details_details; }
            set
            {
                if (_invoice_details_details != value)
                {
                    PropertyChanged("invoice_details_details");
                    _invoice_details_details = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
        [Editable(true)]
        [Column("supplier_number")]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
                    _supplier_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
        public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
                    _material_discount_percent = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
        private decimal _material_price_after_discount;

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_discount")]
        public decimal material_price_after_discount
        {
            get { return _material_price_after_discount; }
            set
            {
                if (_material_price_after_discount != value)
                {
                    PropertyChanged("material_price_after_discount");
                    _material_price_after_discount = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
        public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
                    _quantity_within_invoice = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
        private long _packing_list_number;

        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_number")]
        public long packing_list_number
        {
            get { return _packing_list_number; }
            set
            {
                if (_packing_list_number != value)
                {
                    PropertyChanged("packing_list_number");
                    _packing_list_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the order_serial.
        /// </summary>
        /// <value>
        /// The order_serial.
        /// </value>
        private long _order_serial;

        /// <summary>
        /// Gets or sets the order_serial.
        /// </summary>
        /// <value>
        /// The order_serial.
        /// </value>
	    [Editable(true)]
        [Column("order_serial")]
        public long order_serial
        {
            get { return _order_serial; }
            set
            {
                if (_order_serial != value)
                {
                    PropertyChanged("order_serial");
                    _order_serial = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_stock_number.
        /// </summary>
        /// <value>
        /// The invoice_details_stock_number.
        /// </value>
        private long _invoice_details_stock_number;

        /// <summary>
        /// Gets or sets the invoice_details_stock_number.
        /// </summary>
        /// <value>
        /// The invoice_details_stock_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_stock_number")]
        public long invoice_details_stock_number
        {
            get { return _invoice_details_stock_number; }
            set
            {
                if (_invoice_details_stock_number != value)
                {
                    PropertyChanged("invoice_details_stock_number");
                    _invoice_details_stock_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
        public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
                    _sell_price = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
        public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
                    _bonus_quantity = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The invoice_details_material_bonus_connect.
        /// </value>
        private long _invoice_details_material_bonus_connect;

        /// <summary>
        /// Gets or sets the invoice_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The invoice_details_material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_bonus_connect")]
        public long invoice_details_material_bonus_connect
        {
            get { return _invoice_details_material_bonus_connect; }
            set
            {
                if (_invoice_details_material_bonus_connect != value)
                {
                    PropertyChanged("invoice_details_material_bonus_connect");
                    _invoice_details_material_bonus_connect = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The invoice_details_max_quantity_allowed.
        /// </value>
        private decimal _invoice_details_max_quantity_allowed;

        /// <summary>
        /// Gets or sets the invoice_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The invoice_details_max_quantity_allowed.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_max_quantity_allowed")]
        public decimal invoice_details_max_quantity_allowed
        {
            get { return _invoice_details_max_quantity_allowed; }
            set
            {
                if (_invoice_details_max_quantity_allowed != value)
                {
                    PropertyChanged("invoice_details_max_quantity_allowed");
                    _invoice_details_max_quantity_allowed = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_last_packing_change.
        /// </summary>
        /// <value>
        /// The invoice_details_last_packing_change.
        /// </value>
        private decimal _invoice_details_last_packing_change;

        /// <summary>
        /// Gets or sets the invoice_details_last_packing_change.
        /// </summary>
        /// <value>
        /// The invoice_details_last_packing_change.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_last_packing_change")]
        public decimal invoice_details_last_packing_change
        {
            get { return _invoice_details_last_packing_change; }
            set
            {
                if (_invoice_details_last_packing_change != value)
                {
                    PropertyChanged("invoice_details_last_packing_change");
                    _invoice_details_last_packing_change = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
        public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
                    _import_type = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_amount.
        /// </value>
        private decimal _invoice_details_material_discount_amount;

        /// <summary>
        /// Gets or sets the invoice_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_discount_amount")]
        public decimal invoice_details_material_discount_amount
        {
            get { return _invoice_details_material_discount_amount; }
            set
            {
                if (_invoice_details_material_discount_amount != value)
                {
                    PropertyChanged("invoice_details_material_discount_amount");
                    _invoice_details_material_discount_amount = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_bonus_row.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_row.
        /// </value>
        private string _invoice_details_bonus_row;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_row.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_row")]
        public string invoice_details_bonus_row
        {
            get { return _invoice_details_bonus_row; }
            set
            {
                if (_invoice_details_bonus_row != value)
                {
                    PropertyChanged("invoice_details_bonus_row");
                    _invoice_details_bonus_row = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
        public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
                    _color = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_pay_shape.
        /// </summary>
        /// <value>
        /// The invoice_details_pay_shape.
        /// </value>
        private long _invoice_details_pay_shape;

        /// <summary>
        /// Gets or sets the invoice_details_pay_shape.
        /// </summary>
        /// <value>
        /// The invoice_details_pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_pay_shape")]
        public long invoice_details_pay_shape
        {
            get { return _invoice_details_pay_shape; }
            set
            {
                if (_invoice_details_pay_shape != value)
                {
                    PropertyChanged("invoice_details_pay_shape");
                    _invoice_details_pay_shape = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        private long _indicator;

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
	    [Editable(true)]
        [Column("indicator")]
        public long indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    PropertyChanged("indicator");
                    _indicator = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
        private decimal _invoice_details_bonus_discount;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_discount")]
        public decimal invoice_details_bonus_discount
        {
            get { return _invoice_details_bonus_discount; }
            set
            {
                if (_invoice_details_bonus_discount != value)
                {
                    PropertyChanged("invoice_details_bonus_discount");
                    _invoice_details_bonus_discount = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
        private decimal _invoice_details_supplier_discount_percent;

        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_supplier_discount_percent")]
        public decimal invoice_details_supplier_discount_percent
        {
            get { return _invoice_details_supplier_discount_percent; }
            set
            {
                if (_invoice_details_supplier_discount_percent != value)
                {
                    PropertyChanged("invoice_details_supplier_discount_percent");
                    _invoice_details_supplier_discount_percent = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
        private decimal _current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("current_catalog_sell_price")]
        public decimal current_catalog_sell_price
        {
            get { return _current_catalog_sell_price; }
            set
            {
                if (_current_catalog_sell_price != value)
                {
                    PropertyChanged("current_catalog_sell_price");
                    _current_catalog_sell_price = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
        private DateTime? _expiration_date;

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("expiration_date")]
        public DateTime? expiration_date
        {
            get { return _expiration_date; }
            set
            {
                if (_expiration_date != value)
                {
                    PropertyChanged("expiration_date");
                    _expiration_date = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
        [Properties("expected_material_quantity", "expected_material_quantity", false, 3)]
        public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
                    _expected_material_quantity = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
        private long _mini_terminal;

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("mini_terminal")]
        public long mini_terminal
        {
            get { return _mini_terminal; }
            set
            {
                if (_mini_terminal != value)
                {
                    PropertyChanged("mini_terminal");
                    _mini_terminal = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
        [Properties("materials_barcode", "materials_barcode", false, 1)]
        public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
                    _materials_barcode = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
        private string _materials_material_name;

        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_material_name")]
        [Properties("materials_material_name", "materials_material_name", false, 2)]
        public string materials_material_name
        {
            get { return _materials_material_name; }
            set
            {
                if (_materials_material_name != value)
                {
                    PropertyChanged("materials_material_name");
                    _materials_material_name = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
        private long _materials_min_valid_months;

        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("materials_min_valid_months")]
        public long materials_min_valid_months
        {
            get { return _materials_min_valid_months; }
            set
            {
                if (_materials_min_valid_months != value)
                {
                    PropertyChanged("materials_min_valid_months");
                    _materials_min_valid_months = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
        public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
                    _order_quantity = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
        public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
                    _b2b_status = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the doc_state.
        /// </summary>
        /// <value>
        /// The doc_state.
        /// </value>
        private string _doc_state;

        /// <summary>
        /// Gets or sets the doc_state.
        /// </summary>
        /// <value>
        /// The doc_state.
        /// </value>
	    [Editable(true)]
        [Column("doc_state")]
        public string doc_state
        {
            get { return _doc_state; }
            set
            {
                if (_doc_state != value)
                {
                    PropertyChanged("doc_state");
                    _doc_state = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
        public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
                    _distributor_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
        private long _row_no;

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
	    [Editable(true)]
        [Column("row_no")]
        public long row_no
        {
            get { return _row_no; }
            set
            {
                if (_row_no != value)
                {
                    PropertyChanged("row_no");
                    _row_no = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
        private long _doc_no_en;

        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
	    [Editable(true)]
        [Column("doc_no_en")]
        public long doc_no_en
        {
            get { return _doc_no_en; }
            set
            {
                if (_doc_no_en != value)
                {
                    PropertyChanged("doc_no_en");
                    _doc_no_en = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the cng_doc_count.
        /// </summary>
        /// <value>
        /// The cng_doc_count.
        /// </value>
        private long _cng_doc_count;

        /// <summary>
        /// Gets or sets the cng_doc_count.
        /// </summary>
        /// <value>
        /// The cng_doc_count.
        /// </value>
	    [Editable(true)]
        [Column("cng_doc_count")]
        public long cng_doc_count
        {
            get { return _cng_doc_count; }
            set
            {
                if (_cng_doc_count != value)
                {
                    PropertyChanged("cng_doc_count");
                    _cng_doc_count = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
        private long _declines;

        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
	    [Editable(true)]
        [Column("declines")]
        public long declines
        {
            get { return _declines; }
            set
            {
                if (_declines != value)
                {
                    PropertyChanged("declines");
                    _declines = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
        public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
                    _state = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
        public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
                    _last_update_datetime = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the _invoice_data.
        /// </summary>
        /// <value>
        /// The _invoice_data.
        /// </value>
        private string _invoice_data;

        /// <summary>
        /// Gets or sets the invoice_data.
        /// </summary>
        /// <value>
        /// The invoice_data.
        /// </value>
	    [Editable(true)]
        [Column("invoice_data")]
        [Properties("invoice_data", "invoice_data", true, 90, typeof(string))]
        [PreventFlagChange()]
        public string invoice_data
        {
            get { return _invoice_data; }
            set
            {
                if (_invoice_data != value)
                {
                    _invoice_data = value;
                }
            }
        }


    }
}
