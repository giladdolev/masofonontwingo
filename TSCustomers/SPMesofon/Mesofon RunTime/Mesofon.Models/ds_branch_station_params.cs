using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class ds_branch_station_params : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public ds_branch_station_params()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="station_number">The station_number.</param>
		/// <param name="parameter_name">The parameter_name.</param>
		/// <param name="parameter_type">The parameter_type.</param>
		/// <param name="parameter_value">The parameter_value.</param>
		/// <param name="default_value">The default_value.</param>
		/// <param name="description">The description.</param>
		/// <param name="parameter_entity">The parameter_entity.</param>
        public ds_branch_station_params(long branch_number, long station_number, string parameter_name, string parameter_type, string parameter_value, string default_value, string description, long parameter_entity)
        {
			this._branch_number = branch_number;
			this._station_number = station_number;
			this._parameter_name = parameter_name;
			this._parameter_type = parameter_type;
			this._parameter_value = parameter_value;
			this._default_value = default_value;
			this._description = description;
			this._parameter_entity = parameter_entity;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the station_number.
        /// </summary>
        /// <value>
        /// The station_number.
        /// </value>
        private long _station_number;

        /// <summary>
        /// Gets or sets the station_number.
        /// </summary>
        /// <value>
        /// The station_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("station_number")]
		public long station_number
        {
            get { return _station_number; }
            set
            {
                if (_station_number != value)
                {
                    PropertyChanged("station_number");
					_station_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parameter_name.
        /// </summary>
        /// <value>
        /// The parameter_name.
        /// </value>
        private string _parameter_name;

        /// <summary>
        /// Gets or sets the parameter_name.
        /// </summary>
        /// <value>
        /// The parameter_name.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("parameter_name")]
		public string parameter_name
        {
            get { return _parameter_name; }
            set
            {
                if (_parameter_name != value)
                {
                    PropertyChanged("parameter_name");
					_parameter_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parameter_type.
        /// </summary>
        /// <value>
        /// The parameter_type.
        /// </value>
        private string _parameter_type;

        /// <summary>
        /// Gets or sets the parameter_type.
        /// </summary>
        /// <value>
        /// The parameter_type.
        /// </value>
	    [Editable(true)]
        [Column("parameter_type")]
		public string parameter_type
        {
            get { return _parameter_type; }
            set
            {
                if (_parameter_type != value)
                {
                    PropertyChanged("parameter_type");
					_parameter_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parameter_value.
        /// </summary>
        /// <value>
        /// The parameter_value.
        /// </value>
        private string _parameter_value;

        /// <summary>
        /// Gets or sets the parameter_value.
        /// </summary>
        /// <value>
        /// The parameter_value.
        /// </value>
	    [Editable(true)]
        [Column("parameter_value")]
		public string parameter_value
        {
            get { return _parameter_value; }
            set
            {
                if (_parameter_value != value)
                {
                    PropertyChanged("parameter_value");
					_parameter_value = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the default_value.
        /// </summary>
        /// <value>
        /// The default_value.
        /// </value>
        private string _default_value;

        /// <summary>
        /// Gets or sets the default_value.
        /// </summary>
        /// <value>
        /// The default_value.
        /// </value>
	    [Editable(true)]
        [Column("default_value")]
		public string default_value
        {
            get { return _default_value; }
            set
            {
                if (_default_value != value)
                {
                    PropertyChanged("default_value");
					_default_value = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        private string _description;

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
	    [Editable(true)]
        [Column("description")]
		public string description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    PropertyChanged("description");
					_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parameter_entity.
        /// </summary>
        /// <value>
        /// The parameter_entity.
        /// </value>
        private long _parameter_entity;

        /// <summary>
        /// Gets or sets the parameter_entity.
        /// </summary>
        /// <value>
        /// The parameter_entity.
        /// </value>
	    [Editable(true)]
        [Column("parameter_entity")]
		public long parameter_entity
        {
            get { return _parameter_entity; }
            set
            {
                if (_parameter_entity != value)
                {
                    PropertyChanged("parameter_entity");
					_parameter_entity = value;
                    
                }
            }
        }
	}
}
