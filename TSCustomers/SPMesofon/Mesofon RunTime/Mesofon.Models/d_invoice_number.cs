﻿using Common.Transposition.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesofon.Models
{
    public class d_invoice_number: ModelBase
    {
        public d_invoice_number()
        {

        }
        public d_invoice_number(string doc_type,long doc_number, int b2b_status)
        {
            this._doc_type = doc_type;
            this._doc_number = doc_number;
            this._b2b_status = b2b_status;
        }

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
                    _doc_type = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
        private long _doc_number;

        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
        public long doc_number
        {
            get { return _doc_number; }
            set
            {
                if (_doc_number != value)
                {
                    PropertyChanged("doc_number");
                    _doc_number = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private int _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        public int b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
                    _b2b_status = value;
                }
            }
        }
    }
}
