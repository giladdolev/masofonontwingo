using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class dddw_param_values : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dddw_param_values()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="param_name">The param_name.</param>
		/// <param name="param_value">The param_value.</param>
        public dddw_param_values(string param_name, string param_value)
        {
			this._param_name = param_name;
			this._param_value = param_value;
		}

	
        /// <summary>
        /// Gets or sets the param_name.
        /// </summary>
        /// <value>
        /// The param_name.
        /// </value>
        private string _param_name;

        /// <summary>
        /// Gets or sets the param_name.
        /// </summary>
        /// <value>
        /// The param_name.
        /// </value>
	    [Editable(true)]
        [Column("param_name")]
        [PropertiesAttribute("param_name", "param_name", true, 1)]
        public string param_name
        {
            get { return _param_name; }
            set
            {
                if (_param_name != value)
                {
                    PropertyChanged("param_name");
					_param_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the param_value.
        /// </summary>
        /// <value>
        /// The param_value.
        /// </value>
        private string _param_value;

        /// <summary>
        /// Gets or sets the param_value.
        /// </summary>
        /// <value>
        /// The param_value.
        /// </value>
	    [Editable(true)]
        [Column("param_value")]
        [PropertiesAttribute("param_value", "param_value", true, 0)]
        public string param_value
        {
            get { return _param_value; }
            set
            {
                if (_param_value != value)
                {
                    PropertyChanged("param_value");
					_param_value = value;
                    
                }
            }
        }
	}
}
