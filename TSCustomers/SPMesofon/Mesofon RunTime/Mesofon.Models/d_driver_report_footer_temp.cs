using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_driver_report_footer_temp : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_driver_report_footer_temp()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="item_barcode">The item_barcode.</param>
		/// <param name="original_quantity">The original_quantity.</param>
		/// <param name="actual_quantity">The actual_quantity.</param>
		/// <param name="display_name">The display_name.</param>
		/// <param name="description">The description.</param>
		/// <param name="parent_doc_number">The parent_doc_number.</param>
		/// <param name="rejected_quantity">The rejected_quantity.</param>
        public d_driver_report_footer_temp(string item_barcode, decimal original_quantity, decimal actual_quantity, string display_name, string description, long parent_doc_number, string rejected_quantity)
        {
			this._item_barcode = item_barcode;
			this._original_quantity = original_quantity;
			this._actual_quantity = actual_quantity;
			this._display_name = display_name;
			this._description = description;
			this._parent_doc_number = parent_doc_number;
			this._rejected_quantity = rejected_quantity;
		}

	
        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
        private string _item_barcode;

        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
	    [Editable(true)]
        [Column("item_barcode")]
        [Properties("item_barcode", "item_barcode",true,0,typeof(string))]
		public string item_barcode
        {
            get { return _item_barcode; }
            set
            {
                if (_item_barcode != value)
                {
                    PropertyChanged("item_barcode");
					_item_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the original_quantity.
        /// </summary>
        /// <value>
        /// The original_quantity.
        /// </value>
        private decimal _original_quantity;

        /// <summary>
        /// Gets or sets the original_quantity.
        /// </summary>
        /// <value>
        /// The original_quantity.
        /// </value>
	    [Editable(true)]
        [Column("original_quantity")]
        [Properties("original_quantity", "original_quantity", true, 4, typeof(decimal))]
        public decimal original_quantity
        {
            get { return _original_quantity; }
            set
            {
                if (_original_quantity != value)
                {
                    PropertyChanged("original_quantity");
					_original_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
        private decimal _actual_quantity;

        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
	    [Editable(true)]
        [Column("actual_quantity")]
        [Properties("actual_quantity", "actual_quantity", true, 5, typeof(decimal))]
        public decimal actual_quantity
        {
            get { return _actual_quantity; }
            set
            {
                if (_actual_quantity != value)
                {
                    PropertyChanged("actual_quantity");
					_actual_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
        private string _display_name;

        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
	    [Editable(true)]
        [Column("display_name")]
        [Properties("display_name", "display_name", true, 3, typeof(long))]
        public string display_name
        {
            get { return _display_name; }
            set
            {
                if (_display_name != value)
                {
                    PropertyChanged("display_name");
					_display_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        private string _description;

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
	    [Editable(true)]
        [Column("description")]
        [Properties("description", "description", true, 1, typeof(string))]
        public string description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    PropertyChanged("description");
					_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parent_doc_number.
        /// </summary>
        /// <value>
        /// The parent_doc_number.
        /// </value>
        private long _parent_doc_number;

        /// <summary>
        /// Gets or sets the parent_doc_number.
        /// </summary>
        /// <value>
        /// The parent_doc_number.
        /// </value>
	    [Editable(true)]
        [Column("parent_doc_number")]
        [Properties("parent_doc_number", "parent_doc_number", true, 2, typeof(long))]
        public long parent_doc_number
        {
            get { return _parent_doc_number; }
            set
            {
                if (_parent_doc_number != value)
                {
                    PropertyChanged("parent_doc_number");
					_parent_doc_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the rejected_quantity.
        /// </summary>
        /// <value>
        /// The rejected_quantity.
        /// </value>
        private string _rejected_quantity;

        /// <summary>
        /// Gets or sets the rejected_quantity.
        /// </summary>
        /// <value>
        /// The rejected_quantity.
        /// </value>
	    [Editable(true)]
        [Column("rejected_quantity")]
        [Properties("rejected_quantity", "rejected_quantity", true, 6, typeof(string))]
        [Compute("rejected_quantity" , "SUB actual_quantity original_quantity")]
        public string rejected_quantity
        {
            get { return _rejected_quantity; }
            set
            {
                if (_rejected_quantity != value)
                {
                    PropertyChanged("rejected_quantity");
					_rejected_quantity = value;
                    
                }
            }
        }
	}
}
