using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_pallet_b2b_invoice : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_pallet_b2b_invoice()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="b2b_invoice_details_branch_number">The b2b_invoice_details_branch_number.</param>
		/// <param name="b2b_invoice_details_supplier_number">The b2b_invoice_details_supplier_number.</param>
		/// <param name="b2b_invoice_details_order_number">The b2b_invoice_details_order_number.</param>
		/// <param name="b2b_invoice_details_invoice_number">The b2b_invoice_details_invoice_number.</param>
		/// <param name="b2b_invoice_details_serial_number">The b2b_invoice_details_serial_number.</param>
		/// <param name="b2b_invoice_details_line_number">The b2b_invoice_details_line_number.</param>
		/// <param name="b2b_invoice_details_upd_date_time">The b2b_invoice_details_upd_date_time.</param>
		/// <param name="b2b_invoice_details_status">The b2b_invoice_details_status.</param>
		/// <param name="b2b_invoice_details_item_barcode">The b2b_invoice_details_item_barcode.</param>
		/// <param name="b2b_invoice_details_item_number">The b2b_invoice_details_item_number.</param>
		/// <param name="b2b_invoice_details_units_qty">The b2b_invoice_details_units_qty.</param>
		/// <param name="b2b_invoice_details_item_bonus_code">The b2b_invoice_details_item_bonus_code.</param>
		/// <param name="b2b_invoice_details_bonus_line">The b2b_invoice_details_bonus_line.</param>
		/// <param name="b2b_invoice_details_bonus_description">The b2b_invoice_details_bonus_description.</param>
		/// <param name="b2b_invoice_details_packages_quantity">The b2b_invoice_details_packages_quantity.</param>
		/// <param name="b2b_invoice_details_validity_date">The b2b_invoice_details_validity_date.</param>
		/// <param name="b2b_invoice_details_pack_quantity">The b2b_invoice_details_pack_quantity.</param>
		/// <param name="b2b_invoice_details_pallet_number">The b2b_invoice_details_pallet_number.</param>
		/// <param name="b2b_invoice_move_invoice_type">The b2b_invoice_move_invoice_type.</param>
		/// <param name="b2b_invoice_details_scanned_quantity">The b2b_invoice_details_scanned_quantity.</param>
        public d_mini_terminal_pallet_b2b_invoice(long b2b_invoice_details_branch_number, long b2b_invoice_details_supplier_number, long b2b_invoice_details_order_number, long b2b_invoice_details_invoice_number, long b2b_invoice_details_serial_number, long b2b_invoice_details_line_number, DateTime? b2b_invoice_details_upd_date_time, string b2b_invoice_details_status, string b2b_invoice_details_item_barcode, long b2b_invoice_details_item_number, decimal b2b_invoice_details_units_qty, string b2b_invoice_details_item_bonus_code, long b2b_invoice_details_bonus_line, string b2b_invoice_details_bonus_description, decimal b2b_invoice_details_packages_quantity, DateTime? b2b_invoice_details_validity_date, double b2b_invoice_details_pack_quantity, string b2b_invoice_details_pallet_number, string b2b_invoice_move_invoice_type, decimal b2b_invoice_details_scanned_quantity)
        {
			this._b2b_invoice_details_branch_number = b2b_invoice_details_branch_number;
			this._b2b_invoice_details_supplier_number = b2b_invoice_details_supplier_number;
			this._b2b_invoice_details_order_number = b2b_invoice_details_order_number;
			this._b2b_invoice_details_invoice_number = b2b_invoice_details_invoice_number;
			this._b2b_invoice_details_serial_number = b2b_invoice_details_serial_number;
			this._b2b_invoice_details_line_number = b2b_invoice_details_line_number;
			this._b2b_invoice_details_upd_date_time = b2b_invoice_details_upd_date_time;
			this._b2b_invoice_details_status = b2b_invoice_details_status;
			this._b2b_invoice_details_item_barcode = b2b_invoice_details_item_barcode;
			this._b2b_invoice_details_item_number = b2b_invoice_details_item_number;
			this._b2b_invoice_details_units_qty = b2b_invoice_details_units_qty;
			this._b2b_invoice_details_item_bonus_code = b2b_invoice_details_item_bonus_code;
			this._b2b_invoice_details_bonus_line = b2b_invoice_details_bonus_line;
			this._b2b_invoice_details_bonus_description = b2b_invoice_details_bonus_description;
			this._b2b_invoice_details_packages_quantity = b2b_invoice_details_packages_quantity;
			this._b2b_invoice_details_validity_date = b2b_invoice_details_validity_date;
			this._b2b_invoice_details_pack_quantity = b2b_invoice_details_pack_quantity;
			this._b2b_invoice_details_pallet_number = b2b_invoice_details_pallet_number;
			this._b2b_invoice_move_invoice_type = b2b_invoice_move_invoice_type;
			this._b2b_invoice_details_scanned_quantity = b2b_invoice_details_scanned_quantity;
		}

	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_branch_number.
        /// </value>
        private long _b2b_invoice_details_branch_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_branch_number")]
		public long b2b_invoice_details_branch_number
        {
            get { return _b2b_invoice_details_branch_number; }
            set
            {
                if (_b2b_invoice_details_branch_number != value)
                {
                    PropertyChanged("b2b_invoice_details_branch_number");
					_b2b_invoice_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_supplier_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_supplier_number.
        /// </value>
        private long _b2b_invoice_details_supplier_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_supplier_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_supplier_number")]
		public long b2b_invoice_details_supplier_number
        {
            get { return _b2b_invoice_details_supplier_number; }
            set
            {
                if (_b2b_invoice_details_supplier_number != value)
                {
                    PropertyChanged("b2b_invoice_details_supplier_number");
					_b2b_invoice_details_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_order_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_order_number.
        /// </value>
        private long _b2b_invoice_details_order_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_order_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_order_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_order_number")]
		public long b2b_invoice_details_order_number
        {
            get { return _b2b_invoice_details_order_number; }
            set
            {
                if (_b2b_invoice_details_order_number != value)
                {
                    PropertyChanged("b2b_invoice_details_order_number");
					_b2b_invoice_details_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_invoice_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_invoice_number.
        /// </value>
        private long _b2b_invoice_details_invoice_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_invoice_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_invoice_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_invoice_number")]
		public long b2b_invoice_details_invoice_number
        {
            get { return _b2b_invoice_details_invoice_number; }
            set
            {
                if (_b2b_invoice_details_invoice_number != value)
                {
                    PropertyChanged("b2b_invoice_details_invoice_number");
					_b2b_invoice_details_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_serial_number.
        /// </value>
        private long _b2b_invoice_details_serial_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_serial_number")]
		public long b2b_invoice_details_serial_number
        {
            get { return _b2b_invoice_details_serial_number; }
            set
            {
                if (_b2b_invoice_details_serial_number != value)
                {
                    PropertyChanged("b2b_invoice_details_serial_number");
					_b2b_invoice_details_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_line_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_line_number.
        /// </value>
        private long _b2b_invoice_details_line_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_line_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_line_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_line_number")]
		public long b2b_invoice_details_line_number
        {
            get { return _b2b_invoice_details_line_number; }
            set
            {
                if (_b2b_invoice_details_line_number != value)
                {
                    PropertyChanged("b2b_invoice_details_line_number");
					_b2b_invoice_details_line_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_upd_date_time.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_upd_date_time.
        /// </value>
        private DateTime? _b2b_invoice_details_upd_date_time;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_upd_date_time.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_upd_date_time.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_upd_date_time")]
		public DateTime? b2b_invoice_details_upd_date_time
        {
            get { return _b2b_invoice_details_upd_date_time; }
            set
            {
                if (_b2b_invoice_details_upd_date_time != value)
                {
                    PropertyChanged("b2b_invoice_details_upd_date_time");
					_b2b_invoice_details_upd_date_time = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_status.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_status.
        /// </value>
        private string _b2b_invoice_details_status;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_status.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_status")]
		public string b2b_invoice_details_status
        {
            get { return _b2b_invoice_details_status; }
            set
            {
                if (_b2b_invoice_details_status != value)
                {
                    PropertyChanged("b2b_invoice_details_status");
					_b2b_invoice_details_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_barcode.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_barcode.
        /// </value>
        private string _b2b_invoice_details_item_barcode;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_barcode.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_barcode.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_item_barcode")]
		public string b2b_invoice_details_item_barcode
        {
            get { return _b2b_invoice_details_item_barcode; }
            set
            {
                if (_b2b_invoice_details_item_barcode != value)
                {
                    PropertyChanged("b2b_invoice_details_item_barcode");
					_b2b_invoice_details_item_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_number.
        /// </value>
        private long _b2b_invoice_details_item_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_item_number")]
		public long b2b_invoice_details_item_number
        {
            get { return _b2b_invoice_details_item_number; }
            set
            {
                if (_b2b_invoice_details_item_number != value)
                {
                    PropertyChanged("b2b_invoice_details_item_number");
					_b2b_invoice_details_item_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_units_qty.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_units_qty.
        /// </value>
        private decimal _b2b_invoice_details_units_qty;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_units_qty.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_units_qty.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_units_qty")]
		public decimal b2b_invoice_details_units_qty
        {
            get { return _b2b_invoice_details_units_qty; }
            set
            {
                if (_b2b_invoice_details_units_qty != value)
                {
                    PropertyChanged("b2b_invoice_details_units_qty");
					_b2b_invoice_details_units_qty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_bonus_code.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_bonus_code.
        /// </value>
        private string _b2b_invoice_details_item_bonus_code;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_item_bonus_code.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_item_bonus_code.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_item_bonus_code")]
		public string b2b_invoice_details_item_bonus_code
        {
            get { return _b2b_invoice_details_item_bonus_code; }
            set
            {
                if (_b2b_invoice_details_item_bonus_code != value)
                {
                    PropertyChanged("b2b_invoice_details_item_bonus_code");
					_b2b_invoice_details_item_bonus_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_bonus_line.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_bonus_line.
        /// </value>
        private long _b2b_invoice_details_bonus_line;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_bonus_line.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_bonus_line.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_bonus_line")]
		public long b2b_invoice_details_bonus_line
        {
            get { return _b2b_invoice_details_bonus_line; }
            set
            {
                if (_b2b_invoice_details_bonus_line != value)
                {
                    PropertyChanged("b2b_invoice_details_bonus_line");
					_b2b_invoice_details_bonus_line = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_bonus_description.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_bonus_description.
        /// </value>
        private string _b2b_invoice_details_bonus_description;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_bonus_description.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_bonus_description.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_bonus_description")]
		public string b2b_invoice_details_bonus_description
        {
            get { return _b2b_invoice_details_bonus_description; }
            set
            {
                if (_b2b_invoice_details_bonus_description != value)
                {
                    PropertyChanged("b2b_invoice_details_bonus_description");
					_b2b_invoice_details_bonus_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_packages_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_packages_quantity.
        /// </value>
        private decimal _b2b_invoice_details_packages_quantity;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_packages_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_packages_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_packages_quantity")]
		public decimal b2b_invoice_details_packages_quantity
        {
            get { return _b2b_invoice_details_packages_quantity; }
            set
            {
                if (_b2b_invoice_details_packages_quantity != value)
                {
                    PropertyChanged("b2b_invoice_details_packages_quantity");
					_b2b_invoice_details_packages_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_validity_date.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_validity_date.
        /// </value>
        private DateTime? _b2b_invoice_details_validity_date;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_validity_date.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_validity_date.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_validity_date")]
		public DateTime? b2b_invoice_details_validity_date
        {
            get { return _b2b_invoice_details_validity_date; }
            set
            {
                if (_b2b_invoice_details_validity_date != value)
                {
                    PropertyChanged("b2b_invoice_details_validity_date");
					_b2b_invoice_details_validity_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_pack_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_pack_quantity.
        /// </value>
        private double _b2b_invoice_details_pack_quantity;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_pack_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_pack_quantity")]
		public double b2b_invoice_details_pack_quantity
        {
            get { return _b2b_invoice_details_pack_quantity; }
            set
            {
                if (_b2b_invoice_details_pack_quantity != value)
                {
                    PropertyChanged("b2b_invoice_details_pack_quantity");
					_b2b_invoice_details_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_pallet_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_pallet_number.
        /// </value>
        private string _b2b_invoice_details_pallet_number;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_pallet_number.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_pallet_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("b2b_invoice_details_pallet_number")]
		public string b2b_invoice_details_pallet_number
        {
            get { return _b2b_invoice_details_pallet_number; }
            set
            {
                if (_b2b_invoice_details_pallet_number != value)
                {
                    PropertyChanged("b2b_invoice_details_pallet_number");
					_b2b_invoice_details_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The b2b_invoice_move_invoice_type.
        /// </value>
        private string _b2b_invoice_move_invoice_type;

        /// <summary>
        /// Gets or sets the b2b_invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The b2b_invoice_move_invoice_type.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_move_invoice_type")]
		public string b2b_invoice_move_invoice_type
        {
            get { return _b2b_invoice_move_invoice_type; }
            set
            {
                if (_b2b_invoice_move_invoice_type != value)
                {
                    PropertyChanged("b2b_invoice_move_invoice_type");
					_b2b_invoice_move_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_invoice_details_scanned_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_scanned_quantity.
        /// </value>
        private decimal _b2b_invoice_details_scanned_quantity;

        /// <summary>
        /// Gets or sets the b2b_invoice_details_scanned_quantity.
        /// </summary>
        /// <value>
        /// The b2b_invoice_details_scanned_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_invoice_details_scanned_quantity")]
		public decimal b2b_invoice_details_scanned_quantity
        {
            get { return _b2b_invoice_details_scanned_quantity; }
            set
            {
                if (_b2b_invoice_details_scanned_quantity != value)
                {
                    PropertyChanged("b2b_invoice_details_scanned_quantity");
					_b2b_invoice_details_scanned_quantity = value;
                    
                }
            }
        }
	}
}
