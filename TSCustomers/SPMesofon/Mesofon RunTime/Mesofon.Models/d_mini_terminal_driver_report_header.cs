using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
    [Compute("compute_pallets_number", "rowcount() ")]
    [Compute("compute_pack_quantity", "SUM shipment_pallets_pack_quantity")]
    public class d_mini_terminal_driver_report_header : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_driver_report_header()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="shipment_pallets_date_move">The shipment_pallets_date_move.</param>
		/// <param name="shipment_pallets_branch_number">The shipment_pallets_branch_number.</param>
		/// <param name="shipment_pallets_shipment_number">The shipment_pallets_shipment_number.</param>
		/// <param name="shipment_pallets_pallet_number">The shipment_pallets_pallet_number.</param>
		/// <param name="shipment_pallets_pack_quantity">The shipment_pallets_pack_quantity.</param>
		/// <param name="shipment_pallets_arrive_datetime">The shipment_pallets_arrive_datetime.</param>
		/// <param name="shipment_pallets_arrive_time">The shipment_pallets_arrive_time.</param>
		/// <param name="shipment_pallets_auto_scan_status">The shipment_pallets_auto_scan_status.</param>
		/// <param name="global_parameters_branch_name">The global_parameters_branch_name.</param>
		/// <param name="scan_time">The scan_time.</param>
		/// <param name="print_date">The print_date.</param>
		/// <param name="crossdoc_quantity">The crossdoc_quantity.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="compute_pallets_number">The compute_pallets_number.</param>
		/// <param name="compute_pack_quantity">The compute_pack_quantity.</param>
        public d_mini_terminal_driver_report_header(DateTime shipment_pallets_date_move, long shipment_pallets_branch_number, double shipment_pallets_shipment_number, string shipment_pallets_pallet_number, long shipment_pallets_pack_quantity, string shipment_pallets_arrive_datetime, string shipment_pallets_arrive_time, long shipment_pallets_auto_scan_status, string global_parameters_branch_name, string scan_time, string print_date, long crossdoc_quantity, string employee_name, string compute_pallets_number, string compute_pack_quantity)
        {
			this._shipment_pallets_date_move = shipment_pallets_date_move;
			this._shipment_pallets_branch_number = shipment_pallets_branch_number;
			this._shipment_pallets_shipment_number = shipment_pallets_shipment_number;
			this._shipment_pallets_pallet_number = shipment_pallets_pallet_number;
			this._shipment_pallets_pack_quantity = shipment_pallets_pack_quantity;
			this._shipment_pallets_arrive_datetime = shipment_pallets_arrive_datetime;
			this._shipment_pallets_arrive_time = shipment_pallets_arrive_time;
			this._shipment_pallets_auto_scan_status = shipment_pallets_auto_scan_status;
			this._global_parameters_branch_name = global_parameters_branch_name;
			this._scan_time = scan_time;
			this._print_date = print_date;
			this._crossdoc_quantity = crossdoc_quantity;
			this._employee_name = employee_name;
			this._compute_pallets_number = compute_pallets_number;
			this._compute_pack_quantity = compute_pack_quantity;
		}

	
        /// <summary>
        /// Gets or sets the shipment_pallets_date_move.
        /// </summary>
        /// <value>
        /// The shipment_pallets_date_move.
        /// </value>
        private DateTime? _shipment_pallets_date_move;

        /// <summary>
        /// Gets or sets the shipment_pallets_date_move.
        /// </summary>
        /// <value>
        /// The shipment_pallets_date_move.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_date_move")]
		public DateTime? shipment_pallets_date_move
        {
            get { return _shipment_pallets_date_move; }
            set
            {
                if (_shipment_pallets_date_move != value)
                {
                    PropertyChanged("shipment_pallets_date_move");
					_shipment_pallets_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_branch_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_branch_number.
        /// </value>
        private long _shipment_pallets_branch_number;

        /// <summary>
        /// Gets or sets the shipment_pallets_branch_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_branch_number")]
        [Properties("shipment_pallets_branch_number", "�� ����",true,0,typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]
		public long shipment_pallets_branch_number
        {
            get { return _shipment_pallets_branch_number; }
            set
            {
                if (_shipment_pallets_branch_number != value)
                {
                    PropertyChanged("shipment_pallets_branch_number");
					_shipment_pallets_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_shipment_number.
        /// </value>
        private double _shipment_pallets_shipment_number;

        /// <summary>
        /// Gets or sets the shipment_pallets_shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_shipment_number")]
        [Properties("shipment_pallets_shipment_number", "���� �����", true, 1, typeof(double))]
        [XMLOutput(XMLOutputLocation.Header)]
        public double shipment_pallets_shipment_number
        {
            get { return _shipment_pallets_shipment_number; }
            set
            {
                if (_shipment_pallets_shipment_number != value)
                {
                    PropertyChanged("shipment_pallets_shipment_number");
					_shipment_pallets_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pallet_number.
        /// </value>
        private string _shipment_pallets_pallet_number;

        /// <summary>
        /// Gets or sets the shipment_pallets_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pallet_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_pallet_number")]
        [Properties("shipment_pallets_pallet_number", "���� ����", true, 2, typeof(string))]
        public string shipment_pallets_pallet_number
        {
            get { return _shipment_pallets_pallet_number; }
            set
            {
                if (_shipment_pallets_pallet_number != value)
                {
                    PropertyChanged("shipment_pallets_pallet_number");
					_shipment_pallets_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_pack_quantity.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pack_quantity.
        /// </value>
        private long _shipment_pallets_pack_quantity;

        /// <summary>
        /// Gets or sets the shipment_pallets_pack_quantity.
        /// </summary>
        /// <value>
        /// The shipment_pallets_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_pack_quantity")]
        [Properties("shipment_pallets_pack_quantity", "���� �������", true, 3, typeof(long))]
        public long shipment_pallets_pack_quantity
        {
            get { return _shipment_pallets_pack_quantity; }
            set
            {
                if (_shipment_pallets_pack_quantity != value)
                {
                    PropertyChanged("shipment_pallets_pack_quantity");
					_shipment_pallets_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_datetime.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_datetime.
        /// </value>
        private string _shipment_pallets_arrive_datetime;

        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_datetime.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_datetime.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_arrive_datetime")]
        [Properties("shipment_pallets_arrive_datetime", "�����", true, 4, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string shipment_pallets_arrive_datetime
        {
            get { return _shipment_pallets_arrive_datetime; }
            set
            {
                if (_shipment_pallets_arrive_datetime != value)
                {
                    PropertyChanged("shipment_pallets_arrive_datetime");
					_shipment_pallets_arrive_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_time.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_time.
        /// </value>
        private string _shipment_pallets_arrive_time;

        /// <summary>
        /// Gets or sets the shipment_pallets_arrive_time.
        /// </summary>
        /// <value>
        /// The shipment_pallets_arrive_time.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_arrive_time")]
        [Properties("shipment_pallets_arrive_time", "shipment_pallets_arrive_time", true, 5, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string shipment_pallets_arrive_time
        {
            get { return _shipment_pallets_arrive_time; }
            set
            {
                if (_shipment_pallets_arrive_time != value)
                {
                    PropertyChanged("shipment_pallets_arrive_time");
					_shipment_pallets_arrive_time = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallets_auto_scan_status.
        /// </summary>
        /// <value>
        /// The shipment_pallets_auto_scan_status.
        /// </value>
        private long _shipment_pallets_auto_scan_status;

        /// <summary>
        /// Gets or sets the shipment_pallets_auto_scan_status.
        /// </summary>
        /// <value>
        /// The shipment_pallets_auto_scan_status.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallets_auto_scan_status")]
        [Properties("shipment_pallets_auto_scan_status", "��� ���� ����", true, 6, typeof(string))]
        [TextExpressionAttribute("shipment_pallets_auto_scan_status = 0","��", "��")]
        public long shipment_pallets_auto_scan_status
        {
            get { return _shipment_pallets_auto_scan_status; }
            set
            {
                if (_shipment_pallets_auto_scan_status != value)
                {
                    PropertyChanged("shipment_pallets_auto_scan_status");
					_shipment_pallets_auto_scan_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the global_parameters_branch_name.
        /// </summary>
        /// <value>
        /// The global_parameters_branch_name.
        /// </value>
        private string _global_parameters_branch_name;

        /// <summary>
        /// Gets or sets the global_parameters_branch_name.
        /// </summary>
        /// <value>
        /// The global_parameters_branch_name.
        /// </value>
	    [Editable(true)]
        [Column("global_parameters_branch_name")]
        [Properties("global_parameters_branch_name", "�� ����", true, 7, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string global_parameters_branch_name
        {
            get { return _global_parameters_branch_name; }
            set
            {
                if (_global_parameters_branch_name != value)
                {
                    PropertyChanged("global_parameters_branch_name");
					_global_parameters_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the scan_time.
        /// </summary>
        /// <value>
        /// The scan_time.
        /// </value>
        private string _scan_time;

        /// <summary>
        /// Gets or sets the scan_time.
        /// </summary>
        /// <value>
        /// The scan_time.
        /// </value>
	    [Editable(true)]
        [Column("scan_time")]
        [Properties("scan_time", "scan_time", true, 8, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string scan_time
        {
            get { return _scan_time; }
            set
            {
                if (_scan_time != value)
                {
                    PropertyChanged("scan_time");
					_scan_time = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the print_date.
        /// </summary>
        /// <value>
        /// The print_date.
        /// </value>
        private string _print_date;

        /// <summary>
        /// Gets or sets the print_date.
        /// </summary>
        /// <value>
        /// The print_date.
        /// </value>
	    [Editable(true)]
        [Column("print_date")]
        [Properties("print_date", "print_date", true, 9, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string print_date
        {
            get { return _print_date; }
            set
            {
                if (_print_date != value)
                {
                    PropertyChanged("print_date");
					_print_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the crossdoc_quantity.
        /// </summary>
        /// <value>
        /// The crossdoc_quantity.
        /// </value>
        private long _crossdoc_quantity;

        /// <summary>
        /// Gets or sets the crossdoc_quantity.
        /// </summary>
        /// <value>
        /// The crossdoc_quantity.
        /// </value>
	    [Editable(true)]
        [Column("crossdoc_quantity")]
        [Properties("crossdoc_quantity", "crossdoc_quantity", true, 10, typeof(long))]
        public long crossdoc_quantity
        {
            get { return _crossdoc_quantity; }
            set
            {
                if (_crossdoc_quantity != value)
                {
                    PropertyChanged("crossdoc_quantity");
					_crossdoc_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
        [Properties("employee_name", "employee_name", true, 11, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_pallets_number.
        /// </summary>
        /// <value>
        /// The compute_pallets_number.
        /// </value>
        private object _compute_pallets_number;

        /// <summary>
        /// Gets or sets the compute_pallets_number.
        /// </summary>
        /// <value>
        /// The compute_pallets_number.
        /// </value>
	    [Editable(true)]
        [Column("compute_pallets_number")]
        [Properties("compute_pallets_number", "compute_pallets_number", true, 12, typeof(object))]
        [XMLOutput(XMLOutputLocation.Summary)]
        public object compute_pallets_number
        {
            get { return _compute_pallets_number; }
            set
            {
                if (_compute_pallets_number != value)
                {
                    PropertyChanged("compute_pallets_number");
					_compute_pallets_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_pack_quantity.
        /// </summary>
        /// <value>
        /// The compute_pack_quantity.
        /// </value>
        private object _compute_pack_quantity;

        /// <summary>
        /// Gets or sets the compute_pack_quantity.
        /// </summary>
        /// <value>
        /// The compute_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("compute_pack_quantity")]
        [Properties("compute_pack_quantity", "compute_pack_quantity", true, 13, typeof(object))]
        [XMLOutput(XMLOutputLocation.Summary)]
        public object compute_pack_quantity
        {
            get { return _compute_pack_quantity; }
            set
            {
                if (_compute_pack_quantity != value)
                {
                    PropertyChanged("compute_pack_quantity");
					_compute_pack_quantity = value;
                    
                }
            }
        }
	}
}
