using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class dddw_mini_terminal_crossdoc_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dddw_mini_terminal_crossdoc_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="date_move">The date_move.</param>
		/// <param name="shipment_pallet_number">The shipment_pallet_number.</param>
        public dddw_mini_terminal_crossdoc_list(string date_move, double shipment_pallet_number)
        {
			this._date_move = date_move;
			this._shipment_pallet_number = shipment_pallet_number;
		}

	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private string _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
        [PropertiesAttribute("date_move", "date_move",true,1,typeof(string))]

        public string date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallet_number.
        /// </value>
        private double _shipment_pallet_number;

        /// <summary>
        /// Gets or sets the shipment_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallet_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_pallet_number")]
        [PropertiesAttribute("shipment_pallet_number", "shipment_pallet_number", true, 0, typeof(double))]
        public double shipment_pallet_number
        {
            get { return _shipment_pallet_number; }
            set
            {
                if (_shipment_pallet_number != value)
                {
                    PropertyChanged("shipment_pallet_number");
					_shipment_pallet_number = value;
                    
                }
            }
        }
	}
}
