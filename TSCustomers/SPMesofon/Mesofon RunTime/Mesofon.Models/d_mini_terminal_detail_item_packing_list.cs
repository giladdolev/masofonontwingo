using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_detail_item_packing_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_detail_item_packing_list()
        {
            this._doc_no_en = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="packing_list_details_serial_number">The packing_list_details_serial_number.</param>
		/// <param name="packing_list_details_branch_number">The packing_list_details_branch_number.</param>
		/// <param name="pack_list_number">The pack_list_number.</param>
		/// <param name="packing_list_details_material_number">The packing_list_details_material_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="packing_list_details_material_price">The packing_list_details_material_price.</param>
		/// <param name="packing_list_details_details">The packing_list_details_details.</param>
		/// <param name="packing_list_details_supplier_number">The packing_list_details_supplier_number.</param>
		/// <param name="packing_list_details_material_discount_percent">The packing_list_details_material_discount_percent.</param>
		/// <param name="packing_list_details_material_price_after_discount">The packing_list_details_material_price_after_discount.</param>
		/// <param name="packing_list_details_quantity_within_invoice">The packing_list_details_quantity_within_invoice.</param>
		/// <param name="packing_list_details_order_number">The packing_list_details_order_number.</param>
		/// <param name="packing_list_details_order_serial">The packing_list_details_order_serial.</param>
		/// <param name="packing_list_details_stock_number">The packing_list_details_stock_number.</param>
		/// <param name="packing_list_details_sell_price">The packing_list_details_sell_price.</param>
		/// <param name="packing_list_details_bonus_quantity">The packing_list_details_bonus_quantity.</param>
		/// <param name="packing_list_details_material_bonus_connect">The packing_list_details_material_bonus_connect.</param>
		/// <param name="packing_list_details_max_quantity_allowed">The packing_list_details_max_quantity_allowed.</param>
		/// <param name="packing_list_details_last_order_change">The packing_list_details_last_order_change.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="packing_list_details_material_discount_amount">The packing_list_details_material_discount_amount.</param>
		/// <param name="packing_list_details_bonus_row">The packing_list_details_bonus_row.</param>
		/// <param name="packing_list_details_color">The packing_list_details_color.</param>
		/// <param name="packing_list_details_pay_shape">The packing_list_details_pay_shape.</param>
		/// <param name="packing_list_details_indicator">The packing_list_details_indicator.</param>
		/// <param name="packing_list_details_bonus_discount">The packing_list_details_bonus_discount.</param>
		/// <param name="packing_list_details_supplier_discount_percent">The packing_list_details_supplier_discount_percent.</param>
		/// <param name="packing_list_details_current_catalog_sell_price">The packing_list_details_current_catalog_sell_price.</param>
		/// <param name="expiration_date">The expiration_date.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="packing_list_details_mini_terminal">The packing_list_details_mini_terminal.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_material_name">The materials_material_name.</param>
		/// <param name="materials_min_valid_months">The materials_min_valid_months.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="row_no">The row_no.</param>
		/// <param name="doc_no_en">The doc_no_en.</param>
		/// <param name="compute_1">The compute_1.</param>
        public d_mini_terminal_detail_item_packing_list(long packing_list_details_serial_number, long packing_list_details_branch_number, long pack_list_number, long packing_list_details_material_number, decimal material_quantity, decimal packing_list_details_material_price, string packing_list_details_details, long packing_list_details_supplier_number, decimal packing_list_details_material_discount_percent, decimal packing_list_details_material_price_after_discount, decimal packing_list_details_quantity_within_invoice, long packing_list_details_order_number, long packing_list_details_order_serial, long packing_list_details_stock_number, decimal packing_list_details_sell_price, decimal packing_list_details_bonus_quantity, long packing_list_details_material_bonus_connect, decimal packing_list_details_max_quantity_allowed, decimal packing_list_details_last_order_change, string import_type, decimal packing_list_details_material_discount_amount, string packing_list_details_bonus_row, long packing_list_details_color, long packing_list_details_pay_shape, long packing_list_details_indicator, decimal packing_list_details_bonus_discount, decimal packing_list_details_supplier_discount_percent, decimal packing_list_details_current_catalog_sell_price, DateTime? expiration_date, decimal expected_material_quantity, long packing_list_details_mini_terminal, string materials_barcode, string materials_material_name, long materials_min_valid_months, decimal order_quantity, long b2b_status, long row_no, long doc_no_en, string compute_1)
        {
			this._packing_list_details_serial_number = packing_list_details_serial_number;
			this._packing_list_details_branch_number = packing_list_details_branch_number;
			this._pack_list_number = pack_list_number;
			this._packing_list_details_material_number = packing_list_details_material_number;
			this._material_quantity = material_quantity;
			this._packing_list_details_material_price = packing_list_details_material_price;
			this._packing_list_details_details = packing_list_details_details;
			this._packing_list_details_supplier_number = packing_list_details_supplier_number;
			this._packing_list_details_material_discount_percent = packing_list_details_material_discount_percent;
			this._packing_list_details_material_price_after_discount = packing_list_details_material_price_after_discount;
			this._packing_list_details_quantity_within_invoice = packing_list_details_quantity_within_invoice;
			this._packing_list_details_order_number = packing_list_details_order_number;
			this._packing_list_details_order_serial = packing_list_details_order_serial;
			this._packing_list_details_stock_number = packing_list_details_stock_number;
			this._packing_list_details_sell_price = packing_list_details_sell_price;
			this._packing_list_details_bonus_quantity = packing_list_details_bonus_quantity;
			this._packing_list_details_material_bonus_connect = packing_list_details_material_bonus_connect;
			this._packing_list_details_max_quantity_allowed = packing_list_details_max_quantity_allowed;
			this._packing_list_details_last_order_change = packing_list_details_last_order_change;
			this._import_type = import_type;
			this._packing_list_details_material_discount_amount = packing_list_details_material_discount_amount;
			this._packing_list_details_bonus_row = packing_list_details_bonus_row;
			this._packing_list_details_color = packing_list_details_color;
			this._packing_list_details_pay_shape = packing_list_details_pay_shape;
			this._packing_list_details_indicator = packing_list_details_indicator;
			this._packing_list_details_bonus_discount = packing_list_details_bonus_discount;
			this._packing_list_details_supplier_discount_percent = packing_list_details_supplier_discount_percent;
			this._packing_list_details_current_catalog_sell_price = packing_list_details_current_catalog_sell_price;
			this._expiration_date = expiration_date;
			this._expected_material_quantity = expected_material_quantity;
			this._packing_list_details_mini_terminal = packing_list_details_mini_terminal;
			this._materials_barcode = materials_barcode;
			this._materials_material_name = materials_material_name;
			this._materials_min_valid_months = materials_min_valid_months;
			this._order_quantity = order_quantity;
			this._b2b_status = b2b_status;
			this._row_no = row_no;
			this._doc_no_en = doc_no_en;
			this._compute_1 = compute_1;
		}

	
        /// <summary>
        /// Gets or sets the packing_list_details_serial_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_serial_number.
        /// </value>
        private long _packing_list_details_serial_number;

        /// <summary>
        /// Gets or sets the packing_list_details_serial_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("packing_list_details_serial_number")]
		public long packing_list_details_serial_number
        {
            get { return _packing_list_details_serial_number; }
            set
            {
                if (_packing_list_details_serial_number != value)
                {
                    PropertyChanged("packing_list_details_serial_number");
					_packing_list_details_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_branch_number.
        /// </value>
        private long _packing_list_details_branch_number;

        /// <summary>
        /// Gets or sets the packing_list_details_branch_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("packing_list_details_branch_number")]
		public long packing_list_details_branch_number
        {
            get { return _packing_list_details_branch_number; }
            set
            {
                if (_packing_list_details_branch_number != value)
                {
                    PropertyChanged("packing_list_details_branch_number");
					_packing_list_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
        private long _pack_list_number;

        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("pack_list_number")]
		public long pack_list_number
        {
            get { return _pack_list_number; }
            set
            {
                if (_pack_list_number != value)
                {
                    PropertyChanged("pack_list_number");
					_pack_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_number.
        /// </value>
        private long _packing_list_details_material_number;

        /// <summary>
        /// Gets or sets the packing_list_details_material_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("packing_list_details_material_number")]
		public long packing_list_details_material_number
        {
            get { return _packing_list_details_material_number; }
            set
            {
                if (_packing_list_details_material_number != value)
                {
                    PropertyChanged("packing_list_details_material_number");
					_packing_list_details_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_price.
        /// </value>
        private decimal _packing_list_details_material_price;

        /// <summary>
        /// Gets or sets the packing_list_details_material_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_price.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_material_price")]
		public decimal packing_list_details_material_price
        {
            get { return _packing_list_details_material_price; }
            set
            {
                if (_packing_list_details_material_price != value)
                {
                    PropertyChanged("packing_list_details_material_price");
					_packing_list_details_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_details.
        /// </summary>
        /// <value>
        /// The packing_list_details_details.
        /// </value>
        private string _packing_list_details_details;

        /// <summary>
        /// Gets or sets the packing_list_details_details.
        /// </summary>
        /// <value>
        /// The packing_list_details_details.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_details")]
		public string packing_list_details_details
        {
            get { return _packing_list_details_details; }
            set
            {
                if (_packing_list_details_details != value)
                {
                    PropertyChanged("packing_list_details_details");
					_packing_list_details_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_supplier_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_supplier_number.
        /// </value>
        private long _packing_list_details_supplier_number;

        /// <summary>
        /// Gets or sets the packing_list_details_supplier_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_supplier_number")]
		public long packing_list_details_supplier_number
        {
            get { return _packing_list_details_supplier_number; }
            set
            {
                if (_packing_list_details_supplier_number != value)
                {
                    PropertyChanged("packing_list_details_supplier_number");
					_packing_list_details_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_discount_percent.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_discount_percent.
        /// </value>
        private decimal _packing_list_details_material_discount_percent;

        /// <summary>
        /// Gets or sets the packing_list_details_material_discount_percent.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_material_discount_percent")]
		public decimal packing_list_details_material_discount_percent
        {
            get { return _packing_list_details_material_discount_percent; }
            set
            {
                if (_packing_list_details_material_discount_percent != value)
                {
                    PropertyChanged("packing_list_details_material_discount_percent");
					_packing_list_details_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_price_after_discount.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_price_after_discount.
        /// </value>
        private decimal _packing_list_details_material_price_after_discount;

        /// <summary>
        /// Gets or sets the packing_list_details_material_price_after_discount.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_material_price_after_discount")]
		public decimal packing_list_details_material_price_after_discount
        {
            get { return _packing_list_details_material_price_after_discount; }
            set
            {
                if (_packing_list_details_material_price_after_discount != value)
                {
                    PropertyChanged("packing_list_details_material_price_after_discount");
					_packing_list_details_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The packing_list_details_quantity_within_invoice.
        /// </value>
        private decimal _packing_list_details_quantity_within_invoice;

        /// <summary>
        /// Gets or sets the packing_list_details_quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The packing_list_details_quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_quantity_within_invoice")]
		public decimal packing_list_details_quantity_within_invoice
        {
            get { return _packing_list_details_quantity_within_invoice; }
            set
            {
                if (_packing_list_details_quantity_within_invoice != value)
                {
                    PropertyChanged("packing_list_details_quantity_within_invoice");
					_packing_list_details_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_order_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_number.
        /// </value>
        private long _packing_list_details_order_number;

        /// <summary>
        /// Gets or sets the packing_list_details_order_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_order_number")]
		public long packing_list_details_order_number
        {
            get { return _packing_list_details_order_number; }
            set
            {
                if (_packing_list_details_order_number != value)
                {
                    PropertyChanged("packing_list_details_order_number");
					_packing_list_details_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_order_serial.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_serial.
        /// </value>
        private long _packing_list_details_order_serial;

        /// <summary>
        /// Gets or sets the packing_list_details_order_serial.
        /// </summary>
        /// <value>
        /// The packing_list_details_order_serial.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_order_serial")]
		public long packing_list_details_order_serial
        {
            get { return _packing_list_details_order_serial; }
            set
            {
                if (_packing_list_details_order_serial != value)
                {
                    PropertyChanged("packing_list_details_order_serial");
					_packing_list_details_order_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_stock_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_stock_number.
        /// </value>
        private long _packing_list_details_stock_number;

        /// <summary>
        /// Gets or sets the packing_list_details_stock_number.
        /// </summary>
        /// <value>
        /// The packing_list_details_stock_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_stock_number")]
		public long packing_list_details_stock_number
        {
            get { return _packing_list_details_stock_number; }
            set
            {
                if (_packing_list_details_stock_number != value)
                {
                    PropertyChanged("packing_list_details_stock_number");
					_packing_list_details_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_sell_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_sell_price.
        /// </value>
        private decimal _packing_list_details_sell_price;

        /// <summary>
        /// Gets or sets the packing_list_details_sell_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_sell_price")]
		public decimal packing_list_details_sell_price
        {
            get { return _packing_list_details_sell_price; }
            set
            {
                if (_packing_list_details_sell_price != value)
                {
                    PropertyChanged("packing_list_details_sell_price");
					_packing_list_details_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_bonus_quantity.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_quantity.
        /// </value>
        private decimal _packing_list_details_bonus_quantity;

        /// <summary>
        /// Gets or sets the packing_list_details_bonus_quantity.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_bonus_quantity")]
		public decimal packing_list_details_bonus_quantity
        {
            get { return _packing_list_details_bonus_quantity; }
            set
            {
                if (_packing_list_details_bonus_quantity != value)
                {
                    PropertyChanged("packing_list_details_bonus_quantity");
					_packing_list_details_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_bonus_connect.
        /// </value>
        private long _packing_list_details_material_bonus_connect;

        /// <summary>
        /// Gets or sets the packing_list_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_material_bonus_connect")]
		public long packing_list_details_material_bonus_connect
        {
            get { return _packing_list_details_material_bonus_connect; }
            set
            {
                if (_packing_list_details_material_bonus_connect != value)
                {
                    PropertyChanged("packing_list_details_material_bonus_connect");
					_packing_list_details_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The packing_list_details_max_quantity_allowed.
        /// </value>
        private decimal _packing_list_details_max_quantity_allowed;

        /// <summary>
        /// Gets or sets the packing_list_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The packing_list_details_max_quantity_allowed.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_max_quantity_allowed")]
		public decimal packing_list_details_max_quantity_allowed
        {
            get { return _packing_list_details_max_quantity_allowed; }
            set
            {
                if (_packing_list_details_max_quantity_allowed != value)
                {
                    PropertyChanged("packing_list_details_max_quantity_allowed");
					_packing_list_details_max_quantity_allowed = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_last_order_change.
        /// </summary>
        /// <value>
        /// The packing_list_details_last_order_change.
        /// </value>
        private decimal _packing_list_details_last_order_change;

        /// <summary>
        /// Gets or sets the packing_list_details_last_order_change.
        /// </summary>
        /// <value>
        /// The packing_list_details_last_order_change.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_last_order_change")]
		public decimal packing_list_details_last_order_change
        {
            get { return _packing_list_details_last_order_change; }
            set
            {
                if (_packing_list_details_last_order_change != value)
                {
                    PropertyChanged("packing_list_details_last_order_change");
					_packing_list_details_last_order_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_discount_amount.
        /// </value>
        private decimal _packing_list_details_material_discount_amount;

        /// <summary>
        /// Gets or sets the packing_list_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The packing_list_details_material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_material_discount_amount")]
		public decimal packing_list_details_material_discount_amount
        {
            get { return _packing_list_details_material_discount_amount; }
            set
            {
                if (_packing_list_details_material_discount_amount != value)
                {
                    PropertyChanged("packing_list_details_material_discount_amount");
					_packing_list_details_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_bonus_row.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_row.
        /// </value>
        private string _packing_list_details_bonus_row;

        /// <summary>
        /// Gets or sets the packing_list_details_bonus_row.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_bonus_row")]
		public string packing_list_details_bonus_row
        {
            get { return _packing_list_details_bonus_row; }
            set
            {
                if (_packing_list_details_bonus_row != value)
                {
                    PropertyChanged("packing_list_details_bonus_row");
					_packing_list_details_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_color.
        /// </summary>
        /// <value>
        /// The packing_list_details_color.
        /// </value>
        private long _packing_list_details_color;

        /// <summary>
        /// Gets or sets the packing_list_details_color.
        /// </summary>
        /// <value>
        /// The packing_list_details_color.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_color")]
		public long packing_list_details_color
        {
            get { return _packing_list_details_color; }
            set
            {
                if (_packing_list_details_color != value)
                {
                    PropertyChanged("packing_list_details_color");
					_packing_list_details_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_pay_shape.
        /// </summary>
        /// <value>
        /// The packing_list_details_pay_shape.
        /// </value>
        private long _packing_list_details_pay_shape;

        /// <summary>
        /// Gets or sets the packing_list_details_pay_shape.
        /// </summary>
        /// <value>
        /// The packing_list_details_pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_pay_shape")]
		public long packing_list_details_pay_shape
        {
            get { return _packing_list_details_pay_shape; }
            set
            {
                if (_packing_list_details_pay_shape != value)
                {
                    PropertyChanged("packing_list_details_pay_shape");
					_packing_list_details_pay_shape = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_indicator.
        /// </summary>
        /// <value>
        /// The packing_list_details_indicator.
        /// </value>
        private long _packing_list_details_indicator;

        /// <summary>
        /// Gets or sets the packing_list_details_indicator.
        /// </summary>
        /// <value>
        /// The packing_list_details_indicator.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_indicator")]
		public long packing_list_details_indicator
        {
            get { return _packing_list_details_indicator; }
            set
            {
                if (_packing_list_details_indicator != value)
                {
                    PropertyChanged("packing_list_details_indicator");
					_packing_list_details_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_discount.
        /// </value>
        private decimal _packing_list_details_bonus_discount;

        /// <summary>
        /// Gets or sets the packing_list_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The packing_list_details_bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_bonus_discount")]
		public decimal packing_list_details_bonus_discount
        {
            get { return _packing_list_details_bonus_discount; }
            set
            {
                if (_packing_list_details_bonus_discount != value)
                {
                    PropertyChanged("packing_list_details_bonus_discount");
					_packing_list_details_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The packing_list_details_supplier_discount_percent.
        /// </value>
        private decimal _packing_list_details_supplier_discount_percent;

        /// <summary>
        /// Gets or sets the packing_list_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The packing_list_details_supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_supplier_discount_percent")]
		public decimal packing_list_details_supplier_discount_percent
        {
            get { return _packing_list_details_supplier_discount_percent; }
            set
            {
                if (_packing_list_details_supplier_discount_percent != value)
                {
                    PropertyChanged("packing_list_details_supplier_discount_percent");
					_packing_list_details_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_current_catalog_sell_price.
        /// </value>
        private decimal _packing_list_details_current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the packing_list_details_current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The packing_list_details_current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_current_catalog_sell_price")]
		public decimal packing_list_details_current_catalog_sell_price
        {
            get { return _packing_list_details_current_catalog_sell_price; }
            set
            {
                if (_packing_list_details_current_catalog_sell_price != value)
                {
                    PropertyChanged("packing_list_details_current_catalog_sell_price");
					_packing_list_details_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
        private DateTime? _expiration_date;

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("expiration_date")]
		public DateTime? expiration_date
        {
            get { return _expiration_date; }
            set
            {
                if (_expiration_date != value)
                {
                    PropertyChanged("expiration_date");
					_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_details_mini_terminal.
        /// </summary>
        /// <value>
        /// The packing_list_details_mini_terminal.
        /// </value>
        private long _packing_list_details_mini_terminal;

        /// <summary>
        /// Gets or sets the packing_list_details_mini_terminal.
        /// </summary>
        /// <value>
        /// The packing_list_details_mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_details_mini_terminal")]
		public long packing_list_details_mini_terminal
        {
            get { return _packing_list_details_mini_terminal; }
            set
            {
                if (_packing_list_details_mini_terminal != value)
                {
                    PropertyChanged("packing_list_details_mini_terminal");
					_packing_list_details_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
        private string _materials_material_name;

        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_material_name")]
		public string materials_material_name
        {
            get { return _materials_material_name; }
            set
            {
                if (_materials_material_name != value)
                {
                    PropertyChanged("materials_material_name");
					_materials_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
        private long _materials_min_valid_months;

        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("materials_min_valid_months")]
		public long materials_min_valid_months
        {
            get { return _materials_min_valid_months; }
            set
            {
                if (_materials_min_valid_months != value)
                {
                    PropertyChanged("materials_min_valid_months");
					_materials_min_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
		public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
        private long _row_no;

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
	    [Editable(true)]
        [Column("row_no")]
		public long row_no
        {
            get { return _row_no; }
            set
            {
                if (_row_no != value)
                {
                    PropertyChanged("row_no");
					_row_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
        private long _doc_no_en;

        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
	    [Editable(true)]
        [Column("doc_no_en")]
		public long doc_no_en
        {
            get { return _doc_no_en; }
            set
            {
                if (_doc_no_en != value)
                {
                    PropertyChanged("doc_no_en");
					_doc_no_en = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
        private string _compute_1;

        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_1")]
		public string compute_1
        {
            get { return _compute_1; }
            set
            {
                if (_compute_1 != value)
                {
                    PropertyChanged("compute_1");
					_compute_1 = value;
                    
                }
            }
        }
	}
}
