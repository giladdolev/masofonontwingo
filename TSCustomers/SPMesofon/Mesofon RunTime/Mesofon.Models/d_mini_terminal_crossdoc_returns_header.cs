using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_crossdoc_returns_header : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_crossdoc_returns_header()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="supplier">The supplier.</param>
		/// <param name="distributor">The distributor.</param>
        public d_mini_terminal_crossdoc_returns_header(double supplier, double distributor)
        {
			this._supplier = supplier;
			this._distributor = distributor;
		}

	
        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        /// <value>
        /// The supplier.
        /// </value>
        private double _supplier;

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        /// <value>
        /// The supplier.
        /// </value>
	    [Editable(true)]
        [Column("supplier")]
		public double supplier
        {
            get { return _supplier; }
            set
            {
                if (_supplier != value)
                {
                    PropertyChanged("supplier");
					_supplier = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor.
        /// </summary>
        /// <value>
        /// The distributor.
        /// </value>
        private double _distributor;

        /// <summary>
        /// Gets or sets the distributor.
        /// </summary>
        /// <value>
        /// The distributor.
        /// </value>
	    [Editable(true)]
        [Column("distributor")]
		public double distributor
        {
            get { return _distributor; }
            set
            {
                if (_distributor != value)
                {
                    PropertyChanged("distributor");
					_distributor = value;
                    
                }
            }
        }
	}
}
