using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_package : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_package()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="materials_name">The materials_name.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_number">The materials_number.</param>
		/// <param name="packages_pack_barcode">The packages_pack_barcode.</param>
		/// <param name="packages_quantity">The packages_quantity.</param>
		/// <param name="actual_quantity">The actual_quantity.</param>
		/// <param name="cf_total_quantity">The cf_total_quantity.</param>
        public d_package(string materials_name, string materials_barcode, long materials_number, string packages_pack_barcode, decimal packages_quantity, long actual_quantity, string cf_total_quantity)
        {
			this._materials_name = materials_name;
			this._materials_barcode = materials_barcode;
			this._materials_number = materials_number;
			this._packages_pack_barcode = packages_pack_barcode;
			this._packages_quantity = packages_quantity;
			this._actual_quantity = actual_quantity;
			this._cf_total_quantity = cf_total_quantity;
		}

	
        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
        private string _materials_name;

        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_name")]
		public string materials_name
        {
            get { return _materials_name; }
            set
            {
                if (_materials_name != value)
                {
                    PropertyChanged("materials_name");
					_materials_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_number.
        /// </summary>
        /// <value>
        /// The materials_number.
        /// </value>
        private long _materials_number;

        /// <summary>
        /// Gets or sets the materials_number.
        /// </summary>
        /// <value>
        /// The materials_number.
        /// </value>
	    [Editable(true)]
        [Column("materials_number")]
		public long materials_number
        {
            get { return _materials_number; }
            set
            {
                if (_materials_number != value)
                {
                    PropertyChanged("materials_number");
					_materials_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packages_pack_barcode.
        /// </summary>
        /// <value>
        /// The packages_pack_barcode.
        /// </value>
        private string _packages_pack_barcode;

        /// <summary>
        /// Gets or sets the packages_pack_barcode.
        /// </summary>
        /// <value>
        /// The packages_pack_barcode.
        /// </value>
	    [Editable(true)]
        [Column("packages_pack_barcode")]
		public string packages_pack_barcode
        {
            get { return _packages_pack_barcode; }
            set
            {
                if (_packages_pack_barcode != value)
                {
                    PropertyChanged("packages_pack_barcode");
					_packages_pack_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packages_quantity.
        /// </summary>
        /// <value>
        /// The packages_quantity.
        /// </value>
        private decimal _packages_quantity;

        /// <summary>
        /// Gets or sets the packages_quantity.
        /// </summary>
        /// <value>
        /// The packages_quantity.
        /// </value>
	    [Editable(true)]
        [Column("packages_quantity")]
		public decimal packages_quantity
        {
            get { return _packages_quantity; }
            set
            {
                if (_packages_quantity != value)
                {
                    PropertyChanged("packages_quantity");
					_packages_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
        private long _actual_quantity;

        /// <summary>
        /// Gets or sets the actual_quantity.
        /// </summary>
        /// <value>
        /// The actual_quantity.
        /// </value>
	    [Editable(true)]
        [Column("actual_quantity")]
		public long actual_quantity
        {
            get { return _actual_quantity; }
            set
            {
                if (_actual_quantity != value)
                {
                    PropertyChanged("actual_quantity");
					_actual_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cf_total_quantity.
        /// </summary>
        /// <value>
        /// The cf_total_quantity.
        /// </value>
        private string _cf_total_quantity;

        /// <summary>
        /// Gets or sets the cf_total_quantity.
        /// </summary>
        /// <value>
        /// The cf_total_quantity.
        /// </value>
	    [Editable(true)]
        [Column("cf_total_quantity")]
		public string cf_total_quantity
        {
            get { return _cf_total_quantity; }
            set
            {
                if (_cf_total_quantity != value)
                {
                    PropertyChanged("cf_total_quantity");
					_cf_total_quantity = value;
                    
                }
            }
        }
	}
}
