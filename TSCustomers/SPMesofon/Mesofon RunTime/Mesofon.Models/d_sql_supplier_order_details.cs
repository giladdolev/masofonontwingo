using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_sql_supplier_order_details : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_sql_supplier_order_details()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="material_price_after_discount">The material_price_after_discount.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="material_discount_amount">The material_discount_amount.</param>
		/// <param name="color">The color.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="details">The details.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="pay_shape">The pay_shape.</param>
		/// <param name="sort_number">The sort_number.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="material_bonus_connect">The material_bonus_connect.</param>
		/// <param name="bonus_row">The bonus_row.</param>
		/// <param name="stock_balance">The stock_balance.</param>
		/// <param name="this_month_sales">The this_month_sales.</param>
		/// <param name="last_month_sales">The last_month_sales.</param>
		/// <param name="two_last_months_sales">The two_last_months_sales.</param>
		/// <param name="min_week_sales">The min_week_sales.</param>
		/// <param name="max_week_sales">The max_week_sales.</param>
		/// <param name="recommended_quantity">The recommended_quantity.</param>
		/// <param name="confirmed_orders">The confirmed_orders.</param>
		/// <param name="row_in_order">The row_in_order.</param>
		/// <param name="ordered_not_received">The ordered_not_received.</param>
		/// <param name="row_found">The row_found.</param>
		/// <param name="avg_week_sales">The avg_week_sales.</param>
        public d_sql_supplier_order_details(long serial_number, long branch_number, long order_number, long material_number, decimal material_price_after_discount, decimal quantity_within_invoice, decimal material_discount_amount, long color, decimal sell_price, long supplier_number, decimal material_price, decimal material_quantity, long stock_number, string details, decimal material_discount_percent, long pay_shape, long sort_number, decimal bonus_quantity, long material_bonus_connect, string bonus_row, decimal stock_balance, decimal this_month_sales, decimal last_month_sales, decimal two_last_months_sales, decimal min_week_sales, decimal max_week_sales, decimal recommended_quantity, decimal confirmed_orders, long row_in_order, string ordered_not_received, long row_found, decimal avg_week_sales)
        {
			this._serial_number = serial_number;
			this._branch_number = branch_number;
			this._order_number = order_number;
			this._material_number = material_number;
			this._material_price_after_discount = material_price_after_discount;
			this._quantity_within_invoice = quantity_within_invoice;
			this._material_discount_amount = material_discount_amount;
			this._color = color;
			this._sell_price = sell_price;
			this._supplier_number = supplier_number;
			this._material_price = material_price;
			this._material_quantity = material_quantity;
			this._stock_number = stock_number;
			this._details = details;
			this._material_discount_percent = material_discount_percent;
			this._pay_shape = pay_shape;
			this._sort_number = sort_number;
			this._bonus_quantity = bonus_quantity;
			this._material_bonus_connect = material_bonus_connect;
			this._bonus_row = bonus_row;
			this._stock_balance = stock_balance;
			this._this_month_sales = this_month_sales;
			this._last_month_sales = last_month_sales;
			this._two_last_months_sales = two_last_months_sales;
			this._min_week_sales = min_week_sales;
			this._max_week_sales = max_week_sales;
			this._recommended_quantity = recommended_quantity;
			this._confirmed_orders = confirmed_orders;
			this._row_in_order = row_in_order;
			this._ordered_not_received = ordered_not_received;
			this._row_found = row_found;
			this._avg_week_sales = avg_week_sales;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
        [Properties("serial_number", "serial_number", true, 0 ,typeof(long))]
        public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
        [Properties("branch_number", "branch_number", true, 1, typeof(long))]
        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
        [Properties("order_number", "order_number", true, 2, typeof(long))]
        public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
        [Properties("material_number", "material_number", true, 3, typeof(long))]
        public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
        private decimal _material_price_after_discount;

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_discount")]
        [Properties("material_price_after_discount", "material_price_after_discount", true, 4, typeof(decimal))]
        public decimal material_price_after_discount
        {
            get { return _material_price_after_discount; }
            set
            {
                if (_material_price_after_discount != value)
                {
                    PropertyChanged("material_price_after_discount");
					_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
        [Properties("quantity_within_invoice", "quantity_within_invoice", true, 5, typeof(decimal))]

        public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
        private decimal _material_discount_amount;

        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_amount")]
        [Properties("material_discount_amount", "material_discount_amount", true, 6, typeof(decimal))]
        public decimal material_discount_amount
        {
            get { return _material_discount_amount; }
            set
            {
                if (_material_discount_amount != value)
                {
                    PropertyChanged("material_discount_amount");
					_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
        [Properties("color", "color", true, 7, typeof(long))]
        public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
        [Properties("sell_price", "sell_price", true,8, typeof(decimal))]
        public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [Properties("supplier_number", "supplier_number", true, 9, typeof(long))]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
        [Properties("material_price", "material_price", true, 10, typeof(decimal))]
        public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
        [Properties("material_quantity", "material_quantity", true, 11, typeof(decimal))]
        public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
        [Properties("stock_number", "stock_number", true, 12, typeof(long))]
        public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
        [Properties("details", "details", true, 13, typeof(string))]
        public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
        [Properties("material_discount_percent", "material_discount_percent", true, 14, typeof(decimal))]
        public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
        private long _pay_shape;

        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("pay_shape")]
        [Properties("pay_shape", "pay_shape", true, 15, typeof(long))]
        public long pay_shape
        {
            get { return _pay_shape; }
            set
            {
                if (_pay_shape != value)
                {
                    PropertyChanged("pay_shape");
					_pay_shape = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sort_number.
        /// </summary>
        /// <value>
        /// The sort_number.
        /// </value>
        private long _sort_number;

        /// <summary>
        /// Gets or sets the sort_number.
        /// </summary>
        /// <value>
        /// The sort_number.
        /// </value>
	    [Editable(true)]
        [Column("sort_number")]
        [Properties("sort_number", "sort_number", true, 16, typeof(long))]
        public long sort_number
        {
            get { return _sort_number; }
            set
            {
                if (_sort_number != value)
                {
                    PropertyChanged("sort_number");
					_sort_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
        [Properties("bonus_quantity", "bonus_quantity", true, 17, typeof(decimal))]
        public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
        [Properties("material_bonus_connect", "material_bonus_connect", true, 18, typeof(long))]
        public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
        private string _bonus_row;

        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("bonus_row")]
        [Properties("bonus_row", "bonus_row", true, 19, typeof(string))]
        public string bonus_row
        {
            get { return _bonus_row; }
            set
            {
                if (_bonus_row != value)
                {
                    PropertyChanged("bonus_row");
					_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_balance.
        /// </summary>
        /// <value>
        /// The stock_balance.
        /// </value>
        private decimal _stock_balance;

        /// <summary>
        /// Gets or sets the stock_balance.
        /// </summary>
        /// <value>
        /// The stock_balance.
        /// </value>
	    [Editable(true)]
        [Column("stock_balance")]
        [Properties("stock_balance", "stock_balance", true, 20, typeof(decimal))]
        public decimal stock_balance
        {
            get { return _stock_balance; }
            set
            {
                if (_stock_balance != value)
                {
                    PropertyChanged("stock_balance");
					_stock_balance = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the this_month_sales.
        /// </summary>
        /// <value>
        /// The this_month_sales.
        /// </value>
        private decimal _this_month_sales;

        /// <summary>
        /// Gets or sets the this_month_sales.
        /// </summary>
        /// <value>
        /// The this_month_sales.
        /// </value>
	    [Editable(true)]
        [Column("this_month_sales")]
        [Properties("this_month_sales", "this_month_sales", true, 21, typeof(decimal))]
        public decimal this_month_sales
        {
            get { return _this_month_sales; }
            set
            {
                if (_this_month_sales != value)
                {
                    PropertyChanged("this_month_sales");
					_this_month_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_month_sales.
        /// </summary>
        /// <value>
        /// The last_month_sales.
        /// </value>
        private decimal _last_month_sales;

        /// <summary>
        /// Gets or sets the last_month_sales.
        /// </summary>
        /// <value>
        /// The last_month_sales.
        /// </value>
	    [Editable(true)]
        [Column("last_month_sales")]
        [Properties("last_month_sales", "last_month_sales", true, 22, typeof(decimal))]
        public decimal last_month_sales
        {
            get { return _last_month_sales; }
            set
            {
                if (_last_month_sales != value)
                {
                    PropertyChanged("last_month_sales");
					_last_month_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the two_last_months_sales.
        /// </summary>
        /// <value>
        /// The two_last_months_sales.
        /// </value>
        private decimal _two_last_months_sales;

        /// <summary>
        /// Gets or sets the two_last_months_sales.
        /// </summary>
        /// <value>
        /// The two_last_months_sales.
        /// </value>
	    [Editable(true)]
        [Column("two_last_months_sales")]
        [Properties("two_last_months_sales", "two_last_months_sales", true, 23, typeof(decimal))]
        public decimal two_last_months_sales
        {
            get { return _two_last_months_sales; }
            set
            {
                if (_two_last_months_sales != value)
                {
                    PropertyChanged("two_last_months_sales");
					_two_last_months_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the min_week_sales.
        /// </summary>
        /// <value>
        /// The min_week_sales.
        /// </value>
        private decimal _min_week_sales;

        /// <summary>
        /// Gets or sets the min_week_sales.
        /// </summary>
        /// <value>
        /// The min_week_sales.
        /// </value>
	    [Editable(true)]
        [Column("min_week_sales")]
        [Properties("min_week_sales", "min_week_sales", true, 24, typeof(decimal))]
        public decimal min_week_sales
        {
            get { return _min_week_sales; }
            set
            {
                if (_min_week_sales != value)
                {
                    PropertyChanged("min_week_sales");
					_min_week_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the max_week_sales.
        /// </summary>
        /// <value>
        /// The max_week_sales.
        /// </value>
        private decimal _max_week_sales;

        /// <summary>
        /// Gets or sets the max_week_sales.
        /// </summary>
        /// <value>
        /// The max_week_sales.
        /// </value>
	    [Editable(true)]
        [Column("max_week_sales")]
        [Properties("max_week_sales", "max_week_sales", true, 25, typeof(decimal))]
        public decimal max_week_sales
        {
            get { return _max_week_sales; }
            set
            {
                if (_max_week_sales != value)
                {
                    PropertyChanged("max_week_sales");
					_max_week_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the recommended_quantity.
        /// </summary>
        /// <value>
        /// The recommended_quantity.
        /// </value>
        private decimal _recommended_quantity;

        /// <summary>
        /// Gets or sets the recommended_quantity.
        /// </summary>
        /// <value>
        /// The recommended_quantity.
        /// </value>
	    [Editable(true)]
        [Column("recommended_quantity")]
        [Properties("recommended_quantity", "recommended_quantity", true, 26, typeof(decimal))]
        public decimal recommended_quantity
        {
            get { return _recommended_quantity; }
            set
            {
                if (_recommended_quantity != value)
                {
                    PropertyChanged("recommended_quantity");
					_recommended_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the confirmed_orders.
        /// </summary>
        /// <value>
        /// The confirmed_orders.
        /// </value>
        private decimal _confirmed_orders;

        /// <summary>
        /// Gets or sets the confirmed_orders.
        /// </summary>
        /// <value>
        /// The confirmed_orders.
        /// </value>
	    [Editable(true)]
        [Column("confirmed_orders")]
        [Properties("confirmed_orders", "confirmed_orders", true, 27, typeof(decimal))]
        public decimal confirmed_orders
        {
            get { return _confirmed_orders; }
            set
            {
                if (_confirmed_orders != value)
                {
                    PropertyChanged("confirmed_orders");
					_confirmed_orders = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_in_order.
        /// </summary>
        /// <value>
        /// The row_in_order.
        /// </value>
        private long _row_in_order;

        /// <summary>
        /// Gets or sets the row_in_order.
        /// </summary>
        /// <value>
        /// The row_in_order.
        /// </value>
	    [Editable(true)]
        [Column("row_in_order")]
        [Properties("row_in_order", "row_in_order", true, 28, typeof(long))]
        public long row_in_order
        {
            get { return _row_in_order; }
            set
            {
                if (_row_in_order != value)
                {
                    PropertyChanged("row_in_order");
					_row_in_order = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ordered_not_received.
        /// </summary>
        /// <value>
        /// The ordered_not_received.
        /// </value>
        private string _ordered_not_received;

        /// <summary>
        /// Gets or sets the ordered_not_received.
        /// </summary>
        /// <value>
        /// The ordered_not_received.
        /// </value>
	    [Editable(true)]
        [Column("ordered_not_received")]
        [Properties("ordered_not_received", "ordered_not_received", true, 29, typeof(string))]
        public string ordered_not_received
        {
            get { return _ordered_not_received; }
            set
            {
                if (_ordered_not_received != value)
                {
                    PropertyChanged("ordered_not_received");
					_ordered_not_received = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_found.
        /// </summary>
        /// <value>
        /// The row_found.
        /// </value>
        private long _row_found;

        /// <summary>
        /// Gets or sets the row_found.
        /// </summary>
        /// <value>
        /// The row_found.
        /// </value>
	    [Editable(true)]
        [Column("row_found")]
        [Properties("row_found", "row_found", true, 30, typeof(long))]
        public long row_found
        {
            get { return _row_found; }
            set
            {
                if (_row_found != value)
                {
                    PropertyChanged("row_found");
					_row_found = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the avg_week_sales.
        /// </summary>
        /// <value>
        /// The avg_week_sales.
        /// </value>
        private decimal _avg_week_sales;

        /// <summary>
        /// Gets or sets the avg_week_sales.
        /// </summary>
        /// <value>
        /// The avg_week_sales.
        /// </value>
	    [Editable(true)]
        [Column("avg_week_sales")]
        [Properties("avg_week_sales", "avg_week_sales", true, 31, typeof(decimal))]
        public decimal avg_week_sales
        {
            get { return _avg_week_sales; }
            set
            {
                if (_avg_week_sales != value)
                {
                    PropertyChanged("avg_week_sales");
					_avg_week_sales = value;
                    
                }
            }
        }
	}
}
