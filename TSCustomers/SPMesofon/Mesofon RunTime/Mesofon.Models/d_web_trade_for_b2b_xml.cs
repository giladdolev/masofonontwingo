using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_web_trade_for_b2b_xml : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_web_trade_for_b2b_xml()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="receiver">The receiver.</param>
		/// <param name="doctype">The doctype.</param>
		/// <param name="aprf">The aprf.</param>
		/// <param name="snrf">The snrf.</param>
		/// <param name="ackn">The ackn.</param>
		/// <param name="testind">The testind.</param>
		/// <param name="messdate">The messdate.</param>
		/// <param name="messtime">The messtime.</param>
		/// <param name="statusname">The statusname.</param>
		/// <param name="statuscode">The statuscode.</param>
		/// <param name="statusdatetime">The statusdatetime.</param>
		/// <param name="orderorgno">The orderorgno.</param>
		/// <param name="orderdate">The orderdate.</param>
        public d_web_trade_for_b2b_xml(string sender, string receiver, string doctype, string aprf, string snrf, string ackn, string testind, double messdate, string messtime, string statusname, string statuscode, long statusdatetime, string orderorgno, double orderdate)
        {
			this._sender = sender;
			this._receiver = receiver;
			this._doctype = doctype;
			this._aprf = aprf;
			this._snrf = snrf;
			this._ackn = ackn;
			this._testind = testind;
			this._messdate = messdate;
			this._messtime = messtime;
			this._statusname = statusname;
			this._statuscode = statuscode;
			this._statusdatetime = statusdatetime;
			this._orderorgno = orderorgno;
			this._orderdate = orderdate;
		}

	
        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
        private string _sender;

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
	    [Editable(true)]
        [Column("sender")]
        [PropertiesAttribute("sender", "sender", true,0,typeof(string))]
		public string sender
        {
            get { return _sender; }
            set
            {
                if (_sender != value)
                {
                    PropertyChanged("sender");
					_sender = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
        private string _receiver;

        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
	    [Editable(true)]
        [Column("receiver")]
        [PropertiesAttribute("receiver", "receiver", true, 1, typeof(string))]
        public string receiver
        {
            get { return _receiver; }
            set
            {
                if (_receiver != value)
                {
                    PropertyChanged("receiver");
					_receiver = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
        private string _doctype;

        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
	    [Editable(true)]
        [Column("doctype")]
        [PropertiesAttribute("doctype", "doctype", true, 2, typeof(string))]
        public string doctype
        {
            get { return _doctype; }
            set
            {
                if (_doctype != value)
                {
                    PropertyChanged("doctype");
					_doctype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
        private string _aprf;

        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
	    [Editable(true)]
        [Column("aprf")]
        [PropertiesAttribute("aprf", "aprf", true, 3, typeof(string))]
        public string aprf
        {
            get { return _aprf; }
            set
            {
                if (_aprf != value)
                {
                    PropertyChanged("aprf");
					_aprf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
        private string _snrf;

        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
	    [Editable(true)]
        [Column("snrf")]
        [PropertiesAttribute("snrf", "snrf", true, 4, typeof(string))]
        public string snrf
        {
            get { return _snrf; }
            set
            {
                if (_snrf != value)
                {
                    PropertyChanged("snrf");
					_snrf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
        private string _ackn;

        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
	    [Editable(true)]
        [Column("ackn")]
        [PropertiesAttribute("ackn", "ackn", true, 5, typeof(string))]
        public string ackn
        {
            get { return _ackn; }
            set
            {
                if (_ackn != value)
                {
                    PropertyChanged("ackn");
					_ackn = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
        private string _testind;

        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
	    [Editable(true)]
        [Column("testind")]
        [PropertiesAttribute("testind", "testind", true, 6, typeof(string))]
        public string testind
        {
            get { return _testind; }
            set
            {
                if (_testind != value)
                {
                    PropertyChanged("testind");
					_testind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
        private double _messdate;

        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
	    [Editable(true)]
        [Column("messdate")]
        [PropertiesAttribute("messdate", "messdate", true, 7, typeof(double))]
        public double messdate
        {
            get { return _messdate; }
            set
            {
                if (_messdate != value)
                {
                    PropertyChanged("messdate");
					_messdate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
        private string _messtime;

        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
	    [Editable(true)]
        [Column("messtime")]
        [PropertiesAttribute("messtime", "messtime", true, 8, typeof(string))]
        public string messtime
        {
            get { return _messtime; }
            set
            {
                if (_messtime != value)
                {
                    PropertyChanged("messtime");
					_messtime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the statusname.
        /// </summary>
        /// <value>
        /// The statusname.
        /// </value>
        private string _statusname;

        /// <summary>
        /// Gets or sets the statusname.
        /// </summary>
        /// <value>
        /// The statusname.
        /// </value>
	    [Editable(true)]
        [Column("statusname")]
        [PropertiesAttribute("statusname", "statusname", true, 9, typeof(string))]
        public string statusname
        {
            get { return _statusname; }
            set
            {
                if (_statusname != value)
                {
                    PropertyChanged("statusname");
					_statusname = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the statuscode.
        /// </summary>
        /// <value>
        /// The statuscode.
        /// </value>
        private string _statuscode;

        /// <summary>
        /// Gets or sets the statuscode.
        /// </summary>
        /// <value>
        /// The statuscode.
        /// </value>
	    [Editable(true)]
        [Column("statuscode")]
        [PropertiesAttribute("statuscode", "statuscode", true, 10, typeof(string))]
        public string statuscode
        {
            get { return _statuscode; }
            set
            {
                if (_statuscode != value)
                {
                    PropertyChanged("statuscode");
					_statuscode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the statusdatetime.
        /// </summary>
        /// <value>
        /// The statusdatetime.
        /// </value>
        private long _statusdatetime;

        /// <summary>
        /// Gets or sets the statusdatetime.
        /// </summary>
        /// <value>
        /// The statusdatetime.
        /// </value>
	    [Editable(true)]
        [Column("statusdatetime")]
        [PropertiesAttribute("statusdatetime", "statusdatetime", true, 11, typeof(long))]
        public long statusdatetime
        {
            get { return _statusdatetime; }
            set
            {
                if (_statusdatetime != value)
                {
                    PropertyChanged("statusdatetime");
					_statusdatetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the orderorgno.
        /// </summary>
        /// <value>
        /// The orderorgno.
        /// </value>
        private string _orderorgno;

        /// <summary>
        /// Gets or sets the orderorgno.
        /// </summary>
        /// <value>
        /// The orderorgno.
        /// </value>
	    [Editable(true)]
        [Column("orderorgno")]
        [PropertiesAttribute("orderorgno", "orderorgno", true, 12, typeof(long))]
        public string orderorgno
        {
            get { return _orderorgno; }
            set
            {
                if (_orderorgno != value)
                {
                    PropertyChanged("orderorgno");
					_orderorgno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the orderdate.
        /// </summary>
        /// <value>
        /// The orderdate.
        /// </value>
        private double _orderdate;

        /// <summary>
        /// Gets or sets the orderdate.
        /// </summary>
        /// <value>
        /// The orderdate.
        /// </value>
	    [Editable(true)]
        [Column("orderdate")]
        [PropertiesAttribute("orderdate", "orderdate", true, 13, typeof(double))]
        public double orderdate
        {
            get { return _orderdate; }
            set
            {
                if (_orderdate != value)
                {
                    PropertyChanged("orderdate");
					_orderdate = value;
                    
                }
            }
        }
	}
}
