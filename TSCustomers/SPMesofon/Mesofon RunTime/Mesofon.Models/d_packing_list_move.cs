using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_packing_list_move : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_packing_list_move()
        {
            this._doc_type = "P";
            this._packing_type = "P";
            this._stock_number = 1;
            this._store_number = 1;
 //           this._expected_total_amount = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="doc_type">The doc_type.</param>
		/// <param name="pack_list_number">The pack_list_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="packing_type">The packing_type.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="supply_date">The supply_date.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="discount">The discount.</param>
		/// <param name="mam">The mam.</param>
		/// <param name="station_num">The station_num.</param>
		/// <param name="store_number">The store_number.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="pack_list_total">The pack_list_total.</param>
		/// <param name="log_book">The log_book.</param>
		/// <param name="state">The state.</param>
		/// <param name="external_account_tx">The external_account_tx.</param>
		/// <param name="external_tx_number">The external_tx_number.</param>
		/// <param name="expected_total_amount">The expected_total_amount.</param>
		/// <param name="discount_percent">The discount_percent.</param>
		/// <param name="price_list_number">The price_list_number.</param>
		/// <param name="create_mode">The create_mode.</param>
		/// <param name="van_sale">The van_sale.</param>
		/// <param name="decline_reason">The decline_reason.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="payment_code_number">The payment_code_number.</param>
		/// <param name="payments_number">The payments_number.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="row_saved">The row_saved.</param>
		/// <param name="declines">The declines.</param>
		/// <param name="b2b_reference_number">The b2b_reference_number.</param>
        public d_packing_list_move(string doc_type, long pack_list_number, long branch_number, long supplier_number, string packing_type, long employee_number, DateTime? supply_date, DateTime? date_move, decimal discount, decimal mam, long station_num, long store_number, long stock_number, decimal pack_list_total, long log_book, string state, long external_account_tx, long external_tx_number, decimal expected_total_amount, decimal discount_percent, long price_list_number, string create_mode, long van_sale, long decline_reason, long order_number, long b2b_status, long payment_code_number, long payments_number, long distributor_number, long row_saved, long declines, long b2b_reference_number)
        {
			this._doc_type = doc_type;
			this._pack_list_number = pack_list_number;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._packing_type = packing_type;
			this._employee_number = employee_number;
			this._supply_date = supply_date;
			this._date_move = date_move;
			this._discount = discount;
			this._mam = mam;
			this._station_num = station_num;
			this._store_number = store_number;
			this._stock_number = stock_number;
			this._pack_list_total = pack_list_total;
			this._log_book = log_book;
			this._state = state;
			this._external_account_tx = external_account_tx;
			this._external_tx_number = external_tx_number;
			this._expected_total_amount = expected_total_amount;
			this._discount_percent = discount_percent;
			this._price_list_number = price_list_number;
			this._create_mode = create_mode;
			this._van_sale = van_sale;
			this._decline_reason = decline_reason;
			this._order_number = order_number;
			this._b2b_status = b2b_status;
			this._payment_code_number = payment_code_number;
			this._payments_number = payments_number;
			this._distributor_number = distributor_number;
			this._row_saved = row_saved;
			this._declines = declines;
			this._b2b_reference_number = b2b_reference_number;
		}

	
        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
	    [Editable(true)]
        [Column("doc_type")]
		public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
					_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
        private long _pack_list_number;

        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("pack_list_number")]
		public long pack_list_number
        {
            get { return _pack_list_number; }
            set
            {
                if (_pack_list_number != value)
                {
                    PropertyChanged("pack_list_number");
					_pack_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_type.
        /// </summary>
        /// <value>
        /// The packing_type.
        /// </value>
        private string _packing_type;

        /// <summary>
        /// Gets or sets the packing_type.
        /// </summary>
        /// <value>
        /// The packing_type.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("packing_type")]
		public string packing_type
        {
            get { return _packing_type; }
            set
            {
                if (_packing_type != value)
                {
                    PropertyChanged("packing_type");
					_packing_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
        private DateTime? _supply_date;

        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
	    [Editable(true)]
        [Column("supply_date")]
		public DateTime? supply_date
        {
            get { return _supply_date; }
            set
            {
                if (_supply_date != value)
                {
                    PropertyChanged("supply_date");
					_supply_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
        private decimal _discount;

        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
	    [Editable(true)]
        [Column("discount")]
		public decimal discount
        {
            get { return _discount; }
            set
            {
                if (_discount != value)
                {
                    PropertyChanged("discount");
					_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
        private decimal _mam;

        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
	    [Editable(true)]
        [Column("mam")]
		public decimal mam
        {
            get { return _mam; }
            set
            {
                if (_mam != value)
                {
                    PropertyChanged("mam");
					_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
        private long _station_num;

        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
	    [Editable(true)]
        [Column("station_num")]
		public long station_num
        {
            get { return _station_num; }
            set
            {
                if (_station_num != value)
                {
                    PropertyChanged("station_num");
					_station_num = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
        private long _store_number;

        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
	    [Editable(true)]
        [Column("store_number")]
		public long store_number
        {
            get { return _store_number; }
            set
            {
                if (_store_number != value)
                {
                    PropertyChanged("store_number");
					_store_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pack_list_total.
        /// </summary>
        /// <value>
        /// The pack_list_total.
        /// </value>
        private decimal _pack_list_total;

        /// <summary>
        /// Gets or sets the pack_list_total.
        /// </summary>
        /// <value>
        /// The pack_list_total.
        /// </value>
	    [Editable(true)]
        [Column("pack_list_total")]
		public decimal pack_list_total
        {
            get { return _pack_list_total; }
            set
            {
                if (_pack_list_total != value)
                {
                    PropertyChanged("pack_list_total");
					_pack_list_total = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
        private long _log_book;

        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
	    [Editable(true)]
        [Column("log_book")]
		public long log_book
        {
            get { return _log_book; }
            set
            {
                if (_log_book != value)
                {
                    PropertyChanged("log_book");
					_log_book = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the external_account_tx.
        /// </summary>
        /// <value>
        /// The external_account_tx.
        /// </value>
        private long _external_account_tx;

        /// <summary>
        /// Gets or sets the external_account_tx.
        /// </summary>
        /// <value>
        /// The external_account_tx.
        /// </value>
	    [Editable(true)]
        [Column("external_account_tx")]
		public long external_account_tx
        {
            get { return _external_account_tx; }
            set
            {
                if (_external_account_tx != value)
                {
                    PropertyChanged("external_account_tx");
					_external_account_tx = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the external_tx_number.
        /// </summary>
        /// <value>
        /// The external_tx_number.
        /// </value>
        private long _external_tx_number;

        /// <summary>
        /// Gets or sets the external_tx_number.
        /// </summary>
        /// <value>
        /// The external_tx_number.
        /// </value>
	    [Editable(true)]
        [Column("external_tx_number")]
		public long external_tx_number
        {
            get { return _external_tx_number; }
            set
            {
                if (_external_tx_number != value)
                {
                    PropertyChanged("external_tx_number");
					_external_tx_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
        private decimal _expected_total_amount;

        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
	    [Editable(true)]
        [Column("expected_total_amount")]
		public decimal expected_total_amount
        {
            get { return _expected_total_amount; }
            set
            {
                if (_expected_total_amount != value)
                {
                    PropertyChanged("expected_total_amount");
					_expected_total_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
        private decimal _discount_percent;

        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("discount_percent")]
		public decimal discount_percent
        {
            get { return _discount_percent; }
            set
            {
                if (_discount_percent != value)
                {
                    PropertyChanged("discount_percent");
					_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the price_list_number.
        /// </summary>
        /// <value>
        /// The price_list_number.
        /// </value>
        private long _price_list_number;

        /// <summary>
        /// Gets or sets the price_list_number.
        /// </summary>
        /// <value>
        /// The price_list_number.
        /// </value>
	    [Editable(true)]
        [Column("price_list_number")]
		public long price_list_number
        {
            get { return _price_list_number; }
            set
            {
                if (_price_list_number != value)
                {
                    PropertyChanged("price_list_number");
					_price_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
        private string _create_mode;

        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
	    [Editable(true)]
        [Column("create_mode")]
		public string create_mode
        {
            get { return _create_mode; }
            set
            {
                if (_create_mode != value)
                {
                    PropertyChanged("create_mode");
					_create_mode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the van_sale.
        /// </summary>
        /// <value>
        /// The van_sale.
        /// </value>
        private long _van_sale;

        /// <summary>
        /// Gets or sets the van_sale.
        /// </summary>
        /// <value>
        /// The van_sale.
        /// </value>
	    [Editable(true)]
        [Column("van_sale")]
		public long van_sale
        {
            get { return _van_sale; }
            set
            {
                if (_van_sale != value)
                {
                    PropertyChanged("van_sale");
					_van_sale = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_reason.
        /// </summary>
        /// <value>
        /// The decline_reason.
        /// </value>
        private long _decline_reason;

        /// <summary>
        /// Gets or sets the decline_reason.
        /// </summary>
        /// <value>
        /// The decline_reason.
        /// </value>
	    [Editable(true)]
        [Column("decline_reason")]
		public long decline_reason
        {
            get { return _decline_reason; }
            set
            {
                if (_decline_reason != value)
                {
                    PropertyChanged("decline_reason");
					_decline_reason = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
        private long _payment_code_number;

        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
	    [Editable(true)]
        [Column("payment_code_number")]
		public long payment_code_number
        {
            get { return _payment_code_number; }
            set
            {
                if (_payment_code_number != value)
                {
                    PropertyChanged("payment_code_number");
					_payment_code_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
        private long _payments_number;

        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
	    [Editable(true)]
        [Column("payments_number")]
		public long payments_number
        {
            get { return _payments_number; }
            set
            {
                if (_payments_number != value)
                {
                    PropertyChanged("payments_number");
					_payments_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_saved.
        /// </summary>
        /// <value>
        /// The row_saved.
        /// </value>
        private long _row_saved;

        /// <summary>
        /// Gets or sets the row_saved.
        /// </summary>
        /// <value>
        /// The row_saved.
        /// </value>
	    [Editable(true)]
        [Column("row_saved")]
		public long row_saved
        {
            get { return _row_saved; }
            set
            {
                if (_row_saved != value)
                {
                    PropertyChanged("row_saved");
					_row_saved = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
        private long _declines;

        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
	    [Editable(true)]
        [Column("declines")]
		public long declines
        {
            get { return _declines; }
            set
            {
                if (_declines != value)
                {
                    PropertyChanged("declines");
					_declines = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_reference_number.
        /// </summary>
        /// <value>
        /// The b2b_reference_number.
        /// </value>
        private long? _b2b_reference_number;

        /// <summary>
        /// Gets or sets the b2b_reference_number.
        /// </summary>
        /// <value>
        /// The b2b_reference_number.
        /// </value>
	    [Editable(true)]
        [Column("b2b_reference_number")]
		public long? b2b_reference_number
        {
            get { return _b2b_reference_number; }
            set
            {
                if (_b2b_reference_number != value)
                {
                    PropertyChanged("b2b_reference_number");
					_b2b_reference_number = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
        private long _row_no;

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
	    [Editable(true)]
        [Column("row_no")]
        public long row_no
        {
            get { return _row_no; }
            set
            {
                if (_row_no != value)
                {
                    //PropertyChanged("row_no");
                    _row_no = value;

                }
            }
        }
    }
}
