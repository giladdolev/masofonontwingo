using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_b2b_branches_suppliers : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_b2b_branches_suppliers()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="suppliers_edi_number">The suppliers_edi_number.</param>
        public d_b2b_branches_suppliers(long supplier_number, string suppliers_edi_number)
        {
			this._supplier_number = supplier_number;
			this._suppliers_edi_number = suppliers_edi_number;
		}

	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the suppliers_edi_number.
        /// </summary>
        /// <value>
        /// The suppliers_edi_number.
        /// </value>
        private string _suppliers_edi_number;

        /// <summary>
        /// Gets or sets the suppliers_edi_number.
        /// </summary>
        /// <value>
        /// The suppliers_edi_number.
        /// </value>
	    [Editable(true)]
        [Column("suppliers_edi_number")]
		public string suppliers_edi_number
        {
            get { return _suppliers_edi_number; }
            set
            {
                if (_suppliers_edi_number != value)
                {
                    PropertyChanged("suppliers_edi_number");
					_suppliers_edi_number = value;
                    
                }
            }
        }
	}
}
