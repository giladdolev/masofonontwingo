using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_declines_reasons_display : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_declines_reasons_display()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="display_name">The display_name.</param>
		/// <param name="checkedField">The checkedField.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="parent_doc_type">The parent_doc_type.</param>
		/// <param name="parent_doc_number">The parent_doc_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="line_number">The line_number.</param>
		/// <param name="parent_serial_number">The parent_serial_number.</param>
		/// <param name="status">The status.</param>
		/// <param name="inserted_option">The inserted_option.</param>
		/// <param name="update_datetime">The update_datetime.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="move_decline_code">The move_decline_code.</param>
		/// <param name="is_reject_with_qty">The is_reject_with_qty.</param>
		/// <param name="is_reject_all_invoice">The is_reject_all_invoice.</param>
		/// <param name="is_automatic_applied">The is_automatic_applied.</param>
		/// <param name="is_red_stamp">The is_red_stamp.</param>
		/// <param name="original_quantity">The original_quantity.</param>
		/// <param name="move_actual_quantity">The move_actual_quantity.</param>
		/// <param name="reject_quantity">The reject_quantity.</param>
		/// <param name="move_original_price">The move_original_price.</param>
		/// <param name="actual_price">The actual_price.</param>
		/// <param name="b2b_msg_decline_ref">The b2b_msg_decline_ref.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="details">The details.</param>
		/// <param name="b2b_decline_types_is_active">The b2b_decline_types_is_active.</param>
		/// <param name="max_line_number">The max_line_number.</param>
        public d_declines_reasons_display(string display_name, long checkedField, long branch_number, long supplier_number, long order_number, string parent_doc_type, long parent_doc_number, long material_number, long line_number, long parent_serial_number, string status, long inserted_option, DateTime? update_datetime, long employee_number, long decline_number, string move_decline_code, long is_reject_with_qty, long is_reject_all_invoice, long is_automatic_applied, long is_red_stamp, decimal original_quantity, decimal move_actual_quantity, decimal reject_quantity, decimal move_original_price, decimal actual_price, long b2b_msg_decline_ref, long b2b_status, long distributor_number, string details, long b2b_decline_types_is_active, string max_line_number)
        {
			this._display_name = display_name;
			this._checkedField = checkedField;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._order_number = order_number;
			this._parent_doc_type = parent_doc_type;
			this._parent_doc_number = parent_doc_number;
			this._material_number = material_number;
			this._line_number = line_number;
			this._parent_serial_number = parent_serial_number;
			this._status = status;
			this._inserted_option = inserted_option;
			this._update_datetime = update_datetime;
			this._employee_number = employee_number;
			this._decline_number = decline_number;
			this._move_decline_code = move_decline_code;
			this._is_reject_with_qty = is_reject_with_qty;
			this._is_reject_all_invoice = is_reject_all_invoice;
			this._is_automatic_applied = is_automatic_applied;
			this._is_red_stamp = is_red_stamp;
			this._original_quantity = original_quantity;
			this._move_actual_quantity = move_actual_quantity;
			this._reject_quantity = reject_quantity;
			this._move_original_price = move_original_price;
			this._actual_price = actual_price;
			this._b2b_msg_decline_ref = b2b_msg_decline_ref;
			this._b2b_status = b2b_status;
			this._distributor_number = distributor_number;
			this._details = details;
			this._b2b_decline_types_is_active = b2b_decline_types_is_active;
			this._max_line_number = max_line_number;
		}

	
        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
        private string _display_name;

        /// <summary>
        /// Gets or sets the display_name.
        /// </summary>
        /// <value>
        /// The display_name.
        /// </value>
	    [Editable(true)]
        [Column("display_name")]
        [Properties("display_name", "",true,0,typeof(string),125)]
        public string display_name
        {
            get { return _display_name; }
            set
            {
                if (_display_name != value)
                {
                    PropertyChanged("display_name");
					_display_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the checkedField.
        /// </summary>
        /// <value>
        /// The checkedField.
        /// </value>
        private long _checkedField;

        /// <summary>
        /// Gets or sets the checkedField.
        /// </summary>
        /// <value>
        /// The checkedField.
        /// </value>
	    [Editable(true)]
        [Column("checkedField")]
        [Properties("checkedField", "", true, 2, typeof(bool),30)]
        public long checkedField
        {
            get { return _checkedField; }
            set
            {
                if (_checkedField != value)
                {
                    PropertyChanged("checkedField");
					_checkedField = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parent_doc_type.
        /// </summary>
        /// <value>
        /// The parent_doc_type.
        /// </value>
        private string _parent_doc_type;

        /// <summary>
        /// Gets or sets the parent_doc_type.
        /// </summary>
        /// <value>
        /// The parent_doc_type.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("parent_doc_type")]
		public string parent_doc_type
        {
            get { return _parent_doc_type; }
            set
            {
                if (_parent_doc_type != value)
                {
                    PropertyChanged("parent_doc_type");
					_parent_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parent_doc_number.
        /// </summary>
        /// <value>
        /// The parent_doc_number.
        /// </value>
        private long _parent_doc_number;

        /// <summary>
        /// Gets or sets the parent_doc_number.
        /// </summary>
        /// <value>
        /// The parent_doc_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("parent_doc_number")]
		public long parent_doc_number
        {
            get { return _parent_doc_number; }
            set
            {
                if (_parent_doc_number != value)
                {
                    PropertyChanged("parent_doc_number");
					_parent_doc_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
        private long _line_number;

        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("line_number")]
		public long line_number
        {
            get { return _line_number; }
            set
            {
                if (_line_number != value)
                {
                    PropertyChanged("line_number");
					_line_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the parent_serial_number.
        /// </summary>
        /// <value>
        /// The parent_serial_number.
        /// </value>
        private long _parent_serial_number;

        /// <summary>
        /// Gets or sets the parent_serial_number.
        /// </summary>
        /// <value>
        /// The parent_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("parent_serial_number")]
		public long parent_serial_number
        {
            get { return _parent_serial_number; }
            set
            {
                if (_parent_serial_number != value)
                {
                    PropertyChanged("parent_serial_number");
					_parent_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        private string _status;

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
	    [Editable(true)]
        [Column("status")]
		public string status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    PropertyChanged("status");
					_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the inserted_option.
        /// </summary>
        /// <value>
        /// The inserted_option.
        /// </value>
        private long _inserted_option;

        /// <summary>
        /// Gets or sets the inserted_option.
        /// </summary>
        /// <value>
        /// The inserted_option.
        /// </value>
	    [Editable(true)]
        [Column("inserted_option")]
		public long inserted_option
        {
            get { return _inserted_option; }
            set
            {
                if (_inserted_option != value)
                {
                    PropertyChanged("inserted_option");
					_inserted_option = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the update_datetime.
        /// </summary>
        /// <value>
        /// The update_datetime.
        /// </value>
        private DateTime? _update_datetime;

        /// <summary>
        /// Gets or sets the update_datetime.
        /// </summary>
        /// <value>
        /// The update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("update_datetime")]
		public DateTime? update_datetime
        {
            get { return _update_datetime; }
            set
            {
                if (_update_datetime != value)
                {
                    PropertyChanged("update_datetime");
					_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
	    [Editable(true)]
        [Column("decline_number")]
		public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the move_decline_code.
        /// </summary>
        /// <value>
        /// The move_decline_code.
        /// </value>
        private string _move_decline_code;

        /// <summary>
        /// Gets or sets the move_decline_code.
        /// </summary>
        /// <value>
        /// The move_decline_code.
        /// </value>
	    [Editable(true)]
        [Column("move_decline_code")]
		public string move_decline_code
        {
            get { return _move_decline_code; }
            set
            {
                if (_move_decline_code != value)
                {
                    PropertyChanged("move_decline_code");
					_move_decline_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_reject_with_qty.
        /// </summary>
        /// <value>
        /// The is_reject_with_qty.
        /// </value>
        private long _is_reject_with_qty;

        /// <summary>
        /// Gets or sets the is_reject_with_qty.
        /// </summary>
        /// <value>
        /// The is_reject_with_qty.
        /// </value>
	    [Editable(true)]
        [Column("is_reject_with_qty")]
		public long is_reject_with_qty
        {
            get { return _is_reject_with_qty; }
            set
            {
                if (_is_reject_with_qty != value)
                {
                    PropertyChanged("is_reject_with_qty");
					_is_reject_with_qty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_reject_all_invoice.
        /// </summary>
        /// <value>
        /// The is_reject_all_invoice.
        /// </value>
        private long _is_reject_all_invoice;

        /// <summary>
        /// Gets or sets the is_reject_all_invoice.
        /// </summary>
        /// <value>
        /// The is_reject_all_invoice.
        /// </value>
	    [Editable(true)]
        [Column("is_reject_all_invoice")]
		public long is_reject_all_invoice
        {
            get { return _is_reject_all_invoice; }
            set
            {
                if (_is_reject_all_invoice != value)
                {
                    PropertyChanged("is_reject_all_invoice");
					_is_reject_all_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_automatic_applied.
        /// </summary>
        /// <value>
        /// The is_automatic_applied.
        /// </value>
        private long _is_automatic_applied;

        /// <summary>
        /// Gets or sets the is_automatic_applied.
        /// </summary>
        /// <value>
        /// The is_automatic_applied.
        /// </value>
	    [Editable(true)]
        [Column("is_automatic_applied")]
		public long is_automatic_applied
        {
            get { return _is_automatic_applied; }
            set
            {
                if (_is_automatic_applied != value)
                {
                    PropertyChanged("is_automatic_applied");
					_is_automatic_applied = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_red_stamp.
        /// </summary>
        /// <value>
        /// The is_red_stamp.
        /// </value>
        private long _is_red_stamp;

        /// <summary>
        /// Gets or sets the is_red_stamp.
        /// </summary>
        /// <value>
        /// The is_red_stamp.
        /// </value>
	    [Editable(true)]
        [Column("is_red_stamp")]
		public long is_red_stamp
        {
            get { return _is_red_stamp; }
            set
            {
                if (_is_red_stamp != value)
                {
                    PropertyChanged("is_red_stamp");
					_is_red_stamp = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the original_quantity.
        /// </summary>
        /// <value>
        /// The original_quantity.
        /// </value>
        private decimal _original_quantity;

        /// <summary>
        /// Gets or sets the original_quantity.
        /// </summary>
        /// <value>
        /// The original_quantity.
        /// </value>
	    [Editable(true)]
        [Column("original_quantity")]
		public decimal original_quantity
        {
            get { return _original_quantity; }
            set
            {
                if (_original_quantity != value)
                {
                    PropertyChanged("original_quantity");
					_original_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the move_actual_quantity.
        /// </summary>
        /// <value>
        /// The move_actual_quantity.
        /// </value>
        private decimal _move_actual_quantity;

        /// <summary>
        /// Gets or sets the move_actual_quantity.
        /// </summary>
        /// <value>
        /// The move_actual_quantity.
        /// </value>
	    [Editable(true)]
        [Column("move_actual_quantity")]
		public decimal move_actual_quantity
        {
            get { return _move_actual_quantity; }
            set
            {
                if (_move_actual_quantity != value)
                {
                    PropertyChanged("move_actual_quantity");
					_move_actual_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the reject_quantity.
        /// </summary>
        /// <value>
        /// The reject_quantity.
        /// </value>
        private decimal _reject_quantity;

        /// <summary>
        /// Gets or sets the reject_quantity.
        /// </summary>
        /// <value>
        /// The reject_quantity.
        /// </value>
	    [Editable(true)]
        [Column("reject_quantity")]
        [Properties("reject_quantity", "", true, 1, typeof(decimal),60)]
        [NumberField()]
        public decimal reject_quantity
        {
            get { return _reject_quantity; }
            set
            {
                if (_reject_quantity != value)
                {
                    PropertyChanged("reject_quantity");
					_reject_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the move_original_price.
        /// </summary>
        /// <value>
        /// The move_original_price.
        /// </value>
        private decimal _move_original_price;

        /// <summary>
        /// Gets or sets the move_original_price.
        /// </summary>
        /// <value>
        /// The move_original_price.
        /// </value>
	    [Editable(true)]
        [Column("move_original_price")]
		public decimal move_original_price
        {
            get { return _move_original_price; }
            set
            {
                if (_move_original_price != value)
                {
                    PropertyChanged("move_original_price");
					_move_original_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the actual_price.
        /// </summary>
        /// <value>
        /// The actual_price.
        /// </value>
        private decimal _actual_price;

        /// <summary>
        /// Gets or sets the actual_price.
        /// </summary>
        /// <value>
        /// The actual_price.
        /// </value>
	    [Editable(true)]
        [Column("actual_price")]
		public decimal actual_price
        {
            get { return _actual_price; }
            set
            {
                if (_actual_price != value)
                {
                    PropertyChanged("actual_price");
					_actual_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_msg_decline_ref.
        /// </summary>
        /// <value>
        /// The b2b_msg_decline_ref.
        /// </value>
        private long _b2b_msg_decline_ref;

        /// <summary>
        /// Gets or sets the b2b_msg_decline_ref.
        /// </summary>
        /// <value>
        /// The b2b_msg_decline_ref.
        /// </value>
	    [Editable(true)]
        [Column("b2b_msg_decline_ref")]
		public long b2b_msg_decline_ref
        {
            get { return _b2b_msg_decline_ref; }
            set
            {
                if (_b2b_msg_decline_ref != value)
                {
                    PropertyChanged("b2b_msg_decline_ref");
					_b2b_msg_decline_ref = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
		public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_decline_types_is_active.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_is_active.
        /// </value>
        private long _b2b_decline_types_is_active;

        /// <summary>
        /// Gets or sets the b2b_decline_types_is_active.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_is_active.
        /// </value>
	    [Editable(true)]
        [Column("b2b_decline_types_is_active")]
		public long b2b_decline_types_is_active
        {
            get { return _b2b_decline_types_is_active; }
            set
            {
                if (_b2b_decline_types_is_active != value)
                {
                    PropertyChanged("b2b_decline_types_is_active");
					_b2b_decline_types_is_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the max_line_number.
        /// </summary>
        /// <value>
        /// The max_line_number.
        /// </value>
        private string _max_line_number;

        /// <summary>
        /// Gets or sets the max_line_number.
        /// </summary>
        /// <value>
        /// The max_line_number.
        /// </value>
	    [Editable(true)]
        [Column("max_line_number")]
		public string max_line_number
        {
            get { return _max_line_number; }
            set
            {
                if (_max_line_number != value)
                {
                    PropertyChanged("max_line_number");
					_max_line_number = value;
                    
                }
            }
        }
	}
}
