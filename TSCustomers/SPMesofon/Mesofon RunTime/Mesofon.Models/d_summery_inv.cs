using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_summery_inv : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_summery_inv()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_details_branch_number">The invoice_details_branch_number.</param>
		/// <param name="doc_no">The doc_no.</param>
		/// <param name="invoice_move_branch_number">The invoice_move_branch_number.</param>
		/// <param name="invoice_move_supplier_number">The invoice_move_supplier_number.</param>
		/// <param name="invoice_move_invoice_type">The invoice_move_invoice_type.</param>
		/// <param name="invoice_details_packing_list_number">The invoice_details_packing_list_number.</param>
		/// <param name="total">The total.</param>
        public d_summery_inv(long invoice_details_branch_number, long doc_no, long invoice_move_branch_number, long invoice_move_supplier_number, string invoice_move_invoice_type, long invoice_details_packing_list_number, decimal total)
        {
			this._invoice_details_branch_number = invoice_details_branch_number;
			this._doc_no = doc_no;
			this._invoice_move_branch_number = invoice_move_branch_number;
			this._invoice_move_supplier_number = invoice_move_supplier_number;
			this._invoice_move_invoice_type = invoice_move_invoice_type;
			this._invoice_details_packing_list_number = invoice_details_packing_list_number;
			this._total = total;
		}

	
        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
        private long _invoice_details_branch_number;

        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_branch_number")]
		public long invoice_details_branch_number
        {
            get { return _invoice_details_branch_number; }
            set
            {
                if (_invoice_details_branch_number != value)
                {
                    PropertyChanged("invoice_details_branch_number");
					_invoice_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
        private long _doc_no;

        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
	    [Editable(true)]
        [Column("doc_no")]
		public long doc_no
        {
            get { return _doc_no; }
            set
            {
                if (_doc_no != value)
                {
                    PropertyChanged("doc_no");
					_doc_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_move_branch_number.
        /// </value>
        private long _invoice_move_branch_number;

        /// <summary>
        /// Gets or sets the invoice_move_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_move_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_branch_number")]
		public long invoice_move_branch_number
        {
            get { return _invoice_move_branch_number; }
            set
            {
                if (_invoice_move_branch_number != value)
                {
                    PropertyChanged("invoice_move_branch_number");
					_invoice_move_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_move_supplier_number.
        /// </value>
        private long _invoice_move_supplier_number;

        /// <summary>
        /// Gets or sets the invoice_move_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_move_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_supplier_number")]
		public long invoice_move_supplier_number
        {
            get { return _invoice_move_supplier_number; }
            set
            {
                if (_invoice_move_supplier_number != value)
                {
                    PropertyChanged("invoice_move_supplier_number");
					_invoice_move_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_type.
        /// </value>
        private string _invoice_move_invoice_type;

        /// <summary>
        /// Gets or sets the invoice_move_invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_move_invoice_type.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_invoice_type")]
		public string invoice_move_invoice_type
        {
            get { return _invoice_move_invoice_type; }
            set
            {
                if (_invoice_move_invoice_type != value)
                {
                    PropertyChanged("invoice_move_invoice_type");
					_invoice_move_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_packing_list_number.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_list_number.
        /// </value>
        private long _invoice_details_packing_list_number;

        /// <summary>
        /// Gets or sets the invoice_details_packing_list_number.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_packing_list_number")]
		public long invoice_details_packing_list_number
        {
            get { return _invoice_details_packing_list_number; }
            set
            {
                if (_invoice_details_packing_list_number != value)
                {
                    PropertyChanged("invoice_details_packing_list_number");
					_invoice_details_packing_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
        private decimal _total;

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
	    [Editable(true)]
        [Column("total")]
		public decimal total
        {
            get { return _total; }
            set
            {
                if (_total != value)
                {
                    PropertyChanged("total");
					_total = value;
                    
                }
            }
        }
	}
}
