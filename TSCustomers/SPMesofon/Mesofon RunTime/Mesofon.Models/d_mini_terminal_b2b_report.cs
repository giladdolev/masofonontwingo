using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_b2b_report : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_b2b_report()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="doc_type">The doc_type.</param>
		/// <param name="doc_no">The doc_no.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_name">The materials_name.</param>
		/// <param name="min_valid_months">The min_valid_months.</param>
		/// <param name="invoice_details_expiration_date">The invoice_details_expiration_date.</param>
		/// <param name="ok_valid_months">The ok_valid_months.</param>
		/// <param name="order_quantity_1">The order_quantity_1.</param>
		/// <param name="mt_quantity_1">The mt_quantity_1.</param>
		/// <param name="diff_1">The diff_1.</param>
		/// <param name="order_quantity_2">The order_quantity_2.</param>
		/// <param name="inv_quantity_2">The inv_quantity_2.</param>
		/// <param name="diff_2">The diff_2.</param>
		/// <param name="inv_quantity_3">The inv_quantity_3.</param>
		/// <param name="mt_quantity_3">The mt_quantity_3.</param>
		/// <param name="diff_3">The diff_3.</param>
		/// <param name="date">The date.</param>
		/// <param name="compute">The compute.</param>
		/// <param name="row">The row.</param>
		/// <param name="sum_inv_quantity_3">The sum_inv_quantity_3.</param>
		/// <param name="sum_mt_quantity_3">The sum_mt_quantity_3.</param>
		/// <param name="sum_diff_3">The sum_diff_3.</param>
		/// <param name="sum_diff_1">The sum_diff_1.</param>
		/// <param name="sum_order_quantity_2">The sum_order_quantity_2.</param>
		/// <param name="sum_inv_quantity_2">The sum_inv_quantity_2.</param>
		/// <param name="sum_diff_2">The sum_diff_2.</param>
		/// <param name="sum_order_quantity_1">The sum_order_quantity_1.</param>
		/// <param name="sum_mt_quantity_1">The sum_mt_quantity_1.</param>
        public d_mini_terminal_b2b_report(long branch_number, string branch_name, long supplier_number, string supplier_name, long order_number, string doc_type, long doc_no, string employee_name, string materials_barcode, string materials_name, DateTime? min_valid_months, DateTime? invoice_details_expiration_date, string ok_valid_months, decimal order_quantity_1, decimal mt_quantity_1, decimal diff_1, decimal order_quantity_2, decimal inv_quantity_2, decimal diff_2, decimal inv_quantity_3, decimal mt_quantity_3, decimal diff_3, string date, string compute, string row, string sum_inv_quantity_3, string sum_mt_quantity_3, string sum_diff_3, string sum_diff_1, string sum_order_quantity_2, string sum_inv_quantity_2, string sum_diff_2, string sum_order_quantity_1, string sum_mt_quantity_1)
        {
			this._branch_number = branch_number;
			this._branch_name = branch_name;
			this._supplier_number = supplier_number;
			this._supplier_name = supplier_name;
			this._order_number = order_number;
			this._doc_type = doc_type;
			this._doc_no = doc_no;
			this._employee_name = employee_name;
			this._materials_barcode = materials_barcode;
			this._materials_name = materials_name;
			this._min_valid_months = min_valid_months;
			this._invoice_details_expiration_date = invoice_details_expiration_date;
			this._ok_valid_months = ok_valid_months;
			this._order_quantity_1 = order_quantity_1;
			this._mt_quantity_1 = mt_quantity_1;
			this._diff_1 = diff_1;
			this._order_quantity_2 = order_quantity_2;
			this._inv_quantity_2 = inv_quantity_2;
			this._diff_2 = diff_2;
			this._inv_quantity_3 = inv_quantity_3;
			this._mt_quantity_3 = mt_quantity_3;
			this._diff_3 = diff_3;
			this._date = date;
			this._compute = compute;
			this._row = row;
			this._sum_inv_quantity_3 = sum_inv_quantity_3;
			this._sum_mt_quantity_3 = sum_mt_quantity_3;
			this._sum_diff_3 = sum_diff_3;
			this._sum_diff_1 = sum_diff_1;
			this._sum_order_quantity_2 = sum_order_quantity_2;
			this._sum_inv_quantity_2 = sum_inv_quantity_2;
			this._sum_diff_2 = sum_diff_2;
			this._sum_order_quantity_1 = sum_order_quantity_1;
			this._sum_mt_quantity_1 = sum_mt_quantity_1;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
        [Properties("branch_number", "branch_number", true, 0, typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]
        public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
        [Properties("branch_name", "branch_name", true, 1, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [Properties("supplier_number", "supplier_number", true, 2, typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]
        public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
        [Properties("supplier_name", "supplier_name", true, 3, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
        [Properties("order_number", "order_number", true, 4, typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]
        public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
	    [Editable(true)]
        [Column("doc_type")]
        [Properties("doc_type", "doc_type", true, 5, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
					_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
        private long _doc_no;

        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
	    [Editable(true)]
        [Column("doc_no")]
        [Properties("doc_no", "doc_no", true, 6, typeof(long))]
        [XMLOutput(XMLOutputLocation.Header)]
        public long doc_no
        {
            get { return _doc_no; }
            set
            {
                if (_doc_no != value)
                {
                    PropertyChanged("doc_no");
					_doc_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
        [Properties("employee_name", "employee_name", true, 7, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
        [Properties("materials_barcode", "materials_barcode", true, 8, typeof(string))]
        public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
        private string _materials_name;

        /// <summary>
        /// Gets or sets the materials_name.
        /// </summary>
        /// <value>
        /// The materials_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_name")]
        [Properties("materials_name", "materials_name", true, 9, typeof(string))]
        public string materials_name
        {
            get { return _materials_name; }
            set
            {
                if (_materials_name != value)
                {
                    PropertyChanged("materials_name");
					_materials_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the min_valid_months.
        /// </summary>
        /// <value>
        /// The min_valid_months.
        /// </value>
        private DateTime? _min_valid_months;

        /// <summary>
        /// Gets or sets the min_valid_months.
        /// </summary>
        /// <value>
        /// The min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("min_valid_months")]
        [Properties("min_valid_months", "min_valid_months", false, 10, typeof(DateTime))]
        public DateTime? min_valid_months
        {
            get { return _min_valid_months; }
            set
            {
                if (_min_valid_months != value)
                {
                    PropertyChanged("min_valid_months");
					_min_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
        private DateTime? _invoice_details_expiration_date;

        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_expiration_date")]
        [Properties("invoice_details_expiration_date", "invoice_details_expiration_date", false, 11, typeof(DateTime))]
        public DateTime? invoice_details_expiration_date
        {
            get { return _invoice_details_expiration_date; }
            set
            {
                if (_invoice_details_expiration_date != value)
                {
                    PropertyChanged("invoice_details_expiration_date");
					_invoice_details_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
        private string _ok_valid_months;

        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("ok_valid_months")]
        [Properties("ok_valid_months", "ok_valid_months", false, 12, typeof(string))]
        public string ok_valid_months
        {
            get { return _ok_valid_months; }
            set
            {
                if (_ok_valid_months != value)
                {
                    PropertyChanged("ok_valid_months");
					_ok_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity_1.
        /// </summary>
        /// <value>
        /// The order_quantity_1.
        /// </value>
        private decimal _order_quantity_1;

        /// <summary>
        /// Gets or sets the order_quantity_1.
        /// </summary>
        /// <value>
        /// The order_quantity_1.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity_1")]
        [Properties("order_quantity_1", "order_quantity_1", true, 13, typeof(decimal))]
        public decimal order_quantity_1
        {
            get { return _order_quantity_1; }
            set
            {
                if (_order_quantity_1 != value)
                {
                    PropertyChanged("order_quantity_1");
					_order_quantity_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mt_quantity_1.
        /// </summary>
        /// <value>
        /// The mt_quantity_1.
        /// </value>
        private decimal _mt_quantity_1;

        /// <summary>
        /// Gets or sets the mt_quantity_1.
        /// </summary>
        /// <value>
        /// The mt_quantity_1.
        /// </value>
	    [Editable(true)]
        [Column("mt_quantity_1")]
        [Properties("mt_quantity_1", "mt_quantity_1", true, 14, typeof(decimal))]
        public decimal mt_quantity_1
        {
            get { return _mt_quantity_1; }
            set
            {
                if (_mt_quantity_1 != value)
                {
                    PropertyChanged("mt_quantity_1");
					_mt_quantity_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the diff_1.
        /// </summary>
        /// <value>
        /// The diff_1.
        /// </value>
        private decimal _diff_1;

        /// <summary>
        /// Gets or sets the diff_1.
        /// </summary>
        /// <value>
        /// The diff_1.
        /// </value>
	    [Editable(true)]
        [Column("diff_1")]
        [Properties("diff_1", "diff_1", true, 15, typeof(decimal))]
        public decimal diff_1
        {
            get { return _diff_1; }
            set
            {
                if (_diff_1 != value)
                {
                    PropertyChanged("diff_1");
					_diff_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity_2.
        /// </summary>
        /// <value>
        /// The order_quantity_2.
        /// </value>
        private decimal _order_quantity_2;

        /// <summary>
        /// Gets or sets the order_quantity_2.
        /// </summary>
        /// <value>
        /// The order_quantity_2.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity_2")]
        [Properties("order_quantity_2", "order_quantity_2", true, 16, typeof(decimal))]
        public decimal order_quantity_2
        {
            get { return _order_quantity_2; }
            set
            {
                if (_order_quantity_2 != value)
                {
                    PropertyChanged("order_quantity_2");
					_order_quantity_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the inv_quantity_2.
        /// </summary>
        /// <value>
        /// The inv_quantity_2.
        /// </value>
        private decimal _inv_quantity_2;

        /// <summary>
        /// Gets or sets the inv_quantity_2.
        /// </summary>
        /// <value>
        /// The inv_quantity_2.
        /// </value>
	    [Editable(true)]
        [Column("inv_quantity_2")]
        [Properties("inv_quantity_2", "inv_quantity_2", true, 17, typeof(decimal))]
        public decimal inv_quantity_2
        {
            get { return _inv_quantity_2; }
            set
            {
                if (_inv_quantity_2 != value)
                {
                    PropertyChanged("inv_quantity_2");
					_inv_quantity_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the diff_2.
        /// </summary>
        /// <value>
        /// The diff_2.
        /// </value>
        private decimal _diff_2;

        /// <summary>
        /// Gets or sets the diff_2.
        /// </summary>
        /// <value>
        /// The diff_2.
        /// </value>
	    [Editable(true)]
        [Column("diff_2")]
        [Properties("diff_2", "diff_2", true, 18, typeof(decimal))]
        public decimal diff_2
        {
            get { return _diff_2; }
            set
            {
                if (_diff_2 != value)
                {
                    PropertyChanged("diff_2");
					_diff_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the inv_quantity_3.
        /// </summary>
        /// <value>
        /// The inv_quantity_3.
        /// </value>
        private decimal _inv_quantity_3;

        /// <summary>
        /// Gets or sets the inv_quantity_3.
        /// </summary>
        /// <value>
        /// The inv_quantity_3.
        /// </value>
	    [Editable(true)]
        [Column("inv_quantity_3")]
        [Properties("inv_quantity_3", "inv_quantity_3", true, 19, typeof(decimal))]
        public decimal inv_quantity_3
        {
            get { return _inv_quantity_3; }
            set
            {
                if (_inv_quantity_3 != value)
                {
                    PropertyChanged("inv_quantity_3");
					_inv_quantity_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mt_quantity_3.
        /// </summary>
        /// <value>
        /// The mt_quantity_3.
        /// </value>
        private decimal _mt_quantity_3;

        /// <summary>
        /// Gets or sets the mt_quantity_3.
        /// </summary>
        /// <value>
        /// The mt_quantity_3.
        /// </value>
	    [Editable(true)]
        [Column("mt_quantity_3")]
        [Properties("mt_quantity_3", "mt_quantity_3", true, 20, typeof(decimal))]
        public decimal mt_quantity_3
        {
            get { return _mt_quantity_3; }
            set
            {
                if (_mt_quantity_3 != value)
                {
                    PropertyChanged("mt_quantity_3");
					_mt_quantity_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the diff_3.
        /// </summary>
        /// <value>
        /// The diff_3.
        /// </value>
        private decimal _diff_3;

        /// <summary>
        /// Gets or sets the diff_3.
        /// </summary>
        /// <value>
        /// The diff_3.
        /// </value>
	    [Editable(true)]
        [Column("diff_3")]
        [Properties("diff_3", "diff_3", true, 21, typeof(decimal))]
        public decimal diff_3
        {
            get { return _diff_3; }
            set
            {
                if (_diff_3 != value)
                {
                    PropertyChanged("diff_3");
					_diff_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        private string _date;

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
	    [Editable(true)]
        [Column("date")]
        [Properties("date", "date", true, 22, typeof(string))]
        [XMLOutput(XMLOutputLocation.Header)]
        public string date
        {
            get { return DateTime.Now.ToString("dd/MM/yyyy"); }
            set
            {
                if (_date != value)
                {
                    PropertyChanged("date");
					_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute.
        /// </summary>
        /// <value>
        /// The compute.
        /// </value>
        private string _compute;

        /// <summary>
        /// Gets or sets the compute.
        /// </summary>
        /// <value>
        /// The compute.
        /// </value>
	    [Editable(true)]
        [Column("compute")]
        [Properties("compute", "compute", true, 23, typeof(string))]
        public string compute
        {
            get { return _compute; }
            set
            {
                if (_compute != value)
                {
                    PropertyChanged("compute");
					_compute = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
        [Properties("row", "row", true, 24, typeof(string))]
        [TextExpressionAttribute("getrow()", "", "")]
        public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    PropertyChanged("row");
					_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_inv_quantity_3.
        /// </summary>
        /// <value>
        /// The sum_inv_quantity_3.
        /// </value>
        private string _sum_inv_quantity_3;

        /// <summary>
        /// Gets or sets the sum_inv_quantity_3.
        /// </summary>
        /// <value>
        /// The sum_inv_quantity_3.
        /// </value>
	    [Editable(true)]
        [Column("sum_inv_quantity_3")]
        [Properties("sum_inv_quantity_3", "sum_inv_quantity_3", true, 25, typeof(string))]
        [Compute("sum_inv_quantity_3", "SUMGROUP order_number inv_quantity_3")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_inv_quantity_3
        {
            get { return _sum_inv_quantity_3; }
            set
            {
                if (_sum_inv_quantity_3 != value)
                {
                    PropertyChanged("sum_inv_quantity_3");
					_sum_inv_quantity_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_mt_quantity_3.
        /// </summary>
        /// <value>
        /// The sum_mt_quantity_3.
        /// </value>
        private string _sum_mt_quantity_3;

        /// <summary>
        /// Gets or sets the sum_mt_quantity_3.
        /// </summary>
        /// <value>
        /// The sum_mt_quantity_3.
        /// </value>
	    [Editable(true)]
        [Column("sum_mt_quantity_3")]
        [Properties("sum_mt_quantity_3", "sum_mt_quantity_3", true, 26, typeof(string))]
        [Compute("sum_mt_quantity_3", "SUMGROUP order_number mt_quantity_3")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_mt_quantity_3
        {
            get { return _sum_mt_quantity_3; }
            set
            {
                if (_sum_mt_quantity_3 != value)
                {
                    PropertyChanged("sum_mt_quantity_3");
					_sum_mt_quantity_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_diff_3.
        /// </summary>
        /// <value>
        /// The sum_diff_3.
        /// </value>
        private string _sum_diff_3;

        /// <summary>
        /// Gets or sets the sum_diff_3.
        /// </summary>
        /// <value>
        /// The sum_diff_3.
        /// </value>
	    [Editable(true)]
        [Column("sum_diff_3")]
        [Properties("sum_diff_3", "sum_diff_3", true, 27, typeof(string))]
        [Compute("sum_diff_3", "SUMGROUP order_number diff_3")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_diff_3
        {
            get { return _sum_diff_3; }
            set
            {
                if (_sum_diff_3 != value)
                {
                    PropertyChanged("sum_diff_3");
					_sum_diff_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_diff_1.
        /// </summary>
        /// <value>
        /// The sum_diff_1.
        /// </value>
        private string _sum_diff_1;

        /// <summary>
        /// Gets or sets the sum_diff_1.
        /// </summary>
        /// <value>
        /// The sum_diff_1.
        /// </value>
	    [Editable(true)]
        [Column("sum_diff_1")]
        [Properties("sum_diff_1", "sum_diff_1", true, 28, typeof(string))]
        [Compute("sum_diff_1", "SUMGROUP order_number diff_1")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_diff_1
        {
            get { return _sum_diff_1; }
            set
            {
                if (_sum_diff_1 != value)
                {
                    PropertyChanged("sum_diff_1");
					_sum_diff_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_order_quantity_2.
        /// </summary>
        /// <value>
        /// The sum_order_quantity_2.
        /// </value>
        private string _sum_order_quantity_2;

        /// <summary>
        /// Gets or sets the sum_order_quantity_2.
        /// </summary>
        /// <value>
        /// The sum_order_quantity_2.
        /// </value>
	    [Editable(true)]
        [Column("sum_order_quantity_2")]
        [Properties("sum_order_quantity_2", "sum_order_quantity_2", true, 29, typeof(string))]
        [Compute("sum_order_quantity_2", "SUMGROUP order_number order_quantity_2")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_order_quantity_2
        {
            get { return _sum_order_quantity_2; }
            set
            {
                if (_sum_order_quantity_2 != value)
                {
                    PropertyChanged("sum_order_quantity_2");
					_sum_order_quantity_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_inv_quantity_2.
        /// </summary>
        /// <value>
        /// The sum_inv_quantity_2.
        /// </value>
        private string _sum_inv_quantity_2;

        /// <summary>
        /// Gets or sets the sum_inv_quantity_2.
        /// </summary>
        /// <value>
        /// The sum_inv_quantity_2.
        /// </value>
	    [Editable(true)]
        [Column("sum_inv_quantity_2")]
        [Properties("sum_inv_quantity_2", "sum_inv_quantity_2", true, 30, typeof(string))]
        [Compute("sum_inv_quantity_2", "SUMGROUP order_number inv_quantity_2")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_inv_quantity_2
        {
            get { return _sum_inv_quantity_2; }
            set
            {
                if (_sum_inv_quantity_2 != value)
                {
                    PropertyChanged("sum_inv_quantity_2");
					_sum_inv_quantity_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_diff_2.
        /// </summary>
        /// <value>
        /// The sum_diff_2.
        /// </value>
        private string _sum_diff_2;

        /// <summary>
        /// Gets or sets the sum_diff_2.
        /// </summary>
        /// <value>
        /// The sum_diff_2.
        /// </value>
	    [Editable(true)]
        [Column("sum_diff_2")]
        [Properties("sum_diff_2", "sum_diff_2", true, 31, typeof(string))]
        [Compute("sum_diff_2", "SUMGROUP order_number diff_2")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_diff_2
        {
            get { return _sum_diff_2; }
            set
            {
                if (_sum_diff_2 != value)
                {
                    PropertyChanged("sum_diff_2");
					_sum_diff_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_order_quantity_1.
        /// </summary>
        /// <value>
        /// The sum_order_quantity_1.
        /// </value>
        private string _sum_order_quantity_1;

        /// <summary>
        /// Gets or sets the sum_order_quantity_1.
        /// </summary>
        /// <value>
        /// The sum_order_quantity_1.
        /// </value>
	    [Editable(true)]
        [Column("sum_order_quantity_1")]
        [Properties("sum_order_quantity_1", "sum_order_quantity_1", true, 32, typeof(string))]
        [Compute("sum_order_quantity_1", "SUMGROUP order_number order_quantity_1")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_order_quantity_1
        {
            get { return _sum_order_quantity_1; }
            set
            {
                if (_sum_order_quantity_1 != value)
                {
                    PropertyChanged("sum_order_quantity_1");
					_sum_order_quantity_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sum_mt_quantity_1.
        /// </summary>
        /// <value>
        /// The sum_mt_quantity_1.
        /// </value>
        private string _sum_mt_quantity_1;

        /// <summary>
        /// Gets or sets the sum_mt_quantity_1.
        /// </summary>
        /// <value>
        /// The sum_mt_quantity_1.
        /// </value>
	    [Editable(true)]
        [Column("sum_mt_quantity_1")]
        [Properties("sum_mt_quantity_1", "sum_mt_quantity_1", true, 33, typeof(string))]
        [Compute("sum_mt_quantity_1", "SUMGROUP order_number mt_quantity_1")]
        [XMLOutput(XMLOutputLocation.Summary)]
        public string sum_mt_quantity_1
        {
            get { return _sum_mt_quantity_1; }
            set
            {
                if (_sum_mt_quantity_1 != value)
                {
                    PropertyChanged("sum_mt_quantity_1");
					_sum_mt_quantity_1 = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
        private string _expired;

        /// <summary>
        /// Gets or sets the ok_valid_months.
        /// </summary>
        /// <value>
        /// The ok_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("Expired")]
        [Properties("Expired", "Expired", true, 34, typeof(string))]
        [TextExpression("invoice_details_expiration_date < min_valid_months", "���� ���", "")]
        public string Expired
        {
            get { return _expired; }
            set
            {
                if (_expired != value)
                {
                    _expired = value;

                }
            }
        }
    }
}
