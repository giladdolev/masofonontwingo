using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_marlog_return_cartons : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_marlog_return_cartons()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="carton_barcode">The carton_barcode.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="state">The state.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="marlog_number">The marlog_number.</param>
        public d_marlog_return_cartons(long branch_number, double shipment_number, string carton_barcode, long material_quantity, string state, DateTime? last_update_datetime, long marlog_number)
        {
			this._branch_number = branch_number;
			this._shipment_number = shipment_number;
			this._carton_barcode = carton_barcode;
			this._material_quantity = material_quantity;
			this._state = state;
			this._last_update_datetime = last_update_datetime;
			this._marlog_number = marlog_number;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("shipment_number")]
		public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
        private string _carton_barcode;

        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
	    [Editable(true)]
        [Column("carton_barcode")]
        [PropertiesAttribute("carton_barcode", "���� �����", true,0, typeof(string),140)]
        public string carton_barcode
        {
            get { return _carton_barcode; }
            set
            {
                if (_carton_barcode != value)
                {
                    PropertyChanged("carton_barcode");
					_carton_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private long _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
        [PropertiesAttribute("material_quantity", "����" , true, 2, typeof(long))]
        public long material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
        [PropertiesAttribute("state", "�����", true, 1)]
        public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
        private long _marlog_number;

        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("marlog_number")]
		public long marlog_number
        {
            get { return _marlog_number; }
            set
            {
                if (_marlog_number != value)
                {
                    PropertyChanged("marlog_number");
					_marlog_number = value;
                    
                }
            }
        }
	}
}
