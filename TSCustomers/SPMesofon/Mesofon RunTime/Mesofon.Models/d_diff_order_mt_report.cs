using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;
using System.Web.VisualTree.Elements;

namespace Mesofon.Models
{
	public class d_diff_order_mt_report : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_diff_order_mt_report()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="material_number">The material_number.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="name">The name.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="row">The row.</param>
        public d_diff_order_mt_report(long branch_number, long supplier_number, long order_number, long material_number, decimal order_quantity, decimal quantity_within_invoice, string name, string barcode, string row)
        {
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._order_number = order_number;
			this._material_number = material_number;
			this._order_quantity = order_quantity;
			this._quantity_within_invoice = quantity_within_invoice;
			this._name = name;
			this._barcode = barcode;
			this._row = row;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
        [Properties("order_number", "order_number", false, 6, typeof(long))]
        public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
        [Properties("order_quantity", "���� �������", false, 10, typeof(decimal),94)]
        public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }


        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity_t.
        /// </value>
        private string _order_quantity_t;

        /// <summary>
        /// Gets or sets the order_quantity_t.
        /// </summary>
        /// <value>
        /// The order_quantity_t.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity_t")]
        [Properties("order_quantity_t", "���� �������", false, 11, typeof(decimal), 94)]
        public string order_quantity_t
        {
            get { return _order_quantity_t; }
            set
            {
                if (_order_quantity_t != value)
                {
                    _order_quantity_t = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
		public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string _name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
	    [Editable(true)]
        [Column("name")]
        [Properties("name", "�� ����", false, 4, typeof(string))]
        public string name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    PropertyChanged("name");
					_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
        [Properties("barcode","�����",false, 5,typeof(string))]

        public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
        private string _row;

        /// <summary>
        /// Gets or sets the row.
        /// </summary>
        /// <value>
        /// The row.
        /// </value>
	    [Editable(true)]
        [Column("row")]
        [Properties("row", "��", true, 0, typeof(string) ,width: 39)]
        [TextExpressionAttribute("getrow()", "", "")]
        public string row
        {
            get { return _row; }
            set
            {
                if (_row != value)
                {
                    //PropertyChanged("row");
					_row = value;
                    
                }
            }
        }

        private string _HTMLContentProperty;
        [Properties("Barcode_Name", "����� ��� ����", true,1, typeof(WidgetColumn),160)]
        [WidgetColumn(typeof(LabelElement), "CssClass", "WidgetColumnLabelRightToLeft", "ID", "barcode" , "PixelTop", 2, "PixelWidth", 200, "PixelHeight", 22, "PixelLeft",0, "DataMember", "barcode")]
        [WidgetColumn(typeof(LabelElement), "CssClass", "WidgetColumnLabelNoWrap" , "ID", "name", "PixelTop", 25, "PixelWidth", 200, "PixelHeight", 22, "PixelLeft", 0,"DataMember", "name")]
        //[HTMLContentAttribute("<table style=\"width:100%;direction:rtl !important;white-space: nowrap;table-layout: fixed;\" ><tr><td style=\"border-bottom: 1px solid black; text-align:right;overflow: hidden; text-overflow: ellipsis;\" >{0}</td></tr><tr><td style=\"text-align:right;overflow: hidden; text-overflow: ellipsis;\" >{1}</td></tr> </table>", "barcode,name")]
        public string HTMLContentProperty
        {
            get { return _HTMLContentProperty; }
            set
            {
                if (_HTMLContentProperty != value)
                {
                    _HTMLContentProperty = value;

                }
            }
        }

        private string _HTMLContentPropertyOrder;
        [Properties("order_number_order_quantity", "����� �����", true, 2, typeof(WidgetColumn), width: 94)]
        [WidgetColumn(typeof(LabelElement), "CssClass", "WidgetColumnLabelRightToLeft", "ID", "barcode", "PixelTop", 2, "PixelWidth", 90, "PixelHeight", 22, "PixelLeft", 0, "DataMember", "order_number")]
        [WidgetColumn(typeof(LabelElement), "ID", "name", "PixelTop", 25, "PixelWidth", 90, "PixelHeight", 22, "PixelLeft", 0, "DataMember", "order_quantity_t")]
        //[HTMLContentAttribute("<table style=\"width:100%;direction:rtl !important;white-space: nowrap;table-layout: fixed;\" ><tr><td style=\"border-bottom: 1px solid black; text-align:right;overflow: hidden; text-overflow: ellipsis;\" >{0}</td></tr><tr><td style=\"text-align:right;overflow: hidden; text-overflow: ellipsis;\" >{1}</td></tr> </table>", "order_number,order_quantity")]

        public string HTMLContentPropertyOrder
        {
            get { return _HTMLContentPropertyOrder; }
            set
            {
                if (_HTMLContentPropertyOrder != value)
                {
                    _HTMLContentPropertyOrder = value;

                }
            }
        }
    }
}
