using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class lds_station_hardware_parameters : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public lds_station_hardware_parameters()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="report_db_userid">The report_db_userid.</param>
		/// <param name="report_db_password">The report_db_password.</param>
		/// <param name="target_db_userid">The target_db_userid.</param>
		/// <param name="target_db_password">The target_db_password.</param>
		/// <param name="printer_1">The printer_1.</param>
		/// <param name="printer_2">The printer_2.</param>
		/// <param name="printer_3">The printer_3.</param>
		/// <param name="printer_4">The printer_4.</param>
		/// <param name="printer_5">The printer_5.</param>
		/// <param name="printer_6">The printer_6.</param>
		/// <param name="printer_7">The printer_7.</param>
		/// <param name="printer_8">The printer_8.</param>
		/// <param name="printer_9">The printer_9.</param>
		/// <param name="printer_10">The printer_10.</param>
		/// <param name="printer_11">The printer_11.</param>
		/// <param name="printer_12">The printer_12.</param>
		/// <param name="printer_13">The printer_13.</param>
		/// <param name="printer_14">The printer_14.</param>
		/// <param name="printer_15">The printer_15.</param>
		/// <param name="printer_16">The printer_16.</param>
		/// <param name="printer_1_redirection">The printer_1_redirection.</param>
		/// <param name="printer_2_redirection">The printer_2_redirection.</param>
		/// <param name="printer_3_redirection">The printer_3_redirection.</param>
		/// <param name="printer_4_redirection">The printer_4_redirection.</param>
		/// <param name="printer_5_redirection">The printer_5_redirection.</param>
		/// <param name="printer_6_redirection">The printer_6_redirection.</param>
		/// <param name="printer_7_redirection">The printer_7_redirection.</param>
		/// <param name="printer_8_redirection">The printer_8_redirection.</param>
		/// <param name="printer_9_redirection">The printer_9_redirection.</param>
		/// <param name="printer_10_redirection">The printer_10_redirection.</param>
		/// <param name="printer_11_redirection">The printer_11_redirection.</param>
		/// <param name="printer_12_redirection">The printer_12_redirection.</param>
		/// <param name="printer_13_redirection">The printer_13_redirection.</param>
		/// <param name="printer_14_redirection">The printer_14_redirection.</param>
		/// <param name="printer_15_redirection">The printer_15_redirection.</param>
		/// <param name="printer_16_redirection">The printer_16_redirection.</param>
		/// <param name="drawer_first_open_active">The drawer_first_open_active.</param>
		/// <param name="drawer_second_open_active">The drawer_second_open_active.</param>
		/// <param name="drawer_check_status_active">The drawer_check_status_active.</param>
		/// <param name="drawer_status_return_logic_not">The drawer_status_return_logic_not.</param>
		/// <param name="drawer_on_printer">The drawer_on_printer.</param>
		/// <param name="drawer_hardware_device_type">The drawer_hardware_device_type.</param>
		/// <param name="customer_display_active">The customer_display_active.</param>
		/// <param name="barcode_reader_active">The barcode_reader_active.</param>
		/// <param name="msr_active">The msr_active.</param>
		/// <param name="msr_device_type">The msr_device_type.</param>
		/// <param name="msr_second_track_only">The msr_second_track_only.</param>
		/// <param name="msr_first_track_prefix">The msr_first_track_prefix.</param>
		/// <param name="msr_first_track_suffix">The msr_first_track_suffix.</param>
		/// <param name="msr_second_track_prefix">The msr_second_track_prefix.</param>
		/// <param name="msr_second_track_suffix">The msr_second_track_suffix.</param>
		/// <param name="msr_third_track_prefix">The msr_third_track_prefix.</param>
		/// <param name="msr_third_track_suffix">The msr_third_track_suffix.</param>
		/// <param name="msr_third_track_delay">The msr_third_track_delay.</param>
		/// <param name="msr_com_port">The msr_com_port.</param>
		/// <param name="queue_db_userid">The queue_db_userid.</param>
		/// <param name="queue_db_password">The queue_db_password.</param>
        public lds_station_hardware_parameters(string report_db_userid, string report_db_password, string target_db_userid, string target_db_password, long printer_1, long printer_2, long printer_3, long printer_4, long printer_5, long printer_6, long printer_7, long printer_8, long printer_9, long printer_10, long printer_11, long printer_12, long printer_13, long printer_14, long printer_15, long printer_16, long printer_1_redirection, long printer_2_redirection, long printer_3_redirection, long printer_4_redirection, long printer_5_redirection, long printer_6_redirection, long printer_7_redirection, long printer_8_redirection, long printer_9_redirection, long printer_10_redirection, long printer_11_redirection, long printer_12_redirection, long printer_13_redirection, long printer_14_redirection, long printer_15_redirection, long printer_16_redirection, double drawer_first_open_active, double drawer_second_open_active, double drawer_check_status_active, double drawer_status_return_logic_not, double drawer_on_printer, long drawer_hardware_device_type, double customer_display_active, double barcode_reader_active, double msr_active, long msr_device_type, double msr_second_track_only, string msr_first_track_prefix, string msr_first_track_suffix, string msr_second_track_prefix, string msr_second_track_suffix, string msr_third_track_prefix, string msr_third_track_suffix, decimal msr_third_track_delay, long msr_com_port, string queue_db_userid, string queue_db_password)
        {
			this._report_db_userid = report_db_userid;
			this._report_db_password = report_db_password;
			this._target_db_userid = target_db_userid;
			this._target_db_password = target_db_password;
			this._printer_1 = printer_1;
			this._printer_2 = printer_2;
			this._printer_3 = printer_3;
			this._printer_4 = printer_4;
			this._printer_5 = printer_5;
			this._printer_6 = printer_6;
			this._printer_7 = printer_7;
			this._printer_8 = printer_8;
			this._printer_9 = printer_9;
			this._printer_10 = printer_10;
			this._printer_11 = printer_11;
			this._printer_12 = printer_12;
			this._printer_13 = printer_13;
			this._printer_14 = printer_14;
			this._printer_15 = printer_15;
			this._printer_16 = printer_16;
			this._printer_1_redirection = printer_1_redirection;
			this._printer_2_redirection = printer_2_redirection;
			this._printer_3_redirection = printer_3_redirection;
			this._printer_4_redirection = printer_4_redirection;
			this._printer_5_redirection = printer_5_redirection;
			this._printer_6_redirection = printer_6_redirection;
			this._printer_7_redirection = printer_7_redirection;
			this._printer_8_redirection = printer_8_redirection;
			this._printer_9_redirection = printer_9_redirection;
			this._printer_10_redirection = printer_10_redirection;
			this._printer_11_redirection = printer_11_redirection;
			this._printer_12_redirection = printer_12_redirection;
			this._printer_13_redirection = printer_13_redirection;
			this._printer_14_redirection = printer_14_redirection;
			this._printer_15_redirection = printer_15_redirection;
			this._printer_16_redirection = printer_16_redirection;
			this._drawer_first_open_active = drawer_first_open_active;
			this._drawer_second_open_active = drawer_second_open_active;
			this._drawer_check_status_active = drawer_check_status_active;
			this._drawer_status_return_logic_not = drawer_status_return_logic_not;
			this._drawer_on_printer = drawer_on_printer;
			this._drawer_hardware_device_type = drawer_hardware_device_type;
			this._customer_display_active = customer_display_active;
			this._barcode_reader_active = barcode_reader_active;
			this._msr_active = msr_active;
			this._msr_device_type = msr_device_type;
			this._msr_second_track_only = msr_second_track_only;
			this._msr_first_track_prefix = msr_first_track_prefix;
			this._msr_first_track_suffix = msr_first_track_suffix;
			this._msr_second_track_prefix = msr_second_track_prefix;
			this._msr_second_track_suffix = msr_second_track_suffix;
			this._msr_third_track_prefix = msr_third_track_prefix;
			this._msr_third_track_suffix = msr_third_track_suffix;
			this._msr_third_track_delay = msr_third_track_delay;
			this._msr_com_port = msr_com_port;
			this._queue_db_userid = queue_db_userid;
			this._queue_db_password = queue_db_password;
		}

	
        /// <summary>
        /// Gets or sets the report_db_userid.
        /// </summary>
        /// <value>
        /// The report_db_userid.
        /// </value>
        private string _report_db_userid;

        /// <summary>
        /// Gets or sets the report_db_userid.
        /// </summary>
        /// <value>
        /// The report_db_userid.
        /// </value>
	    [Editable(true)]
        [Column("report_db_userid")]
		public string report_db_userid
        {
            get { return _report_db_userid; }
            set
            {
                if (_report_db_userid != value)
                {
                    PropertyChanged("report_db_userid");
					_report_db_userid = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the report_db_password.
        /// </summary>
        /// <value>
        /// The report_db_password.
        /// </value>
        private string _report_db_password;

        /// <summary>
        /// Gets or sets the report_db_password.
        /// </summary>
        /// <value>
        /// The report_db_password.
        /// </value>
	    [Editable(true)]
        [Column("report_db_password")]
		public string report_db_password
        {
            get { return _report_db_password; }
            set
            {
                if (_report_db_password != value)
                {
                    PropertyChanged("report_db_password");
					_report_db_password = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_userid.
        /// </summary>
        /// <value>
        /// The target_db_userid.
        /// </value>
        private string _target_db_userid;

        /// <summary>
        /// Gets or sets the target_db_userid.
        /// </summary>
        /// <value>
        /// The target_db_userid.
        /// </value>
	    [Editable(true)]
        [Column("target_db_userid")]
		public string target_db_userid
        {
            get { return _target_db_userid; }
            set
            {
                if (_target_db_userid != value)
                {
                    PropertyChanged("target_db_userid");
					_target_db_userid = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_db_password.
        /// </summary>
        /// <value>
        /// The target_db_password.
        /// </value>
        private string _target_db_password;

        /// <summary>
        /// Gets or sets the target_db_password.
        /// </summary>
        /// <value>
        /// The target_db_password.
        /// </value>
	    [Editable(true)]
        [Column("target_db_password")]
		public string target_db_password
        {
            get { return _target_db_password; }
            set
            {
                if (_target_db_password != value)
                {
                    PropertyChanged("target_db_password");
					_target_db_password = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_1.
        /// </summary>
        /// <value>
        /// The printer_1.
        /// </value>
        private long _printer_1;

        /// <summary>
        /// Gets or sets the printer_1.
        /// </summary>
        /// <value>
        /// The printer_1.
        /// </value>
	    [Editable(true)]
        [Column("printer_1")]
		public long printer_1
        {
            get { return _printer_1; }
            set
            {
                if (_printer_1 != value)
                {
                    PropertyChanged("printer_1");
					_printer_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_2.
        /// </summary>
        /// <value>
        /// The printer_2.
        /// </value>
        private long _printer_2;

        /// <summary>
        /// Gets or sets the printer_2.
        /// </summary>
        /// <value>
        /// The printer_2.
        /// </value>
	    [Editable(true)]
        [Column("printer_2")]
		public long printer_2
        {
            get { return _printer_2; }
            set
            {
                if (_printer_2 != value)
                {
                    PropertyChanged("printer_2");
					_printer_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_3.
        /// </summary>
        /// <value>
        /// The printer_3.
        /// </value>
        private long _printer_3;

        /// <summary>
        /// Gets or sets the printer_3.
        /// </summary>
        /// <value>
        /// The printer_3.
        /// </value>
	    [Editable(true)]
        [Column("printer_3")]
		public long printer_3
        {
            get { return _printer_3; }
            set
            {
                if (_printer_3 != value)
                {
                    PropertyChanged("printer_3");
					_printer_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_4.
        /// </summary>
        /// <value>
        /// The printer_4.
        /// </value>
        private long _printer_4;

        /// <summary>
        /// Gets or sets the printer_4.
        /// </summary>
        /// <value>
        /// The printer_4.
        /// </value>
	    [Editable(true)]
        [Column("printer_4")]
		public long printer_4
        {
            get { return _printer_4; }
            set
            {
                if (_printer_4 != value)
                {
                    PropertyChanged("printer_4");
					_printer_4 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_5.
        /// </summary>
        /// <value>
        /// The printer_5.
        /// </value>
        private long _printer_5;

        /// <summary>
        /// Gets or sets the printer_5.
        /// </summary>
        /// <value>
        /// The printer_5.
        /// </value>
	    [Editable(true)]
        [Column("printer_5")]
		public long printer_5
        {
            get { return _printer_5; }
            set
            {
                if (_printer_5 != value)
                {
                    PropertyChanged("printer_5");
					_printer_5 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_6.
        /// </summary>
        /// <value>
        /// The printer_6.
        /// </value>
        private long _printer_6;

        /// <summary>
        /// Gets or sets the printer_6.
        /// </summary>
        /// <value>
        /// The printer_6.
        /// </value>
	    [Editable(true)]
        [Column("printer_6")]
		public long printer_6
        {
            get { return _printer_6; }
            set
            {
                if (_printer_6 != value)
                {
                    PropertyChanged("printer_6");
					_printer_6 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_7.
        /// </summary>
        /// <value>
        /// The printer_7.
        /// </value>
        private long _printer_7;

        /// <summary>
        /// Gets or sets the printer_7.
        /// </summary>
        /// <value>
        /// The printer_7.
        /// </value>
	    [Editable(true)]
        [Column("printer_7")]
		public long printer_7
        {
            get { return _printer_7; }
            set
            {
                if (_printer_7 != value)
                {
                    PropertyChanged("printer_7");
					_printer_7 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_8.
        /// </summary>
        /// <value>
        /// The printer_8.
        /// </value>
        private long _printer_8;

        /// <summary>
        /// Gets or sets the printer_8.
        /// </summary>
        /// <value>
        /// The printer_8.
        /// </value>
	    [Editable(true)]
        [Column("printer_8")]
		public long printer_8
        {
            get { return _printer_8; }
            set
            {
                if (_printer_8 != value)
                {
                    PropertyChanged("printer_8");
					_printer_8 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_9.
        /// </summary>
        /// <value>
        /// The printer_9.
        /// </value>
        private long _printer_9;

        /// <summary>
        /// Gets or sets the printer_9.
        /// </summary>
        /// <value>
        /// The printer_9.
        /// </value>
	    [Editable(true)]
        [Column("printer_9")]
		public long printer_9
        {
            get { return _printer_9; }
            set
            {
                if (_printer_9 != value)
                {
                    PropertyChanged("printer_9");
					_printer_9 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_10.
        /// </summary>
        /// <value>
        /// The printer_10.
        /// </value>
        private long _printer_10;

        /// <summary>
        /// Gets or sets the printer_10.
        /// </summary>
        /// <value>
        /// The printer_10.
        /// </value>
	    [Editable(true)]
        [Column("printer_10")]
		public long printer_10
        {
            get { return _printer_10; }
            set
            {
                if (_printer_10 != value)
                {
                    PropertyChanged("printer_10");
					_printer_10 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_11.
        /// </summary>
        /// <value>
        /// The printer_11.
        /// </value>
        private long _printer_11;

        /// <summary>
        /// Gets or sets the printer_11.
        /// </summary>
        /// <value>
        /// The printer_11.
        /// </value>
	    [Editable(true)]
        [Column("printer_11")]
		public long printer_11
        {
            get { return _printer_11; }
            set
            {
                if (_printer_11 != value)
                {
                    PropertyChanged("printer_11");
					_printer_11 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_12.
        /// </summary>
        /// <value>
        /// The printer_12.
        /// </value>
        private long _printer_12;

        /// <summary>
        /// Gets or sets the printer_12.
        /// </summary>
        /// <value>
        /// The printer_12.
        /// </value>
	    [Editable(true)]
        [Column("printer_12")]
		public long printer_12
        {
            get { return _printer_12; }
            set
            {
                if (_printer_12 != value)
                {
                    PropertyChanged("printer_12");
					_printer_12 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_13.
        /// </summary>
        /// <value>
        /// The printer_13.
        /// </value>
        private long _printer_13;

        /// <summary>
        /// Gets or sets the printer_13.
        /// </summary>
        /// <value>
        /// The printer_13.
        /// </value>
	    [Editable(true)]
        [Column("printer_13")]
		public long printer_13
        {
            get { return _printer_13; }
            set
            {
                if (_printer_13 != value)
                {
                    PropertyChanged("printer_13");
					_printer_13 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_14.
        /// </summary>
        /// <value>
        /// The printer_14.
        /// </value>
        private long _printer_14;

        /// <summary>
        /// Gets or sets the printer_14.
        /// </summary>
        /// <value>
        /// The printer_14.
        /// </value>
	    [Editable(true)]
        [Column("printer_14")]
		public long printer_14
        {
            get { return _printer_14; }
            set
            {
                if (_printer_14 != value)
                {
                    PropertyChanged("printer_14");
					_printer_14 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_15.
        /// </summary>
        /// <value>
        /// The printer_15.
        /// </value>
        private long _printer_15;

        /// <summary>
        /// Gets or sets the printer_15.
        /// </summary>
        /// <value>
        /// The printer_15.
        /// </value>
	    [Editable(true)]
        [Column("printer_15")]
		public long printer_15
        {
            get { return _printer_15; }
            set
            {
                if (_printer_15 != value)
                {
                    PropertyChanged("printer_15");
					_printer_15 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_16.
        /// </summary>
        /// <value>
        /// The printer_16.
        /// </value>
        private long _printer_16;

        /// <summary>
        /// Gets or sets the printer_16.
        /// </summary>
        /// <value>
        /// The printer_16.
        /// </value>
	    [Editable(true)]
        [Column("printer_16")]
		public long printer_16
        {
            get { return _printer_16; }
            set
            {
                if (_printer_16 != value)
                {
                    PropertyChanged("printer_16");
					_printer_16 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_1_redirection.
        /// </summary>
        /// <value>
        /// The printer_1_redirection.
        /// </value>
        private long _printer_1_redirection;

        /// <summary>
        /// Gets or sets the printer_1_redirection.
        /// </summary>
        /// <value>
        /// The printer_1_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_1_redirection")]
		public long printer_1_redirection
        {
            get { return _printer_1_redirection; }
            set
            {
                if (_printer_1_redirection != value)
                {
                    PropertyChanged("printer_1_redirection");
					_printer_1_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_2_redirection.
        /// </summary>
        /// <value>
        /// The printer_2_redirection.
        /// </value>
        private long _printer_2_redirection;

        /// <summary>
        /// Gets or sets the printer_2_redirection.
        /// </summary>
        /// <value>
        /// The printer_2_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_2_redirection")]
		public long printer_2_redirection
        {
            get { return _printer_2_redirection; }
            set
            {
                if (_printer_2_redirection != value)
                {
                    PropertyChanged("printer_2_redirection");
					_printer_2_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_3_redirection.
        /// </summary>
        /// <value>
        /// The printer_3_redirection.
        /// </value>
        private long _printer_3_redirection;

        /// <summary>
        /// Gets or sets the printer_3_redirection.
        /// </summary>
        /// <value>
        /// The printer_3_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_3_redirection")]
		public long printer_3_redirection
        {
            get { return _printer_3_redirection; }
            set
            {
                if (_printer_3_redirection != value)
                {
                    PropertyChanged("printer_3_redirection");
					_printer_3_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_4_redirection.
        /// </summary>
        /// <value>
        /// The printer_4_redirection.
        /// </value>
        private long _printer_4_redirection;

        /// <summary>
        /// Gets or sets the printer_4_redirection.
        /// </summary>
        /// <value>
        /// The printer_4_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_4_redirection")]
		public long printer_4_redirection
        {
            get { return _printer_4_redirection; }
            set
            {
                if (_printer_4_redirection != value)
                {
                    PropertyChanged("printer_4_redirection");
					_printer_4_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_5_redirection.
        /// </summary>
        /// <value>
        /// The printer_5_redirection.
        /// </value>
        private long _printer_5_redirection;

        /// <summary>
        /// Gets or sets the printer_5_redirection.
        /// </summary>
        /// <value>
        /// The printer_5_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_5_redirection")]
		public long printer_5_redirection
        {
            get { return _printer_5_redirection; }
            set
            {
                if (_printer_5_redirection != value)
                {
                    PropertyChanged("printer_5_redirection");
					_printer_5_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_6_redirection.
        /// </summary>
        /// <value>
        /// The printer_6_redirection.
        /// </value>
        private long _printer_6_redirection;

        /// <summary>
        /// Gets or sets the printer_6_redirection.
        /// </summary>
        /// <value>
        /// The printer_6_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_6_redirection")]
		public long printer_6_redirection
        {
            get { return _printer_6_redirection; }
            set
            {
                if (_printer_6_redirection != value)
                {
                    PropertyChanged("printer_6_redirection");
					_printer_6_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_7_redirection.
        /// </summary>
        /// <value>
        /// The printer_7_redirection.
        /// </value>
        private long _printer_7_redirection;

        /// <summary>
        /// Gets or sets the printer_7_redirection.
        /// </summary>
        /// <value>
        /// The printer_7_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_7_redirection")]
		public long printer_7_redirection
        {
            get { return _printer_7_redirection; }
            set
            {
                if (_printer_7_redirection != value)
                {
                    PropertyChanged("printer_7_redirection");
					_printer_7_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_8_redirection.
        /// </summary>
        /// <value>
        /// The printer_8_redirection.
        /// </value>
        private long _printer_8_redirection;

        /// <summary>
        /// Gets or sets the printer_8_redirection.
        /// </summary>
        /// <value>
        /// The printer_8_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_8_redirection")]
		public long printer_8_redirection
        {
            get { return _printer_8_redirection; }
            set
            {
                if (_printer_8_redirection != value)
                {
                    PropertyChanged("printer_8_redirection");
					_printer_8_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_9_redirection.
        /// </summary>
        /// <value>
        /// The printer_9_redirection.
        /// </value>
        private long _printer_9_redirection;

        /// <summary>
        /// Gets or sets the printer_9_redirection.
        /// </summary>
        /// <value>
        /// The printer_9_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_9_redirection")]
		public long printer_9_redirection
        {
            get { return _printer_9_redirection; }
            set
            {
                if (_printer_9_redirection != value)
                {
                    PropertyChanged("printer_9_redirection");
					_printer_9_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_10_redirection.
        /// </summary>
        /// <value>
        /// The printer_10_redirection.
        /// </value>
        private long _printer_10_redirection;

        /// <summary>
        /// Gets or sets the printer_10_redirection.
        /// </summary>
        /// <value>
        /// The printer_10_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_10_redirection")]
		public long printer_10_redirection
        {
            get { return _printer_10_redirection; }
            set
            {
                if (_printer_10_redirection != value)
                {
                    PropertyChanged("printer_10_redirection");
					_printer_10_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_11_redirection.
        /// </summary>
        /// <value>
        /// The printer_11_redirection.
        /// </value>
        private long _printer_11_redirection;

        /// <summary>
        /// Gets or sets the printer_11_redirection.
        /// </summary>
        /// <value>
        /// The printer_11_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_11_redirection")]
		public long printer_11_redirection
        {
            get { return _printer_11_redirection; }
            set
            {
                if (_printer_11_redirection != value)
                {
                    PropertyChanged("printer_11_redirection");
					_printer_11_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_12_redirection.
        /// </summary>
        /// <value>
        /// The printer_12_redirection.
        /// </value>
        private long _printer_12_redirection;

        /// <summary>
        /// Gets or sets the printer_12_redirection.
        /// </summary>
        /// <value>
        /// The printer_12_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_12_redirection")]
		public long printer_12_redirection
        {
            get { return _printer_12_redirection; }
            set
            {
                if (_printer_12_redirection != value)
                {
                    PropertyChanged("printer_12_redirection");
					_printer_12_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_13_redirection.
        /// </summary>
        /// <value>
        /// The printer_13_redirection.
        /// </value>
        private long _printer_13_redirection;

        /// <summary>
        /// Gets or sets the printer_13_redirection.
        /// </summary>
        /// <value>
        /// The printer_13_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_13_redirection")]
		public long printer_13_redirection
        {
            get { return _printer_13_redirection; }
            set
            {
                if (_printer_13_redirection != value)
                {
                    PropertyChanged("printer_13_redirection");
					_printer_13_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_14_redirection.
        /// </summary>
        /// <value>
        /// The printer_14_redirection.
        /// </value>
        private long _printer_14_redirection;

        /// <summary>
        /// Gets or sets the printer_14_redirection.
        /// </summary>
        /// <value>
        /// The printer_14_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_14_redirection")]
		public long printer_14_redirection
        {
            get { return _printer_14_redirection; }
            set
            {
                if (_printer_14_redirection != value)
                {
                    PropertyChanged("printer_14_redirection");
					_printer_14_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_15_redirection.
        /// </summary>
        /// <value>
        /// The printer_15_redirection.
        /// </value>
        private long _printer_15_redirection;

        /// <summary>
        /// Gets or sets the printer_15_redirection.
        /// </summary>
        /// <value>
        /// The printer_15_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_15_redirection")]
		public long printer_15_redirection
        {
            get { return _printer_15_redirection; }
            set
            {
                if (_printer_15_redirection != value)
                {
                    PropertyChanged("printer_15_redirection");
					_printer_15_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the printer_16_redirection.
        /// </summary>
        /// <value>
        /// The printer_16_redirection.
        /// </value>
        private long _printer_16_redirection;

        /// <summary>
        /// Gets or sets the printer_16_redirection.
        /// </summary>
        /// <value>
        /// The printer_16_redirection.
        /// </value>
	    [Editable(true)]
        [Column("printer_16_redirection")]
		public long printer_16_redirection
        {
            get { return _printer_16_redirection; }
            set
            {
                if (_printer_16_redirection != value)
                {
                    PropertyChanged("printer_16_redirection");
					_printer_16_redirection = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_first_open_active.
        /// </summary>
        /// <value>
        /// The drawer_first_open_active.
        /// </value>
        private double _drawer_first_open_active;

        /// <summary>
        /// Gets or sets the drawer_first_open_active.
        /// </summary>
        /// <value>
        /// The drawer_first_open_active.
        /// </value>
	    [Editable(true)]
        [Column("drawer_first_open_active")]
		public double drawer_first_open_active
        {
            get { return _drawer_first_open_active; }
            set
            {
                if (_drawer_first_open_active != value)
                {
                    PropertyChanged("drawer_first_open_active");
					_drawer_first_open_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_second_open_active.
        /// </summary>
        /// <value>
        /// The drawer_second_open_active.
        /// </value>
        private double _drawer_second_open_active;

        /// <summary>
        /// Gets or sets the drawer_second_open_active.
        /// </summary>
        /// <value>
        /// The drawer_second_open_active.
        /// </value>
	    [Editable(true)]
        [Column("drawer_second_open_active")]
		public double drawer_second_open_active
        {
            get { return _drawer_second_open_active; }
            set
            {
                if (_drawer_second_open_active != value)
                {
                    PropertyChanged("drawer_second_open_active");
					_drawer_second_open_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_check_status_active.
        /// </summary>
        /// <value>
        /// The drawer_check_status_active.
        /// </value>
        private double _drawer_check_status_active;

        /// <summary>
        /// Gets or sets the drawer_check_status_active.
        /// </summary>
        /// <value>
        /// The drawer_check_status_active.
        /// </value>
	    [Editable(true)]
        [Column("drawer_check_status_active")]
		public double drawer_check_status_active
        {
            get { return _drawer_check_status_active; }
            set
            {
                if (_drawer_check_status_active != value)
                {
                    PropertyChanged("drawer_check_status_active");
					_drawer_check_status_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_status_return_logic_not.
        /// </summary>
        /// <value>
        /// The drawer_status_return_logic_not.
        /// </value>
        private double _drawer_status_return_logic_not;

        /// <summary>
        /// Gets or sets the drawer_status_return_logic_not.
        /// </summary>
        /// <value>
        /// The drawer_status_return_logic_not.
        /// </value>
	    [Editable(true)]
        [Column("drawer_status_return_logic_not")]
		public double drawer_status_return_logic_not
        {
            get { return _drawer_status_return_logic_not; }
            set
            {
                if (_drawer_status_return_logic_not != value)
                {
                    PropertyChanged("drawer_status_return_logic_not");
					_drawer_status_return_logic_not = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_on_printer.
        /// </summary>
        /// <value>
        /// The drawer_on_printer.
        /// </value>
        private double _drawer_on_printer;

        /// <summary>
        /// Gets or sets the drawer_on_printer.
        /// </summary>
        /// <value>
        /// The drawer_on_printer.
        /// </value>
	    [Editable(true)]
        [Column("drawer_on_printer")]
		public double drawer_on_printer
        {
            get { return _drawer_on_printer; }
            set
            {
                if (_drawer_on_printer != value)
                {
                    PropertyChanged("drawer_on_printer");
					_drawer_on_printer = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the drawer_hardware_device_type.
        /// </summary>
        /// <value>
        /// The drawer_hardware_device_type.
        /// </value>
        private long _drawer_hardware_device_type;

        /// <summary>
        /// Gets or sets the drawer_hardware_device_type.
        /// </summary>
        /// <value>
        /// The drawer_hardware_device_type.
        /// </value>
	    [Editable(true)]
        [Column("drawer_hardware_device_type")]
		public long drawer_hardware_device_type
        {
            get { return _drawer_hardware_device_type; }
            set
            {
                if (_drawer_hardware_device_type != value)
                {
                    PropertyChanged("drawer_hardware_device_type");
					_drawer_hardware_device_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the customer_display_active.
        /// </summary>
        /// <value>
        /// The customer_display_active.
        /// </value>
        private double _customer_display_active;

        /// <summary>
        /// Gets or sets the customer_display_active.
        /// </summary>
        /// <value>
        /// The customer_display_active.
        /// </value>
	    [Editable(true)]
        [Column("customer_display_active")]
		public double customer_display_active
        {
            get { return _customer_display_active; }
            set
            {
                if (_customer_display_active != value)
                {
                    PropertyChanged("customer_display_active");
					_customer_display_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode_reader_active.
        /// </summary>
        /// <value>
        /// The barcode_reader_active.
        /// </value>
        private double _barcode_reader_active;

        /// <summary>
        /// Gets or sets the barcode_reader_active.
        /// </summary>
        /// <value>
        /// The barcode_reader_active.
        /// </value>
	    [Editable(true)]
        [Column("barcode_reader_active")]
		public double barcode_reader_active
        {
            get { return _barcode_reader_active; }
            set
            {
                if (_barcode_reader_active != value)
                {
                    PropertyChanged("barcode_reader_active");
					_barcode_reader_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_active.
        /// </summary>
        /// <value>
        /// The msr_active.
        /// </value>
        private double _msr_active;

        /// <summary>
        /// Gets or sets the msr_active.
        /// </summary>
        /// <value>
        /// The msr_active.
        /// </value>
	    [Editable(true)]
        [Column("msr_active")]
		public double msr_active
        {
            get { return _msr_active; }
            set
            {
                if (_msr_active != value)
                {
                    PropertyChanged("msr_active");
					_msr_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_device_type.
        /// </summary>
        /// <value>
        /// The msr_device_type.
        /// </value>
        private long _msr_device_type;

        /// <summary>
        /// Gets or sets the msr_device_type.
        /// </summary>
        /// <value>
        /// The msr_device_type.
        /// </value>
	    [Editable(true)]
        [Column("msr_device_type")]
		public long msr_device_type
        {
            get { return _msr_device_type; }
            set
            {
                if (_msr_device_type != value)
                {
                    PropertyChanged("msr_device_type");
					_msr_device_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_second_track_only.
        /// </summary>
        /// <value>
        /// The msr_second_track_only.
        /// </value>
        private double _msr_second_track_only;

        /// <summary>
        /// Gets or sets the msr_second_track_only.
        /// </summary>
        /// <value>
        /// The msr_second_track_only.
        /// </value>
	    [Editable(true)]
        [Column("msr_second_track_only")]
		public double msr_second_track_only
        {
            get { return _msr_second_track_only; }
            set
            {
                if (_msr_second_track_only != value)
                {
                    PropertyChanged("msr_second_track_only");
					_msr_second_track_only = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_first_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_first_track_prefix.
        /// </value>
        private string _msr_first_track_prefix;

        /// <summary>
        /// Gets or sets the msr_first_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_first_track_prefix.
        /// </value>
	    [Editable(true)]
        [Column("msr_first_track_prefix")]
		public string msr_first_track_prefix
        {
            get { return _msr_first_track_prefix; }
            set
            {
                if (_msr_first_track_prefix != value)
                {
                    PropertyChanged("msr_first_track_prefix");
					_msr_first_track_prefix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_first_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_first_track_suffix.
        /// </value>
        private string _msr_first_track_suffix;

        /// <summary>
        /// Gets or sets the msr_first_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_first_track_suffix.
        /// </value>
	    [Editable(true)]
        [Column("msr_first_track_suffix")]
		public string msr_first_track_suffix
        {
            get { return _msr_first_track_suffix; }
            set
            {
                if (_msr_first_track_suffix != value)
                {
                    PropertyChanged("msr_first_track_suffix");
					_msr_first_track_suffix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_second_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_second_track_prefix.
        /// </value>
        private string _msr_second_track_prefix;

        /// <summary>
        /// Gets or sets the msr_second_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_second_track_prefix.
        /// </value>
	    [Editable(true)]
        [Column("msr_second_track_prefix")]
		public string msr_second_track_prefix
        {
            get { return _msr_second_track_prefix; }
            set
            {
                if (_msr_second_track_prefix != value)
                {
                    PropertyChanged("msr_second_track_prefix");
					_msr_second_track_prefix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_second_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_second_track_suffix.
        /// </value>
        private string _msr_second_track_suffix;

        /// <summary>
        /// Gets or sets the msr_second_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_second_track_suffix.
        /// </value>
	    [Editable(true)]
        [Column("msr_second_track_suffix")]
		public string msr_second_track_suffix
        {
            get { return _msr_second_track_suffix; }
            set
            {
                if (_msr_second_track_suffix != value)
                {
                    PropertyChanged("msr_second_track_suffix");
					_msr_second_track_suffix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_third_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_third_track_prefix.
        /// </value>
        private string _msr_third_track_prefix;

        /// <summary>
        /// Gets or sets the msr_third_track_prefix.
        /// </summary>
        /// <value>
        /// The msr_third_track_prefix.
        /// </value>
	    [Editable(true)]
        [Column("msr_third_track_prefix")]
		public string msr_third_track_prefix
        {
            get { return _msr_third_track_prefix; }
            set
            {
                if (_msr_third_track_prefix != value)
                {
                    PropertyChanged("msr_third_track_prefix");
					_msr_third_track_prefix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_third_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_third_track_suffix.
        /// </value>
        private string _msr_third_track_suffix;

        /// <summary>
        /// Gets or sets the msr_third_track_suffix.
        /// </summary>
        /// <value>
        /// The msr_third_track_suffix.
        /// </value>
	    [Editable(true)]
        [Column("msr_third_track_suffix")]
		public string msr_third_track_suffix
        {
            get { return _msr_third_track_suffix; }
            set
            {
                if (_msr_third_track_suffix != value)
                {
                    PropertyChanged("msr_third_track_suffix");
					_msr_third_track_suffix = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_third_track_delay.
        /// </summary>
        /// <value>
        /// The msr_third_track_delay.
        /// </value>
        private decimal _msr_third_track_delay;

        /// <summary>
        /// Gets or sets the msr_third_track_delay.
        /// </summary>
        /// <value>
        /// The msr_third_track_delay.
        /// </value>
	    [Editable(true)]
        [Column("msr_third_track_delay")]
		public decimal msr_third_track_delay
        {
            get { return _msr_third_track_delay; }
            set
            {
                if (_msr_third_track_delay != value)
                {
                    PropertyChanged("msr_third_track_delay");
					_msr_third_track_delay = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the msr_com_port.
        /// </summary>
        /// <value>
        /// The msr_com_port.
        /// </value>
        private long _msr_com_port;

        /// <summary>
        /// Gets or sets the msr_com_port.
        /// </summary>
        /// <value>
        /// The msr_com_port.
        /// </value>
	    [Editable(true)]
        [Column("msr_com_port")]
		public long msr_com_port
        {
            get { return _msr_com_port; }
            set
            {
                if (_msr_com_port != value)
                {
                    PropertyChanged("msr_com_port");
					_msr_com_port = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_userid.
        /// </summary>
        /// <value>
        /// The queue_db_userid.
        /// </value>
        private string _queue_db_userid;

        /// <summary>
        /// Gets or sets the queue_db_userid.
        /// </summary>
        /// <value>
        /// The queue_db_userid.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_userid")]
		public string queue_db_userid
        {
            get { return _queue_db_userid; }
            set
            {
                if (_queue_db_userid != value)
                {
                    PropertyChanged("queue_db_userid");
					_queue_db_userid = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the queue_db_password.
        /// </summary>
        /// <value>
        /// The queue_db_password.
        /// </value>
        private string _queue_db_password;

        /// <summary>
        /// Gets or sets the queue_db_password.
        /// </summary>
        /// <value>
        /// The queue_db_password.
        /// </value>
	    [Editable(true)]
        [Column("queue_db_password")]
		public string queue_db_password
        {
            get { return _queue_db_password; }
            set
            {
                if (_queue_db_password != value)
                {
                    PropertyChanged("queue_db_password");
					_queue_db_password = value;
                    
                }
            }
        }
	}
}
