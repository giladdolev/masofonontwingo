using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dw_item_code : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dw_item_code()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="item_number">The item_number.</param>
		/// <param name="code">The code.</param>
		/// <param name="items_barcode">The items_barcode.</param>
		/// <param name="items_name">The items_name.</param>
		/// <param name="item_code_name">The item_code_name.</param>
        public dw_item_code(long item_number, string code, string items_barcode, string items_name, string item_code_name)
        {
			this._item_number = item_number;
			this._code = code;
			this._items_barcode = items_barcode;
			this._items_name = items_name;
			this._item_code_name = item_code_name;
		}

	
        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
        private long _item_number;

        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
	    [Editable(true)]
        [Column("item_number")]
		public long item_number
        {
            get { return _item_number; }
            set
            {
                if (_item_number != value)
                {
                    PropertyChanged("item_number");
					_item_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        private string _code;

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
	    [Editable(true)]
        [Column("code")]
		public string code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    PropertyChanged("code");
					_code = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the items_barcode.
        /// </summary>
        /// <value>
        /// The items_barcode.
        /// </value>
        private string _items_barcode;

        /// <summary>
        /// Gets or sets the items_barcode.
        /// </summary>
        /// <value>
        /// The items_barcode.
        /// </value>
	    [Editable(true)]
        [Column("items_barcode")]
		public string items_barcode
        {
            get { return _items_barcode; }
            set
            {
                if (_items_barcode != value)
                {
                    PropertyChanged("items_barcode");
					_items_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the items_name.
        /// </summary>
        /// <value>
        /// The items_name.
        /// </value>
        private string _items_name;

        /// <summary>
        /// Gets or sets the items_name.
        /// </summary>
        /// <value>
        /// The items_name.
        /// </value>
	    [Editable(true)]
        [Column("items_name")]
		public string items_name
        {
            get { return _items_name; }
            set
            {
                if (_items_name != value)
                {
                    PropertyChanged("items_name");
					_items_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the item_code_name.
        /// </summary>
        /// <value>
        /// The item_code_name.
        /// </value>
        private string _item_code_name;

        /// <summary>
        /// Gets or sets the item_code_name.
        /// </summary>
        /// <value>
        /// The item_code_name.
        /// </value>
	    [Editable(true)]
        [Column("item_code_name")]
		public string item_code_name
        {
            get { return _item_code_name; }
            set
            {
                if (_item_code_name != value)
                {
                    PropertyChanged("item_code_name");
					_item_code_name = value;
                    
                }
            }
        }
	}
}
