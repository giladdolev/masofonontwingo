using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_mt_dddw_suppliers_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mt_dddw_suppliers_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="number">The number.</param>
		/// <param name="name">The name.</param>
        public d_mt_dddw_suppliers_list(long number, string name)
        {
			this._number = number;
			this._name = name;
		}

	
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        private long _number;

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("number")]
        [PropertiesAttribute("number", "number", false, 0, typeof(long))]
        public long number
        {
            get { return _number; }
            set
            {
                if (_number != value)
                {
                    PropertyChanged("number");
					_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        private string _name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
	    [Editable(true)]
        [Column("name")]
        [PropertiesAttribute("name", "name", true, 1)]
        public string name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    PropertyChanged("name");
					_name = value;
                    
                }
            }
        }
	}
}
