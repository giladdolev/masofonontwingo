using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_crossdoc_returns_for_b2b_xml : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_crossdoc_returns_for_b2b_xml()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="receiver">The receiver.</param>
		/// <param name="doctype">The doctype.</param>
		/// <param name="aprf">The aprf.</param>
		/// <param name="snrf">The snrf.</param>
		/// <param name="ackn">The ackn.</param>
		/// <param name="testind">The testind.</param>
		/// <param name="messdate">The messdate.</param>
		/// <param name="messtime">The messtime.</param>
		/// <param name="invoicetype">The invoicetype.</param>
		/// <param name="debitinvoiceno">The debitinvoiceno.</param>
		/// <param name="invoicefunc">The invoicefunc.</param>
		/// <param name="datetime">The datetime.</param>
		/// <param name="referenceno">The referenceno.</param>
		/// <param name="referencedate">The referencedate.</param>
		/// <param name="storeno">The storeno.</param>
		/// <param name="storename">The storename.</param>
		/// <param name="lineno">The lineno.</param>
		/// <param name="itembarcode">The itembarcode.</param>
		/// <param name="proddesc">The proddesc.</param>
		/// <param name="cartno">The cartno.</param>
		/// <param name="unitsqty">The unitsqty.</param>
		/// <param name="reasoncode">The reasoncode.</param>
		/// <param name="supplierno">The supplierno.</param>
        public d_crossdoc_returns_for_b2b_xml(string sender, string receiver, string doctype, string aprf, string snrf, string ackn, string testind, double messdate, string messtime, string invoicetype, string debitinvoiceno, string invoicefunc, long datetime, string referenceno, double referencedate, string storeno, string storename, double lineno, string itembarcode, string proddesc, string cartno, double unitsqty, string reasoncode, string supplierno)
        {
			this._sender = sender;
			this._receiver = receiver;
			this._doctype = doctype;
			this._aprf = aprf;
			this._snrf = snrf;
			this._ackn = ackn;
			this._testind = testind;
			this._messdate = messdate;
			this._messtime = messtime;
			this._invoicetype = invoicetype;
			this._debitinvoiceno = debitinvoiceno;
			this._invoicefunc = invoicefunc;
			this._datetime = datetime;
			this._referenceno = referenceno;
			this._referencedate = referencedate;
			this._storeno = storeno;
			this._storename = storename;
			this._lineno = lineno;
			this._itembarcode = itembarcode;
			this._proddesc = proddesc;
			this._cartno = cartno;
			this._unitsqty = unitsqty;
			this._reasoncode = reasoncode;
			this._supplierno = supplierno;
		}

	
        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
        private string _sender;

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
	    [Editable(true)]
        [Column("sender")]
		public string sender
        {
            get { return _sender; }
            set
            {
                if (_sender != value)
                {
                    PropertyChanged("sender");
					_sender = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
        private string _receiver;

        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
	    [Editable(true)]
        [Column("receiver")]
		public string receiver
        {
            get { return _receiver; }
            set
            {
                if (_receiver != value)
                {
                    PropertyChanged("receiver");
					_receiver = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
        private string _doctype;

        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
	    [Editable(true)]
        [Column("doctype")]
		public string doctype
        {
            get { return _doctype; }
            set
            {
                if (_doctype != value)
                {
                    PropertyChanged("doctype");
					_doctype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
        private string _aprf;

        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
	    [Editable(true)]
        [Column("aprf")]
		public string aprf
        {
            get { return _aprf; }
            set
            {
                if (_aprf != value)
                {
                    PropertyChanged("aprf");
					_aprf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
        private string _snrf;

        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
	    [Editable(true)]
        [Column("snrf")]
		public string snrf
        {
            get { return _snrf; }
            set
            {
                if (_snrf != value)
                {
                    PropertyChanged("snrf");
					_snrf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
        private string _ackn;

        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
	    [Editable(true)]
        [Column("ackn")]
		public string ackn
        {
            get { return _ackn; }
            set
            {
                if (_ackn != value)
                {
                    PropertyChanged("ackn");
					_ackn = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
        private string _testind;

        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
	    [Editable(true)]
        [Column("testind")]
		public string testind
        {
            get { return _testind; }
            set
            {
                if (_testind != value)
                {
                    PropertyChanged("testind");
					_testind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
        private double _messdate;

        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
	    [Editable(true)]
        [Column("messdate")]
		public double messdate
        {
            get { return _messdate; }
            set
            {
                if (_messdate != value)
                {
                    PropertyChanged("messdate");
					_messdate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
        private string _messtime;

        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
	    [Editable(true)]
        [Column("messtime")]
		public string messtime
        {
            get { return _messtime; }
            set
            {
                if (_messtime != value)
                {
                    PropertyChanged("messtime");
					_messtime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoicetype.
        /// </summary>
        /// <value>
        /// The invoicetype.
        /// </value>
        private string _invoicetype;

        /// <summary>
        /// Gets or sets the invoicetype.
        /// </summary>
        /// <value>
        /// The invoicetype.
        /// </value>
	    [Editable(true)]
        [Column("invoicetype")]
		public string invoicetype
        {
            get { return _invoicetype; }
            set
            {
                if (_invoicetype != value)
                {
                    PropertyChanged("invoicetype");
					_invoicetype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the debitinvoiceno.
        /// </summary>
        /// <value>
        /// The debitinvoiceno.
        /// </value>
        private string _debitinvoiceno;

        /// <summary>
        /// Gets or sets the debitinvoiceno.
        /// </summary>
        /// <value>
        /// The debitinvoiceno.
        /// </value>
	    [Editable(true)]
        [Column("debitinvoiceno")]
		public string debitinvoiceno
        {
            get { return _debitinvoiceno; }
            set
            {
                if (_debitinvoiceno != value)
                {
                    PropertyChanged("debitinvoiceno");
					_debitinvoiceno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoicefunc.
        /// </summary>
        /// <value>
        /// The invoicefunc.
        /// </value>
        private string _invoicefunc;

        /// <summary>
        /// Gets or sets the invoicefunc.
        /// </summary>
        /// <value>
        /// The invoicefunc.
        /// </value>
	    [Editable(true)]
        [Column("invoicefunc")]
		public string invoicefunc
        {
            get { return _invoicefunc; }
            set
            {
                if (_invoicefunc != value)
                {
                    PropertyChanged("invoicefunc");
					_invoicefunc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the datetime.
        /// </summary>
        /// <value>
        /// The datetime.
        /// </value>
        private long _datetime;

        /// <summary>
        /// Gets or sets the datetime.
        /// </summary>
        /// <value>
        /// The datetime.
        /// </value>
	    [Editable(true)]
        [Column("datetime")]
		public long datetime
        {
            get { return _datetime; }
            set
            {
                if (_datetime != value)
                {
                    PropertyChanged("datetime");
					_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the referenceno.
        /// </summary>
        /// <value>
        /// The referenceno.
        /// </value>
        private string _referenceno;

        /// <summary>
        /// Gets or sets the referenceno.
        /// </summary>
        /// <value>
        /// The referenceno.
        /// </value>
	    [Editable(true)]
        [Column("referenceno")]
		public string referenceno
        {
            get { return _referenceno; }
            set
            {
                if (_referenceno != value)
                {
                    PropertyChanged("referenceno");
					_referenceno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the referencedate.
        /// </summary>
        /// <value>
        /// The referencedate.
        /// </value>
        private double _referencedate;

        /// <summary>
        /// Gets or sets the referencedate.
        /// </summary>
        /// <value>
        /// The referencedate.
        /// </value>
	    [Editable(true)]
        [Column("referencedate")]
		public double referencedate
        {
            get { return _referencedate; }
            set
            {
                if (_referencedate != value)
                {
                    PropertyChanged("referencedate");
					_referencedate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the storeno.
        /// </summary>
        /// <value>
        /// The storeno.
        /// </value>
        private string _storeno;

        /// <summary>
        /// Gets or sets the storeno.
        /// </summary>
        /// <value>
        /// The storeno.
        /// </value>
	    [Editable(true)]
        [Column("storeno")]
		public string storeno
        {
            get { return _storeno; }
            set
            {
                if (_storeno != value)
                {
                    PropertyChanged("storeno");
					_storeno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the storename.
        /// </summary>
        /// <value>
        /// The storename.
        /// </value>
        private string _storename;

        /// <summary>
        /// Gets or sets the storename.
        /// </summary>
        /// <value>
        /// The storename.
        /// </value>
	    [Editable(true)]
        [Column("storename")]
		public string storename
        {
            get { return _storename; }
            set
            {
                if (_storename != value)
                {
                    PropertyChanged("storename");
					_storename = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the lineno.
        /// </summary>
        /// <value>
        /// The lineno.
        /// </value>
        private double _lineno;

        /// <summary>
        /// Gets or sets the lineno.
        /// </summary>
        /// <value>
        /// The lineno.
        /// </value>
	    [Editable(true)]
        [Column("lineno")]
		public double lineno
        {
            get { return _lineno; }
            set
            {
                if (_lineno != value)
                {
                    PropertyChanged("lineno");
					_lineno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the itembarcode.
        /// </summary>
        /// <value>
        /// The itembarcode.
        /// </value>
        private string _itembarcode;

        /// <summary>
        /// Gets or sets the itembarcode.
        /// </summary>
        /// <value>
        /// The itembarcode.
        /// </value>
	    [Editable(true)]
        [Column("itembarcode")]
		public string itembarcode
        {
            get { return _itembarcode; }
            set
            {
                if (_itembarcode != value)
                {
                    PropertyChanged("itembarcode");
					_itembarcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the proddesc.
        /// </summary>
        /// <value>
        /// The proddesc.
        /// </value>
        private string _proddesc;

        /// <summary>
        /// Gets or sets the proddesc.
        /// </summary>
        /// <value>
        /// The proddesc.
        /// </value>
	    [Editable(true)]
        [Column("proddesc")]
		public string proddesc
        {
            get { return _proddesc; }
            set
            {
                if (_proddesc != value)
                {
                    PropertyChanged("proddesc");
					_proddesc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cartno.
        /// </summary>
        /// <value>
        /// The cartno.
        /// </value>
        private string _cartno;

        /// <summary>
        /// Gets or sets the cartno.
        /// </summary>
        /// <value>
        /// The cartno.
        /// </value>
	    [Editable(true)]
        [Column("cartno")]
		public string cartno
        {
            get { return _cartno; }
            set
            {
                if (_cartno != value)
                {
                    PropertyChanged("cartno");
					_cartno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the unitsqty.
        /// </summary>
        /// <value>
        /// The unitsqty.
        /// </value>
        private double _unitsqty;

        /// <summary>
        /// Gets or sets the unitsqty.
        /// </summary>
        /// <value>
        /// The unitsqty.
        /// </value>
	    [Editable(true)]
        [Column("unitsqty")]
		public double unitsqty
        {
            get { return _unitsqty; }
            set
            {
                if (_unitsqty != value)
                {
                    PropertyChanged("unitsqty");
					_unitsqty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the reasoncode.
        /// </summary>
        /// <value>
        /// The reasoncode.
        /// </value>
        private string _reasoncode;

        /// <summary>
        /// Gets or sets the reasoncode.
        /// </summary>
        /// <value>
        /// The reasoncode.
        /// </value>
	    [Editable(true)]
        [Column("reasoncode")]
		public string reasoncode
        {
            get { return _reasoncode; }
            set
            {
                if (_reasoncode != value)
                {
                    PropertyChanged("reasoncode");
					_reasoncode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplierno.
        /// </summary>
        /// <value>
        /// The supplierno.
        /// </value>
        private string _supplierno;

        /// <summary>
        /// Gets or sets the supplierno.
        /// </summary>
        /// <value>
        /// The supplierno.
        /// </value>
	    [Editable(true)]
        [Column("supplierno")]
		public string supplierno
        {
            get { return _supplierno; }
            set
            {
                if (_supplierno != value)
                {
                    PropertyChanged("supplierno");
					_supplierno = value;
                    
                }
            }
        }
	}
}
