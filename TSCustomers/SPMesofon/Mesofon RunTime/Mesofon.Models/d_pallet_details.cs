using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_pallet_details : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_pallet_details()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="item_barcode">The item_barcode.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="units_qty">The units_qty.</param>
		/// <param name="items_name">The items_name.</param>
        public d_pallet_details(string item_barcode, long invoice_number, decimal units_qty, string items_name)
        {
			this._item_barcode = item_barcode;
			this._invoice_number = invoice_number;
			this._units_qty = units_qty;
			this._items_name = items_name;
		}

	
        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
        private string _item_barcode;

        /// <summary>
        /// Gets or sets the item_barcode.
        /// </summary>
        /// <value>
        /// The item_barcode.
        /// </value>
	    [Editable(true)]
        [Column("item_barcode")]
		public string item_barcode
        {
            get { return _item_barcode; }
            set
            {
                if (_item_barcode != value)
                {
                    PropertyChanged("item_barcode");
					_item_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the units_qty.
        /// </summary>
        /// <value>
        /// The units_qty.
        /// </value>
        private decimal _units_qty;

        /// <summary>
        /// Gets or sets the units_qty.
        /// </summary>
        /// <value>
        /// The units_qty.
        /// </value>
	    [Editable(true)]
        [Column("units_qty")]
		public decimal units_qty
        {
            get { return _units_qty; }
            set
            {
                if (_units_qty != value)
                {
                    PropertyChanged("units_qty");
					_units_qty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the items_name.
        /// </summary>
        /// <value>
        /// The items_name.
        /// </value>
        private string _items_name;

        /// <summary>
        /// Gets or sets the items_name.
        /// </summary>
        /// <value>
        /// The items_name.
        /// </value>
	    [Editable(true)]
        [Column("items_name")]
		public string items_name
        {
            get { return _items_name; }
            set
            {
                if (_items_name != value)
                {
                    PropertyChanged("items_name");
					_items_name = value;
                    
                }
            }
        }
	}
}
