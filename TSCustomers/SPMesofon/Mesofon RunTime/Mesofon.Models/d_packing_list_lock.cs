using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_packing_list_lock : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_packing_list_lock()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="pack_list_number">The pack_list_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="packing_type">The packing_type.</param>
		/// <param name="create_mode">The create_mode.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
        public d_packing_list_lock(long pack_list_number, long branch_number, long supplier_number, string packing_type, string create_mode, DateTime? last_update_datetime)
        {
			this._pack_list_number = pack_list_number;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._packing_type = packing_type;
			this._create_mode = create_mode;
			this._last_update_datetime = last_update_datetime;
		}

	
        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
        private long _pack_list_number;

        /// <summary>
        /// Gets or sets the pack_list_number.
        /// </summary>
        /// <value>
        /// The pack_list_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("pack_list_number")]
		public long pack_list_number
        {
            get { return _pack_list_number; }
            set
            {
                if (_pack_list_number != value)
                {
                    PropertyChanged("pack_list_number");
					_pack_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_type.
        /// </summary>
        /// <value>
        /// The packing_type.
        /// </value>
        private string _packing_type;

        /// <summary>
        /// Gets or sets the packing_type.
        /// </summary>
        /// <value>
        /// The packing_type.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("packing_type")]
		public string packing_type
        {
            get { return _packing_type; }
            set
            {
                if (_packing_type != value)
                {
                    PropertyChanged("packing_type");
					_packing_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
        private string _create_mode;

        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
	    [Editable(true)]
        [Column("create_mode")]
		public string create_mode
        {
            get { return _create_mode; }
            set
            {
                if (_create_mode != value)
                {
                    PropertyChanged("create_mode");
					_create_mode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	}
}
