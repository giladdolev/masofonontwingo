using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_pallet_invoices : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_pallet_invoices()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="state">The state.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="row_serial_number">The row_serial_number.</param>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="pallet_number">The pallet_number.</param>
		/// <param name="pack_quantity">The pack_quantity.</param>
		/// <param name="arrive_datetime">The arrive_datetime.</param>
		/// <param name="approve_employee">The approve_employee.</param>
		/// <param name="auto_scan_status">The auto_scan_status.</param>
		/// <param name="document_type">The document_type.</param>
		/// <param name="create_mode">The create_mode.</param>
        public d_mini_terminal_pallet_invoices(double serial_number, DateTime? date_move, string state, long branch_number, long supplier_number, double invoice_number, double order_number, long row_serial_number, double shipment_number, string pallet_number, long pack_quantity, DateTime? arrive_datetime, long approve_employee, long auto_scan_status, long document_type, string create_mode)
        {
			this._serial_number = serial_number;
			this._date_move = date_move;
			this._state = state;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._invoice_number = invoice_number;
			this._order_number = order_number;
			this._row_serial_number = row_serial_number;
			this._shipment_number = shipment_number;
			this._pallet_number = pallet_number;
			this._pack_quantity = pack_quantity;
			this._arrive_datetime = arrive_datetime;
			this._approve_employee = approve_employee;
			this._auto_scan_status = auto_scan_status;
			this._document_type = document_type;
			this._create_mode = create_mode;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private double _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
	    [Editable(true)]
        [Column("serial_number")]
		public double serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private double _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public double invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private double _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public double order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
        private long _row_serial_number;

        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("row_serial_number")]
		public long row_serial_number
        {
            get { return _row_serial_number; }
            set
            {
                if (_row_serial_number != value)
                {
                    PropertyChanged("row_serial_number");
					_row_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_number")]
		public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
        private string _pallet_number;

        /// <summary>
        /// Gets or sets the pallet_number.
        /// </summary>
        /// <value>
        /// The pallet_number.
        /// </value>
	    [Editable(true)]
        [Column("pallet_number")]
		public string pallet_number
        {
            get { return _pallet_number; }
            set
            {
                if (_pallet_number != value)
                {
                    PropertyChanged("pallet_number");
					_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pack_quantity.
        /// </summary>
        /// <value>
        /// The pack_quantity.
        /// </value>
        private long _pack_quantity;

        /// <summary>
        /// Gets or sets the pack_quantity.
        /// </summary>
        /// <value>
        /// The pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("pack_quantity")]
		public long pack_quantity
        {
            get { return _pack_quantity; }
            set
            {
                if (_pack_quantity != value)
                {
                    PropertyChanged("pack_quantity");
					_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the arrive_datetime.
        /// </summary>
        /// <value>
        /// The arrive_datetime.
        /// </value>
        private DateTime? _arrive_datetime;

        /// <summary>
        /// Gets or sets the arrive_datetime.
        /// </summary>
        /// <value>
        /// The arrive_datetime.
        /// </value>
	    [Editable(true)]
        [Column("arrive_datetime")]
		public DateTime? arrive_datetime
        {
            get { return _arrive_datetime; }
            set
            {
                if (_arrive_datetime != value)
                {
                    PropertyChanged("arrive_datetime");
					_arrive_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the approve_employee.
        /// </summary>
        /// <value>
        /// The approve_employee.
        /// </value>
        private long _approve_employee;

        /// <summary>
        /// Gets or sets the approve_employee.
        /// </summary>
        /// <value>
        /// The approve_employee.
        /// </value>
	    [Editable(true)]
        [Column("approve_employee")]
		public long approve_employee
        {
            get { return _approve_employee; }
            set
            {
                if (_approve_employee != value)
                {
                    PropertyChanged("approve_employee");
					_approve_employee = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the auto_scan_status.
        /// </summary>
        /// <value>
        /// The auto_scan_status.
        /// </value>
        private long _auto_scan_status;

        /// <summary>
        /// Gets or sets the auto_scan_status.
        /// </summary>
        /// <value>
        /// The auto_scan_status.
        /// </value>
	    [Editable(true)]
        [Column("auto_scan_status")]
		public long auto_scan_status
        {
            get { return _auto_scan_status; }
            set
            {
                if (_auto_scan_status != value)
                {
                    PropertyChanged("auto_scan_status");
					_auto_scan_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the document_type.
        /// </summary>
        /// <value>
        /// The document_type.
        /// </value>
        private long _document_type;

        /// <summary>
        /// Gets or sets the document_type.
        /// </summary>
        /// <value>
        /// The document_type.
        /// </value>
	    [Editable(true)]
        [Column("document_type")]
		public long document_type
        {
            get { return _document_type; }
            set
            {
                if (_document_type != value)
                {
                    PropertyChanged("document_type");
					_document_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
        private string _create_mode;

        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
	    [Editable(true)]
        [Column("create_mode")]
		public string create_mode
        {
            get { return _create_mode; }
            set
            {
                if (_create_mode != value)
                {
                    PropertyChanged("create_mode");
					_create_mode = value;
                    
                }
            }
        }
	}
}
