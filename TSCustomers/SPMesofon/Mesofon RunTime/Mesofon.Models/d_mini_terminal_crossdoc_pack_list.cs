using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;
using System.Web.VisualTree.Elements;

namespace Mesofon.Models
{
	public class d_mini_terminal_crossdoc_pack_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_crossdoc_pack_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="state">The state.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="sub_supplier_number">The sub_supplier_number.</param>
		/// <param name="carton_barcode">The carton_barcode.</param>
		/// <param name="row_serial_number">The row_serial_number.</param>
		/// <param name="shipment_pallet_number">The shipment_pallet_number.</param>
		/// <param name="marlog_number">The marlog_number.</param>
		/// <param name="branch_accept_datetime">The branch_accept_datetime.</param>
        public d_mini_terminal_crossdoc_pack_list(string state, long branch_number, DateTime? last_update_datetime, long sub_supplier_number, string carton_barcode, long row_serial_number, double shipment_pallet_number, long marlog_number, DateTime? branch_accept_datetime)
        {
			this._state = state;
			this._branch_number = branch_number;
			this._last_update_datetime = last_update_datetime;
			this._sub_supplier_number = sub_supplier_number;
			this._carton_barcode = carton_barcode;
			this._row_serial_number = row_serial_number;
			this._shipment_pallet_number = shipment_pallet_number;
			this._marlog_number = marlog_number;
			this._branch_accept_datetime = branch_accept_datetime;
		}

	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
        [PropertiesAttribute("state", "�����", true, 1, typeof(GridCheckBoxColumn))]
        [ThreeState("c","s","f")]
        public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sub_supplier_number.
        /// </summary>
        /// <value>
        /// The sub_supplier_number.
        /// </value>
        private long _sub_supplier_number;

        /// <summary>
        /// Gets or sets the sub_supplier_number.
        /// </summary>
        /// <value>
        /// The sub_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("sub_supplier_number")]
		public long sub_supplier_number
        {
            get { return _sub_supplier_number; }
            set
            {
                if (_sub_supplier_number != value)
                {
                    PropertyChanged("sub_supplier_number");
					_sub_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
        private string _carton_barcode;

        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("carton_barcode")]
        [PropertiesAttribute("carton_barcode", "����� �����", true,0,typeof(string),120)]

        public string carton_barcode
        {
            get { return _carton_barcode; }
            set
            {
                if (_carton_barcode != value)
                {
                    PropertyChanged("carton_barcode");
					_carton_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
        private long _row_serial_number;

        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("row_serial_number")]
		public long row_serial_number
        {
            get { return _row_serial_number; }
            set
            {
                if (_row_serial_number != value)
                {
                    PropertyChanged("row_serial_number");
					_row_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallet_number.
        /// </value>
        private double _shipment_pallet_number;

        /// <summary>
        /// Gets or sets the shipment_pallet_number.
        /// </summary>
        /// <value>
        /// The shipment_pallet_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("shipment_pallet_number")]
		public double shipment_pallet_number
        {
            get { return _shipment_pallet_number; }
            set
            {
                if (_shipment_pallet_number != value)
                {
                    PropertyChanged("shipment_pallet_number");
					_shipment_pallet_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
        private long _marlog_number;

        /// <summary>
        /// Gets or sets the marlog_number.
        /// </summary>
        /// <value>
        /// The marlog_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("marlog_number")]
		public long marlog_number
        {
            get { return _marlog_number; }
            set
            {
                if (_marlog_number != value)
                {
                    PropertyChanged("marlog_number");
					_marlog_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_accept_datetime.
        /// </summary>
        /// <value>
        /// The branch_accept_datetime.
        /// </value>
        private DateTime? _branch_accept_datetime;

        /// <summary>
        /// Gets or sets the branch_accept_datetime.
        /// </summary>
        /// <value>
        /// The branch_accept_datetime.
        /// </value>
	    [Editable(true)]
        [Column("branch_accept_datetime")]
		public DateTime? branch_accept_datetime
        {
            get { return _branch_accept_datetime; }
            set
            {
                if (_branch_accept_datetime != value)
                {
                    PropertyChanged("branch_accept_datetime");
					_branch_accept_datetime = value;
                    
                }
            }
        }
	}
}
