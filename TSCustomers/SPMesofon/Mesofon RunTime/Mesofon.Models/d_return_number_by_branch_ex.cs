using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_return_number_by_branch_ex : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_return_number_by_branch_ex()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="return_number">The return_number.</param>
        public d_return_number_by_branch_ex(double return_number)
        {
			this._return_number = return_number;
		}

	
        /// <summary>
        /// Gets or sets the return_number.
        /// </summary>
        /// <value>
        /// The return_number.
        /// </value>
        private double _return_number;

        /// <summary>
        /// Gets or sets the return_number.
        /// </summary>
        /// <value>
        /// The return_number.
        /// </value>
	    [Editable(true)]
        [Column("return_number")]
		public double return_number
        {
            get { return _return_number; }
            set
            {
                if (_return_number != value)
                {
                    PropertyChanged("return_number");
					_return_number = value;
                    
                }
            }
        }
	}
}
