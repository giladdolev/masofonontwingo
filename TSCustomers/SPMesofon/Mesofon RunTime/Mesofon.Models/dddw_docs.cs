using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class dddw_docs : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dddw_docs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="doc_type">The doc_type.</param>
		/// <param name="doc_number">The doc_number.</param>
		/// <param name="b2b_status">The b2b_status.</param>
        public dddw_docs(string doc_type, long doc_number, double b2b_status)
        {
			this._doc_type = doc_type;
			this._doc_number = doc_number;
			this._b2b_status = b2b_status;
		}

	
        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
        private string _doc_type;

        /// <summary>
        /// Gets or sets the doc_type.
        /// </summary>
        /// <value>
        /// The doc_type.
        /// </value>
	    [Editable(true)]
        [Column("doc_type")]
        [Properties("doc_type", "���", true, 0, typeof(string))]
        public string doc_type
        {
            get { return _doc_type; }
            set
            {
                if (_doc_type != value)
                {
                    PropertyChanged("doc_type");
					_doc_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
        private long _doc_number;

        /// <summary>
        /// Gets or sets the doc_number.
        /// </summary>
        /// <value>
        /// The doc_number.
        /// </value>
	    [Editable(true)]
        [Column("doc_number")]
        [Properties("doc_number", "����", true, 1, typeof(long))]
        public long doc_number
        {
            get { return _doc_number; }
            set
            {
                if (_doc_number != value)
                {
                    PropertyChanged("doc_number");
					_doc_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private double _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
        [Properties("b2b_status", "b2b_status", false, 2, typeof(double))]
        public double b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	}
}
