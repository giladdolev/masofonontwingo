using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_detail_items_invoice : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_detail_items_invoice()
        {
            this._doc_no_en = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_details_serial_number">The invoice_details_serial_number.</param>
		/// <param name="invoice_details_branch_number">The invoice_details_branch_number.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="invoice_details_material_number">The invoice_details_material_number.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="invoice_details_material_price">The invoice_details_material_price.</param>
		/// <param name="invoice_details_details">The invoice_details_details.</param>
		/// <param name="invoice_details_supplier_number">The invoice_details_supplier_number.</param>
		/// <param name="invoice_details_material_discount_percent">The invoice_details_material_discount_percent.</param>
		/// <param name="invoice_details_material_price_after_discount">The invoice_details_material_price_after_discount.</param>
		/// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
		/// <param name="invoice_details_packing_list_number">The invoice_details_packing_list_number.</param>
		/// <param name="invoice_details_packing_serial">The invoice_details_packing_serial.</param>
		/// <param name="invoice_details_stock_number">The invoice_details_stock_number.</param>
		/// <param name="invoice_details_sell_price">The invoice_details_sell_price.</param>
		/// <param name="invoice_details_bonus_quantity">The invoice_details_bonus_quantity.</param>
		/// <param name="invoice_details_material_bonus_connect">The invoice_details_material_bonus_connect.</param>
		/// <param name="invoice_details_max_quantity_allowed">The invoice_details_max_quantity_allowed.</param>
		/// <param name="invoice_details_last_packing_change">The invoice_details_last_packing_change.</param>
		/// <param name="invoice_details_import_type">The invoice_details_import_type.</param>
		/// <param name="invoice_details_material_discount_amount">The invoice_details_material_discount_amount.</param>
		/// <param name="invoice_details_bonus_row">The invoice_details_bonus_row.</param>
		/// <param name="invoice_details_color">The invoice_details_color.</param>
		/// <param name="invoice_details_pay_shape">The invoice_details_pay_shape.</param>
		/// <param name="invoice_details_indicator">The invoice_details_indicator.</param>
		/// <param name="invoice_details_bonus_discount">The invoice_details_bonus_discount.</param>
		/// <param name="invoice_details_supplier_discount_percent">The invoice_details_supplier_discount_percent.</param>
		/// <param name="invoice_details_current_catalog_sell_price">The invoice_details_current_catalog_sell_price.</param>
		/// <param name="invoice_details_expiration_date">The invoice_details_expiration_date.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="invoice_details_mini_terminal">The invoice_details_mini_terminal.</param>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="materials_material_name">The materials_material_name.</param>
		/// <param name="materials_min_valid_months">The materials_min_valid_months.</param>
		/// <param name="order_quantity">The order_quantity.</param>
		/// <param name="b2b_status">The b2b_status.</param>
		/// <param name="row_no">The row_no.</param>
		/// <param name="doc_no_en">The doc_no_en.</param>
		/// <param name="compute_1">The compute_1.</param>
        public d_mini_terminal_detail_items_invoice(long invoice_details_serial_number, long invoice_details_branch_number, long invoice_number, long invoice_details_material_number, decimal material_quantity, decimal invoice_details_material_price, string invoice_details_details, long invoice_details_supplier_number, decimal invoice_details_material_discount_percent, decimal invoice_details_material_price_after_discount, decimal quantity_within_invoice, long invoice_details_packing_list_number, long invoice_details_packing_serial, long invoice_details_stock_number, decimal invoice_details_sell_price, decimal invoice_details_bonus_quantity, long invoice_details_material_bonus_connect, decimal invoice_details_max_quantity_allowed, decimal invoice_details_last_packing_change, string invoice_details_import_type, decimal invoice_details_material_discount_amount, string invoice_details_bonus_row, long invoice_details_color, long invoice_details_pay_shape, long invoice_details_indicator, decimal invoice_details_bonus_discount, decimal invoice_details_supplier_discount_percent, decimal invoice_details_current_catalog_sell_price, DateTime? invoice_details_expiration_date, decimal expected_material_quantity, long invoice_details_mini_terminal, string materials_barcode, string materials_material_name, long materials_min_valid_months, decimal order_quantity, long b2b_status, long row_no, long doc_no_en, string compute_1)
        {
			this._invoice_details_serial_number = invoice_details_serial_number;
			this._invoice_details_branch_number = invoice_details_branch_number;
			this._invoice_number = invoice_number;
			this._invoice_details_material_number = invoice_details_material_number;
			this._material_quantity = material_quantity;
			this._invoice_details_material_price = invoice_details_material_price;
			this._invoice_details_details = invoice_details_details;
			this._invoice_details_supplier_number = invoice_details_supplier_number;
			this._invoice_details_material_discount_percent = invoice_details_material_discount_percent;
			this._invoice_details_material_price_after_discount = invoice_details_material_price_after_discount;
			this._quantity_within_invoice = quantity_within_invoice;
			this._invoice_details_packing_list_number = invoice_details_packing_list_number;
			this._invoice_details_packing_serial = invoice_details_packing_serial;
			this._invoice_details_stock_number = invoice_details_stock_number;
			this._invoice_details_sell_price = invoice_details_sell_price;
			this._invoice_details_bonus_quantity = invoice_details_bonus_quantity;
			this._invoice_details_material_bonus_connect = invoice_details_material_bonus_connect;
			this._invoice_details_max_quantity_allowed = invoice_details_max_quantity_allowed;
			this._invoice_details_last_packing_change = invoice_details_last_packing_change;
			this._invoice_details_import_type = invoice_details_import_type;
			this._invoice_details_material_discount_amount = invoice_details_material_discount_amount;
			this._invoice_details_bonus_row = invoice_details_bonus_row;
			this._invoice_details_color = invoice_details_color;
			this._invoice_details_pay_shape = invoice_details_pay_shape;
			this._invoice_details_indicator = invoice_details_indicator;
			this._invoice_details_bonus_discount = invoice_details_bonus_discount;
			this._invoice_details_supplier_discount_percent = invoice_details_supplier_discount_percent;
			this._invoice_details_current_catalog_sell_price = invoice_details_current_catalog_sell_price;
			this._invoice_details_expiration_date = invoice_details_expiration_date;
			this._expected_material_quantity = expected_material_quantity;
			this._invoice_details_mini_terminal = invoice_details_mini_terminal;
			this._materials_barcode = materials_barcode;
			this._materials_material_name = materials_material_name;
			this._materials_min_valid_months = materials_min_valid_months;
			this._order_quantity = order_quantity;
			this._b2b_status = b2b_status;
			this._row_no = row_no;
			this._doc_no_en = doc_no_en;
			this._compute_1 = compute_1;
		}

	
        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
        private long _invoice_details_serial_number;

        /// <summary>
        /// Gets or sets the invoice_details_serial_number.
        /// </summary>
        /// <value>
        /// The invoice_details_serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_details_serial_number")]
		public long invoice_details_serial_number
        {
            get { return _invoice_details_serial_number; }
            set
            {
                if (_invoice_details_serial_number != value)
                {
                    PropertyChanged("invoice_details_serial_number");
					_invoice_details_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
        private long _invoice_details_branch_number;

        /// <summary>
        /// Gets or sets the invoice_details_branch_number.
        /// </summary>
        /// <value>
        /// The invoice_details_branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_details_branch_number")]
		public long invoice_details_branch_number
        {
            get { return _invoice_details_branch_number; }
            set
            {
                if (_invoice_details_branch_number != value)
                {
                    PropertyChanged("invoice_details_branch_number");
					_invoice_details_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_number.
        /// </summary>
        /// <value>
        /// The invoice_details_material_number.
        /// </value>
        private long _invoice_details_material_number;

        /// <summary>
        /// Gets or sets the invoice_details_material_number.
        /// </summary>
        /// <value>
        /// The invoice_details_material_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_details_material_number")]
		public long invoice_details_material_number
        {
            get { return _invoice_details_material_number; }
            set
            {
                if (_invoice_details_material_number != value)
                {
                    PropertyChanged("invoice_details_material_number");
					_invoice_details_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_price.
        /// </summary>
        /// <value>
        /// The invoice_details_material_price.
        /// </value>
        private decimal _invoice_details_material_price;

        /// <summary>
        /// Gets or sets the invoice_details_material_price.
        /// </summary>
        /// <value>
        /// The invoice_details_material_price.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_price")]
		public decimal invoice_details_material_price
        {
            get { return _invoice_details_material_price; }
            set
            {
                if (_invoice_details_material_price != value)
                {
                    PropertyChanged("invoice_details_material_price");
					_invoice_details_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_details.
        /// </summary>
        /// <value>
        /// The invoice_details_details.
        /// </value>
        private string _invoice_details_details;

        /// <summary>
        /// Gets or sets the invoice_details_details.
        /// </summary>
        /// <value>
        /// The invoice_details_details.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_details")]
		public string invoice_details_details
        {
            get { return _invoice_details_details; }
            set
            {
                if (_invoice_details_details != value)
                {
                    PropertyChanged("invoice_details_details");
					_invoice_details_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_number.
        /// </value>
        private long _invoice_details_supplier_number;

        /// <summary>
        /// Gets or sets the invoice_details_supplier_number.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_supplier_number")]
		public long invoice_details_supplier_number
        {
            get { return _invoice_details_supplier_number; }
            set
            {
                if (_invoice_details_supplier_number != value)
                {
                    PropertyChanged("invoice_details_supplier_number");
					_invoice_details_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_percent.
        /// </value>
        private decimal _invoice_details_material_discount_percent;

        /// <summary>
        /// Gets or sets the invoice_details_material_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_discount_percent")]
		public decimal invoice_details_material_discount_percent
        {
            get { return _invoice_details_material_discount_percent; }
            set
            {
                if (_invoice_details_material_discount_percent != value)
                {
                    PropertyChanged("invoice_details_material_discount_percent");
					_invoice_details_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_price_after_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_price_after_discount.
        /// </value>
        private decimal _invoice_details_material_price_after_discount;

        /// <summary>
        /// Gets or sets the invoice_details_material_price_after_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_price_after_discount")]
		public decimal invoice_details_material_price_after_discount
        {
            get { return _invoice_details_material_price_after_discount; }
            set
            {
                if (_invoice_details_material_price_after_discount != value)
                {
                    PropertyChanged("invoice_details_material_price_after_discount");
					_invoice_details_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
		public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_packing_list_number.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_list_number.
        /// </value>
        private long _invoice_details_packing_list_number;

        /// <summary>
        /// Gets or sets the invoice_details_packing_list_number.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_packing_list_number")]
		public long invoice_details_packing_list_number
        {
            get { return _invoice_details_packing_list_number; }
            set
            {
                if (_invoice_details_packing_list_number != value)
                {
                    PropertyChanged("invoice_details_packing_list_number");
					_invoice_details_packing_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_packing_serial.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_serial.
        /// </value>
        private long _invoice_details_packing_serial;

        /// <summary>
        /// Gets or sets the invoice_details_packing_serial.
        /// </summary>
        /// <value>
        /// The invoice_details_packing_serial.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_packing_serial")]
		public long invoice_details_packing_serial
        {
            get { return _invoice_details_packing_serial; }
            set
            {
                if (_invoice_details_packing_serial != value)
                {
                    PropertyChanged("invoice_details_packing_serial");
					_invoice_details_packing_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_stock_number.
        /// </summary>
        /// <value>
        /// The invoice_details_stock_number.
        /// </value>
        private long _invoice_details_stock_number;

        /// <summary>
        /// Gets or sets the invoice_details_stock_number.
        /// </summary>
        /// <value>
        /// The invoice_details_stock_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_stock_number")]
		public long invoice_details_stock_number
        {
            get { return _invoice_details_stock_number; }
            set
            {
                if (_invoice_details_stock_number != value)
                {
                    PropertyChanged("invoice_details_stock_number");
					_invoice_details_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_sell_price.
        /// </summary>
        /// <value>
        /// The invoice_details_sell_price.
        /// </value>
        private decimal _invoice_details_sell_price;

        /// <summary>
        /// Gets or sets the invoice_details_sell_price.
        /// </summary>
        /// <value>
        /// The invoice_details_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_sell_price")]
		public decimal invoice_details_sell_price
        {
            get { return _invoice_details_sell_price; }
            set
            {
                if (_invoice_details_sell_price != value)
                {
                    PropertyChanged("invoice_details_sell_price");
					_invoice_details_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_bonus_quantity.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_quantity.
        /// </value>
        private decimal _invoice_details_bonus_quantity;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_quantity.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_quantity")]
		public decimal invoice_details_bonus_quantity
        {
            get { return _invoice_details_bonus_quantity; }
            set
            {
                if (_invoice_details_bonus_quantity != value)
                {
                    PropertyChanged("invoice_details_bonus_quantity");
					_invoice_details_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The invoice_details_material_bonus_connect.
        /// </value>
        private long _invoice_details_material_bonus_connect;

        /// <summary>
        /// Gets or sets the invoice_details_material_bonus_connect.
        /// </summary>
        /// <value>
        /// The invoice_details_material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_bonus_connect")]
		public long invoice_details_material_bonus_connect
        {
            get { return _invoice_details_material_bonus_connect; }
            set
            {
                if (_invoice_details_material_bonus_connect != value)
                {
                    PropertyChanged("invoice_details_material_bonus_connect");
					_invoice_details_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The invoice_details_max_quantity_allowed.
        /// </value>
        private decimal _invoice_details_max_quantity_allowed;

        /// <summary>
        /// Gets or sets the invoice_details_max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The invoice_details_max_quantity_allowed.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_max_quantity_allowed")]
		public decimal invoice_details_max_quantity_allowed
        {
            get { return _invoice_details_max_quantity_allowed; }
            set
            {
                if (_invoice_details_max_quantity_allowed != value)
                {
                    PropertyChanged("invoice_details_max_quantity_allowed");
					_invoice_details_max_quantity_allowed = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_last_packing_change.
        /// </summary>
        /// <value>
        /// The invoice_details_last_packing_change.
        /// </value>
        private decimal _invoice_details_last_packing_change;

        /// <summary>
        /// Gets or sets the invoice_details_last_packing_change.
        /// </summary>
        /// <value>
        /// The invoice_details_last_packing_change.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_last_packing_change")]
		public decimal invoice_details_last_packing_change
        {
            get { return _invoice_details_last_packing_change; }
            set
            {
                if (_invoice_details_last_packing_change != value)
                {
                    PropertyChanged("invoice_details_last_packing_change");
					_invoice_details_last_packing_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_import_type.
        /// </summary>
        /// <value>
        /// The invoice_details_import_type.
        /// </value>
        private string _invoice_details_import_type;

        /// <summary>
        /// Gets or sets the invoice_details_import_type.
        /// </summary>
        /// <value>
        /// The invoice_details_import_type.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_import_type")]
		public string invoice_details_import_type
        {
            get { return _invoice_details_import_type; }
            set
            {
                if (_invoice_details_import_type != value)
                {
                    PropertyChanged("invoice_details_import_type");
					_invoice_details_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_amount.
        /// </value>
        private decimal _invoice_details_material_discount_amount;

        /// <summary>
        /// Gets or sets the invoice_details_material_discount_amount.
        /// </summary>
        /// <value>
        /// The invoice_details_material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_material_discount_amount")]
		public decimal invoice_details_material_discount_amount
        {
            get { return _invoice_details_material_discount_amount; }
            set
            {
                if (_invoice_details_material_discount_amount != value)
                {
                    PropertyChanged("invoice_details_material_discount_amount");
					_invoice_details_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_bonus_row.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_row.
        /// </value>
        private string _invoice_details_bonus_row;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_row.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_row")]
		public string invoice_details_bonus_row
        {
            get { return _invoice_details_bonus_row; }
            set
            {
                if (_invoice_details_bonus_row != value)
                {
                    PropertyChanged("invoice_details_bonus_row");
					_invoice_details_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_color.
        /// </summary>
        /// <value>
        /// The invoice_details_color.
        /// </value>
        private long _invoice_details_color;

        /// <summary>
        /// Gets or sets the invoice_details_color.
        /// </summary>
        /// <value>
        /// The invoice_details_color.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_color")]
		public long invoice_details_color
        {
            get { return _invoice_details_color; }
            set
            {
                if (_invoice_details_color != value)
                {
                    PropertyChanged("invoice_details_color");
					_invoice_details_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_pay_shape.
        /// </summary>
        /// <value>
        /// The invoice_details_pay_shape.
        /// </value>
        private long _invoice_details_pay_shape;

        /// <summary>
        /// Gets or sets the invoice_details_pay_shape.
        /// </summary>
        /// <value>
        /// The invoice_details_pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_pay_shape")]
		public long invoice_details_pay_shape
        {
            get { return _invoice_details_pay_shape; }
            set
            {
                if (_invoice_details_pay_shape != value)
                {
                    PropertyChanged("invoice_details_pay_shape");
					_invoice_details_pay_shape = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_indicator.
        /// </summary>
        /// <value>
        /// The invoice_details_indicator.
        /// </value>
        private long _invoice_details_indicator;

        /// <summary>
        /// Gets or sets the invoice_details_indicator.
        /// </summary>
        /// <value>
        /// The invoice_details_indicator.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_indicator")]
		public long invoice_details_indicator
        {
            get { return _invoice_details_indicator; }
            set
            {
                if (_invoice_details_indicator != value)
                {
                    PropertyChanged("invoice_details_indicator");
					_invoice_details_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
        private decimal _invoice_details_bonus_discount;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_discount")]
		public decimal invoice_details_bonus_discount
        {
            get { return _invoice_details_bonus_discount; }
            set
            {
                if (_invoice_details_bonus_discount != value)
                {
                    PropertyChanged("invoice_details_bonus_discount");
					_invoice_details_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
        private decimal _invoice_details_supplier_discount_percent;

        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_supplier_discount_percent")]
		public decimal invoice_details_supplier_discount_percent
        {
            get { return _invoice_details_supplier_discount_percent; }
            set
            {
                if (_invoice_details_supplier_discount_percent != value)
                {
                    PropertyChanged("invoice_details_supplier_discount_percent");
					_invoice_details_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The invoice_details_current_catalog_sell_price.
        /// </value>
        private decimal _invoice_details_current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the invoice_details_current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The invoice_details_current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_current_catalog_sell_price")]
		public decimal invoice_details_current_catalog_sell_price
        {
            get { return _invoice_details_current_catalog_sell_price; }
            set
            {
                if (_invoice_details_current_catalog_sell_price != value)
                {
                    PropertyChanged("invoice_details_current_catalog_sell_price");
					_invoice_details_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
        private DateTime? _invoice_details_expiration_date;

        /// <summary>
        /// Gets or sets the invoice_details_expiration_date.
        /// </summary>
        /// <value>
        /// The invoice_details_expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_expiration_date")]
		public DateTime? invoice_details_expiration_date
        {
            get { return _invoice_details_expiration_date; }
            set
            {
                if (_invoice_details_expiration_date != value)
                {
                    PropertyChanged("invoice_details_expiration_date");
					_invoice_details_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_mini_terminal.
        /// </summary>
        /// <value>
        /// The invoice_details_mini_terminal.
        /// </value>
        private long _invoice_details_mini_terminal;

        /// <summary>
        /// Gets or sets the invoice_details_mini_terminal.
        /// </summary>
        /// <value>
        /// The invoice_details_mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_mini_terminal")]
		public long invoice_details_mini_terminal
        {
            get { return _invoice_details_mini_terminal; }
            set
            {
                if (_invoice_details_mini_terminal != value)
                {
                    PropertyChanged("invoice_details_mini_terminal");
					_invoice_details_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
        private string _materials_material_name;

        /// <summary>
        /// Gets or sets the materials_material_name.
        /// </summary>
        /// <value>
        /// The materials_material_name.
        /// </value>
	    [Editable(true)]
        [Column("materials_material_name")]
		public string materials_material_name
        {
            get { return _materials_material_name; }
            set
            {
                if (_materials_material_name != value)
                {
                    PropertyChanged("materials_material_name");
					_materials_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
        private long _materials_min_valid_months;

        /// <summary>
        /// Gets or sets the materials_min_valid_months.
        /// </summary>
        /// <value>
        /// The materials_min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("materials_min_valid_months")]
		public long materials_min_valid_months
        {
            get { return _materials_min_valid_months; }
            set
            {
                if (_materials_min_valid_months != value)
                {
                    PropertyChanged("materials_min_valid_months");
					_materials_min_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
		public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
        private long _row_no;

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
	    [Editable(true)]
        [Column("row_no")]
		public long row_no
        {
            get { return _row_no; }
            set
            {
                if (_row_no != value)
                {
                    PropertyChanged("row_no");
					_row_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
        private long _doc_no_en;

        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
	    [Editable(true)]
        [Column("doc_no_en")]
		public long doc_no_en
        {
            get { return _doc_no_en; }
            set
            {
                if (_doc_no_en != value)
                {
                    PropertyChanged("doc_no_en");
					_doc_no_en = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
        private string _compute_1;

        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_1")]
		public string compute_1
        {
            get { return _compute_1; }
            set
            {
                if (_compute_1 != value)
                {
                    PropertyChanged("compute_1");
					_compute_1 = value;
                    
                }
            }
        }
	}
}
