using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_sql_supplier_order_move : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_sql_supplier_order_move()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="order_number">The order_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="supply_date">The supply_date.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="station_num">The station_num.</param>
		/// <param name="store_number">The store_number.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="state">The state.</param>
		/// <param name="expected_total_amount">The expected_total_amount.</param>
		/// <param name="discount">The discount.</param>
		/// <param name="discount_percent">The discount_percent.</param>
		/// <param name="mam">The mam.</param>
		/// <param name="order_total">The order_total.</param>
		/// <param name="web_number">The web_number.</param>
		/// <param name="last_month_period">The last_month_period.</param>
		/// <param name="last_month_time_back">The last_month_time_back.</param>
		/// <param name="two_months_period">The two_months_period.</param>
		/// <param name="two_months_time_back">The two_months_time_back.</param>
		/// <param name="time_period_of_sales">The time_period_of_sales.</param>
		/// <param name="order_type">The order_type.</param>
		/// <param name="calc_avg_period">The calc_avg_period.</param>
		/// <param name="cancel_datetime">The cancel_datetime.</param>
		/// <param name="payment_code_number">The payment_code_number.</param>
		/// <param name="payments_number">The payments_number.</param>
        public d_sql_supplier_order_move(long order_number, long branch_number, long supplier_number, long employee_number, DateTime? supply_date, DateTime? date_move, long station_num, long store_number, long stock_number, string state, decimal expected_total_amount, decimal discount, decimal discount_percent, decimal mam, decimal order_total, long web_number, long last_month_period, long last_month_time_back, long two_months_period, long two_months_time_back, long time_period_of_sales, long order_type, long calc_avg_period, DateTime? cancel_datetime, long payment_code_number, long payments_number)
        {
			this._order_number = order_number;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._employee_number = employee_number;
			this._supply_date = supply_date;
			this._date_move = date_move;
			this._station_num = station_num;
			this._store_number = store_number;
			this._stock_number = stock_number;
			this._state = state;
			this._expected_total_amount = expected_total_amount;
			this._discount = discount;
			this._discount_percent = discount_percent;
			this._mam = mam;
			this._order_total = order_total;
			this._web_number = web_number;
			this._last_month_period = last_month_period;
			this._last_month_time_back = last_month_time_back;
			this._two_months_period = two_months_period;
			this._two_months_time_back = two_months_time_back;
			this._time_period_of_sales = time_period_of_sales;
			this._order_type = order_type;
			this._calc_avg_period = calc_avg_period;
			this._cancel_datetime = cancel_datetime;
			this._payment_code_number = payment_code_number;
			this._payments_number = payments_number;
		}

	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
        private DateTime? _supply_date;

        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
	    [Editable(true)]
        [Column("supply_date")]
		public DateTime? supply_date
        {
            get { return _supply_date; }
            set
            {
                if (_supply_date != value)
                {
                    PropertyChanged("supply_date");
					_supply_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
        private long _station_num;

        /// <summary>
        /// Gets or sets the station_num.
        /// </summary>
        /// <value>
        /// The station_num.
        /// </value>
	    [Editable(true)]
        [Column("station_num")]
		public long station_num
        {
            get { return _station_num; }
            set
            {
                if (_station_num != value)
                {
                    PropertyChanged("station_num");
					_station_num = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
        private long _store_number;

        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
	    [Editable(true)]
        [Column("store_number")]
		public long store_number
        {
            get { return _store_number; }
            set
            {
                if (_store_number != value)
                {
                    PropertyChanged("store_number");
					_store_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
        private decimal _expected_total_amount;

        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
	    [Editable(true)]
        [Column("expected_total_amount")]
		public decimal expected_total_amount
        {
            get { return _expected_total_amount; }
            set
            {
                if (_expected_total_amount != value)
                {
                    PropertyChanged("expected_total_amount");
					_expected_total_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
        private decimal _discount;

        /// <summary>
        /// Gets or sets the discount.
        /// </summary>
        /// <value>
        /// The discount.
        /// </value>
	    [Editable(true)]
        [Column("discount")]
		public decimal discount
        {
            get { return _discount; }
            set
            {
                if (_discount != value)
                {
                    PropertyChanged("discount");
					_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
        private decimal _discount_percent;

        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("discount_percent")]
		public decimal discount_percent
        {
            get { return _discount_percent; }
            set
            {
                if (_discount_percent != value)
                {
                    PropertyChanged("discount_percent");
					_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
        private decimal _mam;

        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
	    [Editable(true)]
        [Column("mam")]
		public decimal mam
        {
            get { return _mam; }
            set
            {
                if (_mam != value)
                {
                    PropertyChanged("mam");
					_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_total.
        /// </summary>
        /// <value>
        /// The order_total.
        /// </value>
        private decimal _order_total;

        /// <summary>
        /// Gets or sets the order_total.
        /// </summary>
        /// <value>
        /// The order_total.
        /// </value>
	    [Editable(true)]
        [Column("order_total")]
		public decimal order_total
        {
            get { return _order_total; }
            set
            {
                if (_order_total != value)
                {
                    PropertyChanged("order_total");
					_order_total = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the web_number.
        /// </summary>
        /// <value>
        /// The web_number.
        /// </value>
        private long _web_number;

        /// <summary>
        /// Gets or sets the web_number.
        /// </summary>
        /// <value>
        /// The web_number.
        /// </value>
	    [Editable(true)]
        [Column("web_number")]
		public long web_number
        {
            get { return _web_number; }
            set
            {
                if (_web_number != value)
                {
                    PropertyChanged("web_number");
					_web_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_month_period.
        /// </summary>
        /// <value>
        /// The last_month_period.
        /// </value>
        private long _last_month_period;

        /// <summary>
        /// Gets or sets the last_month_period.
        /// </summary>
        /// <value>
        /// The last_month_period.
        /// </value>
	    [Editable(true)]
        [Column("last_month_period")]
		public long last_month_period
        {
            get { return _last_month_period; }
            set
            {
                if (_last_month_period != value)
                {
                    PropertyChanged("last_month_period");
					_last_month_period = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_month_time_back.
        /// </summary>
        /// <value>
        /// The last_month_time_back.
        /// </value>
        private long _last_month_time_back;

        /// <summary>
        /// Gets or sets the last_month_time_back.
        /// </summary>
        /// <value>
        /// The last_month_time_back.
        /// </value>
	    [Editable(true)]
        [Column("last_month_time_back")]
		public long last_month_time_back
        {
            get { return _last_month_time_back; }
            set
            {
                if (_last_month_time_back != value)
                {
                    PropertyChanged("last_month_time_back");
					_last_month_time_back = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the two_months_period.
        /// </summary>
        /// <value>
        /// The two_months_period.
        /// </value>
        private long _two_months_period;

        /// <summary>
        /// Gets or sets the two_months_period.
        /// </summary>
        /// <value>
        /// The two_months_period.
        /// </value>
	    [Editable(true)]
        [Column("two_months_period")]
		public long two_months_period
        {
            get { return _two_months_period; }
            set
            {
                if (_two_months_period != value)
                {
                    PropertyChanged("two_months_period");
					_two_months_period = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the two_months_time_back.
        /// </summary>
        /// <value>
        /// The two_months_time_back.
        /// </value>
        private long _two_months_time_back;

        /// <summary>
        /// Gets or sets the two_months_time_back.
        /// </summary>
        /// <value>
        /// The two_months_time_back.
        /// </value>
	    [Editable(true)]
        [Column("two_months_time_back")]
		public long two_months_time_back
        {
            get { return _two_months_time_back; }
            set
            {
                if (_two_months_time_back != value)
                {
                    PropertyChanged("two_months_time_back");
					_two_months_time_back = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the time_period_of_sales.
        /// </summary>
        /// <value>
        /// The time_period_of_sales.
        /// </value>
        private long _time_period_of_sales;

        /// <summary>
        /// Gets or sets the time_period_of_sales.
        /// </summary>
        /// <value>
        /// The time_period_of_sales.
        /// </value>
	    [Editable(true)]
        [Column("time_period_of_sales")]
		public long time_period_of_sales
        {
            get { return _time_period_of_sales; }
            set
            {
                if (_time_period_of_sales != value)
                {
                    PropertyChanged("time_period_of_sales");
					_time_period_of_sales = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_type.
        /// </summary>
        /// <value>
        /// The order_type.
        /// </value>
        private long _order_type;

        /// <summary>
        /// Gets or sets the order_type.
        /// </summary>
        /// <value>
        /// The order_type.
        /// </value>
	    [Editable(true)]
        [Column("order_type")]
		public long order_type
        {
            get { return _order_type; }
            set
            {
                if (_order_type != value)
                {
                    PropertyChanged("order_type");
					_order_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the calc_avg_period.
        /// </summary>
        /// <value>
        /// The calc_avg_period.
        /// </value>
        private long _calc_avg_period;

        /// <summary>
        /// Gets or sets the calc_avg_period.
        /// </summary>
        /// <value>
        /// The calc_avg_period.
        /// </value>
	    [Editable(true)]
        [Column("calc_avg_period")]
		public long calc_avg_period
        {
            get { return _calc_avg_period; }
            set
            {
                if (_calc_avg_period != value)
                {
                    PropertyChanged("calc_avg_period");
					_calc_avg_period = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cancel_datetime.
        /// </summary>
        /// <value>
        /// The cancel_datetime.
        /// </value>
        private DateTime? _cancel_datetime;

        /// <summary>
        /// Gets or sets the cancel_datetime.
        /// </summary>
        /// <value>
        /// The cancel_datetime.
        /// </value>
	    [Editable(true)]
        [Column("cancel_datetime")]
		public DateTime? cancel_datetime
        {
            get { return _cancel_datetime; }
            set
            {
                if (_cancel_datetime != value)
                {
                    PropertyChanged("cancel_datetime");
					_cancel_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
        private long _payment_code_number;

        /// <summary>
        /// Gets or sets the payment_code_number.
        /// </summary>
        /// <value>
        /// The payment_code_number.
        /// </value>
	    [Editable(true)]
        [Column("payment_code_number")]
		public long payment_code_number
        {
            get { return _payment_code_number; }
            set
            {
                if (_payment_code_number != value)
                {
                    PropertyChanged("payment_code_number");
					_payment_code_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
        private long _payments_number;

        /// <summary>
        /// Gets or sets the payments_number.
        /// </summary>
        /// <value>
        /// The payments_number.
        /// </value>
	    [Editable(true)]
        [Column("payments_number")]
		public long payments_number
        {
            get { return _payments_number; }
            set
            {
                if (_payments_number != value)
                {
                    PropertyChanged("payments_number");
					_payments_number = value;
                    
                }
            }
        }
	}
}
