using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dwh_invoice_print2_old : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dwh_invoice_print2_old()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="materials_barcode">The materials_barcode.</param>
		/// <param name="material_name">The material_name.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="invoice_move_discount">The invoice_move_discount.</param>
		/// <param name="mam">The mam.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="material_new_price">The material_new_price.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="employee_number">The employee_number.</param>
		/// <param name="supply_date">The supply_date.</param>
		/// <param name="compute_0013">The compute_0013.</param>
		/// <param name="employee_name">The employee_name.</param>
		/// <param name="supplier_name">The supplier_name.</param>
		/// <param name="vat_types_percentage">The vat_types_percentage.</param>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="color">The color.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="material_bonus_connect">The material_bonus_connect.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="discount_percent">The discount_percent.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="logo">The logo.</param>
		/// <param name="log_book">The log_book.</param>
		/// <param name="catalog_entities_entity_number">The catalog_entities_entity_number.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="order_date">The order_date.</param>
		/// <param name="bonus_discount">The bonus_discount.</param>
		/// <param name="invoice_type">The invoice_type.</param>
		/// <param name="campaign_new_price">The campaign_new_price.</param>
		/// <param name="campaign_name">The campaign_name.</param>
		/// <param name="invoice_details_indicator">The invoice_details_indicator.</param>
		/// <param name="flag_color">The flag_color.</param>
		/// <param name="flag_color_1">The flag_color_1.</param>
		/// <param name="flag_color_2">The flag_color_2.</param>
		/// <param name="flag_color_3">The flag_color_3.</param>
		/// <param name="vat_number">The vat_number.</param>
		/// <param name="phone_number">The phone_number.</param>
		/// <param name="fax_number">The fax_number.</param>
		/// <param name="expected_total_amount">The expected_total_amount.</param>
		/// <param name="invoice_details_supplier_discount_percent">The invoice_details_supplier_discount_percent.</param>
		/// <param name="long_barcode_ind">The long_barcode_ind.</param>
		/// <param name="introduction_campaign">The introduction_campaign.</param>
		/// <param name="invoice_move_distributor_number">The invoice_move_distributor_number.</param>
		/// <param name="distributor_name">The distributor_name.</param>
		/// <param name="state">The state.</param>
		/// <param name="compute_gp_1">The compute_gp_1.</param>
		/// <param name="compute_gp">The compute_gp.</param>
		/// <param name="materials_name_1">The materials_name_1.</param>
		/// <param name="material_value">The material_value.</param>
		/// <param name="comp_new_price">The comp_new_price.</param>
		/// <param name="compute_6">The compute_6.</param>
		/// <param name="material_new_price1">The material_new_price1.</param>
		/// <param name="long_barcode">The long_barcode.</param>
		/// <param name="line_number">The line_number.</param>
		/// <param name="compute_3">The compute_3.</param>
		/// <param name="total_quantity">The total_quantity.</param>
		/// <param name="compute_2">The compute_2.</param>
		/// <param name="compute_7">The compute_7.</param>
		/// <param name="total_cf">The total_cf.</param>
		/// <param name="invoice_total_cf">The invoice_total_cf.</param>
		/// <param name="compute_vat">The compute_vat.</param>
		/// <param name="mam_total_cf">The mam_total_cf.</param>
		/// <param name="compute_total">The compute_total.</param>
		/// <param name="compute_4">The compute_4.</param>
		/// <param name="total_vat">The total_vat.</param>
		/// <param name="compute_5">The compute_5.</param>
		/// <param name="compute_1">The compute_1.</param>
		/// <param name="compute_8">The compute_8.</param>
        public dwh_invoice_print2_old(string materials_barcode, string material_name, decimal material_price, decimal material_quantity, decimal invoice_move_discount, decimal mam, decimal material_discount_percent, decimal material_new_price, long invoice_number, long supplier_number, long employee_number, DateTime? supply_date, string compute_0013, string employee_name, string supplier_name, decimal vat_types_percentage, long serial_number, long color, decimal bonus_quantity, long material_bonus_connect, string import_type, decimal sell_price, decimal discount_percent, string branch_name, string logo, long log_book, long catalog_entities_entity_number, DateTime? date_move, long order_number, DateTime? order_date, decimal bonus_discount, string invoice_type, decimal campaign_new_price, string campaign_name, long invoice_details_indicator, long flag_color, long flag_color_1, long flag_color_2, long flag_color_3, string vat_number, string phone_number, string fax_number, decimal expected_total_amount, decimal invoice_details_supplier_discount_percent, long long_barcode_ind, long introduction_campaign, long invoice_move_distributor_number, string distributor_name, string state, string compute_gp_1, string compute_gp, string materials_name_1, string material_value, string comp_new_price, string compute_6, string material_new_price1, string long_barcode, string line_number, string compute_3, string total_quantity, string compute_2, string compute_7, string total_cf, string invoice_total_cf, string compute_vat, string mam_total_cf, string compute_total, string compute_4, string total_vat, string compute_5, string compute_1, string compute_8)
        {
			this._materials_barcode = materials_barcode;
			this._material_name = material_name;
			this._material_price = material_price;
			this._material_quantity = material_quantity;
			this._invoice_move_discount = invoice_move_discount;
			this._mam = mam;
			this._material_discount_percent = material_discount_percent;
			this._material_new_price = material_new_price;
			this._invoice_number = invoice_number;
			this._supplier_number = supplier_number;
			this._employee_number = employee_number;
			this._supply_date = supply_date;
			this._compute_0013 = compute_0013;
			this._employee_name = employee_name;
			this._supplier_name = supplier_name;
			this._vat_types_percentage = vat_types_percentage;
			this._serial_number = serial_number;
			this._color = color;
			this._bonus_quantity = bonus_quantity;
			this._material_bonus_connect = material_bonus_connect;
			this._import_type = import_type;
			this._sell_price = sell_price;
			this._discount_percent = discount_percent;
			this._branch_name = branch_name;
			this._logo = logo;
			this._log_book = log_book;
			this._catalog_entities_entity_number = catalog_entities_entity_number;
			this._date_move = date_move;
			this._order_number = order_number;
			this._order_date = order_date;
			this._bonus_discount = bonus_discount;
			this._invoice_type = invoice_type;
			this._campaign_new_price = campaign_new_price;
			this._campaign_name = campaign_name;
			this._invoice_details_indicator = invoice_details_indicator;
			this._flag_color = flag_color;
			this._flag_color_1 = flag_color_1;
			this._flag_color_2 = flag_color_2;
			this._flag_color_3 = flag_color_3;
			this._vat_number = vat_number;
			this._phone_number = phone_number;
			this._fax_number = fax_number;
			this._expected_total_amount = expected_total_amount;
			this._invoice_details_supplier_discount_percent = invoice_details_supplier_discount_percent;
			this._long_barcode_ind = long_barcode_ind;
			this._introduction_campaign = introduction_campaign;
			this._invoice_move_distributor_number = invoice_move_distributor_number;
			this._distributor_name = distributor_name;
			this._state = state;
			this._compute_gp_1 = compute_gp_1;
			this._compute_gp = compute_gp;
			this._materials_name_1 = materials_name_1;
			this._material_value = material_value;
			this._comp_new_price = comp_new_price;
			this._compute_6 = compute_6;
			this._material_new_price1 = material_new_price1;
			this._long_barcode = long_barcode;
			this._line_number = line_number;
			this._compute_3 = compute_3;
			this._total_quantity = total_quantity;
			this._compute_2 = compute_2;
			this._compute_7 = compute_7;
			this._total_cf = total_cf;
			this._invoice_total_cf = invoice_total_cf;
			this._compute_vat = compute_vat;
			this._mam_total_cf = mam_total_cf;
			this._compute_total = compute_total;
			this._compute_4 = compute_4;
			this._total_vat = total_vat;
			this._compute_5 = compute_5;
			this._compute_1 = compute_1;
			this._compute_8 = compute_8;
		}

	
        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
        private string _materials_barcode;

        /// <summary>
        /// Gets or sets the materials_barcode.
        /// </summary>
        /// <value>
        /// The materials_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_barcode")]
		public string materials_barcode
        {
            get { return _materials_barcode; }
            set
            {
                if (_materials_barcode != value)
                {
                    PropertyChanged("materials_barcode");
					_materials_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
		public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_discount.
        /// </summary>
        /// <value>
        /// The invoice_move_discount.
        /// </value>
        private decimal _invoice_move_discount;

        /// <summary>
        /// Gets or sets the invoice_move_discount.
        /// </summary>
        /// <value>
        /// The invoice_move_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_discount")]
		public decimal invoice_move_discount
        {
            get { return _invoice_move_discount; }
            set
            {
                if (_invoice_move_discount != value)
                {
                    PropertyChanged("invoice_move_discount");
					_invoice_move_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
        private decimal _mam;

        /// <summary>
        /// Gets or sets the mam.
        /// </summary>
        /// <value>
        /// The mam.
        /// </value>
	    [Editable(true)]
        [Column("mam")]
		public decimal mam
        {
            get { return _mam; }
            set
            {
                if (_mam != value)
                {
                    PropertyChanged("mam");
					_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
		public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_new_price.
        /// </summary>
        /// <value>
        /// The material_new_price.
        /// </value>
        private decimal _material_new_price;

        /// <summary>
        /// Gets or sets the material_new_price.
        /// </summary>
        /// <value>
        /// The material_new_price.
        /// </value>
	    [Editable(true)]
        [Column("material_new_price")]
		public decimal material_new_price
        {
            get { return _material_new_price; }
            set
            {
                if (_material_new_price != value)
                {
                    PropertyChanged("material_new_price");
					_material_new_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
        private long _employee_number;

        /// <summary>
        /// Gets or sets the employee_number.
        /// </summary>
        /// <value>
        /// The employee_number.
        /// </value>
	    [Editable(true)]
        [Column("employee_number")]
		public long employee_number
        {
            get { return _employee_number; }
            set
            {
                if (_employee_number != value)
                {
                    PropertyChanged("employee_number");
					_employee_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
        private DateTime? _supply_date;

        /// <summary>
        /// Gets or sets the supply_date.
        /// </summary>
        /// <value>
        /// The supply_date.
        /// </value>
	    [Editable(true)]
        [Column("supply_date")]
		public DateTime? supply_date
        {
            get { return _supply_date; }
            set
            {
                if (_supply_date != value)
                {
                    PropertyChanged("supply_date");
					_supply_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_0013.
        /// </summary>
        /// <value>
        /// The compute_0013.
        /// </value>
        private string _compute_0013;

        /// <summary>
        /// Gets or sets the compute_0013.
        /// </summary>
        /// <value>
        /// The compute_0013.
        /// </value>
	    [Editable(true)]
        [Column("compute_0013")]
		public string compute_0013
        {
            get { return _compute_0013; }
            set
            {
                if (_compute_0013 != value)
                {
                    PropertyChanged("compute_0013");
					_compute_0013 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
        private string _employee_name;

        /// <summary>
        /// Gets or sets the employee_name.
        /// </summary>
        /// <value>
        /// The employee_name.
        /// </value>
	    [Editable(true)]
        [Column("employee_name")]
		public string employee_name
        {
            get { return _employee_name; }
            set
            {
                if (_employee_name != value)
                {
                    PropertyChanged("employee_name");
					_employee_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
        private string _supplier_name;

        /// <summary>
        /// Gets or sets the supplier_name.
        /// </summary>
        /// <value>
        /// The supplier_name.
        /// </value>
	    [Editable(true)]
        [Column("supplier_name")]
		public string supplier_name
        {
            get { return _supplier_name; }
            set
            {
                if (_supplier_name != value)
                {
                    PropertyChanged("supplier_name");
					_supplier_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the vat_types_percentage.
        /// </summary>
        /// <value>
        /// The vat_types_percentage.
        /// </value>
        private decimal _vat_types_percentage;

        /// <summary>
        /// Gets or sets the vat_types_percentage.
        /// </summary>
        /// <value>
        /// The vat_types_percentage.
        /// </value>
	    [Editable(true)]
        [Column("vat_types_percentage")]
		public decimal vat_types_percentage
        {
            get { return _vat_types_percentage; }
            set
            {
                if (_vat_types_percentage != value)
                {
                    PropertyChanged("vat_types_percentage");
					_vat_types_percentage = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
		public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
		public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
		public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
        private decimal _discount_percent;

        /// <summary>
        /// Gets or sets the discount_percent.
        /// </summary>
        /// <value>
        /// The discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("discount_percent")]
		public decimal discount_percent
        {
            get { return _discount_percent; }
            set
            {
                if (_discount_percent != value)
                {
                    PropertyChanged("discount_percent");
					_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
		public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the logo.
        /// </summary>
        /// <value>
        /// The logo.
        /// </value>
        private string _logo;

        /// <summary>
        /// Gets or sets the logo.
        /// </summary>
        /// <value>
        /// The logo.
        /// </value>
	    [Editable(true)]
        [Column("logo")]
		public string logo
        {
            get { return _logo; }
            set
            {
                if (_logo != value)
                {
                    PropertyChanged("logo");
					_logo = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
        private long _log_book;

        /// <summary>
        /// Gets or sets the log_book.
        /// </summary>
        /// <value>
        /// The log_book.
        /// </value>
	    [Editable(true)]
        [Column("log_book")]
		public long log_book
        {
            get { return _log_book; }
            set
            {
                if (_log_book != value)
                {
                    PropertyChanged("log_book");
					_log_book = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the catalog_entities_entity_number.
        /// </summary>
        /// <value>
        /// The catalog_entities_entity_number.
        /// </value>
        private long _catalog_entities_entity_number;

        /// <summary>
        /// Gets or sets the catalog_entities_entity_number.
        /// </summary>
        /// <value>
        /// The catalog_entities_entity_number.
        /// </value>
	    [Editable(true)]
        [Column("catalog_entities_entity_number")]
		public long catalog_entities_entity_number
        {
            get { return _catalog_entities_entity_number; }
            set
            {
                if (_catalog_entities_entity_number != value)
                {
                    PropertyChanged("catalog_entities_entity_number");
					_catalog_entities_entity_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_date.
        /// </summary>
        /// <value>
        /// The order_date.
        /// </value>
        private DateTime? _order_date;

        /// <summary>
        /// Gets or sets the order_date.
        /// </summary>
        /// <value>
        /// The order_date.
        /// </value>
	    [Editable(true)]
        [Column("order_date")]
		public DateTime? order_date
        {
            get { return _order_date; }
            set
            {
                if (_order_date != value)
                {
                    PropertyChanged("order_date");
					_order_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
        private decimal _bonus_discount;

        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("bonus_discount")]
		public decimal bonus_discount
        {
            get { return _bonus_discount; }
            set
            {
                if (_bonus_discount != value)
                {
                    PropertyChanged("bonus_discount");
					_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
        private string _invoice_type;

        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
	    [Editable(true)]
        [Column("invoice_type")]
		public string invoice_type
        {
            get { return _invoice_type; }
            set
            {
                if (_invoice_type != value)
                {
                    PropertyChanged("invoice_type");
					_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_new_price.
        /// </summary>
        /// <value>
        /// The campaign_new_price.
        /// </value>
        private decimal _campaign_new_price;

        /// <summary>
        /// Gets or sets the campaign_new_price.
        /// </summary>
        /// <value>
        /// The campaign_new_price.
        /// </value>
	    [Editable(true)]
        [Column("campaign_new_price")]
		public decimal campaign_new_price
        {
            get { return _campaign_new_price; }
            set
            {
                if (_campaign_new_price != value)
                {
                    PropertyChanged("campaign_new_price");
					_campaign_new_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_name.
        /// </summary>
        /// <value>
        /// The campaign_name.
        /// </value>
        private string _campaign_name;

        /// <summary>
        /// Gets or sets the campaign_name.
        /// </summary>
        /// <value>
        /// The campaign_name.
        /// </value>
	    [Editable(true)]
        [Column("campaign_name")]
		public string campaign_name
        {
            get { return _campaign_name; }
            set
            {
                if (_campaign_name != value)
                {
                    PropertyChanged("campaign_name");
					_campaign_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_indicator.
        /// </summary>
        /// <value>
        /// The invoice_details_indicator.
        /// </value>
        private long _invoice_details_indicator;

        /// <summary>
        /// Gets or sets the invoice_details_indicator.
        /// </summary>
        /// <value>
        /// The invoice_details_indicator.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_indicator")]
		public long invoice_details_indicator
        {
            get { return _invoice_details_indicator; }
            set
            {
                if (_invoice_details_indicator != value)
                {
                    PropertyChanged("invoice_details_indicator");
					_invoice_details_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color.
        /// </summary>
        /// <value>
        /// The flag_color.
        /// </value>
        private long _flag_color;

        /// <summary>
        /// Gets or sets the flag_color.
        /// </summary>
        /// <value>
        /// The flag_color.
        /// </value>
	    [Editable(true)]
        [Column("flag_color")]
		public long flag_color
        {
            get { return _flag_color; }
            set
            {
                if (_flag_color != value)
                {
                    PropertyChanged("flag_color");
					_flag_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color_1.
        /// </summary>
        /// <value>
        /// The flag_color_1.
        /// </value>
        private long _flag_color_1;

        /// <summary>
        /// Gets or sets the flag_color_1.
        /// </summary>
        /// <value>
        /// The flag_color_1.
        /// </value>
	    [Editable(true)]
        [Column("flag_color_1")]
		public long flag_color_1
        {
            get { return _flag_color_1; }
            set
            {
                if (_flag_color_1 != value)
                {
                    PropertyChanged("flag_color_1");
					_flag_color_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color_2.
        /// </summary>
        /// <value>
        /// The flag_color_2.
        /// </value>
        private long _flag_color_2;

        /// <summary>
        /// Gets or sets the flag_color_2.
        /// </summary>
        /// <value>
        /// The flag_color_2.
        /// </value>
	    [Editable(true)]
        [Column("flag_color_2")]
		public long flag_color_2
        {
            get { return _flag_color_2; }
            set
            {
                if (_flag_color_2 != value)
                {
                    PropertyChanged("flag_color_2");
					_flag_color_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color_3.
        /// </summary>
        /// <value>
        /// The flag_color_3.
        /// </value>
        private long _flag_color_3;

        /// <summary>
        /// Gets or sets the flag_color_3.
        /// </summary>
        /// <value>
        /// The flag_color_3.
        /// </value>
	    [Editable(true)]
        [Column("flag_color_3")]
		public long flag_color_3
        {
            get { return _flag_color_3; }
            set
            {
                if (_flag_color_3 != value)
                {
                    PropertyChanged("flag_color_3");
					_flag_color_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
        private string _vat_number;

        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
	    [Editable(true)]
        [Column("vat_number")]
		public string vat_number
        {
            get { return _vat_number; }
            set
            {
                if (_vat_number != value)
                {
                    PropertyChanged("vat_number");
					_vat_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the phone_number.
        /// </summary>
        /// <value>
        /// The phone_number.
        /// </value>
        private string _phone_number;

        /// <summary>
        /// Gets or sets the phone_number.
        /// </summary>
        /// <value>
        /// The phone_number.
        /// </value>
	    [Editable(true)]
        [Column("phone_number")]
		public string phone_number
        {
            get { return _phone_number; }
            set
            {
                if (_phone_number != value)
                {
                    PropertyChanged("phone_number");
					_phone_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the fax_number.
        /// </summary>
        /// <value>
        /// The fax_number.
        /// </value>
        private string _fax_number;

        /// <summary>
        /// Gets or sets the fax_number.
        /// </summary>
        /// <value>
        /// The fax_number.
        /// </value>
	    [Editable(true)]
        [Column("fax_number")]
		public string fax_number
        {
            get { return _fax_number; }
            set
            {
                if (_fax_number != value)
                {
                    PropertyChanged("fax_number");
					_fax_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
        private decimal _expected_total_amount;

        /// <summary>
        /// Gets or sets the expected_total_amount.
        /// </summary>
        /// <value>
        /// The expected_total_amount.
        /// </value>
	    [Editable(true)]
        [Column("expected_total_amount")]
		public decimal expected_total_amount
        {
            get { return _expected_total_amount; }
            set
            {
                if (_expected_total_amount != value)
                {
                    PropertyChanged("expected_total_amount");
					_expected_total_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
        private decimal _invoice_details_supplier_discount_percent;

        /// <summary>
        /// Gets or sets the invoice_details_supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The invoice_details_supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_supplier_discount_percent")]
		public decimal invoice_details_supplier_discount_percent
        {
            get { return _invoice_details_supplier_discount_percent; }
            set
            {
                if (_invoice_details_supplier_discount_percent != value)
                {
                    PropertyChanged("invoice_details_supplier_discount_percent");
					_invoice_details_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the long_barcode_ind.
        /// </summary>
        /// <value>
        /// The long_barcode_ind.
        /// </value>
        private long _long_barcode_ind;

        /// <summary>
        /// Gets or sets the long_barcode_ind.
        /// </summary>
        /// <value>
        /// The long_barcode_ind.
        /// </value>
	    [Editable(true)]
        [Column("long_barcode_ind")]
		public long long_barcode_ind
        {
            get { return _long_barcode_ind; }
            set
            {
                if (_long_barcode_ind != value)
                {
                    PropertyChanged("long_barcode_ind");
					_long_barcode_ind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the introduction_campaign.
        /// </summary>
        /// <value>
        /// The introduction_campaign.
        /// </value>
        private long _introduction_campaign;

        /// <summary>
        /// Gets or sets the introduction_campaign.
        /// </summary>
        /// <value>
        /// The introduction_campaign.
        /// </value>
	    [Editable(true)]
        [Column("introduction_campaign")]
		public long introduction_campaign
        {
            get { return _introduction_campaign; }
            set
            {
                if (_introduction_campaign != value)
                {
                    PropertyChanged("introduction_campaign");
					_introduction_campaign = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_distributor_number.
        /// </summary>
        /// <value>
        /// The invoice_move_distributor_number.
        /// </value>
        private long _invoice_move_distributor_number;

        /// <summary>
        /// Gets or sets the invoice_move_distributor_number.
        /// </summary>
        /// <value>
        /// The invoice_move_distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_distributor_number")]
		public long invoice_move_distributor_number
        {
            get { return _invoice_move_distributor_number; }
            set
            {
                if (_invoice_move_distributor_number != value)
                {
                    PropertyChanged("invoice_move_distributor_number");
					_invoice_move_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_name.
        /// </summary>
        /// <value>
        /// The distributor_name.
        /// </value>
        private string _distributor_name;

        /// <summary>
        /// Gets or sets the distributor_name.
        /// </summary>
        /// <value>
        /// The distributor_name.
        /// </value>
	    [Editable(true)]
        [Column("distributor_name")]
		public string distributor_name
        {
            get { return _distributor_name; }
            set
            {
                if (_distributor_name != value)
                {
                    PropertyChanged("distributor_name");
					_distributor_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_gp_1.
        /// </summary>
        /// <value>
        /// The compute_gp_1.
        /// </value>
        private string _compute_gp_1;

        /// <summary>
        /// Gets or sets the compute_gp_1.
        /// </summary>
        /// <value>
        /// The compute_gp_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_gp_1")]
		public string compute_gp_1
        {
            get { return _compute_gp_1; }
            set
            {
                if (_compute_gp_1 != value)
                {
                    PropertyChanged("compute_gp_1");
					_compute_gp_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_gp.
        /// </summary>
        /// <value>
        /// The compute_gp.
        /// </value>
        private string _compute_gp;

        /// <summary>
        /// Gets or sets the compute_gp.
        /// </summary>
        /// <value>
        /// The compute_gp.
        /// </value>
	    [Editable(true)]
        [Column("compute_gp")]
		public string compute_gp
        {
            get { return _compute_gp; }
            set
            {
                if (_compute_gp != value)
                {
                    PropertyChanged("compute_gp");
					_compute_gp = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the materials_name_1.
        /// </summary>
        /// <value>
        /// The materials_name_1.
        /// </value>
        private string _materials_name_1;

        /// <summary>
        /// Gets or sets the materials_name_1.
        /// </summary>
        /// <value>
        /// The materials_name_1.
        /// </value>
	    [Editable(true)]
        [Column("materials_name_1")]
		public string materials_name_1
        {
            get { return _materials_name_1; }
            set
            {
                if (_materials_name_1 != value)
                {
                    PropertyChanged("materials_name_1");
					_materials_name_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_value.
        /// </summary>
        /// <value>
        /// The material_value.
        /// </value>
        private string _material_value;

        /// <summary>
        /// Gets or sets the material_value.
        /// </summary>
        /// <value>
        /// The material_value.
        /// </value>
	    [Editable(true)]
        [Column("material_value")]
		public string material_value
        {
            get { return _material_value; }
            set
            {
                if (_material_value != value)
                {
                    PropertyChanged("material_value");
					_material_value = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the comp_new_price.
        /// </summary>
        /// <value>
        /// The comp_new_price.
        /// </value>
        private string _comp_new_price;

        /// <summary>
        /// Gets or sets the comp_new_price.
        /// </summary>
        /// <value>
        /// The comp_new_price.
        /// </value>
	    [Editable(true)]
        [Column("comp_new_price")]
		public string comp_new_price
        {
            get { return _comp_new_price; }
            set
            {
                if (_comp_new_price != value)
                {
                    PropertyChanged("comp_new_price");
					_comp_new_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_6.
        /// </summary>
        /// <value>
        /// The compute_6.
        /// </value>
        private string _compute_6;

        /// <summary>
        /// Gets or sets the compute_6.
        /// </summary>
        /// <value>
        /// The compute_6.
        /// </value>
	    [Editable(true)]
        [Column("compute_6")]
		public string compute_6
        {
            get { return _compute_6; }
            set
            {
                if (_compute_6 != value)
                {
                    PropertyChanged("compute_6");
					_compute_6 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_new_price1.
        /// </summary>
        /// <value>
        /// The material_new_price1.
        /// </value>
        private string _material_new_price1;

        /// <summary>
        /// Gets or sets the material_new_price1.
        /// </summary>
        /// <value>
        /// The material_new_price1.
        /// </value>
	    [Editable(true)]
        [Column("material_new_price1")]
		public string material_new_price1
        {
            get { return _material_new_price1; }
            set
            {
                if (_material_new_price1 != value)
                {
                    PropertyChanged("material_new_price1");
					_material_new_price1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the long_barcode.
        /// </summary>
        /// <value>
        /// The long_barcode.
        /// </value>
        private string _long_barcode;

        /// <summary>
        /// Gets or sets the long_barcode.
        /// </summary>
        /// <value>
        /// The long_barcode.
        /// </value>
	    [Editable(true)]
        [Column("long_barcode")]
		public string long_barcode
        {
            get { return _long_barcode; }
            set
            {
                if (_long_barcode != value)
                {
                    PropertyChanged("long_barcode");
					_long_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
        private string _line_number;

        /// <summary>
        /// Gets or sets the line_number.
        /// </summary>
        /// <value>
        /// The line_number.
        /// </value>
	    [Editable(true)]
        [Column("line_number")]
		public string line_number
        {
            get { return _line_number; }
            set
            {
                if (_line_number != value)
                {
                    PropertyChanged("line_number");
					_line_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_3.
        /// </summary>
        /// <value>
        /// The compute_3.
        /// </value>
        private string _compute_3;

        /// <summary>
        /// Gets or sets the compute_3.
        /// </summary>
        /// <value>
        /// The compute_3.
        /// </value>
	    [Editable(true)]
        [Column("compute_3")]
		public string compute_3
        {
            get { return _compute_3; }
            set
            {
                if (_compute_3 != value)
                {
                    PropertyChanged("compute_3");
					_compute_3 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_quantity.
        /// </summary>
        /// <value>
        /// The total_quantity.
        /// </value>
        private string _total_quantity;

        /// <summary>
        /// Gets or sets the total_quantity.
        /// </summary>
        /// <value>
        /// The total_quantity.
        /// </value>
	    [Editable(true)]
        [Column("total_quantity")]
		public string total_quantity
        {
            get { return _total_quantity; }
            set
            {
                if (_total_quantity != value)
                {
                    PropertyChanged("total_quantity");
					_total_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_2.
        /// </summary>
        /// <value>
        /// The compute_2.
        /// </value>
        private string _compute_2;

        /// <summary>
        /// Gets or sets the compute_2.
        /// </summary>
        /// <value>
        /// The compute_2.
        /// </value>
	    [Editable(true)]
        [Column("compute_2")]
		public string compute_2
        {
            get { return _compute_2; }
            set
            {
                if (_compute_2 != value)
                {
                    PropertyChanged("compute_2");
					_compute_2 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_7.
        /// </summary>
        /// <value>
        /// The compute_7.
        /// </value>
        private string _compute_7;

        /// <summary>
        /// Gets or sets the compute_7.
        /// </summary>
        /// <value>
        /// The compute_7.
        /// </value>
	    [Editable(true)]
        [Column("compute_7")]
		public string compute_7
        {
            get { return _compute_7; }
            set
            {
                if (_compute_7 != value)
                {
                    PropertyChanged("compute_7");
					_compute_7 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_cf.
        /// </summary>
        /// <value>
        /// The total_cf.
        /// </value>
        private string _total_cf;

        /// <summary>
        /// Gets or sets the total_cf.
        /// </summary>
        /// <value>
        /// The total_cf.
        /// </value>
	    [Editable(true)]
        [Column("total_cf")]
		public string total_cf
        {
            get { return _total_cf; }
            set
            {
                if (_total_cf != value)
                {
                    PropertyChanged("total_cf");
					_total_cf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_total_cf.
        /// </summary>
        /// <value>
        /// The invoice_total_cf.
        /// </value>
        private string _invoice_total_cf;

        /// <summary>
        /// Gets or sets the invoice_total_cf.
        /// </summary>
        /// <value>
        /// The invoice_total_cf.
        /// </value>
	    [Editable(true)]
        [Column("invoice_total_cf")]
		public string invoice_total_cf
        {
            get { return _invoice_total_cf; }
            set
            {
                if (_invoice_total_cf != value)
                {
                    PropertyChanged("invoice_total_cf");
					_invoice_total_cf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_vat.
        /// </summary>
        /// <value>
        /// The compute_vat.
        /// </value>
        private string _compute_vat;

        /// <summary>
        /// Gets or sets the compute_vat.
        /// </summary>
        /// <value>
        /// The compute_vat.
        /// </value>
	    [Editable(true)]
        [Column("compute_vat")]
		public string compute_vat
        {
            get { return _compute_vat; }
            set
            {
                if (_compute_vat != value)
                {
                    PropertyChanged("compute_vat");
					_compute_vat = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mam_total_cf.
        /// </summary>
        /// <value>
        /// The mam_total_cf.
        /// </value>
        private string _mam_total_cf;

        /// <summary>
        /// Gets or sets the mam_total_cf.
        /// </summary>
        /// <value>
        /// The mam_total_cf.
        /// </value>
	    [Editable(true)]
        [Column("mam_total_cf")]
		public string mam_total_cf
        {
            get { return _mam_total_cf; }
            set
            {
                if (_mam_total_cf != value)
                {
                    PropertyChanged("mam_total_cf");
					_mam_total_cf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_total.
        /// </summary>
        /// <value>
        /// The compute_total.
        /// </value>
        private string _compute_total;

        /// <summary>
        /// Gets or sets the compute_total.
        /// </summary>
        /// <value>
        /// The compute_total.
        /// </value>
	    [Editable(true)]
        [Column("compute_total")]
		public string compute_total
        {
            get { return _compute_total; }
            set
            {
                if (_compute_total != value)
                {
                    PropertyChanged("compute_total");
					_compute_total = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_4.
        /// </summary>
        /// <value>
        /// The compute_4.
        /// </value>
        private string _compute_4;

        /// <summary>
        /// Gets or sets the compute_4.
        /// </summary>
        /// <value>
        /// The compute_4.
        /// </value>
	    [Editable(true)]
        [Column("compute_4")]
		public string compute_4
        {
            get { return _compute_4; }
            set
            {
                if (_compute_4 != value)
                {
                    PropertyChanged("compute_4");
					_compute_4 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_vat.
        /// </summary>
        /// <value>
        /// The total_vat.
        /// </value>
        private string _total_vat;

        /// <summary>
        /// Gets or sets the total_vat.
        /// </summary>
        /// <value>
        /// The total_vat.
        /// </value>
	    [Editable(true)]
        [Column("total_vat")]
		public string total_vat
        {
            get { return _total_vat; }
            set
            {
                if (_total_vat != value)
                {
                    PropertyChanged("total_vat");
					_total_vat = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_5.
        /// </summary>
        /// <value>
        /// The compute_5.
        /// </value>
        private string _compute_5;

        /// <summary>
        /// Gets or sets the compute_5.
        /// </summary>
        /// <value>
        /// The compute_5.
        /// </value>
	    [Editable(true)]
        [Column("compute_5")]
		public string compute_5
        {
            get { return _compute_5; }
            set
            {
                if (_compute_5 != value)
                {
                    PropertyChanged("compute_5");
					_compute_5 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
        private string _compute_1;

        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_1")]
		public string compute_1
        {
            get { return _compute_1; }
            set
            {
                if (_compute_1 != value)
                {
                    PropertyChanged("compute_1");
					_compute_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_8.
        /// </summary>
        /// <value>
        /// The compute_8.
        /// </value>
        private string _compute_8;

        /// <summary>
        /// Gets or sets the compute_8.
        /// </summary>
        /// <value>
        /// The compute_8.
        /// </value>
	    [Editable(true)]
        [Column("compute_8")]
		public string compute_8
        {
            get { return _compute_8; }
            set
            {
                if (_compute_8 != value)
                {
                    PropertyChanged("compute_8");
					_compute_8 = value;
                    
                }
            }
        }
	}
}
