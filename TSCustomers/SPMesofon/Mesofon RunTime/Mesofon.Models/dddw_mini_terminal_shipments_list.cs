using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
    public class dddw_mini_terminal_shipments_list : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dddw_mini_terminal_shipments_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="date_move">The date_move.</param>
		/// <param name="shipment_number">The shipment_number.</param>
        public dddw_mini_terminal_shipments_list(string date_move, double shipment_number)
        {
            this._date_move = date_move;
            this._shipment_number = shipment_number.ToString();
        }


        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private string _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
        [PropertiesAttribute("date_move", "date_move", true, 1, typeof(DateTime))]
        public string date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
                    _date_move = value;

                }
            }
        }

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private string _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>

        [Editable(true)]
        [Column("shipment_number")]
        [PropertiesAttribute("shipment_number", "shipment_number", true, 0)]
        public string shipment_number
        {
            get
            {
                return _shipment_number;

            }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
                    _shipment_number = value;

                }
            }
        }
        //gilad change

    }
}
