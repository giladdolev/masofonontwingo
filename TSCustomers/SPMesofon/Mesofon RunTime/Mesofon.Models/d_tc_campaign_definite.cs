using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_tc_campaign_definite : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_tc_campaign_definite()
        {
            this._campaign_status = "c";
            this._campaign_property = "i";
            this._all_days = "y";
            this._status_flag = "c";
            this._more_year = "n";


        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="campaign_grade">The campaign_grade.</param>
		/// <param name="campaign_status">The campaign_status.</param>
		/// <param name="campaign_description">The campaign_description.</param>
		/// <param name="campaign_name">The campaign_name.</param>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="container_number">The container_number.</param>
		/// <param name="branches_numbers">The branches_numbers.</param>
		/// <param name="start_datetime">The start_datetime.</param>
		/// <param name="end_datetime">The end_datetime.</param>
		/// <param name="campaign_property">The campaign_property.</param>
		/// <param name="club_number">The club_number.</param>
		/// <param name="all_days">The all_days.</param>
		/// <param name="without_days">The without_days.</param>
		/// <param name="compute_start">The compute_start.</param>
		/// <param name="compute_end">The compute_end.</param>
		/// <param name="flag">The flag.</param>
		/// <param name="status_flag">The status_flag.</param>
		/// <param name="compute_branch">The compute_branch.</param>
		/// <param name="more_year">The more_year.</param>
		/// <param name="daily_calc">The daily_calc.</param>
		/// <param name="automatic_campaign">The automatic_campaign.</param>
		/// <param name="external_campaign_number">The external_campaign_number.</param>
		/// <param name="allow_cancel_from_branch">The allow_cancel_from_branch.</param>
		/// <param name="campaign_type">The campaign_type.</param>
		/// <param name="clubs">The clubs.</param>
		/// <param name="stars">The stars.</param>
		/// <param name="campaign_period">The campaign_period.</param>
		/// <param name="adhoc_serial_number">The adhoc_serial_number.</param>
		/// <param name="adhoc_key">The adhoc_key.</param>
        public d_tc_campaign_definite(long campaign_grade, string campaign_status, string campaign_description, string campaign_name, long serial_number, long container_number, string branches_numbers, DateTime? start_datetime, DateTime? end_datetime, string campaign_property, long club_number, string all_days, string without_days, DateTime? compute_start, DateTime? compute_end, string flag, string status_flag, string compute_branch, string more_year, string daily_calc, long automatic_campaign, long external_campaign_number, string allow_cancel_from_branch, long campaign_type, string clubs, long stars, long campaign_period, long adhoc_serial_number, string adhoc_key)
        {
			this._campaign_grade = campaign_grade;
			this._campaign_status = campaign_status;
			this._campaign_description = campaign_description;
			this._campaign_name = campaign_name;
			this._serial_number = serial_number;
			this._container_number = container_number;
			this._branches_numbers = branches_numbers;
			this._start_datetime = start_datetime;
			this._end_datetime = end_datetime;
			this._campaign_property = campaign_property;
			this._club_number = club_number;
			this._all_days = all_days;
			this._without_days = without_days;
			this._compute_start = compute_start;
			this._compute_end = compute_end;
			this._flag = flag;
			this._status_flag = status_flag;
			this._compute_branch = compute_branch;
			this._more_year = more_year;
			this._daily_calc = daily_calc;
			this._automatic_campaign = automatic_campaign;
			this._external_campaign_number = external_campaign_number;
			this._allow_cancel_from_branch = allow_cancel_from_branch;
			this._campaign_type = campaign_type;
			this._clubs = clubs;
			this._stars = stars;
			this._campaign_period = campaign_period;
			this._adhoc_serial_number = adhoc_serial_number;
			this._adhoc_key = adhoc_key;
		}

	
        /// <summary>
        /// Gets or sets the campaign_grade.
        /// </summary>
        /// <value>
        /// The campaign_grade.
        /// </value>
        private long _campaign_grade;

        /// <summary>
        /// Gets or sets the campaign_grade.
        /// </summary>
        /// <value>
        /// The campaign_grade.
        /// </value>
	    [Editable(true)]
        [Column("campaign_grade")]
		public long campaign_grade
        {
            get { return _campaign_grade; }
            set
            {
                if (_campaign_grade != value)
                {
                    PropertyChanged("campaign_grade");
					_campaign_grade = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_status.
        /// </summary>
        /// <value>
        /// The campaign_status.
        /// </value>
        private string _campaign_status;

        /// <summary>
        /// Gets or sets the campaign_status.
        /// </summary>
        /// <value>
        /// The campaign_status.
        /// </value>
	    [Editable(true)]
        [Column("campaign_status")]
		public string campaign_status
        {
            get { return _campaign_status; }
            set
            {
                if (_campaign_status != value)
                {
                    PropertyChanged("campaign_status");
					_campaign_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_description.
        /// </summary>
        /// <value>
        /// The campaign_description.
        /// </value>
        private string _campaign_description;

        /// <summary>
        /// Gets or sets the campaign_description.
        /// </summary>
        /// <value>
        /// The campaign_description.
        /// </value>
	    [Editable(true)]
        [Column("campaign_description")]
		public string campaign_description
        {
            get { return _campaign_description; }
            set
            {
                if (_campaign_description != value)
                {
                    PropertyChanged("campaign_description");
					_campaign_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_name.
        /// </summary>
        /// <value>
        /// The campaign_name.
        /// </value>
        private string _campaign_name;

        /// <summary>
        /// Gets or sets the campaign_name.
        /// </summary>
        /// <value>
        /// The campaign_name.
        /// </value>
	    [Editable(true)]
        [Column("campaign_name")]
		public string campaign_name
        {
            get { return _campaign_name; }
            set
            {
                if (_campaign_name != value)
                {
                    PropertyChanged("campaign_name");
					_campaign_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the container_number.
        /// </summary>
        /// <value>
        /// The container_number.
        /// </value>
        private long _container_number;

        /// <summary>
        /// Gets or sets the container_number.
        /// </summary>
        /// <value>
        /// The container_number.
        /// </value>
	    [Editable(true)]
        [Column("container_number")]
		public long container_number
        {
            get { return _container_number; }
            set
            {
                if (_container_number != value)
                {
                    PropertyChanged("container_number");
					_container_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branches_numbers.
        /// </summary>
        /// <value>
        /// The branches_numbers.
        /// </value>
        private string _branches_numbers;

        /// <summary>
        /// Gets or sets the branches_numbers.
        /// </summary>
        /// <value>
        /// The branches_numbers.
        /// </value>
	    [Editable(true)]
        [Column("branches_numbers")]
		public string branches_numbers
        {
            get { return _branches_numbers; }
            set
            {
                if (_branches_numbers != value)
                {
                    PropertyChanged("branches_numbers");
					_branches_numbers = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the start_datetime.
        /// </summary>
        /// <value>
        /// The start_datetime.
        /// </value>
        private DateTime? _start_datetime;

        /// <summary>
        /// Gets or sets the start_datetime.
        /// </summary>
        /// <value>
        /// The start_datetime.
        /// </value>
	    [Editable(true)]
        [Column("start_datetime")]
		public DateTime? start_datetime
        {
            get { return _start_datetime; }
            set
            {
                if (_start_datetime != value)
                {
                    PropertyChanged("start_datetime");
					_start_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the end_datetime.
        /// </summary>
        /// <value>
        /// The end_datetime.
        /// </value>
        private DateTime? _end_datetime;

        /// <summary>
        /// Gets or sets the end_datetime.
        /// </summary>
        /// <value>
        /// The end_datetime.
        /// </value>
	    [Editable(true)]
        [Column("end_datetime")]
		public DateTime? end_datetime
        {
            get { return _end_datetime; }
            set
            {
                if (_end_datetime != value)
                {
                    PropertyChanged("end_datetime");
					_end_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_property.
        /// </summary>
        /// <value>
        /// The campaign_property.
        /// </value>
        private string _campaign_property;

        /// <summary>
        /// Gets or sets the campaign_property.
        /// </summary>
        /// <value>
        /// The campaign_property.
        /// </value>
	    [Editable(true)]
        [Column("campaign_property")]
		public string campaign_property
        {
            get { return _campaign_property; }
            set
            {
                if (_campaign_property != value)
                {
                    PropertyChanged("campaign_property");
					_campaign_property = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the club_number.
        /// </summary>
        /// <value>
        /// The club_number.
        /// </value>
        private long _club_number;

        /// <summary>
        /// Gets or sets the club_number.
        /// </summary>
        /// <value>
        /// The club_number.
        /// </value>
	    [Editable(true)]
        [Column("club_number")]
		public long club_number
        {
            get { return _club_number; }
            set
            {
                if (_club_number != value)
                {
                    PropertyChanged("club_number");
					_club_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the all_days.
        /// </summary>
        /// <value>
        /// The all_days.
        /// </value>
        private string _all_days;

        /// <summary>
        /// Gets or sets the all_days.
        /// </summary>
        /// <value>
        /// The all_days.
        /// </value>
	    [Editable(true)]
        [Column("all_days")]
		public string all_days
        {
            get { return _all_days; }
            set
            {
                if (_all_days != value)
                {
                    PropertyChanged("all_days");
					_all_days = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the without_days.
        /// </summary>
        /// <value>
        /// The without_days.
        /// </value>
        private string _without_days;

        /// <summary>
        /// Gets or sets the without_days.
        /// </summary>
        /// <value>
        /// The without_days.
        /// </value>
	    [Editable(true)]
        [Column("without_days")]
		public string without_days
        {
            get { return _without_days; }
            set
            {
                if (_without_days != value)
                {
                    PropertyChanged("without_days");
					_without_days = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_start.
        /// </summary>
        /// <value>
        /// The compute_start.
        /// </value>
        private DateTime? _compute_start;

        /// <summary>
        /// Gets or sets the compute_start.
        /// </summary>
        /// <value>
        /// The compute_start.
        /// </value>
	    [Editable(true)]
        [Column("compute_start")]
		public DateTime? compute_start
        {
            get { return _compute_start; }
            set
            {
                if (_compute_start != value)
                {
                    PropertyChanged("compute_start");
					_compute_start = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_end.
        /// </summary>
        /// <value>
        /// The compute_end.
        /// </value>
        private DateTime? _compute_end;

        /// <summary>
        /// Gets or sets the compute_end.
        /// </summary>
        /// <value>
        /// The compute_end.
        /// </value>
	    [Editable(true)]
        [Column("compute_end")]
		public DateTime? compute_end
        {
            get { return _compute_end; }
            set
            {
                if (_compute_end != value)
                {
                    PropertyChanged("compute_end");
					_compute_end = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        /// <value>
        /// The flag.
        /// </value>
        private string _flag;

        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        /// <value>
        /// The flag.
        /// </value>
	    [Editable(true)]
        [Column("flag")]
		public string flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    PropertyChanged("flag");
					_flag = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the status_flag.
        /// </summary>
        /// <value>
        /// The status_flag.
        /// </value>
        private string _status_flag;

        /// <summary>
        /// Gets or sets the status_flag.
        /// </summary>
        /// <value>
        /// The status_flag.
        /// </value>
	    [Editable(true)]
        [Column("status_flag")]
		public string status_flag
        {
            get { return _status_flag; }
            set
            {
                if (_status_flag != value)
                {
                    PropertyChanged("status_flag");
					_status_flag = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_branch.
        /// </summary>
        /// <value>
        /// The compute_branch.
        /// </value>
        private string _compute_branch;

        /// <summary>
        /// Gets or sets the compute_branch.
        /// </summary>
        /// <value>
        /// The compute_branch.
        /// </value>
	    [Editable(true)]
        [Column("compute_branch")]
		public string compute_branch
        {
            get { return _compute_branch; }
            set
            {
                if (_compute_branch != value)
                {
                    PropertyChanged("compute_branch");
					_compute_branch = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the more_year.
        /// </summary>
        /// <value>
        /// The more_year.
        /// </value>
        private string _more_year;

        /// <summary>
        /// Gets or sets the more_year.
        /// </summary>
        /// <value>
        /// The more_year.
        /// </value>
	    [Editable(true)]
        [Column("more_year")]
		public string more_year
        {
            get { return _more_year; }
            set
            {
                if (_more_year != value)
                {
                    PropertyChanged("more_year");
					_more_year = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the daily_calc.
        /// </summary>
        /// <value>
        /// The daily_calc.
        /// </value>
        private string _daily_calc;

        /// <summary>
        /// Gets or sets the daily_calc.
        /// </summary>
        /// <value>
        /// The daily_calc.
        /// </value>
	    [Editable(true)]
        [Column("daily_calc")]
		public string daily_calc
        {
            get { return _daily_calc; }
            set
            {
                if (_daily_calc != value)
                {
                    PropertyChanged("daily_calc");
					_daily_calc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the automatic_campaign.
        /// </summary>
        /// <value>
        /// The automatic_campaign.
        /// </value>
        private long _automatic_campaign;

        /// <summary>
        /// Gets or sets the automatic_campaign.
        /// </summary>
        /// <value>
        /// The automatic_campaign.
        /// </value>
	    [Editable(true)]
        [Column("automatic_campaign")]
		public long automatic_campaign
        {
            get { return _automatic_campaign; }
            set
            {
                if (_automatic_campaign != value)
                {
                    PropertyChanged("automatic_campaign");
					_automatic_campaign = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the external_campaign_number.
        /// </summary>
        /// <value>
        /// The external_campaign_number.
        /// </value>
        private long _external_campaign_number;

        /// <summary>
        /// Gets or sets the external_campaign_number.
        /// </summary>
        /// <value>
        /// The external_campaign_number.
        /// </value>
	    [Editable(true)]
        [Column("external_campaign_number")]
		public long external_campaign_number
        {
            get { return _external_campaign_number; }
            set
            {
                if (_external_campaign_number != value)
                {
                    PropertyChanged("external_campaign_number");
					_external_campaign_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the allow_cancel_from_branch.
        /// </summary>
        /// <value>
        /// The allow_cancel_from_branch.
        /// </value>
        private string _allow_cancel_from_branch;

        /// <summary>
        /// Gets or sets the allow_cancel_from_branch.
        /// </summary>
        /// <value>
        /// The allow_cancel_from_branch.
        /// </value>
	    [Editable(true)]
        [Column("allow_cancel_from_branch")]
		public string allow_cancel_from_branch
        {
            get { return _allow_cancel_from_branch; }
            set
            {
                if (_allow_cancel_from_branch != value)
                {
                    PropertyChanged("allow_cancel_from_branch");
					_allow_cancel_from_branch = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_type.
        /// </summary>
        /// <value>
        /// The campaign_type.
        /// </value>
        private long _campaign_type;

        /// <summary>
        /// Gets or sets the campaign_type.
        /// </summary>
        /// <value>
        /// The campaign_type.
        /// </value>
	    [Editable(true)]
        [Column("campaign_type")]
		public long campaign_type
        {
            get { return _campaign_type; }
            set
            {
                if (_campaign_type != value)
                {
                    PropertyChanged("campaign_type");
					_campaign_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the clubs.
        /// </summary>
        /// <value>
        /// The clubs.
        /// </value>
        private string _clubs;

        /// <summary>
        /// Gets or sets the clubs.
        /// </summary>
        /// <value>
        /// The clubs.
        /// </value>
	    [Editable(true)]
        [Column("clubs")]
		public string clubs
        {
            get { return _clubs; }
            set
            {
                if (_clubs != value)
                {
                    PropertyChanged("clubs");
					_clubs = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stars.
        /// </summary>
        /// <value>
        /// The stars.
        /// </value>
        private long _stars;

        /// <summary>
        /// Gets or sets the stars.
        /// </summary>
        /// <value>
        /// The stars.
        /// </value>
	    [Editable(true)]
        [Column("stars")]
		public long stars
        {
            get { return _stars; }
            set
            {
                if (_stars != value)
                {
                    PropertyChanged("stars");
					_stars = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_period.
        /// </summary>
        /// <value>
        /// The campaign_period.
        /// </value>
        private long _campaign_period;

        /// <summary>
        /// Gets or sets the campaign_period.
        /// </summary>
        /// <value>
        /// The campaign_period.
        /// </value>
	    [Editable(true)]
        [Column("campaign_period")]
		public long campaign_period
        {
            get { return _campaign_period; }
            set
            {
                if (_campaign_period != value)
                {
                    PropertyChanged("campaign_period");
					_campaign_period = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_serial_number.
        /// </summary>
        /// <value>
        /// The adhoc_serial_number.
        /// </value>
        private long _adhoc_serial_number;

        /// <summary>
        /// Gets or sets the adhoc_serial_number.
        /// </summary>
        /// <value>
        /// The adhoc_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_serial_number")]
		public long adhoc_serial_number
        {
            get { return _adhoc_serial_number; }
            set
            {
                if (_adhoc_serial_number != value)
                {
                    PropertyChanged("adhoc_serial_number");
					_adhoc_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
        private string _adhoc_key;

        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_key")]
		public string adhoc_key
        {
            get { return _adhoc_key; }
            set
            {
                if (_adhoc_key != value)
                {
                    PropertyChanged("adhoc_key");
					_adhoc_key = value;
                    
                }
            }
        }
	}
}
