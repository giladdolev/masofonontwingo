using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;
using System.Web.VisualTree.Elements;

namespace Mesofon.Models
{
	public class d_mini_terminal_return_details : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_return_details()
        {
            this._details = "space(1)";
            this._stock_number = 1;
            this._bonus_row = "";
            this._doc_no_en = 1;


        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        /// <param name="serial_number">The serial_number.</param>
        /// <param name="branch_number">The branch_number.</param>
        /// <param name="doc_no">The doc_no.</param>
        /// <param name="material_number">The material_number.</param>
        /// <param name="invoice_pack_quantity">The invoice_pack_quantity.</param>
        /// <param name="material_quantity">The material_quantity.</param>
        /// <param name="material_price">The material_price.</param>
        /// <param name="details">The details.</param>
        /// <param name="supplier_number">The supplier_number.</param>
        /// <param name="material_discount_percent">The material_discount_percent.</param>
        /// <param name="material_price_after_discount">The material_price_after_discount.</param>
        /// <param name="quantity_within_invoice">The quantity_within_invoice.</param>
        /// <param name="order_number">The order_number.</param>
        /// <param name="order_serial">The order_serial.</param>
        /// <param name="stock_number">The stock_number.</param>
        /// <param name="sell_price">The sell_price.</param>
        /// <param name="bonus_quantity">The bonus_quantity.</param>
        /// <param name="material_bonus_connect">The material_bonus_connect.</param>
        /// <param name="max_quantity_allowed">The max_quantity_allowed.</param>
        /// <param name="last_order_change">The last_order_change.</param>
        /// <param name="import_type">The import_type.</param>
        /// <param name="material_discount_amount">The material_discount_amount.</param>
        /// <param name="bonus_row">The bonus_row.</param>
        /// <param name="color">The color.</param>
        /// <param name="pay_shape">The pay_shape.</param>
        /// <param name="indicator">The indicator.</param>
        /// <param name="bonus_discount">The bonus_discount.</param>
        /// <param name="supplier_discount_percent">The supplier_discount_percent.</param>
        /// <param name="current_catalog_sell_price">The current_catalog_sell_price.</param>
        /// <param name="date_move">The date_move.</param>
        /// <param name="expected_material_quantity">The expected_material_quantity.</param>
        /// <param name="mini_terminal">The mini_terminal.</param>
        /// <param name="barcode">The barcode.</param>
        /// <param name="material_name">The material_name.</param>
        /// <param name="min_valid_months">The min_valid_months.</param>
        /// <param name="order_quantity">The order_quantity.</param>
        /// <param name="b2b_status">The b2b_status.</param>
        /// <param name="doc_state">The doc_state.</param>
        /// <param name="distributor_number">The distributor_number.</param>
        /// <param name="row_no">The row_no.</param>
        /// <param name="doc_no_en">The doc_no_en.</param>
        /// <param name="cng_doc_count">The cng_doc_count.</param>
        /// <param name="declines">The declines.</param>
        /// <param name="state">The state.</param>
        /// <param name="last_update_datetime">The last_update_datetime.</param>
        /// <param name="carton_barcode">The carton_barcode.</param>
        /// <param name="adhoc_serial_number">The adhoc_serial_number.</param>
        /// <param name="adhoc_key">The adhoc_key.</param>
        /// <param name="row_serial_number">The row_serial_number.</param>
        /// <param name="orig_serial_number">The orig_serial_number.</param>
        public d_mini_terminal_return_details(long serial_number, long branch_number, long doc_no, long material_number, decimal invoice_pack_quantity, decimal material_quantity, decimal material_price, string details, long supplier_number, decimal material_discount_percent, decimal material_price_after_discount, decimal quantity_within_invoice, long order_number, long order_serial, long stock_number, decimal sell_price, decimal bonus_quantity, long material_bonus_connect, decimal max_quantity_allowed, decimal last_order_change, string import_type, decimal material_discount_amount, string bonus_row, long color, long pay_shape, long indicator, decimal bonus_discount, decimal supplier_discount_percent, decimal current_catalog_sell_price, DateTime? date_move, decimal expected_material_quantity, long mini_terminal, string barcode, string material_name, long min_valid_months, decimal order_quantity, long b2b_status, string doc_state, long distributor_number, long row_no, long doc_no_en, long cng_doc_count, long declines, string state, DateTime? last_update_datetime, string carton_barcode, long adhoc_serial_number, string adhoc_key, long row_serial_number, long orig_serial_number)
        {
			this._serial_number = serial_number;
			this._branch_number = branch_number;
			this._doc_no = doc_no;
			this._material_number = material_number;
			this._invoice_pack_quantity = invoice_pack_quantity;
			this._material_quantity = material_quantity;
			this._material_price = material_price;
			this._details = details;
			this._supplier_number = supplier_number;
			this._material_discount_percent = material_discount_percent;
			this._material_price_after_discount = material_price_after_discount;
			this._quantity_within_invoice = quantity_within_invoice;
			this._order_number = order_number;
			this._order_serial = order_serial;
			this._stock_number = stock_number;
			this._sell_price = sell_price;
			this._bonus_quantity = bonus_quantity;
			this._material_bonus_connect = material_bonus_connect;
			this._max_quantity_allowed = max_quantity_allowed;
			this._last_order_change = last_order_change;
			this._import_type = import_type;
			this._material_discount_amount = material_discount_amount;
			this._bonus_row = bonus_row;
			this._color = color;
			this._pay_shape = pay_shape;
			this._indicator = indicator;
			this._bonus_discount = bonus_discount;
			this._supplier_discount_percent = supplier_discount_percent;
			this._current_catalog_sell_price = current_catalog_sell_price;
			this._date_move = date_move;
			this._expected_material_quantity = expected_material_quantity;
			this._mini_terminal = mini_terminal;
			this._barcode = barcode;
			this._material_name = material_name;
			this._min_valid_months = min_valid_months;
			this._order_quantity = order_quantity;
			this._b2b_status = b2b_status;
			this._doc_state = doc_state;
			this._distributor_number = distributor_number;
			this._row_no = row_no;
			this._doc_no_en = doc_no_en;
			this._cng_doc_count = cng_doc_count;
			this._declines = declines;
			this._state = state;
			this._last_update_datetime = last_update_datetime;
			this._carton_barcode = carton_barcode;
			this._adhoc_serial_number = adhoc_serial_number;
			this._adhoc_key = adhoc_key;
			this._row_serial_number = row_serial_number;
			this._orig_serial_number = orig_serial_number;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
        private long _doc_no;

        /// <summary>
        /// Gets or sets the doc_no.
        /// </summary>
        /// <value>
        /// The doc_no.
        /// </value>
	    [Editable(true)]
        [Column("doc_no")]
		public long doc_no
        {
            get { return _doc_no; }
            set
            {
                if (_doc_no != value)
                {
                    PropertyChanged("doc_no");
					_doc_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_pack_quantity.
        /// </summary>
        /// <value>
        /// The invoice_pack_quantity.
        /// </value>
        private decimal _invoice_pack_quantity;

        /// <summary>
        /// Gets or sets the invoice_pack_quantity.
        /// </summary>
        /// <value>
        /// The invoice_pack_quantity.
        /// </value>
	    [Editable(true)]
        [Column("invoice_pack_quantity")]
		public decimal invoice_pack_quantity
        {
            get { return _invoice_pack_quantity; }
            set
            {
                if (_invoice_pack_quantity != value)
                {
                    PropertyChanged("invoice_pack_quantity");
					_invoice_pack_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal? _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
        [Properties("material_quantity", "material_quantity", true, 6, typeof(decimal))]
        public decimal? material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
		public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long? _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
        [Properties("supplier_number", "supplier_number", true, 5 ,typeof(long))]
        public long? supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
		public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
        private decimal _material_price_after_discount;

        /// <summary>
        /// Gets or sets the material_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_price_after_discount")]
		public decimal material_price_after_discount
        {
            get { return _material_price_after_discount; }
            set
            {
                if (_material_price_after_discount != value)
                {
                    PropertyChanged("material_price_after_discount");
					_material_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
        private decimal _quantity_within_invoice;

        /// <summary>
        /// Gets or sets the quantity_within_invoice.
        /// </summary>
        /// <value>
        /// The quantity_within_invoice.
        /// </value>
	    [Editable(true)]
        [Column("quantity_within_invoice")]
		public decimal quantity_within_invoice
        {
            get { return _quantity_within_invoice; }
            set
            {
                if (_quantity_within_invoice != value)
                {
                    PropertyChanged("quantity_within_invoice");
					_quantity_within_invoice = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_serial.
        /// </summary>
        /// <value>
        /// The order_serial.
        /// </value>
        private long _order_serial;

        /// <summary>
        /// Gets or sets the order_serial.
        /// </summary>
        /// <value>
        /// The order_serial.
        /// </value>
	    [Editable(true)]
        [Column("order_serial")]
		public long order_serial
        {
            get { return _order_serial; }
            set
            {
                if (_order_serial != value)
                {
                    PropertyChanged("order_serial");
					_order_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
		public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
		public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The max_quantity_allowed.
        /// </value>
        private decimal _max_quantity_allowed;

        /// <summary>
        /// Gets or sets the max_quantity_allowed.
        /// </summary>
        /// <value>
        /// The max_quantity_allowed.
        /// </value>
	    [Editable(true)]
        [Column("max_quantity_allowed")]
		public decimal max_quantity_allowed
        {
            get { return _max_quantity_allowed; }
            set
            {
                if (_max_quantity_allowed != value)
                {
                    PropertyChanged("max_quantity_allowed");
					_max_quantity_allowed = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_order_change.
        /// </summary>
        /// <value>
        /// The last_order_change.
        /// </value>
        private decimal _last_order_change;

        /// <summary>
        /// Gets or sets the last_order_change.
        /// </summary>
        /// <value>
        /// The last_order_change.
        /// </value>
	    [Editable(true)]
        [Column("last_order_change")]
		public decimal last_order_change
        {
            get { return _last_order_change; }
            set
            {
                if (_last_order_change != value)
                {
                    PropertyChanged("last_order_change");
					_last_order_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
        private decimal _material_discount_amount;

        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_amount")]
		public decimal material_discount_amount
        {
            get { return _material_discount_amount; }
            set
            {
                if (_material_discount_amount != value)
                {
                    PropertyChanged("material_discount_amount");
					_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
        private string _bonus_row;

        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("bonus_row")]
		public string bonus_row
        {
            get { return _bonus_row; }
            set
            {
                if (_bonus_row != value)
                {
                    PropertyChanged("bonus_row");
					_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
		public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
        private long _pay_shape;

        /// <summary>
        /// Gets or sets the pay_shape.
        /// </summary>
        /// <value>
        /// The pay_shape.
        /// </value>
	    [Editable(true)]
        [Column("pay_shape")]
		public long pay_shape
        {
            get { return _pay_shape; }
            set
            {
                if (_pay_shape != value)
                {
                    PropertyChanged("pay_shape");
					_pay_shape = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        private long _indicator;

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
	    [Editable(true)]
        [Column("indicator")]
		public long indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    PropertyChanged("indicator");
					_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
        private decimal _bonus_discount;

        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("bonus_discount")]
		public decimal bonus_discount
        {
            get { return _bonus_discount; }
            set
            {
                if (_bonus_discount != value)
                {
                    PropertyChanged("bonus_discount");
					_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
        private decimal _supplier_discount_percent;

        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("supplier_discount_percent")]
		public decimal supplier_discount_percent
        {
            get { return _supplier_discount_percent; }
            set
            {
                if (_supplier_discount_percent != value)
                {
                    PropertyChanged("supplier_discount_percent");
					_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
        private decimal _current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("current_catalog_sell_price")]
		public decimal current_catalog_sell_price
        {
            get { return _current_catalog_sell_price; }
            set
            {
                if (_current_catalog_sell_price != value)
                {
                    PropertyChanged("current_catalog_sell_price");
					_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
        private long _mini_terminal;

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("mini_terminal")]
		public long mini_terminal
        {
            get { return _mini_terminal; }
            set
            {
                if (_mini_terminal != value)
                {
                    PropertyChanged("mini_terminal");
					_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
        [Properties("barcode", "barcode", true, 1, typeof(string))]
        public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
        [Properties("material_name", "material_name", true, 2, typeof(string))]
        public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the min_valid_months.
        /// </summary>
        /// <value>
        /// The min_valid_months.
        /// </value>
        private long _min_valid_months;

        /// <summary>
        /// Gets or sets the min_valid_months.
        /// </summary>
        /// <value>
        /// The min_valid_months.
        /// </value>
	    [Editable(true)]
        [Column("min_valid_months")]
		public long min_valid_months
        {
            get { return _min_valid_months; }
            set
            {
                if (_min_valid_months != value)
                {
                    PropertyChanged("min_valid_months");
					_min_valid_months = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
        private decimal _order_quantity;

        /// <summary>
        /// Gets or sets the order_quantity.
        /// </summary>
        /// <value>
        /// The order_quantity.
        /// </value>
	    [Editable(true)]
        [Column("order_quantity")]
        public decimal order_quantity
        {
            get { return _order_quantity; }
            set
            {
                if (_order_quantity != value)
                {
                    PropertyChanged("order_quantity");
					_order_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
        private long _b2b_status;

        /// <summary>
        /// Gets or sets the b2b_status.
        /// </summary>
        /// <value>
        /// The b2b_status.
        /// </value>
	    [Editable(true)]
        [Column("b2b_status")]
		public long b2b_status
        {
            get { return _b2b_status; }
            set
            {
                if (_b2b_status != value)
                {
                    PropertyChanged("b2b_status");
					_b2b_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_state.
        /// </summary>
        /// <value>
        /// The doc_state.
        /// </value>
        private string _doc_state;

        /// <summary>
        /// Gets or sets the doc_state.
        /// </summary>
        /// <value>
        /// The doc_state.
        /// </value>
	    [Editable(true)]
        [Column("doc_state")]
        [Properties("doc_state", "doc_state", true, 34, typeof(string))]
        public string doc_state
        {
            get { return _doc_state; }
            set
            {
                if (_doc_state != value)
                {
                    PropertyChanged("doc_state");
					_doc_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
        private long _row_no;

        /// <summary>
        /// Gets or sets the row_no.
        /// </summary>
        /// <value>
        /// The row_no.
        /// </value>
	    [Editable(true)]
        [Column("row_no")]
        [Properties("row_no", "row_no", true, 3, typeof(long))]
        [TextExpressionAttribute("getrow()", "", "")]
        public long row_no
        {
            get { return _row_no; }
            set
            {
                if (_row_no != value)
                {
                   // PropertyChanged("row_no");
					_row_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
        private long _doc_no_en;

        /// <summary>
        /// Gets or sets the doc_no_en.
        /// </summary>
        /// <value>
        /// The doc_no_en.
        /// </value>
	    [Editable(true)]
        [Column("doc_no_en")]
		public long doc_no_en
        {
            get { return _doc_no_en; }
            set
            {
                if (_doc_no_en != value)
                {
                    PropertyChanged("doc_no_en");
					_doc_no_en = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the cng_doc_count.
        /// </summary>
        /// <value>
        /// The cng_doc_count.
        /// </value>
        private long _cng_doc_count;

        /// <summary>
        /// Gets or sets the cng_doc_count.
        /// </summary>
        /// <value>
        /// The cng_doc_count.
        /// </value>
	    [Editable(true)]
        [Column("cng_doc_count")]
		public long cng_doc_count
        {
            get { return _cng_doc_count; }
            set
            {
                if (_cng_doc_count != value)
                {
                    PropertyChanged("cng_doc_count");
					_cng_doc_count = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
        private long _declines;

        /// <summary>
        /// Gets or sets the declines.
        /// </summary>
        /// <value>
        /// The declines.
        /// </value>
	    [Editable(true)]
        [Column("declines")]
        [Properties("declines", "declines", true, 4, typeof(long))]
        public long declines
        {
            get { return _declines; }
            set
            {
                if (_declines != value)
                {
                    PropertyChanged("declines");
					_declines = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
        private string _carton_barcode;

        /// <summary>
        /// Gets or sets the carton_barcode.
        /// </summary>
        /// <value>
        /// The carton_barcode.
        /// </value>
	    [Editable(true)]
        [Column("carton_barcode")]
		public string carton_barcode
        {
            get { return _carton_barcode; }
            set
            {
                if (_carton_barcode != value)
                {
                    PropertyChanged("carton_barcode");
					_carton_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_serial_number.
        /// </summary>
        /// <value>
        /// The adhoc_serial_number.
        /// </value>
        private long _adhoc_serial_number;

        /// <summary>
        /// Gets or sets the adhoc_serial_number.
        /// </summary>
        /// <value>
        /// The adhoc_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_serial_number")]
		public long adhoc_serial_number
        {
            get { return _adhoc_serial_number; }
            set
            {
                if (_adhoc_serial_number != value)
                {
                    PropertyChanged("adhoc_serial_number");
					_adhoc_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
        private string _adhoc_key;

        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_key")]
		public string adhoc_key
        {
            get { return _adhoc_key; }
            set
            {
                if (_adhoc_key != value)
                {
                    PropertyChanged("adhoc_key");
					_adhoc_key = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
        private long _row_serial_number;

        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("row_serial_number")]
		public long row_serial_number
        {
            get { return _row_serial_number; }
            set
            {
                if (_row_serial_number != value)
                {
                    PropertyChanged("row_serial_number");
					_row_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the orig_serial_number.
        /// </summary>
        /// <value>
        /// The orig_serial_number.
        /// </value>
        private long _orig_serial_number;

        /// <summary>
        /// Gets or sets the orig_serial_number.
        /// </summary>
        /// <value>
        /// The orig_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("orig_serial_number")]
		public long orig_serial_number
        {
            get { return _orig_serial_number; }
            set
            {
                if (_orig_serial_number != value)
                {
                    PropertyChanged("orig_serial_number");
					_orig_serial_number = value;
                    
                }
            }
        }

        /// <summary>
        /// Gets or sets the _invoice_data.
        /// </summary>
        /// <value>
        /// The _invoice_data.
        /// </value>
        private string _invoice_data;

        /// <summary>
        /// Gets or sets the invoice_data.
        /// </summary>
        /// <value>
        /// The invoice_data.
        /// </value>
	    [Editable(true)]
        [Column("invoice_data")]
        [Properties("invoice_data", "invoice_data", true, 90, typeof(string))]
        [PreventFlagChange()]
        public string invoice_data
        {
            get { return _invoice_data; }
            set
            {
                if (_invoice_data != value)
                {
                    _invoice_data = value;
                }
            }
        }

    }
}
