using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_branches_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_branches_list()
        {
            this._flag = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="branch_name">The branch_name.</param>
		/// <param name="flag">The flag.</param>
		/// <param name="sort">The sort.</param>
        public d_branches_list(long serial_number, string branch_name, long flag, long sort)
        {
			this._serial_number = serial_number;
			this._branch_name = branch_name;
			this._flag = flag;
			this._sort = sort;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
        private string _branch_name;

        /// <summary>
        /// Gets or sets the branch_name.
        /// </summary>
        /// <value>
        /// The branch_name.
        /// </value>
	    [Editable(true)]
        [Column("branch_name")]
		public string branch_name
        {
            get { return _branch_name; }
            set
            {
                if (_branch_name != value)
                {
                    PropertyChanged("branch_name");
					_branch_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        /// <value>
        /// The flag.
        /// </value>
        private long _flag;

        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        /// <value>
        /// The flag.
        /// </value>
	    [Editable(true)]
        [Column("flag")]
		public long flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    PropertyChanged("flag");
					_flag = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        /// <value>
        /// The sort.
        /// </value>
        private long _sort;

        /// <summary>
        /// Gets or sets the sort.
        /// </summary>
        /// <value>
        /// The sort.
        /// </value>
	    [Editable(true)]
        [Column("sort")]
		public long sort
        {
            get { return _sort; }
            set
            {
                if (_sort != value)
                {
                    PropertyChanged("sort");
					_sort = value;
                    
                }
            }
        }
	}
}
