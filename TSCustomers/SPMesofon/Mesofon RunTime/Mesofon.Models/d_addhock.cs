using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_addhock : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_addhock()
        {
            _usage_datetime = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="adhoc_key">The adhoc_key.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="is_active">The is_active.</param>
		/// <param name="usage_datetime">The usage_datetime.</param>
		/// <param name="order_number">The order_number.</param>
		/// <param name="item_number">The item_number.</param>
		/// <param name="trade_number">The trade_number.</param>
		/// <param name="row_serial_number">The row_serial_number.</param>
		/// <param name="adhoc_type">The adhoc_type.</param>
        public d_addhock(long serial_number, long branch_number, long supplier_number, string adhoc_key, DateTime? date_move, long is_active, DateTime? usage_datetime, long order_number, long item_number, long trade_number, long row_serial_number, long adhoc_type)
        {
			this._serial_number = serial_number;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._adhoc_key = adhoc_key;
			this._date_move = date_move;
			this._is_active = is_active;
			this._usage_datetime = usage_datetime;
			this._order_number = order_number;
			this._item_number = item_number;
			this._trade_number = trade_number;
			this._row_serial_number = row_serial_number;
			this._adhoc_type = adhoc_type;
		}

	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
        private string _adhoc_key;

        /// <summary>
        /// Gets or sets the adhoc_key.
        /// </summary>
        /// <value>
        /// The adhoc_key.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_key")]
		public string adhoc_key
        {
            get { return _adhoc_key; }
            set
            {
                if (_adhoc_key != value)
                {
                    PropertyChanged("adhoc_key");
					_adhoc_key = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private DateTime? _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
		public DateTime? date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the is_active.
        /// </summary>
        /// <value>
        /// The is_active.
        /// </value>
        private long _is_active;

        /// <summary>
        /// Gets or sets the is_active.
        /// </summary>
        /// <value>
        /// The is_active.
        /// </value>
	    [Editable(true)]
        [Column("is_active")]
		public long is_active
        {
            get { return _is_active; }
            set
            {
                if (_is_active != value)
                {
                    PropertyChanged("is_active");
					_is_active = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the usage_datetime.
        /// </summary>
        /// <value>
        /// The usage_datetime.
        /// </value>
        private DateTime? _usage_datetime;

        /// <summary>
        /// Gets or sets the usage_datetime.
        /// </summary>
        /// <value>
        /// The usage_datetime.
        /// </value>
	    [Editable(true)]
        [Column("usage_datetime")]
		public DateTime? usage_datetime
        {
            get { return _usage_datetime; }
            set
            {
                if (_usage_datetime != value)
                {
                    PropertyChanged("usage_datetime");
					_usage_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
        private long _order_number;

        /// <summary>
        /// Gets or sets the order_number.
        /// </summary>
        /// <value>
        /// The order_number.
        /// </value>
	    [Editable(true)]
        [Column("order_number")]
		public long order_number
        {
            get { return _order_number; }
            set
            {
                if (_order_number != value)
                {
                    PropertyChanged("order_number");
					_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
        private long _item_number;

        /// <summary>
        /// Gets or sets the item_number.
        /// </summary>
        /// <value>
        /// The item_number.
        /// </value>
	    [Editable(true)]
        [Column("item_number")]
		public long item_number
        {
            get { return _item_number; }
            set
            {
                if (_item_number != value)
                {
                    PropertyChanged("item_number");
					_item_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the trade_number.
        /// </summary>
        /// <value>
        /// The trade_number.
        /// </value>
        private long _trade_number;

        /// <summary>
        /// Gets or sets the trade_number.
        /// </summary>
        /// <value>
        /// The trade_number.
        /// </value>
	    [Editable(true)]
        [Column("trade_number")]
		public long trade_number
        {
            get { return _trade_number; }
            set
            {
                if (_trade_number != value)
                {
                    PropertyChanged("trade_number");
					_trade_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
        private long _row_serial_number;

        /// <summary>
        /// Gets or sets the row_serial_number.
        /// </summary>
        /// <value>
        /// The row_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("row_serial_number")]
		public long row_serial_number
        {
            get { return _row_serial_number; }
            set
            {
                if (_row_serial_number != value)
                {
                    PropertyChanged("row_serial_number");
					_row_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the adhoc_type.
        /// </summary>
        /// <value>
        /// The adhoc_type.
        /// </value>
        private long _adhoc_type;

        /// <summary>
        /// Gets or sets the adhoc_type.
        /// </summary>
        /// <value>
        /// The adhoc_type.
        /// </value>
	    [Editable(true)]
        [Column("adhoc_type")]
		public long adhoc_type
        {
            get { return _adhoc_type; }
            set
            {
                if (_adhoc_type != value)
                {
                    PropertyChanged("adhoc_type");
					_adhoc_type = value;
                    
                }
            }
        }
	}
}
