using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dwh_invoice_details2 : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dwh_invoice_details2()
        {
            this._bonus_row = null;
            this._material_number = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="material_number">The material_number.</param>
		/// <param name="material_name">The material_name.</param>
		/// <param name="material_quantity">The material_quantity.</param>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="serial_number">The serial_number.</param>
		/// <param name="invoice_move_discount">The invoice_move_discount.</param>
		/// <param name="invoice_move_mam">The invoice_move_mam.</param>
		/// <param name="material_discount_percent">The material_discount_percent.</param>
		/// <param name="material_new_price">The material_new_price.</param>
		/// <param name="packing_list_number">The packing_list_number.</param>
		/// <param name="max_quantity">The max_quantity.</param>
		/// <param name="last_change">The last_change.</param>
		/// <param name="import_type">The import_type.</param>
		/// <param name="packing_serial">The packing_serial.</param>
		/// <param name="stock_number">The stock_number.</param>
		/// <param name="barcode">The barcode.</param>
		/// <param name="vat_number">The vat_number.</param>
		/// <param name="vat_percentage">The vat_percentage.</param>
		/// <param name="material_price">The material_price.</param>
		/// <param name="material_discount_amount">The material_discount_amount.</param>
		/// <param name="details">The details.</param>
		/// <param name="color">The color.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="material_property">The material_property.</param>
		/// <param name="update_parent">The update_parent.</param>
		/// <param name="material_bonus_connect">The material_bonus_connect.</param>
		/// <param name="bonus_quantity">The bonus_quantity.</param>
		/// <param name="sell_price">The sell_price.</param>
		/// <param name="material_suppliers_price_before_discount">The material_suppliers_price_before_discount.</param>
		/// <param name="material_suppliers_price_after_discount">The material_suppliers_price_after_discount.</param>
		/// <param name="branch_sell_price">The branch_sell_price.</param>
		/// <param name="material_suppliers_branch_number">The material_suppliers_branch_number.</param>
		/// <param name="material_suppliers_supplier_number">The material_suppliers_supplier_number.</param>
		/// <param name="material_suppliers_material_number">The material_suppliers_material_number.</param>
		/// <param name="material_suppliers_move_datetime">The material_suppliers_move_datetime.</param>
		/// <param name="material_suppliers_global_discount">The material_suppliers_global_discount.</param>
		/// <param name="material_suppliers_material_discount">The material_suppliers_material_discount.</param>
		/// <param name="bonus_discount">The bonus_discount.</param>
		/// <param name="bonus_row">The bonus_row.</param>
		/// <param name="price_change_status">The price_change_status.</param>
		/// <param name="catalog_price_change">The catalog_price_change.</param>
		/// <param name="check_zero">The check_zero.</param>
		/// <param name="flag_color">The flag_color.</param>
		/// <param name="flag_color_1">The flag_color_1.</param>
		/// <param name="quantity_color">The quantity_color.</param>
		/// <param name="store_number">The store_number.</param>
		/// <param name="temp_row_flag">The temp_row_flag.</param>
		/// <param name="material_suppliers_last_supplier">The material_suppliers_last_supplier.</param>
		/// <param name="material_suppliers_last_supplier_price">The material_suppliers_last_supplier_price.</param>
		/// <param name="material_suppliers_invoice_pack_source">The material_suppliers_invoice_pack_source.</param>
		/// <param name="material_suppliers_source_serial_number">The material_suppliers_source_serial_number.</param>
		/// <param name="indicator">The indicator.</param>
		/// <param name="invoice_details_bonus_discount">The invoice_details_bonus_discount.</param>
		/// <param name="campaign_new_price">The campaign_new_price.</param>
		/// <param name="reduction_percent">The reduction_percent.</param>
		/// <param name="gp">The gp.</param>
		/// <param name="total_stock_purchase">The total_stock_purchase.</param>
		/// <param name="supplier_discount_percent">The supplier_discount_percent.</param>
		/// <param name="current_catalog_sell_price">The current_catalog_sell_price.</param>
		/// <param name="expiration_date">The expiration_date.</param>
		/// <param name="b_flag">The b_flag.</param>
		/// <param name="introduction_campaign">The introduction_campaign.</param>
		/// <param name="expected_material_quantity">The expected_material_quantity.</param>
		/// <param name="mini_terminal">The mini_terminal.</param>
		/// <param name="dprt_no">The dprt_no.</param>
		/// <param name="decline_number">The decline_number.</param>
		/// <param name="state">The state.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
		/// <param name="compute_vat_mode">The compute_vat_mode.</param>
		/// <param name="material_new_price1">The material_new_price1.</param>
		/// <param name="compute_1">The compute_1.</param>
		/// <param name="compute_gp">The compute_gp.</param>
		/// <param name="compute_gp_1">The compute_gp_1.</param>
		/// <param name="material_value">The material_value.</param>
		/// <param name="campaign_price">The campaign_price.</param>
		/// <param name="total_with_vat">The total_with_vat.</param>
		/// <param name="total_vat">The total_vat.</param>
		/// <param name="discount_total_cf">The discount_total_cf.</param>
		/// <param name="invoice_total_cf">The invoice_total_cf.</param>
        public dwh_invoice_details2(long material_number, string material_name, decimal material_quantity, long invoice_number, long supplier_number, long serial_number, decimal invoice_move_discount, decimal invoice_move_mam, decimal material_discount_percent, decimal material_new_price, long packing_list_number, decimal max_quantity, decimal last_change, string import_type, long packing_serial, long stock_number, string barcode, long vat_number, decimal vat_percentage, decimal material_price, decimal material_discount_amount, string details, long color, long branch_number, string material_property, string update_parent, long material_bonus_connect, decimal bonus_quantity, decimal sell_price, decimal material_suppliers_price_before_discount, decimal material_suppliers_price_after_discount, decimal branch_sell_price, long material_suppliers_branch_number, long material_suppliers_supplier_number, long material_suppliers_material_number, DateTime? material_suppliers_move_datetime, decimal material_suppliers_global_discount, decimal material_suppliers_material_discount, decimal bonus_discount, string bonus_row, long price_change_status, string catalog_price_change, long check_zero, long flag_color, long flag_color_1, long quantity_color, long store_number, long temp_row_flag, long material_suppliers_last_supplier, long material_suppliers_last_supplier_price, string material_suppliers_invoice_pack_source, long material_suppliers_source_serial_number, long indicator, decimal invoice_details_bonus_discount, decimal campaign_new_price, decimal reduction_percent, decimal gp, decimal total_stock_purchase, decimal supplier_discount_percent, decimal current_catalog_sell_price, DateTime? expiration_date, long b_flag, long introduction_campaign, decimal expected_material_quantity, long mini_terminal, long dprt_no, long decline_number, string state, DateTime? last_update_datetime, string compute_vat_mode, string material_new_price1, string compute_1, string compute_gp, string compute_gp_1, string material_value, string campaign_price, string total_with_vat, string total_vat, string discount_total_cf, string invoice_total_cf)
        {
			this._material_number = material_number;
			this._material_name = material_name;
			this._material_quantity = material_quantity;
			this._invoice_number = invoice_number;
			this._supplier_number = supplier_number;
			this._serial_number = serial_number;
			this._invoice_move_discount = invoice_move_discount;
			this._invoice_move_mam = invoice_move_mam;
			this._material_discount_percent = material_discount_percent;
			this._material_new_price = material_new_price;
			this._packing_list_number = packing_list_number;
			this._max_quantity = max_quantity;
			this._last_change = last_change;
			this._import_type = import_type;
			this._packing_serial = packing_serial;
			this._stock_number = stock_number;
			this._barcode = barcode;
			this._vat_number = vat_number;
			this._vat_percentage = vat_percentage;
			this._material_price = material_price;
			this._material_discount_amount = material_discount_amount;
			this._details = details;
			this._color = color;
			this._branch_number = branch_number;
			this._material_property = material_property;
			this._update_parent = update_parent;
			this._material_bonus_connect = material_bonus_connect;
			this._bonus_quantity = bonus_quantity;
			this._sell_price = sell_price;
			this._material_suppliers_price_before_discount = material_suppliers_price_before_discount;
			this._material_suppliers_price_after_discount = material_suppliers_price_after_discount;
			this._branch_sell_price = branch_sell_price;
			this._material_suppliers_branch_number = material_suppliers_branch_number;
			this._material_suppliers_supplier_number = material_suppliers_supplier_number;
			this._material_suppliers_material_number = material_suppliers_material_number;
			this._material_suppliers_move_datetime = material_suppliers_move_datetime;
			this._material_suppliers_global_discount = material_suppliers_global_discount;
			this._material_suppliers_material_discount = material_suppliers_material_discount;
			this._bonus_discount = bonus_discount;
			this._bonus_row = bonus_row;
			this._price_change_status = price_change_status;
			this._catalog_price_change = catalog_price_change;
			this._check_zero = check_zero;
			this._flag_color = flag_color;
			this._flag_color_1 = flag_color_1;
			this._quantity_color = quantity_color;
			this._store_number = store_number;
			this._temp_row_flag = temp_row_flag;
			this._material_suppliers_last_supplier = material_suppliers_last_supplier;
			this._material_suppliers_last_supplier_price = material_suppliers_last_supplier_price;
			this._material_suppliers_invoice_pack_source = material_suppliers_invoice_pack_source;
			this._material_suppliers_source_serial_number = material_suppliers_source_serial_number;
			this._indicator = indicator;
			this._invoice_details_bonus_discount = invoice_details_bonus_discount;
			this._campaign_new_price = campaign_new_price;
			this._reduction_percent = reduction_percent;
			this._gp = gp;
			this._total_stock_purchase = total_stock_purchase;
			this._supplier_discount_percent = supplier_discount_percent;
			this._current_catalog_sell_price = current_catalog_sell_price;
			this._expiration_date = expiration_date;
			this._b_flag = b_flag;
			this._introduction_campaign = introduction_campaign;
			this._expected_material_quantity = expected_material_quantity;
			this._mini_terminal = mini_terminal;
			this._dprt_no = dprt_no;
			this._decline_number = decline_number;
			this._state = state;
			this._last_update_datetime = last_update_datetime;
			this._compute_vat_mode = compute_vat_mode;
			this._material_new_price1 = material_new_price1;
			this._compute_1 = compute_1;
			this._compute_gp = compute_gp;
			this._compute_gp_1 = compute_gp_1;
			this._material_value = material_value;
			this._campaign_price = campaign_price;
			this._total_with_vat = total_with_vat;
			this._total_vat = total_vat;
			this._discount_total_cf = discount_total_cf;
			this._invoice_total_cf = invoice_total_cf;
		}

	
        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
        private long? _material_number;

        /// <summary>
        /// Gets or sets the material_number.
        /// </summary>
        /// <value>
        /// The material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_number")]
		public long? material_number
        {
            get { return _material_number; }
            set
            {
                if (_material_number != value)
                {
                    PropertyChanged("material_number");
					_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
        private string _material_name;

        /// <summary>
        /// Gets or sets the material_name.
        /// </summary>
        /// <value>
        /// The material_name.
        /// </value>
	    [Editable(true)]
        [Column("material_name")]
		public string material_name
        {
            get { return _material_name; }
            set
            {
                if (_material_name != value)
                {
                    PropertyChanged("material_name");
					_material_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
        private decimal _material_quantity;

        /// <summary>
        /// Gets or sets the material_quantity.
        /// </summary>
        /// <value>
        /// The material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("material_quantity")]
		public decimal material_quantity
        {
            get { return _material_quantity; }
            set
            {
                if (_material_quantity != value)
                {
                    PropertyChanged("material_quantity");
					_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
        private long _serial_number;

        /// <summary>
        /// Gets or sets the serial_number.
        /// </summary>
        /// <value>
        /// The serial_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("serial_number")]
		public long serial_number
        {
            get { return _serial_number; }
            set
            {
                if (_serial_number != value)
                {
                    PropertyChanged("serial_number");
					_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_discount.
        /// </summary>
        /// <value>
        /// The invoice_move_discount.
        /// </value>
        private decimal _invoice_move_discount;

        /// <summary>
        /// Gets or sets the invoice_move_discount.
        /// </summary>
        /// <value>
        /// The invoice_move_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_discount")]
		public decimal invoice_move_discount
        {
            get { return _invoice_move_discount; }
            set
            {
                if (_invoice_move_discount != value)
                {
                    PropertyChanged("invoice_move_discount");
					_invoice_move_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_move_mam.
        /// </summary>
        /// <value>
        /// The invoice_move_mam.
        /// </value>
        private decimal _invoice_move_mam;

        /// <summary>
        /// Gets or sets the invoice_move_mam.
        /// </summary>
        /// <value>
        /// The invoice_move_mam.
        /// </value>
	    [Editable(true)]
        [Column("invoice_move_mam")]
		public decimal invoice_move_mam
        {
            get { return _invoice_move_mam; }
            set
            {
                if (_invoice_move_mam != value)
                {
                    PropertyChanged("invoice_move_mam");
					_invoice_move_mam = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
        private decimal _material_discount_percent;

        /// <summary>
        /// Gets or sets the material_discount_percent.
        /// </summary>
        /// <value>
        /// The material_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_percent")]
		public decimal material_discount_percent
        {
            get { return _material_discount_percent; }
            set
            {
                if (_material_discount_percent != value)
                {
                    PropertyChanged("material_discount_percent");
					_material_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_new_price.
        /// </summary>
        /// <value>
        /// The material_new_price.
        /// </value>
        private decimal _material_new_price;

        /// <summary>
        /// Gets or sets the material_new_price.
        /// </summary>
        /// <value>
        /// The material_new_price.
        /// </value>
	    [Editable(true)]
        [Column("material_new_price")]
		public decimal material_new_price
        {
            get { return _material_new_price; }
            set
            {
                if (_material_new_price != value)
                {
                    PropertyChanged("material_new_price");
					_material_new_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
        private long _packing_list_number;

        /// <summary>
        /// Gets or sets the packing_list_number.
        /// </summary>
        /// <value>
        /// The packing_list_number.
        /// </value>
	    [Editable(true)]
        [Column("packing_list_number")]
		public long packing_list_number
        {
            get { return _packing_list_number; }
            set
            {
                if (_packing_list_number != value)
                {
                    PropertyChanged("packing_list_number");
					_packing_list_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the max_quantity.
        /// </summary>
        /// <value>
        /// The max_quantity.
        /// </value>
        private decimal _max_quantity;

        /// <summary>
        /// Gets or sets the max_quantity.
        /// </summary>
        /// <value>
        /// The max_quantity.
        /// </value>
	    [Editable(true)]
        [Column("max_quantity")]
		public decimal max_quantity
        {
            get { return _max_quantity; }
            set
            {
                if (_max_quantity != value)
                {
                    PropertyChanged("max_quantity");
					_max_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_change.
        /// </summary>
        /// <value>
        /// The last_change.
        /// </value>
        private decimal _last_change;

        /// <summary>
        /// Gets or sets the last_change.
        /// </summary>
        /// <value>
        /// The last_change.
        /// </value>
	    [Editable(true)]
        [Column("last_change")]
		public decimal last_change
        {
            get { return _last_change; }
            set
            {
                if (_last_change != value)
                {
                    PropertyChanged("last_change");
					_last_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
        private string _import_type;

        /// <summary>
        /// Gets or sets the import_type.
        /// </summary>
        /// <value>
        /// The import_type.
        /// </value>
	    [Editable(true)]
        [Column("import_type")]
		public string import_type
        {
            get { return _import_type; }
            set
            {
                if (_import_type != value)
                {
                    PropertyChanged("import_type");
					_import_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
        private long _packing_serial;

        /// <summary>
        /// Gets or sets the packing_serial.
        /// </summary>
        /// <value>
        /// The packing_serial.
        /// </value>
	    [Editable(true)]
        [Column("packing_serial")]
		public long packing_serial
        {
            get { return _packing_serial; }
            set
            {
                if (_packing_serial != value)
                {
                    PropertyChanged("packing_serial");
					_packing_serial = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
        private long _stock_number;

        /// <summary>
        /// Gets or sets the stock_number.
        /// </summary>
        /// <value>
        /// The stock_number.
        /// </value>
	    [Editable(true)]
        [Column("stock_number")]
		public long stock_number
        {
            get { return _stock_number; }
            set
            {
                if (_stock_number != value)
                {
                    PropertyChanged("stock_number");
					_stock_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
        private string _barcode;

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        /// <value>
        /// The barcode.
        /// </value>
	    [Editable(true)]
        [Column("barcode")]
		public string barcode
        {
            get { return _barcode; }
            set
            {
                if (_barcode != value)
                {
                    PropertyChanged("barcode");
					_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
        private long _vat_number;

        /// <summary>
        /// Gets or sets the vat_number.
        /// </summary>
        /// <value>
        /// The vat_number.
        /// </value>
	    [Editable(true)]
        [Column("vat_number")]
		public long vat_number
        {
            get { return _vat_number; }
            set
            {
                if (_vat_number != value)
                {
                    PropertyChanged("vat_number");
					_vat_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the vat_percentage.
        /// </summary>
        /// <value>
        /// The vat_percentage.
        /// </value>
        private decimal _vat_percentage;

        /// <summary>
        /// Gets or sets the vat_percentage.
        /// </summary>
        /// <value>
        /// The vat_percentage.
        /// </value>
	    [Editable(true)]
        [Column("vat_percentage")]
		public decimal vat_percentage
        {
            get { return _vat_percentage; }
            set
            {
                if (_vat_percentage != value)
                {
                    PropertyChanged("vat_percentage");
					_vat_percentage = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
        private decimal _material_price;

        /// <summary>
        /// Gets or sets the material_price.
        /// </summary>
        /// <value>
        /// The material_price.
        /// </value>
	    [Editable(true)]
        [Column("material_price")]
		public decimal material_price
        {
            get { return _material_price; }
            set
            {
                if (_material_price != value)
                {
                    PropertyChanged("material_price");
					_material_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
        private decimal _material_discount_amount;

        /// <summary>
        /// Gets or sets the material_discount_amount.
        /// </summary>
        /// <value>
        /// The material_discount_amount.
        /// </value>
	    [Editable(true)]
        [Column("material_discount_amount")]
		public decimal material_discount_amount
        {
            get { return _material_discount_amount; }
            set
            {
                if (_material_discount_amount != value)
                {
                    PropertyChanged("material_discount_amount");
					_material_discount_amount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        private string _details;

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
	    [Editable(true)]
        [Column("details")]
		public string details
        {
            get { return _details; }
            set
            {
                if (_details != value)
                {
                    PropertyChanged("details");
					_details = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        private long _color;

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
	    [Editable(true)]
        [Column("color")]
		public long color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    PropertyChanged("color");
					_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_property.
        /// </summary>
        /// <value>
        /// The material_property.
        /// </value>
        private string _material_property;

        /// <summary>
        /// Gets or sets the material_property.
        /// </summary>
        /// <value>
        /// The material_property.
        /// </value>
	    [Editable(true)]
        [Column("material_property")]
		public string material_property
        {
            get { return _material_property; }
            set
            {
                if (_material_property != value)
                {
                    PropertyChanged("material_property");
					_material_property = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the update_parent.
        /// </summary>
        /// <value>
        /// The update_parent.
        /// </value>
        private string _update_parent;

        /// <summary>
        /// Gets or sets the update_parent.
        /// </summary>
        /// <value>
        /// The update_parent.
        /// </value>
	    [Editable(true)]
        [Column("update_parent")]
		public string update_parent
        {
            get { return _update_parent; }
            set
            {
                if (_update_parent != value)
                {
                    PropertyChanged("update_parent");
					_update_parent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
        private long _material_bonus_connect;

        /// <summary>
        /// Gets or sets the material_bonus_connect.
        /// </summary>
        /// <value>
        /// The material_bonus_connect.
        /// </value>
	    [Editable(true)]
        [Column("material_bonus_connect")]
		public long material_bonus_connect
        {
            get { return _material_bonus_connect; }
            set
            {
                if (_material_bonus_connect != value)
                {
                    PropertyChanged("material_bonus_connect");
					_material_bonus_connect = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
        private decimal _bonus_quantity;

        /// <summary>
        /// Gets or sets the bonus_quantity.
        /// </summary>
        /// <value>
        /// The bonus_quantity.
        /// </value>
	    [Editable(true)]
        [Column("bonus_quantity")]
		public decimal bonus_quantity
        {
            get { return _bonus_quantity; }
            set
            {
                if (_bonus_quantity != value)
                {
                    PropertyChanged("bonus_quantity");
					_bonus_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
        private decimal _sell_price;

        /// <summary>
        /// Gets or sets the sell_price.
        /// </summary>
        /// <value>
        /// The sell_price.
        /// </value>
	    [Editable(true)]
        [Column("sell_price")]
		public decimal sell_price
        {
            get { return _sell_price; }
            set
            {
                if (_sell_price != value)
                {
                    PropertyChanged("sell_price");
					_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_price_before_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_price_before_discount.
        /// </value>
        private decimal _material_suppliers_price_before_discount;

        /// <summary>
        /// Gets or sets the material_suppliers_price_before_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_price_before_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_price_before_discount")]
		public decimal material_suppliers_price_before_discount
        {
            get { return _material_suppliers_price_before_discount; }
            set
            {
                if (_material_suppliers_price_before_discount != value)
                {
                    PropertyChanged("material_suppliers_price_before_discount");
					_material_suppliers_price_before_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_price_after_discount.
        /// </value>
        private decimal _material_suppliers_price_after_discount;

        /// <summary>
        /// Gets or sets the material_suppliers_price_after_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_price_after_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_price_after_discount")]
		public decimal material_suppliers_price_after_discount
        {
            get { return _material_suppliers_price_after_discount; }
            set
            {
                if (_material_suppliers_price_after_discount != value)
                {
                    PropertyChanged("material_suppliers_price_after_discount");
					_material_suppliers_price_after_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_sell_price.
        /// </summary>
        /// <value>
        /// The branch_sell_price.
        /// </value>
        private decimal _branch_sell_price;

        /// <summary>
        /// Gets or sets the branch_sell_price.
        /// </summary>
        /// <value>
        /// The branch_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("branch_sell_price")]
		public decimal branch_sell_price
        {
            get { return _branch_sell_price; }
            set
            {
                if (_branch_sell_price != value)
                {
                    PropertyChanged("branch_sell_price");
					_branch_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_branch_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_branch_number.
        /// </value>
        private long _material_suppliers_branch_number;

        /// <summary>
        /// Gets or sets the material_suppliers_branch_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_branch_number.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_branch_number")]
		public long material_suppliers_branch_number
        {
            get { return _material_suppliers_branch_number; }
            set
            {
                if (_material_suppliers_branch_number != value)
                {
                    PropertyChanged("material_suppliers_branch_number");
					_material_suppliers_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_supplier_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_supplier_number.
        /// </value>
        private long _material_suppliers_supplier_number;

        /// <summary>
        /// Gets or sets the material_suppliers_supplier_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_supplier_number.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_supplier_number")]
		public long material_suppliers_supplier_number
        {
            get { return _material_suppliers_supplier_number; }
            set
            {
                if (_material_suppliers_supplier_number != value)
                {
                    PropertyChanged("material_suppliers_supplier_number");
					_material_suppliers_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_material_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_material_number.
        /// </value>
        private long _material_suppliers_material_number;

        /// <summary>
        /// Gets or sets the material_suppliers_material_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_material_number.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_material_number")]
		public long material_suppliers_material_number
        {
            get { return _material_suppliers_material_number; }
            set
            {
                if (_material_suppliers_material_number != value)
                {
                    PropertyChanged("material_suppliers_material_number");
					_material_suppliers_material_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_move_datetime.
        /// </summary>
        /// <value>
        /// The material_suppliers_move_datetime.
        /// </value>
        private DateTime? _material_suppliers_move_datetime;

        /// <summary>
        /// Gets or sets the material_suppliers_move_datetime.
        /// </summary>
        /// <value>
        /// The material_suppliers_move_datetime.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_move_datetime")]
		public DateTime? material_suppliers_move_datetime
        {
            get { return _material_suppliers_move_datetime; }
            set
            {
                if (_material_suppliers_move_datetime != value)
                {
                    PropertyChanged("material_suppliers_move_datetime");
					_material_suppliers_move_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_global_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_global_discount.
        /// </value>
        private decimal _material_suppliers_global_discount;

        /// <summary>
        /// Gets or sets the material_suppliers_global_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_global_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_global_discount")]
		public decimal material_suppliers_global_discount
        {
            get { return _material_suppliers_global_discount; }
            set
            {
                if (_material_suppliers_global_discount != value)
                {
                    PropertyChanged("material_suppliers_global_discount");
					_material_suppliers_global_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_material_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_material_discount.
        /// </value>
        private decimal _material_suppliers_material_discount;

        /// <summary>
        /// Gets or sets the material_suppliers_material_discount.
        /// </summary>
        /// <value>
        /// The material_suppliers_material_discount.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_material_discount")]
		public decimal material_suppliers_material_discount
        {
            get { return _material_suppliers_material_discount; }
            set
            {
                if (_material_suppliers_material_discount != value)
                {
                    PropertyChanged("material_suppliers_material_discount");
					_material_suppliers_material_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
        private decimal _bonus_discount;

        /// <summary>
        /// Gets or sets the bonus_discount.
        /// </summary>
        /// <value>
        /// The bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("bonus_discount")]
		public decimal bonus_discount
        {
            get { return _bonus_discount; }
            set
            {
                if (_bonus_discount != value)
                {
                    PropertyChanged("bonus_discount");
					_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
        private string _bonus_row;

        /// <summary>
        /// Gets or sets the bonus_row.
        /// </summary>
        /// <value>
        /// The bonus_row.
        /// </value>
	    [Editable(true)]
        [Column("bonus_row")]
		public string bonus_row
        {
            get { return _bonus_row; }
            set
            {
                if (_bonus_row != value)
                {
                    PropertyChanged("bonus_row");
					_bonus_row = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the price_change_status.
        /// </summary>
        /// <value>
        /// The price_change_status.
        /// </value>
        private long _price_change_status;

        /// <summary>
        /// Gets or sets the price_change_status.
        /// </summary>
        /// <value>
        /// The price_change_status.
        /// </value>
	    [Editable(true)]
        [Column("price_change_status")]
		public long price_change_status
        {
            get { return _price_change_status; }
            set
            {
                if (_price_change_status != value)
                {
                    PropertyChanged("price_change_status");
					_price_change_status = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the catalog_price_change.
        /// </summary>
        /// <value>
        /// The catalog_price_change.
        /// </value>
        private string _catalog_price_change;

        /// <summary>
        /// Gets or sets the catalog_price_change.
        /// </summary>
        /// <value>
        /// The catalog_price_change.
        /// </value>
	    [Editable(true)]
        [Column("catalog_price_change")]
		public string catalog_price_change
        {
            get { return _catalog_price_change; }
            set
            {
                if (_catalog_price_change != value)
                {
                    PropertyChanged("catalog_price_change");
					_catalog_price_change = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the check_zero.
        /// </summary>
        /// <value>
        /// The check_zero.
        /// </value>
        private long _check_zero;

        /// <summary>
        /// Gets or sets the check_zero.
        /// </summary>
        /// <value>
        /// The check_zero.
        /// </value>
	    [Editable(true)]
        [Column("check_zero")]
		public long check_zero
        {
            get { return _check_zero; }
            set
            {
                if (_check_zero != value)
                {
                    PropertyChanged("check_zero");
					_check_zero = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color.
        /// </summary>
        /// <value>
        /// The flag_color.
        /// </value>
        private long _flag_color;

        /// <summary>
        /// Gets or sets the flag_color.
        /// </summary>
        /// <value>
        /// The flag_color.
        /// </value>
	    [Editable(true)]
        [Column("flag_color")]
		public long flag_color
        {
            get { return _flag_color; }
            set
            {
                if (_flag_color != value)
                {
                    PropertyChanged("flag_color");
					_flag_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the flag_color_1.
        /// </summary>
        /// <value>
        /// The flag_color_1.
        /// </value>
        private long _flag_color_1;

        /// <summary>
        /// Gets or sets the flag_color_1.
        /// </summary>
        /// <value>
        /// The flag_color_1.
        /// </value>
	    [Editable(true)]
        [Column("flag_color_1")]
		public long flag_color_1
        {
            get { return _flag_color_1; }
            set
            {
                if (_flag_color_1 != value)
                {
                    PropertyChanged("flag_color_1");
					_flag_color_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the quantity_color.
        /// </summary>
        /// <value>
        /// The quantity_color.
        /// </value>
        private long _quantity_color;

        /// <summary>
        /// Gets or sets the quantity_color.
        /// </summary>
        /// <value>
        /// The quantity_color.
        /// </value>
	    [Editable(true)]
        [Column("quantity_color")]
		public long quantity_color
        {
            get { return _quantity_color; }
            set
            {
                if (_quantity_color != value)
                {
                    PropertyChanged("quantity_color");
					_quantity_color = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
        private long _store_number;

        /// <summary>
        /// Gets or sets the store_number.
        /// </summary>
        /// <value>
        /// The store_number.
        /// </value>
	    [Editable(true)]
        [Column("store_number")]
		public long store_number
        {
            get { return _store_number; }
            set
            {
                if (_store_number != value)
                {
                    PropertyChanged("store_number");
					_store_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the temp_row_flag.
        /// </summary>
        /// <value>
        /// The temp_row_flag.
        /// </value>
        private long _temp_row_flag;

        /// <summary>
        /// Gets or sets the temp_row_flag.
        /// </summary>
        /// <value>
        /// The temp_row_flag.
        /// </value>
	    [Editable(true)]
        [Column("temp_row_flag")]
		public long temp_row_flag
        {
            get { return _temp_row_flag; }
            set
            {
                if (_temp_row_flag != value)
                {
                    PropertyChanged("temp_row_flag");
					_temp_row_flag = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_last_supplier.
        /// </summary>
        /// <value>
        /// The material_suppliers_last_supplier.
        /// </value>
        private long _material_suppliers_last_supplier;

        /// <summary>
        /// Gets or sets the material_suppliers_last_supplier.
        /// </summary>
        /// <value>
        /// The material_suppliers_last_supplier.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_last_supplier")]
		public long material_suppliers_last_supplier
        {
            get { return _material_suppliers_last_supplier; }
            set
            {
                if (_material_suppliers_last_supplier != value)
                {
                    PropertyChanged("material_suppliers_last_supplier");
					_material_suppliers_last_supplier = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_last_supplier_price.
        /// </summary>
        /// <value>
        /// The material_suppliers_last_supplier_price.
        /// </value>
        private long _material_suppliers_last_supplier_price;

        /// <summary>
        /// Gets or sets the material_suppliers_last_supplier_price.
        /// </summary>
        /// <value>
        /// The material_suppliers_last_supplier_price.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_last_supplier_price")]
		public long material_suppliers_last_supplier_price
        {
            get { return _material_suppliers_last_supplier_price; }
            set
            {
                if (_material_suppliers_last_supplier_price != value)
                {
                    PropertyChanged("material_suppliers_last_supplier_price");
					_material_suppliers_last_supplier_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_invoice_pack_source.
        /// </summary>
        /// <value>
        /// The material_suppliers_invoice_pack_source.
        /// </value>
        private string _material_suppliers_invoice_pack_source;

        /// <summary>
        /// Gets or sets the material_suppliers_invoice_pack_source.
        /// </summary>
        /// <value>
        /// The material_suppliers_invoice_pack_source.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_invoice_pack_source")]
		public string material_suppliers_invoice_pack_source
        {
            get { return _material_suppliers_invoice_pack_source; }
            set
            {
                if (_material_suppliers_invoice_pack_source != value)
                {
                    PropertyChanged("material_suppliers_invoice_pack_source");
					_material_suppliers_invoice_pack_source = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_suppliers_source_serial_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_source_serial_number.
        /// </value>
        private long _material_suppliers_source_serial_number;

        /// <summary>
        /// Gets or sets the material_suppliers_source_serial_number.
        /// </summary>
        /// <value>
        /// The material_suppliers_source_serial_number.
        /// </value>
	    [Editable(true)]
        [Column("material_suppliers_source_serial_number")]
		public long material_suppliers_source_serial_number
        {
            get { return _material_suppliers_source_serial_number; }
            set
            {
                if (_material_suppliers_source_serial_number != value)
                {
                    PropertyChanged("material_suppliers_source_serial_number");
					_material_suppliers_source_serial_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        private long _indicator;

        /// <summary>
        /// Gets or sets the indicator.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
	    [Editable(true)]
        [Column("indicator")]
		public long indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    PropertyChanged("indicator");
					_indicator = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
        private decimal _invoice_details_bonus_discount;

        /// <summary>
        /// Gets or sets the invoice_details_bonus_discount.
        /// </summary>
        /// <value>
        /// The invoice_details_bonus_discount.
        /// </value>
	    [Editable(true)]
        [Column("invoice_details_bonus_discount")]
		public decimal invoice_details_bonus_discount
        {
            get { return _invoice_details_bonus_discount; }
            set
            {
                if (_invoice_details_bonus_discount != value)
                {
                    PropertyChanged("invoice_details_bonus_discount");
					_invoice_details_bonus_discount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_new_price.
        /// </summary>
        /// <value>
        /// The campaign_new_price.
        /// </value>
        private decimal _campaign_new_price;

        /// <summary>
        /// Gets or sets the campaign_new_price.
        /// </summary>
        /// <value>
        /// The campaign_new_price.
        /// </value>
	    [Editable(true)]
        [Column("campaign_new_price")]
		public decimal campaign_new_price
        {
            get { return _campaign_new_price; }
            set
            {
                if (_campaign_new_price != value)
                {
                    PropertyChanged("campaign_new_price");
					_campaign_new_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the reduction_percent.
        /// </summary>
        /// <value>
        /// The reduction_percent.
        /// </value>
        private decimal _reduction_percent;

        /// <summary>
        /// Gets or sets the reduction_percent.
        /// </summary>
        /// <value>
        /// The reduction_percent.
        /// </value>
	    [Editable(true)]
        [Column("reduction_percent")]
		public decimal reduction_percent
        {
            get { return _reduction_percent; }
            set
            {
                if (_reduction_percent != value)
                {
                    PropertyChanged("reduction_percent");
					_reduction_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the gp.
        /// </summary>
        /// <value>
        /// The gp.
        /// </value>
        private decimal _gp;

        /// <summary>
        /// Gets or sets the gp.
        /// </summary>
        /// <value>
        /// The gp.
        /// </value>
	    [Editable(true)]
        [Column("gp")]
		public decimal gp
        {
            get { return _gp; }
            set
            {
                if (_gp != value)
                {
                    PropertyChanged("gp");
					_gp = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_stock_purchase.
        /// </summary>
        /// <value>
        /// The total_stock_purchase.
        /// </value>
        private decimal _total_stock_purchase;

        /// <summary>
        /// Gets or sets the total_stock_purchase.
        /// </summary>
        /// <value>
        /// The total_stock_purchase.
        /// </value>
	    [Editable(true)]
        [Column("total_stock_purchase")]
		public decimal total_stock_purchase
        {
            get { return _total_stock_purchase; }
            set
            {
                if (_total_stock_purchase != value)
                {
                    PropertyChanged("total_stock_purchase");
					_total_stock_purchase = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
        private decimal _supplier_discount_percent;

        /// <summary>
        /// Gets or sets the supplier_discount_percent.
        /// </summary>
        /// <value>
        /// The supplier_discount_percent.
        /// </value>
	    [Editable(true)]
        [Column("supplier_discount_percent")]
		public decimal supplier_discount_percent
        {
            get { return _supplier_discount_percent; }
            set
            {
                if (_supplier_discount_percent != value)
                {
                    PropertyChanged("supplier_discount_percent");
					_supplier_discount_percent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
        private decimal _current_catalog_sell_price;

        /// <summary>
        /// Gets or sets the current_catalog_sell_price.
        /// </summary>
        /// <value>
        /// The current_catalog_sell_price.
        /// </value>
	    [Editable(true)]
        [Column("current_catalog_sell_price")]
		public decimal current_catalog_sell_price
        {
            get { return _current_catalog_sell_price; }
            set
            {
                if (_current_catalog_sell_price != value)
                {
                    PropertyChanged("current_catalog_sell_price");
					_current_catalog_sell_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
        private DateTime? _expiration_date;

        /// <summary>
        /// Gets or sets the expiration_date.
        /// </summary>
        /// <value>
        /// The expiration_date.
        /// </value>
	    [Editable(true)]
        [Column("expiration_date")]
		public DateTime? expiration_date
        {
            get { return _expiration_date; }
            set
            {
                if (_expiration_date != value)
                {
                    PropertyChanged("expiration_date");
					_expiration_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b_flag.
        /// </summary>
        /// <value>
        /// The b_flag.
        /// </value>
        private long _b_flag;

        /// <summary>
        /// Gets or sets the b_flag.
        /// </summary>
        /// <value>
        /// The b_flag.
        /// </value>
	    [Editable(true)]
        [Column("b_flag")]
		public long b_flag
        {
            get { return _b_flag; }
            set
            {
                if (_b_flag != value)
                {
                    PropertyChanged("b_flag");
					_b_flag = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the introduction_campaign.
        /// </summary>
        /// <value>
        /// The introduction_campaign.
        /// </value>
        private long _introduction_campaign;

        /// <summary>
        /// Gets or sets the introduction_campaign.
        /// </summary>
        /// <value>
        /// The introduction_campaign.
        /// </value>
	    [Editable(true)]
        [Column("introduction_campaign")]
		public long introduction_campaign
        {
            get { return _introduction_campaign; }
            set
            {
                if (_introduction_campaign != value)
                {
                    PropertyChanged("introduction_campaign");
					_introduction_campaign = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
        private decimal _expected_material_quantity;

        /// <summary>
        /// Gets or sets the expected_material_quantity.
        /// </summary>
        /// <value>
        /// The expected_material_quantity.
        /// </value>
	    [Editable(true)]
        [Column("expected_material_quantity")]
		public decimal expected_material_quantity
        {
            get { return _expected_material_quantity; }
            set
            {
                if (_expected_material_quantity != value)
                {
                    PropertyChanged("expected_material_quantity");
					_expected_material_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
        private long _mini_terminal;

        /// <summary>
        /// Gets or sets the mini_terminal.
        /// </summary>
        /// <value>
        /// The mini_terminal.
        /// </value>
	    [Editable(true)]
        [Column("mini_terminal")]
		public long mini_terminal
        {
            get { return _mini_terminal; }
            set
            {
                if (_mini_terminal != value)
                {
                    PropertyChanged("mini_terminal");
					_mini_terminal = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the dprt_no.
        /// </summary>
        /// <value>
        /// The dprt_no.
        /// </value>
        private long _dprt_no;

        /// <summary>
        /// Gets or sets the dprt_no.
        /// </summary>
        /// <value>
        /// The dprt_no.
        /// </value>
	    [Editable(true)]
        [Column("dprt_no")]
		public long dprt_no
        {
            get { return _dprt_no; }
            set
            {
                if (_dprt_no != value)
                {
                    PropertyChanged("dprt_no");
					_dprt_no = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
        private long _decline_number;

        /// <summary>
        /// Gets or sets the decline_number.
        /// </summary>
        /// <value>
        /// The decline_number.
        /// </value>
	    [Editable(true)]
        [Column("decline_number")]
		public long decline_number
        {
            get { return _decline_number; }
            set
            {
                if (_decline_number != value)
                {
                    PropertyChanged("decline_number");
					_decline_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_vat_mode.
        /// </summary>
        /// <value>
        /// The compute_vat_mode.
        /// </value>
        private string _compute_vat_mode;

        /// <summary>
        /// Gets or sets the compute_vat_mode.
        /// </summary>
        /// <value>
        /// The compute_vat_mode.
        /// </value>
	    [Editable(true)]
        [Column("compute_vat_mode")]
		public string compute_vat_mode
        {
            get { return _compute_vat_mode; }
            set
            {
                if (_compute_vat_mode != value)
                {
                    PropertyChanged("compute_vat_mode");
					_compute_vat_mode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_new_price1.
        /// </summary>
        /// <value>
        /// The material_new_price1.
        /// </value>
        private string _material_new_price1;

        /// <summary>
        /// Gets or sets the material_new_price1.
        /// </summary>
        /// <value>
        /// The material_new_price1.
        /// </value>
	    [Editable(true)]
        [Column("material_new_price1")]
		public string material_new_price1
        {
            get { return _material_new_price1; }
            set
            {
                if (_material_new_price1 != value)
                {
                    PropertyChanged("material_new_price1");
					_material_new_price1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
        private string _compute_1;

        /// <summary>
        /// Gets or sets the compute_1.
        /// </summary>
        /// <value>
        /// The compute_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_1")]
		public string compute_1
        {
            get { return _compute_1; }
            set
            {
                if (_compute_1 != value)
                {
                    PropertyChanged("compute_1");
					_compute_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_gp.
        /// </summary>
        /// <value>
        /// The compute_gp.
        /// </value>
        private string _compute_gp;

        /// <summary>
        /// Gets or sets the compute_gp.
        /// </summary>
        /// <value>
        /// The compute_gp.
        /// </value>
	    [Editable(true)]
        [Column("compute_gp")]
		public string compute_gp
        {
            get { return _compute_gp; }
            set
            {
                if (_compute_gp != value)
                {
                    PropertyChanged("compute_gp");
					_compute_gp = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the compute_gp_1.
        /// </summary>
        /// <value>
        /// The compute_gp_1.
        /// </value>
        private string _compute_gp_1;

        /// <summary>
        /// Gets or sets the compute_gp_1.
        /// </summary>
        /// <value>
        /// The compute_gp_1.
        /// </value>
	    [Editable(true)]
        [Column("compute_gp_1")]
		public string compute_gp_1
        {
            get { return _compute_gp_1; }
            set
            {
                if (_compute_gp_1 != value)
                {
                    PropertyChanged("compute_gp_1");
					_compute_gp_1 = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the material_value.
        /// </summary>
        /// <value>
        /// The material_value.
        /// </value>
        private string _material_value;

        /// <summary>
        /// Gets or sets the material_value.
        /// </summary>
        /// <value>
        /// The material_value.
        /// </value>
	    [Editable(true)]
        [Column("material_value")]
		public string material_value
        {
            get { return _material_value; }
            set
            {
                if (_material_value != value)
                {
                    PropertyChanged("material_value");
					_material_value = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the campaign_price.
        /// </summary>
        /// <value>
        /// The campaign_price.
        /// </value>
        private string _campaign_price;

        /// <summary>
        /// Gets or sets the campaign_price.
        /// </summary>
        /// <value>
        /// The campaign_price.
        /// </value>
	    [Editable(true)]
        [Column("campaign_price")]
		public string campaign_price
        {
            get { return _campaign_price; }
            set
            {
                if (_campaign_price != value)
                {
                    PropertyChanged("campaign_price");
					_campaign_price = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_with_vat.
        /// </summary>
        /// <value>
        /// The total_with_vat.
        /// </value>
        private string _total_with_vat;

        /// <summary>
        /// Gets or sets the total_with_vat.
        /// </summary>
        /// <value>
        /// The total_with_vat.
        /// </value>
	    [Editable(true)]
        [Column("total_with_vat")]
		public string total_with_vat
        {
            get { return _total_with_vat; }
            set
            {
                if (_total_with_vat != value)
                {
                    PropertyChanged("total_with_vat");
					_total_with_vat = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the total_vat.
        /// </summary>
        /// <value>
        /// The total_vat.
        /// </value>
        private string _total_vat;

        /// <summary>
        /// Gets or sets the total_vat.
        /// </summary>
        /// <value>
        /// The total_vat.
        /// </value>
	    [Editable(true)]
        [Column("total_vat")]
		public string total_vat
        {
            get { return _total_vat; }
            set
            {
                if (_total_vat != value)
                {
                    PropertyChanged("total_vat");
					_total_vat = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discount_total_cf.
        /// </summary>
        /// <value>
        /// The discount_total_cf.
        /// </value>
        private string _discount_total_cf;

        /// <summary>
        /// Gets or sets the discount_total_cf.
        /// </summary>
        /// <value>
        /// The discount_total_cf.
        /// </value>
	    [Editable(true)]
        [Column("discount_total_cf")]
		public string discount_total_cf
        {
            get { return _discount_total_cf; }
            set
            {
                if (_discount_total_cf != value)
                {
                    PropertyChanged("discount_total_cf");
					_discount_total_cf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_total_cf.
        /// </summary>
        /// <value>
        /// The invoice_total_cf.
        /// </value>
        private string _invoice_total_cf;

        /// <summary>
        /// Gets or sets the invoice_total_cf.
        /// </summary>
        /// <value>
        /// The invoice_total_cf.
        /// </value>
	    [Editable(true)]
        [Column("invoice_total_cf")]
		public string invoice_total_cf
        {
            get { return _invoice_total_cf; }
            set
            {
                if (_invoice_total_cf != value)
                {
                    PropertyChanged("invoice_total_cf");
					_invoice_total_cf = value;
                    
                }
            }
        }
	}
}
