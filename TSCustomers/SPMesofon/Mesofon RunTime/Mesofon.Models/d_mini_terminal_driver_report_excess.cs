using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_driver_report_excess : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_driver_report_excess()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="materials_item_barcode">The materials_item_barcode.</param>
		/// <param name="b2b_decline_types_decline_description">The b2b_decline_types_decline_description.</param>
		/// <param name="b2b_declines_move_original_quantity">The b2b_declines_move_original_quantity.</param>
		/// <param name="b2b_declines_move_actual_quantity">The b2b_declines_move_actual_quantity.</param>
		/// <param name="b2b_declines_move_reject_quantity">The b2b_declines_move_reject_quantity.</param>
		/// <param name="b2b_decline_types_display_name">The b2b_decline_types_display_name.</param>
		/// <param name="description">The description.</param>
		/// <param name="b2b_declines_move_parent_doc_number">The b2b_declines_move_parent_doc_number.</param>
        public d_mini_terminal_driver_report_excess(string materials_item_barcode, string b2b_decline_types_decline_description, decimal b2b_declines_move_original_quantity, decimal b2b_declines_move_actual_quantity, decimal b2b_declines_move_reject_quantity, string b2b_decline_types_display_name, string description, long b2b_declines_move_parent_doc_number)
        {
			this._materials_item_barcode = materials_item_barcode;
			this._b2b_decline_types_decline_description = b2b_decline_types_decline_description;
			this._b2b_declines_move_original_quantity = b2b_declines_move_original_quantity;
			this._b2b_declines_move_actual_quantity = b2b_declines_move_actual_quantity;
			this._b2b_declines_move_reject_quantity = b2b_declines_move_reject_quantity;
			this._b2b_decline_types_display_name = b2b_decline_types_display_name;
			this._description = description;
			this._b2b_declines_move_parent_doc_number = b2b_declines_move_parent_doc_number;
		}

	
        /// <summary>
        /// Gets or sets the materials_item_barcode.
        /// </summary>
        /// <value>
        /// The materials_item_barcode.
        /// </value>
        private string _materials_item_barcode;

        /// <summary>
        /// Gets or sets the materials_item_barcode.
        /// </summary>
        /// <value>
        /// The materials_item_barcode.
        /// </value>
	    [Editable(true)]
        [Column("materials_item_barcode")]
        [Properties("materials_item_barcode", "materials_item_barcode",true,0,typeof(string))]

        public string materials_item_barcode
        {
            get { return _materials_item_barcode; }
            set
            {
                if (_materials_item_barcode != value)
                {
                    PropertyChanged("materials_item_barcode");
					_materials_item_barcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_decline_types_decline_description.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_decline_description.
        /// </value>
        private string _b2b_decline_types_decline_description;

        /// <summary>
        /// Gets or sets the b2b_decline_types_decline_description.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_decline_description.
        /// </value>
	    [Editable(true)]
        [Column("b2b_decline_types_decline_description")]
        [Properties("b2b_decline_types_decline_description", "b2b_decline_types_decline_description", true, 1, typeof(string))]
        public string b2b_decline_types_decline_description
        {
            get { return _b2b_decline_types_decline_description; }
            set
            {
                if (_b2b_decline_types_decline_description != value)
                {
                    PropertyChanged("b2b_decline_types_decline_description");
					_b2b_decline_types_decline_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_declines_move_original_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_original_quantity.
        /// </value>
        private decimal _b2b_declines_move_original_quantity;

        /// <summary>
        /// Gets or sets the b2b_declines_move_original_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_original_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_declines_move_original_quantity")]
        [Properties("b2b_declines_move_original_quantity", "b2b_declines_move_original_quantity", true, 2, typeof(decimal))]
        public decimal b2b_declines_move_original_quantity
        {
            get { return _b2b_declines_move_original_quantity; }
            set
            {
                if (_b2b_declines_move_original_quantity != value)
                {
                    PropertyChanged("b2b_declines_move_original_quantity");
					_b2b_declines_move_original_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_declines_move_actual_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_actual_quantity.
        /// </value>
        private decimal _b2b_declines_move_actual_quantity;

        /// <summary>
        /// Gets or sets the b2b_declines_move_actual_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_actual_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_declines_move_actual_quantity")]
        [Properties("b2b_declines_move_actual_quantity", "b2b_declines_move_actual_quantity", true, 3, typeof(decimal))]
        public decimal b2b_declines_move_actual_quantity
        {
            get { return _b2b_declines_move_actual_quantity; }
            set
            {
                if (_b2b_declines_move_actual_quantity != value)
                {
                    PropertyChanged("b2b_declines_move_actual_quantity");
					_b2b_declines_move_actual_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_declines_move_reject_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_reject_quantity.
        /// </value>
        private decimal _b2b_declines_move_reject_quantity;

        /// <summary>
        /// Gets or sets the b2b_declines_move_reject_quantity.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_reject_quantity.
        /// </value>
	    [Editable(true)]
        [Column("b2b_declines_move_reject_quantity")]
        [Properties("b2b_declines_move_reject_quantity", "b2b_declines_move_reject_quantity", true, 4, typeof(decimal))]
        public decimal b2b_declines_move_reject_quantity
        {
            get { return _b2b_declines_move_reject_quantity; }
            set
            {
                if (_b2b_declines_move_reject_quantity != value)
                {
                    PropertyChanged("b2b_declines_move_reject_quantity");
					_b2b_declines_move_reject_quantity = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_decline_types_display_name.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_display_name.
        /// </value>
        private string _b2b_decline_types_display_name;

        /// <summary>
        /// Gets or sets the b2b_decline_types_display_name.
        /// </summary>
        /// <value>
        /// The b2b_decline_types_display_name.
        /// </value>
	    [Editable(true)]
        [Column("b2b_decline_types_display_name")]
        [Properties("b2b_decline_types_display_name", "b2b_decline_types_display_name", true, 5, typeof(string))]
        public string b2b_decline_types_display_name
        {
            get { return _b2b_decline_types_display_name; }
            set
            {
                if (_b2b_decline_types_display_name != value)
                {
                    PropertyChanged("b2b_decline_types_display_name");
					_b2b_decline_types_display_name = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        private string _description;

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
	    [Editable(true)]
        [Column("description")]
        [Properties("description", "description", true, 6, typeof(string))]
        public string description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    PropertyChanged("description");
					_description = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the b2b_declines_move_parent_doc_number.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_parent_doc_number.
        /// </value>
        private long _b2b_declines_move_parent_doc_number;

        /// <summary>
        /// Gets or sets the b2b_declines_move_parent_doc_number.
        /// </summary>
        /// <value>
        /// The b2b_declines_move_parent_doc_number.
        /// </value>
	    [Editable(true)]
        [Column("b2b_declines_move_parent_doc_number")]
        [Properties("b2b_declines_move_parent_doc_number", "b2b_declines_move_parent_doc_number", true, 7, typeof(long))]
        public long b2b_declines_move_parent_doc_number
        {
            get { return _b2b_declines_move_parent_doc_number; }
            set
            {
                if (_b2b_declines_move_parent_doc_number != value)
                {
                    PropertyChanged("b2b_declines_move_parent_doc_number");
					_b2b_declines_move_parent_doc_number = value;
                    
                }
            }
        }
	}
}
