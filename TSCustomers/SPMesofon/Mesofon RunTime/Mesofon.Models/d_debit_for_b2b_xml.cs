using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_debit_for_b2b_xml : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_debit_for_b2b_xml()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="receiver">The receiver.</param>
		/// <param name="doctype">The doctype.</param>
		/// <param name="aprf">The aprf.</param>
		/// <param name="snrf">The snrf.</param>
		/// <param name="ackn">The ackn.</param>
		/// <param name="testind">The testind.</param>
		/// <param name="messdate">The messdate.</param>
		/// <param name="messtime">The messtime.</param>
		/// <param name="invoicetype">The invoicetype.</param>
		/// <param name="debitinvoiceno">The debitinvoiceno.</param>
		/// <param name="invoicefunc">The invoicefunc.</param>
		/// <param name="datetime">The datetime.</param>
		/// <param name="supplierinvoiceno">The supplierinvoiceno.</param>
		/// <param name="supplierinvoicedate">The supplierinvoicedate.</param>
		/// <param name="referenceno">The referenceno.</param>
		/// <param name="referencedate">The referencedate.</param>
		/// <param name="pricelistno">The pricelistno.</param>
		/// <param name="pricelistdate">The pricelistdate.</param>
		/// <param name="supplierno">The supplierno.</param>
		/// <param name="suppliername">The suppliername.</param>
		/// <param name="storeno">The storeno.</param>
		/// <param name="storename">The storename.</param>
		/// <param name="discountpercent">The discountpercent.</param>
		/// <param name="discountamount">The discountamount.</param>
		/// <param name="linessum">The linessum.</param>
		/// <param name="docsum">The docsum.</param>
		/// <param name="invsum">The invsum.</param>
		/// <param name="lineno">The lineno.</param>
		/// <param name="itembarcode">The itembarcode.</param>
		/// <param name="proddesc">The proddesc.</param>
		/// <param name="packno">The packno.</param>
		/// <param name="unitsqty">The unitsqty.</param>
		/// <param name="unitsqtymea">The unitsqtymea.</param>
		/// <param name="reasoncode">The reasoncode.</param>
		/// <param name="itempricenetosph">The itempricenetosph.</param>
		/// <param name="itempricenetosupplier">The itempricenetosupplier.</param>
		/// <param name="pricedifference">The pricedifference.</param>
		/// <param name="linesum">The linesum.</param>
        public d_debit_for_b2b_xml(string sender, string receiver, string doctype, string aprf, string snrf, string ackn, string testind, double messdate, string messtime, string invoicetype, string debitinvoiceno, string invoicefunc, long datetime, string supplierinvoiceno, double supplierinvoicedate, string referenceno, double referencedate, string pricelistno, double pricelistdate, string supplierno, string suppliername, string storeno, string storename, decimal discountpercent, decimal discountamount, decimal linessum, decimal docsum, decimal invsum, double lineno, string itembarcode, string proddesc, double packno, double unitsqty, string unitsqtymea, string reasoncode, decimal itempricenetosph, decimal itempricenetosupplier, decimal pricedifference, decimal linesum)
        {
			this._sender = sender;
			this._receiver = receiver;
			this._doctype = doctype;
			this._aprf = aprf;
			this._snrf = snrf;
			this._ackn = ackn;
			this._testind = testind;
			this._messdate = messdate;
			this._messtime = messtime;
			this._invoicetype = invoicetype;
			this._debitinvoiceno = debitinvoiceno;
			this._invoicefunc = invoicefunc;
			this._datetime = datetime;
			this._supplierinvoiceno = supplierinvoiceno;
			this._supplierinvoicedate = supplierinvoicedate;
			this._referenceno = referenceno;
			this._referencedate = referencedate;
			this._pricelistno = pricelistno;
			this._pricelistdate = pricelistdate;
			this._supplierno = supplierno;
			this._suppliername = suppliername;
			this._storeno = storeno;
			this._storename = storename;
			this._discountpercent = discountpercent;
			this._discountamount = discountamount;
			this._linessum = linessum;
			this._docsum = docsum;
			this._invsum = invsum;
			this._lineno = lineno;
			this._itembarcode = itembarcode;
			this._proddesc = proddesc;
			this._packno = packno;
			this._unitsqty = unitsqty;
			this._unitsqtymea = unitsqtymea;
			this._reasoncode = reasoncode;
			this._itempricenetosph = itempricenetosph;
			this._itempricenetosupplier = itempricenetosupplier;
			this._pricedifference = pricedifference;
			this._linesum = linesum;
		}

	
        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
        private string _sender;

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
	    [Editable(true)]
        [Column("sender")]
		public string sender
        {
            get { return _sender; }
            set
            {
                if (_sender != value)
                {
                    PropertyChanged("sender");
					_sender = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
        private string _receiver;

        /// <summary>
        /// Gets or sets the receiver.
        /// </summary>
        /// <value>
        /// The receiver.
        /// </value>
	    [Editable(true)]
        [Column("receiver")]
		public string receiver
        {
            get { return _receiver; }
            set
            {
                if (_receiver != value)
                {
                    PropertyChanged("receiver");
					_receiver = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
        private string _doctype;

        /// <summary>
        /// Gets or sets the doctype.
        /// </summary>
        /// <value>
        /// The doctype.
        /// </value>
	    [Editable(true)]
        [Column("doctype")]
		public string doctype
        {
            get { return _doctype; }
            set
            {
                if (_doctype != value)
                {
                    PropertyChanged("doctype");
					_doctype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
        private string _aprf;

        /// <summary>
        /// Gets or sets the aprf.
        /// </summary>
        /// <value>
        /// The aprf.
        /// </value>
	    [Editable(true)]
        [Column("aprf")]
		public string aprf
        {
            get { return _aprf; }
            set
            {
                if (_aprf != value)
                {
                    PropertyChanged("aprf");
					_aprf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
        private string _snrf;

        /// <summary>
        /// Gets or sets the snrf.
        /// </summary>
        /// <value>
        /// The snrf.
        /// </value>
	    [Editable(true)]
        [Column("snrf")]
		public string snrf
        {
            get { return _snrf; }
            set
            {
                if (_snrf != value)
                {
                    PropertyChanged("snrf");
					_snrf = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
        private string _ackn;

        /// <summary>
        /// Gets or sets the ackn.
        /// </summary>
        /// <value>
        /// The ackn.
        /// </value>
	    [Editable(true)]
        [Column("ackn")]
		public string ackn
        {
            get { return _ackn; }
            set
            {
                if (_ackn != value)
                {
                    PropertyChanged("ackn");
					_ackn = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
        private string _testind;

        /// <summary>
        /// Gets or sets the testind.
        /// </summary>
        /// <value>
        /// The testind.
        /// </value>
	    [Editable(true)]
        [Column("testind")]
		public string testind
        {
            get { return _testind; }
            set
            {
                if (_testind != value)
                {
                    PropertyChanged("testind");
					_testind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
        private double _messdate;

        /// <summary>
        /// Gets or sets the messdate.
        /// </summary>
        /// <value>
        /// The messdate.
        /// </value>
	    [Editable(true)]
        [Column("messdate")]
		public double messdate
        {
            get { return _messdate; }
            set
            {
                if (_messdate != value)
                {
                    PropertyChanged("messdate");
					_messdate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
        private string _messtime;

        /// <summary>
        /// Gets or sets the messtime.
        /// </summary>
        /// <value>
        /// The messtime.
        /// </value>
	    [Editable(true)]
        [Column("messtime")]
		public string messtime
        {
            get { return _messtime; }
            set
            {
                if (_messtime != value)
                {
                    PropertyChanged("messtime");
					_messtime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoicetype.
        /// </summary>
        /// <value>
        /// The invoicetype.
        /// </value>
        private string _invoicetype;

        /// <summary>
        /// Gets or sets the invoicetype.
        /// </summary>
        /// <value>
        /// The invoicetype.
        /// </value>
	    [Editable(true)]
        [Column("invoicetype")]
		public string invoicetype
        {
            get { return _invoicetype; }
            set
            {
                if (_invoicetype != value)
                {
                    PropertyChanged("invoicetype");
					_invoicetype = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the debitinvoiceno.
        /// </summary>
        /// <value>
        /// The debitinvoiceno.
        /// </value>
        private string _debitinvoiceno;

        /// <summary>
        /// Gets or sets the debitinvoiceno.
        /// </summary>
        /// <value>
        /// The debitinvoiceno.
        /// </value>
	    [Editable(true)]
        [Column("debitinvoiceno")]
		public string debitinvoiceno
        {
            get { return _debitinvoiceno; }
            set
            {
                if (_debitinvoiceno != value)
                {
                    PropertyChanged("debitinvoiceno");
					_debitinvoiceno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoicefunc.
        /// </summary>
        /// <value>
        /// The invoicefunc.
        /// </value>
        private string _invoicefunc;

        /// <summary>
        /// Gets or sets the invoicefunc.
        /// </summary>
        /// <value>
        /// The invoicefunc.
        /// </value>
	    [Editable(true)]
        [Column("invoicefunc")]
		public string invoicefunc
        {
            get { return _invoicefunc; }
            set
            {
                if (_invoicefunc != value)
                {
                    PropertyChanged("invoicefunc");
					_invoicefunc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the datetime.
        /// </summary>
        /// <value>
        /// The datetime.
        /// </value>
        private long _datetime;

        /// <summary>
        /// Gets or sets the datetime.
        /// </summary>
        /// <value>
        /// The datetime.
        /// </value>
	    [Editable(true)]
        [Column("datetime")]
		public long datetime
        {
            get { return _datetime; }
            set
            {
                if (_datetime != value)
                {
                    PropertyChanged("datetime");
					_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplierinvoiceno.
        /// </summary>
        /// <value>
        /// The supplierinvoiceno.
        /// </value>
        private string _supplierinvoiceno;

        /// <summary>
        /// Gets or sets the supplierinvoiceno.
        /// </summary>
        /// <value>
        /// The supplierinvoiceno.
        /// </value>
	    [Editable(true)]
        [Column("supplierinvoiceno")]
		public string supplierinvoiceno
        {
            get { return _supplierinvoiceno; }
            set
            {
                if (_supplierinvoiceno != value)
                {
                    PropertyChanged("supplierinvoiceno");
					_supplierinvoiceno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplierinvoicedate.
        /// </summary>
        /// <value>
        /// The supplierinvoicedate.
        /// </value>
        private double _supplierinvoicedate;

        /// <summary>
        /// Gets or sets the supplierinvoicedate.
        /// </summary>
        /// <value>
        /// The supplierinvoicedate.
        /// </value>
	    [Editable(true)]
        [Column("supplierinvoicedate")]
		public double supplierinvoicedate
        {
            get { return _supplierinvoicedate; }
            set
            {
                if (_supplierinvoicedate != value)
                {
                    PropertyChanged("supplierinvoicedate");
					_supplierinvoicedate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the referenceno.
        /// </summary>
        /// <value>
        /// The referenceno.
        /// </value>
        private string _referenceno;

        /// <summary>
        /// Gets or sets the referenceno.
        /// </summary>
        /// <value>
        /// The referenceno.
        /// </value>
	    [Editable(true)]
        [Column("referenceno")]
		public string referenceno
        {
            get { return _referenceno; }
            set
            {
                if (_referenceno != value)
                {
                    PropertyChanged("referenceno");
					_referenceno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the referencedate.
        /// </summary>
        /// <value>
        /// The referencedate.
        /// </value>
        private double _referencedate;

        /// <summary>
        /// Gets or sets the referencedate.
        /// </summary>
        /// <value>
        /// The referencedate.
        /// </value>
	    [Editable(true)]
        [Column("referencedate")]
		public double referencedate
        {
            get { return _referencedate; }
            set
            {
                if (_referencedate != value)
                {
                    PropertyChanged("referencedate");
					_referencedate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pricelistno.
        /// </summary>
        /// <value>
        /// The pricelistno.
        /// </value>
        private string _pricelistno;

        /// <summary>
        /// Gets or sets the pricelistno.
        /// </summary>
        /// <value>
        /// The pricelistno.
        /// </value>
	    [Editable(true)]
        [Column("pricelistno")]
		public string pricelistno
        {
            get { return _pricelistno; }
            set
            {
                if (_pricelistno != value)
                {
                    PropertyChanged("pricelistno");
					_pricelistno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pricelistdate.
        /// </summary>
        /// <value>
        /// The pricelistdate.
        /// </value>
        private double _pricelistdate;

        /// <summary>
        /// Gets or sets the pricelistdate.
        /// </summary>
        /// <value>
        /// The pricelistdate.
        /// </value>
	    [Editable(true)]
        [Column("pricelistdate")]
		public double pricelistdate
        {
            get { return _pricelistdate; }
            set
            {
                if (_pricelistdate != value)
                {
                    PropertyChanged("pricelistdate");
					_pricelistdate = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplierno.
        /// </summary>
        /// <value>
        /// The supplierno.
        /// </value>
        private string _supplierno;

        /// <summary>
        /// Gets or sets the supplierno.
        /// </summary>
        /// <value>
        /// The supplierno.
        /// </value>
	    [Editable(true)]
        [Column("supplierno")]
		public string supplierno
        {
            get { return _supplierno; }
            set
            {
                if (_supplierno != value)
                {
                    PropertyChanged("supplierno");
					_supplierno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the suppliername.
        /// </summary>
        /// <value>
        /// The suppliername.
        /// </value>
        private string _suppliername;

        /// <summary>
        /// Gets or sets the suppliername.
        /// </summary>
        /// <value>
        /// The suppliername.
        /// </value>
	    [Editable(true)]
        [Column("suppliername")]
		public string suppliername
        {
            get { return _suppliername; }
            set
            {
                if (_suppliername != value)
                {
                    PropertyChanged("suppliername");
					_suppliername = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the storeno.
        /// </summary>
        /// <value>
        /// The storeno.
        /// </value>
        private string _storeno;

        /// <summary>
        /// Gets or sets the storeno.
        /// </summary>
        /// <value>
        /// The storeno.
        /// </value>
	    [Editable(true)]
        [Column("storeno")]
		public string storeno
        {
            get { return _storeno; }
            set
            {
                if (_storeno != value)
                {
                    PropertyChanged("storeno");
					_storeno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the storename.
        /// </summary>
        /// <value>
        /// The storename.
        /// </value>
        private string _storename;

        /// <summary>
        /// Gets or sets the storename.
        /// </summary>
        /// <value>
        /// The storename.
        /// </value>
	    [Editable(true)]
        [Column("storename")]
		public string storename
        {
            get { return _storename; }
            set
            {
                if (_storename != value)
                {
                    PropertyChanged("storename");
					_storename = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discountpercent.
        /// </summary>
        /// <value>
        /// The discountpercent.
        /// </value>
        private decimal _discountpercent;

        /// <summary>
        /// Gets or sets the discountpercent.
        /// </summary>
        /// <value>
        /// The discountpercent.
        /// </value>
	    [Editable(true)]
        [Column("discountpercent")]
		public decimal discountpercent
        {
            get { return _discountpercent; }
            set
            {
                if (_discountpercent != value)
                {
                    PropertyChanged("discountpercent");
					_discountpercent = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the discountamount.
        /// </summary>
        /// <value>
        /// The discountamount.
        /// </value>
        private decimal _discountamount;

        /// <summary>
        /// Gets or sets the discountamount.
        /// </summary>
        /// <value>
        /// The discountamount.
        /// </value>
	    [Editable(true)]
        [Column("discountamount")]
		public decimal discountamount
        {
            get { return _discountamount; }
            set
            {
                if (_discountamount != value)
                {
                    PropertyChanged("discountamount");
					_discountamount = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the linessum.
        /// </summary>
        /// <value>
        /// The linessum.
        /// </value>
        private decimal _linessum;

        /// <summary>
        /// Gets or sets the linessum.
        /// </summary>
        /// <value>
        /// The linessum.
        /// </value>
	    [Editable(true)]
        [Column("linessum")]
		public decimal linessum
        {
            get { return _linessum; }
            set
            {
                if (_linessum != value)
                {
                    PropertyChanged("linessum");
					_linessum = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the docsum.
        /// </summary>
        /// <value>
        /// The docsum.
        /// </value>
        private decimal _docsum;

        /// <summary>
        /// Gets or sets the docsum.
        /// </summary>
        /// <value>
        /// The docsum.
        /// </value>
	    [Editable(true)]
        [Column("docsum")]
		public decimal docsum
        {
            get { return _docsum; }
            set
            {
                if (_docsum != value)
                {
                    PropertyChanged("docsum");
					_docsum = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invsum.
        /// </summary>
        /// <value>
        /// The invsum.
        /// </value>
        private decimal _invsum;

        /// <summary>
        /// Gets or sets the invsum.
        /// </summary>
        /// <value>
        /// The invsum.
        /// </value>
	    [Editable(true)]
        [Column("invsum")]
		public decimal invsum
        {
            get { return _invsum; }
            set
            {
                if (_invsum != value)
                {
                    PropertyChanged("invsum");
					_invsum = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the lineno.
        /// </summary>
        /// <value>
        /// The lineno.
        /// </value>
        private double _lineno;

        /// <summary>
        /// Gets or sets the lineno.
        /// </summary>
        /// <value>
        /// The lineno.
        /// </value>
	    [Editable(true)]
        [Column("lineno")]
		public double lineno
        {
            get { return _lineno; }
            set
            {
                if (_lineno != value)
                {
                    PropertyChanged("lineno");
					_lineno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the itembarcode.
        /// </summary>
        /// <value>
        /// The itembarcode.
        /// </value>
        private string _itembarcode;

        /// <summary>
        /// Gets or sets the itembarcode.
        /// </summary>
        /// <value>
        /// The itembarcode.
        /// </value>
	    [Editable(true)]
        [Column("itembarcode")]
		public string itembarcode
        {
            get { return _itembarcode; }
            set
            {
                if (_itembarcode != value)
                {
                    PropertyChanged("itembarcode");
					_itembarcode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the proddesc.
        /// </summary>
        /// <value>
        /// The proddesc.
        /// </value>
        private string _proddesc;

        /// <summary>
        /// Gets or sets the proddesc.
        /// </summary>
        /// <value>
        /// The proddesc.
        /// </value>
	    [Editable(true)]
        [Column("proddesc")]
		public string proddesc
        {
            get { return _proddesc; }
            set
            {
                if (_proddesc != value)
                {
                    PropertyChanged("proddesc");
					_proddesc = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the packno.
        /// </summary>
        /// <value>
        /// The packno.
        /// </value>
        private double _packno;

        /// <summary>
        /// Gets or sets the packno.
        /// </summary>
        /// <value>
        /// The packno.
        /// </value>
	    [Editable(true)]
        [Column("packno")]
		public double packno
        {
            get { return _packno; }
            set
            {
                if (_packno != value)
                {
                    PropertyChanged("packno");
					_packno = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the unitsqty.
        /// </summary>
        /// <value>
        /// The unitsqty.
        /// </value>
        private double _unitsqty;

        /// <summary>
        /// Gets or sets the unitsqty.
        /// </summary>
        /// <value>
        /// The unitsqty.
        /// </value>
	    [Editable(true)]
        [Column("unitsqty")]
		public double unitsqty
        {
            get { return _unitsqty; }
            set
            {
                if (_unitsqty != value)
                {
                    PropertyChanged("unitsqty");
					_unitsqty = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the unitsqtymea.
        /// </summary>
        /// <value>
        /// The unitsqtymea.
        /// </value>
        private string _unitsqtymea;

        /// <summary>
        /// Gets or sets the unitsqtymea.
        /// </summary>
        /// <value>
        /// The unitsqtymea.
        /// </value>
	    [Editable(true)]
        [Column("unitsqtymea")]
		public string unitsqtymea
        {
            get { return _unitsqtymea; }
            set
            {
                if (_unitsqtymea != value)
                {
                    PropertyChanged("unitsqtymea");
					_unitsqtymea = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the reasoncode.
        /// </summary>
        /// <value>
        /// The reasoncode.
        /// </value>
        private string _reasoncode;

        /// <summary>
        /// Gets or sets the reasoncode.
        /// </summary>
        /// <value>
        /// The reasoncode.
        /// </value>
	    [Editable(true)]
        [Column("reasoncode")]
		public string reasoncode
        {
            get { return _reasoncode; }
            set
            {
                if (_reasoncode != value)
                {
                    PropertyChanged("reasoncode");
					_reasoncode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the itempricenetosph.
        /// </summary>
        /// <value>
        /// The itempricenetosph.
        /// </value>
        private decimal _itempricenetosph;

        /// <summary>
        /// Gets or sets the itempricenetosph.
        /// </summary>
        /// <value>
        /// The itempricenetosph.
        /// </value>
	    [Editable(true)]
        [Column("itempricenetosph")]
		public decimal itempricenetosph
        {
            get { return _itempricenetosph; }
            set
            {
                if (_itempricenetosph != value)
                {
                    PropertyChanged("itempricenetosph");
					_itempricenetosph = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the itempricenetosupplier.
        /// </summary>
        /// <value>
        /// The itempricenetosupplier.
        /// </value>
        private decimal _itempricenetosupplier;

        /// <summary>
        /// Gets or sets the itempricenetosupplier.
        /// </summary>
        /// <value>
        /// The itempricenetosupplier.
        /// </value>
	    [Editable(true)]
        [Column("itempricenetosupplier")]
		public decimal itempricenetosupplier
        {
            get { return _itempricenetosupplier; }
            set
            {
                if (_itempricenetosupplier != value)
                {
                    PropertyChanged("itempricenetosupplier");
					_itempricenetosupplier = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the pricedifference.
        /// </summary>
        /// <value>
        /// The pricedifference.
        /// </value>
        private decimal _pricedifference;

        /// <summary>
        /// Gets or sets the pricedifference.
        /// </summary>
        /// <value>
        /// The pricedifference.
        /// </value>
	    [Editable(true)]
        [Column("pricedifference")]
		public decimal pricedifference
        {
            get { return _pricedifference; }
            set
            {
                if (_pricedifference != value)
                {
                    PropertyChanged("pricedifference");
					_pricedifference = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the linesum.
        /// </summary>
        /// <value>
        /// The linesum.
        /// </value>
        private decimal _linesum;

        /// <summary>
        /// Gets or sets the linesum.
        /// </summary>
        /// <value>
        /// The linesum.
        /// </value>
	    [Editable(true)]
        [Column("linesum")]
		public decimal linesum
        {
            get { return _linesum; }
            set
            {
                if (_linesum != value)
                {
                    PropertyChanged("linesum");
					_linesum = value;
                    
                }
            }
        }
	}
}
