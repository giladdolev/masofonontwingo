using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class dwh_invoice_marlog : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public dwh_invoice_marlog()
        {
            this._move_date = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="distributor_number">The distributor_number.</param>
		/// <param name="shipment_number">The shipment_number.</param>
		/// <param name="move_date">The move_date.</param>
		/// <param name="state">The state.</param>
        public dwh_invoice_marlog(long distributor_number, double shipment_number, DateTime? move_date, string state)
        {
			this._distributor_number = distributor_number;
			this._shipment_number = shipment_number;
			this._move_date = move_date;
			this._state = state;
		}

	
        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
        private long _distributor_number;

        /// <summary>
        /// Gets or sets the distributor_number.
        /// </summary>
        /// <value>
        /// The distributor_number.
        /// </value>
	    [Editable(true)]
        [Column("distributor_number")]
		public long distributor_number
        {
            get { return _distributor_number; }
            set
            {
                if (_distributor_number != value)
                {
                    PropertyChanged("distributor_number");
					_distributor_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
        private double _shipment_number;

        /// <summary>
        /// Gets or sets the shipment_number.
        /// </summary>
        /// <value>
        /// The shipment_number.
        /// </value>
	    [Editable(true)]
        [Column("shipment_number")]
		public double shipment_number
        {
            get { return _shipment_number; }
            set
            {
                if (_shipment_number != value)
                {
                    PropertyChanged("shipment_number");
					_shipment_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the move_date.
        /// </summary>
        /// <value>
        /// The move_date.
        /// </value>
        private DateTime? _move_date;

        /// <summary>
        /// Gets or sets the move_date.
        /// </summary>
        /// <value>
        /// The move_date.
        /// </value>
	    [Editable(true)]
        [Column("move_date")]
		public DateTime? move_date
        {
            get { return _move_date; }
            set
            {
                if (_move_date != value)
                {
                    PropertyChanged("move_date");
					_move_date = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	}
}
