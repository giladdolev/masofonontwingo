using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;
using System.Extensions;

namespace Mesofon.Models
{
	public class d_mini_terminal_trade_order_list : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_mini_terminal_trade_order_list()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="site_order_number">The site_order_number.</param>
		/// <param name="date_move">The date_move.</param>
		/// <param name="target_branch_datetime">The target_branch_datetime.</param>
		/// <param name="approve_ind">The approve_ind.</param>
		/// <param name="state">The state.</param>
		/// <param name="site_state">The site_state.</param>
		/// <param name="date_move_short">The date_move_short.</param>
        public d_mini_terminal_trade_order_list(long branch_number, double site_order_number, string date_move, DateTime? target_branch_datetime, long approve_ind, string state, string site_state, DateTime? date_move_short)
        {
			this._branch_number = branch_number;
			this._site_order_number = site_order_number;
			this._date_move = date_move;
			this._target_branch_datetime = target_branch_datetime;
			this._approve_ind = approve_ind;
			this._state = state;
			this._site_state = site_state;
			this._date_move_short = date_move_short;
		}

	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the site_order_number.
        /// </summary>
        /// <value>
        /// The site_order_number.
        /// </value>
        private double _site_order_number;

        /// <summary>
        /// Gets or sets the site_order_number.
        /// </summary>
        /// <value>
        /// The site_order_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("site_order_number")]
        [PropertiesAttribute("site_order_number", "����� ��� ���", true,0,typeof(double),120)]
        public double site_order_number
        {
            get { return _site_order_number; }
            set
            {
                if (_site_order_number != value)
                {
                    PropertyChanged("site_order_number");
					_site_order_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
        private string _date_move;

        /// <summary>
        /// Gets or sets the date_move.
        /// </summary>
        /// <value>
        /// The date_move.
        /// </value>
	    [Editable(true)]
        [Column("date_move")]
        [PropertiesAttribute("date_move", "����� �����",true,2,typeof(DateTime),110)]

        public string date_move
        {
            get { return _date_move; }
            set
            {
                if (_date_move != value)
                {
                    PropertyChanged("date_move");
					_date_move = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the target_branch_datetime.
        /// </summary>
        /// <value>
        /// The target_branch_datetime.
        /// </value>
        private DateTime? _target_branch_datetime;

        /// <summary>
        /// Gets or sets the target_branch_datetime.
        /// </summary>
        /// <value>
        /// The target_branch_datetime.
        /// </value>
	    [Editable(true)]
        [Column("target_branch_datetime")]
		public DateTime? target_branch_datetime
        {
            get { return _target_branch_datetime; }
            set
            {
                if (_target_branch_datetime != value)
                {
                    PropertyChanged("target_branch_datetime");
					_target_branch_datetime = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the approve_ind.
        /// </summary>
        /// <value>
        /// The approve_ind.
        /// </value>
        private long _approve_ind;

        /// <summary>
        /// Gets or sets the approve_ind.
        /// </summary>
        /// <value>
        /// The approve_ind.
        /// </value>
	    [Editable(true)]
        [Column("approve_ind")]
        [PropertiesAttribute("approve_ind", "�����", true, 1,typeof(Boolean),70)]
        public long approve_ind
        {
            get { return _approve_ind; }
            set
            {
                if (_approve_ind != value)
                {
                    PropertyChanged("approve_ind");
					_approve_ind = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        private string _state;

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
	    [Editable(true)]
        [Column("state")]
		public string state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    PropertyChanged("state");
					_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the site_state.
        /// </summary>
        /// <value>
        /// The site_state.
        /// </value>
        private string _site_state;

        /// <summary>
        /// Gets or sets the site_state.
        /// </summary>
        /// <value>
        /// The site_state.
        /// </value>
	    [Editable(true)]
        [Column("site_state")]
		public string site_state
        {
            get { return _site_state; }
            set
            {
                if (_site_state != value)
                {
                    PropertyChanged("site_state");
					_site_state = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the date_move_short.
        /// </summary>
        /// <value>
        /// The date_move_short.
        /// </value>
        private DateTime? _date_move_short;

        /// <summary>
        /// Gets or sets the date_move_short.
        /// </summary>
        /// <value>
        /// The date_move_short.
        /// </value>
	    [Editable(true)]
        [Column("date_move_short")]
		public DateTime? date_move_short
        {
            get { return _date_move_short; }
            set
            {
                if (_date_move_short != value)
                {
                    PropertyChanged("date_move_short");
					_date_move_short = value;
                    
                }
            }
        }
	}
}
