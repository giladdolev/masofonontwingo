using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Transposition.Extensions;

namespace Mesofon.Models
{
	public class d_invoices_lock : ModelBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
        public d_invoices_lock()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="d_demo1"/> class.
        /// </summary>
		/// <param name="invoice_number">The invoice_number.</param>
		/// <param name="branch_number">The branch_number.</param>
		/// <param name="supplier_number">The supplier_number.</param>
		/// <param name="invoice_type">The invoice_type.</param>
		/// <param name="create_mode">The create_mode.</param>
		/// <param name="last_update_datetime">The last_update_datetime.</param>
        public d_invoices_lock(long invoice_number, long branch_number, long supplier_number, string invoice_type, string create_mode, DateTime? last_update_datetime)
        {
			this._invoice_number = invoice_number;
			this._branch_number = branch_number;
			this._supplier_number = supplier_number;
			this._invoice_type = invoice_type;
			this._create_mode = create_mode;
			this._last_update_datetime = last_update_datetime;
		}

	
        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
        private long _invoice_number;

        /// <summary>
        /// Gets or sets the invoice_number.
        /// </summary>
        /// <value>
        /// The invoice_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_number")]
		public long invoice_number
        {
            get { return _invoice_number; }
            set
            {
                if (_invoice_number != value)
                {
                    PropertyChanged("invoice_number");
					_invoice_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
        private long _branch_number;

        /// <summary>
        /// Gets or sets the branch_number.
        /// </summary>
        /// <value>
        /// The branch_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("branch_number")]
		public long branch_number
        {
            get { return _branch_number; }
            set
            {
                if (_branch_number != value)
                {
                    PropertyChanged("branch_number");
					_branch_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
        private long _supplier_number;

        /// <summary>
        /// Gets or sets the supplier_number.
        /// </summary>
        /// <value>
        /// The supplier_number.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("supplier_number")]
		public long supplier_number
        {
            get { return _supplier_number; }
            set
            {
                if (_supplier_number != value)
                {
                    PropertyChanged("supplier_number");
					_supplier_number = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
        private string _invoice_type;

        /// <summary>
        /// Gets or sets the invoice_type.
        /// </summary>
        /// <value>
        /// The invoice_type.
        /// </value>
		[Key()]
	    [Editable(true)]
        [Column("invoice_type")]
		public string invoice_type
        {
            get { return _invoice_type; }
            set
            {
                if (_invoice_type != value)
                {
                    PropertyChanged("invoice_type");
					_invoice_type = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
        private string _create_mode;

        /// <summary>
        /// Gets or sets the create_mode.
        /// </summary>
        /// <value>
        /// The create_mode.
        /// </value>
	    [Editable(true)]
        [Column("create_mode")]
		public string create_mode
        {
            get { return _create_mode; }
            set
            {
                if (_create_mode != value)
                {
                    PropertyChanged("create_mode");
					_create_mode = value;
                    
                }
            }
        }
	
        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
        private DateTime? _last_update_datetime;

        /// <summary>
        /// Gets or sets the last_update_datetime.
        /// </summary>
        /// <value>
        /// The last_update_datetime.
        /// </value>
	    [Editable(true)]
        [Column("last_update_datetime")]
		public DateTime? last_update_datetime
        {
            get { return _last_update_datetime; }
            set
            {
                if (_last_update_datetime != value)
                {
                    PropertyChanged("last_update_datetime");
					_last_update_datetime = value;
                    
                }
            }
        }
	}
}
