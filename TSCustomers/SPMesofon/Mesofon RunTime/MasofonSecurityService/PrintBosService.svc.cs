﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MasofonSecurityService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PrintBosService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PrintBosService.svc or PrintBosService.svc.cs at the Solution Explorer and start debugging.
    public class PrintBosService : IPrintBosService
    {
        public bool Print(string deviceId, string userId, string password, string template, string data)
        {
            PrintBosJobWorker job = new PrintBosJobWorker("GIZMOX", data);
            Tuple<string, string, bool> ret;
            try
            {
                ret = job.DoWork();
            }
            catch (Exception ex)
            {
                ret = Tuple.Create("-1", ex.Message, false);
            }

            var tempFolder = @"C:\\Superpharm-Temp";

            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            var file = tempFolder + "\\" + DateTime.Now.Ticks.ToString() + ".txt";
            using (StreamWriter st = new StreamWriter(file))
            {
                st.WriteLine("deviceId: " + deviceId);
                st.WriteLine("userId: " + userId);
                st.WriteLine("password: " + password);
                st.WriteLine("template name : " + template);
                st.WriteLine(string.Format("Job Id:{0}, Message:{1} ", ret.Item1, ret.Item2));
                st.WriteLine("data : ");
                st.WriteLine(data);
            }
            return true;
        }
    }
}
