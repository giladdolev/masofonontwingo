﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MasofonSecurityService
{
    public class SecurityService : ISecurityService
    {
        public Tuple<string, string, bool> Authentication(string deviceId)
        {
            DataLayer.Data data = new DataLayer.Data();
            var returnValue = data.GetDeviceAuthorization(deviceId);
            return returnValue;
        }

        public Tuple<List<RegistryItem>, bool> Authentication2(string userId, string password, string deviceId)
        {
            DataLayer.Data data= new DataLayer.Data();
            var registryParameterList =  data.GetUserAuthorization(userId, password, deviceId);
            return registryParameterList;
        }

        public string GetSybaseNewId()
        {
            DataLayer.Data data = new DataLayer.Data();
            return data.GetSybaseNewId();
        }

        public void UpdateRegistryItem(string userId, string password, RegistryItem item)
        {
            DataLayer.Data data = new DataLayer.Data();
            data.UpdateRegistryItem(userId, password, item);
        }

        public void InsertRegistryItem(string userId, string password, RegistryItem item)
        {
            DataLayer.Data data = new DataLayer.Data();
            data.InsertRegistryItem(userId, password, item);
        }

        public void DeleteRegistryItem(string userId, string password, RegistryItem item)
        {
            DataLayer.Data data = new DataLayer.Data();
            //data.DeleteRegistryItem(userId, password, item);
        }

        public void WriteToLog(string userId, string password,string branchNumber , string stationNumber , string message, string description)
        {
            DataLayer.Data data = new DataLayer.Data();
            data.WriteToLog(userId, password, branchNumber,stationNumber, message, description);
        }
    }
}
