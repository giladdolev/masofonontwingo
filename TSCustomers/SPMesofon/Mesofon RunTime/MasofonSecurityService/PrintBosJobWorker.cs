﻿using MasofonSecurityService.PrintBos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace MasofonSecurityService
{
    public class PrintBosJobWorker
    {
        #region Prop

        public int Timeout { get; set; }
        public string JobData { get; set; }
        public string QueueName { get; set; }
        public string DestFolder { get; set; }
        public string PdfFilePath { get; set; }
        public string JobId { get; set; }
        public string StatusMessage { get; set; }
        public string wsUrl { get; set; }
        public string EncodingStr { get; set; }

        #endregion Prop

        public PrintBosJobWorker(string queueName, string data)
        {
            this.Timeout = 10000;
            this.DestFolder = "";
            this.JobData = data;
            this.QueueName = queueName;
            this.PdfFilePath = "";
            this.wsUrl = "http://printbosdev.super-pharm.co.il:8080/PBWcfServiceSite/PrintBOS.svc?wsdl";
            this.EncodingStr = "UTF-8";
        }

        public Tuple<string, string, bool> DoWork()
        {
            PBRequestJOBSJobControlData controlData = new PBRequestJOBSJobControlData()
            {
                queueNameField = QueueName,
                userNameField = "Default"
            };

            string base64str = Convert.ToBase64String(Encoding.UTF8.GetBytes(JobData));

            PBRequestJOBSJob job = new PBRequestJOBSJob()
            {
                bodyField = base64str,
                controlDataField = controlData,
                compressedContentField = false,
                compressedReplyContentField = false,
                contentEncodingField = EncodingStr,
                typeOfReplyTransmissionField = TRANSMISSION.BASE64,
                typeOfTransmissionField = TRANSMISSION.BASE64
            };

            PBRequestJOBS reqItemField = new PBRequestJOBS()
            {
                jobField = new PBRequestJOBSJob[] { job }
            };

            PBRequest request = new PBRequest()
            {
                itemField = reqItemField
            };

            TransferServiceClient client = new TransferServiceClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(wsUrl);
            string serverIP = string.Empty;
            PBResponse response = null;

            try
            {
                response = client.ProcessRequestSync(null, request, Timeout, out serverIP);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                throw new Exception("Could not communicate with PrintBOS web service. Please check if the service is installed and the address is configured correctly.", ex);
            }

            if (response.requestResultField == null || response.requestResultField.Length < 1)
            {
                if (!string.IsNullOrEmpty(response.requestStatusField.statusMessageField))
                    throw new Exception(response.requestStatusField.statusMessageField);
                else
                    throw new Exception(String.Format("Data was sent, but there was a problem on the server. JobId = {0}", response.requestStatusField.requestIdField));
            }

            this.JobId = response.requestResultField[0].jobIdField;
            this.StatusMessage = response.requestResultField[0].statusMessageField;

            return new Tuple<string, string, bool>(JobId, StatusMessage, true);
        }
    }
}