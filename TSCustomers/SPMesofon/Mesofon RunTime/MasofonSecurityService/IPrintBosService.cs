﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MasofonSecurityService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPrintBosService" in both code and config file together.
    [ServiceContract]
    public interface IPrintBosService
    {
        [OperationContract]
        bool Print(string deviceId, string userId, string password, string template, string data);
    }
}
