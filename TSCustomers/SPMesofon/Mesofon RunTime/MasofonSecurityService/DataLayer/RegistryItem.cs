﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasofonSecurityService
{
    [DataContract]
    public class RegistryItem
    {
        [Editable(false)]
        [Column("guid")]
        [DataMember]
        public Guid Id;

        [Editable(true)]
        [Column("branch_number")]
        [DataMember]
        public int BranchNumber;

        [Editable(true)]
        [Column("station_number")]
        [DataMember]
        public int StationNumber;

        [Editable(true)]
        [Column("app_name")]
        [DataMember]
        public string AppName;

        [Editable(true)]
        [Column("date_move")]
        [DataMember]
        public DateTime DateMove;

        [Editable(true)]
        [Column("last_update_datetime")]
        [DataMember]
        public DateTime LastUpdateDatetime;

        [Editable(true)]
        [Column("reg_param")]
        [DataMember]
        public string RegParam;

        //reg_value
        [Editable(true)]
        [Column("reg_value")]
        [DataMember]
        public string RegValue;

        [Editable(true)]
        [Column("reg_description")]
        [DataMember]
        public string RegDescription;

        [Editable(true)]
        [Column("reg_old_value")]
        [DataMember]
        public string RegOldValue;

        internal static List<RegistryItem> GetData(DataSet ds)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                List<RegistryItem> items = new List<RegistryItem>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    try
                    {
                        var registryItem = new RegistryItem();
                        registryItem.Id = Guid.Parse(row["guid"].ToString());
                        registryItem.BranchNumber = int.Parse(row["branch_number"].ToString());
                        registryItem.StationNumber = int.Parse(row["station_number"].ToString());
                        registryItem.AppName = row["app_name"].ToString();
                        registryItem.DateMove = DateTime.Parse(row["date_move"].ToString());
                        DateTime.TryParse(row["last_update_datetime"].ToString(), out registryItem.LastUpdateDatetime);
                        registryItem.RegParam = row["reg_param"].ToString();
                        registryItem.RegValue = row["reg_value"].ToString();
                        registryItem.RegDescription = row["reg_description"].ToString();
                        registryItem.RegOldValue = row["reg_old_value"].ToString();
                        items.Add(registryItem);
                    }
                    catch
                    {

                    }
                   
                }
                return items;
            }
            return null;
        }
    }
}