﻿using Sybase.Data.AseClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace MasofonSecurityService.DataLayer
{
    public class Data
    {
        public Tuple<string,string,bool> GetDeviceAuthorization(string deviceId)
        {
            return Tuple.Create("7","2",true);
        }
        public Tuple<List<RegistryItem>, bool> GetUserAuthorization(string userName, string passowrd,string deviceId)
        {
            AseConnection conn = null;
            List<RegistryItem> registryParameterList = null;
            try
            {
                DataSet ds = new DataSet();
                string ConnectionStrings = ConfigurationManager.ConnectionStrings["Masofon"].ConnectionString;

                conn = new AseConnection();

                conn.ConnectionString = ConnectionStrings;
                AseCommand cmd = null;

                //Alex - Fix the SP to retrive all columns
                string sqlString =
                    $"exec sp__get_registry 'MASOFON', {"7"}, {"2"}";

                conn.Open();

                cmd = new AseCommand(sqlString, conn);
                AseDataAdapter adapter = new AseDataAdapter(cmd);
                adapter.Fill(ds);
                registryParameterList = RegistryItem.GetData(ds);

                return Tuple.Create(registryParameterList,true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return Tuple.Create(registryParameterList, false);
        }

        internal string GetSybaseNewId()
        {
            AseConnection conn = null;

            try
            {
                DataSet ds = new DataSet();
                string ConnectionStrings = ConfigurationManager.ConnectionStrings["Masofon"].ConnectionString;
                conn = new AseConnection();
                conn.ConnectionString = ConnectionStrings;
                AseCommand cmd = null;

                //ToDo replace with SP - ALEX
                string sqlString = string.Format("select newid()");

                conn.Open();

                cmd = new AseCommand(sqlString, conn);
                AseDataAdapter adapter = new AseDataAdapter(cmd);
                adapter.Fill(ds);
                string id = string.Empty;
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    id = ds.Tables[0].Rows[0][0].ToString();
                }

                return id;
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        internal void WriteToLog(string userId, string password,string branchNumber,string stationNumber, string message, string description)
        {
            AseConnection conn = null;
            string sqlString = "";
            try
            {
                DataSet ds = new DataSet();
                string ConnectionStrings = ConfigurationManager.ConnectionStrings["Masofon"].ConnectionString;
                conn = new AseConnection();
                conn.ConnectionString = ConnectionStrings;
                AseCommand cmd = null;

                
                sqlString =
                    string.Format(
                        "insert into global_log (guid, branch_number, station_number, app_name, user_id, last_update_datetime, log_message, log_description) " +
                        "values (newid(), {0}, {1}, '{2}', '{3}', GETDATE(), '{4}', '{5}')",
                        branchNumber, stationNumber, "masofon", userId, message, description);

                conn.Open();

                cmd = new AseCommand(sqlString, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string msg = string.Format("{0} {1} Sqlquery : {2} {1} Trace {3}", e.Message, Environment.NewLine, sqlString, e.StackTrace);
                System.Diagnostics.EventLog.WriteEntry("Mesofon", msg, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public void UpdateRegistryItem(string userId, string password, RegistryItem item)
        {
            AseConnection conn = null;

            try
            {
                DataSet ds = new DataSet();
                string ConnectionStrings = ConfigurationManager.ConnectionStrings["Masofon"].ConnectionString;
                conn = new AseConnection();
                conn.ConnectionString = ConnectionStrings;
                AseCommand cmd = null;

                string sqlString =
                    string.Format(
                        "update global_registry_parameters set reg_param = '{1}', reg_value = '{2}', last_update_datetime = GETDATE() where guid = '{0}'",
                        item.Id, item.RegParam, item.RegValue);

                conn.Open();

                cmd = new AseCommand(sqlString, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public void InsertRegistryItem(string userId, string password, RegistryItem item)
        {
            AseConnection conn = null;

            try
            {
                DataSet ds = new DataSet();
                string ConnectionStrings = ConfigurationManager.ConnectionStrings["Masofon"].ConnectionString;
                conn = new AseConnection();
                conn.ConnectionString = ConnectionStrings;
                AseCommand cmd = null;

                string sqlString =
                    string.Format(
                        "insert into global_registry_parameters (guid, branch_number, station_number, app_name, date_move, reg_param, reg_value, last_update_datetime) values(newid(), {0}, {1}, '{2}', GETDATE(), '{3}', '{4}', GETDATE())",
                         item.BranchNumber, item.StationNumber, item.AppName, item.RegParam, item.RegValue);

                conn.Open();

                cmd = new AseCommand(sqlString, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}