﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MasofonSecurityService
{
    [ServiceContract]
    public interface ISecurityService
    {
        [OperationContract]
        Tuple<string, string, bool> Authentication(string deviceId);

        [OperationContract]
        Tuple<List<RegistryItem>,bool> Authentication2(string userId, string password, string deviceId);

        [OperationContract]
        string GetSybaseNewId();

        [OperationContract]
        void UpdateRegistryItem(string userId, string password, RegistryItem item);

        [OperationContract]
        void InsertRegistryItem(string userId, string password, RegistryItem item);

        [OperationContract]
        void DeleteRegistryItem(string userId, string password, RegistryItem item);

        [OperationContract]
        void WriteToLog(string userId, string password,string branchNumber,string stationNumber, string message, string description);
    }
}
