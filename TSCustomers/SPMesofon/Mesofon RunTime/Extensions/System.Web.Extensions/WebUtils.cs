﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace System.Web.Extensions
{
    public static class WebUtils
    {
        /// <summary>
        /// Gets the item values.
        /// </summary>
        /// <param name="objValues">The values.</param>
        /// <param name="strName">Name of the item.</param>
        /// <returns></returns>
        public static ReadOnlyCollection<string> GetItemValues(NameValueCollection objValues, string strName)
        {
            if (objValues == null || String.IsNullOrEmpty(strName))
            {
                return new ReadOnlyCollection<string>(new string[] { });
            }

            string[] arrValues = objValues.GetValues(strName);
            return new ReadOnlyCollection<string>(arrValues);

        }

        /// <summary>
        /// Get last server error.
        /// </summary>
        /// <returns></returns>
        public static ErrObject Err()
        {
            HttpContext onjContext = HttpContext.Current;
            if (onjContext != null)
            {

                Exception ex = onjContext.Server.GetLastError();
                if (ex != null)
                {
                    Information.Err().Clear();
                    ProjectData.SetProjectError(ex);
                }
            }
            return Information.Err();
        }
    }
}
