
using Microsoft.Win32;
namespace System.Extensions
{
    using System;
    using Collections;
    //    using System.Drawing;
    using CompilerServices;
    using VBEnums;
    using VBExtensions;
    using Globalization;
    using Reflection;
    using Runtime.InteropServices;
    using Runtime.InteropServices.ComTypes;
    using Security;
    using Security.Permissions;
    using Threading;

    //    using System.Windows.Forms;

    /// <summary>
    /// Interaction class
    /// </summary>
    [StandardModuleAttribute()]
    public sealed class InteractionEx
    {
        private InteractionEx()
        {

        }

        // Fields

        private static string _s_CommandLine;
        private static SortedList _s_SortedEnvList;

        /// <summary>
        /// Returns one of two parts, depending on the evaluation of an expression.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="truePart"></param>
        /// <param name="falsePart"></param>
        /// <returns>object</returns>
        public static T IIf<T>(bool expression, T truePart, T falsePart)
        {
            if (expression)
            {
                return truePart;
            }
            return falsePart;
        }

        /// <summary>
        /// Returns one of two parts, depending on the evaluation of an expression.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="truePart"></param>
        /// <param name="falsePart"></param>
        /// <returns>object</returns>
        public static object IIf(bool expression, object truePart, object falsePart)
        {
            if (expression)
            {
                return truePart;
            }
            return falsePart;
        }

        /// <summary>
        /// Get Setting
        /// </summary>
        /// <param name="appName">required. String value.</param>
        /// <param name="section">required. String value.</param>
        /// <param name="key"><Required. String value./param>
        /// <returns>String value.</returns>
        public static string GetSetting(string appName, string section, string key)
        {
            return GetSetting(appName, section, key, "");
        }

        /// <summary>
        /// Get Setting
        /// </summary>
        /// <param name="appName">required. String value.</param>
        /// <param name="section">required. String value.</param>
        /// <param name="key"><Required. String value./param>
        /// <param name="default">Optional. String value.</param>
        /// <returns>String value.</returns>
        public static string GetSetting(string appName, string section, string key, string @default)
        {
            CheckPathComponent(appName);
            CheckPathComponent(section);
            CheckPathComponent(key);
            if (@default == null)
            {
                @default = String.Empty;
            }
            string name = FormRegKey(appName, section);
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(name);
            if (registryKey == null)
            {
                return @default;
            }
            object obj2 = registryKey.GetValue(key, @default);
            registryKey.Close();
            if (obj2 == null)
            {
                return null;
            }

            string strObj2 = obj2 as string;
            if (String.IsNullOrEmpty(strObj2))
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue"));
            }
            return strObj2;
        }

        /// <summary>
        /// From registry key
        /// </summary>
        /// <param name="sApp">Required. String value.</param>
        /// <param name="sSect">Required. String value</param>
        /// <returns>String value.</returns>
        private static string FormRegKey(string sApp, string sSect)
        {
            if (Information.IsNothing(sApp) || (sApp.Length == 0))
            {
                return "Software\\VB and VBA Program Settings";
            }
            if (Information.IsNothing(sSect) || (sSect.Length == 0))
            {
                return ("Software\\VB and VBA Program Settings\\" + sApp);
            }
            return ("Software\\VB and VBA Program Settings\\" + sApp + "\\" + sSect);
        }

        /// <summary>
        /// Check Path Component
        /// </summary>
        /// <param name="s">Required. String value.</param>
        private static void CheckPathComponent(string s)
        {
            if ((s == null) || (s.Length == 0))
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty"));
            }
        }

        /// <summary>
        /// Saves or creates an application entry in the Windows registry.
        /// </summary>
        /// <param name="appName">Required. String expression containing the name of the application or project to which the setting applies.</param>
        /// <param name="section">Required. String expression containing the name of the section in which the key setting is being saved.</param>
        /// <param name="key">Required. String expression containing the name of the key setting being saved.</param>
        /// <param name="setting">Required. Expression containing the value to which Key is being set.</param>
        public static void SaveSetting(string appName, string section, string key, string setting)
        {
            CheckPathComponent(appName);
            CheckPathComponent(section);
            CheckPathComponent(key);
            string subkey = FormRegKey(appName, section);
            RegistryKey registryKey = Registry.CurrentUser.CreateSubKey(subkey);
            if (registryKey == null)
            {
                throw new ArgumentException(Utils.GetResourceString("Interaction_ResKeyNotCreated1", subkey));
            }
            try
            {
                registryKey.SetValue(key, setting);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                registryKey.Close();
            }
        }

        /// <summary>
        /// Creates and returns a reference to a COM object.
        /// </summary>
        /// <param name="progId">Required. String. The program ID of the object to create.</param>
        /// <returns>Creates and returns a reference to a COM object.</returns>
        public static object CreateObject(string progId)
        {
            return CreateObject(progId, "");
        }

        /// <summary>
        /// Creates and returns a reference to a COM object.
        /// </summary>
        /// <param name="progId">Required. String. The program ID of the object to create.</param>
        /// <param name="serverName">Optional. String. The name of the network server where the object will be created.</param>
        /// <returns>Creates and returns a reference to a COM object.</returns>
        public static object CreateObject(string progId, string serverName)
        {
            if (String.IsNullOrEmpty(progId))
            {
                throw new ArgumentNullException("progId");
            }
            object obj2;
            if (progId.Length == 0)
            {
                throw ExceptionUtils.VbMakeException(0x1ad);
            }
            if ((serverName == null) || (serverName.Length == 0))
            {
                serverName = null;
            }
            else if (Thread.CurrentThread.CurrentCulture.CompareInfo.Compare(Environment.MachineName, serverName, CompareOptions.IgnoreCase) == 0)
            {
                serverName = null;
            }
            try
            {
                Type typeFromProgID;
                if (serverName == null)
                {
                    typeFromProgID = Type.GetTypeFromProgID(progId);
                }

                else
                {
                    typeFromProgID = Type.GetTypeFromProgID(progId, serverName, true);
                }
                obj2 = Activator.CreateInstance(typeFromProgID);
            }
            catch (COMException exception)
            {
                if (exception.ErrorCode == -2147023174)
                {
                    throw ExceptionUtils.VbMakeException(0x1ce);
                }
                throw ExceptionUtils.VbMakeException(0x1ad);
            }
            catch (Exception)
            {
                throw ExceptionUtils.VbMakeException(0x1ad);
            }
            return obj2;
        }

        /// <summary>
        /// Returns the argument portion of the command line used to start Visual Basic or an executable program developed with Visual Basic.
        /// </summary>
        /// <returns>Returns the argument portion of the command line used to start Visual Basic or an executable program developed with Visual Basic.</returns>
        public static string Command()
        {
            new EnvironmentPermission(EnvironmentPermissionAccess.Read, "Path").Demand();
            if (_s_CommandLine == null)
            {
                int index = 0;
                string commandLine = Environment.CommandLine;
                if ((commandLine == null) || (commandLine.Length == 0))
                {
                    return String.Empty;
                }
                int length = Environment.GetCommandLineArgs()[0].Length;
                do
                {
                    index = commandLine.IndexOf('"', index);
                    if ((index >= 0) && (index <= length))
                    {
                        commandLine = commandLine.Remove(index, 1);
                    }
                }
                while ((index >= 0) && (index <= length));
                if ((index == 0) || (index > commandLine.Length))
                {
                    _s_CommandLine = String.Empty;
                }

                else
                {
                    _s_CommandLine = StringsEx.LTrim(commandLine.Substring(length));
                }
            }
            return _s_CommandLine;
        }

        /// <summary>
        /// Returns a reference to an object provided by a COM component.
        /// </summary>
        /// <returns>object</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static object GetObject()
        {
            return GetObject(null, null);
        }

        /// <summary>
        /// Returns a reference to an object provided by a COM component.
        /// </summary>
        /// <param name="pathName">Name of the path.</param>
        /// <returns>object</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static object GetObject(string pathName)
        {
            return GetObject(pathName, null);
        }

        /// <summary>
        /// Returns a reference to an object provided by a COM component.
        /// </summary>
        /// <param name="pathName">Name of the path.</param>
        /// <param name="class">The class.</param>
        /// <returns>object</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static object GetObject(string pathName, string @class)
        {
            IPersistFile activeObject;
            if (StringsEx.Len(@class) == 0)
            {
                try
                {
                    //Gets an interface pointer identified by the specified moniker.
                    return Marshal.BindToMoniker(pathName);
                }
                catch (Exception)
                {
                    throw ExceptionUtils.VbMakeException(0x1ad);
                }
            }
            if (pathName == null)
            {
                try
                {
                    //Obtains a running instance of the specified object from the running object table (ROT).
                    return Marshal.GetActiveObject(@class);
                }
                catch (Exception)
                {
                    throw ExceptionUtils.VbMakeException(0x1ad);
                }
            }
            if (StringsEx.Len(pathName) == 0)
            {
                try
                {
                    //Creates an instance of the specified type using the constructor that best matches the specified parameters.
                    return Activator.CreateInstance(Type.GetTypeFromProgID(@class));
                }
                catch (Exception)
                {
                    throw ExceptionUtils.VbMakeException(0x1ad);
                }
            }
            try
            {
                //Obtains a running instance of the specified object from the running object table (ROT).
                activeObject = (IPersistFile)Marshal.GetActiveObject(@class);
            }
            catch (Exception)
            {
                throw ExceptionUtils.VbMakeException(0x1b0);
            }
            try
            {
                //load the object
                activeObject.Load(pathName, 0);
            }
            catch (Exception)
            {
                throw ExceptionUtils.VbMakeException(0x1ad);
            }
            return activeObject;
        }

        /// <summary>
        /// Activates an application that is already running.
        /// </summary>
        /// <param name="processId">Integer specifying the Win32 process ID number assigned to this process. You can use the ID returned by the Shell, provided it is not zero.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static void AppActivate(int processId)
        {
            int windowHandle;
            int num = 0;
            IntPtr ptr2 = default(IntPtr);
            IntPtr window = NativeMethods.GetWindow(NativeMethods.GetDesktopWindow(), 5);
            while (window != ptr2)
            {
                windowHandle = SafeNativeMethods.GetWindowThreadProcessId(window, ref num);
                if (windowHandle > 0 && ((num == processId) && SafeNativeMethods.IsWindowEnabled(window)) && SafeNativeMethods.IsWindowVisible(window))
                {
                    break;
                }
                window = NativeMethods.GetWindow(window, 2);
            }
            if (window == ptr2)
            {
                window = NativeMethods.GetWindow(NativeMethods.GetDesktopWindow(), 5);
                while (window != ptr2)
                {
                    windowHandle = SafeNativeMethods.GetWindowThreadProcessId(window, ref num);
                    if (windowHandle > 0 && num == processId)
                    {
                        break;
                    }
                    window = NativeMethods.GetWindow(window, 2);
                }
            }
            if (window == ptr2)
            {
                throw new ArgumentException(Utils.GetResourceString("ProcessNotFound", StringType.FromInteger(processId)));
            }
            AppActivateHelper(window);
        }

        /// <summary>
        /// Activates an application that is already running.
        /// </summary>
        /// <param name="title">String expression specifying the title in the title bar of the application you want to activate. You can use the title assigned to the application when it was launched.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static void AppActivate(string title)
        {
            IntPtr ptr2 = default(IntPtr);
            IntPtr hwnd = NativeMethods.FindWindow(IntPtr.Zero, ref title);
            if (hwnd == ptr2)
            {
                string lpString = StringsEx.StrDup(0x1ff, ' ');
                int length = StringsEx.Len(title);
                hwnd = NativeMethods.GetWindow(NativeMethods.GetDesktopWindow(), 5);
                while (hwnd != ptr2)
                {
                    if ((NativeMethods.GetWindowText(hwnd, ref lpString, 0x1ff) >= length) && (string.Compare(lpString, 0, title, 0, length, StringComparison.OrdinalIgnoreCase) == 0))
                    {
                        break;
                    }
                    hwnd = NativeMethods.GetWindow(hwnd, 2);
                }
                if (hwnd == ptr2)
                {
                    hwnd = NativeMethods.GetWindow(NativeMethods.GetDesktopWindow(), 5);
                    while (hwnd != ptr2)
                    {
                        if ((NativeMethods.GetWindowText(hwnd, ref lpString, 0x1ff) >= length) && (string.Compare(StringsEx.Right(lpString, length), 0, title, 0, length, true, CultureInfo.CurrentCulture) == 0))
                        {
                            break;
                        }
                        hwnd = NativeMethods.GetWindow(hwnd, 2);
                    }
                }
            }
            if (hwnd == ptr2)
            {
                throw new ArgumentException(Utils.GetResourceString("ProcessNotFound", title));
            }
            AppActivateHelper(hwnd);
        }

        /// <summary>
        /// Runs an executable program and returns an integer containing the program's process ID if it is still running.
        /// </summary>
        /// <param name="pathname">Required. String. Name of the program to execute, together with any required arguments and command-line switches. PathName can also include the drive and the directory path or folder.</param>
        /// <returns>Integer value</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static int Shell(string pathName)
        {
            return Shell(pathName, AppWinStyle.MinimizedFocus, false, -1);
        }

        /// <summary>
        /// Runs an executable program and returns an integer containing the program's process ID if it is still running.
        /// </summary>
        /// <param name="pathname">Required. String. Name of the program to execute, together with any required arguments and command-line switches. PathName can also include the drive and the directory path or folder.</param>
        /// <param name="style">Optional. AppWinStyle. A value chosen from the AppWinStyle Enumeration specifying the style of the window in which the program is to run.</param>
        /// <returns>Integer value</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static int Shell(string pathName, AppWinStyle style)
        {
            return Shell(pathName, style = AppWinStyle.MinimizedFocus, false, -1);
        }

        /// <summary>
        /// Runs an executable program and returns an integer containing the program's process ID if it is still running.
        /// </summary>
        /// <param name="pathname">Required. String. Name of the program to execute, together with any required arguments and command-line switches. PathName can also include the drive and the directory path or folder.</param>
        /// <param name="style">Optional. AppWinStyle. A value chosen from the AppWinStyle Enumeration specifying the style of the window in which the program is to run.</param>
        /// <param name="wait">Optional. Boolean. A value indicating whether the Shell function should wait for completion of the program. If Wait is omitted, Shell uses False.</param>
        /// <returns>Integer value</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static int Shell(string pathName, AppWinStyle style, bool wait)
        {
            return Shell(pathName, style = AppWinStyle.MinimizedFocus, wait = false, -1);
        }

        /// <summary>
        /// Runs an executable program and returns an integer containing the program's process ID if it is still running.
        /// </summary>
        /// <param name="pathname">Required. String. Name of the program to execute, together with any required arguments and command-line switches. PathName can also include the drive and the directory path or folder.</param>
        /// <param name="style">Optional. AppWinStyle. A value chosen from the AppWinStyle Enumeration specifying the style of the window in which the program is to run.</param>
        /// <param name="wait">Optional. Boolean. A value indicating whether the Shell function should wait for completion of the program. If Wait is omitted, Shell uses False.</param>
        /// <param name="timeout">Optional. Integer. The number of milliseconds to wait for completion if Wait is True.</param>
        /// <returns>Integer value</returns>
        [SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static int Shell(string pathName, AppWinStyle style, bool wait, int timeout)
        {
            int dwProcessId;
            var lpStartupInfo = new NativeTypes.STARTUPINFO();
            var lpProcessInformation = new NativeTypes.PROCESS_INFORMATION();
            try
            {
                new UIPermission(UIPermissionWindow.AllWindows).Demand();
            }
            catch (Exception)
            {
                throw;
            }
            if ((style < AppWinStyle.Hide) || (style > ((AppWinStyle)9)))
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Style"));
            }
            NativeMethods.GetStartupInfo(lpStartupInfo);
            lpStartupInfo.dwFlags = 1;
            lpStartupInfo.wShowWindow = (int)style;
            string lpApplicationName = null;
            string lpCurrentDirectory = null;
            if (NativeMethods.CreateProcess(ref lpApplicationName, ref pathName, null, null, false, 0x20, IntPtr.Zero, ref lpCurrentDirectory, lpStartupInfo, lpProcessInformation) == 0)
            {
                throw ExceptionUtils.VbMakeException(0x35);
            }

            NativeMethods.CloseHandle(lpProcessInformation.hThread);
            if (wait)
            {
                dwProcessId = NativeMethods.WaitForSingleObject(lpProcessInformation.hProcess, timeout) == 0 ? 0 : lpProcessInformation.dwProcessId;
            }

            else
            {
                dwProcessId = NativeMethods.WaitForInputIdle(lpProcessInformation.hProcess, 0x2710) == 0 ? 0 : lpProcessInformation.dwProcessId;


            }
            int closeHandle = NativeMethods.CloseHandle(lpProcessInformation.hProcess);

            if (closeHandle == 0 && dwProcessId == 0)
            {
                return 0;
            }
            return dwProcessId;
        }

        /// <summary>
        /// Activates an application that is already running.
        /// </summary>
        /// <param name="hwndApp"></param>
        private static void AppActivateHelper(IntPtr hwndApp)
        {
            int num = 0;
            try
            {
                //Controls the permissions related to user interfaces and the Clipboard.
                //Forces a SecurityException at run time if all callers higher in the call stack have not been granted the permission specified by the current instance. 
                new UIPermission(UIPermissionWindow.AllWindows).Demand();
            }
            catch (Exception)
            {
                throw;
            }

            //check if the application window is not visible or is not enabled
            if (!SafeNativeMethods.IsWindowEnabled(hwndApp) || !SafeNativeMethods.IsWindowVisible(hwndApp))
            {
                IntPtr ptr2 = default(IntPtr);

                //get the window handle
                IntPtr window = NativeMethods.GetWindow(hwndApp, 0);

                //do while window handle is empty
                while (window != ptr2)
                {
                    if (NativeMethods.GetWindow(window, 4) == hwndApp)
                    {
                        //check if the application window is visible and enabled
                        if (SafeNativeMethods.IsWindowEnabled(window) && SafeNativeMethods.IsWindowVisible(window))
                        {
                            break;
                        }
                        hwndApp = window;

                        //set window pointer
                        window = NativeMethods.GetWindow(hwndApp, 0);
                    }
                    //set window pointer
                    window = NativeMethods.GetWindow(window, 2);
                }

                //if the window handle is empty
                if (window == ptr2)
                {
                    throw new ArgumentException(Utils.GetResourceString("ProcessNotFound"));
                }

                //set application handle
                hwndApp = window;
            }
            //Attaches or detaches the input processing mechanism of one thread to that of another thread.
            if (NativeMethods.AttachThreadInput(0, SafeNativeMethods.GetWindowThreadProcessId(hwndApp, ref num), 1) != 0)
            {
                //Sets the window foreground
                if (NativeMethods.SetForegroundWindow(hwndApp) != 0)
                {
                    //Sets focus on the window
                    NativeMethods.SetFocus(hwndApp);
                }

                //Detaches the input processing mechanism of one thread to that of another thread.
                NativeMethods.AttachThreadInput(0, SafeNativeMethods.GetWindowThreadProcessId(hwndApp, ref num), 0);
            }

        }

        /// <summary>
        /// Returns the string associated with an operating-system environment variable.
        /// </summary>
        /// <param name="expression">Required. Expression that evaluates either a string containing the name of an environment variable, or an integer corresponding to the numeric order of an environment string in the environment-string table.</param>
        /// <returns>Returns the string associated with an operating-system environment variable. </returns>
        public static string Environ(string expression)
        {
            expression = StringsEx.Trim(expression);
            if (expression.Length == 0)
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Expression"));
            }
            return Environment.GetEnvironmentVariable(expression);
        }

        /// <summary>
        /// Returns the string associated with an operating-system environment variable.
        /// </summary>
        /// <param name="expression">Required. Expression that evaluates either a string containing the name of an environment variable, or an integer corresponding to the numeric order of an environment string in the environment-string table.</param>
        /// <returns>Returns the string associated with an operating-system environment variable. </returns>
        [EnvironmentPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
        public static string Environ(int expression)
        {
            int num = expression;
            if ((num <= 0) || (num > 0xff))
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_Range0toFF1", "Expression"));
            }
            SortedList sortedEnvList = _s_SortedEnvList;
            if (sortedEnvList == null)
            {
                sortedEnvList = new SortedList(Environment.GetEnvironmentVariables());
                _s_SortedEnvList = sortedEnvList;
            }
            if (num > sortedEnvList.Count)
            {
                return String.Empty;
            }
            string str2 = sortedEnvList.GetKey(num - 1).ToString();
            string str3 = sortedEnvList.GetByIndex(num - 1).ToString();
            return (str2 + "=" + str3);
        }

        /// <summary>
        /// Sounds a tone through the computer's speaker.
        /// </summary>
        public static void Beep()
        {
            try
            {
                new UIPermission(UIPermissionWindow.SafeSubWindows).Demand();
            }
            catch (ArgumentException)
            {
                try
                {
                    new UIPermission(UIPermissionWindow.SafeTopLevelWindows).Demand();
                }
                catch (ArgumentException)
                {
                    return;
                }
            }
            NativeMethods.MessageBeep(0);
        }

        /// <summary>
        /// Get Title From Assembly
        /// </summary>
        /// <param name="callingAssembly">Assembly object.</param>
        /// <returns>String value.</returns>
        public static string GetTitleFromAssembly(Assembly callingAssembly)
        {
            if (callingAssembly == null)
            {
                throw new ArgumentNullException("callingAssembly");
            }
            try
            {
                return callingAssembly.GetName().Name;
            }
            catch (SecurityException)
            {
                string fullName = callingAssembly.FullName;
                int index = fullName.IndexOf(',');
                if (index >= 0)
                {
                    return fullName.Substring(0, index);
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Selects and returns a value from a list of arguments.
        /// </summary>
        /// <param name="index">Index of a value from the array</param>
        /// <param name="choice">Array of objects</param>
        /// <returns>The chosen object</returns>
        public static object Choose(double index, params object[] choice)
        {
            if (choice == null)
            {
                throw new ArgumentNullException("choice");
            }

            int _index = (int)Math.Round((double)(ConversionEx.Fix(index) - 1.0));
            if (choice.Rank != 1)
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_RankEQOne1", "Choice"));
            }
            if ((_index >= 0) && (_index <= choice.GetUpperBound(0)))
            {
                return choice[_index];
            }
            return null;
        }

        /// <summary>
        /// Returns the first value or expression in a list that is true
        /// </summary>
        /// <param name="varExpr">List of values or expressions.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// 
        public static object Switch(params object[] varExpr)
        {
            // Param array must contain values
            if (varExpr == null || varExpr.Length == 0)
            {
                throw new ArgumentNullException("varExpr");
            }

            // Number of elements must be even
            if (varExpr.Length % 2 == 0)
            {
                throw new ArgumentException("Invalid number of arguments");
            }


            for (int i = 0; i < varExpr.Length; i+=2)
            {
                bool boolVar;
                // Find first true expression
                if (bool.TryParse(varExpr[i].ToString(), out boolVar))
                {
                    if (boolVar)
                    {
                        // Return value of first "true" expression
                        return varExpr[i + 1];
                    }
                }

            }

            throw new ArgumentNullException();
        }


        #region MessageBox

        /// <summary>
        /// Displays a message in a dialog box, waits for the user to click a button, and then returns an integer indicating which button the user clicked.
        /// </summary>
        /// <param name="prompt">Required. String expression displayed as the message in the dialog box.</param>
        /// <returns>Constant depending on the user's click</returns>
        public static MsgBoxResult MsgBox(object prompt)
        {
            return MsgBox(prompt, MsgBoxStyle.OkOnly, null);
        }

        /// <summary>
        /// Displays a message in a dialog box, waits for the user to click a button, and then returns an integer indicating which button the user clicked.
        /// </summary>
        /// <param name="prompt">Required. String expression displayed as the message in the dialog box.</param>
        /// <param name="buttons">Optional. Numeric expression that is the sum of values specifying the number and type of buttons to display, the icon style to use, the identity of the default button, and the modality of the message box.</param>
        /// <returns>Constant depending on the user's click</returns>
        public static MsgBoxResult MsgBox(object prompt, MsgBoxStyle buttons)
        {
            return MsgBox(prompt, buttons, null);
        }

        /// <summary>
        /// Displays a message in a dialog box, waits for the user to click a button, and then returns an integer indicating which button the user clicked.
        /// </summary>
        /// <param name="prompt">Required. String expression displayed as the message in the dialog box.</param>
        /// <param name="buttons">Optional. Numeric expression that is the sum of values specifying the number and type of buttons to display, the icon style to use, the identity of the default button, and the modality of the message box.</param>
        /// <param name="title">Optional. String expression displayed in the title bar of the dialog box.</param>
        /// <returns>Constant depending on the user's click</returns>
        public static MsgBoxResult MsgBox(object prompt, MsgBoxStyle buttons, object title)
        {
            MsgBoxResult mr = new MsgBoxResult();
            IVbHost vBHost = HostServices.VBHost;
            if (vBHost != null)
            {
                vBHost.GetParentWindow();
            }
            if (((((int)buttons & 15) > (int)MsgBoxStyle.RetryCancel) || (((int)buttons & 240) > (int)MsgBoxStyle.Information)) || (((int)buttons & 0xf00) > (int)MsgBoxStyle.DefaultButton3))
            {
                buttons = MsgBoxStyle.OkOnly;
            }
            try
            {
                if (prompt != null)
                {
                    StringType.FromObject(prompt);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "Prompt", "String"));
            }
            try
            {
                if (title == null)
                {
                    if (vBHost == null)
                    {
                        string name = Assembly.GetCallingAssembly().GetName().Name;
                    }
                    else
                    {
                        vBHost.GetWindowTitle();
                    }
                }
                else
                {
                    StringType.FromObject(title);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "Title", "String"));
            }
            // return (MsgBoxResult) MessageBox.Show(parentWindow, str, name, ((MessageBoxButtons)Buttons) & ((MessageBoxButtons)15), ((MessageBoxIcon)Buttons) & ((MessageBoxIcon)240), ((MessageBoxDefaultButton)Buttons) & ((MessageBoxDefaultButton)0xf00), ((MessageBoxOptions)Buttons) & ((MessageBoxOptions)(-4096)));
            return mr;
        }


        #endregion
    }
}
