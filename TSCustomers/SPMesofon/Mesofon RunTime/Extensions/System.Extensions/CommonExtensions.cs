﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public static class CommonExtensions
    {
        /// <summary>
        /// Disposes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public static void Dispose(object item)
        {
            IDisposable disposableItem = item as IDisposable;
            if (item != null)
            {
                disposableItem.Dispose();
            }
        }
    }
}
