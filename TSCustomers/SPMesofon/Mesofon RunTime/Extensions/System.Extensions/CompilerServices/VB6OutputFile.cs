using System.ComponentModel;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// VB6OutputFile class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal class VB6OutputFile : VB6File
	{
		// Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		public VB6OutputFile()
		{
		}

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="fileName">The name of the file</param>
		/// <param name="share">Indicates how to open a file when calling file-access functions</param>
		/// <param name="fAppend">Open the file to append to it</param>
		public VB6OutputFile(string fileName, FileSystem.OpenShare share, bool fAppend) : base(fileName, FileSystem.OpenAccess.Write, share, -1)
		{
			base.m_fAppend = fAppend;
		}

		/// <summary>
		/// returns true
		/// </summary>
		/// <returns>Boolean value</returns>
		public override bool CanWrite()
		{
			return true;
		}

		/// <summary>
		/// Returns a Boolean value True when the end of a file opened for Random or sequential Input has been reached.
		/// </summary>
		/// <returns>True</returns>
		public override bool EOF()
		{
			return true;
		}

		/// <summary>
		/// Get the opening mode
		/// </summary>
		/// <returns>OpenMode enum</returns>
		public override FileSystem.OpenMode GetMode()
		{
			if (base.m_fAppend)
			{
				return FileSystem.OpenMode.Append;
			}
			return FileSystem.OpenMode.Output;
		}
	}
}
