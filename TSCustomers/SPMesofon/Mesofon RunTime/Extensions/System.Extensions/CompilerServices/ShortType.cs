using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Contains method to work with short integers
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	public sealed class ShortType
	{
		private ShortType()
		{

		}

		// Methods

		/// <summary>
		/// Converts the value of the specified decimal number to an equivalent 16-bit signed integer.
		/// </summary>
		/// <param name="valueInterface">IConvertible value</param>
		/// <returns>Short value.</returns>
		private static short DecimalToShort(IConvertible valueInterface)
		{
			return Convert.ToInt16(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts the value of the specified object to a 16-bit signed integer.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <returns>Short value.</returns>
		public static short FromObject(object value)
		{
			if (value == null)
			{
				return 0;
			}
			
			IConvertible valueInterface = value as IConvertible;
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return (short)((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (byte)value;
						}
						return valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return (short)value;
						}
						return valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return (short)((int)value);
						}
						return (short)valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return (short)((long)value);
						}
						return (short)valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return (short)Math.Round((double)((float)value));
						}
						return (short)Math.Round((double)valueInterface.ToSingle(null));

					case TypeCode.Double:
						if (value is double)
						{
							return (short)Math.Round((double)value);
						}
						return (short)Math.Round(valueInterface.ToDouble(null));

					case TypeCode.Decimal:
						return DecimalToShort(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null));
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Short"));
		}

		/// <summary>
		/// Converts the specified string representation of a number to an equivalent 16-bit signed integer.
		/// </summary>
		/// <param name="value">A string that contains the number to convert.</param>
		/// <returns>Short value.</returns>
		public static short FromString(string value)
		{
			short num;
			if (value == null)
			{
				return 0;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return (short)num2;
				}
				num = (short)Math.Round(DoubleType.Parse(value));
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Short"), exception);
			}
			return num;
		}
	}
}
