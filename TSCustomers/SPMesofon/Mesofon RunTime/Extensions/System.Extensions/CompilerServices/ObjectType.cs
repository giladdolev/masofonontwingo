using System.Globalization;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// ObjectType class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class ObjectType
	{
		private ObjectType()
		{

		}

		// Fields
		private static readonly VType[][] _widerType = {
			new[] {
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_bool,
				VType.t_bool,
				VType.t_i2,
				VType.t_i4,
				VType.t_i8,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_bool,
				VType.t_ui1,
				VType.t_i2,
				VType.t_i4,
				VType.t_i8,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_i2,
				VType.t_i2,
				VType.t_i2,
				VType.t_i4,
				VType.t_i8,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_i4,
				VType.t_i4,
				VType.t_i4,
				VType.t_i4,
				VType.t_i8,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_i8,
				VType.t_i8,
				VType.t_i8,
				VType.t_i8,
				VType.t_i8,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_dec,
				VType.t_dec,
				VType.t_dec,
				VType.t_dec,
				VType.t_dec,
				VType.t_dec,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_r4,
				VType.t_r4,
				VType.t_r4,
				VType.t_r4,
				VType.t_r4,
				VType.t_r4,
				VType.t_r4,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_bad,
				VType.t_r8,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_char,
				VType.t_str,
				VType.t_bad,
			},
			new[] {
				VType.t_bad,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_r8,
				VType.t_str,
				VType.t_str,
				VType.t_date,
			},
			new[] {
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_bad,
				VType.t_date,
				VType.t_date,
			},
		};



		// Methods

		/// <summary>
		/// Adds two bytes
		/// </summary>
		/// <param name="i1">Byte one</param>
		/// <param name="i2">Byte two</param>
		/// <returns>Sum of two bytes</returns>
		private static object AddByte(byte i1, byte i2)
		{
			short num = (short)(i1 + i2);
			if ((num >= 0) && (num <= 0xff))
			{
				return (byte)num;
			}
			return num;
		}

		/// <summary>
		/// Adds two decimal values
		/// </summary>
		/// <param name="conv1">First decimal value</param>
		/// <param name="conv2">Second decimal value</param>
		/// <returns>Sum of two decimal numbers</returns>
		private static object AddDecimal(IConvertible conv1, IConvertible conv2)
		{
			decimal num = default(decimal);
			if (conv1 != null)
			{
				num = conv1.ToDecimal(null);
			}
			decimal num2 = conv2.ToDecimal(null);
			try
			{
				return decimal.Add(num, num2);
			}
			catch (OverflowException)
			{
				return (Convert.ToDouble(num) + Convert.ToDouble(num2));
			}
		}





		/// <summary>
		/// Adds two double values
		/// </summary>
		/// <param name="d1">Double first value</param>
		/// <param name="d2">Double second value</param>
		/// <returns>Sum of two double values</returns>
		private static object AddDouble(double d1, double d2)
		{
			return (d1 + d2);
		}

		/// <summary>
		/// Adds two short values
		/// </summary>
		/// <param name="i1">Short first value</param>
		/// <param name="i2">Short second value</param>
		/// <returns>Sum of two short values</returns>
		private static object AddInt16(short i1, short i2)
		{
			int num = i1 + i2;
			if ((num >= -32768) && (num <= 0x7fff))
			{
				return (short)num;
			}
			return num;
		}

		/// <summary>
		/// Adds two int values
		/// </summary>
		/// <param name="i1">Integer first value</param>
		/// <param name="i2">Integer second value</param>
		/// <returns>Sum of two integer values</returns>
		private static object AddInt32(int i1, int i2)
		{
			long num = i1 + i2;
			if ((num >= -2147483648L) && (num <= 0x7fffffffL))
			{
				return (int)num;
			}
			return num;
		}

		/// <summary>
		/// Adds two int values
		/// </summary>
		/// <param name="i1">Integer first value</param>
		/// <param name="i2">Integer second value</param>
		/// <returns>Sum of two integer values</returns>
		private static object AddInt64(long i1, long i2)
		{
			try
			{
				return (i1 + i2);
			}
			catch (OverflowException)
			{
				return decimal.Add(new decimal(i1), new decimal(i2));
			}
		}

		/// <summary>
		/// Adds two objects
		/// </summary>
		/// <param name="i1">First object</param>
		/// <param name="i2">Second object</param>
		/// <returns>Sum of two objects</returns>
		public static object AddObj(object o1, object o2)
		{
			object obj2 = default(object);
			IConvertible conv = null;
			TypeCode empty;
			TypeCode typeCode;
			
			IConvertible convertible = o1 as IConvertible;
			if (convertible != null)
			{
				conv = convertible;
			}
			if (conv == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			
			IConvertible convertible2 = o2 as IConvertible;
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			if (((empty == TypeCode.Object) && (o1 is char[])) && (((typeCode == TypeCode.String) || (typeCode == TypeCode.Empty)) || ((typeCode == TypeCode.Object) && (o2 is char[]))))
			{
				o1 = new string(CharArrayType.FromObject(o1));
				conv = (IConvertible)o1;
				empty = TypeCode.String;
			}
			if (((typeCode == TypeCode.Object) && (o2 is char[])) && ((empty == TypeCode.String) || (empty == TypeCode.Empty)))
			{
				o2 = new string(CharArrayType.FromObject(o2));
				convertible2 = (IConvertible)o2;
				typeCode = TypeCode.String;
			}
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case TypeCode.Empty:
					return 0;

				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return o2;

				case TypeCode.String:
				case ((TypeCode)0x38):
					return o2;

				case ((TypeCode)0x39):
				case ((TypeCode)0x72):
				case ((TypeCode)0x85):
				case ((TypeCode)0xab):
				case ((TypeCode)0xd1):
				case ((TypeCode)0xf7):
				case ((TypeCode)0x10a):
				case ((TypeCode)0x11d):
					return o1;

				case ((TypeCode)60):
					return AddInt16((short)ToVBBool(conv), (short)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return AddInt16((short)ToVBBool(conv), convertible2.ToInt16(null));

				case ((TypeCode)0x42):
					return AddInt32(ToVBBool(conv), convertible2.ToInt32(null));

				case ((TypeCode)0x44):
					return AddInt64((long)ToVBBool(conv), convertible2.ToInt64(null));

				case ((TypeCode)70):
					return AddSingle((float)ToVBBool(conv), convertible2.ToSingle(null));

				case ((TypeCode)0x47):
					return AddDouble((double)ToVBBool(conv), convertible2.ToDouble(null));

				case ((TypeCode)0x48):
					return AddDecimal(ToVBBoolConv(conv), convertible2);

				case ((TypeCode)0x4b):
				case ((TypeCode)0x159):
					return AddString(conv, empty, convertible2, typeCode);

				case ((TypeCode)80):
				case ((TypeCode)0x5e):
				case ((TypeCode)320):
				case ((TypeCode)0x142):
				case ((TypeCode)0x15a):
				case ((TypeCode)0x166):
				case ((TypeCode)360):
					return (StringType.FromObject(o1) + StringType.FromObject(o2));

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return AddInt16(conv.ToInt16(null), (short)ToVBBool(convertible2));

				case ((TypeCode)120):
					return AddByte(conv.ToByte(null), convertible2.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return AddInt16(conv.ToInt16(null), convertible2.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return AddInt32(conv.ToInt32(null), convertible2.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x90):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
					return AddInt64(conv.ToInt64(null), convertible2.ToInt64(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return AddSingle(conv.ToSingle(null), convertible2.ToSingle(null));

				case ((TypeCode)0x80):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return AddDouble(conv.ToDouble(null), convertible2.ToDouble(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return AddDecimal(conv, convertible2);

				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
					return AddString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0xae):
					return AddInt32(conv.ToInt32(null), ToVBBool(convertible2));

				case ((TypeCode)0xd4):
					return AddInt64(conv.ToInt64(null), (long)ToVBBool(convertible2));

				case ((TypeCode)250):
					return AddSingle(conv.ToSingle(null), (float)ToVBBool(convertible2));

				case ((TypeCode)0x10d):
					return AddDouble(conv.ToDouble(null), (double)ToVBBool(convertible2));

				case ((TypeCode)0x120):
					return AddDecimal(conv, ToVBBoolConv(convertible2));

				case ((TypeCode)0x156):
				case ((TypeCode)0x158):
					return o1;

				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return AddString(conv, empty, convertible2, typeCode);
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}

		/// <summary>
		/// Adds two single values
		/// </summary>
		/// <param name="f1">First single value</param>
		/// <param name="f2">Second single value</param>
		/// <returns>Sum of two single values</returns>
		private static object AddSingle(float f1, float f2)
		{
			double d = f1 + f2;
			if (((d > 3.40282346638529E+38) || (d < -3.40282346638529E+38)) && (!double.IsInfinity(d) || (!float.IsInfinity(f1) && !float.IsInfinity(f2))))
			{
				return d;
			}
			return (float)d;
		}

		/// <summary>
		/// Adds two strings
		/// </summary>
		/// <param name="conv1">First object</param>
		/// <param name="tc1">Type code of the first object</param>
		/// <param name="conv2">Second object</param>
		/// <param name="tc2">Type code of the second object</param>
		/// <returns>Sum of two strings</returns>
		private static object AddString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return (num + num2);
		}



		/// <summary>
		/// Performs a bitwise And operation.
		/// </summary>
		/// <param name="obj1">Required. Any Boolean or numeric expression.</param>
		/// <param name="obj2">Required. Any Boolean or numeric expression.</param>
		/// <returns>1 if both obj1 and obj2 evaluate to 1; otherwise, 0.</returns>
		public static object BitAndObj(object obj1, object obj2)
		{
			Type type = default(Type);
			bool isEnum = default(bool);
			Type type2 = default(Type);
			bool flag2 = default(bool);
			if ((obj1 == null) && (obj2 == null))
			{
				return 0;
			}
			if (obj1 != null)
			{
				type = obj1.GetType();
				isEnum = type.IsEnum;
			}
			if (obj2 != null)
			{
				type2 = obj2.GetType();
				flag2 = type2.IsEnum;
			}
			switch (GetWidestType(obj1, obj2, false)) {
				case TypeCode.Boolean:
					if (type != type2)
					{
						return (short)(ShortType.FromObject(obj1) & ShortType.FromObject(obj2));
					}
					return (BooleanType.FromObject(obj1) & BooleanType.FromObject(obj2));

				case TypeCode.Byte:
					{
						byte num = (byte)(ByteType.FromObject(obj1) & ByteType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num);
						}
						return num;
					}
				case TypeCode.Int16:
					{
						short num2 = (short)(ShortType.FromObject(obj1) & ShortType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num2);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num2);
						}
						return num2;
					}
				case TypeCode.Int32:
					{
						int num3 = IntegerType.FromObject(obj1) & IntegerType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num3);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num3);
						}
						return num3;
					}
				case TypeCode.Int64:
					{
						long num4 = LongType.FromObject(obj1) & LongType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num4);
							}
							if (flag2)
							{
								return Enum.ToObject(type2, num4);
							}
							break;
						}
						return num4;
					}
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
				case TypeCode.String:
					return (LongType.FromObject(obj1) & LongType.FromObject(obj2));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", Utils.VBFriendlyName(obj1), Utils.VBFriendlyName(obj2)));
		}

		/// <summary>
		/// Performs a bitwise Or operation.
		/// </summary>
		/// <param name="obj1">Required. Any Boolean or numeric expression.</param>
		/// <param name="obj2">Required. Any Boolean or numeric expression.</param>
		/// <returns>0 if both obj1 and obj2 evaluate to 0; otherwise, 1.</returns>
		public static object BitOrObj(object obj1, object obj2)
		{
			Type type = default(Type);
			bool isEnum = default(bool);
			Type type2 = default(Type);
			bool flag2 = default(bool);
			if ((obj1 == null) && (obj2 == null))
			{
				return 0;
			}
			if (obj1 != null)
			{
				type = obj1.GetType();
				isEnum = type.IsEnum;
			}
			if (obj2 != null)
			{
				type2 = obj2.GetType();
				flag2 = type2.IsEnum;
			}
			switch (GetWidestType(obj1, obj2, false)) {
				case TypeCode.Boolean:
					if (type != type2)
					{
						return (short)(ShortType.FromObject(obj1) | ShortType.FromObject(obj2));
					}
					return (BooleanType.FromObject(obj1) | BooleanType.FromObject(obj2));

				case TypeCode.Byte:
					{
						byte num = (byte)(ByteType.FromObject(obj1) | ByteType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num);
						}
						return num;
					}
				case TypeCode.Int16:
					{
						short num2 = (short)(ShortType.FromObject(obj1) | ShortType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num2);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num2);
						}
						return num2;
					}
				case TypeCode.Int32:
					{
						int num3 = IntegerType.FromObject(obj1) | IntegerType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num3);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num3);
						}
						return num3;
					}
				case TypeCode.Int64:
					{
						long num4 = LongType.FromObject(obj1) | LongType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num4);
							}
							if (flag2)
							{
								return Enum.ToObject(type2, num4);
							}
							break;
						}
						return num4;
					}
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
				case TypeCode.String:
					return (LongType.FromObject(obj1) | LongType.FromObject(obj2));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", Utils.VBFriendlyName(obj1), Utils.VBFriendlyName(obj2)));
		}

		/// <summary>
		/// Performs an Xor operation.
		/// </summary>
		/// <param name="obj1">Required. Any Boolean or numeric expression.</param>
		/// <param name="obj2">Required. Any Boolean or numeric expression.</param>
		/// <returns>A numeric value that represents the bitwise exclusion (exclusive bitwise disjunction) of two numeric bit patterns.</returns>
		public static object BitXorObj(object obj1, object obj2)
		{
			Type type = default(Type);
			bool isEnum = default(bool);
			Type type2 = default(Type);
			bool flag2 = default(bool);
			if ((obj1 == null) && (obj2 == null))
			{
				return 0;
			}
			if (obj1 != null)
			{
				type = obj1.GetType();
				isEnum = type.IsEnum;
			}
			if (obj2 != null)
			{
				type2 = obj2.GetType();
				flag2 = type2.IsEnum;
			}
			switch (GetWidestType(obj1, obj2, false)) {
				case TypeCode.Boolean:
					if (type != type2)
					{
						return (short)(ShortType.FromObject(obj1) ^ ShortType.FromObject(obj2));
					}
					return (BooleanType.FromObject(obj1) ^ BooleanType.FromObject(obj2));

				case TypeCode.Byte:
					{
						byte num = (byte)(ByteType.FromObject(obj1) ^ ByteType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num);
						}
						return num;
					}
				case TypeCode.Int16:
					{
						short num2 = (short)(ShortType.FromObject(obj1) ^ ShortType.FromObject(obj2));
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num2);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num2);
						}
						return num2;
					}
				case TypeCode.Int32:
					{
						int num3 = IntegerType.FromObject(obj1) ^ IntegerType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num3);
							}
							if (!flag2)
							{
								break;
							}
							return Enum.ToObject(type2, num3);
						}
						return num3;
					}
				case TypeCode.Int64:
					{
						long num4 = LongType.FromObject(obj1) ^ LongType.FromObject(obj2);
						if (((!isEnum || !flag2) || (type == type2)) && (isEnum && flag2))
						{
							if (isEnum)
							{
								return Enum.ToObject(type, num4);
							}
							if (flag2)
							{
								return Enum.ToObject(type2, num4);
							}
							break;
						}
						return num4;
					}
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
				case TypeCode.String:
					return (LongType.FromObject(obj1) ^ LongType.FromObject(obj2));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", Utils.VBFriendlyName(obj1), Utils.VBFriendlyName(obj2)));
		}




		/// <summary>
		/// Divides two decimal values
		/// </summary>
		/// <param name="conv1">First decimal object</param>
		/// <param name="conv2">Second decimal object</param>
		/// <returns>Devision result</returns>
		private static object DivDecimal(IConvertible conv1, IConvertible conv2)
		{
			decimal num = default(decimal);
			decimal num2 = default(decimal);
			if (conv1 != null)
			{
				num = conv1.ToDecimal(null);
			}
			if (conv2 != null)
			{
				num2 = conv2.ToDecimal(null);
			}
			try
			{
				return decimal.Divide(num, num2);
			}
			catch (OverflowException)
			{
				return (Convert.ToSingle(num) / Convert.ToSingle(num2));
			}
		}

		/// <summary>
		/// Divides two double values
		/// </summary>
		/// <param name="d1">First double value</param>
		/// <param name="d2">Second double value</param>
		/// <returns>Division result</returns>
		private static object DivDouble(double d1, double d2)
		{
			return (d1 / d2);
		}

		/// <summary>
		/// Divides two objects
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		public static object DivObj(object o1, object o2)
		{
			object obj2 = default(object);
			TypeCode empty;
			TypeCode typeCode;
			
			IConvertible convertible = o1 as IConvertible;
			if (convertible == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = convertible.GetTypeCode();
			}
			
			IConvertible convertible2 = o2 as IConvertible;
			
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case TypeCode.Empty:
					return DivDouble(0.0, 0.0);

				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return DivDouble(0.0, convertible2.ToDouble(null));

				case TypeCode.String:
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)0x39):
					return DivDouble((double)ToVBBool(convertible), 0.0);

				case ((TypeCode)60):
					return DivDouble((double)ToVBBool(convertible), (double)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
				case ((TypeCode)0x42):
				case ((TypeCode)0x44):
				case ((TypeCode)0x47):
					return DivDouble((double)ToVBBool(convertible), convertible2.ToDouble(null));

				case ((TypeCode)70):
					return DivSingle((float)ToVBBool(convertible), convertible2.ToSingle(null));

				case ((TypeCode)0x48):
					return DivDecimal(ToVBBoolConv(convertible), (IConvertible)convertible2.ToDecimal(null));

				case ((TypeCode)0x4b):
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)0x72):
				case ((TypeCode)0x85):
				case ((TypeCode)0xab):
				case ((TypeCode)0xd1):
				case ((TypeCode)0xf7):
				case ((TypeCode)0x10a):
				case ((TypeCode)0x11d):
					return DivDouble(convertible.ToDouble(null), 0.0);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
				case ((TypeCode)0xae):
				case ((TypeCode)0xd4):
				case ((TypeCode)0x10d):
					return DivDouble(convertible.ToDouble(null), (double)ToVBBool(convertible2));

				case ((TypeCode)120):
				case ((TypeCode)0x79):
				case ((TypeCode)0x7b):
				case ((TypeCode)0x7d):
				case ((TypeCode)0x80):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
				case ((TypeCode)0x8e):
				case ((TypeCode)0x90):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return DivDouble(convertible.ToDouble(null), convertible2.ToDouble(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return DivSingle(convertible.ToSingle(null), convertible2.ToSingle(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return DivDecimal(convertible, convertible2);

				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)250):
					return DivSingle(convertible.ToSingle(null), (float)ToVBBool(convertible2));

				case ((TypeCode)0x120):
					return DivDecimal(convertible, ToVBBoolConv(convertible2));

				case ((TypeCode)0x156):
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)0x159):
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return DivString(convertible, empty, convertible2, typeCode);

				case ((TypeCode)360):
					return DivStringString(convertible.ToString(null), convertible2.ToString(null));
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}

		/// <summary>
		/// Divides two single values
		/// </summary>
		/// <param name="sng1">First single value</param>
		/// <param name="sng2">Second single value</param>
		/// <returns>Division result</returns>
		private static object DivSingle(float sng1, float sng2)
		{
			float f = sng1 / sng2;
			if (float.IsInfinity(f) && (!float.IsInfinity(sng1) && !float.IsInfinity(sng2)))
			{
				return (((double)sng1) / ((double)sng2));
			}
			return f;
		}

		/// <summary>
		/// Divides two string values
		/// </summary>
		/// <param name="conv1">First object</param>
		/// <param name="tc1">Type code of first object</param>
		/// <param name="conv2">Second object</param>
		/// <param name="tc2">Type code of second object</param>
		/// <returns>Division result</returns>
		private static object DivString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return (num / num2);
		}

		/// <summary>
		/// Divides two strings
		/// </summary>
		/// <param name="s1">First string</param>
		/// <param name="s2">Second string</param>
		/// <returns>Division result</returns>
		private static object DivStringString(string s1, string s2)
		{
			double num = default(double);
			double num2 = default(double);
			if (s1 != null)
			{
				num = DoubleType.FromString(s1);
			}
			if (s2 != null)
			{
				num2 = DoubleType.FromString(s2);
			}
			return (num / num2);
		}

		/// <summary>
		/// Converts object to primitive
		/// </summary>
		/// <param name="o">Input object</param>
		/// <returns>Converted object</returns>
		public static object GetObjectValuePrimitive(object o)
		{
			if (o == null)
			{
				return null;
			}
			
			IConvertible convertible = o as IConvertible;
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.Boolean:
						return convertible.ToBoolean(null);

					case TypeCode.Char:
						return convertible.ToChar(null);

					case TypeCode.SByte:
						return convertible.ToSByte(null);

					case TypeCode.Byte:
						return convertible.ToByte(null);

					case TypeCode.Int16:
						return convertible.ToInt16(null);

					case TypeCode.UInt16:
						return convertible.ToUInt16(null);

					case TypeCode.Int32:
						return convertible.ToInt32(null);

					case TypeCode.UInt32:
						return convertible.ToUInt32(null);

					case TypeCode.Int64:
						return convertible.ToInt64(null);

					case TypeCode.UInt64:
						return convertible.ToUInt64(null);

					case TypeCode.Single:
						return convertible.ToSingle(null);

					case TypeCode.Double:
						return convertible.ToDouble(null);

					case TypeCode.Decimal:
						return convertible.ToDecimal(null);

					case TypeCode.DateTime:
						return convertible.ToDateTime(null);

					case (TypeCode.DateTime | TypeCode.Object):
						return o;

					case TypeCode.String:
						return o;
				}
			}
			return o;
		}

		static internal TypeCode GetWidestType(object obj1, object obj2, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool isAdd)
		{
			
			TypeCode typeCode;
			TypeCode empty;
			
			IConvertible convertible = obj1 as IConvertible;
			IConvertible convertible2 = obj2 as IConvertible;
			
			
			if (convertible != null)
			{
				typeCode = convertible.GetTypeCode();
			}
			else if (obj1 == null)
			{
				typeCode = TypeCode.Empty;
			}
			else if ((obj1 is char[]) && (((Array)obj1).Rank == 1))
			{
				typeCode = TypeCode.String;
			}
			else
			{
				typeCode = TypeCode.Object;
			}
			if (convertible2 != null)
			{
				empty = convertible2.GetTypeCode();
			}
			else if (obj2 == null)
			{
				empty = TypeCode.Empty;
			}
			else if ((obj2 is char[]) && (((Array)obj2).Rank == 1))
			{
				empty = TypeCode.String;
			}
			else
			{
				empty = TypeCode.Object;
			}
			if (obj1 == null)
			{
				return empty;
			}
			if (obj2 == null)
			{
				return typeCode;
			}
			
			if (!isAdd || (((typeCode != TypeCode.DBNull) || (empty != TypeCode.String)) && ((typeCode != TypeCode.String) || (empty != TypeCode.DBNull))))
			{
				
				return TypeCodeFromVType(_widerType[(int)VTypeFromTypeCode(typeCode)][(int)VTypeFromTypeCode(empty)]);
			}
			return TypeCode.DBNull;
		}

		/// <summary>
		/// Divides two bytes
		/// </summary>
		/// <param name="d1">First byte</param>
		/// <param name="d2">Second byte</param>
		/// <returns>Division result</returns>
		private static object IDivideByte(byte d1, byte d2)
		{
			return (byte)(d1 / d2);
		}

		/// <summary>
		/// Divides two short values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		private static object IDivideInt16(short d1, short d2)
		{
			return (short)(d1 / d2);
		}

		/// <summary>
		/// Divides two integer values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		private static object IDivideInt32(int d1, int d2)
		{
			return (d1 / d2);
		}

		/// <summary>
		/// Divides two long values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		private static object IDivideInt64(long d1, long d2)
		{
			return (d1 / d2);
		}

		/// <summary>
		/// Divides two string values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		private static object IDivideString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			long num = 0;
			long num2 = 0;
			if (tc1 == TypeCode.String)
			{
				try
				{
					num = LongType.FromString(conv1.ToString(null));
				}
				catch (InvalidCastException)
				{
					ThrowNoValidOperator(conv1, conv2);
				}
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToInt64(null);
			}
			if (tc2 == TypeCode.String)
			{
				try
				{
					num2 = LongType.FromString(conv2.ToString(null));
				}
				catch (InvalidCastException)
				{
					ThrowNoValidOperator(conv1, conv2);
				}
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToInt64(null);
			}
			return (num / num2);
		}

		/// <summary>
		/// Divides two string values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Division result</returns>
		private static object IDivideStringString(string s1, string s2)
		{
			long num = 0;
			long num2 = 0;
			if (s1 != null)
			{
				num = LongType.FromString(s1);
			}
			if (s2 != null)
			{
				num2 = LongType.FromString(s2);
			}
			return (num / num2);
		}

		/// <summary>
		/// Divides two objects
		/// </summary>
		/// <param name="d1">First object</param>
		/// <param name="d2">Second object</param>
		/// <returns>Division result</returns>
		public static object IDivObj(object o1, object o2)
		{
			object obj2 = default(object);
			TypeCode empty;
			TypeCode typeCode;
			
			IConvertible conv = o1 as IConvertible;
			
			if (conv == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			IConvertible convertible2 = o2 as IConvertible;
			
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case (int)TypeCode.Empty:
					return IDivideInt32(0, 0);

				case TypeCode.Boolean:
					return IDivideInt64(0L, (long)ToVBBool(convertible2));

				case TypeCode.Byte:
					return IDivideByte(0, convertible2.ToByte(null));

				case TypeCode.Int16:
					return IDivideInt16(0, convertible2.ToInt16(null));

				case TypeCode.Int32:
					return IDivideInt32(0, convertible2.ToInt32(null));

				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return IDivideInt64(0L, convertible2.ToInt64(null));

				case TypeCode.String:
					return IDivideInt64(0L, LongType.FromString(convertible2.ToString(null)));

				case ((TypeCode)0x39):
					return IDivideInt16((short)ToVBBool(conv), 0);

				case ((TypeCode)60):
					return IDivideInt16((short)ToVBBool(conv), (short)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return IDivideInt16((short)ToVBBool(conv), convertible2.ToInt16(null));

				case ((TypeCode)0x42):
					return IDivideInt32(ToVBBool(conv), convertible2.ToInt32(null));

				case ((TypeCode)0x44):
				case ((TypeCode)70):
				case ((TypeCode)0x47):
				case ((TypeCode)0x48):
					return IDivideInt64((long)ToVBBool(conv), convertible2.ToInt64(null));

				case ((TypeCode)0x4b):
					return IDivideInt64((long)ToVBBool(conv), LongType.FromString(convertible2.ToString(null)));

				case ((TypeCode)0x72):
					return IDivideByte(conv.ToByte(null), 0);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return IDivideInt16(conv.ToInt16(null), (short)ToVBBool(convertible2));

				case ((TypeCode)120):
					return IDivideByte(conv.ToByte(null), convertible2.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return IDivideInt16(conv.ToInt16(null), convertible2.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return IDivideInt32(conv.ToInt32(null), convertible2.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x7f):
				case ((TypeCode)0x80):
				case ((TypeCode)0x81):
				case ((TypeCode)0x90):
				case ((TypeCode)0x92):
				case ((TypeCode)0x93):
				case ((TypeCode)0x94):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xba):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
				case ((TypeCode)0xde):
				case ((TypeCode)0xdf):
				case ((TypeCode)0xe0):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x105):
				case ((TypeCode)0x106):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)0x12a):
				case ((TypeCode)0x12b):
				case ((TypeCode)300):
					return IDivideInt64(conv.ToInt64(null), convertible2.ToInt64(null));

				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
					return IDivideString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0x85):
					return IDivideInt16(conv.ToInt16(null), 0);

				case ((TypeCode)0xab):
					return IDivideInt32(conv.ToInt32(null), 0);

				case ((TypeCode)0xae):
					return IDivideInt32(conv.ToInt32(null), ToVBBool(convertible2));

				case ((TypeCode)0xd1):
				case ((TypeCode)0xf7):
				case ((TypeCode)0x10a):
				case ((TypeCode)0x11d):
					return IDivideInt64(conv.ToInt64(null), 0L);

				case ((TypeCode)0xd4):
				case ((TypeCode)250):
				case ((TypeCode)0x10d):
				case ((TypeCode)0x120):
					return IDivideInt64(conv.ToInt64(null), (long)ToVBBool(convertible2));

				case ((TypeCode)0x156):
					return IDivideInt64(LongType.FromString(conv.ToString(null)), 0L);

				case ((TypeCode)0x159):
					return IDivideInt64(LongType.FromString(conv.ToString(null)), (long)ToVBBool(convertible2));

				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return IDivideInt64(LongType.FromString(conv.ToString(null)), convertible2.ToInt64(null));

				case ((TypeCode)360):
					return IDivideStringString(conv.ToString(null), convertible2.ToString(null));
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}

		private static object InternalNegObj(object obj, IConvertible conv, TypeCode tc)
		{
			switch (tc) {
				case TypeCode.Empty:
					return 0;

				case TypeCode.Boolean:
					if (obj is bool)
					{
						return -((short)((Convert.ToInt32(((bool)obj)) << 0x1f) >> 0x1f));
					}
					return -((short)((Convert.ToInt32(conv.ToBoolean(null)) << 0x1f) >> 0x1f));

				case TypeCode.Byte:
					if (obj is byte)
					{
						return (short)-((byte)obj);
					}
					return (short)-conv.ToByte(null);

				case TypeCode.Int16:
					int num5;
					if (obj is short)
					{
						num5 = 0 - ((short)obj);
					}
					else
					{
						num5 = 0 - conv.ToInt16(null);
					}
					if ((num5 >= -32768) && (num5 <= 0x7fff))
					{
						return (short)num5;
					}
					return num5;

				case TypeCode.Int32:
					long num6;
					if (obj is int)
					{
						num6 = 0L - ((int)obj);
					}
					else
					{
						num6 = 0L - conv.ToInt32(null);
					}
					if ((num6 >= -2147483648L) && (num6 <= 0x7fffffffL))
					{
						return (int)num6;
					}
					return num6;

				case TypeCode.Int64:
					try
					{
						if (obj is long)
						{
							return (0L - ((long)obj));
						}
						return (0L - conv.ToInt64(null));
					}
					catch (InvalidCastException)
					{
						return decimal.Negate(conv.ToDecimal(null));
					}
					catch (ArgumentException)
					{
						return decimal.Negate(conv.ToDecimal(null));
					}

				case TypeCode.Single:
					goto Label_01BD;

				case TypeCode.Double:
					if (obj is double)
					{
						return -((double)obj);
					}
					return -conv.ToDouble(null);

				case TypeCode.Decimal:
					break;

				case TypeCode.String:
					string strObj = obj as string;
					if (!String.IsNullOrEmpty(strObj))
					{
						return -DoubleType.FromString(strObj);
					}
					return -DoubleType.FromString(conv.ToString(null));

				default:
					throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_OneOperand", Utils.VBFriendlyName(obj)));
			}
			try
			{
				if (obj is decimal)
				{
					return decimal.Negate((decimal)obj);
				}
				return decimal.Negate(conv.ToDecimal(null));
			}
			catch (InvalidCastException)
			{
				return -conv.ToDouble(null);
			}
			catch (ArgumentException)
			{
				return -conv.ToDouble(null);
			}
			
			Label_01BD:
			if (obj is float)
			{
				return -((float)obj);
			}
			return -conv.ToSingle(null);
		}

		/// <summary>
		/// Checks if type is child of other type
		/// </summary>
		/// <param name="typSrc">Child type</param>
		/// <param name="typParent">Parent type</param>
		/// <returns>Boolean result</returns>
		static internal bool IsTypeOf(Type typSrc, Type typParent)
		{
			return ((typSrc == typParent) || typSrc.IsSubclassOf(typParent));
		}



		/// <summary>
		/// Compares two objects
		/// </summary>
		/// <param name="vLeft">First object</param>
		/// <param name="vRight">Second object</param>
		/// <param name="compareOption">Compare options</param>
		/// <returns>Result of comparison</returns>
		public static bool LikeObj(object vLeft, object vRight, StringType.CompareMethod compareOption)
		{
			return StringType.StrLike(StringType.FromObject(vLeft), StringType.FromObject(vRight), compareOption);
		}

		/// <summary>
		/// Calculates modulo from two bytes
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModByte(byte i1, byte i2)
		{
			return (byte)(i1 % i2);
		}

		/// <summary>
		/// Calculates modulo from two decimal numbers
		/// </summary>
		/// <param name="conv1">First value</param>
		/// <param name="conv2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModDecimal(IConvertible conv1, IConvertible conv2)
		{
			decimal num = default(decimal);
			decimal num2 = default(decimal);
			if (conv1 != null)
			{
				num = conv1.ToDecimal(null);
			}
			if (conv2 != null)
			{
				num2 = conv2.ToDecimal(null);
			}
			return decimal.Remainder(num, num2);
		}

		/// <summary>
		/// Calculates modulo from two double values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModDouble(double d1, double d2)
		{
			return (d1 % d2);
		}

		/// <summary>
		/// Calculates modulo from two short values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModInt16(short i1, short i2)
		{
			int num = i1 % i2;
			if ((num >= -32768) && (num <= 0x7fff))
			{
				return (short)num;
			}
			return num;
		}

		/// <summary>
		/// Calculates modulo from two integer values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModInt32(int i1, int i2)
		{
			long num = ((long)i1) % ((long)i2);
			if ((num >= -2147483648L) && (num <= 0x7fffffffL))
			{
				return (int)num;
			}
			return num;
		}

		/// <summary>
		/// Calculates modulo from two long values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModInt64(long i1, long i2)
		{
			try
			{
				return (i1 % i2);
			}
			catch (OverflowException)
			{
				decimal num = decimal.Remainder(new decimal(i1), new decimal(i2));
				if ((decimal.Compare(num, -9223372036854775808m) < 0) || (decimal.Compare(num, 9223372036854775807m) > 0))
				{
					return num;
				}
				return Convert.ToInt64(num);
			}
		}

		/// <summary>
		/// Calculates modulo from two objects
		/// </summary>
		/// <param name="o1">First value</param>
		/// <param name="o2">Second value</param>
		/// <returns>Modulo result</returns>
		public static object ModObj(object o1, object o2)
		{
			object obj2 = default(object);
			TypeCode typeCode;
			TypeCode empty;
			
			IConvertible convertible = o1 as IConvertible;
			
			IConvertible conv = o2 as IConvertible;
			
			if (convertible != null)
			{
				typeCode = convertible.GetTypeCode();
			}
			else if (o1 == null)
			{
				typeCode = TypeCode.Empty;
			}
			else
			{
				typeCode = TypeCode.Object;
			}
			if (conv != null)
			{
				empty = conv.GetTypeCode();
			}
			else
			{
				conv = null;
				if (o2 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			switch ((((int)typeCode * (int)(TypeCode.String | TypeCode.Object)) + empty)) {
				case TypeCode.Empty:
					return ModInt32(0, 0);

				case TypeCode.Boolean:
					return ModInt16(0, (short)ToVBBool(conv));

				case TypeCode.Byte:
					return ModByte(0, conv.ToByte(null));

				case TypeCode.Int16:
					return ModInt16(0, (short)ToVBBool(conv));

				case TypeCode.Int32:
					return ModInt32(0, conv.ToInt32(null));

				case TypeCode.Int64:
					return ModInt64(0L, conv.ToInt64(null));

				case TypeCode.Single:
					return ModSingle(0f, conv.ToSingle(null));

				case TypeCode.Double:
					return ModDouble(0.0, conv.ToDouble(null));

				case TypeCode.Decimal:
					return ModDecimal(null, conv);

				case TypeCode.String:
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)0x39):
					return ModInt16((short)ToVBBool(convertible), 0);

				case ((TypeCode)60):
					return ModInt16((short)ToVBBool(convertible), (short)ToVBBool(conv));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return ModInt16((short)ToVBBool(convertible), conv.ToInt16(null));

				case ((TypeCode)0x42):
					return ModInt32(ToVBBool(convertible), conv.ToInt32(null));

				case ((TypeCode)0x44):
					return ModInt64((long)ToVBBool(convertible), conv.ToInt64(null));

				case ((TypeCode)70):
					return ModSingle((float)ToVBBool(convertible), conv.ToSingle(null));

				case ((TypeCode)0x47):
					return ModDouble((double)ToVBBool(convertible), conv.ToDouble(null));

				case ((TypeCode)0x48):
					return ModDecimal(ToVBBoolConv(convertible), conv);

				case ((TypeCode)0x4b):
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)0x72):
					return ModByte(convertible.ToByte(null), 0);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return ModInt16(convertible.ToInt16(null), (short)ToVBBool(conv));

				case ((TypeCode)120):
					return ModByte(convertible.ToByte(null), conv.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return ModInt16(convertible.ToInt16(null), conv.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return ModInt32(convertible.ToInt32(null), conv.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x90):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
					return ModInt64(convertible.ToInt64(null), conv.ToInt64(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return ModSingle(convertible.ToSingle(null), conv.ToSingle(null));

				case ((TypeCode)0x80):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return ModDouble(convertible.ToDouble(null), conv.ToDouble(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return ModDecimal(convertible, conv);

				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)0x85):
					return ModInt16(convertible.ToInt16(null), 0);

				case ((TypeCode)0xab):
					return ModInt32(convertible.ToInt32(null), 0);

				case ((TypeCode)0xae):
					return ModInt32(convertible.ToInt32(null), ToVBBool(conv));

				case ((TypeCode)0xd1):
					return ModInt64(convertible.ToInt64(null), 0L);

				case ((TypeCode)0xd4):
					return ModInt64(convertible.ToInt64(null), (long)ToVBBool(conv));

				case ((TypeCode)0xf7):
					return ModSingle(convertible.ToSingle(null), 0f);

				case ((TypeCode)250):
					return ModSingle(convertible.ToSingle(null), (float)ToVBBool(conv));

				case ((TypeCode)0x10a):
					return ModDouble(convertible.ToDouble(null), 0.0);

				case ((TypeCode)0x10d):
					return ModDouble(convertible.ToDouble(null), (double)ToVBBool(conv));

				case ((TypeCode)0x11d):
					return ModDecimal(convertible, null);

				case ((TypeCode)0x120):
					return ModDecimal(convertible, ToVBBoolConv(conv));

				case ((TypeCode)0x156):
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)0x159):
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return ModString(convertible, typeCode, conv, empty);

				case ((TypeCode)360):
					return ModStringString(convertible.ToString(null), conv.ToString(null));
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}

		/// <summary>
		/// Calculates modulo from two decimal values
		/// </summary>
		/// <param name="f1">First value</param>
		/// <param name="f2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModSingle(float sng1, float sng2)
		{
			return (sng1 % sng2);
		}

		/// <summary>
		/// Calculates modulo from two string values
		/// </summary>
		/// <param name="conv1">First value</param>
		/// <param name="tc1">Type code of first value</param>
		/// <param name="conv2">Second value</param>
		/// <param name="tc2">Type code of second value</param>
		/// <returns>Modulo result</returns>
		private static object ModString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return (num % num2);
		}

		/// <summary>
		/// Calculates modulo from two strings
		/// </summary>
		/// <param name="s1">First value</param>
		/// <param name="s2">Second value</param>
		/// <returns>Modulo result</returns>
		private static object ModStringString(string s1, string s2)
		{
			double num = default(double);
			double num2 = default(double);
			if (s1 != null)
			{
				num = DoubleType.FromString(s1);
			}
			if (s2 != null)
			{
				num2 = DoubleType.FromString(s2);
			}
			return (num % num2);
		}

		/// <summary>
		/// Multiply two bytes
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulByte(byte i1, byte i2)
		{
			int num = i1 * i2;
			if ((num >= 0) && (num <= 0xff))
			{
				return (byte)num;
			}
			if ((num >= -32768) && (num <= 0x7fff))
			{
				return (short)num;
			}
			return num;
		}

		/// <summary>
		/// Multiply two decimal values
		/// </summary>
		/// <param name="conv1">First value</param>
		/// <param name="conv2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulDecimal(IConvertible conv1, IConvertible conv2)
		{
			decimal num = conv1.ToDecimal(null);
			decimal num2 = conv2.ToDecimal(null);
			try
			{
				return decimal.Multiply(num, num2);
			}
			catch (OverflowException)
			{
				return (Convert.ToDouble(num) * Convert.ToDouble(num2));
			}
		}

		/// <summary>
		/// Multiply two double values
		/// </summary>
		/// <param name="d1">First value</param>
		/// <param name="d2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulDouble(double d1, double d2)
		{
			return (d1 * d2);
		}

		/// <summary>
		/// Multiply two short values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulInt16(short i1, short i2)
		{
			int num = i1 * i2;
			if ((num >= -32768) && (num <= 0x7fff))
			{
				return (short)num;
			}
			return num;
		}

		/// <summary>
		/// Multiply two integer values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulInt32(int i1, int i2)
		{
			long num = i1 * i2;
			if ((num >= -2147483648L) && (num <= 0x7fffffffL))
			{
				return (int)num;
			}
			return num;
		}

		/// <summary>
		/// Multiply two long values
		/// </summary>
		/// <param name="i1">First value</param>
		/// <param name="i2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulInt64(long i1, long i2)
		{
			object obj2;
			try
			{
				obj2 = i1 * i2;
			}
			catch (OverflowException)
			{
				try
				{
					obj2 = decimal.Multiply(new decimal(i1), new decimal(i2));
				}
				catch (OverflowException)
				{
					obj2 = i1 * i2;
				}
			}
			return obj2;
		}

		/// <summary>
		/// Multiply two objects
		/// </summary>
		/// <param name="o1">First value</param>
		/// <param name="o2">Second value</param>
		/// <returns>Multiplication result</returns>
		public static object MulObj(object o1, object o2)
		{
			object obj2 = default(object);
			TypeCode empty;
			TypeCode typeCode;
			IConvertible conv = o1 as IConvertible;
			
			if (conv == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			IConvertible convertible2 = o2 as IConvertible;
			
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case TypeCode.Empty:
				case TypeCode.Int32:
				case ((TypeCode)0xab):
					return 0;

				case TypeCode.Boolean:
				case TypeCode.Int16:
				case ((TypeCode)0x39):
				case ((TypeCode)0x85):
					return (short)0;

				case TypeCode.Byte:
				case ((TypeCode)0x72):
					return (byte)0;

				case TypeCode.Int64:
				case ((TypeCode)0xd1):
					return 0L;

				case TypeCode.Single:
				case ((TypeCode)0xf7):
					return 0f;

				case TypeCode.Double:
				case ((TypeCode)0x10a):
					return 0.0;

				case TypeCode.Decimal:
				case ((TypeCode)0x11d):
					return decimal.Zero;

				case TypeCode.String:
				case ((TypeCode)0x156):
					return 0.0;

				case ((TypeCode)60):
					return MulInt16((short)ToVBBool(conv), (short)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return MulInt16((short)ToVBBool(conv), convertible2.ToInt16(null));

				case ((TypeCode)0x42):
					return MulInt32(ToVBBool(conv), convertible2.ToInt32(null));

				case ((TypeCode)0x44):
					return MulInt64((long)ToVBBool(conv), convertible2.ToInt64(null));

				case ((TypeCode)70):
					return MulSingle((float)ToVBBool(conv), convertible2.ToSingle(null));

				case ((TypeCode)0x47):
					return MulDouble((double)ToVBBool(conv), convertible2.ToDouble(null));

				case ((TypeCode)0x48):
					return MulDecimal(ToVBBoolConv(conv), convertible2);

				case ((TypeCode)0x4b):
				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
				case ((TypeCode)0x159):
				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return MulString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return MulInt16(conv.ToInt16(null), (short)ToVBBool(convertible2));

				case ((TypeCode)120):
					return MulByte(conv.ToByte(null), convertible2.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return MulInt16(conv.ToInt16(null), convertible2.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return MulInt32(conv.ToInt32(null), convertible2.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x90):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
					return MulInt64(conv.ToInt64(null), convertible2.ToInt64(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return MulSingle(conv.ToSingle(null), convertible2.ToSingle(null));

				case ((TypeCode)0x80):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return MulDouble(conv.ToDouble(null), convertible2.ToDouble(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return MulDecimal(conv, convertible2);

				case ((TypeCode)0xae):
					return MulInt32(conv.ToInt32(null), ToVBBool(convertible2));

				case ((TypeCode)0xd4):
					return MulInt64(conv.ToInt64(null), (long)ToVBBool(convertible2));

				case ((TypeCode)250):
					return MulSingle(conv.ToSingle(null), (float)ToVBBool(convertible2));

				case ((TypeCode)0x10d):
					return MulDouble(conv.ToDouble(null), (double)ToVBBool(convertible2));

				case ((TypeCode)0x120):
					return MulDecimal(conv, ToVBBoolConv(convertible2));

				case ((TypeCode)360):
					return MulStringString(conv.ToString(null), convertible2.ToString(null));
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}

		/// <summary>
		/// Multiply two single values
		/// </summary>
		/// <param name="f1">First value</param>
		/// <param name="f2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulSingle(float f1, float f2)
		{
			double d = f1 * f2;
			if (((d > 3.40282346638529E+38) || (d < -3.40282346638529E+38)) && (!double.IsInfinity(d) || (!float.IsInfinity(f1) && !float.IsInfinity(f2))))
			{
				return d;
			}
			return (float)d;
		}

		/// <summary>
		/// Multiply two objects
		/// </summary>
		/// <param name="conv1">First object</param>
		/// <param name="tc1">Type code of the first object</param>
		/// <param name="conv2">Second object</param>
		/// <param name="tc2">Type code of second object</param>
		/// <returns>Multiplication result</returns>
		private static object MulString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return (num * num2);
		}

		/// <summary>
		/// Multiply two strings
		/// </summary>
		/// <param name="s1">First value</param>
		/// <param name="s2">Second value</param>
		/// <returns>Multiplication result</returns>
		private static object MulStringString(string s1, string s2)
		{
			double num = default(double);
			double num2 = default(double);
			if (s1 != null)
			{
				num = DoubleType.FromString(s1);
			}
			if (s2 != null)
			{
				num2 = DoubleType.FromString(s2);
			}
			return (num * num2);
		}

		/// <summary>
		/// Performs a unary minus (–) operation.
		/// </summary>
		/// <param name="obj">Required. Any numeric expression.</param>
		/// <returns>The negative value of obj.</returns>
		public static object NegObj(object obj)
		{
			TypeCode empty;
			IConvertible conv = obj as IConvertible;
			
			if (conv == null)
			{
				if (obj == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			return InternalNegObj(obj, conv, empty);
		}

		/// <summary>
		/// Performs a Not operation.
		/// </summary>
		/// <param name="obj">Required. Any Boolean or numeric expression.</param>
		/// <returns>For Boolean operations, False if obj is True; otherwise, True. For bitwise operations, 1 if obj is 0; otherwise, 0.</returns>
		public static object NotObj(object obj)
		{
			Type type = default(Type);
			TypeCode typeCode;
			if (obj == null)
			{
				return -1;
			}
			
			IConvertible convertible = obj as IConvertible;
			if (convertible != null)
			{
				typeCode = convertible.GetTypeCode();
			}
			else
			{
				typeCode = TypeCode.Object;
			}
			switch (typeCode) {
				case TypeCode.Boolean:
					return !convertible.ToBoolean(null);

				case TypeCode.Byte:
					{
						type = obj.GetType();
						byte num = Convert.ToByte(~convertible.ToByte(null));
						if (!type.IsEnum)
						{
							return num;
						}
						return Enum.ToObject(type, num);
					}
				case TypeCode.Int16:
					{
						type = obj.GetType();
						short num2 = Convert.ToInt16(~convertible.ToInt16(null));
						if (!type.IsEnum)
						{
							return num2;
						}
						return Enum.ToObject(type, num2);
					}
				case TypeCode.Int32:
					{
						type = obj.GetType();
						int num3 = ~convertible.ToInt32(null);
						if (!type.IsEnum)
						{
							return num3;
						}
						return Enum.ToObject(type, num3);
					}
				case TypeCode.Int64:
					{
						type = obj.GetType();
						long num4 = ~convertible.ToInt64(null);
						if (!type.IsEnum)
						{
							return num4;
						}
						return Enum.ToObject(type, num4);
					}
				case TypeCode.Single:
					return ~Convert.ToInt64(convertible.ToDecimal(null));

				case TypeCode.Double:
					return ~Convert.ToInt64(convertible.ToDecimal(null));

				case TypeCode.Decimal:
					return ~Convert.ToInt64(convertible.ToDecimal(null));

				case TypeCode.String:
					return ~LongType.FromString(convertible.ToString(null));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_OneOperand", Utils.VBFriendlyName(obj)));
		}
		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="o1">Required. Any expression.</param>
		/// <param name="o2">Required. Any expression.</param>
		/// <param name="textCompare">Required. True to perform a case-insensitive string comparison; otherwise, False.</param>
		/// <returns>Return Value</returns>
		public static int ObjTst(object o1, object o2, bool textCompare)
		{
			int num2 = 0;
			TypeCode empty;
			TypeCode typeCode;
			
			IConvertible conv = o1 as IConvertible;
			if (conv == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			
			IConvertible convertible2 = o2 as IConvertible;
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			if (((empty == TypeCode.Object) && (o1 is char[])) && (((typeCode == TypeCode.String) || (typeCode == TypeCode.Empty)) || ((typeCode == TypeCode.Object) && (o2 is char[]))))
			{
				o1 = new string(CharArrayType.FromObject(o1));
				conv = (IConvertible)o1;
				empty = TypeCode.String;
			}
			
			char[] chararray = o2 as char[];
			if (((typeCode == TypeCode.Object) && (chararray != null)) && ((empty == TypeCode.String) || (empty == TypeCode.Empty)))
			{
				o2 = new string(chararray);
				convertible2 = (IConvertible)o2;
				typeCode = TypeCode.String;
			}
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case TypeCode.Empty:
					return 0;

				case TypeCode.Boolean:
					return ObjTstInt32(0, ToVBBool(convertible2));

				case TypeCode.Char:
					return ObjTstChar('\0', convertible2.ToChar(null));

				case TypeCode.Byte:
					return ObjTstByte(0, convertible2.ToByte(null));

				case TypeCode.Int16:
					return ObjTstInt16(0, convertible2.ToInt16(null));

				case TypeCode.Int32:
					return ObjTstInt32(0, convertible2.ToInt32(null));

				case TypeCode.Int64:
					return ObjTstInt64(0L, convertible2.ToInt64(null));

				case TypeCode.Single:
					return ObjTstSingle(0f, convertible2.ToSingle(null));

				case TypeCode.Double:
					return ObjTstDouble(0.0, convertible2.ToDouble(null));

				case TypeCode.Decimal:
					return ObjTstDecimal((IConvertible)0, convertible2);

				case TypeCode.DateTime:
					return ObjTstDateTime(DateType.FromObject(null), convertible2.ToDateTime(null));

				case TypeCode.String:
					return ObjTstStringString(null, o2.ToString(), textCompare);

				case ((TypeCode)0x39):
					return ObjTstInt32(ToVBBool(conv), 0);

				case ((TypeCode)60):
					return ObjTstInt16((short)ToVBBool(conv), (short)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return ObjTstInt16((short)ToVBBool(conv), convertible2.ToInt16(null));

				case ((TypeCode)0x42):
					return ObjTstInt32(ToVBBool(conv), convertible2.ToInt32(null));

				case ((TypeCode)0x44):
					return ObjTstInt64((long)ToVBBool(conv), convertible2.ToInt64(null));

				case ((TypeCode)70):
					return ObjTstSingle((float)ToVBBool(conv), convertible2.ToSingle(null));

				case ((TypeCode)0x47):
					return ObjTstDouble((double)ToVBBool(conv), convertible2.ToDouble(null));

				case ((TypeCode)0x48):
					return ObjTstDecimal((IConvertible)ToVBBool(conv), convertible2);

				case ((TypeCode)0x4b):
					return ObjTstBoolean(conv.ToBoolean(null), BooleanType.FromString(convertible2.ToString(null)));

				case ((TypeCode)0x4c):
					return ObjTstChar(conv.ToChar(null), '\0');

				case ((TypeCode)80):
					return ObjTstChar(conv.ToChar(null), convertible2.ToChar(null));

				case ((TypeCode)0x5e):
				case ((TypeCode)0x15a):
					return ObjTstStringString(conv.ToString(null), convertible2.ToString(null), textCompare);

				case ((TypeCode)0x72):
					return ObjTstByte(conv.ToByte(null), 0);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return ObjTstInt16(conv.ToInt16(null), (short)ToVBBool(convertible2));

				case ((TypeCode)120):
					return ObjTstByte(conv.ToByte(null), convertible2.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return ObjTstInt16(conv.ToInt16(null), convertible2.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return ObjTstInt32(conv.ToInt32(null), convertible2.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x90):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
					return ObjTstInt64(conv.ToInt64(null), convertible2.ToInt64(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return ObjTstSingle(conv.ToSingle(null), convertible2.ToSingle(null));

				case ((TypeCode)0x80):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return ObjTstDouble(conv.ToDouble(null), convertible2.ToDouble(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return ObjTstDecimal(conv, convertible2);

				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
					return ObjTstString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0x85):
					return ObjTstInt16(conv.ToInt16(null), 0);

				case ((TypeCode)0xab):
					return ObjTstInt32(conv.ToInt32(null), 0);

				case ((TypeCode)0xae):
					return ObjTstInt32(conv.ToInt32(null), ToVBBool(convertible2));

				case ((TypeCode)0xd1):
					return ObjTstInt64(conv.ToInt64(null), 0L);

				case ((TypeCode)0xd4):
					return ObjTstInt64(conv.ToInt64(null), (long)ToVBBool(convertible2));

				case ((TypeCode)0xf7):
					return ObjTstSingle(conv.ToSingle(null), 0f);

				case ((TypeCode)250):
					return ObjTstSingle(conv.ToSingle(null), (float)ToVBBool(convertible2));

				case ((TypeCode)0x10a):
					return ObjTstDouble(conv.ToDouble(null), 0.0);

				case ((TypeCode)0x10d):
					return ObjTstDouble(conv.ToDouble(null), (double)ToVBBool(convertible2));

				case ((TypeCode)0x11d):
					return ObjTstDecimal(conv, (IConvertible)0);

				case ((TypeCode)0x120):
					return ObjTstDecimal(conv, (IConvertible)ToVBBool(convertible2));

				case ((TypeCode)0x130):
					return ObjTstDateTime(conv.ToDateTime(null), DateType.FromObject(null));

				case ((TypeCode)320):
					return ObjTstDateTime(conv.ToDateTime(null), convertible2.ToDateTime(null));

				case ((TypeCode)0x142):
					return ObjTstDateTime(conv.ToDateTime(null), DateType.FromString(convertible2.ToString(null), CultureInfo.InvariantCulture));

				case ((TypeCode)0x156):
					return ObjTstStringString(o1.ToString(), null, textCompare);

				case ((TypeCode)0x159):
					return ObjTstBoolean(BooleanType.FromString(conv.ToString(null)), convertible2.ToBoolean(null));

				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return ObjTstString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0x166):
					return ObjTstDateTime(DateType.FromString(conv.ToString(null), CultureInfo.InvariantCulture), convertible2.ToDateTime(null));

				case ((TypeCode)360):
					return ObjTstStringString(conv.ToString(null), convertible2.ToString(null), textCompare);
			}
			ThrowNoValidOperator(o1, o2);
			return num2;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="b1">Required. Boolean expression.</param>
		/// <param name="b2">Required. Boolean expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstBoolean(bool b1, bool b2)
		{
			if (b1 == b2)
			{
				return 0;
			}
			if (Convert.ToInt32(b1) <= Convert.ToInt32(b2))
			{
				return 1;
			}
			return -1;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="by1">Required. Byte expression.</param>
		/// <param name="by2">Required. Byte expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstByte(byte by1, byte by2)
		{
			if (by1 < by2)
			{
				return -1;
			}
			if (by1 > by2)
			{
				return 1;
			}
			return 0;
		}
		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="ch1">Required. Char expression.</param>
		/// <param name="ch2">Required. Char expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstChar(char ch1, char ch2)
		{
			if (ch1 < ch2)
			{
				return -1;
			}
			if (ch1 > ch2)
			{
				return 1;
			}
			return 0;
		}
		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="var1">Required. Datetime expression.</param>
		/// <param name="var2">Required. Datetime expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstDateTime(DateTime var1, DateTime var2)
		{
			long ticks = var1.Ticks;
			long num3 = var2.Ticks;
			if (ticks < num3)
			{
				return -1;
			}
			if (ticks > num3)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="i1">Required. Any expression.</param>
		/// <param name="i2">Required. Any expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstDecimal(IConvertible i1, IConvertible i2)
		{
			decimal num = i1.ToDecimal(null);
			decimal num2 = i2.ToDecimal(null);
			if (decimal.Compare(num, num2) < 0)
			{
				return -1;
			}
			if (decimal.Compare(num, num2) > 0)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="d1">Required. Double expression.</param>
		/// <param name="d2">Required. Double expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstDouble(double d1, double d2)
		{
			if (d1 < d2)
			{
				return -1;
			}
			if (d1 > d2)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="i1">Required. Short expression.</param>
		/// <param name="i2">Required. Short expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstInt16(short d1, short d2)
		{
			if (d1 < d2)
			{
				return -1;
			}
			if (d1 > d2)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="d1">Required. Integer expression.</param>
		/// <param name="d2">Required. Integer expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstInt32(int d1, int d2)
		{
			if (d1 < d2)
			{
				return -1;
			}
			if (d1 > d2)
			{
				return 1;
			}
			return 0;
		}
		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="d1">Required. Double expression.</param>
		/// <param name="d2">Required. Double expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstInt64(long d1, long d2)
		{
			if (d1 < d2)
			{
				return -1;
			}
			if (d1 > d2)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="d1">Required. Decimal expression.</param>
		/// <param name="d2">Required. Decimal expression.</param>
		/// <returns>Return Value</returns>
		private static int ObjTstSingle(float d1, float d2)
		{
			if (d1 < d2)
			{
				return -1;
			}
			if (d1 > d2)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="conv1">Required. Any expression.</param>
		/// <param name="tc1">Typecode of first expression</param>
		/// <param name="conv2">Required. Any expression.</param>
		/// <param name="tc2">Typecode of second expression</param>
		/// <returns>Return Value</returns>
		private static int ObjTstString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return ObjTstDouble(num, num2);
		}

		/// <summary>
		/// Performs binary or text string comparison when given two objects.
		/// </summary>
		/// <param name="s1">Required. String expression.</param>
		/// <param name="s2">Required. String expression.</param>
		/// <param name="textCompare"></param>
		/// <returns>Return Value</returns>
		private static int ObjTstStringString(string s1, string s2, bool textCompare)
		{
			if (s1 == null)
			{
				if (s2.Length > 0)
				{
					return -1;
				}
				return 0;
			}
			if (s2 == null)
			{
				if (s1.Length > 0)
				{
					return 1;
				}
				return 0;
			}
			if (textCompare)
			{
				return Utils.CultureInfo.CompareInfo.Compare(s1, s2, CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase);
			}
			return string.CompareOrdinal(s1, s2);
		}

		/// <summary>
		/// Performs a unary plus (+) operation.
		/// </summary>
		/// <param name="obj">Required. Any numeric expression.</param>
		/// <returns>The value of obj. (The sign of the obj is unchanged.)</returns>
		public static object PlusObj(object obj)
		{
			TypeCode empty;
			if (obj == null)
			{
				return 0;
			}
			IConvertible convertible = obj as IConvertible;
			
			empty = convertible == null ? TypeCode.Object : convertible.GetTypeCode();
			
			switch (empty) {
				case TypeCode.Empty:
					return 0;

				case TypeCode.Boolean:
					if (obj is bool)
					{
						return (short)((Convert.ToInt32(((bool)obj)) << 0x1f) >> 0x1f);
					}
					return (short)((Convert.ToInt32(convertible.ToBoolean(null)) << 0x1f) >> 0x1f);

				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return obj;

				case TypeCode.String:
					return DoubleType.FromObject(obj);
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_OneOperand", Utils.VBFriendlyName(obj)));
		}

		/// <summary>
		/// Performs an exponent (^) operation.
		/// </summary>
		/// <param name="obj1">Required. Any numeric expression.</param>
		/// <param name="obj2">Required. Any numeric expression.</param>
		/// <returns>The result of obj1 raised to the power of obj2.</returns>
		public static object PowObj(object obj1, object obj2)
		{
			if ((obj1 == null) && (obj2 == null))
			{
				return 1.0;
			}
			switch (GetWidestType(obj1, obj2, false)) {
				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
				case TypeCode.String:
					return Math.Pow(DoubleType.FromObject(obj1), DoubleType.FromObject(obj2));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", Utils.VBFriendlyName(obj1), Utils.VBFriendlyName(obj2)));
		}

		/// <summary>
		/// Performs a string concatenation (&) operation.
		/// </summary>
		/// <param name="vLeft">Required. Any expression.</param>
		/// <param name="vRight">Required. Any expression.</param>
		/// <returns>A string representing the concatenation of vLeft and vRight.</returns>
		public static object StrCatObj(object vLeft, object vRight)
		{
			bool flag = vLeft is DBNull;
			bool flag2 = vRight is DBNull;
			if (flag & flag2)
			{
				return vLeft;
			}
			if (flag & !flag2)
			{
				vLeft = "";
			}
			else if (flag2 & !flag)
			{
				vRight = "";
			}
			return (StringType.FromObject(vLeft) + StringType.FromObject(vRight));
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="i1">Required. Any byte expression.</param>
		/// <param name="i2">Required. Any byte expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubByte(byte i1, byte i2)
		{
			short num = (short)(i1 - i2);
			if ((num >= 0) && (num <= 0xff))
			{
				return (byte)num;
			}
			return num;
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="conv1">Required. Any numeric expression.</param>
		/// <param name="conv2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubDecimal(IConvertible conv1, IConvertible conv2)
		{
			decimal num = conv1.ToDecimal(null);
			decimal num2 = conv2.ToDecimal(null);
			try
			{
				return decimal.Subtract(num, num2);
			}
			catch (OverflowException)
			{
				return (Convert.ToDouble(num) - Convert.ToDouble(num2));
			}
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="d1">Required. Any numeric expression.</param>
		/// <param name="d2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubDouble(double d1, double d2)
		{
			return (d1 - d2);
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="i1">Required. Any numeric expression.</param>
		/// <param name="i2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubInt16(short i1, short i2)
		{
			int num = i1 - i2;
			if ((num >= -32768) && (num <= 0x7fff))
			{
				return (short)num;
			}
			return num;
		}
		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="i1">Required. Any numeric expression.</param>
		/// <param name="i2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubInt32(int i1, int i2)
		{
			long num = i1 - i2;
			if ((num >= -2147483648L) && (num <= 0x7fffffffL))
			{
				return (int)num;
			}
			return num;
		}
		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="i1">Required. Any numeric expression.</param>
		/// <param name="i2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubInt64(long i1, long i2)
		{
			try
			{
				return (i1 - i2);
			}
			catch (ArgumentOutOfRangeException)
			{
				return decimal.Subtract(new decimal(i1), new decimal(i2));
			}
		}
		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="o1">Required. Any numeric expression.</param>
		/// <param name="o2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		public static object SubObj(object o1, object o2)
		{
			object obj2 = default(object);
			TypeCode empty;
			TypeCode typeCode;
			IConvertible conv = o1 as IConvertible;
			
			if (conv == null)
			{
				if (o1 == null)
				{
					empty = TypeCode.Empty;
				}
				else
				{
					empty = TypeCode.Object;
				}
			}
			else
			{
				empty = conv.GetTypeCode();
			}
			IConvertible convertible2 = o2 as IConvertible;
			
			if (convertible2 == null)
			{
				if (o2 == null)
				{
					typeCode = TypeCode.Empty;
				}
				else
				{
					typeCode = TypeCode.Object;
				}
			}
			else
			{
				typeCode = convertible2.GetTypeCode();
			}
			switch ((((int)empty * (int)(TypeCode.String | TypeCode.Object)) + typeCode)) {
				case TypeCode.Empty:
					return 0;

				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return InternalNegObj(o2, convertible2, typeCode);

				case TypeCode.String:
					return SubStringString(null, convertible2.ToString(null));

				case ((TypeCode)0x39):
				case ((TypeCode)0x72):
				case ((TypeCode)0x85):
				case ((TypeCode)0xab):
				case ((TypeCode)0xd1):
				case ((TypeCode)0xf7):
				case ((TypeCode)0x10a):
				case ((TypeCode)0x11d):
					return o1;

				case ((TypeCode)60):
					return SubInt16((short)ToVBBool(conv), (short)ToVBBool(convertible2));

				case ((TypeCode)0x3f):
				case ((TypeCode)0x40):
					return SubInt16((short)ToVBBool(conv), convertible2.ToInt16(null));

				case ((TypeCode)0x42):
					return SubInt32(ToVBBool(conv), convertible2.ToInt32(null));

				case ((TypeCode)0x44):
					return SubInt64((long)ToVBBool(conv), convertible2.ToInt64(null));

				case ((TypeCode)70):
					return SubSingle((float)ToVBBool(conv), convertible2.ToSingle(null));

				case ((TypeCode)0x47):
					return SubDouble((double)ToVBBool(conv), convertible2.ToDouble(null));

				case ((TypeCode)0x48):
					return SubDecimal(ToVBBoolConv(conv), convertible2);

				case ((TypeCode)0x4b):
				case ((TypeCode)0x84):
				case ((TypeCode)0x97):
				case ((TypeCode)0xbd):
				case ((TypeCode)0xe3):
				case ((TypeCode)0x109):
				case ((TypeCode)0x11c):
				case ((TypeCode)0x12f):
				case ((TypeCode)0x159):
				case ((TypeCode)0x15c):
				case ((TypeCode)0x15d):
				case ((TypeCode)0x15f):
				case ((TypeCode)0x161):
				case ((TypeCode)0x163):
				case ((TypeCode)0x164):
				case ((TypeCode)0x165):
					return SubString(conv, empty, convertible2, typeCode);

				case ((TypeCode)0x75):
				case ((TypeCode)0x88):
					return SubInt16(conv.ToInt16(null), (short)ToVBBool(convertible2));

				case ((TypeCode)120):
					return SubByte(conv.ToByte(null), convertible2.ToByte(null));

				case ((TypeCode)0x79):
				case ((TypeCode)0x8b):
				case ((TypeCode)140):
					return SubInt16(conv.ToInt16(null), convertible2.ToInt16(null));

				case ((TypeCode)0x7b):
				case ((TypeCode)0x8e):
				case ((TypeCode)0xb1):
				case ((TypeCode)0xb2):
				case ((TypeCode)180):
					return SubInt32(conv.ToInt32(null), convertible2.ToInt32(null));

				case ((TypeCode)0x7d):
				case ((TypeCode)0x90):
				case ((TypeCode)0xb6):
				case ((TypeCode)0xd7):
				case ((TypeCode)0xd8):
				case ((TypeCode)0xda):
				case ((TypeCode)220):
					return SubInt64(conv.ToInt64(null), convertible2.ToInt64(null));

				case ((TypeCode)0x7f):
				case ((TypeCode)0x92):
				case ((TypeCode)0xb8):
				case ((TypeCode)0xde):
				case ((TypeCode)0xfd):
				case ((TypeCode)0xfe):
				case ((TypeCode)0x100):
				case ((TypeCode)0x102):
				case ((TypeCode)260):
				case ((TypeCode)0x106):
				case ((TypeCode)0x12a):
					return SubSingle(conv.ToSingle(null), convertible2.ToSingle(null));

				case ((TypeCode)0x80):
				case ((TypeCode)0x93):
				case ((TypeCode)0xb9):
				case ((TypeCode)0xdf):
				case ((TypeCode)0x105):
				case ((TypeCode)0x110):
				case ((TypeCode)0x111):
				case ((TypeCode)0x113):
				case ((TypeCode)0x115):
				case ((TypeCode)0x117):
				case ((TypeCode)280):
				case ((TypeCode)0x119):
				case ((TypeCode)0x12b):
					return SubDouble(conv.ToDouble(null), convertible2.ToDouble(null));

				case ((TypeCode)0x81):
				case ((TypeCode)0x94):
				case ((TypeCode)0xba):
				case ((TypeCode)0xe0):
				case ((TypeCode)0x123):
				case ((TypeCode)0x124):
				case ((TypeCode)0x126):
				case ((TypeCode)0x128):
				case ((TypeCode)300):
					return SubDecimal(conv, convertible2);

				case ((TypeCode)0xae):
					return SubInt32(conv.ToInt32(null), ToVBBool(convertible2));

				case ((TypeCode)0xd4):
					return SubInt64(conv.ToInt64(null), (long)ToVBBool(convertible2));

				case ((TypeCode)250):
					return SubSingle(conv.ToSingle(null), (float)ToVBBool(convertible2));

				case ((TypeCode)0x10d):
					return SubDouble(conv.ToDouble(null), (double)ToVBBool(convertible2));

				case ((TypeCode)0x120):
					return SubDecimal(conv, ToVBBoolConv(convertible2));

				case ((TypeCode)0x156):
					return SubStringString(conv.ToString(null), null);

				case ((TypeCode)360):
					return SubStringString(conv.ToString(null), convertible2.ToString(null));
			}
			ThrowNoValidOperator(o1, o2);
			return obj2;
		}
		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="f1">Required. Any numeric expression.</param>
		/// <param name="f2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubSingle(float f1, float f2)
		{
			double d = f1 - f2;
			if (((d > 3.40282346638529E+38) || (d < -3.40282346638529E+38)) && (!double.IsInfinity(d) || (!float.IsInfinity(f1) && !float.IsInfinity(f2))))
			{
				return d;
			}
			return (float)d;
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="conv1">Required. Any numeric expression.</param>
		/// <param name="tc1">Type code of first expression</param>
		/// <param name="conv2">Required. Any numeric expression.</param>
		/// <param name="tc2">Type code of second expression</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubString(IConvertible conv1, TypeCode tc1, IConvertible conv2, TypeCode tc2)
		{
			double num;
			double num2;
			if (tc1 == TypeCode.String)
			{
				num = DoubleType.FromString(conv1.ToString(null));
			}
			else if (tc1 == TypeCode.Boolean)
			{
				num = ToVBBool(conv1);
			}
			else
			{
				num = conv1.ToDouble(null);
			}
			if (tc2 == TypeCode.String)
			{
				num2 = DoubleType.FromString(conv2.ToString(null));
			}
			else if (tc2 == TypeCode.Boolean)
			{
				num2 = ToVBBool(conv2);
			}
			else
			{
				num2 = conv2.ToDouble(null);
			}
			return (num - num2);
		}

		/// <summary>
		/// Performs a subtraction (–) operation.
		/// </summary>
		/// <param name="s1">Required. Any numeric expression.</param>
		/// <param name="s2">Required. Any numeric expression.</param>
		/// <returns>The difference between o1 and o2.</returns>
		private static object SubStringString(string s1, string s2)
		{
			double num = default(double);
			double num2 = default(double);
			if (s1 != null)
			{
				num = DoubleType.FromString(s1);
			}
			if (s2 != null)
			{
				num2 = DoubleType.FromString(s2);
			}
			return (num - num2);
		}

		private static void ThrowNoValidOperator(object obj1, object obj2)
		{
			string resourceString;
			string str2;
			string str3;
			string str4 = "Nothing";
			if (obj1 == null)
			{
				str3 = "Nothing";
				str4 = Utils.VBFriendlyName(obj2);
			}
			else if (obj2 == null)
			{
				str3 = Utils.VBFriendlyName(obj1);
				str4 = "Nothing";
			}
			else
			{
				str3 = Utils.VBFriendlyName(obj1);
				str4 = Utils.VBFriendlyName(obj2);
			}
			
			string strObj1 = obj1 as string;
			if (!String.IsNullOrEmpty(strObj1))
			{
				resourceString = Utils.GetResourceString("NoValidOperator_StringType1", StringsEx.Left(strObj1, 0x20));
			}
			else if (obj1 == null)
			{
				resourceString = "'Nothing'";
			}
			else
			{
				resourceString = Utils.GetResourceString("NoValidOperator_NonStringType1", str3);
			}
			
			string strObj2 = obj2 as string;
			if (!String.IsNullOrEmpty(strObj2))
			{
				str2 = Utils.GetResourceString("NoValidOperator_StringType1", StringsEx.Left(strObj2, 0x20));
			}
			else if (obj2 == null)
			{
				str2 = "'Nothing'";
			}
			else
			{
				str2 = Utils.GetResourceString("NoValidOperator_NonStringType1", str4);
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", resourceString, str2));
		}

		/// <summary>
		/// Support boolean values in vb
		/// </summary>
		/// <param name="conv">Input object</param>
		/// <returns>Integer value</returns>
		private static int ToVBBool(IConvertible conv)
		{
			if (conv.ToBoolean(null))
			{
				return -1;
			}
			return 0;
		}

		/// <summary>
		/// Support boolean values in vb
		/// </summary>
		/// <param name="conv">Input object</param>
		/// <returns>IConvertible value</returns>
		private static IConvertible ToVBBoolConv(IConvertible conv)
		{
			if (conv.ToBoolean(null))
			{
				return (IConvertible)(-1);
			}
			return (IConvertible)0;
		}

		/// <summary>
		/// Converts VType to typecode
		/// </summary>
		/// <param name="vartyp">Input object</param>
		/// <returns>Type code value</returns>
		private static TypeCode TypeCodeFromVType(VType vartyp)
		{
			switch (vartyp) {
				case VType.t_bool:
					return TypeCode.Boolean;

				case VType.t_ui1:
					return TypeCode.Byte;

				case VType.t_i2:
					return TypeCode.Int16;

				case VType.t_i4:
					return TypeCode.Int32;

				case VType.t_i8:
					return TypeCode.Int64;

				case VType.t_dec:
					return TypeCode.Decimal;

				case VType.t_r4:
					return TypeCode.Single;

				case VType.t_r8:
					return TypeCode.Double;

				case VType.t_char:
					return TypeCode.Char;

				case VType.t_str:
					return TypeCode.String;

				case VType.t_date:
					return TypeCode.DateTime;
			}
			return TypeCode.Object;
		}




		/// <summary>
		/// Converts type code to VType
		/// </summary>
		/// <param name="typ">Input object</param>
		/// <returns>VType value</returns>
		private static VType VTypeFromTypeCode(TypeCode typ)
		{
			switch (typ) {
				case TypeCode.Boolean:
					return VType.t_bool;

				case TypeCode.Char:
					return VType.t_char;

				case TypeCode.Byte:
					return VType.t_ui1;

				case TypeCode.Int16:
					return VType.t_i2;

				case TypeCode.Int32:
					return VType.t_i4;

				case TypeCode.Int64:
					return VType.t_i8;

				case TypeCode.Single:
					return VType.t_r4;

				case TypeCode.Double:
					return VType.t_r8;

				case TypeCode.Decimal:
					return VType.t_dec;

				case TypeCode.DateTime:
					return VType.t_date;

				case TypeCode.String:
					return VType.t_str;
			}
			return VType.t_bad;
		}

		/// <summary>
		/// Performs an Xor comparison.
		/// </summary>
		/// <param name="obj1">Required. Any Boolean or numeric expression.</param>
		/// <param name="obj2">Required. Any Boolean or numeric expression.</param>
		/// <returns>A Boolean or numeric value.</returns>
		public static object XorObj(object obj1, object obj2)
		{
			if ((obj1 == null) && (obj2 == null))
			{
				return false;
			}
			switch (GetWidestType(obj1, obj2, false)) {
				case TypeCode.Boolean:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
				case TypeCode.String:
					return (BooleanType.FromObject(obj1) ^ BooleanType.FromObject(obj2));
			}
			throw new InvalidCastException(Utils.GetResourceString("NoValidOperator_TwoOperands", Utils.VBFriendlyName(obj1), Utils.VBFriendlyName(obj2)));
		}

		// Nested Types
		/// <summary>
		/// CC enum
		/// </summary>
		private enum CC : byte
		{
			/// <summary>
			/// Err value
			/// </summary>
			Err = 0,
			/// <summary>
			/// Narr value
			/// </summary>
			Narr = 2,
			/// <summary>
			/// Same value
			/// </summary>
			Same = 1,
			/// <summary>
			/// Wide value
			/// </summary>
			Wide = 3
		}

		/// <summary>
		/// VType enum
		/// </summary>
		internal enum VType
		{
			t_bad,
			t_bool,
			t_ui1,
			t_i2,
			t_i4,
			t_i8,
			t_dec,
			t_r4,
			t_r8,
			t_char,
			t_str,
			t_date
		}

		/// <summary>
		/// VType2 enum
		/// </summary>
		internal enum VType2
		{
			t_bad,
			t_bool,
			t_ui1,
			t_char,
			t_i2,
			t_i4,
			t_i8,
			t_r4,
			t_r8,
			t_date,
			t_dec,
			t_ref,
			t_str
		}
	}

}
