using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// DecimalType class provides methods for manipulating data that potentially represents a Decimal value.
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	public sealed class DecimalType
	{

		private DecimalType()
		{

		}
		// Methods

		/// <summary>
		/// Converts the specified Boolean value to the equivalent decimal number.
		/// </summary>
		/// <param name="value">The Boolean value to convert.</param>
		/// <returns>Decimal value.</returns>
		public static decimal FromBoolean(bool value)
		{
			if (value)
			{
				return decimal.MinusOne;
			}
			return decimal.Zero;
		}

		/// <summary>
		/// Converts the value of the specified object to an equivalent decimal number.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <returns>Decimal value.</returns>
		public static decimal FromObject(object value)
		{
			return FromObject(value, null);
		}

		/// <summary>
		/// Converts the value of the specified object to an equivalent decimal number.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <param name="numberFormat">NumberFormatInfo object</param>
		/// <returns>Decimal value.</returns>
		public static decimal FromObject(object value, IFormatProvider numberFormat)
		{
			if (value == null)
			{
				return decimal.Zero;
			}
			
			IConvertible convertible = value as IConvertible;
			
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.Boolean:
						return FromBoolean(convertible.ToBoolean(null));

					case TypeCode.Byte:
						return new decimal(convertible.ToByte(null));

					case TypeCode.Int16:
						return new decimal(convertible.ToInt16(null));

					case TypeCode.Int32:
						return new decimal(convertible.ToInt32(null));

					case TypeCode.Int64:
						return new decimal(convertible.ToInt64(null));

					case TypeCode.Single:
						return new decimal(convertible.ToSingle(null));

					case TypeCode.Double:
						return new decimal(convertible.ToDouble(null));

					case TypeCode.Decimal:
						return convertible.ToDecimal(null);

					case TypeCode.String:
						return FromString(convertible.ToString(null), numberFormat);
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Decimal"));
		}

		/// <summary>
		/// Returns a Decimal value that corresponds to the specified string.
		/// </summary>
		/// <param name="value">Required. String to convert to a Decimal value.</param>
		/// <returns>The Decimal value that corresponds to Value.</returns>
		public static decimal FromString(string value)
		{
			return FromString(value, null);
		}

		/// <summary>
		/// Converts the specified string representation of a number to an equivalent decimal number.
		/// </summary>
		/// <param name="value">A string that contains a number to convert.</param>
		/// <param name="numberFormat">Required. NumberFormatInfo value.</param>
		/// <returns>Decimal value.</returns>
		public static decimal FromString(string value, IFormatProvider numberFormat)
		{
			decimal num = default(decimal);
			if (value == null)
			{
				return decimal.Zero;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return new decimal(num2);
				}
				num = Parse(value, numberFormat);
			}
			catch (OverflowException)
			{
				throw ExceptionUtils.VbMakeException(6);
			}
			catch (FormatException)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Decimal"));
			}
			return num;
		}

		/// <summary>
		/// Converts the string representation of a number to its decimal number equivalent.
		/// </summary>
		/// <param name="value">A string that contains a number to convert.</param>
		/// <param name="numberFormat">NumberFormatInfo object</param>
		/// <returns>
		/// Decimal number.
		/// </returns>
		public static decimal Parse(string value, IFormatProvider numberFormat)
		{
			decimal number;
			
			if (decimal.TryParse(StringType.ToHalfwidthNumbers(value), NumberStyles.Any, numberFormat, out number))
			{
				return number;
			}
			
			return decimal.Parse(StringType.ToHalfwidthNumbers(value), NumberStyles.Float | NumberStyles.AllowThousands | NumberStyles.AllowParentheses | NumberStyles.AllowTrailingSign, numberFormat);
		}
	}
}
