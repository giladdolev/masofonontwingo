using System.Runtime.InteropServices;
using System.Reflection;
using System.ComponentModel;
using System.Extensions.VBAttributes;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// StructUtils
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	internal sealed class StructUtils
	{
		// Methods

		/// <summary>
		/// EnumerateUDT
		/// </summary>
		/// <param name="oStruct">Required.ValueType value.</param>
		/// <param name="intfRecEnum">Required. IRecordEnum value.</param>
		/// <param name="fGet">Boolean value.</param>
		/// <returns>Object value</returns>
		static internal object EnumerateUDT(ValueType oStruct, IRecordEnum intfRecEnum, bool fGet)
		{
			object obj2 = default(object);
			Type typ = oStruct.GetType();
			if ((VB6File.VariantType)Information.VarTypeFromComType(typ) != VB6File.VariantType.UserDefinedType)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "oStruct"));
			}
			FieldInfo[] fields = typ.GetFields();
			int num2 = 0;
			int upperBound = fields.GetUpperBound(0);
			if (upperBound < num2)
			{
				upperBound = num2;
			}
			int num4 = upperBound;
			for (int i = num2; i <= num4; i++)
			{
				FieldInfo fieldInfo = fields[i];
				Type fieldType = fieldInfo.FieldType;
				object obj3 = fieldInfo.GetValue(oStruct);
				if ((VB6File.VariantType)Information.VarTypeFromComType(fieldType) == VB6File.VariantType.UserDefinedType)
				{
					if (fieldType.IsPrimitive)
					{
						throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", fieldInfo.Name, fieldType.Name)), 5);
					}
					EnumerateUDT((ValueType)obj3, intfRecEnum, fGet);
				}
				else
				{
					intfRecEnum.Callback(fieldInfo, ref obj3);
				}
				if (fGet)
				{
					fieldInfo.SetValue(oStruct, obj3);
				}
			}
			return obj2;
		}

		/// <summary>
		/// Gets Record Length
		/// </summary>
		/// <param name="o">Required. Object value.</param>
		/// <param name="packSize">Optional. Integer value</param>
		/// <returns>Integer value</returns>
		public static int GetRecordLength(object o, [OptionalAttribute(), DefaultParameterValueAttribute(-1)]int packSize)
		{
			if (o == null)
			{
				return 0;
			}
			StructByteLengthHandler handler = new StructByteLengthHandler(packSize);
			IRecordEnum intfRecEnum = handler;
			if (intfRecEnum == null)
			{
				throw ExceptionUtils.VbMakeException(5);
			}
			EnumerateUDT((ValueType)o, intfRecEnum, false);
			return handler.Length;
		}

		// Nested Types

		/// <summary>
		/// StructByteLengthHandler class
		/// </summary>
		private sealed class StructByteLengthHandler : IRecordEnum
		{
			// Fields
			private int _packSize;
			private int _structLength;

			// Methods

			/// <summary>
			/// Create new instance from StructByteLengthHandler
			/// </summary>
			/// <param name="packSize">The pack size</param>
			public StructByteLengthHandler(int packSize)
			{
				_packSize = packSize;
			}

			// Properties

			// Properties
			/// <summary>
			/// Get structure length
			/// </summary>
			public int Length
			{
				get
				{
					if (_packSize == 1)
					{
						return _structLength;
					}
					return (_structLength + (_structLength % _packSize));
				}
			}

			/// <summary>
			/// Call Back 
			/// </summary>
			/// <param name="field_info">Required.FieldInfo value.</param>
			/// <param name="vValue">Required.Object value.</param>
			/// <returns>Boolean value.</returns>
			public bool Callback(FieldInfo field_info, ref object vValue)
			{
				int num = 0;
				int num2 = 0;
				Type fieldType = field_info.FieldType;
				if (fieldType == null)
				{
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Empty")), 5);
				}
				if (fieldType.IsArray)
				{
					VBFixedArrayAttribute attribute = default(VBFixedArrayAttribute);
					int length;
					int num4 = 0;
					object[] customAttributes = field_info.GetCustomAttributes(typeof(VBFixedArrayAttribute), false);
					if ((customAttributes != null) && (customAttributes.Length != 0))
					{
						attribute = (VBFixedArrayAttribute)customAttributes[0];
					}
					Type elementType = fieldType.GetElementType();
					if (attribute == null)
					{
						length = 1;
						num4 = 4;
					}
					else
					{
						length = attribute.Length;
						GetFieldSize(field_info, elementType, ref num, ref num4);
					}
					SetAlignment(num);
					_structLength += length * num4;
					return false;
				}
				GetFieldSize(field_info, fieldType, ref num, ref num2);
				SetAlignment(num);
				_structLength += num2;
				return false;
			}

			/// <summary>
			/// Get Field Size
			/// </summary>
			/// <param name="field_info">FieldInfo value.</param>
			/// <param name="fieldType">Type value.</param>
			/// <param name="align">Integer value.</param>
			/// <param name="size">Integer value</param>
			private static void GetFieldSize(FieldInfo field_info, Type fieldType, ref int align, ref int size)
			{
				switch (Type.GetTypeCode(fieldType)) {
					case TypeCode.DBNull:
						throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "DBNull")), 5);

					case TypeCode.Boolean:
						align = 2;
						size = 2;
						break;

					case TypeCode.Char:
						align = 2;
						size = 2;
						break;

					case TypeCode.Byte:
						align = 1;
						size = 1;
						break;

					case TypeCode.Int16:
						align = 2;
						size = 2;
						break;

					case TypeCode.Int32:
						align = 4;
						size = 4;
						break;

					case TypeCode.Int64:
						align = 8;
						size = 8;
						break;

					case TypeCode.Single:
						align = 4;
						size = 4;
						break;

					case TypeCode.Double:
						align = 8;
						size = 8;
						break;

					case TypeCode.Decimal:
						align = 0x10;
						size = 0x10;
						break;

					case TypeCode.DateTime:
						align = 8;
						size = 8;
						break;

					case TypeCode.String:
						{
							object[] customAttributes = field_info.GetCustomAttributes(typeof(VBFixedStringAttribute), false);
							if ((customAttributes != null) && (customAttributes.Length != 0))
							{
								VBFixedStringAttribute attribute = (VBFixedStringAttribute)customAttributes[0];
								int length = attribute.Length;
								if (length == 0)
								{
									length = -1;
								}
								size = length;
								break;
							}
							align = 4;
							size = 4;
							break;
						}
				}
				if (fieldType == typeof(Exception))
				{
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Exception")), 5);
				}
				if (fieldType == typeof(Missing))
				{
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Missing")), 5);
				}
				if (fieldType == typeof(object))
				{
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Object")), 5);
				}
			}

			/// <summary>
			/// Set Alignment
			/// </summary>
			/// <param name="size">Integer value.</param>
			public void SetAlignment(int size)
			{
				if (_packSize != 1)
				{
					_structLength += _structLength % size;
				}
			}
		}
	}
}
