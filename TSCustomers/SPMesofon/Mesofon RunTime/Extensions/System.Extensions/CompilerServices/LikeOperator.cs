using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Sealed class LikeOperator
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class LikeOperator
	{
		private LikeOperator()
		{

		}

		/// <summary>
		/// Enumeration - kind of char
		/// </summary>
		private enum CharKind
		{
			None,
			ExpandedChar1,
			ExpandedChar2
		}

		/// <summary>
		/// Struct LigatureInfo
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential)]
		private struct LigatureInfo
		{
			internal CharKind Kind;
			internal char CharBeforeExpansion;
		}

		/// <summary>
		/// Struct Range
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential)]
		private struct Range
		{
			internal int Start;
			internal int StartLength;
			internal int End;
			internal int EndLength;
		}

		/// <summary>
		/// Enumeration - type of pattern
		/// </summary>
		private enum PatternType
		{
			STRING,
			EXCLIST,
			INCLIST,
			DIGIT,
			ANYCHAR,
			STAR,
			NONE
		}

		/// <summary>
		/// Struct PatternGroup
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential)]
		private struct PatternGroup
		{
			internal PatternType PatType;
			internal int MaxSourceIndex;
			internal int CharCount;
			internal int StringPatternStart;
			internal int StringPatternEnd;
			internal int MinSourceIndex;
			internal List<Range> RangeList;
			public int StartIndexOfPossibleMatch;
		}


		private static string[] _ligatureExpansions;
		private static byte[] _ligatureMap;

		/// <summary>
		/// Compares a string against a pattern.
		/// </summary>
		/// <param name="source">Required. String value.</param>
		/// <param name="pattern">Required. Any String expression conforming to the pattern-matching conventions described in "Remarks."</param>
		/// <param name="compareOption">Required. CompareOption value.</param>
		/// <returns>Boolean value</returns>
		public static bool LikeString(string source, string pattern, StringType.CompareMethod compareOption)
		{
			CompareInfo compareInfo;
			CompareOptions ordinal;
			char ch;
			int num = 0;
			int length;
			LigatureInfo[] inputLigatureInfo = null;
			int num3 = 0;
			int num4 = 0;
			LigatureInfo[] infoArray2 = null;
			bool flag7;
			if (pattern == null)
			{
				length = 0;
			}
			else
			{
				length = pattern.Length;
			}
			if (source == null)
			{
				num4 = 0;
			}
			else
			{
				num4 = source.Length;
			}
			if (compareOption == StringType.CompareMethod.Binary)
			{
				ordinal = CompareOptions.Ordinal;
				compareInfo = null;
			}
			else
			{
				compareInfo = Utils.CultureInfo.CompareInfo;
				ordinal = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase;
				byte[] localeSpecificLigatureTable = new byte[(_ligatureExpansions.Length - 1) + 1];
				flag7 = false;
				ExpandString(ref source, ref num4, ref infoArray2, localeSpecificLigatureTable, compareInfo, ordinal, ref flag7, false);
				flag7 = false;
				ExpandString(ref pattern, ref length, ref inputLigatureInfo, localeSpecificLigatureTable, compareInfo, ordinal, ref flag7, false);
			}
			while ((num < length) && (num3 < num4))
			{
				ch = pattern[num];
				switch (ch) {
					case '?':
					case (char)0xff1f:
						SkipToEndOfExpandedChar(infoArray2, num4, ref num3);
						break;

					case '#':
					case (char)0xff03:
						if (!char.IsDigit(source[num3]))
						{
							return false;
						}
						break;

					case '[':
					case (char)0xff3b:
						{
							bool flag2 = false;
							bool flag3 = false;
							bool flag4 = false;
							flag7 = false;
							MatchRange(source, num4, ref num3, infoArray2, pattern, length, ref num, inputLigatureInfo, ref flag3, ref flag2,
							ref flag4, compareInfo, ordinal, ref flag7, null, false);
							if (flag4)
							{
								throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
							}
							if (flag2)
							{
								return false;
							}
							if (!flag3)
							{
								break;
							}
							num++;
							continue;
						}
					case '*':
					case (char)0xff0a:
						bool flag5 = false;
						bool flag6 = false;
						MatchAsterisk(source, num4, num3, infoArray2, pattern, length, num, inputLigatureInfo, ref flag5, ref flag6,
						compareInfo, ordinal);
						if (flag6)
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
						}
						return !flag5;

					default:
						if (CompareChars(source, num4, num3, ref num3, infoArray2, pattern, length, num, ref num, inputLigatureInfo,
						compareInfo, ordinal, false, false) != 0)
						{
							return false;
						}
						break;
				}
				num++;
				num3++;
			}
			while (num < length)
			{
				ch = pattern[num];
				if ((ch == '*') || (ch == 0xff0a))
				{
					num++;
				}
				else
				{
					if (((num + 1) >= length) || (((ch != '[') || (pattern[num + 1] != ']')) && ((ch != 0xff3b) || (pattern[num + 1] != 0xff3d))))
					{
						break;
					}
					num += 2;
				}
			}
			return ((num >= length) && (num3 >= num4));
		}


		private static void MatchAsterisk(string source, int sourceLength, int sourceIndex, LigatureInfo[] sourceLigatureInfo, string pattern, int patternLength, int patternIndex, LigatureInfo[] pattternLigatureInfo, ref bool mismatch, ref bool patternError,
		CompareInfo comparer, CompareOptions options)
		{
			mismatch = false;
			patternError = false;
			if (patternIndex < patternLength)
			{
				int num2 = 0;
				PatternGroup[] patternGroups = null;
				BuildPatternGroups(source, sourceLength, ref sourceIndex, sourceLigatureInfo, pattern, patternLength, ref patternIndex, pattternLigatureInfo, ref patternError, ref num2,
				comparer, options, ref patternGroups);
				if (!patternError)
				{
					if (patternGroups[num2 + 1].PatType != PatternType.NONE)
					{
						int num4 = 0;
						int num5 = sourceIndex;
						int index = num2 + 1;
						do
						{
							num4 += patternGroups[index].CharCount;
							index++;
						}
						while (patternGroups[index].PatType != PatternType.NONE);
						sourceIndex = sourceLength;
						SubtractChars(source, sourceLength, ref sourceIndex, num4, sourceLigatureInfo, options);
						MatchAsterisk(source, sourceLength, sourceIndex, sourceLigatureInfo, pattern, pattternLigatureInfo, patternGroups, num2, ref mismatch, ref patternError,
						comparer, options);
						if (patternError)
						{
							return;
						}
						if (mismatch)
						{
							return;
						}
						sourceLength = patternGroups[num2 + 1].StartIndexOfPossibleMatch;
						if (sourceLength <= 0)
						{
							return;
						}
						patternGroups[index].MaxSourceIndex = sourceLength;
						patternGroups[index].MinSourceIndex = sourceLength;
						patternGroups[index].StartIndexOfPossibleMatch = 0;
						patternGroups[num2 + 1] = patternGroups[index];
						patternGroups[num2].MinSourceIndex = 0;
						patternGroups[num2].StartIndexOfPossibleMatch = 0;
						index = num2 + 1;
						int num3 = sourceLength;
						while (index > 0)
						{
							switch (patternGroups[index].PatType) {
								case PatternType.STRING:
									num3 -= patternGroups[index].CharCount;
									break;

								case PatternType.EXCLIST:
								case PatternType.INCLIST:
									num3--;
									break;

								case PatternType.DIGIT:
								case PatternType.ANYCHAR:
									num3 -= patternGroups[index].CharCount;
									break;
							}
							patternGroups[index].MaxSourceIndex = num3;
							index--;
						}
						sourceIndex = num5;
					}
					MatchAsterisk(source, sourceLength, sourceIndex, sourceLigatureInfo, pattern, pattternLigatureInfo, patternGroups, 0, ref mismatch, ref patternError,
					comparer, options);
				}
			}
		}

		private static void MatchAsterisk(string source, int sourceLength, int sourceIndex, LigatureInfo[] sourceLigatureInfo, string pattern, LigatureInfo[] patternLigatureInfo, PatternGroup[] patternGroups, int pGIndex, ref bool mismatch, ref bool patternError,
		CompareInfo comparer, CompareOptions options)
		{
			PatternGroup @group;
			int index = pGIndex;
			int maxSourceIndex = sourceIndex;
			int num3 = -1;
			int num2 = -1;
			patternGroups[pGIndex].MinSourceIndex = sourceIndex;
			patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
			pGIndex++;
			Label_002D:
			@group = patternGroups[pGIndex];
			switch (@group.PatType) {
				case PatternType.STRING:
					{
						Label_006A:
						if (sourceIndex > @group.MaxSourceIndex)
						{
							mismatch = true;
							return;
						}
						patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
						int stringPatternStart = @group.StringPatternStart;
						int num6 = 0;
						int leftStart = sourceIndex;
						bool flag = true;
						do
						{
							int num8 = CompareChars(source, sourceLength, leftStart, ref leftStart, sourceLigatureInfo, pattern, @group.StringPatternEnd + 1, stringPatternStart, ref stringPatternStart, patternLigatureInfo,
							comparer, options, false, false);
							if (flag)
							{
								flag = false;
								num6 = leftStart + 1;
							}
							if (num8 != 0)
							{
								sourceIndex = num6;
								index = pGIndex - 1;
								maxSourceIndex = sourceIndex;
								goto Label_006A;
							}
							stringPatternStart++;
							leftStart++;
							if (stringPatternStart > @group.StringPatternEnd)
							{
								sourceIndex = leftStart;
								goto Label_02F4;
							}
						}
						while (leftStart < sourceLength);
						mismatch = true;
						return;
					}
				case PatternType.EXCLIST:
				case PatternType.INCLIST:
					{
						while (true)
						{
							if (sourceIndex > @group.MaxSourceIndex)
							{
								mismatch = true;
								return;
							}
							patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
							if (MatchRangeAfterAsterisk(source, sourceLength, ref sourceIndex, sourceLigatureInfo, pattern, patternLigatureInfo, @group, comparer, options))
							{
								goto Label_02F4;
							}
							index = pGIndex - 1;
							maxSourceIndex = sourceIndex;
						}
					}
				case PatternType.DIGIT:
					{
						Label_010A:
						if (sourceIndex > @group.MaxSourceIndex)
						{
							mismatch = true;
							return;
						}
						patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
						int num11 = @group.CharCount;
						for (int j = 1; j <= num11; j++)
						{
							char c = source[sourceIndex];
							sourceIndex++;
							if (!char.IsDigit(c))
							{
								index = pGIndex - 1;
								maxSourceIndex = sourceIndex;
								goto Label_010A;
							}
						}
						goto Label_02F4;
					}
				case PatternType.ANYCHAR:
					if (sourceIndex <= @group.MaxSourceIndex)
					{
						break;
					}
					mismatch = true;
					return;

				case PatternType.STAR:
					patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
					@group.MinSourceIndex = sourceIndex;
					if (patternGroups[index].PatType == PatternType.STAR)
					{
						goto Label_02E9;
					}
					if (sourceIndex <= @group.MaxSourceIndex)
					{
						goto Label_0285;
					}
					mismatch = true;
					return;

				case PatternType.NONE:
					patternGroups[pGIndex].StartIndexOfPossibleMatch = @group.MaxSourceIndex;
					if (sourceIndex < @group.MaxSourceIndex)
					{
						index = pGIndex - 1;
						maxSourceIndex = @group.MaxSourceIndex;
					}
					if ((patternGroups[index].PatType == PatternType.STAR) || (patternGroups[index].PatType == PatternType.NONE))
					{
						return;
					}
					goto Label_0285;

				default:
					goto Label_02F4;
			}
			patternGroups[pGIndex].StartIndexOfPossibleMatch = sourceIndex;
			int charCount = @group.CharCount;
			for (int i = 1; i <= charCount; i++)
			{
				if (sourceIndex >= sourceLength)
				{
					mismatch = true;
					return;
				}
				SkipToEndOfExpandedChar(sourceLigatureInfo, sourceLength, ref sourceIndex);
				sourceIndex++;
			}
			goto Label_02F4;
			Label_0285:
			num3 = pGIndex;
			sourceIndex = maxSourceIndex;
			pGIndex = index;
			do
			{
				SubtractChars(source, sourceLength, ref sourceIndex, patternGroups[pGIndex].CharCount, sourceLigatureInfo, options);
				pGIndex--;
			}
			while (patternGroups[pGIndex].PatType != PatternType.STAR);
			sourceIndex = Math.Max(sourceIndex, patternGroups[pGIndex].MinSourceIndex + 1);
			patternGroups[pGIndex].MinSourceIndex = sourceIndex;
			num2 = pGIndex;
			Label_02E9:
			pGIndex++;
			goto Label_002D;
			Label_02F4:
			if (pGIndex == index)
			{
				if (sourceIndex == maxSourceIndex)
				{
					sourceIndex = patternGroups[num3].MinSourceIndex;
					pGIndex = num3;
					index = num3;
				}
				else if (sourceIndex < maxSourceIndex)
				{
					PatternGroup[] groupArray = patternGroups;
					int num13 = num2;
					groupArray[num13].MinSourceIndex++;
					sourceIndex = patternGroups[num2].MinSourceIndex;
					pGIndex = num2 + 1;
				}
				else
				{
					pGIndex++;
					index = num2;
				}
			}
			else
			{
				pGIndex++;
			}
			goto Label_002D;
		}

		private static bool MatchRangeAfterAsterisk(string source, int sourceLength, ref int sourceIndex, LigatureInfo[] sourceLigatureInfo, string pattern, LigatureInfo[] patternLigatureInfo, PatternGroup pG, CompareInfo comparer, CompareOptions options)
		{
			List<Range> rangeList = pG.RangeList;
			int leftEnd = sourceIndex;
			bool flag = false;
			foreach (Range range in rangeList)
			{
				int num4;
				int num2 = 1;
				if ((patternLigatureInfo != null) && (patternLigatureInfo[range.Start].Kind == CharKind.ExpandedChar1))
				{
					num4 = 0;
					if (CompareChars(source, sourceLength, sourceIndex, ref leftEnd, sourceLigatureInfo, pattern, range.Start + range.StartLength, range.Start, ref num4, patternLigatureInfo,
					comparer, options, true, false) == 0)
					{
						flag = true;
						break;
					}
				}
				num4 = 0;
				int num3 = CompareChars(source, sourceLength, sourceIndex, ref leftEnd, sourceLigatureInfo, pattern, range.Start + range.StartLength, range.Start, ref num4, patternLigatureInfo,
				comparer, options, false, true);
				if ((num3 > 0) && (range.End >= 0))
				{
					num4 = 0;
					num2 = CompareChars(source, sourceLength, sourceIndex, ref leftEnd, sourceLigatureInfo, pattern, range.End + range.EndLength, range.End, ref num4, patternLigatureInfo,
					comparer, options, false, true);
				}
				if ((num3 == 0) || ((num3 > 0) && (num2 <= 0)))
				{
					flag = true;
					break;
				}
			}
			if (pG.PatType == PatternType.EXCLIST)
			{
				flag = !flag;
			}
			sourceIndex = leftEnd + 1;
			return flag;
		}

		/// <summary>
		/// Subtract chars from string
		/// </summary>
		/// <param name="input">Input string</param>
		/// <param name="inputLength">Length of the string</param>
		/// <param name="current">Current length of the string</param>
		/// <param name="charsToSubtract">Number of chars to subtract</param>
		/// <param name="inputLigatureInfo">Input LigatureInfo</param>
		/// <param name="options">Compare Options</param>
		private static void SubtractChars(string input, int inputLength, ref int current, int charsToSubtract, LigatureInfo[] inputLigatureInfo, CompareOptions options)
		{
			if (options == CompareOptions.Ordinal)
			{
				current -= charsToSubtract;
				if (current < 0)
				{
					current = 0;
				}
			}
			else
			{
				int num2 = charsToSubtract;
				for (int i = 1; i <= num2; i++)
				{
					SubtractOneCharInTextCompareMode(input, inputLength, ref current, inputLigatureInfo, options);
					if (current < 0)
					{
						current = 0;
						break;
					}
				}
			}
		}

		/// <summary>
		/// Subtracts one character in text compare mode
		/// </summary>
		/// <param name="input">Input string</param>
		/// <param name="inputLength">Length of a string</param>
		/// <param name="current">Current length of a string</param>
		/// <param name="inputLigatureInfo">Input LigatureInfo</param>
		/// <param name="options">Compare options</param>
		private static void SubtractOneCharInTextCompareMode(string input, int inputLength, ref int current, LigatureInfo[] inputLigatureInfo, CompareOptions options)
		{
			if (current >= inputLength)
			{
				current--;
			}
			else if ((inputLigatureInfo != null) && (inputLigatureInfo[current].Kind == CharKind.ExpandedChar2))
			{
				current -= 2;
			}
			else
			{
				current--;
			}
		}

		private static void BuildPatternGroups(string source, int sourceLength, ref int sourceIndex, LigatureInfo[] sourceLigatureInfo, string pattern, int patternLength, ref int patternIndex, LigatureInfo[] patternLigatureInfo, ref bool patternError, ref int pGIndexForLastAsterisk,
		CompareInfo comparer, CompareOptions options, ref PatternGroup[] patternGroups)
		{
			patternError = false;
			pGIndexForLastAsterisk = 0;
			patternGroups = new PatternGroup[0x10];
			int num3 = 15;
			PatternType nONE = PatternType.NONE;
			int index = 0;
			do
			{
				PatternGroup[] groupArray2;
				int num6;
				if (index >= num3)
				{
					PatternGroup[] array = new PatternGroup[(num3 + 0x10) + 1];
					patternGroups.CopyTo(array, 0);
					patternGroups = array;
					num3 += 0x10;
				}
				switch (pattern[patternIndex]) {
					case '*':
					case (char)0xff0a:
						if (nONE != PatternType.STAR)
						{
							nONE = PatternType.STAR;
							patternGroups[index].PatType = PatternType.STAR;
							pGIndexForLastAsterisk = index;
							index++;
						}
						break;
					case '[':
					case (char)0xff3b:
						{
							bool seenNot = false;
							List<Range> rangeList = new List<Range>();
							if (!ValidateRangePattern(pattern, patternLength, ref patternIndex, patternLigatureInfo, comparer, options, ref seenNot, ref rangeList))
							{
								patternError = true;
								return;
							}
							if (rangeList.Count != 0)
							{
								if (seenNot)
								{
									nONE = PatternType.EXCLIST;
								}
								else
								{
									nONE = PatternType.INCLIST;
								}
								patternGroups[index].PatType = nONE;
								patternGroups[index].CharCount = 1;
								patternGroups[index].RangeList = rangeList;
								index++;
							}
							break;
						}
					case '#':
					case (char)0xff03:
						if (nONE == PatternType.DIGIT)
						{
							groupArray2 = patternGroups;
							num6 = index - 1;
							groupArray2[num6].CharCount++;
						}
						else
						{
							patternGroups[index].PatType = PatternType.DIGIT;
							patternGroups[index].CharCount = 1;
							index++;
							nONE = PatternType.DIGIT;
						}
						break;
					case '?':
					case (char)0xff1f:
						if (nONE == PatternType.ANYCHAR)
						{
							groupArray2 = patternGroups;
							num6 = index - 1;
							groupArray2[num6].CharCount++;
						}
						else
						{
							patternGroups[index].PatType = PatternType.ANYCHAR;
							patternGroups[index].CharCount = 1;
							index++;
							nONE = PatternType.ANYCHAR;
						}
						break;
					default:
						{
							int num5 = patternIndex;
							int num4 = patternIndex;
							if (num4 >= patternLength)
							{
								num4 = patternLength - 1;
							}
							if (nONE == PatternType.STRING)
							{
								groupArray2 = patternGroups;
								num6 = index - 1;
								groupArray2[num6].CharCount++;
								patternGroups[index - 1].StringPatternEnd = num4;
							}
							else
							{
								patternGroups[index].PatType = PatternType.STRING;
								patternGroups[index].CharCount = 1;
								patternGroups[index].StringPatternStart = num5;
								patternGroups[index].StringPatternEnd = num4;
								index++;
								nONE = PatternType.STRING;
							}
							break;
						}
				}
				patternIndex++;
			}
			while (patternIndex < patternLength);
			patternGroups[index].PatType = PatternType.NONE;
			patternGroups[index].MinSourceIndex = sourceLength;
			int num = sourceLength;
			while (index > 0)
			{
				switch (patternGroups[index].PatType) {
					case PatternType.STRING:
						num -= patternGroups[index].CharCount;
						break;

					case PatternType.EXCLIST:
					case PatternType.INCLIST:
						num--;
						break;

					case PatternType.DIGIT:
					case PatternType.ANYCHAR:
						num -= patternGroups[index].CharCount;
						break;
				}
				patternGroups[index].MaxSourceIndex = num;
				index--;
			}
		}

		private static bool ValidateRangePattern(string pattern, int patternLength, ref int patternIndex, LigatureInfo[] patternLigatureInfo, CompareInfo comparer, CompareOptions options, ref bool seenNot, ref List<Range> rangeList)
		{
			bool flag = false;
			int sourceIndex = -1;
			bool rangePatternEmpty = false;
			bool mismatch = false;
			MatchRange(null, -1, ref sourceIndex, null, pattern, patternLength, ref patternIndex, patternLigatureInfo, ref rangePatternEmpty, ref mismatch,
			ref flag, comparer, options, ref seenNot, rangeList, true);
			return !flag;
		}

		private static void MatchRange(string source, int sourceLength, ref int sourceIndex, LigatureInfo[] sourceLigatureInfo, string pattern, int patternLength, ref int patternIndex, LigatureInfo[] patternLigatureInfo, ref bool rangePatternEmpty, ref bool mismatch,
		ref bool patternError, CompareInfo comparer, CompareOptions options, [OptionalAttribute(), DefaultParameterValueAttribute(false)]ref bool seenNot, [OptionalAttribute(), DefaultParameterValueAttribute(null)]List<Range> rangeList, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool validatePatternWithoutMatching)
		{
			Range range;
			string str2;
			int num2 = 0;
			int num3 = 0;
			int num5 = 0;
			rangePatternEmpty = false;
			mismatch = false;
			patternError = false;
			seenNot = false;
			patternIndex++;
			if (patternIndex >= patternLength)
			{
				patternError = true;
				return;
			}
			char ch = pattern[patternIndex];
			switch (ch) {
				case '!':
				case (char)0xff01:
					seenNot = true;
					patternIndex++;
					if (patternIndex >= patternLength)
					{
						mismatch = true;
						return;
					}
					ch = pattern[patternIndex];
					break;
			}
			if ((ch == ']') || (ch == 0xff3d))
			{
				if (seenNot)
				{
					seenNot = false;
					if (!validatePatternWithoutMatching)
					{
						mismatch = CompareChars(source[sourceIndex], '!', comparer, options) != 0;
					}
					if (rangeList != null)
					{
						range.Start = patternIndex - 1;
						range.StartLength = 1;
						range.End = -1;
						range.EndLength = 0;
						rangeList.Add(range);
					}
					return;
				}
				rangePatternEmpty = true;
				return;
			}
			Label_00CE:
			str2 = null;
			string right = null;
			if ((ch == ']') || (ch == 0xff3d))
			{
				mismatch = !seenNot;
				return;
			}
			if ((!validatePatternWithoutMatching && (patternLigatureInfo != null)) && (patternLigatureInfo[patternIndex].Kind == CharKind.ExpandedChar1))
			{
				if (CompareChars(source, sourceLength, sourceIndex, ref num3, sourceLigatureInfo, pattern, patternLength, patternIndex, ref num2, patternLigatureInfo,
				comparer, options, true, false) != 0)
				{
					goto Label_0145;
				}
				sourceIndex = num3;
				patternIndex = num2;
				goto Label_037F;
			}
			num2 = patternIndex;
			SkipToEndOfExpandedChar(patternLigatureInfo, patternLength, ref num2);
			Label_0145:
			range.Start = patternIndex;
			range.StartLength = (num2 - patternIndex) + 1;
			if (options == CompareOptions.Ordinal)
			{
				str2 = pattern[patternIndex].ToString();
			}
			else if ((patternLigatureInfo != null) && (patternLigatureInfo[patternIndex].Kind == CharKind.ExpandedChar1))
			{
				str2 = patternLigatureInfo[patternIndex].CharBeforeExpansion.ToString();
				patternIndex = num2;
			}
			else
			{
				str2 = pattern.Substring(patternIndex, (num2 - patternIndex) + 1);
				patternIndex = num2;
			}
			if ((((num2 + 2) >= patternLength) || ((pattern[num2 + 1] != '-') && (pattern[num2 + 1] != 0xff0d))) || ((pattern[num2 + 2] == ']') || (pattern[num2 + 2] == 0xff3d)))
			{
				if (!validatePatternWithoutMatching)
				{
					num5 = 0;
					if (CompareChars(source, sourceLength, sourceIndex, ref num3, sourceLigatureInfo, pattern, range.Start + range.StartLength, range.Start, ref num5, patternLigatureInfo,
					comparer, options, false, true) == 0)
					{
						goto Label_037F;
					}
				}
				range.End = -1;
				range.EndLength = 0;
				goto Label_0409;
			}
			patternIndex += 2;
			if ((!validatePatternWithoutMatching && (patternLigatureInfo != null)) && (patternLigatureInfo[patternIndex].Kind == CharKind.ExpandedChar1))
			{
				if (CompareChars(source, sourceLength, sourceIndex, ref num3, sourceLigatureInfo, pattern, patternLength, patternIndex, ref num2, patternLigatureInfo,
				comparer, options, true, false) != 0)
				{
					goto Label_0279;
				}
				patternIndex = num2;
				goto Label_037F;
			}
			num2 = patternIndex;
			SkipToEndOfExpandedChar(patternLigatureInfo, patternLength, ref num2);
			Label_0279:
			range.End = patternIndex;
			range.EndLength = (num2 - patternIndex) + 1;
			if (options == CompareOptions.Ordinal)
			{
				right = pattern[patternIndex].ToString();
			}
			else if ((patternLigatureInfo != null) && (patternLigatureInfo[patternIndex].Kind == CharKind.ExpandedChar1))
			{
				right = patternLigatureInfo[patternIndex].CharBeforeExpansion.ToString();
				patternIndex = num2;
			}
			else
			{
				right = pattern.Substring(patternIndex, (num2 - patternIndex) + 1);
				patternIndex = num2;
			}
			if (CompareChars(str2, right, comparer, options) > 0)
			{
				patternError = true;
				return;
			}
			if (validatePatternWithoutMatching)
			{
				goto Label_0409;
			}
			int rightEnd = 0;
			if (CompareChars(source, sourceLength, sourceIndex, ref num3, sourceLigatureInfo, pattern, range.Start + range.StartLength, range.Start, ref rightEnd, patternLigatureInfo,
			comparer, options, false, true) < 0)
			{
				goto Label_0409;
			}
			num5 = 0;
			if (CompareChars(source, sourceLength, sourceIndex, ref num3, sourceLigatureInfo, pattern, range.End + range.EndLength, range.End, ref num5, patternLigatureInfo,
			comparer, options, false, true) > 0)
			{
				goto Label_0409;
			}
			Label_037F:
			if (seenNot)
			{
				mismatch = true;
				return;
			}
			do
			{
				patternIndex++;
				if (patternIndex >= patternLength)
				{
					patternError = true;
					return;
				}
			}
			while ((pattern[patternIndex] != ']') && (pattern[patternIndex] != 0xff3d));
			sourceIndex = num3;
			return;
			Label_0409:
			if (rangeList != null)
			{
				rangeList.Add(range);
			}
			patternIndex++;
			if (patternIndex >= patternLength)
			{
				patternError = true;
			}
			else
			{
				ch = pattern[patternIndex];
				goto Label_00CE;
			}
		}

		private static int CompareChars(string left, int leftLength, int leftStart, ref int leftEnd, LigatureInfo[] leftLigatureInfo, string right, int rightLength, int rightStart, ref int rightEnd, LigatureInfo[] rightLigatureInfo,
		CompareInfo comparer, CompareOptions options, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool matchBothCharsOfExpandedCharInRight, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool useUnexpandedCharForRight)
		{
			leftEnd = leftStart;
			rightEnd = rightStart;
			if (options == CompareOptions.Ordinal)
			{
				return (left[leftStart] - right[rightStart]);
			}
			if (useUnexpandedCharForRight)
			{
				if ((rightLigatureInfo != null) && (rightLigatureInfo[rightEnd].Kind == CharKind.ExpandedChar1))
				{
					right = right.Substring(rightStart, rightEnd - rightStart);
					right = right + rightLigatureInfo[rightEnd].CharBeforeExpansion.ToString();
					rightEnd++;
					return CompareChars(left.Substring(leftStart, (leftEnd - leftStart) + 1), right, comparer, options);
				}
			}
			else if (matchBothCharsOfExpandedCharInRight)
			{
				int num2 = rightEnd;
				SkipToEndOfExpandedChar(rightLigatureInfo, rightLength, ref rightEnd);
				if (num2 < rightEnd)
				{
					int num4 = 0;
					if ((leftEnd + 1) < leftLength)
					{
						num4 = 1;
					}
					int num3 = CompareChars(left.Substring(leftStart, ((leftEnd - leftStart) + 1) + num4), right.Substring(rightStart, (rightEnd - rightStart) + 1), comparer, options);
					if (num3 == 0)
					{
						leftEnd += num4;
					}
					return num3;
				}
			}
			if ((leftEnd == leftStart) && (rightEnd == rightStart))
			{
				return comparer.Compare(left[leftStart].ToString(), right[rightStart].ToString(), options);
			}
			return CompareChars(left.Substring(leftStart, (leftEnd - leftStart) + 1), right.Substring(rightStart, (rightEnd - rightStart) + 1), comparer, options);
		}

		/// <summary>
		/// Compares the first characters of two strings
		/// </summary>
		/// <param name="left">First string</param>
		/// <param name="right">Second string</param>
		/// <param name="comparer">Compare info</param>
		/// <param name="options">Compare options</param>
		/// <returns>0 if equal, greater than 0 if first character is greater and less than 0 if second character is greater</returns>
		private static int CompareChars(string left, string right, CompareInfo comparer, CompareOptions options)
		{
			if (options == CompareOptions.Ordinal)
			{
				return (left[0] - right[0]);
			}
			return comparer.Compare(left, right, options);
		}

		/// <summary>
		/// Compares two characters
		/// </summary>
		/// <param name="left">First character</param>
		/// <param name="right">Second character</param>
		/// <param name="comparer">Compare info</param>
		/// <param name="options">Compare options</param>
		/// <returns>0 if equal, greater than 0 if first character is greater and less than 0 if second character is greater</returns>
		private static int CompareChars(char left, char right, CompareInfo comparer, CompareOptions options)
		{
			if (options == CompareOptions.Ordinal)
			{
				return (left - right);
			}
			return comparer.Compare(left.ToString(), right.ToString(), options);
		}

		/// <summary>
		/// Skips to the end of expanded character
		/// </summary>
		/// <param name="inputLigatureInfo">Input LigatureInfo</param>
		/// <param name="length">Length of the string</param>
		/// <param name="current">Current length of the string</param>
		private static void SkipToEndOfExpandedChar(LigatureInfo[] inputLigatureInfo, int length, ref int current)
		{
			if (((inputLigatureInfo != null) && (current < length)) && (inputLigatureInfo[current].Kind == CharKind.ExpandedChar1))
			{
				current++;
			}
		}

		private static void ExpandString(ref string input, ref int length, ref LigatureInfo[] inputLigatureInfo, byte[] localeSpecificLigatureTable, CompareInfo comparer, CompareOptions options, ref bool widthChanged, bool useFullWidth)
		{
			widthChanged = false;
			if (length != 0)
			{
				int num = 0;
				CultureInfo cultureInfo = Utils.CultureInfo;
				Encoding encoding = Encoding.GetEncoding(cultureInfo.TextInfo.ANSICodePage);
				int dwMapFlags = 0x100;
				bool flag = false;
				if (!encoding.IsSingleByte)
				{
					dwMapFlags = 0x400100;
					if (StringsEx.IsValidCodePage(0x3a4))
					{
						if (useFullWidth)
						{
							dwMapFlags = 0xa00100;
						}
						else
						{
							dwMapFlags = 0x600100;
						}
						input = StringsEx.vbLCMapString(cultureInfo, dwMapFlags, input);
						flag = true;
						if (input.Length != length)
						{
							length = input.Length;
							widthChanged = true;
						}
					}
				}
				if (!flag)
				{
					input = StringsEx.vbLCMapString(cultureInfo, dwMapFlags, input);
				}
				int num6 = length - 1;
				for (int i = 0; i <= num6; i++)
				{
					char ch = input[i];
					if (CanCharExpand(ch, localeSpecificLigatureTable, comparer, options) != 0)
					{
						num++;
					}
				}
				if (num > 0)
				{
					inputLigatureInfo = new LigatureInfo[((length + num) - 1) + 1];
					StringBuilder builder = new StringBuilder((length + num) - 1);
					int index = 0;
					int num7 = length - 1;
					for (int j = 0; j <= num7; j++)
					{
						char ch2 = input[j];
						if (CanCharExpand(ch2, localeSpecificLigatureTable, comparer, options) != 0)
						{
							string str = GetCharExpansion(ch2, localeSpecificLigatureTable, comparer, options);
							builder.Append(str);
							inputLigatureInfo[index].Kind = CharKind.ExpandedChar1;
							inputLigatureInfo[index].CharBeforeExpansion = ch2;
							index++;
							inputLigatureInfo[index].Kind = CharKind.ExpandedChar2;
							inputLigatureInfo[index].CharBeforeExpansion = ch2;
						}
						else
						{
							builder.Append(ch2);
						}
						index++;
					}
					input = builder.ToString();
					length = builder.Length;
				}
			}
		}

		private static string GetCharExpansion(char ch, byte[] localeSpecificLigatureTable, CompareInfo comparer, CompareOptions options)
		{
			int index = CanCharExpand(ch, localeSpecificLigatureTable, comparer, options);
			if (index == 0)
			{
				return ch.ToString();
			}
			return _ligatureExpansions[index];
		}

		private static int CanCharExpand(char ch, byte[] localeSpecificLigatureTable, CompareInfo comparer, CompareOptions options)
		{
			int num = 0;
			byte index = LigatureIndex(ch);
			if (index == 0)
			{
				return 0;
			}
			if (localeSpecificLigatureTable[index] == 0)
			{
				if (comparer.Compare(ch.ToString(), _ligatureExpansions[index]) == 0)
				{
					localeSpecificLigatureTable[index] = 1;
				}
				else
				{
					localeSpecificLigatureTable[index] = 2;
				}
			}
			if (localeSpecificLigatureTable[index] == 1)
			{
				return index;
			}
			return num;
		}

		private static byte LigatureIndex(char ch)
		{
			if ((StringsEx.Asc(ch) >= 0xc6) && (StringsEx.Asc(ch) <= 0x153))
			{
				return _ligatureMap[StringsEx.Asc(ch) - 0xc6];
			}
			return 0;
		}







	}



}
