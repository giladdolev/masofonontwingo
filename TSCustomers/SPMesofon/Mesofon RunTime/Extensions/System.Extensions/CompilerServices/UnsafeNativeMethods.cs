using System.Runtime.InteropServices;
using System.Security;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Seadled class UnsafeNativeMethods
	/// </summary>
	[ComVisibleAttribute(false), SuppressUnmanagedCodeSecurityAttribute()]
	static internal class UnsafeNativeMethods
	{
		// Fields
		public const int LCID_US_ENGLISH = 0x409;
		public const int MEMBERID_NIL = 0;

		// Methods
		//public UnsafeNativeMethods();



		/// <summary>
		/// For a locale specified by identifier, maps one input character string to another using a specified transformation, or generates a sort key for the input string
		/// </summary>
		/// <param name="locale">Locale identifier that specifies the locale.</param>
		/// <param name="dwMapFlags">Flags specifying the type of transformation to use during string mapping or the type of sort key to generate</param>
		/// <param name="lpSrcStr">String to map</param>
		/// <param name="cchSrc">Size, in characters, of the source string indicated by lpSrcStr</param>
		/// <param name="lpDestStr">Pointer to a buffer in which this function retrieves the mapped string or a sort key</param>
		/// <param name="cchDest">Size, in characters, of the destination string indicated by lpDestStr</param>
		/// <returns>Returns the number of characters or bytes in the translated string or sort key, including a terminating null character, if successful.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern int LCMapString(int locale, int dwMapFlags, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpSrcStr, int cchSrc, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpDestStr, int cchDest);

		/// <summary>
		/// For a locale specified by identifier, maps one input character string to another using a specified transformation, or generates a sort key for the input string
		/// </summary>
		/// <param name="locale">Locale identifier that specifies the locale.</param>
		/// <param name="dwMapFlags">Flags specifying the type of transformation to use during string mapping or the type of sort key to generate</param>
		/// <param name="lpSrcStr">Pointer to a source string that the function maps or uses for sort key generation. This string cannot have a size of 0</param>
		/// <param name="cchSrc">Size, in characters, of the source string indicated by lpSrcStr</param>
		/// <param name="lpDestStr">Pointer to a buffer in which this function retrieves the mapped string or a sort key</param>
		/// <param name="cchDest">Size, in characters, of the destination string indicated by lpDestStr</param>
		/// <returns>Returns the number of characters or bytes in the translated string or sort key, including a terminating null character, if successful.</returns> 
		[DllImportAttribute("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern int LCMapStringA(int locale, int dwMapFlags, [MarshalAsAttribute(UnmanagedType.LPArray)]byte[] lpSrcStr, int cchSrc, [MarshalAsAttribute(UnmanagedType.LPArray)]byte[] lpDestStr, int cchDest);







		/// <summary>
		/// Sets the current local time and date.
		/// </summary>
		/// <param name="systime">A pointer to a SYSTEMTIME structure that contains the new local date and time.</param>
		/// <returns>If the function succeeds, the return value is nonzero. If the function fails, the return value is zero.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		static internal extern int SetLocalTime(NativeTypes.SystemTime systime);





		// Nested Types

		/// <summary>
		/// Exposes objects, methods and properties to programming tools and other applications that support Automation.
		/// </summary>
		[ComImportAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020400-0000-0000-C000-000000000046")]
		public interface IDispatch
		{
			[PreserveSigAttribute()]
			int GetTypeInfoCount();
			[PreserveSigAttribute()]
			int GetTypeInfo([InAttribute()]int index, [InAttribute()]int lcid, [MarshalAsAttribute(UnmanagedType.Interface)]out ITypeInfo pTypeInfo);
			[PreserveSigAttribute()]
			int GetIDsOfNames();
			[PreserveSigAttribute()]
			int Invoke();
		}

		/// <summary>
		/// Provides access to the type information for an object's coclass entry in its type library.
		/// </summary>
		[ComImportAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never), GuidAttribute("B196B283-BAB4-101A-B69C-00AA00341D07"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
		public interface IProvideClassInfo
		{
			[return: MarshalAsAttribute(UnmanagedType.Interface)]
			ITypeInfo GetClassInfo();
		}

		/// <summary>
		/// The ITypeComp interface provides a fast way to access information that compilers need when binding to and instantiating structures and interfaces.
		/// </summary>
		[ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020403-0000-0000-C000-000000000046"), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public interface ITypeComp
		{
			void RemoteBind([InAttribute(), MarshalAsAttribute(UnmanagedType.LPWStr)]string szName, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int lHashVal, [InAttribute(), MarshalAsAttribute(UnmanagedType.U2)]short wFlags, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeInfo[] ppTInfo, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]tagDESCKIND[] pDescKind, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]tagFUNCDESC[] ppFuncDesc, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]tagVARDESC[] ppVarDesc, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeComp[] ppTypeComp, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] pDummy);
			void RemoteBindType([InAttribute(), MarshalAsAttribute(UnmanagedType.LPWStr)]string szName, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int lHashVal, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeInfo[] ppTInfo);
		}

		/// <summary>
		/// This section describes ITypeInfo, an interface typically used for reading information about objects.
		/// </summary>
		[ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020401-0000-0000-C000-000000000046"), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public interface ITypeInfo
		{
			[PreserveSigAttribute()]
			int GetTypeAttr(out IntPtr pTypeAttr);
			[PreserveSigAttribute()]
			int GetTypeComp(out ITypeComp pTComp);
			[PreserveSigAttribute()]
			int GetFuncDesc([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out IntPtr pFuncDesc);
			[PreserveSigAttribute()]
			int GetVarDesc([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out IntPtr pVarDesc);
			[PreserveSigAttribute()]
			int GetNames([InAttribute()]int memid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] rgBstrNames, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int cMaxNames, [MarshalAsAttribute(UnmanagedType.U4)]out int cNames);
			[PreserveSigAttribute()]
			int GetRefTypeOfImplType([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out int pRefType);
			[PreserveSigAttribute()]
			int GetImplTypeFlags([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, [OutAttribute()]int pImplTypeFlags);
			[PreserveSigAttribute()]
			int GetIDsOfNames([InAttribute()]IntPtr rgszNames, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int cNames, out IntPtr pMemId);
			[PreserveSigAttribute()]
			int Invoke();
			[PreserveSigAttribute()]
			int GetDocumentation([InAttribute()]int memid, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrName, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrDocString, [MarshalAsAttribute(UnmanagedType.U4)]out int pdwHelpContext, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrHelpFile);
			[PreserveSigAttribute()]
			int GetDllEntry([InAttribute()]int memid, [InAttribute()]tagINVOKEKIND invkind, [OutAttribute(), MarshalAsAttribute(UnmanagedType.BStr)]string pBstrDllName, [OutAttribute(), MarshalAsAttribute(UnmanagedType.BStr)]string pBstrName, [OutAttribute(), MarshalAsAttribute(UnmanagedType.U2)]short pwOrdinal);
			[PreserveSigAttribute()]
			int GetRefTypeInfo([InAttribute()]IntPtr hreftype, out ITypeInfo pTypeInfo);
			[PreserveSigAttribute()]
			int AddressOfMember();
			[PreserveSigAttribute()]
			int CreateInstance([InAttribute()]ref IntPtr pUnkOuter, [InAttribute()]ref Guid riid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.IUnknown)]object ppvObj);
			[PreserveSigAttribute()]
			int GetMops([InAttribute()]int memid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.BStr)]string pBstrMops);
			[PreserveSigAttribute()]
			int GetContainingTypeLib([OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeLib[] ppTLib, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] pIndex);
			[PreserveSigAttribute()]
			void ReleaseTypeAttr(IntPtr typeAttr);
			[PreserveSigAttribute()]
			void ReleaseFuncDesc(IntPtr funcDesc);
			[PreserveSigAttribute()]
			void ReleaseVarDesc(IntPtr varDesc);
		}

		/// <summary>
		/// Represents a type library, the data that describes a set of objects.
		/// </summary>
		[ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020402-0000-0000-C000-000000000046"), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public interface ITypeLib
		{
			void RemoteGetTypeInfoCount([OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] pcTInfo);
			void GetTypeInfo([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeInfo[] ppTInfo);
			void GetTypeInfoType([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]tagTYPEKIND[] pTKind);
			void GetTypeInfoOfGuid([InAttribute()]ref Guid guid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeInfo[] ppTInfo);
			void RemoteGetLibAttr([OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]tagTLIBATTR[] ppTLibAttr, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] pDummy);
			void GetTypeComp([OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeComp[] ppTComp);
			void RemoteGetDocumentation(int index, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int refPtrFlags, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] pBstrName, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] pBstrDocString, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] pdwHelpContext, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] pBstrHelpFile);
			void RemoteIsName([InAttribute(), MarshalAsAttribute(UnmanagedType.LPWStr)]string szNameBuf, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int lHashVal, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]IntPtr[] pfName, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] pBstrLibName);
			void RemoteFindName([InAttribute(), MarshalAsAttribute(UnmanagedType.LPWStr)]string szNameBuf, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int lHashVal, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]ITypeInfo[] ppTInfo, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]int[] rgMemId, [InAttribute(), OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]short[] pcFound, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] pBstrLibName);
			void LocalReleaseTLibAttr();
		}

		/// <summary>
		/// Identifies the type description being bound to.
		/// </summary>
		[EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public enum tagDESCKIND
		{

			/// <summary>
			/// Indicates that no match was found.
			/// </summary>
			DESCKIND_NONE,

			/// <summary>
			/// Indicates that a tagFUNCDESC was returned.
			/// </summary>
			DESCKIND_FUNCDESC,

			/// <summary>
			/// Indicates that a VARDESC was returned.
			/// </summary>
			DESCKIND_VARDESC,

			/// <summary>
			/// Indicates that a TYPECOMP was returned.
			/// </summary>
			DESCKIND_TYPECOMP,

			/// <summary>
			/// Indicates that an IMPLICITAPPOBJ was returned.
			/// </summary>
			DESCKIND_IMPLICITAPPOBJ,

			/// <summary>
			/// Indicates an end of enumeration marker.
			/// </summary>
			DESCKIND_MAX
		}

		/// <summary>
		/// Holds information about a function.
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct tagFUNCDESC
		{
			public int memid;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public short lprgscode;
			public IntPtr lprgelemdescParam;
			public int funckind;
			public int invkind;
			public int callconv;
			[MarshalAsAttribute(UnmanagedType.I2)]
			public short cParams;
			[MarshalAsAttribute(UnmanagedType.I2)]
			public short cParamsOpt;
			[MarshalAsAttribute(UnmanagedType.I2)]
			public short oVft;
			[MarshalAsAttribute(UnmanagedType.I2)]
			public short cScodes;
			public value_tagELEMDESC elemdescFunc;
			[MarshalAsAttribute(UnmanagedType.I2)]
			public short wFuncFlags;
		}

		/// <summary>
		/// Specifies how to invoke a function by IDispatch::Invoke Method.
		/// </summary>
		[EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public enum tagINVOKEKIND
		{

			/// <summary>
			/// The member is called using a normal function invocation syntax.
			/// </summary>
			INVOKE_FUNC = 1,

			/// <summary>
			/// The function is invoked using a normal property-access syntax.
			/// </summary>
			INVOKE_PROPERTYGET = 2,

			/// <summary>
			/// The function is invoked using a property value assignment syntax.
			/// </summary>
			INVOKE_PROPERTYPUT = 4,

			/// <summary>
			/// The function is invoked using a property reference assignment syntax.
			/// </summary>
			INVOKE_PROPERTYPUTREF = 8
		}

		/// <summary>
		/// Holds information about a function parameter.
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct tagPARAMDESC
		{
			public IntPtr pparamdescex;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public short wParamFlags;
		}

		/// <summary>
		/// Identifies the target operating system platform.
		/// </summary>
		[EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public enum tagSYSKIND
		{

			/// <summary>
			/// The target operating system for the type library is Apple Macintosh. 
			/// </summary>
			SYS_MAC = 2,

			/// <summary>
			/// The target operating system for the type library is 16-bit Windows systems.
			/// </summary>
			SYS_WIN16 = 0
		}

		/// <summary>
		/// Contains information about a type library. Information from this structure is used to identify the type library and to provide national language support for member names.
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct tagTLIBATTR
		{
			public Guid guid;
			public int lcid;
			public tagSYSKIND syskind;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public short wMajorVerNum;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public short wMinorVerNum;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public short wLibFlags;
		}

		/// <summary>
		/// Sealed class tagTYPEDESC
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public sealed class tagTYPEDESC
		{
			public IntPtr unionMember;
			public short vt;
			//public tagTYPEDESC();
		}

		/// <summary>
		/// Specifies various types of data and functions.
		/// </summary>
		[EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public enum tagTYPEKIND
		{

			/// <summary>
			/// A set of enumerators.
			/// </summary>
			TKIND_ENUM,

			/// <summary>
			/// A structure with no methods.
			/// </summary>
			TKIND_RECORD,

			/// <summary>
			/// A module that can only have static functions and data.
			/// </summary>
			TKIND_MODULE,

			/// <summary>
			/// A type that has virtual functions, all of which are pure.
			/// </summary>
			TKIND_INTERFACE,

			/// <summary>
			/// A set of methods and properties that are accessible through IDispatch::Invoke.
			/// </summary>
			TKIND_DISPATCH,

			/// <summary>
			/// A set of implemented components interfaces.
			/// </summary>
			TKIND_COCLASS,

			/// <summary>
			/// A type that is an alias for another type.
			/// </summary>
			TKIND_ALIAS,

			/// <summary>
			/// A union of all members that have an offset of zero.
			/// </summary>
			TKIND_UNION,

			/// <summary>
			/// End of enumeration marker.
			/// </summary>
			TKIND_MAX
		}

		/// <summary>
		/// Describes a variable, constant, or data member.
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct tagVARDESC
		{
			public int memid;
			public IntPtr lpstrSchema;
			public IntPtr unionMember;
			[MarshalAsAttribute(UnmanagedType.U2)]
			public value_tagELEMDESC elemdescVar;
			public short wVarFlags;
			public int varkind;
		}

		/// <summary>
		/// Holds information about a code element.
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct value_tagELEMDESC
		{
			public tagTYPEDESC tdesc;
			public tagPARAMDESC paramdesc;
		}
	}


}
