using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Security.Permissions;
using System.Security;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// ProjectData class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class ProjectData //:IDisposable
	{
		// Fields
		internal Hashtable _assemblyData = new Hashtable();
		internal ErrObject _err;
		[ThreadStaticAttribute()]
		private static ProjectData _s_oProject;

		// Methods

		/// <summary>
		/// ctor
		/// </summary>
		private ProjectData()
		{
		}

		/// <summary>
		/// Clear the project errors.
		/// </summary>
		public static void ClearProjectError()
		{
			Information.Err().Clear();
		}

		/// <summary>
		/// Performs the work for the Raise method of the Err object. A helper method.
		/// </summary>
		/// <param name="hr">An integer value that identifies the nature of the error.</param>
		/// <returns>An Exception object.</returns>
		public static Exception CreateProjectError(int hr)
		{
			ErrObject obj2 = Information.Err();
			obj2.Clear();
			int num = ErrObject.MapErrorNumber(hr);
			return obj2.CreateException(hr, Utils.GetResourceString((Utils.vbErrors)num));
		}

		/// <summary>
		/// Application will be ended.
		/// </summary>
		[SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
		public static void EndApp()
		{
			// FileSystem.CloseAllFiles(Assembly.GetCallingAssembly());
			Environment.Exit(0);
		}

		//protected override void Finalize()
		//{
		//    Marshal.FreeHGlobal(this.m_numprsPtr);
		//    Marshal.FreeHGlobal(this.m_DigitArray);
		//    this.m_numprsPtr = IntPtr.Zero;
		//    this.m_DigitArray = IntPtr.Zero;
		//}

		/// <summary>
		/// Get Assembly Data
		/// </summary>
		/// <param name="assem">Assembly object</param>
		/// <returns>AssemblyData value.</returns>
		internal AssemblyData GetAssemblyData(Assembly assem)
		{
			if (assem == Assembly.GetExecutingAssembly())
			{
				throw new SecurityException();
			}
			AssemblyData data = (AssemblyData)_assemblyData[assem];
			if (data == null)
			{
				data = new AssemblyData();
				_assemblyData[assem] = data;
			}
			return data;
		}

		/// <summary>
		/// Gets Project Data
		/// </summary>
		/// <returns>ProjectData</returns>
		static internal ProjectData GetProjectData()
		{
			ProjectData oProject = _s_oProject;
			if (oProject == null)
			{
				oProject = new ProjectData();
				_s_oProject = oProject;
			}
			return oProject;
		}

		/// <summary>
		///  To capture exceptions in the Err object.
		/// </summary>
		/// <param name="ex">The Exception object to be caught.</param>
		public static void SetProjectError(Exception ex)
		{
			Information.Err().CaptureException(ex);
		}

		/// <summary>
		/// The Visual Basic compiler uses this helper method to capture exceptions in the Err object.
		/// </summary>
		/// <param name="ex">The Exception object to be caught.</param>
		/// <param name="lErl">The line number of the exception.</param>
		public static void SetProjectError(Exception ex, int lErl)
		{
			Information.Err().CaptureException(ex, lErl);
		}

	

	}
}
