using System.ComponentModel;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Contains methods to work with binary file
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal class VB6BinaryFile : VB6RandomFile
	{
		// Methods
		public VB6BinaryFile(string fileName, FileSystem.OpenAccess access, FileSystem.OpenShare share) : base(fileName, access, share, -1)
		{
		}
		/// <summary>
		/// returns true
		/// </summary>
		/// <returns>Boolean value</returns>
		public override bool CanInput()
		{
			return true;
		}
		/// <summary>
		/// returns true
		/// </summary>
		/// <returns>Boolean value</returns>
		public override bool CanWrite()
		{
			return true;
		}
	}
}
