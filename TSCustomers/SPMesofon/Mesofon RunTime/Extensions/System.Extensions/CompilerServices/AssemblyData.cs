using System.Collections;
using System.ComponentModel;
using System.IO;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// AssemblyData class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal sealed class AssemblyData
	{
		// Fields

		/// <summary>
		///  Keeps file attributes
		/// </summary>
		internal FileAttributes _dirAttributes;
		/// <summary>
		/// Array of FileSystemInfo
		/// </summary>
		internal FileSystemInfo[] _dirFiles;

		/// <summary>
		/// Keeps the directory next file index
		/// </summary>
		internal int _dirNextFileIndex;
		/// <summary>
		/// ArrayList
		/// </summary>
		public ArrayList _files;

		// Methods
		// Methods
		/// <summary>
		/// Initialization of new empty array list
		/// </summary>
		public AssemblyData()
		{
			ArrayList list = new ArrayList(0x100);
			object obj2 = null;
			int num = 0;
			do
			{
				list.Add(obj2);
				num++;
			}
			while (num <= 0xff);
			_files = list;
		}

		/// <summary>
		/// Get Channel object by index
		/// </summary>
		/// <param name="lChannel">Required. Integer value, index of the object</param>
		/// <returns>VB6File</returns>
		internal VB6File GetChannelObj(int lChannel)
		{
			object obj2;
			if (lChannel < _files.Count)
			{
				obj2 = _files[lChannel];
			}
			else
			{
				obj2 = null;
			}
			return (VB6File)obj2;
		}

		/// <summary>
		/// Setting VB6File
		/// </summary>
		/// <param name="lChannel">Required.Integer value.</param>
		/// <param name="oFile">Required.VB6File</param>
		internal void SetChannelObj(int lChannel, VB6File oFile)
		{
			if (_files == null)
			{
				_files = new ArrayList(0x100);
			}
			if (oFile == null)
			{
				VB6File file = (VB6File)_files[lChannel];
				if (file != null)
				{
					file.CloseFile();
				}
				_files[lChannel] = null;
			}
			else
			{
				object obj2 = oFile;
				_files[lChannel] = obj2;
			}
		}
	}
}
