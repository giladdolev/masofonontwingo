using System.Extensions.VBExtensions;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Security;
using System.Reflection;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// OptionCompareAttribute class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), AttributeUsageAttribute(AttributeTargets.Parameter, Inherited = false, AllowMultiple = false)]
	public sealed class OptionCompareAttribute : Attribute
	{
		/// <summary>
		/// OptionCompareAttribute class constructor
		/// </summary>
		public OptionCompareAttribute()

		{
		}
	}

	/// <summary>
	/// DoubleType class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class DoubleType
	{
		private DoubleType()
		{

		}
		/// <summary>
		/// Converts decimal to double
		/// </summary>
		/// <param name="valueInterface">IConvertible variable</param>
		/// <returns>Double value converted from decimal</returns>
		private static double DecimalToDouble(IConvertible valueInterface)
		{
			return Convert.ToDouble(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts object to double value
		/// </summary>
		/// <param name="value">Object to be converted to double</param>
		/// <returns>Double value converted from the given object</returns>
		public static double FromObject(object value)
		{
			return FromObject(value, null);
		}

		/// <summary>
		/// Converts object to double value
		/// </summary>
		/// <param name="value">Object to be converted to double</param>
		/// <param name="numberFormat">Defines how numeric values are formatted and displayed, depending on the culture</param>
		/// <returns>Double value converted from the given object</returns>
		public static double FromObject(object value, NumberFormatInfo numberFormat)
		{
			if (value == null)
			{
				return 0.0;
			}
			
			IConvertible valueInterface = value as IConvertible;
			
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return ((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return ((byte)value);
						}
						return valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return ((short)value);
						}
						return valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return ((int)value);
						}
						return valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return ((long)value);
						}
						return valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return ((float)value);
						}
						return valueInterface.ToSingle(null);

					case TypeCode.Double:
						if (value is double)
						{
							return (double)value;
						}
						return valueInterface.ToDouble(null);

					case TypeCode.Decimal:
						return DecimalToDouble(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null), numberFormat);
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Double"));
		}

		/// <summary>
		/// Converts a string expression to double value
		/// </summary>
		/// <param name="value">String expression</param>
		/// <returns>Double value converted from string</returns>
		public static double FromString(string value)
		{
			return FromString(value, null);
		}

		/// <summary>
		/// Converts a string expression to double value
		/// </summary>
		/// <param name="value">String expression</param>
		/// <param name="numberFormat">Defines how numeric values are formatted and displayed, depending on the culture</param>
		/// <returns>Formated double value</returns>
		public static double FromString(string value, NumberFormatInfo numberFormat)
		{
			double num = default(double);
			if (value == null)
			{
				return 0.0;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return (double)num2;
				}
				num = Parse(value, numberFormat);
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Double"), exception);
			}
			return num;
		}

		/// <summary>
		/// Parse a string to double
		/// </summary>
		/// <param name="value">String expression</param>
		/// <returns>The converted string expression</returns>
		public static double Parse(string value)
		{
			return Parse(value, null);
		}

		/// <summary>
		/// Parse a string to double
		/// </summary>
		/// <param name="value">String expression</param>
		/// <param name="numberFormat">Defines how numeric values are formatted and displayed, depending on the culture</param>
		/// <returns>Double value parsed from a string</returns>
		public static double Parse(string value, NumberFormatInfo numberFormat)
		{
			value = StringType.ToHalfwidthNumbers(value);
			try
			{
				return double.Parse(value, NumberStyles.Any, (IFormatProvider)numberFormat);
			}
			catch (Exception)
			{
				if (numberFormat == null)
				{
					numberFormat = Thread.CurrentThread.CurrentCulture.NumberFormat;
				}
				if ((StringType.StrCmp(numberFormat.CurrencyDecimalSeparator, numberFormat.NumberDecimalSeparator, false) == 0) || (StringsEx.InStr(value, numberFormat.NumberDecimalSeparator, StringType.CompareMethod.Binary) == 0))
				{
					throw;
				}
				return double.Parse(value, NumberStyles.Float | NumberStyles.AllowThousands | NumberStyles.AllowParentheses | NumberStyles.AllowTrailingSign, (IFormatProvider)numberFormat);
			}
		}

		/// <summary>
		/// Try to parse string to double
		/// </summary>
		/// <param name="value">String expression</param>
		/// <param name="result">Double value parsed from a string</param>
		/// <returns>True if the string is successfully parsed to double, else False</returns>
		static internal bool TryParse(string value, ref double result)
		{
			value = StringType.ToHalfwidthNumbers(value);
			return double.TryParse(value, NumberStyles.Any, (IFormatProvider)null, out result);
		}
	}

	/// <summary>
	/// VB6File abstract class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal abstract class VB6File : IDisposable
	{

		/// <summary>
		/// EOF_CHAR constant
		/// </summary>
		protected const int EOF_CHAR = 0x1a;

		/// <summary>
		/// EOF_INDICATOR constant
		/// </summary>
		protected const int EOF_INDICATOR = -1;

		/// <summary>
		/// FIN_LINEINP constant
		/// </summary>
		protected const short FIN_LINEINP = 0;

		/// <summary>
		/// FIN_NUMBER constant
		/// </summary>
		protected const short FIN_NUMBER = 3;

		/// <summary>
		/// FIN_NUMBER constant
		/// </summary>
		protected const short FIN_NUMTERMCHAR = 6;

		/// <summary>
		/// FIN_QSTRING constant
		/// </summary>
		protected const short FIN_QSTRING = 1;

		/// <summary>
		/// FIN_STRING constant
		/// </summary>
		protected const short FIN_STRING = 2;

		/// <summary>
		/// lchComma constant
		/// </summary>
		protected const int lchComma = 0x2c;

		/// <summary>
		/// lchCR constant
		/// </summary>
		protected const int lchCR = 13;

		/// <summary>
		/// lchDoubleQuote constant
		/// </summary>
		protected const int lchDoubleQuote = 0x22;

		/// <summary>
		/// lchIntlSpace constant
		/// </summary>
		protected const int lchIntlSpace = 0x3000;

		/// <summary>
		/// lchLF constant
		/// </summary>
		protected const int lchLF = 10;

		/// <summary>
		/// lchPound constant
		/// </summary>
		protected const int lchPound = 0x23;

		/// <summary>
		/// lchSpace constant
		/// </summary>
		protected const int lchSpace = 0x20;

		/// <summary>
		/// lchTab constant
		/// </summary>
		protected const int lchTab = 9;

		/// <summary>
		/// OpenAccess variable
		/// </summary>
		internal FileSystem.OpenAccess m_access;

		/// <summary>
		/// Encoding variable
		/// </summary>
		protected Encoding m_ae;

		/// <summary>
		/// BinaryReader object
		/// </summary>
		protected BinaryReader m_br;


		/// <summary>
		/// BinaryWriter object
		/// </summary>
		protected BinaryWriter m_bw;

		/// <summary>
		/// EOF boolean
		/// </summary>
		internal bool m_eof;

		/// <summary>
		/// Append boolean
		/// </summary>
		internal bool m_fAppend;

		/// <summary>
		/// FileStream object
		/// </summary>
		internal FileStream m_file;

		/// <summary>
		/// Current Column integer variable
		/// </summary>
		internal int m_lCurrentColumn;

		/// <summary>
		/// Record Length integer variable
		/// </summary>
		internal int m_lRecordLen;

		/// <summary>
		/// Record Start long variable
		/// </summary>
		internal long m_lRecordStart;

		/// <summary>
		/// Width integer variable
		/// </summary>
		internal int m_lWidth;

		/// <summary>
		/// Position long variable
		/// </summary>
		internal long m_position;

		/// <summary>
		/// Full path string variable
		/// </summary>
		internal string m_sFullPath;

		/// <summary>
		/// OpenShare variable
		/// </summary>
		internal FileSystem.OpenShare m_share;

		/// <summary>
		/// StreamReader object
		/// </summary>
		protected StreamReader m_sr;

		/// <summary>
		/// StreamWriter object
		/// </summary>
		protected StreamWriter m_sw;

		/// <summary>
		/// VB6File class constructor
		/// </summary>
		protected VB6File()
		{
		}

		/// <summary>
		/// Supports the Visual Basic Print and PrintLine functions
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct TabInfo
		{
			public short Column;
		}

		/// <summary>
		/// Supports the Visual Basic Print and PrintLine functions
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public struct SpcInfo
		{
			public short Count;
		}

        internal virtual void Get(ref ValueType Value, long RecordNumber = 0L)
        {
            throw ExceptionUtils.VbMakeException(54);
        }
		/// <summary>
		/// VB6File class constructor
		/// </summary>
		/// <param name="sPath">Path of the file</param>
		/// <param name="access">Indicates how to open a file when calling file-access functions</param>
		/// <param name="share">Indicates how to open a file when calling file-access functions</param>
		/// <param name="lRecordLen">Used to specify the size of each record</param>
		protected VB6File(string sPath, FileSystem.OpenAccess access, FileSystem.OpenShare share, int lRecordLen)
		{
			if (((access != FileSystem.OpenAccess.Read) && (access != FileSystem.OpenAccess.ReadWrite)) && (access != FileSystem.OpenAccess.Write))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Access"));
			}
			m_access = access;
			if (((share != FileSystem.OpenShare.Shared) && (share != FileSystem.OpenShare.LockRead)) && ((share != FileSystem.OpenShare.LockReadWrite) && (share != FileSystem.OpenShare.LockWrite)))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Share"));
			}
			m_share = share;
			m_lRecordLen = lRecordLen;
			Path = sPath;
			m_sFullPath = new FileInfo(Path).FullName;
		}

		/// <summary>
		/// Path string variable
		/// </summary>
		public string Path
		{
			get;
			set;
		}

		/// <summary>
		/// Adds spaces to a string
		/// </summary>
		/// <param name="s">String expression</param>
		/// <returns>Formated string</returns>
		private static string AddSpaces(string s)
		{
			string negativeSign = Thread.CurrentThread.CurrentCulture.NumberFormat.NegativeSign;
			if (negativeSign.Length == 1)
			{
				if (s[0] == negativeSign[0])
				{
					return (s + " ");
				}
			}
			else if (StringType.StrCmp(StringsEx.Left(s, negativeSign.Length), negativeSign, false) == 0)
			{
				return (s + " ");
			}
			return (" " + s + " ");
		}
	



		public virtual bool CanInput()
		{
			return false;
		}

		public virtual bool CanWrite()
		{
			return false;
		}

		/// <summary>
		/// Checks the EOF
		/// </summary>
		/// <param name="lChar">Characters length</param>
		/// <returns>True if is EOF, else False</returns>
		protected static bool CheckEOF(int lChar)
		{
			if ((lChar != -1) && (lChar != 0x1a))
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Closes the file
		/// </summary>
		public virtual void CloseFile()
		{
			CloseTheFile();
		}

		/// <summary>
		/// Closes the file
		/// </summary>
		protected void CloseTheFile()
		{
			if (m_sw != null)
			{
				m_sw.Close();
				m_sw = null;
			}
			if (m_sr != null)
			{
				m_sr.Close();
				m_sr = null;
			}
			if (m_file != null)
			{
				m_file.Close();
				m_file = null;
			}
		}




		/// <summary>
		/// Variant type enum
		/// </summary>
		internal enum VT : short
		{
			/// <summary>
			/// Array
			/// </summary>
			Array = 0x2000,

			/// <summary>
			/// Boolean. (True or False.)
			/// </summary>
			Boolean = 11,

			/// <summary>
			/// Byte.
			/// </summary>
			Byte = 0x11,

			/// <summary>
			/// ByteArray.
			/// </summary>
			ByteArray = 0x2011,

			/// <summary>
			/// Char.
			/// </summary>
			Char = 0x12,

			/// <summary>
			/// CharArray.
			/// </summary>
			CharArray = 0x2012,

			/// <summary>
			/// Currency.
			/// </summary>
			Currency = 6,

			/// <summary>
			/// Date.
			/// </summary>
			Date = 7,

			/// <summary>
			/// DBNull.
			/// </summary>
			DBNull = 1,

			/// <summary>
			/// Decimal.
			/// </summary>
			Decimal = 14,

			/// <summary>
			/// Double.
			/// </summary>
			Double = 5,

			/// <summary>
			/// Null reference. 
			/// </summary>
			Empty = 0,

			/// <summary>
			/// Null reference. 
			/// </summary>
			Error = 10,

			/// <summary>
			/// Integer.
			/// </summary>
			Integer = 3,

			/// <summary>
			/// Long.
			/// </summary>
			Long = 20,

			/// <summary>
			/// Shrot.
			/// </summary>
			Short = 2,

			/// <summary>
			/// Single.
			/// </summary>
			Single = 4,

			/// <summary>
			/// String.
			/// </summary>
			String = 8,

			/// <summary>
			/// Structure.
			/// </summary>
			Structure = 0x24,

			/// <summary>
			/// Variant.
			/// </summary>
			Variant = 12
		}

		/// <summary>
		/// Get the EOF
		/// </summary>
		/// <returns>True if it is the EOF, else False</returns>
		public virtual bool EOF()
		{
			return m_eof;
		}


		/// <summary>
		/// Get the absolute path
		/// </summary>
		/// <returns>The absolute path</returns>
		public string GetAbsolutePath()
		{
			return m_sFullPath;
		}

		/// <summary>
		/// Get the column
		/// </summary>
		/// <returns>Current column number</returns>
		public int GetColumn()
		{
			return m_lCurrentColumn;
		}

		private static string GetFileInTerm(short iTermType)
		{
			switch (iTermType) {
				case 0:
					return "\r";

				case 1:
					return "\"";

				case 2:
					return ",\r";

				case 3:
					return " ,\t\r";

				case 6:
					return " ,\t\r";
			}
			throw ExceptionUtils.VbMakeException(5);
		}

		/// <summary>
		/// Gets fixed length string
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="byteLength">The length in bytes</param>
		/// <returns>Fixed length string</returns>
		public virtual string GetFixedLengthString(long recordNumber, int byteLength)
		{
			SetRecord(recordNumber);
			return ReadString(byteLength);
		}


		/// <summary>
		/// Gets length prefixed string
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <returns>Length prefixed string</returns>
		public virtual string GetLengthPrefixedString(long recordNumber)
		{
			SetRecord(recordNumber);
			if (EOF())
			{
				return "";
			}
			return ReadString();
		}

		/// <summary>
		/// Gets the OpenMode
		/// </summary>
		/// <returns></returns>
		public abstract FileSystem.OpenMode GetMode();

		/// <summary>
		/// Gets an object
		/// </summary>
		/// <param name="value">Object</param>
		/// <param name="recordNumber">Record number</param>
		/// <param name="containedInVariant">Contained in variant boolean value</param>
		public virtual void GetObject<T>(ref T value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber, [OptionalAttribute(), DefaultParameterValueAttribute(true)]bool containedInVariant)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Gets the position
		/// </summary>
		/// <returns>Long value representing the current position</returns>
		public long GetPos()
		{
			return m_position;
		}

		/// <summary>
		/// Gets a stream
		/// </summary>
		/// <returns>Stream object</returns>
		public Stream GetStream()
		{
			return m_file;
		}

		/// <summary>
		/// Gets a StreamReader
		/// </summary>
		/// <returns>StreamReader object</returns>
		public virtual StreamReader GetStreamReader()
		{
			return m_sr;
		}


		/// <summary>
		/// Gets the width
		/// </summary>
		/// <returns>Integer value representing the width</returns>
		public int GetWidth()
		{
			return m_lWidth;
		}

		/// <summary>
		/// Reads data from an open sequential file and assigns the data to variables
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		public virtual void Input<T>(ref T value)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Inputs a number
		/// </summary>
		/// <param name="vt">Variant Type</param>
		/// <returns>Object</returns>
		protected virtual object InputNum(VariantType vt)
		{
			SkipWhiteSpaceEOF();
			object obj2 = ReadInField(3);
			SkipTrailingWhiteSpace();
			return obj2;
		}

		/// <summary>
		/// Inputs an object
		/// </summary>
		/// <param name="value">Reference to an object</param>
		protected virtual void InputObject(ref object value)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Returns String value containing characters from a file opened in Input or Binary mode
		/// </summary>
		/// <returns>Returns String value containing characters from a file opened in Input or Binary mode</returns>
		protected virtual string InputStr()
		{
			string str;
			if (SkipWhiteSpaceEOF() == 0x22)
			{
				m_sr.Read();
				m_position += 1L;
				str = ReadInField(1);
			}
			else
			{
				str = ReadInField(2);
			}
			SkipTrailingWhiteSpace();
			return str;
		}

		/// <summary>
		/// Returns String value containing characters from a file opened in Input or Binary mode
		/// </summary>
		/// <param name="lLen">Any valid numeric expression specifying the number of characters to read</param>
		/// <returns>Returns String value containing characters from a file opened in Input or Binary mode</returns>
		public string InputString(int lLen)
		{
			StringBuilder builder = new StringBuilder(lLen);
			FileSystem.OpenMode mode = GetMode();
			int num3 = lLen;
			for (int i = 1; i <= num3; i++)
			{
				int num2;
				if (mode == FileSystem.OpenMode.Binary)
				{
					num2 = m_br.Read();
					m_position += 1L;
					if (num2 == -1)
					{
						break;
					}
				}
				else
				{
					if (mode != FileSystem.OpenMode.Input)
					{
						throw ExceptionUtils.VbMakeException(0x36);
					}
					num2 = m_sr.Read();
					m_position += 1L;
					if ((num2 == -1) | (num2 == 0x1a))
					{
						m_eof = true;
						throw ExceptionUtils.VbMakeException(0x3e);
					}
				}
				if (num2 != 0)
				{
					builder.Append(StringsEx.ChrW(num2));
				}
			}
			if (mode == FileSystem.OpenMode.Binary)
			{
				m_eof = m_br.PeekChar() == -1;
			}
			else
			{
				m_eof = CheckEOF(m_sr.Peek());
			}
			return builder.ToString();
		}

		//private void InternalWriteHelper(params object[] Output)
		//{
		//    Type type2 = typeof(SpcInfo);
		//    Type type = type2;
		//    NumberFormatInfo numberFormat = Utils.GetInvariantCultureInfo().NumberFormat;
		//    int upperBound = Output.GetUpperBound(0);
		//    for (int i = 0; i <= upperBound; i++)
		//    {
		//        object obj2 = Output[i];
		//        if (obj2 == null)
		//        {
		//            this.WriteString("#ERROR 448#");
		//            continue;
		//        }
		//        if (type != type2)
		//        {
		//            this.WriteString(",");
		//        }
		//        type = obj2.GetType();
		//        if (type == type2)
		//        {
		//            SpcInfo info3 = (SpcInfo)obj2;
		//            this.SPC(info3.Count);
		//            continue;
		//        }
		//        if (type == typeof(TabInfo))
		//        {
		//            TabInfo ti = (TabInfo)obj2;
		//            if (ti.Column >= 0)
		//            {
		//                this.PrintTab(ti);
		//            }
		//            continue;
		//        }
		//        if (type == typeof(Missing))
		//        {
		//            this.WriteString("#ERROR 448#");
		//            continue;
		//        }
		//        switch (Type.GetTypeCode(type))
		//        {
		//            case TypeCode.DBNull:
		//                {
		//                    this.WriteString("#NULL#");
		//                    continue;
		//                }
		//            case TypeCode.Boolean:
		//                {
		//                    if (!BooleanType.FromObject(obj2))
		//                    {
		//                        break;
		//                    }
		//                    this.WriteString("#TRUE#");
		//                    continue;
		//                }
		//            case TypeCode.Char:
		//                {
		//                    this.WriteString(StringType.FromChar(CharType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.Byte:
		//                {
		//                    this.WriteString(StringType.FromByte(ByteType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.Int16:
		//                {
		//                    this.WriteString(StringType.FromInteger(ShortType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.Int32:
		//                {
		//                    this.WriteString(StringType.FromInteger(IntegerType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.Int64:
		//                {
		//                    this.WriteString(StringType.FromLong(LongType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.Single:
		//                {
		//                    this.WriteString(this.IOStrFromSingle(SingleType.FromObject(obj2), numberFormat));
		//                    continue;
		//                }
		//            case TypeCode.Double:
		//                {
		//                    this.WriteString(this.IOStrFromDouble(DoubleType.FromObject(obj2), numberFormat));
		//                    continue;
		//                }
		//            case TypeCode.Decimal:
		//                {
		//                    this.WriteString(this.IOStrFromDecimal(DecimalType.FromObject(obj2), numberFormat));
		//                    continue;
		//                }
		//            case TypeCode.DateTime:
		//                {
		//                    this.WriteString(this.FormatUniversalDate(DateType.FromObject(obj2)));
		//                    continue;
		//                }
		//            case TypeCode.String:
		//                {
		//                    this.WriteString(this.GetQuotedString(obj2.ToString()));
		//                    continue;
		//                }
		//            default:
		//                goto Label_0262;
		//        }
		//        this.WriteString("#FALSE#");
		//        continue;
		//    Label_0262:
		//        if ((obj2 is char[]) && (((Array)obj2).Rank == 1))
		//        {
		//            this.WriteString(new string(CharArrayType.FromObject(obj2)));
		//        }
		//        else
		//        {
		//            throw ExceptionUtils.VbMakeException(5);
		//        }
		//    }
		//}

		/// <summary>
		/// Check if it is a comma
		/// </summary>
		/// <param name="lch">Char converted into an integer value</param>
		/// <returns>True if it is a comma, False if not</returns>
		protected static bool IntlIsComma(int lch)
		{
			return (lch == 0x2c);
		}

		/// <summary>
		/// Check if it is double quote
		/// </summary>
		/// <param name="lch">Char converted into an integer value</param>
		/// <returns>True if it is a double quote, False if not</returns>
		protected static bool IntlIsDoubleQuote(int lch)
		{
			return (lch == 0x22);
		}

		/// <summary>
		/// Check if it is space
		/// </summary>
		/// <param name="lch">Char converted into an integer value</param>
		/// <returns>True if it is space, else False</returns>
		protected static bool IntlIsSpace(int lch)
		{
			return ((lch == 0x20) | (lch == 0x3000));
		}

		//private string IOStrFromDecimal(decimal Value, NumberFormatInfo NumberFormat)
		//{
		//    return Value.ToString("G29", NumberFormat);
		//}

		//private string IOStrFromDouble(double Value, NumberFormatInfo NumberFormat)
		//{
		//    return Value.ToString(null, NumberFormat);
		//}

		//private string IOStrFromSingle(float Value, NumberFormatInfo NumberFormat)
		//{
		//    return Value.ToString(null, NumberFormat);
		//}

		/// <summary>
		/// Checks the length
		/// </summary>
		/// <param name="length">Length as an integer value</param>
		public void LengthCheck(int length)
		{
			if (m_lRecordLen != -1)
			{
				if (length > m_lRecordLen)
				{
					throw ExceptionUtils.VbMakeException(0x3b);
				}
				if ((GetPos() + length) > (m_lRecordStart + m_lRecordLen))
				{
					throw ExceptionUtils.VbMakeException(0x3b);
				}
			}
		}

		/// <summary>
		/// Reads a single line from an open sequential file
		/// </summary>
		/// <returns>Single line from an open sequential file</returns>
		public string LineInput()
		{
			string s = m_sr.ReadLine();
			if (s == null)
			{
				s = "";
			}
			m_position += m_ae.GetByteCount(s) + 2;
			m_eof = CheckEOF(m_sr.Peek());
			return s;
		}

		/// <summary>
		/// Returns a Long value specifying the current read/write position within an open file
		/// </summary>
		/// <returns>Long value specifying the current read/write position</returns>
		public virtual long LOC()
		{
			if ((m_lRecordLen == -1) || (GetMode() != FileSystem.OpenMode.Random))
			{
				return (m_position + 1L);
			}
			if (m_lRecordLen == 0)
			{
				throw ExceptionUtils.VbMakeException(0x33);
			}
			if (m_position == 0L)
			{
				return 0L;
			}
			return ((m_position / ((long)m_lRecordLen)) + 1L);
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function
		/// </summary>
		public virtual void Lock()
		{
			m_file.Lock(0L, 0x7fffffffL);
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function
		/// </summary>
		/// <param name="record">Record number</param>
		public virtual void Lock(long record)
		{
			if (m_lRecordLen == -1)
			{
				m_file.Lock(record - 1L, 1L);
			}
			else
			{
				m_file.Lock((record - 1L) * m_lRecordLen, (long)m_lRecordLen);
			}
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function.
		/// </summary>
		/// <param name="recordStart">Number of the first record or byte to lock or unlock</param>
		/// <param name="recordEnd">Number of the last record or byte to lock or unlock</param>
		public virtual void Lock(long recordStart, long recordEnd)
		{
			if (m_lRecordLen == -1)
			{
				m_file.Lock(recordStart - 1L, recordEnd - recordStart);
			}
			else
			{
				m_file.Lock((recordStart - 1L) * m_lRecordLen, (recordEnd - recordStart) * m_lRecordLen);
			}
		}

		/// <summary>
		/// Returns a Long representing the size, in bytes, of a file opened using the FileOpen function
		/// </summary>
		/// <returns></returns>
		public long LOF()
		{
			return m_file.Length;
		}

		/// <summary>
		/// Opens a file
		/// </summary>
		public virtual void OpenFile()
		{
			try
			{
				if (File.Exists(m_sFullPath))
				{
					m_file = new FileStream(m_sFullPath, FileMode.Open, (FileAccess)m_access, (FileShare)m_share);
				}
				else
				{
					m_file = new FileStream(m_sFullPath, FileMode.Create, (FileAccess)m_access, (FileShare)m_share);
				}
			}
			catch (SecurityException)
			{
				throw ExceptionUtils.VbMakeException(0x35);
			}
		}

		/// <summary>
		/// Writes display-formatted data to a sequential file
		/// </summary>
		/// <param name="output">Zero or more comma-delimited expressions to write to a file</param>
		public void Print(params object[] output)
		{
			SetPrintMode();
			if ((output != null) && (output.Length != 0))
			{
				int upperBound = output.GetUpperBound(0);
				int num2 = -1;
				int num5 = upperBound;
				for (int i = 0; i <= num5; i++)
				{
					Type underlyingType;
					string s = null;
					object obj2 = output[i];
					if (obj2 == null)
					{
						underlyingType = null;
					}
					else
					{
						underlyingType = obj2.GetType();
						if (underlyingType.IsEnum)
						{
							underlyingType = Enum.GetUnderlyingType(underlyingType);
						}
					}
					if (obj2 == null)
					{
						s = "";
					}
					if (underlyingType == null)
					{
						s = "";
					}
					else
					{
						switch (Type.GetTypeCode(underlyingType)) {
							case TypeCode.DBNull:
								s = "Null";
								goto Label_027E;

							case TypeCode.Boolean:
								s = StringType.FromBoolean(BooleanType.FromObject(obj2));
								goto Label_027E;

							case TypeCode.Char:
								s = StringType.FromChar(CharType.FromObject(obj2));
								goto Label_027E;

							case TypeCode.Byte:
								s = AddSpaces(StringType.FromByte(ByteType.FromObject(obj2)));
								goto Label_027E;

							case TypeCode.Int16:
								s = AddSpaces(StringType.FromInteger(ShortType.FromObject(obj2)));
								goto Label_027E;

							case TypeCode.Int32:
								s = AddSpaces(StringType.FromInteger(IntegerType.FromObject(obj2)));
								goto Label_027E;

							case TypeCode.Int64:
								s = AddSpaces(StringType.FromLong(LongType.FromObject(obj2)));
								goto Label_027E;

							case TypeCode.Single:
								s = AddSpaces(StringType.FromSingle(SingleType.FromObject(obj2), CultureInfo.InvariantCulture));
								goto Label_027E;

							case TypeCode.Double:
								s = AddSpaces(StringType.FromDouble(DoubleType.FromObject(obj2), CultureInfo.InvariantCulture));
								goto Label_027E;

							case TypeCode.Decimal:
								s = AddSpaces(StringType.FromDecimal(DecimalType.FromObject(obj2, CultureInfo.InvariantCulture), CultureInfo.InvariantCulture));
								goto Label_027E;

							case TypeCode.DateTime:
								s = StringType.FromDate(DateType.FromObject(obj2)) + " ";
								goto Label_027E;

							case TypeCode.String:
								s = obj2.ToString();
								goto Label_027E;
						}
						if (underlyingType == typeof(TabInfo))
						{
							PrintTab((TabInfo)obj2);
							num2 = i;
							goto Label_02A5;
						}
						if (underlyingType == typeof(SpcInfo))
						{
							SpcInfo info3 = (SpcInfo)obj2;
							SPC(info3.Count);
							num2 = i;
							goto Label_02A5;
						}
						if (underlyingType != typeof(Missing))
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_UnsupportedIOType1", Utils.VBFriendlyName(underlyingType)));
						}
						s = "Error 448";
					}
					Label_027E:
					if (num2 != (i - 1))
					{
						int column = GetColumn();
						SetColumn(column + (14 - (column % 14)));
					}
					WriteString(s);
					Label_02A5:;
				}
			}
		}

		/// <summary>
		/// Writes display-formatted data to a sequential file
		/// </summary>
		/// <param name="output">Zero or more comma-delimited expressions to write to a file</param>
		public void PrintLine(params object[] output)
		{
			Print(output);
			WriteLine(null);
		}


		private void PrintTab(TabInfo ti)
		{
			if (ti.Column == -1)
			{
				int column = GetColumn();
				column += 14 - (column % 14);
				SetColumn(column);
			}
			else
			{
				Tab(ti.Column);
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Bool value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(bool value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Bool value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(byte value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Bool value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(char value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Bool value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(DateTime value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Decimal value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(decimal value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Double value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(double value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Short value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(short value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Integer value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(int value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Long value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(long value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Object to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(object value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Float value to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(float value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">ValueType to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(ValueType value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">String to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(string value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool stringIsFixedLength)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Writes data from a variable to a disk file
		/// </summary>
		/// <param name="value">Array to be written</param>
		/// <param name="recordNumber">Record number at which writing begins</param>
		public virtual void Put(Array value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool arrayIsDynamic, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool stringIsFixedLength)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		/// <summary>
		/// Puts array description
		/// </summary>
		/// <param name="arr">Array object</param>
		private void PutArrayDesc(Array arr)
		{
			short rank;
			if (arr == null)
			{
				rank = 0;
			}
			else
			{
				rank = (short)arr.Rank;
			}
			m_bw.Write(rank);
			m_position += 2L;
			if (rank != 0)
			{
				int num3 = rank - 1;
				for (int i = 0; i <= num3; i++)
				{
					m_bw.Write(arr.GetLength(i));
					m_bw.Write(arr.GetLowerBound(i));
					m_position += 8L;
				}
			}
		}

		/// <summary>
		/// Puts a boolean
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="b">The Boolean that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutBoolean(long recordNumber, bool b, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 2;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)11);
			}
			if (b)
			{
				m_bw.Write((short)(-1));
			}
			else
			{
				m_bw.Write((short)0);
			}
			m_position += length;
		}

		/// <summary>
		/// Puts a byte
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="byt">The byte that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutByte(long recordNumber, byte byt, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 1;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)0x11);
			}
			m_bw.Write(byt);
			m_position += length;
		}

		/// <summary>
		/// Puts a char
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="ch">The character that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutChar(long recordNumber, char ch, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 2;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)0x12);
			}
			m_bw.Write(ch);
			m_position += length;
		}


		/// <summary>
		/// Puts a date
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="dt">The date that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutDate(long recordNumber, DateTime dt, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 8;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)7);
			}
			double num = dt.ToOADate();
			m_bw.Write(num);
			m_position += length;
		}

		/// <summary>
		/// Puts a decimal value
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="dec">The decimal value that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutDecimal(long recordNumber, decimal dec, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			byte num7 = default(byte);
			int length = 0x10;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)14);
			}
			int[] bits = decimal.GetBits(dec);
			byte num = (byte)((bits[3] & 0x7fffffff) / 0x10000);
			int num3 = bits[0];
			int num4 = bits[1];
			int num2 = bits[2];
			if ((bits[3] & -2147483648) != 0)
			{
				num7 = 0x80;
			}
			m_bw.Write((short)14);
			m_bw.Write(num);
			m_bw.Write(num7);
			m_bw.Write(num2);
			m_bw.Write(num3);
			m_bw.Write(num4);
			m_position += length;
		}

		/// <summary>
		/// Puts a double value
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="dbl">The double value that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutDouble(long recordNumber, double dbl, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 8;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)5);
			}
			m_bw.Write(dbl);
			m_position += length;
		}

		/// <summary>
		/// Puts a dynamic array
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="arr">The array that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		/// <param name="fixedStringLength">The length of the fixed string</param>
		public void PutDynamicArray(long recordNumber, Array arr, [OptionalAttribute(), DefaultParameterValueAttribute(true)]bool containedInVariant, [OptionalAttribute(), DefaultParameterValueAttribute(-1)]int fixedStringLength)
		{
			int rank;
			if (arr == null)
			{
				rank = 0;
			}
			else
			{
				rank = arr.Rank;
				arr.GetUpperBound(0);
			}
			switch (rank) {
				case 1:
					break;

				case 2:
					break;

				case 0:
					break;

				default:
					throw new ArgumentException(Utils.GetResourceString("Argument_UnsupportedArrayDimensions"));
			}
			SetRecord(recordNumber);
			if (containedInVariant)
			{
				VT vt = VTType(arr);
				m_bw.Write((short)vt);
				m_position += 2L;
				if ((vt & VT.Array) == VT.Empty)
				{
					throw ExceptionUtils.VbMakeException(0x1ca);
				}
			}
			PutArrayDesc(arr);
			if (rank != 0)
			{
				// this.PutArrayData(arr, arr.GetType().GetElementType(), FixedStringLength, upperBound, num3);
			}
		}



		/// <summary>
		/// Puts fixed array
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="arr">The array that should be added</param>
		/// <param name="elementType">Type of the elements</param>
		/// <param name="fixedStringLength">Length of the fixed string</param>
		/// <param name="firstBound">Integer representing the first bound</param>
		/// <param name="secondBound">Integer representing the second bound</param>
		public void PutFixedArray(long recordNumber, Array arr, Type elementType, [OptionalAttribute(), DefaultParameterValueAttribute(-1)]int fixedStringLength, [OptionalAttribute(), DefaultParameterValueAttribute(-1)]int firstBound, [OptionalAttribute(), DefaultParameterValueAttribute(-1)]int secondBound)
		{
			SetRecord(recordNumber);
			if (elementType == null)
			{
				elementType = arr.GetType().GetElementType();
			}
			//this.PutArrayData(arr, ElementType, FixedStringLength, FirstBound, SecondBound);
		}

		/// <summary>
		/// Puts fixed length strings
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="s">String expression that should be added</param>
		/// <param name="length">The length of the string</param>
		public void PutFixedLengthString(long recordNumber, string s, int length)
		{
			char character = ' ';
			if (s == null)
			{
				s = "";
			}
			if (StringType.StrCmp(s, "", false) == 0)
			{
				character = '\0';
			}
			int byteCount = m_ae.GetByteCount(s);
			if (byteCount > length)
			{
				if (byteCount == s.Length)
				{
					s = StringsEx.Left(s, length);
				}
				else
				{
					byte[] bytes = m_ae.GetBytes(s);
					s = m_ae.GetString(bytes, 0, length);
					byteCount = m_ae.GetByteCount(s);
				}
			}
			if (byteCount < length)
			{
				s = s + StringsEx.StrDup(length - byteCount, character);
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			m_sw.Write(s);
			m_position += length;
		}

		/// <summary>
		/// Puts an integer value
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="l">Integer that shold be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutInteger(long recordNumber, int l, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 4;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)3);
			}
			m_bw.Write(l);
			m_position += length;
		}

		/// <summary>
		/// Puts a long value
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="l">Long value that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutLong(long recordNumber, long l, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 8;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)20);
			}
			m_bw.Write(l);
			m_position += length;
		}

		/// <summary>
		/// Puts an object
		/// </summary>
		/// <param name="value">Object that should be added</param>
		/// <param name="recordNumber">Record number</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public virtual void PutObject(object value, [OptionalAttribute(), DefaultParameterValueAttribute(0L)]long recordNumber, [OptionalAttribute(), DefaultParameterValueAttribute(true)]bool containedInVariant)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}


		/// <summary>
		/// Puts a short value
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="i">Short value that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutShort(long recordNumber, short i, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 2;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)2);
			}
			m_bw.Write(i);
			m_position += length;
		}

		/// <summary>
		/// Puts a single value 
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="sng">Float value that should be added</param>
		/// <param name="containedInVariant">Boolean value representing if contained in variant</param>
		public void PutSingle(long recordNumber, float sng, [OptionalAttribute(), DefaultParameterValueAttribute(false)]bool containedInVariant)
		{
			int length = 4;
			if (containedInVariant)
			{
				length += 2;
			}
			SetRecord(recordNumber);
			LengthCheck(length);
			if (containedInVariant)
			{
				m_bw.Write((short)4);
			}
			m_bw.Write(sng);
			m_position += length;
		}


		/// <summary>
		/// Puts a string value with length
		/// </summary>
		/// <param name="recordNumber">Record number</param>
		/// <param name="s">String expression that should be added</param>
		public void PutStringWithLength(long recordNumber, string s)
		{
			if (s == null)
			{
				s = "";
			}
			int byteCount = m_ae.GetByteCount(s);
			SetRecord(recordNumber);
			LengthCheck(byteCount + 2);
			m_bw.Write((short)byteCount);
			if (byteCount != 0)
			{
				m_sw.Write(s);
			}
			m_position += byteCount + 2;
		}


		/// <summary>
		/// Read a field
		/// </summary>
		/// <param name="iTermType">Short value representing the term type</param>
		/// <returns></returns>
		protected string ReadInField(short iTermType)
		{
			StringBuilder builder = new StringBuilder();
			string fileInTerm = GetFileInTerm(iTermType);
			int lChar = m_sr.Peek();
			if (!CheckEOF(lChar))
			{
				while (fileInTerm.IndexOf(StringsEx.ChrW(lChar)) == -1)
				{
					lChar = m_sr.Read();
					m_position += 1L;
					if (lChar != 0)
					{
						builder.Append(StringsEx.ChrW(lChar));
					}
					lChar = m_sr.Peek();
					if (CheckEOF(lChar))
					{
						m_eof = true;
						break;
					}
				}
			}
			else
			{
				m_eof = true;
			}
			if ((iTermType == 2) || (iTermType == 3))
			{
				return StringsEx.RTrim(builder.ToString());
			}
			return builder.ToString();
		}

		/// <summary>
		/// Reads a string
		/// </summary>
		/// <returns>String that was read</returns>
		protected string ReadString()
		{
			int length = m_br.ReadInt16();
			m_position += 2L;
			if (length == 0)
			{
				return null;
			}
			LengthCheck(length);
			return ReadString(length);
		}

		/// <summary>
		/// Reads a string
		/// </summary>
		/// <param name="byteLength">Length of the string in bytes</param>
		/// <returns>String that was read</returns>
		protected string ReadString(int byteLength)
		{
			if (byteLength == 0)
			{
				return null;
			}
			byte[] bytes = m_br.ReadBytes(byteLength);
			m_position += byteLength;
			return m_ae.GetString(bytes);
		}

		/// <summary>
		/// Specifies the current read/write position within a file opened using the FileOpen function
		/// </summary>
		/// <returns>Long value specifying the current read/write position within a file opened using the FileOpen function</returns>
		public virtual long Seek()
		{
			return (m_position + 1L);
		}

		/// <summary>
		/// Sets the position for the next read/write operation within a file opened using the FileOpen function
		/// </summary>
		/// <param name="baseOnePosition">Number that indicates where the next read/write operation should occur</param>
		public virtual void Seek(long baseOnePosition)
		{
			if (baseOnePosition <= 0L)
			{
				throw ExceptionUtils.VbMakeException(0x3f);
			}
			long num = baseOnePosition - 1L;
			if (num > m_file.Length)
			{
				m_file.SetLength(num);
			}
			m_file.Position = num;
			m_position = num;
			m_eof = m_position >= m_file.Length;
		}

		/// <summary>
		/// Sets the seek offset
		/// </summary>
		/// <param name="offset">Long value representing the offset</param>
		public void SeekOffset(long offset)
		{
			m_position = offset;
			m_file.Position = offset;
		}

		/// <summary>
		/// Sets the number of columns
		/// </summary>
		/// <param name="lColumn">Number of columns</param>
		public void SetColumn(int lColumn)
		{
			if (((m_lWidth != 0) && (m_lCurrentColumn != 0)) && ((lColumn + 14) > m_lWidth))
			{
				WriteLine(null);
			}
			else
			{
				SPC(lColumn - m_lCurrentColumn);
			}
		}

		/// <summary>
		/// Sets the printing mode
		/// </summary>
		public void SetPrintMode()
		{
			switch (GetMode()) {
				case FileSystem.OpenMode.Input:
				case FileSystem.OpenMode.Binary:
				case FileSystem.OpenMode.Random:
					throw ExceptionUtils.VbMakeException(0x36);
			}
		}

		/// <summary>
		/// Sets the record
		/// </summary>
		/// <param name="recordNumber">Record number to be set</param>
		public void SetRecord(long recordNumber)
		{
			if ((m_lRecordLen != 0) && (recordNumber != 0L))
			{
				long pos = 0;
				if (m_lRecordLen == -1)
				{
					if (recordNumber == -1L)
					{
						return;
					}
					pos = recordNumber - 1L;
				}
				else if (recordNumber == -1L)
				{
					pos = GetPos();
					if (pos == 0L)
					{
						m_lRecordStart = 0L;
						return;
					}
					if ((pos % ((long)m_lRecordLen)) == 0L)
					{
						m_lRecordStart = pos;
						return;
					}
					pos = m_lRecordLen * ((pos / ((long)m_lRecordLen)) + 1L);
				}
				else if (recordNumber != 0L)
				{
					if (m_lRecordLen == -1)
					{
						pos = recordNumber;
					}
					else
					{
						pos = (recordNumber - 1L) * m_lRecordLen;
					}
				}
				SeekOffset(pos);
				m_lRecordStart = pos;
			}
		}

		/// <summary>
		/// Sets the record width
		/// </summary>
		/// <param name="recordWidth">Integer value representing the record width</param>
		public void SetWidth(int recordWidth)
		{
			if ((recordWidth < 0) || (recordWidth > 0xff))
			{
				throw ExceptionUtils.VbMakeException(5);
			}
			m_lWidth = recordWidth;
		}



		/// <summary>
		/// Skips the trailing white space
		/// </summary>
		protected void SkipTrailingWhiteSpace()
		{
			int lChar = m_sr.Peek();
			if (CheckEOF(lChar))
			{
				m_eof = true;
			}
			else
			{
				if ((IntlIsSpace(lChar) || IntlIsDoubleQuote(lChar)) || (lChar == 9))
				{
					lChar = m_sr.Read();
					m_position += 1L;
					lChar = m_sr.Peek();
					if (!CheckEOF(lChar))
					{
						while (IntlIsSpace(lChar) || (lChar == 9))
						{
							m_sr.Read();
							m_position += 1L;
							lChar = m_sr.Peek();
							if (CheckEOF(lChar))
							{
								m_eof = true;
								return;
							}
						}
					}
					else
					{
						m_eof = true;
						return;
					}
				}
				if (lChar == 13)
				{
					lChar = m_sr.Read();
					m_position += 1L;
					if (CheckEOF(lChar))
					{
						m_eof = true;
						return;
					}
					if (m_sr.Peek() == 10)
					{
						lChar = m_sr.Read();
						m_position += 1L;
					}
				}
				else if (IntlIsComma(lChar))
				{
					lChar = m_sr.Read();
					m_position += 1L;
				}
				lChar = m_sr.Peek();
				if (CheckEOF(lChar))
				{
					m_eof = true;
				}
			}
		}

		/// <summary>
		/// Skips over the white space
		/// </summary>
		/// <returns>Integer value with position</returns>
		protected int SkipWhiteSpace()
		{
			int lChar = m_sr.Peek();
			if (!CheckEOF(lChar))
			{
				while (IntlIsSpace(lChar) || (lChar == 9))
				{
					m_sr.Read();
					m_position += 1L;
					lChar = m_sr.Peek();
					if (CheckEOF(lChar))
					{
						m_eof = true;
						return lChar;
					}
				}
				return lChar;
			}
			m_eof = true;
			return lChar;
		}

		/// <summary>
		/// Skips over the white space
		/// </summary>
		/// <returns>Integer value with position</returns>
		protected int SkipWhiteSpaceEOF()
		{
			int lChar = SkipWhiteSpace();
			if (CheckEOF(lChar))
			{
				throw ExceptionUtils.VbMakeException(0x3e);
			}
			return lChar;
		}

		/// <summary>
		/// Used with the Print or PrintLine function to position output
		/// </summary>
		/// <param name="iCount">The number of spaces to insert before displaying or printing the next expression in a list</param>
		public void SPC(int iCount)
		{
			if (iCount <= 0)
			{
				return;
			}
			int column = GetColumn();
			int width = GetWidth();
			if (width != 0)
			{
				if (iCount >= width)
				{
					iCount = iCount % width;
				}
				if ((iCount + column) > width)
				{
					iCount -= width - column;
					goto Label_0038;
				}
			}
			iCount += column;
			if (iCount >= column)
			{
				goto Label_0041;
			}
			Label_0038:
			WriteLine(null);
			column = 0;
			Label_0041:
			if (iCount > column)
			{
				string s = new string(' ', iCount - column);
				WriteString(s);
			}
		}

		/// <summary>
		/// Used with the Print or PrintLine functions to position output
		/// </summary>
		/// <param name="column">The column number moved to before displaying or printing the next expression in a list</param>
		public void Tab(int column)
		{
			if (column < 1)
			{
				column = 1;
			}
			column--;
			int _column = GetColumn();
			int width = GetWidth();
			if ((width != 0) && (column >= width))
			{
				column = column % width;
			}
            if (column < _column)
			{
				WriteLine(null);
                _column = 0;
			}
            if (column > _column)
			{
                string s = new string(' ', column - _column);
				WriteString(s);
			}
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function
		/// </summary>
		public virtual void Unlock()
		{
			m_file.Unlock(0L, 0x7fffffffL);
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function
		/// </summary>
		/// <param name="record">Number of the only record or byte to lock or unlock</param>
		public virtual void Unlock(long record)
		{
			if (m_lRecordLen == -1)
			{
				m_file.Unlock(record - 1L, 1L);
			}
			else
			{
				m_file.Unlock((record - 1L) * m_lRecordLen, (long)m_lRecordLen);
			}
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened using the Open function
		/// </summary>
		/// <param name="recordStart">Number of the first record or byte to lock or unlock</param>
		/// <param name="recordEnd">Number of the last record or byte to lock or unlock</param>
		public virtual void Unlock(long recordStart, long recordEnd)
		{
			if (m_lRecordLen == -1)
			{
				m_file.Unlock(recordStart - 1L, recordEnd - recordStart);
			}
			else
			{
				m_file.Unlock((recordStart - 1L) * m_lRecordLen, (recordEnd - recordStart) * m_lRecordLen);
			}
		}


		/// <summary>
		/// Converts Type to Variant Type
		/// </summary>
		/// <param name="typ">Type to be convert to Variant Type</param>
		/// <returns>Variant type from the given Type</returns>
		static internal VT VTFromComType(Type typ)
		{
			if (typ != null)
			{
				if (typ.IsArray)
				{
					typ = typ.GetElementType();
					if (typ.IsArray)
					{
						return (VT.Variant | VT.Array);
					}
					VT vt2 = VTFromComType(typ);
					if ((vt2 & VT.Array) != VT.Empty)
					{
						return (VT.Variant | VT.Array);
					}
					return (vt2 | VT.Array);
				}
				if (typ.IsEnum)
				{
					typ = Enum.GetUnderlyingType(typ);
				}
				if (typ == null)
				{
					return VT.Empty;
				}
				switch (Type.GetTypeCode(typ)) {
					case TypeCode.DBNull:
						return VT.DBNull;

					case TypeCode.Boolean:
						return VT.Boolean;

					case TypeCode.Char:
						return VT.Char;

					case TypeCode.Byte:
						return VT.Byte;

					case TypeCode.Int16:
						return VT.Short;

					case TypeCode.Int32:
						return VT.Integer;

					case TypeCode.Int64:
						return VT.Long;

					case TypeCode.Single:
						return VT.Single;

					case TypeCode.Double:
						return VT.Double;

					case TypeCode.Decimal:
						return VT.Decimal;

					case TypeCode.DateTime:
						return VT.Date;

					case TypeCode.String:
						return VT.String;
				}
				if (typ == typeof(Missing))
				{
					return VT.Error;
				}
				if (ObjectType.IsTypeOf(typ, typeof(Exception)))
				{
					return VT.Error;
				}
				if (typ.IsValueType)
				{
					return VT.Structure;
				}
			}
			return VT.Variant;
		}

		/// <summary>
		/// Converts variant type name to enum
		/// </summary>
		/// <param name="varName">Variant type name</param>
		/// <returns>Variant Type enum</returns>
		static internal VT VTType(object varName)
		{
			if (varName == null)
			{
				return VT.Variant;
			}
			return VTFromComType(varName.GetType());
		}




		/// <summary>
		/// Write helper
		/// </summary>
		/// <param name="output">Output object</param>
		public void WriteHelper(params object[] output)
		{
			//this.InternalWriteHelper(Output);
			WriteString(",");
		}


		/// <summary>
		/// Writes a string
		/// </summary>
		/// <param name="s">String that should be written</param>
		public virtual void WriteLine(string s)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}

		public void WriteLineHelper(params object[] output)
		{
			//this.InternalWriteHelper(Output);
			WriteLine(null);
		}




		/// <summary>
		/// Writes a string followed by a line terminator to the text stream
		/// </summary>
		/// <param name="s">String that should be written</param>
		public virtual void WriteString(string s)
		{
			throw ExceptionUtils.VbMakeException(0x36);
		}



		/// <summary>
		/// Indicates the type of a variant object
		/// </summary>
		public enum VariantType
		{

			/// <summary>
			/// Array.
			/// </summary>
			Array = 0x2000,

			/// <summary>
			/// Boolean. (True or False.)
			/// </summary>
			Boolean = 11,

			/// <summary>
			/// Byte. (0 through 255.)
			/// </summary>
			Byte = 0x11,

			/// <summary>
			/// Char. (0 through 65535.)
			/// </summary>
			Char = 0x12,

			/// <summary>
			/// Currency.
			/// </summary>
			Currency = 6,

			/// <summary>
			/// DataObject.
			/// </summary>
			DataObject = 13,

			/// <summary>
			/// Date.
			/// </summary>
			Date = 7,

			/// <summary>
			/// Decimal.
			/// </summary>
			Decimal = 14,

			/// <summary>
			/// Double.
			/// </summary>
			Double = 5,

			/// <summary>
			/// Null reference.
			/// </summary>
			Empty = 0,

			/// <summary>
			/// System.Exception
			/// </summary>
			Error = 10,

			/// <summary>
			/// Integer.
			/// </summary>
			Integer = 3,

			/// <summary>
			/// Long.
			/// </summary>
			Long = 20,

			/// <summary>
			/// Null object. 
			/// </summary>
			Null = 1,

			/// <summary>
			/// Any type can be stored in a variable of type Object. 
			/// </summary>
			Object = 9,

			/// <summary>
			/// Short.
			/// </summary>
			Short = 2,

			/// <summary>
			/// Single.
			/// </summary>
			Single = 4,

			/// <summary>
			/// String.
			/// </summary>
			String = 8,

			/// <summary>
			/// User-defined type. 
			/// </summary>
			UserDefinedType = 0x24,

			/// <summary>
			/// Variant
			/// </summary>
			Variant = 12
		}


		public void Dispose()
		{
			m_file.Dispose();
		}
	}
}
