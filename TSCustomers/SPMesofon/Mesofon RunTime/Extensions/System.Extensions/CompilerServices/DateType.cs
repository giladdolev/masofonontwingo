using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// DateType class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	public static class DateType
	{
		// Methods
		/// <summary>
		/// Get DateTime from object.
		/// </summary>
		/// <param name="value">Object.</param>
		/// <returns>DateTime object.</returns>
		public static DateTime FromObject(object value)
		{
			if (value == null)
			{
				DateTime time = default(DateTime);
				return time;
			}
			
			IConvertible convertible = value as IConvertible;
			
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.DateTime:
						return convertible.ToDateTime(null);

					case TypeCode.String:
						return FromString(convertible.ToString(null), CultureInfo.CurrentCulture);
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Date"));
		}

		/// <summary>
		/// Convert DateTime from string.
		/// </summary>
		/// <param name="value">String expression.</param>
		/// <returns>DateTime object.</returns>
		public static DateTime FromString(string value)
		{
			return FromString(value, Utils.CultureInfo);
		}

		/// <summary>
		/// Convert DateTime from string.
		/// </summary>
		/// <param name="value">String expression.</param>
		/// <param name="culture">CultureInfo object.</param>
		/// <returns>DateTime object</returns>
		public static DateTime FromString(string value, CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			DateTime time;
			if (!DateTime.TryParse(StringType.ToHalfwidthNumbers(value), culture, DateTimeStyles.NoCurrentDateDefault | DateTimeStyles.AllowWhiteSpaces, out time))
			{
				DateTimeFormatInfo format = culture.DateTimeFormat.Clone() as DateTimeFormatInfo;
				format.ShortDatePattern = "dd/MM/yyyy";
				
				if (!DateTime.TryParse(value, format, DateTimeStyles.NoCurrentDateDefault | DateTimeStyles.AllowWhiteSpaces, out time))
					throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Date"));
			}
			
			return time;
		}

        public static DateTime Parse(string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
	}
}
