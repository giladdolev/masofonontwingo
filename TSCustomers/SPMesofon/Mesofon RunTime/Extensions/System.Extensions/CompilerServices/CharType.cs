using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Class CharType
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public static class CharType
	{
		// Methods

		/// <summary>
		/// Converts the value of the specified object to a Unicode character.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface.</param>
		/// <returns>A Unicode character</returns>
		public static char FromObject(object value)
		{
			if (value == null)
			{
				return '\0';
			}
			IConvertible convertible = value as IConvertible;
			
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.Char:
						return convertible.ToChar(null);

					case TypeCode.String:
						return FromString(convertible.ToString(null));
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Char"));
		}

		/// <summary>
		/// Converts the first character of a specified string to a Unicode character.
		/// </summary>
		/// <param name="value">Input string parameter</param>
		/// <returns>A Unicode character</returns>
		public static char FromString(string value)
		{
			if ((value != null) && (value.Length != 0))
			{
				return value[0];
			}
			return '\0';
		}
	}

}
