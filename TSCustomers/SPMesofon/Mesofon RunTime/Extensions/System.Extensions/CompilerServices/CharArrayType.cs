using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// CharArrayType class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public static class CharArrayType
	{
		// Methods
		/// <summary>
		/// Returns a CharArray value that corresponds to the specified object.
		/// </summary>
		/// <param name="value">Required. Object to convert to a CharArray value.</param>
		/// <returns>The CharArray value that corresponds to Value.</returns>
		public static char[] FromObject(object value)
		{
			if (value == null)
			{
				return "".ToCharArray();
			}
			char[] charValue = value as char[];
			if (charValue != null && (((Array)value).Rank == 1))
			{
				return charValue;
			}
			IConvertible convertible = value as IConvertible;
			
			if ((convertible == null) || (convertible.GetTypeCode() != TypeCode.String))
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Char()"));
			}
			return convertible.ToString(null).ToCharArray();
		}

		/// <summary>
		/// Returns a CharArray value that corresponds to the specified string.
		/// </summary>
		/// <param name="value">Required. String to convert to a CharArray value.</param>
		/// <returns>The CharArray value that corresponds to Value.</returns>
		public static char[] FromString(string value)
		{
			if (value == null)
			{
				value = "";
			}
			return value.ToCharArray();
		}
	}
}
