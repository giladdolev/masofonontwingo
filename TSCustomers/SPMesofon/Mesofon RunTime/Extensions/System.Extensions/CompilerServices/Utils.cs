using System.Text;
using System.Resources;
using System.Reflection;
using System.Threading;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Security.Permissions;
using System.ComponentModel;
using System.Security;
using System.Extensions.VBExtensions;
using System.Collections.Generic;

namespace System.Extensions.CompilerServices
{
    /// <summary>
    /// This class provides attributes that are applied to the standard module construct when it is emitted to Intermediate Language (IL). It is not intended to be called directly from your code.
    /// </summary>
	[AttributeUsageAttribute(AttributeTargets.Class, Inherited = false, AllowMultiple = false), EditorBrowsableAttribute(EditorBrowsableState.Never)]
    public sealed class StandardModuleAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the StandardModuleAttribute class.
        /// </summary>
        public StandardModuleAttribute()
		{
    }
	}

    /// <summary>
    /// Contains utilities that the Visual Basic compiler uses.
    /// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
    public sealed class Utils
    {
        private Utils()
        {

        }
        // Fields

        /// <summary>
        /// Constant back slash character
        /// </summary>
        internal const char chBackslash = '\\';

        /// <summary>
        /// Constant new line
        /// </summary>
        internal const char chCharH0A = '\n';
        internal const char chCharH0B = '\v';
        internal const char chCharH0C = '\f';
        internal const char chCharH0D = '\r';

        /// <summary>
        /// Constant colon character
        /// </summary>
        internal const char chColon = ':';

        /// <summary>
        /// Constant double quote character
        /// </summary>
        internal const char chDblQuote = '"';

        /// <summary>
        /// Constant hyphen character
        /// </summary>
        internal const char chHyphen = '-';

        /// <summary>
        /// Constant space character
        /// </summary>
        internal const char chIntlSpace = '　';

        /// <summary>
        /// Constant A letter character
        /// </summary>
        internal const char chLetterA = 'A';

        /// <summary>
        /// Constant Z letter character
        /// </summary>
        internal const char chLetterZ = 'Z';
        
        /// <summary>
        /// Constant new line
        /// </summary>
        internal const char chLineFeed = '\n';

        /// <summary>
        /// Constant null character
        /// </summary>
        internal const char chNull = '\0';

        /// <summary>
        /// Constant period character
        /// </summary>
        internal const char chPeriod = '.';

        /// <summary>
        /// Constant plus character
        /// </summary>
        internal const char chPlus = '+';

        /// <summary>
        /// Constant forward slash character
        /// </summary>
        internal const char chSlash = '/';

        /// <summary>
        /// Constant Space character
        /// </summary>
        internal const char chSpace = ' ';

        /// <summary>
        /// Constant tab character
        /// </summary>
        internal const char chTab = '\t';

        /// <summary>
        /// Constant zero character
        /// </summary>
        internal const char chZero = '0';

        /// <summary>
        /// Constant ERROR_INVALID_PARAMETER
        /// </summary>
        private const int ERROR_INVALID_PARAMETER = 0x57;

        /// <summary>
        /// Constant FACILITY_CONTROL
        /// </summary>
        internal const int FACILITY_CONTROL = 0xa0000;

        /// <summary>
        /// Constant FACILITY_ITF
        /// </summary>
        internal const int FACILITY_ITF = 0x40000;

        /// <summary>
        /// Constant FACILITY_RPC
        /// </summary>
        internal const int FACILITY_RPC = 0x10000;

        /// <summary>
        /// Empty spaces array
        /// </summary>
		static internal char[] s_achIntlSpace = new char[] {
			' ',
			'　',
		};

        /// <summary>
        /// Resource manager variable
        /// </summary>
		private static bool _s_TriedLoadingResourceManager;

        /// <summary>
        /// Resource manager variable
        /// </summary>
		private static ResourceManager _s_VBAResourceManager;

        /// <summary>
        /// Resource manager variable needed for threading
        /// </summary>
		private static readonly object _resourceManagerSyncObj = new object();

        /// <summary>
        /// Constant ResourceMsgDefault
        /// </summary>
        private const string RESOURCE_MSG_DEFAULT = "Message text unavailable.  Resource file 'Microsoft.VisualBasic resources' not found.";

        /// <summary>
        /// Constant SCODE_FACILITY
        /// </summary>
        internal const int SCODE_FACILITY = 0x1fff0000;

        /// <summary>
        /// Constant SEVERITY_ERROR
        /// </summary>
        internal const int SEVERITY_ERROR = -2147483648;

        /// <summary>
        /// Constant VBDefaultErrorID
        /// </summary>
        private const string VB_DEFAULT_ERROR_ID = "ID95";

        /// <summary>
        /// Constant VoidType
        /// </summary>
		private static readonly Type _voidType = Type.GetType("System.Void");

		private static Dictionary<int, int> _sMapHRESULT;

		static Utils()
		{
			_sMapHRESULT = new Dictionary<int, int>();
			_sMapHRESULT.Add(-2147467263, 0x8000);
			_sMapHRESULT.Add(-2147467262, 430);
			_sMapHRESULT.Add(-2147467260, 0x11f);
			_sMapHRESULT.Add(-2147352575, 0x1b6);
			_sMapHRESULT.Add(-2147352573, 0x1b6);
			_sMapHRESULT.Add(-2147352572, 0x1c0);
			_sMapHRESULT.Add(-2147352571, 13);
			_sMapHRESULT.Add(-2147352570, 0x1b6);
			_sMapHRESULT.Add(-2147352569, 0x1be);
			_sMapHRESULT.Add(-2147352568, 0x1ca);
			_sMapHRESULT.Add(-2147352566, 6);
			_sMapHRESULT.Add(-2147352565, 9);
			_sMapHRESULT.Add(-2147352564, 0x1bf);
            //IRA	_sMapHRESULT.Add(-2147352563, 10);
            //IRA	_sMapHRESULT.Add(-2147352563, 10);
			_sMapHRESULT.Add(-2147352562, 450);
			_sMapHRESULT.Add(-2147352561, 0x1c1);
			_sMapHRESULT.Add(-2147352559, 0x1c3);
			_sMapHRESULT.Add(-2147352558, 11);
			_sMapHRESULT.Add(-2147319786, 0x8016);
			_sMapHRESULT.Add(-2147319785, 0x1cd);
			_sMapHRESULT.Add(-2147319784, 0x8018);
			_sMapHRESULT.Add(-2147319783, 0x8019);
			_sMapHRESULT.Add(-2147319780, 0x801c);
			_sMapHRESULT.Add(-2147319779, 0x801d);
			_sMapHRESULT.Add(-2147319769, 0x8027);
			_sMapHRESULT.Add(-2147319768, 0x8028);
			_sMapHRESULT.Add(-2147319767, 0x8029);
			_sMapHRESULT.Add(-2147319766, 0x802a);
			_sMapHRESULT.Add(-2147319765, 0x802b);
			_sMapHRESULT.Add(-2147319764, 0x802c);
			_sMapHRESULT.Add(-2147319763, 0x802d);
			_sMapHRESULT.Add(-2147319762, 0x802e);
			_sMapHRESULT.Add(-2147319761, 0x1c5);
			_sMapHRESULT.Add(-2147317571, 0x88bd);
			_sMapHRESULT.Add(-2147317563, 0x88c5);
			_sMapHRESULT.Add(-2147316576, 13);
			_sMapHRESULT.Add(-2147316575, 9);
			_sMapHRESULT.Add(-2147316574, 0x39);
			_sMapHRESULT.Add(-2147316573, 0x142);
			_sMapHRESULT.Add(-2147312566, 0x30);
			_sMapHRESULT.Add(-2147312509, 0x9c83);
			_sMapHRESULT.Add(-2147312508, 0x9c84);
			_sMapHRESULT.Add(-2147287039, 0x8006);
			_sMapHRESULT.Add(-2147287038, 0x35);
			_sMapHRESULT.Add(-2147287037, 0x4c);
			_sMapHRESULT.Add(-2147287036, 0x43);
			_sMapHRESULT.Add(-2147287035, 70);
			_sMapHRESULT.Add(-2147287034, 0x8004);
			_sMapHRESULT.Add(-2147287032, 7);
			_sMapHRESULT.Add(-2147287022, 0x43);
			_sMapHRESULT.Add(-2147287021, 70);
			_sMapHRESULT.Add(-2147287015, 0x8003);
			_sMapHRESULT.Add(-2147287011, 0x8005);
			_sMapHRESULT.Add(-2147287010, 0x8004);
			_sMapHRESULT.Add(-2147287008, 0x4b);
			_sMapHRESULT.Add(-2147287007, 70);
			_sMapHRESULT.Add(-2147286960, 0x3a);
			_sMapHRESULT.Add(-2147286928, 0x3d);
			_sMapHRESULT.Add(-2147286789, 0x8018);
			_sMapHRESULT.Add(-2147286788, 0x35);
			_sMapHRESULT.Add(-2147286787, 0x8018);
			_sMapHRESULT.Add(-2147286786, 0x8000);
			_sMapHRESULT.Add(-2147286784, 70);
			_sMapHRESULT.Add(-2147286783, 70);
			_sMapHRESULT.Add(-2147286782, 0x8005);
			_sMapHRESULT.Add(-2147286781, 0x39);
			_sMapHRESULT.Add(-2147286780, 0x8019);
			_sMapHRESULT.Add(-2147286779, 0x8019);
			_sMapHRESULT.Add(-2147286778, 0x8015);
			_sMapHRESULT.Add(-2147286777, 0x8019);
			_sMapHRESULT.Add(-2147286776, 0x8019);
			_sMapHRESULT.Add(-2147221230, 0x1ad);
			_sMapHRESULT.Add(-2147221164, 0x1ad);
			_sMapHRESULT.Add(-2147221021, 0x1ad);
			_sMapHRESULT.Add(-2147221018, 0x1b0);
			_sMapHRESULT.Add(-2147221014, 0x1b0);
			_sMapHRESULT.Add(-2147221005, 0x1ad);
			_sMapHRESULT.Add(-2147221003, 0x1ad);
			_sMapHRESULT.Add(-2147220994, 0x1ad);
			_sMapHRESULT.Add(-2147024891, 70);
			_sMapHRESULT.Add(-2147024882, 7);
			_sMapHRESULT.Add(-2147024809, 5);

			_sMapHRESULT.Add(-2147023174, 0x1ce);
			_sMapHRESULT.Add(-2146959355, 0x1ce);
		}

        // Methods
        private static string AdjustArraySuffix(string sRank)
        {
			string str2 = default(string);
            for (int i = sRank.Length; i > 0; i--)
            {
                char ch = sRank[i - 1];
				switch (ch) {
                    case '(':
                        {
                            str2 = str2 + ")";
                            continue;
                        }
                    case ')':
                        {
                            str2 = str2 + "(";
                            continue;
                        }
                    case ',':
                        {
                            str2 = str2 + StringType.FromChar(ch);
                            continue;
                        }
                }
                str2 = StringType.FromChar(ch) + str2;
            }
            return str2;
        }      

        /// <summary>
        /// Used as a helper for Redim.
        /// </summary>
        /// <param name="arySrc">The array to be copied.</param>
        /// <param name="aryDest">The destination array.</param>
        /// <returns>The copied array.</returns>
        public static Array CopyArray(Array arySrc, Array aryDest)
        {
			if (aryDest == null)
            {
                throw new ArgumentNullException("aryDest");
            }

            // Null reference check
            if (arySrc != null)
            {
                // Preserve source array length
                int length = arySrc.Length;

                // If it null no need to copy anything
                if (length == 0)
                {
                    return aryDest;
                }

                // Check arrays rank
                if (aryDest.Rank != arySrc.Rank)
                {
                    // Throw an exception in case array rank differs
                    throw ExceptionUtils.VbMakeException(new InvalidCastException(GetResourceString("Array_RankMismatch")), 9);
                }
                int num9 = aryDest.Rank - 2;
                for (int i = 0; i <= num9; i++)
                {
                    if (aryDest.GetUpperBound(i) != arySrc.GetUpperBound(i))
                    {
                        throw ExceptionUtils.VbMakeException(new ArrayTypeMismatchException(GetResourceString("Array_TypeMismatch")), 9);
                    }
                }
                if (length > aryDest.Length)
                {
                    length = aryDest.Length;
                }
                if (arySrc.Rank > 1)
                {
                    int rank = arySrc.Rank;
                    int num7 = arySrc.GetLength(rank - 1);
                    int num6 = aryDest.GetLength(rank - 1);
                    if (num6 != 0)
                    {
                        int num5 = Math.Min(num7, num6);
                        int num8 = (arySrc.Length / num7) - 1;
                        for (int j = 0; j <= num8; j++)
                        {
                            Array.Copy(arySrc, j * num7, aryDest, j * num6, num5);
                        }
                    }
                    return aryDest;
                }
                Array.Copy(arySrc, aryDest, length);
            }
            return aryDest;
        }

        /// <summary>
        /// Returns current culture info object
        /// </summary>
        /// <value>CultureInfo object</value>
        public static CultureInfo CultureInfo
        {
            get { return Thread.CurrentThread.CurrentCulture; }
        }

        /// <summary>
        /// Returns datetime format for current culture
        /// </summary>
        /// <value>DateTimeFormat object</value>
        public static DateTimeFormatInfo DateTimeFormatInfo
        {
            get { return Thread.CurrentThread.CurrentCulture.DateTimeFormat; }
        }

        /// <summary>
        /// Returns default encoding
        /// </summary>
        /// <returns>Encoding object</returns>
		static internal Encoding GetFileIOEncoding()
        {
            return Encoding.Default;
        }

        /// <summary>
        ///    Gets the System.Globalization.CultureInfo object that is culture-independent
        /// </summary>
        /// <value>CultureInfo</value>
        public static CultureInfo InvariantCultureInfo
        {
            get { return CultureInfo.InvariantCulture; }
        }

        /// <summary>
        /// Return ANSI code page
        /// </summary>
        /// <returns>Integer code for code page</returns>
		static internal int GetLocaleCodePage()
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ANSICodePage;
        }

        /// <summary>
        /// Retrieves and formats a localized resource string or error message.
        /// </summary>
		/// <param name="resourceId">Resource id parameter</param>
        /// <returns>String value</returns>
		public static string GetResourceString(vbErrors resourceId)
        {
			return GetResourceString("ID" + StringType.FromInteger((int)resourceId));
        }

        /// <summary>
        /// Retrieves and formats a localized resource string or error message.
        /// </summary>
        /// <param name="resourceKey">The identifier of the string or error message to retrieve.</param>
        /// <returns>A formatted resource string or error message.</returns>
        public static string GetResourceString(string resourceKey)
        {
            // Declare local variable
            string str2;

            // Null reference check

            if (VBAResourceManager == null)
            {
                return RESOURCE_MSG_DEFAULT;
            }

            try
            {
                str2 = VBAResourceManager.GetString(resourceKey, CultureInfo) ?? VBAResourceManager.GetString("ID95");
            }
            catch (ArgumentException)
            {
                str2 = RESOURCE_MSG_DEFAULT;
            }
            catch (MissingManifestResourceException)
            {
                str2 = RESOURCE_MSG_DEFAULT;
            }
            return str2;
        }

        /// <summary>
        ///  Retrieves and formats a localized resource string or error message.
        /// </summary>
        /// <param name="resourceKey">The identifier of the string or error message to retrieve.</param>
        /// <param name="notUsed">Not used</param>
        /// <returns>A formatted resource string or error message.</returns>
        public static string GetResourceString(string resourceKey, bool notUsed)
        {
            string str2;
            if (VBAResourceManager == null)
            {
                return RESOURCE_MSG_DEFAULT;
            }
            try
            {
                str2 = VBAResourceManager.GetString(resourceKey, CultureInfo);
                if (str2 == null)
                {
                    str2 = VBAResourceManager.GetString(resourceKey);
                }
            }
            catch (ArgumentException)
            {
                str2 = null;
            }
            catch (MissingManifestResourceException)
            {
                str2 = null;
            }

            return str2;
        }

        /// <summary>
        ///  Retrieves and formats a localized resource string or error message.
        /// </summary>
        /// <param name="resourceKey">The identifier of the string or error message to retrieve.</param>
		/// <param name="parm1">Parameter used in message</param>
        /// <returns>A formatted resource string or error message.</returns>
		public static string GetResourceString(string resourceKey, string parm1)
        {
            StringBuilder builder = new StringBuilder(GetResourceString(resourceKey));
			builder.Replace("|1", parm1);
            return builder.ToString();
        }

        /// <summary>
        ///  Retrieves and formats a localized resource string or error message.
        /// </summary>
        /// <param name="resourceKey">The identifier of the string or error message to retrieve.</param>
		/// <param name="parm1">Parameter used in message</param>
		/// <param name="parm2">Parameter used in message</param>
        /// <returns>A formatted resource string or error message.</returns>
		public static string GetResourceString(string resourceKey, string parm1, string parm2)
        {
            StringBuilder builder = new StringBuilder(GetResourceString(resourceKey));
			builder.Replace("|1", parm1);
			builder.Replace("|2", parm2);
            return builder.ToString();
        }


        /// <summary>
        /// Checking if the Type is numeric
        /// </summary>
        /// <param name="typ">Required. Type value</param>
        /// <returns>Boolean value.</returns>
		static internal bool IsNumericType(Type typ)
        {
            if (!typ.IsArray)
            {
				switch (Type.GetTypeCode(typ)) {
                    case TypeCode.Boolean:
                    case TypeCode.Byte:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.Single:
                    case TypeCode.Double:
                    case TypeCode.Decimal:
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if the typecode is numeric
        /// </summary>
		/// <param name="typCode">TypeCode object</param>
        /// <returns>True if it is numeric otherwise false</returns>
		static internal bool IsNumericTypeCode(TypeCode typCode)
        {
			switch (typCode) {
                case TypeCode.Boolean:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Maps number to HRESULT
        /// </summary>
        /// <param name="lNumber">lNumber value</param>
        /// <returns>Converted HRESULT</returns>
		static internal int MapHRESULT(int lNumber)
        {
            if (lNumber > 0)
            {
                return 0;
            }
            if ((lNumber & 0x1fff0000) == 0xa0000)
            {
                return (lNumber & 0xffff);
            }
            switch (lNumber)
            {
                case -2147467263:
                    return 0x8000;
            }
			int ret;

			if ( _sMapHRESULT.TryGetValue( lNumber, out ret ) )
			{
				return ret;
			}

            return lNumber;
        }

        /// <summary>
        /// Returns a Visual Basic method signature.
        /// </summary>
		/// <param name="method">A MethodBase object to return a Visual Basic method signature for.</param>
        /// <returns>The Visual Basic method signature for the supplied MethodBase object.</returns>
		public static string MethodToString(MethodBase method)
        {
			string str = default(string);
			MethodInfo info = (MethodInfo)method;
            Type returnType = info.ReturnType;
            if (((info.Attributes & MethodAttributes.Virtual) != MethodAttributes.ReuseSlot) && !info.DeclaringType.IsInterface)
            {
                str = str + "Overrides ";
            }
            if (info.IsPublic)
            {
                str = str + "Public ";
            }
            else if (info.IsPrivate)
            {
                str = str + "Private ";
            }
            else if (info.IsPrivate)
            {
                str = str + "Friend ";
            }
			if ((returnType == null) || (returnType == _voidType))
            {
                str = str + "Sub ";
            }
            else
            {
                str = str + "Function ";
            }
            str = str + info.Name + " (";
            bool flag = true;
            foreach (ParameterInfo info2 in info.GetParameters())
            {
                if (!flag)
                {
                    str = str + ", ";
                }
                else
                {
                    flag = false;
                    str = str + " ";
                }
                if (info2.IsOptional)
                {
                    str = str + "Optional ";
                }
                if (info2.ParameterType.IsByRef)
                {
                    str = str + "ByRef ";
                }
                else
                {
                    str = str + "ByVal ";
                    if (info2.ParameterType.IsArray)
                    {
                        object[] customAttributes = info2.GetCustomAttributes(typeof(ParamArrayAttribute), false);
                        if ((customAttributes != null) && (customAttributes.Length > 0))
                        {
                            str = str + "ParamArray ";
                        }
                    }
                }
                str = str + info2.Name + " As ";
                if (info2.ParameterType.IsByRef)
                {
                    str = str + VBFriendlyNameOfTypeName(info2.ParameterType.GetElementType().FullName);
                }
                else
                {
                    str = str + VBFriendlyNameOfTypeName(info2.ParameterType.FullName);
                }
                if (info2.IsOptional)
                {
                    Type type;
                    object defaultValue = info2.DefaultValue;
                    if (defaultValue == null)
                    {
                        type = typeof(object);
                    }
                    else
                    {
                        type = defaultValue.GetType();
						if (type != _voidType)
                        {
                            if (typeof(Enum).IsAssignableFrom(type))
                            {
                                str = str + " = " + Enum.GetName(type, defaultValue);
                            }
                            else
                            {
                                str = str + " = " + StringType.FromObject(defaultValue);
                            }
                        }
                    }
                }
            }
            str = str + " )";
			if ((returnType != null) && (returnType != _voidType))
            {
                str = str + " As " + VBFriendlyNameOfTypeName(returnType.FullName);
            }
            return str;
        }

     
        /// <summary>
        /// Sets the culture of the current thread.
        /// </summary>
		/// <param name="culture">A CultureInfo object to set as the culture of the current thread.</param>
        /// <returns>The previous value of the CurrentCulture property for the current thread.</returns>
		public static object SetCultureInfo(CultureInfo culture)
        {
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = culture;
            return currentCulture;
        }

        /// <summary>
        /// Sets the date value in the Date object.
        /// </summary>
        /// <param name="dtTime">Required. Any Date object.</param>
		[DebuggerHiddenAttribute(), SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static void SetDate(DateTime vDate)
        {
            NativeTypes.SystemTime systime = new NativeTypes.SystemTime();
            SafeNativeMethods.GetLocalTime(systime);
            systime.wYear = (short)vDate.Year;
            systime.wMonth = (short)vDate.Month;
            systime.wDay = (short)vDate.Day;
            if (UnsafeNativeMethods.SetLocalTime(systime) == 0)
            {
                if (Marshal.GetLastWin32Error() == 0x57)
                {
                    throw new ArgumentException(GetResourceString("Argument_InvalidValue"));
                }
                throw new SecurityException(GetResourceString("SetLocalDateFailure"));
            }
        }
        /// <summary>
        /// Sets the date and time value in the Date object.
        /// </summary>
        /// <param name="dtTime">Required. Any Date object.</param>
		[DebuggerHiddenAttribute(), SecurityPermissionAttribute(SecurityAction.Demand, UnmanagedCode = true)]
        public static void SetTime(DateTime dtTime)
        {
            NativeTypes.SystemTime systime = new NativeTypes.SystemTime();
            SafeNativeMethods.GetLocalTime(systime);
            systime.wHour = (short)dtTime.Hour;
            systime.wMinute = (short)dtTime.Minute;
            systime.wSecond = (short)dtTime.Second;
            systime.wMilliseconds = (short)dtTime.Millisecond;
            if (UnsafeNativeMethods.SetLocalTime(systime) == 0)
            {
                if (Marshal.GetLastWin32Error() == 0x57)
                {
                    throw new ArgumentException(GetResourceString("Argument_InvalidValue"));
                }
                throw new SecurityException(GetResourceString("SetLocalTimeFailure"));
            }
        }

        /// <summary>
        /// Standards the format.
        /// </summary>
        /// <param name="s">The arguments.</param>
        /// <returns></returns>
        public static string StdFormat(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                throw new ArgumentNullException("s");
            }

            char ch = default(char);
			char ch2 = default(char);
			char ch3 = default(char);
            NumberFormatInfo numberFormat = Thread.CurrentThread.CurrentCulture.NumberFormat;
            int index = s.IndexOf(numberFormat.NumberDecimalSeparator, StringComparison.Ordinal);
            if (index == -1)
            {
                return s;
            }
            
			if (s.Length > 0)
				ch = s[0];
			if (s.Length > 1)
				ch2 = s[1];
			if (s.Length > 2)
				ch3 = s[2];

            if (s[index] == '.')
            {
                if ((ch == '0') && (ch2 == '.'))
                {
                    return s.Substring(1);
                }
                if (((ch != '-') && (ch != '+')) && (ch != ' '))
                {
                    return s;
                }
                if ((ch2 != '0') || (ch3 != '.'))
                {
                    return s;
                }
            }
            StringBuilder builder = new StringBuilder(s);
            builder[index] = '.';
            if ((ch == '0') && (ch2 == '.'))
            {
                return builder.ToString(1, builder.Length - 1);
            }
            if ((((ch == '-') || (ch == '+')) || (ch == ' ')) && ((ch2 == '0') && (ch3 == '.')))
            {
                builder.Remove(1, 1);
                return builder.ToString();
            }
            return builder.ToString();
        }
        /// <summary>
        /// Throws a localized exception.
        /// </summary>
        /// <param name="hr">The error identifier.</param>
        public static void ThrowException(int hr)
        {
            throw ExceptionUtils.VbMakeException(hr);
        }

        /// <summary>
        /// Convert type to vb friendly name
        /// </summary>
		/// <param name="obj">Object parameter</param>
        /// <returns>Vb friendly name</returns>
		public static string VBFriendlyName(object obj)
        {
			if (obj == null)
            {
                return "Nothing";
            }
			Type type = obj.GetType();
            if (type.IsCOMObject)
            {
				return Information.TypeName(obj);
            }
            return VBFriendlyNameOfTypeName(type.Name);
        }

        /// <summary>
        /// Convert type to vb friendly name
        /// </summary>
        /// <param name="Obj">Object parameter</param>
        /// <returns>Vb friendly name</returns>
		static internal string VBFriendlyName(Type typ)
        {
            return VBFriendlyNameOfTypeName(typ.Name);
        }



        /// <summary>
        /// Convert type to vb friendly name
        /// </summary>
		/// <param name="typeName">String type name</param>
        /// <returns>Vb friendly name</returns>
		static internal string VBFriendlyNameOfTypeName(string typeName)
        {
            string str = default(string);
			int num = typeName.Length - 1;
			if (typeName[num] == ']')
            {
				int index = typeName.IndexOf('[');
                if ((index + 1) == num)
                {
                    str = "()";
                }
                else
                {
					str = typeName.Substring(index, (num - index) + 1).Replace('[', '(').Replace(']', ')');
                }
				typeName = typeName.Substring(0, index);
            }
			string str2 = Information.VbTypeName(typeName);
            if (str2 == null)
            {
				str2 = typeName;
            }
            if (str == null)
            {
                return str2;
            }
            return (str2 + AdjustArraySuffix(str));
        }

        // Properties

        /// <summary>
        /// VBAResourceManager property
        /// </summary>
		static internal ResourceManager VBAResourceManager
        {
            get
            {
				if (_s_VBAResourceManager == null)
                {
					object resourceManagerSyncObj = _resourceManagerSyncObj;
                    StringsEx.CheckForSyncLockOnValueType(resourceManagerSyncObj);
                    lock (resourceManagerSyncObj)
                    {
						if (!_s_TriedLoadingResourceManager)
                        {
                            try
                            {
								_s_VBAResourceManager = new ResourceManager("System.Extensions.Resources", Assembly.GetExecutingAssembly());
                            }
                            catch (ArgumentException)
                            {
                            }
							_s_TriedLoadingResourceManager = true;
                        }
                    }
                }
				return _s_VBAResourceManager;
            }
        }

        /// <summary>
        /// vbErrors enum
        /// </summary>
        public enum vbErrors
        {
            /// <summary>
            /// Abort value
            /// </summary>
            Abort = 0x11f,
            /// <summary>
            /// ActionNotSupported value
            /// </summary>
            ActionNotSupported = 0x1bd,
            /// <summary>
            /// AdviseLimit value
            /// </summary>
            AdviseLimit = 0x60,
            /// <summary>
            /// AmbiguousName value
            /// </summary>
            AmbiguousName = 0x802c,
            /// <summary>
            /// ArrayLocked value
            /// </summary>
            ArrayLocked = 10,
            /// <summary>
            /// BadCallToFriendFunction value
            /// </summary>
            BadCallToFriendFunction = 0x61,
            /// <summary>
            /// BadFileMode value
            /// </summary>
            BadFileMode = 0x36,
            /// <summary>
            /// BadFileNameOrNumber value
            /// </summary>
            BadFileNameOrNumber = 0x34,
            /// <summary>
            /// BadFunctionId value
            /// </summary>
            BadFunctionId = 0x8006,
            /// <summary>
            /// BadLibId value
            /// </summary>
            BadLibId = 0x88c3,
            /// <summary>
            /// BadModuleKind value
            /// </summary>
            BadModuleKind = 0x88bd,
            /// <summary>
            /// BadPatStr value
            /// </summary>
            BadPatStr = 0x5d,
            /// <summary>
            /// BadRecordLen value
            /// </summary>
            BadRecordLen = 0x3b,
            /// <summary>
            /// BadRecordNum value
            /// </summary>
            BadRecordNum = 0x3f,
            /// <summary>
            /// BadTypeId value
            /// </summary>
            BadTypeId = 0x88c2,
            /// <summary>
            /// BrokenLibRef value
            /// </summary>
            BrokenLibRef = 0x88cd,
            /// <summary>
            /// BufferTooSmall value
            /// </summary>
            BufferTooSmall = 0x8016,
            /// <summary>
            /// CantContinue value
            /// </summary>
            CantContinue = 0x11,
            /// <summary>
            /// CantCreateObject value
            /// </summary>
            CantCreateObject = 0x1ad,
            /// <summary>
            /// CantCreateTmpFile value
            /// </summary>
            CantCreateTmpFile = 0x142,
            /// <summary>
            /// CantEvalWatch value
            /// </summary>
            CantEvalWatch = 0x9c65,
            /// <summary>
            /// CantFindDllEntryPoint value
            /// </summary>
            CantFindDllEntryPoint = 0xe741,
            /// <summary>
            /// CantPassPrivateObject value
            /// </summary>
            CantPassPrivateObject = 0x62,
            /// <summary>
            /// CantSaveFileToTemp value
            /// </summary>
            CantSaveFileToTemp = 0x2df,
            /// <summary>
            /// CantUseNull value
            /// </summary>
            CantUseNull = 0x5e,
            /// <summary>
            /// CircularType value
            /// </summary>
            CircularType = 0x9c84,
            /// <summary>
            /// CodeResourceLockError value
            /// </summary>
            CodeResourceLockError = 0x1c7,
            /// <summary>
            /// CodeResourceNotFound value
            /// </summary>
            CodeResourceNotFound = 0x1c6,
            /// <summary>
            /// CompileError value
            /// </summary>
            CompileError = 0x9c64,
            /// <summary>
            /// DevUnavailable value
            /// </summary>
            DevUnavailable = 0x44,
            /// <summary>
            /// DifferentDrive value
            /// </summary>
            DifferentDrive = 0x4a,
            /// <summary>
            /// DiskFull value
            /// </summary>
            DiskFull = 0x3d,
            /// <summary>
            /// DiskNotReady value
            /// </summary>
            DiskNotReady = 0x47,
            /// <summary>
            /// DivByZero value
            /// </summary>
            DivByZero = 11,
            /// <summary>
            /// DLLBadCallingConv value
            /// </summary>
            DLLBadCallingConv = 0x31,
            /// <summary>
            /// DLLCallException value
            /// </summary>
            DLLCallException = 0x63,
            /// <summary>
            /// DLLLoadErr value
            /// </summary>
            DLLLoadErr = 0x30,
            /// <summary>
            /// DoesntImplementICollection value
            /// </summary>
            DoesntImplementICollection = 100,
            /// <summary>
            /// DuplicateKey value
            /// </summary>
            DuplicateKey = 0x1c9,
            /// <summary>
            /// ElementNotFound value
            /// </summary>
            ElementNotFound = 0x802b,
            /// <summary>
            /// EndOfFile value
            /// </summary>
            EndOfFile = 0x3e,
            /// <summary>
            /// Eof value
            /// </summary>
            Eof = 0x88c4,
            /// <summary>
            /// ExpectedFuncNotModule value
            /// </summary>
            ExpectedFuncNotModule = 0x88c7,
            /// <summary>
            /// ExpectedFuncNotProject value
            /// </summary>
            ExpectedFuncNotProject = 0x88c9,
            /// <summary>
            /// ExpectedFuncNotRecord value
            /// </summary>
            ExpectedFuncNotRecord = 0x88c8,
            /// <summary>
            /// ExpectedFuncNotVar value
            /// </summary>
            ExpectedFuncNotVar = 0x88ca,
            /// <summary>
            /// ExpectedTokens value
            /// </summary>
            ExpectedTokens = 0x9c6f,
            /// <summary>
            /// ExpectedTypeNotProj value
            /// </summary>
            ExpectedTypeNotProj = 0x88cb,
            /// <summary>
            /// ExprTooComplex value
            /// </summary>
            ExprTooComplex = 0x10,
            /// <summary>
            /// FileAlreadyExists value
            /// </summary>
            FileAlreadyExists = 0x3a,
            /// <summary>
            /// FileAlreadyOpen value
            /// </summary>
            FileAlreadyOpen = 0x37,
            /// <summary>
            /// FileLockViolation
            /// </summary>
            FileLockViolation = 0x8007,
            /// <summary>
            /// FileNotFound value
            /// </summary>
            FileNotFound = 0x35,
            /// <summary>
            /// FileNotFoundWithName value
            /// </summary>
            FileNotFoundWithName = 0x9d33,
            /// <summary>
            /// FuncArityMismatch value
            /// </summary>
            FuncArityMismatch = 450,
            /// <summary>
            /// GetNotSupported value
            /// </summary>
            GetNotSupported = 0x18a,
            /// <summary>
            /// GetNotSupportedAtRuntime value
            /// </summary>
            GetNotSupportedAtRuntime = 0x189,
            /// <summary>
            /// IdentNotMember value
            /// </summary>
            IdentNotMember = 0x1cd,
            /// <summary>
            /// IdTooLong value
            /// </summary>
            IdTooLong = 0x9c6d,
            /// <summary>
            /// IllegalChar value
            /// </summary>
            IllegalChar = 0x9c6c,
            /// <summary>
            /// IllegalFor value
            /// </summary>
            IllegalFor = 0x5c,
            /// <summary>
            /// IllegalFuncCall value
            /// </summary>
            IllegalFuncCall = 5,
            /// <summary>
            /// InconsistentPropFuncs value
            /// </summary>
            InconsistentPropFuncs = 0x9c83,
            /// <summary>
            /// IncorrectTypeChar value
            /// </summary>
            IncorrectTypeChar = 0x9c6a,
            /// <summary>
            /// InternalError value
            /// </summary>
            InternalError = 0x33,
            /// <summary>
            /// InvalidClipboardFormat value
            /// </summary>
            InvalidClipboardFormat = 460,
            /// <summary>
            /// InvalidDllFunctionName value
            /// </summary>
            InvalidDllFunctionName = 0x1c5,
            /// <summary>
            /// InvalidFileFormat value
            /// </summary>
            InvalidFileFormat = 0x141,
            /// <summary>
            /// InvalidNumLit value
            /// </summary>
            InvalidNumLit = 0x9c6b,
            /// <summary>
            /// InvalidOrdinal value
            /// </summary>
            InvalidOrdinal = 0x1c4,
            /// <summary>
            /// InvalidPicture value
            /// </summary>
            InvalidPicture = 0x1e1,
            /// <summary>
            /// InvalidPropertyArrayIndex value
            /// </summary>
            InvalidPropertyArrayIndex = 0x17d,
            /// <summary>
            /// InvalidPropertyValue value
            /// </summary>
            InvalidPropertyValue = 380,
            /// <summary>
            /// InvalidResourceFormat value
            /// </summary>
            InvalidResourceFormat = 0x145,
            /// <summary>
            /// InvalidState value
            /// </summary>
            InvalidState = 0x8029,
            /// <summary>
            /// InvalidTypeInfoKind value
            /// </summary>
            InvalidTypeInfoKind = 0x88d1,
            /// <summary>
            /// InvalidTypeLibFunction value
            /// </summary>
            InvalidTypeLibFunction = 0x88d2,
            /// <summary>
            /// InvalidTypeLibVariable value
            /// </summary>
            InvalidTypeLibVariable = 0x1ca,
            /// <summary>
            /// InvDataRead value
            /// </summary>
            InvDataRead = 0x8018,
            /// <summary>
            /// IOError value
            /// </summary>
            IOError = 0x39,
            /// <summary>
            /// LastTrappable value
            /// </summary>
            LastTrappable = 0x2ea,
            /// <summary>
            /// LibNotRegistered value
            /// </summary>
            LibNotRegistered = 0x801d,
            /// <summary>
            /// LocaleSettingNotSupported value
            /// </summary>
            LocaleSettingNotSupported = 0x1bf,
            /// <summary>
            /// LostTLB value
            /// </summary>
            LostTLB = 0x1ba,
            /// <summary>
            /// MissingEndBrack value
            /// </summary>
            MissingEndBrack = 0x9c69,
            /// <summary>
            /// MissingVbaTypeLib value
            /// </summary>
            MissingVbaTypeLib = 0x9c66,
            /// <summary>
            /// ModNameConflict value
            /// </summary>
            ModNameConflict = 0x802d,
            /// <summary>
            /// ModuleAsType value
            /// </summary>
            ModuleAsType = 0x88d0,
            /// <summary>
            /// NamedArgsNotSupported value
            /// </summary>
            NamedArgsNotSupported = 0x1be,
            /// <summary>
            /// NamedParamNotFound value
            /// </summary>
            NamedParamNotFound = 0x1c0,
            /// <summary>
            /// NeedPropertyArrayIndex value
            /// </summary>
            NeedPropertyArrayIndex = 0x181,
            /// <summary>
            /// NoContainingLib value
            /// </summary>
            NoContainingLib = 0x88c1,
            /// <summary>
            /// None value
            /// </summary>
            None = 0,
            /// <summary>
            /// NoSuchControlOrProperty value
            /// </summary>
            NoSuchControlOrProperty = 0x1a7,
            /// <summary>
            /// NotEnum value
            /// </summary>
            NotEnum = 0x1c3,
            /// <summary>
            /// NotObject value
            /// </summary>
            NotObject = 0x1a8,
            /// <summary>
            /// NotYetImplemented value
            /// </summary>
            NotYetImplemented = 0x8000,
            /// <summary>
            /// ObjDoesNotSupportEvents value
            /// </summary>
            ObjDoesNotSupportEvents = 0x1cb,
            /// <summary>
            /// ObjNotRegistered value
            /// </summary>
            ObjNotRegistered = 0x1cf,
            /// <summary>
            /// ObjNotSet value
            /// </summary>
            ObjNotSet = 0x5b,
            /// <summary>
            /// OLEAutomationError value
            /// </summary>
            OLEAutomationError = 440,
            /// <summary>
            /// OLEFileNotFound value
            /// </summary>
            OLEFileNotFound = 0x1b0,
            /// <summary>
            /// OLENoDefault value
            /// </summary>
            OLENoDefault = 0x1bb,
            /// <summary>
            /// OLENoPropOrMethod value
            /// </summary>
            OLENoPropOrMethod = 0x1b6,
            /// <summary>
            /// OLENotSupported value
            /// </summary>
            OLENotSupported = 430,
            /// <summary>
            /// OperationNotAllowedInDll value
            /// </summary>
            OperationNotAllowedInDll = 0x9c63,
            /// <summary>
            /// OutOfBounds value
            /// </summary>
            OutOfBounds = 9,
            /// <summary>
            /// OutOfMemory value
            /// </summary>
            OutOfMemory = 7,
            /// <summary>
            /// OutOfStack value
            /// </summary>
            OutOfStack = 0x1c,
            /// <summary>
            /// OutOfStrSpace value
            /// </summary>
            OutOfStrSpace = 14,
            /// <summary>
            /// Overflow value
            /// </summary>
            Overflow = 6,
            /// <summary>
            /// ParameterNotOptional value
            /// </summary>
            ParameterNotOptional = 0x1c1,
            /// <summary>
            /// PathFileAccess value
            /// </summary>
            PathFileAccess = 0x4b,
            /// <summary>
            /// PathNotFound value
            /// </summary>
            PathNotFound = 0x4c,
            /// <summary>
            /// PermissionDenied value
            /// </summary>
            PermissionDenied = 70,
            /// <summary>
            /// PrinterError value
            /// </summary>
            PrinterError = 0x1e2,
            /// <summary>
            /// PropertyNotFound value
            /// </summary>
            PropertyNotFound = 0x1a6,
            /// <summary>
            /// QualifiedNameDisallowed value
            /// </summary>
            QualifiedNameDisallowed = 0x8028,
            /// <summary>
            /// ReadFault value
            /// </summary>
            ReadFault = 0x8004,
            /// <summary>
            /// RegistryAccess value
            /// </summary>
            RegistryAccess = 0x801c,
            /// <summary>
            /// ReplacementsTooLong value
            /// </summary>
            ReplacementsTooLong = 0x2ea,
            /// <summary>
            /// ResumeWOErr value
            /// </summary>
            ResumeWOErr = 20,
            /// <summary>
            /// ReturnWOGoSub value
            /// </summary>
            ReturnWOGoSub = 3,
            /// <summary>
            /// SearchTextNotFound value
            /// </summary>
            SearchTextNotFound = 0x2e8,
            /// <summary>
            /// SeekErr value
            /// </summary>
            SeekErr = 0x8003,
            /// <summary>
            /// ServerNotFound value
            /// </summary>
            ServerNotFound = 0x1ce,
            /// <summary>
            /// SetNotPermitted value
            /// </summary>
            SetNotPermitted = 0x183,
            /// <summary>
            /// SetNotSupported value
            /// </summary>
            SetNotSupported = 0x17f,
            /// <summary>
            /// SetNotSupportedAtRuntime value
            /// </summary>
            SetNotSupportedAtRuntime = 0x17e,
            /// <summary>
            /// ShareRequired value
            /// </summary>
            ShareRequired = 0x8015,
            /// <summary>
            /// SizeTooBig value
            /// </summary>
            SizeTooBig = 0x88c5,
            /// <summary>
            /// StatementTooComplex value
            /// </summary>
            StatementTooComplex = 0x9c6e,
            /// <summary>
            /// TooManyClients value
            /// </summary>
            TooManyClients = 0x2f,
            /// <summary>
            /// TooManyFiles value
            /// </summary>
            TooManyFiles = 0x43,
            /// <summary>
            /// TypeMismatch value
            /// </summary>
            TypeMismatch = 13,
            /// <summary>
            /// UndefinedProc value
            /// </summary>
            UndefinedProc = 0x23,
            /// <summary>
            /// UndefinedType value
            /// </summary>
            UndefinedType = 0x8027,
            /// <summary>
            /// UNDONE value
            /// </summary>
            UNDONE = 0x1d,
            /// <summary>
            /// UnknownLcid value
            /// </summary>
            UnknownLcid = 0x802e,
            /// <summary>
            /// UnsuitableFuncPropMatch value
            /// </summary>
            UnsuitableFuncPropMatch = 0x88cc,
            /// <summary>
            /// UnsupFormat value
            /// </summary>
            UnsupFormat = 0x8019,
            /// <summary>
            /// UnsupportedTypeLibFeature value
            /// </summary>
            UnsupportedTypeLibFeature = 0x88ce,
            /// <summary>
            /// Usage value
            /// </summary>
            Usage = 0x801f,
            /// <summary>
            /// UserDefined value
            /// </summary>
            UserDefined = 0x5f,
            /// <summary>
            /// UserInterrupt value
            /// </summary>
            UserInterrupt = 0x12,
            /// <summary>
            /// UserReset value
            /// </summary>
            UserReset = 0x9c68,
            /// <summary>
            /// WriteFault value
            /// </summary>
            WriteFault = 0x8005,
            /// <summary>
            /// WrongTypeKind value
            /// </summary>
            WrongTypeKind = 0x802a
        }
 

 

    }





}
