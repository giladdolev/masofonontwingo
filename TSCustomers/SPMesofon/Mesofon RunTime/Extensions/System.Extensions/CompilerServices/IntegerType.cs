using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// IntegerType class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class IntegerType
	{
		private IntegerType()
		{

		}

		// Methods

		/// <summary>
		/// Converts the value of the specified decimal number to an equivalent 32-bit signed integer.
		/// </summary>
		/// <param name="valueInterface">Required.IConvertible value.</param>
		/// <returns>Integer value.</returns>
		private static int DecimalToInteger(IConvertible valueInterface)
		{
			return Convert.ToInt32(valueInterface.ToDecimal(null));
		}


		/// <summary>
		/// Converts the value of the specified object to a 32-bit signed integer.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <returns>Integer value.</returns>
		public static int FromObject(object value)
		{
			if (value == null)
			{
				return 0;
			}
			IConvertible valueInterface = value as IConvertible;
			
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return (int)((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (byte)value;
						}
						return valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return (short)value;
						}
						return valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return (int)value;
						}
						return valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return (int)((long)value);
						}
						return (int)valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return (int)Math.Round((double)((float)value));
						}
						return (int)Math.Round((double)valueInterface.ToSingle(null));

					case TypeCode.Double:
						if (value is double)
						{
							return (int)Math.Round((double)value);
						}
						return (int)Math.Round(valueInterface.ToDouble(null));

					case TypeCode.Decimal:
						return DecimalToInteger(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null));
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Integer"));
		}

		/// <summary>
		/// Converts the specified string representation of a number to an equivalent 32-bit signed integer.
		/// </summary>
		/// <param name="value">A string that contains the number to convert.</param>
		/// <returns>Integer value.</returns>
		public static int FromString(string value)
		{
			int num;
			if (value == null)
			{
				return 0;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return (int)num2;
				}
				num = (int)Math.Round(DoubleType.Parse(value));
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Integer"), exception);
			}
			return num;
		}
	}
}
