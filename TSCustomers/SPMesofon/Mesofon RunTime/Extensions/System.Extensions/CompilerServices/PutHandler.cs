using System.Reflection;
using System.ComponentModel;
using System.Extensions.VBAttributes;
using System.Globalization;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// IRecordEnum interface
	/// </summary>
	internal interface IRecordEnum
	{
		// Methods
		bool Callback(FieldInfo fieldInfo, ref object value);
	}

	/// <summary>
	/// PutHandler class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal sealed class PutHandler : IRecordEnum
	{


		// Fields
		public VB6File m_oFile;

		// Methods
		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="oFile">VB6File value</param>
		public PutHandler(VB6File oFile)
		{
			m_oFile = oFile;
		}

		/// <summary>
		/// Call back
		/// </summary>
		/// <param name="field_info">Required.FieldInfo object.</param>
		/// <param name="vValue">Required.Object value.</param>
		/// <returns>Boolean value.</returns>
		public bool Callback(FieldInfo field_info, ref object vValue)
		{
			bool flag2 = default(bool);
			Type fieldType = field_info.FieldType;
			if (fieldType == null)
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Empty")), 5);
			}
			if (fieldType.IsArray)
			{
				VBFixedArrayAttribute attribute = default(VBFixedArrayAttribute);
				int length = 0;
				object[] customAttributes = field_info.GetCustomAttributes(typeof(VBFixedArrayAttribute), false);
				if ((customAttributes != null) && (customAttributes.Length != 0))
				{
					attribute = (VBFixedArrayAttribute)customAttributes[0];
				}
				Type elementType = fieldType.GetElementType();
				if (elementType == typeof(string))
				{
					customAttributes = field_info.GetCustomAttributes(typeof(VBFixedStringAttribute), false);
					if ((customAttributes == null) || (customAttributes.Length == 0))
					{
						length = -1;
					}
					else
					{
						length = ((VBFixedStringAttribute)customAttributes[0]).Length;
					}
				}
				if (attribute == null)
				{
					m_oFile.PutDynamicArray(0L, (Array)vValue, false, -1);
					return flag2;
				}
				m_oFile.PutFixedArray(0L, (Array)vValue, elementType, length, attribute.UpperBound1, attribute.UpperBound2);
				return flag2;
			}
			switch (Type.GetTypeCode(fieldType)) {
				case TypeCode.DBNull:
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "DBNull")), 5);

				case TypeCode.Boolean:
					m_oFile.PutBoolean(0L, BooleanType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Char:
					m_oFile.PutChar(0L, CharType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Byte:
					m_oFile.PutByte(0L, ByteType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Int16:
					m_oFile.PutShort(0L, ShortType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Int32:
					m_oFile.PutInteger(0L, IntegerType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Int64:
					m_oFile.PutLong(0L, LongType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Single:
					m_oFile.PutSingle(0L, SingleType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Double:
					m_oFile.PutDouble(0L, DoubleType.FromObject(vValue), false);
					return flag2;

				case TypeCode.Decimal:
					m_oFile.PutDecimal(0L, DecimalType.FromObject(vValue, CultureInfo.InvariantCulture), false);
					return flag2;

				case TypeCode.DateTime:
					m_oFile.PutDate(0L, DateType.FromObject(vValue), false);
					return flag2;

				case TypeCode.String:
					{
						string str = default(string);
						if (vValue != null)
						{
							str = vValue.ToString();
						}
						object[] array2 = field_info.GetCustomAttributes(typeof(VBFixedStringAttribute), false);
						if ((array2 != null) && (array2.Length != 0))
						{
							VBFixedStringAttribute attribute2 = (VBFixedStringAttribute)array2[0];
							int num2 = attribute2.Length;
							if (num2 == 0)
							{
								num2 = -1;
							}
							m_oFile.PutFixedLengthString(0L, str, num2);
							return flag2;
						}
						m_oFile.PutStringWithLength(0L, str);
						return flag2;
					}
			}
			if (fieldType == typeof(object))
			{
				m_oFile.PutObject(vValue, 0L, true);
				return flag2;
			}
			if (fieldType == typeof(Exception))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Exception")), 5);
			}
			if (fieldType == typeof(Missing))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Missing")), 5);
			}
			throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, fieldType.Name)), 5);
		}
	}
}
