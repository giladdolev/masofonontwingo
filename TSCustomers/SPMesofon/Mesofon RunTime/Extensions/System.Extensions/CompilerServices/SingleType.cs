using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// SingleType class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class SingleType
	{
		private SingleType()
		{

		}
		// Methods

		/// <summary>
		/// Converts a specified decimal value to a single-precision floating-point number.
		/// </summary>
		/// <param name="valueInterface">IConvertible value</param>
		/// <returns>Float value.</returns>
		private static float DecimalToSingle(IConvertible valueInterface)
		{
			return Convert.ToSingle(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts the value of the specified object to a single-precision floating-point number.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <returns>The Single value that corresponds to Value.</returns>
		public static float FromObject(object value)
		{
			return FromObject(value, null);
		}

		/// <summary>
		/// Converts the value of the specified object to a single-precision floating-point number.
		/// </summary>
		/// <param name="value">An object that implements the IConvertible interface, or null.</param>
		/// <param name="numberFormat">A NumberFormatInfo object that defines how numeric values are formatted and displayed, depending on the culture.</param>
		/// <returns>The Single value that corresponds to Value.</returns>
		public static float FromObject(object value, NumberFormatInfo numberFormat)
		{
			if (value == null)
			{
				return 0f;
			}
			
			IConvertible valueInterface = value as IConvertible;
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return (float)((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (float)((byte)value);
						}
						return (float)valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return (float)((short)value);
						}
						return (float)valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return (float)((int)value);
						}
						return (float)valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return (float)((long)value);
						}
						return (float)valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return (float)value;
						}
						return valueInterface.ToSingle(null);

					case TypeCode.Double:
						if (value is double)
						{
							return (float)((double)value);
						}
						return (float)valueInterface.ToDouble(null);

					case TypeCode.Decimal:
						return DecimalToSingle(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null), numberFormat);
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Single"));
		}

		/// <summary>
		/// Returns a Single value that corresponds to the specified string.
		/// </summary>
		/// <param name="value">Required. String to convert to a Single value.</param>
		/// <returns>The Single value that corresponds to Value.</returns>
		public static float FromString(string value)
		{
			return FromString(value, null);
		}

		/// <summary>
		/// Converts the specified string representation of a number to an equivalent single-precision floating-point number.
		/// </summary>
		/// <param name="value">A string that contains the number to convert.</param>
		/// <param name="numberFormat">A NumberFormatInfo object that defines how numeric values are formatted and displayed, depending on the culture.</param>
		/// <returns>The Single value that corresponds to Value.</returns>
		public static float FromString(string value, NumberFormatInfo numberFormat)
		{
			float num;
			if (value == null)
			{
				return 0f;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return (float)num2;
				}
				num = (float)DoubleType.Parse(value, numberFormat);
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Single"), exception);
			}
			return num;
		}
	}
}
