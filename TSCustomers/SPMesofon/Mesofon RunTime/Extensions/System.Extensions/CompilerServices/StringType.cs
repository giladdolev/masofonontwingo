using System.Text;
using System.Threading;
using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// StringType class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	public sealed class StringType
	{

		private StringType()
		{

		}
		/// <summary>
		/// GENERAL_FORMAT constant
		/// </summary>
		private const string GENERAL_FORMAT = "G";

		/// <summary>
		/// OptionCompareTextFlags constant
		/// </summary>
		internal const CompareOptions OptionCompareTextFlags = (CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase);

		/// <summary>
		/// Returns the number of source characters to skip over to handle an asterisk in the pattern
		/// </summary>
		/// <param name="pattern">String expression representing a pattern</param>
		/// <param name="source">String expression representing the source</param>
		/// <param name="sourceEndIndex">Integer value representing the source's end index</param>
		/// <param name="compareOption">Indicates how to compare strings when calling comparison functions</param>
		/// <param name="ci">Culture-sensitive string comparisons info</param>
		/// <returns>Integer value representing the source characters to skip over to handle an asterisk in the pattern</returns>
		private static int AsteriskSkip(string pattern, string source, int sourceEndIndex, CompareMethod compareOption, CompareInfo ci)
		{
			int num2 = 0;
			int num4 = 0;
			int num3 = StringsEx.Len(pattern);
			while (num4 < num3)
			{
				bool flag = default(bool);
				bool flag2 = default(bool);
				bool flag3 = default(bool);
				switch (pattern[num4]) {
					case '-':
						if (pattern[num4 + 1] == ']')
						{
							flag2 = true;
						}
						break;

					case '!':
						if (pattern[num4 + 1] == ']')
						{
							flag2 = true;
						}
						else
						{
							flag3 = true;
						}
						break;

					case '[':
						if (flag)
						{
							flag2 = true;
						}
						else
						{
							flag = true;
						}
						break;

					case ']':
						if (flag2 || !flag)
						{
							num2++;
							flag3 = true;
						}
						flag2 = false;
						flag = false;
						break;

					case '*':
						if (num2 > 0)
						{
							CompareOptions ordinal;
							if (flag3)
							{
								num2 = MultipleAsteriskSkip(pattern, source, num2, compareOption);
								return (sourceEndIndex - num2);
							}
							string str = pattern.Substring(0, num4);
							if (compareOption == CompareMethod.Binary)
							{
								ordinal = CompareOptions.Ordinal;
							}
							else
							{
								ordinal = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase;
							}
							return ci.LastIndexOf(source, str, ordinal);
						}
						break;

					case '?':
					case '#':
						if (flag)
						{
							flag2 = true;
						}
						else
						{
							num2++;
							flag3 = true;
						}
						break;

					default:
						if (flag)
						{
							flag2 = true;
						}
						else
						{
							num2++;
						}
						break;
				}
				num4++;
			}
			return (sourceEndIndex - num2);
		}

		/// <summary>
		/// Returns a String value that corresponds to the specified Boolean.
		/// </summary>
		/// <param name="value">Required. Boolean to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromBoolean(bool value)
		{
			if (value)
			{
				return bool.TrueString;
			}
			return bool.FalseString;
		}

		/// <summary>
		/// Returns a String value that corresponds to the specified Byte.
		/// </summary>
		/// <param name="value">Required. Byte to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromByte(byte value)
		{
			return value.ToString(null, null);
		}


		/// <summary>
		/// Returns a String value that corresponds to the specified Char.
		/// </summary>
		/// <param name="value">Required. Char to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromChar(char value)
		{
			return value.ToString();
		}







		/// <summary>
		/// Returns a String value that corresponds to the specified date.
		/// </summary>
		/// <param name="value">Required. Date to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromDate(DateTime value)
		{
			long ticks = value.TimeOfDay.Ticks;
			if ((ticks == value.Ticks) || (((value.Year == 0x76b) && (value.Month == 12)) && (value.Day == 30)))
			{
				return value.ToString("T", null);
			}
			if (ticks == 0L)
			{
				return value.ToString("d", null);
			}
			return value.ToString("G", null);
		}

		/// <summary>
		/// Returns a String value that corresponds to the specified Decimal.
		/// </summary>
		/// <param name="value">Required. Decimal to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromDecimal(decimal value)
		{
			return FromDecimal(value, null);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Decimal and number format information.
		/// </summary>
		/// <param name="value">Required. Decimal to convert to a String value.</param>
		/// <param name="numberFormat">A NumberFormatInfo object that defines how numeric values are formatted and displayed, depending on the culture.</param>
		/// <returns>
		/// The String value corresponding to Value.
		/// </returns>
		public static string FromDecimal(decimal value, IFormatProvider numberFormat)
		{
			return value.ToString("G", numberFormat);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Double.
		/// </summary>
		/// <param name="value">Required. Double to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromDouble(double value)
		{
			return FromDouble(value, null);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Double and number format information.
		/// </summary>
		/// <param name="value">Required. Double to convert to a String value.</param>
		/// <param name="numberFormat">A NumberFormatInfo object that defines how numeric values are formatted and displayed, depending on the culture.</param>
		/// <returns>
		/// The String value corresponding to Value.
		/// </returns>
		public static string FromDouble(double value, IFormatProvider numberFormat)
		{
			return value.ToString("G", numberFormat);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Integer.
		/// </summary>
		/// <param name="value">Required. Integer to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromInteger(int value)
		{
			return value.ToString(null, null);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Int64 (64-bit integer).
		/// </summary>
		/// <param name="value">Required. Int64 to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromLong(long value)
		{
			return value.ToString(null, null);
		}

		/// <summary>
		/// Returns a String value that corresponds to the specified object.
		/// </summary>
		/// <param name="value">Required. Object to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromObject(object value)
		{
			if (value == null)
			{
				return null;
			}
			
			string strValue = value as string;
			if (!String.IsNullOrEmpty(strValue))
			{
				return strValue;
			}
			IConvertible convertible = value as IConvertible;
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.Boolean:
						return FromBoolean(convertible.ToBoolean(null));

					case TypeCode.Char:
						return FromChar(convertible.ToChar(null));

					case TypeCode.SByte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case (TypeCode.DateTime | TypeCode.Object):
						goto Label_0132;

					case TypeCode.Byte:
						return FromByte(convertible.ToByte(null));

					case TypeCode.Int16:
						return FromShort(convertible.ToInt16(null));

					case TypeCode.Int32:
						return FromInteger(convertible.ToInt32(null));

					case TypeCode.Int64:
						return FromLong(convertible.ToInt64(null));

					case TypeCode.Single:
						return FromSingle(convertible.ToSingle(null), CultureInfo.InvariantCulture);

					case TypeCode.Double:
						return FromDouble(convertible.ToDouble(null), CultureInfo.InvariantCulture);

					case TypeCode.Decimal:
						return FromDecimal(convertible.ToDecimal(null), CultureInfo.InvariantCulture);

					case TypeCode.DateTime:
						return FromDate(convertible.ToDateTime(CultureInfo.InvariantCulture));

					case TypeCode.String:
						return convertible.ToString(null);
				}
			}
			else if ((value is char[]) && (((Array)value).Rank == 1))
			{
				return new string(CharArrayType.FromObject(value));
			}
			Label_0132:
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "String"));
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Int16 (16-bit integer).
		/// </summary>
		/// <param name="value">Required. Int616 to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromShort(short value)
		{
			return value.ToString(null, (IFormatProvider)null);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Single.
		/// </summary>
		/// <param name="value">Required. Single to convert to a String value.</param>
		/// <returns>The String value corresponding to Value.</returns>
		public static string FromSingle(float value)
		{
			return FromSingle(value, null);
		}

		/// <summary>
		/// Returns a String value that corresponds to a specified Single and number format information.
		/// </summary>
		/// <param name="value">Required. Float to convert to a String value.</param>
		/// <param name="numberFormat">A NumberFormatInfo object that defines how numeric values are formatted and displayed, depending on the culture.</param>
		/// <returns>
		/// The String value corresponding to Value.
		/// </returns>
		public static string FromSingle(float value, IFormatProvider numberFormat)
		{
			return value.ToString(null, numberFormat);
		}

		/// <summary>
		/// Determines whether the value is a hexadecimal or octal value.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <param name="i64Value">Type: System..::..UInt64%</param>
		/// <returns>True if value is hexadecimal or octal; otherwise, false.</returns>
		static internal bool IsHexOrOctValue(string value, ref long i64Value)
		{
			int num = 0;
			int length = value.Length;
			while (num < length)
			{
				char ch = value[num];
				if (ch == '&')
				{
					ch = char.ToLower(value[num + 1], CultureInfo.InvariantCulture);
					string str = ToHalfwidthNumbers(value.Substring(num + 2));
					switch (ch) {
						case 'h':
							i64Value = Convert.ToInt64(str, 0x10);
							goto Label_007C;

						case 'o':
							i64Value = Convert.ToInt64(str, 8);
							goto Label_007C;
					}
					throw new FormatException();
				}
				if ((ch != ' ') && (ch != '　'))
				{
					return false;
				}
				num++;
			}
			return false;
			Label_007C:
			return true;
		}

		public static void MidStmtStr(ref string sDest, int startPosition, int maxInsertLength, string sInsert)
		{
			int length = 0;
			int num3 = 0;
			if (sDest != null)
			{
				length = sDest.Length;
			}
			if (sInsert != null)
			{
				num3 = sInsert.Length;
			}
			startPosition--;
			if ((startPosition < 0) || (startPosition >= length))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Start"));
			}
			if (maxInsertLength < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Length"));
			}
			if (num3 > maxInsertLength)
			{
				num3 = maxInsertLength;
			}
			if (num3 > (length - startPosition))
			{
				num3 = length - startPosition;
			}
			if (num3 != 0)
			{
				StringBuilder builder = new StringBuilder(length);
				if (startPosition > 0)
				{
					builder.Append(sDest, 0, startPosition);
				}
				builder.Append(sInsert, 0, num3);
				int count = length - (startPosition + num3);
				if (count > 0)
				{
					builder.Append(sDest, startPosition + num3, count);
				}
				sDest = builder.ToString();
			}
		}

		/// <summary>
		/// Returns the number of source characters to skip over to handle multiple asterisks in the pattern
		/// </summary>
		/// <param name="pattern">String expression representing a pattern</param>
		/// <param name="source">String expression representing the source</param>
		/// <param name="count">Integer value representing the number of characters</param>
		/// <param name="compareOption">Indicates how to compare strings when calling comparison functions</param>
		/// <returns>Integer value representing the source characters to skip over to handle multiple asterisks in the pattern</returns>
		private static int MultipleAsteriskSkip(string pattern, string source, int count, CompareMethod compareOption)
		{
			int num3 = StringsEx.Len(source);
			while (count < num3)
			{
				bool flag;
                string _source = source.Substring(num3 - count);
				try
				{
                    flag = StrLike(_source, pattern, compareOption);
				}
				catch (ArgumentException)
				{
					flag = false;
				}
				if (flag)
				{
					return count;
				}
				count++;
			}
			return count;
		}

		/// <summary>
		/// Returns -1, 0, or 1, based on the result of a string comparison.
		/// </summary>
		/// <param name="sLeft">Required. Any valid String expression.</param>
		/// <param name="sRight">Required. Any valid String expression.</param>
		/// <param name="textCompare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
		/// <returns>Integer value.</returns>
		public static int StrCmp(string sLeft, string sRight, bool textCompare)
		{
			if (sLeft == null)
			{
				sLeft = "";
			}
			if (sRight == null)
			{
				sRight = "";
			}
			if (textCompare)
			{
				return Utils.CultureInfo.CompareInfo.Compare(sLeft, sRight, CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase);
			}
			return string.CompareOrdinal(sLeft, sRight);
		}

		/// <summary>
		/// Compares the parameters Source and Pattern and returns the same results as the Like operator.
		/// </summary>
		/// <param name="source">Required. Any String expression.</param>
		/// <param name="pattern">Any String expression conforming to the pattern-matching conventions described in Like Operator.</param>
		/// <param name="compareOption">Specifies how to compare strings to patterns, according to the CompareMethod.</param>
		/// <returns>A Boolean value indicating whether or not the string satisfies the pattern.</returns>
		public static bool StrLike(string source, string pattern, CompareMethod compareOption)
		{
			if (compareOption == CompareMethod.Binary)
			{
				return StrLikeBinary(source, pattern);
			}
			return StrLikeText(source, pattern);
		}

		/// <summary>
		/// Compares the parameters Source and Pattern and returns the same results as the Like operator, using binary comparison.
		/// </summary>
		/// <param name="source">Required. Any String expression.</param>
		/// <param name="pattern">Required. Any String expression conforming to the pattern-matching conventions described in Like Operator.</param>
		/// <returns>A Boolean value indicating whether or not the string satisfies the pattern.</returns>
		public static bool StrLikeBinary(string source, string pattern)
		{
			
			bool flag = default(bool);
			int length = 0;
			int num2 = 0;
			char ch3 = default(char);
			int num4 = 0;
			int num5 = 0;
			if (pattern == null)
			{
				length = 0;
			}
			else
			{
				length = pattern.Length;
			}
			if (source == null)
			{
				num4 = 0;
			}
			else
			{
				num4 = source.Length;
			}
			if (num5 < num4)
			{
				ch3 = source[num5];
			}
			while (num2 < length)
			{
				bool flag2 = default(bool);
				bool flag4 = default(bool);
				bool flag5 = default(bool);
				char p = pattern[num2];
				if (p == '*')
				{
					if (flag)
					{
						flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
						flag4 = true;
					}
					else
					{
						int num3 = AsteriskSkip(pattern.Substring(num2 + 1), source.Substring(num5), num4 - num5, CompareMethod.Binary, StringsEx.s_InvariantCompareInfo);
						if (num3 < 0)
						{
							return false;
						}
						if (num3 > 0)
						{
							num5 += num3;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
				}
				else
				{
					char ch = default(char);
					bool flag3 = default(bool);
					char ch4 = default(char);
					if (p == '?')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (ch3 >= ch4) && (ch3 <= ch);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							}
						}
						else
						{
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '#')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (ch3 >= ch4) && (ch3 <= ch);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							}
						}
						else
						{
							if (!char.IsDigit(ch3))
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '-')
					{
						if ((flag && flag3) && flag4)
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
						}
						if (!flag || ((flag && !flag4) && !flag3))
						{
							flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						flag3 = true;
					}
					else if (p == '!')
					{
						if (flag && !flag5)
						{
							flag5 = true;
							flag2 = true;
						}
						else
						{
							flag4 = true;
							flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '[')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (ch3 >= ch4) && (ch3 <= ch);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							}
						}
						else
						{
							flag = true;
							ch4 = '\0';
							ch = '\0';
							flag4 = false;
						}
					}
					else if (p == ']')
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							flag4 = true;
						}
						if (flag4)
						{
							if (!flag2)
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						else if (flag3)
						{
							if (!flag2)
							{
								break;
							}
						}
						else if (flag5)
						{
							if ('!' != ch3)
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						flag2 = false;
						flag4 = false;
						flag5 = false;
						flag3 = false;
					}
					else
					{
						flag4 = true;
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (ch3 > ch4) && (ch3 <= ch);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag2 = StrLikeCompareBinary(flag5, flag2, p, ch3);
							}
						}
						else
						{
							if ((p != ch3) && !flag5)
							{
								break;
							}
							flag5 = false;
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
							else if (num5 > num4)
							{
								return false;
							}
						}
					}
				}
				num2++;
			}
			if (flag)
			{
				if (num4 != 0)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
				}
				return false;
			}
			return ((num2 == length) && (num5 == num4));
		}

		/// <summary>
		/// Compares the parameters p and s 
		/// </summary>
		/// <param name="ci">Culture-sensitive string comparisons info</param>
		/// <param name="seenNot">Bool value</param>
		/// <param name="match">Bool value</param>
		/// <param name="p">Character value for comparison</param>
		/// <param name="s">Character value for comparison</param>
		/// <param name="options">Indicates how to compare strings when calling comparison functions</param>
		/// <returns>True if the parameters are equal, else False</returns>
		private static bool StrLikeCompare(CompareInfo ci, bool seenNot, bool match, char p, char s, CompareOptions options)
		{
			if (seenNot && match)
			{
				if (options == CompareOptions.Ordinal)
				{
					return (p != s);
				}
				return (ci.Compare(FromChar(p), FromChar(s), options) != 0);
			}
			if (seenNot || match)
			{
				return match;
			}
			if (options == CompareOptions.Ordinal)
			{
				return (p == s);
			}
			return (ci.Compare(FromChar(p), FromChar(s), options) == 0);
		}

		/// <summary>
		/// Compares the parameters p and s binary
		/// </summary>
		/// <param name="seenNot">Bool value</param>
		/// <param name="match">Bool value</param>
		/// <param name="p">Character value for comparison</param>
		/// <param name="s">Character value for comparison</param>
		/// <returns>True if the parameters are equal, else False</returns>
		private static bool StrLikeCompareBinary(bool seenNot, bool match, char p, char s)
		{
			if (seenNot && match)
			{
				return (p != s);
			}
			if (!seenNot && !match)
			{
				return (p == s);
			}
			return match;
		}

		/// <summary>
		/// Indicates how to compare strings when calling comparison functions.
		/// </summary>
		public enum CompareMethod
		{
			/// <summary>
			/// As binary compare strings when calling comparison functions
			/// </summary>
			Binary,
			/// <summary>
			/// As text compare strings when calling comparison functions
			/// </summary>
			Text
		}

		/// <summary>
		/// Compares the parameters Source and Pattern and returns the same results as the Like operator, using text comparison.
		/// </summary>
		/// <param name="source">Required. Any String expression.</param>
		/// <param name="pattern">Required. Any String expression conforming to the pattern-matching conventions described in Like Operator.</param>
		/// <returns>A Boolean value indicating whether or not the string satisfies the pattern. </returns>
		public static bool StrLikeText(string source, string pattern)
		{
			
			if (String.IsNullOrEmpty(source))
			{
                throw new ArgumentNullException("source");
			}
			
			bool flag = default(bool);
			int length = 0;
			int num2 = 0;
			char ch3 = default(char);
			int num4 = 0;
			int num5 = 0;
			if (pattern == null)
			{
				length = 0;
			}
			else
			{
				length = pattern.Length;
			}
			if (source == null)
			{
				num4 = 0;
			}
			else
			{
				num4 = source.Length;
			}
			if (num5 < num4)
			{
				ch3 = source[num5];
			}
			CompareInfo compareInfo = Utils.CultureInfo.CompareInfo;
			CompareOptions options = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase;
			while (num2 < length)
			{
				bool flag2 = default(bool);
				bool flag4 = default(bool);
				bool flag5 = default(bool);
				char p = pattern[num2];
				if (p == '*')
				{
					if (flag)
					{
						flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
						flag4 = true;
					}
					else
					{
						int num3 = AsteriskSkip(pattern.Substring(num2 + 1), source.Substring(num5), num4 - num5, CompareMethod.Text, compareInfo);
						if (num3 < 0)
						{
							return false;
						}
						if (num3 > 0)
						{
							num5 += num3;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
				}
				else
				{
					char ch = default(char);
					bool flag3 = default(bool);
					char ch4 = default(char);
					if (p == '?')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (compareInfo.Compare(FromChar(ch4), FromChar(ch3), options) < 0) && (compareInfo.Compare(FromChar(ch), FromChar(ch3), options) >= 0);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							}
						}
						else
						{
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '#')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (compareInfo.Compare(FromChar(ch4), FromChar(ch3), options) < 0) && (compareInfo.Compare(FromChar(ch), FromChar(ch3), options) >= 0);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							}
						}
						else
						{
							if (!char.IsDigit(ch3))
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '-')
					{
						if ((flag && flag3) && flag4)
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
						}
						if (!flag || ((flag && !flag4) && !flag3))
						{
							flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						flag3 = true;
					}
					else if (p == '!')
					{
						if (flag && !flag5)
						{
							flag5 = true;
							flag2 = true;
						}
						else
						{
							flag4 = true;
							flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
					}
					else if (p == '[')
					{
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									flag2 = (compareInfo.Compare(FromChar(ch4), FromChar(ch3), options) < 0) && (compareInfo.Compare(FromChar(ch), FromChar(ch3), options) >= 0);
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag4 = true;
								flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							}
						}
						else
						{
							flag = true;
							ch4 = '\0';
							ch = '\0';
							flag4 = false;
						}
					}
					else if (p == ']')
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							flag4 = true;
						}
						if (flag4)
						{
							if (!flag2)
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						else if (flag3)
						{
							if (!flag2)
							{
								break;
							}
						}
						else if (flag5)
						{
							if (compareInfo.Compare("!", FromChar(ch3)) != 0)
							{
								break;
							}
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
						}
						flag2 = false;
						flag4 = false;
						flag5 = false;
						flag3 = false;
					}
					else
					{
						flag4 = true;
						if (flag)
						{
							if (flag3)
							{
								flag3 = false;
								ch = p;
								if (ch4 > ch)
								{
									throw ExceptionUtils.VbMakeException(0x5d);
								}
								if ((flag5 && flag2) || (!flag5 && !flag2))
								{
									if (options == CompareOptions.Ordinal)
									{
										flag2 = (ch3 > ch4) && (ch3 <= ch);
									}
									else
									{
										flag2 = (compareInfo.Compare(FromChar(ch4), FromChar(ch3), options) < 0) && (compareInfo.Compare(FromChar(ch), FromChar(ch3), options) >= 0);
									}
									if (flag5)
									{
										flag2 = !flag2;
									}
								}
							}
							else
							{
								ch4 = p;
								flag2 = StrLikeCompare(compareInfo, flag5, flag2, p, ch3, options);
							}
						}
						else
						{
							if (options == CompareOptions.Ordinal)
							{
								if ((p != ch3) && !flag5)
								{
									break;
								}
							}
							else if ((compareInfo.Compare(FromChar(p), FromChar(ch3), options) != 0) && !flag5)
							{
								break;
							}
							flag5 = false;
							num5++;
							if (num5 < num4)
							{
								ch3 = source[num5];
							}
							else if (num5 > num4)
							{
								return false;
							}
						}
					}
				}
				num2++;
			}
			if (flag)
			{
				if (num4 != 0)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Pattern"));
				}
				return false;
			}
			return ((num2 == length) && (num5 == num4));
		}


		public static string ToHalfwidthNumbers(string s)
		{
			CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
			int num2 = currentCulture.LCID & 0x3ff;
			if (((num2 != 4) && (num2 != 0x11)) && (num2 != 0x12))
			{
				return s;
			}
			return StringsEx.vbLCMapString(currentCulture, 0x400000, s);
		}
	}





}
