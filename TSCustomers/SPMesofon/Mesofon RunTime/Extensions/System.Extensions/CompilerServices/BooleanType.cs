using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// BooleanType class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public static class BooleanType
	{
		// Methods
		// Methods
		/// <summary>
		/// Converts the value of the specified IConvertible value to an equivalent Boolean value.
		/// </summary>
		/// <param name="valueInterface">Required. IConvertible value.</param>
		/// <returns>Boolean value.</returns>
		private static bool DecimalToBoolean(IConvertible valueInterface)
		{
			return Convert.ToBoolean(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts the object value to an equivalent Boolean value.  
		/// </summary>
		/// <param name="value">Required. Object value.</param>
		/// <returns>Boolean value</returns>
		public static bool FromObject(object value)
		{
			if (value == null)
			{
				return false;
			}
			IConvertible valueInterface = value as IConvertible;
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						if (value is bool)
						{
							return (bool)value;
						}
						return valueInterface.ToBoolean(null);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (((byte)value) > 0);
						}
						return (valueInterface.ToByte(null) > 0);

					case TypeCode.Int16:
						if (value is short)
						{
							return (((short)value) > 0);
						}
						return (valueInterface.ToInt16(null) > 0);

					case TypeCode.Int32:
						if (value is int)
						{
							return (((int)value) > 0);
						}
						return (valueInterface.ToInt32(null) > 0);

					case TypeCode.Int64:
						if (value is long)
						{
							return (((long)value) > 0L);
						}
						return (valueInterface.ToInt64(null) > 0L);

					case TypeCode.Single:
						if (value is float)
						{
							return !(((float)value) == 0f);
						}
						return !(valueInterface.ToSingle(null) == 0f);

					case TypeCode.Double:
						if (value is double)
						{
							return !(((double)value) == 0.0);
						}
						return !(valueInterface.ToDouble(null) == 0.0);

					case TypeCode.Decimal:
						return DecimalToBoolean(valueInterface);

					case TypeCode.String:
						string strValue = value as string;
						if (!String.IsNullOrEmpty(strValue))
						{
							return FromString(strValue);
						}
						return FromString(valueInterface.ToString(null));
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Boolean"));
		}

		/// <summary>
		///  Converts the string value to an equivalent Boolean value.  
		/// </summary>
		/// <param name="value">Required. String value.</param>
		/// <returns>Boolean value.</returns>
		public static bool FromString(string value)
		{
			bool flag = default(bool);
			if (value == null)
			{
				value = "";
			}
			try
			{
				long num = 0;
				CultureInfo cultureInfo = Utils.CultureInfo;
				if (string.Compare(value, bool.FalseString, true, cultureInfo) == 0)
				{
					return false;
				}
				if (string.Compare(value, bool.TrueString, true, cultureInfo) == 0)
				{
					return true;
				}
				if (StringType.IsHexOrOctValue(value, ref num))
				{
					return (num > 0L);
				}
				flag = !(DoubleType.Parse(value) == 0.0);
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Boolean"), exception);
			}
			return flag;
		}
	}
}
