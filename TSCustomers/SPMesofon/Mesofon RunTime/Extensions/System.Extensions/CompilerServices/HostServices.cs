using System.ComponentModel;
using System.Windows.Forms;

namespace System.Extensions.CompilerServices
{

	/// <summary>
	/// Creates a host service
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class HostServices
	{
		private HostServices()
		{

		}

		// Fields
		private static IVbHost _s_host;

		// Properties
		/// <summary>
		///  Gets or sets the current Visual Basic host object
		/// </summary>
		public static IVbHost VBHost
		{
			get { return _s_host; }
			set { _s_host = value; }
		}
	}
	/// <summary>
	/// Represents a host window for Visual Basic
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public interface IVbHost
	{
		// Methods
		IWin32Window GetParentWindow();
		string GetWindowTitle();
	}

}
