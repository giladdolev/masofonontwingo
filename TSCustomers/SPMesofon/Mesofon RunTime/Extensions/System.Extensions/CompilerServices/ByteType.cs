using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// ByteType class that provides methods for manipulating data that potentially represents a Byte value.
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never), StandardModuleAttribute()]
	public static class ByteType
	{
		// Methods

		/// <summary>
		/// Converts a Decimal to an 8-bit unsigned integer.
		/// </summary>
		/// <param name="valueInterface">IConvertible value.</param>
		/// <returns>Byte value.</returns>
		private static byte DecimalToByte(IConvertible valueInterface)
		{
			return Convert.ToByte(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts an object to byte
		/// </summary>
		/// <param name="value">Required. Valid object value.</param>
		/// <returns>Byte value.</returns>
		public static byte FromObject(object value)
		{
			if (value == null)
			{
				return 0;
			}
			// try to cast value as IConvertible
			IConvertible valueInterface = value as IConvertible;
			
			if (valueInterface != null)
			{
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return (byte)((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (byte)value;
						}
						return valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return (byte)((short)value);
						}
						return (byte)valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return (byte)((int)value);
						}
						return (byte)valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return (byte)((long)value);
						}
						return (byte)valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return (byte)Math.Round((double)((float)value));
						}
						return (byte)Math.Round((double)valueInterface.ToSingle(null));

					case TypeCode.Double:
						if (value is double)
						{
							return (byte)Math.Round((double)value);
						}
						return (byte)Math.Round(valueInterface.ToDouble(null));

					case TypeCode.Decimal:
						return DecimalToByte(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null));
				}
			}
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Byte"));
		}

		/// <summary>
		/// Converts the string to byte
		/// </summary>
		/// <param name="value">Required. Valid string value.</param>
		/// <returns>Byte value</returns>
		public static byte FromString(string value)
		{
			byte num = default(byte);
			if (value == null)
			{
				return 0;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return (byte)num2;
				}
				num = (byte)Math.Round(DoubleType.Parse(value));
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Byte"), exception);
			}
			return num;
		}
	}
}
