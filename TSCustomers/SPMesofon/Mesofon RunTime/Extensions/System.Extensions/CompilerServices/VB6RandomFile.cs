using System.ComponentModel;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// The VB6RandomFile class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal class VB6RandomFile : VB6File
	{
		// Methods

		// Methods
		/// <summary>
		/// Create instance of VB6RandomFile
		/// </summary>
		/// <param name="fileName">The file name</param>
		/// <param name="access">The OpenAccess enumeration</param>
		/// <param name="share">The OpenShare enumeration</param>
		/// <param name="lRecordLen">The record length</param>
		public VB6RandomFile(string fileName, FileSystem.OpenAccess access, FileSystem.OpenShare share, int lRecordLen) : base(fileName, access, share, lRecordLen)
		{
		}

		/// <summary>
		/// Close the file
		/// </summary>
		public override void CloseFile()
		{
			if (base.m_sw != null)
			{
				base.m_sw.Flush();
			}
			CloseTheFile();
		}

		/// <summary>
		/// Retrieve open mode for the file
		/// </summary>
		/// <returns>Random open mode for the file</returns>
		public override FileSystem.OpenMode GetMode()
		{
			return FileSystem.OpenMode.Random;
		}
	}
}
