using System.Reflection;
using System.ComponentModel;
using System.Extensions.VBAttributes;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// GetHandler class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal sealed class GetHandler : IRecordEnum
	{
		/// <summary>
		/// Represent VB6File file
		/// </summary>
		private VB6File _m_oFile;

		// Methods
		/// <summary>
		/// Create new instance from the class 
		/// </summary>
		/// <param name="oFile"></param>
		public GetHandler(VB6File oFile)
		{
			// Set file to the specified inputted file
			_m_oFile = oFile;
		}

		/// <summary>
		/// Call back function
		/// </summary>
		/// <param name="field_info">Required. FieldInfo object.</param>
		/// <param name="vValue">Required. Object value.</param>
		/// <returns>Boolean value.</returns>
		public bool Callback(FieldInfo field_info, ref object vValue)
		{
			bool flag2 = default(bool);
			Type fieldType = field_info.FieldType;
			if (fieldType == null)
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Empty")), 5);
			}
			if (fieldType.IsArray)
			{
				Array array = default(Array);
				object[] customAttributes = field_info.GetCustomAttributes(typeof(VBFixedArrayAttribute), false);
				if ((customAttributes == null) || (customAttributes.Length == 0))
				{
					_m_oFile.GetDynamicArray(ref array, fieldType.GetElementType());
				}
				else
				{
					VBFixedArrayAttribute attribute = (VBFixedArrayAttribute)customAttributes[0];
					int firstBound = attribute.UpperBound1;
					int secondBound = attribute.UpperBound2;
					array = (Array)vValue;
					_m_oFile.GetFixedArray(0L, ref array, fieldType.GetElementType(), firstBound, secondBound, -1);
				}
				vValue = array;
				return flag2;
			}
			switch (Type.GetTypeCode(fieldType)) {
				case TypeCode.DBNull:
					throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "DBNull")), 5);

				case TypeCode.Boolean:
					vValue = _m_oFile.GetBoolean(0L);
					return flag2;

				case TypeCode.Char:
					vValue = _m_oFile.GetChar(0L);
					return flag2;

				case TypeCode.Byte:
					vValue = _m_oFile.GetByte(0L);
					return flag2;

				case TypeCode.Int16:
					vValue = _m_oFile.GetShort(0L);
					return flag2;

				case TypeCode.Int32:
					vValue = _m_oFile.GetInteger(0L);
					return flag2;

				case TypeCode.Int64:
					vValue = _m_oFile.GetLong(0L);
					return flag2;

				case TypeCode.Single:
					vValue = _m_oFile.GetSingle(0L);
					return flag2;

				case TypeCode.Double:
					vValue = _m_oFile.GetDouble(0L);
					return flag2;

				case TypeCode.Decimal:
					vValue = _m_oFile.GetDecimal(0L);
					return flag2;

				case TypeCode.DateTime:
					vValue = _m_oFile.GetDate(0L);
					return flag2;

				case TypeCode.String:
					{
						object[] array2 = field_info.GetCustomAttributes(typeof(VBFixedStringAttribute), false);
						if ((array2 != null) && (array2.Length != 0))
						{
							VBFixedStringAttribute attribute2 = (VBFixedStringAttribute)array2[0];
							int length = attribute2.Length;
							if (length == 0)
							{
								length = -1;
							}
							vValue = _m_oFile.GetFixedLengthString(0L, length);
							return flag2;
						}
						vValue = _m_oFile.GetLengthPrefixedString(0L);
						return flag2;
					}
			}
			if (fieldType == typeof(object))
			{
				_m_oFile.GetObject(ref vValue, 0L, true);
				return flag2;
			}
			if (fieldType == typeof(Exception))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Exception")), 5);
			}
			if (fieldType == typeof(Missing))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, "Missing")), 5);
			}
			throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_UnsupportedFieldType2", field_info.Name, fieldType.Name)), 5);
		}
	}
}
