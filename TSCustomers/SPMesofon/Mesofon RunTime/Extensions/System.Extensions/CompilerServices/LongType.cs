using System.Globalization;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// Holds signed 64-bit (8-byte) integers ranging in value from -9,223,372,036,854,775,808 through 9,223,372,036,854,775,807 (9.2...E+18).
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class LongType
	{
		private LongType()
		{

		}

		// Methods
		/// <summary>
		/// Converts decimal number to long number
		/// </summary>
		/// <param name="valueInterface">Decimal number</param>
		/// <returns>Long number</returns>
		private static long DecimalToLong(IConvertible valueInterface)
		{
			return Convert.ToInt64(valueInterface.ToDecimal(null));
		}

		/// <summary>
		/// Converts object to long number
		/// </summary>
		/// <param name="value">Object value</param>
		/// <returns>Long number</returns>
		public static long FromObject(object value)
		{
			// Null reference checking
			if (value == null)
			{
				return 0L;
			}
			IConvertible valueInterface = value as IConvertible;
			
			if (valueInterface != null)
			{
				// Check type code and convert to long number
				switch (valueInterface.GetTypeCode()) {
					case TypeCode.Boolean:
						return (long)((Convert.ToInt32(valueInterface.ToBoolean(null)) << 0x1f) >> 0x1f);

					case TypeCode.Byte:
						if (value is byte)
						{
							return (long)((byte)value);
						}
						return (long)valueInterface.ToByte(null);

					case TypeCode.Int16:
						if (value is short)
						{
							return (long)((short)value);
						}
						return (long)valueInterface.ToInt16(null);

					case TypeCode.Int32:
						if (value is int)
						{
							return (long)((int)value);
						}
						return (long)valueInterface.ToInt32(null);

					case TypeCode.Int64:
						if (value is long)
						{
							return (long)value;
						}
						return valueInterface.ToInt64(null);

					case TypeCode.Single:
						if (value is float)
						{
							return (long)Math.Round((double)((float)value));
						}
						return (long)Math.Round((double)valueInterface.ToSingle(null));

					case TypeCode.Double:
						if (value is double)
						{
							return (long)Math.Round((double)value);
						}
						return (long)Math.Round(valueInterface.ToDouble(null));

					case TypeCode.Decimal:
						return DecimalToLong(valueInterface);

					case TypeCode.String:
						return FromString(valueInterface.ToString(null));
				}
			}
			// Throw and exception
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(value), "Long"));
		}

		/// <summary>
		/// Converts string to long number
		/// </summary>
		/// <param name="value">String value</param>
		/// <returns>Long number</returns>
		public static long FromString(string value)
		{
			long num;
			if (value == null)
			{
				return 0L;
			}
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(value, ref num2))
				{
					return num2;
				}
				num = Convert.ToInt64(decimal.Parse(StringType.ToHalfwidthNumbers(value), NumberStyles.Any, null));
			}
			catch (FormatException exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Long"), exception);
			}
			return num;
		}
	}





}
