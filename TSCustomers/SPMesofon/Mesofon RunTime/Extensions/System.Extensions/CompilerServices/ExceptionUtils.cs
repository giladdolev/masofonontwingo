using System.IO;
using System.ComponentModel;
using System.Extensions.VBExtensions;
using System.Collections.Generic;

namespace System.Extensions.CompilerServices
{
	delegate Exception ExceptionBuilderHandler(string description);

	/// <summary>
	/// Class ExceptionUtils
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public sealed class ExceptionUtils
	{
		private ExceptionUtils()
		{

		}

		// Fields
		internal const int CLASS_E_NOTLICENSED = -2147221230;
		internal const int CO_E_APPDIDNTREG = -2147220994;
		internal const int CO_E_APPNOTFOUND = -2147221003;
		internal const int CO_E_CLASSSTRING = -2147221005;
		internal const int CO_E_SERVER_EXEC_FAILURE = -2146959355;
		internal const int DISP_E_ARRAYISLOCKED = -2147352563;
		internal const int DISP_E_BADINDEX = -2147352565;
		internal const int DISP_E_BADPARAMCOUNT = -2147352562;
		internal const int DISP_E_BADVARTYPE = -2147352568;
		internal const int DISP_E_DIVBYZERO = -2147352558;
		internal const int DISP_E_MEMBERNOTFOUND = -2147352573;
		internal const int DISP_E_NONAMEDARGS = -2147352569;
		internal const int DISP_E_NOTACOLLECTION = -2147352559;
		internal const int DISP_E_OVERFLOW = -2147352566;
		internal const int DISP_E_PARAMNOTFOUND = -2147352572;
		internal const int DISP_E_PARAMNOTOPTIONAL = -2147352561;
		internal const int DISP_E_TYPEMISMATCH = -2147352571;
		internal const int DISP_E_UNKNOWNINTERFACE = -2147352575;
		internal const int DISP_E_UNKNOWNLCID = -2147352564;
		internal const int DISP_E_UNKNOWNNAME = -2147352570;
		internal const int E_ABORT = -2147467260;
		internal const int E_ACCESSDENIED = -2147024891;
		internal const int E_INVALIDARG = -2147024809;
		internal const int E_NOINTERFACE = -2147467262;
		internal const int E_NOTIMPL = -2147467263;
		internal const int E_OUTOFMEMORY = -2147024882;
		internal const int MK_E_CANTOPENFILE = -2147221014;
		internal const int MK_E_INVALIDEXTENSION = -2147221018;
		internal const int MK_E_UNAVAILABLE = -2147221021;
		internal const int REGDB_E_CLASSNOTREG = -2147221164;
		internal const int STG_E_ACCESSDENIED = -2147287035;
		internal const int STG_E_CANTSAVE = -2147286781;
		internal const int STG_E_DISKISWRITEPROTECTED = -2147287021;
		internal const int STG_E_EXTANTMARSHALLINGS = -2147286776;
		internal const int STG_E_FILEALREADYEXISTS = -2147286960;
		internal const int STG_E_FILENOTFOUND = -2147287038;
		internal const int STG_E_INSUFFICIENTMEMORY = -2147287032;
		internal const int STG_E_INUSE = -2147286784;
		internal const int STG_E_INVALIDFUNCTION = -2147287039;
		internal const int STG_E_INVALIDHANDLE = -2147287034;
		internal const int STG_E_INVALIDHEADER = -2147286789;
		internal const int STG_E_INVALIDNAME = -2147286788;
		internal const int STG_E_LOCKVIOLATION = -2147287007;
		internal const int STG_E_MEDIUMFULL = -2147286928;
		internal const int STG_E_NOMOREFILES = -2147287022;
		internal const int STG_E_NOTCURRENT = -2147286783;
		internal const int STG_E_NOTFILEBASEDSTORAGE = -2147286777;
		internal const int STG_E_OLDDLL = -2147286779;
		internal const int STG_E_OLDFORMAT = -2147286780;
		internal const int STG_E_PATHNOTFOUND = -2147287037;
		internal const int STG_E_READFAULT = -2147287010;
		internal const int STG_E_REVERTED = -2147286782;
		internal const int STG_E_SEEKERROR = -2147287015;
		internal const int STG_E_SHAREREQUIRED = -2147286778;
		internal const int STG_E_SHAREVIOLATION = -2147287008;
		internal const int STG_E_TOOMANYOPENFILES = -2147287036;
		internal const int STG_E_UNIMPLEMENTEDFUNCTION = -2147286786;
		internal const int STG_E_UNKNOWN = -2147286787;
		internal const int STG_E_WRITEFAULT = -2147287011;
		internal const int TYPE_E_AMBIGUOUSNAME = -2147319764;
		internal const int TYPE_E_BADMODULEKIND = -2147317571;
		internal const int TYPE_E_BUFFERTOOSMALL = -2147319786;
		internal const int TYPE_E_CANTCREATETMPFILE = -2147316573;
		internal const int TYPE_E_CANTLOADLIBRARY = -2147312566;
		internal const int TYPE_E_CIRCULARTYPE = -2147312508;
		internal const int TYPE_E_DLLFUNCTIONNOTFOUND = -2147319761;
		internal const int TYPE_E_ELEMENTNOTFOUND = -2147319765;
		internal const int TYPE_E_INCONSISTENTPROPFUNCS = -2147312509;
		internal const int TYPE_E_INVALIDSTATE = -2147319767;
		internal const int TYPE_E_INVDATAREAD = -2147319784;
		internal const int TYPE_E_IOERROR = -2147316574;
		internal const int TYPE_E_LIBNOTREGISTERED = -2147319779;
		internal const int TYPE_E_NAMECONFLICT = -2147319763;
		internal const int TYPE_E_OUTOFBOUNDS = -2147316575;
		internal const int TYPE_E_QUALIFIEDNAMEDISALLOWED = -2147319768;
		internal const int TYPE_E_REGISTRYACCESS = -2147319780;
		internal const int TYPE_E_SIZETOOBIG = -2147317563;
		internal const int TYPE_E_TYPEMISMATCH = -2147316576;
		internal const int TYPE_E_UNDEFINEDTYPE = -2147319769;
		internal const int TYPE_E_UNKNOWNLCID = -2147319762;
		internal const int TYPE_E_UNSUPFORMAT = -2147319783;
		internal const int TYPE_E_WRONGTYPEKIND = -2147319766;

		private static Dictionary<int, ExceptionBuilderHandler> _sExceptionDictionery;

		static ExceptionUtils()
		{
			// build delegates
			ExceptionBuilderHandler defaultExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildDefaultException);
			ExceptionBuilderHandler invalidOperationExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.InvalidOperationException);
			ExceptionBuilderHandler argumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildArgumentException);
			ExceptionBuilderHandler missingMemberExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildMissingMemberException);
			ExceptionBuilderHandler overflowExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildOverflowException);
			ExceptionBuilderHandler outOfMemoryArgumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildOutOfMemoryArgumentException);
			ExceptionBuilderHandler indexOutOfRangeArgumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildIndexOutOfRangeArgumentException);
			
			ExceptionBuilderHandler divideByZeroExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildDivideByZeroException);
			ExceptionBuilderHandler invalidCastExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildInvalidCastException);
			ExceptionBuilderHandler stackOverflowArgumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildStackOverflowArgumentException);
			ExceptionBuilderHandler typeLoadExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildTypeLoadException);
			
			ExceptionBuilderHandler fileNotFoundExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildFileNotFoundException);
			ExceptionBuilderHandler endOfStreamExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildEndOfStreamException);
			ExceptionBuilderHandler iOExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildIOException);
			
			ExceptionBuilderHandler nullReferenceArgumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildNullReferenceArgumentException);
			ExceptionBuilderHandler missingFieldExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildMissingFieldException);
			ExceptionBuilderHandler unknownTypeArgumentExceptionHandler = new ExceptionBuilderHandler(ExceptionUtils.BuildUnknownTypeArgumentException);
			
			
			// add delegates to dictionary
			_sExceptionDictionery = new Dictionary<int, ExceptionBuilderHandler>();
			_sExceptionDictionery.Add(0, defaultExceptionHandler);
			
			_sExceptionDictionery.Add(3, invalidOperationExceptionHandler);
			_sExceptionDictionery.Add(20, invalidOperationExceptionHandler);
			_sExceptionDictionery.Add(0x5e, invalidOperationExceptionHandler);
			_sExceptionDictionery.Add(100, invalidOperationExceptionHandler);
			
			_sExceptionDictionery.Add(5, argumentExceptionHandler);
			_sExceptionDictionery.Add(0x1c0, argumentExceptionHandler);
			_sExceptionDictionery.Add(0x1be, argumentExceptionHandler);
			_sExceptionDictionery.Add(0x1c1, argumentExceptionHandler);
			
			_sExceptionDictionery.Add(0x1b6, missingMemberExceptionHandler);
			
			_sExceptionDictionery.Add(6, overflowExceptionHandler);
			
			_sExceptionDictionery.Add(7, outOfMemoryArgumentExceptionHandler);
			_sExceptionDictionery.Add(14, outOfMemoryArgumentExceptionHandler);
			
			_sExceptionDictionery.Add(9, indexOutOfRangeArgumentExceptionHandler);
			_sExceptionDictionery.Add(11, divideByZeroExceptionHandler);
			_sExceptionDictionery.Add(13, invalidCastExceptionHandler);
			_sExceptionDictionery.Add(0x1c, stackOverflowArgumentExceptionHandler);
			_sExceptionDictionery.Add(0x30, typeLoadExceptionHandler);
			
			_sExceptionDictionery.Add(0x35, fileNotFoundExceptionHandler);
			_sExceptionDictionery.Add(0x1b0, fileNotFoundExceptionHandler);
			_sExceptionDictionery.Add(0x4c, fileNotFoundExceptionHandler);
			
			_sExceptionDictionery.Add(0x3e, endOfStreamExceptionHandler);
			
			_sExceptionDictionery.Add(0x39, iOExceptionHandler);
			_sExceptionDictionery.Add(0x34, iOExceptionHandler);
			_sExceptionDictionery.Add(0x36, iOExceptionHandler);
			_sExceptionDictionery.Add(0x37, iOExceptionHandler);
			_sExceptionDictionery.Add(0x3a, iOExceptionHandler);
			_sExceptionDictionery.Add(0x3b, iOExceptionHandler);
			_sExceptionDictionery.Add(0x3d, iOExceptionHandler);
			_sExceptionDictionery.Add(0x3f, iOExceptionHandler);
			_sExceptionDictionery.Add(0x43, iOExceptionHandler);
			_sExceptionDictionery.Add(0x44, iOExceptionHandler);
			_sExceptionDictionery.Add(70, iOExceptionHandler);
			_sExceptionDictionery.Add(0x47, iOExceptionHandler);
			_sExceptionDictionery.Add(0x4a, iOExceptionHandler);
			_sExceptionDictionery.Add(0x4b, iOExceptionHandler);
			
			_sExceptionDictionery.Add(0x5b, nullReferenceArgumentExceptionHandler);
			
			_sExceptionDictionery.Add(0x1a6, missingFieldExceptionHandler);
			
			_sExceptionDictionery.Add(0x1ad, unknownTypeArgumentExceptionHandler);
			_sExceptionDictionery.Add(0x1ce, unknownTypeArgumentExceptionHandler);
		}



		private static Exception BuildUnknownTypeArgumentException(string description)
		{
            return new ArgumentException("Unknown type exception: " + description, "description");
		}

		private static Exception BuildMissingFieldException(string description)
		{
			return new MissingFieldException(description);
		}

		private static Exception BuildNullReferenceArgumentException(string description)
		{
            return new ArgumentException("Null Reference Exception: " + description, "description");
		}

		private static Exception BuildIOException(string description)
		{
			return new IOException(description);
		}

		private static Exception BuildEndOfStreamException(string description)
		{
			return new EndOfStreamException(description);
		}

		private static Exception BuildFileNotFoundException(string description)
		{
			return new FileNotFoundException(description);
		}

		private static Exception BuildTypeLoadException(string description)
		{
			return new TypeLoadException(description);
		}

		private static Exception BuildStackOverflowArgumentException(string description)
		{
            return new ArgumentException("Stack Overflow Exception: " + description, "description");
		}

		private static Exception BuildInvalidCastException(string description)
		{
			return new InvalidCastException(description);
		}

		private static Exception BuildDivideByZeroException(string description)
		{
			return new DivideByZeroException(description);
		}

		private static Exception BuildIndexOutOfRangeArgumentException(string description)
		{
            return new ArgumentException("Index Out Of Range Exception: " + description, "description");
		}

		private static Exception BuildOutOfMemoryArgumentException(string description)
		{
            return new ArgumentException("Out Of Memory Exception: " + description, "description");
		}

		private static Exception BuildOverflowException(string description)
		{
			return new OverflowException(description);
		}

		private static Exception BuildMissingMemberException(string description)
		{
			return new MissingMemberException(description);
		}

		private static Exception BuildArgumentException(string description)
		{
			return new ArgumentException(description);
		}

		private static Exception BuildDefaultException(string description)
		{
			return default(Exception);
		}

		private static Exception InvalidOperationException(string description)
		{
			return new InvalidOperationException(description);
		}

		// Methods

		/// <summary>
		/// Builds the Exception 
		/// </summary>
		/// <param name="number">Required.Error number.</param>
		/// <param name="description"Required.>Error description</param>
		/// <param name="vBDefinedError">Required.Boolean if the error is defined in VB</param>
		/// <returns>Returns the exception.</returns>
		static internal Exception BuildException(int number, string description, ref bool vBDefinedError)
		{
			vBDefinedError = true;
			
			Exception ret;
			ExceptionBuilderHandler ebh;
			
			if (_sExceptionDictionery.TryGetValue(number, out ebh))
			{
				ret = ebh(description);
			}
			else
			{
				vBDefinedError = false;
                ret = new ArgumentException("Unknown type exception: " + description, "number");
			}
			return ret;
		}


		/// <summary>
		/// Makes the exception
		/// </summary>
		/// <param name="hr">Required. vbErrors value</param>
		/// <returns>Returns the exception</returns>
		static internal Exception VbMakeException(int hr)
		{
			string resourceString;
			if ((hr > 0) && (hr <= 0xffff))
			{
				resourceString = Utils.GetResourceString((Utils.vbErrors)hr);
			}
			else
			{
				resourceString = "";
			}
			return VbMakeExceptionEx(hr, resourceString);
		}

		/// <summary>
		/// Makes the exception
		/// </summary>
		/// <param name="ex">Required. Exception value.</param>
		/// <param name="hr">Required. vbErrors value</param>
		/// <returns>Returns the exception.</returns>
		static internal Exception VbMakeException(Exception ex, int hr)
		{
			Information.Err().SetUnmappedError(hr);
			return ex;
		}

		/// <summary>
		/// Makes the exception
		/// </summary>
		/// <param name="number">Required.Error number.</param>
		/// <param name="sMsg">Required.Error message.</param>
		/// <returns>Returns the exception</returns>
		static internal Exception VbMakeExceptionEx(int number, string sMsg)
		{
			bool flag = default(bool);
			Exception exception = BuildException(number, sMsg, ref flag);
			if (flag)
			{
				Information.Err().SetUnmappedError(number);
			}
			return exception;
		}
	}

}
