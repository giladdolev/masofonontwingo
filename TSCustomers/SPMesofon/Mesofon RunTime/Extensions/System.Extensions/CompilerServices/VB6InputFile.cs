using System.ComponentModel;
using System.Extensions.VBExtensions;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// VB6InputFile class
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal class VB6InputFile : VB6File
	{
		// Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="fileName">The name of the file</param>
		/// <param name="share">Indicates how to open a file when calling file-access functions</param>
		public VB6InputFile(string fileName, FileSystem.OpenShare share) : base(fileName, FileSystem.OpenAccess.Read, share, -1)
		{
		}

		/// <summary>
		/// Get the opening mode
		/// </summary>
		/// <returns>OpenMode enum</returns>
		public override FileSystem.OpenMode GetMode()
		{
			// Return Input, file opened for read access
			return FileSystem.OpenMode.Input;
		}

	}
}
