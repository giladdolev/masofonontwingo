using System.Reflection;
using System.IO;
using System.ComponentModel;

namespace System.Extensions.CompilerServices
{
	/// <summary>
	/// IOUtils class
	/// </summary>
	[StandardModuleAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	static internal class IOUtils
	{
		// Methods
		/// <summary>
		/// Finds first file in the assembly
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData to search</param>
		/// <returns>A string that is the name of the parent directory, the name of the last directory in the hierarchy, or the name of a file, including the file name extension.</returns>
		private static string FindFileFilter(AssemblyData oAssemblyData)
		{
			
			
			// Take all FileSystemInfo objects
			FileSystemInfo[] dirFiles = oAssemblyData._dirFiles;
			
			// Loop through FileSystemInfo objects
			for (int i = oAssemblyData._dirNextFileIndex; 1 != 0; i++)
			{
				if (i > dirFiles.GetUpperBound(0))
				{
                    oAssemblyData._dirFiles = null;
                    oAssemblyData._dirNextFileIndex = 0;
					return null;
				}
				FileSystemInfo info = dirFiles[i];
				if (((info.Attributes & (FileAttributes.Directory | FileAttributes.System | FileAttributes.Hidden)) == 0) || ((info.Attributes & oAssemblyData._dirAttributes) != 0))
				{
                    oAssemblyData._dirNextFileIndex = i + 1;
					return dirFiles[i].Name;
				}
			}
		}

		/// <summary>
		/// Finds first file in the assembly
		/// </summary>
		/// <param name="assem">Assembly object</param>
		/// <param name="pathName">Path to the directory</param>
		/// <param name="attributes">FileAttributes object</param>
		/// <returns>A string that is the name of the parent directory, the name of the last directory in the hierarchy, or the name of a file, including the file name extension.</returns>
		static internal string FindFirstFile(Assembly assem, string pathName, FileAttributes attributes)
		{
			// Declare local variable
			string directoryName = default(string);
			
			// Declare local variable
			string fileName;
			
			// FileSystemInfo array to keeps FileSystemInfo objects
			FileSystemInfo[] fileSystemInfos;
			
			// Check the path if it is directory
			if ((pathName.Length > 0) && (pathName[pathName.Length - 1] == Path.DirectorySeparatorChar))
			{
				// Set directory name
				directoryName = pathName;
				
				// Set filename filter
				fileName = "*.*";
			}
			else
			{
				// Check if path is empty
				if (pathName.Length == 0)
				{
					// Set filename filter
					fileName = "*.*";
				}
				else
				{
					// Get filename from path
					fileName = Path.GetFileName(pathName);
					
					// Get directory name from path
					directoryName = Path.GetDirectoryName(pathName);
					
					// Check if filename is empty or equals to '.'
					if (((fileName == null) || (fileName.Length == 0)) || (StringType.StrCmp(fileName, ".", false) == 0))
					{
						// Set filename filter
						fileName = "*.*";
					}
				}
				
				// Check if directory name is empty
				if ((directoryName == null) || (directoryName.Length == 0))
				{
					// Check if path contains root
					if (Path.IsPathRooted(pathName))
					{
						// Set directoryName to root
						directoryName = Path.GetPathRoot(pathName);
					}
					else
					{
						// Preserve current directory
						directoryName = Environment.CurrentDirectory;
						
						// Check if directoryName is not DirectorySeparatorChar
						if (directoryName[directoryName.Length - 1] != Path.DirectorySeparatorChar)
						{
							// Add DirectorySeparatorChar to directoryName
							directoryName = directoryName + StringType.FromChar(Path.DirectorySeparatorChar);
						}
					}
				}
				// Check if directoryName is not DirectorySeparatorChar
				else if (directoryName[directoryName.Length - 1] != Path.DirectorySeparatorChar)
				{
					// Add DirectorySeparatorChar to directoryName
					directoryName = directoryName + StringType.FromChar(Path.DirectorySeparatorChar);
				}
				
				// Check if fileName is referent path
				if (StringType.StrCmp(fileName, "..", false) == 0)
				{
					// Add referent path to directoryName
					directoryName = directoryName + "..\\";
					
					// Set fileName filter
					fileName = "*.*";
				}
			}
			
			// Get directory parent
			DirectoryInfo parent = Directory.GetParent(directoryName + fileName);
			try
			{
				// Get file system info objects
				fileSystemInfos = parent.GetFileSystemInfos(fileName);
			}
			catch (DirectoryNotFoundException)
			{
				// Return empty string in case of exception
				return "";
			}
			catch (NullReferenceException)
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			catch (IOException)
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			
			// Set assembly data from project
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(assem);
			
			// Set file system infos
            assemblyData._dirFiles = fileSystemInfos;
			
			// Set directory next file index
            assemblyData._dirNextFileIndex = 0;
			
			// Set file attributes
            assemblyData._dirAttributes = attributes;
			
			// Set directory name
			//assemblyData.DirName = directoryName;
			
			// If we have more file system info objects
			if ((fileSystemInfos != null) && (fileSystemInfos.Length != 0))
			{
				// Call the function again
				return FindFileFilter(assemblyData);
			}
			
			// Return empty string
			return "";
		}

		/// <summary>
		/// Finds next file in the assembly
		/// </summary>
		/// <param name="assem">Assembly object</param>
		/// <returns>A string that is the name of the parent directory, the name of the last directory in the hierarchy, or the name of a file, including the file name extension.</returns>
		static internal string FindNextFile(Assembly assem)
		{
			// Get assembly data from project
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(assem);
			
			// Check if contains directories
            if (assemblyData._dirFiles == null)
			{
				// Throw an exception
				throw new ArgumentException(Utils.GetResourceString("DIR_IllegalCall"));
			}
			
			// Check for valid index
            if (assemblyData._dirNextFileIndex > assemblyData._dirFiles.GetUpperBound(0))
			{
				// Set m_DirFiles to null
                assemblyData._dirFiles = null;
				
				// Set m_DirNextFileIndex to 0
                assemblyData._dirNextFileIndex = 0;
				
				// Return null
				return null;
			}
			// Call the overload to find the desired file
			return FindFileFilter(assemblyData);
		}
	}





}
