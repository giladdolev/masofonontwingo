using System.Globalization;
using System.ComponentModel;
using System.Extensions.CompilerServices;

namespace System.Extensions
{
	/// <summary>
	/// Class Collection Item
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal sealed class CollectionItem
	{
		// Fields

		/// <summary>
		/// Stores the data from this object
		/// </summary>
		internal object Data_O;

		// Methods

		/// <summary>
		/// Sets the value of the Data_O object
		/// </summary>
		/// <param name="Key">Object whose value is assigned to Data_O</param>
		public CollectionItem()
		{
		}

		/// <summary>
		/// Sets the value of the Data_O object
		/// </summary>
		/// <param name="key">Object whose value is assigned to Data_O</param>
		public CollectionItem(object key)
		{
			Data_O = key;
		}

		/// <summary>
		/// Sets the value of the Data_O object
		/// </summary>
		/// <param name="key">String whose value is assigned to Data_O</param>
		public CollectionItem(string key)
		{
			if (key == null)
			{
				Data_O = null;
			}
			else
			{
				Data_O = key.ToUpper(CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Check if the given object is equal to this object
		/// </summary>
		/// <param name="obj">Object that needs to compare</param>
		/// <returns>True if the object is equal to this</returns>
		public override bool Equals(object obj)
		{
			CollectionItem item;
			if (obj == this)
			{
				return true;
			}
			CollectionItem collObj = obj as CollectionItem;
			if (collObj != null)
			{
				item = collObj;
			}
			else
			{
				return false;
			}
			if ((Data_O == null) || (item.Data_O == null))
			{
				return false;
			}
			string sLeft = StringType.FromObject(Data_O);
			string sRight = StringType.FromObject(item.Data_O);
			return (StringType.StrCmp(sLeft, sRight, false) == 0);
		}

		/// <summary>
		/// Gets the hash code of this item
		/// </summary>
		/// <returns>Returns the hash code</returns>
		public override int GetHashCode()
		{
			if (Data_O == null)
			{
				return 0;
			}
			return StringType.FromObject(Data_O).GetHashCode();
		}

		/// <summary>
		/// Convert the item to string
		/// </summary>
		/// <returns>Returns data from this item converted to string</returns>
		public override string ToString()
		{
			if (Data_O != null)
			{
				return Data_O.ToString();
			}
			
			return string.Empty;
		}
	}

}
