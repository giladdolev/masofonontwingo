using System.Linq;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Extensions.VBEnums;
using System.Extensions.CompilerServices;
using System.Extensions.VBExtensions;

namespace System.Extensions
{
	/// <summary>
	/// Strings class
	/// </summary>
	[StandardModuleAttribute()]
	public static class StringsEx
	{

		// Fields
		/// <summary>
		/// Code Page Identifier
		/// </summary>
		private const int CODEPAGE_SIMPLIFIED_CHINESE = 0x3a8;
		/// <summary>
		/// Code Page Identifier
		/// </summary>
		private const int CODEPAGE_TRADITIONAL_CHINESE = 950;
		private static readonly string[] _currencyNegativeFormatStrings = new string[] {
			"($n)",
			"-$n",
			"$-n",
			"$n-",
			"(n$)",
			"-n$",
			"n-$",
			"n$-",
			"-n $",
			"-$ n",
			"n $-",
			"$ n-",
			"$- n",
			"n- $",
			"($ n)",
			"(n $)",
		};
		private static readonly string[] _currencyPositiveFormatStrings = new string[] {
			"$n",
			"n$",
			"$ n",
			"n $",
		};
		private const int InvariantCultureID = 0x7f;
		private static string _s_CachedOnOffFormatStyle;
		private static string _s_CachedTrueFalseFormatStyle;
		private static string _s_CachedYesNoFormatStyle;
		static internal readonly CompareInfo s_InvariantCompareInfo = CultureInfo.InvariantCulture.CompareInfo;
		private static CultureInfo _s_LastUsedOnOffCulture;
		private static CultureInfo _s_LastUsedTrueFalseCulture;
		private static CultureInfo _s_LastUsedYesNoCulture;
		private static object _s_SyncObject = new object();
		private const string NAMEDFORMAT_CURRENCY = "currency";
		private const string NAMEDFORMAT_FIXED = "fixed";
		private const string NAMEDFORMAT_GENERAL_DATE = "general date";
		private const string NAMEDFORMAT_GENERAL_NUMBER = "general number";
		private const string NAMEDFORMAT_LONG_DATE = "long date";
		private const string NAMEDFORMAT_LONG_TIME = "long time";
		private const string NAMEDFORMAT_MEDIUM_DATE = "medium date";
		private const string NAMEDFORMAT_MEDIUM_TIME = "medium time";
		private const string NAMEDFORMAT_ON_OFF = "on/off";
		private const string NAMEDFORMAT_PERCENT = "percent";
		private const string NAMEDFORMAT_SCIENTIFIC = "scientific";
		private const string NAMEDFORMAT_SHORT_DATE = "short date";
		private const string NAMEDFORMAT_SHORT_TIME = "short time";
		private const string NAMEDFORMAT_STANDARD = "standard";
		private const string NAMEDFORMAT_TRUE_FALSE = "true/false";
		private const string NAMEDFORMAT_YES_NO = "yes/no";
		private static readonly string[] _numberNegativeFormatStrings = new string[] {
			"(n)",
			"-n",
			"- n",
			"n-",
			"n -",
		};
		private const CompareOptions STANDARD_COMPARE_FLAGS = (CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase);

		/// <summary>
		/// Returns a Variant (String) converted as specified.
		/// </summary>
		/// <param name="str">Required. String expression to be converted.</param>
		/// <param name="conversion">Required. Integer. The sum of values specifying the type of conversion to perform.</param>
		/// <returns>String value</returns>
		public static string StrConv(string str, VbStrConvEnum conversion)
		{
			return StrConv(str, conversion, 0);
		}

		/// <summary>
		/// Returns a Variant (String) converted as specified.
		/// </summary>
		/// <param name="str">Required. String expression to be converted.</param>
		/// <param name="conversion">Required. Integer. The sum of values specifying the type of conversion to perform.</param>
		/// <param name="localeID">Optional. The LocaleID, if different than the system LocaleID. (The system LocaleID is the default.)</param>
		/// <returns>String value</returns>
		public static string StrConv(string str, VbStrConvEnum conversion, int localeID)
		{
			string str2 = default(string);
			try
			{
				int num = 0;
				CultureInfo cultureInfo;
				if ((localeID == 0) || (localeID == 1))
				{
					cultureInfo = Utils.CultureInfo;
					localeID = cultureInfo.LCID;
				}
				else
				{
					try
					{
						cultureInfo = new CultureInfo(localeID & 0xffff);
					}
					catch (Exception)
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_LCIDNotSupported1", StringType.FromInteger(localeID)));
					}
				}
				int num2 = PRIMARYLANGID(localeID);
				if ((conversion & ~(VbStrConvEnum.LinguisticCasing | VbStrConvEnum.TraditionalChinese | VbStrConvEnum.SimplifiedChinese | VbStrConvEnum.Hiragana | VbStrConvEnum.Katakana | VbStrConvEnum.Narrow | VbStrConvEnum.Wide | VbStrConvEnum.ProperCase)) != VbStrConvEnum.None)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidVbStrConv"));
				}
				switch ((((int)conversion) & 0x300)) {
					case 0x300:
						throw new ArgumentException(Utils.GetResourceString("Argument_StrConvSCandTC"));

					case 0x100:
						if (!IsValidCodePage(0x3a8) || !IsValidCodePage(950))
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_SCNotSupported"));
						}
						num |= 0x2000000;
						break;

					case 0x200:
						if (!IsValidCodePage(0x3a8) || !IsValidCodePage(950))
						{
							throw new ArgumentException(Utils.GetResourceString("Argument_TCNotSupported"));
						}
						num |= 0x4000000;
						break;
				}
				switch ((conversion & VbStrConvEnum.ProperCase)) {
					case VbStrConvEnum.None:
						if ((conversion & VbStrConvEnum.LinguisticCasing) != VbStrConvEnum.None)
						{
							throw new ArgumentException(Utils.GetResourceString("LinguisticRequirements"));
						}
						goto Label_0175;

					case VbStrConvEnum.Uppercase:
						if (conversion != VbStrConvEnum.Uppercase)
						{
							break;
						}
						return cultureInfo.TextInfo.ToUpper(str);

					case VbStrConvEnum.Lowercase:
						if (conversion != VbStrConvEnum.Lowercase)
						{
							goto Label_016D;
						}
						return cultureInfo.TextInfo.ToLower(str);

					case VbStrConvEnum.ProperCase:
						num = 0;
						goto Label_0175;

					default:
						goto Label_0175;
				}
				num |= 0x200;
				goto Label_0175;
				Label_016D:
				num |= 0x100;
				Label_0175:
				if (((conversion & (VbStrConvEnum.Hiragana | VbStrConvEnum.Katakana)) != VbStrConvEnum.None) && ((num2 != 0x11) || !ValidLCID(localeID)))
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_JPNNotSupported"));
				}
				if ((conversion & (VbStrConvEnum.Narrow | VbStrConvEnum.Wide)) != VbStrConvEnum.None)
				{
					if (((num2 != 0x11) && (num2 != 0x12)) && (num2 != 4))
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_WideNarrowNotApplicable"));
					}
					if (!ValidLCID(localeID))
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_LocalNotSupported"));
					}
				}
				switch ((conversion & (VbStrConvEnum.Narrow | VbStrConvEnum.Wide))) {
					case VbStrConvEnum.Wide:
						num |= 0x800000;
						break;

					case VbStrConvEnum.Narrow:
						num |= 0x400000;
						break;

					case (VbStrConvEnum.Narrow | VbStrConvEnum.Wide):
						throw new ArgumentException(Utils.GetResourceString("Argument_IllegalWideNarrow"));
				}
				VbStrConvEnum conv = conversion & (VbStrConvEnum.Hiragana | VbStrConvEnum.Katakana);
				if (conv != VbStrConvEnum.None)
				{
					if (conv == (VbStrConvEnum.Hiragana | VbStrConvEnum.Katakana))
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_IllegalKataHira"));
					}
					if (conv == VbStrConvEnum.Katakana)
					{
						num |= 0x200000;
					}
					else if (conv == VbStrConvEnum.Hiragana)
					{
						num |= 0x100000;
					}
				}
				if ((conversion & VbStrConvEnum.ProperCase) == VbStrConvEnum.ProperCase)
				{
					return ProperCaseString(cultureInfo, num, str);
				}
				if (num != 0)
				{
					return vbLCMapString(cultureInfo, num, str);
				}
				str2 = str;
			}
			catch (Exception)
			{
				throw;
			}
			return str2;
		}

		// Methods

		// Methods
		/// <summary>
		/// Returns an Integer representing the character code corresponding to the first letter in a string.
		/// </summary>
		/// <param name="string">The required char argument is any valid char expression.</param>
		/// <returns>Integer value</returns>
		public static int Asc(char @string)
		{
			int num;
			int num2 = Convert.ToInt32(@string);
			if (num2 < 0x80)
			{
				return num2;
			}
			try
			{
				byte[] buffer;
				Encoding fileIOEncoding = Utils.GetFileIOEncoding();
				char[] chars = new char[] { @string };
				if (fileIOEncoding.GetMaxByteCount(1) == 1)
				{
					buffer = new byte[1];
					fileIOEncoding.GetBytes(chars, 0, 1, buffer, 0);
					return buffer[0];
				}
				buffer = new byte[2];
				if (fileIOEncoding.GetBytes(chars, 0, 1, buffer, 0) == 1)
				{
					return buffer[0];
				}
				if (BitConverter.IsLittleEndian)
				{
					byte num4 = buffer[0];
					buffer[0] = buffer[1];
					buffer[1] = num4;
				}
				num = BitConverter.ToInt16(buffer, 0);
			}
			catch (Exception)
			{
				throw;
			}
			return num;
		}

		/// <summary>
		/// Returns an Integer representing the character code corresponding to the first letter in a string.
		/// </summary>
		/// <param name="string">The required string argument is any valid string expression. If the string contains no characters, a run-time error occurs.</param>
		/// <returns>Integer value</returns>
		public static int Asc(string @string)
		{
			if ((@string == null) || (@string.Length == 0))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_LengthGTZero1", "String"));
			}
			char ch = @string[0];
			return Asc(ch);
		}

		/// <summary>
		/// Returns an Integer value that represents the character code corresponding to a character.
		/// </summary>
		/// <param name="string">Required. Any valid Char or String expression. If String is a String expression, only the first character of the string is used for input. If String is Nothing or contains no characters, an ArgumentException error occurs.</param>
		/// <returns>Integer number</returns>
		public static int AscW(char @string)
		{
			return @string;
		}

		/// <summary>
		///  Returns an Integer value that represents the character code corresponding to a character.
		/// </summary>
		/// <param name="string">Required. Any valid Char or String expression. If String is a String expression, only the first character of the string is used for input. If String is Nothing or contains no characters, an ArgumentException error occurs.</param>
		/// <returns>Integer value.</returns>
		public static int AscW(string @string)
		{
			if ((@string == null) || (@string.Length == 0))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_LengthGTZero1", "String"));
			}
			return @string[0];
		}

		/// <summary>
		/// Returns the character associated with the specified character code.
		/// </summary>
		/// <param name="charCode">Required. An Integer expression representing the code point, or character code, for the character.</param>
		/// <returns>Char value</returns>
		public static char Chr(int charCode)
		{
			char ch;
			if ((charCode < -32768) || (charCode > 0xffff))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_RangeTwoBytes1", "CharCode"));
			}
			if ((charCode >= 0) && (charCode <= 0x7f))
			{
				return Convert.ToChar(charCode);
			}
			try
			{
				Encoding encoding = Encoding.GetEncoding(Utils.GetLocaleCodePage());
				if ((encoding.GetMaxByteCount(1) == 1) && ((charCode < 0) || (charCode > 0xff)))
				{
					throw ExceptionUtils.VbMakeException(5);
				}
				char[] chars = new char[2];
				byte[] bytes = new byte[2];
				Decoder decoder = encoding.GetDecoder();
				if ((charCode >= 0) && (charCode <= 0xff))
				{
					bytes[0] = (byte)(charCode & 0xff);
					decoder.GetChars(bytes, 0, 1, chars, 0);
				}
				else
				{
					bytes[0] = (byte)((charCode & 0xff00) / 0x100);
					bytes[1] = (byte)(charCode & 0xff);
					decoder.GetChars(bytes, 0, 2, chars, 0);
				}
				ch = chars[0];
			}
			catch (Exception)
			{
				throw;
			}
			return ch;
		}

		/// <summary>
		/// Returns the character associated with the specified character code.
		/// </summary>
		/// <param name="charCode">Required. An Integer expression representing the code point, or character code, for the character.</param>
		/// <returns>Char value</returns>
		public static char ChrW(int charCode)
		{
			if ((charCode < -32768) || (charCode > 0xffff))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_RangeTwoBytes1", "CharCode"));
			}
			return Convert.ToChar((int)(charCode & 0xffff));
		}

		/// <summary>
		/// Returns a String containing the character associated with the specified byte code
		/// </summary>
		/// <param name="code">Required.Byte expression</param>
		/// <returns>String value.</returns>
		public static string ChrB(byte code)
		{
			return Encoding.Unicode.GetString(new byte[] { code });
		}

		/// <summary>
		/// Returns an byte value
		/// </summary>
		/// <param name="text">Required. String value.</param>
		/// <returns>Byte value.</returns>
		public static byte AscB(string text)
		{
			return (byte)(Asc(text) & 0xff);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <returns>String array</returns>
		public static string[] Filter(object[] source, string match)
		{
			return Filter(source, match, true, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <param name="include">Optional. Boolean value indicating whether to return substrings that include or exclude Match.</param>
		/// <returns>String array</returns>
		public static string[] Filter(object[] source, string match, bool include)
		{
			return Filter(source, match, include, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <param name="include">Optional. Boolean value indicating whether to return substrings that include or exclude Match.</param>
		/// <param name="compare">Optional. Numeric value indicating the kind of string comparison to use. See "Settings" for values.</param>
		/// <returns>String array</returns>
		public static string[] Filter(object[] source, string match, bool include, StringType.CompareMethod compare)
		{
			int num2 = Information.UBound(source, 1);
			string[] _source = new string[num2 + 1];
			try
			{
				int num3 = num2;
				for (int i = 0; i <= num3; i++)
				{
                    _source[i] = StringType.FromObject(source[i]);
				}
			}
			catch (Exception)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "Source", "String"));
			}
            return Filter(_source, match, include, compare);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <returns>String array</returns>
		public static string[] Filter(string[] source, string match)
		{
			return Filter(source, match, true, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <param name="include">Optional. Boolean value indicating whether to return substrings that include or exclude Match.</param>
		/// <returns>String array</returns>
		public static string[] Filter(string[] source, string match, bool include)
		{
			return Filter(source, match, include, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based array containing a subset of a String array based on specified filter criteria.
		/// </summary>
		/// <param name="source">Required. One-dimensional array of strings to be searched.</param>
		/// <param name="match">Required. String to search for.</param>
		/// <param name="include">Optional. Boolean value indicating whether to return substrings that include or exclude Match.</param>
		/// <param name="compare">Optional. Numeric value indicating the kind of string comparison to use. See "Settings" for values.</param>
		/// <returns>String array</returns>
		public static string[] Filter(string[] source, string match, bool include, StringType.CompareMethod compare)
		{
			
			if (source == null)
			{
                throw new ArgumentNullException("source");
			}
			
			string[] strArray;
			try
			{
				CompareOptions ignoreCase = default(CompareOptions);
				int num3 = 0;
				if (source.Rank != 1)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_RankEQOne1"));
				}
				if ((match == null) || (match.Length == 0))
				{
					return null;
				}
				int length = source.Length;
				CompareInfo compareInfo = Utils.CultureInfo.CompareInfo;
				if (compare == StringType.CompareMethod.Text)
				{
					ignoreCase = CompareOptions.IgnoreCase;
				}
				string[] strArray2 = new string[(length - 1) + 1];
				int num5 = length - 1;
				for (int i = 0; i <= num5; i++)
				{
					string _source = source[i];
                    if ((_source != null) && ((compareInfo.IndexOf(_source, match, ignoreCase) >= 0) == include))
					{
                        strArray2[num3] = _source;
						num3++;
					}
				}
				if (num3 == 0)
				{
					return new string[0];
				}
				if (num3 == strArray2.Length)
				{
					return strArray2;
				}
				strArray2 = (string[])Utils.CopyArray((Array)strArray2, new string[(num3 - 1) + 1]);
				strArray = strArray2;
			}
			catch (Exception)
			{
				throw;
			}
			return strArray;
		}

		/// <summary>
		/// Formats the number.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static string FormatNumber(string expression)
		{
			return FormatNumber(expression, 0, false);
		}

		/// <summary>
		/// Formats the number.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="minimumSize">The minimum size.</param>
		/// <returns></returns>
		public static string FormatNumber(string expression, int minimumSize)
		{
			return FormatNumber(expression, minimumSize, false);
		}

		/// <summary>
		/// Formats the number.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="minimumSize">The minimum size.</param>
		/// <param name="groupDigits">if set to <c>true</c> [group digits].</param>
		/// <returns></returns>
		public static string FormatNumber(string expression, int minimumSize, bool groupDigits)
		{
			
			Decimal d = 0;
			if (!Decimal.TryParse(expression, out d))
			{
				return groupDigits ? "0" : String.Empty;
			}
			long value = Convert.ToInt64(Math.Round(d));
			if (groupDigits)
			{
				return String.Format(CultureInfo.InvariantCulture, "{0:n0}", value);
			}
			if (minimumSize != 0)
			{
				return String.Format(CultureInfo.InvariantCulture, String.Concat("{0:d", minimumSize, "}"), value);
			}
			if (value == 0)
			{
				return string.Empty;
			}
			return String.Format(CultureInfo.InvariantCulture, "{0:d}", value);
		}

		public static string FormatNumber(int expression)
		{
			return FormatNumber(expression, 0, false);
		}

		public static string FormatNumber(int expression, int minimumSize)
		{
			return FormatNumber(expression, minimumSize, false);
		}

		public static string FormatNumber(int expression, int minimumSize, bool groupDigits)
		{
			
			return FormatNumber(expression.ToString(CultureInfo.InvariantCulture), minimumSize, groupDigits);
		}

		/// <summary>
		/// Returns a string formatted according to instructions contained in a format String expression.
		/// </summary>
		/// <param name="expression">Required. Any valid expression.</param>
		/// <returns>String formatted</returns>
		public static string Format(object expression)
		{
			return Format(expression, "");
		}

		/// <summary>
		/// Returns a string formatted according to instructions contained in a format String expression.
		/// </summary>
		/// <param name="expression">Required. Any valid expression.</param>
		/// <param name="style">Optional. A valid named or user-defined format String expression.</param>
		/// <returns>String formatted</returns>
		public static string Format(object expression, string style)
		{
			string str;
			try
			{
				IFormattable formattable;
				double num;
				float num2;
				IFormatProvider formatProvider = null;
				if (expression == null)
				{
					return "";
				}
				if (string.IsNullOrEmpty(style))
				{
					return StringType.FromObject(expression);
				}
				TypeCode typeCode = ((IConvertible)expression).GetTypeCode();
				if (style.Length > 0)
				{
					try
					{
						string str2 = default(string);
						if (FormatNamed(expression, style, ref str2))
						{
							return str2;
						}
					}
					catch (InvalidCastException)
					{
						return StringType.FromObject(expression);
					}
				}
				
				formattable = expression as IFormattable;
				if (formattable == null)
				{
					typeCode = Convert.GetTypeCode(expression);
					if ((typeCode != TypeCode.String) && (typeCode != TypeCode.Boolean))
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Expression"));
					}
				}
				IConvertible convertibleExpression = expression as IConvertible;
				
				switch (typeCode) {
					case TypeCode.Empty:
						return "";

					case TypeCode.Object:
						return formattable.ToString(style, formatProvider);

					case TypeCode.DBNull:
						return "";

					case TypeCode.Boolean:
						return string.Format(formatProvider, style, new object[] { StringType.FromBoolean(convertibleExpression.ToBoolean(null)) });

					case TypeCode.Char:
						return formattable.ToString(style, formatProvider);

					case TypeCode.Byte:
						return formattable.ToString(style, formatProvider);

					case TypeCode.Int16:
						return formattable.ToString(style, formatProvider);

					case TypeCode.Int32:
						return formattable.ToString(style, formatProvider);

					case TypeCode.Int64:
						return formattable.ToString(style, formatProvider);

					case TypeCode.Single:
						num2 = (convertibleExpression).ToSingle(null);
						if ((style != null) && (style.Length != 0))
						{
							goto Label_020F;
						}
						return StringType.FromSingle(num2, CultureInfo.InvariantCulture);

					case TypeCode.Double:
						num = (convertibleExpression).ToDouble(null);
						if ((style != null) && (style.Length != 0))
						{
							break;
						}
						return StringType.FromDouble(num, CultureInfo.InvariantCulture);

					case TypeCode.Decimal:
						return formattable.ToString(style, formatProvider);

					case TypeCode.DateTime:
						return formattable.ToString(style, formatProvider);

					case TypeCode.String:
                        if (style.StartsWith("#", StringComparison.OrdinalIgnoreCase))
                        {
                            long value = 0;

                            if (Int64.TryParse(expression.ToString(), out value))
                            {
                                return value.ToString(style, formatProvider);

                            }
                        }
						return string.Format(formatProvider, style, new object[] { expression });

					default:
						goto Label_0244;
				}
				if (num == 0.0)
				{
					num = 0.0;
				}
				return num.ToString(style, formatProvider);
				Label_020F:
				if (num2 == 0f)
				{
					num2 = 0f;
				}
				return num2.ToString(style, formatProvider);
				Label_0244:
				str = formattable.ToString(style, formatProvider);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <returns>The expression formatted.</returns>
		public static string FormatCurrency(object expression)
		{
            return FormatCurrency(expression, -1, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal.</param>
		/// <returns>The expression formatted.</returns>
		public static string FormatCurrency(object expression, int numDigitsAfterDecimal)
		{
            return FormatCurrency(expression, numDigitsAfterDecimal, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal.</param>
		/// <param name="includeLeadingDigit">Optional. TriState enumeration that indicates whether or not a leading zero is displayed for fractional values. </param>
		/// <returns>The expression formatted.</returns>
		public static string FormatCurrency(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit)
		{
            return FormatCurrency(expression, numDigitsAfterDecimal, includeLeadingDigit, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal.</param>
		/// <param name="includeLeadingDigit">Optional. TriState enumeration that indicates whether or not a leading zero is displayed for fractional values. </param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState enumeration that indicates whether or not to place negative values within parentheses.</param>
		/// <returns>The expression formatted.</returns>
        public static string FormatCurrency(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers)
		{
            return FormatCurrency(expression, numDigitsAfterDecimal, includeLeadingDigit, useParensForNegativeNumbers, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a currency value using the currency symbol defined in the system control panel.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal.</param>
		/// <param name="includeLeadingDigit">Optional. TriState enumeration that indicates whether or not a leading zero is displayed for fractional values. </param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState enumeration that indicates whether or not to place negative values within parentheses.</param>
		/// <param name="groupDigits">Optional. TriState enumeration that indicates whether or not numbers are grouped using the group delimiter specified in the computer's regional settings.</param>
		/// <returns>The expression formatted.</returns>
        public static string FormatCurrency(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers, Tristate groupDigits)
		{
			string str;
			try
			{
				IFormatProvider provider = default(IFormatProvider);
				ValidateTriState(includeLeadingDigit);
				ValidateTriState(useParensForNegativeNumbers);
				ValidateTriState(groupDigits);
				if (numDigitsAfterDecimal > 0x63)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_Range0to99_1", "NumDigitsAfterDecimal"));
				}
				if (expression == null || expression == DBNull.Value)
				{
					return string.Empty;
				}
				Type typ = expression.GetType();
				if (typ == typeof(string))
				{
					expression = DoubleType.FromObject(expression);
				}
				else if (!Utils.IsNumericType(typ))
				{
					throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(typ), "Currency"));
				}
				IFormattable formattable = (IFormattable)expression;
                if (includeLeadingDigit == Tristate.False)
				{
					double num = DoubleType.FromObject(expression);
					if ((num >= 1.0) || (num <= -1.0))
					{
                        includeLeadingDigit = Tristate.True;
					}
				}
				string format = GetCurrencyFormatString(includeLeadingDigit, numDigitsAfterDecimal, useParensForNegativeNumbers, groupDigits, ref provider);
				str = formattable.ToString(format, provider);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns a string expression representing a date/time value.
		/// </summary>
		/// <param name="expression">Required. Date expression to be formatted.</param>
		/// <returns>String expression representing a date/time value</returns>
		public static string FormatDateTime(DateTime expression)
		{
			return FormatDateTime(expression, DateFormat.GeneralDate);
		}

		/// <summary>
		/// Returns a string expression representing a date/time value.
		/// </summary>
		/// <param name="expression">Required. Date expression to be formatted.</param>
		/// <param name="namedFormat">Optional. Numeric value that indicates the date/time format used. </param>
		/// <returns>String expression representing a date/time value</returns>
		public static string FormatDateTime(DateTime expression, DateFormat namedFormat)
		{
			string str;
			try
			{
				string str2;
				switch (namedFormat) {
					case DateFormat.GeneralDate:
						if (expression.TimeOfDay.Ticks != expression.Ticks)
						{
							break;
						}
						str2 = "T";
						goto Label_008D;

					case DateFormat.LongDate:
						str2 = "D";
						goto Label_008D;

					case DateFormat.ShortDate:
						str2 = "d";
						goto Label_008D;

					case DateFormat.LongTime:
						str2 = "T";
						goto Label_008D;

					case DateFormat.ShortTime:
						str2 = "HH:mm";
						goto Label_008D;

					default:
						throw ExceptionUtils.VbMakeException(5);
				}
				if (expression.TimeOfDay.Ticks == 0L)
				{
					str2 = "d";
				}
				else
				{
					str2 = "G";
				}
				Label_008D:
				str = expression.ToString(str2, null);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns a boolean if the string expression is formatted 
		/// </summary>
		/// <param name="expression">Required. Date expression to be formatted.</param>
		/// <param name="style">Required. A valid named or user-defined format String expression.</param>
		/// <param name="returnValue">String that will be formatted</param>
		/// <returns>Boolean value.</returns>
		private static bool FormatNamed(object expression, string style, ref string returnValue)
		{
			int length = style.Length;
			returnValue = null;
			switch (length) {
				case 5:
					{
						char ch9 = style[0];
						if (((ch9 != 'f') && (ch9 != 'F')) || (s_InvariantCompareInfo.Compare(style, "fixed", CompareOptions.IgnoreCase) != 0))
						{
							break;
						}
						returnValue = DoubleType.FromObject(expression).ToString("0.00", null);
						return true;
					}
				case 6:
					{
						char ch8 = style[0];
						if ((ch8 != 'y') && (ch8 != 'Y'))
						{
							if (((ch8 != 'o') && (ch8 != 'O')) || (s_InvariantCompareInfo.Compare(style, "on/off", CompareOptions.IgnoreCase) != 0))
							{
								break;
							}
							returnValue = ((int)((Convert.ToInt32(BooleanType.FromObject(expression)) << 0x1f) >> 0x1f)).ToString(CachedOnOffFormatStyle, null);
							return true;
						}
						if (s_InvariantCompareInfo.Compare(style, "yes/no", CompareOptions.IgnoreCase) != 0)
						{
							break;
						}
						returnValue = ((int)((Convert.ToInt32(BooleanType.FromObject(expression)) << 0x1f) >> 0x1f)).ToString(CachedYesNoFormatStyle, null);
						return true;
					}
				case 7:
					{
						char ch7 = style[0];
						if (((ch7 != 'p') && (ch7 != 'P')) || (s_InvariantCompareInfo.Compare(style, "percent", CompareOptions.IgnoreCase) != 0))
						{
							break;
						}
						returnValue = DoubleType.FromObject(expression).ToString("0.00%", null);
						return true;
					}
				case 8:
					{
						char ch6 = style[0];
						if ((ch6 != 's') && (ch6 != 'S'))
						{
							if (((ch6 != 'c') && (ch6 != 'C')) || (s_InvariantCompareInfo.Compare(style, "currency", CompareOptions.IgnoreCase) != 0))
							{
								break;
							}
							returnValue = DoubleType.FromObject(expression).ToString("C", null);
							return true;
						}
						if (s_InvariantCompareInfo.Compare(style, "standard", CompareOptions.IgnoreCase) != 0)
						{
							break;
						}
						returnValue = DoubleType.FromObject(expression).ToString("N2", null);
						return true;
					}
				case 9:
					{
						char ch5 = style[5];
						if ((ch5 != 't') && (ch5 != 'T'))
						{
							if (((ch5 != 'd') && (ch5 != 'D')) || (s_InvariantCompareInfo.Compare(style, "long date", CompareOptions.IgnoreCase) != 0))
							{
								break;
							}
							returnValue = DateType.FromObject(expression).ToString("D", null);
							return true;
						}
						if (s_InvariantCompareInfo.Compare(style, "long time", CompareOptions.IgnoreCase) != 0)
						{
							break;
						}
						returnValue = DateType.FromObject(expression).ToString("T", null);
						return true;
					}
				case 10:
					switch (style[6]) {
						case 'A':
						case 'a':
							if (s_InvariantCompareInfo.Compare(style, "true/false", CompareOptions.IgnoreCase) != 0)
							{
								break;
							}
							returnValue = ((int)((Convert.ToInt32(BooleanType.FromObject(expression)) << 0x1f) >> 0x1f)).ToString(CachedTrueFalseFormatStyle, null);
							return true;

						case 'D':
						case 'd':
							if (s_InvariantCompareInfo.Compare(style, "short date", CompareOptions.IgnoreCase) != 0)
							{
								break;
							}
							returnValue = DateType.FromObject(expression).ToString("d", null);
							return true;

						case 'I':
						case 'i':
							{
								if (s_InvariantCompareInfo.Compare(style, "scientific", CompareOptions.IgnoreCase) != 0)
								{
									break;
								}
								double d = DoubleType.FromObject(expression);
								if (double.IsNaN(d) || double.IsInfinity(d))
								{
									returnValue = d.ToString("G", null);
								}
								else
								{
									returnValue = d.ToString("0.00E+00", null);
								}
								return true;
							}
						case 'T':
						case 't':
							if (s_InvariantCompareInfo.Compare(style, "short time", CompareOptions.IgnoreCase) != 0)
							{
								break;
							}
							returnValue = DateType.FromObject(expression).ToString("t", null);
							return true;
					}
					break;

				case 11:
					{
						char ch3 = style[7];
						if ((ch3 != 't') && (ch3 != 'T'))
						{
							if (((ch3 == 'd') || (ch3 == 'D')) && (s_InvariantCompareInfo.Compare(style, "medium date", CompareOptions.IgnoreCase) == 0))
							{
								returnValue = DateType.FromObject(expression).ToString("D", null);
								return true;
							}
							break;
						}
						if (s_InvariantCompareInfo.Compare(style, "medium time", CompareOptions.IgnoreCase) != 0)
						{
							break;
						}
						returnValue = DateType.FromObject(expression).ToString("T", null);
						return true;
					}
				case 12:
					{
						char ch2 = style[0];
						if (((ch2 != 'g') && (ch2 != 'G')) || (s_InvariantCompareInfo.Compare(style, "general date", CompareOptions.IgnoreCase) != 0))
						{
							break;
						}
						returnValue = DateType.FromObject(expression).ToString("G", null);
						return true;
					}
				case 14:
					{
						char ch = style[0];
						if (((ch == 'g') || (ch == 'G')) && (s_InvariantCompareInfo.Compare(style, "general number", CompareOptions.IgnoreCase) == 0))
						{
							returnValue = DoubleType.FromObject(expression).ToString("G", null);
							return true;
						}
						break;
					}
			}
			return false;
		}

		/// <summary>
		/// Returns an expression formatted as a number.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <returns>String value.</returns>
		public static string FormatNumber(object expression)
		{
            return FormatNumber(expression, -1, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a number.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal. </param>
		/// <returns>String value.</returns>
		public static string FormatNumber(object expression, int numDigitsAfterDecimal)
		{
            return FormatNumber(expression, numDigitsAfterDecimal, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a number.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal. </param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether a leading 0 is displayed for fractional values.</param>
		/// <returns>String value.</returns>
        public static string FormatNumber(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit)
		{
            return FormatNumber(expression, numDigitsAfterDecimal, includeLeadingDigit, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a number.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal. </param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether a leading 0 is displayed for fractional values.</param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState constant that indicates whether to place negative values within parentheses.</param>
		/// <returns>String value.</returns>
        public static string FormatNumber(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers)
		{
            return FormatNumber(expression, numDigitsAfterDecimal, includeLeadingDigit, useParensForNegativeNumbers, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a number.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places are displayed to the right of the decimal. </param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether a leading 0 is displayed for fractional values.</param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState constant that indicates whether to place negative values within parentheses.</param>
		/// <param name="groupDigits">Optional. TriState constant that indicates whether or not numbers are grouped using the group delimiter specified in the locale settings.</param>
		/// <returns>String value.</returns>
        public static string FormatNumber(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers, Tristate groupDigits)
		{
			string str;
			try
			{
				ValidateTriState(includeLeadingDigit);
				ValidateTriState(useParensForNegativeNumbers);
				ValidateTriState(groupDigits);
				if (expression == null)
				{
					return "";
				}
				Type typ = expression.GetType();
				if (typ == typeof(string))
				{
					expression = DoubleType.FromObject(expression);
				}
				else if (typ == typeof(bool))
				{
					if (BooleanType.FromObject(expression))
					{
						expression = -1.0;
					}
					else
					{
						expression = 0.0;
					}
				}
				else if (!Utils.IsNumericType(typ))
				{
					throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(typ), "Currency"));
				}
				str = ((IFormattable)expression).ToString(GetNumberFormatString(numDigitsAfterDecimal, includeLeadingDigit, useParensForNegativeNumbers, groupDigits), null);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <returns></returns>
		public static string FormatPercent(object expression)
		{
            return FormatPercent(expression, -1, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places to the right of the decimal are displayed.</param>
		/// <returns></returns>
		public static string FormatPercent(object expression, int numDigitsAfterDecimal)
		{
            return FormatPercent(expression, numDigitsAfterDecimal, Tristate.UseDefault, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places to the right of the decimal are displayed.</param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether or not a leading zero displays for fractional values.</param>
		/// <returns></returns>
        public static string FormatPercent(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit)
		{
            return FormatPercent(expression, numDigitsAfterDecimal, includeLeadingDigit, Tristate.UseDefault, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places to the right of the decimal are displayed.</param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether or not a leading zero displays for fractional values.</param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState constant that indicates whether or not to place negative values within parentheses.</param>
		/// <returns></returns>
        public static string FormatPercent(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers)
		{
            return FormatPercent(expression, numDigitsAfterDecimal, includeLeadingDigit, useParensForNegativeNumbers, Tristate.UseDefault);
		}

		/// <summary>
		/// Returns an expression formatted as a percentage (that is, multiplied by 100) with a trailing % character.
		/// </summary>
		/// <param name="expression">Required. Expression to be formatted.</param>
		/// <param name="numDigitsAfterDecimal">Optional. Numeric value indicating how many places to the right of the decimal are displayed.</param>
		/// <param name="includeLeadingDigit">Optional. TriState constant that indicates whether or not a leading zero displays for fractional values.</param>
		/// <param name="useParensForNegativeNumbers">Optional. TriState constant that indicates whether or not to place negative values within parentheses.</param>
		/// <param name="groupDigits">Optional. TriState constant that indicates whether or not numbers are grouped using the group delimiter specified in the locale settings. </param>
		/// <returns></returns>
        public static string FormatPercent(object expression, int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers, Tristate groupDigits)
		{
			ValidateTriState(includeLeadingDigit);
			ValidateTriState(useParensForNegativeNumbers);
			ValidateTriState(groupDigits);
			if (expression == null)
			{
				return "";
			}
			Type typ = expression.GetType();
			if (typ == typeof(string))
			{
				expression = DoubleType.FromObject(expression);
			}
			else if (!Utils.IsNumericType(typ))
			{
				throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", Utils.VBFriendlyName(typ), "numeric"));
			}
			IFormattable formattable = (IFormattable)expression;
			string format = GetFormatString(numDigitsAfterDecimal, includeLeadingDigit, useParensForNegativeNumbers, groupDigits, FormatType.Percent);
			return formattable.ToString(format, null);
		}

		/// <summary>
		/// Returns a Char value representing the character from the specified index in the supplied string.
		/// </summary>
		/// <param name="str">Required. Any valid String expression.</param>
		/// <param name="index">Required. Integer expression. The (1-based) index of the character in str to be returned.</param>
		/// <returns>Char value.</returns>
		public static char GetChar(string str, int index)
		{
			if (str == null)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_LengthGTZero1", "String"));
			}
			if (index < 1)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEOne1", "Index"));
			}
			if (index > str.Length)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_IndexLELength2", "Index", "String"));
			}
			return str[index - 1];
		}

        static internal string GetCurrencyFormatString(Tristate includeLeadingDigit, int numDigitsAfterDecimal, Tristate useParensForNegativeNumbers, Tristate groupDigits, ref IFormatProvider formatProvider)
		{
			string str3;
			string str2 = "C";
			NumberFormatInfo format = (NumberFormatInfo)Utils.CultureInfo.GetFormat(typeof(NumberFormatInfo));
			format = (NumberFormatInfo)format.Clone();
            if (groupDigits == Tristate.False)
			{
				format.CurrencyGroupSizes = new int[] { 0 };
			}
			int currencyPositivePattern = format.CurrencyPositivePattern;
			int currencyNegativePattern = format.CurrencyNegativePattern;
            if (useParensForNegativeNumbers != Tristate.UseDefault)
			{
                if (useParensForNegativeNumbers == Tristate.False)
				{
					switch (currencyNegativePattern) {
						case 0:
							currencyNegativePattern = 1;
							goto Label_016A;

						case 1:
						case 2:
						case 3:
						case 5:
						case 6:
						case 7:
						case 8:
						case 9:
						case 10:
						case 11:
						case 12:
						case 13:
							goto Label_016A;

						case 4:
							currencyNegativePattern = 5;
							goto Label_016A;

						case 14:
							currencyNegativePattern = 9;
							goto Label_016A;

						case 15:
							currencyNegativePattern = 10;
							goto Label_016A;
					}
				}
				else
				{
                    useParensForNegativeNumbers = Tristate.True;
					switch (currencyNegativePattern) {
						case 1:
						case 2:
						case 3:
							currencyNegativePattern = 0;
							goto Label_016A;

						case 4:
							goto Label_016A;

						case 5:
						case 6:
						case 7:
							currencyNegativePattern = 4;
							goto Label_016A;

						case 8:
						case 10:
						case 13:
							currencyNegativePattern = 15;
							goto Label_016A;

						case 9:
						case 11:
						case 12:
							currencyNegativePattern = 14;
							goto Label_016A;
					}
				}
			}
			else
			{
				switch (currencyNegativePattern) {
					case 0:
					case 4:
					case 14:
					case 15:
                        useParensForNegativeNumbers = Tristate.True;
						goto Label_016A;
				}
                useParensForNegativeNumbers = Tristate.False;
			}
			Label_016A:
			format.CurrencyNegativePattern = currencyNegativePattern;
			if (numDigitsAfterDecimal == -1)
			{
				numDigitsAfterDecimal = format.CurrencyDecimalDigits;
			}
			format.CurrencyDecimalDigits = numDigitsAfterDecimal;
			formatProvider = new FormatInfoHolder(format);
            if (includeLeadingDigit != Tristate.False)
			{
				return str2;
			}
			format.NumberGroupSizes = format.CurrencyGroupSizes;
			format.NumberNegativePattern = format.CurrencyNegativePattern;
			format.NumberDecimalDigits = format.CurrencyDecimalDigits;
			string str = _currencyPositiveFormatStrings[currencyPositivePattern] + ";" + _currencyNegativeFormatStrings[currencyNegativePattern];
            if (groupDigits == Tristate.False)
			{
                if (includeLeadingDigit == Tristate.False)
				{
					str3 = "#";
				}
				else
				{
					str3 = "0";
				}
			}
            else if (includeLeadingDigit == Tristate.False)
			{
				str3 = "#,###";
			}
			else
			{
				str3 = "#,##0";
			}
			if (numDigitsAfterDecimal > 0)
			{
				str3 = str3 + "." + new string('0', numDigitsAfterDecimal);
			}
			if (string.CompareOrdinal("$", format.CurrencySymbol) != 0)
			{
				str = str.Replace("$", format.CurrencySymbol);
			}
			return str.Replace("n", str3);
		}

        static internal string GetFormatString(int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers, Tristate groupDigits, FormatType formatTypeValue)
		{
			string str2;
			string str3;
			string str4;
			StringBuilder builder = new StringBuilder(30);
			NumberFormatInfo format = (NumberFormatInfo)Utils.CultureInfo.GetFormat(typeof(NumberFormatInfo));
			if (numDigitsAfterDecimal < -1)
			{
				throw ExceptionUtils.VbMakeException(5);
			}
			if (numDigitsAfterDecimal == -1)
			{
				if (formatTypeValue == FormatType.Percent)
				{
					numDigitsAfterDecimal = format.NumberDecimalDigits;
				}
				else if (formatTypeValue == FormatType.Number)
				{
					numDigitsAfterDecimal = format.NumberDecimalDigits;
				}
				else if (formatTypeValue == FormatType.Currency)
				{
					numDigitsAfterDecimal = format.CurrencyDecimalDigits;
				}
			}
            if (groupDigits == Tristate.UseDefault)
			{
                groupDigits = Tristate.True;
				if (formatTypeValue == FormatType.Percent)
				{
					if (IsArrayEmpty(format.PercentGroupSizes))
					{
                        groupDigits = Tristate.False;
					}
				}
				else if (formatTypeValue == FormatType.Number)
				{
					if (IsArrayEmpty(format.NumberGroupSizes))
					{
                        groupDigits = Tristate.False;
					}
				}
				else if ((formatTypeValue == FormatType.Currency) && IsArrayEmpty(format.CurrencyGroupSizes))
				{
                    groupDigits = Tristate.False;
				}
			}
            if (useParensForNegativeNumbers == Tristate.UseDefault)
			{
                useParensForNegativeNumbers = Tristate.False;
				if (formatTypeValue == FormatType.Number)
				{
					if (format.NumberNegativePattern == 0)
					{
                        useParensForNegativeNumbers = Tristate.True;
					}
				}
				else if ((formatTypeValue == FormatType.Currency) && (format.CurrencyNegativePattern == 0))
				{
                    useParensForNegativeNumbers = Tristate.True;
				}
			}
            if (groupDigits == Tristate.True)
			{
				str3 = "#,##";
			}
			else
			{
				str3 = "";
			}
            if (includeLeadingDigit != Tristate.False)
			{
				str4 = "0";
			}
			else
			{
				str4 = "#";
			}
			if (numDigitsAfterDecimal > 0)
			{
				str2 = "." + new string('0', numDigitsAfterDecimal);
			}
			else
			{
				str2 = "";
			}
			if (formatTypeValue == FormatType.Currency)
			{
				builder.Append(format.CurrencySymbol);
			}
			builder.Append(str3);
			builder.Append(str4);
			builder.Append(str2);
			if (formatTypeValue == FormatType.Percent)
			{
				builder.Append(format.PercentSymbol);
			}
            if (useParensForNegativeNumbers == Tristate.True)
			{
				string str5 = builder.ToString();
				builder.Append(";(");
				builder.Append(str5);
				builder.Append(")");
			}
			return builder.ToString();
		}


        static internal string GetNumberFormatString(int numDigitsAfterDecimal, Tristate includeLeadingDigit, Tristate useParensForNegativeNumbers, Tristate groupDigits)
		{
			string str3;
			NumberFormatInfo format = (NumberFormatInfo)Utils.CultureInfo.GetFormat(typeof(NumberFormatInfo));
			if (numDigitsAfterDecimal == -1)
			{
				numDigitsAfterDecimal = format.NumberDecimalDigits;
			}
			else if ((numDigitsAfterDecimal > 0x63) || (numDigitsAfterDecimal < -1))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_Range0to99_1", "NumDigitsAfterDecimal"));
			}
            if (groupDigits == Tristate.UseDefault)
			{
				if ((format.NumberGroupSizes == null) || (format.NumberGroupSizes.Length == 0))
				{
                    groupDigits = Tristate.False;
				}
				else
				{
                    groupDigits = Tristate.True;
				}
			}
			int numberNegativePattern = format.NumberNegativePattern;
            if (useParensForNegativeNumbers == Tristate.UseDefault)
			{
				if (numberNegativePattern == 0)
				{
                    useParensForNegativeNumbers = Tristate.True;
				}
				else
				{
                    useParensForNegativeNumbers = Tristate.False;
				}
			}
            else if (useParensForNegativeNumbers == Tristate.False)
			{
				if (numberNegativePattern == 0)
				{
					numberNegativePattern = 1;
				}
			}
			else
			{
                useParensForNegativeNumbers = Tristate.True;
				switch (numberNegativePattern) {
					case 1:
					case 2:
					case 3:
					case 4:
						numberNegativePattern = 0;
						goto Label_00BA;
				}
			}
			Label_00BA:
            if (useParensForNegativeNumbers == Tristate.UseDefault)
			{
                useParensForNegativeNumbers = Tristate.True;
			}
			string expression = "n;" + _numberNegativeFormatStrings[numberNegativePattern];
            if (includeLeadingDigit != Tristate.False)
			{
				str3 = "0";
			}
			else
			{
				str3 = "#";
			}
            if ((groupDigits != Tristate.False) && (format.NumberGroupSizes.Length != 0))
			{
				if (format.NumberGroupSizes.Length == 1)
				{
					str3 = "#," + new string('#', format.NumberGroupSizes[0]) + str3;
				}
				else
				{
					str3 = new string('#', format.NumberGroupSizes[0] - 1) + str3;
					int upperBound = format.NumberGroupSizes.GetUpperBound(0);
					for (int i = 1; i <= upperBound; i++)
					{
						str3 = "," + new string('#', format.NumberGroupSizes[i]) + "," + str3;
					}
				}
			}
			if (numDigitsAfterDecimal > 0)
			{
				str3 = str3 + "." + new string('0', numDigitsAfterDecimal);
			}
			return Replace(expression, "n", str3, 1, -1, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <returns>Integer value.</returns>
		public static int InStr(string string1, string string2)
		{
			return InStr(string1, string2, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <param name="compare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
		/// <returns>Integer value.</returns>
		public static int InStr(string string1, string string2, StringType.CompareMethod compare)
		{
			if (compare == StringType.CompareMethod.Binary)
			{
				return (InternalInStrBinary(0, string1, string2) + 1);
			}
			return (InternalInStrText(0, string1, string2) + 1);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="start">Optional. Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.</param>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <returns></returns>
		public static int InStr(int start, string string1, string string2)
		{
			return InStr(start, string1, string2, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="start">Optional. Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.</param>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <param name="compare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
		/// <returns></returns>
		public static int InStr(int start, string string1, string string2, StringType.CompareMethod compare)
		{
			if (start < 1)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Start"));
			}
			if (compare == StringType.CompareMethod.Binary)
			{
				return (InternalInStrBinary(start - 1, string1, string2) + 1);
			}
			return (InternalInStrText(start - 1, string1, string2) + 1);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <returns>Integer value.</returns>
		public static int InStrB(string string1, string string2)
		{
			return InStrB(string1, string2, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <param name="compare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
		/// <returns>Integer value.</returns>
		public static int InStrB(string string1, string string2, StringType.CompareMethod compare)
		{
			int index = InStr(string1, string2, compare);
			return index < 2 ? index : index * 2 - 1;
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="start">Optional. Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.</param>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <returns></returns>
		public static int InStrB(int start, string string1, string string2)
		{
			return InStrB(start, string1, string2, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns an integer specifying the start position of the first occurrence of one string within another.
		/// </summary>
		/// <param name="start">Optional. Numeric expression that sets the starting position for each search. If omitted, search begins at the first character position. The start index is 1-based.</param>
		/// <param name="string1">Required. String expression being searched.</param>
		/// <param name="string2">Required. String expression sought.</param>
		/// <param name="compare">Optional. Specifies the type of string comparison. If Compare is omitted, the Option Compare setting determines the type of comparison.</param>
		/// <returns></returns>
		public static int InStrB(int start, string string1, string string2, StringType.CompareMethod compare)
		{
			int index = InStr(start, string1, string2, compare);
			return index < 2 ? index : index * 2 - 1;

		}

		/// <summary>
		/// returns the number of bytes used to represent the characters in a text string or structure
		/// </summary>
		/// <param name="obj">The object.</param>
		/// <returns></returns>
		public static int LenB(object obj)
		{
			if (obj == null)
			{
				return 0;
			}
			
			// Length of string in bytes
			if (obj is string)
			{
				return obj.ToString().Length * sizeof(Char);
			}
			
			// Length of Structure in bytes
			try
			{
				return Marshal.SizeOf(obj);
				//TODO - Missing fixed length string in VB6 Type (structure)
			}
			catch (ArgumentException)
			{
				//Allow user to check for <0 as error
				return -1;
			}
		}

		/// <summary>
		/// Returns the position of the first occurrence of one string within another, starting from the right side of the string.
		/// </summary>
		/// <param name="stringCheck">Required. String expression being searched.</param>
		/// <param name="stringMatch">Required. String expression being searched for.</param>
		/// <returns>Integer value</returns>
		public static int InStrRev(string stringCheck, string stringMatch)
		{
			return InStrRev(stringCheck, stringMatch, -1, StringType.CompareMethod.Binary);
		}
		/// <summary>
		/// Returns the position of the first occurrence of one string within another, starting from the right side of the string.
		/// </summary>
		/// <param name="stringCheck">Required. String expression being searched.</param>
		/// <param name="stringMatch">Required. String expression being searched for.</param>
		/// <param name="start">Optional. Numeric expression setting the one-based starting position for each search, starting from the left side of the string. </param>
		/// <returns>Integer value</returns>
		public static int InStrRev(string stringCheck, string stringMatch, int start)
		{
			return InStrRev(stringCheck, stringMatch, start, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns the position of the first occurrence of one string within another, starting from the right side of the string.
		/// </summary>
		/// <param name="stringCheck">Required. String expression being searched.</param>
		/// <param name="stringMatch">Required. String expression being searched for.</param>
		/// <param name="start">Optional. Numeric expression setting the one-based starting position for each search, starting from the left side of the string. </param>
		/// <param name="compare">Optional. Numeric value indicating the kind of comparison to use when evaluating substrings. If omitted, a binary comparison is performed. See Settings for values.</param>
		/// <returns>Integer value</returns>
		public static int InStrRev(string stringCheck, string stringMatch, int start, StringType.CompareMethod compare)
		{
			int num;
			try
			{
				int length;
				if ((start == 0) || (start < -1))
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_MinusOneOrGTZero1", "Start"));
				}
				if (stringCheck == null)
				{
					length = 0;
				}
				else
				{
					length = stringCheck.Length;
				}
				if (start == -1)
				{
					start = length;
				}
				if ((start > length) || (length == 0))
				{
					return 0;
				}
				if ((stringMatch == null) || (stringMatch.Length == 0))
				{
					return start;
				}
				if (compare == StringType.CompareMethod.Binary)
				{
					return (s_InvariantCompareInfo.LastIndexOf(stringCheck, stringMatch, start - 1, start, CompareOptions.Ordinal) + 1);
				}
				num = Utils.CultureInfo.CompareInfo.LastIndexOf(stringCheck, stringMatch, start - 1, start, CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase) + 1;
			}
			catch (Exception)
			{
				throw;
			}
			return num;
		}

		private static int InternalInStrBinary(int startPos, string sSrc, string sFind)
		{
			int length;
			if (sSrc != null)
			{
				length = sSrc.Length;
			}
			else
			{
				length = 0;
			}
			if ((startPos > length) || (length == 0))
			{
				return -1;
			}
			if ((sFind != null) && (sFind.Length != 0))
			{
				return s_InvariantCompareInfo.IndexOf(sSrc, sFind, startPos, CompareOptions.Ordinal);
			}
			return startPos;
		}

		private static int InternalInStrText(int lStartPos, string sSrc, string sFind)
		{
			int length;
			if (sSrc != null)
			{
				length = sSrc.Length;
			}
			else
			{
				length = 0;
			}
			if ((lStartPos > length) || (length == 0))
			{
				return -1;
			}
			if ((sFind != null) && (sFind.Length != 0))
			{
				return s_InvariantCompareInfo.IndexOf(sSrc, sFind, lStartPos, CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase);
			}
			return lStartPos;
		}

		private static string InternalStrReverse(string expression, int srcIndex, int length)
		{
			StringBuilder builder = new StringBuilder(length);
			builder.Length = length;
			TextElementEnumerator textElementEnumerator = StringInfo.GetTextElementEnumerator(expression, srcIndex);
			if (!textElementEnumerator.MoveNext())
			{
				return "";
			}
			int num2 = 0;
			int num = length - 1;
			while (num2 < srcIndex)
			{
				builder[num] = expression[num2];
				num--;
				num2++;
			}
			int elementIndex = textElementEnumerator.ElementIndex;
			while (num >= 0)
			{
				srcIndex = elementIndex;
				if (textElementEnumerator.MoveNext())
				{
					elementIndex = textElementEnumerator.ElementIndex;
				}
				else
				{
					elementIndex = length;
				}
				for (num2 = elementIndex - 1; num2 >= srcIndex; num2--)
				{
					builder[num] = expression[num2];
					num--;
				}
			}
			return builder.ToString();
		}

        /// <summary>
        /// Determines whether the specified text is numeric.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>
        ///   <c>true</c> if the specified text is numeric; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNumeric(string text)
        {
            double o;
            return Double.TryParse(text, out o);
        }
		/// <summary>
		/// Checking if the array is empty
		/// </summary>
		/// <param name="ary">required. Array that need to be check.</param>
		/// <returns>Boolean value.</returns>
		private static bool IsArrayEmpty(Array ary)
		{
			return ((ary == null) || (ary.Length == 0));
		}

		/// <summary>
		/// Returns a string created by joining a number of substrings contained in an array.
		/// </summary>
		/// <param name="sourceArray">Required. One-dimensional array containing substrings to be joined.</param>
		/// <returns>String value.</returns>
		public static string Join(object[] sourceArray)
		{
			return Join(sourceArray, " ");
		}

		/// <summary>
		/// Returns a string created by joining a number of substrings contained in an array.
		/// </summary>
		/// <param name="sourceArray">Required. One-dimensional array containing substrings to be joined.</param>
		/// <param name="delimiter">Optional. Any string, used to separate the substrings in the returned string. </param>
		/// <returns>String value.</returns>
		public static string Join(object[] sourceArray, string delimiter)
		{
			int num2 = Information.UBound(sourceArray, 1);
			string[] _sourceArray = new string[num2 + 1];
			try
			{
				int num3 = num2;
				for (int i = 0; i <= num3; i++)
				{
                    _sourceArray[i] = StringType.FromObject(sourceArray[i]);
				}
			}
			catch (Exception)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "SourceArray", "String"));
			}
			return Join(sourceArray, delimiter);
		}

		/// <summary>
		/// Returns a string created by joining a number of substrings contained in an array.
		/// </summary>
		/// <param name="sourceArray">Required. One-dimensional array containing substrings to be joined.</param>
		/// <returns>String value.</returns>
		public static string Join(string[] sourceArray)
		{
			return Join(sourceArray, " ");
		}

		/// <summary>
		/// Returns a string created by joining a number of substrings contained in an array.
		/// </summary>
		/// <param name="sourceArray">Required. One-dimensional array containing substrings to be joined.</param>
		/// <param name="delimiter">Optional. Any string, used to separate the substrings in the returned string. </param>
		/// <returns>String value.</returns>
		public static string Join(string[] sourceArray, string delimiter)
		{
			if (sourceArray == null)
			{
                throw new ArgumentNullException("sourceArray");
			}
			
			string str;
			try
			{
				if (IsArrayEmpty(sourceArray))
				{
					return null;
				}
				if (sourceArray.Rank != 1)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_RankEQOne1"));
				}
				str = string.Join(delimiter, sourceArray);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns a string or character converted to lowercase.
		/// </summary>
		/// <param name="value">Required. Any valid Char expression.</param>
		/// <returns>Char value</returns>
		public static char LCase(char value)
		{
			char ch;
			try
			{
				ch = Thread.CurrentThread.CurrentCulture.TextInfo.ToLower(value);
			}
			catch (Exception)
			{
				throw;
			}
			return ch;
		}

		/// <summary>
		/// Returns a string or character converted to lowercase.
		/// </summary>
		/// <param name="value">Required. Any valid String expression.</param>
		/// <returns>String value</returns>
		public static string LCase(string value)
		{
			string str;
			try
			{
				if (value == null)
				{
					return null;
				}
				str = Thread.CurrentThread.CurrentCulture.TextInfo.ToLower(value);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns a string that contains a specified number of characters from the left side of a string.
		/// </summary>
		/// <param name="str">Required. String expression from which the leftmost characters are returned.</param>
		/// <param name="length">Required. Integer expression. Numeric expression indicating how many characters to return.</param>
		/// <returns>String value.</returns>
		public static string Left(string str, int length)
		{
			if (length < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Length"));
			}
			if ((length == 0) || (str == null))
			{
				return "";
			}
			if (length >= str.Length)
			{
				return str;
			}
			return str.Substring(0, length);
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid Bool expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(bool expression)
		{
			return 2;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid Byte expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(byte expression)
		{
			return 1;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid Char expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(char expression)
		{
			return 2;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid DateTime expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(DateTime expression)
		{
			return 8;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid decimal expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(decimal expression)
		{
			return 8;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid double expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(double expression)
		{
			return 8;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid short expression.</param>
		/// <returns>Integer value</returns>
		public static int Len(short expression)
		{
			return 2;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid Integer expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(int expression)
		{
			return 4;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid long expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(long expression)
		{
			return 8;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid object expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(object expression)
		{
			IConvertible convertible = default(IConvertible);
			if (expression == null)
			{
				return 0;
			}
			
			char[] charExpression = expression as char[];
			convertible = expression as IConvertible;
			if (convertible != null)
			{
				switch (convertible.GetTypeCode()) {
					case TypeCode.Object:
					case TypeCode.DBNull:
					case TypeCode.SByte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case (TypeCode.DateTime | TypeCode.Object):
						goto Label_00A5;

					case TypeCode.Boolean:
						return 2;

					case TypeCode.Char:
						return 2;

					case TypeCode.Byte:
						return 1;

					case TypeCode.Int16:
						return 2;

					case TypeCode.Int32:
						return 4;

					case TypeCode.Int64:
						return 8;

					case TypeCode.Single:
						return 4;

					case TypeCode.Double:
						return 8;

					case TypeCode.Decimal:
						return 0x10;

					case TypeCode.DateTime:
						return 8;

					case TypeCode.String:
						return expression.ToString().Length;
				}
			}
			else if (charExpression != null)
			{
				return charExpression.Length;
			}
			Label_00A5:
			if (!(expression is ValueType))
			{
				throw ExceptionUtils.VbMakeException(13);
			}
			return StructUtils.GetRecordLength(expression, 1);
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid float expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(float expression)
		{
			return 4;
		}

		/// <summary>
		/// Returns an integer containing either the number of characters in a string or the nominal number of bytes required to store a variable.
		/// </summary>
		/// <param name="expression">Any valid String expression or variable name. </param>
		/// <returns>Integer value</returns>
		public static int Len(string expression)
		{
			if (expression == null)
			{
				return 0;
			}
			return expression.Length;
		}

		/// <summary>
		/// Returns a left-aligned string containing the specified string, adjusted to the specified length.
		/// </summary>
		/// <param name="source">Required. String expression. String variable to be adjusted.</param>
		/// <param name="length">Required. Integer expression. Length of returned string.</param>
		/// <param name="padChar">Optional. Char value.</param>
		/// <returns>String value.</returns>
		public static string LSet(string source, int length)
		{
			if (length == 0)
			{
				return "";
			}
			if (source == null)
			{
				return new string(' ', length);
			}
			if (length > source.Length)
			{
				return source.PadRight(length);
			}
			return source.Substring(0, length);
		}

		/// <summary>
		/// Returns a string that contains a copy of a specified string without leading spaces (LTrim), without trailing spaces (RTrim), or without leading or trailing spaces (Trim).
		/// </summary>
		/// <param name="str">Required. Any valid String expression. If str equals Nothing, the function returns an empty string.</param>
		/// <returns>String value.</returns>
		public static string LTrim(string str)
		{
			if ((str == null) || (str.Length == 0))
			{
				return "";
			}
			char ch = str[0];
			if ((ch != ' ') && (ch != '　'))
			{
				return str;
			}
			return str.TrimStart(Utils.s_achIntlSpace);
		}

		/// <summary>
		/// Returns a string containing a specified number of characters from a string.
		/// </summary>
		/// <param name="str">Required. String expression from which characters are returned.</param>
		/// <param name="start">Required. Integer expression. Starting position of the characters to return.</param>
		/// <returns>String value.</returns>
		public static string Mid(string str, int start)
		{
			string str2;
			try
			{
				if (str == null)
				{
					return null;
				}
				str2 = Mid(str, start, str.Length);
			}
			catch (Exception)
			{
				throw;
			}
			return str2;
		}

		/// <summary>
		/// Returns a string containing a specified number of characters from a string.
		/// </summary>
		/// <param name="str">Required. String expression from which characters are returned.</param>
		/// <param name="start">Required. Integer expression. Starting position of the characters to return.</param>
		/// <param name="length">Optional. Integer expression. Number of characters to return.</param>
		/// <returns>String value.</returns>
		public static string Mid(string str, int start, int length)
		{
			if (start <= 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GTZero1", "Start"));
			}
			if (length < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Length"));
			}
			if ((length == 0) || (str == null))
			{
				return "";
			}
			int _length = str.Length;
            if (start > _length)
			{
				return "";
			}
            if ((start + length) > _length)
			{
				return str.Substring(start - 1);
			}
			return str.Substring(start - 1, length);
		}

		/// <summary>
		/// Returns a string containing a specified number of bytes from a string.
		/// </summary>
		/// <param name="str">Required. String expression from which characters are returned.</param>
		/// <param name="start">Required. Integer expression. Starting position of the characters to return.</param>
		/// <returns>String value.</returns>
		public static string MidB(string str, int start)
		{
			//Start position must be odd number
			if (start % 2 == 0)
			{
				// If start position is even number return question mark
				return new string('?', start / 2);
			}
			checked
			{
				// Returns a specific number of bytes from a text
				return Mid(str, (start + 1) / 2);

			}
		}

		/// <summary>
		/// Returns a string containing a specified number of bytes from a string.
		/// </summary>
		/// <param name="str">Required. String expression from which characters are returned.</param>
		/// <param name="start">Required. Integer expression. Starting position of the characters to return.</param>
		/// <param name="length">Optional. Integer expression. Number of characters to return.</param>
		/// <returns>String value.</returns>
		public static string MidB(string str, int start, int length)
		{
			//Start position must be odd number
			if (start % 2 == 0 || length % 2 != 0)
			{
				// If start position is even number return question mark
				return new string('?', length / 2);
			}
			
			checked
			{
				// Returns a specific number of bytes from a text
				return Mid(str, (start + 1) / 2, length / 2);

			}
		}

		/// <summary>
		/// Extracts a primary language identifier from a language identifier.
		/// </summary>
		/// <param name="lcid">Language identifier.</param>
		/// <returns>Returns the primary language identifier.</returns>
		private static int PRIMARYLANGID(int lcid)
		{
			return (lcid & 0x3ff);
		}

		/// <summary>
		/// Transforms a string with the proper case
		/// </summary>
		/// <param name="loc">Locale identifier that specifies the locale.</param>
		/// <param name="dwMapFlags">Flags specifying the type of transformation to use during string mapping or the type of sort key to generate.</param>
		/// <param name="sSrc">Pointer to a source string that the function maps or uses for sort key generation.</param>
		/// <returns>Returns the transformed string</returns>
		private static string ProperCaseString(CultureInfo loc, int dwMapFlags, string sSrc)
		{
			int length;
			if (sSrc == null)
			{
				length = 0;
			}
			else
			{
				length = sSrc.Length;
			}
			if (length == 0)
			{
				return "";
			}
			StringBuilder builder = new StringBuilder(vbLCMapString(loc, dwMapFlags | 0x100, sSrc));
			return loc.TextInfo.ToTitleCase(builder.ToString());
		}

		/// <summary>
		/// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
		/// </summary>
		/// <param name="expression">Required. String expression containing substring to replace.</param>
		/// <param name="find">Required. Substring being searched for.</param>
		/// <param name="replacement">Required. Replacement substring.</param>
		/// <returns>String value.</returns>
		public static string Replace(string expression, string find, string replacement)
		{
			return Replace(expression, find, replacement, 1, -1, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
		/// </summary>
		/// <param name="expression">Required. String expression containing substring to replace.</param>
		/// <param name="find">Required. Substring being searched for.</param>
		/// <param name="replacement">Required. Replacement substring.</param>
		/// <param name="start">Optional. Position within Expression where substring search is to begin. If omitted, 1 is assumed.</param>
		/// <returns>String value.</returns>
		public static string Replace(string expression, string find, string replacement, int start)
		{
			return Replace(expression, find, replacement, start, -1, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
		/// </summary>
		/// <param name="expression">Required. String expression containing substring to replace.</param>
		/// <param name="find">Required. Substring being searched for.</param>
		/// <param name="replacement">Required. Replacement substring.</param>
		/// <param name="start">Optional. Position within Expression where substring search is to begin. If omitted, 1 is assumed.</param>
		/// <param name="count">Optional. Number of substring substitutions to perform. </param>
		/// <returns>String value.</returns>
		public static string Replace(string expression, string find, string replacement, int start, int count)
		{
			return Replace(expression, find, replacement, start, count, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a string in which a specified substring has been replaced with another substring a specified number of times.
		/// </summary>
		/// <param name="expression">Required. String expression containing substring to replace.</param>
		/// <param name="find">Required. Substring being searched for.</param>
		/// <param name="replacement">Required. Replacement substring.</param>
		/// <param name="start">Optional. Position within Expression where substring search is to begin. If omitted, 1 is assumed.</param>
		/// <param name="count">Optional. Number of substring substitutions to perform. </param>
		/// <param name="compare">Optional. Numeric value indicating the kind of comparison to use when evaluating substrings.</param>
		/// <returns>String value.</returns>
		public static string Replace(string expression, string find, string replacement, int start, int count, StringType.CompareMethod compare)
		{
			string str;
			try
			{
				if (count < -1)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_GEMinusOne1", "Count"));
				}
				if (start <= 0)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_GTZero1", "Start"));
				}
				if ((expression == null) || (start > expression.Length))
				{
					return null;
				}
				if (start != 1)
				{
					expression = expression.Substring(start - 1);
				}
				if (((find == null) || (find.Length == 0)) || (count == 0))
				{
					return expression;
				}
				if (count == -1)
				{
					count = expression.Length;
				}
				str = Join(Split(expression, find, count + 1, compare), replacement);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Returns a string containing a specified number of characters from the right side of a string.
		/// </summary>
		/// <param name="str">Required. String expression from which the rightmost characters are returned.</param>
		/// <param name="length">Required. Integer. Numeric expression indicating how many characters to return.</param>
		/// <returns>String value.</returns>
		public static string Right(string str, int length)
		{
			if (length < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Length"));
			}
			if ((length == 0) || (str == null))
			{
				return "";
			}
            int _length = str.Length;
            if (length >= _length)
			{
				return str;
			}
            return str.Substring(_length - length, length);
		}

		/// <summary>
		/// Returns a right-aligned string containing the specified string adjusted to the specified length.
		/// </summary>
		/// <param name="source">Required. String expression. Name of string variable.</param>
		/// <param name="length">Required. Integer expression. Length of returned string.</param>
		/// <returns>String value.</returns>
		public static string RSet(string source, int length)
		{
			if (length == 0)
			{
				return "";
			}
			if (source == null)
			{
				return new string(' ', length);
			}
			if (length > source.Length)
			{
				return source.PadLeft(length);
			}
			return source.Substring(0, length);
		}

		/// <summary>
		/// Returns a string that contains a copy of a specified string without leading spaces (LTrim), without trailing spaces (RTrim), or without leading or trailing spaces (Trim).
		/// </summary>
		/// <param name="str">Required. Any valid String expression. If str equals Nothing, the function returns an empty string.</param>
		/// <returns>String value.</returns>
		public static string RTrim(string str)
		{
			string str2;
			try
			{
				if ((str == null) || (str.Length == 0))
				{
					return "";
				}
				switch (str[str.Length - 1]) {
					case ' ':
					case '　':
						return str.TrimEnd(Utils.s_achIntlSpace);
				}
				str2 = str;
			}
			catch (Exception)
			{
				throw;
			}
			return str2;
		}

		/// <summary>
		/// Returns a string consisting of the specified number of spaces.
		/// </summary>
		/// <param name="number">Required. Integer expression. The number of spaces you want in the string.</param>
		/// <returns>String value.</returns>
		public static string Space(int number)
		{
			if (number < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Number"));
			}
			return new string(' ', number);
		}

		/// <summary>
		/// Returns a zero-based, one-dimensional array containing a specified number of substrings.
		/// </summary>
		/// <param name="expression">Required. String expression containing substrings and delimiters.</param>
		/// <returns>String value.</returns>
		public static string[] Split(string expression)
		{
			return Split(expression, " ", -1, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based, one-dimensional array containing a specified number of substrings.
		/// </summary>
		/// <param name="expression">Required. String expression containing substrings and delimiters.</param>
		/// <param name="delimiter">Optional. Any string of characters used to identify substring limits.</param>
		/// <returns>String value.</returns>
		public static string[] Split(string expression, string delimiter)
		{
			return Split(expression, delimiter, -1, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based, one-dimensional array containing a specified number of substrings.
		/// </summary>
		/// <param name="expression">Required. String expression containing substrings and delimiters.</param>
		/// <param name="delimiter">Optional. Any string of characters used to identify substring limits.</param>
		/// <param name="limit">Optional. Maximum number of substrings into which the input string should be split.</param>
		/// <returns>String value.</returns>
		public static string[] Split(string expression, string delimiter, int limit)
		{
			return Split(expression, delimiter, limit, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns a zero-based, one-dimensional array containing a specified number of substrings.
		/// </summary>
		/// <param name="expression">Required. String expression containing substrings and delimiters.</param>
		/// <param name="delimiter">Optional. Any string of characters used to identify substring limits.</param>
		/// <param name="limit">Optional. Maximum number of substrings into which the input string should be split.</param>
		/// <param name="compare">Optional. Numeric value indicating the comparison to use when evaluating substrings. </param>
		/// <returns>String value.</returns>
		public static string[] Split(string expression, string delimiter, int limit, StringType.CompareMethod compare)
		{
			string[] strArray;
			try
			{
				int length;
				if (string.IsNullOrEmpty(expression))
				{
					return new string[] {};
				}
				if (limit == -1)
				{
					limit = expression.Length + 1;
				}
				if (delimiter == null)
				{
					length = 0;
				}
				else
				{
					length = delimiter.Length;
				}
				if (length == 0)
				{
					return new string[] { expression };
				}
				strArray = SplitHelper(expression, delimiter, limit, (int)compare);
			}
			catch (Exception)
			{
				throw;
			}
			return strArray;
		}

		private static string[] SplitHelper(string sSrc, string sFind, int cMaxSubStrings, int compare)
		{
			CompareInfo invariantCompareInfo;
			int num2 = 0;
			CompareOptions ordinal;
			int length = 0;
			int num5 = 0;
			int num6 = 0;
			if (sFind == null)
			{
				length = 0;
			}
			else
			{
				length = sFind.Length;
			}
			if (sSrc == null)
			{
				num6 = 0;
			}
			else
			{
				num6 = sSrc.Length;
			}
			if (length == 0)
			{
				return new string[] { sSrc };
			}
			if (num6 == 0)
			{
				return new string[] { sSrc };
			}
			int num = 20;
			if (num > cMaxSubStrings)
			{
				num = cMaxSubStrings;
			}
			string[] strArray = new string[num + 1];
			if (compare == 0)
			{
				ordinal = CompareOptions.Ordinal;
				invariantCompareInfo = s_InvariantCompareInfo;
			}
			else
			{
				invariantCompareInfo = Utils.CultureInfo.CompareInfo;
				ordinal = CompareOptions.IgnoreWidth | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreCase;
			}
			while (num5 < num6)
			{
				string str;
				int num4 = invariantCompareInfo.IndexOf(sSrc, sFind, num5, num6 - num5, ordinal);
				if ((num4 == -1) || ((num2 + 1) == cMaxSubStrings))
				{
					str = sSrc.Substring(num5);
					if (str == null)
					{
						str = "";
					}
					strArray[num2] = str;
					break;
				}
				str = sSrc.Substring(num5, num4 - num5);
				if (str == null)
				{
					str = "";
				}
				strArray[num2] = str;
				num5 = num4 + length;
				num2++;
				if (num2 > num)
				{
					num += 20;
					if (num > cMaxSubStrings)
					{
						num = cMaxSubStrings + 1;
					}
					strArray = (string[])Utils.CopyArray((Array)strArray, new string[num + 1]);
				}
				strArray[num2] = "";
				if (num2 == cMaxSubStrings)
				{
					str = sSrc.Substring(num5);
					if (str == null)
					{
						str = "";
					}
					strArray[num2] = str;
					break;
				}
			}
			if ((num2 + 1) == strArray.Length)
			{
				return strArray;
			}
			return (string[])Utils.CopyArray((Array)strArray, new string[num2 + 1]);
		}

		/// <summary>
		/// Returns -1, 0, or 1, based on the result of a string comparison.
		/// </summary>
		/// <param name="string1">Required. Any valid String expression.</param>
		/// <param name="string2">Required. Any valid String expression.</param>
		/// <returns>Integer value.</returns>
		public static int StrComp(string string1, string string2)
		{
			return StrComp(string1, string2, StringType.CompareMethod.Binary);
		}

		/// <summary>
		/// Returns -1, 0, or 1, based on the result of a string comparison.
		/// </summary>
		/// <param name="string1">Required. Any valid String expression.</param>
		/// <param name="string2">Required. Any valid String expression.</param>
		/// <param name="compare">Optional. Specifies the type of string comparison.</param>
		/// <returns>Integer value.</returns>
		public static int StrComp(string string1, string string2, StringType.CompareMethod compare)
		{
			int num;
			try
			{
				int num2;
				if (string1 == string2)
				{
					return 0;
				}
				if (compare == StringType.CompareMethod.Binary)
				{
					num2 = StringType.StrCmp(string1, string2, false);
				}
				else
				{
					if (compare != StringType.CompareMethod.Text)
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Compare"));
					}
					num2 = StringType.StrCmp(string1, string2, true);
				}
				if (num2 == 0)
				{
					return 0;
				}
				if (num2 > 0)
				{
					return 1;
				}
				num = -1;
			}
			catch (Exception)
			{
				throw;
			}
			return num;
		}

	
		/// <summary>
		/// Returns a string or object consisting of the specified character repeated the specified number of times.
		/// </summary>
		/// <param name="number">Required. Integer expression. The length to the string to be returned.</param>
		/// <param name="character">Required. Any valid Char</param>
		/// <returns>String value</returns>
		public static string StrDup(int number, char character)
		{
			if (number < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Number"));
			}
			return new string(character, number);
		}

		/// <summary>
		/// Returns a string or object consisting of the specified character repeated the specified number of times.
		/// </summary>
		/// <param name="number">Required. Integer expression. The length to the string to be returned.</param>
		/// <param name="character">Required. Any valid object</param>
		/// <returns>object value</returns>
		public static object StrDup(int number, object character)
		{
			char ch;
			if (number < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Number"));
			}
			if (character == null)
			{
				throw new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Character"));
			}
			
			string str = character as string;
			if (!String.IsNullOrEmpty(str))
			{
				if (str.Length == 0)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_LengthGTZero1", "Character"));
				}
				ch = str[0];
			}
			else
			{
				try
				{
					ch = CharType.FromObject(character);
				}
				catch (Exception)
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Character"));
				}
			}
			return new string(ch, number);
		}

		/// <summary>
		/// Returns a string or object consisting of the specified character repeated the specified number of times.
		/// </summary>
		/// <param name="number">Required. Integer expression. The length to the string to be returned.</param>
		/// <param name="character">Required. Any valid string</param>
		/// <returns>object value</returns>
		public static string StrDup(int number, string character)
		{
			if (number < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_GEZero1", "Number"));
			}
			if ((character == null) || (character.Length == 0))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_LengthGTZero1", "Character"));
			}
			return new string(character[0], number);
		}

		/// <summary>
		/// Returns a string in which the character order of a specified string is reversed.
		/// </summary>
		/// <param name="expression">Required. String expression whose characters are to be reversed.</param>
		/// <returns>String value.</returns>
		public static string StrReverse(string expression)
		{
			if (expression == null)
			{
				return "";
			}
			int length = expression.Length;
			if (length == 0)
			{
				return "";
			}
			int num3 = length - 1;
			for (int i = 0; i <= num3; i++)
			{
				char c = expression[i];
				switch (char.GetUnicodeCategory(c)) {
					case UnicodeCategory.Surrogate:
					case UnicodeCategory.NonSpacingMark:
					case UnicodeCategory.SpacingCombiningMark:
					case UnicodeCategory.EnclosingMark:
						return InternalStrReverse(expression, i, length);
				}
			}
			char[] array = expression.ToCharArray();
			Array.Reverse(array);
			return new string(array);
		}

		/// <summary>
		/// Returns a string that contains a copy of a specified string without leading spaces (LTrim), without trailing spaces (RTrim), or without leading or trailing spaces (Trim).
		/// </summary>
		/// <param name="str">Required. Any valid String expression. If str equals Nothing, the function returns an empty string.</param>
		/// <returns>String value.</returns>
		public static string Trim(string str)
		{
		    return string.IsNullOrEmpty(str) ? "" : str.Trim();
		}

	    /// <summary>
		/// Returns a string or character containing the specified string converted to uppercase.
		/// </summary>
		/// <param name="value">Required. Any valid Char expression.</param>
		/// <returns>Char value.</returns>
		public static char UCase(char value)
		{
			char ch;
			try
			{
				ch = Thread.CurrentThread.CurrentCulture.TextInfo.ToUpper(value);
			}
			catch (Exception)
			{
				throw;
			}
			return ch;
		}

		/// <summary>
		/// Returns a string or character containing the specified string converted to uppercase.
		/// </summary>
		/// <param name="value">Required. Any valid String expression.</param>
		/// <returns>String value.</returns>
		public static string UCase(string value)
		{
			string str;
			try
			{
				if (value == null)
				{
					return "";
				}
				str = Thread.CurrentThread.CurrentCulture.TextInfo.ToUpper(value);
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}


        private static void ValidateTriState(Tristate param)
		{
            if (((param != Tristate.True) && (param != Tristate.False)) && (param != Tristate.UseDefault))
			{
				throw ExceptionUtils.VbMakeException(5);
			}
		}

		/// <summary>
		/// Checks if a local identifier is valid
		/// </summary>
		/// <param name="localeID">Integer local identifier</param>
		/// <returns>True if the local identifier is valid, otherwise false</returns>
		static internal bool ValidLCID(int localeID)
		{
			return CultureInfo.GetCultures(CultureTypes.AllCultures).Any(culture => culture.LCID == localeID);
		}

		static internal string vbLCMapString(CultureInfo loc, int dwMapFlags, string sSrc)
		{
			int num3;
			int length;
			if (sSrc == null)
			{
				length = 0;
			}
			else
			{
				length = sSrc.Length;
			}
			if (length == 0)
			{
				return "";
			}
			int lCID = loc.LCID;
			Encoding encoding = Encoding.GetEncoding(loc.TextInfo.ANSICodePage);
			if (encoding.GetMaxByteCount(1) > 1)
			{
				string s = sSrc;
				byte[] bytes = encoding.GetBytes(s);
				num3 = UnsafeNativeMethods.LCMapStringA(lCID, dwMapFlags, bytes, bytes.Length, null, 0);
				byte[] buffer = new byte[(num3 - 1) + 1];
				num3 = UnsafeNativeMethods.LCMapStringA(lCID, dwMapFlags, bytes, bytes.Length, buffer, num3);
				return encoding.GetString(buffer);
			}
			string lpDestStr = new string(' ', length);
			num3 = UnsafeNativeMethods.LCMapString(lCID, dwMapFlags, ref sSrc, length, ref lpDestStr, length);
			return lpDestStr;
		}

		// Properties
		private static string CachedOnOffFormatStyle
		{
			get
			{
				CultureInfo cultureInfo = Utils.CultureInfo;
				object syncObject = _s_SyncObject;
				CheckForSyncLockOnValueType(syncObject);
				lock (syncObject)
				{
					if (_s_LastUsedOnOffCulture != cultureInfo)
					{
						_s_LastUsedOnOffCulture = cultureInfo;
						_s_CachedOnOffFormatStyle = Utils.GetResourceString("OnOffFormatStyle");
					}
					return _s_CachedOnOffFormatStyle;
				}
			}
		}

		private static string CachedTrueFalseFormatStyle
		{
			get
			{
				CultureInfo cultureInfo = Utils.CultureInfo;
				object syncObject = _s_SyncObject;
				CheckForSyncLockOnValueType(syncObject);
				lock (syncObject)
				{
					if (_s_LastUsedTrueFalseCulture != cultureInfo)
					{
						_s_LastUsedTrueFalseCulture = cultureInfo;
						_s_CachedTrueFalseFormatStyle = Utils.GetResourceString("TrueFalseFormatStyle");
					}
					return _s_CachedTrueFalseFormatStyle;
				}
			}
		}

		private static string CachedYesNoFormatStyle
		{
			get
			{
				CultureInfo cultureInfo = Utils.CultureInfo;
				object syncObject = _s_SyncObject;
				CheckForSyncLockOnValueType(syncObject);
				lock (syncObject)
				{
					if (_s_LastUsedYesNoCulture != cultureInfo)
					{
						_s_LastUsedYesNoCulture = cultureInfo;
						_s_CachedYesNoFormatStyle = Utils.GetResourceString("YesNoFormatStyle");
					}
					return _s_CachedYesNoFormatStyle;
				}
			}
		}

		/// <summary>
		/// Checks if a code page is valid
		/// </summary>
		/// <param name="codepage">The code page</param>
		/// <returns>True if the code page is valid, otherwise false</returns>
		static internal bool IsValidCodePage(int codepage)
		{
			try
			{
				// Test the codepage by trying to create encoding
				var encoding = Encoding.GetEncoding(codepage);
				if (encoding == null)
				{
					return false;
				}
				return true;
			}
			catch (ArgumentOutOfRangeException)
			{
				return false;
			}
			catch (ArgumentException)
			{
				return false;
			}
		}

		// Nested Types

		/// <summary>
		/// Represents the formats for numbers.
		/// </summary>
		internal enum FormatType
		{
			/// <summary>
			/// Represents a general number format.
			/// </summary>
			Number,
			/// <summary>
			/// Represents a percentage format.
			/// </summary>
			Percent,
			/// <summary>
			/// Represents a currency format.
			/// </summary>
			Currency
		}

		private enum NamedFormats
		{

			/// <summary>
			/// Unknown.
			/// </summary>
			UNKNOWN,

			/// <summary>
			/// Display number with no thousand separator.
			/// </summary>
			GENERAL_NUMBER,

			/// <summary>
			/// Display a time using your system's long time format; includes hours, minutes, seconds.
			/// </summary>
			LONG_TIME,

			/// <summary>
			/// Display time in 12-hour format using hours and minutes and the AM/PM designator.
			/// </summary>
			MEDIUM_TIME,

			/// <summary>
			/// Display a date using your system's short date format.
			/// </summary>
			SHORT_TIME,

			/// <summary>
			/// Display a date and/or time. 
			/// </summary>
			GENERAL_DATE,

			/// <summary>
			/// Display a date according to your system's long date format.
			/// </summary>
			LONG_DATE,

			/// <summary>
			/// Display a date using the medium date format appropriate for the language version of the host application.
			/// </summary>
			MEDIUM_DATE,

			/// <summary>
			/// Display a date using your system's short date format.
			/// </summary>
			SHORT_DATE,

			/// <summary>
			/// Display at least one digit to the left and two digits to the right of the decimal separator.
			/// </summary>
			FIXED,

			/// <summary>
			/// Display number with thousand separator, at least one digit to the left and two digits to the right of the decimal separator.
			/// </summary>
			STANDARD,

			/// <summary>
			/// Display number multiplied by 100 with a percent sign (%) appended to the right; always display two digits to the right of the decimal separator.
			/// </summary>
			PERCENT,

			/// <summary>
			/// Use standard scientific notation.
			/// </summary>
			SCIENTIFIC,

			/// <summary>
			/// Display number with thousand separator, if appropriate; display two digits to the right of the decimal separator. Output is based on system locale settings.
			/// </summary>
			CURRENCY,

			/// <summary>
			/// Display False if number is 0; otherwise, display True.
			/// </summary>
			TRUE_FALSE,

			/// <summary>
			/// Display No if number is 0; otherwise, display Yes.
			/// </summary>
			YES_NO,

			/// <summary>
			/// Display Off if number is 0; otherwise, display On.
			/// </summary>
			ON_OFF
		}

		/// <summary>
		/// Indicates a Boolean value or whether the default should be used when calling number-formatting functions.
		/// </summary>
		public enum Tristate
		{
			/// <summary>
			/// False. The numeric value of this member is 0.
			/// </summary>
			False = 0,
			/// <summary>
			/// True. The numeric value of this member is -1.
			/// </summary>
			True = -1,
			/// <summary>
			/// Default setting. The numeric value of this member is -2.
			/// </summary>
			UseDefault = -2
		}
		public enum DateFormat
		{
			/// <summary>
			/// For real numbers, displays a date and time.
			/// </summary>
			GeneralDate,
			/// <summary>
			/// Displays a date using the long-date format specified in your computer's regional settings.
			/// </summary>
			LongDate,
			/// <summary>
			/// Displays a date using the short-date format specified in your computer's regional settings. 
			/// </summary>
			ShortDate,
			/// <summary>
			/// Displays a time using the long-time format specified in your computer's regional settings. 
			/// </summary>
			LongTime,
			/// <summary>
			/// Displays a time using the short-time format specified in your computer's regional settings.
			/// </summary>
			ShortTime
		}

		public static void CheckForSyncLockOnValueType(object obj)
		{
			if ((obj != null) && obj.GetType().IsValueType)
			{
				throw new ArgumentException(Utils.GetResourceString("SyncLockRequiresReferenceType1", Utils.VBFriendlyName(obj.GetType())));
			}
		}
	}

	/// <summary>
	/// FormatInfoHolder class
	/// </summary>
	internal sealed class FormatInfoHolder : IFormatProvider
	{
		// Fields
		private NumberFormatInfo _nfi;

		// Methods

		/// <summary>
		/// ctor
		/// </summary>
		/// <param name="nfi">Required. NumberFormatInfo value.</param>
		public FormatInfoHolder(NumberFormatInfo nfi)
		{
			this._nfi = nfi;
		}

		/// <summary>
		/// Get Format
		/// </summary>
		/// <param name="service">Required. Type value.</param>
		/// <returns>Object value.</returns>
		public object GetFormat(Type service)
		{
			if (service != typeof(NumberFormatInfo))
			{
				throw new ArgumentException(Utils.GetResourceString("InternalError"));
			}
			return _nfi;
		}
	}
}
