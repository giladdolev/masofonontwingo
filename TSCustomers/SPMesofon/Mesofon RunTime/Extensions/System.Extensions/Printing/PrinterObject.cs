﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Extensions.CompilerServices;
using System.Extensions.Printing;
using System.Extensions.VBExtensions;
using System.IO;
using System.Windows.Forms;

internal class PrinterObject
{
    private float m_dpiX;

    private float m_dpiY;

    private string m_documentName;

    private Graphics m_graphics;

    private int m_pageNumber;

    private PrintAction m_printAction;

    private PrintDocumentBuilder m_printDocumentBuilder;

    private PrinterModel m_printerModel;

    private string m_printerName;

    private string m_printFileName;

    private PrinterObjectState m_state;

    public string DocumentName
    {
        get
        {
            return this.m_documentName;
        }
        set
        {
            if (value == null)
            {
                value = "";
            }
            this.m_documentName = value;
        }
    }

    public float DpiX
    {
        get
        {
            return this.m_dpiX;
        }
    }

    public float DpiY
    {
        get
        {
            return this.m_dpiY;
        }
    }

    public int PageNumber
    {
        get
        {
            return this.m_pageNumber;
        }
    }

    public PrintAction PrintAction
    {
        get
        {
            return this.m_printAction;
        }
        set
        {
            if (value != PrintAction.PrintToFile && value != PrintAction.PrintToPreview && value != PrintAction.PrintToPrinter)
            {
                throw new ArgumentOutOfRangeException("value");
            }
            this.m_printAction = value;
        }
    }

    public PrinterModel PrinterModel
    {
        get
        {
            return this.m_printerModel;
        }
    }

    public string PrintFileName
    {
        get
        {
            return this.m_printFileName;
        }
        set
        {
            this.m_printFileName = value;
        }
    }

    public PrinterObjectState State
    {
        get
        {
            return this.m_state;
        }
    }

    public PrinterObject()
    {
        this.m_printerName = this.GetDefaultPrinterName();
        this.SetUp();
        this.SetUpOnce();
    }

    public PrinterObject(string printerName)
    {
        this.m_printerName = printerName;
        this.SetUp();
        this.SetUpOnce();
    }

    public Graphics CreateMeasurementGraphics()
    {
        return this.m_printerModel.PrinterSettings.CreateMeasurementGraphics(this.m_printerModel.PageSettings);
    }

    public void EndDoc()
    {
        if (this.m_state == PrinterObjectState.NoPrint)
        {
            return;
        }
        if (this.m_state == PrinterObjectState.PaintPage)
        {
            this.m_printDocumentBuilder.EndPage();
        }
        this.m_printDocumentBuilder.EndPrint();
        PlaybackPrintDocument printDocument = this.m_printDocumentBuilder.GetPrintDocument();
        printDocument.DocumentName = this.DocumentName;
        try
        {
            if (this.m_printAction == PrintAction.PrintToFile && !this.IsValidFileName(this.m_printFileName))
            {
                throw new IOException(Utils.GetResourceString("ExceptionStr_Printer_FileNameIsInvalid"));
            }
            try
            {
                switch (this.m_printAction)
                {
                    case PrintAction.PrintToFile:
                        {
                            printDocument.PrinterSettings.PrintToFile = true;
                            printDocument.PrinterSettings.PrintFileName = this.m_printFileName;
                            (new FileStream(this.PrintFileName, FileMode.Create)).Close();
                            printDocument.Print();
                            break;
                        }
                    case PrintAction.PrintToPreview:
                        {
                            (new PrintPreviewDialog()
                            {
                                Document = printDocument
                            }).ShowDialog();
                            break;
                        }
                    case PrintAction.PrintToPrinter:
                        {
                            printDocument.Print();
                            break;
                        }
                }
            }
            catch (Exception exception)
            {
                ProjectData.SetProjectError(exception);
                Information.Err().Raise(482, null, null, null, null);
                ProjectData.ClearProjectError();
            }
        }
        finally
        {
            printDocument.Clear();
            this.SetUp();
        }
    }

    private string GetDefaultPrinterName()
    {
        PrinterSettings printerSetting = new PrinterSettings();
        if (!printerSetting.IsValid)
        {
            Information.Err().Raise(482, null, null, null, null);
        }
        return printerSetting.PrinterName;
    }

    public Graphics GetGraphics()
    {
        if (this.m_state == PrinterObjectState.NoPrint)
        {
            this.m_printDocumentBuilder.StartPrint(this.m_printerModel.PrinterSettings);
            this.m_state = PrinterObjectState.SetupPage;
        }
        if (this.m_state == PrinterObjectState.SetupPage)
        {
            this.m_graphics = this.m_printDocumentBuilder.StartPage(this.m_printerModel.PageSettings, this.m_printerModel.MarginBounds, this.m_printerModel.PageBounds);
            this.m_state = PrinterObjectState.PaintPage;
        }
        return this.m_graphics;
    }

    private bool IsValidFileName(string fileName)
    {
        bool flag;
        if (fileName == null)
        {
            return false;
        }
        fileName = fileName.Trim();
        if (String.IsNullOrEmpty(fileName))
        {
            return false;
        }
        try
        {
            string fullPath = Path.GetFullPath(fileName);
            if (Directory.Exists(fullPath))
            {
                flag = false;
            }
            else if (!fullPath.StartsWith("\\\\.\\", StringComparison.CurrentCulture))
            {
                return true;
            }
            else
            {
                flag = false;
            }
        }
        catch (ArgumentException argumentException)
        {
            ProjectData.SetProjectError(argumentException);
            flag = false;
            ProjectData.ClearProjectError();
        }
        catch (IOException oException)
        {
            ProjectData.SetProjectError(oException);
            flag = false;
            ProjectData.ClearProjectError();
        }
        return flag;
    }

    public void KillDoc()
    {
        if (this.m_state == PrinterObjectState.NoPrint)
        {
            return;
        }
        if (this.m_state == PrinterObjectState.PaintPage)
        {
            this.m_printDocumentBuilder.EndPage();
        }
        this.m_printDocumentBuilder.EndPrint();
        this.m_printDocumentBuilder.GetPrintDocument().Clear();
        this.SetUp();
    }

    public void NewPage()
    {
        if (this.m_state == PrinterObjectState.NoPrint)
        {
            this.m_printDocumentBuilder.StartPrint(this.m_printerModel.PrinterSettings);
            this.m_state = PrinterObjectState.SetupPage;
        }
        if (this.m_state == PrinterObjectState.PaintPage)
        {
            this.m_printDocumentBuilder.EndPage();
        }
        else if (this.m_state == PrinterObjectState.SetupPage)
        {
            this.m_graphics = this.m_printDocumentBuilder.StartPage(this.m_printerModel.PageSettings, this.m_printerModel.MarginBounds, this.m_printerModel.PageBounds);
            this.m_printDocumentBuilder.EndPage();
        }
        this.m_pageNumber = checked(this.m_pageNumber + 1);
        this.m_state = PrinterObjectState.SetupPage;
        this.m_graphics = null;
    }

    private void SetUp()
    {
        this.m_printerModel = new PrinterModel(this.m_printerName);
        this.m_printDocumentBuilder = new PrintDocumentBuilder();
        this.m_pageNumber = 1;
        this.m_state = PrinterObjectState.NoPrint;
    }

    private void SetUpOnce()
    {
        using (Graphics graphic = this.m_printerModel.PrinterSettings.CreateMeasurementGraphics())
        {
            this.m_dpiX = graphic.DpiX;
            this.m_dpiY = graphic.DpiY;
        }
        this.m_printAction = PrintAction.PrintToPrinter;
        this.m_documentName = "document";
    }
}
