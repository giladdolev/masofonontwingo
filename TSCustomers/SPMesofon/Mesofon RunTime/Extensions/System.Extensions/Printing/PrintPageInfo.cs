﻿using System.Drawing;
using System.Drawing.Printing;

namespace System.Extensions.Printing
{
    internal class PrintPageInfo
    {
        private PageSettings m_pageSettings;

        private Image m_image;

        public Image Image
        {
            get
            {
                return this.m_image;
            }
            set
            {
                this.m_image = value;
            }
        }

        public PageSettings PageSettings
        {
            get
            {
                return this.m_pageSettings;
            }
            set
            {
                this.m_pageSettings = value;
            }
        }

        public PrintPageInfo()
        {
        }
    }
}
