﻿namespace System.Extensions.Printing
{
    internal enum PrinterObjectState
    {
        NoPrint,
        SetupPage,
        PaintPage
    }
}
