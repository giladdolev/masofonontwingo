﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Extensions.CompilerServices;
using System.Extensions.VBExtensions;

namespace System.Extensions.Printing
{
    internal class GraphicsFactory : IDisposable
    {
        private int m_foreColor;

        private short m_drawStyle;

        private short m_drawWidth;

        private int m_fillColor;

        private short m_fillStyle;

        private Font m_font;

        private bool m_rightToLeft;

        private bool m_fontTransparent;

        public short DrawStyle
        {
            get
            {
                return this.m_drawStyle;
            }
            set
            {
                if (value < 0 || value > 4)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_drawStyle = value;
            }
        }

        public short DrawWidth
        {
            get
            {
                return this.m_drawWidth;
            }
            set
            {
                if (value <= 0)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_drawWidth = value;
            }
        }

        public int FillColor
        {
            get
            {
                return this.m_fillColor;
            }
            set
            {
                this.m_fillColor = value;
            }
        }

        public short FillStyle
        {
            get
            {
                return this.m_fillStyle;
            }
            set
            {
                if (value < 0 || value > 7)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_fillStyle = value;
            }
        }

        public Font Font
        {
            get
            {
                return this.m_font;
            }
            set
            {
                if (value == null)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                if (!this.m_font.Equals(value))
                {
                    this.m_font.Dispose();
                }
                this.m_font = value;
            }
        }

        public bool FontBold
        {
            get
            {
                return this.m_font.Bold;
            }
            set
            {
                this.SetFontStyle(FontStyle.Bold, value);
            }
        }

        public short FontCount
        {
            get
            {
                return checked((short)(checked((int)FontFamily.Families.Length)));
            }
        }

        public bool FontItalic
        {
            get
            {
                return this.m_font.Italic;
            }
            set
            {
                this.SetFontStyle(FontStyle.Italic, value);
            }
        }

        public string FontName
        {
            get
            {
                return this.m_font.Name;
            }
            set
            {
                try
                {
                    using (FontFamily fontFamily = new FontFamily(value))
                    {
                        FontStyle fontStyle = FontStyle.Regular;
                        fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Bold, this.FontBold);
                        if (!fontFamily.IsStyleAvailable(fontStyle))
                        {
                            fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Bold, !this.FontBold);
                        }
                        fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Italic, this.FontItalic);
                        if (!fontFamily.IsStyleAvailable(fontStyle))
                        {
                            fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Italic, !this.FontItalic);
                        }
                        fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Underline, this.FontUnderline);
                        if (!fontFamily.IsStyleAvailable(fontStyle))
                        {
                            fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Underline, !this.FontUnderline);
                        }
                        fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Strikeout, this.FontStrikethru);
                        if (!fontFamily.IsStyleAvailable(fontStyle))
                        {
                            fontStyle = this.MakeFontStyle(fontStyle, FontStyle.Strikeout, !this.FontStrikethru);
                        }
                        this.m_font.Dispose();
                        this.m_font = new Font(fontFamily, this.FontSize, fontStyle);
                    }
                }
                catch (ArgumentException argumentException)
                {
                    ProjectData.SetProjectError(argumentException);
                    this.m_font.Dispose();
                    this.m_font = (Font)SystemFonts.DefaultFont.Clone();
                    ProjectData.ClearProjectError();
                }
            }
        }

        public string this[int index]
        {
            get
            {
                return FontFamily.Families[index].Name;
            }
        }

        public float FontSize
        {
            get
            {
                return this.m_font.SizeInPoints;
            }
            set
            {
                if ((double)value <= 0.0001)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_font.Dispose();
                this.m_font = new Font(this.m_font.Name, value, this.m_font.Style);
            }
        }

        public bool FontStrikethru
        {
            get
            {
                return this.m_font.Strikeout;
            }
            set
            {
                this.SetFontStyle(FontStyle.Strikeout, value);
            }
        }

        public bool FontTransparent
        {
            get
            {
                return this.m_fontTransparent;
            }
            set
            {
                this.m_fontTransparent = value;
            }
        }

        public bool FontUnderline
        {
            get
            {
                return this.m_font.Underline;
            }
            set
            {
                this.SetFontStyle(FontStyle.Underline, value);
            }
        }

        public int ForeColor
        {
            get
            {
                return this.m_foreColor;
            }
            set
            {
                this.m_foreColor = value;
            }
        }

        public bool RightToLeft
        {
            get
            {
                return this.m_rightToLeft;
            }
            set
            {
                this.m_rightToLeft = value;
            }
        }

        public GraphicsFactory()
        {
            this.m_foreColor = 0;
            this.m_drawStyle = 0;
            this.m_drawWidth = 1;
            this.m_fillColor = 0;
            this.m_fillStyle = 1;
            this.m_font = (Font)SystemFonts.DefaultFont.Clone();
            this.m_rightToLeft = false;
            this.m_fontTransparent = true;
        }

        public Brush CreateDrawBrush(int color)
        {
            return new SolidBrush(ColorTranslator.FromOle(color));
        }

        public Pen CreateDrawPen(int color)
        {
            Pen pen = new Pen(ColorTranslator.FromOle(color), (float)this.DrawWidth);
            if (this.DrawWidth == 1)
            {
                switch (this.DrawStyle)
                {
                    case 0:
                        {
                            pen.DashStyle = DashStyle.Solid;
                            break;
                        }
                    case 1:
                        {
                            pen.DashStyle = DashStyle.Dash;
                            break;
                        }
                    case 2:
                        {
                            pen.DashStyle = DashStyle.Dot;
                            break;
                        }
                    case 3:
                        {
                            pen.DashStyle = DashStyle.DashDot;
                            break;
                        }
                    case 4:
                        {
                            pen.DashStyle = DashStyle.DashDotDot;
                            break;
                        }
                }
            }
            else
            {
                pen.DashStyle = DashStyle.Solid;
            }
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;
            return pen;
        }

        public Brush CreateFillBrush()
        {
            Brush solidBrush;
            Color color;
            color = (this.FillStyle != 1 ? ColorTranslator.FromOle(this.FillColor) : Color.Transparent);
            if (this.FillStyle == 0 || this.FillStyle == 1)
            {
                solidBrush = new SolidBrush(color);
            }
            else
            {
                HatchStyle hatchStyle = new HatchStyle();
                switch (this.FillStyle)
                {
                    case 2:
                        {
                            hatchStyle = HatchStyle.Horizontal;
                            break;
                        }
                    case 3:
                        {
                            hatchStyle = HatchStyle.Vertical;
                            break;
                        }
                    case 4:
                        {
                            hatchStyle = HatchStyle.ForwardDiagonal;
                            break;
                        }
                    case 5:
                        {
                            hatchStyle = HatchStyle.BackwardDiagonal;
                            break;
                        }
                    case 6:
                        {
                            hatchStyle = HatchStyle.Cross;
                            break;
                        }
                    case 7:
                        {
                            hatchStyle = HatchStyle.DiagonalCross;
                            break;
                        }
                }
                solidBrush = new HatchBrush(hatchStyle, color, Color.Transparent);
            }
            return solidBrush;
        }

        public StringFormat CreateStringFormat()
        {
            StringFormat formatFlags = (StringFormat)StringFormat.GenericTypographic.Clone();
            if (!this.m_rightToLeft)
            {
                formatFlags.FormatFlags = formatFlags.FormatFlags & (StringFormatFlags.DirectionVertical | StringFormatFlags.FitBlackBox | StringFormatFlags.DisplayFormatControl | StringFormatFlags.NoFontFallback | StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.NoWrap | StringFormatFlags.LineLimit | StringFormatFlags.NoClip);
            }
            else
            {
                formatFlags.FormatFlags = formatFlags.FormatFlags | StringFormatFlags.DirectionRightToLeft;
            }
            return formatFlags;
        }

        private FontStyle MakeFontStyle(FontStyle fontStyle, FontStyle fontStyleBit, bool value)
        {
            if (!value)
            {
                fontStyle = fontStyle & ~fontStyleBit;
            }
            else
            {
                fontStyle = fontStyle | fontStyleBit;
            }
            return fontStyle;
        }

        private void SetFontStyle(FontStyle fontStyleBit, bool value)
        {
            FontStyle fontStyle = this.MakeFontStyle(this.m_font.Style, fontStyleBit, value);
            if (this.m_font.FontFamily.IsStyleAvailable(fontStyle))
            {
                this.m_font.Dispose();
                this.m_font = new Font(this.FontName, this.FontSize, fontStyle);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            m_font.Dispose();
        }
    }
}
