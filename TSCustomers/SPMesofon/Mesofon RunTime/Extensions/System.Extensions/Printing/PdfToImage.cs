﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace System.Extensions.Printing
{
    public class PdfToImage
    {
        public enum Layout
        {
            Portrait,
            Landscape
        }

        public static Layout GetLayout(string sourcefile)
        {
            PdfDocument doc = new PdfDocument();
            try
            {
                doc.LoadFromFile(sourcefile);
                int len = doc.Pages.Count;
                if (len > 0)
                {
                    Image image = doc.SaveAsImage(0);
                    if (image.Width > image.Height)
                        return Layout.Landscape;
                }
            }
            catch
            {

            }
            finally
            {
                doc.Close();
            }
            return Layout.Portrait; // By default returns portrait
        }

        public static List<Image> Convert2(string sourcefile)
        {
            List<Image> lstImages = new List<Image>();
            try
            {
                PdfDocument doc = new PdfDocument();
                doc.LoadFromFile(sourcefile);
                int len = doc.Pages.Count;
                for (int i = 0; i < len; i++)
                {
                    Image image = doc.SaveAsImage(i);
                    lstImages.Add(image);
                }
                doc.Close();

            }
            catch
            {

            }
            return lstImages;
        }

        public static Image MergeFiles(List<Image> Images)
        {
            Image img3 = null;
            try
            {
                List<int> imageWidths = new List<int>();
                int nIndex = 0;
                int width = 0;
                int height = 0;
                foreach (Image img in Images)
                {
                    imageWidths.Add(img.Width);
                    height += img.Height;
                }
                imageWidths.Sort();
                width = imageWidths[imageWidths.Count - 1];
                img3 = new Bitmap(width, height);
                Graphics g = Graphics.FromImage(img3);
                g.Clear(SystemColors.AppWorkspace);
                foreach (Image img in Images)
                {
                    if (nIndex == 0)
                    {
                        g.DrawImage(img, new Point(0, 0));
                        nIndex++;
                        height = img.Height;
                    }
                    else
                    {
                        g.DrawImage(img, new Point(0, height));
                        height += img.Height;
                    }
                    img.Dispose();
                }
                g.Dispose();
            }
            catch { }
            return img3;
        }

       
    }
}
