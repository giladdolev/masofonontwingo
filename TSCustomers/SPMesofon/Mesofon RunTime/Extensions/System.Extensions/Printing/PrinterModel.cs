﻿using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.Extensions.VBExtensions;

namespace System.Extensions.Printing
{
    internal class PrinterModel
    {
        private const float TwipsPerInch = 1440f;

        private PrinterSettings m_printerSettings;

        private PageSettings m_pageSettings;

        private Rectangle m_marginBounds;

        private Rectangle m_pageBounds;

        public short ColorMode
        {
            get
            {
                if (this.m_pageSettings.Color)
                {
                    return 2;
                }
                return 1;
            }
            set
            {
                if (value == 2)
                {
                    if (!this.m_printerSettings.SupportsColor)
                    {
                        Information.Err().Raise(380, null, null, null, null);
                    }
                    this.m_pageSettings.Color = true;
                }
                else if (value != 1)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                else
                {
                    this.m_pageSettings.Color = false;
                }
            }
        }

        public short Copies
        {
            get
            {
                return this.m_printerSettings.Copies;
            }
            set
            {
                if (value <= 0)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                if (value > this.m_printerSettings.MaximumCopies)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_printerSettings.Copies = value;
            }
        }

        public string DeviceName
        {
            get
            {
                return this.m_printerSettings.PrinterName;
            }
        }

        public short Duplex
        {
            get
            {
                switch (this.m_printerSettings.Duplex)
                {
                    case System.Drawing.Printing.Duplex.Simplex:
                        {
                            return 1;
                        }
                    case System.Drawing.Printing.Duplex.Vertical:
                        {
                            return 3;
                        }
                    case System.Drawing.Printing.Duplex.Horizontal:
                        {
                            return 2;
                        }
                }
                return 1;
            }
            set
            {
                if (value == 1)
                {
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Simplex;
                }
                else if (value == 3)
                {
                    if (!this.m_printerSettings.CanDuplex)
                    {
                        Information.Err().Raise(380, null, null, null, null);
                    }
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
                }
                else if (value != 2)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                else
                {
                    if (!this.m_printerSettings.CanDuplex)
                    {
                        Information.Err().Raise(380, null, null, null, null);
                    }
                    this.m_printerSettings.Duplex = System.Drawing.Printing.Duplex.Horizontal;
                }
            }
        }

        public int Height
        {
            get
            {
                return checked((int)Math.Round((double)((float)((float)this.m_pageBounds.Height * 14.4f))));
            }
            set
            {
                int num = checked((int)Math.Round((double)((float)((float)value / 14.4f))));
                if ((float)num <= this.m_pageSettings.HardMarginY)
                {
                    return;
                }
                PaperSize paperSize = new PaperSize()
                {
                    RawKind = 256,
                    Width = this.m_pageBounds.Width,
                    Height = num
                };
                this.m_pageSettings.PaperSize = paperSize;
                this.UpdatePageBounds();
            }
        }

        public bool IsDefaultPrinter
        {
            get
            {
                return this.m_printerSettings.IsDefaultPrinter;
            }
        }

        public Rectangle MarginBounds
        {
            get
            {
                return this.m_marginBounds;
            }
        }

        public short Orientation
        {
            get
            {
                if (this.m_pageSettings.Landscape)
                {
                    return 2;
                }
                return 1;
            }
            set
            {
                if (value == 2)
                {
                    this.m_pageSettings.Landscape = true;
                }
                else if (value != 1)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                else
                {
                    this.m_pageSettings.Landscape = false;
                }
                this.UpdatePageBounds();
            }
        }

        public Rectangle PageBounds
        {
            get
            {
                return this.m_pageBounds;
            }
        }

        public PageSettings PageSettings
        {
            get
            {
                return this.m_pageSettings;
            }
        }

        public short PaperBin
        {
            get
            {
                return checked((short)this.m_pageSettings.PaperSource.RawKind);
            }
            set
            {
                IEnumerator enumerator = null;
                PaperSource paperSource = null;
                try
                {
                    enumerator = this.m_printerSettings.PaperSources.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        PaperSource current = (PaperSource)enumerator.Current;
                        if (current.RawKind != value)
                        {
                            continue;
                        }
                        paperSource = current;
                    }
                }
                finally
                {
                    if (enumerator is IDisposable)
                    {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                if (paperSource == null)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_pageSettings.PaperSource = paperSource;
            }
        }

        public short PaperSize
        {
            get
            {
                return checked((short)this.m_pageSettings.PaperSize.RawKind);
            }
            set
            {
                IEnumerator enumerator = null;
                PaperSize paperSize = null;
                try
                {
                    enumerator = this.m_printerSettings.PaperSizes.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        PaperSize current = (PaperSize)enumerator.Current;
                        if (current.RawKind != value)
                        {
                            continue;
                        }
                        paperSize = current;
                    }
                }
                finally
                {
                    if (enumerator is IDisposable)
                    {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                if (paperSize == null)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_pageSettings.PaperSize = paperSize;
                this.UpdatePageBounds();
            }
        }

        public PrinterSettings PrinterSettings
        {
            get
            {
                return this.m_printerSettings;
            }
        }

        public short PrintQuality
        {
            get
            {
                if (this.m_pageSettings.PrinterResolution.Kind == PrinterResolutionKind.Custom)
                {
                    return checked((short)this.m_pageSettings.PrinterResolution.X);
                }
                return checked((short)this.m_pageSettings.PrinterResolution.Kind);
            }
            set
            {
                IEnumerator enumerator = null;
                PrinterResolution printerResolution = null;
                try
                {
                    enumerator = this.m_printerSettings.PrinterResolutions.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        PrinterResolution current = (PrinterResolution)enumerator.Current;
                        if (value >= 0)
                        {
                            if (!(current.X == value | current.Y == value))
                            {
                                continue;
                            }
                            printerResolution = current;
                        }
                        else
                        {
                            if ((short)current.Kind != value)
                            {
                                continue;
                            }
                            printerResolution = current;
                        }
                    }
                }
                finally
                {
                    if (enumerator is IDisposable)
                    {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                if (printerResolution == null)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.m_pageSettings.PrinterResolution = printerResolution;
            }
        }

        public int Width
        {
            get
            {
                return checked((int)Math.Round((double)((float)((float)this.m_pageBounds.Width * 14.4f))));
            }
            set
            {
                int num = checked((int)Math.Round((double)((float)((float)value / 14.4f))));
                if ((float)num <= this.m_pageSettings.HardMarginX)
                {
                    return;
                }
                PaperSize paperSize = new PaperSize()
                {
                    RawKind = 256,
                    Width = num,
                    Height = this.m_pageBounds.Height
                };
                this.m_pageSettings.PaperSize = paperSize;
                this.UpdatePageBounds();
            }
        }

        public PrinterModel(string printerName)
        {
            this.m_printerSettings = new PrinterSettings()
            {
                PrinterName = printerName
            };
            if (!this.m_printerSettings.IsValid)
            {
                Information.Err().Raise(482, null, null, null, null);
            }
            this.m_pageSettings = new PageSettings(this.m_printerSettings);
            this.UpdatePageBounds();
        }

        private void UpdatePageBounds()
        {
            this.m_pageBounds = this.m_pageSettings.Bounds;
            RectangleF printableArea = this.m_pageSettings.PrintableArea;
            if (this.m_pageSettings.Landscape)
            {
                this.m_marginBounds = new Rectangle(checked((int)Math.Round((double)printableArea.Y)), checked((int)Math.Round((double)printableArea.X)), checked((int)Math.Round((double)printableArea.Height)), checked((int)Math.Round((double)printableArea.Width)));
            }
            else
            {
                this.m_marginBounds = new Rectangle(checked((int)Math.Round((double)printableArea.X)), checked((int)Math.Round((double)printableArea.Y)), checked((int)Math.Round((double)printableArea.Width)), checked((int)Math.Round((double)printableArea.Height)));
            }
        }
    }
}