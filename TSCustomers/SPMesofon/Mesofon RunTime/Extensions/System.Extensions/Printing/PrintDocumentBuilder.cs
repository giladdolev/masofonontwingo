﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

namespace System.Extensions.Printing
{
    internal class PrintDocumentBuilder
    {
        private PrintPageInfo m_currentPageInfo;

        private PrintPageEventArgs m_currentPrintPageEventArgs;

        private List<PrintPageInfo> m_pageList;

        private PreviewPrintController m_printController;

        private PlaybackPrintDocument m_printDocument;

        public PrintDocumentBuilder()
        {
        }

        public void EndPage()
        {
            this.m_pageList.Add(this.m_currentPageInfo);
            this.m_printController.OnEndPage(this.m_printDocument, this.m_currentPrintPageEventArgs);
        }

        public void EndPrint()
        {
            PrintEventArgs printEventArg = new PrintEventArgs();
            this.m_printController.OnEndPrint(this.m_printDocument, printEventArg);
            PreviewPageInfo[] previewPageInfo = this.m_printController.GetPreviewPageInfo();
            int count = checked(this.m_pageList.Count - 1);
            for (int i = 0; i <= count; i++)
            {
                PrintPageInfo item = this.m_pageList[i];
                item.Image = previewPageInfo[i].Image;
                this.m_printDocument.AddPage(item);
            }
        }

        public PlaybackPrintDocument GetPrintDocument()
        {
            return this.m_printDocument;
        }

        public Graphics StartPage(PageSettings pageSettings, Rectangle marginBounds, Rectangle pageBounds)
        {
            this.m_currentPageInfo = new PrintPageInfo()
            {
                PageSettings = (PageSettings)pageSettings.Clone()
            };
            this.m_currentPrintPageEventArgs = new PrintPageEventArgs(null, marginBounds, pageBounds, pageSettings);
            Graphics graphic = this.m_printController.OnStartPage(this.m_printDocument, this.m_currentPrintPageEventArgs);
            graphic.PageUnit = GraphicsUnit.Pixel;
            return graphic;
        }

        public void StartPrint(PrinterSettings printerSettings)
        {
            this.m_pageList = new List<PrintPageInfo>();
            this.m_printController = new PreviewPrintController();
            this.m_printDocument = new PlaybackPrintDocument()
            {
                PrinterSettings = (PrinterSettings)printerSettings.Clone()
            };
            PrintEventArgs printEventArg = new PrintEventArgs();
            this.m_printController.OnStartPrint(this.m_printDocument, printEventArg);
        }
    }
}
