﻿using System.Collections;
using System.Drawing.Printing;

namespace System.Extensions.Printing
{
    public sealed class PrinterCollection : IEnumerable
    {
        /// <summary>Returns the number of printers in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.GlobalModule.Printers" /> collection.</summary>
        /// <returns>An Integer representing the number of printers in the collection.</returns>
        public int Count
        {
            get
            {
                return PrinterSettings.InstalledPrinters.Count;
            }
        }

        /// <summary>Returns a specific member of the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.GlobalModule.Printers" /> collection by index number.</summary>
        /// <returns>A <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object.</returns>
        /// <param name="index">An Integer representing the index of the printer.</param>
        public Printer this[int index]
        {
            get
            {
                return new Printer(PrinterSettings.InstalledPrinters[index]);
            }
        }

        /// <summary>Initializes a <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.PrinterCollection" /> object.</summary>
        public PrinterCollection()
        {
        }

        public IEnumerator GetEnumerator()
        {
            return new PrinterCollection.PrinterEnumerator();
        }

        private class PrinterEnumerator : IEnumerator
        {
            private IEnumerator m_installedPrintersEnumerator;

            public object Current
            {
                get
                {
                    return new Printer(this.m_installedPrintersEnumerator.Current as String);
                }
            }

            public PrinterEnumerator()
            {
                this.m_installedPrintersEnumerator = PrinterSettings.InstalledPrinters.GetEnumerator();
            }

            public bool MoveNext()
            {
                return this.m_installedPrintersEnumerator.MoveNext();
            }

            public void Reset()
            {
                this.m_installedPrintersEnumerator.Reset();
            }
        }
    }
}
