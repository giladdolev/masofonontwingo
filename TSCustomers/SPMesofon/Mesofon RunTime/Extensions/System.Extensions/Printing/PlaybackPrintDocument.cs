﻿using System.Collections.Generic;
using System.Drawing.Printing;

namespace System.Extensions.Printing
{
    internal class PlaybackPrintDocument : PrintDocument
    {
        private int m_current;

        private List<PrintPageInfo> m_pageList;

        public PlaybackPrintDocument()
        {
            this.m_pageList = new List<PrintPageInfo>();
        }

        public void AddPage(PrintPageInfo pageInfo)
        {
            this.m_pageList.Add(pageInfo);
        }

        public void Clear()
        {
            List<PrintPageInfo>.Enumerator enumerator = new List<PrintPageInfo>.Enumerator();
            try
            {
                enumerator = this.m_pageList.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    PrintPageInfo current = enumerator.Current;
                    if (current.Image == null)
                    {
                        continue;
                    }
                    current.Image.Dispose();
                }
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
            this.m_pageList.Clear();
        }

        protected override void OnBeginPrint(PrintEventArgs e)
        {
            if (this.m_pageList.Count == 0)
            {
                e.Cancel = true;
            }
            this.m_current = 0;
        }

        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(this.m_pageList[this.m_current].Image, 0, 0);
            this.m_current = checked(this.m_current + 1);
            if (this.m_current < this.m_pageList.Count)
            {
                e.HasMorePages = true;
            }
        }

        protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
        {
            e.PageSettings = this.m_pageList[this.m_current].PageSettings;
        }
    }
}
