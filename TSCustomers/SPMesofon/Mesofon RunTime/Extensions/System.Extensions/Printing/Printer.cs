﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Extensions.CompilerServices;
using System.Extensions.VBExtensions;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Extensions.Printing
{
    /// <summary>Provides a Printer object for use by upgraded Visual Basic 6.0 printing code.</summary>
    public class Printer
    {
        private CoordinateSpace m_coordinateSpace;

        private GraphicsFactory m_graphicsFactory;

        private PrinterObject m_printerObject;

        private const int MaxStringLength = 10000;

        /// <summary>Gets or sets a value specifying whether the output will print in color or in monochrome on a device that supports color.</summary>
        /// <returns>Returns a Short.</returns>
        public short ColorMode
        {
            get
            {
                return this.m_printerObject.PrinterModel.ColorMode;
            }
            set
            {
                this.CheckWithinPage("ColorMode");
                this.m_printerObject.PrinterModel.ColorMode = value;
            }
        }

        /// <summary>Gets or sets a value that determines the number of copies to be printed.</summary>
        /// <returns>A Short representing the number of copies to be printed.</returns>
        public short Copies
        {
            get
            {
                return this.m_printerObject.PrinterModel.Copies;
            }
            set
            {
                this.CheckWithinPage("Copies");
                this.m_printerObject.PrinterModel.Copies = value;
            }
        }

        /// <summary>Gets or sets the horizontal coordinates for the next printing or drawing method.</summary>
        /// <returns>Returns a Single.</returns>
        public float CurrentX
        {
            get
            {
                return this.m_coordinateSpace.CurrentX;
            }
            set
            {
                this.m_coordinateSpace.CurrentX = value;
            }
        }

        /// <summary>Gets or sets the vertical coordinates for the next printing or drawing method.</summary>
        /// <returns>Returns a Single.</returns>
        public float CurrentY
        {
            get
            {
                return this.m_coordinateSpace.CurrentY;
            }
            set
            {
                this.m_coordinateSpace.CurrentY = value;
            }
        }

        /// <summary>Gets the name of the current printer.</summary>
        /// <returns>Returns a String.</returns>
        public string DeviceName
        {
            get
            {
                return this.m_printerObject.PrinterModel.DeviceName;
            }
        }

        /// <summary>Gets or sets the document name to display (for example, in a print status dialog box or printer queue) while printing the document.</summary>
        /// <returns>A String to display while printing the document. The default is "document".</returns>
        public string DocumentName
        {
            get
            {
                return this.m_printerObject.DocumentName;
            }
            set
            {
                this.CheckWithinPage("DocumentName");
                this.m_printerObject.DocumentName = value;
            }
        }

        /// <summary>Gets or sets a value that determines the line style for output from graphics methods.</summary>
        /// <returns>Returns a Short.</returns>
        public short DrawStyle
        {
            get
            {
                return this.m_graphicsFactory.DrawStyle;
            }
            set
            {
                this.m_graphicsFactory.DrawStyle = value;
            }
        }

        /// <summary>Gets or sets the line width for output from graphics methods.</summary>
        /// <returns>Returns a Short.</returns>
        public short DrawWidth
        {
            get
            {
                return this.m_graphicsFactory.DrawWidth;
            }
            set
            {
                this.m_graphicsFactory.DrawWidth = value;
            }
        }

        /// <summary>Gets or sets a value that determines whether a page is printed on both sides (if the printer supports this feature).</summary>
        /// <returns>Returns a Short.</returns>
        public short Duplex
        {
            get
            {
                return this.m_printerObject.PrinterModel.Duplex;
            }
            set
            {
                this.CheckWithinPage("Duplex");
                this.m_printerObject.PrinterModel.Duplex = value;
            }
        }

        /// <summary>Gets or sets the color that is used to fill in shapes created by using the <see cref="Printer.Circle" /> and <see cref="Printer.Line" /> graphics methods.</summary>
        /// <returns>Returns an Integer.</returns>
        public int FillColor
        {
            get
            {
                return this.m_graphicsFactory.FillColor;
            }
            set
            {
                this.m_graphicsFactory.FillColor = value;
            }
        }

        /// <summary>Gets or sets the pattern used to fill shapes created by using the <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle(System.Boolean,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single,System.Single)" /> and <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line(System.Boolean,System.Single,System.Single,System.Boolean,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)" /> graphics methods.</summary>
        /// <returns>Returns a Short. The default is 1.</returns>
        public short FillStyle
        {
            get
            {
                return this.m_graphicsFactory.FillStyle;
            }
            set
            {
                this.m_graphicsFactory.FillStyle = value;
            }
        }

        /// <summary>Gets or sets a FontFamily by name.</summary>
        /// <returns>Returns a <see cref="T:System.Drawing.Font" />.</returns>
        public Font Font
        {
            get
            {
                return this.m_graphicsFactory.Font;
            }
            set
            {
                this.m_graphicsFactory.Font = value;
            }
        }

        /// <summary>Gets or sets the bold font style.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool FontBold
        {
            get
            {
                return this.m_graphicsFactory.FontBold;
            }
            set
            {
                this.m_graphicsFactory.FontBold = value;
            }
        }

        /// <summary>Returns the number of fonts available for the current display device or active printer.</summary>
        /// <returns>Returns a Short.</returns>
        public short FontCount
        {
            get
            {
                return this.m_graphicsFactory.FontCount;
            }
        }

        /// <summary>Gets or sets the italic font style.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool FontItalic
        {
            get
            {
                return this.m_graphicsFactory.FontItalic;
            }
            set
            {
                this.m_graphicsFactory.FontItalic = value;
            }
        }

        /// <summary>Gets or sets the name of the font in which text is displayed for a printing operation.</summary>
        /// <returns>Returns a String.</returns>
        public string FontName
        {
            get
            {
                return this.m_graphicsFactory.FontName;
            }
            set
            {
                this.m_graphicsFactory.FontName = value;
            }
        }

        /// <summary>Gets all font names that are available for the current printer.</summary>
        /// <returns>Returns a String.</returns>
        /// <param name="index">The index of the font within the collection.</param>
        public string Fonts(int index)
        {
            return this.m_graphicsFactory[index];
        }
        /// <summary>Gets or sets the size of the font that is used for text in a run-time printing operation.</summary>
        /// <returns>Returns a Single.</returns>
        public float FontSize
        {
            get
            {
                return this.m_graphicsFactory.FontSize;
            }
            set
            {
                this.m_graphicsFactory.FontSize = value;
            }
        }

        /// <summary>Gets or sets the strikethrough font style.</summary>
        /// <returns>Returns a Boolean.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public bool FontStrikethru
        {
            get
            {
                return this.m_graphicsFactory.FontStrikethru;
            }
            set
            {
                this.m_graphicsFactory.FontStrikethru = value;
            }
        }

        /// <summary>Gets or sets a value that determines whether background graphics on a Printer object are printed behind text characters.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool FontTransparent
        {
            get
            {
                return this.m_graphicsFactory.FontTransparent;
            }
            set
            {
                this.m_graphicsFactory.FontTransparent = value;
            }
        }

        /// <summary>Gets or sets the underlined font style.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool FontUnderline
        {
            get
            {
                return this.m_graphicsFactory.FontUnderline;
            }
            set
            {
                this.m_graphicsFactory.FontUnderline = value;
            }
        }

        /// <summary>Gets or sets the color in which text and graphics are printed.</summary>
        /// <returns>Returns an Integer.</returns>
        public int ForeColor
        {
            get
            {
                return this.m_graphicsFactory.ForeColor;
            }
            set
            {
                this.m_graphicsFactory.ForeColor = value;
            }
        }

        /// <summary>Gets or sets the height of a page.</summary>
        /// <returns>Returns an Integer.</returns>
        public int Height
        {
            get
            {
                return this.m_printerObject.PrinterModel.Height;
            }
            set
            {
                this.CheckWithinPage("Height");
                this.m_printerObject.PrinterModel.Height = value;
                this.m_coordinateSpace.SetMarginBounds(this.m_printerObject.PrinterModel.MarginBounds);
            }
        }

        /// <summary>Returns a value that determines whether the currently selected printer is defined as the default printer in Control Panel.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool IsDefaultPrinter
        {
            get
            {
                return this.m_printerObject.PrinterModel.IsDefaultPrinter;
            }
        }

        /// <summary>Gets or sets a value indicating whether documents are printed in portrait or landscape mode.</summary>
        /// <returns>Returns a Short.</returns>
        public short Orientation
        {
            get
            {
                return this.m_printerObject.PrinterModel.Orientation;
            }
            set
            {
                this.CheckWithinPage("Orientation");
                this.m_printerObject.PrinterModel.Orientation = value;
                this.m_coordinateSpace.SetMarginBounds(this.m_printerObject.PrinterModel.MarginBounds);
            }
        }

        /// <summary>Returns the page number of the page that is currently being printed.</summary>
        /// <returns>Returns a Short.</returns>
        public short Page
        {
            get
            {
                return checked((short)this.m_printerObject.PageNumber);
            }
        }

        /// <summary>Gets or sets a value indicating the default paper bin on the printer from which paper is fed during print operations.</summary>
        /// <returns>Returns a Short.</returns>
        public short PaperBin
        {
            get
            {
                return this.m_printerObject.PrinterModel.PaperBin;
            }
            set
            {
                this.CheckWithinPage("PaperBin");
                this.m_printerObject.PrinterModel.PaperBin = value;
            }
        }

        /// <summary>Gets or sets a value indicating the paper size for the current printer.</summary>
        /// <returns>Returns a Short.</returns>
        public short PaperSize
        {
            get
            {
                return this.m_printerObject.PrinterModel.PaperSize;
            }
            set
            {
                this.CheckWithinPage("PaperSize");
                this.m_printerObject.PrinterModel.PaperSize = value;
                this.m_coordinateSpace.SetMarginBounds(this.m_printerObject.PrinterModel.MarginBounds);
            }
        }

        /// <summary>Gets or sets a value that determines whether the print output is directed to a printer, to a print preview window, or to a file.</summary>
        /// <returns>Returns a <see cref="T:System.Drawing.Printing.PrintAction" /> enumeration.</returns>
        public PrintAction PrintAction
        {
            get
            {
                return this.m_printerObject.PrintAction;
            }
            set
            {
                this.CheckWithinPage("PrintAction");
                this.m_printerObject.PrintAction = value;
            }
        }

        /// <summary>Gets or sets a value that specifies the file name of an Encapsulated PostScript file and the path to which the file will be saved when the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.PrintAction" /> property is set to PrintToFile.</summary>
        /// <returns>Returns a String that contains a file path and name.</returns>
        public string PrintFileName
        {
            get
            {
                return this.m_printerObject.PrintFileName;
            }
            set
            {
                this.CheckWithinPage("PrintFileName");
                this.m_printerObject.PrintFileName = value;
            }
        }

        /// <summary>Gets or sets a value that indicates the printer resolution.</summary>
        /// <returns>Returns a Short.</returns>
        public short PrintQuality
        {
            get
            {
                return this.m_printerObject.PrinterModel.PrintQuality;
            }
            set
            {
                this.CheckWithinPage("PrintQuality");
                this.m_printerObject.PrinterModel.PrintQuality = value;
            }
        }

        /// <summary>Gets or sets a Boolean value that indicates the text display direction on a right-to-left system.</summary>
        /// <returns>Returns a Boolean.</returns>
        public bool RightToLeft
        {
            get
            {
                return this.m_graphicsFactory.RightToLeft;
            }
            set
            {
                this.m_graphicsFactory.RightToLeft = value;
            }
        }

        /// <summary>Gets or sets the number of units for the vertical measurement of the page when you use graphics methods.</summary>
        /// <returns>Returns a Single.</returns>
        public float ScaleHeight
        {
            get
            {
                return this.m_coordinateSpace.ScaleHeight;
            }
            set
            {
                this.m_coordinateSpace.ScaleHeight = value;
            }
        }

        /// <summary>Gets or sets the horizontal coordinates for the left edge of the page when you are using graphics methods.</summary>
        /// <returns>Returns a Single.</returns>
        public float ScaleLeft
        {
            get
            {
                return this.m_coordinateSpace.ScaleLeft;
            }
            set
            {
                this.m_coordinateSpace.ScaleLeft = value;
            }
        }

        /// <summary>Gets or sets a value indicating the unit of measurement for the coordinates of an object when you are using graphics methods.</summary>
        /// <returns>Returns a Short.</returns>
        public short ScaleMode
        {
            get
            {
                return this.m_coordinateSpace.ScaleMode;
            }
            set
            {
                this.m_coordinateSpace.ScaleMode = value;
            }
        }

        /// <summary>Gets or sets the vertical coordinates for the top edge of the page when you are using graphics methods.</summary>
        /// <returns>Returns a Single.</returns>
        public float ScaleTop
        {
            get
            {
                return this.m_coordinateSpace.ScaleTop;
            }
            set
            {
                this.m_coordinateSpace.ScaleTop = value;
            }
        }

        /// <summary>Gets or sets the number of units for the horizontal measurement of the page when you use graphics methods.</summary>
        /// <returns>Returns a Single.</returns>
        public float ScaleWidth
        {
            get
            {
                return this.m_coordinateSpace.ScaleWidth;
            }
            set
            {
                this.m_coordinateSpace.ScaleWidth = value;
            }
        }

        /// <summary>Gets a value indicating the number of twips per pixel for an object measured horizontally.</summary>
        /// <returns>Returns a Single.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public float TwipsPerPixelX
        {
            get
            {
                return this.m_coordinateSpace.TwipsPerPixelX;
            }
        }

        /// <summary>Gets a value indicating the number of twips per pixel for an object measured vertically.</summary>
        /// <returns>Returns a Single.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public float TwipsPerPixelY
        {
            get
            {
                return this.m_coordinateSpace.TwipsPerPixelY;
            }
        }

        /// <summary>Gets or sets the width of a page.</summary>
        /// <returns>Returns an Integer.</returns>
        public int Width
        {
            get
            {
                return this.m_printerObject.PrinterModel.Width;
            }
            set
            {
                this.CheckWithinPage("Width");
                this.m_printerObject.PrinterModel.Width = value;
                this.m_coordinateSpace.SetMarginBounds(this.m_printerObject.PrinterModel.MarginBounds);
            }
        }

        /// <summary>Initializes a new instance of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> class.</summary>
        public Printer()
        {
            this.m_printerObject = new PrinterObject();
            this.Initialize();
        }

        internal Printer(string printerName)
        {
            this.m_printerObject = new PrinterObject(printerName);
            this.Initialize();
        }

        private void AddTabToLayout(ref string Layout, ref int CurrentCol, int Column)
        {
            int column;
            if (Column >= 0)
            {
                column = Column;
                if (column < 1)
                {
                    column = 1;
                }
                if (column < CurrentCol)
                {
                    Layout = string.Concat(Layout, "\r\n", StringsEx.Space(checked(column - 1)));
                }
                else
                {
                    Layout = string.Concat(Layout, StringsEx.Space(checked(column - CurrentCol)));
                }
                CurrentCol = column;
            }
            else
            {
                int currentCol = checked(CurrentCol - 1);
                int num = checked((checked(currentCol / 14 + 1)) * 14);
                column = checked(num + 1);
                Layout = string.Concat(Layout, StringsEx.Space(checked(column - CurrentCol)));
                CurrentCol = column;
            }
        }

        private RectangleF CalculateBoxBounds(float x1, float y1, float x2, float y2)
        {
            RectangleF rectangleF = new RectangleF();
            x1 = this.m_coordinateSpace.ToPixelsX(x1);
            y1 = this.m_coordinateSpace.ToPixelsY(y1);
            x2 = this.m_coordinateSpace.ToPixelsX(x2);
            y2 = this.m_coordinateSpace.ToPixelsY(y2);
            rectangleF.X = Math.Min(x1, x2);
            rectangleF.Y = Math.Min(y1, y1);
            rectangleF.Width = Math.Abs(x2 - x1);
            rectangleF.Height = Math.Abs(y2 - y1);
            return rectangleF;
        }

        private RectangleF CalculateCircleBounds(float x, float y, float radius, float aspect)
        {
            float single;
            float single1;
            RectangleF rectangleF = new RectangleF();
            if (radius < 0f)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            float pixelsWidth = this.m_coordinateSpace.ToPixelsWidth(radius);
            if (aspect <= 0f)
            {
                aspect = -aspect;
                single1 = pixelsWidth;
                single = pixelsWidth * aspect;
            }
            else if (aspect < 1f)
            {
                single1 = pixelsWidth;
                single = pixelsWidth * aspect;
            }
            else
            {
                single1 = pixelsWidth / aspect;
                single = pixelsWidth;
            }
            single1 = Math.Abs(single1);
            single = Math.Abs(single);
            float pixelsX = this.m_coordinateSpace.ToPixelsX(x);
            float pixelsY = this.m_coordinateSpace.ToPixelsY(y);
            rectangleF.X = pixelsX - single1;
            rectangleF.Y = pixelsY - single;
            rectangleF.Width = single1 * 2f;
            rectangleF.Height = single * 2f;
            return rectangleF;
        }

        private float CalculateSweep(float startAngle, float endAngle)
        {
            float degree = this.ToDegree(startAngle);
            float single = this.ToDegree(endAngle);
            if (degree > single)
            {
                return single - degree;
            }
            return single - degree - 360f;
        }

        private void CheckWithinPage(string value)
        {
            if (this.m_printerObject.State == PrinterObjectState.PaintPage)
            {
                Information.Err().Raise(396, null, string.Concat("'", value, "' ", Utils.GetResourceString("ExceptionStr_Printer_PropertyCannotBeSetWithinPage")), null, null);
            }
        }

        /// <summary>Prints a circle, an ellipse, or an arc on a page.</summary>
        /// <param name="x">Single value indicating the horizontal coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="y">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black" /> is used.</param>
        /// <param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle" /> and <paramref name="endAngle" /> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle" /> is 0 radians; the default for <paramref name="endAngle" /> is 2 * pi radians.</param>
        /// <param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle" /> and <paramref name="endAngle" /> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle" /> is 0 radians; the default for <paramref name="endAngle" /> is 2 * pi radians.</param>
        /// <param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Circle(float x, float y, float radius, int color = -1, float startAngle = float.NaN, float endAngle = float.NaN, float aspect = 1f)
        {
            this.Circle(false, x, y, radius, color, startAngle, endAngle, aspect);
        }

        /// <summary>Prints a circle, an ellipse, or an arc on a page, specifying whether the center point is relative to the current location.</summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the center of the circle, ellipse, or arc is printed relative to the coordinates specified in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" /> properties of the object.</param>
        /// <param name="x">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="y">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measurement used.</param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black" /> is used.</param>
        /// <param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startangle" /> and <paramref name="endAngle" /> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle" /> is 0 radians; the default for <paramref name="endAngle" /> is 2 * pi radians.</param>
        /// <param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle" /> and <paramref name="endAngle" /> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle" /> is 0 radians; the default for <paramref name="endAngle" /> is 2 * pi radians.</param>
        /// <param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Circle(bool relativeStart, float x, float y, float radius, int color = -1, float startAngle = float.NaN, float endAngle = float.NaN, float aspect = 1f)
        {
            if ((double)Math.Abs(startAngle) >= 6.28318530717959 | (double)Math.Abs(endAngle) >= 6.28318530717959)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if (relativeStart)
            {
                x = x + this.CurrentX;
                y = y + this.CurrentY;
            }
            this.CurrentX = x;
            this.CurrentY = y;
            if (color == -1)
            {
                color = 0;
            }
            RectangleF rectangleF = this.CalculateCircleBounds(x, y, radius, aspect);
            Graphics graphics = this.m_printerObject.GetGraphics();
            if (!(float.IsNaN(startAngle) & float.IsNaN(endAngle)))
            {
                if (float.IsNaN(startAngle))
                {
                    startAngle = 0f;
                }
                if (float.IsNaN(endAngle))
                {
                    endAngle = 0f;
                }
                float degree = this.ToDegree(startAngle);
                float single = this.CalculateSweep(startAngle, endAngle);
                if (startAngle < 0f && endAngle < 0f)
                {
                    using (Brush brush = this.m_graphicsFactory.CreateFillBrush())
                    {
                        graphics.FillPie(brush, rectangleF.X, rectangleF.Y, rectangleF.Width, rectangleF.Height, degree, single);
                    }
                }
                using (Pen pen = this.m_graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawArc(pen, rectangleF, degree, single);
                    float single1 = rectangleF.X + rectangleF.Width / 2f;
                    float single2 = rectangleF.Y + rectangleF.Height / 2f;
                    if (startAngle < 0f)
                    {
                        PointF pointF = this.PointOnCircle(rectangleF, startAngle);
                        graphics.DrawLine(pen, single1, single2, pointF.X, pointF.Y);
                    }
                    if (endAngle < 0f)
                    {
                        PointF pointF1 = this.PointOnCircle(rectangleF, endAngle);
                        graphics.DrawLine(pen, single1, single2, pointF1.X, pointF1.Y);
                    }
                }
            }
            else
            {
                using (Brush brush1 = this.m_graphicsFactory.CreateFillBrush())
                {
                    graphics.FillEllipse(brush1, rectangleF);
                }
                using (Pen pen1 = this.m_graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawEllipse(pen1, rectangleF);
                }
            }
        }

        /// <summary>Ends a print operation sent to the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object, releasing the document to the print device or spooler.</summary>
        public void EndDoc()
        {
            try
            {
                this.m_printerObject.EndDoc();
            }
            finally
            {
                this.CurrentX = 0f;
                this.CurrentY = 0f;
            }
        }

        private void FlipImage(Image image, bool flipX, bool flipY)
        {
            if (flipX && flipY)
            {
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            else if (flipX)
            {
                image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            else if (flipY)
            {
                image.RotateFlip(RotateFlipType.Rotate180FlipX);
            }
        }

        private Image GetInMemoryImage(Image picture, MemoryStream stream, bool flip, Rectangle srcRect, Rectangle pictureRect)
        {
            if (picture == null)
            {
                return null;
            }
            if (this.UseInMemoryMetafile(picture, flip))
            {
                if (stream == null)
                {
                    return null;
                }
                return new Metafile(stream);
            }
            Bitmap bitmap = new Bitmap(srcRect.Width, srcRect.Height);
            using (Graphics graphic = Graphics.FromImage(bitmap))
            {
                graphic.DrawImageUnscaled(picture, pictureRect.X, pictureRect.Y);
            }
            return bitmap;
        }

        private MemoryStream GetMemoryStream(Image picture, bool flip, Rectangle srcRect, Rectangle pictureRect)
        {
            if (picture == null)
            {
                return null;
            }
            if (!this.UseInMemoryMetafile(picture, flip))
            {
                return null;
            }
            MemoryStream memoryStream = new MemoryStream();
            using (Graphics graphic = Graphics.FromHwndInternal(IntPtr.Zero))
            {
                using (Metafile metafile = new Metafile(memoryStream, graphic.GetHdc(), srcRect, MetafileFrameUnit.Pixel))
                {
                    using (Graphics graphic1 = Graphics.FromImage(metafile))
                    {
                        graphic1.DrawImageUnscaled(picture, pictureRect.X, pictureRect.Y);
                    }
                }
            }
            memoryStream.Position = (long)0;
            return memoryStream;
        }

        private void Initialize()
        {
            this.m_graphicsFactory = new GraphicsFactory();
            this.m_coordinateSpace = new CoordinateSpace(this.m_printerObject.PrinterModel.MarginBounds, this.m_printerObject.DpiX, this.m_printerObject.DpiY);
        }

        /// <summary>Immediately stops the current print job.</summary>
        public void KillDoc()
        {
            this.m_printerObject.KillDoc();
        }

        /// <summary>Prints lines on a page.</summary>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" /> property values.</param>
        /// <param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" /> property values.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(float x2, float y2)
        {
            this.Line(true, 0f, 0f, false, x2, y2, -1, false, false);
        }

        /// <summary>Prints lines, squares, or rectangles on a page.</summary>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param>
        /// <param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param>
        /// <param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black" /> is used.</param>
        /// <param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1" />, <paramref name="y1" />, <paramref name="x2" />, and <paramref name="y2" /> coordinates specify opposite corners of the rectangle.</param>
        /// <param name="fill">Optional. Boolean. If the <paramref name="box" /> parameter is used and the <paramref name="fill" /> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill" /> without <paramref name="box" />. If <paramref name="box" /> is used without <paramref name="fill" />, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle" /> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle" /> is transparent.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(float x1, float y1, float x2, float y2, int color = -1, bool box = false, bool fill = false)
        {
            this.Line(false, x1, y1, false, x2, y2, color, box, fill);
        }

        /// <summary>Prints lines, squares, or rectangles on a page.</summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" /> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object.</param>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param>
        /// <param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param>
        /// <param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" /> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object.</param>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param>
        /// <param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black" /> is used.</param>
        /// <param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1" />, <paramref name="y1" />, <paramref name="x2" />, and <paramref name="y2" /> coordinates specify opposite corners of the rectangle.</param>
        /// <param name="fill">Optional. Boolean. If the <paramref name="box" /> parameter is used and the <paramref name="fill" /> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill" /> without <paramref name="box" />. If <paramref name="box" /> is used without <paramref name="fill" />, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor" /> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle" /> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle" /> is transparent.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(bool relativeStart, float x1, float y1, bool relativeEnd, float x2, float y2, int color = -1, bool box = false, bool fill = false)
        {
            PointF pointF = new PointF();
            PointF pixelsX = new PointF();
            if (color == -1)
            {
                if (!box)
                {
                    color = 0;
                }
                else
                {
                    color = this.ForeColor;
                }
            }
            if (relativeStart)
            {
                x1 = x1 + this.CurrentX;
                y1 = y1 + this.CurrentY;
            }
            if (relativeEnd)
            {
                x2 = x2 + x1;
                y2 = y2 + y1;
            }
            this.CurrentX = x2;
            this.CurrentY = y2;
            Graphics graphics = this.m_printerObject.GetGraphics();
            if (!box)
            {
                pointF.X = this.m_coordinateSpace.ToPixelsX(x1);
                pointF.Y = this.m_coordinateSpace.ToPixelsY(y1);
                pixelsX.X = this.m_coordinateSpace.ToPixelsX(x2);
                pixelsX.Y = this.m_coordinateSpace.ToPixelsY(y2);
                if (Math.Abs(pointF.X - pixelsX.X) < 1f && Math.Abs(pointF.Y - pixelsX.Y) < 1f)
                {
                    pixelsX.X = pixelsX.X + 1f;
                }
                using (Pen pen = this.m_graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawLine(pen, pointF, pixelsX);
                }
            }
            else
            {
                RectangleF rectangleF = this.CalculateBoxBounds(x1, y1, x2, y2);
                if (!fill)
                {
                    using (Brush brush = this.m_graphicsFactory.CreateFillBrush())
                    {
                        graphics.FillRectangle(brush, rectangleF);
                    }
                    using (Pen pen1 = this.m_graphicsFactory.CreateDrawPen(color))
                    {
                        graphics.DrawRectangle(pen1, rectangleF.X, rectangleF.Y, rectangleF.Width, rectangleF.Height);
                    }
                }
                else
                {
                    using (Brush brush1 = this.m_graphicsFactory.CreateDrawBrush(color))
                    {
                        graphics.FillRectangle(brush1, rectangleF);
                    }
                }
            }
        }

        private SizeF MeasureStringInternal(Graphics g, string line, StringFormat format)
        {
            SizeF width;
            if (StringsEx.Trim(line).Length != 0)
            {
                width = g.MeasureString(line, this.Font, 2147483647, format);
            }
            else
            {
                string str = "W";
                string str1 = string.Concat(line, str);
                SizeF sizeF = g.MeasureString(str, this.Font, 2147483647, format);
                width = g.MeasureString(str1, this.Font, 2147483647, format);
                width.Width = width.Width - sizeF.Width;
            }
            return width;
        }

        /// <summary>Stops the printing on the current page and resumes printing on a new page.</summary>
        public void NewPage()
        {
            this.m_printerObject.NewPage();
            this.CurrentX = 0f;
            this.CurrentY = 0f;
        }

        private void NormalizeRectangle(ref Rectangle rect)
        {
            rect.X = Math.Min(rect.X, checked(rect.X + rect.Width));
            rect.Y = Math.Min(rect.Y, checked(rect.Y + rect.Height));
            rect.Width = Math.Abs(rect.Width);
            rect.Height = Math.Abs(rect.Height);
        }

        private void Output(bool newLine, params object[] args)
        {
            StringBuilder stringBuilder = new StringBuilder(this.TabLayout(args));
            stringBuilder.Replace("\r\n", "\n");
            stringBuilder.Replace("\r", "\n");
            string[] strArrays = stringBuilder.ToString().Split(new char[] { '\n' });
            int length = checked(checked((int)strArrays.Length) - 2);
            for (int i = 0; i <= length; i++)
            {
                this.PrintLine(strArrays[i], true);
            }
            if (checked(checked((int)strArrays.Length) - 1) >= 0)
            {
                this.PrintLine(strArrays[checked(checked((int)strArrays.Length) - 1)], newLine);
            }
        }

        /// <summary>Prints the contents of an image file on a page.</summary>
        /// <param name="picture">
        ///   <see cref="T:System.Drawing.Image" /> value representing the image to be printed.</param>
        /// <param name="x1">Single value indicating the horizontal destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measure used.</param>
        /// <param name="y1">Single value indicating the vertical destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property determines the units of measure used.</param>
        /// <param name="width1">Optional. Single value indicating the destination width of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If the destination width is larger or smaller than the source width, picture is stretched or compressed to fit. If omitted, the source width is used.</param>
        /// <param name="height1">Optional. Single value indicating the destination height of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If the destination height is larger or smaller than the source height, picture is stretched or compressed to fit. If omitted, the source height is used.</param>
        /// <param name="x2">Optional. Single values indicating the coordinates (x-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If omitted, 0 is assumed.</param>
        /// <param name="y2">Optional. Single values indicating the coordinates (y-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If omitted, 0 is assumed.</param>
        /// <param name="width2">Optional. Single value indicating the source width of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If omitted, the entire source width is used.</param>
        /// <param name="height2">Optional. Single value indicating the source height of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property of object determines the units of measurement used. If omitted, the entire source height is used.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PaintPicture(Image picture, float x1, float y1, float width1 = float.NaN, float height1 = float.NaN, float x2 = 0f, float y2 = 0f, float width2 = float.NaN, float height2 = float.NaN)
        {
            Rectangle rectangle = new Rectangle();
            Rectangle width = new Rectangle();
            Rectangle rectangle1 = new Rectangle();
            if (picture == null)
            {
                Information.Err().Raise(91, null, null, null, null);
            }
            float single = this.ScaleX((float)picture.Width / picture.HorizontalResolution, 5, this.ScaleMode);
            float single1 = this.ScaleY((float)picture.Height / picture.VerticalResolution, 5, this.ScaleMode);
            if (float.IsNaN(width2))
            {
                width2 = single - x2;
            }
            if (float.IsNaN(height2))
            {
                height2 = single1 - y2;
            }
            if (float.IsNaN(width1))
            {
                width1 = width2;
            }
            if (float.IsNaN(height1))
            {
                height1 = height2;
            }
            rectangle.X = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsX(x1))));
            rectangle.Y = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsY(y1))));
            rectangle.Width = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsWidth(width1))));
            rectangle.Height = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsHeight(height1))));
            this.NormalizeRectangle(ref rectangle);
            rectangle1.X = checked((int)Math.Round(Math.Round((double)(this.ScaleX(x2 - this.ScaleLeft, this.ScaleMode, 5) * picture.HorizontalResolution))));
            rectangle1.Y = checked((int)Math.Round(Math.Round((double)(this.ScaleY(y2 - this.ScaleTop, this.ScaleMode, 5) * picture.VerticalResolution))));
            rectangle1.Width = checked((int)Math.Round(Math.Round((double)(this.ScaleX(width2, this.ScaleMode, 5) * picture.HorizontalResolution))));
            rectangle1.Height = checked((int)Math.Round(Math.Round((double)(this.ScaleY(height2, this.ScaleMode, 5) * picture.VerticalResolution))));
            this.NormalizeRectangle(ref rectangle1);
            width.X = 0;
            width.Y = 0;
            width.Width = picture.Width;
            width.Height = picture.Height;
            width.X = checked(width.X - rectangle1.X);
            width.Y = checked(width.Y - rectangle1.Y);
            rectangle1.X = 0;
            rectangle1.Y = 0;
            Rectangle rectangle2 = Rectangle.Intersect(rectangle1, width);
            if (rectangle2.Width <= 0 || rectangle2.Height <= 0)
            {
                Information.Err().Raise(5, null, null, null, null);
            }
            bool flag = false;
            if (Math.Sign(width1) != Math.Sign(width2))
            {
                flag = true;
            }
            bool flag1 = false;
            if (Math.Sign(height1) != Math.Sign(height2))
            {
                flag1 = true;
            }
            bool flag2 = flag | flag1;
            using (MemoryStream memoryStream = this.GetMemoryStream(picture, flag2, rectangle1, width))
            {
                using (Image inMemoryImage = this.GetInMemoryImage(picture, memoryStream, flag2, rectangle1, width))
                {
                    if (inMemoryImage != null)
                    {
                        this.FlipImage(inMemoryImage, flag, flag1);
                        this.m_printerObject.GetGraphics().DrawImage(inMemoryImage, rectangle);
                    }
                }
            }
        }

        private PointF PointOnCircle(RectangleF rect, float angle)
        {
            PointF pointF = new PointF();
            float width = rect.Width / 2f;
            float height = rect.Height / 2f;
            float x = rect.X + width;
            float y = rect.Y + height;
            angle = (float)(6.28318530717959 - (double)Math.Abs(angle));
            float single = (float)Math.Tan((double)angle);
            float single1 = (float)Math.Sqrt((double)(width * width * height * height / (width * width * single * single + height * height)));
            if (1.5707963267949 < (double)angle && (double)angle < 4.71238898038469)
            {
                single1 = -single1;
            }
            pointF.X = x + single1;
            pointF.Y = y + single1 * single;
            return pointF;
        }

        /// <summary>Prints text to a page.</summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Print(params object[] args)
        {
            this.Output(true, args);
        }

        private void PrintLine(string line, bool newLine)
        {
            RectangleF rectangleF = new RectangleF();
            if (line == null)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            Graphics graphics = this.m_printerObject.GetGraphics();
            if (line.Length > 10000)
            {
                line = line.Substring(0, 10000);
            }
            using (StringFormat stringFormat = this.m_graphicsFactory.CreateStringFormat())
            {
                SizeF sizeF = this.MeasureStringInternal(graphics, line, stringFormat);
                if (this.m_coordinateSpace.ToPixelsY(this.CurrentY) + sizeF.Height > this.m_coordinateSpace.ToPixelsHeight(this.ScaleHeight))
                {
                    this.m_printerObject.NewPage();
                    graphics = this.m_printerObject.GetGraphics();
                    this.CurrentY = 0f;
                }
                rectangleF.X = this.m_coordinateSpace.ToPixelsX(this.CurrentX);
                rectangleF.Y = this.m_coordinateSpace.ToPixelsY(this.CurrentY);
                rectangleF.Width = sizeF.Width;
                rectangleF.Height = sizeF.Height;
                if (this.RightToLeft)
                {
                    rectangleF.X = this.m_coordinateSpace.ToPixelsWidth(this.ScaleWidth) - rectangleF.X;
                }
                if (!this.FontTransparent)
                {
                    using (Brush brush = this.m_graphicsFactory.CreateDrawBrush(16777215))
                    {
                        graphics.FillRectangle(brush, rectangleF);
                    }
                }
                using (Brush brush1 = this.m_graphicsFactory.CreateDrawBrush(this.ForeColor))
                {
                    graphics.DrawString(line, this.Font, brush1, rectangleF.X, rectangleF.Y, stringFormat);
                }
                if (!newLine)
                {
                    this.CurrentX = this.CurrentX + this.m_coordinateSpace.FromPixelsWidth(sizeF.Width);
                }
                else
                {
                    this.CurrentX = 0f;
                    this.CurrentY = this.CurrentY + this.m_coordinateSpace.FromPixelsHeight(sizeF.Height);
                    if (this.m_coordinateSpace.ToPixelsY(this.CurrentY) > this.m_coordinateSpace.ToPixelsHeight(this.ScaleHeight))
                    {
                        this.m_printerObject.NewPage();
                        return;
                    }
                }
            }
        }

        /// <summary>Prints a single point on a page.</summary>
        /// <param name="x">Single value indicating the horizontal coordinates of the point to print.</param>
        /// <param name="y">Single value indicating the vertical coordinates of the point to print.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(float x, float y)
        {
            this.PSet(false, x, y, -1);
        }

        /// <summary>Prints a single point in a specified color on a page.</summary>
        /// <param name="x">Single value indicating the horizontal coordinates of the point to print.</param>
        /// <param name="y">Single value indicating the vertical coordinates of the point to print.</param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor" /> property setting is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(float x, float y, int color)
        {
            this.PSet(false, x, y, color);
        }

        /// <summary>Prints a single point in a specified color on a page, optionally specifying a point relative to the current coordinates.</summary>
        /// <param name="relativeStart">Boolean value indicating whether the coordinates are relative to the current graphics position (as set by <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX" />, <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY" />).</param>
        /// <param name="x">Single value indicating the horizontal coordinates of the point to print.</param>
        /// <param name="y">Single value indicating the vertical coordinates of the point to print.</param>
        /// <param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor" /> property setting is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void PSet(bool relativeStart, float x, float y, int color)
        {
            if (color == -1)
            {
                color = this.ForeColor;
            }
            if (relativeStart)
            {
                x = x + this.CurrentX;
                y = y + this.CurrentY;
            }
            this.CurrentX = x;
            this.CurrentY = y;
            int num = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsX(x))));
            int num1 = checked((int)Math.Round(Math.Round((double)this.m_coordinateSpace.ToPixelsY(y))));
            Graphics graphics = this.m_printerObject.GetGraphics();
            if (this.DrawWidth != 1)
            {
                using (Pen pen = this.m_graphicsFactory.CreateDrawPen(color))
                {
                    graphics.DrawLine(pen, num, num1, checked(num + 1), num1);
                }
            }
            else
            {
                using (Bitmap bitmap = new Bitmap(1, 1))
                {
                    bitmap.SetPixel(0, 0, ColorTranslator.FromOle(color));
                    graphics.DrawImage(bitmap, num, num1);
                }
            }
        }

        /// <summary>Defines the coordinate system of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object.</summary>
        public void Scale()
        {
            this.m_coordinateSpace.Scale();
        }

        /// <summary>Defines the coordinate system of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer" /> object.</summary>
        /// <param name="x1">Single value indicating the horizontal coordinates that defines the upper-left corner of the object. Parentheses must enclose the values.</param>
        /// <param name="y1">Single value indicating the vertical coordinates that defines the upper-left corner of the object. Parentheses must enclose the values.</param>
        /// <param name="x2">Single value indicating the horizontal coordinates that defines the lower-right corner of the object. Parentheses must enclose the values.</param>
        /// <param name="y2">Single value indicating the vertical coordinates that defines the lower-right corner of the object. Parentheses must enclose the values.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Scale(float x1, float y1, float x2, float y2)
        {
            this.m_coordinateSpace.Scale(x1, y1, x2, y2);
        }

        /// <summary>Converts the value for the width of a page from one of the units of measure of the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property to another.</summary>
        /// <returns>Returns a Single.</returns>
        /// <param name="value">Specify the number of units of measure to be converted.</param>
        /// <param name="fromScale">Optional. A constant or value specifying the coordinate system from which the width of the object is to be converted. The possible values of <paramref name="fromScale" /> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property.</param>
        /// <param name="toScale">Optional. A constant or value specifying the coordinate system to which the width of the object is to be converted. The possible values of <paramref name="toScale" /> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public float ScaleX(float value, short fromScale = 8, short toScale = -1)
        {
            return this.m_coordinateSpace.ScaleX(value, fromScale, toScale);
        }

        /// <summary>Converts the value for the height of a page from one of the units of measure of the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property to another.</summary>
        /// <returns>Returns a Single.</returns>
        /// <param name="value">Specify the number of units of measure to be converted.</param>
        /// <param name="fromScale">Optional. A constant or value specifying the coordinate system from which the height of the object is to be converted. The possible values of <paramref name="fromScale" /> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property.</param>
        /// <param name="toScale">Optional. A constant or value specifying the coordinate system to which the height of the object is to be converted. The possible values of <paramref name="toScale" /> are the same as those for the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode" /> property.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public float ScaleY(float value, short fromScale = 8, short toScale = -1)
        {
            return this.m_coordinateSpace.ScaleY(value, fromScale, toScale);
        }

        private string TabLayout(params object[] Args)
        {
            string str;
            TabInfo tabInfo = new TabInfo();
            SpcInfo spcInfo = new SpcInfo();
            SpcInfo spcInfo1;
            TabInfo tabInfo1;
            if (Args == null)
            {
                return "";
            }
            string str1 = null;
            int length = 1;
            bool flag = true;
            int num = Information.UBound(Args, 1);
            for (int i = 0; i <= num; i++)
            {
                object objectValue = RuntimeHelpers.GetObjectValue(Args[i]);
                if (objectValue is TabInfo)
                {
                    object obj = objectValue;
                    if (obj != null)
                    {
                        tabInfo1 = (TabInfo)obj;
                    }
                    else
                    {
                        tabInfo1 = tabInfo;
                    }
                    this.AddTabToLayout(ref str1, ref length, tabInfo1.Column);
                    flag = true;
                }
                else if (!(objectValue is SpcInfo))
                {
                    if (!flag)
                    {
                        this.AddTabToLayout(ref str1, ref length, -1);
                    }
                    if (objectValue != DBNull.Value)
                    {
                        str = (objectValue != null ? objectValue.ToString() : "");
                    }
                    else
                    {
                        str = "Null";
                    }
                    if (objectValue != null)
                    {
                        if (!(objectValue is DateTime))
                        {
                            if (!(objectValue is byte))
                            {
                                if (!(objectValue is short))
                                {
                                    if (!(objectValue is int))
                                    {
                                        if (!(objectValue is long))
                                        {
                                            if (!(objectValue is float))
                                            {
                                                if (!(objectValue is double))
                                                {
                                                    if (!(objectValue is decimal))
                                                    {
                                                        if (!objectValue.GetType().IsEnum)
                                                        {
                                                            goto Label0;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (!String.Equals(str.Substring(0, 1), CultureInfo.CurrentCulture.NumberFormat.NegativeSign, StringComparison.OrdinalIgnoreCase))
                            {
                                str = string.Concat(" ", str);
                            }
                            str = string.Concat(str, " ");
                        }
                        else
                        {
                            str = string.Concat(str, " ");
                        }
                    }
                Label0:
                    str1 = string.Concat(str1, str);
                    int num1 = StringsEx.InStrRev(str, "\r", -1, System.Extensions.CompilerServices.StringType.CompareMethod.Binary);
                    int num2 = StringsEx.InStrRev(str, "\n", -1, System.Extensions.CompilerServices.StringType.CompareMethod.Binary);
                    if (num1 > 0 || num2 > 0)
                    {
                        int integer = InteractionEx.IIf(num1 > num2, num1, num2);
                        length = checked(checked(str.Length - integer) + 1);
                    }
                    else
                    {
                        length = checked(length + str.Length);
                    }
                    flag = false;
                }
                else
                {
                    object obj1 = objectValue;
                    if (obj1 != null)
                    {
                        spcInfo1 = (SpcInfo)obj1;
                    }
                    else
                    {
                        spcInfo1 = spcInfo;
                    }
                    SpcInfo spcInfo2 = spcInfo1;
                    if (spcInfo2.Count > 0)
                    {
                        str1 = string.Concat(str1, StringsEx.Space(spcInfo2.Count));
                        length = checked(length + spcInfo2.Count);
                    }
                    flag = true;
                }
            }
            return str1;
        }

        /// <summary>Returns the height of a text string as it would be printed in the current font.</summary>
        /// <returns>Returns a Single.</returns>
        /// <param name="text">The String to be measured.</param>
        public float TextHeight(string text)
        {
            float single = 0;
            if (text == null)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if (text.Length == 0)
            {
                text = " ";
            }
            using (Graphics graphic = this.m_printerObject.CreateMeasurementGraphics())
            {
                graphic.PageUnit = GraphicsUnit.Pixel;
                using (StringFormat stringFormat = this.m_graphicsFactory.CreateStringFormat())
                {
                    float height = this.MeasureStringInternal(graphic, text, stringFormat).Height;
                    single = this.m_coordinateSpace.FromPixelsHeight(height);
                }
            }
            return single;
        }

        /// <summary>Returns the width of a text string as it would be printed in the current font.</summary>
        /// <returns>Returns a Single</returns>
        /// <param name="text">The String to be measured.</param>
        public float TextWidth(string text)
        {
            float single = 0;
            if (text == null)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            using (Graphics graphic = this.m_printerObject.CreateMeasurementGraphics())
            {
                graphic.PageUnit = GraphicsUnit.Pixel;
                using (StringFormat stringFormat = this.m_graphicsFactory.CreateStringFormat())
                {
                    float width = this.MeasureStringInternal(graphic, text, stringFormat).Width;
                    single = this.m_coordinateSpace.FromPixelsWidth(width);
                }
            }
            return single;
        }

        private float ToDegree(float angle)
        {
            angle = (float)(6.28318530717959 - (double)Math.Abs(angle));
            return (float)((double)(angle * 180f) / 3.14159265358979);
        }

        private bool UseInMemoryMetafile(Image picture, bool flip)
        {
            if (!flip && picture is Metafile)
            {
                return true;
            }
            return false;
        }

        /// <summary>Prints text to a page without adding a carriage return.</summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Write(params object[] args)
        {
            this.Output(false, args);
        }
    }
}