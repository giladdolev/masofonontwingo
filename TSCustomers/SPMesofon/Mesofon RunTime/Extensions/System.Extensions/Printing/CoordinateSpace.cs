﻿using System.Drawing;
using System.Extensions.VBExtensions;

namespace System.Extensions.Printing
{
    internal class CoordinateSpace
    {
        private const float TwipsPerInch = 1440f;

        private float m_dpiX;

        private float m_dpiY;

        private short m_scaleMode;

        private float m_originX;

        private float m_originY;

        private float m_positionX;

        private float m_positionY;

        private float m_userWidth;

        private float m_userHeight;

        private Rectangle m_marginBounds;

        public float CurrentX
        {
            get
            {
                return this.FromPixelsX(this.m_positionX);
            }
            set
            {
                this.m_positionX = this.ToPixelsX(value);
            }
        }

        public float CurrentY
        {
            get
            {
                return this.FromPixelsY(this.m_positionY);
            }
            set
            {
                this.m_positionY = this.ToPixelsY(value);
            }
        }

        public float ScaleHeight
        {
            get
            {
                if (this.m_scaleMode == 0)
                {
                    return this.m_userHeight;
                }
                return (float)this.m_marginBounds.Height * this.ZoomY();
            }
            set
            {
                this.CheckOverflow(value);
                if (this.NearZero(value))
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.ScaleMode = 0;
                this.m_userHeight = value;
            }
        }

        public float ScaleLeft
        {
            get
            {
                return this.m_originX;
            }
            set
            {
                this.CheckOverflow(value);
                this.ScaleMode = 0;
                this.m_originX = value;
            }
        }

        public short ScaleMode
        {
            get
            {
                return this.m_scaleMode;
            }
            set
            {
                if (value < 0 || value > 7)
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                if (value != 0)
                {
                    this.m_originX = 0f;
                    this.m_originY = 0f;
                    this.m_userWidth = 0f;
                    this.m_userHeight = 0f;
                }
                else if (value == 0 && this.m_scaleMode != 0)
                {
                    this.m_originX = 0f;
                    this.m_originY = 0f;
                    this.m_userWidth = this.ScaleWidth;
                    this.m_userHeight = this.ScaleHeight;
                }
                this.m_scaleMode = value;
            }
        }

        public float ScaleTop
        {
            get
            {
                return this.m_originY;
            }
            set
            {
                this.CheckOverflow(value);
                this.ScaleMode = 0;
                this.m_originY = value;
            }
        }

        public float ScaleWidth
        {
            get
            {
                if (this.m_scaleMode == 0)
                {
                    return this.m_userWidth;
                }
                return (float)this.m_marginBounds.Width * this.ZoomX();
            }
            set
            {
                this.CheckOverflow(value);
                if (this.NearZero(value))
                {
                    Information.Err().Raise(380, null, null, null, null);
                }
                this.ScaleMode = 0;
                this.m_userWidth = value;
            }
        }

        public float TwipsPerPixelX
        {
            get
            {
                return 1440f / this.m_dpiX;
            }
        }

        public float TwipsPerPixelY
        {
            get
            {
                return 1440f / this.m_dpiY;
            }
        }

        private float ZoomX()
        {
            return this.ZoomX(this.m_scaleMode);
        }
        private float ZoomX(short mode)
        {
            switch (mode)
            {
                case 0:
                    {
                        return this.m_userWidth / (float)this.m_marginBounds.Width;
                    }
                case 1:
                    {
                        return 14.4f;
                    }
                case 2:
                    {
                        return 0.72f;
                    }
                case 3:
                    {
                        return 0.01f * this.m_dpiX;
                    }
                case 4:
                    {
                        return 0.12f;
                    }
                case 5:
                    {
                        return 0.01f;
                    }
                case 6:
                    {
                        return 0.254f;
                    }
                case 7:
                    {
                        return 0.0254f;
                    }
                case 8:
                    {
                        return 25.4f;
                    }
            }
            return 0f;
        }
        private float ZoomY()
        {
            return this.ZoomY(this.m_scaleMode);
        }
        private float ZoomY(short mode)
        {
            switch (mode)
            {
                case 0:
                    {
                        return this.m_userHeight / (float)this.m_marginBounds.Height;
                    }
                case 1:
                    {
                        return 14.4f;
                    }
                case 2:
                    {
                        return 0.72f;
                    }
                case 3:
                    {
                        return 0.01f * this.m_dpiY;
                    }
                case 4:
                    {
                        return 0.06f;
                    }
                case 5:
                    {
                        return 0.01f;
                    }
                case 6:
                    {
                        return 0.254f;
                    }
                case 7:
                    {
                        return 0.0254f;
                    }
                case 8:
                    {
                        return 25.4f;
                    }
            }
            return 0f;
        }
        public CoordinateSpace(Rectangle marginBounds, float dpiX, float dpiY)
        {
            this.SetMarginBounds(marginBounds);
            this.SetDpi(dpiX, dpiY);
            this.m_scaleMode = 1;
            this.m_positionX = 0f;
            this.m_positionY = 0f;
            this.m_originX = 0f;
            this.m_originY = 0f;
            this.m_userWidth = 0f;
            this.m_userHeight = 0f;
        }

        private void CheckOverflow(float value)
        {
            if (Math.Abs(value) > 2.14748365E+09f)
            {
                Information.Err().Raise(6, null, null, null, null);
            }
        }

        public float FromPixelsHeight(float height)
        {
            return height * this.ZoomY() / (this.m_dpiY / 100f);
        }

        public float FromPixelsWidth(float width)
        {
            return width * this.ZoomX() / (this.m_dpiX / 100f);
        }

        public float FromPixelsX(float x)
        {
            return this.m_originX + x * this.ZoomX() / (this.m_dpiX / 100f);
        }

        public float FromPixelsY(float y)
        {
            return this.m_originY + y * this.ZoomY() / (this.m_dpiY / 100f);
        }

        private bool NearZero(float value)
        {
            if (Math.Abs(value) < 1E-10f)
            {
                return true;
            }
            return false;
        }

        public void Scale()
        {
            this.ScaleMode = 1;
        }

        public void Scale(float x1, float y1, float x2, float y2)
        {
            this.CheckOverflow(x1);
            this.CheckOverflow(y1);
            this.CheckOverflow(x2);
            this.CheckOverflow(y2);
            if (this.NearZero(x2 - x1))
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if (this.NearZero(y2 - y1))
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            this.m_originX = x1;
            this.m_originY = y1;
            this.m_userWidth = x2 - x1;
            this.m_userHeight = y2 - y1;
            this.m_scaleMode = 0;
        }

        public float ScaleX(float value, short fromScale, short toScale)
        {
            if (toScale == -1)
            {
                toScale = this.ScaleMode;
            }
            if (fromScale < 0 || fromScale > 8)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if (toScale < 0 || toScale > 8)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if ((fromScale == 0 || toScale == 0) && this.ScaleMode != 0)
            {
                Information.Err().Raise(5, null, null, null, null);
            }
            this.CheckOverflow(value);
            return value * this.ZoomX(toScale) / this.ZoomX(fromScale);
        }

        public float ScaleY(float value, short fromScale, short toScale)
        {
            if (toScale == -1)
            {
                toScale = this.ScaleMode;
            }
            if (fromScale < 0 || fromScale > 8)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if (toScale < 0 || toScale > 8)
            {
                Information.Err().Raise(380, null, null, null, null);
            }
            if ((fromScale == 0 || toScale == 0) && this.ScaleMode != 0)
            {
                Information.Err().Raise(5, null, null, null, null);
            }
            this.CheckOverflow(value);
            return value * this.ZoomY(toScale) / this.ZoomY(fromScale);
        }

        public void SetDpi(float dpiX, float dpiY)
        {
            this.m_dpiX = dpiX;
            this.m_dpiY = dpiY;
        }

        public void SetMarginBounds(Rectangle marginBounds)
        {
            this.m_marginBounds = marginBounds;
        }

        public float ToPixelsHeight(float height)
        {
            float single = height / this.ZoomY() * (this.m_dpiY / 100f);
            this.CheckOverflow(single);
            return single;
        }

        public float ToPixelsWidth(float width)
        {
            float single = width / this.ZoomX() * (this.m_dpiX / 100f);
            this.CheckOverflow(single);
            return single;
        }

        public float ToPixelsX(float x)
        {
            float single = (x - this.m_originX) / this.ZoomX() * (this.m_dpiX / 100f);
            this.CheckOverflow(single);
            return single;
        }

        public float ToPixelsY(float y)
        {
            float single = (y - this.m_originY) / this.ZoomY() * (this.m_dpiY / 100f);
            this.CheckOverflow(single);
            return single;
        }
    }
}
