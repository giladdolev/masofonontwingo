﻿using System.Extensions.CompilerServices;

namespace System.Extensions.Printing
{
    /// <summary>This class supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
    [StandardModule]
    public sealed class GlobalModule
    {
        internal const int DefaultIntegerValue = -1;

        internal const short DefaultShortValue = -1;

        internal const float DefaultSingleValue = float.NaN;

        private static PrinterCollection g_printerCollection;

        /// <summary>
        /// This class supports the .NET Framework infrastructure and is not intended to be used directly from your code.
        /// </summary>
        /// <value>
        /// The printers.
        /// </value>
        public static PrinterCollection Printers
        {
            get
            {
                if (GlobalModule.g_printerCollection == null)
                {
                    GlobalModule.g_printerCollection = new PrinterCollection();
                }
                return GlobalModule.g_printerCollection;
            }
        }


        static GlobalModule()
        {
        }

        internal enum ErrorCode
        {
            InvalidProcedureCall = 5,
            Overflow = 6,
            ObjectVariableNotSet = 91,
            InvalidPropertyValue = 380,
            PropertyCannotBeSetWithinPage = 396,
            DoesNotSupportThisPropertyOrMethod = 438,
            NamedArgumentNotFound = 448,
            PrinterError = 482
        }
    }

}
