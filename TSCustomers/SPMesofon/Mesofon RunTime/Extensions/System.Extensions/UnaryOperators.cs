using System.Extensions.VBExtensions;

namespace System.Extensions
{
	/// <summary>
	/// Hanldes unary operators that aren't supported in .Net target.
	/// </summary>
	public static class UnaryOperators
	{
		public static int Not(object value)
		{
			int _value = ConversionHelper.ObjectToInt32(value);
            return ~_value;
		}

		public static int BooleanMinus(bool value)
		{
            int _value = ConversionHelper.BooleanToInt(value);
            return -_value;
		}

		public static DateTime DateMinus(DateTime value)
		{
			TimeSpan span = value - DateAndTimeConversion.Zero;
			double days = span.TotalDays;
			return DateAndTime.ConvertToDate(days);
		}

		public static double StringMinus(string value)
		{
			double doubleValue = ConversionHelper.ObjectToDouble(value);
			return -doubleValue;
		}
	}
}
