using System.Drawing;
using System.Collections.Generic;

namespace System.Extensions
{
	public static class ColorConversion
	{
		private static Dictionary<int, Color> _sColorCollection = InitColorConversion();

		static Dictionary<int, Color> InitColorConversion()
		{
			Dictionary<int, Color> sColorCollection;
			sColorCollection = new Dictionary<int, Color>();
			sColorCollection.Add(-1, Color.Transparent);
			sColorCollection.Add(-2147483648, SystemColors.ScrollBar);
			sColorCollection.Add(-2147483647, SystemColors.Desktop);
			sColorCollection.Add(-2147483646, SystemColors.ActiveCaption);
			sColorCollection.Add(-2147483645, SystemColors.InactiveCaption);
			sColorCollection.Add(-2147483644, SystemColors.Menu);
			sColorCollection.Add(-2147483643, SystemColors.Window);
			sColorCollection.Add(-2147483642, SystemColors.WindowFrame);
			sColorCollection.Add(-2147483641, SystemColors.MenuText);
			sColorCollection.Add(-2147483640, SystemColors.WindowText);
			sColorCollection.Add(-2147483639, SystemColors.ActiveCaptionText);
			sColorCollection.Add(-2147483638, SystemColors.ActiveBorder);
			sColorCollection.Add(-2147483637, SystemColors.InactiveBorder);
			sColorCollection.Add(-2147483636, SystemColors.AppWorkspace);
			sColorCollection.Add(-2147483635, SystemColors.Highlight);
			sColorCollection.Add(-2147483634, SystemColors.HighlightText);
			sColorCollection.Add(-2147483633, SystemColors.Control);
			sColorCollection.Add(-2147483632, SystemColors.ControlDark);
			sColorCollection.Add(-2147483631, SystemColors.GrayText);
			sColorCollection.Add(-2147483630, SystemColors.ControlText);
			sColorCollection.Add(-2147483629, SystemColors.InactiveCaptionText);
			sColorCollection.Add(-2147483628, SystemColors.ControlLightLight);
			sColorCollection.Add(-2147483627, SystemColors.ControlDarkDark);
			sColorCollection.Add(-2147483626, SystemColors.ControlLight);
			
			sColorCollection.Add(-2147483625, SystemColors.InfoText);
			sColorCollection.Add(-2147483624, SystemColors.Info);
			
			return sColorCollection;

		}

		/// <summary>
		/// Gets the color node code from integer.
		/// </summary>
		/// <param name="color">Integer color value.</param>
		/// <returns></returns>
		public static Color Int32ToColor(int color)
		{
			Color ret;
			
			if (_sColorCollection.TryGetValue(color, out ret))
			{
				return ret;
			}
			
			if ((color & -268435456) == 0x8000000)
			{
				return ColorTranslator.FromWin32(color);
			}
			
			return ColorTranslator.FromOle(color);
		}


	}
}
