using System.Linq;
using System.Collections;
using System.Extensions.CompilerServices;

namespace System.Extensions
{
	/// <summary>
	/// Class CollectionExtensions
	/// </summary>
	public static class CollectionExtensions
	{
        /// <summary>
        /// Gets the upper bound of a collection object
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static int UBound(object collection)
        {
            Array arr = collection as Array;
            if (arr != null)
            {
                return arr.Length - 1;
            }
            ICollection coll = collection as ICollection;
            if (coll != null)
            {
                return coll.Count - 1;
            }

            return 0;
        }
		/// <summary>
		/// Gets the upper bound of a collection object
		/// </summary>
		/// <param name="collection">The collection.</param>
		/// <returns>The upper bound of the collection</returns>
		public static int UBound(this ICollection collection)
		{
			// Initialize a default value for UpperBound
			int upperBound = 0;
			
			// If the collection is not null
			if (collection != null)
			{
				// Get the count of elements and adjust to get the upper bound
				upperBound = collection.Count - 1;
			}
			
			return upperBound;
		}

		/// <summary>
		/// Gets the lower bound of a collection object
		/// </summary>
		/// <param name="collection">The collection object.</param>
		/// <returns>The lower bound of the collection</returns>
		public static int LBound(this ICollection collection)
		{
			// In .Net the lower bound will be always 0
			return 0;
		}
	}

	/// <summary>
	/// Collection class
	/// </summary>
	public sealed class Collection : IList
	{
		// Fields
		private Hashtable _hashData = new Hashtable();
		private ForEachEnum[] _iterator;
		private ArrayList _keyList = new ArrayList();
		private int _maxIterator;

		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>Total number of items</returns>
		public int Add(object item)
		{
			Add(item, null, null, null);
			return _hashData.Count;
		}


		// Methods
		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="key">The key.</param>
		/// <exception cref="System.ArgumentException">
		/// </exception>
		public void Add(object item, string key)
		{
			Add(item, key, null, null);
		}

		// Methods
		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="key">The key.</param>
		/// <param name="before">The before.</param>
		/// <exception cref="System.ArgumentException">
		/// </exception>
		public void Add(object item, string key, object before)
		{
			Add(item, key, before, null);
		}

		// Methods
		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="key">The key.</param>
		/// <param name="before">The before.</param>
		/// <param name="after">The after.</param>
		/// <exception cref="System.ArgumentException">
		/// </exception>
		public void Add(object item, string key, object before, object after)
		{
			if ((before != null) && (after != null))
			{
				throw new ArgumentException(Utils.GetResourceString("Collection_BeforeAfterExclusive"));
			}

            CollectionItem collectionItemKey = new CollectionItem(key);
            CollectionItem collectionItem = new CollectionItem(item);
            if ((key != null) && _hashData.ContainsKey(collectionItemKey))
			{
                _hashData[collectionItemKey] = collectionItem;
				return;
			}
			
			if ((before == null) && (after == null))
			{
                _hashData.Add(collectionItemKey, collectionItem);
                _keyList.Add(collectionItemKey);
			}
			else if (after == null)
			{
				string strBefore = before as string;
				if (!String.IsNullOrEmpty(strBefore))
				{
					CollectionItem item3 = new CollectionItem(strBefore);
					int index = _keyList.IndexOf(item3);
                    _keyList.Insert(index, collectionItemKey);
                    _hashData.Add(collectionItemKey, collectionItem);
				}
				else
				{
                    _keyList.Insert(IntegerType.FromObject(before) - 1, collectionItemKey);
                    _hashData.Add(collectionItemKey, collectionItem);
				}
			}
			else if (before == null)
			{
				string strAfter = after as string;
				if (!String.IsNullOrEmpty(strAfter))
				{
					CollectionItem item4 = new CollectionItem(strAfter);
					int num3 = _keyList.IndexOf(item4);
                    _keyList.Insert(num3 + 1, collectionItemKey);
                    _hashData.Add(collectionItemKey, collectionItem);
				}
				else
				{
                    _keyList.Insert(IntegerType.FromObject(after), collectionItemKey);
                    _hashData.Add(collectionItemKey, collectionItem);
				}
			}
			int num4 = _maxIterator - 1;
			for (int i = 0; i <= num4; i++)
			{
                _iterator[i].AdjustIndex(_keyList.IndexOf(collectionItemKey) + 1, false);
			}
		}

		/// <summary>
		/// Data is equal.
		/// </summary>
		/// <param name="obj1">The obj1.</param>
		/// <param name="obj2">The obj2.</param>
		/// <returns>True if the two objects are equal</returns>
		private static bool DataIsEqual(object obj1, object obj2)
		{
			return ((obj1 == obj2) || ((obj1.GetType() == obj2.GetType()) && Equals(obj1, obj2)));
		}

		/// <summary>
		/// Returns an enumerator that iterates through a collection.
		/// </summary>
		/// <returns>
		/// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
		/// </returns>
		public IEnumerator GetEnumerator()
		{
			_maxIterator++;
			_iterator = (ForEachEnum[])Utils.CopyArray((Array)_iterator, new ForEachEnum[(_maxIterator - 1) + 1]);
			_iterator[_maxIterator - 1] = new ForEachEnum(this);
			return _iterator[_maxIterator - 1];
		}

		/// <summary>
		/// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="System.InvalidCastException"></exception>
		public void CopyTo(Array array, int index)
		{
			throw new InvalidCastException(Utils.GetResourceString("InvalidCast_FromTo", GetType().FullName, Utils.VBFriendlyName(array)));
		}

		public void CopyTo(Object[] array, int index)
		{
			((ICollection)this).CopyTo(array, index);
		}


		/// <summary>
		/// Removes all items from the <see cref="T:System.Collections.IList" />.
		/// </summary>
		public void Clear()
		{
			_hashData.Clear();
			_keyList.Clear();
		}

		/// <summary>
		/// Determines whether the <see cref="T:System.Collections.IList" /> contains a specific value.
		/// </summary>
		/// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
		/// <returns>
		/// true if the <see cref="T:System.Object" /> is found in the <see cref="T:System.Collections.IList" />; otherwise, false.
		/// </returns>
		public bool Contains(object value)
		{
			return (IndexOf(value) != -1);
		}

		/// <summary>
		/// Determines whether the <see cref="T:System.Collections.IList" /> contains the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if the specified key contains key; otherwise, <c>false</c>.
		/// </returns>
		public bool ContainsKey(object key)
		{
			return _keyList.IndexOf(key) != -1;
		}

		/// <summary>
		/// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
		/// </summary>
		/// <param name="value">The object to locate in the <see cref="T:System.Collections.IList" />.</param>
		/// <returns>
		/// The index of <paramref name="value" /> if found in the list; otherwise, -1.
		/// </returns>
		public int IndexOf(object value)
		{
			IDictionaryEnumerator enumerator = _hashData.GetEnumerator();
			while (enumerator.MoveNext())
			{
				object obj2 = ((CollectionItem)enumerator.Value).Data_O;
				if (DataIsEqual(obj2, value))
				{
					return _keyList.IndexOf(enumerator.Key);
				}
			}
			return -1;
		}

		/// <summary>
		/// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
		/// <param name="value">The object to insert into the <see cref="T:System.Collections.IList" />.</param>
		public void Insert(int index, object value)
		{
			checked
			{
				index++;

			}
			CollectionItem item2 = new CollectionItem(index);
			CollectionItem item = new CollectionItem(value);
			_keyList.Insert(index, item2);
			_hashData.Add(item2, item);
			int num2 = _maxIterator - 1;
			for (int i = 0; i <= num2; i++)
			{
				_iterator[i].AdjustIndex(index, false);
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.IList" />.
		/// </summary>
		/// <param name="value">The object to remove from the <see cref="T:System.Collections.IList" />.</param>
		public void Remove(object value)
		{
			int index = IndexOf(value);
			if (index != -1)
			{
				RemoveAt(index);
			}
		}

		/// <summary>
		/// Removes the <see cref="T:System.Collections.IList" /> item at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
		public void RemoveAt(int index)
		{
			CollectionItem key = (CollectionItem)_keyList[index];
			int num2 = _maxIterator - 1;
			for (int i = 0; i <= num2; i++)
			{
				checked
				{
					_iterator[i].AdjustIndex(index + 1, true);

				}
			}
			_keyList.RemoveAt(index);
			_hashData.Remove(key);
		}

		/// <summary>
		/// Indexes the check.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <exception cref="System.IndexOutOfRangeException"></exception>
		private void IndexCheck(int index)
		{
			if ((index < 1) || (index > _keyList.Count))
			{
                throw new ArgumentOutOfRangeException("index", "Index out of range in:" + Utils.GetResourceString("Argument_CollectionIndex"));
			}
		}

		/// <summary>
		/// Removes the specified index.
		/// </summary>
		/// <param name="index">The index.</param>
		/// <exception cref="System.ArgumentException"></exception>
		public void Remove(int index)
		{
			IndexCheck(index);
			int indexToRemove = 0;
			checked
			{
				indexToRemove = index - 1;

			}
			CollectionItem key = (CollectionItem)_keyList[indexToRemove];
			if (key == null)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_CollectionIndex"));
			}
			int num2 = _maxIterator - 1;
			for (int i = 0; i <= num2; i++)
			{
				_iterator[i].AdjustIndex(index, true);
			}
			_hashData.Remove(key);
			_keyList.RemoveAt(indexToRemove);
		}

		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <exception cref="System.ArgumentException"></exception>
		public void Remove(string key)
		{
            CollectionItem collectionItemKey = new CollectionItem(key);
			if (!_hashData.ContainsKey(key))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Key"));
			}
			int num2 = _maxIterator - 1;
			for (int i = 0; i <= num2; i++)
			{
                _iterator[i].AdjustIndex(_keyList.IndexOf(collectionItemKey) + 1, false);
			}
            _hashData.Remove(collectionItemKey);
            _keyList.Remove(collectionItemKey);
		}

		// Properties
		/// <summary>
		/// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
		/// </summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.ICollection" />.</returns>
		public int Count
		{
			get { return _hashData.Count; }
		}




		/// <summary>
		/// Gets or sets the <see cref="System.Object"/> at the specified index.
		/// </summary>
		/// <value>
		/// The <see cref="System.Object"/>.
		/// </value>
		/// <param name="index">The index.</param>
		/// <returns>The object with the given index</returns>
		public object this[int index]
		{
			get
			{
				CollectionItem item2 = (CollectionItem)_keyList[index];
				CollectionItem item = (CollectionItem)_hashData[item2];
				return item.Data_O;
			}
			set
			{
				CollectionItem item = (CollectionItem)_keyList[index];
				_hashData[item] = value;
			}
		}

		/// <summary>
		/// Gets the <see cref="System.Object"/> at the specified index.
		/// </summary>
		/// <value>
		/// The <see cref="System.Object"/>.
		/// </value>
		/// <param name="index">The index.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">
		/// </exception>
		public object this[object index]
		{
			get
			{
				CollectionItem item;
				CollectionItem item2;
				if ((index is string) || (index is char))
				{
					item2 = new CollectionItem(index.ToString());
					item = (CollectionItem)_hashData[item2];
					if (item == null)
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Index"));
					}
				}
				else
				{
					int num;
					try
					{
						num = IntegerType.FromObject(index);
					}
					catch (Exception)
					{
						throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Index"));
					}
					IndexCheck(num);
					item2 = (CollectionItem)_keyList[num - 1];
					item = (CollectionItem)_hashData[item2];
				}
				return item.Data_O;
			}
		}

		/// <summary>
		/// Gets the items.
		/// </summary>
		/// <value>
		/// The items.
		/// </value>
		public object[] Items
		{
			get
			{
				int count = _hashData.Count;
				object[] array2 = new object[count + 1];
				array2[0] = "Empty placeholder to adjust for 1 based array";
				int num3 = count;
				for (int i = 1; i <= num3; i++)
				{
					CollectionItem item2 = (CollectionItem)_keyList[i - 1];
					CollectionItem item = (CollectionItem)_hashData[item2];
					array2[i] = item.Data_O;
				}
				return array2;
			}
		}

		/// <summary>
		/// Gets the keys.
		/// </summary>
		/// <value>
		/// The keys.
		/// </value>
		public object[] Keys
		{
			get { return new object[] { "Empty placeholder to adjust for 1 based array" }.Union(_keyList.ToArray()).ToArray(); }
		}

		/// <summary>
		/// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
		/// </summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.ICollection" />.</returns>
		int ICollection.Count
		{
			get { return _hashData.Count; }
		}

		/// <summary>
		/// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
		/// </summary>
		/// <returns>true if access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe); otherwise, false.</returns>
		bool ICollection.IsSynchronized
		{
			get { return false; }
		}

		/// <summary>
		/// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
		/// </summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.</returns>
		object ICollection.SyncRoot
		{
			get { return this; }
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed size.
		/// </summary>
		/// <returns>true if the <see cref="T:System.Collections.IList" /> has a fixed size; otherwise, false.</returns>
		bool IList.IsFixedSize
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> is read-only.
		/// </summary>
		/// <returns>true if the <see cref="T:System.Collections.IList" /> is read-only; otherwise, false.</returns>
		bool IList.IsReadOnly
		{
			get { return false; }
		}

	}

}
