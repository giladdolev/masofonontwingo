﻿using System.ComponentModel;
using System.Extensions;
using System.Runtime;

namespace System.Windows.Forms.Extensions
{
    /// <summary>
    /// StaticLocalInitFlag class.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [DynamicallyInvokable]
    [Serializable]
    public sealed class StaticLocalInitFlag
    {
        /// <summary>
        /// State property.
        /// </summary>
        [DynamicallyInvokable]
        public short State { get; set; }

        [DynamicallyInvokable]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public StaticLocalInitFlag()
            : base()
        {
        }
    }
}