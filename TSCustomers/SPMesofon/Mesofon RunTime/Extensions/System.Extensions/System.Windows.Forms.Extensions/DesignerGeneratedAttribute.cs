﻿namespace System.Windows.Forms.Extensions
{
    using System;
    using System.ComponentModel;
    using System.Extensions;
    using Runtime;

    /// <summary>
    /// The DesignerGeneratedAttribute class.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    [DynamicallyInvokable]
    public sealed class DesignerGeneratedAttribute : Attribute
    {
        [DynamicallyInvokable]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public DesignerGeneratedAttribute()
        {
        }
    }
}