// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "System.Extensions.VBAttributes")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "System.Windows.Forms.Extensions")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "left", Scope = "member", Target = "System.Extensions.BinaryOperators.#ObjectComparison(System.Object,System.Object,System.Extensions.BinaryOperatorType)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "op", Scope = "member", Target = "System.Extensions.BinaryOperators.#ObjectComparison(System.Object,System.Object,System.Extensions.BinaryOperatorType)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "right", Scope = "member", Target = "System.Extensions.BinaryOperators.#ObjectComparison(System.Object,System.Object,System.Extensions.BinaryOperatorType)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "objCollection", Scope = "member", Target = "System.Extensions.CollectionExtensions.#LBound(System.Collections.ICollection)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily", Scope = "member", Target = "System.Extensions.CompilerServices.ObjectType.#AddObj(System.Object,System.Object)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily", Scope = "member", Target = "System.Extensions.StringsEx.#Format(System.Object,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily", Scope = "member", Target = "System.Extensions.CompilerServices.ObjectType.#ObjTst(System.Object,System.Object,System.Boolean)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "System.Extensions.CompilerServices.GetHandler.#.ctor(System.Extensions.CompilerServices.VB6File)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "System.Extensions.CompilerServices.PutHandler.#.ctor(System.Extensions.CompilerServices.VB6File)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "System.Extensions.CompilerServices.VB6File.#GetStream()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "info", Scope = "member", Target = "System.Extensions.StringsEx.#ValidLCID(System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "name", Scope = "member", Target = "System.Extensions.InteractionEx.#MsgBox(System.Object,System.Extensions.VBEnums.MsgBoxStyle,System.Object)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Scope = "member", Target = "System.Extensions.VBExtensions.DateAndTime.#.cctor()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface", Scope = "type", Target = "System.Extensions.Collection")]
[assembly: SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#", Scope = "member", Target = "System.Extensions.VBExtensions.Information+ITypeInfo.#GetMops(System.Int32,System.String)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "System.Extensions.CompilerServices.IVbHost.#GetParentWindow()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "System.Extensions.CompilerServices.IVbHost.#GetWindowTitle()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Scope = "member", Target = "System.Extensions.VBExtensions.ErrObject.#Raise(System.Int32,System.Object,System.Object,System.Object,System.Object)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope = "member", Target = "System.Extensions.VBExtensions.Information.#TypeName(System.Object)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1039:ListsAreStronglyTyped", Scope = "type", Target = "System.Extensions.Collection")]
[assembly: SuppressMessage("Microsoft.Design", "CA1035:ICollectionImplementationsHaveStronglyTypedMembers", Scope = "type", Target = "System.Extensions.Collection")]
[assembly: SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "System.Extensions.CompilerServices.StringType.#StrLikeText(System.String,System.String)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member", Target = "System.Extensions.CompilerServices.StringType.#StrLikeBinary(System.String,System.String)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "System.Extensions.CompilerServices.StringType.#StrLikeBinary(System.String,System.String)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.Extensions.NativeMethods.CloseHandle(System.IntPtr)", Scope = "member", Target = "System.Extensions.InteractionEx.#Shell(System.String,System.Extensions.VBEnums.AppWinStyle,System.Boolean,System.Int32)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.Extensions.NativeMethods.AttachThreadInput(System.Int32,System.Int32,System.Int32)", Scope = "member", Target = "System.Extensions.InteractionEx.#AppActivateHelper(System.IntPtr)")]
[assembly: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "System.Extensions.NativeMethods.MessageBeep(System.Int32)", Scope = "member", Target = "System.Extensions.InteractionEx.#Beep()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Scope = "type", Target = "System.Extensions.CompilerServices.PutHandler")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Scope = "type", Target = "System.Extensions.CompilerServices.UnsafeNativeMethods+tagTYPEDESC")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Scope = "type", Target = "System.Extensions.NativeTypes+SECURITY_ATTRIBUTES")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#", Scope = "member", Target = "System.Extensions.CompilerServices.StringType.#MidStmtStr(System.String&,System.Int32,System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Scope = "member", Target = "System.Extensions.VBExtensions.ErrObject.#Raise(System.Int32,System.Object,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Scope = "member", Target = "System.Extensions.VBExtensions.ErrObject.#Raise(System.Int32,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Scope = "member", Target = "System.Extensions.VBExtensions.ErrObject.#Raise(System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Scope = "member", Target = "System.Extensions.VBExtensions.ErrObject.#Raise(System.Int32,System.Object,System.Object,System.Object)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "System.Extensions.VBExtensions.FileSystem.#vbIOOpenFile(System.Reflection.Assembly,System.Int32,System.String,System.Extensions.VBExtensions.FileSystem+OpenMode,System.Extensions.VBExtensions.FileSystem+OpenAccess,System.Extensions.VBExtensions.FileSystem+OpenShare,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "System.Extensions.ScriptingExtensions.#OpenFile(System.String,System.IO.FileMode,System.IO.FileAccess,System.Text.Encoding,System.Boolean)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1703:ResourceStringsShouldBeSpelledCorrectly", MessageId = "Vb", Scope = "resource", Target = "System.Extensions.Resources.resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1703:ResourceStringsShouldBeSpelledCorrectly", MessageId = "Conv", Scope = "resource", Target = "System.Extensions.Resources.resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1703:ResourceStringsShouldBeSpelledCorrectly", MessageId = "Str", Scope = "resource", Target = "System.Extensions.Resources.resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1703:ResourceStringsShouldBeSpelledCorrectly", MessageId = "Dir", Scope = "resource", Target = "System.Extensions.Resources.resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1703:ResourceStringsShouldBeSpelledCorrectly", MessageId = "Param", Scope = "resource", Target = "System.Extensions.Resources.resources")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags", Scope = "type", Target = "System.Extensions.VBEnums.AppWinStyle")]
