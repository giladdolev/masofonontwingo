using System.Text;
using System.Runtime.InteropServices;

namespace System.Extensions
{
	/// <summary>
	/// NativeMethods class
	/// </summary>
	[ComVisibleAttribute(false)]
	static internal class NativeMethods
	{

		#region Structures
		/// <summary>
		/// SHGFI contains constants
		/// </summary>
		public static class SHGFI
		{
			/// <summary>
			/// SHGFI_TYPENAME constant
			/// </summary>
			public const uint SHGFI_TYPENAME = 0x400;
			/// <summary>
			/// SHGFI_USEFILEATTRIBUTES constant
			/// </summary>
			public const uint SHGFI_USEFILEATTRIBUTES = 0x10;
		}

		/// <summary>
		/// File Info structure
		/// </summary>
		[StructLayoutAttribute(LayoutKind.Sequential)]
		public struct SHFILEINFO
		{
			/// <summary>
			/// File handle.
			/// </summary>
			private IntPtr _hIcon;

			/// <summary>
			/// Handle to icon
			/// </summary>
			public int _icon;

			/// <summary>
			/// Handle to file attributes
			/// </summary>
			public uint _uAttributes;

			/// <summary>
			/// Display name
			/// </summary>
			[MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 260)]
			public string szDisplayName;

			/// <summary>
			/// Type name
			/// </summary>
			[MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 80)]
			public string szTypeName;
		}


		#endregion


		#region Methods
		/// <summary>
		/// Attaches or detaches the input processing mechanism of one thread to that of another thread.
		/// </summary>
		/// <param name="idAttach">The identifier of the thread to be attached to another thread</param>
		/// <param name="idAttachTo">The identifier of the thread to which idAttach will be attached.</param>
		/// <param name="fAttach">If this parameter is TRUE, the two threads are attached. If the parameter is FALSE, the threads are detached.</param>
		/// <returns>
		/// If the function succeeds, the return value is nonzero.
		/// </returns>
		[DllImportAttribute("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern int AttachThreadInput(int idAttach, int idAttachTo, int fAttach);
		/// <summary>
		/// Closes the handle.
		/// </summary>
		/// <param name="hObject">The object.</param>
		/// <returns></returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern int CloseHandle(IntPtr hObject);
		/// <summary>
		/// Creates a new process and its primary thread. The new process runs in the security context of the calling process.
		/// </summary>
		/// <param name="lpApplicationName">The name of the module to be executed.</param>
		/// <param name="lpCommandLine">The command line to be executed. </param>
		/// <param name="lpProcessAttributes">A pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle to the new process object can be inherited by child processes.</param>
		/// <param name="lpThreadAttributes">A pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle to the new thread object can be inherited by child processes.</param>
		/// <param name="bInheritHandles">If this parameter TRUE, each inheritable handle in the calling process is inherited by the new process. If the parameter is FALSE, the handles are not inherited. </param>
		/// <param name="dwCreationFlags">The flags that control the priority class and the creation of the process. </param>
		/// <param name="lpEnvironment">A pointer to the environment block for the new process.</param>
		/// <param name="lpCurrentDirectory">The full path to the current directory for the process. </param>
		/// <param name="lpStartupInfo">A pointer to a STARTUPINFO or STARTUPINFOEX structure.</param>
		/// <param name="lpProcessInformation">A pointer to a PROCESS_INFORMATION structure that receives identification information about the new process. </param>
		/// <returns>If the function succeeds, the return value is nonzero.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern int CreateProcess([MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpApplicationName, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpCommandLine, NativeTypes.SECURITY_ATTRIBUTES lpProcessAttributes, NativeTypes.SECURITY_ATTRIBUTES lpThreadAttributes, [MarshalAsAttribute(UnmanagedType.Bool)]bool bInheritHandles, int dwCreationFlags, IntPtr lpEnvironment, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpCurrentDirectory, NativeTypes.STARTUPINFO lpStartupInfo, NativeTypes.PROCESS_INFORMATION lpProcessInformation);
		/// <summary>
		/// Retrieves a handle to the top-level window whose class name and window name match the specified strings. 
		/// </summary>
		/// <param name="lpClassName">The class name or a class atom created by a previous call to the RegisterClass or RegisterClassEx function.</param>
		/// <param name="lpWindowName">he window name (the window's title). If this parameter is NULL, all window names match.</param>
		/// <returns>If the function succeeds, the return value is a handle to the window that has the specified class name and window name.</returns>
		[DllImportAttribute("user32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern IntPtr FindWindow(IntPtr lpClassName, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpWindowName);
		/// <summary>
		/// Retrieves a handle to the desktop window. 
		/// </summary>
		/// <returns>The return value is a handle to the desktop window.</returns>
		[DllImportAttribute("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern IntPtr GetDesktopWindow();
		/// <summary>
		/// Retrieves the contents of the STARTUPINFO structure that was specified when the calling process was created.
		/// </summary>
		/// <param name="lpStartupInfo">A pointer to a STARTUPINFO structure that receives the startup information.</param>
		[DllImportAttribute("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern void GetStartupInfo([InAttribute(), OutAttribute()]NativeTypes.STARTUPINFO lpStartupInfo);
		/// <summary>
		/// Retrieves information about the file system and volume associated with the specified root directory.
		/// </summary>
		/// <param name="lpRootPathName">A pointer to a string that contains the root directory of the volume to be described.</param>
		/// <param name="lpVolumeNameBuffer">A pointer to a buffer that receives the name of a specified volume. </param>
		/// <param name="nVolumeNameSize">The length of a volume name buffer, in TCHARs.</param>
		/// <param name="lpVolumeSerialNumber">A pointer to a variable that receives the volume serial number.</param>
		/// <param name="lpMaximumComponentLength">A pointer to a variable that receives the maximum length, in TCHARs, of a file name component that a specified file system supports.</param>
		/// <param name="lpFileSystemFlags">A pointer to a variable that receives flags associated with the specified file system.</param>
		/// <param name="lpFileSystemNameBuffer">A pointer to a buffer that receives the name of the file system, for example, the FAT file system or the NTFS file system. </param>
		/// <param name="nFileSystemNameSize">The length of the file system name buffer, in TCHARs. </param>
		/// <returns>If all the requested information is retrieved, the return value is nonzero.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		static internal extern int GetVolumeInformation(string lpRootPathName, StringBuilder lpVolumeNameBuffer, int nVolumeNameSize, ref int lpVolumeSerialNumber, ref int lpMaximumComponentLength, ref int lpFileSystemFlags, IntPtr lpFileSystemNameBuffer, int nFileSystemNameSize);
		/// <summary>
		/// Retrieves a handle to a window that has the specified relationship (Z-Order or owner) to the specified window.
		/// </summary>
		/// <param name="hwnd">A handle to a window.</param>
		/// <param name="wFlag">The relationship between the specified window and the window whose handle is to be retrieved.</param>
		/// <returns>If the function succeeds, the return value is a window handle.</returns>
		[DllImportAttribute("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern IntPtr GetWindow(IntPtr hwnd, int wFlag);
		/// <summary>
		/// Copies the text of the specified window's title bar (if it has one) into a buffer. 
		/// </summary>
		/// <param name="hwnd">A handle to the window or control containing the text.</param>
		/// <param name="lpString">The buffer that will receive the text. </param>
		/// <param name="cch">The maximum number of characters to copy to the buffer, including the null character.</param>
		/// <returns>If the function succeeds, the return value is the length, in characters, of the copied string, not including the terminating null character. </returns>
		[DllImportAttribute("user32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern int GetWindowText(IntPtr hwnd, [MarshalAsAttribute(UnmanagedType.VBByRefStr)]ref string lpString, int cch);
		/// <summary>
		/// Sets the keyboard focus to the specified window. 
		/// </summary>
		/// <param name="hwnd">A handle to the window that will receive the keyboard input.</param>
		/// <returns>If the function succeeds, the return value is the handle to the window that previously had the keyboard focus. </returns>
		[DllImportAttribute("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern IntPtr SetFocus(IntPtr hwnd);
		/// <summary>
		/// Brings the thread that created the specified window into the foreground and activates the window.
		/// </summary>
		/// <param name="hwnd">A handle to the window that should be activated and brought to the foreground.</param>
		/// <returns>If the window was brought to the foreground, the return value is nonzero.</returns>
		[DllImportAttribute("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern int SetForegroundWindow(IntPtr hwnd);
		/// <summary>
		/// Waits until the specified process has finished processing its initial input and is waiting for user input with no input pending, or until the time-out interval has elapsed.
		/// </summary>
		/// <param name="process">A handle to the process. </param>
		/// <param name="milliseconds">The time-out interval, in milliseconds.</param>
		/// <returns></returns>
		[DllImportAttribute("user32", CharSet = CharSet.Auto, SetLastError = true)]
		static internal extern int WaitForInputIdle(IntPtr process, int milliseconds);
		/// <summary>
		/// Waits until the specified object is in the signaled state or the time-out interval elapses.
		/// </summary>
		/// <param name="hHandle">The h handle.</param>
		/// <param name="dwMilliseconds">The dw milliseconds.</param>
		/// <returns></returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		static internal extern int WaitForSingleObject(IntPtr hHandle, int dwMilliseconds);
		/// <summary>
		/// Retrieves a bitmask representing the currently available disk drives.
		/// </summary>
		/// <returns>Integer value.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Unicode)]
		static internal extern int GetLogicalDrives();
		/// <summary>
		/// Moves an existing file or a directory, including its children.
		/// </summary>
		/// <param name="lpExistingFileName">The current name of the file or directory on the local computer.</param>
		/// <param name="lpNewFileName">The new name for the file or directory.</param>
		/// <returns>If the function succeeds, the return value is nonzero.If the function fails, the return value is zero.</returns>
		[DllImportAttribute("kernel32", CharSet = CharSet.Unicode)]
		static internal extern int MoveFile(string lpExistingFileName, string lpNewFileName);
		/// <summary>
		/// Plays a waveform sound. The waveform sound for each sound type is identified by an entry in the registry.
		/// </summary>
		/// <param name="uType">The sound to be played. The sounds are set by the user through the Sound control panel application, and then stored in the registry.</param>
		/// <returns>If the function succeeds, the return value is nonzero.If the function fails, the return value is zero.</returns>
		[DllImportAttribute("user32", CharSet = CharSet.Unicode)]
		static internal extern int MessageBeep(int uType);
		/// <summary>
		/// Retrieves information about an object in the file system, such as a file, folder, directory, or drive root.
		/// </summary>
		/// <param name="pszPath">A pointer to a null-terminated string of maximum length MAX_PATH that contains the path and file name. </param>
		/// <param name="dwFileAttributes">A combination of one or more file attribute flags (FILE_ATTRIBUTE_ values as defined in Winnt.h). </param>
		/// <param name="psfi">Pointer to a SHFILEINFO structure to receive the file information.</param>
		/// <param name="cbSizeFileInfo">The size, in bytes, of the SHFILEINFO structure pointed to by the psfi parameter.</param>
		/// <param name="uFlags">The flags that specify the file information to retrieve.</param>
		/// <returns>Returns a value whose meaning depends on the uFlags parameter. </returns>
		[DllImportAttribute("shell32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);


		#endregion
	}


}
