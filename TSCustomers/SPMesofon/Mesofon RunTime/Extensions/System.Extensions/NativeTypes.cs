﻿using System.Runtime.InteropServices;
using System.ComponentModel;

namespace System.Extensions
{
    /// <summary>
    /// The NativeTypes class
    /// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
internal sealed class NativeTypes
{
    // Fields
    internal const int GW_CHILD = 5;
    internal const int GW_HWNDFIRST = 0;
    internal const int GW_HWNDLAST = 1;
    internal const int GW_HWNDNEXT = 2;
    internal const int GW_HWNDPREV = 3;
    internal const int GW_MAX = 5;
    internal const int GW_OWNER = 4;
    internal const int LCMAP_FULLWIDTH = 0x800000;
    internal const int LCMAP_HALFWIDTH = 0x400000;
    internal const int LCMAP_HIRAGANA = 0x100000;
    internal const int LCMAP_KATAKANA = 0x200000;
    internal const int LCMAP_LOWERCASE = 0x100;
    internal const int LCMAP_SIMPLIFIED_CHINESE = 0x2000000;
    internal const int LCMAP_TRADITIONAL_CHINESE = 0x4000000;
    internal const int LCMAP_UPPERCASE = 0x200;
    internal const int NORMAL_PRIORITY_CLASS = 0x20;
    internal const int STARTF_USESHOWWINDOW = 1;

    // Nested Types

    /// <summary>
    /// Contains information about a newly created process and its primary thread. 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal sealed class PROCESS_INFORMATION
    {
        // A handle to the newly created process.
        public IntPtr hProcess;

        // A handle to the primary thread of the newly created process.
        public IntPtr hThread;

        // A value that can be used to identify a process. 
        public int dwProcessId;

        // A value that can be used to identify a thread.
        public int dwThreadId;
    }

    /// <summary>
    /// Contains the security descriptor for an object and specifies whether the handle retrieved by specifying this structure is inheritable.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal sealed class SECURITY_ATTRIBUTES
    {
        // The size, in bytes, of this structure.
        public int nLength;

        // A pointer to a SECURITY_DESCRIPTOR structure that controls access to the object.
        public int lpSecurityDescriptor;

        // A Boolean value that specifies whether the returned handle is inherited when a new process is created.
        public int bInheritHandle;
    }

    /// <summary>
    /// Specifies the window station, desktop, standard handles, and appearance of the main window for a process at creation time.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    internal sealed class STARTUPINFO
    {
        // The size of the structure, in bytes.
        public int cb;

        // Reserved; must be NULL.
        public int lpReserved;

        // The name of the desktop, or the name of both the desktop and window station for this process. 
        public int lpDesktop;

        // For console processes, this is the title displayed in the title bar if a new console window is created. 
        public int lpTitle;

        // If dwFlags specifies STARTF_USEPOSITION, this member is the x offset of the upper left corner of a window if a new window is created, in pixels.
        public int dwX;

        // If dwFlags specifies STARTF_USEPOSITION, this member is the y offset of the upper left corner of a window if a new window is created, in pixels.
        public int dwY;

        // If dwFlags specifies STARTF_USESIZE, this member is the width of the window if a new window is created, in pixels.
        public int dwXSize;

        // If dwFlags specifies STARTF_USESIZE, this member is the height of the window if a new window is created, in pixels. 
        public int dwYSize;

        // If dwFlags specifies STARTF_USECOUNTCHARS, if a new console window is created in a console process, this member specifies the screen buffer width, in character columns. 
        public int dwXCountChars;

        // If dwFlags specifies STARTF_USECOUNTCHARS, if a new console window is created in a console process, this member specifies the screen buffer height, in character rows.
        public int dwYCountChars;

        // If dwFlags specifies STARTF_USEFILLATTRIBUTE, this member is the initial text and background colors if a new console window is created in a console application. 
        public int dwFillAttribute;

        // A bitfield that determines whether certain STARTUPINFO members are used when the process creates a window. 
        public int dwFlags;

        // If dwFlags specifies STARTF_USESHOWWINDOW, this member can be any of the values that can be specified in the nCmdShow parameter for the ShowWindow function, except for SW_SHOWDEFAULT
        public int wShowWindow;

        // Reserved for use by the C Run-time; must be zero.
        public int cbReserved2;

        // Reserved for use by the C Run-time; must be NULL.
        public long lpReserved2;

        // If dwFlags specifies STARTF_USESTDHANDLES, this member is the standard input handle for the process.
        public IntPtr hStdInput;

        // If dwFlags specifies STARTF_USESTDHANDLES, this member is the standard output handle for the process. 
        public IntPtr hStdOutput;

        // If dwFlags specifies STARTF_USESTDHANDLES, this member is the standard error handle for the process. 
        public IntPtr hStdError;
    }

    /// <summary>
    /// Specifies a date and time, using individual members for the month, day, year, weekday, hour, minute, second, and millisecond.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal sealed class SystemTime
    {
        // The year.
        public short wYear;

        // The month. 
        public short wMonth;

        // The day of the week.
        public short wDayOfWeek;

        // The day of the month.
        public short wDay;

        // The hour. 
        public short wHour;

        // The minute.
        public short wMinute;

        // The second.
        public short wSecond;

        // The millisecond. 
        public short wMilliseconds;
    }
}
}
