﻿using System.Security;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace System.Extensions
{
    /// <summary>
    /// SafeNativeMethods class
    /// </summary>
    [SuppressUnmanagedCodeSecurity, ComVisible(false), EditorBrowsable(EditorBrowsableState.Never)]
    internal static class SafeNativeMethods
    {
        // Methods
        /// <summary>
        /// Retrieves the current local date and time.
        /// </summary>
        /// <param name="systime">A pointer to a SYSTEMTIME structure to receive the current local date and time.</param>
        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        internal static extern void GetLocalTime(NativeTypes.SystemTime systime);
        /// <summary>
        /// Retrieves the identifier of the thread that created the specified window and, optionally, the identifier of the process that created the window. Retrieves the identifier of the thread that created the specified window and, optionally, the identifier of the process that created the window. 
        /// </summary>
        /// <param name="hwnd">A handle to the window.</param>
        /// <param name="lpdwProcessId">A pointer to a variable that receives the process identifier.</param>
        /// <returns> The return value is the identifier of the thread that created the window.</returns>
        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        internal static extern int GetWindowThreadProcessId(IntPtr hwnd, ref int lpdwProcessId);
        /// <summary>
        /// Determines whether the specified window is enabled for mouse and keyboard input.
        /// </summary>
        /// <param name="hwnd">A handle to the window to be tested.</param>
        /// <returns>If the window is enabled, the return value is nonzero.</returns>
        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        [return : MarshalAs(UnmanagedType.Bool)]
        internal static extern bool IsWindowEnabled(IntPtr hwnd);
        /// <summary>
        /// Determines the visibility state of the specified window.
        /// </summary>
        /// <param name="hwnd">A handle to the window to be tested.</param>
        /// <returns>If the specified window, its parent window, its parent's parent window, and so forth, have the WS_VISIBLE style, the return value is nonzero. </returns>
        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool IsWindowVisible(IntPtr hwnd);
    }
}
