﻿using System.ComponentModel;
using System.Runtime;
using System.Runtime.Serialization;

namespace System.Extensions
{
    // Type: Microsoft.VisualBasic.CompilerServices.IncompleteInitialization
    // Assembly: Microsoft.VisualBasic, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\Microsoft.VisualBasic.dll

    /// <summary>
    /// The Visual Basic compiler uses this class during static local initialization; it is not meant to be called directly from your code. An exception of this type is thrown if a static local variable fails to initialize.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [DynamicallyInvokable]
    [Serializable]
    public sealed class IncompleteInitialization : Exception
    {
        /// <summary>
        /// Initializes a new instance of the IncompleteInitialization class.
        /// </summary>
        /// <param name="message">A string representing the message to be sent.</param>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public IncompleteInitialization(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new instance of the IncompleteInitialization class.
        /// </summary>
        /// <param name="message">A string representing the message to be sent.</param>
        /// <param name="innerException">An Exception object.</param>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public IncompleteInitialization(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the IncompleteInitialization class.
        /// </summary>
        [DynamicallyInvokable]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public IncompleteInitialization()
        {
        }

        /// <summary>
        /// Initializes a new instance of the IncompleteInitialization class.
        /// </summary>
        /// <param name="info">An SerializationInfo object.</param>
        /// <param name="context">An StreamingContext object.</param>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        private IncompleteInitialization(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}