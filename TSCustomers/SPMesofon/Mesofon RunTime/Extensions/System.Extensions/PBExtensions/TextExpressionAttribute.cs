﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class TextExpressionAttribute :Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.PropertiesAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The name of the column attribute.
        public TextExpressionAttribute(string Exp, string TrueExp, string FalseExp)
        {
            this.Exp = Exp;
            this.TrueExp = TrueExp;
            this.FalseExp = FalseExp;
        }

        public string Exp { get; set; }

        public string TrueExp { get; set; }
        public string FalseExp { get; set; }
    }
}
