﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public enum Buttons
    {
        Button1,
        Button2
    }
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public sealed class ButtonButtonColumnAttribute : Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.PropertiesAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The name of the column attribute.
        public ButtonButtonColumnAttribute(Buttons Button, string buttonName, string text, string visibleExp)
        {
            this.Button = Button;
            Name = buttonName;
            Text = text;
            VisibleExp = visibleExp;
        }

        //
        // Summary:
        //     Gets the Buttons of the attribute.
        //
        // Returns:
        //     The Buttons of the attribute.
        public Buttons Button { get; set; }


        //
        // Summary:
        //     Gets the name of the attribute.
        //
        // Returns:
        //     The name of the attribute.
        public string Name { get; set; }
         

        //
        // Summary:
        //     Gets the Caption of the attribute.
        //
        // Returns:
        //     The Caption of the attribute.
        public string Text { get; set; }

        //
        // Summary:
        //     Gets the Visible of the attribute.
        //
        // Returns:
        //     The Visible of the attribute.
        public string VisibleExp { get; set; }

        public override object TypeId
        {
            get
            {
                return this;
            }
        }
    }

 

}
