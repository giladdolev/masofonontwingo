﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class ComputeAttribute : Attribute
    {
        public ComputeAttribute(string propertyName,string computeExpression)
        {
            this.propertyName = propertyName;
            this.computeExpression = computeExpression;
        }
        public string propertyName { get; set; }
        public string computeExpression { get; set; }

        public override object TypeId
        {
            get
            {
                return this;
            }
        }
    }
}
