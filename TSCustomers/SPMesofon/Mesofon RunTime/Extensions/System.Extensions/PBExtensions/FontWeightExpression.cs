﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class FontWeightExpressionAttribute : Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.FontWeightExpressionAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The name of the column attribute.
        public FontWeightExpressionAttribute(string Exp, string TrueCSSClassName, string FalseCSSClassName)
        {
            this.Exp = Exp;
            this.TrueCSSClassName = TrueCSSClassName;
            this.FalseCSSClassName = FalseCSSClassName;
        }

        public string Exp { get; set; }

        public string TrueCSSClassName { get; set; }
        public string FalseCSSClassName { get; set; }
    }
}
