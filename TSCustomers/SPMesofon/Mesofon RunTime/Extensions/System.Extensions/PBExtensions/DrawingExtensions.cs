﻿using System.Drawing;

namespace System.Extensions
{
    public static class DrawingExtensions
    {
        /// <summary>
        /// Gets the font style.
        /// </summary>
        /// <param name="weight">The PB font weight.</param>
        /// <returns></returns>
        public static FontStyle GetFontStyle(FontStyle style, int weight)
        {
            return GetFontStyle(style, FontStyle.Bold, weight == 700);
        }
        /// <summary>
        /// Gets the font style.
        /// </summary>
        /// <param name="style">The style.</param>
        /// <param name="newStyle">The new style.</param>
        /// <param name="on">if set to <c>true</c> switch new style on.</param>
        /// <returns></returns>
        public static FontStyle GetFontStyle(FontStyle style, FontStyle newStyle, bool on)
        {
            if (on)
            {
                return style | newStyle;
            }
            return style ^ newStyle;
        }

        /// <summary>
        /// Sets the font weight.
        /// </summary>
        /// <param name="prototypeFont">The prototype font.</param>
        /// <param name="weight">The weight.</param>
        /// <returns></returns>
        public static Font SetFontWeight(Font prototypeFont, int weight)
        {
            return new Font(prototypeFont, GetFontStyle(prototypeFont.Style, weight));
        }
     }
}
