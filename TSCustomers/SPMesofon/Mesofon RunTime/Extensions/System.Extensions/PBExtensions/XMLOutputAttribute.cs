﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public enum XMLOutputLocation
    {
        Header,
        Summary,
        Footer
    }

    /// <summary>
    /// 
    /// </summary>
    public class XMLOutputAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="XMLLocation"></param>
        public XMLOutputAttribute(XMLOutputLocation XMLLocation)
        {
            this.XMLLocation = XMLLocation;
        }

        /// <summary>
        /// 
        /// </summary>
        public XMLOutputLocation XMLLocation { get; set; }
    }
}
