﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class BackColorAttribute : Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.BackColorAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The BackColor
        public BackColorAttribute(string exp, string cssClass)
        {
            Exp = exp;
            CSSClass = cssClass;
        }

        //
        // Summary:
        //     Gets the Exp of the attribute.
        //
        // Returns:
        //     The Exp of the attribute.
        public string Exp { get; set; }

        //
        // Summary:
        //     Gets the CSSClass of the attribute.
        //
        // Returns:
        //     The CSSClass of the attribute.
        public string CSSClass { get; set; }
    }
}
