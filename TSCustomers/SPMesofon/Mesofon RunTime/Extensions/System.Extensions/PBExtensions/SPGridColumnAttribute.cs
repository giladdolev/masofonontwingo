﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class SPGridColumnAttribute : Attribute
    {
        /// <summary>
        /// Initializes Data Members for SPGridColumn
        /// </summary>
        /// <param name="dataMembers">Data Members</param>
        public SPGridColumnAttribute(string dataMembers)
        {
            DataMembers = dataMembers;
        }
        /// <summary>
        /// Data Members
        /// </summary>
        public string DataMembers { get; set; }
    }
}
