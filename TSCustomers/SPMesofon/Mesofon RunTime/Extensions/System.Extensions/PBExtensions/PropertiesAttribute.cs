﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class PropertiesAttribute : Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.PropertiesAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The name of the column attribute.
        public PropertiesAttribute(string name, string caption, bool visible,int displayIndex, Type type = null,int width = -1, bool enabled = true)
        {
            Name = name;
            Caption = caption;
            Visible = visible;
            DisplayIndex = displayIndex;
            if (type == null)
                Type = typeof(string);
            else
                Type = type;
            Width = width;
            Enabled = enabled;
        }
        public Type Type { get; set; }
        //
        // Summary:
        //     Gets the name of the attribute.
        //
        // Returns:
        //     The name of the attribute.
        public string Name { get; set; }
        //
        // Summary:
        //     Gets or sets the order of the column.
        //
        // Returns:
        //     The order of the column.
        public int DisplayIndex { get; set; }

        //
        // Summary:
        //     Gets the Caption of the attribute.
        //
        // Returns:
        //     The Caption of the attribute.
        public string Caption { get; set; }

        //
        // Summary:
        //     Gets the Visible of the attribute.
        //
        // Returns:
        //     The Visible of the attribute.
        public bool Visible { get; set; }

        //
        // Summary:
        //     Gets the Width of the attribute.
        //
        // Returns:
        //     The Width of the attribute.
        public int Width { get; set; }

        //
        // Summary:
        //     Gets the Enabled of the attribute.
        //
        // Returns:
        //     The Enabled of the attribute.
        public bool Enabled { get; set; }

    }

}
