﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class ProtectExpressionAttribute : Attribute
    {
        /// <summary>
        /// Expression that indicating whether the control can respond to user.
        /// </summary>
        /// <param name="Exp"></param>
        /// <param name="TrueExp"></param>
        /// <param name="FalseExp"></param>
        public ProtectExpressionAttribute(string Exp, string TrueExp, string FalseExp)
        {
            this.Exp = Exp;
            this.TrueExp = TrueExp;
            this.FalseExp = FalseExp;
        }

        public string Exp { get; set; }

        public string TrueExp { get; set; }
        public string FalseExp { get; set; }
    }
}
