﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class CustomColumnAttribute : Attribute
    {
        public CustomColumnAttribute(string RendererMethod)
        {
            this.RendererMethod = RendererMethod;
        }

        public string RendererMethod { get; set; }
    }
}
