﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class HTMLContentAttribute : Attribute
    {
        /// <summary>
        /// Replace **AutoIncrement** with auto number
        /// </summary>
        /// <param name="HtmlContent"></param>
        /// <param name="DataMembers"></param>
        public HTMLContentAttribute(string HtmlContent, string DataMembers)
        {
            this.HtmlContent = HtmlContent;
            this.DataMembers = DataMembers;
        }

        public string HtmlContent { get; set; }
        public string DataMembers { get; set; }
    }
}
