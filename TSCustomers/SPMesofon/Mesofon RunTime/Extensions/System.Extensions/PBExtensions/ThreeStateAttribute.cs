﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class ThreeStateAttribute : Attribute
    {
        public ThreeStateAttribute(string on,string off,string othersate)
        {
            this.On = on;
            this.Off = off;
            this.OtherState = othersate;
        }
        public string On { get; set; }
        public string Off { get; set; }
        public string OtherState { get; set; }
    }
}
