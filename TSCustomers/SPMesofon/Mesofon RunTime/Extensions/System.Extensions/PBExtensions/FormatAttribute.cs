﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public class FormatAttribute : Attribute
    {
        //
        // Summary:
        //     Initializes a new instance of the System.ComponentModel.DataAnnotations.Schema.FormatAttribute
        //     class.
        //
        // Parameters:
        //   name:
        //     The custom format
        public FormatAttribute(string CustomFormat)
        {
            this.CustomFormat = CustomFormat;
        }

        //
        // Summary:
        //     Gets the Custom Format of the attribute.
        //
        // Returns:
        //     The Custom Format of the attribute.
        public string CustomFormat { get; set; }

        
    }
}
