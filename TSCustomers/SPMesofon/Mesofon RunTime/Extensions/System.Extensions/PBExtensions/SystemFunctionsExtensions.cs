﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    public static class SystemFunctionsExtensions
    {
        /// <summary>
        /// Reports whether the value of a string is a number.
        /// </summary>
        /// <param name="n">A string whose value you want to test to determine whether it is a valid number</param>
        /// <returns>Boolean. Returns true if string is a valid number and false if it is not.</returns>
        public static bool IsNumber(string str)
        {
            double res;
            return Double.TryParse(str, out res);
        }


        /// <summary>
        /// Reports whether the value of a string is a valid time value.
        /// </summary>
        /// <param name="timevalue">A string whose value you want to test to determine whether it is a valid time</param>
        /// <returns>Boolean. Returns true if timevalue is a valid time and false if it is not.</returns>
        public static bool IsTime(string timevalue)
        {
            TimeSpan dummyTimeSpan;
            return TimeSpan.TryParse(timevalue, out dummyTimeSpan);
        }


        /// <summary>
        /// Tests whether a string value is a valid date.
        /// </summary>
        /// <param name="datevalue">A string whose value you want to test to determine whether it is a valid date</param>
        /// <returns>Boolean. Returns true if datevalue is a valid date and false if it is not. If datevalue is null, IsDate returns null.</returns>
        public static bool? IsDate(string datevalue)
        {
            if (datevalue == null)
            {
                return null;
            }
            DateTime dummyDate;
            return DateTime.TryParse(datevalue, out dummyDate);
        }

        /// <summary>
        /// Builds a string of the specified length by repeating the specified characters until the result string is long enough.
        /// </summary>
        /// <param name="str">A string whose value will be repeated to fill the return string</param>
        /// <param name="n">A long whose value is the number of characters in the string you want returned</param>
        /// <returns>String. Returns a string n characters long filled with repetitions of the characters in the argument chars. If the argument chars has more than n characters, the first n characters of chars are used to fill the return string. If the argument chars has fewer than n characters, the characters in chars are repeated until the return string has n characters.</returns>
        public static string Fill(string str, int n)
        {

            if (string.IsNullOrEmpty(str) || str.Length == n)
            {
                return str;
            }


            if (n < 0)
            {
                return "";
            }

            //If the argument chars has more than n characters, the first n characters of chars are used to fill the return string.
            if (str.Length > n)
            {
                return str.Substring(0, n);
            }

            //If the argument chars has fewer than n characters, the characters in chars are repeated until the return string has n characters
            string returnValue = str;
            while (returnValue.Length < n)
            {
                returnValue += str;
            }
            return returnValue.Substring(0, n);
        }

        public static PropertiesAttribute GetPropertiesAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<PropertiesAttribute>().FirstOrDefault();

        }

        public static FormatAttribute GetFormatAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<FormatAttribute>().FirstOrDefault();

        }
        public static PreventFlagChangeAttribute GetPreventFlagChangeAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<PreventFlagChangeAttribute>().FirstOrDefault();

        }
        public static NumberFieldAttribute GetNumberFieldAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<NumberFieldAttribute>().FirstOrDefault();

        }

        public static ThreeStateAttribute GetThreeStateAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<ThreeStateAttribute>().FirstOrDefault();

        }

        public static XMLOutputAttribute GetXMLOutputAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<XMLOutputAttribute>().FirstOrDefault();

        }
        
        public static WidgetColumnAttribute[] GetWidgetColumnAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<WidgetColumnAttribute>().ToArray();

        }
        
        public static ComputeAttribute[] GetComputeAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<ComputeAttribute>().ToArray();

        }
        public static ButtonButtonColumnAttribute[] GetButtonButtonColumnAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<ButtonButtonColumnAttribute>().OrderBy(x => x.Button).ToArray();

        }

        public static HTMLContentAttribute GetHTMLContentAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<HTMLContentAttribute>().FirstOrDefault();
        }

        public static BackColorAttribute GetBackColorAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<BackColorAttribute>().FirstOrDefault();
        }

        public static TextExpressionAttribute GetTextExpressionAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<TextExpressionAttribute>().FirstOrDefault();
        }

        public static ProtectExpressionAttribute GetProtectExpressionAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<ProtectExpressionAttribute>().FirstOrDefault();
        }

        public static SPGridColumnAttribute GetSPGridColumnAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<SPGridColumnAttribute>().FirstOrDefault();
        }

        public static CustomColumnAttribute GetCustomColumnAttribute(PropertyDescriptor property)
        {
            if (property == null)
            {
                return null;
            }
            return property.Attributes.OfType<CustomColumnAttribute>().FirstOrDefault();
        }
    }
}
