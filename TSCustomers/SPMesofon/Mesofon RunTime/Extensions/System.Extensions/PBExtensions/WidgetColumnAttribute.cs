﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Extensions
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class WidgetColumnAttribute : Attribute
    {
        public WidgetColumnAttribute(Type ElementType,params object[] Properties)
        {
            this.ElementType = ElementType;

            if(Properties.Length % 2 != 0)
            {
                throw new Exception("GalilCS - Invalid amout of properties");
            }
            this.Properties = new Dictionary<string, object>();
            for (int i = 0; i < Properties.Length; i=i+2)
            {
                this.Properties.Add(Convert.ToString(Properties[i]), Properties[i + 1]);
            }
        }

        public Type ElementType { get; set; }
        public Dictionary<string, object> Properties{ get; set; }


        public override object TypeId
        {
            get
            {
                return this;
            }
        }
    }
}
