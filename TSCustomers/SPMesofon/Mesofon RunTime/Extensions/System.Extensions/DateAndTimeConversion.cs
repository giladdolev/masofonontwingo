﻿namespace System.Extensions
{
    using System;
    using CompilerServices;
    using VBExtensions;
    using Globalization;

    /// <summary>
    /// Class DateAndTime
    /// </summary>
    [StandardModule]
    public static class DateAndTimeConversion
    {
 

        public static readonly DateTime Zero = new DateTime(1899, 12, 30);

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance. Use VB6 formatting rules
        /// </summary>
        /// <param name="dtDateTime">The date time.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public static string ToString(DateTime dtDateTime)
        {
            DateTimeFormatInfo formatInfo = DateTimeFormatInfo.CurrentInfo;

            string format;
            if (dtDateTime.Date == Zero)
            {
                format = formatInfo.LongTimePattern;
            }
            else if (dtDateTime.TimeOfDay.Ticks == 0L)
            {
                format = formatInfo.ShortDatePattern;
            }
            else
                format = formatInfo.ShortDatePattern + " " + formatInfo.LongTimePattern;

            return dtDateTime.ToString(format, CultureInfo.CurrentCulture);
        }



        public static DateTime CVDate(int i)
        {
            if (i == 0)
            {
                return new DateTime(1, 1, 1, 0, 0, 0);
            }

            else
            {
                var referenceDate = new DateTime(1899, 12, 30);

                return referenceDate.AddDays(i);
            }
        }
        /// <summary>
        /// Converts the object to dateTime
        /// </summary>
        /// <param name="dateString">Object value</param>
        /// <returns>DateTime value</returns>
        public static DateTime ConvertToDate(object dateString)
        {
            return DateType.FromObject(dateString);
        }

        public static DateTime ConvertToDate(bool value)
        {
            return ConvertToDate(value ? -1 : 0);
        }

        public static DateTime ConvertToDate(double value)
        {
            TimeSpan span;

            if (value < 0)
            {
                double wholePart = Math.Ceiling(value),
                    fraction = wholePart - value;//Negate fraction part

                span = TimeSpan.FromDays(wholePart) + TimeSpan.FromDays(fraction);
            }
            else
                span = TimeSpan.FromDays(value);

            //round seconds
            span = TimeSpan.FromSeconds(Math.Round(span.TotalSeconds, MidpointRounding.AwayFromZero));

            return Zero + span;
        }

        public static DateTime ConvertToDate(int value)
        {
            return ConvertToDate((double)value);
        }

        public static int ConvertDateToInt(DateTime inputDate)
        {
            var referenceDate = new DateTime(1979, 12, 31);

            return Convert.ToInt32(inputDate.Subtract(referenceDate).TotalDays);
        }

        public static DateTime DateValue(string value)
        {
            return DateAndTime.DateValue(value);
        }
        /// <summary>
        /// Returns or sets a Date value containing the current time of day according to your system.
        /// </summary>
        /// <value>
        /// The time of day.
        /// </value>
        public static DateTime TimeOfDay
        {
            get
            {
                TimeSpan timeOfDay = DateTime.Now.TimeOfDay;
                return Zero + timeOfDay;
            }
            set
            {
                //Set the current time of day. 
                Utils.SetTime(value);
            }
        }
    }
}