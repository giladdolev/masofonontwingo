﻿namespace System.Extensions
{
    /// <summary>
    /// Represent type of errors that can occur
    /// </summary>
    internal enum Errors
    {
        /// <summary>
        /// Cannot allocate error.
        /// </summary>
        CArrCantAlloc = 0x155,

        /// <summary>
        /// Illegal index error.
        /// </summary>
        CArrIllegalIndex = 340,

        /// <summary>
        /// Static control error.
        /// </summary>
        CArrLdStaticControl = 0x16a,

        /// <summary>
        /// Object already loaded error.
        /// </summary>
        CArrObjectAlreadyLoaded = 360,

        /// <summary>
        /// Object not array error.
        /// </summary>
        CArrObjectNotArray = 0x157,

        /// <summary>
        /// Device unavailable error.
        /// </summary>
        DevUnavailable = 0x44,

        /// <summary>
        /// File not found error.
        /// </summary>
        FileNotFound = 0x35,

        /// <summary>
        /// Illegal function call error.
        /// </summary>
        IllegalFuncCall = 5,

        /// <summary>
        /// Invalid picture type error.
        /// </summary>
        InvalidPictureType = 0x1e5,

        /// <summary>
        /// Invalid property error.
        /// </summary>
        InvalidPropertyArrayIndex = 0x17d,

        /// <summary>
        /// Out of bounds error.
        /// </summary>
        OutOfBounds = 9,

        /// <summary>
        /// Out of memory error.
        /// </summary>
        OutOfMemory = 7,

        /// <summary>
        /// Path not found error.
        /// </summary>
        PathNotFound = 0x4c,

        /// <summary>
        /// Property illegal value error.
        /// </summary>
        PropIllegalValue = 380,

        /// <summary>
        /// Resource ID not found error.
        /// </summary>
        ResourceIDNotFound = 0x146,

        /// <summary>
        /// Type mismatch error.
        /// </summary>
        TypeMismatch = 13
    } 

}
