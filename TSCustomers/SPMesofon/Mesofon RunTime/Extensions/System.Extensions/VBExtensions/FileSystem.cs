using System.Security;
using System.Text;
using System.Reflection;
using System.Globalization;
using System.Security.Permissions;
using System.IO;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Extensions.VBEnums;
using System.Extensions.CompilerServices;

namespace System.Extensions.VBExtensions
{
	/// <summary>
	/// FileSystem class
	/// </summary>
	[SecurityCriticalAttribute()]
	public sealed class FileSystem
	{
		private FileSystem()
		{

		}

		// Fields
		private const int A_ALLBITS = 0x3f;
		private const int A_ARCH = 0x20;
		private const int A_HIDDEN = 2;
		private const int A_NORMAL = 0;
		private const int A_RDONLY = 1;
		private const int A_SUBDIR = 0x10;
		private const int A_SYSTEM = 4;
		private const int A_VOLID = 8;
		private const int ERROR_ACCESS_DENIED = 5;
		private const int ERROR_ALREADY_EXISTS = 0xb7;
		private const int ERROR_BAD_NETPATH = 0x35;
		private const int ERROR_FILE_EXISTS = 80;
		private const int ERROR_FILE_NOT_FOUND = 2;
		private const int ERROR_INVALID_ACCESS = 12;
		private const int ERROR_INVALID_PARAMETER = 0x57;
		private const int ERROR_NOT_SAME_DEVICE = 0x11;
		private const int ERROR_WRITE_PROTECT = 0x13;
		internal const int FIRST_LOCAL_CHANNEL = 1;
		internal const int LAST_LOCAL_CHANNEL = 0xff;
		internal const int MAX_PATH = 260;
		internal const string sDateFormat = "d";
		internal const string sDateTimeFormat = "F";
		internal const string sTimeFormat = "T";

		// Methods

		/// <summary>
		/// Initializes the <see cref="FileSystem"/> class.
		/// </summary>
		static FileSystem()
		{
			NumberFormatInfo info2 = new NumberFormatInfo();
			info2.CurrencyDecimalDigits = 4;
			info2.CurrencyGroupSeparator = "";
			info2.CurrencySymbol = "";
			info2.NumberGroupSeparator = "";
			DateTimeFormatInfo info = new DateTimeFormatInfo();
			info.DateSeparator = "-";
			info.ShortDatePattern = "\\#yyyy-MM-dd\\#";
			info.LongTimePattern = "\\#HH:mm:ss\\#";
			info.FullDateTimePattern = "\\#yyyy-MM-dd HH:mm:ss\\#";
		}

        /// <summary>
        /// Reads data from an open disk file into a variable. The My feature gives you better productivity and performance in file I/O operations than FileGet. For more information, see <see cref="T:Microsoft.VisualBasic.FileIO.FileSystem"/>.
        /// </summary>
        /// <param name="FileNumber">Required. Any valid file number.</param><param name="Value">Required. Valid variable name into which data is read.</param><param name="RecordNumber">Optional. Record number (Random mode files) or byte number (Binary mode files) at which reading starts.</param><exception cref="T:System.ArgumentException"><paramref name="RecordNumber"/> &lt; 1 and not equal to -1.</exception><exception cref="T:System.IO.IOException"><paramref name="FileNumber"/> does not exist.</exception><exception cref="T:System.IO.IOException">File mode is invalid.</exception><filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>

        public static void FileGet(int FileNumber, ref ValueType Value, long RecordNumber = -1L)
        {
            try
            {
                ValidateGetPutRecordNumber(RecordNumber);
                GetStream(Assembly.GetCallingAssembly(), FileNumber).Get(ref Value, RecordNumber);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

		/// <summary>
		/// Adds the file to the list of open files.
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="oFile">VB6File object.</param>
		private static void AddFileToList(AssemblyData oAssemblyData, int fileNumber, VB6File oFile)
		{
			if (oFile == null)
			{
				throw ExceptionUtils.VbMakeException(0x33);
			}
			oFile.OpenFile();
			oAssemblyData.SetChannelObj(fileNumber, oFile);
		}

		/// <summary>
		/// Changes the current directory or folder.
		/// </summary>
		/// <param name="path">String that identifies which directory or folder becomes the new default directory or folder.</param>
		public static void ChDir(string path)
		{
			path = StringsEx.RTrim(path);
			if ((path == null) || (path.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty")), 0x34);
			}
			if (StringType.StrCmp(path, "\\", false) == 0)
			{
				path = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory());
			}
			try
			{
				Directory.SetCurrentDirectory(path);
			}
			catch (FileNotFoundException)
			{
				throw ExceptionUtils.VbMakeException(new FileNotFoundException(Utils.GetResourceString("FileSystem_PathNotFound1", path)), 0x4c);
			}
		}

		/// <summary>
		/// Changes the current drive.
		/// </summary>
		/// <param name="drive">String that specifies an existing drive.</param>
		public static void ChDrive(char drive)
		{
			drive = char.ToUpper(drive, CultureInfo.InvariantCulture);
			if ((drive < 'A') || (drive > 'Z'))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Drive"));
			}
			if (!UnsafeValidDrive(drive))
			{
				throw ExceptionUtils.VbMakeException(new IOException(Utils.GetResourceString("FileSystem_DriveNotFound1", StringType.FromChar(drive))), 0x44);
			}
			Directory.SetCurrentDirectory(StringType.FromChar(drive) + StringType.FromChar(Path.VolumeSeparatorChar));
		}

		/// <summary>
		/// Changes the current drive.
		/// </summary>
		/// <param name="drive">String that specifies an existing drive.</param>
		public static void ChDrive(string drive)
		{
			if ((drive != null) && (drive.Length != 0))
			{
				ChDrive(drive[0]);
			}
		}

		/// <summary>
		/// Indicates how to open a file when calling file-access functions.
		/// </summary>
		public enum OpenMode
		{

			/// <summary>
			/// File opened to append to it.
			/// </summary>
			Append,

			/// <summary>
			/// File opened for binary access.
			/// </summary>
			Binary,

			/// <summary>
			/// File opened for read access.
			/// </summary>
			Input,

			/// <summary>
			/// File opened for write access.
			/// </summary>
			Output,

			/// <summary>
			/// File opened for random access.
			/// </summary>
			Random
		}

		/// <summary>
		/// Indicates how to open a file when calling file-access functions.
		/// </summary>
		public enum OpenShare
		{

			/// <summary>
			/// LockReadWrite. This is the default.
			/// </summary>
			Default,

			/// <summary>
			/// Other processes cannot read the file.
			/// </summary>
			LockRead,

			/// <summary>
			/// Other processes cannot read or write to the file.
			/// </summary>
			LockReadWrite,

			/// <summary>
			/// Other processes cannot write to the file.
			/// </summary>
			LockWrite,

			/// <summary>
			/// Any process can read or write to the file.
			/// </summary>
			Shared
		}

		/// <summary>
		/// Checks if the file is open.
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData object.</param>
		/// <param name="sPath">Required. String value.</param>
		/// <param name="newFileMode">Required. OpenModeTypes value.</param>
		/// <returns>Boolean value.</returns>
		static internal bool CheckFileOpen(AssemblyData oAssemblyData, string sPath, OpenModeTypes newFileMode)
		{
			sPath = sPath.ToUpper(CultureInfo.InvariantCulture);
			int num3 = 0xff;
			for (int i = 1; i <= num3; i++)
			{
				VB6File channelOrNull = GetChannelOrNull(oAssemblyData, i);
				if (channelOrNull != null)
				{
					OpenMode mode = (OpenMode)channelOrNull.GetMode();
					if (string.CompareOrdinal(sPath.ToUpperInvariant(), channelOrNull.GetAbsolutePath().ToUpperInvariant()) == 0)
					{
						if (newFileMode == OpenModeTypes.Any)
						{
							return true;
						}
						if (((newFileMode | ((OpenModeTypes)((int)mode))) != OpenModeTypes.Input) && ((((newFileMode | ((OpenModeTypes)((int)mode))) | OpenModeTypes.Binary) | OpenModeTypes.Random) != (OpenModeTypes.Binary | OpenModeTypes.Random)))
						{
							return true;
						}
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Throws exception if VB6File.CanInput() returns false
		/// </summary>
		/// <param name="oFile">Required. VB6File object</param>
		private static void CheckInputCapable(VB6File oFile)
		{
			if (!oFile.CanInput())
			{
				throw ExceptionUtils.VbMakeException(0x36);
			}
		}




		/// <summary>
		/// Closes all of the files.
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData object.</param>
		static internal void CloseAllFiles(AssemblyData oAssemblyData)
		{
			int fileNumber = 1;
			do
			{
				InternalCloseFile(oAssemblyData, fileNumber);
				fileNumber++;
			}
			while (fileNumber <= 0xff);
		}

		/// <summary>
		/// Closes all of the files.
		/// </summary>
		/// <param name="assem">Assembly object.</param>
		static internal void CloseAllFiles(Assembly assem)
		{
			CloseAllFiles(ProjectData.GetProjectData().GetAssemblyData(assem));
		}

		/// <summary>
		/// Returns a string representing the current path.
		/// </summary>
		/// <returns>String representing the current path</returns>
		public static string CurDir()
		{
			return Directory.GetCurrentDirectory();
		}

		/// <summary>
		/// Returns a string representing the current path.
		/// </summary>
		/// <param name="drive">Char expression that specifies an existing drive.</param>
		/// <returns>String representing the current path</returns>
		public static string CurDir(char drive)
		{
			drive = char.ToUpper(drive, CultureInfo.InvariantCulture);
			if ((drive < 'A') || (drive > 'Z'))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Drive")), 0x44);
			}
			string fullPath = Path.GetFullPath(StringType.FromChar(drive) + StringType.FromChar(Path.VolumeSeparatorChar) + ".");
			if (!UnsafeValidDrive(drive))
			{
				throw ExceptionUtils.VbMakeException(new IOException(Utils.GetResourceString("FileSystem_DriveNotFound1", StringType.FromChar(drive))), 0x44);
			}
			return fullPath;
		}

		/// <summary>
		/// A string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive.
		/// </summary>
		/// <returns>String value.</returns>
		public static string Dir()
		{
			return IOUtils.FindNextFile(Assembly.GetCallingAssembly());
		}

		/// <summary>
		/// Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. 
		/// </summary>
		/// <param name="pathName">Required. String expression that specifies a file name, directory or folder name, or drive volume label.</param>
		/// <returns>A string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive.</returns>
		public static string Dir(string pathName)
		{
			return Dir(pathName, FileAttribute.Normal);
		}

		/// <summary>
		/// Returns a string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive. 
		/// </summary>
		/// <param name="pathName">Required. String expression that specifies a file name, directory or folder name, or drive volume label.</param>
		/// <param name="attributes">Optional. Enumeration or numeric expression whose value specifies file attributes. </param>
		/// <returns>A string representing the name of a file, directory, or folder that matches a specified pattern or file attribute, or the volume label of a drive.</returns>
		public static string Dir(string pathName, FileAttribute attributes)
		{
			if (String.IsNullOrEmpty(pathName))
			{
                throw new ArgumentNullException("pathName");
			}
			
			if (attributes == FileAttribute.Volume)
			{
				string pathRoot = default(string);
				StringBuilder lpVolumeNameBuffer = new StringBuilder(0x100);
				if (pathName.Length > 0)
				{
					pathRoot = Path.GetPathRoot(pathName);
				}
				int lpVolumeSerialNumber = 0;
				int lpMaximumComponentLength = 0;
				int lpFileSystemFlags = 0;
				if (NativeMethods.GetVolumeInformation(pathRoot, lpVolumeNameBuffer, 0x100, ref lpVolumeSerialNumber, ref lpMaximumComponentLength, ref lpFileSystemFlags, IntPtr.Zero, 0) != 0)
				{
					return lpVolumeNameBuffer.ToString();
				}
				return "";
			}
			FileAttributes _attributes = ((FileAttributes)attributes) | FileAttributes.Normal;
            return IOUtils.FindFirstFile(Assembly.GetCallingAssembly(), pathName, _attributes);
		}

		/// <summary>
		/// Returns a Boolean value True when the end of a file opened for Random or sequential Input has been reached.
		/// </summary>
		/// <param name="fileNumber">Required. An Integer containing any valid file number.</param>
		/// <returns>Boolean value.</returns>
		public static bool EOF(int fileNumber)
		{
			return GetStream(Assembly.GetCallingAssembly(), fileNumber).EOF();
		}

		/// <summary>
		/// Returns an enumeration representing the file mode for files opened using the FileOpen function.
		/// </summary>
		/// <param name="fileNumber">Required. Integer. Any valid file number.</param>
		/// <returns>An enumeration representing the file mode for files opened using the FileOpen function.</returns>
		public static OpenMode FileAttr(int fileNumber)
		{
			return (OpenMode)GetStream(Assembly.GetCallingAssembly(), fileNumber).GetMode();
		}

		/// <summary>
		/// Concludes input/output (I/O) to a file opened using the FileOpen function.
		/// </summary>
		/// <param name="fileNumbers">Parameter array of 0 or more channels to be closed.</param>
		public static void FileClose(params int[] fileNumbers)
		{
			try
			{
				Assembly callingAssembly = Assembly.GetCallingAssembly();
				AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(callingAssembly);
				if ((fileNumbers == null) || (fileNumbers.Length == 0))
				{
					CloseAllFiles(assemblyData);
				}
				else
				{
					int upperBound = fileNumbers.GetUpperBound(0);
					for (int i = 0; i <= upperBound; i++)
					{
						InternalCloseFile(assemblyData, fileNumbers[i]);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Copies a file.
		/// </summary>
		/// <param name="source">String that specifies the name of the file to be copied.</param>
		/// <param name="destination">String that specifies the target file name.</param>
		public static void FileCopy(string source, string destination)
		{
			if ((source == null) || (source.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty1", "Source")), 0x34);
			}
			if ((destination == null) || (destination.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty1", "Destination")), 0x34);
			}
			if (PathContainsWildcards(source))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Source")), 0x34);
			}
			if (PathContainsWildcards(destination))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Destination")), 0x34);
			}
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(Assembly.GetCallingAssembly());
			if (CheckFileOpen(assemblyData, destination, OpenModeTypes.Output))
			{
				throw ExceptionUtils.VbMakeException(new IOException(Utils.GetResourceString("FileSystem_FileAlreadyOpen1", destination)), 0x37);
			}
			if (CheckFileOpen(assemblyData, source, OpenModeTypes.Input))
			{
				throw ExceptionUtils.VbMakeException(new IOException(Utils.GetResourceString("FileSystem_FileAlreadyOpen1", source)), 0x37);
			}
			try
			{
				File.Copy(source, destination, true);
				File.SetAttributes(destination, FileAttributes.Archive);
			}
			catch (FileNotFoundException exception)
			{
				throw ExceptionUtils.VbMakeException(exception, 0x35);
			}
			catch (IOException exception2)
			{
				throw ExceptionUtils.VbMakeException(exception2, 0x37);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Returns a Date value that indicates the date and time a file was created or last modified.
		/// </summary>
		/// <param name="pathName">Required. String expression that specifies a file name. PathName may include the directory or folder, and the drive.</param>
		/// <returns>Date value that indicates the date and time a file was created or last modified.</returns>
		public static DateTime FileDateTime(string pathName)
		{
			if (PathContainsWildcards(pathName))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "PathName")), 0x34);
			}
			if (!File.Exists(pathName))
			{
				throw new FileNotFoundException(Utils.GetResourceString("FileSystem_FileNotFound1", pathName));
			}
			return new FileInfo(pathName).LastWriteTime;
		}

		/// <summary>
		/// Specifying the length of a file in bytes.
		/// </summary>
		/// <param name="pathName">String that specifies a file.</param>
		/// <returns>Returns a long value specifying the length of a file in bytes.</returns>
		public static long FileLen(string pathName)
		{
			if (!File.Exists(pathName))
			{
				throw new FileNotFoundException(Utils.GetResourceString("FileSystem_FileNotFound1", pathName));
			}
			return new FileInfo(pathName).Length;
		}

		/// <summary>
		/// Indicates how to open a file when calling file-access functions.
		/// </summary>
		public enum OpenAccess
		{

			/// <summary>
			/// Read and write access permitted. This is default.
			/// </summary>
			Default,

			/// <summary>
			/// Read access permitted.
			/// </summary>
			Read,

			/// <summary>
			/// Read and write access permitted.
			/// </summary>
			ReadWrite,

			/// <summary>
			/// Write access permitted.
			/// </summary>
			Write = 2
		}

		/// <summary>
		/// Opens a file for input or output.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="fileName">String that specifies a file name</param>
		/// <param name="mode">Enumeration specifying the file mode.</param>
		public static void FileOpen(int fileNumber, string fileName, OpenMode mode)
		{
			FileOpen(fileNumber, fileName, mode, OpenAccess.Default, OpenShare.Default, -1);
		}

		/// <summary>
		/// Opens a file for input or output.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="fileName">String that specifies a file name</param>
		/// <param name="mode">Enumeration specifying the file mode.</param>
		/// <param name="access"> Enumeration specifying the operations permitted on the open file.</param>
		public static void FileOpen(int fileNumber, string fileName, OpenMode mode, OpenAccess access)
		{
			FileOpen(fileNumber, fileName, mode, access, OpenShare.Default, -1);
		}

		/// <summary>
		/// Opens a file for input or output.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="fileName">String that specifies a file name</param>
		/// <param name="mode">Enumeration specifying the file mode.</param>
		/// <param name="access"> Enumeration specifying the operations permitted on the open file.</param>
		/// <param name="share">Enumeration specifying the operations not permitted on the open file by other processes.</param>
		public static void FileOpen(int fileNumber, string fileName, OpenMode mode, OpenAccess access, OpenShare share)
		{
			FileOpen(fileNumber, fileName, mode, access, share, -1);
		}

		/// <summary>
		/// Opens a file for input or output.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="fileName">String that specifies a file name</param>
		/// <param name="mode">Enumeration specifying the file mode.</param>
		/// <param name="access"> Enumeration specifying the operations permitted on the open file.</param>
		/// <param name="share">Enumeration specifying the operations not permitted on the open file by other processes.</param>
		/// <param name="recordLength">Number less than or equal to 32,767 (bytes).</param>
		public static void FileOpen(int fileNumber, string fileName, OpenMode mode, OpenAccess access, OpenShare share, int recordLength)
		{
			try
			{
				ValidateMode(mode);
				ValidateAccess(access);
				ValidateShare(share);
				if ((fileNumber < 1) || (fileNumber > 0xff))
				{
					throw ExceptionUtils.VbMakeException(0x34);
				}
				vbIOOpenFile(Assembly.GetCallingAssembly(), fileNumber, fileName, mode, access, share, recordLength);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, bool value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, bool value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, byte value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, byte value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, char value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, char value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, DateTime value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, DateTime value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, decimal value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, decimal value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, double value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, double value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, short value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, short value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, int value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, int value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Indicates how to open a file when calling file-access functions.
		/// </summary>
		internal enum OpenModeTypes
		{

			/// <summary>
			/// Any
			/// </summary>
			Any,

			/// <summary>
			/// File opened to append to it.
			/// </summary>
			Append,

			/// <summary>
			/// File opened for binary access.
			/// </summary>
			Binary,

			/// <summary>
			/// File opened for read access.
			/// </summary>
			Input,

			/// <summary>
			/// File opened for write access.
			/// </summary>
			Output,

			/// <summary>
			/// File opened for random access.
			/// </summary>
			Random
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, long value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		public static void FilePut(int fileNumber, long value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, float value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		public static void FilePut(int fileNumber, float value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number (Random mode files) or byte number (Binary mode files) at which writing begins.</param>
		public static void FilePut(int fileNumber, ValueType value)
		{
			FilePut(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		public static void FilePut(int fileNumber, ValueType value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="RecordNumber">Record number or byte number at which writing begins.</param>
		[ObsoleteAttribute("Use FilePutObject to write Object types, or coerce FileNumber and RecordNumber to Integer for writing non-Object types")]
		public static void FilePut(object fileNumber, object value)
		{
			throw new ArgumentException(Utils.GetResourceString("UseFilePutObject"));
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		[ObsoleteAttribute("Use FilePutObject to write Object types, or coerce FileNumber and RecordNumber to Integer for writing non-Object types")]
		public static void FilePut(object fileNumber, object value, object recordNumber)
		{
			throw new ArgumentException(Utils.GetResourceString("UseFilePutObject"));
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		public static void FilePut(int fileNumber, string value)
		{
			FilePut(fileNumber, value, -1L, false);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		public static void FilePut(int fileNumber, string value, long recordNumber)
		{
			FilePut(fileNumber, value, recordNumber, false);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		/// <param name="stringIsFixedLength">Applies only when writing a string. Specifies whether to write a two-byte string length descriptor for the string to the file.</param>
		public static void FilePut(int fileNumber, string value, long recordNumber, bool stringIsFixedLength)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber, stringIsFixedLength);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		public static void FilePut(int fileNumber, Array value)
		{
			FilePut(fileNumber, value, -1L, false, false);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		public static void FilePut(int fileNumber, Array value, long recordNumber)
		{
			FilePut(fileNumber, value, recordNumber, false, false);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		/// <param name="arrayIsDynamic"> Applies only when writing an array. Specifies whether the array is to be treated as dynamic, and whether to write an array descriptor for the string describing the length.</param>
		public static void FilePut(int fileNumber, Array value, long recordNumber, bool arrayIsDynamic)
		{
			FilePut(fileNumber, value, recordNumber, arrayIsDynamic, false);
		}

		/// <summary>
		/// Writes data from a variable to a disk file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="value">Valid variable name containing data written to disk.</param>
		/// <param name="recordNumber">Record number or byte number at which writing begins.</param>
		/// <param name="arrayIsDynamic"> Applies only when writing an array. Specifies whether the array is to be treated as dynamic, and whether to write an array descriptor for the string describing the length.</param>
		/// <param name="stringIsFixedLength">Applies only when writing a string. Specifies whether to write a two-byte string length descriptor for the string to the file.</param>
		public static void FilePut(int fileNumber, Array value, long recordNumber, bool arrayIsDynamic, bool stringIsFixedLength)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).Put(value, recordNumber, arrayIsDynamic, stringIsFixedLength);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data from a variable to a disk file. 
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="value">Required. Valid variable name that contains data written to disk.</param>
		public static void FilePutObject(int fileNumber, object value)
		{
			FilePutObject(fileNumber, value, -1L);
		}

		/// <summary>
		/// Writes data from a variable to a disk file. 
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="value">Required. Valid variable name that contains data written to disk.</param>
		/// <param name="recordNumber">Optional. Record number (Random mode files) or byte number (Binary mode files) at which writing starts.</param>
		public static void FilePutObject(int fileNumber, object value, long recordNumber)
		{
			try
			{
				ValidateGetPutRecordNumber(recordNumber);
				GetStream(Assembly.GetCallingAssembly(), fileNumber, OpenModeTypes.Binary | OpenModeTypes.Random).PutObject(value, recordNumber, true);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Assigns an output line width to a file opened using the FileOpen function.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="recordWidth">Required. Numeric expression in the range 0–255, inclusive, which indicates how many characters appear on a line before a new line is started.</param>
		public static void FileWidth(int fileNumber, int recordWidth)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).SetWidth(recordWidth);
		}

		/// <summary>
		/// Returns an Integer value representing the next file number available for use by the FileOpen function.
		/// </summary>
		/// <returns>An Integer value representing the next file number available for use by the FileOpen function.</returns>
		public static int FreeFile()
		{
			Assembly callingAssembly = Assembly.GetCallingAssembly();
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(callingAssembly);
			int lChannel = 1;
			while (assemblyData.GetChannelObj(lChannel) != null)
			{
				lChannel++;
				if (lChannel > 0xff)
				{
					throw ExceptionUtils.VbMakeException(0x43);
				}
			}
			return lChannel;
		}

		/// <summary>
		/// Returns a FileAttribute value representing the attributes of a file, directory, or folder.
		/// </summary>
		/// <param name="pathName">String that specifies a file, directory, or folder name. </param>
		/// <returns>Returns a FileAttribute value representing the attributes of a file, directory, or folder.</returns>
		public static FileAttribute GetAttr(string pathName)
		{
			if (String.IsNullOrEmpty(pathName))
			{
                throw new ArgumentNullException("pathName");
			}
			
			char[] anyOf = new char[] {
				'*',
				'?',
			};
			if (pathName.IndexOfAny(anyOf) >= 0)
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			FileInfo info = new FileInfo(pathName);
			if (info.Exists)
			{
				return (((FileAttribute)info.Attributes) & (FileAttribute.Archive | FileAttribute.Directory | FileAttribute.Volume | FileAttribute.System | FileAttribute.Hidden | FileAttribute.ReadOnly));
			}
			DirectoryInfo info2 = new DirectoryInfo(pathName);
			if (info2.Exists)
			{
				return (((FileAttribute)info2.Attributes) & (FileAttribute.Archive | FileAttribute.Directory | FileAttribute.Volume | FileAttribute.System | FileAttribute.Hidden | FileAttribute.ReadOnly));
			}
			if (Path.GetFileName(pathName).Length == 0)
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			throw new FileNotFoundException(Utils.GetResourceString("FileSystem_FileNotFound1", pathName));
		}



		/// <summary>
		/// Gets the channel.
		/// </summary>
		/// <param name="oAssemblyData">Assembly object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <returns>VB6File object.</returns>
		static internal VB6File GetChannelObj(Assembly assem, int fileNumber)
		{
			VB6File channelOrNull = GetChannelOrNull(ProjectData.GetProjectData().GetAssemblyData(assem), fileNumber);
			if (channelOrNull == null)
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			return channelOrNull;
		}

		/// <summary>
		/// Gets the channel.
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <returns>Returns a VB6File object or null.</returns>
		private static VB6File GetChannelOrNull(AssemblyData oAssemblyData, int fileNumber)
		{
			return oAssemblyData.GetChannelObj(fileNumber);
		}

		/// <summary>
		/// Get the stream.
		/// </summary>
		/// <param name="assem">Assembly object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <returns>VB6File object.</returns>
		private static VB6File GetStream(Assembly assem, int fileNumber)
		{
			return GetStream(assem, fileNumber, OpenModeTypes.Binary | OpenModeTypes.Append | OpenModeTypes.Random | OpenModeTypes.Output | OpenModeTypes.Input);
		}

		/// <summary>
		/// Get the stream.
		/// </summary>
		/// <param name="assem">Assembly object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="mode">OpenModeTypes enum.</param>
		/// <returns>VB6File object.</returns>
		private static VB6File GetStream(Assembly assem, int fileNumber, OpenModeTypes mode)
		{
			if ((fileNumber < 1) || (fileNumber > 0xff))
			{
				throw ExceptionUtils.VbMakeException(0x34);
			}
			VB6File channelObj = GetChannelObj(assem, fileNumber);
			if ((OpenModeTypesFromOpenMode((OpenMode)channelObj.GetMode()) | mode) == ~OpenModeTypes.Any)
			{
				channelObj = null;
				throw ExceptionUtils.VbMakeException(0x36);
			}
			return channelObj;
		}

		/// <summary>
		/// Returns String value containing characters from a file opened in Input or Binary mode.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="charCount">Required. Any valid numeric expression specifying the number of characters to read.</param>
		/// <returns>String value.</returns>
		public static string InputString(int fileNumber, int charCount)
		{
			string str;
			try
			{
				if ((charCount < 0) || (charCount > 1073741823.5))
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "CharCount"));
				}
				VB6File channelObj = GetChannelObj(Assembly.GetCallingAssembly(), fileNumber);
				channelObj.Lock();
				try
				{
					str = channelObj.InputString(charCount);
				}
				finally
				{
					channelObj.Unlock();
				}
			}
			catch (Exception)
			{
				throw;
			}
			return str;
		}

		/// <summary>
		/// Closing the file
		/// </summary>
		/// <param name="oAssemblyData">Required. AssemblyData object.</param>
		/// <param name="fileNumber">Required. Integer value.</param>
		private static void InternalCloseFile(AssemblyData oAssemblyData, int fileNumber)
		{
			if (fileNumber == 0)
			{
				CloseAllFiles(oAssemblyData);
			}
			else
			{
				VB6File channelOrNull = GetChannelOrNull(oAssemblyData, fileNumber);
				if (channelOrNull != null)
				{
					oAssemblyData.SetChannelObj(fileNumber, null);
					if (channelOrNull != null)
					{
						channelOrNull.CloseFile();
					}
				}
			}
		}

		/// <summary>
		/// Deletes files from a disk.
		/// </summary>
		/// <param name="pathName">Required. String expression that specifies one or more file names to be deleted.</param>
		public static void Kill(string pathName)
		{
			int num = 0;
			string fileName;
			string directoryName = Path.GetDirectoryName(pathName);
			if ((directoryName == null) || (directoryName.Length == 0))
			{
				directoryName = Environment.CurrentDirectory;
				fileName = pathName;
			}
			else
			{
				fileName = Path.GetFileName(pathName);
			}
			FileInfo[] files = new DirectoryInfo(directoryName).GetFiles(fileName);
			directoryName = directoryName + StringType.FromChar(Path.PathSeparator);
			if (files != null)
			{
				int upperBound = files.GetUpperBound(0);
				for (int i = 0; i <= upperBound; i++)
				{
					FileInfo info2 = files[i];
					if ((info2.Attributes & (FileAttributes.System | FileAttributes.Hidden)) == 0)
					{
						fileName = info2.FullName;
						if (CheckFileOpen(ProjectData.GetProjectData().GetAssemblyData(Assembly.GetCallingAssembly()), fileName, OpenModeTypes.Any))
						{
							throw ExceptionUtils.VbMakeException(new IOException(Utils.GetResourceString("FileSystem_FileAlreadyOpen1", fileName)), 0x37);
						}
						try
						{
							File.Delete(fileName);
							num++;
						}
						catch (IOException exception)
						{
							throw ExceptionUtils.VbMakeException(exception, 0x37);
						}
						catch (Exception)
						{
							throw;
						}
					}
				}
			}
			if (num == 0)
			{
				throw new FileNotFoundException(Utils.GetResourceString("KILL_NoFilesFound1", pathName));
			}
		}

		/// <summary>
		/// Reads a single line from an open sequential file and assigns it to a String variable.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <returns>Reads a single line from an open sequential file and assigns it to a String variable.</returns>
		public static string LineInput(int fileNumber)
		{
			VB6File stream = GetStream(Assembly.GetCallingAssembly(), fileNumber);
			CheckInputCapable(stream);
			if (stream.EOF())
			{
				throw ExceptionUtils.VbMakeException(0x3e);
			}
			return stream.LineInput();
		}

		/// <summary>
		/// Specifies the current read/write position in an open file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <returns>Returns a Long value that specifies the current read/write position in an open file.</returns>
		public static long Loc(int fileNumber)
		{
			return GetStream(Assembly.GetCallingAssembly(), fileNumber).LOC();
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function. 
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		public static void Lock(int fileNumber)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Lock();
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function. 
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="record">Required. Number of the only record to lock or unlock</param>
		public static void Lock(int fileNumber, long record)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Lock(record);
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="fromRecord">Required. Number of the first record to lock or unlock.</param>
		/// <param name="toRecord">Required. Number of the last record to lock or unlock.</param>
		public static void Lock(int fileNumber, long fromRecord, long toRecord)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Lock(fromRecord, toRecord);
		}

		/// <summary>
		/// Returns a Long representing the size, in bytes, of a file opened by using the FileOpen function. 
		/// </summary>
		/// <param name="fileNumber">Required. An Integer that contains a valid file number.</param>
		/// <returns>Returns a Long representing the size, in bytes, of a file opened by using the FileOpen function.</returns>
		public static long LOF(int fileNumber)
		{
			return GetStream(Assembly.GetCallingAssembly(), fileNumber).LOF();
		}

		/// <summary>
		/// Creates a new directory. 
		/// </summary>
		/// <param name="path">Required. String expression that identifies the directory to be created. </param>
		public static void MkDir(string path)
		{
			if ((path == null) || (path.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty")), 0x34);
			}
			if (Directory.Exists(path))
			{
				throw ExceptionUtils.VbMakeException(0x4b);
			}
			Directory.CreateDirectory(path);
		}

		/// <summary>
		/// Returns OpenModeTypes, that indicates how to open a file when calling file-access functions.
		/// </summary>
		/// <param name="om">OpenMode object.</param>
		/// <returns>Returns OpenModeTypes object.</returns>
		private static OpenModeTypes OpenModeTypesFromOpenMode(OpenMode om)
		{
			if (om == OpenMode.Input)
			{
				return OpenModeTypes.Input;
			}
			if (om == OpenMode.Output)
			{
				return OpenModeTypes.Output;
			}
			if (om == OpenMode.Append)
			{
				return OpenModeTypes.Append;
			}
			if (om == OpenMode.Binary)
			{
				return OpenModeTypes.Binary;
			}
			if (om == OpenMode.Random)
			{
				return OpenModeTypes.Random;
			}
			if (om != ((OpenMode)(-1)))
			{
				throw new ArgumentException("The OpenMode parameter value is unknown", "om");
			}
			return OpenModeTypes.Any;
		}

		/// <summary>
		/// Checks if the path contains wildcards
		/// </summary>
		/// <param name="path">String that represents the path.</param>
		/// <returns>True if the path contains wildcards or False if it doesn't contain.</returns>
		private static bool PathContainsWildcards(string path)
		{
			if (path == null)
			{
				return false;
			}
			return ((path.IndexOf('*') != -1) || (path.IndexOf('?') != -1));
		}

		/// <summary>
		/// Writes display-formatted data to a sequential file.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="output">Optional. Zero or more comma-delimited expressions to write to a file.</param>
		public static void Print(int fileNumber, params object[] output)
		{
			try
			{
				GetStream(Assembly.GetCallingAssembly(), fileNumber).Print(output);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes display-formatted data to a sequential file.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="output">Required. Zero or more comma-delimited expressions to write to a file.</param>
		public static void PrintLine(int fileNumber, params object[] output)
		{
			try
			{
				GetStream(Assembly.GetCallingAssembly(), fileNumber).PrintLine(output);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Renames a disk file or directory.
		/// </summary>
		/// <param name="oldPath">String that specifies the existing file name and location. </param>
		/// <param name="newPath">String that specifies the new file name and location. </param>
		public static void Rename(string oldPath, string newPath)
		{
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(Assembly.GetCallingAssembly());
			oldPath = VB6CheckPathname(assemblyData, oldPath, (OpenMode)(-1));
			newPath = VB6CheckPathname(assemblyData, newPath, (OpenMode)(-1));
			new FileIOPermission(FileIOPermissionAccess.Write | FileIOPermissionAccess.Read, oldPath).Demand();
			new FileIOPermission(FileIOPermissionAccess.Write, newPath).Demand();
			if (NativeMethods.MoveFile(oldPath, newPath) == 0)
			{
				switch (Marshal.GetLastWin32Error()) {
					case 2:
						throw ExceptionUtils.VbMakeException(0x35);

					case 80:
					case 0xb7:
						throw ExceptionUtils.VbMakeException(0x3a);

					case 12:
						throw ExceptionUtils.VbMakeException(0x4b);

					case 0x11:
						throw ExceptionUtils.VbMakeException(0x4a);
				}
				throw ExceptionUtils.VbMakeException(5);
			}
		}

		/// <summary>
		/// Closes all disk files opened.
		/// </summary>
		public static void Reset()
		{
			CloseAllFiles(Assembly.GetCallingAssembly());
		}

		/// <summary>
		/// Removes an existing directory. 
		/// </summary>
		/// <param name="path">Required. String expression that identifies the directory or folder to be removed.</param>
		public static void RmDir(string path)
		{
			if ((path == null) || (path.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty")), 0x34);
			}
			try
			{
				Directory.Delete(path);
			}
			catch (DirectoryNotFoundException exception)
			{
				throw ExceptionUtils.VbMakeException(exception, 0x4c);
			}
			catch (Exception exception2)
			{
				throw ExceptionUtils.VbMakeException(exception2, 0x4b);
			}
		}

		/// <summary>
		/// Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function.
		/// </summary>
		/// <param name="fileNumber">Required. An Integer that contains a valid file number.</param>
		/// <returns>Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function. </returns>
		public static long Seek(int fileNumber)
		{
			return GetStream(Assembly.GetCallingAssembly(), fileNumber).Seek();
		}

		/// <summary>
		/// Returns a Long specifying the current read/write position in a file opened by using the FileOpen function, or sets the position for the next read/write operation in a file opened by using the FileOpen function.
		/// </summary>
		/// <param name="fileNumber">Required. An Integer that contains a valid file number.</param>
		/// <param name="position">Required. Number in the range 1–2,147,483,647, inclusive, that indicates where the next read/write operation should occur.</param>
		public static void Seek(int fileNumber, long position)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Seek(position);
		}

		/// <summary>
		/// Sets attribute information for a file.
		/// </summary>
		/// <param name="pathName">Required. String expression that specifies a file name. PathName can include directory or folder, and drive.</param>
		/// <param name="attributes">Required. Constant or numeric expression, whose sum specifies file attributes.</param>
		public static void SetAttr(string pathName, FileAttribute attributes)
		{
			if ((pathName == null) || (pathName.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_PathNullOrEmpty")), 0x34);
			}
			Assembly callingAssembly = Assembly.GetCallingAssembly();
			VB6CheckPathname(ProjectData.GetProjectData().GetAssemblyData(callingAssembly), pathName, OpenMode.Input);
			if ((attributes | (FileAttribute.Archive | FileAttribute.System | FileAttribute.Hidden | FileAttribute.ReadOnly)) != (FileAttribute.Archive | FileAttribute.System | FileAttribute.Hidden | FileAttribute.ReadOnly))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Attributes"));
			}
			FileAttributes fileAttributes = (FileAttributes)attributes;
			File.SetAttributes(pathName, fileAttributes);
		}





		/// <summary>
		/// Used with the Print or PrintLine function to position output.
		/// </summary>
		/// <param name="count">The number of spaces to insert before displaying or printing the next expression in a list.</param>
		/// <returns>SpcInfo structure.</returns>
		public static SpcInfo SPC(short count)
		{
			SpcInfo info = new SpcInfo();
			if (count < 1)
			{
				count = 0;
			}
			info.Count = count;
			return info;
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		public static void Unlock(int fileNumber)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Unlock();
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="record">Required. Number of the only record to lock or unlock</param>
		public static void Unlock(int fileNumber, long record)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Unlock(record);
		}

		/// <summary>
		/// Controls access by other processes to all or part of a file opened by using the Open function.
		/// </summary>
		/// <param name="fileNumber">Required. Any valid file number.</param>
		/// <param name="fromRecord">Required. Number of the first record to lock or unlock.</param>
		/// <param name="toRecord">Required. Number of the last record to lock or unlock.</param>
		public static void Unlock(int fileNumber, long fromRecord, long toRecord)
		{
			GetStream(Assembly.GetCallingAssembly(), fileNumber).Unlock(fromRecord, toRecord);
		}

		/// <summary>
		/// Unsafe Valid Drive
		/// </summary>
		/// <param name="cDrive">Required. char value</param>
		/// <returns>Boolean value.</returns>
		private static bool UnsafeValidDrive(char cDrive)
		{
			int num = cDrive - 'A';
			return ((NativeMethods.GetLogicalDrives() & ((long)Math.Round(Math.Pow(2.0, (double)num)))) != 0L);
		}

		/// <summary>
		/// Validates the access.
		/// </summary>
		/// <param name="access">OpenAccess object.</param>
		private static void ValidateAccess(OpenAccess access)
		{
			if (((access != OpenAccess.Default) && (access != OpenAccess.Read)) && ((access != OpenAccess.ReadWrite) && (access != OpenAccess.Write)))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Access"));
			}
		}

		/// <summary>
		/// Validates the record number.
		/// </summary>
		/// <param name="recordNumber">A long value representing the record number.</param>
		private static void ValidateGetPutRecordNumber(long recordNumber)
		{
			if ((recordNumber < 1L) && (recordNumber != -1L))
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "RecordNumber")), 0x3f);
			}
		}

		/// <summary>
		/// Validates the OpenMode.
		/// </summary>
		/// <param name="mode">OpenMode object.</param>
		private static void ValidateMode(OpenMode mode)
		{
			if ((((mode != OpenMode.Input) && (mode != OpenMode.Output)) && ((mode != OpenMode.Random) && (mode != OpenMode.Append))) && (mode != OpenMode.Binary))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Mode"));
			}
		}

		/// <summary>
		/// Validates the OpenShare.
		/// </summary>
		/// <param name="share">OpenShare object.</param>
		private static void ValidateShare(OpenShare share)
		{
			if ((((share != OpenShare.Default) && (share != OpenShare.Shared)) && ((share != OpenShare.LockRead) && (share != OpenShare.LockReadWrite))) && (share != OpenShare.LockWrite))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Share"));
			}
		}

		/// <summary>
		/// Checks the path name.
		/// </summary>
		/// <param name="oAssemblyData">AssemblyData object.</param>
		/// <param name="sPath">String that identifies the directory or folder.</param>
		/// <param name="mode">OpenMode enum, that indicates how to open a file</param>
		/// <returns>A string representing the path.</returns>
		static internal string VB6CheckPathname(AssemblyData oAssemblyData, string sPath, OpenMode mode)
		{
			if ((sPath.IndexOf("?", StringComparison.Ordinal) != -1) || (sPath.IndexOf("*", StringComparison.Ordinal) != -1))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidPathChars1", sPath));
			}
			string fullName = new FileInfo(sPath).FullName;
			if (CheckFileOpen(oAssemblyData, fullName, OpenModeTypesFromOpenMode(mode)))
			{
				throw ExceptionUtils.VbMakeException(0x37);
			}
			return fullName;
		}

		/// <summary>
		///  Opens a file for input or output.
		/// </summary>
		/// <param name="assem">Assembly object.</param>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="fileName">String that specifies a file name.</param>
		/// <param name="mode">Enumeration specifying the file mode.</param>
		/// <param name="access">Enumeration specifying the operations permitted on the open file.</param>
		/// <param name="share">Enumeration specifying the operations not permitted on the open file by other processes.</param>
		/// <param name="recordLength">Number less than or equal to 32,767 (bytes).</param>
		private static void vbIOOpenFile(Assembly assem, int fileNumber, string fileName, OpenMode mode, OpenAccess access, OpenShare share, int recordLength)
		{
			VB6File file;
			AssemblyData assemblyData = ProjectData.GetProjectData().GetAssemblyData(assem);
			if (GetChannelOrNull(assemblyData, fileNumber) != null)
			{
				throw ExceptionUtils.VbMakeException(0x37);
			}
			if ((fileName == null) || (fileName.Length == 0))
			{
				throw ExceptionUtils.VbMakeException(0x4b);
			}
			fileName = new FileInfo(fileName).FullName;
			if (CheckFileOpen(assemblyData, fileName, OpenModeTypesFromOpenMode(mode)))
			{
				throw ExceptionUtils.VbMakeException(0x37);
			}
			if ((recordLength != -1) && (recordLength <= 0))
			{
				throw ExceptionUtils.VbMakeException(5);
			}
			if (mode == OpenMode.Binary)
			{
				recordLength = 1;
			}
			else if (recordLength == -1)
			{
				if (mode == OpenMode.Random)
				{
					recordLength = 0x80;
				}
				else
				{
					recordLength = 0x200;
				}
			}
			if (share == OpenShare.Default)
			{
				share = OpenShare.LockReadWrite;
			}
			OpenMode openMode = mode;
            switch (openMode)
            {
				case OpenMode.Input:
					if ((access != OpenAccess.Read) && (access != OpenAccess.Default))
					{
						throw new ArgumentException(Utils.GetResourceString("FileSystem_IllegalInputAccess"));
					}
					file = new VB6InputFile(fileName, share);
					break;

				case OpenMode.Output:
					if ((access != OpenAccess.Write) && (access != OpenAccess.Default))
					{
						throw new ArgumentException(Utils.GetResourceString("FileSystem_IllegalOutputAccess"));
					}
					file = new VB6OutputFile(fileName, share, false);
					break;

				case OpenMode.Random:
					if (access == OpenAccess.Default)
					{
						access = OpenAccess.ReadWrite;
					}
					file = new VB6RandomFile(fileName, access, share, recordLength);
					break;

				case OpenMode.Append:
					if (((access != OpenAccess.Write) && (access != OpenAccess.ReadWrite)) && (access != OpenAccess.Default))
					{
						throw new ArgumentException(Utils.GetResourceString("FileSystem_IllegalAppendAccess"));
					}
					file = new VB6OutputFile(fileName, share, true);
					break;

				default:
					if (mode != OpenMode.Binary)
					{
						throw ExceptionUtils.VbMakeException(0x33);
					}
					if (access == OpenAccess.Default)
					{
						access = OpenAccess.ReadWrite;
					}
					file = new VB6BinaryFile(fileName, access, share);
					break;
			}
			AddFileToList(assemblyData, fileNumber, file);
		}

		/// <summary>
		/// Writes data to a sequential file.
		/// </summary>
		/// <param name="fileNumber">Any valid file number.</param>
		/// <param name="output">One or more comma-delimited expressions to write to a file.</param>
		public static void Write(int fileNumber, params object[] output)
		{
			try
			{
				GetStream(Assembly.GetCallingAssembly(), fileNumber).WriteHelper(output);
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Writes data to a sequential file. Data written with Write is usually read from a file by using Input.
		/// </summary>
		/// <param name="fileNumber">Required. An Integer expression that contains any valid file number.</param>
		/// <param name="output">Required. One or more comma-delimited expressions to write to a file.</param>
		public static void WriteLine(int fileNumber, params object[] output)
		{
			try
			{
				GetStream(Assembly.GetCallingAssembly(), fileNumber).WriteLineHelper(output);
			}
			catch (Exception)
			{
				throw;
			}
		}

		// Nested Types
		/// <summary>
		/// VB File Type enumeration
		/// </summary>
		internal enum vbFileType
		{

			/// <summary>
			/// Print file.
			/// </summary>
			vbPrintFile,

			/// <summary>
			/// Write file.
			/// </summary>
			vbWriteFile
		}
	}

	/// <summary>
	/// Supports the Visual Basic Print and PrintLine functions.
	/// </summary>
	[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public struct SpcInfo
	{
		public short Count
		{
			get;
			set;
		}


		public override int GetHashCode()
		{
			return Count;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SpcInfo))
				return false;
			
			return Equals((SpcInfo)obj);
		}

		public bool Equals(SpcInfo other)
		{
			if (Count != other.Count)
				return false;
			else
				return true;

		}

		public static bool operator ==(SpcInfo spcInfo1, SpcInfo spcInfo2)
		{
			return spcInfo1.Equals(spcInfo2);
		}

		public static bool operator !=(SpcInfo spcInfo1, SpcInfo spcInfo2)
		{
			return !spcInfo1.Equals(spcInfo2);
		}
	}

	/// <summary>
	/// Supports the Visual Basic Print and PrintLine functions.
	/// </summary>
	[StructLayoutAttribute(LayoutKind.Sequential), EditorBrowsableAttribute(EditorBrowsableState.Never)]
	public struct TabInfo
	{
		public short Column
		{
			get;
			set;
		}

		public override int GetHashCode()
		{
			return Column;
		}


		public override bool Equals(object obj)
		{
			if (!(obj is TabInfo))
				return false;
			
			return Equals((TabInfo)obj);
		}

		public bool Equals(TabInfo other)
		{
			if (Column != other.Column)
				return false;
			else
				return true;

		}

		public static bool operator ==(TabInfo tabInfo1, TabInfo tabInfo2)
		{
			return tabInfo1.Equals(tabInfo2);
		}

		public static bool operator !=(TabInfo tabInfo1, TabInfo tabInfo2)
		{
			return !tabInfo1.Equals(tabInfo2);
		}
	}

}
