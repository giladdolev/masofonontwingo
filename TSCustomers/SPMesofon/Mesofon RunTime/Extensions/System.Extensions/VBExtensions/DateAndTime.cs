namespace System.Extensions.VBExtensions
{
	using System;
	using CompilerServices;
	using VBEnums;
	using Globalization;
	using Runtime.InteropServices;
	using Security.Permissions;
	using Threading;

	/// <summary>
	/// Class DateAndTime
	/// </summary>
	[StandardModuleAttribute()]
	public static class DateAndTime
	{


		#region Properties

		/// <summary>
		/// Represents an instant in time, typically expressed as a date and time of day.
		/// </summary>
		public static DateTime Now
		{
			get { return DateTime.Now; }
		}

		/// <summary>
		/// Returns a Single representing the number of seconds elapsed since midnight.
		/// </summary>
		public static double Timer
		{
			get { return (((DateTime.Now.Ticks % 0xc92a69c000L)) / 10000000.0); }
		}

		/// <summary>
		/// Gets the current date.
		/// </summary>
		public static DateTime Today
		{
			get { return DateTime.Today; }
			set { Utils.SetDate(value); }
		}



		/// <summary>
		/// Returns or sets a String value representing the current date according to your system.
		/// </summary>
		/// <value>
		/// The date string.
		/// </value>
		/// <exception cref="System.InvalidCastException"></exception>
		public static string DateString
		{
			get
			{
				//check the Culture type
				if (IsDBCSCulture())
				{
					//Format todays date
					return DateTime.Today.ToString("yyyy\\-MM\\-dd", Utils.InvariantCultureInfo);
				}
				//Format todays date
				return DateTime.Today.ToString("MM\\-dd\\-yyyy", Utils.InvariantCultureInfo);
			}
			[HostProtectionAttribute(SecurityAction.LinkDemand, Resources = HostProtectionResource.ExternalProcessMgmt)]
			set
			{
				DateTime time;
				try
				{
					string s = StringType.ToHalfwidthNumbers(value);
					//check the Culture type
					if (IsDBCSCulture())
					{
						// set as "yyyy\\-MM\\-dd" format
						time = DateTime.ParseExact(s, AcceptedDateFormatsDBCS, Utils.InvariantCultureInfo, DateTimeStyles.AllowWhiteSpaces);
					}

					else
					{
						// set as "MM\\-dd\\-yyyy" format
						time = DateTime.ParseExact(s, AcceptedDateFormatsSBCS, Utils.InvariantCultureInfo, DateTimeStyles.AllowWhiteSpaces);
					}
				}
				catch (StackOverflowException)
				{
					throw;
				}
				catch (OutOfMemoryException)
				{
					throw;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception)
				{
					//throw invalid cast fromS sring exception
					throw ExceptionUtils.VbMakeException(new InvalidCastException(Utils.GetResourceString("InvalidCast_FromStringTo", StringsEx.Left(value, 0x20), "Date")), 5);
				}
				//set the new date
				Utils.SetDate(time);
			}
		}

		/// <summary>
		/// Determines whether is DBCS culture.
		/// </summary>
		/// <returns>Returns true if is DBCS culture </returns>
		private static bool IsDBCSCulture()
		{
			//Represents the maximum size of a double byte character set (DBCS) size, in bytes, for the current operating system. 
			if (Marshal.SystemMaxDBCSCharSize == 1)
			{
				return false;
			}
			return true;
		}


		#endregion Properties

		/// <summary>
		/// String array that keeps date formatSBCS
		/// </summary>
		static internal readonly string[] AcceptedDateFormatsSBCS;
		/// <summary>
		/// String array that keeps date formatDBCS
		/// </summary>
		static internal readonly string[] AcceptedDateFormatsDBCS;
		public static readonly DateTime Zero = new DateTime(1899, 12, 30);
		/// <summary>
		/// Arrays of types of date formating
		/// </summary>
		static DateAndTime()
		{
			AcceptedDateFormatsSBCS = new[] {
				"M-d-yyyy",
				"M-d-y",
				"M/d/yyyy",
				"M/d/y",
			};
			AcceptedDateFormatsDBCS = new[] {
				"yyyy-M-d",
				"y-M-d",
				"yyyy/M/d",
				"y/M/d",
			};
		}

		/// <summary>
		/// Returns the calendar of the current culture
		/// </summary>
		private static Calendar CurrentCalendar
		{
			get { return Thread.CurrentThread.CurrentCulture.Calendar; }
		}

		/// <summary>
		/// Returns a Date value containing a date and time value to which a specified time interval has been added.
		/// </summary>
		/// <param name="interval">Required. String value.</param>
		/// <param name="number">Required. Double. Floating-point expression representing the number of intervals you want to add. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past). Any fractional part of Number is ignored.</param>
		/// <param name="dateValue">Required. Date. An expression representing the date and time to which the interval is to be added. DateValue itself is not changed in the calling program.</param>
		/// <returns>DateTime</returns>
		public static DateTime DateAdd(string interval, double number, object dateValue)
		{
			DateTime time2;
			try
			{
				time2 = DateType.FromObject(dateValue);
			}
			catch (Exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("Argument_InvalidDateValue1", "DateValue"));
			}
			return DateAdd(DateIntervalFromString(interval), number, time2);
		}

		/// <summary>
		/// Returns a Date value containing a date and time value to which a specified time interval has been added.
		/// </summary>
		/// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to add.</param>
		/// <param name="number">Required. Double. Floating-point expression representing the number of intervals you want to add. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past). Any fractional part of Number is ignored.</param>
		/// <param name="dateValue">Required. Date. An expression representing the date and time to which the interval is to be added. DateValue itself is not changed in the calling program.</param>
		/// <returns>DateTime</returns>
		public static DateTime DateAdd(DateInterval interval, double number, DateTime dateValue)
		{
			var years = (int)Math.Round(ConversionEx.Fix(number));
			switch (interval) {
				case DateInterval.Year:
					return CurrentCalendar.AddYears(dateValue, years);
				case DateInterval.Quarter:
					return dateValue.AddMonths(years * 3);
				case DateInterval.Month:
					return CurrentCalendar.AddMonths(dateValue, years);
				case DateInterval.DayOfYear:
				case DateInterval.Day:
				case DateInterval.Weekday:
					return dateValue.AddDays(years);
				case DateInterval.WeekOfYear:
					return dateValue.AddDays(years * 7.0);
				case DateInterval.Hour:
					return dateValue.AddHours(years);
				case DateInterval.Minute:
					return dateValue.AddMinutes(years);
				case DateInterval.Second:
					return dateValue.AddSeconds(years);
			}
			throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Interval"));
		}

		/// <summary>
		/// Returns an Integer value containing a number representing the day of the week.
		/// </summary>
		/// <param name="dateValue">Required. Date value for which you want to determine the day of the week.</param>
		/// <returns>Integer</returns>
		public static int Weekday(DateTime dateValue)
		{
			return Weekday(dateValue, FirstDayOfWeek.Sunday);
		}

		/// <summary>
		/// Returns an Integer value containing a number representing the day of the week.
		/// </summary>
		/// <param name="dateValue">Required. Date value for which you want to determine the day of the week.</param>
		/// <param name="dayOfWeek">Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week. If not specified, FirstDayOfWeek.Sunday is used.</param>
		/// <returns>Integer</returns>
		public static int Weekday(DateTime dateValue, FirstDayOfWeek dayOfWeek)
		{
			if (dayOfWeek == FirstDayOfWeek.System)
			{
				dayOfWeek = (FirstDayOfWeek)(DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek + 1);
			}
			else if ((dayOfWeek < FirstDayOfWeek.Sunday) || (dayOfWeek > FirstDayOfWeek.Saturday))
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "DayOfWeek"));
			}
			int num = ((int)CurrentCalendar.GetDayOfWeek(dateValue)) + 1;
			return ((((num - (int)dayOfWeek) + 7) % 7) + 1);
		}

		/// <summary>
		/// Returns a Variant (Integer) containing the specified part of a given date.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="dateValue">The date value.</param>
		/// <returns>int</returns>
		/// <exception cref="System.InvalidCastException"></exception>
		public static int DatePart(string interval, object dateValue)
		{
			return DatePart(interval, dateValue, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
		}

		/// <summary>
		/// Returns a Variant (Integer) containing the specified part of a given date.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="dateValue">The date value.</param>
		/// <param name="dayOfWeek">The day of week.</param>
		/// <returns>int</returns>
		/// <exception cref="System.InvalidCastException"></exception>
		public static int DatePart(string interval, object dateValue, FirstDayOfWeek dayOfWeek)
		{
			return DatePart(interval, dateValue, dayOfWeek, FirstWeekOfYear.Jan1);
		}

		/// <summary>
		/// Returns a Variant (Integer) containing the specified part of a given date.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="dateValue">The date value.</param>
		/// <param name="dayOfWeek">The day of week.</param>
		/// <param name="weekOfYear">The week of year.</param>
		/// <returns>int</returns>
		/// <exception cref="System.InvalidCastException"></exception>
		public static int DatePart(string interval, object dateValue, FirstDayOfWeek dayOfWeek, FirstWeekOfYear weekOfYear)
		{
			DateTime time;
			try
			{
				//convert the object value to time
				time = DateType.FromObject(dateValue);
			}
			catch (Exception)
			{
				throw new InvalidCastException(Utils.GetResourceString("Argument_InvalidDateValue1", "DateValue"));
			}
			return DatePart(DateIntervalFromString(interval), time, dayOfWeek, weekOfYear);
		}


		/// <summary>
		/// Returns a Variant (Integer) containing the specified part of a given date.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="dateValue">The date value.</param>
		/// <param name="firstDayOfWeekValue">The first day of week value.</param>
		/// <param name="firstWeekOfYearValue">The first week of year value.</param>
		/// <returns>Integer value.</returns>
		/// <exception cref="System.ArgumentException"></exception>
		public static int DatePart(DateInterval interval, DateTime dateValue, FirstDayOfWeek firstDayOfWeekValue, FirstWeekOfYear firstWeekOfYearValue)
		{
			DayOfWeek firstDayOfWeek;
			CalendarWeekRule calendarWeekRule = default(CalendarWeekRule);
			
			//look for the date part
			switch (interval) {
				case DateInterval.Year:
					//get the year
					return CurrentCalendar.GetYear(dateValue);
				case DateInterval.Quarter:
					//get the quarter
					return (((dateValue.Month - 1) / 3) + 1);
				case DateInterval.Month:
					//get the month
					return CurrentCalendar.GetMonth(dateValue);
				case DateInterval.DayOfYear:
					//get the day number of the all year
					return CurrentCalendar.GetDayOfYear(dateValue);
				case DateInterval.Day:
					//get the day number of the all month
					return CurrentCalendar.GetDayOfMonth(dateValue);
				case DateInterval.WeekOfYear:
					if (firstDayOfWeekValue != FirstDayOfWeek.System)
					{
						firstDayOfWeek = (DayOfWeek)(firstDayOfWeekValue - 1);
						break;
					}
					
					//look for the first day of week according to system week rule
					firstDayOfWeek = Utils.CultureInfo.DateTimeFormat.FirstDayOfWeek;
					break;
				case DateInterval.Weekday:
					//get the day number of the week
					return Weekday(dateValue, firstDayOfWeekValue);
				case DateInterval.Hour:
					//get the Hour
					return CurrentCalendar.GetHour(dateValue);
				case DateInterval.Minute:
					//get the Minute
					return CurrentCalendar.GetMinute(dateValue);
				case DateInterval.Second:
					//get the Second
					return CurrentCalendar.GetSecond(dateValue);
				default:
					
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Interval"));
			}
			switch (firstWeekOfYearValue) {
				case FirstWeekOfYear.System:
					//look for a week of the year according to system week rule
					calendarWeekRule = Utils.CultureInfo.DateTimeFormat.CalendarWeekRule;
					break;
				case FirstWeekOfYear.Jan1:
					//look for the first week of the year
					calendarWeekRule = CalendarWeekRule.FirstDay;
					break;
				case FirstWeekOfYear.FirstFourDays:
					//look for the first week of the year that has only four days
					calendarWeekRule = CalendarWeekRule.FirstFourDayWeek;
					break;
				case FirstWeekOfYear.FirstFullWeek:
					//look for the first full week of the year
					calendarWeekRule = CalendarWeekRule.FirstFullWeek;
					break;
			}
			
			//getting specified week of the yead
			return CurrentCalendar.GetWeekOfYear(dateValue, calendarWeekRule, firstDayOfWeek);
		}

		public static DateTime CVDate(int i)
		{
			if (i == 0)
			{
				return new DateTime(1, 1, 1, 0, 0, 0);
			}

			else
			{
				var referenceDate = new DateTime(1899, 12, 30);
				
				return referenceDate.AddDays(i);
			}
		}
		/// <summary>
		/// Converts the object to dateTime
		/// </summary>
		/// <param name="dateString">Object value</param>
		/// <returns>DateTime value</returns>
		public static DateTime ConvertToDate(object dateString)
		{
			return DateType.FromObject(dateString);
		}

		public static DateTime ConvertToDate(bool value)
		{
			return ConvertToDate(value ? -1 : 0);
		}

		public static DateTime ConvertToDate(double value)
		{
			TimeSpan span;
			
			if (value < 0)
			{
				double wholePart = Math.Ceiling(value), fraction = wholePart - value; //Negate fraction part
				
				span = TimeSpan.FromDays(wholePart) + TimeSpan.FromDays(fraction);
			}
			else
				span = TimeSpan.FromDays(value);
			
			//round seconds
			span = TimeSpan.FromSeconds(Math.Round(span.TotalSeconds, MidpointRounding.AwayFromZero));
			
			return Zero + span;
		}

		public static DateTime ConvertToDate(int value)
		{
			return ConvertToDate((double)value);
		}

		public static int ConvertDateToInt(DateTime inputDate)
		{
			var referenceDate = new DateTime(1979, 12, 31);
			
			return Convert.ToInt32(inputDate.Subtract(referenceDate).TotalDays);
		}

		/// <summary>
		/// Returns a Long value specifying the number of time intervals between two Date values.
		/// </summary>
		/// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
		/// <param name="date1">Required. Date. The first date/time value you want to use in the calculation.</param>
		/// <param name="date2">Required. Date. The second date/time value you want to use in the calculation.</param>
		/// <returns>Long value.</returns>
		public static long DateDiff(string interval, object date1, object date2)
		{
			return DateDiff(interval, date1, date2, FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
		}

		/// <summary>
		/// Returns a Long value specifying the number of time intervals between two Date values.
		/// </summary>
		/// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
		/// <param name="date1">Required. Date. The first date/time value you want to use in the calculation.</param>
		/// <param name="date2">Required. Date. The second date/time value you want to use in the calculation.</param>
		/// <param name="dayOfWeek">Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week.</param>
		/// <returns>Long value.</returns>
		public static long DateDiff(string interval, object date1, object date2, FirstDayOfWeek dayOfWeek)
		{
			return DateDiff(interval, date1, date2, dayOfWeek = FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1);
		}

		/// <summary>
		/// Returns a Long value specifying the number of time intervals between two Date values.
		/// </summary>
		/// <param name="interval">Required. DateInterval enumeration value or String expression representing the time interval you want to use as the unit of difference between Date1 and Date2.</param>
		/// <param name="date1">Required. Date. The first date/time value you want to use in the calculation.</param>
		/// <param name="date2">Required. Date. The second date/time value you want to use in the calculation.</param>
		/// <param name="dayOfWeek">Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week.</param>
		/// <param name="weekOfYear">Optional. A value chosen from the FirstWeekOfYear enumeration that specifies the first week of the year. </param>
		/// <returns>Long value.</returns>
		public static long DateDiff(string interval, object date1, object date2, FirstDayOfWeek dayOfWeek, FirstWeekOfYear weekOfYear)
		{
			
			if (!(date1 is DateTime))
			{
				throw new InvalidCastException(Utils.GetResourceString("Argument_InvalidDateValue1", "Date1"));
			}
			
			
			if (!(date2 is DateTime))
			{
				throw new InvalidCastException(Utils.GetResourceString("Argument_InvalidDateValue1", "Date2"));
			}
			return DateDiff(DateIntervalFromString(interval), DateType.FromObject(date1), DateType.FromObject(date2), dayOfWeek, weekOfYear);
		}

		/// <summary>
		/// Creates date interval from given string
		/// </summary>
		/// <param name="interval">String that represents the date interval</param>
		/// <returns>DateInterval object</returns>
		private static DateInterval DateIntervalFromString(string interval)
		{
			if (interval != null)
			{
				interval = interval.ToUpperInvariant();
			}
			string sLeft = interval;
			if (StringType.StrCmp(sLeft, "YYYY", false) == 0)
			{
				return DateInterval.Year;
			}
			if (StringType.StrCmp(sLeft, "Y", false) == 0)
			{
				return DateInterval.DayOfYear;
			}
			if (StringType.StrCmp(sLeft, "M", false) == 0)
			{
				return DateInterval.Month;
			}
			if (StringType.StrCmp(sLeft, "D", false) == 0)
			{
				return DateInterval.Day;
			}
			if (StringType.StrCmp(sLeft, "H", false) == 0)
			{
				return DateInterval.Hour;
			}
			if (StringType.StrCmp(sLeft, "N", false) == 0)
			{
				return DateInterval.Minute;
			}
			if (StringType.StrCmp(sLeft, "S", false) == 0)
			{
				return DateInterval.Second;
			}
			if (StringType.StrCmp(sLeft, "WW", false) == 0)
			{
				return DateInterval.WeekOfYear;
			}
			if (StringType.StrCmp(sLeft, "W", false) == 0)
			{
				return DateInterval.Weekday;
			}
			if (StringType.StrCmp(sLeft, "Q", false) != 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Interval"));
			}
			return DateInterval.Quarter;
		}

		/// <summary>
		/// Returns a Variant (Long) specifying the number of time intervals between two specified dates.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="date1">The date1.</param>
		/// <param name="date2">The date2.</param>
		/// <returns>long</returns>
		/// <exception cref="System.ArgumentException"></exception>
		public static long DateDiff(DateInterval interval, DateTime date1, DateTime date2)
		{
			return DateDiff(interval, date1, date2, FirstDayOfWeek.Sunday);
		}

		/// <summary>
		/// Returns a Variant (Long) specifying the number of time intervals between two specified dates.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="date1">The date1.</param>
		/// <param name="date2">The date2.</param>
		/// <param name="dayOfWeek">The day of week.</param>
		/// <returns>long</returns>
		/// <exception cref="System.ArgumentException"></exception>
		public static long DateDiff(DateInterval interval, DateTime date1, DateTime date2, FirstDayOfWeek dayOfWeek)
		{
			return DateDiff(interval, date1, date2, dayOfWeek);
		}

		/// <summary>
		/// Returns a Variant (Long) specifying the number of time intervals between two specified dates.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <param name="date1">The date1.</param>
		/// <param name="date2">The date2.</param>
		/// <param name="dayOfWeek">The day of week.</param>
		/// <param name="weekOfYear">The week of year.</param>
		/// <returns>long</returns>
		/// <exception cref="System.ArgumentException"></exception>
		public static long DateDiff(DateInterval interval, DateTime date1, DateTime date2, FirstDayOfWeek dayOfWeek, FirstWeekOfYear weekOfYear)
		{
			Calendar currentCalendar = default(Calendar);
			
			//Subtract the value betwwen the two days
			TimeSpan span = date2.Subtract(date1);
			switch (interval) {
				case DateInterval.Year:
					currentCalendar = CurrentCalendar;
					//returns the year difference
					return (currentCalendar.GetYear(date2) - currentCalendar.GetYear(date1));
				case DateInterval.Quarter:
					//returns the quarter difference
					currentCalendar = CurrentCalendar;
					return ((((currentCalendar.GetYear(date2) - currentCalendar.GetYear(date1)) * 4) + ((currentCalendar.GetMonth(date2) - 1) / 3)) - ((currentCalendar.GetMonth(date1) - 1) / 3));
				case DateInterval.Month:
					//returns the month difference
					currentCalendar = CurrentCalendar;
					return ((((currentCalendar.GetYear(date2) - currentCalendar.GetYear(date1)) * 12) + currentCalendar.GetMonth(date2)) - currentCalendar.GetMonth(date1));
				case DateInterval.DayOfYear:
				case DateInterval.Day:
					//returns the day difference
					return (long)Math.Round(ConversionEx.Fix(span.TotalDays));
				case DateInterval.WeekOfYear:
					
					//set date1 to its first day of that week  
					date1 = date1.AddDays((0 - GetDayOfWeek(date1, dayOfWeek)));
					//set date2 to its first day of that week
					date2 = date2.AddDays((0 - GetDayOfWeek(date2, dayOfWeek)));
					//returns the week of year difference
					return (((long)Math.Round(ConversionEx.Fix(date2.Subtract(date1).TotalDays))) / 7L);
				case DateInterval.Weekday:
					//return the week day difference
					return (((long)Math.Round(ConversionEx.Fix(span.TotalDays))) / 7L);
				case DateInterval.Hour:
					//return the hour difference
					return (long)Math.Round(ConversionEx.Fix(span.TotalHours));
				case DateInterval.Minute:
					//return the minute difference
					return (long)Math.Round(ConversionEx.Fix(span.TotalMinutes));
				case DateInterval.Second:
					//return the second difference
					return (long)Math.Round(ConversionEx.Fix(span.TotalSeconds));
			}
			throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Interval"));
		}

		/// <summary>
		/// Gets the day of the week
		/// </summary>
		/// <param name="dt">DateTime</param>
		/// <param name="weekdayFirst">Enum indicating the first day of the week</param>
		/// <returns>The day of the week</returns>
		private static int GetDayOfWeek(DateTime dt, FirstDayOfWeek weekdayFirst)
		{
			if ((weekdayFirst < FirstDayOfWeek.System) || (weekdayFirst > FirstDayOfWeek.Saturday))
			{
				throw ExceptionUtils.VbMakeException(5);
			}
			if (weekdayFirst == FirstDayOfWeek.System)
			{
				weekdayFirst = (FirstDayOfWeek)(Utils.DateTimeFormatInfo.FirstDayOfWeek + 1);
			}
			return (((((dt.DayOfWeek - ((DayOfWeek)((int)weekdayFirst))) + 8) % (int)(DayOfWeek.Saturday | DayOfWeek.Monday))) + 1);
		}
		/// <summary>
		/// Gets the hour
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Hour(DateTime dateValue)
		{
			return CurrentCalendar.GetHour(dateValue);
		}
		/// <summary>
		/// Gets the minute
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Minute(DateTime dateValue)
		{
			return CurrentCalendar.GetMinute(dateValue);
		}
		/// <summary>
		/// Gets the second
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Second(DateTime dateValue)
		{
			return CurrentCalendar.GetSecond(dateValue);
		}
		/// <summary>
		/// Gets the month
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Month(DateTime dateValue)
		{
			return CurrentCalendar.GetMonth(dateValue);
		}

		/// <summary>
		/// Gets the year
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Year(DateTime dateValue)
		{
			return CurrentCalendar.GetYear(dateValue);
		}
		/// <summary>
		/// Gets the day of the week
		/// </summary>
		/// <param name="dateValue">DateTime value</param>
		/// <returns>Integer value.</returns>
		public static int Day(DateTime dateValue)
		{
			return CurrentCalendar.GetDayOfMonth(dateValue);
		}

		/// <summary>
		/// Returns a Date value containing the date information represented by a string, with the time information set to midnight (00:00:00).
		/// </summary>
		/// <param name="StringDate">Required. String expression representing a date/time value from 00:00:00 on January 1 of the year 1 through 23:59:59 on December 31, 9999.</param>
		/// <returns>DateTime</returns>
		public static DateTime DateValue(string value)
		{
			if (value == null)
				throw new InvalidCastException("Couldn't convert the specified string to date.");
			
			double numericValue;
			if (double.TryParse(value, out numericValue))
			{
				if (numericValue >= 0 && value.Contains("."))
				{
					string[] parts = value.Split('.');
					if (parts.Length == 2)
					{
						int hours = int.Parse(parts[0], CultureInfo.CurrentCulture), minutes = int.Parse(parts[1], CultureInfo.CurrentCulture);
						
						if (hours < 24 && minutes < 60)
							return Zero + TimeSpan.FromMinutes(hours * 60 + minutes);
					}
				}
				
				return ConvertToDate(numericValue);
			}
			else if (value.Contains("."))
			{
				string[] parts = value.Split('.');
				if (parts.Length == 3)
				{
					int hours, minutes, seconds;
					if (int.TryParse(parts[0], out hours) && int.TryParse(parts[1], out minutes) && int.TryParse(parts[2], out seconds))
					{
						if (hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60 && seconds >= 0 && seconds < 60)
							return Zero + TimeSpan.FromMinutes(hours * 60 + minutes) + TimeSpan.FromSeconds(seconds);
					}
				}
			}
			
			return DateType.FromString(value, CultureInfo.CurrentCulture).Date;
		}
		/// <summary>
		/// Returns a Date value representing a specified hour, minute, and second, with the date information set relative to January 1 of the year 1.
		/// </summary>
		/// <param name="hour">Required. Integer expression from 0 through 23. </param>
		/// <param name="minute">Required. Integer expression from 0 through 59.</param>
		/// <param name="second">Required. Integer expression from 0 through 59.</param>
		/// <returns>DateTime value.</returns>
		public static DateTime TimeSerial(int hour, int minute, int second)
		{
			int num = checked(hour * 60 * 60 + minute * 60 + second);
			if (num < 0)
				checked
				{
					num += 86400;
				}
			return new DateTime(checked((long)num * 10000000L));
		}

		/// <summary>
		/// Returns a String value containing the name of the specified weekday.
		/// </summary>
		/// <param name="weekday">Required. Integer. The numeric designation for the weekday, from 1 through 7</param>
		/// <returns>String value.</returns>
		public static string WeekdayName(int weekday)
		{
			return WeekdayName(weekday, false, FirstDayOfWeek.System);
		}

		/// <summary>
		/// Returns a String value containing the name of the specified weekday.
		/// </summary>
		/// <param name="weekday">Required. Integer. The numeric designation for the weekday, from 1 through 7</param>
		/// <param name="abbreviate">Optional. Boolean value that indicates if the weekday name is to be abbreviated. </param>
		/// <returns>String value.</returns>
		public static string WeekdayName(int weekday, bool abbreviate)
		{
			return WeekdayName(weekday, abbreviate, FirstDayOfWeek.System);
		}

		/// <summary>
		/// Returns a String value containing the name of the specified weekday.
		/// </summary>
		/// <param name="weekday">Required. Integer. The numeric designation for the weekday, from 1 through 7</param>
		/// <param name="abbreviate">Optional. Boolean value that indicates if the weekday name is to be abbreviated. </param>
		/// <param name="firstDayOfWeekValue">Optional. A value chosen from the FirstDayOfWeek enumeration that specifies the first day of the week.</param>
		/// <returns>String value.</returns>
		public static string WeekdayName(int weekday, bool abbreviate, FirstDayOfWeek firstDayOfWeekValue)
		{
			if (weekday < 1 || weekday > 7)
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Weekday"));

			else if (firstDayOfWeekValue < FirstDayOfWeek.System || firstDayOfWeekValue > FirstDayOfWeek.Saturday)
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "FirstDayOfWeekValue"));
			else
			{
				DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)Utils.CultureInfo.GetFormat(typeof(DateTimeFormatInfo));
				if (firstDayOfWeekValue == FirstDayOfWeek.System)
					firstDayOfWeekValue = (FirstDayOfWeek)(dateTimeFormatInfo.FirstDayOfWeek + 1);
				string str;
				try
				{
					str = !abbreviate ? dateTimeFormatInfo.GetDayName((DayOfWeek)((int)(weekday + firstDayOfWeekValue - 2) % 7)) : dateTimeFormatInfo.GetAbbreviatedDayName((DayOfWeek)((int)(weekday + firstDayOfWeekValue - 2) % 7));
				}
				catch (StackOverflowException)
				{
					throw;
				}
				catch (OutOfMemoryException)
				{
					throw;
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception)/* ex */
				{
					throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Weekday"));
				}
				if (str.Length != 0)
					return str;
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Weekday"));
			}
		}
	}
}
