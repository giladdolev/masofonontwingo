using System.Reflection;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Extensions.CompilerServices;

namespace System.Extensions.VBExtensions
{
	/// <summary>
	/// Sealed Class Information
	/// </summary>
	[StandardModuleAttribute()]
	public sealed class Information
	{
		private Information()
		{

		}

		/// <summary>
		/// Provides access to the type information for an object's coclass entry in its type library.
		/// </summary>
		[ComImportAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never), GuidAttribute("B196B283-BAB4-101A-B69C-00AA00341D07"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IProvideClassInfo
		{
			[return: MarshalAsAttribute(UnmanagedType.Interface)]
			ITypeInfo GetClassInfo();
		}

		/// <summary>
		/// Exposes objects, methods and properties to programming tools and other applications that support Automation.
		/// </summary>
		[ComImportAttribute(), EditorBrowsableAttribute(EditorBrowsableState.Never), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020400-0000-0000-C000-000000000046")]
		private interface IDispatch
		{
			[PreserveSigAttribute()]
			int GetTypeInfoCount();
			[PreserveSigAttribute()]
			int GetTypeInfo([InAttribute()]int index, [InAttribute()]int lcid, [MarshalAsAttribute(UnmanagedType.Interface)]out ITypeInfo pTypeInfo);
			[PreserveSigAttribute()]
			int GetIDsOfNames();
			[PreserveSigAttribute()]
			int Invoke();
		}

		/// <summary>
		/// This section describes ITypeInfo, an interface typically used for reading information about objects.
		/// </summary>
		[ComImportAttribute(), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown), GuidAttribute("00020401-0000-0000-C000-000000000046"), EditorBrowsableAttribute(EditorBrowsableState.Never)]
		private interface ITypeInfo
		{
			[PreserveSigAttribute()]
			int GetTypeAttr(out IntPtr pTypeAttr);
			//[PreserveSig]
			//int GetTypeComp(out UnsafeNativeMethods.ITypeComp pTComp);
			[PreserveSigAttribute()]
			int GetFuncDesc([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out IntPtr pFuncDesc);
			[PreserveSigAttribute()]
			int GetVarDesc([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out IntPtr pVarDesc);
			[PreserveSigAttribute()]
			int GetNames([InAttribute()]int memid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.LPArray)]string[] rgBstrNames, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int cMaxNames, [MarshalAsAttribute(UnmanagedType.U4)]out int cNames);
			[PreserveSigAttribute()]
			int GetRefTypeOfImplType([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, out int pRefType);
			[PreserveSigAttribute()]
			int GetImplTypeFlags([InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int index, [OutAttribute()]int pImplTypeFlags);
			[PreserveSigAttribute()]
			int GetIDsOfNames([InAttribute()]IntPtr rgszNames, [InAttribute(), MarshalAsAttribute(UnmanagedType.U4)]int cNames, out IntPtr pMemId);
			[PreserveSigAttribute()]
			int Invoke();
			[PreserveSigAttribute()]
			int GetDocumentation([InAttribute()]int memid, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrName, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrDocString, [MarshalAsAttribute(UnmanagedType.U4)]out int pdwHelpContext, [MarshalAsAttribute(UnmanagedType.BStr)]out string pBstrHelpFile);
			//[PreserveSig]
			//int GetDllEntry([In] int memid, [In] UnsafeNativeMethods.tagINVOKEKIND invkind, [Out, MarshalAs(UnmanagedType.BStr)] string pBstrDllName, [Out, MarshalAs(UnmanagedType.BStr)] string pBstrName, [Out, MarshalAs(UnmanagedType.U2)] short pwOrdinal);
			[PreserveSigAttribute()]
			int GetRefTypeInfo([InAttribute()]IntPtr hreftype, out ITypeInfo pTypeInfo);
			[PreserveSigAttribute()]
			int AddressOfMember();
			[PreserveSigAttribute()]
			int CreateInstance([InAttribute()]ref IntPtr pUnkOuter, [InAttribute()]ref Guid riid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.IUnknown)]object ppvObj);
			[PreserveSigAttribute()]
			int GetMops([InAttribute()]int memid, [OutAttribute(), MarshalAsAttribute(UnmanagedType.BStr)]string pBstrMops);
			//[PreserveSig]
			//int GetContainingTypeLib([Out, MarshalAs(UnmanagedType.LPArray)] UnsafeNativeMethods.ITypeLib[] ppTLib, [Out, MarshalAs(UnmanagedType.LPArray)] int[] pIndex);
			[PreserveSigAttribute()]
			void ReleaseTypeAttr(IntPtr typeAttr);
			[PreserveSigAttribute()]
			void ReleaseFuncDesc(IntPtr funcDesc);
			[PreserveSigAttribute()]
			void ReleaseVarDesc(IntPtr varDesc);
		}








		// Fields
		private static readonly int[] _qBColorTable = new int[] {
			0,
			0x800000,
			0x8000,
			0x808000,
			0x80,
			0x800080,
			0x8080,
			0xc0c0c0,
			0x808080,
			0xff0000,
			0xff00,
			0xffff00,
			0xff,
			0xff00ff,
			0xffff,
			0xffffff,
		};

		[EditorBrowsableAttribute(EditorBrowsableState.Never)]
		public static int Erl()
		{
			return ProjectData.GetProjectData()._err.Erl;
		}

		/// <summary>
		/// Contains information about run-time errors.
		/// </summary>
		/// <returns>ErrObject</returns>
		public static ErrObject Err()
		{
			ProjectData projectData = ProjectData.GetProjectData();
            if (projectData._err == null)
			{
                projectData._err = new ErrObject();
			}
            return projectData._err;
		}

		/// <summary>
		/// Determinates if an object is array
		/// </summary>
		/// <param name="varName">The object.</param>
		/// <returns>True if object is array</returns>
		public static bool IsArray(object varName)
		{
			if (varName == null)
			{
				return false;
			}
			return (varName is Array);
		}

		/// <summary>
		/// Returns a Boolean value that indicates whether an object is DateTime object.
		/// </summary>
		/// <param name="expression">The object</param>
		/// <returns>True if object is DateTime object</returns>
		public static bool IsDate(object expression)
		{
			if (expression != null)
			{
				if (expression is DateTime)
				{
					return true;
				}
				string strExpression = expression.ToString();
				if (String.IsNullOrEmpty(strExpression))
				{
					return false;
				}
				DateTime result;
				return DateTime.TryParse(strExpression, out result);
			}
			return false;
		}

		/// <summary>
		/// Returns a Boolean value that indicates whether an expression evaluates to the System.DBNull class.
		/// </summary>
		/// <param name="expression">Required. Object expression.</param>
		/// <returns>Boolean value</returns>
		public static bool IsDBNull(object expression)
		{
			if (expression == null)
			{
				return false;
			}
			return (expression is DBNull);
		}

		/// <summary>
		/// Determinates if an object is exception
		/// </summary>
		/// <param name="expression">The object.</param>
		/// <returns>True if the object is exception</returns>
		public static bool IsError(object expression)
		{
			if (expression == null)
			{
				return false;
			}
			return (expression is Exception);
		}

		/// <summary>
		/// Returns a Boolean value indicating whether an expression has no object assigned to it.
		/// </summary>
		/// <param name="expression">Required. Object expression.</param>
		/// <returns>Boolean value</returns>
		public static bool IsNothing(object expression)
		{
			return (expression == null);
		}

		// <summary>
		/// Returns a Boolean value indicating whether an expression can be evaluated as a number.
		/// </summary>
		/// <param name="expression">Required. Object expression.</param>
		/// <returns>Boolean value</returns>
		public static bool IsNumeric(object expression)
		{
			IConvertible convertible = null;
			double num = default(double);
			
			IConvertible convertibleExpression = expression as IConvertible;
			if (convertibleExpression != null)
			{
				convertible = convertibleExpression;
			}
			if (convertible == null)
			{
				char[] charExpression = expression as char[];
				if (convertibleExpression == null)
				{
					return false;
				}
				expression = charExpression;
			}
			TypeCode typeCode = convertible.GetTypeCode();
			if ((typeCode != TypeCode.String) && (typeCode != TypeCode.Char))
			{
				return Utils.IsNumericTypeCode(typeCode);
			}
			string str = convertible.ToString(null);
			try
			{
				long num2 = 0;
				if (StringType.IsHexOrOctValue(str, ref num2))
				{
					return true;
				}
			}
			catch (FormatException)
			{
				return false;
			}
			return DoubleType.TryParse(str, ref num);
		}

		/// <summary>
		/// Returns a Boolean value indicating whether an object is reference.
		/// </summary>
		/// <param name="expression">The object.</param>
		/// <returns>Returns true if the object is ValueType</returns>
		public static bool IsReference(object expression)
		{
			return !(expression is ValueType);
		}

		/// <summary>
		/// Returns the lower bound of an array
		/// </summary>
		/// <param name="array">The Array.</param>
		/// <returns>The lower bound of the array.</returns>
		public static int LBound(Array array)
		{
			return LBound(array, 1);
		}

		/// <summary>
		/// Returns the lower bound of an array
		/// </summary>
		/// <param name="array">The Array.</param>
		/// <param name="rank">The rank of the array.</param>
		/// <returns>The lower bound of the array.</returns>
		public static int LBound(Array array, int rank)
		{
			if (array == null)
			{
				throw ExceptionUtils.VbMakeException(new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Array")), 9);
			}
			if ((rank < 1) || (rank > array.Rank))
			{
				throw new RankException(Utils.GetResourceString("Argument_InvalidRank1", "Rank"));
			}
			return array.GetLowerBound(rank - 1);
		}

		/// <summary>
		/// Returns the code of the color for a given index
		/// </summary>
		/// <param name="color">The index of the color</param>
		/// <returns>The color code</returns>
		public static int QBColor(int color)
		{
			if ((color & 0xfff0) != 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Color"));
			}
			return _qBColorTable[color];
		}

		/// <summary>
		/// Determinates the color by combining the values for Red, Green and Blue
		/// </summary>
		/// <param name="red">Value for Red</param>
		/// <param name="green">Value for Green</param>
		/// <param name="blue">Value for Blue</param>
		/// <returns>The integer value of the color</returns>
		public static int RGB(int red, int green, int blue)
		{
			if ((red & -2147483648) != 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Red"));
			}
			if ((green & -2147483648) != 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Green"));
			}
			if ((blue & -2147483648) != 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Blue"));
			}
			if (red > 0xff)
			{
				red = 0xff;
			}
			if (green > 0xff)
			{
				green = 0xff;
			}
			if (blue > 0xff)
			{
				blue = 0xff;
			}
			return (((blue * 0x10000) + (green * 0x100)) + red);
		}

		/// <summary>
		/// Returns the system type name
		/// </summary>
		/// <param name="vbName">String vb type name</param>
		/// <returns>String system type name</returns>
		public static string SystemTypeName(string vbName)
		{
			return GetVbTypeToNetType(StringsEx.Trim(vbName).ToUpperInvariant());
		}

		private static string GetVbTypeToNetType(string vbName)
		{
			if (StringType.StrCmp(vbName, "OBJECT", false) == 0)
			{
				return "System.Object";
			}
			if (StringType.StrCmp(vbName, "SHORT", false) == 0)
			{
				return "System.Int16";
			}
			if (StringType.StrCmp(vbName, "INTEGER", false) == 0)
			{
				return "System.Int32";
			}
			if (StringType.StrCmp(vbName, "SINGLE", false) == 0)
			{
				return "System.Single";
			}
			if (StringType.StrCmp(vbName, "DOUBLE", false) == 0)
			{
				return "System.Double";
			}
			if (StringType.StrCmp(vbName, "DATE", false) == 0)
			{
				return "System.DateTime";
			}
			if (StringType.StrCmp(vbName, "STRING", false) == 0)
			{
				return "System.String";
			}
			if (StringType.StrCmp(vbName, "BOOLEAN", false) == 0)
			{
				return "System.Boolean";
			}
			if (StringType.StrCmp(vbName, "DECIMAL", false) == 0)
			{
				return "System.Decimal";
			}
			if (StringType.StrCmp(vbName, "BYTE", false) == 0)
			{
				return "System.Byte";
			}
			if (StringType.StrCmp(vbName, "CHAR", false) == 0)
			{
				return "System.Char";
			}
			if (StringType.StrCmp(vbName, "LONG", false) == 0)
			{
				return "System.Int64";
			}
			return null;
		}

		/// <summary>
		/// Returns a String value containing data-type information about a variable.
		/// </summary>
		/// <param name="varName">Required. Object variable. If Option Strict is Off, you can pass a variable of any data type except a structure.</param>
		/// <returns>String value</returns>
		public static string TypeName(object varName)
		{
			bool flag = default(bool);
			string name = default(string);
			if (varName == null)
			{
				return "Nothing";
			}
			Type type = varName.GetType();
			if (type.IsArray)
			{
				flag = true;
				type = type.GetElementType();
			}
			if (type.IsEnum)
			{
				name = type.Name;
			}
			else
			{
				switch (Type.GetTypeCode(type)) {
					case TypeCode.DBNull:
						name = "DBNull";
						goto Label_01F3;

					case TypeCode.Boolean:
						name = "Boolean";
						goto Label_01F3;

					case TypeCode.Char:
						name = "Char";
						goto Label_01F3;

					case TypeCode.Byte:
						name = "Byte";
						goto Label_01F3;

					case TypeCode.Int16:
						name = "Short";
						goto Label_01F3;

					case TypeCode.Int32:
						name = "Integer";
						goto Label_01F3;

					case TypeCode.Int64:
						name = "Long";
						goto Label_01F3;

					case TypeCode.Single:
						name = "Single";
						goto Label_01F3;

					case TypeCode.Double:
						name = "Double";
						goto Label_01F3;

					case TypeCode.Decimal:
						name = "Decimal";
						goto Label_01F3;

					case TypeCode.DateTime:
						name = "Date";
						goto Label_01F3;

					case TypeCode.String:
						name = "String";
						goto Label_01F3;
				}
				name = type.Name;
				if (type.IsCOMObject && (string.CompareOrdinal(name, "__ComObject") == 0))
				{
					do
					{
						string str3;
						string str4;
						int num2;
						string str5;
						ITypeInfo classInfo;
						if (varName is IProvideClassInfo)
						{
							object obj2 = default(object);
							IProvideClassInfo info2 = (IProvideClassInfo)obj2;
							try
							{
								classInfo = info2.GetClassInfo();
								if (classInfo.GetDocumentation(-1, out str3, out str4, out num2, out str5) >= 0)
								{
									name = str3;
									break;
								}
								classInfo = null;
							}
							catch (Exception)
							{
							}
						}
						IDispatch dispatch = varName as IDispatch;
						if (dispatch != null)
						{
							if ((dispatch.GetTypeInfo(0, 0x409, out classInfo) >= 0) && (classInfo.GetDocumentation(-1, out str3, out str4, out num2, out str5) >= 0))
							{
								name = str3;
								break;
							}
						}
					}
					while (0 != 0);
					if (name[0] == '_')
					{
						name = name.Substring(1);
					}
				}
			}
			int index = name.IndexOf('+');
			if (index >= 0)
			{
				name = name.Substring(index + 1);
			}
			Label_01F3:
			if (!flag)
			{
				return name;
			}
			Array array = (Array)varName;
			if (array.Rank == 1)
			{
				name = name + "[]";
			}
			else
			{
				name = name + "[" + new string(',', array.Rank - 1) + "]";
			}
			return Utils.VBFriendlyNameOfTypeName(name);
		}

		/// <summary>
		/// Returns the highest available subscript for the indicated dimension of an array.
		/// </summary>
		/// <param name="array">Required. Array of any data type. The array in which you want to find the highest possible subscript of a dimension.</param>
		/// <returns>Integer value</returns>
		public static int UBound(Array array)
		{
			return UBound(array, 1);
		}

		/// <summary>
		/// Returns the highest available subscript for the indicated dimension of an array.
		/// </summary>
		/// <param name="array">Required. Array of any data type. The array in which you want to find the highest possible subscript of a dimension.</param>
		/// <param name="rank">Optional. Integer. The dimension for which the highest possible subscript is to be returned. Use 1 for the first dimension, 2 for the second, and so on. If Rank is omitted, 1 is assumed.</param>
		/// <returns>Integer value</returns>
		public static int UBound(Array array, int rank)
		{
			if (array == null)
			{
				throw ExceptionUtils.VbMakeException(new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Array")), 9);
			}
			if ((rank < 1) || (rank > array.Rank))
			{
				throw new RankException(Utils.GetResourceString("Argument_InvalidRank1", "Rank"));
			}
			return array.GetUpperBound(rank - 1);
		}

		/// <summary>
		/// Indicates the type of a variant object, returned by the VarType function.
		/// </summary>
		public enum VariantType
		{

			/// <summary>
			/// Array.
			/// </summary>
			Array = 0x2000,

			/// <summary>
			/// A Boolean, value representing true (-1) or false (0).
			/// </summary>
			Boolean = 11,

			/// <summary>
			/// Byte.
			/// </summary>
			Byte = 0x11,

			/// <summary>
			/// Char.
			/// </summary>
			Char = 0x12,

			/// <summary>
			/// Currency.
			/// </summary>
			Currency = 6,

			/// <summary>
			/// DataObject.
			/// </summary>
			DataObject = 13,

			/// <summary>
			/// Date
			/// </summary>
			Date = 7,

			/// <summary>
			/// Decimal.
			/// </summary>
			Decimal = 14,

			/// <summary>
			/// Double.
			/// </summary>
			Double = 5,

			/// <summary>
			/// Null reference. 
			/// </summary>
			Empty = 0,

			/// <summary>
			/// System.Exception
			/// </summary>
			Error = 10,

			/// <summary>
			/// Integer.
			/// </summary>
			Integer = 3,

			/// <summary>
			/// Long.
			/// </summary>
			Long = 20,

			/// <summary>
			/// Null object. 
			/// </summary>
			Null = 1,

			/// <summary>
			/// Any type can be stored in a variable of type Object.
			/// </summary>
			Object = 9,

			/// <summary>
			/// Short.
			/// </summary>
			Short = 2,

			/// <summary>
			/// Single.
			/// </summary>
			Single = 4,

			/// <summary>
			/// String.
			/// </summary>
			String = 8,

			/// <summary>
			/// User-defined type.
			/// </summary>
			UserDefinedType = 0x24,

			/// <summary>
			/// Variant.
			/// </summary>
			Variant = 12
		}

		/// <summary>
		/// Returns variant type indicating the subtype of a variable.
		/// </summary>
		/// <param name="varName">Name of the variable.</param>
		/// <returns>variant type</returns>
		public static VariantType VarType(object varName)
		{
			//check if variable is not null
			if (varName == null)
			{
				//retuens an object
				return VariantType.Object;
			}
			//get variant type 
			return VarTypeFromComType(varName.GetType());
		}

		/// <summary>
		/// Returns variant type from Type
		/// </summary>
		/// <param name="typ">The type.</param>
		/// <returns>The variant type</returns>
		static internal VariantType VarTypeFromComType(Type typ)
		{
			if (typ != null)
			{
				if (typ.IsArray)
				{
					typ = typ.GetElementType();
					if (typ.IsArray)
					{
						return (VariantType.Array | VariantType.Object);
					}
					VariantType type2 = VarTypeFromComType(typ);
					if ((type2 & VariantType.Array) != VariantType.Empty)
					{
						return (VariantType.Array | VariantType.Object);
					}
					return (type2 | VariantType.Array);
				}
				if (typ.IsEnum)
				{
					typ = Enum.GetUnderlyingType(typ);
				}
				if (typ == null)
				{
					return VariantType.Empty;
				}
				switch (Type.GetTypeCode(typ)) {
					case TypeCode.DBNull:
						return VariantType.Null;

					case TypeCode.Boolean:
						return VariantType.Boolean;

					case TypeCode.Char:
						return VariantType.Char;

					case TypeCode.Byte:
						return VariantType.Byte;

					case TypeCode.Int16:
						return VariantType.Short;

					case TypeCode.Int32:
						return VariantType.Integer;

					case TypeCode.Int64:
						return VariantType.Long;

					case TypeCode.Single:
						return VariantType.Single;

					case TypeCode.Double:
						return VariantType.Double;

					case TypeCode.Decimal:
						return VariantType.Decimal;

					case TypeCode.DateTime:
						return VariantType.Date;

					case TypeCode.String:
						return VariantType.String;
				}
				if ((typ == typeof(Missing)) || ObjectType.IsTypeOf(typ, typeof(Exception)))
				{
					return VariantType.Error;
				}
				if (typ.IsValueType)
				{
					return VariantType.UserDefinedType;
				}
			}
			return VariantType.Object;
		}

		/// <summary>
		/// Returns the vb type name
		/// </summary>
		/// <param name="urtName">String type</param>
		/// <returns>Vb type name</returns>
		public static string VbTypeName(string urtName)
		{
			urtName = StringsEx.Trim(urtName).ToUpperInvariant();
			if (StringType.StrCmp(StringsEx.Left(urtName, 7), "SYSTEM.", false) == 0)
			{
				urtName = StringsEx.Mid(urtName, 8);
			}
			string sLeft = urtName;
			return ConvertNetTypeToVbType(sLeft);
		}

		private static string ConvertNetTypeToVbType(string vbTypeName)
		{
			if (StringType.StrCmp(vbTypeName, "OBJECT", false) == 0)
			{
				return "Object";
			}
			if (StringType.StrCmp(vbTypeName, "INT16", false) == 0)
			{
				return "Short";
			}
			if (StringType.StrCmp(vbTypeName, "INT32", false) == 0)
			{
				return "Integer";
			}
			if (StringType.StrCmp(vbTypeName, "SINGLE", false) == 0)
			{
				return "Single";
			}
			if (StringType.StrCmp(vbTypeName, "DOUBLE", false) == 0)
			{
				return "Double";
			}
			if (StringType.StrCmp(vbTypeName, "DATETIME", false) == 0)
			{
				return "Date";
			}
			if (StringType.StrCmp(vbTypeName, "STRING", false) == 0)
			{
				return "String";
			}
			if (StringType.StrCmp(vbTypeName, "BOOLEAN", false) == 0)
			{
				return "Boolean";
			}
			if (StringType.StrCmp(vbTypeName, "DECIMAL", false) == 0)
			{
				return "Decimal";
			}
			if (StringType.StrCmp(vbTypeName, "BYTE", false) == 0)
			{
				return "Byte";
			}
			if (StringType.StrCmp(vbTypeName, "CHAR", false) == 0)
			{
				return "Char";
			}
			if (StringType.StrCmp(vbTypeName, "INT64", false) == 0)
			{
				return "Long";
			}
			return null;
		}
	}





}
