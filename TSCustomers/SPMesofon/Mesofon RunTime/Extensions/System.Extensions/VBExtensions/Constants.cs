﻿using System.Extensions.CompilerServices;
using System.Extensions.VBEnums;

namespace System.Extensions.VBExtensions
{
    /// <summary>
    /// VBConstants class
    /// </summary>
    [StandardModule, DynamicallyInvokable]
    public sealed class Constants
    {
        private Constants()
        {

        }
        // Fields

        #region String constants

        /// <summary>
        /// Define vb Back constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbBack = "\b";

        /// <summary>
        /// Define vb carriage-return constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbCr = "\r";

        /// <summary>
        /// Define vb carriage-return combined with a linefeed character constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbCrLf = "\r\n";

        /// <summary>
        /// Define vbform-feed character constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbFormFeed = "\f";

        /// <summary>
        /// Define vb  linefeed character constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbLf = "\n";

        /// <summary>
        /// Define vb new line constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbNewLine = "\r\n";

        /// <summary>
        /// Define vb Null char constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbNullChar = "\0";

        /// <summary>
        /// Define vb Null string constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbNullString = null;

        /// <summary>
        /// Define vb tab constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbTab = "\t";

        /// <summary>
        /// Define vb vertical tab constant
        /// </summary>
        [DynamicallyInvokable]
        public const string vbVerticalTab = "\v";
        #endregion

        #region MsgBox
        
        /// <summary>
        /// Define vb Abort Retry Ignore message style
        /// </summary>
        public const MsgBoxStyle vbAbortRetryIgnore = MsgBoxStyle.AbortRetryIgnore;

        /// <summary>
        /// Define vb Application Modal message style
        /// </summary>
        public const MsgBoxStyle vbApplicationModal = MsgBoxStyle.OkOnly;

        /// <summary>
        /// Define vb Critical message style
        /// </summary>
        public const MsgBoxStyle vbCritical = MsgBoxStyle.Critical;

        /// <summary>
        /// Define vb Default Button1 message style
        /// </summary>
        public const MsgBoxStyle vbDefaultButton1 = MsgBoxStyle.OkOnly;

        /// <summary>
        /// Define vb Default Button2 message style
        /// </summary>
        public const MsgBoxStyle vbDefaultButton2 = MsgBoxStyle.DefaultButton2;

        /// <summary>
        /// Define vb Default Button3 message style
        /// </summary>
        /// 
        public const MsgBoxStyle vbDefaultButton3 = MsgBoxStyle.DefaultButton3;

        /// <summary>
        /// Define vb Exclamation message style
        /// </summary>
        public const MsgBoxStyle vbExclamation = MsgBoxStyle.Exclamation;

        /// <summary>
        /// Define vb Information message style
        /// </summary>
        public const MsgBoxStyle vbInformation = MsgBoxStyle.Information;

        /// <summary>
        /// Define vb Message Box Help message style
        /// </summary>
        public const MsgBoxStyle vbMsgBoxHelp = MsgBoxStyle.MsgBoxHelp;

        /// <summary>
        /// Define vb Message Box Right message style
        /// </summary>
        public const MsgBoxStyle vbMsgBoxRight = MsgBoxStyle.MsgBoxRight;

        /// <summary>
        /// Define vb Message Box Right to Left Reading message style
        /// </summary>
        public const MsgBoxStyle vbMsgBoxRtlReading = MsgBoxStyle.MsgBoxRtlReading;

        /// <summary>
        /// Define vb Message Box Set Foreground message style
        /// </summary>
        public const MsgBoxStyle vbMsgBoxSetForeground = MsgBoxStyle.MsgBoxSetForeground;

        /// <summary>
        /// Define vb Ok Cancel message style
        /// </summary>
        public const MsgBoxStyle vbOKCancel = MsgBoxStyle.OkCancel;

        /// <summary>
        /// Define vb Ok Only message style
        /// </summary>
        public const MsgBoxStyle vbOKOnly = MsgBoxStyle.OkOnly;

        /// <summary>
        /// Define vb Ok Cancel message style
        /// </summary>
        public const MsgBoxStyle vbOkCancel = MsgBoxStyle.OkCancel;

        /// <summary>
        /// Define vb Ok Only message style
        /// </summary>
        public const MsgBoxStyle vbOkOnly = MsgBoxStyle.OkOnly;

        /// <summary>
        /// Define vb Question message style
        /// </summary>
        public const MsgBoxStyle vbQuestion = MsgBoxStyle.Question;

        /// <summary>
        /// Define vb System Modal message style
        /// </summary>
        public const MsgBoxStyle vbSystemModal = MsgBoxStyle.SystemModal;

        /// <summary>
        /// Define vb Yes No message style
        /// </summary>
        public const MsgBoxStyle vbYesNo = MsgBoxStyle.YesNo;

        /// <summary>
        /// Define vb Yes No Cancel message style
        /// </summary>
        public const MsgBoxStyle vbYesNoCancel = MsgBoxStyle.YesNoCancel;

        /// <summary>
        /// Define vb Retry Cancel message style
        /// </summary>
        public const MsgBoxStyle vbRetryCancel = MsgBoxStyle.RetryCancel;

        /// <summary>
        /// Define vb Abort message style
        /// </summary>
        public const MsgBoxResult vbAbort = MsgBoxResult.Abort;

        /// <summary>
        /// Define vb Cancel message style
        /// </summary>
        public const MsgBoxResult vbCancel = MsgBoxResult.Cancel;

        /// <summary>
        /// Define vb Ignore message style
        /// </summary>
        public const MsgBoxResult vbIgnore = MsgBoxResult.Ignore;

        /// <summary>
        /// Define vb No message style
        /// </summary>
        public const MsgBoxResult vbNo = MsgBoxResult.No;

        /// <summary>
        /// Define vb Ok message style
        /// </summary>
        public const MsgBoxResult vbOK = MsgBoxResult.OK;

        /// <summary>
        /// Define vb Retry message style
        /// </summary>
        public const MsgBoxResult vbRetry = MsgBoxResult.Retry;

        /// <summary>
        /// Define vb Yes message style
        /// </summary>
        public const MsgBoxResult vbYes = MsgBoxResult.Yes;
        #endregion

        #region File System Constants

        /// <summary>
        /// Define vb Archive file attribute
        /// </summary>
        public const FileAttribute vbArchive = FileAttribute.Archive;

        /// <summary>
        /// Define vb Directory file attribute
        /// </summary>
        public const FileAttribute vbDirectory = FileAttribute.Directory;

        /// <summary>
        /// Define vb Hidden file attribute
        /// </summary>
        public const FileAttribute vbHidden = FileAttribute.Hidden;

        /// <summary>
        /// Define vb Normal file attribute
        /// </summary>
        public const FileAttribute vbNormal = FileAttribute.Normal;

        /// <summary>
        /// Define vb ReadOnly file attribute
        /// </summary>
        public const FileAttribute vbReadOnly = FileAttribute.ReadOnly;

        /// <summary>
        /// Define vb System file attribute
        /// </summary>
        public const FileAttribute vbSystem = FileAttribute.System;

        /// <summary>
        /// Define vb Volume file attribute
        /// </summary>
        public const FileAttribute vbVolume = FileAttribute.Volume;
        #endregion

        #region DateTime Constants

        /// <summary>
        /// Define vb First Four Days DateAndTIme constant
        /// </summary>
        public const FirstWeekOfYear vbFirstFourDays = FirstWeekOfYear.FirstFourDays;

        /// <summary>
        /// Define vb First Full Week DateAndTIme constant
        /// </summary>
        public const FirstWeekOfYear vbFirstFullWeek = FirstWeekOfYear.FirstFullWeek;

        /// <summary>
        /// Define vb Jan1 DateAndTIme constant
        /// </summary>
        public const FirstWeekOfYear vbFirstJan1 = FirstWeekOfYear.Jan1;

        /// <summary>
        /// Define vb Friday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbFriday = FirstDayOfWeek.Friday;

        /// <summary>
        /// Define vb Monday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbMonday = FirstDayOfWeek.Monday;

        /// <summary>
        /// Define vb Saturday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbSaturday = FirstDayOfWeek.Saturday;

        /// <summary>
        /// Define vb Sunday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbSunday = FirstDayOfWeek.Sunday;

        /// <summary>
        /// Define vb Thursday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbThursday = FirstDayOfWeek.Thursday;

        /// <summary>
        /// Define vb Tuesday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbTuesday = FirstDayOfWeek.Tuesday;

        /// <summary>
        /// Define vb Use System DateAndTIme constant
        /// </summary>
        public const FirstWeekOfYear vbUseSystem = FirstWeekOfYear.System;

        /// <summary>
        /// Define vb Use System Day Of Week DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbUseSystemDayOfWeek = FirstDayOfWeek.System;

        /// <summary>
        /// Define vb Wednesday DateAndTIme constant
        /// </summary>
        public const FirstDayOfWeek vbWednesday = FirstDayOfWeek.Wednesday;
        #endregion

        #region Variant Type Constants

        /// <summary>
        /// Define vb Array variant type constant
        /// </summary>
        public const Information.VariantType vbArray = Information.VariantType.Array;

        /// <summary>
        /// Define vb Boolean variant type constant
        /// </summary>
        public const Information.VariantType vbBoolean = Information.VariantType.Boolean;

        /// <summary>
        /// Define vb Byte variant type constant
        /// </summary>
        public const Information.VariantType vbByte = Information.VariantType.Byte;

        /// <summary>
        /// Define vb Currency variant type constant
        /// </summary>
        public const Information.VariantType vbCurrency = Information.VariantType.Currency;

        /// <summary>
        /// Define vb Date variant type constant
        /// </summary>
        public const Information.VariantType vbDate = Information.VariantType.Date;

        /// <summary>
        /// Define vb Decimal variant type constant
        /// </summary>
        public const Information.VariantType vbDecimal = Information.VariantType.Decimal;

        /// <summary>
        /// Define vb Double variant type constant
        /// </summary>
        public const Information.VariantType vbDouble = Information.VariantType.Double;

        /// <summary>
        /// Define vb Empty variant type constant
        /// </summary>
        public const Information.VariantType vbEmpty = Information.VariantType.Empty;

        /// <summary>
        /// Define vb Long variant type constant
        /// </summary>
        public const Information.VariantType vbLong = Information.VariantType.Long;

        /// <summary>
        /// Define vb Null variant type constant
        /// </summary>
        public const Information.VariantType vbNull = Information.VariantType.Null;

        /// <summary>
        /// Define vb Object variant type constant
        /// </summary>
        public const Information.VariantType vbObject = Information.VariantType.Object;

        /// <summary>
        /// Define vb Single variant type constant
        /// </summary>
        public const Information.VariantType vbSingle = Information.VariantType.Single;

        /// <summary>
        /// Define vb String variant type constant
        /// </summary>
        public const Information.VariantType vbString = Information.VariantType.String;

        /// <summary>
        /// Define vb User Defined Type variant type constant
        /// </summary>
        public const Information.VariantType vbUserDefinedType = Information.VariantType.UserDefinedType;

        /// <summary>
        /// Define vb Variant variant type constant
        /// </summary>
        public const Information.VariantType vbVariant = Information.VariantType.Variant;

        /// <summary>
        /// Define vb Integer variant type constant
        /// </summary>
        public const Information.VariantType vbInteger = Information.VariantType.Integer;
        #endregion

        #region Win Style Constants

        /// <summary>
        /// Define vb Hide AppWinStyle
        /// </summary>
        public const AppWinStyle vbHide = AppWinStyle.Hide;

        /// <summary>
        /// Define vb Maximized Focus AppWinStyle
        /// </summary>
        public const AppWinStyle vbMaximizedFocus = AppWinStyle.MaximizedFocus;

        /// <summary>
        /// Define vb Minimized focus AppWinStyle
        /// </summary>
        public const AppWinStyle vbMinimizedFocus = AppWinStyle.MinimizedFocus;

        /// <summary>
        /// Define vb Minimized No Focus AppWinStyle
        /// </summary>
        public const AppWinStyle vbMinimizedNoFocus = AppWinStyle.MinimizedNoFocus;

        /// <summary>
        /// Define vb Normal Focus AppWinStyle
        /// </summary>
        public const AppWinStyle vbNormalFocus = AppWinStyle.NormalFocus;

        /// <summary>
        /// Define vb Normal No Focus AppWinStyle
        /// </summary>
        public const AppWinStyle vbNormalNoFocus = AppWinStyle.NormalNoFocus;     
        #endregion

        #region String Compare Method Constants

        /// <summary>
        /// Define vb Binary Compare constant
        /// </summary>
        public const StringType.CompareMethod vbBinaryCompare = StringType.CompareMethod.Binary;

        /// <summary>
        /// Define vb Text Compare constant
        /// </summary>
        public const StringType.CompareMethod vbTextCompare = StringType.CompareMethod.Text;
        #endregion

        #region String TriState Constants

        /// <summary>
        /// Define vb False TriState
        /// </summary>
        public const StringsEx.Tristate vbFalse = StringsEx.Tristate.False;

        /// <summary>
        /// Define vb True TriState
        /// </summary>
        public const StringsEx.Tristate vbTrue = StringsEx.Tristate.True;

        /// <summary>
        /// Define vb UseDefault TriState
        /// </summary>
        public const StringsEx.Tristate vbUseDefault = StringsEx.Tristate.UseDefault;
        #endregion

        #region Date Format Constants

        /// <summary>
        /// Define vb General Date date format
        /// </summary>
        public const StringsEx.DateFormat vbGeneralDate = StringsEx.DateFormat.GeneralDate;

        /// <summary>
        /// Define vb Long Date date format
        /// </summary>
        public const StringsEx.DateFormat vbLongDate = StringsEx.DateFormat.LongDate;

        /// <summary>
        /// Define vb Long Time date format
        /// </summary>
        public const StringsEx.DateFormat vbLongTime = StringsEx.DateFormat.LongTime;

        /// <summary>
        /// Define vb Short Date date format
        /// </summary>
        public const StringsEx.DateFormat vbShortDate = StringsEx.DateFormat.ShortDate;

        /// <summary>
        /// Define vb Short Time date format
        /// </summary>
        public const StringsEx.DateFormat vbShortTime = StringsEx.DateFormat.ShortTime;
        #endregion

        #region String Conversion Constants

        /// <summary>
        /// Define vb Hiragana conversion constant
        /// </summary>
        public const VbStrConvEnum vbHiragana = VbStrConvEnum.Hiragana;

        /// <summary>
        /// Define vb Katakana conversion constant
        /// </summary>
        public const VbStrConvEnum vbKatakana = VbStrConvEnum.Katakana;

        /// <summary>
        /// Define vb Linguistic Casing conversion constant
        /// </summary>
        public const VbStrConvEnum vbLinguisticCasing = VbStrConvEnum.LinguisticCasing;

        /// <summary>
        /// Define vb Lower Case conversion constant
        /// </summary>
        public const VbStrConvEnum vbLowercase = VbStrConvEnum.Lowercase;

        /// <summary>
        /// Define vb Narrow conversion constant
        /// </summary>
        public const VbStrConvEnum vbNarrow = VbStrConvEnum.Narrow;

        /// <summary>
        /// Define vb Proper Case conversion constant
        /// </summary>
        public const VbStrConvEnum vbProperCase = VbStrConvEnum.ProperCase;

        /// <summary>
        /// Define vb Simplified Chinese conversion constant
        /// </summary>
        public const VbStrConvEnum vbSimplifiedChinese = VbStrConvEnum.SimplifiedChinese;

        /// <summary>
        /// Define vb Traditional Chinese conversion constant
        /// </summary>
        public const VbStrConvEnum vbTraditionalChinese = VbStrConvEnum.TraditionalChinese;

        /// <summary>
        /// Define vb Upper Case conversion constant
        /// </summary>
        public const VbStrConvEnum vbUppercase = VbStrConvEnum.Uppercase;

        /// <summary>
        /// Define vb Wide conversion constant
        /// </summary>
        public const VbStrConvEnum vbWide = VbStrConvEnum.Wide;
        #endregion

        #region CallType Constants
       
        #endregion

        /// <summary>
        /// Define vb Object Error constant
        /// </summary>
        public const int vbObjectError = -2147221504;

        // Methods
    }
}
