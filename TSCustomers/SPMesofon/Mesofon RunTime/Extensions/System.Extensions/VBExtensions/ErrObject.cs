using System.Runtime.InteropServices;
using System.Reflection;
using System.Globalization;
using System.IO;
using System.Extensions.CompilerServices;

namespace System.Extensions.VBExtensions
{
	/// <summary>
	/// Class ErrObject
	/// </summary>
	public sealed class ErrObject
	{
		// Fields

		/// <summary>
		/// Boolean to determinate if Clear function should be called
		/// </summary>
		private bool _clearOnCapture;

		/// <summary>
		/// Boolean to determinate if a description is set
		/// </summary>
		private bool _descriptionIsSet;

		/// <summary>
		/// Boolean to determinate if a help context is set
		/// </summary>
		private bool _helpContextIsSet;

		/// <summary>
		/// Boolean to determinate if a help file is set
		/// </summary>
		private bool _helpFileIsSet;


		/// <summary>
		/// Define static readonly COM Exception Type
		/// </summary>
		private static readonly Type _s_COMExceptionType = typeof(COMException);

		/// <summary>
		/// Stores current descripion
		/// </summary>
		private string _m_curDescription;

		/// <summary>
		/// Stores current error line number
		/// </summary>
		private int _m_curErl;

		/// <summary>
		/// Stored current exception
		/// </summary>
		private Exception _m_curException;

		/// <summary>
		/// Stores current help context
		/// </summary>
		private int _m_curHelpContext;

		/// <summary>
		/// Stores current help file
		/// </summary>
		private string _m_curHelpFile;

		/// <summary>
		/// Stores current number
		/// </summary>
		private int _m_curNumber;

		/// <summary>
		/// Stores current source
		/// </summary>
		private string _m_curSource;

		/// <summary>
		/// Define static readonly Directory Not Found Exception Type
		/// </summary>
		private static readonly Type _s_DirectoryNotFoundExceptionType = typeof(DirectoryNotFoundException);

		/// <summary>
		/// Define static readonly Divide By Zero Exception Type
		/// </summary>
		private static readonly Type _s_DivideByZeroExceptionType = typeof(DivideByZeroException);

		/// <summary>
		/// Define static readonly Dll Not Found Exception Type
		/// </summary>
		private static readonly Type _s_DllNotFoundExceptionType = typeof(DllNotFoundException);

		/// <summary>
		/// Define static readonly Entry Point Not Found Exception Type
		/// </summary>
		private static readonly Type _s_EntryPoNotFoundExceptionType = typeof(EntryPointNotFoundException);

		/// <summary>
		/// Define static readonly File Not Found Exception Type
		/// </summary>
		private static readonly Type _s_FileNotFoundExceptionType = typeof(FileNotFoundException);

		/// <summary>
		/// Define static readonly Format Exception Type
		/// </summary>
		private static readonly Type _s_FormatExceptionType = typeof(FormatException);

		// <summary>
		/// Define static readonly Index Out Of Range Exception Type
		/// </summary>
		private static readonly Type _s_IndexOutOfRangeExceptionType = typeof(IndexOutOfRangeException);

		/// <summary>
		/// Define static readonly Invalid Cast Exception Type
		/// </summary>
		private static readonly Type _s_InvalidCastExceptionType = typeof(InvalidCastException);

		/// <summary>
		/// Define static readonly IO Exception Type
		/// </summary>
		private static readonly Type _s_IOExceptionType = typeof(IOException);

		/// <summary>
		/// Define static readonly Not Finite NUmber Exception Type
		/// </summary>
		private static readonly Type _s_NotFiniteNumberExceptionType = typeof(NotFiniteNumberException);

		/// <summary>
		/// Define static readonly Not Supported Exception Type
		/// </summary>
		private static readonly Type _s_NotSupportedExceptionType = typeof(NotSupportedException);

		/// <summary>
		/// Define static readonly Null Reference Exception Type
		/// </summary>
		private static readonly Type _s_NullReferenceExceptionType = typeof(NullReferenceException);

		/// <summary>
		/// Define static readonly Out Of Memory Exception Type
		/// </summary>
		private static readonly Type _s_OutOfMemoryExceptionType = typeof(OutOfMemoryException);

		/// <summary>
		/// Define static readonly Overflow Exception Type
		/// </summary>
		private static readonly Type _s_OverflowExceptionType = typeof(OverflowException);

		/// <summary>
		/// Define static readonly Rank Exception Type
		/// </summary>
		private static readonly Type _s_RankExceptionType = typeof(RankException);

		/// <summary>
		/// Define static readonly SEHE Exception Type
		/// </summary>
		private static readonly Type _s_SEHExceptionType = typeof(SEHException);

		/// <summary>
		/// Define static readonly Type Load Exception Type
		/// </summary>
		private static readonly Type _s_TypeLoadExceptionType = typeof(TypeLoadException);

		/// <summary>
		/// Boolean to determinate if a number is set
		/// </summary>
		private bool _numberIsSet;

		/// <summary>
		/// Boolean to determinate if a source is set
		/// </summary>
		private bool _sourceIsSet;

		// Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		internal ErrObject()
		{
			Clear();
		}

		/// <summary>
		/// Captures the exception
		/// </summary>
		/// <param name="ex">Exception that was thrown</param>
		internal void CaptureException(Exception ex)
		{
			if (ex != _m_curException)
			{
				if (_clearOnCapture)
				{
					Clear();
				}
				else
				{
					_clearOnCapture = true;
				}
				_m_curException = ex;
			}
		}

		/// <summary>
		/// Captures the exception that was thrown
		/// </summary>
		/// <param name="ex">Exception that was thrown</param>
		/// <param name="lErl">Integer that represents location and line number</param>
		internal void CaptureException(Exception ex, int lErl)
		{
			CaptureException(ex);
			_m_curErl = lErl;
		}

		/// <summary>
		/// Clears and resets all variables
		/// </summary>
		public void Clear()
		{
			_m_curException = null;
			_m_curNumber = 0;
			_m_curSource = "";
			_m_curDescription = "";
			_m_curHelpFile = "";
			_m_curHelpContext = 0;
			_m_curErl = 0;
			_numberIsSet = false;
			_sourceIsSet = false;
			_descriptionIsSet = false;
			_helpFileIsSet = false;
			_helpContextIsSet = false;
			_clearOnCapture = true;
		}

		// <summary>
		/// Creates the exception
		/// </summary>
		/// <param name="number">Required. Integer number.</param>
		/// <param name="description">Required. String value.</param>
		/// <returns>Returns mapped exception</returns>
		internal Exception CreateException(int number, string description)
		{
			Clear();
			this.Number = number;
			if (number == 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Number"));
			}
			Exception exception2 = MapNumberToException(_m_curNumber, description);
			_clearOnCapture = false;
			return exception2;
		}

		/// <summary>
		/// Filters the default message
		/// </summary>
		/// <param name="msg">required. String value.</param>
		/// <returns>Returns filtered message</returns>
		private string FilterDefaultMessage(string msg)
		{
			if (_m_curException != null)
			{
				int number = Number;
				if ((msg == null) || (msg.Length == 0))
				{
					msg = Utils.GetResourceString("ID" + StringType.FromInteger(number));
					return msg;
				}
				if (string.CompareOrdinal("Exception from HRESULT: 0x", 0, msg, 0, Math.Min(msg.Length, 0x1a)) == 0)
				{
					string resourceString = Utils.GetResourceString("ID" + StringType.FromInteger(_m_curNumber), false);
					if (resourceString != null)
					{
						msg = resourceString;
					}
				}
			}
			return msg;
		}


		public Exception Exception
		{
			get { return _m_curException; }
		}

		/// <summary>
		/// Creates Help link
		/// </summary>
		/// <param name="helpFile">Required. String value.</param>
		/// <param name="helpContext">Required. Integer value.</param>
		/// <returns>Returns the help link</returns>
		private static string MakeHelpLink(string helpFile, int helpContext)
		{
			return (helpFile + "#" + StringType.FromInteger(helpContext));
		}

		/// <summary>
		/// Maps the error number
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <returns>Returns error number</returns>
		static internal int MapErrorNumber(int number)
		{
			if (number > 0xffff)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Number"));
			}
			if (number < 0)
			{
				if ((number & 0x1fff0000) == 0xa0000)
				{
					return (number & 0xffff);
				}
				if ((number & 0x1fff0000) == 0x10000)
				{
				}
				switch (number) {
					case -2147467263:
						return 0x1bd;

					case -2147467262:
						return 430;

					case -2147467260:
						return 0x11f;

					case -2147352575:
						return 0x1b6;

					case -2147352573:
						return 0x1b6;

					case -2147352572:
						return 0x1c0;

					case -2147352571:
						return 13;

					case -2147352570:
						return 0x1b6;

					case -2147352569:
						return 0x1be;

					case -2147352568:
						return 0x1ca;

					case -2147352566:
						return 6;

					case -2147352565:
						return 9;

					case -2147352564:
						return 0x1bf;

					case -2147352563:
						return 10;

					case -2147352562:
						return 450;

					case -2147352561:
						return 0x1c1;

					case -2147352559:
						return 0x1c3;

					case -2147352558:
						return 11;

					case -2147319786:
						return 0x8016;

					case -2147319785:
						return 0x1cd;

					case -2147319784:
						return 0x8018;

					case -2147319783:
						return 0x8019;

					case -2147319780:
						return 0x801c;

					case -2147319779:
						return 0x801d;

					case -2147319769:
						return 0x8027;

					case -2147319768:
						return 0x8028;

					case -2147319767:
						return 0x8029;

					case -2147319766:
						return 0x802a;

					case -2147319765:
						return 0x802b;

					case -2147319764:
						return 0x802c;

					case -2147319763:
						return 0x802d;

					case -2147319762:
						return 0x802e;

					case -2147319761:
						return 0x1c5;

					case -2147317571:
						return 0x88bd;

					case -2147317563:
						return 0x88c5;

					case -2147316576:
						return 13;

					case -2147316575:
						return 9;

					case -2147316574:
						return 0x39;

					case -2147316573:
						return 0x142;

					case -2147312566:
						return 0x30;

					case -2147312509:
						return 0x9c83;

					case -2147312508:
						return 0x9c84;

					case -2147287039:
						return 0x8006;

					case -2147287038:
						return 0x35;

					case -2147287037:
						return 0x4c;

					case -2147287036:
						return 0x43;

					case -2147287035:
						return 70;

					case -2147287034:
						return 0x8004;

					case -2147287032:
						return 7;

					case -2147287022:
						return 0x43;

					case -2147287021:
						return 70;

					case -2147287015:
						return 0x8003;

					case -2147287011:
						return 0x8005;

					case -2147287010:
						return 0x8004;

					case -2147287008:
						return 0x4b;

					case -2147287007:
						return 70;

					case -2147286960:
						return 0x3a;

					case -2147286928:
						return 0x3d;

					case -2147286789:
						return 0x8018;

					case -2147286788:
						return 0x35;

					case -2147286787:
						return 0x8018;

					case -2147286786:
						return 0x8000;

					case -2147286784:
						return 70;

					case -2147286783:
						return 70;

					case -2147286782:
						return 0x8005;

					case -2147286781:
						return 0x39;

					case -2147286780:
						return 0x8019;

					case -2147286779:
						return 0x8019;

					case -2147286778:
						return 0x8015;

					case -2147286777:
						return 0x8019;

					case -2147286776:
						return 0x8019;

					case -2147221230:
						return 0x1ad;

					case -2147221164:
						return 0x1ad;

					case -2147221021:
						return 0x1ad;

					case -2147221018:
						return 0x1b0;

					case -2147221014:
						return 0x1b0;

					case -2147221005:
						return 0x1ad;

					case -2147221003:
						return 0x1ad;

					case -2147220994:
						return 0x1ad;

					case -2147024891:
						return 70;

					case -2147024882:
						return 7;

					case -2147024809:
						return 5;

					case -2147023174:
						return 0x1ce;

					case -2146959355:
						return 0x1ad;
				}
			}
			return number;
		}

		/// <summary>
		/// Maps the exception that was thrown to a number
		/// </summary>
		/// <param name="e">Exception that was thrown</param>
		/// <returns>Integer corresponding to the thrown exception</returns>
		private static int MapExceptionToNumber(Exception e)
		{
			Type type = e.GetType();
			if (type == _s_IndexOutOfRangeExceptionType)
			{
				return 9;
			}
			if (type == _s_RankExceptionType)
			{
				return 9;
			}
			if (type == _s_DivideByZeroExceptionType)
			{
				return 11;
			}
			if (type == _s_OverflowExceptionType)
			{
				return 6;
			}
			if (type == _s_NotFiniteNumberExceptionType)
			{
				NotFiniteNumberException exception = (NotFiniteNumberException)e;
				if (exception.OffendingNumber == 0.0)
				{
					return 11;
				}
				return 6;
			}
			if (type == _s_NullReferenceExceptionType)
			{
				return 0x5b;
			}
			if (type == _s_InvalidCastExceptionType)
			{
				return 13;
			}
			if (type == _s_NotSupportedExceptionType)
			{
				return 13;
			}
			if (type == _s_COMExceptionType)
			{
				COMException exception2 = (COMException)e;
				return Utils.MapHRESULT(exception2.ErrorCode);
			}
			if (type == _s_SEHExceptionType)
			{
				return 0x63;
			}
			if (type == _s_DllNotFoundExceptionType)
			{
				return 0x35;
			}
			if (type == _s_EntryPoNotFoundExceptionType)
			{
				return 0x1c5;
			}
			if (type == _s_TypeLoadExceptionType)
			{
				return 0x1ad;
			}
			if (type == _s_OutOfMemoryExceptionType)
			{
				return 7;
			}
			if (type == _s_FormatExceptionType)
			{
				return 13;
			}
			if (type == _s_DirectoryNotFoundExceptionType)
			{
				return 0x4c;
			}
			if (type == _s_IOExceptionType)
			{
				return 0x39;
			}
			if (type == _s_FileNotFoundExceptionType)
			{
				return 0x35;
			}
			if (e is MissingMemberException)
			{
				return 0x1b6;
			}
			if (e is InvalidOleVariantTypeException)
			{
				return 0x1ca;
			}
			return 5;
		}

		/// <summary>
		/// Maps the number to the exception
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <param name="description">String expression describing the error</param>
		/// <returns>Exception corresponding to the error number and description</returns>
		private static Exception MapNumberToException(int number, string description)
		{
			bool vBDefinedError = false;
			return ExceptionUtils.BuildException(number, description, ref vBDefinedError);
		}

		/// <summary>
		/// Parses the HelpLink
		/// </summary>
		/// <param name="helpLink">Help link path</param>
		private void ParseHelpLink(string helpLink)
		{
			if ((helpLink == null) || (helpLink.Length == 0))
			{
				if (!_helpContextIsSet)
				{
					HelpContext = 0;
				}
				if (!_helpFileIsSet)
				{
					HelpFile = "";
				}
			}
			else
			{
				int length = StringsEx.s_InvariantCompareInfo.IndexOf(helpLink, "#", CompareOptions.Ordinal);
				if (length != -1)
				{
					if (!_helpContextIsSet)
					{
						if (length < helpLink.Length)
						{
							HelpContext = IntegerType.FromString(helpLink.Substring(length + 1));
						}
						else
						{
							HelpContext = 0;
						}
					}
					if (!_helpFileIsSet)
					{
						HelpFile = helpLink.Substring(0, length);
					}
				}
				else
				{
					if (!_helpContextIsSet)
					{
						HelpContext = 0;
					}
					if (!_helpFileIsSet)
					{
						HelpFile = helpLink;
					}
				}
			}
		}

		/// <summary>
		/// Generates a run-time error
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		public void Raise(int number)
		{
			Raise(number, null, null, null, null);
		}

		/// <summary>
		/// Generates a run-time error
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <param name="source">String expression naming the object or application that generated the error</param>
		public void Raise(int number, object source)
		{
			Raise(number, source, null, null, null);
		}

		/// <summary>
		/// Generates a run-time error
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <param name="source">String expression naming the object or application that generated the error</param>
		/// <param name="description">String expression describing the error</param>
		public void Raise(int number, object source, object description)
		{
			Raise(number, source, description, null, null);
		}

		/// <summary>
		/// Generates a run-time error
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <param name="source">String expression naming the object or application that generated the error</param>
		/// <param name="description">String expression describing the error</param>
		/// <param name="helpFile">The fully qualified path to the Help file in which help on this error can be found</param>
		public void Raise(int number, object source, object description, object helpFile)
		{
			Raise(number, source, description, helpFile, null);
		}

		/// <summary>
		/// Generates a run-time error
		/// </summary>
		/// <param name="number">Integer that identifies the error</param>
		/// <param name="source">String expression naming the object or application that generated the error</param>
		/// <param name="description">String expression describing the error</param>
		/// <param name="helpFile">The fully qualified path to the Help file in which help on this error can be found</param>
		/// <param name="helpContext">The context ID identifying a topic within HelpFile that provides help for the error</param>
		public void Raise(int number, object source, object description, object helpFile, object helpContext)
		{
			if (number == 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValue1", "Number"));
			}
			this.Number = number;
			if (source != null)
			{
				this.Source = StringType.FromObject(source);
			}
			else
			{
				IVbHost vBHost = HostServices.VBHost;
				if (vBHost == null)
				{
					string fullName = Assembly.GetCallingAssembly().FullName;
					int num = StringsEx.InStr(fullName, ",", StringType.CompareMethod.Binary);
					if (num < 1)
					{
						this.Source = fullName;
					}
					else
					{
						this.Source = StringsEx.Left(fullName, num - 1);
					}
				}
				else
				{
					this.Source = vBHost.GetWindowTitle();
				}
			}
			if (helpFile != null)
			{
				this.HelpFile = StringType.FromObject(helpFile);
			}
			if (helpContext != null)
			{
				this.HelpContext = IntegerType.FromObject(helpContext);
			}
			if (description != null)
			{
				this.Description = StringType.FromObject(description);
			}
			else if (!_descriptionIsSet)
			{
                this.Description = Utils.GetResourceString((Utils.vbErrors)_m_curNumber);
			}
			Exception exception = MapNumberToException(_m_curNumber, _m_curDescription);
			exception.Source = _m_curSource;
			exception.HelpLink = MakeHelpLink(_m_curHelpFile, _m_curHelpContext);
			_clearOnCapture = false;
			throw exception;
		}

		/// <summary>
		/// Sets unmapped Errors
		/// </summary>
		/// <param name="number">Required. Integer value.</param>
		public void SetUnmappedError(int number)
		{
			Clear();
			this.Number = number;
			_clearOnCapture = false;
		}

		// Properties

		/// <summary>
		/// Gets/Sets the error Description
		/// </summary>
		public string Description
		{
			get
			{
				if (_descriptionIsSet)
				{
					return _m_curDescription;
				}
				if (_m_curException != null)
				{
					Description = FilterDefaultMessage(_m_curException.Message);
					return _m_curDescription;
				}
				return "";
			}
			set
			{
				_m_curDescription = value;
				_descriptionIsSet = true;
			}
		}

		/// <summary>
		/// Gets Erl
		/// </summary>
		public int Erl
		{
			get { return _m_curErl; }
		}

		/// <summary>
		/// Gets/Sets the HelpContext
		/// </summary>
		public int HelpContext
		{
			get
			{
				if (_helpContextIsSet)
				{
					return _m_curHelpContext;
				}
				if (_m_curException != null)
				{
					ParseHelpLink(_m_curException.HelpLink);
					return _m_curHelpContext;
				}
				return 0;
			}
			set
			{
				_m_curHelpContext = value;
				_helpContextIsSet = true;
			}
		}

		/// <summary>
		/// Gets/Sets the HelpFile
		/// </summary>
		public string HelpFile
		{
			get
			{
				if (_helpFileIsSet)
				{
					return _m_curHelpFile;
				}
				if (_m_curException != null)
				{
					ParseHelpLink(_m_curException.HelpLink);
					return _m_curHelpFile;
				}
				return "";
			}
			set
			{
				_m_curHelpFile = value;
				_helpFileIsSet = true;
			}
		}

		/// <summary>
		/// Gets/Sets the LastDllError
		/// </summary>
		public static int LastDllError
		{
			get { return Marshal.GetLastWin32Error(); }
		}

		/// <summary>
		/// Gets/Sets the error Number
		/// </summary>
		public int Number
		{
			get
			{
				if (_numberIsSet)
				{
					return _m_curNumber;
				}
				if (_m_curException != null)
				{
					Number = MapExceptionToNumber(_m_curException);
					return _m_curNumber;
				}
				return 0;
			}
			set
			{
				_m_curNumber = MapErrorNumber(value);
				_numberIsSet = true;
			}
		}

		/// <summary>
		/// Gets/Sets the Source
		/// </summary>
		public string Source
		{
			get
			{
				if (_sourceIsSet)
				{
					return _m_curSource;
				}
				if (_m_curException != null)
				{
					Source = _m_curException.Source;
					return _m_curSource;
				}
				return "";
			}
			set
			{
				_m_curSource = value;
				_sourceIsSet = true;
			}
		}
	}





}
