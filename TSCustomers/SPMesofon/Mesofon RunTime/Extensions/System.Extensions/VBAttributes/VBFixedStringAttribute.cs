using System.Extensions.CompilerServices;

namespace System.Extensions.VBAttributes
{
	/// <summary>
	/// VBFixedStringAttribute class
	/// </summary>
	[AttributeUsageAttribute(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public sealed class VBFixedStringAttribute : Attribute
	{
		// Fields
		/// <summary>
		/// Length of the string
		/// </summary>
		private int _length;

		// Methods

		/// <summary>
		/// Initializes the value of the m_Length field.
		/// </summary>
		/// <param name="length">Length of the string</param>
		public VBFixedStringAttribute(int length)
		{
			if ((length < 1) || (length > 0x7fff))
			{
				throw new ArgumentException(Utils.GetResourceString("Invalid_VBFixedString"));
			}
			_length = length;
		}

		// Properties

		/// <summary>
		/// Gets the length.
		/// </summary>
		public int Length
		{
			get { return _length; }
		}
	}
}
