using System.Extensions.CompilerServices;

namespace System.Extensions.VBAttributes
{
	/// <summary>
	/// VBFixedArrayAttribute class
	/// </summary>
	[AttributeUsageAttribute(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public sealed class VBFixedArrayAttribute : Attribute
	{
		// Fields

		/// <summary>
		/// Keeps the first upper bound
		/// </summary>
		private int _UpperBound1;

		/// <summary>
		/// Gets or sets the first upper bound.
		/// </summary>
		/// <value>
		/// The upper first bound.
		/// </value>
		public int UpperBound1
		{
			get { return _UpperBound1; }
		}

		/// <summary>
		/// Keeps the second upper bound
		/// </summary>
		private int _UpperBound2;

		/// <summary>
		/// Gets or sets the second upper bound.
		/// </summary>
		/// <value>
		/// The upper second bound.
		/// </value>
		public int UpperBound2
		{
			get { return _UpperBound2; }
		}

		// Methods

		/// <summary>
		/// Initializes the value of the Bounds property.
		/// </summary>
		/// <param name="upperBound1">size of the first dimension of the array</param>
		public VBFixedArrayAttribute(int upperBound1)
		{
			if (upperBound1 < 0)
			{
				throw new ArgumentException(Utils.GetResourceString("Invalid_VBFixedArray"));
			}
			_UpperBound1 = upperBound1;
			_UpperBound2 = -1;
		}

		/// <summary>
		/// Initializes the value of the Bounds property.
		/// </summary>
		/// <param name="upperBound1">size of the first dimension of the array</param>
		/// <param name="upperBound2">size of the second dimension of the array</param>
		public VBFixedArrayAttribute(int upperBound1, int upperBound2)
		{
			if ((upperBound1 < 0) || (upperBound2 < 0))
			{
				throw new ArgumentException(Utils.GetResourceString("Invalid_VBFixedArray"));
			}
			_UpperBound1 = upperBound1;
			_UpperBound2 = upperBound2;
		}

		// Properties

		/// <summary>
		/// Returns the bounds of the array.
		/// </summary>
		public int[] Bounds
		{
			get
			{
				if (_UpperBound2 == -1)
				{
					return new int[] { _UpperBound1 };
				}
				return new int[] {
					_UpperBound1,
					_UpperBound2,
				};
			}
		}

		/// <summary>
		/// Gets the length.
		/// </summary>
		public int Length
		{
			get
			{
				if (_UpperBound2 == -1)
				{
					return (_UpperBound1 + 1);
				}
				return ((_UpperBound1 + 1) * (_UpperBound2 + 1));
			}
		}
	}
}
