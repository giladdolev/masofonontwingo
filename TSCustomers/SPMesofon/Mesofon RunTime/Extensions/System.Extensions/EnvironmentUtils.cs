using System.Collections;

namespace System.Extensions
{
	/// <summary>
	/// EnvironmentUtils class
	/// </summary>
	public static class EnvironmentUtils
	{
		/// <summary>
		/// Gets the environment variable.
		/// </summary>
		/// <param name="strVariable">The variable.</param>
		/// <param name="strTarget">The target.</param>
		/// <returns>The value of the environment variable specified by the variable and target parameters</returns>
		public static string GetEnvironmentVariable(string strVariable, string strTarget)
		{
			if (String.IsNullOrEmpty(strVariable))
			{
				return null;
			}
			EnvironmentVariableTarget target = GetEnvironmentTarget(strTarget);
			return Environment.GetEnvironmentVariable(strVariable, target);
		}
		/// <summary>
		/// Gets the environment variables.
		/// </summary>
		/// <param name="strTarget">The target.</param>
		/// <returns> A dictionary that contains all environment variable names and their values from the source specified by the target parameter</returns>
		public static IDictionary GetEnvironmentVariables(string strTarget)
		{
			EnvironmentVariableTarget target = GetEnvironmentTarget(strTarget);
			return Environment.GetEnvironmentVariables(target);
		}

		/// <summary>
		/// Gets the environment target.
		/// </summary>
		/// <param name="strTarget">The target.</param>
		/// <returns>EnvironmentVariableTarget value.</returns>
		private static EnvironmentVariableTarget GetEnvironmentTarget(string strTarget)
		{
			switch (strTarget.ToUpperInvariant()) {
				case "SYSTEM":
					return EnvironmentVariableTarget.Machine;
				case "USER":
					return EnvironmentVariableTarget.User;
				case "PROCESS":
					return EnvironmentVariableTarget.Process;
			}
			return EnvironmentVariableTarget.Process;
		}
	}
}
