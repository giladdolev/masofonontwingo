﻿namespace System.Extensions.VBEnums
{
    #region Enums

    /// <summary>
    /// Indicates the window style to use for the invoked program when calling the Shell function.
    /// </summary>
    public enum AppWinStyle
    {
        /// <summary>
        /// Window is hidden and focus is passed to the hidden window.
        /// </summary>
        Hide = 0,
        /// <summary>
        /// Window is maximized with focus.
        /// </summary>
        MaximizedFocus = 3,
        /// <summary>
        /// Window is displayed as an icon with focus.
        /// </summary>
        MinimizedFocus = 2,
        /// <summary>
        /// Window is displayed as an icon. The currently active window remains active. 
        /// </summary>
        MinimizedNoFocus = 6,
        /// <summary>
        /// Window has focus and is restored to its original size and position.
        /// </summary>
        NormalFocus = 1,
        /// <summary>
        /// Window is restored to its most recent size and position. The currently active window remains active.
        /// </summary>
        NormalNoFocus = 4
    }

    /// <summary>
    /// Specifies constants defining which buttons to display on a MessageBox.
    /// </summary>
    public enum MessageBoxButtons
    {
        /// <summary>
        /// The message box contains an OK button.
        /// </summary>
        OK,
        /// <summary>
        /// The message box contains OK and Cancel buttons.
        /// </summary>
        OKCancel,
        /// <summary>
        /// The message box contains Abort, Retry, and Ignore buttons.
        /// </summary>
        AbortRetryIgnore,
        /// <summary>
        /// The message box contains Yes, No, and Cancel buttons.
        /// </summary>
        YesNoCancel,
        /// <summary>
        /// The message box contains Yes and No buttons.
        /// </summary>
        YesNo,
        /// <summary>
        /// The message box contains Retry and Cancel buttons.
        /// </summary>
        RetryCancel
    }

    /// <summary>
    /// Indicates which button was pressed on a message box, returned by the MsgBox function.
    /// </summary>
    public enum MsgBoxResult
    {
        /// <summary>
        /// Abort button was pressed.
        /// </summary>
        Abort = 3,
        /// <summary>
        /// Cancel button was pressed.
        /// </summary>
        Cancel = 2,
        /// <summary>
        /// Ignore button was pressed.
        /// </summary>
        Ignore = 5,
        /// <summary>
        /// No button was pressed.
        /// </summary>
        No = 7,
        /// <summary>
        /// OK button was pressed. 
        /// </summary>
        OK = 1,
        /// <summary>
        /// Retry button was pressed. 
        /// </summary>
        Retry = 4,
        /// <summary>
        /// Yes button was pressed.
        /// </summary>
        Yes = 6
    }

    /// <summary>
    /// Indicates which buttons to display when calling the MsgBox function.
    /// </summary>
    [Flags]
    public enum MsgBoxStyle
    {
        /// <summary>
        /// Not defined
        /// </summary>
        None,
        /// <summary>
        /// Abort, Retry, and Ignore buttons.
        /// </summary>
        AbortRetryIgnore = 2,
        /// <summary>
        /// Application modal message box. 
        /// </summary>
        ApplicationModal = 0,
        /// <summary>
        /// Critical message.
        /// </summary>
        Critical = 0x10,
        /// <summary>
        /// First button is default.
        /// </summary>
        DefaultButton1 = 0,
        /// <summary>
        /// Second button is default.
        /// </summary>
        DefaultButton2 = 0x100,
        /// <summary>
        /// Third button is default.
        /// </summary>
        DefaultButton3 = 0x200,
        /// <summary>
        /// Warning message.
        /// </summary>
        Exclamation = 0x30,
        /// <summary>
        /// Information message.
        /// </summary>
        Information = 0x40,
        /// <summary>
        /// Help text.
        /// </summary>
        MsgBoxHelp = 0x4000,
        /// <summary>
        /// Right-aligned text.
        /// </summary>
        MsgBoxRight = 0x80000,
        /// <summary>
        /// Right-to-left reading text.
        /// </summary>
        MsgBoxRtlReading = 0x100000,
        /// <summary>
        /// Foreground message box window.
        /// </summary>
        MsgBoxSetForeground = 0x10000,
        /// <summary>
        /// OK and Cancel buttons.
        /// </summary>
        OkCancel = 1,
        /// <summary>
        /// OK button only (default). 
        /// </summary>
        OkOnly = 0,
        /// <summary>
        /// Warning query. 
        /// </summary>
        Question = 0x20,
        /// <summary>
        /// Retry and Cancel buttons.
        /// </summary>
        RetryCancel = 5,
        /// <summary>
        /// System modal message box.
        /// </summary>
        SystemModal = 0x1000,
        /// <summary>
        /// Yes and No buttons. 
        /// </summary>
        YesNo = 4,
        /// <summary>
        /// Yes, No, and Cancel buttons.
        /// </summary>
        YesNoCancel = 3
    }


    /// <summary>
    /// Types
    /// </summary>
    internal enum VT : short
    {
        /// <summary>
        /// Array type
        /// </summary>
        Array = 0x2000,
        /// <summary>
        /// Boolean type
        /// </summary>
        Boolean = 11,
        /// <summary>
        /// Byte type
        /// </summary>
        Byte = 0x11,
        /// <summary>
        /// ByteArray type
        /// </summary>
        ByteArray = 0x2011,
        /// <summary>
        /// Char type
        /// </summary>
        Char = 0x12,
        /// <summary>
        /// CharArray type
        /// </summary>
        CharArray = 0x2012,
        /// <summary>
        /// Currency type
        /// </summary>
        Currency = 6,
        /// <summary>
        /// Date type
        /// </summary>
        Date = 7,
        /// <summary>
        /// DBNull type
        /// </summary>
        DBNull = 1,
        /// <summary>
        /// Decimal type
        /// </summary>
        Decimal = 14,
        /// <summary>
        /// Double type
        /// </summary>
        Double = 5,
        /// <summary>
        /// Empty
        /// </summary>
        Empty = 0,
        /// <summary>
        /// Error
        /// </summary>
        Error = 10,
        /// <summary>
        /// Integer type
        /// </summary>
        Integer = 3,
        /// <summary>
        /// Long type
        /// </summary>
        Long = 20,
        /// <summary>
        /// Short type
        /// </summary>
        Short = 2,
        /// <summary>
        /// Single type
        /// </summary>
        Single = 4,
        /// <summary>
        /// String type
        /// </summary>
        String = 8,
        /// <summary>
        /// Structure type
        /// </summary>
        Structure = 0x24,
        /// <summary>
        /// Variant type
        /// </summary>
        Variant = 12
    }

    /// <summary>
    /// Indicates which type of conversion to perform when calling the StrConv function.
    /// </summary>
    [Flags]
    public enum VbStrConvEnum
    {
        /// <summary>
        /// Performs no conversion.
        /// </summary>
        None = 0,
        /// <summary>
        /// Converts Katakana characters in the string to Hiragana characters. 
        /// </summary>
        Hiragana = 0x20,
        /// <summary>
        /// Converts Hiragana characters in the string to Katakana characters. 
        /// </summary>
        Katakana = 0x10,
        /// <summary>
        /// Converts the string from file system rules for casing to linguistic rules
        /// </summary>
        LinguisticCasing = 0x400,
        /// <summary>
        /// Converts the string to lowercase characters.
        /// </summary>
        Lowercase = 2,
        /// <summary>
        /// Converts wide (double-byte) characters in the string to narrow (single-byte) characters. 
        /// </summary>
        Narrow = 8,
        /// <summary>
        /// Converts the first letter of every word in the string to uppercase. 
        /// </summary>
        ProperCase = 3,
        /// <summary>
        /// Converts the string to Simplified Chinese characters.
        /// </summary>
        SimplifiedChinese = 0x100,
        /// <summary>
        /// Converts the string to Traditional Chinese characters. 
        /// </summary>
        TraditionalChinese = 0x200,
        /// <summary>
        /// Converts the string to uppercase characters.
        /// </summary>
        Uppercase = 1,
        /// <summary>
        /// Converts narrow (single-byte) characters in the string to wide (double-byte) characters.
        /// </summary>
        Wide = 4
    }

    #endregion

    /// <summary>
    /// Indicates the first day of the week to use when calling date-related functions.
    /// </summary>
    public enum FirstDayOfWeek
    {

        /// <summary>
        /// The first day of the week as specified in the system settings
        /// </summary>
        System,

        /// <summary>
        /// Sunday
        /// </summary>
        Sunday,

        /// <summary>
        /// Monday
        /// </summary>
        Monday,

        /// <summary>
        /// Tuesday
        /// </summary>
        Tuesday,

        /// <summary>
        /// Wednesday
        /// </summary>
        Wednesday,

        /// <summary>
        /// Thursday
        /// </summary>
        Thursday,

        /// <summary>
        /// Friday
        /// </summary>
        Friday,

        /// <summary>
        /// Saturday
        /// </summary>
        Saturday
    }

    /// <summary>
    /// Indicates the first week of the year to use when calling date-related functions.
    /// </summary>
    public enum FirstWeekOfYear
    {

        /// <summary>
        /// The weekspecified in your system settings as the first week of the year
        /// </summary>
        System,

        /// <summary>
        /// The week in which January 1 occurs
        /// </summary>
        Jan1,

        /// <summary>
        /// The first week that has at least four days in the new year
        /// </summary>
        FirstFourDays,

        /// <summary>
        /// The first full week of the year
        /// </summary>
        FirstFullWeek
    }
    /// <summary>
    /// Indicates how to determine and format date intervals when calling date-related functions.
    /// </summary>
    public enum DateInterval
    {
        /// <summary>
        /// Year
        /// </summary>
        Year,

        /// <summary>
        /// Quarter of year (1 through 4)
        /// </summary>
        Quarter,

        /// <summary>
        /// Month (1 through 12)
        /// </summary>
        Month,

        /// <summary>
        /// Day of year (1 through 366)
        /// </summary>
        DayOfYear,

        /// <summary>
        /// Day of month (1 through 31)
        /// </summary>
        Day,

        /// <summary>
        /// Week of year (1 through 53)
        /// </summary>
        WeekOfYear,

        /// <summary>
        /// Day of week (1 through 7)
        /// </summary>
        Weekday,

        /// <summary>
        /// Hour (0 through 23)
        /// </summary>
        Hour,

        /// <summary>
        /// Minute (0 through 59)
        /// </summary>
        Minute,

        /// <summary>
        /// Second (0 through 59)
        /// </summary>
        Second
    }
}
