﻿namespace System.Extensions.VBEnums
{
    /// <summary>
    /// Provides attributes for files and directories.
    /// </summary>
    [Flags]
    public enum FileAttribute
    {
        /// <summary>
        /// Not defined
        /// </summary>
        None,

        /// <summary>
        /// File has changed since last backup. Attribute is read/write.
        /// </summary>
        Archive = 0x20,

        /// <summary>
        /// Folder or directory. Attribute is read-only.
        /// </summary>
        Directory = 0x10,

        /// <summary>
        /// Hidden file. Attribute is read/write.
        /// </summary>
        Hidden = 2,

        /// <summary>
        /// Normal file. No attributes are set.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Read-only file. Attribute is read/write.
        /// </summary>
        ReadOnly = 1,

        /// <summary>
        /// System file. Attribute is read/write.
        /// </summary>
        System = 4,

        /// <summary>
        /// Disk drive volume label. Attribute is read-only.
        /// </summary>
        Volume = 8
    }
}
