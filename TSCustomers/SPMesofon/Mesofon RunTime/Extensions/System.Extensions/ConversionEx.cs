namespace System.Extensions
{
	using System;
	using CompilerServices;
	using VBExtensions;
	using Globalization;
	using Threading;

	/// <summary>
	/// Conversion class contains the procedures used to perform various conversion operations.
	/// </summary>
	[StandardModuleAttribute()]
	public sealed class ConversionEx
	{

		private ConversionEx()
		{

		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>Double number</returns>
		public static double Fix(double number)
		{
			//if the number zero or more
			if (number >= 0.0)
			{
				//Get the integer portion
				return Math.Floor(number);
			}
			//Get the integer portion as negative number
			return -Math.Floor(-number);
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>Int number</returns>
		public static int Fix(int number)
		{
			//Get the integer portion
			return number;
		}
		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>Float number</returns>
		public static float Fix(float number)
		{
			//if the number zero or more
			if (number >= 0f)
			{
				//Get the integer portion
				return (float)Math.Floor((double)number);
			}
			
			//Get the integer portion as negative number
			return (float)-Math.Floor((double)-number);
		}


		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>Decimal number</returns>
		public static decimal Fix(decimal number)
		{
			//if the number is less than zero
			if (number < decimal.Zero)
			{
				//Get the integer portion as negative number
				return decimal.Negate(decimal.Floor(decimal.Negate(number)));
			}
			
			//Get the integer portion
			return decimal.Floor(number);
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>short number</returns>
		public static short Fix(short number)
		{
			//Get the integer portion
			return number;
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>long number</returns>
		public static long Fix(long number)
		{
			//Get the integer portion
			return number;
		}

		/// <summary>
		/// Returns a string representing the hexadecimal value of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>string number</returns>
		public static string Hex(byte number)
		{
			//return as string
			return number.ToString("X", CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Returns a string representing the hexadecimal value of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>string number</returns>
		public static string Hex(short number)
		{
			//return as string
			return number.ToString("X", CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Returns a string representing the hexadecimal value of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>string number</returns>
		public static string Hex(int number)
		{
			//return as string
			return number.ToString("X", CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Returns a string representing the hexadecimal value of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>string number</returns>
		public static string Hex(long number)
		{
			//return as string
			return number.ToString("X", CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Returns a string representing the hexadecimal value of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>string number</returns>
		/// <exception cref="System.ArgumentNullException"></exception>
		/// <exception cref="System.ArgumentException"></exception>
		public static string Hex(object number)
		{
			long num;
			// Check if the number is null
			if (number == null)
			{
				// Throw Exception
				throw new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Number"));
			}
			
			// Save the number as IConvertible type
			IConvertible convertible = number as IConvertible;
			
			// If the number is legit
			if (convertible != null)
			{
				// Get the number type
				switch (convertible.GetTypeCode()) {
					case TypeCode.Boolean:
					case TypeCode.Char:
					case TypeCode.DateTime:
					case TypeCode.SByte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case (TypeCode.DateTime | TypeCode.Object):
						goto Label_00FC;

					case TypeCode.Byte:
						// Convert to hexadecimal
						return Hex(convertible.ToByte(null));

					case TypeCode.Int16:
						// Convert to hexadecimal
						return Hex(convertible.ToInt16(null));

					case TypeCode.Int32:
						// Convert to hexadecimal
						return Hex(convertible.ToInt32(null));

					case TypeCode.Int64:
					case TypeCode.Single:
					case TypeCode.Double:
					case TypeCode.Decimal:
						// Convert to hexadecimal
						num = convertible.ToInt64(null);
						goto Label_00BA;

					case TypeCode.String:
						num = LongType.FromString(convertible.ToString(null));
						goto Label_00BA;
				}
			}
			goto Label_00FC;
			Label_00BA:
			// If the number is zero
			if (num == 0L)
			{
				return "0";
			}
			// Handle negative numbers
			if ((num <= 0L) && (num >= -2147483648L))
			{
				// Convert to hexadecimal
				return Hex((int)num);
			}
			return Hex(num);
			Label_00FC:
			// Raise an unrecognized type exception
			throw new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "Number", Utils.VBFriendlyName(number)));
		}

		/// <summary>
		/// It reduces the value to the nearest decimal
		/// </summary>
		/// <param name="number">Required. Decimal value.</param>
		/// <returns>Decimal number</returns>
		public static decimal Int(decimal number)
		{
			return decimal.Floor(number);
		}

		/// <summary>
		/// It reduces the value to the nearest double
		/// </summary>
		/// <param name="number">Required. Double value.</param>
		/// <returns>Double number</returns>
		public static double Int(double number)
		{
			return Math.Floor(number);
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>short Number</returns>
		public static short Int(short number)
		{
			//Get the integer portion of a number
			return number;
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>int Number</returns>
		public static int Int(int number)
		{
			//Get the integer portion of a number
			return number;
		}

		/// <summary>
		///Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>long Number</returns>
		public static long Int(long number)
		{
			//Get the integer portion of a number
			return number;
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>(object Number</returns>
		/// <exception cref="System.ArgumentNullException"></exception>
		/// <exception cref="System.ArgumentException"></exception>
		public static object Int(object number)
		{
			// Check if the number is null
			if (number == null)
			{
				// Throw Exception
				throw new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Number"));
			}
			
			// Save the number as IConvertible type
			IConvertible convertible = number as IConvertible;
			
			//if the number is legit
			if (convertible != null)
			{
				//get the number type
				switch (convertible.GetTypeCode()) {
					case TypeCode.Boolean:
						//Convert boolean to int
						return convertible.ToInt32(null) > 0 ? -1 : 0;

					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.Int16:
					case TypeCode.UInt16:
					case TypeCode.Int32:
					case TypeCode.UInt32:
					case TypeCode.Int64:
					case TypeCode.UInt64:
						
						//return the number as is
						return number;

					case TypeCode.Single:
						//Convert to int
						return Int(convertible.ToSingle(null));

					case TypeCode.Double:
						//Convert to int
						return Int(convertible.ToDouble(null));

					case TypeCode.Decimal:
						//Convert to int
						return Int(convertible.ToDecimal(null));

					case TypeCode.String:
						//Convert string to int
						return Int(DoubleType.FromString(convertible.ToString(null)));

					case TypeCode.DateTime:
						//Convert date to number and returns the integer portion of it.
						return Math.Floor(((DateTime)number).ToOADate());
				}
			}
			//raise an unrecognized type exception
			throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_NotNumericType2", "Number", number.GetType().FullName)), 13);
		}

		/// <summary>
		/// Returns the integer portion of a number.
		/// </summary>
		/// <param name="number">The number.</param>
		/// <returns>Float number.</returns>
		public static float Int(float number)
		{
			return (float)Math.Floor(number);
		}

		/// <summary>
		/// Converts boolean value to integer value
		/// </summary>
		/// <param name="number">Required. Boolean value.</param>
		/// <returns>Integer value.</returns>
		public static int BoolToInt(bool number)
		{
			if (number)
			{
				return -1;
			}
			return 0;
		}

		/// <summary>
		/// Converts string to boolean
		/// </summary>
		/// <param name="text">Required. String value</param>
		/// <returns>Boolean value.</returns>
		public static bool StringToBool(string text)
		{
		    if (text == string.Empty)
		    {
		        return false;
		    }

			double result = 0;
			
			if (double.TryParse(text, out result))
			{
				return Convert.ToBoolean(result);
			}
			
			return Convert.ToBoolean(text, CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Returns the numbers contained
		/// </summary>
		/// <param name="expression">Required. Any valid char</param>
		/// <returns>Integer value.</returns>
		public static int Val(char expression)
		{
			int num = expression;
			if ((num >= 0x31) && (num <= 0x39))
			{
				return (num - 0x30);
			}
			return 0;
		}

		/// <summary>
		/// Returns the numbers contained in a object value
		/// </summary>
		/// <param name="expression">Required. Any valid object value</param>
		/// <returns>Double value.</returns>
		public static double Val(object expression)
		{
			double num;
			
			string strExpression = expression as string;
			if (!String.IsNullOrEmpty(strExpression))
			{
				return Val(strExpression);
			}
			if (expression is char)
			{
				return Val((char)expression);
			}
			if (Information.IsNumeric(expression))
			{
				return DoubleType.FromObject(expression);
			}
			try
			{
				num = Val(StringType.FromObject(expression));
			}
			catch (Exception)
			{
				throw ExceptionUtils.VbMakeException(new ArgumentException(Utils.GetResourceString("Argument_InvalidValueType2", "Expression", Utils.VBFriendlyName(expression))), 0x1b6);
			}
			return num;
		}

		/// <summary>
		/// Returns the numbers contained in a string
		/// </summary>
		/// <param name="inputStr">Required. Any valid string value</param>
		/// <returns>Double value.</returns>
		public static double Val(string inputStr)
		{
			if (String.IsNullOrEmpty(inputStr))
			{
                throw new ArgumentNullException("inputStr");
			}
			
			int num1 = inputStr != null ? inputStr.Length : 0;
			int index = 0;
			while (index < num1)
			{
				switch (inputStr[index]) {
					case '\t':
					case '\n':
					case '\r':
					case ' ':
					case '　':
						checked
						{
							++index;
						}
						continue;
					default:
						goto label_4;
				}
			}
			label_4:
			if (index >= num1)
				return 0.0;
			if (inputStr[index] == 38)
				return HexOrOctValue(inputStr, checked(index + 1));
			bool flag1 = false;
			bool flag2 = false;
			bool flag3 = false;
			double num2 = 0.0;
			char ch1 = inputStr[index];
			switch (ch1) {
				case '-':
					flag3 = true;
					checked
					{
						++index;
					}
					break;
				case '+':
					checked
					{
						++index;
					}
					break;
			}
			int num3 = 0;
			double num4 = 0;
			int num5 = 0;
			while (index < num1)
			{
				ch1 = inputStr[index];
				char ch2 = ch1;
				if (ch2 == 9 || ch2 == 10 || (ch2 == 13 || ch2 == 32) || ch2 == 12288)
					checked
					{
						++index;
					}
				else if (ch2 == 48)
				{
					if (num3 != 0 || flag1)
					{
						num4 = num4 * 10.0 + ch1 - 48.0;
						checked
						{
							++index;
						}
						checked
						{
							++num3;
						}
					}
					else
						checked
						{
							++index;
						}
				}
				else if (ch2 >= 49 && ch2 <= 57)
				{
					num4 = num4 * 10.0 + ch1 - 48.0;
					checked
					{
						++index;
					}
					checked
					{
						++num3;
					}
				}
				else if (ch2 == 46)
				{
					checked
					{
						++index;
					}
					if (!flag1)
					{
						flag1 = true;
						num5 = num3;
					}
					else
						break;
				}
				else if (ch2 == 101 || ch2 == 69 || (ch2 == 100 || ch2 == 68))
				{
					flag2 = true;
					checked
					{
						++index;
					}
					break;
				}
				else
					break;
			}
			int num6 = 0;
			if (flag1)
				num6 = checked(num3 - num5);
			if (flag2)
			{
				bool flag4 = false;
				bool flag5 = false;
				while (index < num1)
				{
					ch1 = inputStr[index];
					char ch2 = ch1;
					if (ch2 == 9 || ch2 == 10 || (ch2 == 13 || ch2 == 32) || ch2 == 12288)
						checked
						{
							++index;
						}
					else if (ch2 >= 48 && ch2 <= 57)
					{
						num2 = num2 * 10.0 + ch1 - 48.0;
						checked
						{
							++index;
						}
					}
					else if (ch2 == 43)
					{
						if (!flag4)
						{
							flag4 = true;
							checked
							{
								++index;
							}
						}
						else
							break;
					}
					else if (ch2 == 45 && !flag4)
					{
						flag4 = true;
						flag5 = true;
						checked
						{
							++index;
						}
					}
					else
						break;
				}
				if (flag5)
				{
					double num7 = num2 + num6;
					num4 *= Math.Pow(10.0, -num7);
				}
				else
				{
					double y = num2 - num6;
					num4 *= Math.Pow(10.0, y);
				}
			}
			else if (flag1 && num6 != 0)
				num4 /= Math.Pow(10.0, num6);
			if (double.IsInfinity(num4))
				throw ExceptionUtils.VbMakeException(6);
			if (flag3)
				num4 = -num4;
			switch (ch1) {
				case '%':
					if (num6 > 0)
						throw ExceptionUtils.VbMakeException(13);
					num4 = checked((short)Math.Round(num4));
					break;
				case '&':
					if (num6 > 0)
						throw ExceptionUtils.VbMakeException(13);
					num4 = checked((int)Math.Round(num4));
					break;
				case '!':
					break;
				case '@':
					num4 = Convert.ToDouble(new Decimal(num4));
					break;
			}
			return num4;
		}

		/// <summary>
		/// Checking if the value is hex or oct
		/// </summary>
		/// <param name="inputStr">Required.String value.</param>
		/// <param name="i">Required. Integer value.</param>
		/// <returns>Double value.</returns>
		private static double HexOrOctValue(string inputStr, int i)
		{
			int num2;
			long num5 = 0;
			int num = 0;
			int length = inputStr.Length;
			char ch = inputStr[i];
			i++;
			if ((ch != 'H') && (ch != 'h'))
			{
				if ((ch != 'O') && (ch != 'o'))
				{
					return 0.0;
				}
				while ((i < length) && (num < 0x16))
				{
					ch = inputStr[i];
					i++;
					char ch2 = ch;
					if ((((ch2 != '\t') && (ch2 != '\n')) && ((ch2 != '\r') && (ch2 != ' '))) && (ch2 != '　'))
					{
						if (ch2 == '0')
						{
							if (num == 0)
							{
								continue;
							}
							num2 = 0;
						}
						else
						{
							if ((ch2 < '1') || (ch2 > '7'))
							{
								break;
							}
							num2 = ch - '0';
						}
						if (num5 >= 0x1000000000000000L)
						{
							num5 = (num5 & 0xfffffffffffffffL) * 8L;
							num5 |= 0x1000000000000000L;
						}
						else
						{
							num5 *= 8L;
						}
						num5 += num2;
						num++;
					}
				}
			}
			else
			{
				while ((i < length) && (num < 0x11))
				{
					ch = inputStr[i];
					i++;
					char ch3 = ch;
					if ((((ch3 != '\t') && (ch3 != '\n')) && ((ch3 != '\r') && (ch3 != ' '))) && (ch3 != '　'))
					{
						if (ch3 == '0')
						{
							if (num == 0)
							{
								continue;
							}
							num2 = 0;
						}
						else if ((ch3 >= '1') && (ch3 <= '9'))
						{
							num2 = ch - '0';
						}
						else if ((ch3 >= 'A') && (ch3 <= 'F'))
						{
							num2 = ch - '7';
						}
						else
						{
							if ((ch3 < 'a') || (ch3 > 'f'))
							{
								break;
							}
							num2 = ch - 'W';
						}
						if ((num == 15) && (num5 > 0x7ffffffffffffffL))
						{
							num5 = (num5 & 0x7ffffffffffffffL) * 0x10L;
							num5 |= -9223372036854775808L;
						}
						else
						{
							num5 *= 0x10L;
						}
						num5 += num2;
						num++;
					}
				}
				if (num == 0x10)
				{
					i++;
					if (i < length)
					{
						ch = inputStr[i];
					}
				}
				if (num <= 8)
				{
					if ((num > 4) || (ch == '&'))
					{
						if (num5 > 0x7fffffffL)
						{
							num5 = -2147483648L + (num5 & 0x7fffffffL);
						}
					}
					else if (((num > 2) || (ch == '%')) && (num5 > 0x7fffL))
					{
						num5 = -32768L + (num5 & 0x7fffL);
					}
				}
				switch (ch) {
					case '%':
						num5 = (short)num5;
						break;

					case '&':
						num5 = (int)num5;
						break;
				}
				return num5;
			}
			if (num == 0x16)
			{
				i++;
				if (i < length)
				{
					ch = inputStr[i];
				}
			}
			if (num5 <= 0x100000000L)
			{
				if ((num5 > 0xffffL) || (ch == '&'))
				{
					if (num5 > 0x7fffffffL)
					{
						num5 = -2147483648L + (num5 & 0x7fffffffL);
					}
				}
				else if (((num5 > 0xffL) || (ch == '%')) && (num5 > 0x7fffL))
				{
					num5 = -32768L + (num5 & 0x7fffL);
				}
			}
			switch (ch) {
				case '%':
					num5 = (short)num5;
					break;

				case '&':
					num5 = (int)num5;
					break;
			}
			return num5;
		}

		/// <summary>
		/// Returns the error message that corresponds to a given error number.
		/// </summary>
		/// <param name="errorNumber">Required. Any valid error number.</param>
		/// <returns>String value.</returns>
		public static string ErrorToString(int errorNumber)
		{
			if (errorNumber >= 0xffff)
			{
				throw new ArgumentException(Utils.GetResourceString("MaxErrNumber"));
			}
			if (errorNumber > 0)
			{
				errorNumber = -2146828288 | errorNumber;
			}
			if ((errorNumber & 0x1fff0000) == 0xa0000)
			{
				errorNumber &= 0xffff;
				return Utils.GetResourceString((Utils.vbErrors)errorNumber);
			}
			if (errorNumber != 0)
			{
				return Utils.GetResourceString(Utils.vbErrors.UserDefined);
			}
			return String.Empty;
		}

		/// <summary>
		/// Returns a String representation of a number.
		/// </summary>
		/// <param name="number">Required. An Object containing any valid numeric expression.</param>
		/// <returns>String value.</returns>  
		public static string Str(object number)
		{
			string str;
			if (number == null)
			{
				throw new ArgumentNullException(Utils.GetResourceString("Argument_InvalidNullValue1", "Number"));
			}
			var convertible = number as IConvertible;
			if (convertible == null)
			{
				throw new InvalidCastException(Utils.GetResourceString("ArgumentNotNumeric1", "Number"));
			}
			TypeCode typeCode = convertible.GetTypeCode();
			switch (typeCode) {
				case TypeCode.DBNull:
					return "Null";

				case TypeCode.Boolean:
					if (!convertible.ToBoolean(null))
					{
						return "False";
					}
					return "True";

				case TypeCode.SByte:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.UInt16:
				case TypeCode.Int32:
				case TypeCode.UInt32:
				case TypeCode.Int64:
				case TypeCode.UInt64:
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					str = Convert.ToString(number, CultureInfo.CurrentCulture);
					break;
				case TypeCode.DateTime:
					DateTime dtemp = Convert.ToDateTime(number, CultureInfo.InvariantCulture);
					str = dtemp.ToShortDateString();
					break;

				default:
					if (typeCode == TypeCode.String)
					{
						try
						{
							str = Convert.ToString(Convert.ToDouble(convertible.ToString(null), CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
							break;
						}
						catch (StackOverflowException)
						{
							throw;
						}
						catch (OutOfMemoryException)
						{
							throw;
						}
						catch (ThreadAbortException)
						{
							throw;
						}
						catch (Exception)
						{
							throw new InvalidCastException(Utils.GetResourceString("ArgumentNotNumeric1", "Number"));
						}
					}
					throw new InvalidCastException(Utils.GetResourceString("ArgumentNotNumeric1", "Number"));
			}
			if ((str.Length > 0) && (str[0] != '-'))
			{
				return (" " + Utils.StdFormat(str));
			}
			return Utils.StdFormat(str);
		}


	}
}
