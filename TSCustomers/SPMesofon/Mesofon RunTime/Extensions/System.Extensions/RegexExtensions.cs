using System.Text.RegularExpressions;
using System.Reflection;

namespace System.Extensions
{
    /// <summary>
    /// RegexExtensions contains method to work with regular expressions
    /// </summary>
    public static class RegexExtensions
    {
        /// <summary>
        /// Gets the pattern of the Regex object.
        /// </summary>
        /// <param name="regex">The regex object.</param>
        /// <returns>String value.</returns>
        public static string GetPattern(this Regex regex)
        {
            if (regex == null)
            {
                throw new ArgumentNullException("regex");
            }

            // Get object's type
            Type t = regex.GetType();

            // Get the field info for the pattern field
            FieldInfo fieldInfo = t.GetField("pattern", BindingFlags.Instance | BindingFlags.NonPublic);

            if (fieldInfo != null)
            {
                // Get the value of the pattern
                object pattern = fieldInfo.GetValue(regex);

                // Return the pattern
                return pattern.ToString();
            }

            return string.Empty;
        }

        public static void SetPattern(this Regex regex, string pattern)
        {
            if (regex == null)
            {
                throw new ArgumentNullException("regex");
            }

            // Get object's type
            Type objType = regex.GetType();

            // Get the field info for the pattern field
            FieldInfo fieldInfo = objType.GetField("pattern", BindingFlags.Instance | BindingFlags.NonPublic);

            if (fieldInfo != null)
            {
                // Get the value of the pattern
                fieldInfo.SetValue(regex, pattern);
            }
        }

        public static bool GetIgnoreCase(this Regex regex)
        {
            if (regex == null)
            {
                throw new ArgumentNullException("regex");
            }

            // Get object's type
            Type objType = regex.GetType();

            // Get the field info for the pattern field
            FieldInfo fieldInfo = objType.GetField("roptions", BindingFlags.Instance | BindingFlags.NonPublic);


            if (fieldInfo != null)
            {
                // Get the value of the pattern
                object objRegexOptions = fieldInfo.GetValue(regex);

                // Get default option
                RegexOptions enmRegexOptions = objRegexOptions == null ? RegexOptions.None : (RegexOptions)objRegexOptions;

                // Return the pattern
                return (enmRegexOptions & RegexOptions.IgnoreCase) == RegexOptions.IgnoreCase;
            }

            return false;
        }

        public static void SetIgnoreCase(this Regex regex, bool value)
        {
            if (regex == null)
            {
                throw new ArgumentNullException("regex");
            }

            // Get object's type
            Type objType = regex.GetType();

            // Get the field info for the pattern field
            FieldInfo fieldInfo = objType.GetField("roptions", BindingFlags.Instance | BindingFlags.NonPublic);

            if (fieldInfo == null)
            {
                return;
            }

            // Get the value of the pattern
            object objRegexOptions = fieldInfo.GetValue(regex);

            // Get default option
            RegexOptions enmRegexOptions = objRegexOptions == null ? RegexOptions.None: (RegexOptions) objRegexOptions;
            
            // Add IgnoreCase
            if (value && !enmRegexOptions.HasFlag(RegexOptions.IgnoreCase))
            {
                fieldInfo.SetValue(regex, enmRegexOptions | RegexOptions.IgnoreCase);
                return;
            }

            // Remove IgnoreCase
            if (!value && enmRegexOptions.HasFlag(RegexOptions.IgnoreCase))
            {
                fieldInfo.SetValue(regex, enmRegexOptions & ~RegexOptions.IgnoreCase);
            }
        }
    }
}
