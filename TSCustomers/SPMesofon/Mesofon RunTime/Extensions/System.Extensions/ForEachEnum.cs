using System.ComponentModel;
using System.Collections;

namespace System.Extensions
{
	/// <summary>
	/// Class ForEachEnum
	/// </summary>
	[EditorBrowsableAttribute(EditorBrowsableState.Never)]
	internal sealed class ForEachEnum : IEnumerator
	{
		// Fields
		private Collection _mCollectionObject;
		private int _mIndex;

		// Methods

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="coll">The collection.</param>
		public ForEachEnum(Collection coll)
		{
			_mCollectionObject = coll;
		}

		/// <summary>
		/// Adjusts the index of the collection
		/// </summary>
		/// <param name="itemIndex">Index from the collection</param>
		/// <param name="remove">Boolean if the index should be increased or decreased</param>
		public void AdjustIndex(int itemIndex, bool @remove)
		{
			if (itemIndex <= _mIndex)
			{
				if (@remove)
				{
					_mIndex--;
				}
				else
				{
					_mIndex++;
				}
			}
		}

		/// <summary>
		/// Moves to the next index in the collection
		/// </summary>
		/// <returns>True if the index is less than the total number of objects in the collection</returns>
		public bool MoveNext()
		{
			_mIndex++;
			return (_mIndex <= _mCollectionObject.Count);
		}

		/// <summary>
		/// The index is set to 0
		/// </summary>
		public void Reset()
		{
			_mIndex = 0;
		}




		/// <summary>
		/// Gets the current object from the collection
		/// </summary>
		object IEnumerator.Current
		{
			get
			{
				if (_mIndex > _mCollectionObject.Count)
				{
					return null;
				}
				return _mCollectionObject[_mIndex];
			}
		}
	}

}
