namespace System.Extensions
{
	/// <summary>
	/// A boolean retaining its numeric value.
	/// </summary>
	public struct BooleanInt
	{
		/// <summary>
		/// Keeps the integer
		/// </summary>
		private int _value;

		/// <summary>
		/// Integer of the boolean 
		/// </summary>
		/// <param name="value">Required. Integer value.</param>
		public BooleanInt(int value)
		{
			this._value = value;
		}

		public override bool Equals(object obj)
		{
			if (obj is BooleanInt)
			{
				BooleanInt booleanInt = (BooleanInt)obj;
				return (_value == booleanInt._value);
			}
			else
			{
				return false;
			}
		}
		public static bool operator ==(BooleanInt c1, BooleanInt c2)
		{
            return (c1._value == c2._value);
		}

		public static bool operator !=(BooleanInt c1, BooleanInt c2)
		{
            return (c1._value != c2._value);
		}

		public override int GetHashCode()
		{
			return _value;
		}

		/// <summary>
		/// Converts to Boolean.
		/// </summary>
		/// <returns>Boolean value.</returns>
		public bool ToBoolean()
		{
			return _value != 0;
		}

		/// <summary>
		/// Converts to Int32.
		/// </summary>
		/// <returns>Integer value.</returns>
		public int ToInt32()
		{
			return _value;
		}

		/// <summary>
		/// Converts to Double.
		/// </summary>
		/// <returns>Double value.</returns>
		public double ToDouble()
		{
			return _value;
		}

		/// <summary>
		/// Converts to DateTime.
		/// </summary>
		/// <returns>Date Time value</returns>
		public DateTime ToDateTime()
		{
			return DateAndTimeConversion.ConvertToDate(_value);
		}

		/// <summary>
		/// Prints the boolean value.
		/// </summary>
		/// <returns>String value.</returns>
		public override string ToString()
		{
			return ToBoolean().ToString();
		}
	}
}
