﻿namespace System.Extensions
{
    /// <summary>
    /// Binary Operator type.
    /// </summary>
    public enum BinaryOperatorType
    {

        /// <summary>
        /// Less than operator.
        /// </summary>
        LessThan,

        /// <summary>
        /// Greater than operator.
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Less than or equal operator.
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// Greater than or equal operator.
        /// </summary>
        GreaterThanOrEqual,

        /// <summary>
        /// Equality operator.
        /// </summary>
        Equality,

        /// <summary>
        /// Inequality operator.
        /// </summary>
        Inequality,

        /// <summary>
        /// Add operator.
        /// </summary>
        Add,

        /// <summary>
        /// Subtract operator.
        /// </summary>
        Subtract,

        /// <summary>
        /// Bitwise AND operator.
        /// </summary>
        BitwiseAnd,

        /// <summary>
        /// Bitwise OR operator.
        /// </summary>
        BitwiseOr,

        /// <summary>
        /// Logical AND operator.
        /// </summary>
        LogicalAnd,

        /// <summary>
        /// Logical OR operator.
        /// </summary>
        LogicalOr,

        /// <summary>
        /// Exclusive OR operator.
        /// </summary>
        ExclusiveOr
    }

    /// <summary>
    /// Contains runtime methods that replace VB6 operators for certain operands.
    /// </summary>
    public static class BinaryOperators
    {
        
        /// <summary>
        /// Integer-divide Boolean and String, E.g. True \ "3".
        /// </summary>
        /// <param name="left">Required. Boolean value.</param>
        /// <param name="right">Required. String value.</param>
        /// <returns>BooleanInt value.</returns>
        public static BooleanInt BooleanDivide(bool left, string right)
        {
            // Convert right to boolean
            bool rightBoolean = ConversionHelper.StringToBoolean(right);

            if (!rightBoolean)
                throw new DivideByZeroException();

            // Return BooleanInt corresponding to the left boolean value
            return new BooleanInt(left ? 1 : 0);
        }

		 /// <summary>
        /// Compares object values.
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>Boolean value.</returns>
        public static bool ObjectComparison(object left, object right, BinaryOperatorType op)
        {
			return false;
		}
		
        /// <summary>
        /// Compares boolean values.
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>Boolean value.</returns>
        public static bool BooleanComparison(object left, object right, BinaryOperatorType op)
        {
            // Convert operands to bool
            bool leftBool = ConversionHelper.ObjectToBoolean(left),
                rightBool = ConversionHelper.ObjectToBoolean(right);

            // Then, convert to int
            int leftInt = ConversionHelper.BooleanToInt(leftBool),
                rightInt = ConversionHelper.BooleanToInt(rightBool);

            // Compare
            switch (op)
            {
                case BinaryOperatorType.LessThan:
                    return leftInt < rightInt;
                case BinaryOperatorType.GreaterThan:
                    return leftInt > rightInt;
                case BinaryOperatorType.LessThanOrEqual:
                    return leftInt <= rightInt;
                case BinaryOperatorType.GreaterThanOrEqual:
                    return leftInt >= rightInt;
                case BinaryOperatorType.Equality:
                    return leftInt == rightInt;
                case BinaryOperatorType.Inequality:
                    return leftInt != rightInt;
                default:
                    throw new NotSupportedException();
            }
        }

       /// <summary>
        /// Compares numeric values.
       /// </summary>
       /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
       /// <returns>Boolean value.</returns>
        public static bool NumericComparison(object left, object right, BinaryOperatorType op)
        {
           if (left == null || left == DBNull.Value)
           {
               left = 0;
           }

           if (right == null || right == DBNull.Value)
           {
               right = 0;
           }

            // Convert operands to bool
            double leftDouble = ConversionHelper.ObjectToDouble(left),
                rightDouble = ConversionHelper.ObjectToDouble(right);

            // Compare
            switch (op)
            {
                case BinaryOperatorType.LessThan:
                    return leftDouble < rightDouble;
                case BinaryOperatorType.GreaterThan:
                    return leftDouble > rightDouble;
                case BinaryOperatorType.LessThanOrEqual:
                    return leftDouble <= rightDouble;
                case BinaryOperatorType.GreaterThanOrEqual:
                    return leftDouble >= rightDouble;
                case BinaryOperatorType.Equality:
                    return leftDouble == rightDouble;
                case BinaryOperatorType.Inequality:
                    return leftDouble != rightDouble;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Date comparison
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>Boolean value.</returns>
        public static bool DateComparison(object left, object right, BinaryOperatorType op)
        {
            // Convert operands to bool
            DateTime leftDate = ConversionHelper.ObjectToDateTime(left),
                rightDate = ConversionHelper.ObjectToDateTime(right);

            // Compare
            switch (op)
            {
                case BinaryOperatorType.LessThan:
                    return leftDate < rightDate;
                case BinaryOperatorType.GreaterThan:
                    return leftDate > rightDate;
                case BinaryOperatorType.LessThanOrEqual:
                    return leftDate <= rightDate;
                case BinaryOperatorType.GreaterThanOrEqual:
                    return leftDate >= rightDate;
                case BinaryOperatorType.Equality:
                    return leftDate == rightDate;
                case BinaryOperatorType.Inequality:
                    return leftDate != rightDate;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Compares strings.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <param name="op">The op.</param>
        /// <returns>Boolean value.</returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public static bool StringComparison(string left, string right, BinaryOperatorType op)
        {
            int i = string.Compare(left, right,System.StringComparison.OrdinalIgnoreCase);
            switch (op)
            {
                case BinaryOperatorType.LessThan:
                    return i < 0;
                case BinaryOperatorType.GreaterThan:
                    return i > 0;
                case BinaryOperatorType.LessThanOrEqual:
                    return i <= 0;
                case BinaryOperatorType.GreaterThanOrEqual:
                    return i >= 0;
                case BinaryOperatorType.Equality:
                    return i == 0;
                case BinaryOperatorType.Inequality:
                    return i != 0;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Adds date values
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>DateTime value.</returns>
        public static DateTime DateAddition(object left, object right, BinaryOperatorType op)
        {
            // Convert operands to date
            DateTime leftDate = ConversionHelper.ObjectToDateTime(left),
                rightDate = ConversionHelper.ObjectToDateTime(right);

            // Convert right date to TimeSpan
            TimeSpan span = rightDate - DateAndTimeConversion.Zero;

            // Add date and span
            switch (op)
            {
                case BinaryOperatorType.Add:
                    return leftDate + span;
                case BinaryOperatorType.Subtract:
                    return leftDate - span;
                default:
                    throw new NotSupportedException();
            }
        }

       

        /// <summary>
        /// Adds date values
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>DateTime value.</returns>
        public static double DateSubstract(object left, object right, BinaryOperatorType op)
        {
            // Convert operands to date
            DateTime leftDate = ConversionHelper.ObjectToDateTime(left),
                rightDate = ConversionHelper.ObjectToDateTime(right);


            // Add date and span
            switch (op)
            {
                case BinaryOperatorType.Add:
                    throw  new NotImplementedException();
                case BinaryOperatorType.Subtract:
                    return leftDate.Subtract(rightDate).Days + 
                           Convert.ToDouble(leftDate.Subtract(rightDate).Hours) / 24 +
                           Convert.ToDouble(leftDate.Subtract(rightDate).Minutes) / 1440 + 
                           Convert.ToDouble(leftDate.Subtract(rightDate).Seconds) / 86400 + 
                           Convert.ToDouble(leftDate.Subtract(rightDate).Milliseconds) / 86400000;
                default:
                    throw new NotSupportedException();
            }
        }
        
        /// <summary>
        /// Performs a bitwise operation.
        /// </summary>
        /// <param name="left">Required. Object value.</param>
        /// <param name="right">Required. Object value.</param>
        /// <param name="op">Required. BinaryOperatorType value.</param>
        /// <returns>Integer value.</returns>
        public static int BitwiseOperation(object left, object right, BinaryOperatorType op)
        {
            // Convert operands to int
            int leftInt = ConversionHelper.ObjectToInt32(left),
                rightInt = ConversionHelper.ObjectToInt32(right);

            // Perform bitwise operation
            switch (op)
            {
                case BinaryOperatorType.BitwiseAnd:
                case BinaryOperatorType.LogicalAnd:
                    return leftInt & rightInt;
                case BinaryOperatorType.BitwiseOr:
                case BinaryOperatorType.LogicalOr:
                    return leftInt | rightInt;
                case BinaryOperatorType.ExclusiveOr:
                    return leftInt ^ rightInt;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
