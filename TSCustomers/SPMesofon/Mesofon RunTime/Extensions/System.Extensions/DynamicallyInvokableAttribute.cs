﻿namespace System.Extensions
{
    /// <summary>
    /// Each blessed API will be annotated with a "DynamicallyInvokableAttribute".
    /// This "DynamicallyInvokableAttribute" is a type defined in its own assembly.
    /// So the ctor is always a MethodDef and the type a TypeDef.
    /// We cache this ctor MethodDef token for faster custom attribute lookup.
    /// If this attribute type doesn't exist in the assembly, it means the assembly
    /// doesn't contain any blessed APIs.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    public sealed class DynamicallyInvokableAttribute : Attribute
    {
    }
}