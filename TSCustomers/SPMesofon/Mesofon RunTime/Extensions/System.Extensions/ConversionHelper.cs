using System.Collections.Generic;
using System.Globalization;

namespace System.Extensions
{
	static internal class ConversionHelper
	{
		private static HashSet<Type> _numericTypes = new HashSet<Type>(new[] {
			typeof(byte),
			typeof(sbyte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong),
			typeof(float),
			typeof(double),
			typeof(decimal),
		});

		static internal int ObjectToInt32(object value)
		{
			if (value == null)
				throw new InvalidCastException("Couldn't convert the specified value to Int32.");

            if (value is Enum)
                return (int)value;

			if (value is DateTime)
			{
				double totalDays = ((DateTime)value - DateAndTimeConversion.Zero).TotalDays;
				return (int)Math.Round(totalDays, MidpointRounding.AwayFromZero);
			}
			
			string strValue = value as string;
			if (!String.IsNullOrEmpty(strValue))
			{
				double numericValue;
				if (double.TryParse(strValue, out numericValue))
					return (int)Math.Round(numericValue, MidpointRounding.AwayFromZero);
				
				throw new InvalidCastException("Couldn't convert the specified value to Int32.");
			}
			
			if (value is bool?)
			{
				return BooleanToInt((bool)value);
			}
			
			if (value is BooleanInt)
			{
				return ((BooleanInt)value).ToInt32();
			}
			
			Type type = value.GetType();
			if (_numericTypes.Contains(type))
			{
				double numericValue = Convert.ToDouble(value, CultureInfo.CurrentCulture);
				return (int)Math.Round(numericValue, MidpointRounding.AwayFromZero);
			}
			
			throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
		}

		static internal double ObjectToDouble(object value)
		{
			if (value == null)
				throw new InvalidCastException("Couldn't convert the specified value to Double.");
			
			if (value is Enum)
				return (int)value;
			
			if (value is DateTime)
				return (int)((DateTime)value - DateAndTimeConversion.Zero).TotalDays;
			
			string strValue = value as string;
			if (!String.IsNullOrEmpty(strValue))
			{
				double numericValue;
				if (double.TryParse(strValue, out numericValue))
					return numericValue;
				
				throw new InvalidCastException("Couldn't convert the specified value to Int32.");
			}
			
			if (value is bool)
			{
				return BooleanToInt((bool)value);
			}
			
			Type type = value.GetType();
			if (_numericTypes.Contains(type))
			{
				double numericValue = Convert.ToDouble(value, CultureInfo.InvariantCulture);
				return numericValue;
			}
			
			throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
		}

		static internal DateTime ObjectToDateTime(object value)
		{
			if (value == null)
				throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
			
			if (value is DateTime)
				return (DateTime)value;
			
			string strValue = value as string;
			if (!String.IsNullOrEmpty(strValue))
				return DateAndTimeConversion.DateValue(strValue);
			
			if (value is bool)
			{
                int _value = BooleanToInt((bool)value);
                return DateAndTimeConversion.ConvertToDate(_value);
			}
			
			Type type = value.GetType();
			if (_numericTypes.Contains(type))
			{
				double numericValue = Convert.ToDouble(value, CultureInfo.CurrentCulture);
				return DateAndTimeConversion.ConvertToDate(numericValue);
			}
			
			throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
		}

		static internal bool ObjectToBoolean(object value)
		{
			if (value == null)
				throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
			
			if (value is bool)
				return (bool)value;
			
			string strValue = value as string;
			if (!String.IsNullOrEmpty(strValue))
				return StringToBoolean(strValue);
			
			Type type = value.GetType();
			
			if (_numericTypes.Contains(type))
			{
				double numericValue = Convert.ToDouble(value, CultureInfo.InvariantCulture);
				return numericValue != 0;
			}
			
			if (value is DateTime)
			{
				DateTime timeValue = (DateTime)value;
				double elapsedDays = (timeValue - DateAndTimeConversion.Zero).TotalDays;
				
				return elapsedDays != 0;
			}
			
			throw new InvalidCastException("Couldn't convert the specified value to Boolean.");
		}

		static internal bool StringToBoolean(string value)
		{
            bool _value;
            if (bool.TryParse(value, out _value))
                return _value;
			
			double numericValue;
			if (double.TryParse(value, out numericValue))
				return numericValue != 0;
			
			throw new InvalidCastException("The specified string couldn't be converted to boolean");
		}

		static internal int BooleanToInt(bool value)
		{
			return value ? -1 : 0;
		}
	}
}
