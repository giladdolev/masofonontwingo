﻿using System.Extensions.VBExtensions;

namespace System.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns a Date value containing a date and time value to which a specified time interval has been added.
        /// </summary>
        /// <param name="interval">Required. String value.</param>
        /// <param name="number">Required. Double. Floating-point expression representing the number of intervals you want to add. Number can be positive (to get date/time values in the future) or negative (to get date/time values in the past). Any fractional part of Number is ignored.</param>
        /// <param name="dateValue">Required. Date. An expression representing the date and time to which the interval is to be added. DateValue itself is not changed in the calling program.</param>
        /// <returns>DateTime</returns>
        public static DateTime DateAdd(string interval, double number, object dateValue)
        {
            return DateAndTime.DateAdd(interval, number, dateValue);
        }
    }
}
