using System.IO;

namespace System.Extensions
{
	/// <summary>
	/// Facilitates sequential access to file.
	/// </summary>
	public class TextStream : IDisposable
	{

		#region "Private variables"

		/// <summary>
		/// Holds object of StreamReader class
		/// </summary>
		private StreamReader _sR;

		/// <summary>
		/// Holds object of StreamReader class
		/// </summary>
		private StreamWriter _sW;

		/// <summary>
		///  Indicates input/output mode.
		/// </summary>
		public IOModeSettings menmIOMode
		{
			get;
			set;
		}

		/// <summary>
		/// When Appending case this holds the text readed from file
		/// </summary>
		private string _readText;


		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="TextStream"/> class.
		/// </summary>
		/// <param name="strFileName">The filename.</param>
		public TextStream(string strFileName, IOModeSettings iOMode)
		{
			menmIOMode = iOMode;
			switch (menmIOMode) {
				case IOModeSettings.ForAppending:
					_sR = new StreamReader(strFileName);
					_readText = _sR.ReadToEnd();
					break;

				case IOModeSettings.ForReading:
					_sR = new StreamReader(strFileName);
					break;

				case IOModeSettings.ForWriting:
					_sW = new StreamWriter(strFileName);
					break;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TextStream"/> class.
		/// </summary>
		/// <param name="streamWriter">The object stream writer.</param>
		public TextStream(StreamWriter streamWriter)
		{
			menmIOMode = IOModeSettings.ForWriting;
			_sW = streamWriter;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TextStream"/> class.
		/// </summary>
		/// <param name="streamReader">The object stream reader.</param>
		public TextStream(StreamReader streamReader)
		{
			menmIOMode = IOModeSettings.ForReading;
			_sR = streamReader;
		}


		#region "Properties"

		/// <summary>
		/// check if the file pointer is at the end of a TextStream file; 
		/// </summary>
		public bool AtEndOfStream
		{
			get { return _sR.EndOfStream; }
		}

		/// <summary>
		/// Counts lines in the text stream.
		/// </summary>
		public int Line
		{
			get { return CountLines(); }
		}


		#endregion


		#region "Methods"

		/// <summary>
		/// Writes a specified string and newline character to a TextStream file.
		/// </summary>
		/// <param name="text">The text you want to write to the file</param>
		public void WriteLine(string text)
		{
			if (menmIOMode == IOModeSettings.ForAppending)
				_sW.WriteLine(_readText + text);
			_sW.WriteLine(text);
		}

		/// <summary>
		/// Writes a specified string to a TextStream file.
		/// </summary>
		/// <param name="text">The text you want to write to the file</param>
		public void Write(string text)
		{
			_sW.Write(text);
		}

		/// <summary>
		/// Reads an entire line from a TextStream file 
		/// </summary>
		/// <returns>the resulting string.</returns>
		public string ReadLine()
		{
			return _sR.ReadLine();
		}

		/// <summary>
		/// Reads an entire TextStream file
		/// </summary>
		/// <returns>the resulting string.</returns>
		public string ReadAll()
		{
			return _sR.ReadToEnd();
		}

		/// <summary>
		/// Reads a specified number of characters from a TextStream file and returns the resulting string.
		/// </summary>
		/// <param name="characters">Number of characters you want to read from the file.</param>
		/// <returns></returns>
		public string Read(int characters)
		{
			char[] chBuffer = new char[characters];
			_sR.Read(chBuffer, 0, characters);
			
			return new String(chBuffer, 0, characters);
		}

		/// <summary>
		/// Closes an open TextStream file.
		/// </summary>
		public void Close()
		{
			if (_sR != null)
			{
				_sR.Close();
			}
			if (_sW != null)
			{
				_sW.Close();
			}
		}

		#endregion


		#region "Internal Methods"

		public int CountLines()
		{
			int count = 0;
			while ((_sR.ReadLine()) != null)
			{
				count++;
			}
			return count;
		}


		#endregion


		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				
				if (_sR != null)
				{
					_sR.Dispose();
				}
				if (_sW != null)
				{
					_sW.Dispose();
				}
			}
		}
	}


}
