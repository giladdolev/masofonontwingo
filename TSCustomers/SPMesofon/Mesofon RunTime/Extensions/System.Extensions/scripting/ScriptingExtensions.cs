using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Extensions
{
	/// <summary>
	/// ScriptingExtensions class contains method to work with files.
	/// </summary>
	public static class ScriptingExtensions
	{



		/// <summary>
		/// File Attribute
		/// </summary>
		private static class FileAttribute
		{
			/// <summary>
			/// FILE_ATTRIBUTE_NORMAL constant
			/// </summary>
			public const uint FILE_ATTRIBUTE_NORMAL = 0x80;
		}






		/// <summary>
		/// Return file type.
		/// </summary>
		/// <param name="strFileName">Name of the STR file.</param>
		/// <returns>File type</returns>
		public static string FileType(string strFileName)
		{
			NativeMethods.SHFILEINFO info = new NativeMethods.SHFILEINFO();
			
			uint dwFileAttributes = FileAttribute.FILE_ATTRIBUTE_NORMAL;
			uint uFlags = (uint)(NativeMethods.SHGFI.SHGFI_TYPENAME | NativeMethods.SHGFI.SHGFI_USEFILEATTRIBUTES);
			
			NativeMethods.SHGetFileInfo(strFileName, dwFileAttributes, ref info, (uint)Marshal.SizeOf(info), uFlags);
			
			return info.szTypeName;
		}

		/// <summary>
		/// Creates the text file.
		/// </summary>
		/// <param name="strFilePath">The string path.</param>
		/// <returns>TextStream object</returns>
		public static TextStream CreateTextFile(string strFilePath)
		{
			return CreateTextFile(strFilePath, false, false);
		}

		/// <summary>
		/// Creates the text file.
		/// </summary>
		/// <param name="strFilePath">The string path.</param>
		/// <param name="overwrite">if set to <c>true</c> overwrite file.</param>
		/// <returns>TextStream object</returns>
		public static TextStream CreateTextFile(string strFilePath, bool overwrite)
		{
			return CreateTextFile(strFilePath, overwrite, false);
		}

		/// <summary>
		/// Creates the text file.
		/// </summary>
		/// <param name="strFilePath">The string path.</param>
		/// <param name="overwrite">if set to <c>true</c> overwrite file.</param>
		/// <param name="unicode">if set to <c>true</c> create unicode file.</param>
		/// <returns>TextStream object</returns>
		public static TextStream CreateTextFile(string strFilePath, bool overwrite, bool unicode)
		{
			// Make sure the file name is NOT NULL NOR EMPTY
			if (!string.IsNullOrEmpty(strFilePath))
			{
				// Get the ASCII encoding object
				Encoding encoding = Encoding.ASCII;
				
				// Set the file access permissions
				FileAccess fileAccess = overwrite ? FileAccess.ReadWrite : FileAccess.Read;
				
				// if only read permissions are granted
				if (fileAccess == FileAccess.Read)
				{
					// if the file DOESN't exist
					if (!File.Exists(strFilePath))
					{
						// Give ReadWrite permissions (to simulate VB6 behavior)
						fileAccess = FileAccess.ReadWrite;
					}
				}
				
				// if Unicode was specified
				if (unicode)
				{
					// Change encoding to Unicode
					encoding = Encoding.Unicode;
				}
				
				return OpenFile(strFilePath, FileMode.OpenOrCreate, fileAccess, encoding);
			}
			
			return null;
		}

		/// <summary>
		/// Opens the file.
		/// </summary>
		/// <param name="strFilePath">The file path.</param>
		/// <param name="fileMode">The file mode.</param>
		/// <param name="fileAccess">The file access.</param>
		/// <param name="encoding">The encoding.</param>
		/// <returns>TextStream object</returns>
		private static TextStream OpenFile(string strFilePath, FileMode fileMode, FileAccess fileAccess, Encoding encoding, bool append = false)
		{
			// Create a FileStream object with the provided file name
			FileStream fileStream = new FileStream(strFilePath, fileMode, fileAccess);
			
			if (append)
			{
				fileStream.Seek(0, SeekOrigin.End);
			}
			
			// For read-only files reader should be created
			if (fileAccess == FileAccess.Read)
			{
				// Create a StreamWriter for writing to the file
				StreamReader streamReader = new StreamReader(fileStream, encoding);
				
				// Return stream writer to caller
				return new TextStream(streamReader);
			}
			else
			{
				// Create a StreamWriter for writing to the file
				StreamWriter streamWriter = new StreamWriter(fileStream, encoding);
				
				// Set auto flush to mimic legacy behavior
				streamWriter.AutoFlush = true;
				
				// Return stream writer to caller
				return new TextStream(streamWriter);
			}
		}

		/// <summary>
		/// Opens the text file.
		/// </summary>
		/// <param name="strFilePath">The string file path.</param>
		/// <returns>
		/// TextStream object
		/// </returns>
		/// <exception cref="System.Extensions.CompatibilityException"></exception>
		/// <exception cref="System.IO.FileNotFoundException"></exception>
		public static TextStream OpenTextFile(string strFilePath)
		{
			return OpenTextFile(strFilePath, IOModeSettings.ForReading, false, TristateEnum.False);
		}

		/// <summary>
		/// Opens the text file.
		/// </summary>
		/// <param name="strFilePath">The string file path.</param>
		/// <param name="iOMode">The file IO mode (Read or Append).</param>
		/// <returns>
		/// TextStream object
		/// </returns>
		/// <exception cref="System.Extensions.CompatibilityException"></exception>
		/// <exception cref="System.IO.FileNotFoundException"></exception>
		public static TextStream OpenTextFile(string strFilePath, IOModeSettings iOMode)
		{
			return OpenTextFile(strFilePath, iOMode, false, TristateEnum.False);
		}

		/// <summary>
		/// Opens the text file.
		/// </summary>
		/// <param name="strFilePath">The string file path.</param>
		/// <param name="iOMode">The file IO mode (Read or Append).</param>
		/// <param name="create">if set to <c>true</c> [BLN create].</param>
		/// <returns>
		/// TextStream object
		/// </returns>
		/// <exception cref="System.Extensions.CompatibilityException"></exception>
		/// <exception cref="System.IO.FileNotFoundException"></exception>
		public static TextStream OpenTextFile(string strFilePath, IOModeSettings iOMode, bool create)
		{
			return OpenTextFile(strFilePath, iOMode, create, TristateEnum.False);
		}

		/// <summary>
		/// Opens the text file.
		/// </summary>
		/// <param name="strFilePath">The string file path.</param>
		/// <param name="iOMode">The file IO mode (Read or Append).</param>
		/// <param name="create">if set to <c>true</c> [BLN create].</param>
		/// <param name="format">The format.</param>
		/// <returns>
		/// TextStream object
		/// </returns>
		/// <exception cref="System.Extensions.CompatibilityException"></exception>
		/// <exception cref="System.IO.FileNotFoundException"></exception>
		public static TextStream OpenTextFile(string strFilePath, IOModeSettings iOMode, bool create, TristateEnum format)
		{
			Encoding encoding = GetEncoding(format);
			
			if (!File.Exists(strFilePath))
			{
				if (!create)
				{
					// Raise legacy compatible file not found exception
					throw new CompatibilityException(new FileNotFoundException());
				}
				switch (iOMode) {
					case IOModeSettings.ForReading:
						// Create empty file and open for read
						return OpenFile(strFilePath, FileMode.Create, FileAccess.Read, encoding);

					case IOModeSettings.ForWriting:
					case IOModeSettings.ForAppending:
						// Create empty file and open for write
						return OpenFile(strFilePath, FileMode.Create, FileAccess.Write, encoding);

				}
			}
			
			switch (iOMode) {
				case IOModeSettings.ForReading:
					// Open existing file for read
					return OpenFile(strFilePath, FileMode.Open, FileAccess.Read, encoding);

				case IOModeSettings.ForWriting:
					// Open existing file for write
					return OpenFile(strFilePath, FileMode.Create, FileAccess.Write, encoding);

				case IOModeSettings.ForAppending:
					// Open existing file for append (writer location at the end of the file stream)
					return OpenFile(strFilePath, FileMode.Create, FileAccess.Write, encoding, true);
			}
			
			// All options processed, we should no ever reach here
			return null;
		}

		/// <summary>
		/// Gets the special folder.
		/// </summary>
		/// <param name="specialFolderType">Type of the special folder.</param>
		/// <returns>Path to special folder.</returns>
		public static string GetSpecialFolder(SpecialFolderEnum specialFolderType)
		{
			switch (specialFolderType) {
				case SpecialFolderEnum.WindowsFolder:
					return Environment.GetFolderPath(Environment.SpecialFolder.Windows);

				case SpecialFolderEnum.SystemFolder:
					return Environment.GetFolderPath(Environment.SpecialFolder.SystemX86);

				case SpecialFolderEnum.TemporaryFolder:
					return Path.GetTempFileName();
			}
			return null;
		}

		/// <summary>
		/// Gets the encoding.
		/// </summary>
		/// <param name="format">The format.</param>
		/// <returns>Encoding object.</returns>
        private static Encoding GetEncoding(TristateEnum enmFormat)
		{
		    //TriStateEnum Encoding Values: https://msdn.microsoft.com/en-us/library/aa265347(v=vs.60).aspx
            if (enmFormat == TristateEnum.False)
            {
                return Encoding.ASCII;
            }
		    if (enmFormat == TristateEnum.True)
		    {
		        return Encoding.Unicode;
		    }
		    return Encoding.Default;
		}


	    /// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="strPath">The string path.</param>
		/// <param name="force">if set to <c>true</c> delete read-only files.</param>
		public static void DeleteFile(string strPath, bool force)
		{
			// Get containing folder
			string strDirectory = Path.GetDirectoryName(strPath);
			
			// Get files
			string[] files = Directory.GetFiles(strDirectory, strPath, SearchOption.AllDirectories);
			
			// Process all files found
			foreach (string strFile in files)
			{
				if (force)
				{
					// Ensure file can be deleted
					File.SetAttributes(strFile, FileAttributes.Normal);
				}
				
				// Delete file
				File.Delete(strFile);
			}
		}



		/// <summary>
		/// Determines whether drive exists.
		/// </summary>
		/// <param name="strDrive">The drive.</param>
		/// <returns>If driver exists or not.</returns>
		public static bool IsDriveExists(string strDrive)
		{
			return DriveInfo.GetDrives().FirstOrDefault(x => x.Name == strDrive) != null;
		}

		/// <summary>
		/// Gets the size of the directory.
		/// </summary>
		/// <param name="strPath">The string path.</param>
		/// <returns>Directory size</returns>
		public static long GetDirectorySize(string strPath)
		{
			long lSize = 0L;
			DirectoryInfo directory = new DirectoryInfo(strPath);
			foreach (FileInfo file in directory.GetFiles())
			{
				lSize += file.Length;
			}
			foreach (DirectoryInfo subDirectory in directory.GetDirectories())
			{
				lSize += GetDirectorySize(subDirectory.FullName);
			}
			return lSize;
		}


	}
}
