﻿namespace System.Extensions
{
    #region "Enums"

    /// <summary>
    ///  Indicates input/output mode.
    /// </summary>
    public enum IOModeSettings
    {
        /// <summary>
        /// For reading
        /// </summary>
        ForReading,
        /// <summary>
        /// For writing
        /// </summary>
        ForWriting,
        /// <summary>
        /// For appending
        /// </summary>
        ForAppending
    }

    public enum TristateEnum
    {
        /// <summary>
        /// Opens the file as Unicode
        /// </summary>
        True,
        /// <summary>
        /// Opens the file as ASCII
        /// </summary>
        False,
        /// <summary>
        /// Use default system setting
        /// </summary>
        UseDefault
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SpecialFolderEnum
    {
        /// <summary>
        /// The windows folder
        /// </summary>
        WindowsFolder,
        /// <summary>
        /// The system folder
        /// </summary>
        SystemFolder,
        /// <summary>
        /// The temporary folder
        /// </summary>
        TemporaryFolder
    }

    #endregion
}
