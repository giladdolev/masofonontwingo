using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Web;

namespace System.Extensions
{
	[SerializableAttribute()]
    public class CompatibilityException : Exception
    {

        #region Fields

        /// <summary>
        /// Error number
        /// </summary>
		private int _number = 0;

        /// <summary>
        /// File name
        /// </summary>
		private string _mstrFile = String.Empty;

        /// <summary>
        /// Line number
        /// </summary>
		private int _line = 0;

        /// <summary>
        /// Column Number
        /// </summary>
		private int _column = 0;


        #endregion


        #region Contructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompatibilityException"/> class.
        /// </summary>
        public CompatibilityException()
        {

        }

        /// <summary>
        /// Deserializer constructor
        /// </summary>
        /// <param name="info">SerializationInfo object</param>
        /// <param name="ctx">StreamingContext object</param>
		protected CompatibilityException(SerializationInfo info, StreamingContext ctx) : base(info, ctx)
        {
            // Get error number
			_number = info.GetInt32("number");
        }


        /// <summary>
        /// Constructor
        /// </summary>
		/// <param name="innerException">An internal Exception object</param>
		public CompatibilityException(Exception innerException) : this(innerException == null ? "" : innerException.Message, innerException)
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
		/// <param name="errorCode">The error code</param>
        /// <param name="strMessage">The error message</param>
		public CompatibilityException(int errorCode, string strMessage) : base(strMessage)
        {
            //Save the value of the error code
			_number = errorCode;

        }

        /// <summary>
		/// Initializes a new instance of the <see cref="CompatibilityException"/> class.
		/// </summary>
		/// <param name="strMessage">The string message.</param>
		public CompatibilityException(string strMessage) : base(strMessage)
		{
			// Get default error number
			_number = 0;
		}

		/// <summary>
        /// Constructor
        /// </summary>
		/// <param name="errorCode">The error code</param>
        /// <param name="strMessage">The error message</param>
		/// <param name="innerException">An internal Exception object</param>
		public CompatibilityException(int errorCode, string strMessage, Exception innerException) : this(strMessage, innerException)
        {
            //Save the value of the error code
			_number = errorCode;

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strMessage">The error message</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(string strMessage, string strSource, Exception objInnerException)
            : base(strMessage, objInnerException)
        {
            // Save the value of source
            Source = strSource;
        }

        /// <summary>
        /// Gets or sets the name of the application or the object that causes the error.
        /// </summary>
        public override sealed string Source
        {
			get { return base.Source; }
			set { base.Source = value; }
            }


        /// <summary>
        /// Maps the exception automatic number.
        /// </summary>
		/// <param name="exception">The object Exception.</param>
        /// <returns>The number corresponding to the exception type.</returns>
		public static int MapExceptionToNumber(Exception exception)
        {
			
			if (exception == null)
            {
                throw new ArgumentNullException("exception");
			}
			Type type = exception.GetType();
			if (type == typeof(IndexOutOfRangeException))
			{
                return 9;
            }
			if (type == typeof(RankException))
            {
                return 9;
            }
			if (type == typeof(DivideByZeroException))
            {
                return 11;
            }
			if (type == typeof(OverflowException))
            {
                return 6;
            }
			if (type == typeof(NotFiniteNumberException))
            {
                NotFiniteNumberException notFiniteNumberException = (NotFiniteNumberException)exception;
                if (notFiniteNumberException.OffendingNumber == 0.0)
                {
                    return 11;
                }
                return 6;
            }
			if (type == typeof(NullReferenceException))
            {
                return 0x5b;
            }
			if (exception is AccessViolationException)
            {
                return -2147467261;
            }
			if (type == typeof(InvalidCastException))
            {
                return 13;
            }
			if (type == typeof(NotSupportedException))
            {
                return 13;
            }
			if (type == typeof(COMException))
            {
				return ((COMException)exception).ErrorCode;
            }
			if (type == typeof(SEHException))
            {
                return 0x63;
            }
			if (type == typeof(DllNotFoundException))
            {
                return 0x35;
            }
			if (type == typeof(EntryPointNotFoundException))
            {
                return 0x1c5;
            }
			if (type == typeof(TypeLoadException))
            {
                return 0x1ad;
            }
			if (type == typeof(OutOfMemoryException))
            {
                return 7;
            }
			if (type == typeof(FormatException))
            {
                return 13;
            }
			if (type == typeof(DirectoryNotFoundException))
            {
                return 0x4c;
            }
			if (type == typeof(IOException))
            {
                return 0x39;
            }
			if (type == typeof(FileNotFoundException))
            {
                return 0x35;
            }
			if (exception is MissingMemberException)
            {
                return 0x1b6;
            }
			if (exception is InvalidOleVariantTypeException)
            {
                return 0x1ca;
            }

			HttpUnhandledException httpUnhandledException = exception as HttpUnhandledException;
			if (httpUnhandledException != null)
            {
                return httpUnhandledException.WebEventCode;
            }
            return 0;
        }





        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strMessage">The error message</param>
		/// <param name="innerException">An internal Exception object</param>
		public CompatibilityException(string strMessage, Exception innerException) : base(strMessage, innerException)
        {

            // If it's a COMException set the error number to  the error code inside the COMException object
			COMException cOMException = innerException as COMException;
			if (cOMException != null)
            {
				_number = cOMException.ErrorCode;
            }

            // If it's a web Error, get the error code from WebEventCode property
			HttpUnhandledException httpUnhandledException = innerException as HttpUnhandledException;
            if (httpUnhandledException != null)
            {
				_number = httpUnhandledException.WebEventCode;

                // Get the current HTTPContext
				HttpContext context = HttpContext.Current;

				if (context != null)
                {
                    // Get file name from path in request
					_mstrFile = Path.GetFileName(context.Request.PhysicalPath);
                }
            }

            // Create a StackTrace object
			StackTrace stackTrace = new StackTrace(innerException, true);
            // Get the top stack frame
			StackFrame stackFrame = stackTrace.GetFrame(0);
            // Get the line number from the stack frame

            // Get the line number from StackTrace object
			_line = stackFrame.GetFileLineNumber();
            // Get the column number from StackTrace object
			_column = stackFrame.GetFileColumnNumber();

       }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="strMessage">The error message</param>
        public CompatibilityException(int intErrorCode, string strSource, string strMessage)
            : this(intErrorCode, strMessage)
        {
            // Save the value of source
            this.Source = strSource;

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="strMessage">The error message</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(int intErrorCode, string strSource, string strMessage, Exception objInnerException)
            : this(intErrorCode, strMessage, objInnerException)
        {
            // Save the value of source
            this.Source = strSource;

        }













        #endregion


        #region Properties

        /// <summary>
        /// Returns the error number
        /// </summary>
        public int Number
        {
			get { return _number; }
        }

        /// <summary>
        /// Returns a string that indicates whether the error was generated by IIS, a scripting language, or a component.
        /// </summary>
        public string Category
        {
            get
            {
                // If the exception is not empty and the inner exception is not empty
				if (InnerException != null)
                {
                    // Return the inner exception name : this is an adapter behavior and might be wrong.
					return InnerException.GetType().Name;
                }

                // Return the type of error
				return GetType().Name;
            }
        }

        /// <summary>
        /// Returns the file name of the Exception
        /// </summary>
        public string File
        {
			get { return _mstrFile; }
        }

        /// <summary>
        /// Returns a short description of the error.
        /// </summary>
        public string Description
        {
			get { return Message; }
        }




        /// <summary>
        /// Indicates the line within the .aspx file that generated the error.
        /// </summary>
        public int Line
        {
			get { return _line; }
        }


        /// <summary>
        /// Returns a value that indicates the column position within the .asp file that generated the error.
        /// </summary>
        public int Column
        {
			get { return _column; }
        }





        /// <summary>
        /// Returns the Description of ASP exceptions
        /// </summary>
        public string ASPDescription
        {
			get { return Description; }
        }

        /// <summary>
        /// Returns the code of ASP exceptions
        /// </summary>
        public string ASPCode
        {
			get { return Number.ToString(CultureInfo.InvariantCulture); }
        }













        #endregion


        #region Serialization

        /// <summary>
        /// sets the System.Runtime.Serialization.SerializationInfo with information about the exception.
        /// </summary>
		/// <param name="info">SerializationInfo object</param>
		/// <param name="context">StreamingContext object</param>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // Call the base
			base.GetObjectData(info, context);

            // Add  "number" for the SerializationInfo object
			info.AddValue("number", _number);
        }














        #endregion
    }
}
