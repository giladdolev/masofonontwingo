﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Gizmox Transposition")]
[assembly: AssemblyProduct("Transposition Studio Extensions")]
[assembly: AssemblyCopyright("Copyright © 2005-2014 Gizmox Transposition")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.2.5")]
[assembly: AssemblyFileVersion("1.2.5")]
[assembly: CLSCompliant(true)]

