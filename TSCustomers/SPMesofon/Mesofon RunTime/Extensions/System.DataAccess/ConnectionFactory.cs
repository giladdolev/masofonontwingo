﻿using System.Configuration;
using System.Data;
using System.Data.Common;

namespace System.DataAccess
{
    /// <summary>
    /// Creates provider-independed connection
    /// </summary>
    public class ConnectionFactory
    {
        private readonly DbProviderFactory _provider;
        private readonly string _connectionString;
        private readonly string _name;

        public ConnectionFactory() : this("default")
        {

        }

        public ConnectionFactory(string connectionName)
        {
            try
            {
                ConnectionStringSettings connectionStringSetting = ConfigurationManager.ConnectionStrings[connectionName];
                if (connectionStringSetting != null)
                {
                    _name = connectionStringSetting.ProviderName;
                    //Rem By galil CS - for Superpharam
                    //_provider = DbProviderFactories.GetFactory(connectionStringSetting.ProviderName);
                    _connectionString = connectionStringSetting.ConnectionString;
                }
            }
            catch (Exception)
            {
                // THis empty exception use to prevent error messages in WinForms designer

            }
        }

        /// <summary>
        /// Creates new connection for Sybase only.
        /// TODO refactor for sybase - done for superpharam
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ConfigurationErrorsException"></exception>
        public IDbConnection CreateAse()
        {
            var connection = new Sybase.Data.AseClient.AseConnection(_connectionString); 
            if (connection == null)
            {
                System.Diagnostics.EventLog.WriteEntry("Mesofon", string.Format("Failed to create a connection using the connection string named '{0}' in web.config.", _name), System.Diagnostics.EventLogEntryType.Error);
                throw new ConfigurationErrorsException(string.Format("Failed to create a connection using the connection string named '{0}' in web.config.", _name));
            }
            connection.ConnectionString = _connectionString;
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Creates new connection.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ConfigurationErrorsException"></exception>
        public IDbConnection Create()
        {
            if (_provider == null)
            {
                return null;
            }
            var connection = _provider.CreateConnection();
            if (connection != null)
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
            }
            return connection;
        }
    }
}