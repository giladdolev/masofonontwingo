﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Common.Transposition.Extensions;
using IsolationLevel = System.Data.IsolationLevel;

namespace System.DataAccess
{
    /// <summary>
    /// An unit of work implementation which can work with or without transaction
    /// </summary>
    /// <seealso cref="Common.Transposition.Extensions.IUnitOfWork" />
    /// <seealso cref="System.IDisposable" />
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private List<IRepository> _repositories;
        private readonly IsolationLevel _isolationLevel = IsolationLevel.Unspecified;

        /// <summary>
        /// </summary>
        public UnitOfWork()
        {

        }

        public UnitOfWork(IsolationLevel isolationLevel)
        {
            _isolationLevel = isolationLevel;
        }


        private IDbConnection Connection
        {
            get
            {
                return _connection = _connection ?? new ConnectionFactory().CreateAse();
            }
        }

        /// <summary>
        /// Gets the repositories.
        /// </summary>
        /// <value> 
        /// The repositories.
        /// </value>
        public List<IRepository> Repositories
        {
            get { return _repositories = _repositories ?? new List<IRepository>(); }

        }

        /// <summary>
        /// Commit changes.
        /// </summary>
        public void SaveChanges()
        {
            if (_isolationLevel != IsolationLevel.Unspecified)
            {
                _transaction = Connection.BeginTransaction(_isolationLevel);
            }
            foreach (IRepository repository in _repositories)
            {
                repository.Update(this);
            }

            if (_transaction != null)
            {
                _transaction.Commit();
                _transaction = null;
            }
        }

        /// <summary>
        /// Retrieves the specified select query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">The select query.</param>
        /// <param name="map">The map.</param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public List<T> Retrieve<T>(string sql, Action<IDataRecord, T> map, CommandType commandType, params object[] parameters) where T : new()
        {
            List<T> list = new List<T>();
            bool closeConnectionInFinally = false;
            try
            {
                if (Connection != null && !string.IsNullOrEmpty(sql))
                {

                    using (var command = Connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.CommandType = commandType;
                        command.CommandTimeout = 60;
                        if (parameters != null)
                        {
                            //var modSql = sql;
                            for (int i = 0; i < parameters.Length; i++)
                            {
                                //modSql = modSql.Replace("@" + i, Convert.ToString(parameters[i]));
                                AddParameter(command, "@" + i, parameters[i]);
                            }
                        }

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                T item = new T();
                                map(reader, item);
                                list.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                closeConnectionInFinally = true;
            }
            finally
            {
                if (closeConnectionInFinally)
                {
                    CloseConnection();
                }
            }
            return list;

        }


        /// <summary>
        /// Retrieves the specified select query.
        /// </summary>
        /// <param name="sql">The select query.</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IDataReader Retrieve(string sql, Dictionary<string, object> parameters = null)
        {
            IDataReader reader = null;
            bool closeConnectionInFinally = false;
            try
            {
                if (Connection == null)
                {
                    return null;
                }

                using (var command = Connection.CreateCommand())
                {
                    command.CommandText = sql;

                    if (parameters != null)
                    {
                        foreach (var param in parameters)
                        {
                            AddParameter(command, param.Key, param.Value);
                        }
                    }
                    reader = command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                closeConnectionInFinally = true;
            }
            finally
            {
                if (closeConnectionInFinally)
                {
                    CloseConnection();
                }
            }
            return reader;
        }

        /// <summary>
        /// Executes the specified SQL with list of parameters.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <param name="parameters">The parameters.</param>
        public void Execute(string sql, Dictionary<string, object> parameters)
        {
            bool closeConnectionInFinally = false;
            try
            {
                using (IDbCommand command = Connection.CreateCommand())
                {
                    var modSql = sql;
                    try
                    {
                        command.CommandText = sql;
                        foreach (var param in parameters)
                        {
                            var search = param.Key.StartsWith("@") ? param.Key : "@" + param.Key;
                            modSql = modSql.Replace(search, Convert.ToString(param.Value));
                            if (sql.Contains(search))
                            {
                                AddParameter(command, param.Key, param.Value);
                            }
                        }
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        closeConnectionInFinally = true;
                        System.Diagnostics.EventLog.WriteEntry("Mesofon", e.Message + "Trace" + e.StackTrace + "   query: " + modSql, System.Diagnostics.EventLogEntryType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                closeConnectionInFinally = true;
            }
            finally
            {
                if (closeConnectionInFinally)
                {
                    CloseConnection();
                }
            }
        }

        /// <summary>
        /// Adds the query parameter.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">The parameter value.</param>
        protected void AddParameter(IDbCommand cmd, string parameterName, object parameterValue)
        {
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = parameterName;
            if (parameterValue == null || (parameterValue is DateTime && Convert.ToString(parameterValue) == "1/1/0001 12:00:00 AM"))
            {
                param.DbType = DbType.String;
                param.Value = DBNull.Value;
            }
            else
            {
                param.Value = parameterValue;
            }
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debug.WriteLine("ParamName : " + param.ParameterName + "  / Value :" + param.Value);
            }
            cmd.Parameters.Add(param);
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <typeparam name="T">Field data type</typeparam>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="convert">The conversion method.</param>
        /// <returns></returns>
        public static T GetValue<T>(IDataRecord reader, string fieldName, Func<object, T> convert)
        {
            int ordinal = reader.GetOrdinal(fieldName);
            if (ordinal >= 0 && !reader.IsDBNull(ordinal))
            {
                try
                {
                    return convert(reader.GetValue(ordinal));
                }
                catch (InvalidCastException ex)
                {
                    Debug.WriteLine(String.Format("Invalid type {0} of field {1}\n{2}", typeof(T).Name, fieldName, ex));
                }
            }

            return default(T);
        }


        public static T GetValue<T>(IDataRecord reader, int ordinal, Func<object, T> convert)
        {
            if (ordinal >= 0 && !reader.IsDBNull(ordinal))
            {
                try
                {
                    return convert(reader.GetValue(ordinal));
                }
                catch (InvalidCastException ex)
                {
                    Debug.WriteLine(String.Format("Invalid type {0} of field number {1}\n{2}", typeof(T).Name, ordinal, ex));
                }
            }

            return default(T);
        }


        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        public static DateTime? GetDateTime(IDataRecord reader, string fieldName)
        {
            int ordinal = reader.GetOrdinal(fieldName);
            if (ordinal >= 0 && !reader.IsDBNull(ordinal))
            {
                try
                {
                    return reader.GetDateTime(ordinal);
                }
                catch (InvalidCastException ex)
                {
                    Debug.WriteLine(String.Format("Invalid type {0} of field {1}\n{2}", typeof(DateTime).Name, fieldName, ex));
                }
            }

            return null;
        }

        public void Dispose()
        {
            try
            {
                if (_transaction != null)
                {
                    _transaction.Rollback();
                    _transaction.Dispose();
                    _transaction = null;
                }
            }

            catch
            {

            }
            finally
            {
                CloseConnection();
            }
        }

        public void CloseConnection()
        {
            if (_connection != null)
            {
                _connection.Close();
            }
        }
    }
}