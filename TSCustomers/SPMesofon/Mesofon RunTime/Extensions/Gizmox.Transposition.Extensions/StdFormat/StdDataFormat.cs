using Microsoft.VisualBasic;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Allows formatting to be applied to data as it is read from and written to a database.
    /// </summary>
    public class StdDataFormat
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public FormatType Type { get; set; }
        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        public string Format { get; set; }
        /// <summary>
        /// Gets or sets the true value.
        /// </summary>
        /// <value>
        /// The true value.
        /// </value>
        public object TrueValue { get; set; }
        /// <summary>
        /// Gets or sets the false value.
        /// </summary>
        /// <value>
        /// The false value.
        /// </value>
        public object FalseValue { get; set; }
        /// <summary>
        /// Gets or sets the null value.
        /// </summary>
        /// <value>
        /// The null value.
        /// </value>
        public object NullValue { get; set; }
        /// <summary>
        /// Gets or sets the first day of week.
        /// </summary>
        /// <value>
        /// The first day of week.
        /// </value>
        public FirstDayOfWeek FirstDayOfWeek { get; set; }
        /// <summary>
        /// Gets or sets the first week of year.
        /// </summary>
        /// <value>
        /// The first week of year.
        /// </value>
        public FirstWeekOfYear FirstWeekOfYear { get; set; }
    }
}

