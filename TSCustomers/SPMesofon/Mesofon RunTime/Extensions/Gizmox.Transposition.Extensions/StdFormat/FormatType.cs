namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Specifies the format type.
    /// </summary>
    public enum FormatType
    {
        /// <summary>
        /// Format type is general
        /// </summary>
        fmtGeneral,
        /// <summary>
        /// Format type is custom
        /// </summary>
        fmtCustom,
        /// <summary>
        /// Format type is picture
        /// </summary>
        fmtPicture,
        /// <summary>
        /// Format type is object
        /// </summary>
        fmtObject,
        /// <summary>
        /// Format type is checkbox
        /// </summary>
        fmtCheckBox,
        /// <summary>
        /// Format type is boolean
        /// </summary>
        fmtBoolean,
        /// <summary>
        /// Format type is bytes
        /// </summary>
        fmtBytes
    }
}

