﻿using System;
using System.Runtime.InteropServices;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// The NativeMethods class
    /// </summary>
    internal class NativeMethods
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="NativeMethods"/> class from being created.
        /// </summary>
        private NativeMethods()
        {
                
        }
        
        /// <summary>
        /// The Integer pointer
        /// </summary>
        internal static IntPtr NullIntPtr;

        //support vb6
        /// <summary>
        /// The GetDC function retrieves a handle to a device context (DC) for the client area of a specified window or for the entire screen.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose DC is to be retrieved. If this value is NULL, GetDC retrieves the DC for the entire screen.</param>
        /// <returns>If the function succeeds, the return value is a handle to the DC for the specified window's client area.</returns>
        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr GetDC(IntPtr hWnd);

        /// <summary>
        /// The ReleaseDC function releases a device context (DC), freeing it for use by other applications. 
        /// </summary>
        /// <param name="hWnd">A handle to the window whose DC is to be released.</param>
        /// <param name="hDC">A handle to the DC to be released.</param>
        /// <returns>The return value indicates whether the DC was released. If the DC was released, the return value is 1.</returns>
        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        /// <summary>
        /// Retrieves device-specific information for the specified device.
        /// </summary>
        /// <param name="hDC">A handle to the DC.</param>
        /// <param name="nIndex">The item to be returned. </param>
        /// <returns>The return value specifies the value of the desired item.</returns>
        [DllImport("gdi32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern int GetDeviceCaps(IntPtr hDC, int nIndex);        
    }
}
