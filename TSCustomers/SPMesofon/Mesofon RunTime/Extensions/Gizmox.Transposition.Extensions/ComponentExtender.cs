﻿using System;
using System.Collections.Generic;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// ComponentExtender class
    /// </summary>
    internal class ComponentExtender : Dictionary<object, object>
    {
        /// <summary>
        /// This method does nothing. It exists merely so that the extended component can hold a direct reference to the extender,
        /// avoiding memory leaks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void DoNothing(object sender, EventArgs e)
        {
            // Do nothing
        }
    }
}
