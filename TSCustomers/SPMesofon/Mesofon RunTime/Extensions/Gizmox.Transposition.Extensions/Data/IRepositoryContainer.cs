﻿namespace Common.Transposition.Extensions.Data
{
    public interface IRepositoryContainer
    {
        IRepository Repository { get; }
    }
}
