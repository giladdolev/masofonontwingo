﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace Common.Transposition.Extensions
{

    public interface IRepository<T> : IRepository where T : ModelBase, new()
    {
        List<T> PrimaryList { get; set; }
    }

    public interface IRepository
    {

        /// <summary>
        /// update row data
        /// </summary>
        /// <param name="row"></param>
        /// <param name="sourceBuffer"></param>
        /// <param name="target"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        bool UpdateRow(int row, ModelBuffer sourceBuffer, IRepository target, int rowIndex);

        /// <summary>
        /// Updates the database with the changes made
        /// </summary>
        /// <param name="uow"></param>
        /// <returns></returns>
        long Update(IUnitOfWork uow);

        /// <summary>
        /// Obtains the number of rows that are currently available in the primary buffer.
        /// </summary>
        /// <returns>Returns the number of rows that are currently available, 0 if no rows are currently available</returns>
        int RowCount();

        /// <summary>
        /// Retrieves data from the database
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns>Returns the number of rows displayed</returns>
        long Retrieve(params object[] parameters);
        //gilad test
        long Retrieve2(params object[] parameters);

        /// <summary>
        /// Typical uses for Modify are:
        /// Changing the update status of different tables in the DataWindow so that you can update more than one table
        /// Modifying the WHERE clause of the DataWindow object’s SQL SELECT statement
        /// Turning on Query mode or Prompt For Criteria so users can specify the data they want
        /// Changing the status of Retrieve Only As Needed
        /// Changing the data source of the DataWindow object
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// Returns the empty string (“”) if it succeeds and an error message if an error occurs. 
        /// The error message takes the form "Line n Column n incorrect syntax". The character columns are counted from the beginning of the compiled text of modstring.
        /// If any argument’s value is null, in PowerBuilder and JavaScript the method returns null.
        /// </returns>
        string Modify(string expression);

 
        /// <summary>
        /// Evaluate expressions involving values of a particular row and column. When you include Describe’s Evaluate function in the property list, the value of the evaluated expression is included in the reported information.
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>
        /// If the property list contains an invalid item, Describe returns an exclamation point (!) for that item and ignores the rest of the property list. Describe returns a question mark (?) if there is no value for a property.
        /// </returns>
        string Describe(string expression);

        /// <summary>
        /// Gets the value of an item for the specified row and column
        /// </summary>
        /// <typeparam name="T1">The type of column as a return value</typeparam>
        /// <param name="row">A value identifying the row location of the data.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="buffer">The buffer from which you want to get item</param>
        /// <param name="isOriginalValue"></param>
        /// <returns>Value of an item for the specified row and column</returns>
        T1 GetItemValue<T1>(long row, string columnName, ModelBuffer buffer = ModelBuffer.Primary, bool isOriginalValue = false);


        /// <summary>
        /// Gets the values of an item for the specified column
        /// </summary>
        /// <typeparam name="T1">The type of column as a return value</typeparam>
        /// <param name="columnName"></param>
        /// <param name="buffer"></param>
        /// <returns>Values of an items for the specified columns</returns>
        T1[] GetItemValues<T1>(string columnName, ModelBuffer buffer = ModelBuffer.Primary);

        /// <summary>
        /// Gets an item from buffer for specific row
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="buffer"></param>
        /// <returns>Returns the item in the specified row, and null or 0 if an error occurs</returns>
        T GetItem<T>(int row, ModelBuffer buffer = ModelBuffer.Primary);


        /// <summary>
        /// Get number of the rows that have been deleted.
        /// </summary>
        /// <returns>Returns 0 if no rows have been deleted or if all the deleted rows have been updated in the database.</returns>
        long DeletedCount();

        /// <summary>
        /// Get number of the rows that have been modified.
        /// </summary>
        /// <returns>Returns the number of rows that have been modified</returns>
        long ModifiedCount();

        /// <summary>
        /// Sets the value of a row and column in a Repository to the specified value.
        /// </summary>
        /// <param name="row">The row location of the data.</param>
        /// <param name="column">The column location of the data.</param>
        /// <param name="value">The value to which you want to set the data at the row and column location. The datatype of the value must be the same datatype as the column.</param>
        /// <returns></returns>
        long SetItem(long row, object column, object value);

        int AcceptText();

        /// <summary>
        /// Deletes the specified row.
        /// </summary>
        /// <param name="row">A value identifying the row you want to delete. To delete the current row, specify 0 for row.</param>
        void Delete(int row);

        /// <summary>
        /// A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.
        /// </summary>
        /// <param name="dataViewElement">The data view element.</param>
        /// <param name="row">A value identifying the row before which you want to insert a row. To insert a row at the end, specify 0.</param>
        /// <returns>Returns the number of the row that was added if it succeeds and –1 if an error occurs.</returns>
        int Insert(int row);

        /// <summary>
        /// Clears a range of rows from one repository and inserts them in another. Alternatively, RowsMove moves rows from one buffer to another within a single repository
        /// </summary>
        /// <param name="startRow">The number of the first row you want to move.</param>
        /// <param name="endRow">The number of the last row you want to move.</param>
        /// <param name="source`er">identifying the buffer from which you want to move the rows.</param>
        /// <param name="target">The name of the repository to which you want to move the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the moved rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to move the rows..</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int RowsMove(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow,
            ModelBuffer targetBuffer);


        /// <summary>
        /// Gets data from buffer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetBuffer">The name of the buffer from which you want to get data</param>
        /// <returns>Return buffer list</returns>
        List<T> GetBufferList<T>(ModelBuffer targetBuffer);

        /// <summary>
        /// Set data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        long SetData(params object[] data);

        /// <summary>
        /// Copies a range of rows from one repository to another, or from one buffer to another within a single repository.
        /// </summary>
        /// <param name="startRow">The number of the first row you want to copy.</param>
        /// <param name="endRow">The number of the last row you want to copy.</param>
        /// <param name="sourceBuffer">identifying the buffer from which you want to copy the rows.</param>
        /// <param name="target">The name of the repository to which you want to copy the rows.</param>
        /// <param name="beforeRow">The number of the row before which you want to insert the copy rows. To insert after the last row, use any value that is greater than the number of existing rows.</param>
        /// <param name="targetBuffer">The name of the repository to which you want to copy the rows.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int RowsCopy(int startRow, int endRow, ModelBuffer sourceBuffer, IRepository target, int beforeRow,
            ModelBuffer targetBuffer);

        /// <summary>
        /// Specifies filter criteria for a repository.
        /// </summary>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the filter criteria. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int SetFilter(string expression);

        /// <summary>
        /// Displays rows in a repository that pass the current filter criteria. Rows that do not meet the filter criteria are moved to the filter buffer.
        /// The Filter method retrieves all rows before applying the filter.
        /// To change the filter criteria, use the SetFilter method. 
        /// </summary>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        int Filter();

        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="expression">A string whose value is a boolean expression that you want to use as the search criterion. The expression includes column names.</param>
        /// <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// –1 General error
        /// –5 Bad argument</returns>
        int? Find(string expression, int start, int end);

        /// <summary>
        /// Finds the next row in a repository in which data meets a specified condition.
        /// </summary>
        /// <param name="columnName">A string of column names for The expression.</param>
        /// <param name="searchFor">A string whose value is a boolean expression that you want to use as the search criterion. The expression.</param>
        ///  <param name="start">A value identifying the row location at which to begin the search. Start can be greater than the number of rows.</param>
        /// <param name="end">A value identifying the row location at which to end the search. End can be greater than the number of rows. To search backward, make end less than start.</param>
        /// <returns>Returns the number of the first row that meets the search criteria within the search range. Returns 0 if no rows are found and one of these negative numbers if an error occurs:
        /// –1 General error
        /// –5 Bad argument</returns>
        int? Find(string columnName, string searchFor, int start, int end);

        /// <summary>
        /// Specifies sort criteria for a repository.
        /// </summary>
        /// <param name="expression">
        /// A string whose value is valid sort criteria for the repository. The expression includes column names or numbers. A column number must be preceded by a pound sign (#).
        /// A repository can have sort criteria specified as part of its definition. SetSort overrides the definition, providing new sort criteria for the repository. However, it does not actually sort the rows. Call the Sort method to perform the actual sorting.
        /// </param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int SetSort(string expression);

        /// <summary>
        /// Sort criteria for a newly created repository. To specify sorting for existing repository.
        /// </summary>
        /// <param name="expression">
        /// A string whose value represents valid sort criteria.
        /// </param>
        /// <returns>Returns 1 if it succeeds and -1 if an error occurs. </returns>
        int Sort(string expression = "");

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <returns>The return value identifies the status of the item at row, column in the specified buffer.</returns>
        ModelAction GetItemStatus(int row, object column, ModelBuffer buffer);

        /// <summary>
        /// Reports the modification status of a row or a column within a row
        /// </summary>
        /// <param name="row">A value identifying the row for which you want the status.</param>
        /// <param name="column">The column for which you want the status. Column can be a column number or a column name. 
        /// Specify 0 to get the status of the whole row.</param>
        /// <param name="buffer">A value identifying the buffer containing the row for which you want status</param>
        /// <param name="status">A value that specifying the new status.</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int SetItemStatus(int row, object column, ModelBuffer buffer, ModelAction status);

        /// <summary>
        /// Get objects that have knowledge about the data they are retrieving.
        /// </summary>
        /// <returns>Returns a string that contains names of columns.</returns>
        string ColumnsNames();

        /// <summary>
        /// Obtains the value of an item in a value list or code table associated with a column in a repository.
        /// </summary>
        /// <param name="row">The number of the item in the value list or the code table for the edit style.</param>
        /// <param name="column">The column for which you want the item. Column can be a column number (integer) or a column name (string)</param>
        /// <returns>
        /// Returns the item identified by index in the value list or the code table associated with column of repository. Returns the empty string (“ ”) if the index is not valid or the column does not have a value list or code table. 
        /// </returns>
        string GetValue(object column, int row, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary);

        /// <summary>
        /// Clears all the data from a repository
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs. </returns>
        int Reset();


        /// <summary>
        /// Saves the data in XML format.
        /// </summary>
        /// <param name="filename">File name</param>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs</returns>
        int SaveToXml(string filename);


        /// <summary>
        /// Clears the update flags in the primary and filter buffers and empties the delete buffer
        /// </summary>
        /// <returns>Returns 1 if it succeeds and –1 if an error occurs.</returns>
        int ResetUpdate();

        /// <summary>
        /// Inserts data into a repository control tab-separated, comma-separated, or XML data in a string.
        /// </summary>
        /// <param name="importedString">A string from which you want to copy the data. The string should contain tab-separated or comma-separated columns or XML with one row per line </param>
        /// <returns></returns>
        int ImportString(string importedString);


        /// <summary>
        /// Print
        /// </summary>
        /// <returns></returns>
        int Print();

        /// <summary>
        /// Printing to PrintBOS API
        /// </summary>
        /// <param name="DeviceID"> ID of device </param>
        /// <param name="UserID"> ID of user </param>
        /// <param name="Password"> Password </param>
        int Print(string DeviceID, string UserID, string Password,string templateName,string PrinterName);

        /// <summary>
        /// Reports the SQL SELECT statement
        /// </summary>
        /// <returns>Returns the current SQL SELECT statement</returns>
        string GetSQLSelect();

        /// <summary>
        /// Reports the number of rows that are not displayed in the DataWindow because of the current filter criteria.
        /// </summary>
        /// <returns>Returns the number of rows in repository that are not displayed because they do not meet the current filter criteria. Returns 0 if all rows are displayed</returns>
        int FilteredCount();

        void Export(string xml);

        /// <summary>
        /// Accesses a single row when specifying the row number. It accesses all the data in the DataWindow control when omitting the row number.
        /// </summary>
        /// <param name="row">The number of the row you want to access. To access data for all rows, omit rownum.</param>
        /// <param name="dataSource">The source of the data.</param>
        /// <param name="buffer">The name of the buffer from which you want to get or set data.</param>
        /// <returns></returns>
        List<ModelBase> GetDataRows(int row = -1, ModelDataSource dataSource = ModelDataSource.Current, ModelBuffer buffer = ModelBuffer.Primary);

        /// <summary>
        /// Sets data to Element
        /// </summary>
        /// <param name="list">List of data</param>
        void SetDataRows(List<ModelBase> list);


        /// <summary>
        /// Sets data to Element
        /// </summary>
        /// <param name="array">Array of data</param>
        void SetDataRows(ModelBase[] array);

        /// <summary>
        /// Gets sort expression
        /// </summary>
        /// <returns>Returns string of sort expression</returns>
        string GetSort();

        /// <summary>
        /// Gets the filter expression
        /// </summary>
        /// <returns>Returns filter expression</returns>
        string GetFilter();

        /// <summary>
        /// Get the number of columns in the repository.
        /// </summary>
        /// <returns></returns>
        int ColumnCount();

        /// <summary>
        /// Gets DataTable
        /// </summary>
        /// <returns>Return DataTable</returns>
        DataTable GetDataTable();

        /// <summary>
        /// Gets PrimaryList of repository
        /// </summary>
        /// <returns>Returns PrimaryList</returns>
        IList GetPrimaryList();
        //gilad test
        void AppendPrimaryList(ModelBase model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startRow"></param>
        /// <param name="endRow"></param>
        /// <param name="buffer"></param>
        /// <returns></returns>
        int RowsDiscard(int startRow, int endRow, ModelBuffer buffer);

        /// <summary>
        /// Gets or sets a ReadOnly property
        /// </summary>
        bool ReadOnly { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int FirstRowOnPage();

        /// <summary>
        /// Gets type of repository model
        /// </summary>
        /// <returns>Returns type of model</returns>
        Type GetTypeOfModel();

        /// <summary>
        /// Gets a LookupTables property
        /// </summary>
        Dictionary<string, Dictionary<string, object>> LookupTables { get; }

        /// <summary>
        /// Gets column name
        /// </summary>
        /// <returns>Returns name of column</returns>
        string GetColumnName();

        /// <summary>
        /// Returns an integer containing the number of the current row in the DataStore.
        /// </summary>
        /// <returns></returns>
        long GetRow();

        void GetXML(out XmlDocument document, out string name);

        void SetXMLGroup(string expression);

        string GetXMLGroup();

        void SetXMLGroupMovePropertiesToAttribute(string expression);

        string GetXMLGroupMovePropertiesToAttribute();

        void RowsDiscard(int row);

        int Print(string deviceID, string userID, string password, string PrinterName, params IRepository[] repository);
    }
}
