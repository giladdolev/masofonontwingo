﻿using System.Collections.Generic;

namespace Common.Transposition.Extensions
{
    public interface IUnitOfWork
    {
        void Execute(string query, Dictionary<string, object> parameters);
    }
}
