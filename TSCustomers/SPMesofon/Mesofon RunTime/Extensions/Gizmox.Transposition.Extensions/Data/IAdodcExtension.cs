﻿namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Interface that enables to add AdodcExtension class to System.Windows.Forms.Extensions
    /// </summary>
    public interface IAdodcExtension
    {
        /// <summary>
        /// Gets the binding source.
        /// </summary>
        /// <returns></returns>
        object GetBindingSource { get; }
    }
}
