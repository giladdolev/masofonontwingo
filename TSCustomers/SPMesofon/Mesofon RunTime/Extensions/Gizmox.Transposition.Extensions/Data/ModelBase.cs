﻿namespace Common.Transposition.Extensions
{
    public enum ModelAction
    {
        None,
        Delete,
        Insert,
        UpdateByKeys,
        UpdateByFields,
        DeleteThenInsert,
        Discard
    }

    public enum ModelBuffer
    {
        Primary,
        Delete,
        Filter
    }

    public enum ModelDataSource
    {
        Current,
        Original
    }
    public class ModelBase
    {
        private ModelBase _original;
        public ModelAction Action { get; set; }
        public ModelBuffer Buffer { get; set; }

        public ModelBase Original
        {
            get
            {
                return _original ?? this;
            }
        }

        protected void PropertyChanged(string propertyName)
        {
            SetOriginal();
        }

        private void SetOriginal()
        {
            if (_original == null)
            {
                _original = (ModelBase) this.MemberwiseClone();
            }
        }

        protected void ResetOriginal()
        {
            _original = null;
        }
    }
}
