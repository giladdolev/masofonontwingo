﻿using System.Collections.Generic;

namespace Common.Transposition.Extensions
{
    public class CodeTable
    {
        private string _columnName;
        private Dictionary<string, string> _table;
        public CodeTable(string ColumnName)
        {
            _columnName = ColumnName;
        }

        public Dictionary<string, string> Table
        {
            get
            {
                if (_table == null)
                {
                    _table = new Dictionary<string, string>();
                }
                return _table;
            }

        }

        public string ColumnName
        {
            get
            {
                return _columnName;
            }
        }
    }
}
