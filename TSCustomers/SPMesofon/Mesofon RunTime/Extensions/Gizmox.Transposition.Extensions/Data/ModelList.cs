﻿using System.Collections.Generic;

namespace Common.Transposition.Extensions.Data
{
    public class ModelList<T> : List<T>, IRepositoryContainer where T : ModelBase, new()
    {
         public ModelList(IRepository repository, IEnumerable<T> list):base(list)
        {
            Repository = repository;
            
        }
        public IRepository Repository { get; private set; }

    }
}
