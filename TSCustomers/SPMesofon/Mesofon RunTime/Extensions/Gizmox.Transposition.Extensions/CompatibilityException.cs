using System;
using System.Globalization;
using System.Runtime.Serialization;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
using System.Web;
using System.IO;
using System.Diagnostics;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Handles Exceptions - Should be compatible with VB6 and ASP ErrObject
    /// </summary>
    [Serializable()]
    public sealed class CompatibilityException : Exception
    {
        #region Fields

        /// <summary>
        /// Error number
        /// </summary>
        private int mintNumber = 0;

        /// <summary>
        /// File name
        /// </summary>
        private string mstrFile = String.Empty;

        /// <summary>
        /// Line number
        /// </summary>
        private int mintLine = 0;

        /// <summary>
        /// Column Number
        /// </summary>
        private int mintColumn = 0;


        /// <summary>
        /// The context ID for a topic in a Help file
        /// </summary>
        private int mHelpContext = 0;

        /// <summary>
        /// The fully qualified path to a Help file
        /// </summary>
        private string mHelpFile = string.Empty;

        #endregion

        #region Contructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CompatibilityException"/> class.
        /// </summary>
        public CompatibilityException()
        {

        }

        /// <summary>
        /// Deserializer constructor
        /// </summary>
        /// <param name="info">SerializationInfo object</param>
        /// <param name="ctx">StreamingContext object</param>
        private CompatibilityException(SerializationInfo info, StreamingContext ctx)
            : base(info, ctx)
        {
            // Get error number
            mintNumber = info.GetInt32("number");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(Exception objInnerException)
            : this(objInnerException == null ? string.Empty : objInnerException.Message, objInnerException)
        {

        }

        public CompatibilityException(string strMessage)
            : base(strMessage)
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strMessage">The error message</param>
        public CompatibilityException(int intErrorCode, string strMessage)
            : base(strMessage)
        {
            //Save the value of the error code
            mintNumber = intErrorCode;

            // Set the error number in Information.Err object
            Information.Err().Number = mintNumber;

            // Set the error description in Information.Err object
            Information.Err().Description = strMessage;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strMessage">The error message</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(int intErrorCode, string strMessage, Exception objInnerException)
            : this(strMessage, objInnerException)
        {
            //Save the value of the error code
            mintNumber = intErrorCode;

            // Set the error number in Information.Err object
            Information.Err().Number = mintNumber;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strMessage">The error message</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(string strMessage, string strSource, Exception objInnerException)
            : this(0, strSource, strMessage, objInnerException)
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="strMessage">The error message</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(string strMessage, Exception objInnerException)
            : base(strMessage, objInnerException)
        {

            // If it's a COMException set the error number to  the error code inside the COMException object
            COMException objCOMException = objInnerException as COMException;
            if (objCOMException != null)
            {
                mintNumber = objCOMException.ErrorCode;
            }

            // If it's a web Error, get the error code from WebEventCode property
            HttpUnhandledException objHttpUnhandledException = objInnerException as HttpUnhandledException;
            if (objHttpUnhandledException != null)
            {
                mintNumber = objHttpUnhandledException.WebEventCode;

                // Get the current HTTPContext
                HttpContext objContext = HttpContext.Current;

                if (objContext != null)
                {
                    // Get file name from path in request
                    mstrFile = Path.GetFileName(objContext.Request.PhysicalPath);
                }
            }

            if (objInnerException != null)
            {
                // Create a StackTrace object
                StackTrace objStackTrace = new StackTrace(objInnerException, true);
                // Get the top stack frame
                StackFrame objStackFrame = objStackTrace.GetFrame(0);
                // Get the line number from the stack frame

                // Get the line number from StackTrace object
                mintLine = objStackFrame.GetFileLineNumber();
                // Get the column number from StackTrace object
                mintColumn = objStackFrame.GetFileColumnNumber();
            }

            // Set the error number in Information.Err object
            Information.Err().Number = mintNumber;

            // Set the error description in Information.Err object
            Information.Err().Description = strMessage;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="strMessage">The error message</param>
        public CompatibilityException(int intErrorCode, string strSource, string strMessage)
            : this(intErrorCode, strMessage)
        {
            // Save the value of source
            Source = strSource;

            // Set the source of Information.Err object
            Information.Err().Source = strSource;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="intErrorCode">The error code</param>
        /// <param name="strSource">The source of the error</param>
        /// <param name="strMessage">The error message</param>
        /// <param name="objInnerException">An internal Exception object</param>
        public CompatibilityException(int intErrorCode, string strSource, string strMessage, Exception objInnerException)
            : this(intErrorCode, strMessage, objInnerException)
        {
            // Save the value of source
            Source = strSource;

            // Set the source of Information.Err object
            Information.Err().Source = strSource;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Returns the error number
        /// </summary>
        public int Number
        {
            get { return mintNumber; }
        }

        /// <summary>
        /// Returns a string that indicates whether the error was generated by IIS, a scripting language, or a component.
        /// </summary>
        public string Category
        {
            get
            {
                // If the exception is not empty and the inner exception is not empty
                if (InnerException != null)
                {
                    // Return the inner exception name : this is an adapter behavior and might be wrong.
                    return InnerException.GetType().Name;
                }

                // Return the type of error
                return GetType().Name;
            }
        }

        /// <summary>
        /// Returns the file name of the Exception
        /// </summary>
        public string File
        {
            get { return mstrFile; }
        }

        /// <summary>
        /// Returns a short description of the error.
        /// </summary>
        public string Description
        {
            get { return Message; }
        }




        /// <summary>
        /// Indicates the line within the .aspx file that generated the error.
        /// </summary>
        public int Line
        {
            get { return mintLine; }
        }


        /// <summary>
        /// Returns a value that indicates the column position within the .asp file that generated the error.
        /// </summary>
        public int Column
        {
            get { return mintColumn; }
        }

        /// <summary>
        /// Mimics vb ErrObject 
        /// Returns the context ID for a topic in a Help file
        /// </summary>
        public int HelpContext
        {
            get { return mHelpContext; }
        }

        /// <summary>
        /// Mimics vb ErrObject 
        /// Returns a fully qualified path to a Help file
        /// </summary>
        public string HelpFile
        {
            get { return mHelpFile; }
        }

        /// <summary>
        /// Mimics vb ErrObject 
        /// Returns system error code from a call to a DLL
        /// </summary>
        public int LastDLLError
        {
            get { return Marshal.GetLastWin32Error(); }
        }

        /// <summary>
        /// Returns the Description of ASP exceptions
        /// </summary>
        public string ASPDescription
        {
            get { return Description; }
        }

        /// <summary>
        /// Returns the code of ASP exceptions
        /// </summary>
        public string ASPCode
        {
            get { return Number.ToString(CultureInfo.InvariantCulture); }
        }
        #endregion

        #region Serialization

        /// <summary>
        /// sets the System.Runtime.Serialization.SerializationInfo with information about the exception.
        /// </summary>
        /// <param name="objInfo">SerializationInfo object</param>
        /// <param name="objContext">StreamingContext object</param>
        public override void GetObjectData(SerializationInfo objInfo, StreamingContext objContext)
        {
            // Call the base
            base.GetObjectData(objInfo, objContext);

            // Add  "number" for the SerializationInfo object
            objInfo.AddValue("number", mintNumber);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Maps the exception automatic number.
        /// </summary>
        /// <param name="objException">The object Exception.</param>
        /// <returns>The number corresponding to the exception type.</returns>
        public static int MapExceptionToNumber(Exception objException)
        {
            if (objException == null)
            {
                return 0;
            }

            Type type = objException.GetType();
            if (type == typeof(IndexOutOfRangeException))
            {
                return 9;
            }
            if (type == typeof(RankException))
            {
                return 9;
            }
            if (type == typeof(DivideByZeroException))
            {
                return 11;
            }
            if (type == typeof(OverflowException))
            {
                return 6;
            }
            if (type == typeof(NotFiniteNumberException))
            {
                NotFiniteNumberException exception = (NotFiniteNumberException)objException;
                if (exception.OffendingNumber == 0.0)
                {
                    return 11;
                }
                return 6;
            }
            if (type == typeof(NullReferenceException))
            {
                return 0x5b;
            }
            if (objException is AccessViolationException)
            {
                return -2147467261;
            }
            if (type == typeof(InvalidCastException))
            {
                return 13;
            }
            if (type == typeof(NotSupportedException))
            {
                return 13;
            }
            if (type == typeof(COMException))
            {
                return ((COMException)objException).ErrorCode;
            }
            if (type == typeof(SEHException))
            {
                return 0x63;
            }
            if (type == typeof(DllNotFoundException))
            {
                return 0x35;
            }
            if (type == typeof(EntryPointNotFoundException))
            {
                return 0x1c5;
            }
            if (type == typeof(TypeLoadException))
            {
                return 0x1ad;
            }
            if (type == typeof(OutOfMemoryException))
            {
                return 7;
            }
            if (type == typeof(FormatException))
            {
                return 13;
            }
            if (type == typeof(DirectoryNotFoundException))
            {
                return 0x4c;
            }
            if (type == typeof(IOException))
            {
                return 0x39;
            }
            if (type == typeof(FileNotFoundException))
            {
                return 0x35;
            }
            if (objException is MissingMemberException)
            {
                return 0x1b6;
            }
            if (objException is InvalidOleVariantTypeException)
            {
                return 0x1ca;
            }

            HttpUnhandledException objHttpUnhandledException = objException as HttpUnhandledException;
            if (objHttpUnhandledException != null)
            {
                return objHttpUnhandledException.WebEventCode;
            }
            return 0;
        }

        #endregion
    }
}
