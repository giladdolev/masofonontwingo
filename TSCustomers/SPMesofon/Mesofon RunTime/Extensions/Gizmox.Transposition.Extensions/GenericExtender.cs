using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// GenericExtender class 
    /// </summary>
    public static class GenericExtender
	{

        // Create component events accessor
        private static Func<Component, EventHandlerList> componentEventsAccessor = CreateComponentEventsAccessor();
        private static object extenderEventKey = new object();



        /// <summary>
        /// Creates the component events accessor.
        /// </summary>
        /// <returns>A delegate of type TDelegate that represents the compiled lambda expression.</returns>
        private static Func<Component, EventHandlerList> CreateComponentEventsAccessor()
        {
            // Define param
            ParameterExpression param = Expression.Parameter(typeof(Component));

            // Access the Events property
            MemberExpression prop = Expression.Property(param, "Events");

            // Compile expression
            Expression<Func<Component, EventHandlerList>> exp = Expression.Lambda<Func<Component, EventHandlerList>>(prop, param);
            return exp.Compile();
        }


		/// <summary>
		/// Gets the or create extender.
		/// </summary>
		/// <param name="objComponent">The component.</param>
        /// <returns>ComponentExtender object.</returns>
		private static Dictionary<object, object> GetOrCreateExtender(Component objComponent)
		{
            if (objComponent == null)
                return null;

            // Get the omponent events list
            EventHandlerList componentEvents = componentEventsAccessor(objComponent);
            if (componentEvents == null)
                return null;

            ComponentExtender extender;

            // Get the extender event delegate
            Delegate extenderEvent = componentEvents[extenderEventKey];
            if (extenderEvent == null)
            {
                // Create extender and handler delegate
                extender = new ComponentExtender();
                componentEvents[extenderEventKey] = new EventHandler(extender.DoNothing);
            }
            else
            {
                // Get the extender from the handler delegate
                extender = extenderEvent.Target as ComponentExtender;
            }

            return extender;
		}


		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="objComponent">The obj component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objValue">The value.</param>
		public static void SetValue(Component objComponent, object objKey, object objValue)
		{
			// Set a simple (non-indexed) value in the extender
			SetValue(objComponent, objKey, null, objValue);
		}


		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objIndex">Index of the parameterized property.</param>
		/// <param name="objValue">The value.</param>
		public static void SetValue(Component objComponent, object objKey, object objIndex, object objValue)
		{
			// if value already cached
			if (ValueExistInExtender(objComponent, objKey, objIndex))
			{
				// Erase it to avoid duplications or for override purposes
				RemoveValueFromExtender(objComponent, objKey, objIndex);
			}
			
			// Get the extender for the Component (if missing, create new one)
			Dictionary<object, object> objComponentExtender = GetOrCreateExtender(objComponent);
			
			// Exit method if coudn't get nor create extender for the component
			if (objComponentExtender == null)
			{
				return;
			}
			
			// Process indexed property
			if (objIndex != null)
			{
				object objExtenderValues = null;
				Dictionary<object, object> objValuesStorage = null;
				
				// Get property storage
				if (objComponentExtender.TryGetValue(objKey, out objExtenderValues))
				{
					// Use existing storage collection
					objValuesStorage = objExtenderValues as Dictionary<object, object>;
				}

				else
				{
					// Initialize storage
					objValuesStorage = new Dictionary<object, object>();
					
					// Save storage to global cache
					objComponentExtender.Add(objKey, objValuesStorage);
				}
				// Update property (indexer) value
				objValuesStorage.Add(objIndex, objValue);
			}

			else
			{
				// Set a simple extension value
				objComponentExtender.Add(objKey, objValue);
			}
		}

        /// <summary>
        /// Gets or creates the value.
        /// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The  key.</param>
        /// <returns>Returns the object of type.</returns>
		public static T GetOrCreateValue<T>(Component objComponent, object objKey) where T : new()
		{
			bool blnCreate = true;
			return GetOrCreateValue<T>(objComponent, objKey, blnCreate);
		}


		/// <summary>
		/// Gets or creates the value.
		/// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The  key.</param>
		/// <param name="blnCreate">if set to <c>true</c> create value if value not found.</param>
        /// <returns>Returns the object of type.</returns>
		public static T GetOrCreateValue<T>(Component objComponent, object objKey, bool blnCreate) where T : new()
		{
			return GetOrCreateValue<T>(objComponent, objKey, null, blnCreate);
		}

        /// <summary>
        /// Gets or creates the value.
        /// </summary>
        /// <typeparam name="T">Type parameter</typeparam>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The  key.</param>
        /// <param name="objIndex">Object value.</param>
        /// <returns>Returns the object of type.</returns>
        public static T GetOrCreateValue<T>(Component objComponent, object objKey, object objIndex) where T : new()
		{
			bool blnCreate = true;
			return GetOrCreateValue<T>(objComponent, objKey, objIndex, blnCreate);
		}


		/// <summary>
		/// Gets the or create value.
		/// </summary>
        /// <typeparam name="T">Type parameter.</typeparam>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objIndex">Index of the parameterized property.</param>
		/// <param name="blnCreate">if set to <c>true</c>create value if value not found.</param>
        /// <returns>Returns the object of type.</returns>
		public static T GetOrCreateValue<T>(Component objComponent, object objKey, object objIndex, bool blnCreate) where T : new()
		{
			return GetValue<T>(objComponent, objKey, objIndex, blnCreate, delegate { return new T(); });
		}


		/// <summary>
		/// Gets the value or default. Use to retrieve primitive types
		/// </summary>
		/// <typeparam name="T">Type parameter.</typeparam>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objIndex">Index of the obj.</param>
		/// <param name="objDefaultValue">The default value.</param>
        /// <returns>Returns the object of type.</returns>
		public static T GetValueOrDefault<T>(Component objComponent, object objKey, object objIndex, T objDefaultValue)
		{
			return GetValue<T>(objComponent, objKey, objIndex, true, delegate { return objDefaultValue; });
		}


		/// <summary>
		/// Gets the value or default. Use to retrieve primitive types
		/// </summary>
		/// <typeparam name="T">Type parametar.</typeparam>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objDefaultValue">The default value.</param>
        /// <returns>Returns the object of type.</returns>
		public static T GetValueOrDefault<T>(Component objComponent, object objKey, T objDefaultValue)
		{
			return GetValueOrDefault<T>(objComponent, objKey, null, objDefaultValue);
		}


		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <typeparam name="T">Type parametar.</typeparam>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objIndex">Index of the obj.</param>
		/// <param name="blnCreate">if set to <c>true</c> [BLN create].</param>
		/// <param name="objCreateValue">The create value.</param>
		/// <returns>Returns the object of type.</returns>
		private static T GetValue<T>(Component objComponent, object objKey, object objIndex, bool blnCreate, Func<T> objCreateValue)
		{
			// Initialize component extensions store
			Dictionary<object, object> objComponentExtender = GetOrCreateExtender(objComponent);
			
			// Exit method if couldn't get nor create extender for the component
			if (objComponentExtender == null)
			{
				return default(T);
			}
			
			object objValue = null;
			object objExtenderValues = null;
			
			// Initialize a ValuesStorage object
			Dictionary<object, object> objValuesStorage = null;
			
			// If an indexer was passed to the method
			if (objIndex != null)
			{
				// If value is not found for the specific key
				if (!objComponentExtender.TryGetValue(objKey, out objExtenderValues))
				{
					if (blnCreate)
					{
						// Cast the value to a collection
						objValuesStorage = new Dictionary<object, object>();
						
						// Set value to a new T instance
						objValue = objCreateValue();
						
						// Add the new T instance to the ValueStorage
						objValuesStorage.Add(objIndex, objValue);
						
						// Add new extender to component extender object
						objComponentExtender.Add(objKey, objValuesStorage);
					}
				}

				else
				// if there is already an ExtenderValue associated with the objKey
				{
					// Cast the ExtenderValue to a Dictionary type
					objValuesStorage = objExtenderValues as Dictionary<object, object>;
					
					// Get the value that was requested from the method
					if (!objValuesStorage.TryGetValue(objIndex, out objValue) && blnCreate)
					{
						// Set value to a new T instance
						objValue = objCreateValue();
						
						// Add the new T instance to the ValueStorage
						objValuesStorage.Add(objIndex, objValue);
					}
				}
			}

			else
			// if NO indexer was passed to the method
			{
				if (!objComponentExtender.TryGetValue(objKey, out objValue) && blnCreate)
				{
					// Set value to a new T instance
					objValue = objCreateValue();
					
					// Add the new T instance to the ValueStorage
					objComponentExtender.Add(objKey, objValue);
				}
			}
			
			try
			{
				return (T)objValue;
			}
			catch
			{
			}
			return default(T);
		}

        /// <summary>
        /// Removes the value from extender.
        /// </summary>
        /// <param name="objComponent">The component object.</param>
        /// <param name="objKey">The key object.</param>
		public static void RemoveValueFromExtender(Component objComponent, object objKey)
		{
			object objIndex = default(object);
			RemoveValueFromExtender(objComponent, objKey, objIndex);
		}


		/// <summary>
		/// Removes the value from extender.
		/// </summary>
		/// <param name="objComponent">The component object.</param>
		/// <param name="objKey">The key object.</param>
		/// <param name="objIndex">Index of the value.</param>
		public static void RemoveValueFromExtender(Component objComponent, object objKey, object objIndex)
		{
			// Get the extender object
			Dictionary<object, object> objExtender = GetOrCreateExtender(objComponent);
			
			// if Extender object is NOT NULL
			if (objExtender != null && objKey != null)
			{
				// if Index object is NOT NULL
				if (objIndex != null)
				{
					object objExtenderValues;
					
					// Initialize dictionary object
					Dictionary<object, object> objValuesStorage = null;
					
					// If the value exists with that key
					if (objExtender.TryGetValue(objKey, out objExtenderValues))
					{
						// Cast the variable
						objValuesStorage = objExtenderValues as Dictionary<object, object>;
						
						// Remove the item from the collection
						objValuesStorage.Remove(objIndex);
					}
				}

				else
				{
					// Remove the value from the Extenders object
					objExtender.Remove(objKey);
				}
			}
		}

        /// <summary>
        /// Values the exist in extender.
        /// </summary>
        /// <param name="objComponent">The component.</param>
        /// <param name="objKey">The key.</param>
        /// <returns>Boolean value.</returns>
		public static bool ValueExistInExtender(Component objComponent, object objKey)
		{
			object objIndex = default(object);
			return ValueExistInExtender(objComponent, objKey, objIndex);
		}


		/// <summary>
		/// Values the exist in extender.
		/// </summary>
		/// <param name="objComponent">The component.</param>
		/// <param name="objKey">The key.</param>
		/// <param name="objIndex">The Index.</param>
		/// <returns>Boolean value.</returns>
		public static bool ValueExistInExtender(Component objComponent, object objKey, object objIndex)
		{
			// Get the extender object
			Dictionary<object, object> objExtender = GetOrCreateExtender(objComponent);
			
			// If Component Extender is NULL
			if (objExtender == null)
			{
				return false;
			}
			
			// Initialize variable for the Values
			object objExtenderValues = null;
			
			// Initialize variable for the "ValueExist" flag
			bool blnValueExist = false;
			
			// if component and key are NOT NULL
			if (objComponent != null && objKey != null)
			{
				// if Component Extender has value associated with the Key
				if (objExtender.TryGetValue(objKey, out objExtenderValues))
				{
					// If indexed property
					if (objIndex != null)
					{
						// Cast the extender values to a dictionary  
						Dictionary<object, object> objValueStorage = objExtenderValues as Dictionary<object, object>;
						
						// Check if there is a Value in the Storage Container
						blnValueExist = objValueStorage.ContainsKey(objIndex);
					}

					else
					// if simple property
					{
						// Value exists
						blnValueExist = true;
					}
				}
			}
			return blnValueExist;
		}

	}

}
