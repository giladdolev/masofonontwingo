﻿using System.ComponentModel;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Base class of the extension definition classes
    /// </summary>
    public class ExtendedPropertiesBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedPropertiesBase"/> class.
        /// </summary>
        public ExtendedPropertiesBase()
        {
            // Sets default values for the class properties .
            SetClassPropertiesDefaultValue();
        }

        /// <summary>
        /// Sets default values for the class properties .
        /// </summary>
        protected void SetClassPropertiesDefaultValue()
        {
            // Iterates through all properties
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(this))
            {
                // Gets properties with DefaultValue attribute  
                var myAttribute = (DefaultValueAttribute)property.Attributes[typeof(DefaultValueAttribute)];

                if (myAttribute != null)
                {
                    // Sets the default value
                    property.SetValue(this, myAttribute.Value);
                }
            }
        }
    }
}
