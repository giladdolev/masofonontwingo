﻿using System.Collections.Generic;

namespace Common.Transposition.Extensions
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Adds range of items into collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (collection == null)
            {
                return;
            }

            if (items == null)
            {
                return;
            }
            foreach (var item in items)
            {
                collection.Add(item);
            }
        }
    }
}
