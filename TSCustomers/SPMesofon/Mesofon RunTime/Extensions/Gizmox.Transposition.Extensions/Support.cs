﻿using System;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Support class
    /// </summary>
    public static class Support
    {
        /// <summary>
        /// Rate twips per PixelX
        /// </summary>
        private static double mTwipsPerPixelX;
        /// <summary>
        /// Rate twips per PixelY
        /// </summary>
        private static double mTwipsPerPixelY;
        /// <summary>
        /// Indicate if the twips per pixel is set
        /// </summary>
        private static bool mIsTwipsPerPixelSetUp;

        /// <summary>
        /// One char horizontal measures 120 twips
        /// </summary>
        public const double TWIPS_PER_CHAR_HORIZONTAL = 120;
        /// <summary>
        /// One char vertical measures 240 twips
        /// </summary>
        public const double TWIPS_PER_CHAR_VERTICAL = 240;
        /// <summary>
        /// One inch measures 1440 twips
        /// </summary>
        public const double TWIPS_PER_INCH = 1440;
        /// <summary>
        /// One point measure 20 twips
        /// </summary>
        public const double TWIPS_PER_POINT = 20;
        /// <summary>
        /// One centimeter measures 566.92913385826773 twips
        /// </summary>
        public const double TWIPS_PER_CM = 566.92913385826773;
        /// <summary>
        /// One millimeter measures 56.692913385826778 twips
        /// </summary>
        public const double TWIPS_PER_MM = 56.692913385826778;
        /// <summary>
        /// One himetric measures 0.56692913385826771 twips
        /// </summary>
        public const double TWIPS_PER_HIMETRIC = 0.56692913385826771;
        /// <summary>
        /// One inch measures 25.4mm
        /// </summary>
        public const double MM_PER_INCH = 25.4;
        /// <summary>
        /// One inch measures 2.54cm
        /// </summary>
        public const double CM_PER_INCH = 2.54;
        /// <summary>
        /// One inch measures 2540.0 himetric
        /// </summary>
        public const double HIMETRIC_PER_INCH = 2540.0;
        /// <summary>
        /// One millimeter measures 100 himetric
        /// </summary>
        public const int HIMETRIC_PER_MM = 100;


        /// <summary>
        /// Gets a value that is used to convert twips to pixels based on screen settings.
        /// </summary>
        /// <returns>A Double that contains the conversion factor.</returns>
        public static float TwipsPerPixelX()
        {
            SetUpTwipsPerPixel();
            return (float)mTwipsPerPixelX;
        }

        /// <summary>
        /// Gets a value that is used to convert twips to pixels based on screen settings.
        /// </summary>
        /// <returns>A Double that contains the conversion factor.</returns>
        public static float TwipsPerPixelY()
        {
            SetUpTwipsPerPixel(false);
            return (float)mTwipsPerPixelY;
        }

        /// <summary>
        /// Set up twips per pixel
        /// </summary>
        /// <param name="Force">Bool expression used to forcefully set twips per pixels.</param>
        private static void SetUpTwipsPerPixel(bool Force = false)
        {
            if (!mIsTwipsPerPixelSetUp || Force)
            {
                int intResult = 0;
                mTwipsPerPixelX = 0.0;
                mTwipsPerPixelY = 0.0;
                try
                {
                    IntPtr dC = NativeMethods.GetDC(NativeMethods.NullIntPtr);
                    if (!dC.Equals(NativeMethods.NullIntPtr))
                    {
                        mTwipsPerPixelX = 1440.0 / ((double)NativeMethods.GetDeviceCaps(dC, 0x58));
                        mTwipsPerPixelY = 1440.0 / ((double)NativeMethods.GetDeviceCaps(dC, 90));
                        intResult = NativeMethods.ReleaseDC(NativeMethods.NullIntPtr, dC);
                    }
                }
                catch(Exception)
                {
                    // If the DC was released, the return value is 1, Otherwise 0
                    if (intResult == 0)
                    {
                        throw;    
                    }
                    
                }
                mIsTwipsPerPixelSetUp = true;
                if ((mTwipsPerPixelX == 0.0) || (mTwipsPerPixelY == 0.0))
                {
                    mTwipsPerPixelX = 15.0;
                    mTwipsPerPixelY = 15.0;
                }
            }
        }
    }
}
