using System;
using System.Runtime.InteropServices;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Simulate objects references manager the same way as VB6 does
    /// </summary>
    [ComVisible(true)]
    public abstract class RefObject : IRefObject
	{
        /// <summary>
        /// The reference count
        /// </summary>
        private int mintReferenceCount;

        /// <summary>
        /// Adds the reference.
        /// </summary>
        public void AddRef()
        {
            // Increase the reference count
            mintReferenceCount++;
        }

        /// <summary>
        /// Releases the reference.
        /// </summary>
        public void ReleaseRef()
        {
            // Decrease the count
            mintReferenceCount--;

            // Check if more references exist
            if (mintReferenceCount == 0)
            {
                // Invoke dispose
                CallDispose();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void CallDispose()
        {
            IDisposable objMe = this as IDisposable;
            if (objMe != null)
            {
                objMe.Dispose();
            }
        }
        
	}
}
