using System.Linq;

namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Manage the references counter
    /// </summary>
    public static class RefCounter
    {
        /// <summary>
        /// Adds object reference.
        /// </summary>
        /// <typeparam name="T">The type parameter.</typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns>The object with added reference.</returns>
        public static T AddRef<T>(T obj)
        {
            IRefObject objTmp = obj as IRefObject;

            // Add reference only for object of type IRefObject
            if (objTmp != null)
            {
                objTmp.AddRef();
            }

            return obj;
        }

        /// <summary>
        /// Adds objects reference
        /// </summary>
        /// <param name="objs">The objects.</param>
        public static void AddRef(params object[] objs)
        {
            // Input validation
            if (objs == null)
            {
                return;
            }

            // Add reference only for objects of type IRefObject
            foreach (IRefObject obj in objs.OfType<IRefObject>())
            {
                (obj).AddRef();
            }
        }


        /// <summary>
        /// Removes the references.
        /// </summary>
        /// <param name="objs">The objects.</param>
        public static void RemoveRef(params object[] objs)
        {
            // Input validation
            if (objs == null)
            {
                return;
            }

            // Release reference only for objects of type IRefObject
            foreach (IRefObject obj in objs.OfType<IRefObject>())
            {
                (obj).ReleaseRef();
            }
        }


        /// <summary>
        /// Releases target reference and adds source reference.
        /// </summary>
        /// <typeparam name="T">The type parameter.</typeparam>
        /// <param name="target">The target.</param>
        /// <param name="source">The source.</param>
        public static void SetRef<T>(ref T target, T source)
        {
            IRefObject targetMimic = target as IRefObject;

            // Release target reference
            if (targetMimic != null)
            {
                targetMimic.ReleaseRef();
            }

            // target object is now equal to source (since target is ByRef)
            target = source;
            targetMimic = target as IRefObject;

            // Add source reference
            if (targetMimic != null)
            {
                targetMimic.AddRef();
            }
        }
    }
}
