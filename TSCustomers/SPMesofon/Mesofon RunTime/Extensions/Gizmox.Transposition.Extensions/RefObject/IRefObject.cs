﻿namespace Common.Transposition.Extensions
{
    /// <summary>
    /// Simulate objects references manager the same way as VB6 does
    /// </summary>
    interface IRefObject
    {
        /// <summary>
        /// Adds the object reference
        /// </summary>
        void AddRef();

        /// <summary>
        /// Releases object reference
        /// </summary>
        void ReleaseRef();
    }
}
